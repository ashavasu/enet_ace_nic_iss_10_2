/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: laprplow.c,v 1.142.2.1 2018/03/16 13:44:08 siva Exp $
* Description: Protocol Low Level Routines
*********************************************************************/
#include "lahdrs.h"

#include "iss.h"
#include "lacli.h"
#include "cli.h"

extern UINT4        fsla[8];
extern UINT4        stdla[6];
extern UINT4        FsLaSystemControl[10];
extern UINT4        FsLaPortChannelDLAGDistributingPortList[12];
extern UINT4        FsLaPortChannelSelectionPolicyBitList[12];
PRIVATE VOID        LaNotifyProtocolShutdownStatus (VOID);
PRIVATE INT4        LaCheckLaStaticPortSpeed (UINT4 *pu4ErrorCode,
                                              tLaLacAggEntry * pAggEntry,
                                              INT4
                                              i4PortChannelDefaultPortIndex);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsLaSystemControl
Input       :  The Indices

The Object 
retValFsLaSystemControl
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaSystemControl (INT4 *pi4RetValFsLaSystemControl)
{
    *pi4RetValFsLaSystemControl = (INT4) LA_SYSTEM_CONTROL;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsLaStatus
Input       :  The Indices

The Object 
retValFsLaStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaStatus (INT4 *pi4RetValFsLaStatus)
{
    *pi4RetValFsLaStatus = (INT4) LA_PROTOCOL_ADMIN_STATUS ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaOperStatus
 Input       :  The Indices

                The Object 
                retValFsLaOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaOperStatus (INT4 *pi4RetValFsLaOperStatus)
{
    *pi4RetValFsLaOperStatus = (INT4) LA_MODULE_STATUS;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsLaTraceOption
Input       :  The Indices

The Object 
retValFsLaTraceOption
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaTraceOption (INT4 *pi4RetValFsLaTraceOption)
{
    *pi4RetValFsLaTraceOption = (INT4) gLaGlobalInfo.u4TraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsLaMaxPortsPerPortChannel
Input       :  The Indices

The Object 
retValFsLaMaxPortsPerPortChannel
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaMaxPortsPerPortChannel (INT4 *pi4RetValFsLaMaxPortsPerPortChannel)
{
    *pi4RetValFsLaMaxPortsPerPortChannel = LA_MAX_PORTS_PER_AGG;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsLaMaxPortChannels
Input       :  The Indices

The Object 
retValFsLaMaxPortChannels
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaMaxPortChannels (INT4 *pi4RetValFsLaMaxPortChannels)
{
    *pi4RetValFsLaMaxPortChannels = LA_MAX_AGG;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaActorSystemID
 Input       :  The Indices

                The Object 
                retValFsLaActorSystemID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaActorSystemID (tMacAddr * pRetValFsLaActorSystemID)
{
    LA_MEMCPY (pRetValFsLaActorSystemID,
               gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaNoPartnerIndep
 Input       :  The Indices

                The Object
                retValFsLaNoPartnerIndep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaNoPartnerIndep (INT4 *pi4RetValFsLaNoPartnerIndep)
{
    *pi4RetValFsLaNoPartnerIndep = gu4PartnerConfig;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGSystemStatus
 Input       :  The Indices

                The Object
                retValFsLaDLAGSystemStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGSystemStatus (INT4 *pi4RetValFsLaDLAGSystemStatus)
{

    *pi4RetValFsLaDLAGSystemStatus = (INT4) LA_DLAG_SYSTEM_STATUS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGSystemID
 Input       :  The Indices

                The Object
                retValFsLaDLAGSystemID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGSystemID (tMacAddr * pRetValFsLaDLAGSystemID)
{

    LA_MEMCPY (pRetValFsLaDLAGSystemID,
               (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr),
               LA_MAC_ADDRESS_SIZE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGSystemPriority
 Input       :  The Indices

                The Object
                retValFsLaDLAGSystemPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGSystemPriority (INT4 *pi4RetValFsLaDLAGSystemPriority)
{

    *pi4RetValFsLaDLAGSystemPriority =
        LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGPeriodicSyncTime
 Input       :  The Indices

                The Object
                retValFsLaDLAGPeriodicSyncTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGPeriodicSyncTime (UINT4 *pu4RetValFsLaDLAGPeriodicSyncTime)
{

    *pu4RetValFsLaDLAGPeriodicSyncTime =
        LA_DLAG_GLOBAL_INFO.u4GlobalDLAGPeriodicSyncTime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRolePlayed
 Input       :  The Indices

                The Object
                retValFsLaDLAGRolePlayed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRolePlayed (INT4 *pi4RetValFsLaDLAGRolePlayed)
{
    *pi4RetValFsLaDLAGRolePlayed = LA_AA_DLAG_ROLE_PLAYED;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGDistributingPortIndex
 Input       :  The Indices

                The Object
                retValFsLaDLAGDistributingPortIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGDistributingPortIndex (INT4
                                     *pi4RetValFsLaDLAGDistributingPortIndex)
{

    *pi4RetValFsLaDLAGDistributingPortIndex =
        (INT4) LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGDistributingPortList
 Input       :  The Indices

                The Object
                retValFsLaDLAGDistributingPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGDistributingPortList (tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsLaDLAGDistributingPortList)
{

    LA_MEMCPY (pRetValFsLaDLAGDistributingPortList->pu1_OctetList,
               LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingPortList,
               LA_PORT_LIST_SIZE);
    pRetValFsLaDLAGDistributingPortList->i4_Length = LA_PORT_LIST_SIZE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGClearCounters
 Input       :  The Indices

                The Object
                retValFsLaMCLAGClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGClearCounters (INT4 *pi4RetValFsLaMCLAGClearCounters)
{
    *pi4RetValFsLaMCLAGClearCounters = (INT4) gu1MclagClearCounter;
    return SNMP_SUCCESS;
}

/**************************************************************************
 Function    :  nmhGetFsLaClearStatistics
 Input       :  The Indices

                The Object 
                retValFsLaClearStatistics
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaClearStatistics (INT4 *pi4RetValFsLaClearStatistics)
{
    *pi4RetValFsLaClearStatistics = (INT4) gu1LacpClearCounter;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGSystemControl
 Input       :  The Indices

                The Object
                retValFsLaMCLAGSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGSystemControl (INT4 *pi4RetValFsLaMCLAGSystemControl)
{
    *pi4RetValFsLaMCLAGSystemControl = (INT4) gLaMclagSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGSystemStatus
 Input       :  The Indices

                The Object
                retValFsLaMCLAGSystemStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGSystemStatus (INT4 *pi4RetValFsLaMCLAGSystemStatus)
{
    *pi4RetValFsLaMCLAGSystemStatus = (INT4) LA_MCLAG_SYSTEM_STATUS;

    return SNMP_SUCCESS;
}

/***************************************************************************
 Function    :  nmhSetFsLaClearStatistics
 Input       :  The Indices

                The Object 
                setValFsLaClearStatistics
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaClearStatistics (INT4 i4SetValFsLaClearStatistics)
{
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    INT4                i4Index = LA_INIT_VAL;
    INT4                i4PrevIndex = LA_INIT_VAL;
    UINT2               u2AggIndex = LA_INIT_VAL;

    gu1LacpClearCounter = (UINT1) i4SetValFsLaClearStatistics;

    if (i4SetValFsLaClearStatistics != LA_ENABLED)
    {
        return SNMP_FAILURE;
    }

    /* Clearing  the Bridge counters */
    gu4RecTrgdCount = LA_INIT_VAL;

    if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    do
    {
        if (LaGetAggEntry ((UINT2) i4Index, &pLaLacAggEntry) != LA_SUCCESS)
        {
            continue;
        }
        if (pLaLacAggEntry == NULL)
        {
            continue;
        }
        u2AggIndex = pLaLacAggEntry->u2AggIndex;

        LaUtilClearPortChannelCounters (u2AggIndex);

        i4PrevIndex = i4Index;
    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS));

    gu1LacpClearCounter = LA_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGSystemID
 Input       :  The Indices

                The Object
                retValFsLaMCLAGSystemID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGSystemID (tMacAddr * pRetValFsLaMCLAGSystemID)
{
    LA_MEMCPY (pRetValFsLaMCLAGSystemID,
               (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr),
               LA_MAC_ADDRESS_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGSystemPriority
 Input       :  The Indices

                The Object
                retValFsLaMCLAGSystemPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGSystemPriority (INT4 *pi4RetValFsLaMCLAGSystemPriority)
{
    *pi4RetValFsLaMCLAGSystemPriority =
        LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGPeriodicSyncTime
 Input       :  The Indices

                The Object
                retValFsLaMCLAGPeriodicSyncTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGPeriodicSyncTime (UINT4 *pu4RetValFsLaMCLAGPeriodicSyncTime)
{
    *pu4RetValFsLaMCLAGPeriodicSyncTime =
        LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaRecTmrDuration
 Input       :  The Indices

                The Object
                retValFsLaRecTmrDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaRecTmrDuration (UINT4 *pu4RetValFsLaRecTmrDuration)
{
    *pu4RetValFsLaRecTmrDuration = gu4RecoveryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaRecThreshold
 Input       :  The Indices

                The Object
                retValFsLaRecThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaRecThreshold (UINT4 *pu4RetValFsLaRecThreshold)
{
    *pu4RetValFsLaRecThreshold = gu4RecThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaTotalErrRecCount
 Input       :  The Indices

                The Object
                retValFsLaTotalErrRecCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaTotalErrRecCount (UINT4 *pu4RetValFsLaTotalErrRecCount)
{
    *pu4RetValFsLaTotalErrRecCount = gu4RecTrgdCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDefaultedStateThreshold
 Input       :  The Indices

                The Object
                retValFsLaDefaultedStateThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDefaultedStateThreshold (UINT4 *pu4RetValFsLaDefaultedStateThreshold)
{
    *pu4RetValFsLaDefaultedStateThreshold = gu4DefaultedStateThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaHardwareFailureRecThreshold
 Input       :  The Indices

                The Object
                retValFsLaHardwareFailureRecThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaHardwareFailureRecThreshold (UINT4
                                       *pu4RetValFsLaHardwareFailureRecThreshold)
{
    *pu4RetValFsLaHardwareFailureRecThreshold = gu4HardwareFailureRecThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaSameStateRecThreshold
 Input       :  The Indices

                The Object
                retValFsLaSameStateRecThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaSameStateRecThreshold (UINT4 *pu4RetValFsLaSameStateRecThreshold)
{
    *pu4RetValFsLaSameStateRecThreshold = gu4SameStateRecThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaRecThresholdExceedAction
 Input       :  The Indices

                The Object
                retValFsLaRecThresholdExceedAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaRecThresholdExceedAction (INT4 *pi4RetValFsLaRecThresholdExceedAction)
{
    *pi4RetValFsLaRecThresholdExceedAction = (INT4) gu4RecThresholdExceedAction;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLaSystemControl
Input       :  The Indices

The Object 
setValFsLaSystemControl
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaSystemControl (INT4 i4SetValFsLaSystemControl)
{
    if (i4SetValFsLaSystemControl == (INT4) LA_SYSTEM_CONTROL)
    {
        LA_TRC (MGMT_TRC,
                "SNMP: LA is already in the same System Control state\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsLaSystemControl == LA_START)
    {
        if (LaHandleInit () != LA_SUCCESS)
        {
            LA_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                    "SNMP: LA Initialization FAILED\n");
            if (LaHandleShutDown () == LA_FAILURE)
            {
                return SNMP_FAILURE;
            }
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (LaHandleShutDown () != LA_SUCCESS)
        {
            LA_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: LA Shutdown FAILED\n");
            return SNMP_FAILURE;
        }
        /* Notify MSR with the LA oids */
        LaNotifyProtocolShutdownStatus ();
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaStatus
Input       :  The Indices

The Object 
setValFsLaStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaStatus (INT4 i4SetValFsLaStatus)
{
    if (LA_SYSTEM_CONTROL != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MODULE_SHUTDOWN);
        LA_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "SNMP: LA System Shutdown; Cannot Enable/Disable LA\n");
        return SNMP_FAILURE;
    }

    if (i4SetValFsLaStatus == (INT4) LA_PROTOCOL_ADMIN_STATUS ())
    {
        LA_TRC (MGMT_TRC, "SNMP: LA Module already has the same status\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsLaStatus == LA_ENABLED)
    {
        LaEnable ();
    }
    else
    {
        LaDisable ();
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaTraceOption
Input       :  The Indices

The Object 
setValFsLaTraceOption
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaTraceOption (INT4 i4SetValFsLaTraceOption)
{
    gLaGlobalInfo.u4TraceOption = (UINT4) i4SetValFsLaTraceOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaActorSystemID
 Input       :  The Indices

                The Object 
                setValFsLaActorSystemID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaActorSystemID (tMacAddr SetValFsLaActorSystemID)
{

    if (LA_MEMCMP (gLaGlobalInfo.LaSystem.SystemMacAddr,
                   SetValFsLaActorSystemID, LA_MAC_ADDRESS_SIZE) != 0)
    {
        if (LaUpdateActorSystemID (SetValFsLaActorSystemID) != LA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaNoPartnerIndep
 Input       :  The Indices

                The Object
                setValFsLaNoPartnerIndep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaNoPartnerIndep (INT4 i4SetValFsLaNoPartnerIndep)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaSelect           PrevAggSelected;
    tLaLacPortState     PrevPartnerState;

    if (gu4PartnerConfig == (UINT4) i4SetValFsLaNoPartnerIndep)
    {
        return SNMP_SUCCESS;
    }

    gu4PartnerConfig = i4SetValFsLaNoPartnerIndep;

    LaGetNextAggEntry (NULL, &pAggEntry);

    while (pAggEntry != NULL)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

        while (pPortEntry != NULL)
        {
            PrevAggSelected = pPortEntry->AggSelected;
            PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;

            if (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED)
            {
                if (gu4PartnerConfig == LA_ENABLED)
                {
                    /* When Independent mode is enabled, the ports operate as individual
                       ports and these ports will become visible to higher layer protocols,
                       when no partner in the switch */

                    pPortEntry->LaLacPartnerInfo.LaLacPortState.
                        LaSynchronization = LA_FALSE;

                    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired =
                        LA_TRUE;

                    LaL2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                                      pPortEntry->pAggEntry->
                                                      u2AggIndex);

                    LaHlCreatePhysicalPort (pPortEntry);

                    /* Change AggSelected variable to LA_UNSELECTED */
                    pPortEntry->AggSelected = LA_UNSELECTED;
                    pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;

                    if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
                    {
                        pAggEntry->UsingPartnerAdminInfo = LA_FALSE;
                    }

                }
                else
                {
                    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired =
                        LA_FALSE;

                    LaLacUpdateSelectedFromPartnerAdmin (pPortEntry);
                    LaLacRecordPartnerAdminIfInfo (pPortEntry);

                    if (pAggEntry->UsingPartnerAdminInfo == LA_FALSE)
                    {
                        pAggEntry->UsingPartnerAdminInfo = LA_TRUE;
                    }

                    /* Delete from HL */
                    LaHlDeletePhysicalPort (pPortEntry);
                }

                LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected,
                                        PrevPartnerState);

            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaDLAGSystemStatus
 Input       :  The Indices

                The Object
                setValFsLaDLAGSystemStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDLAGSystemStatus (INT4 i4SetValFsLaDLAGSystemStatus)
{

    if (LA_DLAG_SYSTEM_STATUS == (UINT1) i4SetValFsLaDLAGSystemStatus)

    {
        return SNMP_SUCCESS;
    }

    LA_AA_DLAG_ROLE_PLAYED = LaActiveDLAGGetRolePlayed ();

    /* Get the role played from DISS or NVRAM */

    if (i4SetValFsLaDLAGSystemStatus == LA_DLAG_ENABLED)
    {
        LA_DLAG_SYSTEM_STATUS = (UINT1) i4SetValFsLaDLAGSystemStatus;
        LaActiveDLAGEnable ();
    }
    else
    {
        LaActiveDLAGDisable ();
        LA_DLAG_SYSTEM_STATUS = (UINT1) i4SetValFsLaDLAGSystemStatus;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaDLAGSystemID
 Input       :  The Indices

                The Object
                setValFsLaDLAGSystemID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDLAGSystemID (tMacAddr SetValFsLaDLAGSystemID)
{

    LA_MEMCPY ((LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr),
               SetValFsLaDLAGSystemID, LA_MAC_ADDRESS_SIZE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaDLAGSystemPriority
 Input       :  The Indices

                The Object
                setValFsLaDLAGSystemPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDLAGSystemPriority (INT4 i4SetValFsLaDLAGSystemPriority)
{

    LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority =
        (UINT2) i4SetValFsLaDLAGSystemPriority;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaDLAGPeriodicSyncTime
 Input       :  The Indices
                The Object
                setValFsLaDLAGPeriodicSyncTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDLAGPeriodicSyncTime (UINT4 u4SetValFsLaDLAGPeriodicSyncTime)
{

    LA_DLAG_GLOBAL_INFO.u4GlobalDLAGPeriodicSyncTime =
        u4SetValFsLaDLAGPeriodicSyncTime;

    if (u4SetValFsLaDLAGPeriodicSyncTime == 0)
    {
        /* Stop the timer in case of periodic time is 0 */
        if ((LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr))) !=
            LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStopTimer failed for Global D-LAG "
                    "Periodic Sync Timer\n");
            return LA_FAILURE;
        }
    }
    else
    {
        /* Start the timer for new duration */
        if (LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr.LaTmrFlag ==
            LA_TMR_STOPPED)
        {

            if (LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr))
                != LA_SUCCESS)
            {
                return LA_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaDLAGDistributingPortIndex
 Input       :  The Indices

                The Object
                setValFsLaDLAGDistributingPortIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDLAGDistributingPortIndex (INT4 i4SetValFsLaDLAGDistributingPortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;

    /* If the distributing port index is not zero, it has to be added to
     * distributing port list */
    if (i4SetValFsLaDLAGDistributingPortIndex != 0)
    {
        /* If the port index is already added in the port list, skip adding into port list */
        OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                 GlobalDLAGDistributingPortList,
                                 i4SetValFsLaDLAGDistributingPortIndex,
                                 LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_FALSE)
        {
            OSIX_BITLIST_SET_BIT (LA_DLAG_GLOBAL_INFO.
                                  GlobalDLAGDistributingPortList,
                                  i4SetValFsLaDLAGDistributingPortIndex,
                                  LA_PORT_LIST_SIZE);

            /* Get the oper staus of the port index, If the oper status is down,
             * dont add into the distributing oper port list
             * This is useful in the case of MSR save and restore, because
             * while restoration, directly set will be called. If the distribution port
             * is shut down, then dont add into the list */

            LaGetPortEntry ((UINT2) i4SetValFsLaDLAGDistributingPortIndex,
                            &pPortEntry);
            if (pPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
            {
                OSIX_BITLIST_SET_BIT (LA_DLAG_GLOBAL_INFO.
                                      GlobalDLAGDistributingOperPortList,
                                      i4SetValFsLaDLAGDistributingPortIndex,
                                      LA_PORT_LIST_SIZE);
                LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount++;
            }
        }
    }
    /* If the distributing port index is zero, it has to be reset from both the
     * distributing port list and distributing oper port list */
    else
    {
        /* Check whether the index is already added into the port list
         * if true means reset the port list */
        OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                 GlobalDLAGDistributingPortList,
                                 LA_DLAG_GLOBAL_INFO.
                                 u4GlobalDLAGDistributingIfIndex,
                                 LA_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {

            OSIX_BITLIST_RESET_BIT (LA_DLAG_GLOBAL_INFO.
                                    GlobalDLAGDistributingOperPortList,
                                    LA_DLAG_GLOBAL_INFO.
                                    u4GlobalDLAGDistributingIfIndex,
                                    LA_PORT_LIST_SIZE);
            OSIX_BITLIST_RESET_BIT (LA_DLAG_GLOBAL_INFO.
                                    GlobalDLAGDistributingPortList,
                                    LA_DLAG_GLOBAL_INFO.
                                    u4GlobalDLAGDistributingIfIndex,
                                    LA_PORT_LIST_SIZE);
            LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount--;
        }
    }

    LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingIfIndex =
        (UINT4) i4SetValFsLaDLAGDistributingPortIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaDLAGDistributingPortList
 Input       :  The Indices

                The Object
                setValFsLaDLAGDistributingPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDLAGDistributingPortList (tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsLaDLAGDistributingPortList)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    BOOL1               bResult = OSIX_FALSE;

    LA_MEMCPY (LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingPortList,
               pSetValFsLaDLAGDistributingPortList->pu1_OctetList,
               pSetValFsLaDLAGDistributingPortList->i4_Length);

    for (; u4DLAGDistributingIfIndex < LA_MAX_PORTS;
         u4DLAGDistributingIfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                 GlobalDLAGDistributingPortList,
                                 u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE,
                                 bResult);

        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue the for loop */
            continue;
        }
        /* Get the oper staus of the port index, If the oper status is down,
         * dont add into the distributing oper port list
         * This is useful in the case of MSR save and restore, because
         * while restoration, directly set will be called. If the distribution port
         * is shut down, then dont add into the list */

        LaGetPortEntry ((UINT2) u4DLAGDistributingIfIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
        {

            OSIX_BITLIST_SET_BIT (LA_DLAG_GLOBAL_INFO.
                                  GlobalDLAGDistributingOperPortList,
                                  u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE);
            u4Count++;
        }
    }

    /* If the distributing port index is not zero, it has to be added to
     * distributing port list */
    bResult = OSIX_FALSE;
    if (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingIfIndex != 0)
    {
        /* If the port index is already added in the port list, skip adding into port list */
        OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                 GlobalDLAGDistributingPortList,
                                 LA_DLAG_GLOBAL_INFO.
                                 u4GlobalDLAGDistributingIfIndex,
                                 LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_FALSE)
        {
            /* Get the oper staus of the port index, If the oper status is down,
             * dont add into the distributing oper port list
             * This is useful in the case of MSR save and restore, because
             * while restoration, directly set will be called. If the distribution port
             * is shut down, then dont add into the list */

            LaGetPortEntry ((UINT2) LA_DLAG_GLOBAL_INFO.
                            u4GlobalDLAGDistributingIfIndex, &pPortEntry);
            if (pPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
            {

                OSIX_BITLIST_SET_BIT (LA_DLAG_GLOBAL_INFO.
                                      GlobalDLAGDistributingOperPortList,
                                      LA_DLAG_GLOBAL_INFO.
                                      u4GlobalDLAGDistributingIfIndex,
                                      LA_PORT_LIST_SIZE);
                OSIX_BITLIST_SET_BIT (LA_DLAG_GLOBAL_INFO.
                                      GlobalDLAGDistributingPortList,
                                      LA_DLAG_GLOBAL_INFO.
                                      u4GlobalDLAGDistributingIfIndex,
                                      LA_PORT_LIST_SIZE);
                u4Count++;
            }
        }
    }

    LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount = u4Count;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaMCLAGClearCounters
 Input       :  The Indices

                The Object
                setValFsLaMCLAGClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaMCLAGClearCounters (INT4 i4SetValFsLaMCLAGClearCounters)
{
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;

    gu1MclagClearCounter = (UINT1) i4SetValFsLaMCLAGClearCounters;
    if (i4SetValFsLaMCLAGClearCounters == LA_TRUE)
    {
        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            return (SNMP_SUCCESS);
        }
        do
        {
            LaGetAggEntry ((UINT2) i4Index, &pLaLacAggEntry);

            /* Reset MC-lag Tx and Rx counters */
            pLaLacAggEntry->u4DLAGPeriodicSyncPduTxCount = 0;
            pLaLacAggEntry->u4DLAGPeriodicSyncPduRxCount = 0;
            pLaLacAggEntry->u4DLAGEventUpdatePduTxCount = 0;
            pLaLacAggEntry->u4DLAGEventUpdatePduRxCount = 0;
            i4PrevIndex = i4Index;
        }
        while (nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
               SNMP_SUCCESS);
    }
    gu1MclagClearCounter = LA_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaMCLAGSystemControl
 Input       :  The Indices

                The Object
                setValFsLaMCLAGSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaMCLAGSystemControl (INT4 i4SetValFsLaMCLAGSystemControl)
{
#ifdef ICCH_WANTED
    UINT4               u4InstanceId = 0;
#endif
    if (i4SetValFsLaMCLAGSystemControl == (INT4) gLaMclagSystemControl)
    {
        return SNMP_SUCCESS;
    }
    gLaMclagSystemControl = i4SetValFsLaMCLAGSystemControl;
    if (i4SetValFsLaMCLAGSystemControl == LA_START)
    {
        LaHbResumeProtocolOperation ();

    }
    if (i4SetValFsLaMCLAGSystemControl == LA_SHUTDOWN)
    {
#ifdef ICCH_WANTED
        gu1LaMclagShutDownInProgress = OSIX_TRUE;
        if (LaNoMclag (u4InstanceId) == LA_FAILURE)
        {
            gu1LaMclagShutDownInProgress = OSIX_FALSE;
            return SNMP_FAILURE;
        }
        /* When MC-LAG shutdown is successful, the variable 
         * gu1LaMclagShutDownInProgress is reset after receiving notification from
         * Hb module in the function  LaNotifyMclagShutdownComplete
         */
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaMCLAGSystemStatus
 Input       :  The Indices

                The Object
                setValFsLaMCLAGSystemStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaMCLAGSystemStatus (INT4 i4SetValFsLaMCLAGSystemStatus)
{
#ifdef ICCH_WANTED
    UINT4               u4RolePlayed = 0;
#endif

    if (LA_MCLAG_SYSTEM_STATUS == (UINT1) i4SetValFsLaMCLAGSystemStatus)
    {
        return SNMP_SUCCESS;
    }

#ifdef ICCH_WANTED
    u4RolePlayed = LaIcchGetRolePlayed ();
    if (u4RolePlayed == LA_ICCH_MASTER)
    {
        LA_AA_DLAG_ROLE_PLAYED = LA_MCLAG_SYSTEM_ROLE_MASTER;
    }
    else if (u4RolePlayed == LA_ICCH_SLAVE)
    {
        LA_AA_DLAG_ROLE_PLAYED = LA_MCLAG_SYSTEM_ROLE_SLAVE;
    }
    else
    {
        LA_AA_DLAG_ROLE_PLAYED = LA_MCLAG_SYSTEM_ROLE_NONE;
    }
#endif

    if (i4SetValFsLaMCLAGSystemStatus == LA_ENABLED)
    {
#if (defined HB_WANTED && defined ICCH_WANTED)
        /*Start Sending HB packets to the peer node */
        HbApiSendEvtFromMCLAG (HB_MCLAG_START_HB_MSG);
#endif
#ifdef NPAPI_WANTED
        if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
        {
            if (FNP_FAILURE == LaFsLaHwDlagStatus (LA_DLAG_ENABLED))
            {
                LA_TRC (INIT_SHUT_TRC, "LaActiveMCLAGEnable FAILED\n");
                return LA_FAILURE;
            }
        }
#endif
        LA_MCLAG_SYSTEM_STATUS = (UINT1) i4SetValFsLaMCLAGSystemStatus;
#ifdef ICCH_WANTED
        LaActiveMCLAGEnable ();
        IcchApiSendMCLAGStatus (MCLAG_ENABLED);
#endif
        LA_AA_DLAG_MASTER_DOWN = LA_FALSE;
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) LA_SYSLOG_ID,
                      "Configuration on MC-LAG interfaces shall be done uniformly across MC-LAG nodes"));
    }
#ifdef ICCH_WANTED
    else
    {
        LA_MCLAG_SYSTEM_STATUS = (UINT1) i4SetValFsLaMCLAGSystemStatus;
        LaActiveMCLAGDisable ();
        /*Start Sending HB packets to the peer node */
        HbApiSendEvtFromMCLAG (HB_MCLAG_STOP_HB_MSG);

        IcchApiSendMCLAGStatus (MCLAG_DISABLED);
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaMCLAGSystemID
 Input       :  The Indices

                The Object
                setValFsLaMCLAGSystemID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaMCLAGSystemID (tMacAddr SetValFsLaMCLAGSystemID)
{
    tMacAddr            DefaultMacAddr;
    LA_MEMSET (DefaultMacAddr, 0, LA_MAC_ADDRESS_SIZE);

    LA_MEMCPY ((LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr),
               SetValFsLaMCLAGSystemID, LA_MAC_ADDRESS_SIZE);
    /* When no mc-lag system-identifier command is given,the value will be set as 
     * zero.Then bIsMCLAGGlobalSysIDConfigured flag   should be reset to FALSE*/
    if ((LA_MEMCMP (&LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                    DefaultMacAddr, LA_MAC_ADDRESS_SIZE) != 0))
    {
        gLaGlobalInfo.bIsMCLAGGlobalSysIDConfigured = LA_TRUE;
    }
    else
    {
        gLaGlobalInfo.bIsMCLAGGlobalSysIDConfigured = LA_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaMCLAGSystemPriority
 Input       :  The Indices

                The Object
                setValFsLaMCLAGSystemPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaMCLAGSystemPriority (INT4 i4SetValFsLaMCLAGSystemPriority)
{
    LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority =
        (UINT2) i4SetValFsLaMCLAGSystemPriority;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaMCLAGPeriodicSyncTime
 Input       :  The Indices

                The Object
                setValFsLaMCLAGPeriodicSyncTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaMCLAGPeriodicSyncTime (UINT4 u4SetValFsLaMCLAGPeriodicSyncTime)
{
    LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime =
        u4SetValFsLaMCLAGPeriodicSyncTime;

    if (u4SetValFsLaMCLAGPeriodicSyncTime == 0)
    {
        /* Stop the timer in case of periodic time is 0 */
        if ((LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr))) !=
            LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStopTimer failed for Global MC-LAG "
                    "Periodic Sync Timer\n");
            return LA_FAILURE;
        }
    }
    else
    {
        /* Start the timer for new duration */
        if (LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr.LaTmrFlag ==
            LA_TMR_STOPPED)
        {

            if (LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr))
                != LA_SUCCESS)
            {
                return LA_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaRecTmrDuration
 Input       :  The Indices

                The Object
                setValFsLaRecTmrDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaRecTmrDuration (UINT4 u4SetValFsLaRecTmrDuration)
{
    gu4RecoveryTime = u4SetValFsLaRecTmrDuration;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaRecThreshold
 Input       :  The Indices

                The Object
                setValFsLaRecThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaRecThreshold (UINT4 u4SetValFsLaRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex;

    gu4RecThreshold = u4SetValFsLaRecThreshold;

    gu4DefaultedStateThreshold = u4SetValFsLaRecThreshold;
    gu4HardwareFailureRecThreshold = u4SetValFsLaRecThreshold;
    gu4SameStateRecThreshold = u4SetValFsLaRecThreshold;

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        LaGetPortEntry (u2PortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }
        pPortEntry->u4DefaultedStateThreshold = u4SetValFsLaRecThreshold;
        pPortEntry->u4HardwareFailureRecThreshold = u4SetValFsLaRecThreshold;
        pPortEntry->u4SameStateRecThreshold = u4SetValFsLaRecThreshold;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaDefaultedStateThreshold
 Input       :  The Indices

                The Object
                setValFsLaDefaultedStateThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaDefaultedStateThreshold (UINT4 u4SetValFsLaDefaultedStateThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex;

    gu4DefaultedStateThreshold = u4SetValFsLaDefaultedStateThreshold;
    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {

        LaGetPortEntry (u2PortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }
        pPortEntry->u4DefaultedStateThreshold =
            u4SetValFsLaDefaultedStateThreshold;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaHardwareFailureRecThreshold
 Input       :  The Indices

                The Object
                setValFsLaHardwareFailureRecThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaHardwareFailureRecThreshold (UINT4
                                       u4SetValFsLaHardwareFailureRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex;
    gu4HardwareFailureRecThreshold = u4SetValFsLaHardwareFailureRecThreshold;
    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {

        LaGetPortEntry (u2PortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }
        pPortEntry->u4HardwareFailureRecThreshold =
            u4SetValFsLaHardwareFailureRecThreshold;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaSameStateRecThreshold
 Input       :  The Indices

                The Object
                setValFsLaSameStateRecThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaSameStateRecThreshold (UINT4 u4SetValFsLaSameStateRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex;

    gu4SameStateRecThreshold = u4SetValFsLaSameStateRecThreshold;
    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        LaGetPortEntry (u2PortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }
        pPortEntry->u4SameStateRecThreshold = u4SetValFsLaSameStateRecThreshold;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaRecThresholdExceedAction
 Input       :  The Indices

                The Object
                setValFsLaRecThresholdExceedAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaRecThresholdExceedAction (INT4 i4SetValFsLaRecThresholdExceedAction)
{
    gu4RecThresholdExceedAction = (UINT4) i4SetValFsLaRecThresholdExceedAction;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsLaSystemControl
Input       :  The Indices

The Object 
testValFsLaSystemControl
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaSystemControl (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsLaSystemControl)
{
    if ((i4TestValFsLaSystemControl != LA_START) &&
        (i4TestValFsLaSystemControl != LA_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifdef VLAN_WANTED
    if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_BASE_BRIDGE_LA_ENABLED);
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsLaStatus
Input       :  The Indices

The Object 
testValFsLaStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsLaStatus)
{

    if (LA_SYSTEM_CONTROL != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLaStatus == LA_ENABLED) ||
        (i4TestValFsLaStatus == LA_DISABLED))
    {
        return SNMP_SUCCESS;

    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhTestv2FsLaTraceOption
Input       :  The Indices

The Object 
testValFsLaTraceOption
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaTraceOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsLaTraceOption)
{
    if (i4TestValFsLaTraceOption < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaActorSystemID
 Input       :  The Indices
                                                                                
                The Object
                testValFsLaActorSystemID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaActorSystemID (UINT4 *pu4ErrorCode,
                            tMacAddr TestValFsLaActorSystemID)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;
    INT4                i4RetVal;

    if ((i4RetVal = LaIsValidSystemID (TestValFsLaActorSystemID)) == LA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    while (pTmpAggEntry != NULL)
    {
        if (pTmpAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
        {
            CLI_SET_ERR (CLI_LA_DLAG_STATUS_ENABLED);
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "D-LAG Status is enabled, In Po(%u)"
                         " port-channel, so System ID cannot be changed.\n",
                         pTmpAggEntry->u2AggIndex);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaNoPartnerIndep
 Input       :  The Indices

                The Object
                testValFsLaNoPartnerIndep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaNoPartnerIndep (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsLaNoPartnerIndep)
{
    if ((i4TestValFsLaNoPartnerIndep != LA_ENABLED) &&
        (i4TestValFsLaNoPartnerIndep != LA_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaDLAGSystemStatus
 Input       :  The Indices

                The Object
                testValFsLaDLAGSystemStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDLAGSystemStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsLaDLAGSystemStatus)
{

    tMacAddr            TempMacAddr;
    /* Only valid inputs for FsLaDLAGSystemStatus MIB object
     * are enable/disable */
    if ((i4TestValFsLaDLAGSystemStatus != LA_DLAG_ENABLED)
        && (i4TestValFsLaDLAGSystemStatus != LA_DLAG_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((LA_DLAG_SYSTEM_STATUS == (UINT1) i4TestValFsLaDLAGSystemStatus))
    {
        /* Same Value configured */
        return SNMP_SUCCESS;
    }

    /* DLAG cannot be enabled if mandatory params are not enabled */
    LA_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));
    if (LA_MEMCMP
        (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr, TempMacAddr,
         LA_MAC_ADDRESS_SIZE) == 0)
    {
        CLI_SET_ERR (CLI_LA_DLAG_GLOBAL_STATUS_INTER_COLUMNAR_DEPENDENCY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaDLAGSystemID
 Input       :  The Indices

                The Object
                testValFsLaDLAGSystemID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDLAGSystemID (UINT4 *pu4ErrorCode,
                           tMacAddr TestValFsLaDLAGSystemID)
{

    tMacAddr            TempMacAddr;

    /* Validate the Input D-LAG System ID
     * If input MAC Address is : 00:00:00:00 then Test is called before
     * Resetting D-LAG System ID to default Address, in this case No need
     * to validate Default Address.
     * Note: LaIsValidSystemID is not called for Default Address as
     * address 00:00:00:00 is treated as invalid address in function
     * LaIsValidSystemID ().  */

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_DLAG_SYS_ID_DLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    LA_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));

    if ((LA_MEMCMP (TestValFsLaDLAGSystemID, TempMacAddr,
                    LA_MAC_ADDRESS_SIZE)) != 0)
    {
        if ((LaIsValidSystemID (TestValFsLaDLAGSystemID)) != LA_SUCCESS)
        {
            CLI_SET_ERR (CLI_LA_DLAG_INVALID_SYS_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    /*  Distributed System ID configuration should not be allowed
     *  when D-LAG status is enabled,*/
    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_DLAG_SYS_ID_DLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LA_MEMCMP (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                   TestValFsLaDLAGSystemID, LA_MAC_ADDRESS_SIZE) == 0)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaDLAGSystemPriority
 Input       :  The Indices

                The Object
                testValFsLaDLAGSystemPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDLAGSystemPriority (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsLaDLAGSystemPriority)
{

    /* If priority is less than minumum (or) greter than maximum
       then return Failure */
    if ((i4TestValFsLaDLAGSystemPriority < LA_DLAG_MIN_SYS_PRIORITY)
        || (i4TestValFsLaDLAGSystemPriority > LA_DLAG_MAX_SYS_PRIORITY))
    {
        CLI_SET_ERR (CLI_LA_DLAG_INVALID_SYS_PRIORITY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*  Distributed System priority configuration should not be allowed
     *  when D-LAG status is enabled,*/

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_DLAG_SYS_PRIO_DLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority ==
        (UINT2) i4TestValFsLaDLAGSystemPriority)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaDLAGPeriodicSyncTime
 Input       :  The Indices

                The Object
                testValFsLaDLAGPeriodicSyncTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDLAGPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsLaDLAGPeriodicSyncTime)
{

    if (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGPeriodicSyncTime ==
        u4TestValFsLaDLAGPeriodicSyncTime)
    {
        return SNMP_SUCCESS;

    }

    /* Input Value range checking : 0 >= Periodic-sync time <= 90
     * Since u4TestValFsLaDLAGPeriodicSyncTime is of type unsigned
     * integer checking 'less than zero' is not necessary as 'negative values'
     * will always become 'greater than 90' after wrap around
     * after wrap around -ve value will be wrapped around to
     * = UINT_MAX - ((1-(ABS(-ve val)))) */

    if (u4TestValFsLaDLAGPeriodicSyncTime > LA_AA_DLAG_MAX_PERIODIC_SYNC_TIME)
    {
        CLI_SET_ERR (CLI_LA_AA_DLAG_INVALID_PERIODIC_SYNC_TIME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaDLAGDistributingPortIndex
 Input       :  The Indices

                The Object
                testValFsLaDLAGDistributingPortIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDLAGDistributingPortIndex (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsLaDLAGDistributingPortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_DLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLaDLAGDistributingPortIndex == 0)
        || (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingIfIndex ==
            (UINT4) i4TestValFsLaDLAGDistributingPortIndex))
    {
        return SNMP_SUCCESS;
    }

    /* Check If Distributing port is operationally UP */
    LaGetPortEntry ((UINT2) i4TestValFsLaDLAGDistributingPortIndex,
                    &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->u1PortOperStatus == CFA_IF_DOWN)
    {
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_OPER_STATUS_DOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Distributing port index
     * Ports that are already part of Port-channel cannot be used as
     * Distributing port, So following logic scans through member port list
     * and checks if distributing index is already present */

    if (L2IwfIsPortInPortChannel
        ((UINT4) i4TestValFsLaDLAGDistributingPortIndex) == L2IWF_SUCCESS)
    {
        /* Port is part of port-channel */
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_IS_MEMBER_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaDLAGDistributingPortList
 Input       :  The Indices

                The Object
                testValFsLaDLAGDistributingPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDLAGDistributingPortList (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsLaDLAGDistributingPortList)
{

    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT1               u1OperStatus = 0;
    BOOL1               bResult = OSIX_FALSE;

    /*  Distributed port configuration should not be allowed
     *  when D-LAG status is enabled,*/

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_DLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* The following check is added in Test routine instead of Set routine
     * because, the code after this check involves scanning member port
     * list which is CPU utilizing operation; so scanning list is
     * avoided when Input Index is 0 or when input index is same as
     * previous Distributing index */

    if ((LA_MEMCMP (LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingPortList,
                    pTestValFsLaDLAGDistributingPortList->
                    pu1_OctetList, LA_PORT_LIST_SIZE) == 0))
    {
        return SNMP_SUCCESS;
    }

    /* Check If Distributing port is operationally UP */

    for (; u4DLAGDistributingIfIndex <= LA_MAX_PORTS;
         u4DLAGDistributingIfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET
            (pTestValFsLaDLAGDistributingPortList->pu1_OctetList,
             u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue the for loop */
            continue;
        }
        /* Check If Distributing port is operationally UP,
         * If not then packet transmission attempt need not
         * be made on the failed distributing port */

        CfaGetIfOperStatus (u4DLAGDistributingIfIndex, &u1OperStatus);
        if (u1OperStatus == CFA_IF_DOWN)
        {
            CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_OPER_STATUS_DOWN);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (L2IwfIsPortInPortChannel (u4DLAGDistributingIfIndex)
            == L2IWF_SUCCESS)
        {
            /* Port is part of port-channel */
            CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_IS_MEMBER_PORT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaMCLAGClearCounters
 Input       :  The Indices

                The Object
                testValFsLaMCLAGClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaMCLAGClearCounters (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsLaMCLAGClearCounters)
{
    if (gu1MclagClearCounter == i4TestValFsLaMCLAGClearCounters)
    {
        return SNMP_SUCCESS;
    }

    if ((gu1MclagClearCounter != LA_TRUE) && (gu1MclagClearCounter != LA_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsLaClearStatistics
Input       :  The Indices

               The Object 
               testValFsLaClearStatistics
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaClearStatistics (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsLaClearStatistics)
{
    if ((i4TestValFsLaClearStatistics != LA_ENABLED) &&
        (i4TestValFsLaClearStatistics != LA_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*To avoid inconsistency in continuous disable/enable introduced 
     *HB_MCLAG_DISABLE_TIMER timer.Timer will start when mc-lag is disabled.
     *On the timer expiry  gu1HbMclagDisableInProgress will be reset to OSIX_FALSE*/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaMCLAGSystemStatus
 Input       :  The Indices

                The Object
                testValFsLaMCLAGSystemStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaMCLAGSystemStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsLaMCLAGSystemStatus)
{

    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        LA_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "SNMP: MC-LAG is Shutdown; Cannot Enable/Disable MC-LAG\r\n");
        return SNMP_FAILURE;
    }

    /* Only valid inputs for FsLaMCLAGSystemStatus MIB object
     * are enable/disable */
    if ((i4TestValFsLaMCLAGSystemStatus != LA_ENABLED)
        && (i4TestValFsLaMCLAGSystemStatus != LA_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((LA_MCLAG_SYSTEM_STATUS == (UINT1) i4TestValFsLaMCLAGSystemStatus))
    {
        /* Same Value configured */
        return SNMP_SUCCESS;
    }

    /*To avoid inconsistency in continuous disable/enable introduced 
     *HB_MCLAG_DISABLE_TIMER timer.Timer will start when mc-lag is disabled.
     *On the timer expiry  gu1HbMclagDisableInProgress will be reset to OSIX_FALSE*/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaMCLAGSystemID
 Input       :  The Indices

                The Object
                testValFsLaMCLAGSystemID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaMCLAGSystemID (UINT4 *pu4ErrorCode,
                            tMacAddr TestValFsLaMCLAGSystemID)
{
    tMacAddr            TempMacAddr;

    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        return SNMP_FAILURE;
    }

    /* Validate the Input MC-LAG System ID
     * If input MAC Address is : 00:00:00:00 then Test is called before
     * Resetting MC-LAG System ID to default Address, in this case No need
     * to validate Default Address.
     * Note: LaIsValidSystemID is not called for Default Address as
     * address 00:00:00:00 is treated as invalid address in function
     * LaIsValidSystemID ().  */

    if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SYS_ID_MCLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    LA_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));

    if ((LA_MEMCMP (TestValFsLaMCLAGSystemID, TempMacAddr,
                    LA_MAC_ADDRESS_SIZE)) != 0)
    {
        if ((LaIsValidSystemID (TestValFsLaMCLAGSystemID)) != LA_SUCCESS)
        {
            CLI_SET_ERR (CLI_LA_MCLAG_INVALID_SYS_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    /*  Distributed System ID configuration should not be allowed
     *  when MC-LAG status is enabled,*/
    if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SYS_ID_MCLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LA_MEMCMP (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                   TestValFsLaMCLAGSystemID, LA_MAC_ADDRESS_SIZE) == 0)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaMCLAGSystemPriority
 Input       :  The Indices

                The Object
                testValFsLaMCLAGSystemPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaMCLAGSystemPriority (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsLaMCLAGSystemPriority)
{

    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        return SNMP_FAILURE;
    }

    /* If priority is less than minumum (or) greter than maximum
       then return Failure */
    if ((i4TestValFsLaMCLAGSystemPriority < LA_DLAG_MIN_SYS_PRIORITY)
        || (i4TestValFsLaMCLAGSystemPriority > LA_DLAG_MAX_SYS_PRIORITY))
    {
        CLI_SET_ERR (CLI_LA_MCLAG_INVALID_SYS_PRIORITY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*  Distributed System priority configuration should not be allowed
     *  when D-LAG status is enabled,*/

    if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SYS_PRIO_MCLAG_STATUS_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority ==
        (UINT2) i4TestValFsLaMCLAGSystemPriority)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaMCLAGPeriodicSyncTime
 Input       :  The Indices

                The Object
                testValFsLaMCLAGPeriodicSyncTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaMCLAGPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsLaMCLAGPeriodicSyncTime)
{
    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        return SNMP_FAILURE;
    }

    if (LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime ==
        u4TestValFsLaMCLAGPeriodicSyncTime)
    {
        return SNMP_SUCCESS;

    }

    /* Input Value range checking : 0 >= Periodic-sync time <= 90
     * Since u4TestValFsLaMCLAGPeriodicSyncTime is of type unsigned
     * integer checking 'less than zero' is not necessary as 'negative values'
     * will always become 'greater than 90' after wrap around
     * after wrap around -ve value will be wrapped around to
     * = UINT_MAX - ((1-(ABS(-ve val)))) */

    if (u4TestValFsLaMCLAGPeriodicSyncTime > LA_AA_DLAG_MAX_PERIODIC_SYNC_TIME)
    {
        CLI_SET_ERR (CLI_LA_AA_MCLAG_INVALID_PERIODIC_SYNC_TIME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaRecTmrDuration
 Input       :  The Indices

                The Object
                testValFsLaRecTmrDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaRecTmrDuration (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsLaRecTmrDuration)
{
    if (gu4RecoveryTime == u4TestValFsLaRecTmrDuration)
    {
        return SNMP_SUCCESS;
    }

    if (gu4RecoveryTime > LA_MAX_RECOVERY_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaRecThreshold
 Input       :  The Indices

                The Object
                testValFsLaRecThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaRecThreshold (UINT4 *pu4ErrorCode, UINT4 u4TestValFsLaRecThreshold)
{
    if (gu4RecThreshold == u4TestValFsLaRecThreshold)
    {
        return SNMP_SUCCESS;
    }

    if (gu4RecThreshold > LA_MAX_REC_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaDefaultedStateThreshold
 Input       :  The Indices

                The Object
                testValFsLaDefaultedStateThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaDefaultedStateThreshold (UINT4 *pu4ErrorCode,
                                      UINT4
                                      u4TestValFsLaDefaultedStateThreshold)
{
    if (gu4DefaultedStateThreshold == u4TestValFsLaDefaultedStateThreshold)
    {
        return SNMP_SUCCESS;
    }
    if (u4TestValFsLaDefaultedStateThreshold > LA_MAX_DEF_STATE_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaHardwareFailureRecThreshold
 Input       :  The Indices

                The Object
                testValFsLaHardwareFailureRecThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaHardwareFailureRecThreshold (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4TestValFsLaHardwareFailureRecThreshold)
{
    if (gu4HardwareFailureRecThreshold ==
        u4TestValFsLaHardwareFailureRecThreshold)
    {
        return SNMP_SUCCESS;
    }
    if (u4TestValFsLaHardwareFailureRecThreshold > LA_MAX_REC_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaSameStateRecThreshold
 Input       :  The Indices

                The Object
                testValFsLaSameStateRecThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaSameStateRecThreshold (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsLaSameStateRecThreshold)
{
    if (gu4SameStateRecThreshold == u4TestValFsLaSameStateRecThreshold)
    {
        return SNMP_SUCCESS;
    }
    if (u4TestValFsLaSameStateRecThreshold > LA_MAX_REC_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaRecThresholdExceedAction
 Input       :  The Indices

                The Object
                testValFsLaRecThresholdExceedAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaRecThresholdExceedAction (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsLaRecThresholdExceedAction)
{
    if (gu4RecThresholdExceedAction ==
        (UINT4) i4TestValFsLaRecThresholdExceedAction)
    {
        return SNMP_SUCCESS;
    }
    if ((i4TestValFsLaRecThresholdExceedAction != LA_ACT_NONE) &&
        (i4TestValFsLaRecThresholdExceedAction != LA_ACT_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLaSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaSystemControl (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaActorSystemID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaActorSystemID (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaNoPartnerIndep
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaNoPartnerIndep (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDLAGSystemStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDLAGSystemStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDLAGSystemID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDLAGSystemID (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDLAGSystemPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDLAGSystemPriority (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDLAGPeriodicSyncTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDLAGPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDLAGDistributingPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDLAGDistributingPortIndex (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDLAGDistributingPortList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDLAGDistributingPortList (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaMCLAGClearCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaMCLAGClearCounters (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsLaClearStatistics
Output      :  The Dependency Low Lev Routine Take the Indices &
               check whether dependency is met or not.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaClearStatistics (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaMCLAGSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaMCLAGSystemControl (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaMCLAGSystemStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaMCLAGSystemStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaMCLAGSystemID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaMCLAGSystemID (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaMCLAGSystemPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaMCLAGSystemPriority (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaMCLAGPeriodicSyncTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaMCLAGPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaRecTmrDuration
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaRecTmrDuration (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaRecThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaRecThreshold (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaDefaultedStateThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaDefaultedStateThreshold (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaHardwareFailureRecThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaHardwareFailureRecThreshold (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaSameStateRecThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaSameStateRecThreshold (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLaRecThresholdExceedAction
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaRecThresholdExceedAction (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLaPortChannelTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsLaPortChannelTable
Input       :  The Indices
FsLaPortChannelIfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaPortChannelTable (INT4 i4FsLaPortChannelIfIndex)
{
    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsLaPortChannelTable
Input       :  The Indices
FsLaPortChannelIfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaPortChannelTable (INT4 *pi4FsLaPortChannelIfIndex)
{
    if (LaSnmpLowGetFirstValidAggIndex (pi4FsLaPortChannelIfIndex) ==
        LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexFsLaPortChannelTable
Input       :  The Indices
FsLaPortChannelIfIndex
nextFsLaPortChannelIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaPortChannelTable (INT4 i4FsLaPortChannelIfIndex,
                                     INT4 *pi4NextFsLaPortChannelIfIndex)
{
    if (LaSnmpLowGetNextValidAggIndex (i4FsLaPortChannelIfIndex,
                                       pi4NextFsLaPortChannelIfIndex)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsLaPortChannelGroup
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelGroup
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelGroup (INT4 i4FsLaPortChannelIfIndex,
                            INT4 *pi4RetValFsLaPortChannelGroup)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsLaPortChannelGroup =
            pAggEntry->AggConfigEntry.u2ActorAdminKey;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelMacSelection
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelMacSelection
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelMacSelection (INT4 i4FsLaPortChannelIfIndex,
                                   INT4 *pi4RetValFsLaPortChannelMacSelection)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    *pi4RetValFsLaPortChannelMacSelection = 1;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelAdminMacAddress
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelAdminMacAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsLaPortChannelAdminMacAddress (INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr *
                                      pRetValFsLaPortChannelAdminMacAddress)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    MEMSET (pRetValFsLaPortChannelAdminMacAddress, 0, LA_MAC_ADDRESS_SIZE);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelMode
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelMode (INT4 i4FsLaPortChannelIfIndex,
                           INT4 *pi4RetValFsLaPortChannelMode)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsLaPortChannelMode = pAggEntry->LaLacpMode;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelPortCount
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelPortCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelPortCount (INT4 i4FsLaPortChannelIfIndex,
                                INT4 *pi4RetValFsLaPortChannelPortCount)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsLaPortChannelPortCount = pAggEntry->u1ConfigPortCount;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelActivePortCount
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelActivePortCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelActivePortCount (INT4 i4FsLaPortChannelIfIndex,
                                      INT4
                                      *pi4RetValFsLaPortChannelActivePortCount)
{
    tLaLacAggEntry     *pAggEntry;

    *pi4RetValFsLaPortChannelActivePortCount = 0;
    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry != NULL)
        {
            *pi4RetValFsLaPortChannelActivePortCount =
                pAggEntry->u1DistributingPortCount;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelSelectionPolicy
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelSelectionPolicy
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelSelectionPolicy (INT4 i4FsLaPortChannelIfIndex,
                                      INT4
                                      *pi4RetValFsLaPortChannelSelectionPolicy)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsLaPortChannelSelectionPolicy =
            pAggEntry->AggConfigEntry.LinkSelectPolicy;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsLaPortChannelDefaultPortIndex
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
retValFsLaPortChannelDefaultPortIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortChannelDefaultPortIndex (INT4 i4FsLaPortChannelIfIndex,
                                       INT4
                                       *pi4RetValFsLaPortChannelDefaultPortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pDefPortEntry = NULL;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        if (pAggEntry->bDynPortRealloc == LA_FALSE)
        {
            /* No default port is configured */
            *pi4RetValFsLaPortChannelDefaultPortIndex = 0;
            return SNMP_SUCCESS;
        }

        /* Get default PortEntry for this Agg */
        if (LaGetDefPortEntryForAgg (pAggEntry, &pDefPortEntry) == LA_SUCCESS)
        {
            *pi4RetValFsLaPortChannelDefaultPortIndex = (INT4)
                pDefPortEntry->u2PortIndex;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMaxPorts
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMaxPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMaxPorts (INT4 i4FsLaPortChannelIfIndex,
                               INT4 *pi4RetValFsLaPortChannelMaxPorts)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValFsLaPortChannelMaxPorts =
            (INT4) pAggEntry->u1MaxPortsToAttach;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelSelectionPolicyBitList
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelSelectionPolicyBitList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelSelectionPolicyBitList (INT4 i4FsLaPortChannelIfIndex,
                                             INT4
                                             *pi4RetValFsLaPortChannelSelectionPolicyBitList)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsLaPortChannelSelectionPolicyBitList = (INT4)
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGDistributingPortIndex
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGDistributingPortIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGDistributingPortIndex (INT4 i4FsLaPortChannelIfIndex,
                                                INT4
                                                *pi4RetValFsLaPortChannelDLAGDistributingPortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelDLAGDistributingPortIndex =
            (INT4) pAggEntry->u4DLAGDistributingIfIndex;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGSystemID
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGSystemID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGSystemID (INT4 i4FsLaPortChannelIfIndex,
                                   tMacAddr *
                                   pRetValFsLaPortChannelDLAGSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LA_MEMCPY (pRetValFsLaPortChannelDLAGSystemID,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGSystemPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGSystemPriority (INT4 i4FsLaPortChannelIfIndex,
                                         INT4
                                         *pi4RetValFsLaPortChannelDLAGSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelDLAGSystemPriority =
            (INT4) pAggEntry->DLAGSystem.u2SystemPriority;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGPeriodicSyncTime
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelDLAGPeriodicSyncTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGPeriodicSyncTime (INT4 i4FsLaPortChannelIfIndex,
                                           UINT4
                                           *pu4RetValFsLaPortChannelDLAGPeriodicSyncTime)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGPeriodicSyncTime =
            pAggEntry->u4DLAGPeriodicSyncTime;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGMSSelectionWaitTime
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelDLAGMSSelectionWaitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGMSSelectionWaitTime (INT4 i4FsLaPortChannelIfIndex,
                                              UINT4
                                              *pu4RetValFsLaPortChannelDLAGMSSelectionWaitTime)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGMSSelectionWaitTime =
            pAggEntry->u4DLAGMSSelectionWaitTime;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGRolePlayed
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGRolePlayed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGRolePlayed (INT4 i4FsLaPortChannelIfIndex,
                                     INT4
                                     *pi4RetValFsLaPortChannelDLAGRolePlayed)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelDLAGRolePlayed =
            (INT4) pAggEntry->u1DLAGRolePlayed;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGStatus (INT4 i4FsLaPortChannelIfIndex,
                                 INT4 *pi4RetValFsLaPortChannelDLAGStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelDLAGStatus = (INT4) pAggEntry->u1DLAGStatus;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGRedundancy
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGMSSelectionPolicy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGRedundancy (INT4 i4FsLaPortChannelIfIndex,
                                     INT4
                                     *pi4RetValFsLaPortChannelDLAGRedundancy)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelDLAGRedundancy =
            (INT4) pAggEntry->u1DLAGRedundancy;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGMaxKeepAliveCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGMaxKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGMaxKeepAliveCount (INT4 i4FsLaPortChannelIfIndex,
                                            INT4
                                            *pi4RetValFsLaPortChannelDLAGMaxKeepAliveCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelDLAGMaxKeepAliveCount =
            (INT4) pAggEntry->u4DLAGMaxKeepAliveCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGPeriodicSyncPduTxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGPeriodicSyncPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGPeriodicSyncPduTxCount (INT4 i4FsLaPortChannelIfIndex,
                                                 UINT4
                                                 *pu4RetValFsLaPortChannelDLAGPeriodicSyncPduTxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGPeriodicSyncPduTxCount =
            (UINT4) pAggEntry->u4DLAGPeriodicSyncPduTxCount;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGPeriodicSyncPduRxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGPeriodicSyncPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGPeriodicSyncPduRxCount (INT4 i4FsLaPortChannelIfIndex,
                                                 UINT4
                                                 *pu4RetValFsLaPortChannelDLAGPeriodicSyncPduRxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGPeriodicSyncPduRxCount =
            (UINT4) pAggEntry->u4DLAGPeriodicSyncPduRxCount;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGEventUpdatePduTxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGEventUpdatePduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGEventUpdatePduTxCount (INT4 i4FsLaPortChannelIfIndex,
                                                UINT4
                                                *pu4RetValFsLaPortChannelDLAGEventUpdatePduTxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGEventUpdatePduTxCount =
            (UINT4) pAggEntry->u4DLAGEventUpdatePduTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGEventUpdatePduRxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGEventUpdatePduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGEventUpdatePduRxCount (INT4 i4FsLaPortChannelIfIndex,
                                                UINT4
                                                *pu4RetValFsLaPortChannelDLAGEventUpdatePduRxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGEventUpdatePduRxCount =
            (UINT4) pAggEntry->u4DLAGEventUpdatePduRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGElectedAsMasterCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGElectedAsMasterCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGElectedAsMasterCount (INT4 i4FsLaPortChannelIfIndex,
                                               UINT4
                                               *pu4RetValFsLaPortChannelDLAGElectedAsMasterCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelDLAGElectedAsMasterCount =
            (UINT4) pAggEntry->u4DLAGElectedAsMasterCount;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGElectedAsSlaveCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelDLAGElectedAsSlaveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelDLAGElectedAsSlaveCount (INT4 i4FsLaPortChannelIfIndex,
                                              UINT4
                                              *pu4RetValFsLaPortChannelDLAGElectedAsSlaveCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {

        *pu4RetValFsLaPortChannelDLAGElectedAsSlaveCount =
            (UINT4) pAggEntry->u4DLAGElectedAsSlaveCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelTrapTxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelTrapTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsLaPortChannelTrapTxCount (INT4 i4FsLaPortChannelIfIndex,
                                  UINT4 *pu4RetValFsLaPortChannelTrapTxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        *pu4RetValFsLaPortChannelTrapTxCount =
            (UINT4) pAggEntry->u4DLAGTrapTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDLAGDistributingPortList
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelDLAGDistributingPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDLAGDistributingPortList (INT4 i4FsLaPortChannelIfIndex,
                                               tSNMP_OCTET_STRING_TYPE
                                               *
                                               pRetValFsLaPortChannelDLAGDistributingPortList)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        MEMCPY (pRetValFsLaPortChannelDLAGDistributingPortList->pu1_OctetList,
                pAggEntry->DLAGDistributingPortList, LA_PORT_LIST_SIZE);
        pRetValFsLaPortChannelDLAGDistributingPortList->i4_Length =
            LA_PORT_LIST_SIZE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGSystemID
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGSystemID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGSystemID (INT4 i4FsLaPortChannelIfIndex,
                                    tMacAddr *
                                    pRetValFsLaPortChannelMCLAGSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LA_MEMCPY (pRetValFsLaPortChannelMCLAGSystemID,
               pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGSystemPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGSystemPriority (INT4 i4FsLaPortChannelIfIndex,
                                          INT4
                                          *pi4RetValFsLaPortChannelMCLAGSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaPortChannelMCLAGSystemPriority =
        (INT4) pAggEntry->DLAGSystem.u2SystemPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGRolePlayed
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGRolePlayed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGRolePlayed (INT4 i4FsLaPortChannelIfIndex,
                                      INT4
                                      *pi4RetValFsLaPortChannelMCLAGRolePlayed)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaPortChannelMCLAGRolePlayed =
        (INT4) pAggEntry->u1MCLAGRolePlayed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGStatus (INT4 i4FsLaPortChannelIfIndex,
                                  INT4 *pi4RetValFsLaPortChannelMCLAGStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    if (gLaMclagSystemControl != LA_START)
    {
        *pi4RetValFsLaPortChannelMCLAGStatus = LA_MCLAG_DISABLED;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaPortChannelMCLAGStatus = (INT4) pAggEntry->u1MCLAGStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGMaxKeepAliveCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGMaxKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGMaxKeepAliveCount (INT4 i4FsLaPortChannelIfIndex,
                                             INT4
                                             *pi4RetValFsLaPortChannelMCLAGMaxKeepAliveCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry != NULL)
    {
        *pi4RetValFsLaPortChannelMCLAGMaxKeepAliveCount =
            (INT4) pAggEntry->u4DLAGMaxKeepAliveCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGPeriodicSyncPduTxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGPeriodicSyncPduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGPeriodicSyncPduTxCount (INT4 i4FsLaPortChannelIfIndex,
                                                  UINT4
                                                  *pu4RetValFsLaPortChannelMCLAGPeriodicSyncPduTxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLaPortChannelMCLAGPeriodicSyncPduTxCount =
        (UINT4) pAggEntry->u4DLAGPeriodicSyncPduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGPeriodicSyncPduRxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGPeriodicSyncPduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGPeriodicSyncPduRxCount (INT4 i4FsLaPortChannelIfIndex,
                                                  UINT4
                                                  *pu4RetValFsLaPortChannelMCLAGPeriodicSyncPduRxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLaPortChannelMCLAGPeriodicSyncPduRxCount =
        (UINT4) pAggEntry->u4DLAGPeriodicSyncPduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGEventUpdatePduTxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGEventUpdatePduTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGEventUpdatePduTxCount (INT4 i4FsLaPortChannelIfIndex,
                                                 UINT4
                                                 *pu4RetValFsLaPortChannelMCLAGEventUpdatePduTxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLaPortChannelMCLAGEventUpdatePduTxCount =
        (UINT4) pAggEntry->u4DLAGEventUpdatePduTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelMCLAGEventUpdatePduRxCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelMCLAGEventUpdatePduRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelMCLAGEventUpdatePduRxCount (INT4 i4FsLaPortChannelIfIndex,
                                                 UINT4
                                                 *pu4RetValFsLaPortChannelMCLAGEventUpdatePduRxCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLaPortChannelMCLAGEventUpdatePduRxCount =
        (UINT4) pAggEntry->u4DLAGEventUpdatePduRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelClearStatistics
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                retValFsLaPortChannelClearStatistics
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelClearStatistics (INT4 i4FsLaPortChannelIfIndex,
                                      INT4
                                      *pi4RetValFsLaPortChannelClearStatistics)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaPortChannelClearStatistics =
        (INT4) pAggEntry->u1PortChannelClearCounters;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelUpCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelUpCount (INT4 i4FsLaPortChannelIfIndex,
                              UINT4 *pu4RetValFsLaPortChannelUpCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortChannelUpCount = pAggEntry->u4OperUpCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDownCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelDownCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDownCount (INT4 i4FsLaPortChannelIfIndex,
                                UINT4 *pu4RetValFsLaPortChannelDownCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortChannelDownCount = pAggEntry->u4OperDownCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelHotStandByPortsCount
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelHotStandByPortsCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelHotStandByPortsCount (INT4 i4FsLaPortChannelIfIndex,
                                           UINT4
                                           *pu4RetValFsLaPortChannelHotStandByPortsCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortChannelHotStandByPortsCount =
        pAggEntry->u4StandByPortCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelOperChgTimeStamp
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelOperChgTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelOperChgTimeStamp (INT4 i4FsLaPortChannelIfIndex,
                                       UINT4
                                       *pu4RetValFsLaPortChannelOperChgTimeStamp)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortChannelOperChgTimeStamp =
        pAggEntry->AggTimeOfLastOperChange;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortChannelDownReason
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                retValFsLaPortChannelDownReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortChannelDownReason (INT4 i4FsLaPortChannelIfIndex,
                                 INT4 *pi4RetValFsLaPortChannelDownReason)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaPortChannelDownReason = pAggEntry->i4DownReason;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsLaPortChannelMacSelection
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
setValFsLaPortChannelMacSelection
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaPortChannelMacSelection (INT4 i4FsLaPortChannelIfIndex,
                                   INT4 i4SetValFsLaPortChannelMacSelection)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (i4SetValFsLaPortChannelMacSelection);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsLaPortChannelAdminMacAddress
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
setValFsLaPortChannelAdminMacAddress
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaPortChannelAdminMacAddress (INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr
                                      SetValFsLaPortChannelAdminMacAddress)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (SetValFsLaPortChannelAdminMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsLaPortChannelSelectionPolicy
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
setValFsLaPortChannelSelectionPolicy
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaPortChannelSelectionPolicy (INT4 i4FsLaPortChannelIfIndex,
                                      INT4
                                      i4SetValFsLaPortChannelSelectionPolicy)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tLaLacAggEntry     *pAggEntry;
    UINT4               u4SeqNum = 0;
    INT4                i4SelPolicyBitList = 0;

#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0;
#endif

    if (LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry)
        == LA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) pAggEntry->AggConfigEntry.LinkSelectPolicy !=
        i4SetValFsLaPortChannelSelectionPolicy)
    {
#ifdef NPAPI_WANTED
        /* If Trunk is created in hw then update selection policy */
        if (pAggEntry->u1DistributingPortCount > 0)
        {
            /* Set the port channel status to disabled */
            LaUpdatePortChannelStatusToHw (pAggEntry->u2AggIndex,
                                           LA_PORT_DISABLED, LA_FALSE);
        }

        i4RetVal =
            LaFsLaHwSetSelectionPolicy (pAggEntry->u2AggIndex,
                                        (UINT1)
                                        i4SetValFsLaPortChannelSelectionPolicy);
        if (pAggEntry->u1DistributingPortCount > 0)
        {
            /* Update the actual status of port-channel to h/w */
            LaUpdatePortChannelStatusToHw (pAggEntry->u2AggIndex,
                                           LA_PORT_DISABLED, LA_TRUE);
        }

        if (i4RetVal != FNP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#endif
        LaAggDeleteCacheTableWithAggIndex ((UINT2) i4FsLaPortChannelIfIndex);
        pAggEntry->AggConfigEntry.LinkSelectPolicy =
            i4SetValFsLaPortChannelSelectionPolicy;
        LaSetSelectionPolicyBitList (pAggEntry,
                                     i4SetValFsLaPortChannelSelectionPolicy);
        MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
        i4SelPolicyBitList =
            (INT4) pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList;
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsLaPortChannelSelectionPolicyBitList, u4SeqNum,
                              FALSE, LaLock, LaUnLock, 1, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FsLaPortChannelIfIndex,
                          i4SelPolicyBitList));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDefaultPortIndex
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelDefaultPortIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDefaultPortIndex (INT4 i4FsLaPortChannelIfIndex,
                                       INT4
                                       i4SetValFsLaPortChannelDefaultPortIndex)
{
    tLaLacAggEntry     *pLaAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pDefPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pLaAggEntry);

    if (pLaAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValFsLaPortChannelDefaultPortIndex == 0)
    {
        /* Remove default port */
        if (pLaAggEntry->bDynPortRealloc == LA_FALSE)
        {
            /* No default port is already configured. Do nothing */
            return SNMP_SUCCESS;
        }

        if (LaGetDefPortEntryForAgg (pLaAggEntry, &pDefPortEntry) == LA_FAILURE)
        {
            /* Unknown error; Should not occur */
            return SNMP_FAILURE;
        }

        /* Disable LACP on aggregator and port */
        pLaAggEntry->LaLacpMode = LA_MODE_DISABLED;
        if ((LaChangeLacpMode (pDefPortEntry, pDefPortEntry->LaLacpMode,
                               LA_MODE_DISABLED)) == LA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* Reset the PortEntry flags to denote that the aggregation logic
         * will be static */
        pDefPortEntry->bDynAggSelect = LA_FALSE;
        pDefPortEntry->pDefAggEntry = NULL;
        /* Reset the AggEntry flags to denote that the aggregation logic will
         * be static */
        pLaAggEntry->bDynPortRealloc = LA_FALSE;
        /* Delete Port from Dynamic realloc port list */
        LA_SLL_DELETE (&(gLaGlobalInfo.LaDynReallocPortList),
                       &(pDefPortEntry->NextDefPortEntry));
    }
    else
    {
        LaGetPortEntry ((UINT2) i4SetValFsLaPortChannelDefaultPortIndex,
                        &pPortEntry);

        if (pPortEntry == NULL)
        {
            CLI_SET_ERR (CLI_LA_INVALID_PORT);
            return SNMP_FAILURE;
        }

        if (pLaAggEntry == pPortEntry->pDefAggEntry)
        {
            /* Same value, do nothing */
            return SNMP_SUCCESS;
        }

        if (pPortEntry->pAggEntry != NULL)
        {
            /* Port is already attached to different channel interface */
            CLI_SET_ERR (CLI_LA_DEF_PORT_ALREADY_USED_ERR);
            return SNMP_FAILURE;
        }

        if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Retrieval of port %d properties failed!\n",
                         pPortEntry->u2PortIndex);
            CLI_SET_ERR (CLI_LA_INVALID_PORT);
            return SNMP_FAILURE;
        }

        /* Interface MTU should match with the Port channel MTU */

        if (CfaIfInfo.u4IfMtu != pLaAggEntry->u4Mtu)
        {
            CLI_SET_ERR (CLI_LA_MTU_MISMATCH);
            return SNMP_FAILURE;
        }

        /* Set the flags of AggEntry and PortEntry to denote that
         * the aggregation logic will be dynamically selected */
        pLaAggEntry->bDynPortRealloc = LA_TRUE;
        pPortEntry->bDynAggSelect = LA_TRUE;
        pPortEntry->pDefAggEntry = pLaAggEntry;

        /* Add Port to Dynamic realloc port list */
        LA_SLL_ADD (&(gLaGlobalInfo.LaDynReallocPortList),
                    &(pPortEntry->NextDefPortEntry));

        if ((LaChangeActorAdminKey ((UINT2)
                                    i4SetValFsLaPortChannelDefaultPortIndex,
                                    pLaAggEntry->AggConfigEntry.
                                    u2ActorAdminKey)) == LA_FAILURE)
        {
            /* Cleanup */
            pLaAggEntry->bDynPortRealloc = LA_FALSE;
            pPortEntry->bDynAggSelect = LA_FALSE;
            pPortEntry->pDefAggEntry = NULL;
            LA_SLL_DELETE (&(gLaGlobalInfo.LaDynReallocPortList),
                           &(pPortEntry->NextDefPortEntry));
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelMaxPorts
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelMaxPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelMaxPorts (INT4 i4FsLaPortChannelIfIndex,
                               INT4 i4SetValFsLaPortChannelMaxPorts)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2NextPort = 0;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        pAggEntry->u1MaxPortsToAttach = (UINT1) i4SetValFsLaPortChannelMaxPorts;
        if ((pAggEntry->LaLacpMode == LA_MODE_LACP))
        {
            LaUtilGetNextAggPort ((UINT2) i4FsLaPortChannelIfIndex,
                                  u2Port, &u2NextPort);
            while (u2NextPort != 0)
            {
                LaGetPortEntry (u2NextPort, &pPortEntry);
                if (pPortEntry != NULL)
                {
                    LaLacpSelectionLogic (pPortEntry);
                }
                u2Port = u2NextPort;
                LaUtilGetNextAggPort ((UINT2) i4FsLaPortChannelIfIndex,
                                      u2Port, &u2NextPort);
            }
        }

#ifdef ICCH_WANTED
        if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
            (LaIcchGetRolePlayed () == LA_MCLAG_SYSTEM_ROLE_MASTER))
        {
            LaActiveMCLAGSortOnStandbyConstraints (pAggEntry);

        }
#else
        if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
            (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER))
        {
            LaActiveDLAGSortOnStandbyConstraints (pAggEntry);

        }
#endif

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelSelectionPolicyBitList
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelSelectionPolicyBitList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelSelectionPolicyBitList (INT4
                                             i4FsLaPortChannelIfIndex,
                                             INT4
                                             i4SetValFsLaPortChannelSelectionPolicyBitList)
{
    tLaLacAggEntry     *pAggEntry;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0;
#endif
    if (LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry)
        == LA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList != (UINT4)
        i4SetValFsLaPortChannelSelectionPolicyBitList)
    {
#ifdef NPAPI_WANTED
        /* If Trunk is created in hw then update selection policy */
        if (pAggEntry->u1DistributingPortCount > 0)
        {
            /* Set the port channel status to disabled */
            LaUpdatePortChannelStatusToHw (pAggEntry->u2AggIndex,
                                           LA_PORT_DISABLED, LA_FALSE);
        }

        i4RetVal =
            LaFsLaHwSetSelectionPolicyBitList (pAggEntry->u2AggIndex,
                                               i4SetValFsLaPortChannelSelectionPolicyBitList);
        if (pAggEntry->u1DistributingPortCount > 0)
        {
            /* Update the actual status of port-channel to h/w */
            LaUpdatePortChannelStatusToHw (pAggEntry->u2AggIndex,
                                           LA_PORT_DISABLED, LA_TRUE);
        }
        if (i4RetVal != FNP_SUCCESS)
        {
            if (i4RetVal == FNP_NOT_SUPPORTED)
            {
                CLI_SET_ERR (CLI_LA_SELECTION_POLICY_NOT_SUPPORTED_IN_HW);
            }
            return SNMP_FAILURE;
        }

#endif
        LaAggDeleteCacheTableWithAggIndex ((UINT2) i4FsLaPortChannelIfIndex);
        pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList = 0;
        pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList = (UINT4)
            i4SetValFsLaPortChannelSelectionPolicyBitList;
        LaSetSelectionPolicy (pAggEntry,
                              i4SetValFsLaPortChannelSelectionPolicyBitList);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGDistributingPortIndex
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelDLAGDistributingPortIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGDistributingPortIndex (INT4 i4FsLaPortChannelIfIndex,
                                                INT4
                                                i4SetValFsLaPortChannelDLAGDistributingPortIndex)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4SeqNum = 0;
    BOOL1               bResult = OSIX_FALSE;
    tSNMP_OCTET_STRING_TYPE DLAGDistributePorts;
    tLaPortList        *pDistributePorts = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the distributing port index is not zero, it has to be added to 
     * distributing port list */
    if (i4SetValFsLaPortChannelDLAGDistributingPortIndex != 0)
    {
        /* If the port index is already added in the port list, skip adding into port list */
        OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingPortList,
                                 i4SetValFsLaPortChannelDLAGDistributingPortIndex,
                                 LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_FALSE)
        {
            OSIX_BITLIST_SET_BIT (pAggEntry->DLAGDistributingPortList,
                                  i4SetValFsLaPortChannelDLAGDistributingPortIndex,
                                  LA_PORT_LIST_SIZE);

            /* Get the oper staus of the port index, If the oper status is down, 
             * dont add into the distributing oper port list 
             * This is useful in the case of MSR save and restore, because
             * while restoration, directly set will be called. If the distribution port
             * is shut down, then dont add into the list */
            LaGetPortEntry ((UINT2)
                            i4SetValFsLaPortChannelDLAGDistributingPortIndex,
                            &pPortEntry);
            if (pPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
            {
                OSIX_BITLIST_SET_BIT (pAggEntry->DLAGDistributingOperPortList,
                                      i4SetValFsLaPortChannelDLAGDistributingPortIndex,
                                      LA_PORT_LIST_SIZE);
                pAggEntry->u4DLAGDistributingPortListCount++;
            }
        }
    }
    /* If the distributing port index is zero, it has to be reset from both the
     * distributing port list and distributing oper port list */
    else
    {
        /* Check whether the index is already added into the port list
         * if true means reset the port list */
        OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingPortList,
                                 pAggEntry->u4DLAGDistributingIfIndex,
                                 LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_TRUE)
        {

            OSIX_BITLIST_RESET_BIT (pAggEntry->DLAGDistributingOperPortList,
                                    pAggEntry->u4DLAGDistributingIfIndex,
                                    LA_PORT_LIST_SIZE);
            OSIX_BITLIST_RESET_BIT (pAggEntry->DLAGDistributingPortList,
                                    pAggEntry->u4DLAGDistributingIfIndex,
                                    LA_PORT_LIST_SIZE);
            pAggEntry->u4DLAGDistributingPortListCount--;
        }
    }

    pDistributePorts =
        (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

    if (pDistributePorts == NULL)
    {
        return bResult;
    }
    MEMSET (*pDistributePorts, 0, sizeof (tLaPortList));
    MEMSET (&DLAGDistributePorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    DLAGDistributePorts.pu1_OctetList = *pDistributePorts;

    MEMCPY (DLAGDistributePorts.pu1_OctetList,
            pAggEntry->DLAGDistributingPortList, LA_PORT_LIST_SIZE);

    DLAGDistributePorts.i4_Length = LA_PORT_LIST_SIZE;
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsLaPortChannelDLAGDistributingPortList, u4SeqNum,
                          FALSE, LaLock, LaUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4FsLaPortChannelIfIndex,
                      &DLAGDistributePorts));

    FsUtilReleaseBitList ((UINT1 *) pDistributePorts);

    pAggEntry->u4DLAGDistributingIfIndex =
        (UINT4) i4SetValFsLaPortChannelDLAGDistributingPortIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGSystemID
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelDLAGSystemID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGSystemID (INT4 i4FsLaPortChannelIfIndex,
                                   tMacAddr SetValFsLaPortChannelDLAGSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LA_MEMCPY ((pAggEntry->DLAGSystem.SystemMacAddr),
               SetValFsLaPortChannelDLAGSystemID, LA_MAC_ADDRESS_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelDLAGSystemPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGSystemPriority (INT4 i4FsLaPortChannelIfIndex,
                                         INT4
                                         i4SetValFsLaPortChannelDLAGSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggEntry->DLAGSystem.u2SystemPriority =
        (UINT2) i4SetValFsLaPortChannelDLAGSystemPriority;

    /* We Need to change the Actor System priority as well */
    pAggEntry->AggConfigEntry.
        ActorSystem.u2SystemPriority =
        (UINT2) i4SetValFsLaPortChannelDLAGSystemPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGPeriodicSyncTime
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelDLAGPeriodicSyncTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGPeriodicSyncTime (INT4 i4FsLaPortChannelIfIndex,
                                           UINT4
                                           u4SetValFsLaPortChannelDLAGPeriodicSyncTime)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggEntry->u4DLAGPeriodicSyncTime =
        u4SetValFsLaPortChannelDLAGPeriodicSyncTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGMSSelectionWaitTime
 Input       :  The Indices
                FsLaPortChannelIfIndex                                                                                                                                  
                The Object
                setValFsLaPortChannelDLAGMSSelectionWaitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGMSSelectionWaitTime (INT4 i4FsLaPortChannelIfIndex,
                                              UINT4
                                              u4SetValFsLaPortChannelDLAGMSSelectionWaitTime)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggEntry->u4DLAGMSSelectionWaitTime =
        u4SetValFsLaPortChannelDLAGMSSelectionWaitTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelDLAGStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsLaPortChannelDLAGStatus (INT4 i4FsLaPortChannelIfIndex,
                                 INT4 i4SetValFsLaPortChannelDLAGStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If input value and stored value are same then return success */
    if (pAggEntry->u1DLAGStatus == (UINT1) i4SetValFsLaPortChannelDLAGStatus)
    {
        return SNMP_SUCCESS;
    }

    pAggEntry->u1DLAGStatus = LA_DLAG_DISABLED;
    if (i4SetValFsLaPortChannelDLAGStatus == LA_DLAG_ENABLED)
    {
        /* D-LAG functionality init */
        if (LaDLAGInit (pAggEntry) != LA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        pAggEntry->u1DLAGStatus = LA_DLAG_ENABLED;
    }
    else
    {
        if (LaDLAGDeInit (pAggEntry) != LA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGRedundancy
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelDLAGMSSelectionPolicy
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGRedundancy (INT4 i4FsLaPortChannelIfIndex,
                                     INT4 i4SetValFsLaPortChannelDLAGRedundancy)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Input and existing value same, return success */
    if (pAggEntry->u1DLAGRedundancy ==
        (UINT1) i4SetValFsLaPortChannelDLAGRedundancy)
    {
        return SNMP_SUCCESS;
    }

    pAggEntry->u1DLAGRedundancy = (UINT1) i4SetValFsLaPortChannelDLAGRedundancy;

    if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
    {
        LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

        if (((UINT1) i4SetValFsLaPortChannelDLAGRedundancy) ==
            LA_DLAG_REDUNDANCY_ON)
        {
            /* Change The Node Role Played as Master */
            pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
            LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);
            pAggEntry->u4DLAGElectedAsMasterCount++;

            /* Copy Source Mac as Elector System ID */
            LA_MEMCPY (pAggEntry->LaDLAGElector,
                       gLaGlobalInfo.LaSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
            pAggEntry->bLaDLAGIsDesignatedElector = LA_TRUE;

            if (pAggEntry->DLAGMSSelectionWaitTmr.LaTmrFlag == LA_TMR_STOPPED)
            {
                if ((LaStartTimer (&(pAggEntry->DLAGMSSelectionWaitTmr))) !=
                    LA_SUCCESS)
                {
                    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "TMR: LaStartTimer failed for Master"
                            " Slave selection wait timer.\n");
                    return SNMP_FAILURE;
                }
            }

            /* Send high-priority event-update message to remote D-LAG nodes */
            LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
        else if (((UINT1) i4SetValFsLaPortChannelDLAGRedundancy)
                 == LA_DLAG_REDUNDANCY_OFF)
        {
            if (LaStopTimer (&pAggEntry->DLAGMSSelectionWaitTmr) == LA_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* Change The Node Role Played as None */
            pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
            LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);

            /* Copy Source Mac as Elector System ID */
            LA_MEMSET (pAggEntry->LaDLAGElector, 0, LA_MAC_ADDRESS_SIZE);
            pAggEntry->bLaDLAGIsDesignatedElector = LA_FALSE;

            /* Send high-priority event-update message to remote D-LAG nodes */
            LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_RED_SWITCHED_OFF;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelDLAGDistributingPortList
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelDLAGDistributingPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelDLAGDistributingPortList (INT4 i4FsLaPortChannelIfIndex,
                                               tSNMP_OCTET_STRING_TYPE
                                               *
                                               pSetValFsLaPortChannelDLAGDistributingPortList)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pAggEntry->DLAGDistributingPortList,
            pSetValFsLaPortChannelDLAGDistributingPortList->pu1_OctetList,
            pSetValFsLaPortChannelDLAGDistributingPortList->i4_Length);

    for (; u4DLAGDistributingIfIndex < LA_MAX_PORTS;
         u4DLAGDistributingIfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingPortList,
                                 u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE,
                                 bResult);

        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue the for loop */
            continue;
        }
        /* Get the oper staus of the port index, If the oper status is down, 
         * dont add into the distributing oper port list 
         * This is useful in the case of MSR save and restore, because
         * while restoration, directly set will be called. If the distribution port
         * is shut down, then dont add into the list */
        LaGetPortEntry ((UINT2) u4DLAGDistributingIfIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
        {
            OSIX_BITLIST_SET_BIT (pAggEntry->DLAGDistributingOperPortList,
                                  u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE);
            u4Count++;
        }
    }

    /* If the distributing port index is not zero, it has to be added to 
     * distributing port list */
    bResult = OSIX_FALSE;
    if (pAggEntry->u4DLAGDistributingIfIndex != 0)
    {
        /* If the port index is already added in the port list, skip adding into port list */
        OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingPortList,
                                 pAggEntry->u4DLAGDistributingIfIndex,
                                 LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_FALSE)
        {
            /* Get the oper staus of the port index, If the oper status is down, 
             * dont add into the distributing oper port list 
             * This is useful in the case of MSR save and restore, because
             * while restoration, directly set will be called. If the distribution port
             * is shut down, then dont add into the list */
            LaGetPortEntry ((UINT2) pAggEntry->u4DLAGDistributingIfIndex,
                            &pPortEntry);

            if (pPortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
            {
                OSIX_BITLIST_SET_BIT (pAggEntry->DLAGDistributingOperPortList,
                                      pAggEntry->u4DLAGDistributingIfIndex,
                                      LA_PORT_LIST_SIZE);
                OSIX_BITLIST_SET_BIT (pAggEntry->DLAGDistributingPortList,
                                      pAggEntry->u4DLAGDistributingIfIndex,
                                      LA_PORT_LIST_SIZE);
                u4Count++;
            }
        }
    }
    pAggEntry->u4DLAGDistributingPortListCount = u4Count;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelMCLAGSystemID
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelMCLAGSystemID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelMCLAGSystemID (INT4 i4FsLaPortChannelIfIndex,
                                    tMacAddr SetValFsLaPortChannelMCLAGSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tMacAddr            DefaultMacAddr;

    LA_MEMSET (DefaultMacAddr, 0, LA_MAC_ADDRESS_SIZE);

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LA_MEMCPY ((pAggEntry->DLAGSystem.SystemMacAddr),
               SetValFsLaPortChannelMCLAGSystemID, LA_MAC_ADDRESS_SIZE);

    /* When no mc-lag system-identifier command is given,the value will be set as
     * zero.Then bIsMCLAGGlobalSysIDConfigured flag   should be reset to FALSE*/

    if (LA_MEMCMP (&pAggEntry->DLAGSystem.SystemMacAddr, DefaultMacAddr,
                   LA_MAC_ADDRESS_SIZE) != 0)
    {
        pAggEntry->bIsMCLAGSystemIdConfigured = LA_TRUE;
    }
    else
    {
        pAggEntry->bIsMCLAGSystemIdConfigured = LA_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelMCLAGSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelMCLAGSystemPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelMCLAGSystemPriority (INT4 i4FsLaPortChannelIfIndex,
                                          INT4
                                          i4SetValFsLaPortChannelMCLAGSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggEntry->DLAGSystem.u2SystemPriority =
        (UINT2) i4SetValFsLaPortChannelMCLAGSystemPriority;

    /* We Need to change the Actor System priority as well */
    pAggEntry->AggConfigEntry.
        ActorSystem.u2SystemPriority =
        (UINT2) i4SetValFsLaPortChannelMCLAGSystemPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelClearStatistics
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                setValFsLaPortChannelClearStatistics
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelClearStatistics (INT4 i4FsLaPortChannelIfIndex,
                                      INT4
                                      i4SetValFsLaPortChannelClearStatistics)
{
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    UINT2               u2AggIndex = LA_INIT_VAL;

    if (i4SetValFsLaPortChannelClearStatistics != LA_ENABLED)
    {
        return SNMP_FAILURE;
    }

    if ((LaGetAggEntryByKey ((UINT2) i4FsLaPortChannelIfIndex, &pLaLacAggEntry))
        == LA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (pLaLacAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLaLacAggEntry->u1PortChannelClearCounters = LA_ENABLED;
    u2AggIndex = pLaLacAggEntry->u2AggIndex;

    LaUtilClearPortChannelCounters (u2AggIndex);

    pLaLacAggEntry->u1PortChannelClearCounters = LA_DISABLED;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaPortChannelMCLAGStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                setValFsLaPortChannelMCLAGStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortChannelMCLAGStatus (INT4 i4FsLaPortChannelIfIndex,
                                  INT4 i4SetValFsLaPortChannelMCLAGStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
#ifdef ICCH_WANTED
    UINT4               u4RolePlayed = 0;
#endif

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If input value and stored value are same then return success */
    if (pAggEntry->u1MCLAGStatus == (UINT1) i4SetValFsLaPortChannelMCLAGStatus)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValFsLaPortChannelMCLAGStatus == LA_MCLAG_ENABLED)
    {
        pAggEntry->u1MCLAGStatus = LA_MCLAG_ENABLED;
#ifdef ICCH_WANTED
        if (LA_MCLAG_SYSTEM_STATUS != LA_MCLAG_ENABLED)
        {
            pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_MASTER;
        }
        else
        {
            u4RolePlayed = LaIcchGetRolePlayed ();
            if (u4RolePlayed == LA_ICCH_MASTER)
            {
                pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_MASTER;
            }
            else if (u4RolePlayed == LA_ICCH_SLAVE)
            {
                pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_SLAVE;
            }
            else
            {
                pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_NONE;
            }
        }
#endif
#ifdef ICCH_WANTED
        /* MC-LAG functionality init */
        if (LaDLAGInit (pAggEntry) != LA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#endif
    }
    else
    {
        pAggEntry->u1MCLAGStatus = LA_MCLAG_DISABLED;
        if (LaDLAGDeInit (pAggEntry) != LA_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        LaVlanUpdatePortIsolationForMcLag (pAggEntry->u2AggIndex,
                                           VLAN_PI_UNMASK);
    }

    /* Give indication to Vlan for updating IVR */

    LaVlanUpdateMclagStatus (pAggEntry->u2AggIndex, pAggEntry->u1MCLAGStatus);

    /* disable STP on mc-lag interface */
    if (LaAstDisableStpOnPort ((UINT4) i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsLaPortChannelMacSelection
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
testValFsLaPortChannelMacSelection
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaPortChannelMacSelection (UINT4 *pu4ErrorCode,
                                      INT4 i4FsLaPortChannelIfIndex,
                                      INT4 i4TestValFsLaPortChannelMacSelection)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (i4TestValFsLaPortChannelMacSelection);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsLaPortChannelAdminMacAddress
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
testValFsLaPortChannelAdminMacAddress
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaPortChannelAdminMacAddress (UINT4 *pu4ErrorCode,
                                         INT4 i4FsLaPortChannelIfIndex,
                                         tMacAddr
                                         TestValFsLaPortChannelAdminMacAddress)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (TestValFsLaPortChannelAdminMacAddress);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsLaPortChannelSelectionPolicy
Input       :  The Indices
FsLaPortChannelIfIndex

The Object 
testValFsLaPortChannelSelectionPolicy
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaPortChannelSelectionPolicy (UINT4 *pu4ErrorCode,
                                         INT4 i4FsLaPortChannelIfIndex,
                                         INT4
                                         i4TestValFsLaPortChannelSelectionPolicy)
{
    UINT2               u2LocalPortId;
    UINT4               u4ContextId = 0;
    UINT4               u4BridgeMode = 0;
    INT4                i4RetVal = LA_SUCCESS;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsLaPortChannelSelectionPolicy < LA_MIN_DISTRIBUTION_VAL) ||
        (i4TestValFsLaPortChannelSelectionPolicy > LA_MAX_DISTRIBUTION_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifndef MPLS_WANTED
    if ((i4TestValFsLaPortChannelSelectionPolicy == LA_MPLS_VC_LABEL) ||
        (i4TestValFsLaPortChannelSelectionPolicy == LA_MPLS_TUNNEL_LABEL) ||
        (i4TestValFsLaPortChannelSelectionPolicy == LA_MPLS_VC_TUNNEL))
    {
        CLI_SET_ERR (CLI_LA_SELECTION_POLICY_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if (i4TestValFsLaPortChannelSelectionPolicy == LA_ISID)
    {
        i4RetVal =
            LaVcmGetContextInfoFromIfIndex ((UINT2) i4FsLaPortChannelIfIndex,
                                            &u4ContextId, &u2LocalPortId);
        if (i4RetVal == LA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4RetVal = LaL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

        if (i4RetVal == LA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* Bridge Mode should be only PBB */
        if ((u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_INVALID_BRIDGE_MODE))
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCheckLaStaticPortSpeed                             */
/*                                                                           */
/* Description        : This function is called to check                     */
/*                      portchannel port type and speed                      */
/*                                                                           */
/* Input(s)           : pu4ErrorCode,                                        */
/*                      pAggEntry,                                           */
/*                      i4PortChannelDefaultPortIndex                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS - On success                            */
/*                      SNMP_FAILURE - On failure                            */
/*****************************************************************************/
INT4
LaCheckLaStaticPortSpeed (UINT4 *pu4ErrorCode,
                          tLaLacAggEntry * pAggEntry,
                          INT4 i4PortChannelDefaultPortIndex)
{
    UINT2               u2Index = 0;
    INT4                i4ConfPortCtrlSpeed;
    INT4                i4TestValIssPortCtrlSpeed;
    tCfaIfInfo          CfaIfInfo;
    tLaLacPortEntry    *pPortEntry;

    /* When u1HwLaWithDiffPortSpeedSup is not supported,
     * LAG can't be created between ports that are operating in different speed
     */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_LA_WITH_DIFF_PORT_SPEED))
    {
        if (LaCfaGetIfInfo ((UINT4) i4PortChannelDefaultPortIndex, &CfaIfInfo)
            != CFA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Retrieval of port %d properties failed!\n",
                         i4PortChannelDefaultPortIndex);
            CLI_SET_ERR (CLI_LA_INVALID_PORT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        i4TestValIssPortCtrlSpeed = (INT4) CfaIfInfo.u4IfHighSpeed;

        for (u2Index = 1; u2Index <= ISS_HW_FRONT_PORTS_P1_To_P4; u2Index++)
        {
            LaGetPortEntry (u2Index, &pPortEntry);
            if ((pPortEntry != NULL) && (pPortEntry->bDynAggSelect == LA_TRUE))
            {
                if (pPortEntry->LaLacActorAdminInfo.u2IfKey ==
                    pAggEntry->AggConfigEntry.u2ActorAdminKey)
                {
                    if (LaCfaGetIfInfo (u2Index, &CfaIfInfo) == CFA_SUCCESS)
                    {
                        i4ConfPortCtrlSpeed = (INT4) CfaIfInfo.u4IfHighSpeed;
                        if (i4ConfPortCtrlSpeed != i4TestValIssPortCtrlSpeed)
                        {
                            CLI_SET_ERR (CLI_LA_NO_DIFF_SPEED);
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
        }
    }
    else
    {
        UNUSED_PARAM (u2Index);
        UNUSED_PARAM (i4ConfPortCtrlSpeed);
        UNUSED_PARAM (i4TestValIssPortCtrlSpeed);
        UNUSED_PARAM (CfaIfInfo);
        UNUSED_PARAM (pPortEntry);
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (pAggEntry);
        UNUSED_PARAM (i4PortChannelDefaultPortIndex);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDefaultPortIndex
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelDefaultPortIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDefaultPortIndex (UINT4 *pu4ErrorCode,
                                          INT4 i4FsLaPortChannelIfIndex,
                                          INT4
                                          i4TestValFsLaPortChannelDefaultPortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2PnacAuthStatus;
    UINT1               u1BridgedStatus = CFA_ENABLED;
    UINT1               u1PbPortType;

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LaL2IwfIsLaConfigAllowed
        ((UINT4) i4TestValFsLaPortChannelDefaultPortIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_MIRR_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsLaPortChannelDefaultPortIndex != 0)
    {
        if (LaSnmpLowValidatePortIndex
            (i4TestValFsLaPortChannelDefaultPortIndex) == LA_SUCCESS)
        {
            LaGetPortEntry ((UINT2) i4TestValFsLaPortChannelDefaultPortIndex,
                            &pPortEntry);
        }
        else
        {
            /* Invalid port entry */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

        if (pAggEntry == NULL || pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        if (pAggEntry == pPortEntry->pDefAggEntry)
        {
            /* Same port is being configured again, do nothing */
            return SNMP_SUCCESS;
        }
        if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Retrieval of port %d properties failed!\n",
                         pPortEntry->u2PortIndex);
            CLI_SET_ERR (CLI_LA_INVALID_PORT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        CfaGetIfBridgedIfaceStatus ((UINT4) i4FsLaPortChannelIfIndex,
                                    &u1BridgedStatus);
        if (CfaIfInfo.u1BridgedIface != u1BridgedStatus)
        {
            CLI_SET_ERR (CLI_LA_INCOMPATIBLE_MODE);
            *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
            return SNMP_FAILURE;
        }

        /* Interface MTU should match with the Port channel MTU */

        if (CfaIfInfo.u4IfMtu != pAggEntry->u4Mtu)
        {
            CLI_SET_ERR (CLI_LA_MTU_MISMATCH);
            *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
            return SNMP_FAILURE;
        }

        /* No ports should be already configured for the port channel,
         * The aggregate should be static (no dynamic selected  ports attached),
         * The port should be static Aggregated (no dynamic selection logic) */
        if ((pAggEntry->u1ConfigPortCount != 0) ||
            (pAggEntry->bDynPortRealloc == LA_TRUE) ||
            (pPortEntry->bDynAggSelect == LA_TRUE))
        {
            if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
            {
                CLI_SET_ERR (CLI_LA_KEY_CHANGE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            CLI_SET_ERR (CLI_LA_INVALID_DEF_PORT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* When u1HwLaCreationAtCepPortSup is not supported.
         * LAG can't be created on CEP port.
         */
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_LAG_ON_CEP_PORT))
        {
            if (L2IwfGetPbPortType
                ((UINT4) i4TestValFsLaPortChannelDefaultPortIndex,
                 &u1PbPortType) == L2IWF_SUCCESS)
            {
                if (u1PbPortType == L2IWF_CUSTOMER_EDGE_PORT)
                {
                    CLI_SET_ERR (CLI_LA_NO_CUSTOMER_EDGE_PORT_ERR);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            UNUSED_PARAM (u1PbPortType);
        }

        if (LaCheckLaStaticPortSpeed (pu4ErrorCode,
                                      pAggEntry,
                                      i4TestValFsLaPortChannelDefaultPortIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (LaPnacGetStartedStatus () == PNAC_TRUE)
        {
            LaL2IwfGetPortPnacAuthControl ((UINT2)
                                           i4TestValFsLaPortChannelDefaultPortIndex,
                                           &u2PnacAuthStatus);
            if (u2PnacAuthStatus != PNAC_PORTCNTRL_FORCEAUTHORIZED)
            {
                CLI_SET_ERR (CLI_LA_PNAC_ENABLE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelMaxPorts
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                testValFsLaPortChannelMaxPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelMaxPorts (UINT4 *pu4ErrorCode,
                                  INT4 i4FsLaPortChannelIfIndex,
                                  INT4 i4TestValFsLaPortChannelMaxPorts)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLaPortChannelMaxPorts < LA_PORT_CHANNEL_MIN_PORTS) ||
        (i4TestValFsLaPortChannelMaxPorts > LA_PORT_CHANNEL_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        if ((pAggEntry->LaLacpMode == LA_MODE_MANUAL) &&
            (pAggEntry->u1ConfigPortCount > i4TestValFsLaPortChannelMaxPorts))
        {
            CLI_SET_ERR (CLI_LA_INVALID_MAX_PORT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelSelectionPolicyBitList
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                testValFsLaPortChannelSelectionPolicyBitList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelSelectionPolicyBitList (UINT4 *pu4ErrorCode,
                                                INT4 i4FsLaPortChannelIfIndex,
                                                INT4
                                                i4TestValFsLaPortChannelSelectionPolicyBitList)
{

    UINT2               u2LocalPortId;
    UINT4               u4ContextId = 0;
    UINT4               u4BridgeMode = 0;
    INT4                i4RetVal = LA_SUCCESS;

    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsLaPortChannelSelectionPolicyBitList < LA_SELECT_SRC_MAC) ||
        (i4TestValFsLaPortChannelSelectionPolicyBitList > LA_SELECT_MAX_RANGE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsLaPortChannelSelectionPolicyBitList &
        LA_SELECT_SERVICE_INSTANCE)
    {
        i4RetVal =
            LaVcmGetContextInfoFromIfIndex ((UINT2) i4FsLaPortChannelIfIndex,
                                            &u4ContextId, &u2LocalPortId);
        if (i4RetVal == LA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4RetVal = LaL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

        if (i4RetVal == LA_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* Bridge Mode should be only PBB */
        if ((u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_INVALID_BRIDGE_MODE))
        {
            CLI_SET_ERR (CLI_LA_SERVICE_INSTANCE_POLICY_IN_PBB);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGDistributingPortIndex
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelDLAGDistributingPortIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGDistributingPortIndex (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4FsLaPortChannelIfIndex,
                                                   INT4
                                                   i4TestValFsLaPortChannelDLAGDistributingPortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2NextIndex = 0;
    UINT2               u2Index = 0;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /*  Distributed port configuration should not be allowed
     *  when D-LAG status is enabled,*/
    if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_DLAG_STATUS_ENABLED);
        return SNMP_FAILURE;
    }

    /* The following check is added in Test routine instead of Set routine
     * because, the code after this check involves scanning member port
     * list which is CPU utilizing operation; so scanning list is
     * avoided when Input Index is 0 or when input index is same as
     * previous Distributing index */
    if ((i4TestValFsLaPortChannelDLAGDistributingPortIndex == 0)
        || (pAggEntry->u4DLAGDistributingIfIndex ==
            (UINT4) i4TestValFsLaPortChannelDLAGDistributingPortIndex))
    {
        return SNMP_SUCCESS;
    }

    /* Check If Distributing port is operationally UP */
    LaGetPortEntry ((UINT2) i4TestValFsLaPortChannelDLAGDistributingPortIndex,
                    &pPortEntry);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pPortEntry->u1PortOperStatus == CFA_IF_DOWN)
    {
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_OPER_STATUS_DOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate Distributing port index
     * Ports that are already part of Port-channel cannot be used as
     * Distributing port, So following logic scans through member port list
     * and checks if distributing index is already present */
    while ((LaUtilGetNextAggPort ((UINT2) i4FsLaPortChannelIfIndex,
                                  u2Index, &u2NextIndex)) != LA_FAILURE)
    {
        if (u2NextIndex ==
            (UINT2) i4TestValFsLaPortChannelDLAGDistributingPortIndex)
        {
            CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_IS_MEMBER_PORT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        u2Index = u2NextIndex;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGSystemID
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelDLAGSystemID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGSystemID (UINT4 *pu4ErrorCode,
                                      INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr
                                      TestValFsLaPortChannelDLAGSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tMacAddr            TempMacAddr;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validate the Input D-LAG System ID
     * If input MAC Address is : 00:00:00:00 then Test is called before
     * Resetting D-LAG System ID to default Address, in this case No need
     * to validate Default Address.
     * Note: LaIsValidSystemID is not called for Default Address as 
     * address 00:00:00:00 is treated as invalid address in function 
     * LaIsValidSystemID ().  */
    LA_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));
    if ((LA_MEMCMP (TestValFsLaPortChannelDLAGSystemID, TempMacAddr,
                    LA_MAC_ADDRESS_SIZE)) != 0)
    {
        if ((LaIsValidSystemID (TestValFsLaPortChannelDLAGSystemID))
            != LA_SUCCESS)
        {
            CLI_SET_ERR (CLI_LA_DLAG_INVALID_SYS_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /*  Distributed System ID configuration should not be allowed
     *  when D-LAG status is enabled,*/
    if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_DLAG_SYS_ID_DLAG_STATUS_ENABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelDLAGSystemPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGSystemPriority (UINT4 *pu4ErrorCode,
                                            INT4 i4FsLaPortChannelIfIndex,
                                            INT4
                                            i4TestValFsLaPortChannelDLAGSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Input System Priority range-checking 
     * It should be within the range 0 (MIN) >= input_value <= 65535 (MAX) */
    if ((i4TestValFsLaPortChannelDLAGSystemPriority < LA_DLAG_MIN_SYS_PRIORITY)
        || (i4TestValFsLaPortChannelDLAGSystemPriority >
            LA_DLAG_MAX_SYS_PRIORITY))
    {
        CLI_SET_ERR (CLI_LA_DLAG_INVALID_SYS_PRIORITY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /*  Distributed System priority configuration should not be allowed
     *  when D-LAG status is enabled,*/
    if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_DLAG_SYS_PRIO_DLAG_STATUS_ENABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsLaPortChannelDLAGPeriodicSyncTime
Input       :  The Indices
                FsLaPortChannelIfIndex                                                                                                                                  
                The Object
                testValFsLaPortChannelDLAGPeriodicSyncTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGPeriodicSyncTime (UINT4 *pu4ErrorCode,
                                              INT4 i4FsLaPortChannelIfIndex,
                                              UINT4
                                              u4TestValFsLaPortChannelDLAGPeriodicSyncTime)
{

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Input Value range checking : 0 >= Periodic-sync time <= 90000
     * Since u4TestValFsLaPortChannelDLAGPeriodicSyncTime is of type unsigned
     * integer checking 'less than zero' is not necessary as 'negative values'
     * will always become 'greater than 90000' after wrap around
     * after wrap around -ve value will be wrapped around to 
     * = UINT_MAX - ((1-(ABS(-ve val)))) */
    if (u4TestValFsLaPortChannelDLAGPeriodicSyncTime >
        LA_DLAG_MAX_PERIODIC_SYNC_TIME)
    {
        CLI_SET_ERR (CLI_LA_DLAG_INVALID_PERIODIC_SYNC_TIME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGMSSelectionWaitTime
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                testValFsLaPortChannelDLAGMSSelectionWaitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGMSSelectionWaitTime (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsLaPortChannelIfIndex,
                                                 UINT4
                                                 u4TestValFsLaPortChannelDLAGMSSelectionWaitTime)
{

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Input Value range checking : 0 >= MS-Selection-wait time <= 90000
     * Since u4TestValFsLaPortChannelDLAGMSSelectionWaitTime is of type unsigned
     * integer checking 'less than zero' is not necessary as 'negative values'
     * will always become 'greater than 90000' after wrap around
     * after wrap around -ve value will be wrapped around to 
     * = UINT_MAX - ((1-(ABS(-ve val)))) */
    if (u4TestValFsLaPortChannelDLAGMSSelectionWaitTime >
        LA_DLAG_MAX_MS_SELECTION_WAIT_TIME)
    {
        CLI_SET_ERR (CLI_LA_DLAG_INVALID_MS_SELECTION_WAIT_TIME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelDLAGStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsLaPortChannelIfIndex,
                                    INT4 i4TestValFsLaPortChannelDLAGStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tMacAddr            TempMacAddr;
    tLaPortList         DefaultPortList;

    MEMSET (&DefaultPortList, 0, LA_PORT_LIST_SIZE);

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Only valid inputs for FsLaPortChannelDLAGStatus MIB object
     * are enable/disable */
    if ((i4TestValFsLaPortChannelDLAGStatus != LA_DLAG_ENABLED)
        && (i4TestValFsLaPortChannelDLAGStatus != LA_DLAG_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Check If the D-LAG mandatory parameters
     * - D-LAG System ID,
     * - D-LAG Distributing Index is configured
     * Note: D-LAG inter-columnar dependency: D-LAG status
     * must not be allowed without mandatory parameters being SET
     */
    LA_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));
    if ((LA_MEMCMP
         ((pAggEntry->DLAGDistributingPortList), DefaultPortList,
          LA_PORT_LIST_SIZE) == 0)
        ||
        (LA_MEMCMP
         ((pAggEntry->DLAGSystem.SystemMacAddr), TempMacAddr,
          LA_MAC_ADDRESS_SIZE) == 0))
    {
        CLI_SET_ERR (CLI_LA_DLAG_STATUS_INTER_COLUMNAR_DEPENDENCY_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGRedundancy
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelDLAGMSSelectionPolicy
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGRedundancy (UINT4 *pu4ErrorCode,
                                        INT4 i4FsLaPortChannelIfIndex,
                                        INT4
                                        i4TestValFsLaPortChannelDLAGRedundancy)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Only valid inputs for FsLaPortChannelDLAGRedundancy MIB object
     * are ON/OFF */
    if ((i4TestValFsLaPortChannelDLAGRedundancy != LA_DLAG_REDUNDANCY_ON)
        && (i4TestValFsLaPortChannelDLAGRedundancy != LA_DLAG_REDUNDANCY_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelDLAGDistributingPortList
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object
                testValFsLaPortChannelDLAGDistributingPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelDLAGDistributingPortList (UINT4 *pu4ErrorCode,
                                                  INT4 i4FsLaPortChannelIfIndex,
                                                  tSNMP_OCTET_STRING_TYPE
                                                  *
                                                  pTestValFsLaPortChannelDLAGDistributingPortList)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT2               u2NextIndex = 0;
    UINT2               u2Index = 0;
    BOOL1               bResult = OSIX_FALSE;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        /* If Globally DLAG enabled per channel config not allowed */
        CLI_SET_ERR (CLI_LA_DLAG_PER_CHANNEL_CONFIG_ERROR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if (LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /*  Distributed port configuration should not be allowed
     *  when D-LAG status is enabled,*/
    if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_DLAG_STATUS_ENABLED);
        return SNMP_FAILURE;
    }

    /* The following check is added in Test routine instead of Set routine
     * because, the code after this check involves scanning member port
     * list which is CPU utilizing operation; so scanning list is
     * avoided when Input Index is 0 or when input index is same as
     * previous Distributing index */

    if ((LA_MEMCMP (pAggEntry->DLAGDistributingPortList,
                    pTestValFsLaPortChannelDLAGDistributingPortList->
                    pu1_OctetList, LA_PORT_LIST_SIZE) == 0))
    {
        return SNMP_SUCCESS;
    }
    /* Check If Distributing port is operationally UP */

    for (; u4DLAGDistributingIfIndex <= LA_MAX_PORTS;
         u4DLAGDistributingIfIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET
            (pTestValFsLaPortChannelDLAGDistributingPortList->pu1_OctetList,
             u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE, bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue the for loop */
            continue;
        }
        /* Check If Distributing port is operationally UP,
         * If not then packet transmission attempt need not
         * be made on the failed distributing port */
        LaGetPortEntry ((UINT2) u4DLAGDistributingIfIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }
        if (pPortEntry->u1PortOperStatus == CFA_IF_DOWN)
        {
            CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_OPER_STATUS_DOWN);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    /* Validate Distributing port index
     * Ports that are already part of Port-channel cannot be used as
     * Distributing port, So following logic scans through member port list
     * and checks if distributing index is already present */
    while ((LaUtilGetNextAggPort ((UINT2) i4FsLaPortChannelIfIndex,
                                  u2Index, &u2NextIndex)) != LA_FAILURE)
    {
        for (u4DLAGDistributingIfIndex = 1;
             u4DLAGDistributingIfIndex <= LA_MAX_PORTS;
             u4DLAGDistributingIfIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET
                (pTestValFsLaPortChannelDLAGDistributingPortList->pu1_OctetList,
                 u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                continue;
            }
            if (u2NextIndex == (UINT2) u4DLAGDistributingIfIndex)
            {
                CLI_SET_ERR (CLI_LA_DLAG_DIST_PORT_IS_MEMBER_PORT);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        u2Index = u2NextIndex;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelMCLAGSystemID
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelMCLAGSystemID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelMCLAGSystemID (UINT4 *pu4ErrorCode,
                                       INT4 i4FsLaPortChannelIfIndex,
                                       tMacAddr
                                       TestValFsLaPortChannelMCLAGSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tMacAddr            TempMacAddr;

    /* Validate the Input MC-LAG System ID
     * If input MAC Address is : 00:00:00:00 then Test is called before
     * Resetting MC-LAG System ID to default Address, in this case No need
     * to validate Default Address.
     * Note: LaIsValidSystemID is not called for Default Address as
     * address 00:00:00:00 is treated as invalid address in function
     * LaIsValidSystemID ().  */
    LA_MEMSET (TempMacAddr, 0, sizeof (TempMacAddr));

    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        return SNMP_FAILURE;
    }

    if ((LA_MEMCMP (TestValFsLaPortChannelMCLAGSystemID, TempMacAddr,
                    LA_MAC_ADDRESS_SIZE)) != 0)
    {
        if ((LaIsValidSystemID (TestValFsLaPortChannelMCLAGSystemID))
            != LA_SUCCESS)
        {
            CLI_SET_ERR (CLI_LA_MCLAG_INVALID_SYS_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /*  MC-LAG System ID configuration should not be allowed
     *  when MC-LAG status is enabled,*/
    if (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_MCLAG_SYS_ID_MCLAG_STATUS_ENABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelMCLAGSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelMCLAGSystemPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelMCLAGSystemPriority (UINT4 *pu4ErrorCode,
                                             INT4 i4FsLaPortChannelIfIndex,
                                             INT4
                                             i4TestValFsLaPortChannelMCLAGSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        return SNMP_FAILURE;
    }

    /* Input System Priority range-checking
     * It should be within the range 0 (MIN) >= input_value <= 65535 (MAX) */
    if ((i4TestValFsLaPortChannelMCLAGSystemPriority < LA_DLAG_MIN_SYS_PRIORITY)
        || (i4TestValFsLaPortChannelMCLAGSystemPriority >
            LA_DLAG_MAX_SYS_PRIORITY))
    {
        CLI_SET_ERR (CLI_LA_MCLAG_INVALID_SYS_PRIORITY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /*  MC-LAG System priority configuration should not be allowed
     *  when MC-LAG status is enabled,*/
    if (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_LA_MCLAG_SYS_PRIO_MCLAG_STATUS_ENABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortChannelClearStatistics
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelClearStatistics
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelClearStatistics (UINT4 *pu4ErrorCode,
                                         INT4 i4FsLaPortChannelIfIndex,
                                         INT4
                                         i4TestValFsLaPortChannelClearStatistics)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if ((i4TestValFsLaPortChannelClearStatistics != LA_ENABLED) &&
        (i4TestValFsLaPortChannelClearStatistics != LA_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the port channel entry */
    if ((LaGetAggEntryByKey ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry))
        == LA_FAILURE)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 Function    :  nmhTestv2FsLaPortChannelMCLAGStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex

                The Object 
                testValFsLaPortChannelMCLAGStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortChannelMCLAGStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsLaPortChannelIfIndex,
                                     INT4 i4TestValFsLaPortChannelMCLAGStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaPortList         DefaultPortList;
    UINT4               u4PortContextId = VCM_INVALID_CONTEXT_ID;
    UINT2               u2LocalPort = 0;

    MEMSET (&DefaultPortList, 0, LA_PORT_LIST_SIZE);

    if (gLaMclagSystemControl != LA_START)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_SHUTDOWN_ERR);
        return SNMP_FAILURE;
    }

    /* Only valid inputs for FsLaPortChannelMCLAGStatus MIB object
     * are enable/disable */
    if ((i4TestValFsLaPortChannelMCLAGStatus != LA_MCLAG_ENABLED)
        && (i4TestValFsLaPortChannelMCLAGStatus != LA_MCLAG_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the port channel index */
    if ((LaSnmpLowValidateAggIndex (i4FsLaPortChannelIfIndex)) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get the port channel entry */
    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* If the member ports are not in LACP mode */

    if (pAggEntry->LaLacpMode == LA_MODE_MANUAL)
    {
        CLI_SET_ERR (CLI_LA_PORT_NON_LACP_MODE);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LaVcmGetContextInfoFromIfIndex ((UINT4) i4FsLaPortChannelIfIndex,
                                        &u4PortContextId, &u2LocalPort)
        == LA_FAILURE)
    {
        /* Port does not belong to any context */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4PortContextId != VCM_DEFAULT_CONTEXT)
    {
        CLI_SET_ERR (CLI_LA_MCLAG_NON_DEFAULT_CTXT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLaPortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaPortChannelTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLaPortTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsLaPortTable
Input       :  The Indices
FsLaPortIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaPortTable (INT4 i4FsLaPortIndex)
{
    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsLaPortTable
Input       :  The Indices
FsLaPortIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaPortTable (INT4 *pi4FsLaPortIndex)
{
    if (LaSnmpLowGetFirstValidPortIndex (pi4FsLaPortIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexFsLaPortTable
Input       :  The Indices
FsLaPortIndex
nextFsLaPortIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaPortTable (INT4 i4FsLaPortIndex, INT4 *pi4NextFsLaPortIndex)
{
    if (LaSnmpLowGetNextValidPortIndex (i4FsLaPortIndex,
                                        pi4NextFsLaPortIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsLaPortMode
Input       :  The Indices
FsLaPortIndex

The Object 
retValFsLaPortMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortMode (INT4 i4FsLaPortIndex, INT4 *pi4RetValFsLaPortMode)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsLaPortMode = pPortEntry->LaLacpMode;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsLaPortBundleState
Input       :  The Indices
FsLaPortIndex

The Object 
retValFsLaPortBundleState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortBundleState (INT4 i4FsLaPortIndex,
                           INT4 *pi4RetValFsLaPortBundleState)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry;

    *pi4RetValFsLaPortBundleState = LA_PORT_DOWN;

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        pAggEntry = pPortEntry->pAggEntry;

        if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
        {
            if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
            {
                *pi4RetValFsLaPortBundleState = LA_PORT_UP_INDIVIDUAL;

            }
            else if (pPortEntry->u1PortOperStatus == LA_OPER_DOWN)
            {
                *pi4RetValFsLaPortBundleState = LA_PORT_DOWN;
            }
        }
        else                    /* if( pPortEntry->LaLacpMode != LA_MODE_DISABLED) */
        {
            if (pAggEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPortEntry->u1PortOperStatus == LA_OPER_DOWN)
            {
                *pi4RetValFsLaPortBundleState = LA_PORT_DOWN;
            }
            else if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
            {
                if ((pPortEntry->AggSelected == LA_SELECTED) &&
                    (pPortEntry->LaLacActorInfo.
                     LaLacPortState.LaDistributing == LA_TRUE))
                {
#ifdef NPAPI_WANTED
                    if (pPortEntry->LaLacHwPortStatus !=
                        LA_HW_PORT_ADDED_IN_LAGG)
                    {

                        if (((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
                             (pPortEntry->LaLacActorInfo.
                              LaLacPortState.LaAggregation == LA_TRUE)) &&
                            /* Sync is TRUE and Collecting and Distributing
                             * is FALSE */
                            (((pPortEntry->LaLacActorInfo.
                               LaLacPortState.LaSynchronization == LA_TRUE) &&
                              (pPortEntry->LaLacActorInfo.
                               LaLacPortState.LaCollecting != LA_TRUE) &&
                              (pPortEntry->LaLacActorInfo.
                               LaLacPortState.LaDistributing != LA_TRUE)) ||
                             /* Sync is FALSE and Collecting and Distributing
                              * is TRUE */
                             ((pPortEntry->LaLacActorInfo.
                               LaLacPortState.LaSynchronization == LA_FALSE) &&
                              ((pPortEntry->LaLacActorInfo.
                                LaLacPortState.LaCollecting == LA_TRUE) &&
                               (pPortEntry->LaLacActorInfo.
                                LaLacPortState.LaDistributing == LA_TRUE)))))
                        {
                            *pi4RetValFsLaPortBundleState = LA_PORT_STANDBY;
                        }
                        else
                        {
                            *pi4RetValFsLaPortBundleState = LA_PORT_DOWN;
                        }
                    }
                    else
#endif
                    {
                        *pi4RetValFsLaPortBundleState = LA_PORT_UP_IN_BNDL;
                    }
                }
                else if (pPortEntry->AggSelected == LA_STANDBY)
                {
                    *pi4RetValFsLaPortBundleState = LA_PORT_STANDBY;
                }
                else if ((pPortEntry->AggSelected == LA_UNSELECTED)
                         ||
                         ((pPortEntry->AggSelected == LA_SELECTED) &&
                          (pPortEntry->LaLacActorInfo.
                           LaLacPortState.LaDistributing != LA_TRUE)))
                {
                    if (gu4PartnerConfig == LA_ENABLED)
                    {
                        *pi4RetValFsLaPortBundleState = LA_PORT_UP_INDIVIDUAL;
                    }
                    else if (((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
                              (pPortEntry->LaLacActorInfo.
                               LaLacPortState.LaAggregation == LA_TRUE)) &&
                             /* Sync is TRUE and Collecting and Distributing
                              * is FALSE */
                             (((pPortEntry->LaLacActorInfo.
                                LaLacPortState.LaSynchronization == LA_TRUE) &&
                               (pPortEntry->LaLacActorInfo.
                                LaLacPortState.LaCollecting != LA_TRUE) &&
                               (pPortEntry->LaLacActorInfo.
                                LaLacPortState.LaDistributing != LA_TRUE)) ||
                              /* Sync is FALSE and Collecting and Distributing
                               * is TRUE */
                              ((pPortEntry->LaLacActorInfo.
                                LaLacPortState.LaSynchronization == LA_FALSE) &&
                               ((pPortEntry->LaLacActorInfo.
                                 LaLacPortState.LaCollecting == LA_TRUE) &&
                                (pPortEntry->LaLacActorInfo.
                                 LaLacPortState.LaDistributing == LA_TRUE)))))
                    {
                        *pi4RetValFsLaPortBundleState = LA_PORT_STANDBY;
                    }
                    else
                    {
                        *pi4RetValFsLaPortBundleState = LA_PORT_DOWN;
                    }
                }
                else if (((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
                          (pPortEntry->LaLacActorInfo.
                           LaLacPortState.LaAggregation == LA_TRUE)) &&
                         /* Sync is TRUE and Collecting and Distributing
                          * is FALSE */
                         (((pPortEntry->LaLacActorInfo.
                            LaLacPortState.LaSynchronization == LA_TRUE) &&
                           (pPortEntry->LaLacActorInfo.
                            LaLacPortState.LaCollecting != LA_TRUE) &&
                           (pPortEntry->LaLacActorInfo.
                            LaLacPortState.LaDistributing != LA_TRUE)) ||
                          /* Sync is FALSE and Collecting and Distributing
                           * is TRUE */
                          ((pPortEntry->LaLacActorInfo.
                            LaLacPortState.LaSynchronization == LA_FALSE) &&
                           ((pPortEntry->LaLacActorInfo.
                             LaLacPortState.LaCollecting == LA_TRUE) &&
                            (pPortEntry->LaLacActorInfo.
                             LaLacPortState.LaDistributing == LA_TRUE)))))
                {
                    *pi4RetValFsLaPortBundleState = LA_PORT_STANDBY;
                }

            }

            else if (((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
                      (pPortEntry->LaLacActorInfo.
                       LaLacPortState.LaAggregation == LA_TRUE)) &&
                     /* Sync is TRUE and Collecting and Distributing
                      * is FALSE */
                     (((pPortEntry->LaLacActorInfo.
                        LaLacPortState.LaSynchronization == LA_TRUE) &&
                       (pPortEntry->LaLacActorInfo.
                        LaLacPortState.LaCollecting != LA_TRUE) &&
                       (pPortEntry->LaLacActorInfo.
                        LaLacPortState.LaDistributing != LA_TRUE)) ||
                      /* Sync is FALSE and Collecting and Distributing
                       * is TRUE */
                      ((pPortEntry->LaLacActorInfo.
                        LaLacPortState.LaSynchronization == LA_FALSE) &&
                       ((pPortEntry->LaLacActorInfo.
                         LaLacPortState.LaCollecting == LA_TRUE) &&
                        (pPortEntry->LaLacActorInfo.
                         LaLacPortState.LaDistributing == LA_TRUE)))))
            {
                *pi4RetValFsLaPortBundleState = LA_PORT_STANDBY;
            }

        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsLaPortActorResetAdminState
Input       :  The Indices
FsLaPortIndex

The Object 
retValFsLaPortActorResetAdminState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortActorResetAdminState (INT4 i4FsLaPortIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsLaPortActorResetAdminState)
{

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        pRetValFsLaPortActorResetAdminState->pu1_OctetList[0] = 0;
        pRetValFsLaPortActorResetAdminState->i4_Length = 1;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsLaPortAggregateWaitTime
Input       :  The Indices
FsLaPortIndex

The Object 
retValFsLaPortAggregateWaitTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortAggregateWaitTime (INT4 i4FsLaPortIndex,
                                 UINT4 *pu4RetValFsLaPortAggregateWaitTime)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValFsLaPortAggregateWaitTime =
            (pPortEntry->u4WaitWhileTime) * 100;

        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsLaPortPartnerResetAdminState
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                retValFsLaPortPartnerResetAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortPartnerResetAdminState (INT4 i4FsLaPortIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsLaPortPartnerResetAdminState)
{
    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        pRetValFsLaPortPartnerResetAdminState->pu1_OctetList[0] = 0;
        pRetValFsLaPortPartnerResetAdminState->i4_Length = 1;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortActorAdminPort
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                retValFsLaPortActorAdminPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortActorAdminPort (INT4 i4FsLaPortIndex,
                              INT4 *pi4RetValFsLaPortActorAdminPort)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry) == LA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsLaPortActorAdminPort =
        pPortEntry->LaLacActorAdminInfo.u2IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaPortRestoreMtu
 Input       :  The Indices
                FsLaPortIndex

The Object   :  retValFsLaPortRestoreMtu
Output       :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
Returns      :   SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortRestoreMtu (INT4 i4FsLaPortIndex,
                          INT4 *pi4RetValFsLaPortRestoreMtu)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    /* Check if an entry for port index exists */
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsLaPortRestoreMtu = pPortEntry->u4RestoreMtu;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortSelectAggregator
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                retValFsLaPortSelectAggregator
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortSelectAggregator (INT4 i4FsLaPortIndex,
                                INT4 *pi4RetValFsLaPortSelectAggregator)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->bDynAggSelect == LA_TRUE)
    {
        *pi4RetValFsLaPortSelectAggregator = LA_PORT_DYNAMIC_AGG;
    }
    else
    {
        *pi4RetValFsLaPortSelectAggregator = LA_PORT_STATIC_AGG;
    }

    return SNMP_SUCCESS;
}

 /****************************************************************************
  Function    :  nmhGetFsLaPortErrStateDetCount
  Input       :  The Indices
                 FsLaPortIndex

                 The Object
                 retValFsLaPortErrStateDetCount
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortErrStateDetCount (INT4 i4FsLaPortIndex,
                                UINT4 *pu4RetValFsLaPortErrStateDetCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get the error detected count */
    *pu4RetValFsLaPortErrStateDetCount = pPortEntry->u4ErrorDetectCount;

    return SNMP_SUCCESS;
}

 /****************************************************************************
  Function    :  nmhGetFsLaPortErrStateRecCount
  Input       :  The Indices
                 FsLaPortIndex

                 The Object
                 retValFsLaPortErrStateRecCount
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsLaPortErrStateRecCount (INT4 i4FsLaPortIndex,
                                UINT4 *pu4RetValFsLaPortErrStateRecCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLaPortErrStateRecCount = pPortEntry->u4RecTrgdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortDefaultedStateThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortDefaultedStateThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortDefaultedStateThreshold (INT4 i4FsLaPortIndex,
                                       UINT4
                                       *pu4RetValFsLaPortDefaultedStateThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortDefaultedStateThreshold =
        pPortEntry->u4DefaultedStateThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortHardwareFailureRecThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortHardwareFailureRecThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortHardwareFailureRecThreshold (INT4 i4FsLaPortIndex,
                                           UINT4
                                           *pu4RetValFsLaPortHardwareFailureRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortHardwareFailureRecThreshold =
        pPortEntry->u4HardwareFailureRecThreshold;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaPortSameStateRecThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortSameStateRecThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortSameStateRecThreshold (INT4 i4FsLaPortIndex,
                                     UINT4
                                     *pu4RetValFsLaPortSameStateRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortSameStateRecThreshold =
        pPortEntry->u4SameStateRecThreshold;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaPortUpInBundleCount
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortUpInBundleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortUpInBundleCount (INT4 i4FsLaPortIndex,
                               UINT4 *pu4RetValFsLaPortUpInBundleCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortUpInBundleCount = pPortEntry->u4UpInBundleCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortDownInBundleCount
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortDownInBundleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortDownInBundleCount (INT4 i4FsLaPortIndex,
                                 UINT4 *pu4RetValFsLaPortDownInBundleCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortDownInBundleCount = pPortEntry->u4DownInBundleCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortBundStChgTimeStamp
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortBundStChgTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortBundStChgTimeStamp (INT4 i4FsLaPortIndex,
                                  UINT4 *pu4RetValFsLaPortBundStChgTimeStamp)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortBundStChgTimeStamp = pPortEntry->u4BundStChgTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortDownInBundleReason
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortDownInBundleReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortDownInBundleReason (INT4 i4FsLaPortIndex,
                                  INT4 *pi4RetValFsLaPortDownInBundleReason)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsLaPortDownInBundleReason = pPortEntry->u1PortDownReason;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortErrStateDetTimeStamp
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortErrStateDetTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortErrStateDetTimeStamp (INT4 i4FsLaPortIndex,
                                    UINT4
                                    *pu4RetValFsLaPortErrStateDetTimeStamp)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortErrStateDetTimeStamp = pPortEntry->u4ErrStateDetTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortErrStateRecTimeStamp
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortErrStateRecTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortErrStateRecTimeStamp (INT4 i4FsLaPortIndex,
                                    UINT4
                                    *pu4RetValFsLaPortErrStateRecTimeStamp)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortErrStateRecTimeStamp = pPortEntry->u4ErrStateRecTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortRecTrigReason
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortRecTrigReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortRecTrigReason (INT4 i4FsLaPortIndex,
                             INT4 *pi4RetValFsLaPortRecTrigReason)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsLaPortRecTrigReason = pPortEntry->u1RecoveryTmrReason;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortDefStateRecCount
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortDefStateRecCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortDefStateRecCount (INT4 i4FsLaPortIndex,
                                UINT4 *pu4RetValFsLaPortDefStateRecCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortDefStateRecCount =
        pPortEntry->u4DefaultedStateRecTrgrdCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortSameStateRecCount
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortSameStateRecCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortSameStateRecCount (INT4 i4FsLaPortIndex,
                                 UINT4 *pu4RetValFsLaPortSameStateRecCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortSameStateRecCount = pPortEntry->u4SameStateCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortHwFailRecCount
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortHwFailRecCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortHwFailRecCount (INT4 i4FsLaPortIndex,
                              UINT4 *pu4RetValFsLaPortHwFailRecCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortHwFailRecCount =
        pPortEntry->u4HardwareFailureRecTrgrdCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortDefStateRecTimeStamp
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortDefStateRecTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortDefStateRecTimeStamp (INT4 i4FsLaPortIndex,
                                    UINT4
                                    *pu4RetValFsLaPortDefStateRecTimeStamp)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortDefStateRecTimeStamp = pPortEntry->u4DefStateRecTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortSameStateRecTimeStamp
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortSameStateRecTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortSameStateRecTimeStamp (INT4 i4FsLaPortIndex,
                                     UINT4
                                     *pu4RetValFsLaPortSameStateRecTimeStamp)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortSameStateRecTimeStamp = pPortEntry->u4SameStateRecTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortHwFailRecTimeStamp
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortHwFailRecTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortHwFailRecTimeStamp (INT4 i4FsLaPortIndex,
                                  UINT4 *pu4RetValFsLaPortHwFailRecTimeStamp)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsLaPortHwFailRecTimeStamp = pPortEntry->u4HwFailRecTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaPortRecState
 Input       :  The Indices
                FsLaPortIndex

                The Object
                retValFsLaPortRecState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaPortRecState (INT4 i4FsLaPortIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsLaPortRecState)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValFsLaPortRecState->pu1_OctetList[0] = pPortEntry->u1RecState;
    pRetValFsLaPortRecState->i4_Length = sizeof (UINT1);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsLaPortMode
Input       :  The Indices
FsLaPortIndex

The Object 
setValFsLaPortMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaPortMode (INT4 i4FsLaPortIndex, INT4 i4SetValFsLaPortMode)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacpMode         OldLacpMode;
    tLaLacAggEntry     *pAggEntry;
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1BridgedIfaceStatus = CFA_ENABLED;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->LaLacpMode == (UINT4) i4SetValFsLaPortMode)
    {
        CfaGetIfBridgedIfaceStatus ((UINT4) i4FsLaPortIndex,
                                    &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_DISABLED)
        {
            VlanUnmapPort ((UINT2) i4FsLaPortIndex);
        }
        return SNMP_SUCCESS;
    }

    /* MirrorToPort should not be allowed to aggregate */
    if (LaIssMirrorStatusCheck ((UINT2) i4FsLaPortIndex) == ISS_FAILURE)
    {
        CLI_SET_ERR (CLI_LA_MIRRORTO_PORT_ERR);
        return SNMP_FAILURE;
    }

    pAggEntry = pPortEntry->pAggEntry;
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    OldLacpMode = pPortEntry->LaLacpMode;

    if ((pAggEntry->u1ConfigPortCount == 0)
        || (pAggEntry->u1ConfigPortCount == 1))
    {
        pAggEntry->LaLacpMode = i4SetValFsLaPortMode;
    }
    if ((i4SetValFsLaPortMode != LA_MODE_DISABLED)
        && (pAggEntry->LaLacpMode != (UINT4) i4SetValFsLaPortMode))
    {
        CLI_SET_ERR (CLI_LA_INCOMPATIBLE_MODE);
        return SNMP_FAILURE;
    }

    if (i4SetValFsLaPortMode == LA_MODE_MANUAL)
    {
        if (pAggEntry->u1ConfigPortCount > pAggEntry->u1MaxPortsToAttach)
        {
            CLI_SET_ERR (CLI_LA_MAXPORT_MANUAL_ERR);
            return SNMP_FAILURE;
        }
    }

    if (LaChangeLacpMode (pPortEntry, OldLacpMode,
                          i4SetValFsLaPortMode) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_CHANGE_LACPMODE_ERR);
        return SNMP_FAILURE;
    }

    LaIcchGetIcclIfIndex (&u4IcclIfIndex);

    if (pAggEntry->u2AggIndex == u4IcclIfIndex)
    {
        LA_UNLOCK ();
        if (i4SetValFsLaPortMode == LA_MODE_MANUAL)
        {
            /* Disable LLDP on ICCL ports */
            if (LA_TRUE !=
                LaLLdpUpdateLLdpStatusOnIccl ((UINT4) i4FsLaPortIndex,
                                              LA_AGG_CAPABLE))
            {
                LA_TRC (ALL_FAILURE_TRC,
                        "Disabling LLDP on ICCL port has failed\r\n");
            }
        }
        else if (i4SetValFsLaPortMode == LA_MODE_DISABLED)
        {
            /* Enable LLDP when a port is removed from ICCL port channel */
            if (LA_TRUE !=
                LaLLdpUpdateLLdpStatusOnIccl ((UINT4) i4FsLaPortIndex,
                                              LA_AGG_NOT_CAPABLE))
            {
                LA_TRC (ALL_FAILURE_TRC,
                        "Enabling LLDP on ICCL port has failed\r\n");
            }

        }
        LA_LOCK ();
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortActorResetAdminState
Input       :  The Indices
FsLaPortIndex

The Object 
setValFsLaPortActorResetAdminState
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaPortActorResetAdminState (INT4 i4FsLaPortIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsLaPortActorResetAdminState)
{
    UINT1               u1SetPortState;

    u1SetPortState = pSetValFsLaPortActorResetAdminState->pu1_OctetList[0];

    if ((u1SetPortState & LA_LACPACTIVITY_BITMASK))
    {
        LaChangeActorPortState ((UINT2) i4FsLaPortIndex,
                                LA_LACP_ACTIVITY, LA_RESET);
    }

    if ((u1SetPortState & LA_LACPTIMEOUT_BITMASK))
    {
        LaChangeActorPortState ((UINT2) i4FsLaPortIndex,
                                LA_LACP_TIMEOUT, LA_RESET);
    }

    if ((u1SetPortState & LA_AGGREGATION_BITMASK))
    {
        LaChangeActorPortState ((UINT2) i4FsLaPortIndex,
                                LA_AGGREGATION, LA_RESET);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaPortAggregateWaitTime
Input       :  The Indices
FsLaPortIndex

The Object 
setValFsLaPortAggregateWaitTime
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsLaPortAggregateWaitTime (INT4 i4FsLaPortIndex,
                                 UINT4 u4SetValFsLaPortAggregateWaitTime)
{

    tLaLacPortEntry    *pPortEntry;
    UINT4               u4PresentWaitTime = 0;
    UINT4               u4RemainingTime = 0;
    UINT4               u4ElapsedTime = 0;
    UINT4               u4StartTmrPeriod = 0;
    UINT4               u4SetValTime = 0;

    if (LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry) == LA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u4SetValTime = u4SetValFsLaPortAggregateWaitTime / 100;

    if (pPortEntry->WaitWhileTmr.LaTmrFlag == LA_TMR_STOPPED)
    {
        pPortEntry->u4WaitWhileTime = u4SetValTime;

        return SNMP_SUCCESS;
    }
    else if (pPortEntry->WaitWhileTmr.LaTmrFlag == LA_TMR_RUNNING)
    {
        if (LA_GET_REMAINING_TIME (LA_TIMER_LIST_ID,
                                   &pPortEntry->WaitWhileTmr.LaAppTimer,
                                   &u4RemainingTime) != LA_TMR_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4RemainingTime == 0)
        {
            pPortEntry->u4WaitWhileTime = u4SetValTime;

            /* No need to stop timer it will automatically start with the 
             * configured value*/
            return SNMP_SUCCESS;

        }
        u4PresentWaitTime = pPortEntry->u4WaitWhileTime;

        u4ElapsedTime = u4PresentWaitTime - u4RemainingTime;

        if (u4SetValTime > u4PresentWaitTime)
        {
            /* New value is greater than the old value */
            u4StartTmrPeriod = (u4SetValTime - u4PresentWaitTime) +
                u4RemainingTime;
        }
        else
        {
            /* New value is less than the old value */
            if (u4ElapsedTime > u4SetValTime)
            {
                /* Already the time is elapsed, expire immediately */
                u4StartTmrPeriod = 1;
            }
            else
            {
                /* Start the timer for remaining time */
                u4StartTmrPeriod = u4SetValTime - u4ElapsedTime;
            }
        }
    }
    if (LaStopTimer (&pPortEntry->WaitWhileTmr) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: Unable to STOP the RUNNING WaitWhile Timer\n");
        return SNMP_FAILURE;
    }

    /* Note: Here the osix timer is started directly with the
     * calculated duration. LaStartTimer() cannot be used here.
     */
    if (LA_START_TMR (LA_TIMER_LIST_ID,
                      &pPortEntry->WaitWhileTmr.LaAppTimer,
                      u4StartTmrPeriod) != LA_TMR_SUCCESS)
    {
        LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "PRP-LOW: Starting WaitWhile Timer FAILED\n");
        return SNMP_FAILURE;
    }
    pPortEntry->u4WaitWhileTime = u4SetValTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaPortPartnerResetAdminState
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                setValFsLaPortPartnerResetAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortPartnerResetAdminState (INT4 i4FsLaPortIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsLaPortPartnerResetAdminState)
{
    UINT1               u1SetPortState;

    u1SetPortState = pSetValFsLaPortPartnerResetAdminState->pu1_OctetList[0];
    if ((u1SetPortState & LA_LACPACTIVITY_BITMASK))
    {
        LaChangePartnerPortState ((UINT2) i4FsLaPortIndex,
                                  LA_LACP_ACTIVITY, LA_RESET);
    }

    if ((u1SetPortState & LA_LACPTIMEOUT_BITMASK))
    {
        LaChangePartnerPortState ((UINT2) i4FsLaPortIndex,
                                  LA_LACP_TIMEOUT, LA_RESET);
    }

    if ((u1SetPortState & LA_AGGREGATION_BITMASK))
    {
        LaChangePartnerPortState ((UINT2) i4FsLaPortIndex,
                                  LA_AGGREGATION, LA_RESET);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortActorAdminPort
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                setValFsLaPortActorAdminPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortActorAdminPort (INT4 i4FsLaPortIndex,
                              INT4 i4SetValFsLaPortActorAdminPort)
{
    if (LaChangePortActorAdminPort ((UINT2) i4FsLaPortIndex,
                                    (UINT2) i4SetValFsLaPortActorAdminPort)
        != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFsLaPortRestoreMtu
 *  Input       :  The Indices   
 *                 FsLaPortIndex
 *                 The Object 
 *                 setValFsLaPortRestoreMtu
i*  Output      :  The Set Low Lev Routine Take the Indices &                    *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ***************************************************************************/
INT1
nmhSetFsLaPortRestoreMtu (INT4 i4FsLaPortIndex, INT4 i4SetValFsLaPortRestoreMtu)
{

#ifdef MSR_WANTED
    /* RestoreMtu is allowed to set only by msr for restoring 
     * the Mtu of the port once the port moves out of the bundle.
     */
    UINT4               u4CurrTaskId;
    CONST UINT1        *pTaskName;

    u4CurrTaskId = OsixGetCurTaskId ();
    pTaskName = OsixExGetTaskName (u4CurrTaskId);

    if (STRCMP (pTaskName, "MSR") == 0)
    {
        tLaLacPortEntry    *pPortEntry = NULL;

        /* Check if an entry for port index exists */
        LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
            return SNMP_FAILURE;
        }
        pPortEntry->u4RestoreMtu = i4SetValFsLaPortRestoreMtu;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4FsLaPortIndex);
    UNUSED_PARAM (i4SetValFsLaPortRestoreMtu);
    return SNMP_FAILURE;
#endif

}

/****************************************************************************
 Function    :  nmhSetFsLaPortDefaultedStateThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                setValFsLaPortDefaultedStateThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortDefaultedStateThreshold (INT4 i4FsLaPortIndex,
                                       UINT4
                                       u4SetValFsLaPortDefaultedStateThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        return SNMP_FAILURE;
    }
    pPortEntry->u4DefaultedStateThreshold =
        u4SetValFsLaPortDefaultedStateThreshold;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsLaPortHardwareFailureRecThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                setValFsLaPortHardwareFailureRecThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortHardwareFailureRecThreshold (INT4 i4FsLaPortIndex,
                                           UINT4
                                           u4SetValFsLaPortHardwareFailureRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        return SNMP_FAILURE;
    }
    pPortEntry->u4HardwareFailureRecThreshold =
        u4SetValFsLaPortHardwareFailureRecThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsLaPortSameStateRecThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                setValFsLaPortSameStateRecThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLaPortSameStateRecThreshold (INT4 i4FsLaPortIndex,
                                     UINT4
                                     u4SetValFsLaPortSameStateRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        return SNMP_FAILURE;
    }
    pPortEntry->u4SameStateRecThreshold = u4SetValFsLaPortSameStateRecThreshold;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsLaPortMode
Input       :  The Indices
FsLaPortIndex

The Object 
testValFsLaPortMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaPortMode (UINT4 *pu4ErrorCode, INT4 i4FsLaPortIndex,
                       INT4 i4TestValFsLaPortMode)
{
    UINT2               u2PnacAuthStatus = PNAC_PORTCNTRL_FORCEAUTHORIZED;
    UINT1               u1TunnelStatus = 0;
    UINT1               u1Status = 0;
    UINT1               u1PortType = 0;

/* Check whether the port is configured as promiscous port or host port*/

    L2IwfGetVlanPortType ((UINT4) i4FsLaPortIndex, &u1PortType);
    if ((u1PortType == VLAN_HOST_PORT) || (u1PortType == VLAN_PROMISCOUS_PORT))
    {
        CLI_SET_ERR (CLI_LA_PORT_MODE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (LaL2IwfIsLaConfigAllowed ((UINT4) i4FsLaPortIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_MIRR_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsLaPortMode != LA_MODE_LACP &&
        i4TestValFsLaPortMode != LA_MODE_DISABLED &&
        i4TestValFsLaPortMode != LA_MODE_MANUAL)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsLaPortMode == LA_MODE_LACP) ||
        (i4TestValFsLaPortMode == LA_MODE_MANUAL))
    {

        /*Port cannot be member of a port channel, if it shared with
         * other contexts*/

        LaL2IwfGetSispPortCtrlStatus ((UINT4) i4FsLaPortIndex, &u1Status);

        if (u1Status == L2IWF_ENABLED)
        {
            CLI_SET_ERR (CLI_LA_SISP_CANNOT_BE_PO_MEMBER);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* In case of 802.1ad bridges(provider-core/provider-edge),
         * Enabling the protocol status on a port is not allowed
         * when the protocol tunnel status is set to Tunnel/Discard 
         * on a port.
         */
        LaL2IwfGetProtocolTunnelStatusOnPort ((UINT2) i4FsLaPortIndex,
                                              L2_PROTO_LACP, &u1TunnelStatus);
        if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            || (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (LaDsGetStartedStatus () == DS_TRUE)
        {
            if (LaDsCheckDiffservPort (i4FsLaPortIndex) != DS_SUCCESS)
            {
                /* Diffsrv Port should not be Lacp enabled */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (LaPnacGetStartedStatus () == PNAC_TRUE)
        {
            LaL2IwfGetPortPnacAuthControl ((UINT2) i4FsLaPortIndex,
                                           &u2PnacAuthStatus);

            if (u2PnacAuthStatus != PNAC_PORTCNTRL_FORCEAUTHORIZED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

    }
    /*check whether the port
     *is the last downlink in the UFD group*/
    if (LaCfaUfdIsLastDownlinkInGroup ((UINT4) i4FsLaPortIndex) != OSIX_FALSE)
    {
        CLI_SET_ERR (CLI_LA_PORT_LAST_DOWNLINK_GROUP);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsLaPortActorResetAdminState
Input       :  The Indices
FsLaPortIndex

The Object 
testValFsLaPortActorResetAdminState
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaPortActorResetAdminState (UINT4 *pu4ErrorCode,
                                       INT4 i4FsLaPortIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsLaPortActorResetAdminState)
{
    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pTestValFsLaPortActorResetAdminState->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValFsLaPortActorResetAdminState->pu1_OctetList[0] & 0x80) ||
        (pTestValFsLaPortActorResetAdminState->pu1_OctetList[0] & 0x40) ||
        (pTestValFsLaPortActorResetAdminState->pu1_OctetList[0] & 0x20))
    {

        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsLaPortAggregateWaitTime
Input       :  The Indices
FsLaPortIndex

The Object 
testValFsLaPortAggregateWaitTime
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsLaPortAggregateWaitTime (UINT4 *pu4ErrorCode, INT4 i4FsLaPortIndex,
                                    UINT4 u4TestValFsLaPortAggregateWaitTime)
{
    UINT4               u4TestVal = 0;

    u4TestVal = u4TestValFsLaPortAggregateWaitTime / 100;

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) == LA_SUCCESS)
    {
        if (u4TestVal <= LA_AGG_WAIT_TIME)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortPartnerResetAdminState
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                testValFsLaPortPartnerResetAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortPartnerResetAdminState (UINT4 *pu4ErrorCode,
                                         INT4 i4FsLaPortIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsLaPortPartnerResetAdminState)
{
    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pTestValFsLaPortPartnerResetAdminState->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValFsLaPortPartnerResetAdminState->pu1_OctetList[0] & 0x80) ||
        (pTestValFsLaPortPartnerResetAdminState->pu1_OctetList[0] & 0x40) ||
        (pTestValFsLaPortPartnerResetAdminState->pu1_OctetList[0] & 0x20))
    {

        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortActorAdminPort
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                testValFsLaPortActorAdminPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortActorAdminPort (UINT4 *pu4ErrorCode, INT4 i4FsLaPortIndex,
                                 INT4 i4TestValFsLaPortActorAdminPort)
{
    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLaPortActorAdminPort <= 0)
        || (i4TestValFsLaPortActorAdminPort > LA_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortRestoreMtu
 Input       :  The Indices
                FsLaPortIndex

                The Object 
                testValFsLaPortRestoreMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortRestoreMtu (UINT4 *pu4ErrorCode, INT4 i4FsLaPortIndex,
                             INT4 i4TestValFsLaPortRestoreMtu)
{

    tLaLacPortEntry    *pPortEntry = NULL;
    UNUSED_PARAM (i4TestValFsLaPortRestoreMtu);

    /* Check if an entry for port index exists */
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortDefaultedStateThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                testValFsLaPortDefaultedStateThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortDefaultedStateThreshold (UINT4 *pu4ErrorCode,
                                          INT4 i4FsLaPortIndex,
                                          UINT4
                                          u4TestValFsLaPortDefaultedStateThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    /* Check if an entry for port index exists */
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPortEntry->u4DefaultedStateThreshold ==
        u4TestValFsLaPortDefaultedStateThreshold)
    {
        return SNMP_SUCCESS;
    }
    if (u4TestValFsLaPortDefaultedStateThreshold > LA_MAX_DEF_STATE_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortHardwareFailureRecThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                testValFsLaPortHardwareFailureRecThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortHardwareFailureRecThreshold (UINT4 *pu4ErrorCode,
                                              INT4 i4FsLaPortIndex,
                                              UINT4
                                              u4TestValFsLaPortHardwareFailureRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    /* Check if an entry for port index exists */
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPortEntry->u4HardwareFailureRecThreshold ==
        u4TestValFsLaPortHardwareFailureRecThreshold)
    {
        return SNMP_SUCCESS;
    }
    if (u4TestValFsLaPortHardwareFailureRecThreshold > LA_MAX_REC_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsLaPortSameStateRecThreshold
 Input       :  The Indices
                FsLaPortIndex

                The Object
                testValFsLaPortSameStateRecThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaPortSameStateRecThreshold (UINT4 *pu4ErrorCode,
                                        INT4 i4FsLaPortIndex,
                                        UINT4
                                        u4TestValFsLaPortSameStateRecThreshold)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    /* Check if an entry for port index exists */
    LaGetPortEntry ((UINT2) i4FsLaPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Port Entry does not exist\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LaSnmpLowValidatePortIndex (i4FsLaPortIndex) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPortEntry->u4SameStateRecThreshold ==
        u4TestValFsLaPortSameStateRecThreshold)
    {
        return SNMP_SUCCESS;
    }
    if (u4TestValFsLaPortSameStateRecThreshold > LA_MAX_REC_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLaPortTable
 Input       :  The Indices
                FsLaPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLaPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLaHwFailTrapObjectsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLaHwFailTrapObjectsTable
 Input       :  The Indices
                FsLaTrapPortChannelIndex
                FsLaTrapPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaHwFailTrapObjectsTable (INT4
                                                    i4FsLaTrapPortChannelIndex,
                                                    INT4 i4FsLaTrapPortIndex)
{
    UNUSED_PARAM (i4FsLaTrapPortChannelIndex);
    UNUSED_PARAM (i4FsLaTrapPortIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLaHwFailTrapObjectsTable
 Input       :  The Indices
                FsLaTrapPortChannelIndex
                FsLaTrapPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaHwFailTrapObjectsTable (INT4 *pi4FsLaTrapPortChannelIndex,
                                            INT4 *pi4FsLaTrapPortIndex)
{
    UNUSED_PARAM (pi4FsLaTrapPortChannelIndex);
    UNUSED_PARAM (pi4FsLaTrapPortIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLaHwFailTrapObjectsTable
 Input       :  The Indices
                FsLaTrapPortChannelIndex
                nextFsLaTrapPortChannelIndex
                FsLaTrapPortIndex
                nextFsLaTrapPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaHwFailTrapObjectsTable (INT4 i4FsLaTrapPortChannelIndex,
                                           INT4
                                           *pi4NextFsLaTrapPortChannelIndex,
                                           INT4 i4FsLaTrapPortIndex,
                                           INT4 *pi4NextFsLaTrapPortIndex)
{
    UNUSED_PARAM (i4FsLaTrapPortChannelIndex);
    UNUSED_PARAM (pi4NextFsLaTrapPortChannelIndex);
    UNUSED_PARAM (i4FsLaTrapPortIndex);
    UNUSED_PARAM (pi4NextFsLaTrapPortIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLaHwFailTrapType
 Input       :  The Indices
                FsLaTrapPortChannelIndex
                FsLaTrapPortIndex

                The Object 
                retValFsLaHwFailTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaHwFailTrapType (INT4 i4FsLaTrapPortChannelIndex,
                          INT4 i4FsLaTrapPortIndex,
                          INT4 *pi4RetValFsLaHwFailTrapType)
{
    UNUSED_PARAM (i4FsLaTrapPortChannelIndex);
    UNUSED_PARAM (i4FsLaTrapPortIndex);
    UNUSED_PARAM (pi4RetValFsLaHwFailTrapType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  LaNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
LaNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsla, (sizeof (fsla) / sizeof (UINT4)), au1ObjectOid[0]);
    SNMPGetOidString (stdla, (sizeof (stdla) / sizeof (UINT4)),
                      au1ObjectOid[1]);
    SNMPGetOidString (FsLaSystemControl,
                      (sizeof (FsLaSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[2]);

    /* Send a notification to MSR to process the LA shutdown, with 
     * LA oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 3, MSR_INVALID_CNTXT);
#endif
    return;
}

/* LOW LEVEL Routines for Table : FsLaDLAGTrapObjectsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLaDLAGTrapObjectsTable
 Input       :  The Indices
                FsLaDLAGTrapPortChannelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaDLAGTrapObjectsTable (INT4
                                                  i4FsLaDLAGTrapPortChannelIndex)
{
    UNUSED_PARAM (i4FsLaDLAGTrapPortChannelIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLaDLAGTrapObjectsTable
 Input       :  The Indices
                FsLaDLAGTrapPortChannelIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaDLAGTrapObjectsTable (INT4 *pi4FsLaDLAGTrapPortChannelIndex)
{
    UNUSED_PARAM (pi4FsLaDLAGTrapPortChannelIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLaDLAGTrapObjectsTable
 Input       :  The Indices
                FsLaDLAGTrapPortChannelIndex
                nextFsLaDLAGTrapPortChannelIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaDLAGTrapObjectsTable (INT4 i4FsLaDLAGTrapPortChannelIndex,
                                         INT4
                                         *pi4NextFsLaDLAGTrapPortChannelIndex)
{
    UNUSED_PARAM (i4FsLaDLAGTrapPortChannelIndex);
    UNUSED_PARAM (pi4NextFsLaDLAGTrapPortChannelIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLaDLAGTrapType
 Input       :  The Indices
                FsLaDLAGTrapPortChannelIndex

                The Object 
                retValFsLaDLAGTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGTrapType (INT4 i4FsLaDLAGTrapPortChannelIndex,
                        INT4 *pi4RetValFsLaDLAGTrapType)
{
    UNUSED_PARAM (i4FsLaDLAGTrapPortChannelIndex);
    UNUSED_PARAM (pi4RetValFsLaDLAGTrapType);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLaDLAGRemotePortChannelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLaDLAGRemotePortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaDLAGRemotePortChannelTable (INT4
                                                        i4FsLaPortChannelIfIndex,
                                                        tMacAddr
                                                        FsLaDLAGRemotePortChannelSystemID)
{
    if (LaDLAGSnmpLowValidateRemotePortChannelIndex (i4FsLaPortChannelIfIndex,
                                                     FsLaDLAGRemotePortChannelSystemID)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLaDLAGRemotePortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsLaDLAGRemotePortChannelTable (INT4 *pi4FsLaPortChannelIfIndex,
                                                tMacAddr *
                                                pFsLaDLAGRemotePortChannelSystemID)
{

    if ((LaDLAGSnmpLowGetFirstValidRemotePortChannelIndex
         (pi4FsLaPortChannelIfIndex,
          pFsLaDLAGRemotePortChannelSystemID)) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLaDLAGRemotePortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                nextFsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                nextFsLaDLAGRemotePortChannelSystemID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaDLAGRemotePortChannelTable (INT4 i4FsLaPortChannelIfIndex,
                                               INT4
                                               *pi4NextFsLaPortChannelIfIndex,
                                               tMacAddr
                                               FsLaDLAGRemotePortChannelSystemID,
                                               tMacAddr *
                                               pNextFsLaDLAGRemotePortChannelSystemID)
{
    if (LaDLAGSnmpLowGetNextValidRemotePortChannelIndex
        (i4FsLaPortChannelIfIndex, pi4NextFsLaPortChannelIfIndex,
         FsLaDLAGRemotePortChannelSystemID,
         pNextFsLaDLAGRemotePortChannelSystemID) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortChannelSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID

                The Object 
                retValFsLaDLAGRemotePortChannelSystemPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortChannelSystemPriority (INT4 i4FsLaPortChannelIfIndex,
                                               tMacAddr
                                               FsLaDLAGRemotePortChannelSystemID,
                                               INT4
                                               *pi4RetValFsLaDLAGRemotePortChannelSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {

        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            *pi4RetValFsLaDLAGRemotePortChannelSystemPriority =
                (UINT2) pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortChannelRolePlayed
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID

                The Object 
                retValFsLaDLAGRemotePortChannelRolePlayed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortChannelRolePlayed (INT4 i4FsLaPortChannelIfIndex,
                                           tMacAddr
                                           FsLaDLAGRemotePortChannelSystemID,
                                           INT4
                                           *pi4RetValFsLaDLAGRemotePortChannelRolePlayed)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {

        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            *pi4RetValFsLaDLAGRemotePortChannelRolePlayed =
                (UINT4) pRemoteAggEntry->u1DLAGRolePlayed;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortChannelRolePlayed
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID

                The Object
                retValFsLaMCLAGRemotePortChannelRolePlayed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortChannelRolePlayed (INT4 i4FsLaPortChannelIfIndex,
                                            tMacAddr
                                            FsLaMCLAGRemotePortChannelSystemID,
                                            INT4
                                            *pi4RetValFsLaMCLAGRemotePortChannelRolePlayed)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);

    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaMCLAGRemotePortChannelRolePlayed =
        (UINT4) pRemoteAggEntry->u1DLAGRolePlayed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortChannelKeepAliveCount
 Input       :  The Indices
                FsLaPortChannelIfIndex                                                                                                                                                  FsLaDLAGRemotePortChannelSystemID

                The Object
                retValFsLaDLAGRemotePortChannelKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortChannelKeepAliveCount (INT4 i4FsLaPortChannelIfIndex,
                                               tMacAddr
                                               FsLaDLAGRemotePortChannelSystemID,
                                               INT4
                                               *pi4RetValFsLaDLAGRemotePortChannelKeepAliveCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            *pi4RetValFsLaDLAGRemotePortChannelKeepAliveCount =
                (INT4) pRemoteAggEntry->u4DLAGKeepAliveCount;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortChannelSpeed
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID

                The Object
                retValFsLaDLAGRemotePortChannelSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortChannelSpeed (INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr
                                      FsLaDLAGRemotePortChannelSystemID,
                                      UINT4
                                      *pu4RetValFsLaDLAGRemotePortChannelSpeed)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            *pu4RetValFsLaDLAGRemotePortChannelSpeed =
                pRemoteAggEntry->u4PortSpeed;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortChannelHighSpeed
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID

                The Object
                retValFsLaDLAGRemotePortChannelHighSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortChannelHighSpeed (INT4 i4FsLaPortChannelIfIndex,
                                          tMacAddr
                                          FsLaDLAGRemotePortChannelSystemID,
                                          UINT4
                                          *pu4RetValFsLaDLAGRemotePortChannelHighSpeed)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            *pu4RetValFsLaDLAGRemotePortChannelHighSpeed =
                pRemoteAggEntry->u4PortHighSpeed;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortChannelMtu
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID

                The Object
                retValFsLaDLAGRemotePortChannelMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortChannelMtu (INT4 i4FsLaPortChannelIfIndex,
                                    tMacAddr FsLaDLAGRemotePortChannelSystemID,
                                    INT4 *pi4RetValFsLaDLAGRemotePortChannelMtu)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            *pi4RetValFsLaDLAGRemotePortChannelMtu =
                (INT4) pRemoteAggEntry->u4Mtu;
        }
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsLaDLAGRemotePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLaDLAGRemotePortTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                FsLaDLAGRemotePortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaDLAGRemotePortTable (INT4 i4FsLaPortChannelIfIndex,
                                                 tMacAddr
                                                 FsLaDLAGRemotePortChannelSystemID,
                                                 INT4 i4FsLaDLAGRemotePortIndex)
{
    if (LaDLAGSnmpLowValidateRemotePortIndex (i4FsLaPortChannelIfIndex,
                                              FsLaDLAGRemotePortChannelSystemID,
                                              i4FsLaDLAGRemotePortIndex) ==
        LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLaDLAGRemotePortTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                FsLaDLAGRemotePortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaDLAGRemotePortTable (INT4 *pi4FsLaPortChannelIfIndex,
                                         tMacAddr *
                                         pFsLaDLAGRemotePortChannelSystemID,
                                         INT4 *pi4FsLaDLAGRemotePortIndex)
{

    if (LaDLAGSnmpLowGetFirstValidRemotePortIndex (pi4FsLaPortChannelIfIndex,
                                                   pFsLaDLAGRemotePortChannelSystemID,
                                                   pi4FsLaDLAGRemotePortIndex)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLaDLAGRemotePortTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                nextFsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                nextFsLaDLAGRemotePortChannelSystemID
                FsLaDLAGRemotePortIndex
                nextFsLaDLAGRemotePortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaDLAGRemotePortTable (INT4 i4FsLaPortChannelIfIndex,
                                        INT4 *pi4NextFsLaPortChannelIfIndex,
                                        tMacAddr
                                        FsLaDLAGRemotePortChannelSystemID,
                                        tMacAddr *
                                        pNextFsLaDLAGRemotePortChannelSystemID,
                                        INT4 i4FsLaDLAGRemotePortIndex,
                                        INT4 *pi4NextFsLaDLAGRemotePortIndex)
{
    if (LaDLAGSnmpLowGetNextValidRemotePortIndex (i4FsLaPortChannelIfIndex,
                                                  pi4NextFsLaPortChannelIfIndex,
                                                  FsLaDLAGRemotePortChannelSystemID,
                                                  pNextFsLaDLAGRemotePortChannelSystemID,
                                                  i4FsLaDLAGRemotePortIndex,
                                                  pi4NextFsLaDLAGRemotePortIndex)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortBundleState
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                FsLaDLAGRemotePortIndex

                The Object
                retValFsLaDLAGRemotePortBundleState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortBundleState (INT4 i4FsLaPortChannelIfIndex,
                                     tMacAddr FsLaDLAGRemotePortChannelSystemID,
                                     INT4 i4FsLaDLAGRemotePortIndex,
                                     INT4
                                     *pi4RetValFsLaDLAGRemotePortBundleState)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            LaDLAGGetRemotePortEntry ((UINT4) i4FsLaDLAGRemotePortIndex,
                                      pRemoteAggEntry, &pRemotePortEntry);

            if (pRemotePortEntry != NULL)
            {
                *pi4RetValFsLaDLAGRemotePortBundleState =
                    (UINT2) pRemotePortEntry->u1BundleState;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortSyncStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                FsLaDLAGRemotePortIndex

                The Object 
                retValFsLaDLAGRemotePortSyncStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortSyncStatus (INT4 i4FsLaPortChannelIfIndex,
                                    tMacAddr FsLaDLAGRemotePortChannelSystemID,
                                    INT4 i4FsLaDLAGRemotePortIndex,
                                    INT4 *pi4RetValFsLaDLAGRemotePortSyncStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            LaDLAGGetRemotePortEntry ((UINT4) i4FsLaDLAGRemotePortIndex,
                                      pRemoteAggEntry, &pRemotePortEntry);

            if (pRemotePortEntry != NULL)
            {
                *pi4RetValFsLaDLAGRemotePortSyncStatus =
                    (UINT2) pRemotePortEntry->u1SyncState;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaDLAGRemotePortPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaDLAGRemotePortChannelSystemID
                FsLaDLAGRemotePortIndex

                The Object
                retValFsLaDLAGRemotePortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaDLAGRemotePortPriority (INT4 i4FsLaPortChannelIfIndex,
                                  tMacAddr FsLaDLAGRemotePortChannelSystemID,
                                  INT4 i4FsLaDLAGRemotePortIndex,
                                  INT4 *pi4RetValFsLaDLAGRemotePortPriority)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);
        if (pRemoteAggEntry != NULL)
        {
            LaDLAGGetRemotePortEntry ((UINT4) i4FsLaDLAGRemotePortIndex,
                                      pRemoteAggEntry, &pRemotePortEntry);

            if (pRemotePortEntry != NULL)
            {
                *pi4RetValFsLaDLAGRemotePortPriority =
                    (UINT2) pRemotePortEntry->u2Priority;
            }
        }
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsLaMCLAGRemotePortChannelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLaMCLAGRemotePortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaMCLAGRemotePortChannelTable (INT4
                                                         i4FsLaPortChannelIfIndex,
                                                         tMacAddr
                                                         FsLaMCLAGRemotePortChannelSystemID)
{
    if (LaDLAGSnmpLowValidateRemotePortChannelIndex (i4FsLaPortChannelIfIndex,
                                                     FsLaMCLAGRemotePortChannelSystemID)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLaMCLAGRemotePortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaMCLAGRemotePortChannelTable (INT4
                                                 *pi4FsLaPortChannelIfIndex,
                                                 tMacAddr *
                                                 pFsLaMCLAGRemotePortChannelSystemID)
{
#ifdef ICCH_WANTED
    if ((LaMCLAGSnmpLowGetFirstValidRemotePortChannelIndex
         (pi4FsLaPortChannelIfIndex,
          pFsLaMCLAGRemotePortChannelSystemID)) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (pi4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pFsLaMCLAGRemotePortChannelSystemID);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLaMCLAGRemotePortChannelTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                nextFsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                nextFsLaMCLAGRemotePortChannelSystemID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaMCLAGRemotePortChannelTable (INT4 i4FsLaPortChannelIfIndex,
                                                INT4
                                                *pi4NextFsLaPortChannelIfIndex,
                                                tMacAddr
                                                FsLaMCLAGRemotePortChannelSystemID,
                                                tMacAddr *
                                                pNextFsLaMCLAGRemotePortChannelSystemID)
{
#ifdef ICCH_WANTED
    if (LaMCLAGSnmpLowGetNextValidRemotePortChannelIndex
        (i4FsLaPortChannelIfIndex, pi4NextFsLaPortChannelIfIndex,
         FsLaMCLAGRemotePortChannelSystemID,
         pNextFsLaMCLAGRemotePortChannelSystemID) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pi4NextFsLaPortChannelIfIndex);
    UNUSED_PARAM (FsLaMCLAGRemotePortChannelSystemID);
    UNUSED_PARAM (pNextFsLaMCLAGRemotePortChannelSystemID);
#endif
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortChannelSystemPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID

                The Object 
                retValFsLaMCLAGRemotePortChannelSystemPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortChannelSystemPriority (INT4 i4FsLaPortChannelIfIndex,
                                                tMacAddr
                                                FsLaMCLAGRemotePortChannelSystemID,
                                                INT4
                                                *pi4RetValFsLaMCLAGRemotePortChannelSystemPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);

    if (pAggEntry != NULL)
    {
        LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                           pAggEntry, &pRemoteAggEntry);

        if (pRemoteAggEntry != NULL)
        {
            *pi4RetValFsLaMCLAGRemotePortChannelSystemPriority =
                (UINT2) pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortChannelKeepAliveCount
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID

                The Object 
                retValFsLaMCLAGRemotePortChannelKeepAliveCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortChannelKeepAliveCount (INT4 i4FsLaPortChannelIfIndex,
                                                tMacAddr
                                                FsLaMCLAGRemotePortChannelSystemID,
                                                INT4
                                                *pi4RetValFsLaMCLAGRemotePortChannelKeepAliveCount)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaMCLAGRemotePortChannelKeepAliveCount =
        (INT4) pRemoteAggEntry->u4DLAGKeepAliveCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortChannelSpeed
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID

                The Object 
                retValFsLaMCLAGRemotePortChannelSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortChannelSpeed (INT4 i4FsLaPortChannelIfIndex,
                                       tMacAddr
                                       FsLaMCLAGRemotePortChannelSystemID,
                                       UINT4
                                       *pu4RetValFsLaMCLAGRemotePortChannelSpeed)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLaMCLAGRemotePortChannelSpeed = pRemoteAggEntry->u4PortSpeed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortChannelHighSpeed
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID

                The Object 
                retValFsLaMCLAGRemotePortChannelHighSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortChannelHighSpeed (INT4 i4FsLaPortChannelIfIndex,
                                           tMacAddr
                                           FsLaMCLAGRemotePortChannelSystemID,
                                           UINT4
                                           *pu4RetValFsLaMCLAGRemotePortChannelHighSpeed)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return FAILURE;
    }
    *pu4RetValFsLaMCLAGRemotePortChannelHighSpeed =
        pRemoteAggEntry->u4PortHighSpeed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortChannelMtu
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID

                The Object 
                retValFsLaMCLAGRemotePortChannelMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortChannelMtu (INT4 i4FsLaPortChannelIfIndex,
                                     tMacAddr
                                     FsLaMCLAGRemotePortChannelSystemID,
                                     INT4
                                     *pi4RetValFsLaMCLAGRemotePortChannelMtu)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaMCLAGRemotePortChannelMtu = (INT4) pRemoteAggEntry->u4Mtu;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsLaMCLAGRemotePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLaMCLAGRemotePortTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLaMCLAGRemotePortTable (INT4 i4FsLaPortChannelIfIndex,
                                                  tMacAddr
                                                  FsLaMCLAGRemotePortChannelSystemID,
                                                  INT4
                                                  i4FsLaMCLAGRemotePortIndex)
{
    if (LaDLAGSnmpLowValidateRemotePortIndex (i4FsLaPortChannelIfIndex,
                                              FsLaMCLAGRemotePortChannelSystemID,
                                              i4FsLaMCLAGRemotePortIndex) ==
        LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLaMCLAGRemotePortTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLaMCLAGRemotePortTable (INT4 *pi4FsLaPortChannelIfIndex,
                                          tMacAddr *
                                          pFsLaMCLAGRemotePortChannelSystemID,
                                          INT4 *pi4FsLaMCLAGRemotePortIndex)
{
#ifdef ICCH_WANTED
    if (LaMCLAGSnmpLowGetFirstValidRemotePortIndex (pi4FsLaPortChannelIfIndex,
                                                    pFsLaMCLAGRemotePortChannelSystemID,
                                                    pi4FsLaMCLAGRemotePortIndex)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (pi4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pFsLaMCLAGRemotePortChannelSystemID);
    UNUSED_PARAM (pi4FsLaMCLAGRemotePortIndex);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLaMCLAGRemotePortTable
 Input       :  The Indices
                FsLaPortChannelIfIndex
                nextFsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                nextFsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex
                nextFsLaMCLAGRemotePortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsLaMCLAGRemotePortTable (INT4 i4FsLaPortChannelIfIndex,
                                         INT4 *pi4NextFsLaPortChannelIfIndex,
                                         tMacAddr
                                         FsLaMCLAGRemotePortChannelSystemID,
                                         tMacAddr *
                                         pNextFsLaMCLAGRemotePortChannelSystemID,
                                         INT4 i4FsLaMCLAGRemotePortIndex,
                                         INT4 *pi4NextFsLaMCLAGRemotePortIndex)
{
#ifdef ICCH_WANTED
    if (LaMCLAGSnmpLowGetNextValidRemotePortIndex (i4FsLaPortChannelIfIndex,
                                                   pi4NextFsLaPortChannelIfIndex,
                                                   FsLaMCLAGRemotePortChannelSystemID,
                                                   pNextFsLaMCLAGRemotePortChannelSystemID,
                                                   i4FsLaMCLAGRemotePortIndex,
                                                   pi4NextFsLaMCLAGRemotePortIndex)
        == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pi4NextFsLaPortChannelIfIndex);
    UNUSED_PARAM (FsLaMCLAGRemotePortChannelSystemID);
    UNUSED_PARAM (pNextFsLaMCLAGRemotePortChannelSystemID);
    UNUSED_PARAM (i4FsLaMCLAGRemotePortIndex);
    UNUSED_PARAM (pi4NextFsLaMCLAGRemotePortIndex);
#endif
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortBundleState
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex

                The Object 
                retValFsLaMCLAGRemotePortBundleState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortBundleState (INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr
                                      FsLaMCLAGRemotePortChannelSystemID,
                                      INT4 i4FsLaMCLAGRemotePortIndex,
                                      INT4
                                      *pi4RetValFsLaMCLAGRemotePortBundleState)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);

    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemotePortEntry ((UINT4) i4FsLaMCLAGRemotePortIndex,
                              pRemoteAggEntry, &pRemotePortEntry);

    if (pRemotePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaMCLAGRemotePortBundleState =
        (UINT2) pRemotePortEntry->u1BundleState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortSyncStatus
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex

                The Object 
                retValFsLaMCLAGRemotePortSyncStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortSyncStatus (INT4 i4FsLaPortChannelIfIndex,
                                     tMacAddr
                                     FsLaMCLAGRemotePortChannelSystemID,
                                     INT4 i4FsLaMCLAGRemotePortIndex,
                                     INT4
                                     *pi4RetValFsLaMCLAGRemotePortSyncStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemotePortEntry ((UINT4) i4FsLaMCLAGRemotePortIndex,
                              pRemoteAggEntry, &pRemotePortEntry);

    if (pRemotePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaMCLAGRemotePortSyncStatus =
        (UINT2) pRemotePortEntry->u1SyncState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortPriority
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex

                The Object 
                retValFsLaMCLAGRemotePortPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortPriority (INT4 i4FsLaPortChannelIfIndex,
                                   tMacAddr FsLaMCLAGRemotePortChannelSystemID,
                                   INT4 i4FsLaMCLAGRemotePortIndex,
                                   INT4 *pi4RetValFsLaMCLAGRemotePortPriority)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    LaDLAGGetRemotePortEntry ((UINT4) i4FsLaMCLAGRemotePortIndex,
                              pRemoteAggEntry, &pRemotePortEntry);

    if (pRemotePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsLaMCLAGRemotePortPriority =
        (UINT2) pRemotePortEntry->u2Priority;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsLaMCLAGRemotePortSlotIndex
 Input       :  The Indices
                FsLaPortChannelIfIndex
                FsLaMCLAGRemotePortChannelSystemID
                FsLaMCLAGRemotePortIndex

                The Object
                retValFsLaMCLAGRemotePortSlotIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLaMCLAGRemotePortSlotIndex (INT4 i4FsLaPortChannelIfIndex,
                                    tMacAddr FsLaMCLAGRemotePortChannelSystemID,
                                    INT4 i4FsLaMCLAGRemotePortIndex,
                                    INT4 *pi4RetValFsLaMCLAGRemotePortSlotIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaMCLAGRemotePortChannelSystemID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    LaDLAGGetRemotePortEntry ((UINT4) i4FsLaMCLAGRemotePortIndex,
                              pRemoteAggEntry, &pRemotePortEntry);

    if (pRemotePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLaMCLAGRemotePortSlotIndex =
        (UINT2) pRemotePortEntry->u4SlotIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsLaMCLAGSystemControl
 Input       :  The Indices

                The Object
                testValFsLaMCLAGSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLaMCLAGSystemControl (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsLaMCLAGSystemControl)
{

    if ((i4TestValFsLaMCLAGSystemControl != LA_START) &&
        (i4TestValFsLaMCLAGSystemControl != LA_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifdef ICCH_WANTED
    if (gu1LaMclagShutDownInProgress == OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_LA_MCLAG_SHUT_IN_PROGRESS);
        return SNMP_FAILURE;
    }
    if (LaIcchApiTestICCLParams (i4TestValFsLaMCLAGSystemControl) ==
        OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_LA_ICCL_IN_USE_ERR);
        return SNMP_FAILURE;
    }

#endif
    return SNMP_SUCCESS;
}
