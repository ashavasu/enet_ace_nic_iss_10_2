/*****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: laagg.c,v 1.31 2017/10/16 09:41:00 siva Exp $
* Licensee Aricent Inc., 2001-2002
*****************************************************************************/
/*    FILE  NAME            : laagg.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Aggregator module                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains functions belonging to the  */
/*                            Aggregator module of LA; i.e.Frame Collector,  */
/*                            Frame Distributor, Aggregator Parser and       */
/*                            Aggregator Multiplexer.                        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaHandleOutFrame                                     */
/*                                                                           */
/* Description        : This function receives the outgoing frame from Bridge*/
/*                      Vlan or Stap modules. It forwards the frame to the   */
/*                      Aggregator Mux corresponding to the link chosen by   */
/*                      the Distribution Algorithm                           */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the data buffer to be transmitted  */
/*                      u2AggIfIndex - Index of the Aggregator               */
/*                      pu1DestHwAddr - Pointer to the Destination Hw Address*/
/*                      u4PktSize - Size of the data frame                   */
/*                      u2Protocol - Protocol of the data in the buffer      */
/*                      u1EncapType - Encapsulation type to be used          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaHandleOutFrame (tLaBufChainHeader * pBuf, UINT2 u2AggIndex,
                  UINT1 *pu1DestHwAddr, UINT4 u4PktSize, UINT2 u2Protocol,
                  UINT1 u1EncapType)
{
    tLaPktParams        LaPktParams;
    UINT2               u2SelectedPortIndex;
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry;
    UINT2               u2PortIndex;

    if (pBuf == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                "AGG: Outgoing CRU Buffer - Null Pointer\n");
        return LA_FAILURE;
    }

    LA_MEMSET (&LaPktParams, 0, sizeof (tLaPktParams));

    if (u4PktSize > LA_MAX_ETH_FRAME_SIZE)
    {

        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                "AGG: Invalid length of Ethernet Frame; Discarding frame...\n");
        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "AGG: Outgoing frame CRU Buffer Release FAILED\n");
        }
        return LA_FAILURE;
    }

    LA_PKT_DUMP (DUMP_TRC, pBuf, u4PktSize,
                 "CTRL: Dumping Outgoing frame received from Bridge...\n");

    /* Storing the au1DestHwAddr, u4PktSize, u2Protocol, u1EncapType in a structure
     * and passing the pointer to the structure throughout the LA module as these
     * fields are not needed by the LA module for its processing. */

    LaPktParams.pu1DestHwAddr = pu1DestHwAddr;
    LaPktParams.u4PktSize = u4PktSize;
    LaPktParams.u2Protocol = u2Protocol;
    LaPktParams.u1EncapType = u1EncapType;
    if (u2AggIndex < LA_MIN_AGG_INDEX)
    {
        /* This is a physical port */
        u2PortIndex = u2AggIndex;
        LaGetPortEntry (u2PortIndex, &pPortEntry);

        /* If aggregation is enabled then do not send on the physical port */
        if ((pPortEntry == NULL) ||
            (pPortEntry->LaLacpMode != LA_MODE_DISABLED))
        {
            LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                    "AGG: This port is used for PortChannel - Discarding frame..\n");

            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: CRU Buffer Release FAILED\n");
            }
            return LA_FAILURE;
        }
        LaHandOverOutFrame (pBuf, u2PortIndex, &LaPktParams);
        return LA_SUCCESS;
    }
    else if (u2AggIndex <= LA_MAX_AGG_INDEX)
    {
        /* This is an aggregator index */
        LaGetAggEntry (u2AggIndex, &pAggEntry);
        if ((pAggEntry == NULL) || (pAggEntry->u1AggOperStatus == LA_OPER_DOWN))
        {
            LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                    "AGG: Invalid index / Agg Oper Down; Discarding frame..\n");
            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Invalid Index - frame CRU Buffer Release FAILED !\n");
            }
            return LA_FAILURE;
        }

        if ((u1EncapType == CFA_ENCAP_LLC) && (u2Protocol == CFA_PROT_BPDU))
            /*Don't apply distribution algorithm to STAP pdus. Reason - 
             * Two devices connected back to back are (mis)configured such that
             * between the two devices , one aggregator exists in the first device, 
             * whereas 2 aggregators exist in the second device. In such a situation
             * if distribution algorithm is applied on the aggregator, it might 
             * result in a loop.*/
        {
            LaGetNextAggDistributingPort (u2AggIndex, 0, &u2PortIndex);
            if (u2PortIndex == 0)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: Ports not available to transmit; Discarding frame..\n");
                if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
                {
                    LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                            "AGG: Invalid Index - frame CRU Buffer Release FAILED !\n");
                }
                return LA_FAILURE;
            }
            LaHandOverOutFrame (pBuf, u2PortIndex, &LaPktParams);
            return LA_SUCCESS;
        }
        if (LaAggSelectLink (pBuf, u2AggIndex,
                             &u2SelectedPortIndex) != LA_SUCCESS)
        {
            LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                    "AGG: LaAggSelectLink function FAILED\n");

            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing frame CRU Buffer Release FAILED\n");
            }
            return LA_FAILURE;
        }
        LaGetPortEntry (u2SelectedPortIndex, &pPortEntry);

        if (LaAggMultiplexFrame (pBuf, pPortEntry, &LaPktParams, LA_TRUE)
            != LA_SUCCESS)
        {
            LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                    "AGG: LaAggMultiplexFrame FAILED\n");

            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing frame CRU Buffer Release FAILED\n");
            }

            return LA_FAILURE;
        }

    }
    else
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                     "AGG: Agg Index %d - value out of bounds; Discarding frame...\n",
                     u2AggIndex);

        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "AGG: Outgoing frame CRU Buffer Release FAILED\n");
        }
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggParseFrame                                      */
/*                                                                           */
/* Description        : This function receives the incoming frame from the   */
/*                      Control Parser. This function is called for every    */
/*                      PDUs and other unknown and illegal subtypes)         */
/*                      Depending on the type of Marker PDU, it passes the   */
/*                      frame to either the Marker Responder or the Marker   */
/*                      Receiver. It returns LA_UNKNOWN for all the frames   */
/*                      of Slow Protocol type but of unknown Subtype.        */
/*                                                                           */
/* Input(s)           : pu1LinearBuf - Pointer to data in the linear buffer  */
/*                      pPortEntry - Pointer to the port entry          */
/*                      pLaLacAggIfEntry - Pointer to the Aggregator If entry*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE / LA_UNKNOWN                 */
/*****************************************************************************/

INT4
LaAggParseFrame (UINT1 *pu1LinearBuf, tLaLacPortEntry * pPortEntry,
                 tLaLacAggIfEntry * pLaLacAggIfEntry)
{
    UINT1               u1Subtype;
    UINT1              *pu1MarkerPdu = NULL;

    if (pu1LinearBuf == NULL || pPortEntry == NULL || pLaLacAggIfEntry == NULL)
    {
        LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    /* Storing the starting address of the linear buffer */
    pu1MarkerPdu = pu1LinearBuf;

    /* Checking if it is a valid Subtype */
    pu1LinearBuf += LA_PROTOCOL_SUBTYPE_OFFSET;
    LA_GET_1BYTE (u1Subtype, pu1LinearBuf);

    if (u1Subtype == LA_MARKER_SUBTYPE)
    {
        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            /* pu1MarkerPdu points to the Destination Address (start of frame) */
            if (LaAggProcessMarkerPdu (pu1MarkerPdu, pPortEntry,
                                       pLaLacAggIfEntry) != LA_SUCCESS)
            {
                LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggProcessMarkerPdu function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "AGG: Port: %d :: Marker PDU processed SUCCESSFULLY\n",
                         pPortEntry->u2PortIndex);
        }
        else
        {
            return LA_FAILURE;
        }
    }

    else if ((u1Subtype < LA_MIN_ILLEGAL_SUBTYPE) ||
             (u1Subtype > LA_MAX_ILLEGAL_SUBTYPE))
    {

        /* Incrementing the number of Illegally Received PDUs as Subtype
         * is incorrect */
        (pPortEntry->LaPortStats.u4IllegalRx)++;

        /* Inrement the Aggregator If Statistics */
        /* All the statistics are negatively incremented for the
         * Aggregator, because these frames have already been counted
         * once, at the port level */
        (pLaLacAggIfEntry->i4AggInErrors)--;

        /* Discard frame */

        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "AGG: Port: %d :: Frame is of ILLEGAL SUBTYPE\n",
                     pPortEntry->u2PortIndex);
        return LA_FAILURE;

    }                            /* End of Illegal Slow protocols */
    else
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "AGG: Port: %d :: Frame is of UNKNOWN SUBTYPE\n",
                     pPortEntry->u2PortIndex);
        return LA_UNKNOWN;

    }                            /* End of unsupported Slow protocols */

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaAggProcessMarkerPdu                                */
/*                                                                           */
/* Description        : This function processes all the frames carrying the  */
/*                      Subtype value as Marker Subtype.                     */
/*                                                                           */
/* Input(s)           : pu1MarkerPdu - Pointer to the data in the buffer     */
/*                      pPortEntry - Pointer to the port entry          */
/*                      pLaLacAggIfEntry - Pointer to the Aggregator If entry*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggProcessMarkerPdu (UINT1 *pu1MarkerPdu,
                       tLaLacPortEntry * pPortEntry,
                       tLaLacAggIfEntry * pLaLacAggIfEntry)
{
    UINT1               u1TlvType;
    UINT1              *pu1Buffer;

    if (pu1MarkerPdu == NULL || pPortEntry == NULL || pLaLacAggIfEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    pu1Buffer = pu1MarkerPdu;

    pu1Buffer += LA_MARKER_TLV_OFFSET;
    LA_GET_1BYTE (u1TlvType, pu1Buffer);

    if (u1TlvType == LA_MARKER_INFORMATION)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "AGG: Port: %u :: Marker Request PDU Received\n",
                     pPortEntry->u2PortIndex);

        if (*pu1Buffer != LA_MARKER_INFO_LENGTH)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "AGG: Marker Information Length ERROR\n");
            /* Incrementing the Illegal Pdus Rx and Aggregator If statistics */
            (pPortEntry->LaPortStats.u4IllegalRx)++;

            /* All the statistics are negatively incremented for the
             * Aggregator, because these frames have already been counted
             * once, at the port level */
            (pLaLacAggIfEntry->i4AggInErrors)--;
            return LA_FAILURE;
        }

        /* Incrementing the number of Marker Request PDUs Received */
        (pPortEntry->LaPortStats.u4MarkerPduRx)++;

        /* Incrementing Aggregator If statistics if the port is attached */
        if (pPortEntry->LaAggAttached == LA_TRUE)
        {
            /* All the statistics are negatively incremented for the
             * Aggregator, because these frames have already been counted
             * once, at the port level */
            pLaLacAggIfEntry->i4AggInOctets += -(LA_SLOW_PROTOCOL_SIZE);
            (pLaLacAggIfEntry->i4AggInFrames)--;
            (pLaLacAggIfEntry->i4AggInMcastPkts)--;
        }

        /* Passing the pointer to the Destination Address (i.e. start of
         * frame), in order to pass the entire Reuest PDU */
        if (LaAggRespondToMarkerRequest (pu1MarkerPdu, pPortEntry,
                                         pLaLacAggIfEntry) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "AGG: LaAggRespondToMarkerRequest function FAILED\n");

            return LA_FAILURE;
        }

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "AGG: Port: %u :: Marker Request PDU processed SUCCESSFULLY\n",
                     pPortEntry->u2PortIndex);

    }                            /* End of Marker Request PDU */

    else if (u1TlvType == LA_MARKER_RESPONSE_INFORMATION)
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "AGG: Port: %u :: Marker Response PDU Received\n",
                     pPortEntry->u2PortIndex);

        if (*pu1Buffer != LA_MARKER_RESPONSE_INFO_LENGTH)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "AGG: Marker Response Information Length ERROR\n");
            /* Incrementing the Illegal Pdus Rx and Aggregator If statistics */
            (pPortEntry->LaPortStats.u4IllegalRx)++;

            /* All the statistics are negatively incremented for the
             * Aggregator, because these frames have already been counted
             * once, at the port level */
            (pLaLacAggIfEntry->i4AggInErrors)--;
            return LA_FAILURE;
        }

        /* Incrementing the number of Marker Response PDUs Received */
        (pPortEntry->LaPortStats.u4MarkerResponsePduRx)++;

        /* Incrementing Aggregator If statistics if the port is attached */
        if (pPortEntry->LaAggAttached == LA_TRUE)
        {
            /* All the statistics are negatively incremented for the
             * Aggregator, because these frames have already been counted
             * once, at the port level */
            pLaLacAggIfEntry->i4AggInOctets += -(LA_SLOW_PROTOCOL_SIZE);
            (pLaLacAggIfEntry->i4AggInFrames)--;
            (pLaLacAggIfEntry->i4AggInMcastPkts)--;
        }

        /* Passing the pointer to the Destination Address (i.e. start of
         * frame), in order to pass the entire Reuest PDU */
        if (LaAggReceiveMarkerResponse (pu1MarkerPdu, pPortEntry) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "AGG: LaAggReceiveMarkerResponse function FAILED\n");

            return LA_FAILURE;
        }

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "AGG: Port: %u :: Marker Response PDU processed SUCCESSFULLY\n",
                     pPortEntry->u2PortIndex);

    }                            /* End of Marker Response PDU */
    else
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "AGG: Port: %u :: UNKNOWN TLV_TYPE PDU Received\n",
                     pPortEntry->u2PortIndex);

        /* Increment the number of Illegally Received PDUs as Marker Tlvtype
         * is incorrect */
        (pPortEntry->LaPortStats.u4IllegalRx)++;

        /* Incrementing the Aggregator If statistics */
        /* All the statistics are negatively incremented for the
         * Aggregator, because these frames have already been counted
         * once, at the port level */
        (pLaLacAggIfEntry->i4AggInErrors)--;

        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggPurgeEntry                                      */
/*                                                                           */
/* Description        : This function is called by the LaAggReceiveMarker-   */
/*                      Response function and on Marker Timer Expiry.This    */
/*                      function deletes all the table entries corresponding */
/*                      to the port on which the Marker Response has been    */
/*                      received or for which the Marker Timer expires. It   */
/*                      then signals readiness to LAC module to detach port. */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the Port Entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggPurgeEntry (tLaLacPortEntry * pPortEntry)
{

    UINT4               u4CacheHashIndex;
    tLaCacheEntry      *pLaFirstCacheEntry = NULL;
    tLaCacheEntry      *pLaNextCacheEntry = NULL;

    if (pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    /* Deleting all the Cache entries with this Port Index, after stopping the
     * corresponding Cache Timer */

    LA_HASH_SCAN_TBL (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex)
    {

        pLaFirstCacheEntry = (tLaCacheEntry *) LA_HASH_GET_FIRST_BUCKET_NODE
            (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex);

        while (pLaFirstCacheEntry != NULL)
        {
            pLaNextCacheEntry = (tLaCacheEntry *) LA_HASH_GET_NEXT_BUCKET_NODE
                (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                 &pLaFirstCacheEntry->NextCacheEntry);

            if (pPortEntry->u2PortIndex == pLaFirstCacheEntry->u2PortIndex)
            {

                if (pLaFirstCacheEntry->LaCacheTmr.LaTmrFlag != LA_TMR_STOPPED)
                {
                    if (LaStopTimer (&(pLaFirstCacheEntry->LaCacheTmr))
                        != LA_SUCCESS)
                    {
                        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                                     ALL_FAILURE_TRC,
                                     "AGG: Stopping Cache Timer for Port %d FAILED\n",
                                     pPortEntry->u2PortIndex);
                        return LA_FAILURE;
                    }
                }

                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: Deleting Cache Entry with Port Index %u\n",
                             pLaFirstCacheEntry->u2PortIndex);

                LA_HASH_DEL_NODE (gLaGlobalInfo.pLaCacheTable,
                                  &pLaFirstCacheEntry->NextCacheEntry,
                                  u4CacheHashIndex);

                if (LA_CACHEENTRY_FREE_MEM_BLOCK (pLaFirstCacheEntry)
                    != LA_MEM_SUCCESS)
                {
                    LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "AGG: Cache Entry Memory Block Release FAILED\n");
                    return LA_FAILURE;
                }

            }                    /* End of Node matching Port Index */

            pLaFirstCacheEntry = pLaNextCacheEntry;

        }                        /* End of while */

    }                            /* End of Hash Scan Cache Table */

    /* Deleting the Marker entry corresponding to the port entry on which it is
     * received */
    if (pPortEntry->pLaMarkerEntry != NULL)
    {
        if (LA_MARKERENTRY_FREE_MEM_BLOCK (pPortEntry->pLaMarkerEntry)
            != LA_MEM_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "AGG: Marker Entry Memory Block Release FAILED\n");
            return LA_FAILURE;
        }

        pPortEntry->pLaMarkerEntry = NULL;
    }

    LaLacPortChangeAggregation (pPortEntry);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggSelectLink                                      */
/*                                                                           */
/* Description        : This function receievs the outgoing frame and applies*/
/*                      the distribution algorithm based on the Selection    */
/*                      policy, in order to determine on which outgoing  link*/
/*                      to send the frame on.                                */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the data buffer                    */
/*                      u2AggIndex - The index of the aggregator             */
/*                                                                           */
/* Output(s)          : pu2SelectedPortIndex - The index of the port selected*/
/*                                             by the distribution algorithm */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                      gLaGlobalInfo.LaLinkSelectPolicy                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggSelectLink (tLaBufChainHeader * pBuf, UINT2 u2AggIndex,
                 UINT2 *pu2SelectedPortIndex)
{
    tLaMacAddr          LaSa;
    tLaMacAddr          LaDa;
    UINT2               u2Mask = 0;
    UINT2               u2Result = 0;
    UINT2               u2ResultAddr = 0;
    UINT2               u2ResultVlan = 0;
    UINT4               u4Result = 0;
    UINT2               u2NumOfBits = 0;
    UINT4               u4CacheHashIndex = 0;
    tLaCacheEntry      *pLaCacheEntry = NULL;
    tLaLinkSelectPolicy AggPolicy;
    UINT2               au2ActivePortArray[LA_MAX_PORTS_PER_AGG];
    UINT1               u1PortCount;
    UINT1               u1Threshold;
    UINT2               u2Val = 0;
    UINT2               u2ValAddr = 0;
    UINT2               u2ValVlan = 0;
    UINT4               u4Val = 0;
    UINT2               u2DestVal = 0;
    tVlanId             LaVlan = 0;
    UINT4               u4LaIsid = 0;
    UINT4               u4MplsVcLabel = 0;
    UINT4               u4MplsTunnelLabel = 0;
    UINT4               u4MplsVcTunnelLabel = 0;
    UINT2               u2Count = 0;
    UINT4               u4XorValue = 0;

    if (pBuf == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    LA_MEMSET (au2ActivePortArray, 0, sizeof (au2ActivePortArray));
    LA_MEMSET (&LaDa, 0, sizeof (tLaMacAddr));
    LA_MEMSET (&LaSa, 0, sizeof (tLaMacAddr));
    LA_MEMSET (&AggPolicy, 0, sizeof (tLaLinkSelectPolicy));

    /* Policy is based on the aggregate Index */
    AggPolicy = LA_GET_LINK_SELECTION_POLICY (u2AggIndex);

    switch ((INT4) AggPolicy)
    {

        case LA_SA:

            LA_TRC (DATA_PATH_TRC, "AGG: Link Selection Policy is SA based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaSa, LA_ETH_ADDR_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif

            LaAggCacheEntryTableHashFn (&LaSa, &u4CacheHashIndex);

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (LA_MEMCMP (&(LaSa), pLaCacheEntry->SrcMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this Src Address in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            LA_MEMCPY (&u2Val, &LaSa[4], 2);
            u2Val = (UINT2) (LA_NTOHS (u2Val));

            u2Result = (UINT2) ((u2Val & u2Mask) ^
                                ((u2Val & (u2Mask << u2NumOfBits)) >>
                                 u2NumOfBits));

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, &LaSa, NULL, LaVlan, u4LaIsid,
                                   u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC, "AGG: Distribution Algorithm Applied\n");

            break;

        case LA_SA_IP:
        case LA_DA_IP:
        case LA_SA_DA_IP:

            /* Fall Through */
        case LA_DA:
        case LA_SA_DA_IP_PORT_PROTO:
            LA_TRC (DATA_PATH_TRC, "AGG: Link Selection Policy is DA based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaDa, LA_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif

            LaAggCacheEntryTableHashFn (&LaDa, &u4CacheHashIndex);

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (LA_MEMCMP (&LaDa, &pLaCacheEntry->DestMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this Dest Address in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);
            LA_MEMCPY (&u2Val, &LaDa[4], 2);

            u2Val = (UINT2) (LA_NTOHS (u2Val));
            u2Result = (UINT2) ((u2Val & u2Mask) ^
                                ((u2Val & (u2Mask << u2NumOfBits)) >>
                                 u2NumOfBits));

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, NULL, &LaDa, LaVlan, u4LaIsid,
                                   u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC, "AGG: Distribution Algorithm Applied\n");

            break;

        case LA_SA_DA:
        case LA_ENHANCED:
        case LA_RANDOMIZED:
            /* Enhanced and randomized modes are applicable only in supported hardware
             * so will be using default LA_SA_DA */

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is SA_DA based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaDa, LA_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaSa, LA_ETH_ADDR_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif

            LaAggCacheEntryTableHashFn (&LaSa, &u4CacheHashIndex);

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (LA_MEMCMP (&LaSa, pLaCacheEntry->SrcMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0) &&
                    (LA_MEMCMP (&LaDa, pLaCacheEntry->DestMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this Src Address in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            LA_MEMCPY (&u2Val, &LaSa[4], 2);
            u2Val = (UINT2) (LA_NTOHS (u2Val));

            LA_MEMCPY (&u2DestVal, &LaDa[4], 2);
            u2DestVal = (UINT2) (LA_NTOHS (u2DestVal));

            u2Result = (u2Val & u2Mask) ^ (u2DestVal & u2Mask);

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, &LaSa, &LaDa, LaVlan, u4LaIsid,
                                   u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC, "AGG: Distribution Algorithm Applied\n");

            break;

        case LA_VLAN_ID:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is VLAN_ID based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaVlan, LA_VLAN_OFFSET,
                                      LA_VLAN_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u2Val = LA_NTOHS (LaVlan);
            u2Val &= LA_VLAN_MASK;
            u4CacheHashIndex = u2Val % LA_CACHE_TABLE_SIZE;

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (u2Val == pLaCacheEntry->VlanId))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this VLAN ID in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            u2Result = u2Val;

            for (u2Count = 1; u2Count <= ((sizeof (u2Val) * 8) / u2NumOfBits);
                 u2Count++)
            {
                u2Result = (UINT2) ((u2Result & u2Mask) ^
                                    ((u2Val >> (u2Count * u2NumOfBits)) &
                                     u2Mask));

            }

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, &LaSa, &LaDa, u2Val, u4LaIsid,
                                   u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC, "AGG: Distribution Algorithm Applied\n");

            break;

        case LA_ISID:
        {

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is LA_ISID based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u4LaIsid, LA_ISID_OFFSET,
                                      LA_ISID_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u4Val = LA_NTOHL (u4LaIsid);
            u4Val &= LA_ISID_MASK;
            u4CacheHashIndex = u4Val % LA_CACHE_TABLE_SIZE;

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (u4Val == pLaCacheEntry->u4Isid))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this ISID in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            u4Result = u4Val;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of LA_MAX_PORTS_PER_AGG is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            for (u2Count = 1; u2Count <= ((sizeof (u4Val) * 8) / u2NumOfBits);
                 u2Count++)
            {
                u4XorValue = (u4Val >> (u2Count * u2NumOfBits));
                u4Result = (UINT4) ((u4Result & u2Mask) ^
                                    (u4XorValue & u2Mask));
                if (u4XorValue == 0)
                {
                    /* Becasue if Xor value is 0 then the u4Result wont change */
                    break;
                }
            }

            if (u1PortCount < u1Threshold)
            {
                if (u4Result > ((UINT4) u1PortCount - 1))
                {
                    u4Result = 0;
                }
            }

            if (LaAggGetValidPort
                (au2ActivePortArray, (UINT2) u4Result, u1PortCount, u2AggIndex,
                 &LaSa, &LaDa, LaVlan, u4Val,
                 u4MplsVcLabel, u4MplsTunnelLabel, u4MplsVcTunnelLabel,
                 pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC, "AGG: Distribution Algorithm Applied\n");

            break;
        }

        case LA_SA_VID:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is SA VLANID based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaSa, LA_ETH_ADDR_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }
            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaVlan, LA_VLAN_OFFSET,
                                      LA_VLAN_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u2ValVlan = LA_NTOHS (LaVlan);
            u2ValVlan &= LA_VLAN_MASK;
            u4CacheHashIndex = u2ValVlan % LA_CACHE_TABLE_SIZE;

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (LA_MEMCMP (&(LaSa), pLaCacheEntry->SrcMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0) &&
                    (u2ValVlan == pLaCacheEntry->VlanId))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this Vlan Id and Src Addr in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            LA_MEMCPY (&u2ValAddr, &LaSa[4], 2);
            u2ValAddr = (UINT2) (LA_NTOHS (u2ValAddr));

            u2ResultAddr = (UINT2) ((u2ValAddr & u2Mask) ^
                                    ((u2ValAddr & (u2Mask << u2NumOfBits)) >>
                                     u2NumOfBits));

            u2ResultVlan = u2ValVlan;

            for (u2Count = 1;
                 u2Count <= ((sizeof (u2ValVlan) * 8) / u2NumOfBits); u2Count++)
            {
                u2ResultVlan = (UINT2) ((u2ResultVlan & u2Mask) ^
                                        ((u2ValVlan >> (u2Count * u2NumOfBits))
                                         & u2Mask));

            }

            u2Result = u2ResultAddr | u2ResultVlan;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, &LaSa, NULL, u2ValVlan, u4LaIsid,
                                   u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC,
                    "AGG: SA VLANID Distribution Algorithm Applied\n");

            break;

        case LA_DA_VID:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is DA VLANID based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaDa, LA_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }
            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaVlan, LA_VLAN_OFFSET,
                                      LA_VLAN_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u2ValVlan = LA_NTOHS (LaVlan);
            u2ValVlan &= LA_VLAN_MASK;
            u4CacheHashIndex = u2ValVlan % LA_CACHE_TABLE_SIZE;

            LaAggCacheEntryTableHashFn (&LaDa, &u4CacheHashIndex);

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (LA_MEMCMP (&LaDa, &pLaCacheEntry->DestMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0) &&
                    (u2ValVlan == pLaCacheEntry->VlanId))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this VLAN ID and Dest Address in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);
            LA_MEMCPY (&u2ValAddr, &LaDa[4], 2);

            u2ValAddr = (UINT2) (LA_NTOHS (u2ValAddr));
            u2ResultAddr = (UINT2) ((u2ValAddr & u2Mask) ^
                                    ((u2ValAddr & (u2Mask << u2NumOfBits)) >>
                                     u2NumOfBits));

            u2ResultVlan = u2ValVlan;

            for (u2Count = 1;
                 u2Count <= ((sizeof (u2ValVlan) * 8) / u2NumOfBits); u2Count++)
            {
                u2ResultVlan = (UINT2) ((u2ResultVlan & u2Mask) ^
                                        ((u2ValVlan >> (u2Count * u2NumOfBits))
                                         & u2Mask));

            }

            u2Result = u2ResultAddr | u2ResultVlan;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, NULL, &LaDa, u2ValVlan, u4LaIsid,
                                   u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC,
                    "AGG: DA VLANID Distribution Algorithm Applied\n");

            break;

        case LA_SA_DA_VID:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is SA_DA VLANID based\n");

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaDa, LA_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaSa, LA_ETH_ADDR_OFFSET,
                                      LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

            if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaVlan, LA_VLAN_OFFSET,
                                      LA_VLAN_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u2ValVlan = LA_NTOHS (LaVlan);
            u2ValVlan &= LA_VLAN_MASK;
            u4CacheHashIndex = u2ValVlan % LA_CACHE_TABLE_SIZE;

            LaAggCacheEntryTableHashFn (&LaSa, &u4CacheHashIndex);

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (LA_MEMCMP (&LaSa, pLaCacheEntry->SrcMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0) &&
                    (LA_MEMCMP (&LaDa, pLaCacheEntry->DestMacAddr,
                                LA_MAC_ADDRESS_SIZE) == 0) &&
                    (u2ValVlan == pLaCacheEntry->VlanId))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this Src Address, Dest Address
             * and Vlan ID in the Cache Table. Hence apply the 
             * Distribution Algorithm and select the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            LA_MEMCPY (&u2ValAddr, &LaSa[4], 2);
            u2ValAddr = (UINT2) (LA_NTOHS (u2ValAddr));

            LA_MEMCPY (&u2DestVal, &LaDa[4], 2);
            u2DestVal = (UINT2) (LA_NTOHS (u2DestVal));

            u2ResultAddr = (u2ValAddr & u2Mask) ^ (u2DestVal & u2Mask);

            u2ResultVlan = u2ValVlan;

            for (u2Count = 1;
                 u2Count <= ((sizeof (u2ValVlan) * 8) / u2NumOfBits); u2Count++)
            {
                u2ResultVlan = (UINT2) ((u2ResultVlan & u2Mask) ^
                                        ((u2ValVlan >> (u2Count * u2NumOfBits))
                                         & u2Mask));

            }

            u2Result = u2ResultAddr | u2ResultVlan;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of i4LaAggMaxPorts is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            if (u1PortCount < u1Threshold)
            {
                if (u2Result > ((UINT2) u1PortCount - 1))
                {
                    u2Result = 0;
                }
            }

            if (LaAggGetValidPort (au2ActivePortArray, u2Result, u1PortCount,
                                   u2AggIndex, &LaSa, &LaDa, u2ValVlan,
                                   u4LaIsid, u4MplsVcLabel, u4MplsTunnelLabel,
                                   u4MplsVcTunnelLabel,
                                   pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC,
                    "AGG: SA_DA VLAN ID Distribution Algorithm Applied\n");

            break;

#ifdef MPLS_WANTED
        case LA_MPLS_VC_LABEL:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is LA_MPLS_VC_LABEL based\n");

            if (LA_COPY_FROM_CRU_BUF
                (pBuf, (UINT1 *) &u4MplsVcLabel, LA_MPLS_VC_OFFSET,
                 LA_MPLS_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            /* First 20 bits represents the Label. Right shift to get the correct value 
             * of the Label */
            u4Val = LA_NTOHL (u4MplsVcLabel);
            u4Val &= LA_MPLS_MASK >> 12;
            u4CacheHashIndex = u4Val % LA_CACHE_TABLE_SIZE;

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (u4Val == pLaCacheEntry->u4MplsVcLabel))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this MPLS VC LABEL in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            u4Result = u4Val;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of LA_MAX_PORTS_PER_AGG is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            for (u2Count = 1; u2Count <= ((sizeof (u4Val) * 8) / u2NumOfBits);
                 u2Count++)
            {
                u4XorValue = (u4Val >> (u2Count * u2NumOfBits));
                u4Result = (UINT4) ((u4Result & u2Mask) ^
                                    (u4XorValue & u2Mask));
                if (u4XorValue == 0)
                {
                    /* Becasue if Xor value is 0 then the u4Result wont change */
                    break;
                }
            }

            if (u1PortCount < u1Threshold)
            {
                if (u4Result > ((UINT4) u1PortCount - 1))
                {
                    u4Result = 0;
                }
            }

            if (LaAggGetValidPort
                (au2ActivePortArray, (UINT2) u4Result, u1PortCount, u2AggIndex,
                 &LaSa, &LaDa, LaVlan, u4LaIsid,
                 u4Val, u4MplsTunnelLabel,
                 u4MplsVcTunnelLabel, pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC,
                    "AGG: LA_MPLS_VC_LABEL Distribution Algorithm Applied\n");

            break;

        case LA_MPLS_TUNNEL_LABEL:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is LA_MPLS_TUNNEL_LABEL based\n");

            if (LA_COPY_FROM_CRU_BUF
                (pBuf, (UINT1 *) &u4MplsTunnelLabel, LA_MPLS_TUNNEL_OFFSET,
                 LA_MPLS_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u4Val = LA_NTOHL (u4MplsTunnelLabel);
            u4Val &= LA_MPLS_MASK >> 12;
            u4CacheHashIndex = u4Val % LA_CACHE_TABLE_SIZE;

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (u4Val == pLaCacheEntry->u4MplsTunnelLabel))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this MPLS TUNNEL LABEL in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            u4Result = u4Val;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of LA_MAX_PORTS_PER_AGG is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            for (u2Count = 1; u2Count <= ((sizeof (u4Val) * 8) / u2NumOfBits);
                 u2Count++)
            {
                u4XorValue = (u4Val >> (u2Count * u2NumOfBits));
                u4Result = (UINT4) ((u4Result & u2Mask) ^
                                    (u4XorValue & u2Mask));
                if (u4XorValue == 0)
                {
                    /* Becasue if Xor value is 0 then the u4Result wont change */
                    break;
                }
            }

            if (u1PortCount < u1Threshold)
            {
                if (u4Result > ((UINT4) u1PortCount - 1))
                {
                    u4Result = 0;
                }
            }

            if (LaAggGetValidPort
                (au2ActivePortArray, (UINT2) u4Result, u1PortCount, u2AggIndex,
                 &LaSa, &LaDa, LaVlan, u4LaIsid,
                 u4MplsVcLabel, u4Val,
                 u4MplsVcTunnelLabel, pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC,
                    "AGG: LA_MPLS_TUNNEL_LABEL Distribution Algorithm Applied\n");

            break;

        case LA_MPLS_VC_TUNNEL:

            LA_TRC (DATA_PATH_TRC,
                    "AGG: Link Selection Policy is LA_MPLS_VC_TUNNEL based\n");

            if (LA_COPY_FROM_CRU_BUF
                (pBuf, (UINT1 *) &u4MplsVcTunnelLabel, LA_MPLS_VC_TUNNEL_OFFSET,
                 LA_MPLS_SIZE) == LA_CRU_FAILURE)
            {
                LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "AGG: Outgoing Frame Copy from CRU Buffer FAILED\n");
                return LA_FAILURE;
            }

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif
            u4Val = LA_NTOHL (u4MplsVcTunnelLabel);
            u4Val &= LA_MPLS_MASK >> 12;
            u4CacheHashIndex = u4Val % LA_CACHE_TABLE_SIZE;

            LA_HASH_SCAN_BKT (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                              pLaCacheEntry, tLaCacheEntry *)
            {

                if ((u2AggIndex == pLaCacheEntry->u2AggIndex) &&
                    (u4Val == pLaCacheEntry->u4MplsVcTunnelLabel))
                {

                    if (LaAggGetPortFromCacheTable (pBuf, pLaCacheEntry,
                                                    pu2SelectedPortIndex) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                "AGG: Unable to get Port from Cache Table\n");
                        return LA_FAILURE;
                    }

                    LA_TRC_ARG1 (DATA_PATH_TRC,
                                 "AGG: Matching Port from Cache Table: %d\n",
                                 *pu2SelectedPortIndex);

                    return LA_SUCCESS;

                }                /* End of matching entry processing */
            }                    /* End of Cache Entry Table Scan */

            /* This implies that no entry exists for this MPLS VC TUNNEL LABEL in the
             * Cache Table. Hence apply the Distribution Algorithm and select
             * the outgoing port */

            if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                     &u1PortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            if (u1PortCount == 0)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: No Port is attached to Aggregator %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaAggGetMask (u2AggIndex, &u1Threshold, &u2Mask, &u2NumOfBits);

            u4Result = u4Val;

            /* In continuation of the comment given in LaAggGetMask() function,
             * if the value of LA_MAX_PORTS_PER_AGG is 14, if the Threshold value
             * is say 8 ports then the actual number of active ports can be less
             * than 8 or greater than 8 and less than 14.The distribution will
             * select one port among the Threshold number of ports.If the actual
             * number of active ports is less than the Threshold, and if the
             * distribution algorithm selects any of the ports which are not
             * currently active, then the first port in the Active Port Array is
             * chosen.*/

            for (u2Count = 1; u2Count <= ((sizeof (u4Val) * 8) / u2NumOfBits);
                 u2Count++)
            {
                u4XorValue = (u4Val >> (u2Count * u2NumOfBits));
                u4Result = (UINT4) ((u4Result & u2Mask) ^
                                    (u4XorValue & u2Mask));
                if (u4XorValue == 0)
                {
                    /* Becasue if Xor value is 0 then the u4Result wont change */
                    break;
                }
            }

            if (u1PortCount < u1Threshold)
            {
                if (u4Result > ((UINT4) u1PortCount - 1))
                {
                    u4Result = 0;
                }
            }

            if (LaAggGetValidPort
                (au2ActivePortArray, (UINT2) u4Result, u1PortCount, u2AggIndex,
                 &LaSa, &LaDa, LaVlan, u4LaIsid,
                 u4MplsVcLabel, u4MplsTunnelLabel, u4Val,
                 pu2SelectedPortIndex) != LA_SUCCESS)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: LaAggGetValidPort function FAILED\n");
                return LA_FAILURE;
            }

            LA_TRC (DATA_PATH_TRC,
                    "AGG: LA_MPLS_VC_TUNNEL Distribution Algorithm Applied\n");

            break;

#endif

        default:

            LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                    "AGG: Invalid Value of Link Selection Policy\n");

            return LA_FAILURE;

    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggGetMask                                         */
/*                                                                           */
/* Description        : This function is called by the Select Link function  */
/*                      in order to compute the mask value for performing    */
/*                      EX-OR operation on the MAC Address.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1Threshold - Value to which MaxPorts for a given   */
/*                                     Aggregator is rounded off             */
/*                      pu2Mask - The mask value returned, depending on the  */
/*                                Max Ports per Aggregator                   */
/*                      pu2NumOfBits - The number of bits set in the mask    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaAggGetMask (UINT2 u2AggIndex, UINT1 *pu1Threshold,
              UINT2 *pu2Mask, UINT2 *pu2NumOfBits)
{

    INT4                i4LaAggMaxPorts = 0;
    UINT1               u1Value = 1;
    UINT1               u1LaMaxPorts;
    UINT1               u1Count = 0;

    nmhGetFsLaPortChannelMaxPorts ((INT4) u2AggIndex, &i4LaAggMaxPorts);

    /* The value of i4LaAggMaxPorts is rounded off to a value
     * which is a multiple of the powers of 2 and a corresponding mask is then
     * computed. FOr example, if the value of i4LaAggMaxPorts is 14, then
     * this will be rounded off to 8 by right shifting u1LaMaxPorts by 1 and
     * left shifting u1Value by 1 till it becomes 0 i.e. when u1LaMaxPorts starts
     * with 14 and reaches 0, u1Value will be 8.*/

    u1LaMaxPorts = (UINT1) i4LaAggMaxPorts;

    u1LaMaxPorts = (UINT1) (u1LaMaxPorts >> 1);

    while (u1LaMaxPorts)
    {
        u1LaMaxPorts = (UINT1) (u1LaMaxPorts >> 1);
        u1Value = (UINT1) (u1Value << 1);
        u1Count++;
    }

    /* The Threshold value is the value(in powers of 2) to which
     * i4LaAggMaxPorts has been rounded off. The number od bits field
     * gives the number of bits set in the Mask value. */
    *pu1Threshold = u1Value;
    *pu2Mask = (UINT2) (u1Value - 1);
    *pu2NumOfBits = (UINT2) u1Count;

}

/*****************************************************************************/
/* Function Name      : LaAggGetValidPort                                    */
/*                                                                           */
/* Description        : This function checks if an entry is present in the   */
/*                      Marker Entry for the portindex passed as input.      */
/*                      If a match is found, then it selects the next port   */
/*                      from the Active port list and checks if an entry     */
/*                      is present for this port. This repeats until there   */
/*                      is a portindex for which there is null entry in the  */
/*                      Marker Entry. It then adds an entry for this         */
/*                      port in the CacheEntry Table.It then returns the     */
/*                      finally selected port.                               */
/*                                                                           */
/* Input(s)           : pau1ActivePortArray - Array of active ports          */
/*                      u2Result - Index to the selected port in the Active  */
/*                                 port list                                 */
/*                      u1PortCount - Count of Active Ports attached to this */
/*                                    Aggregator                             */
/*                      u2AggIndex - Index of the Aggregator                 */
/*                      pSrcMacAddr - Source Address present in the data     */
/*                                    buffer, NULL if Link Selection Policy  */
/*                                    is DA based                            */
/*                      pDestMacAddr - Destination Address present in the    */
/*                                     data buffer, NULL if Link Selection   */
/*                                     Policy is SA based                    */
/*                      u2Vlan - VLAN Id present in the packet. NULL, if     */
/*                               selection policy is not VLAN_ID based       */
/*                      u4Isid - ISID present in the packet. NULL, if        */
/*                               selection policy is not ISID based          */
/*                      u4MplsVcLabel - Vc Label present in the packet. NULL,*/
/*                                      if selection policy is not MPLS based*/
/*                      u4MplsTunnelLabel - Tunnel Label present in the      */
/*                                          packet. NULL, if selection policy*/
/*                                          is not MPLS based                */
/*                      u4MplsVcTunnelLabel - Vc Tunnel Label present in the */
/*                                          packet. NULL, if selection policy*/
/*                                          is not MPLS based                */
/*                                                                           */
/* Output(s)          : pu2SelectedPortIndex - The finally selected port     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggGetValidPort (UINT2 *pau2ActivePortArray, UINT2 u2Result,
                   UINT1 u1PortCount, UINT2 u2AggIndex,
                   tLaMacAddr * pSrcMacAddr, tLaMacAddr * pDestMacAddr,
                   UINT2 u2Vlan, UINT4 u4Isid, UINT4 u4MplsVcLabel,
                   UINT4 u4MplsTunnelLabel, UINT4 u4MplsVcTunnelLabel,
                   UINT2 *pu2SelectedPortIndex)
{
    UINT1               u1Flag = 0;
    UINT2               u2Index;
    UINT2               u2NextIndex;
    UINT4               u4CacheHashIndex;
    tLaCacheEntry      *pLaCacheEntry = NULL;
    tLaLacPortEntry    *pPortEntry;
    tLaLinkSelectPolicy AggPolicy;

    u4CacheHashIndex = 0;

    if (pau2ActivePortArray == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    /* Identify a suitable port from the Active Port List through which Marker
     * PDU has not been sent */

    /* The 3rd expression in the FOR loop has been omitted because the index
     * is getting incremented inside the loop */

    for (u2Index = u2Result; u2Index <= (UINT2) u1PortCount;)
    {

        LaGetPortEntry (pau2ActivePortArray[u2Index], &pPortEntry);

        if (pPortEntry == NULL)
        {
            return LA_FAILURE;
        }

        if (pPortEntry->pLaMarkerEntry != NULL)
        {

            /* Do the following assignment to get the next suitable port from
             * the active port list */
            u2NextIndex = ++u2Index;

            if (u2NextIndex >= (UINT2) u1PortCount)
            {
                /* Reassign u2Index to 0 to start search from beginning of
                 * active port list; Assign u2Result to u1PortCount so that
                 * port search is done again for the remaining ports in the
                 * active port list till the port with which port search was
                 * started */

                u1Flag++;
                if (u1Flag == 2)
                {
                    LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                 "AGG: Marker PDU sent on ALL PORTS"
                                 "attached to Agg %d; Cannot select link\n",
                                 u2AggIndex);
                    return LA_FAILURE;
                }

                u2Index = 0;
                u1PortCount = (UINT1) u2Result;
            }

            LA_TRC_ARG1 (DATA_PATH_TRC,
                         "AGG: MarkerPDU sent on Port %d; Choosing another port\n",
                         pPortEntry->u2PortIndex);
            continue;

        }                        /* End of IF Marker Entry is not Null (i.e. Marker Pdu has been sent
                                   on that port */
        else
        {
            /* If no match is found, it means that the no Marker PDU has been
             * sent on this port. Hence this is the finally selected port.So add
             * an entry for this in the CacheEntry Table. */

            if (LA_CACHEENTRY_ALLOC_MEM_BLOCK (pLaCacheEntry) == NULL)
            {
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: Cache Entry Memory Block Allocation FAILED\n");

                /* If cache entries are full, return the first active/distributing
                 * port as the selected port */
                *pu2SelectedPortIndex = pau2ActivePortArray[u2Index];

                LA_TRC_ARG2 (DATA_PATH_TRC,
                             "AGG: Selected Port %d of Agg %d\n",
                             *pu2SelectedPortIndex, u2AggIndex);

                return LA_SUCCESS;
            }

            LA_MEMSET (pLaCacheEntry, 0, sizeof (tLaCacheEntry));

            LA_HASH_INIT_NODE (&pLaCacheEntry->NextCacheEntry);

            LA_GET_SYS_TIME (&(pLaCacheEntry->LastAccessTime));

            pLaCacheEntry->u2PortIndex = pau2ActivePortArray[u2Index];
            pLaCacheEntry->u2AggIndex = u2AggIndex;
            pLaCacheEntry->VlanId = u2Vlan;
            pLaCacheEntry->u4Isid = u4Isid;
            pLaCacheEntry->u4MplsVcLabel = u4MplsVcLabel;
            pLaCacheEntry->u4MplsTunnelLabel = u4MplsTunnelLabel;
            pLaCacheEntry->u4MplsVcTunnelLabel = u4MplsVcTunnelLabel;
            pLaCacheEntry->bMarkerStatus = LA_FALSE;
            pLaCacheEntry->LaCacheTmr.pEntry = (VOID *) pLaCacheEntry;
            pLaCacheEntry->LaCacheTmr.LaAppTimer.u4Data = LA_CACHE_TMR_TYPE;
            pLaCacheEntry->LaCacheTmr.LaTmrFlag = LA_TMR_STOPPED;

            if (LaStartTimer (&(pLaCacheEntry->LaCacheTmr)) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "AGG: Starting Cache Timer for Port %d FAILED\n",
                             pPortEntry->u2PortIndex);

                if (LA_CACHEENTRY_FREE_MEM_BLOCK (pLaCacheEntry)
                    != LA_MEM_SUCCESS)
                {
                    LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                            "AGG: Cache Entry Memory Block Release FAILED\n");
                }

                pLaCacheEntry = NULL;

                return LA_FAILURE;
            }

            /* Storing the Source Mac Address in the CacheEntry Table */
            if (pSrcMacAddr != NULL)
            {
                LA_MEMCPY (pLaCacheEntry->SrcMacAddr, pSrcMacAddr,
                           LA_MAC_ADDRESS_SIZE);
            }
            else
            {
                /* Storing the Source Mac Address as zeroes in the CacheEntry
                 * Table */
                LA_MEMSET (pLaCacheEntry->SrcMacAddr, 0, LA_MAC_ADDRESS_SIZE);
            }

            /*Storing the Destination Mac Address in the CacheEntry Table */
            if (pDestMacAddr != NULL)
            {
                LA_MEMCPY (pLaCacheEntry->DestMacAddr, pDestMacAddr,
                           LA_MAC_ADDRESS_SIZE);
            }
            else
            {
                /* Storing the Destination Mac Address as zeroes in the CacheEntry
                 * Table */
                LA_MEMSET (pLaCacheEntry->DestMacAddr, 0, LA_MAC_ADDRESS_SIZE);
            }

            AggPolicy = LA_GET_LINK_SELECTION_POLICY (u2AggIndex);

            switch ((INT4) AggPolicy)
            {

                case LA_SA:    /* Fall through */
                case LA_ENHANCED:
                case LA_RANDOMIZED:
                    /* Enhanced and randomized modes are applicable only in supported hardware
                     * so will be using default LA_SA_DA */
                case LA_SA_DA:
                    if (pSrcMacAddr != NULL)
                    {
                        LaAggCacheEntryTableHashFn (pSrcMacAddr,
                                                    &u4CacheHashIndex);
                    }
                    break;

                case LA_SA_IP:
                case LA_DA_IP:
                case LA_SA_DA_IP:
                    /* Fall Through */

                case LA_DA:
                case LA_SA_DA_IP_PORT_PROTO:
                    if (pDestMacAddr != NULL)
                    {
                        LaAggCacheEntryTableHashFn (pDestMacAddr,
                                                    &u4CacheHashIndex);
                    }
                    break;

                case LA_SA_VID:
                case LA_DA_VID:
                case LA_SA_DA_VID:
                    /* Fall Through */

                case LA_VLAN_ID:
                    u4CacheHashIndex = u2Vlan % LA_CACHE_TABLE_SIZE;
                    break;

                case LA_ISID:
                    u4CacheHashIndex = u4Isid % LA_CACHE_TABLE_SIZE;
                    break;

#ifdef MPLS_WANTED
                case LA_MPLS_VC_LABEL:
                    u4CacheHashIndex = u4MplsVcLabel % LA_CACHE_TABLE_SIZE;
                    break;

                case LA_MPLS_TUNNEL_LABEL:
                    u4CacheHashIndex = u4MplsTunnelLabel % LA_CACHE_TABLE_SIZE;
                    break;

                case LA_MPLS_VC_TUNNEL:
                    u4CacheHashIndex =
                        u4MplsVcTunnelLabel % LA_CACHE_TABLE_SIZE;
                    break;
#endif

                default:
                    LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                            "AGG: Invalid Value of Link Selection Policy\n");
                    return LA_FAILURE;
            }

            LA_HASH_ADD_NODE (gLaGlobalInfo.pLaCacheTable,
                              &pLaCacheEntry->NextCacheEntry, u4CacheHashIndex,
                              NULL);

#ifdef TRACE_WANTED
            LaDisplayCacheTable ();
#endif

            LA_TRC_ARG1 (DATA_PATH_TRC,
                         "AGG: Cache Node Added for Port %d\n",
                         pLaCacheEntry->u2PortIndex);

            *pu2SelectedPortIndex = pau2ActivePortArray[u2Index];

            LA_TRC_ARG2 (DATA_PATH_TRC,
                         "AGG: Selected Port %d of Agg %d\n",
                         *pu2SelectedPortIndex, u2AggIndex);

            break;

        }                        /* End of else part (no match found) */
    }                            /* End of FOR loop */

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggMultiplexFrame                                  */
/*                                                                           */
/* Description        : This function is called from either Marker Responder,*/
/*                      Marker Receiver or Frame Distributor (i.e. for Marker*/
/*                      PDUs or for frames from Mac Client).It passes the    */
/*                      frames to the Control Parser (lower layer) only if   */
/*                      the port state is Distributing.                      */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the data buffer to be transmitted  */
/*                      pPortEntry - Pointer to the Port Entry          */
/*                      pPktParams - Pointer to the frame parameters         */
/*                                   structure                               */
/*                      bIsMacClientFrame - Boolean value indicating if frame*/
/*                                          is from Mac Client or not        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.aLaLacPortEntry                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggMultiplexFrame (tLaBufChainHeader * pBuf,
                     tLaLacPortEntry * pPortEntry,
                     tLaPktParams * pPktParams, tLaBoolean bIsMacClientFrame)
{
    tLaCallBackArgs     LaCallBackArgs;
    INT4                i4RetVal = LA_FAILURE;

    if (pBuf == NULL || pPortEntry == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    LA_MEMSET (&LaCallBackArgs, 0, sizeof (tLaCallBackArgs));

    /* If frame is from Mac Client, then do the following */
    if (bIsMacClientFrame == LA_TRUE)
    {
        LaCallBackArgs.u2Port = pPortEntry->u2PortIndex;
        /* Verify with Application's state before sending out frames */
        i4RetVal = LaCustCallBack (LA_PKT_TX_EVENT, &LaCallBackArgs);

        if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
             LA_TRUE) && (i4RetVal == LA_SUCCESS) &&
            (LaCallBackArgs.u2PortState == LA_OPER_UP))
        {
            LaHandOverOutFrame (pBuf, pPortEntry->u2PortIndex, pPktParams);
        }
        else
        {
            /* Port state is not Distributing, so discard frame. */

            LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "AGG: Port: %u :: DISTRIBUTING NOT Enabled !!"
                         "Discarding Frame", pPortEntry->u2PortIndex);

            /* Incrementing the Aggregator If Statistics */
            LaIncrementStatsOnDistributorDiscard (pBuf, pPortEntry);

            return LA_FAILURE;
        }
    }                            /* End of Mac Client processing */
    else
    {
        LaHandOverOutFrame (pBuf, pPortEntry->u2PortIndex, pPktParams);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleCacheTmrExpiry                               */
/*                                                                           */
/* Description        : This function is called on Cache Timer Expiry.       */
/*                      This compares the last access time and the current   */
/*                      system time. If difference is equal or greater than  */
/*                      the timeout, then delete the corresponding entry from*/
/*                      the CacheEntry Table. Else, if difference is not     */
/*                      equal to timeout, restart the Cache Timer.           */
/*                                                                           */
/* Input(s)           : pLaCacheEntry - Pointer to the Cache Entry           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                      gLaGlobalInfo.LaLinkSelectPolicy                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaHandleCacheTmrExpiry (tLaCacheEntry * pLaCacheEntry)
{
    UINT4               u4Timeout;
    tLaSysTime          CurrSysTime;
    UINT4               u4CacheHashIndex;
    tLaLinkSelectPolicy AggPolicy;

    if (pLaCacheEntry == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    LA_GET_SYS_TIME (&CurrSysTime);

    u4Timeout = CurrSysTime - (pLaCacheEntry->LastAccessTime);

    /* When the first frame for an SA or DA or SA&DA (based on selection policy) is
     * received, the Cache timer for that entry is started. Everytime a frame is
     * received matching an entry, the time is updated in LastAccessTime field.
     * Hence, compute actual timeout difference.If timeout is not reached, then
     * restart the timer. */

    if (u4Timeout >= LA_CACHE_TIMEOUT)
    {

        AggPolicy = LA_GET_LINK_SELECTION_POLICY (pLaCacheEntry->u2AggIndex);

        switch (AggPolicy)
        {

            case LA_SA:        /* Fall through */
            case LA_ENHANCED:
            case LA_RANDOMIZED:
                /* Enhanced and randomized modes are applicable only in supported hardware
                 * so will be using default LA_SA_DA */

            case LA_SA_DA:

                LaAggCacheEntryTableHashFn (&(pLaCacheEntry->SrcMacAddr),
                                            &u4CacheHashIndex);
                break;

            case LA_SA_IP:
            case LA_DA_IP:
            case LA_SA_DA_IP:
                /* Fall Through */
            case LA_DA:

                LaAggCacheEntryTableHashFn (&(pLaCacheEntry->DestMacAddr),
                                            &u4CacheHashIndex);
                break;

            case LA_SA_VID:
            case LA_DA_VID:
            case LA_SA_DA_VID:
                /* Fall Through */

            case LA_VLAN_ID:
                u4CacheHashIndex = pLaCacheEntry->VlanId % LA_CACHE_TABLE_SIZE;
                break;

            case LA_ISID:
                u4CacheHashIndex = pLaCacheEntry->u4Isid % LA_CACHE_TABLE_SIZE;
                break;

#ifdef MPLS_WANTED
            case LA_MPLS_VC_LABEL:
                u4CacheHashIndex =
                    pLaCacheEntry->u4MplsVcLabel % LA_CACHE_TABLE_SIZE;
                break;

            case LA_MPLS_TUNNEL_LABEL:
                u4CacheHashIndex =
                    pLaCacheEntry->u4MplsTunnelLabel % LA_CACHE_TABLE_SIZE;
                break;

            case LA_MPLS_VC_TUNNEL:
                u4CacheHashIndex =
                    pLaCacheEntry->u4MplsVcTunnelLabel % LA_CACHE_TABLE_SIZE;
                break;
#endif

            default:
                LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                        "AGG: Invalid Value of Link Selection Policy\n");
                return LA_FAILURE;
        }

        LA_TRC_ARG1 (DATA_PATH_TRC,
                     "AGG: Deleting Cache Entry with Port Index %d\n",
                     pLaCacheEntry->u2PortIndex);

        LA_HASH_DEL_NODE (gLaGlobalInfo.pLaCacheTable,
                          &pLaCacheEntry->NextCacheEntry, u4CacheHashIndex);

        if (LA_CACHEENTRY_FREE_MEM_BLOCK (pLaCacheEntry) != LA_MEM_SUCCESS)
        {
            LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                    "AGG: Cache Entry Memory Block Release FAILED\n");
            return LA_FAILURE;
        }

    }                            /* End of timeout occuring */

    else
    {
        if (pLaCacheEntry->LaCacheTmr.LaTmrFlag == LA_TMR_STOPPED)
        {
            if (LaStartTimer (&(pLaCacheEntry->LaCacheTmr)) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "AGG: Re-starting Cache Timer for Port %d FAILED\n",
                             pLaCacheEntry->u2PortIndex);
                return LA_FAILURE;
            }
        }

        LA_TRC_ARG1 (DATA_PATH_TRC,
                     "AGG: Restarting CacheTmr of CacheEntry with PortIndex %d\n",
                     pLaCacheEntry->u2PortIndex);

    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggGetPortFromCacheTable                           */
/*                                                                           */
/* Description        : This function checks if a Marker PDU has been sent   */
/*                      on the port corresponding to pLaCacheEntry given as  */
/*                      input. If not, then it returns this port as the      */
/*                      SelectedPortIndex. Else, it should discard the frame.*/
/*                      Hence it returns Failure.                            */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the data buffer                    */
/*                      pLaCacheEntry - Pointer to the matching Cache Entry  */
/*                                                                           */
/* Output(s)          : pu2SelectedPortIndex - The index of the selected port*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggGetPortFromCacheTable (tLaBufChainHeader * pBuf,
                            tLaCacheEntry * pLaCacheEntry,
                            UINT2 *pu2SelectedPortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (pBuf == NULL || pLaCacheEntry == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    if (pLaCacheEntry->bMarkerStatus != LA_TRUE)
    {
        *pu2SelectedPortIndex = pLaCacheEntry->u2PortIndex;
        LA_GET_SYS_TIME (&(pLaCacheEntry->LastAccessTime));

        LA_TRC_ARG1 (DATA_PATH_TRC,
                     "AGG: Port Index from Matching Cache Entry %d\n",
                     *pu2SelectedPortIndex);
    }
    else
    {

        LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                     "Marker PDU sent on Port %d. Hence discard frame\n",
                     pLaCacheEntry->u2PortIndex);

        LaGetPortEntry (pLaCacheEntry->u2PortIndex, &pPortEntry);

        /*Increment the Aggregator If Statistics */
        if (pPortEntry != NULL)
        {
            LaIncrementStatsOnDistributorDiscard (pBuf, pPortEntry);
        }

        /* Discard frame */
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggDeleteMarkerEntry                               */
/*                                                                           */
/* Description        : This function deletes Marker entries corresponding   */
/*                      to the port entry given as input.                    */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the port entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggDeleteMarkerEntry (tLaLacPortEntry * pPortEntry)
{
    tLaMarkerEntry     *pLaMarkerEntry = NULL;

    if (pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    pLaMarkerEntry = pPortEntry->pLaMarkerEntry;

    if (pLaMarkerEntry != NULL)
    {

        if (pLaMarkerEntry->LaMarkerTmr.LaTmrFlag != LA_TMR_STOPPED)
        {
            if (LaStopTimer (&(pLaMarkerEntry->LaMarkerTmr)) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "AGG: Stopping Marker Timer for Port %d FAILED\n",
                             pPortEntry->u2PortIndex);
                return LA_FAILURE;
            }
        }                        /* End of IF Timer running */

        if (LA_MARKERENTRY_FREE_MEM_BLOCK (pLaMarkerEntry) != LA_MEM_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "AGG: Marker Entry Memory Block Release FAILED\n");
            return LA_FAILURE;
        }

        pPortEntry->pLaMarkerEntry = NULL;

    }                            /* End of Marker Entry not being equal to NULL */

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggDeleteCacheEntry                                */
/*                                                                           */
/* Description        : This function deletes entries from the Cache Table   */
/*                      corresponding to the port entry given as input.      */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the port entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggDeleteCacheEntry (tLaLacPortEntry * pPortEntry)
{
    tLaHashNode        *pLaFirstCacheEntry = NULL;
    tLaHashNode        *pLaNextCacheEntry = NULL;
    UINT4               u4CacheHashIndex;

    if (pPortEntry == NULL)
    {
        LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "AGG: Null Pointer\n");
        return LA_FAILURE;
    }

    LA_HASH_SCAN_TBL (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex)
    {
        pLaFirstCacheEntry = LA_HASH_GET_FIRST_BUCKET_NODE
            (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex);

        while (pLaFirstCacheEntry != NULL)
        {

            pLaNextCacheEntry = LA_HASH_GET_NEXT_BUCKET_NODE
                (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                 pLaFirstCacheEntry);

            if (((tLaCacheEntry *) pLaFirstCacheEntry)->u2PortIndex
                == pPortEntry->u2PortIndex)
            {

                if (((tLaCacheEntry *) pLaFirstCacheEntry)->LaCacheTmr.LaTmrFlag
                    != LA_TMR_STOPPED)
                {
                    if (LaStopTimer
                        (&(((tLaCacheEntry *) pLaFirstCacheEntry)->LaCacheTmr))
                        != LA_SUCCESS)
                    {
                        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                                     ALL_FAILURE_TRC,
                                     "AGG: Stopping Cache Timer for Port %d FAILED\n",
                                     pPortEntry->u2PortIndex);
                        return LA_FAILURE;
                    }
                }                /* End of IF Timer running */

                LA_TRC_ARG1 (DATA_PATH_TRC,
                             "AGG: Deleting Cache Entry with Port Index %d\n",
                             ((tLaCacheEntry *) pLaFirstCacheEntry)->
                             u2PortIndex);

                LA_HASH_DEL_NODE (gLaGlobalInfo.pLaCacheTable,
                                  pLaFirstCacheEntry, u4CacheHashIndex);

                if (LA_CACHEENTRY_FREE_MEM_BLOCK
                    ((tLaCacheEntry *) pLaFirstCacheEntry) != LA_MEM_SUCCESS)
                {
                    LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "AGG: Cache Entry Memory Block Release FAILED\n");
                    return LA_FAILURE;
                }

            }                    /* End of IF */

            pLaFirstCacheEntry = pLaNextCacheEntry;

        }                        /* End of WHILE */
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggDeleteCacheTableWithAggIndex                    */
/*                                                                           */
/* Description        : This function deletes entries from the Cache Table   */
/*                      associated with a given Aggregation.                 */
/*                                                                           */
/* Input(s)           : u2AggIndex                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggDeleteCacheTableWithAggIndex (UINT2 u2AggIndex)
{
    tLaHashNode        *pLaFirstCacheEntry = NULL;
    tLaHashNode        *pLaNextCacheEntry = NULL;
    UINT4               u4CacheHashIndex;

    LA_TRC (CONTROL_PLANE_TRC,
            "AGG: Cache Table Deletion per Aggregation .....\r\n");

    LA_HASH_SCAN_TBL (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex)
    {
        pLaFirstCacheEntry = LA_HASH_GET_FIRST_BUCKET_NODE
            (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex);

        while (pLaFirstCacheEntry != NULL)
        {

            pLaNextCacheEntry = LA_HASH_GET_NEXT_BUCKET_NODE
                (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                 pLaFirstCacheEntry);

            if (((tLaCacheEntry *) pLaFirstCacheEntry)->u2AggIndex ==
                u2AggIndex)
            {
                if (((tLaCacheEntry *) pLaFirstCacheEntry)->LaCacheTmr.LaTmrFlag
                    != LA_TMR_STOPPED)
                {
                    if (LaStopTimer
                        (&(((tLaCacheEntry *) pLaFirstCacheEntry)->LaCacheTmr))
                        != LA_SUCCESS)
                    {
                        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                     "AGG: Stopping Cache Timer(%x) FAILED\n",
                                     pLaFirstCacheEntry);
                    }
                }                /* End of IF Timer running */

                LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                             "AGG: Deleting Cache Entry (%x) Port Index %d\n",
                             ((tLaCacheEntry *) pLaFirstCacheEntry),
                             ((tLaCacheEntry *) pLaFirstCacheEntry)->
                             u2PortIndex);
                LA_HASH_DEL_NODE (gLaGlobalInfo.pLaCacheTable,
                                  pLaFirstCacheEntry, u4CacheHashIndex);

                if (LA_CACHEENTRY_FREE_MEM_BLOCK
                    ((tLaCacheEntry *) pLaFirstCacheEntry) != LA_MEM_SUCCESS)
                {
                    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "AGG: Cache Entry Memory Block Release FAILED\n");
                }
            }

            pLaFirstCacheEntry = pLaNextCacheEntry;

        }                        /* End of WHILE */
    }

    LA_TRC (CONTROL_PLANE_TRC, "AGG: Cache Table Deletion Success.....\r\n");

    return LA_SUCCESS;
}

/* End of File */
