/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: laport.c,v 1.44 2017/11/16 14:26:55 siva Exp $
*
*********************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : laport.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Aggregator module                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains external module routines    */
/*                            used in LA.                                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 October 2007/        Initial Create.                        */
/*            L2 Team                                                        */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"
#if (defined DLAG_WANTED) || (defined ICCH_WANTED)
#include "fsla.h"
#endif
#ifdef NPAPI_WANTED
#ifdef SNMP_2_WANTED
#include "fssnmp.h"
#endif
#endif

extern UINT1       *EoidGetEnterpriseOid (void);

/*****************************************************************************/
/* Function Name      : LaVcmGetContextInfoFromIfIndex                       */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                UINT2 *pu2LocalPortId)
{
    if (VCM_FAILURE == VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                                     pu2LocalPortId))
    {
        return LA_FAILURE;
    }

    if (VCM_FALSE == VcmIsVcExist (*pu4ContextId))
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function name      : LaL2IwfGetSispPortCtrlStatus                         */
/*                                                                           */
/* Description        : This API will be used by Link aggregation to get     */
/*                      SISP enabled / disabled on an interface.             */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Index of the port to be created.       */
/*                      *pu1Status  - SISP enable/disable status             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
LaL2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    return (L2IwfGetSispPortCtrlStatus (u4IfIndex, pu1Status));
}

/*****************************************************************************/
/* Function Name      : LaCfaCliGetIfName                                    */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
}

/****************************************************************************/
/* Function Name      : LaCfaGetIfBridgedIfaceStatus                         */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface status from the given interface index.     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1Status - Interface status of the given IfIndex.   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*
 * *************************************************************************/
INT4
LaCfaGetIfBridgedIfaceStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    return (CfaGetIfBridgedIfaceStatus (u4IfIndex, pu1Status));
}

/*****************************************************************************/
/* Function Name      : LaCfaGetIfInfo                                       */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfInfo   - Interface information.                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : LaCfaSetIfInfo                                       */
/*                                                                           */
/* Description        : This function calls the CFA Module to set the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : i4CfaIfParam - To indicate the CFA param.            */
/*                      u4IfIndex - Interface index                          */
/*                      pIfInfo   - Interface information.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaSetIfInfo (i4CfaIfParam, u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : LaCfaUfdIsLastDownlinkInGroup                        */
/*                                                                           */
/* Description        : This function calls the CFA Module to check          */
/*                      whether the interface is in UFD group                */
/*                      and also ast downlink or not                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaUfdIsLastDownlinkInGroup (UINT4 u4IfIndex)
{
    return (CfaUfdIsLastDownlinkInGroup (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : LaCfaDeletePortChannel                               */
/*                                                                           */
/* Description        : This function is used to indicate delete port-channel*/
/*                       in the CFA module and other L2 modules.             */
/*                                                                           */
/* Input(s)           : u4Index - IfIndex of the portchannel port            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaCfaDeletePortChannel (UINT4 u4IfIndex)
{
    if (CfaDeletePortChannelIndication ((UINT2) u4IfIndex) != CFA_SUCCESS)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCfaGetIfCounters                                   */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface counters.                                  */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfCounter- Pointer to the interface counter.        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaGetIfCounters (UINT2 u2IfIndex, tIfCountersStruct * pIfCounter)
{
    return (CfaGetIfCounters (u2IfIndex, pIfCounter));
}

/*****************************************************************************/
/* Function Name      : LaCfaGetSysMacAddress                                */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      system MAC address.                                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : SwitchMac - switch MAC address                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaCfaGetSysMacAddress (tMacAddr SwitchMac)
{
    CfaGetSysMacAddress (SwitchMac);
}

/*****************************************************************************/
/* Function Name      : LaCfaNotifyProtoToApp                                */
/*                                                                           */
/* Description        : This function calls the CFA Module to notify the     */
/*                      applications.                                        */
/*                                                                           */
/* Input(s)           : u1ModeId - Specifies from which module the           */
/*                      notication is given.                                 */
/*                      NotifyProtoToApp - contains the indications specific */
/*                      to the notification.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaCfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp)
{
    CfaNotifyProtoToApp (u1ModeId, NotifyProtoToApp);
}

/*****************************************************************************/
/* Function Name      : LaPnacGetStartedStatus                               */
/*                                                                           */
/* Description        : This function calls the PNAC module to get the       */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaPnacGetStartedStatus (VOID)
{
    return (PnacGetStartedStatus ());
}

/*****************************************************************************/
/* Function Name      : LaDsCheckDiffservPort                                */
/*                                                                           */
/* Description        : This function calls the Diffserv module to check     */
/*                      whether the given port is a Diffsrv port.            */
/*                                                                           */
/* Input(s)           : i4TrunkPort - Trunk Port                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaDsCheckDiffservPort (INT4 i4TrunkPort)
{
    return (DsCheckDiffservPort (i4TrunkPort));
}

/*****************************************************************************/
/* Function Name      : LaDsGetStartedStatus                                 */
/*                                                                           */
/* Description        : This function calls the Diffserv module to get the   */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaDsGetStartedStatus (VOID)
{
    return (DsGetStartedStatus ());
}

/*****************************************************************************/
/* Function Name      : LaIssFreeAggregatorMac                               */
/*                                                                           */
/* Description        : This function calls the ISS module to free the       */
/*                      Aggregator MAC address.                              */
/*                                                                           */
/* Input(s)           : u4AggIndex - Aggregator Index.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaIssFreeAggregatorMac (UINT4 u4AggIndex)
{
    IssFreeAggregatorMac (u4AggIndex);
}

/*****************************************************************************/
/* Function Name      : LaIssGetAggregatorMac                                */
/*                                                                           */
/* Description        : This function calls the ISS module to get the        */
/*                      Aggregator MAC address.                              */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggregator Index.                       */
/*                                                                           */
/* Output(s)          : AggrMac.                                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaIssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac)
{
    return (IssGetAggregatorMac (u2AggIndex, AggrMac));
}

/*****************************************************************************/
/* Function Name      : LaIssMirrorStatusCheck                               */
/*                                                                           */
/* Description        : This function calls the ISS module to check if       */
/*                      mirroring can be enabled on a given port.            */
/*                                                                           */
/* Input(s)           : u2Port - Interface index.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaIssMirrorStatusCheck (UINT2 u2Port)
{
    return (IssMirrorStatusCheck (u2Port));
}

/*****************************************************************************/
/* Function Name      : LaIssGetAsyncMode                                    */
/*                                                                           */
/* Description        : This function calls the ISS module to check          */
/*                      NPAPI operating in Synchronous or Asynchronous mode  */
/*                                                                           */
/* Input(s)           : i4ProtoId - Protocol Id.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaIssGetAsyncMode (INT4 i4ProtoId)
{
    return (IssGetAsyncMode (i4ProtoId));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfGetNextValidPort                              */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the next */
/*                      Valid port.                                          */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2NextPort - Next Interface Index                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfGetNextValidPort (UINT2 u2IfIndex, UINT2 *pu2NextPort)
{
    return (L2IwfGetNextValidPort (u2IfIndex, pu2NextPort));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfCreatePortIndicationFromLA                    */
/*                                                                           */
/* Description        : This function calls the L2IWF module to indicate the */
/*                      CREATE port.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u1PortOperStatus - Oper status                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaL2IwfCreatePortIndicationFromLA (UINT4 u4IfIndex, UINT1 u1PortOperStatus)
{
    L2IwfCreatePortIndicationFromLA (u4IfIndex, u1PortOperStatus);
}

/*****************************************************************************/
/* Function Name      : LaL2IwfDeleteAndAddPortToPortChannel                 */
/*                                                                           */
/* Description        : This function calls the L2IWF module to indicate the */
/*                      port delete.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u2AggId - Aggregator Id                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfDeleteAndAddPortToPortChannel (UINT2 u2IfIndex, UINT2 u2AggId)
{
    return (L2IwfDeleteAndAddPortToPortChannel (u2IfIndex, u2AggId));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfGetPortOperStatus                             */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the oper */
/*                      status of the port.                                  */
/*                                                                           */
/* Input(s)           : i4ModuleId- Module Identifier                        */
/*                      u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu1OperStatus - Oper status                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT2 u2IfIndex,
                          UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u2IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfGetPortPnacAuthControl                        */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the port */
/*                      AuthControl                                          */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2PortAuthControl                                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfGetPortPnacAuthControl (UINT2 u2IfIndex, UINT2 *pu2PortAuthControl)
{
    return (L2IwfGetPortPnacAuthControl (u2IfIndex, pu2PortAuthControl));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfGetProtocolTunnelStatusOnPort                 */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      tunnel status for the given port.                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u2Protocol - Protocol identifier                     */
/*                                                                           */
/* Output(s)          : pu1TunnelStatus - Tunnel status                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                      UINT1 *pu1TunnelStatus)
{
    L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, u2Protocol, pu1TunnelStatus);
}

/*****************************************************************************/
/* Function Name      : LaL2IwfHandleOutgoingPktOnPort                       */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      PNAC packet.                                         */
/*                                                                           */
/* Input(s)           : pBuf - pointer to the outgoing packet                */
/*                      TaggedPortBitmap - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT2 u2IfIndex, UINT4 u4PktSize,
                                UINT2 u2Protocol, UINT1 u1Encap)
{
    return (L2IwfHandleOutgoingPktOnPort (pBuf, u2IfIndex, u4PktSize,
                                          u2Protocol, u1Encap));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfLaHLPortOperIndication                        */
/*                                                                           */
/* Description        : This function calls the L2IWF module to indicate the */
/*                      oper status for the higher layer.                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u1PortOperStatus - Oper status                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfLaHLPortOperIndication (UINT2 u2IfIndex, UINT1 u1OperStatus)
{
    INT4                i4RetStatus = FAILURE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    UINT1               u1BridgeStatus = CFA_ENABLED;

    i4RetStatus = L2IwfLaHLPortOperIndication (u2IfIndex, u1OperStatus);

    if (i4RetStatus == SUCCESS)
    {
        CfaGetIfName ((UINT4) u2IfIndex, au1IfName);
        CfaGetIfBridgedIfaceStatus ((UINT4) u2IfIndex, &u1BridgeStatus);

        if (u1BridgeStatus != CFA_ENABLED)
        {
            IpUpdateInterfaceStatus ((UINT2)
                                     (CfaGetIfIpPort ((UINT4) u2IfIndex)),
                                     au1IfName, u1OperStatus);
        }
        if (CfaIfmConfigNetworkInterface
            (LA_CFA_NET_IF_UPD, (UINT4) u2IfIndex, 0, NULL) == CFA_FAILURE)
        {
            i4RetStatus = FAILURE;
        }

    }
    return i4RetStatus;

}

/*****************************************************************************/
/* Function Name      : LaL2IwfPbSetDefPortType                              */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      default bridge port type for the port.               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfPbSetDefPortType (UINT2 u2IfIndex)
{
    return (L2IwfPbSetDefPortType (u2IfIndex));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfPbSetPcPortTypeToPhyPort                      */
/*                                                                           */
/* Description        : This function calls the L2IWF module to set the      */
/*                      bridge port type of the port channel to the port.    */
/*                                                                           */
/* Input(s)           : u4PcIfIndex, u4PhyIfIndex                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfPbSetPcPortTypeToPhyPort (UINT4 u4PcIfIndex, UINT4 u4PhyIfIndex)
{
    return (L2IwfPbSetPcPortTypeToPhyPort (u4PcIfIndex, u4PhyIfIndex));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfRemovePortFromPortChannel                     */
/*                                                                           */
/* Description        : This function calls the L2IWF module to indicate the */
/*                      port delete.                                         */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                      u2AggId - Aggregator Id                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfRemovePortFromPortChannel (UINT2 u2IfIndex, UINT2 u2AggId)
{
    return (L2IwfRemovePortFromPortChannel (u2IfIndex, u2AggId));
}

/*****************************************************************************/
/* Function Name      : LaCfaGetLinkTrapEnabledStatus                        */
/*                                                                           */
/* Description        : This function calls the CFA module to indicate the   */
/*             status of snmp trap status for an interface         */
/*                                                                           */
/* Input(s)           : u2Port - Interface index                             */
/*                      *pu1TrapStatus - Trap Status                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaGetLinkTrapEnabledStatus (UINT2 u2Port, UINT1 *pu1TrapStatus)
{
    return CfaGetLinkTrapEnabledStatus ((UINT4) u2Port, pu1TrapStatus);
}

#ifdef L2RED_WANTED
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
/*****************************************************************************/
/* Function Name      : LaAstHandleUpdateEvents                              */
/*                                                                           */
/* Description        : This function calls the STP module to update the     */
/*                      event.                                               */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaAstHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (AstHandleUpdateEvents (u1Event, pData, u2DataLen));
}
#else
#ifdef VLAN_WANTED
/*****************************************************************************/
/* Function Name      : LaVlanRedHandleUpdateEvents                          */
/*                                                                           */
/* Description        : This function calls the VLAN module to update the    */
/*                      event.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaVlanRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (VlanRedHandleUpdateEvents (u1Event, pData, u2DataLen));
}
#else
#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : LaLldpRedRcvPktFromRm                                */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to LLDP */
/*                      task.                                                */
/*                                                                           */
/* Input(s)           : u1Event   - Event to be sent to LLDP                 */
/*                      pData   - Msg to be enqueue.                         */
/*                      u2DataLen - Msg size.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
UINT4
LaLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif
#endif
#endif

/*****************************************************************************/
/* Function Name      : LaRmEnqMsgToRmFromAppl                               */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                        UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : LaRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return LA_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRmGetNodeState                                     */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : LaRmGetStandbyNodeCount                              */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
LaRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : LaRmGetStaticConfigStatus                            */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
LaRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : LaRmHandleProtocolEvent                           */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_INITIATE_BULK_UPDATE /            */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
LaRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : LaRmRegisterProtocols                                */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : LaRmDeRegisterProtocols                              */
/*                                                                           */
/* Description        : This function is to deregister LA module with RM.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
LaRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_LA_APP_ID));
}

/*****************************************************************************/
/* Function Name      : LaRmReleaseMemoryForMsg                              */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : LaRmSetBulkUpdatesStatus                             */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}
#endif
#ifdef NPAPI_WANTED
/*****************************************************************************
 * Function Name      : LaNpFailSyslog
 *
 * Description        : This function sends the NP Fail Error  Message to
 *                      SysLog.
 *
 * Input(s)           : 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *
 ****************************************************************************/
VOID
LaNpFailSyslog (tLaNpTrapLogInfo * pNpLogInfo)
{
#ifdef SYSLOG_WANTED
    UINT1               au1NpOperation[40];
    UINT1               au1NpMesg[55];

    MEMSET (au1NpOperation, '\0', sizeof (au1NpOperation));
    MEMSET (au1NpMesg, '\0', sizeof (au1NpMesg));

    switch (pNpLogInfo->u4NpCallId)
    {
        case AS_FS_LA_HW_CREATE_AGG_GROUP:
            SPRINTF ((CHR1 *) au1NpOperation, "%s", "FsLaHwCreateAggGroup ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u",
                     pNpLogInfo->u2AggIndex);
            break;

        case AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
            SPRINTF ((CHR1 *) au1NpOperation, "%s", "FsLaHwAddLinkToAggGroup ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u in the port %u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u2PortNumber);
            break;

        case AS_FS_LA_HW_DELETE_AGGREGATOR:
            SPRINTF ((CHR1 *) au1NpOperation, "%s", "FsLaHwDeleteAggregator ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u",
                     pNpLogInfo->u2AggIndex);
            break;

        case AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
            SPRINTF ((CHR1 *) au1NpOperation, "%s",
                     "FsLaHwRemoveLinkFromAggGroup ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u in the port %u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u2PortNumber);
            break;

        case AS_FS_LA_HW_SET_SELECTION_POLICY:
            SPRINTF ((CHR1 *) au1NpOperation, "%s",
                     "FsLaHwSetSelectionPolicy ");
            SPRINTF ((CHR1 *) au1NpMesg,
                     "for the agg %u,while applying policy " "%u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u1SelectionPolicy);
            break;

        case AS_FS_LA_HW_ENABLE_COLLECTION:
            SPRINTF ((CHR1 *) au1NpOperation, "%s", "FsLaHwEnableCollection ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u in the port %u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u2PortNumber);
            break;

        case AS_FS_LA_HW_ENABLE_DISTRIBUTION:
            SPRINTF ((CHR1 *) au1NpOperation, "%s",
                     "FsLaHwEnableDistribution ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u in the port %u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u2PortNumber);
            break;

        case AS_FS_LA_HW_DISABLE_COLLECTION:
            SPRINTF ((CHR1 *) au1NpOperation, "%s", "FsLaHwDisableCollection ");
            SPRINTF ((CHR1 *) au1NpMesg, "for the agg %u in the port %u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u2PortNumber);
            break;

        case AS_FS_LA_HW_SET_PORT_CHANNEL_STATUS:
            SPRINTF ((CHR1 *) au1NpOperation, "%s",
                     "FsLaHwSetPortChannelStatus ");
            SPRINTF ((CHR1 *) au1NpMesg,
                     "for the agg %u in the instance:%u for " "state %u",
                     pNpLogInfo->u2AggIndex, pNpLogInfo->u2Inst,
                     pNpLogInfo->u1StpState);
            break;
    }
    /* Send the Log Message to SysLog */
    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, LA_SYSLOG_ID,
                  "[NP-FAULT - LA] %s operation has failed %s\n",
                  (CHR1 *) au1NpOperation, (CHR1 *) au1NpMesg));
#else
    UNUSED_PARAM (pNpLogInfo);
#endif
    return;
}

/*****************************************************************************
 * Function Name      : LaNpFailNotify
 *
 * Description        : This function sends a SNMP trap  Message to 
 *                      administrator if any NP call fails.
 *
 * Input(s)           : tLaNpTrapLogInfo - Contains the information of the NP 
 *                      call which gets fails.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *
 ****************************************************************************/
VOID
LaNpFailNotify (tLaNpTrapLogInfo * pNpLogInfo)
{
#ifdef  SNMP_2_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4NpCallId;
    UINT4               u4GenTrapType = ENTERPRISE_SPECIFIC;
    UINT4               u4SpecTrapType = 1;
    UINT1               au1Buf[LA_OBJECT_NAME_LEN];
    INT1                LA_NP_FAIULRE_TRAPS_OID[SNMP_MAX_OID_LENGTH];

    MEMSET (LA_NP_FAIULRE_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);

    SPRINTF ((CHR1 *) LA_NP_FAIULRE_TRAPS_OID, "1.3.6.1.4.1.%s.159.2.4.1",
             EoidGetEnterpriseOid ());

    switch (pNpLogInfo->u4NpCallId)
    {
        case AS_FS_LA_HW_CREATE_AGG_GROUP:
            u4NpCallId = 0;
            break;
        case AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
            u4NpCallId = 1;
            break;
        case AS_FS_LA_HW_DELETE_AGGREGATOR:
            u4NpCallId = 2;
            break;
        case AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
            u4NpCallId = 3;
            break;
        case AS_FS_LA_HW_SET_SELECTION_POLICY:
            u4NpCallId = 4;
            break;
        case AS_FS_LA_HW_ENABLE_COLLECTION:
            u4NpCallId = 5;
            break;
        case AS_FS_LA_HW_DISABLE_COLLECTION:
            u4NpCallId = 6;
            break;
        case AS_FS_LA_HW_ENABLE_DISTRIBUTION:
            u4NpCallId = 7;
            break;
        default:
            return;
    }

    pEnterpriseOid = SNMP_AGT_GetOidFromString (LA_NP_FAIULRE_TRAPS_OID);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "fsLaTrapPortChannelIndex");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = LaMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    pVbList =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0,
                                                 pNpLogInfo->u2AggIndex,
                                                 NULL, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        LaMemFreeVarBindList (pVbList);
        return;
    }
    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "fsLaTrapPortIndex");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = LaMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        LaMemFreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0,
                                                 pNpLogInfo->u2PortNumber,
                                                 NULL, NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        LaMemFreeVarBindList (pStartVb);
        return;
    }
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((char *) au1Buf, "fsLaHwFailTrapType");
    pOid = LaMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        LaMemFreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0,
                                                 u4NpCallId,
                                                 NULL, NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        LaMemFreeVarBindList (pStartVb);
        return;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);
#else
    UNUSED_PARAM (pNpLogInfo);
#endif /* SNMP_2_WANTED */
}
#endif /*NPAPI_WANTED */
/******************************************************************************
* Function   : LaParseSubIdNew
* Input      : ppu1TempPtr
* Output     : value of ppu1TempPtr
* Returns    : Pointer to the OID
*******************************************************************************/
INT4
LaParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

#ifdef SNMP_2_WANTED
/******************************************************************************
* Function : LaMakeObjIdFromDotNew                                         *
* Input    : pi1TextStr
* Output   : NONE
* Returns  : pOidPtr or NULL
*******************************************************************************/
tSNMP_OID_TYPE     *
LaMakeObjIdFromDotNew (INT1 *textStr)
{
    tSNMP_OID_TYPE     *poidPtr = NULL;
    INT1               *ptempPtr = NULL, *pdotPtr = NULL;
    INT2                i2Temp = 0;
    UINT2               u2dotCount = 0;
    UINT1              *pu1TmpPtr = NULL;
    UINT4               u4Len = 0;
    INT1                ai1tempBuffer[257];
    INT2                i2OidTableSize;
    struct MIB_OID     *pTableName = NULL;

    pTableName = orig_mib_oid_table;

    i2OidTableSize = (INT2) sizeof (orig_mib_oid_table);

    MEMSET (ai1tempBuffer, 0, 257);

    /* see if there is an alpha descriptor at begining */
    if (0 != ISALPHA (*textStr))
    {
        pdotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (NULL == pdotPtr)
        {
            pdotPtr = textStr + STRLEN ((INT1 *) textStr);
        }
        ptempPtr = textStr;

        for (i2Temp = 0; ((ptempPtr < pdotPtr) && (i2Temp < 256)); i2Temp++)
        {
            ai1tempBuffer[i2Temp] = *ptempPtr++;
        }
        ai1tempBuffer[i2Temp] = '\0';

        for (i2Temp = 0;
             ((i2Temp < (i2OidTableSize / (INT2) sizeof (struct MIB_OID))) &&
              (pTableName[i2Temp].pName) != NULL); i2Temp++)
        {
            if (0 == (STRCMP (pTableName[i2Temp].pName,
                              (INT1 *) ai1tempBuffer))
                && (STRLEN ((INT1 *) ai1tempBuffer) ==
                    STRLEN (pTableName[i2Temp].pName)))
            {
                u4Len = ((STRLEN (pTableName[i2Temp].pNumber) <
                          sizeof (ai1tempBuffer)) ?
                         STRLEN (pTableName[i2Temp].pNumber) :
                         sizeof (ai1tempBuffer) - 1);
                STRNCPY ((INT1 *) ai1tempBuffer,
                         pTableName[i2Temp].pNumber, u4Len);
                ai1tempBuffer[u4Len] = '\0';
                break;
            }
        }

        if ((i2Temp < (i2OidTableSize / (INT2) sizeof (struct MIB_OID))) &&
            (NULL == pTableName[i2Temp].pName))
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        if (STRLEN (ai1tempBuffer) + STRLEN (pdotPtr) < sizeof (ai1tempBuffer))
        {
            STRCAT ((INT1 *) ai1tempBuffer, (INT1 *) pdotPtr);
        }
    }
    else
    {                            /* is not alpha, so just copy into ai1tempBuffer */
        STRCPY ((INT1 *) ai1tempBuffer, (INT1 *) textStr);
    }
    /* Now we've got something
     * with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2dotCount = 0;
    for (i2Temp = 0; ((i2Temp < 257) &&
                      (ai1tempBuffer[i2Temp] != '\0')); i2Temp++)
    {
        if ('.' == ai1tempBuffer[i2Temp])
        {
            u2dotCount++;
        }
    }
    if (NULL == (poidPtr = alloc_oid ((INT4) (u2dotCount + 1))))
    {
        return (NULL);
    }
    poidPtr->u4_Length = (UINT4) (u2dotCount + 1);

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) ai1tempBuffer;
    for (i2Temp = 0; i2Temp < u2dotCount + 1; i2Temp++)
    {
        if (LaParseSubIdNew (&pu1TmpPtr, &(poidPtr->pu4_OidList[i2Temp]))
            == OSIX_FAILURE)
        {
            free_oid (poidPtr);
            return (NULL);
        }
        if ('.' == *pu1TmpPtr)
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (poidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (poidPtr);

}
#endif /*SNMP_2_WANTED */

/*****************************************************************************/
/* Function Name      : LaL2IwfIsLaConfigAllowed                             */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check port   */
/*                      can be configured with LAGG                          */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2NextPort - Next Interface Index                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfIsLaConfigAllowed (UINT4 u4IfIndex)
{
    INT4                i4RetVal = 0;

    i4RetVal = L2IwfIsLaConfigAllowed (u4IfIndex);

    if (i4RetVal == OSIX_FALSE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaL2IwfIsPortChannelConfigAllowed                    */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      configuration is allowed on the portchannel          */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu2NextPort - Next Interface Index                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfIsPortChannelConfigAllowed (UINT4 u4IfIndex)
{
    INT4                i4RetVal = 0;

    i4RetVal = L2IwfIsPortChannelConfigAllowed (u4IfIndex);

    if (i4RetVal == OSIX_FALSE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

#if (defined DLAG_WANTED) || (defined ICCH_WANTED)
/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetBridgeIndexFromIfIndex                */
/*                                                                           */
/* Description        : This function converts the CFA Index to              */
/*                      Bridge Index by referring Interface Map Table        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu4BrgIndex - pointer to Bridge Index                */
/*                                                                           */
/* Return Value(s)    : LA_FAILURE - On Failure                              */
/*                      LA_SUCCESS - On Success                              */
/*****************************************************************************/
INT4
LaActiveDLAGGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex)
{

#ifdef MBSM_WANTED
    if (MbsmGetBridgeIndexFromIfIndex (u4IfIndex, pu4BrgIndex) != MBSM_SUCCESS)
    {
        return LA_FAILURE;
    }
#else
    *pu4BrgIndex = u4IfIndex;
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetIfIndexFromBridgeIndex                */
/*                                                                           */
/* Description        : This function converts the Bridge Index to           */
/*                      CFA Index by referring CFA Interface Map Table       */
/*                                                                           */
/* Input(s)           : u4BrgIndex - Bridge index                            */
/*                                                                           */
/* Output(s)          : pu4IfIndex - Pointer to Interface Index              */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Succes                               */
/*                      KA_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
LaActiveDLAGGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex)
{
#ifdef MBSM_WANTED
    if (MbsmGetIfIndexFromBridgeIndex (u4BrgIndex, pu4IfIndex) != MBSM_SUCCESS)
    {
        return LA_FAILURE;
    }
#else
    *pu4IfIndex = u4BrgIndex;
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetSlotIdFromBridgeIndex                 */
/*                                                                           */
/* Description        : This function converts the Bridge Index to           */
/*                      CFA Index by referring CFA Interface Map Table       */
/*                                                                           */
/* Input(s)           : u4BrgIndex - Bridge index                            */
/*                                                                           */
/* Output(s)          : pu4SlotId - Pointer to Slot id                       */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Succes                               */
/*                      KA_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
LaActiveDLAGGetSlotIdFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4SlotId)
{
#ifdef MBSM_WANTED
    if (MbsmGetSlotFromPort (u4BrgIndex, (INT4 *) pu4SlotId) == MBSM_FAILURE)
    {
        return LA_FAILURE;

    }
#else
    INT4                i4SlotId = 0;
    INT4                i4PortId = 0;
    CfaGetSlotAndPortFromIfIndex (u4BrgIndex, &i4SlotId, &i4PortId);
    *(pu4SlotId) = (UINT4) i4SlotId;
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetRolePlayed                            */
/*                                                                           */
/* Description        : This function Gets the DLAG role to be played        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
LaActiveDLAGGetRolePlayed (VOID)
{
    return ((UINT1) CfaGetRetrieveNodeState ());
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGCheckIsLocalPort                         */
/*                                                                           */
/* Description        : This function checks if the port is local or Remote  */
/*                                                                           */
/* Input(s)           : u2PortNumber - Port Number                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_FAILURE- If port is remote port                   */
/*                      LA_SUCCESS- If port is Local port                    */
/*****************************************************************************/
INT4
LaActiveDLAGCheckIsLocalPort (UINT2 u2PortNumber)
{
#ifdef ICCH_WANTED
    if (u2PortNumber > SYS_DEF_MAX_INTERFACES)
    {
        return LA_FAILURE;
    }
    else
    {
        return LA_SUCCESS;
    }
#endif /* ICCH_WANTED */
#ifdef MBSM_WANTED
    UINT4               u4SwitchId = 0;
    UINT4               u4SlotId = 0xff;

    if (IssGetStackingModel () == ISS_DISS_STACKING_MODEL)
    {
        u4SwitchId = (UINT4) IssGetSwitchid ();

        MbsmGetSlotIdFromRemotePort ((UINT4) u2PortNumber, &u4SlotId);

        if (u4SwitchId != u4SlotId)
        {
            return LA_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u2PortNumber);
#endif
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetSwitchMacAddress                      */
/*                                                                           */
/* Description        : This function is to get Switch mac-address           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1HwAddr - Pointer to store switch Mac-address      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaActiveDLAGGetSwitchMacAddress (UINT1 *pu1HwAddr)
{
    LA_MEMCPY (pu1HwAddr, gLaGlobalInfo.LaSystem.SystemMacAddr,
               sizeof (tLaMacAddr));
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGValidateSourceMacAddress                 */
/*                                                                           */
/* Description        : This function is to validate source  mac-address     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1HwAddr - Pointer to Hw Mac-address                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaActiveDLAGValidateSourceMacAddress (UINT1 *pu1HwAddr)
{
    UNUSED_PARAM (pu1HwAddr);
    return LA_SUCCESS;
}
#endif /* DLAG_WANTED / ICCH_WANTED */
/*****************************************************************************/
/* Function Name      : LaGetActiveSlotNo                                    */
/*                                                                           */
/* Description        : This function is to get the active cards slot no     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pi4SlotNumber - Active slot number                   */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaGetActiveSlotNo (INT4 *pi4SlotNumber)
{
    UNUSED_PARAM (pi4SlotNumber);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaVlanUpdatePortIsolationForMcLag                    */
/*                                                                           */
/* Description        : This function calls the VLAN module to creates       */
/*                       port isolation for MC-LAG                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - MC-LAG interface index                   */
/*                      u1Action - Masks/Unmasks                             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaVlanUpdatePortIsolationForMcLag (UINT4 u4IfIndex, UINT1 u1Action)
{
#ifdef VLAN_WANTED
    if (VlanUpdatePortIsolationForMcLag (u4IfIndex, u1Action) == VLAN_FAILURE)
    {
        return LA_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Action);
#endif /* VLAN_WANTED */
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAstDisableStpOnPort                                */
/*                                                                           */
/* Description        : This function calls the AST module to disable STP on */
/*                      a given port                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index on which STP is to be    */
/*                      disabled.                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT1
LaAstDisableStpOnPort (UINT4 u4IfIndex)
{
#ifdef RSTP_WANTED
    if (AstDisableStpOnPort (u4IfIndex) == OSIX_FAILURE)
    {
        return LA_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaVlanUpdateMclagStatus                              */
/*                                                                           */
/* Description        : This function calls the VLAN module to update the    */
/*                      mclag status of vlan for IVR                         */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index of port-channel          */
/*                      u1MclagStatus  - Mclag status of the port - channel  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT1
LaVlanUpdateMclagStatus (UINT2 u2IfIndex, UINT1 u1MclagStatus)
{
#ifdef VLAN_WANTED
    if (VlanUpdateMclagStatus (u2IfIndex, u1MclagStatus) == OSIX_FAILURE)
    {
        return LA_FAILURE;
    }
#else
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (u1MclagStatus);
#endif
    return LA_SUCCESS;
}

#ifdef ICCH_WANTED
/*****************************************************************************/
/* Function Name      : LaIcchHandleProtocolEvent                            */
/*                                                                           */
/* Description        : This function calls the ICCH module to intimate about*/
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                    (ICCH_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                     ICCH_BULK_UPDT_ABORT /                */
/*                                     ICCH_SLAVE_EVT_PROCESSED              */
/*                                     ICCH_MASTER_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (ICCH_MEMALLOC_FAIL /     */
/*                                      ICCH_SENDTO_FAIL / ICCH_PROCESS_FAIL)*/
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
LaIcchHandleProtocolEvent (tIcchProtoEvt * pEvt)
{
    return (IcchApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : LaIcchSetBulkUpdatesStatus                           */
/*                                                                           */
/* Description        : This function calls the ICCH module to set the Status*/
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_FAILURE/ICCH_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaIcchSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (IcchSetBulkUpdatesStatus (u4AppId));
}

/*****************************************************************************/
/* Function Name      : LaIcchEnqMsgToIcchFromAppl                           */
/*                                                                           */
/* Description        : This function calls the ICCH Module to enqueue the   */
/*                      message from applications to ICCH task.              */
/*                      ICCH_MESSAGE, ICCH_BULK_REQ_MESSAGE,                 */
/*                      ICCH_BULK_TAIL_MESSAGE types are enqueued to ICCH    */
/*                                                                           */
/* Input(s)           : pIcchMsg - msg from appl.                            */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_FAILURE/ICCH_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
UINT4
LaIcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg, UINT2 u2DataLen,
                            UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
    return (IcchEnqMsgToIcchFromAppl
            (pIcchMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : LaIcchRegisterProtocols                              */
/*                                                                           */
/* Description        : This function calls the ICCH module to register.     */
/*                                                                           */
/* Input(s)           : tIcchRegParams - Reg. params to be provided by       */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaIcchRegisterProtocols (tIcchRegParams * pIcchReg)
{
    return (IcchRegisterProtocols (pIcchReg));
}

/*****************************************************************************/
/* Function Name      : LaIcchDeRegisterProtocols                            */
/*                                                                           */
/* Description        : This function calls the ICCH module to De-Register.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
LaIcchDeRegisterProtocols (tIcchRegParams * pIcchReg)
{
    return (IcchDeRegisterProtocols (pIcchReg->u4EntId));
}

/*****************************************************************************/
/* Function Name      : LaIcchGetRolePlayed                                  */
/*                                                                           */
/* Description        : This function calls the ICCH module to register.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Node state of the switch                             */
/*                                                                           */
/*****************************************************************************/
UINT4
LaIcchGetRolePlayed (VOID)
{
    return (IcchApiGetRolePlayed ());
}

/*****************************************************************************/
/* Function Name      : LaCfaCreateIcchIPInterface                           */
/*                                                                           */
/* Description        : This function calls the CFA Module to create         */
/*                      interfaces related to ICCH module                    */
/*                                                                           */
/* Input(s)           : u2InstanceId - ICCL instance ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS / CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaCfaCreateIcchIPInterface (UINT2 u2InstanceId)
{
    return (CfaCreateIcchIPInterface (u2InstanceId));
}

#endif

/*****************************************************************************/
/* Function Name      : LaIcchGetIcclIfIndex                                 */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL              */
/*                      interface index.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaIcchGetIcclIfIndex (UINT4 *pu4IcclIfIndex)
{
#ifdef ICCH_WANTED
    UINT4               u4IcclIfIndex = 0;

    IcchGetIcclIfIndex (&u4IcclIfIndex);
    *pu4IcclIfIndex = u4IcclIfIndex;
#else
    UNUSED_PARAM (pu4IcclIfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : LaIcchSetIcclIfIndex                                 */
/*                                                                           */
/* Description        : This function is used to set the ICCL                */
/*                      interface index.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaIcchSetIcclIfIndex (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    IcchSetIcclIfIndex (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : LaLLdpUpdateLLdpStatusOnIccl                         */
/*                                                                           */
/* Description        : This function is used to enables or disables LLDP    */
/*                      on ICCL port                                         */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index of port on which LLDP    */
/*                                  has to be disabled or enabled            */
/*                      u1AggCap  - Aggregation Capability                   */
/*                                  (TRUE indicates capable of being         */
/*                                  aggregated and FALSE indicates  not      */
/*                                  capable of being aggregated)             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/
UINT4
LaLLdpUpdateLLdpStatusOnIccl (UINT4 u4IfIndex, UINT1 u1AggCap)
{
#ifdef LLDP_WANTED
    if (OSIX_FAILURE == (LLdpApiUpdateLLdpStatusOnIccl (u4IfIndex, u1AggCap)))
    {
        return LA_FALSE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AggCap);
#endif
    return LA_TRUE;
}

/*****************************************************************************/
/* Function Name      : LaVlanIcchProcessMclagDown                           */
/*                                                                           */
/* Description        : This function calls the VLAN Module to  process      */
/*                      MC-LAG PortChannel down scenario in Master.          */
/*                                                                           */
/* Input(s)           : u4IfIndex - InterfaceIndex of the MC-LAG PortChannel.*/
/*                      u1Status - MCLAG PortChannel Status in Slave         */
/*                                 LA_PORT_DOWN/LA_PORT_UP_IN_BNDL           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/

VOID
LaVlanIcchProcessMclagDown (UINT4 u4IfIndex, UINT1 u1Status)
{
#ifdef ICCH_WANTED
    LA_UNLOCK ();
    VlanApiIcchProcessMclagDown (u4IfIndex, u1Status);
    LA_LOCK ();
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : LaVlanProcessMclagOperUp                             */
/*                                                                           */
/* Description        : This function calls the VLAN Module to  process      */
/*                      MC-LAG PortChannel OperUp Scenario.                  */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/

VOID
LaVlanProcessMclagOperUp (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    LA_UNLOCK ();
    VlanApiIcchProcessMclagOperUp (u4IfIndex);
    LA_LOCK ();
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : LaVlanIcchCheckPortStatus                            */
/*                                                                           */
/* Description        : This function calls the VLAN Module which inturn     */
/*                      sends a ICCH Get Port Status message to the Peer.    */
/*                      This should be hit from the Slave Node on MC-LAG     */
/*                      PortChannel down scenario.                           */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
LaVlanIcchCheckPortStatus (UINT4 u4IfIndex)
{
#ifdef ICCH_WANTED
    VlanApiIcchCheckPortStatus (u4IfIndex);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return;
}

/*****************************************************************************
*    Function Name         : LaCfaGetGlobalModTrc
*
*    Description           : This function is used to Get the Trace events from
*                                CFA module
*
*    Input(s)              : None
*
*    Output(s)             : gau4ModTrcEvents
*
*    Global Variables Referred : gau4ModTrcEvents
*
*    Returns               : gau4ModTrcEvents[0]
*******************************************************************************/
UINT4
LaCfaGetGlobalModTrc ()
{
#ifdef CFA_WANTED
    return CfaGetGlobalModTrc ();
#else
    return CONTROL_PLANE_TRC;
#endif
}

/*****************************************************************************/
/* Function Name      : LaSnoopNotifyCardRemovalEvent                        */
/*                                                                           */
/* Description        : This Function updates the Card removal event to SNOOP*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
VOID
LaSnoopNotifyCardRemovalEvent (VOID)
{
#ifdef IGS_WANTED
    SnoopNotifyCardRemovalEvent ();
#endif
}

/***********************************************************************
 *  Function Name   : LaIssRetrieveEnhancedLBStatus
 *  Description     : This function is used to obtain the port-channels
 *                    that have enhanced load balancing configured.
 *  Input(s)        : au4PortChannelList[], pu4NumOfPortChannels
 *  Output(s)       : None
 *  Returns         : None
 **********************************************************************/
VOID
LaIssRetrieveEnhancedLBStatus (UINT4 au4PortChannelList[],
                               UINT4 *pu4NumOfPortChannels)
{
    INT4                i4Index = 0;
    INT4                i4Key = 0;
    INT4                i4ChannelIfIndex = 0;
    INT4                i4PortSelPolicy = 0;

    LA_TRC (ENTRY_TRC, "Entry LaIssRetrieveEnhancedLBStatus called\r\n");

    LA_LOCK ();
    if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
    {
        /* no port-channels in the system */
        LA_UNLOCK ();
        LA_TRC (CONTROL_PLANE_TRC,
                "LaIssRetrieveEnhancedLBStatus: No port-channels configured\r\n");
        LA_TRC (EXIT_TRC, "Exit LaIssRetrieveEnhancedLBStatus called\r\n");
        return;
    }
    do
    {
        i4ChannelIfIndex = i4Index;
        if (nmhGetFsLaPortChannelSelectionPolicyBitList
            (i4ChannelIfIndex, &i4PortSelPolicy) == SNMP_SUCCESS)
        {
            if (i4PortSelPolicy == LA_SELECT_ENHANCED)
            {
                if (nmhGetDot3adAggActorAdminKey (i4ChannelIfIndex, &i4Key)
                    == SNMP_SUCCESS)
                {
                    au4PortChannelList[*pu4NumOfPortChannels] = (UINT4) i4Key;
                    *pu4NumOfPortChannels = ((*pu4NumOfPortChannels) + 1);
                }
            }
        }
    }
    while (nmhGetNextIndexDot3adAggTable (i4ChannelIfIndex, &i4Index)
           != SNMP_FAILURE);

    LA_UNLOCK ();
    LA_TRC (EXIT_TRC, "Exit LaIssRetrieveEnhancedLBStatus called\r\n");
    return;
}
