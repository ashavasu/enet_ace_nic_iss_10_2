/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lasys.c,v 1.158.2.3 2018/03/23 13:23:21 siva Exp $
 *
 * Description: This file contains the init, deinit functions for
 *              Link Aggregation module.
 *
 *******************************************************************/

#ifndef _LASYS_C
#define _LASYS_C
#include "lahdrs.h"
#include "mux.h"
extern UINT4        FsLaActorSystemID[10];

/*****************************************************************************/
/* Function Name      : LaDefaultSettings                                    */
/*                                                                           */
/* Description        : This function is called from LaInit() to set the     */
/*                      default initial values in the global info structure. */
/*                                                                           */
/* Input(s)           : Pointer to the Global Info structure                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : All values in the global structure are initialised.  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All values initialised.                              */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - on receipt of NULL global pointer. */
/*****************************************************************************/
INT4
LaDefaultSettings (VOID)
{
    tLaSystem          *pLaSystem;
    tLaSystem          *pLaPartnerSystem;
    tLaLacPortState    *pLaPartnerDefaultState;
    tLaLacPortState    *pLaPortDefaultState;
    tLaGlobalInfo      *pgLaGlobalInfo;

    pgLaGlobalInfo = &gLaGlobalInfo;

    /* Initialise the default System Id */
    pLaSystem = &(pgLaGlobalInfo->LaSystem);
    pLaSystem->u2SystemPriority = LA_DEFAULT_SYSTEM_PRIORITY;

    /* Initialise the default Partner System Id */
    pLaPartnerSystem = &(pgLaGlobalInfo->LaPartnerSystem);
    pLaPartnerSystem->u2SystemPriority = LA_DEFAULT_PARTNER_SYSTEM_PRIORITY;

    /* Initialise Partner Port States with default port states and expecting     
     * best from the Partner i.e. both collecting and distributing are enabled 
     * for partner
     */
    pLaPartnerDefaultState = &(pgLaGlobalInfo->LaPartnerDefaultState);

    pLaPartnerDefaultState->LaLacpActivity = LA_DEFAULT_PARTNER_LACP_ACTIVITY;
    pLaPartnerDefaultState->LaLacpTimeout = LA_DEFAULT_PARTNER_LACP_TIMEOUT;
    /* Each Partner is brought up as aggregatable */
    pLaPartnerDefaultState->LaAggregation = LA_DEFAULT_PARTNER_AGGREGATION;
    pLaPartnerDefaultState->LaSynchronization = LA_TRUE;
    pLaPartnerDefaultState->LaCollecting = LA_TRUE;
    pLaPartnerDefaultState->LaDistributing = LA_TRUE;

    pLaPartnerDefaultState->LaDefaulted = LA_TRUE;
    pLaPartnerDefaultState->LaExpired = LA_FALSE;

    /* Initialise Actor Port states with default port states */
    pLaPortDefaultState = &(pgLaGlobalInfo->LaPortDefaultState);

    pLaPortDefaultState->LaLacpActivity = LA_DEFAULT_LACP_ACTIVITY;
    pLaPortDefaultState->LaLacpTimeout = LA_DEFAULT_LACP_TIMEOUT;
    /* Each Actor is brought up as Individual */
    pLaPortDefaultState->LaAggregation = LA_DEFAULT_AGGREGATION;
    pLaPortDefaultState->LaSynchronization = LA_FALSE;
    pLaPortDefaultState->LaCollecting = LA_FALSE;
    pLaPortDefaultState->LaDistributing = LA_FALSE;
    pLaPortDefaultState->LaDefaulted = LA_TRUE;
    pLaPortDefaultState->LaExpired = LA_FALSE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleInitFailure.                                 */
/*                                                                           */
/* Description        : This function is called when the initialization of   */
/*                      Link Aggregation fails at any point during           */
/*                      initialization.                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : All memory pools.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All memory pools deleted.                            */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaHandleInitFailure (VOID)
{
    tLaGlobalInfo      *pgLaGlobalInfo;
    tLaIntfMesg        *pIntfMesg;
    tLaCtrlMesg        *pMesg;

    pgLaGlobalInfo = &gLaGlobalInfo;

    while (LA_DEQUEUE_MESSAGES (LA_CTRLQ_ID, &pMesg) == LA_OSIX_SUCCESS)
    {

        if (pMesg == NULL)
            continue;

        /* Release the buffer to pool */

        LA_FREE_MEM_BLOCK (LA_CTRLMESG_POOL_ID, (UINT1 *) pMesg);
    }

    LA_DELETE_QUEUE (LA_CTRLQ_ID);

    while (LA_DEQUEUE_MESSAGES (LA_INTFQ_ID, &pIntfMesg) == LA_OSIX_SUCCESS)
    {
        if (pIntfMesg == NULL)
            continue;

        /* Release the buffer to pool */

        LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pIntfMesg);
    }

    LA_DELETE_QUEUE (LA_INTFQ_ID);

    /* Deleting LA memory pools */
    LaSizingMemDeleteMemPools ();

#if defined (DLAG_WANTED) || defined (ICCH_WANTED)
    /* Deleting DLAG memory pools */
    LaDlagSizingMemDeleteMemPools ();
#endif

    if (pgLaGlobalInfo->pLaCacheTable != NULL)
    {
        LA_DELETE_CACHE_HASH_TABLE (pgLaGlobalInfo->pLaCacheTable,
                                    LaReleaseCacheMemBlock);
    }

    if (pgLaGlobalInfo->LaTmrListId != LA_INVALID_VAL)
    {
        LaTimerDeInit ();
    }

    if (gLaProtocolSemId != LA_INVALID_VAL)
    {
        LA_DELETE_SEMAPHORE (gLaProtocolSemId);

    }
}

/*****************************************************************************/
/* Function Name      : LaHandleInit                                         */
/*                                                                           */
/* Description        : This function allocates memory pools for all tables  */
/*                      in the Link Aggregation module. It initialises the   */
/*                      LA timer task and timer queue. It also initialises   */
/*                      the GLobal structure gLaGLobalInfo.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All  variables in the GLobal structure are           */
/*                      initialised.                                         */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
LaHandleInit (VOID)
{
    UINT2               u2PortIndex = 0;
    UINT2               u2PrevPort = 0;
    tLaBoolean          bProtoLockVal = LA_FALSE;

    /* Store the Protocol Lock value */
    bProtoLockVal = gLaGlobalInfo.bProtocolLocked;

    MEMSET (&gLaGlobalInfo, 0, sizeof (gLaGlobalInfo));

    /* Restore the Protocol Lock value */
    gLaGlobalInfo.bProtocolLocked = bProtoLockVal;

    LA_SYSTEM_CONTROL = LA_SHUTDOWN;
    LA_MODULE_STATUS = LA_DISABLED;

#ifdef L2RED_WANTED
    LA_MEMSET (&gLaRedGlobalInfo, 0, sizeof (tLaRedGlobalInfo));
    gLaRedGlobalInfo.u2BulkUpdNextPort = LA_MIN_PORTS;
    LA_BULK_REQ_RECD () = LA_FALSE;
    LA_NODE_STATUS () = LA_NODE_IDLE;
    LA_NUM_STANDBY_NODES () = 0;
#else
    LA_RM_GET_NODE_STATUS ();
#endif
    LA_RM_GET_NUM_STANDBY_NODES_UP ();

    /* By default the LA is disabled, and hence the default protocol
     * admin status is also disabled. */
    LA_INIT_PROTOCOL_ADMIN_STATUS ();
    /* Initialize the Recovery triggered Time and count  */
    gu4RecoveryTime = LA_DEFAULT_RECOVERY_TIME;
    gu4RecThreshold = LA_DEFAULT_REC_THRESHOLD;
    gu4RecTrgdCount = 0;
    gu4DefaultedStateThreshold = LA_DEF_STATE_DEFAULT_THRESHOLD;
    gu4SameStateRecThreshold = LA_DEFAULT_REC_THRESHOLD;
    gu4HardwareFailureRecThreshold = LA_DEFAULT_REC_THRESHOLD;

    /* Assign Trace Option */
    gLaGlobalInfo.u4TraceOption = 0;

    /* Creating LA memory pools */
    if (LaSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LaHandleInit: LA creation of Mempools Failed \r\n");

        return LA_FAILURE;
    }
#if defined (DLAG_WANTED) || defined (ICCH_WANTED)
    /* Creating DLAG memory pools */
    if (LaDlagSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LaHandleInit: DLAG creation of Mempools Failed \r\n");

        return LA_FAILURE;
    }
#endif
    LaAssignMempoolIds ();
    /* Creation of Cache Hash Table */
    gLaGlobalInfo.pLaCacheTable =
        LA_CREATE_CACHE_HASH_TABLE (LA_CACHE_TABLE_SIZE, NULL, LA_FALSE);

    if (gLaGlobalInfo.pLaCacheTable == NULL)
    {

        LA_TRC (INIT_SHUT_TRC, "Cache Hash Table Creation FAILED\n");
        return LA_FAILURE;
    }

    /* Timer Initialization */
    if (LaTimerInit () != LA_SUCCESS)
    {

        LA_TRC (INIT_SHUT_TRC, "LaTimerInit() FAILED\n");
        return LA_FAILURE;
    }

    /* Initialise with default settings for Actor, Partner and Aggregator */
    LaDefaultSettings ();
    LA_FREE_AGGS = LA_MAX_AGG;
    LA_SLL_INIT (LA_AGG_SLL);
    UTL_SLL_Init (&(gLaGlobalInfo.LaDynReallocPortList),
                  FSAP_OFFSETOF (tLaLacPortEntry, NextDefPortEntry));
    LaLacMuxMachineInit ();

    /* LA has been successfully initialised. Make LaSystemControl to START */
    LA_SYSTEM_CONTROL = LA_START;
#ifdef NPAPI_WANTED
    /* If La is shutdown and started on an active node, then hardware
     * shoule be initialized. */
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (FNP_FAILURE == LaFsLaHwInit ())
        {
            LA_TRC (INIT_SHUT_TRC, "FsLaHwInit() FAILED\n");
            return LA_FAILURE;
        }
    }
#endif

    while (LaL2IwfGetNextValidPort (u2PrevPort, &u2PortIndex) == L2IWF_SUCCESS)
    {
        if (LaHandleCreatePort (u2PortIndex) != MST_SUCCESS)
        {
            LA_TRC_ARG1 (INIT_SHUT_TRC,
                         "Port: %u :: Port Entry Creation FAILED\n ",
                         u2PortIndex);
        }
        /* Oper status will be obtained at the time of Module Enable */
        u2PrevPort = u2PortIndex;
    }

    LA_SET_INITIALIZED ();
    /* Register with RM module, to get event and messages from RM. */
    if (LaRedRegisterWithRM () != LA_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC, "LaRedRegisterWithRM() FAILED\n");
        return LA_FAILURE;
    }
#ifdef ICCH_WANTED
    /* Register with ICCH module, to get event and messages from ICCH. */
    if (LaIcchRegisterWithICCH () != LA_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC, "LaIcchRegisterWithICCH() FAILED\n");
        return LA_FAILURE;
    }

#endif /*ICCH_WANTED */
    if (LaRegisterWithPacketHandler () != LA_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC, "LaRegisterWithPacketHandler() FAILED\n");
        return LA_FAILURE;
    }

    /* Initialize Active-Active DLAG params */
    LaActiveDLAGInitParams ();
#ifdef ICCH_WANTED

    /* LaSetIcclProperties will create the ICCL port-channel in CFA, LA and Other modules.
       This will again try to take LaLock which acquired before in CLI/SNMP flow. So the 
       Lock is taken based on the previous Lock acquired status. 
     */
    if (gLaGlobalInfo.bProtocolLocked == LA_TRUE)
    {
        LaUnLock ();
        LaSetIcclProperties ();
        LaLock ();
    }
    LaActiveMCLAGInitParams ();
#endif

    LA_TRC (CONTROL_PLANE_TRC, "SYS: LINK AGGREGATION MODULE INITIALISED\n");
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleShutDown                                     */
/*                                                                           */
/* Description        : This function is called during system shut down or   */
/*                      when the manager wants to shut the Link Aggregation  */
/*                      module down.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : All memory pools.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All memory pools released.                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaHandleShutDown (VOID)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaIntfMesg        *pMesg = NULL;
    tLaIntfMesg        *pIntfMesg = NULL;
    UINT2               u2PortIndex;
#ifdef ICCH_WANTED
    UINT4               u4InstanceId = 0;
#endif

    /* If the LA module is shut down return FAILURE */
    if (LA_SYSTEM_CONTROL != LA_START)
    {
        LA_TRC (INIT_SHUT_TRC, "System is not running. Cannot Shut down.\n");
        return LA_SUCCESS;
    }
#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (LA_RED_AUDIT_TSKID () != 0)
    {
        LA_DELETE_TASK (LA_RED_AUDIT_TSKID ());
        LA_RED_AUDIT_TSKID () = 0;
    }
    LA_RED_IS_AUDIT_STOP () = LA_TRUE;
#endif
#endif
    LA_INITIALIZED () = FALSE;
    while (LA_DEQUEUE_MESSAGES (LA_CTRLQ_ID, &pMesg) == LA_OSIX_SUCCESS)
    {
        if (pMesg == NULL)
            continue;

        /* Release the buffer to pool */

        LA_FREE_MEM_BLOCK (LA_CTRLMESG_POOL_ID, (UINT1 *) pMesg);
    }

    while (LA_DEQUEUE_MESSAGES (LA_INTFQ_ID, &pIntfMesg) == LA_OSIX_SUCCESS)
    {
        if (pIntfMesg == NULL)
            continue;

        /* Release the buffer to pool */

        LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pIntfMesg);
    }

    /* Disable the LA Module */
    LaDisable ();

    LA_SYSTEM_CONTROL = LA_SHUTDOWN;

    LaDeleteAllAggEntries ();

    /* Delete the aggregator entry memory pool */
    if (LA_DELETE_MEM_POOL (LA_AGGENTRY_POOL_ID) != LA_MEM_SUCCESS)
    {

        LA_TRC (INIT_SHUT_TRC, "Agg Entry Memory Pool Release FAILED\n");
        return LA_FAILURE;
    }

    LA_AGGENTRY_POOL_ID = 0;

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {

        LaGetPortEntry (u2PortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }

        /* Delete the Port Entry pointer from the array  of Port entry 
         * pointers in the global info */
        LaDeleteFromPortTable (pPortEntry);
        if (pPortEntry->pLaMarkerEntry != NULL)
        {
            if (LA_MARKERENTRY_FREE_MEM_BLOCK (pPortEntry->pLaMarkerEntry) !=
                LA_MEM_SUCCESS)
            {
                LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "MARK: Marker Entry Memory Block Release FAILED\n");
            }
            pPortEntry->pLaMarkerEntry = NULL;
        }

        LA_PORTENTRY_FREE_MEM_BLOCK (pPortEntry);

    }                            /* End of FOR LOOP */

    /* Delete the Cache Table */
    LA_DELETE_CACHE_HASH_TABLE (gLaGlobalInfo.pLaCacheTable,
                                LaReleaseCacheMemBlock);
    gLaGlobalInfo.pLaCacheTable = NULL;

    /* Stop the timer task and delete the timer list and the timer task */
    if (LaTimerDeInit () != LA_SUCCESS)
    {

        LA_TRC (INIT_SHUT_TRC, "LaTmrDeinit() FAILED\n");
    }

    /* Delete the Port Entry Mem pool */
    if (LA_DELETE_MEM_POOL (LA_PORTENTRY_POOL_ID) != LA_MEM_SUCCESS)
    {

        LA_TRC (INIT_SHUT_TRC, "Port Entry Memory Pool Release FAILED\n");
    }
    LA_PORTENTRY_POOL_ID = 0;

    /* Delete the Cache Entry Mem Pool */
    if (LA_DELETE_MEM_POOL (LA_CACHEENTRY_POOL_ID) != LA_MEM_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC, "Cache  Entry Memory Pool Release FAILED\n");
    }
    LA_CACHEENTRY_POOL_ID = 0;

    /* Delete the Marker Entry Mem Pool */
    if (LA_DELETE_MEM_POOL (LA_MARKERENTRY_POOL_ID) != LA_MEM_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC, "Marker Entry Memory Pool Release FAILED\n");
    }
    LA_MARKERENTRY_POOL_ID = 0;

    if (LA_DELETE_MEM_POOL (LA_DLAGCONSPORTENTRY_POOL_ID) != LA_MEM_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC,
                "Consolidated Port Entry Memory Pool Release FAILED\n");
    }
    LA_DLAGCONSPORTENTRY_POOL_ID = 0;

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (FNP_FAILURE == LaFsLaHwDeInit ())
        {
            LA_TRC (INIT_SHUT_TRC, "Hardware DeInitialisation of LA FAILED\n");
        }
    }
#endif
#ifdef L2RED_WANTED
    LA_MEMSET (&gLaRedGlobalInfo, 0, sizeof (tLaRedGlobalInfo));
    gLaRedGlobalInfo.u2BulkUpdNextPort = LA_MIN_PORTS;
    LA_NODE_STATUS () = LA_NODE_IDLE;
    LA_BULK_REQ_RECD () = LA_FALSE;
    LA_NUM_STANDBY_NODES () = 0;
    /* HITLESS RESTART */
    LA_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
#endif
    LaRedDeRegisterWithRM ();

    /* Initialize active-active DLAG params */
    LaActiveDLAGInitParams ();
#ifdef ICCH_WANTED
    LaActiveMCLAGInitParams ();
#endif

#ifdef ICCH_WANTED

    /*When LA is shutdown MC-LAG should also be shutdown */
    gLaMclagSystemControl = LA_SHUTDOWN;
    if (LaNoMclag (u4InstanceId) == LA_FAILURE)
    {
        LA_TRC (INIT_SHUT_TRC, "MC-LAG shutdown failed\r\n");
    }

    /* De-Register with ICCH module since LA is going Shut Down */
    if (LaIcchDeRegisterWithICCH () != LA_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC, "LaIcchDeRegisterWithICCH() FAILED\n");
        return LA_FAILURE;
    }

#endif /*ICCH_WANTED */

    LA_TRC (INIT_SHUT_TRC, "SYS: LINK AGGREGATION MODULE SHUT DOWN\n");

    /* Deregister with packet handler */
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_LA_MARKER);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_LA_LACP);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaInitialisePortEntry                                */
/*                                                                           */
/* Description        : This function initialises the default Actor and      */
/*                      Partner and Port variables in the Link Aggregation   */
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry *pPortEntry                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaInitialisePortEntry (tLaLacPortEntry * pPortEntry)
{

    tLaLacIfInfo       *pActorAdminInfo = NULL;
    tLaLacIfInfo       *pPartnerAdminInfo = NULL;

    /* Initialise the pointer to next node  to NULL 
     * NOTE: TMO_SLL_Node just contains the pointer to the next node and is the
     * first field in LaLacPortEntry. Since, pPortEntry points to a port 
     * entry, it actually points to TMO_SLL_node, the first field in the 
     * structure. So,a typecast of the pointer enables the access of 
     * TMO_SLL_Node which contains a pointer to the next Port entry
     */
    LA_SLL_INIT_NODE (&pPortEntry->NextPortEntry);
    LA_SLL_INIT_NODE (&pPortEntry->NextDefPortEntry);

    /* Initialise  Actor Admin information */
    pActorAdminInfo = &(pPortEntry->LaLacActorAdminInfo);

    pActorAdminInfo->u2IfIndex = pPortEntry->u2PortIndex;
    pActorAdminInfo->LaSystem = gLaGlobalInfo.LaSystem;
    pActorAdminInfo->u2IfKey = 0;
    pActorAdminInfo->u2IfPriority = LA_DEFAULT_PORT_PRIORITY;
    pActorAdminInfo->LaLacPortState = gLaGlobalInfo.LaPortDefaultState;

    /*Initialise the Actor Oper Info to be same as Actor Admin Info */
    pPortEntry->LaLacActorInfo = pPortEntry->LaLacActorAdminInfo;

    /* Initialise partner admin information */
    pPartnerAdminInfo = &(pPortEntry->LaLacPartnerAdminInfo);

    pPartnerAdminInfo->LaSystem = gLaGlobalInfo.LaPartnerSystem;
    pPartnerAdminInfo->u2IfKey = 0x0;
    pPartnerAdminInfo->u2IfIndex = 0x0;
    pPartnerAdminInfo->u2IfPriority = LA_DEFAULT_PARTNER_PORT_PRIORITY;
    pPartnerAdminInfo->LaLacPortState = gLaGlobalInfo.LaPartnerDefaultState;

    /*Initialise the Partner Oper Info to be same as Partner Admin Info */
    pPortEntry->LaLacPartnerInfo = pPortEntry->LaLacPartnerAdminInfo;

    pPortEntry->pAggEntry = NULL;

    pPortEntry->LaRxmState = LA_RXM_STATE_NULL;
    pPortEntry->LaMuxState = LA_MUX_STATE_DETACHED;
    pPortEntry->PTxSmState = LA_PTXM_STATE_NO_PERIODIC;

    pPortEntry->NttFlag = LA_FALSE;
    pPortEntry->AggSelected = LA_UNSELECTED;
    pPortEntry->LaAggAttached = LA_FALSE;
    pPortEntry->OperIndividual = LA_FALSE;
    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

    /* LACP is disabled when a port comes up */
    pPortEntry->LaLacpMode = LA_MODE_DISABLED;
    pPortEntry->LaPortEnabled = LA_PORT_DISABLED;
    pPortEntry->LaLacpEnabled = LA_LACP_DISABLED;

    pPortEntry->u4PeriodicTime = LA_DEFAULT_PERIODIC_TIME;
    pPortEntry->u4WaitWhileTime = LA_DEFAULT_WAIT_WHILE_TIME;
    pPortEntry->u4HoldWhileTime = LA_DEFAULT_HOLD_WHILE_TIME;
    pPortEntry->u4CurrentWhileTime = LA_DEFAULT_CURRENT_WHILE_TIME;

    pPortEntry->LaTickTimer.LaTmrFlag = LA_TMR_STOPPED;
    pPortEntry->PeriodicTmr.LaTmrFlag = LA_TMR_STOPPED;
    pPortEntry->WaitWhileTmr.LaTmrFlag = LA_TMR_STOPPED;
    pPortEntry->HoldWhileTmr.LaTmrFlag = LA_TMR_STOPPED;
    pPortEntry->CurrentWhileTmr.LaTmrFlag = LA_TMR_STOPPED;
    pPortEntry->RecoveryTmr.LaTmrFlag = LA_TMR_STOPPED;

    pPortEntry->LaTickTimer.pEntry = (VOID *) pPortEntry;
    pPortEntry->PeriodicTmr.pEntry = (VOID *) pPortEntry;
    pPortEntry->WaitWhileTmr.pEntry = (VOID *) pPortEntry;
    pPortEntry->HoldWhileTmr.pEntry = (VOID *) pPortEntry;
    pPortEntry->CurrentWhileTmr.pEntry = (VOID *) pPortEntry;
    pPortEntry->RecoveryTmr.pEntry = (VOID *) pPortEntry;

    pPortEntry->LaTickTimer.LaAppTimer.u4Data = LA_TICK_TMR_TYPE;
    pPortEntry->PeriodicTmr.LaAppTimer.u4Data = LA_PERIODIC_TMR_TYPE;
    pPortEntry->WaitWhileTmr.LaAppTimer.u4Data = LA_WAIT_WHILE_TMR_TYPE;
    pPortEntry->HoldWhileTmr.LaAppTimer.u4Data = LA_HOLD_WHILE_TMR_TYPE;
    pPortEntry->CurrentWhileTmr.LaAppTimer.u4Data = LA_CURRENT_WHILE_TMR_TYPE;
    pPortEntry->RecoveryTmr.LaAppTimer.u4Data = LA_RECOVERY_TMR_TYPE;

    pPortEntry->pLaMarkerEntry = NULL;

    pPortEntry->u4HoldCount = 0;
    pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
    pPortEntry->u4SameStateCount = 0;
    pPortEntry->u4ErrorDetectCount = 0;
    pPortEntry->u4RecTrgdCount = 0;
    pPortEntry->u4DefaultedStateThreshold = LA_DEF_STATE_DEFAULT_THRESHOLD;
    pPortEntry->u4DefaultedStateRecTrgrdCount = 0;
    pPortEntry->u4HardwareFailureRecThreshold = LA_DEFAULT_REC_THRESHOLD;
    pPortEntry->u4HardwareFailureRecTrgrdCount = 0;
    pPortEntry->u4SameStateRecThreshold = LA_DEFAULT_REC_THRESHOLD;
    pPortEntry->u1InPassiveDefState = LA_FALSE;
    return;
}

/*****************************************************************************/
/* Function Name      : LaHandleCreatePort                                   */
/*                                                                           */
/* Description        : This function  allocates memory blocks               */
/*                      for the port, its default aggregtor's config entry   */
/*                      and interface entry.                                 */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_INVALID_VALUE - when invalid values are passed*/
/*                      LA_ERR_MEM_FAILURE - On memory allocation failure.   */
/*****************************************************************************/
INT4
LaHandleCreatePort (UINT2 u2PortIndex)
{

    tCfaIfInfo          IfInfo;
    tLaLacPortEntry    *pPortEntry = NULL;

    LA_MEMSET (&IfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    if (LA_SYSTEM_CONTROL != LA_START)
    {
        LA_TRC (INIT_SHUT_TRC, "LA module is shut down. "
                " Cannot create ports\n");
        return LA_FAILURE;
    }

    if ((u2PortIndex < LA_MIN_PORTS) || (u2PortIndex > LA_MAX_PORTS))
    {

        LA_TRC (INIT_SHUT_TRC, "Invalid Port Index value\n");
        return LA_ERR_INVALID_VALUE;
    }
    if ((LaCfaGetIfInfo (u2PortIndex, &IfInfo) != CFA_SUCCESS) &&
        (IfInfo.u1BridgedIface == CFA_DISABLED))
    {
        return LA_FAILURE;
    }

    if (IfInfo.u1IfType != CFA_ENET)
    {
        return LA_SUCCESS;
    }

    /* Check if an entry for port index exists */
    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry != NULL)
    {
        LA_TRC (INIT_SHUT_TRC,
                "Port with same Index EXISTS Another Entry with"
                " same Index cannot be created\n");
        return LA_FAILURE;
    }

    /* Allocate a block for the port entry */
    if (LA_PORTENTRY_ALLOC_MEM_BLOCK (pPortEntry) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "LA_PORTENTRY_ALLOC_MEM_BLOCK FAILED\n");
        return LA_ERR_MEM_FAILURE;
    }

    /* Memset the port entry to zero */
    LA_MEMSET (pPortEntry, 0, sizeof (tLaLacPortEntry));

    pPortEntry->u2PortIndex = u2PortIndex;

    LaInitialisePortEntry (pPortEntry);
    /* Copy the Port Address from the CfaIfInfo in to the Port Entry */

    /* Link State updation should be got from CFA - Link Speed and 
     * Half/Full Duplex */
    pPortEntry->u4LinkSpeed = IfInfo.u4IfSpeed;
    pPortEntry->u4LinkHighSpeed = IfInfo.u4IfHighSpeed;
    pPortEntry->u4LinkDuplex = IfInfo.u1DuplexStatus;
    pPortEntry->u4Mtu = IfInfo.u4IfMtu;
    pPortEntry->u4RestoreMtu = IfInfo.u4IfMtu;

    pPortEntry->i4PauseAdminMode = (INT4) IfInfo.u1PauseAdminNode;
    pPortEntry->i4RestorePauseAdminMode = (INT4) IfInfo.u1PauseAdminNode;
    LA_MEMCPY (pPortEntry->au1MacAddr, IfInfo.au1MacAddr, LA_MAC_ADDRESS_SIZE);

    /* Enter the Port Entry in the port table. */
    LaAddToPortTable (pPortEntry);

    LaL2IwfGetPortOperStatus (LA_MODULE, u2PortIndex,
                              &pPortEntry->u1PortOperStatus);
    /* Set the aggregator operational status to down. It will be made up
     * when the port is enabled for lacp or manual aggregation and port
     * is operational either in the bundle or as a individual port
     */
    pPortEntry->u1AggOperStatus = (UINT1) CFA_IF_DOWN;

    /* Initialize the Active-Active DLAG paramters */

    pPortEntry->u1DLAGPeerReady = LA_FALSE;
    pPortEntry->u1DLAGMasterAck = LA_FALSE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  LaInitPort.                                         */
/*                                                                           */
/* Description        :  This function is called from LaCreatePort() to      */
/*                       initialise the state machines and start the Tick    */
/*                       Timer.                                              */
/*                                                                           */
/* Input(s)           :  pPortEntry.                                         */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :  None                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :  None                                                */
/*                                                                           */
/* Return Value(s)    :  None                                                */
/*****************************************************************************/
VOID
LaInitPort (tLaLacPortEntry * pPortEntry)
{
    /* Initialise the variables for LACP state machines */
    pPortEntry->LaPortEnabled = LA_PORT_DISABLED;

    pPortEntry->LaLacpEnabled = LA_LACP_DISABLED;

    /* Initialise the Tick timer for the port */
    pPortEntry->LaTickTimer.LaAppTimer.u4Data = LA_TICK_TMR_TYPE;
    pPortEntry->LaTickTimer.pEntry = (VOID *) pPortEntry;
    pPortEntry->LaTickTimer.LaTmrFlag = LA_TMR_STOPPED;

    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_PORT_INIT);

    LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_PORT_INIT);

    LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_PORT_INIT);

    LaLacRxMachine (pPortEntry, LA_RXM_EVENT_PORT_INIT, NULL);

    pPortEntry->u2SyncUp = LA_FALSE;

    /* Start the LaTickTimer */
    if (LaStartTimer (&(pPortEntry->LaTickTimer)) == LA_FAILURE)
    {
        return;
    }

}

/*****************************************************************************/
/* Function Name      : LaHandleEnablePort.                                  */
/*                                                                           */
/* Description        : This function is called to enable the state machines */
/*                      for a port.                                          */
/*                                                                           */
/* Input(s)           : Port Entry Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaHandleEnablePort (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tCfaIfInfo          CfaIfInfo;
    tMacAddr            MacAddr = { 0 };

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));
    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaLacpActivity ==
         LA_PASSIVE))
    {
        LaInitPort (pPortEntry);
    }

    if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "Retrieval of port %d properties failed!\n",
                     pPortEntry->u2PortIndex);
        return;
    }

    pPortEntry->u4LinkSpeed = CfaIfInfo.u4IfSpeed;
    pPortEntry->u4LinkHighSpeed = CfaIfInfo.u4IfHighSpeed;
    pPortEntry->u4LinkDuplex = CfaIfInfo.u1DuplexStatus;
    pPortEntry->u4Mtu = CfaIfInfo.u4IfMtu;
    pPortEntry->i4PauseAdminMode = (INT4) CfaIfInfo.u1PauseAdminNode;
    pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;

    /* Change the LaLacpEnabled based on LacpMode since the
     * LaLacpEnabled can change if the link goes dynamically 
     * from Full-dup to half-dup and back.
     */
    if (pPortEntry->u4LinkDuplex == ETH_STATS_DUP_ST_HALFDUP)
    {
        pPortEntry->LaLacpEnabled = LA_LACP_DISABLED;
    }
    else
    {
        pPortEntry->LaLacpEnabled = LA_LACP_ENABLED;
    }

    /* Copy the configured aggregation capability to the 
     * operational value.
     */
    pPortEntry->LaLacActorInfo.LaLacPortState.LaAggregation =
        pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaAggregation;

    pPortEntry->AggSelected = LA_UNSELECTED;

    /* Initialise the variables for LACP state machines */
    pPortEntry->LaPortEnabled = LA_PORT_ENABLED;

    /* Call the state machines */
    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_PORT_ENABLED);
    LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);
    LaLacRxMachine (pPortEntry, LA_RXM_EVENT_PORT_ENABLED, NULL);

    /* when Port is attached or enabled in an aggregator 
     * If Only D-LAG status is enabled in Aggregator:-
     * Then when a port is attached/enabled in an aggregator, then low-priority
     * event-update message must be sent to notify the remote D-LAG node about
     * this event, so that remote D-LAG node can update the status of the port
     * if already present or create an entry if new entry.
     * 
     * If D-LAG Redundancy feature is also enabled in Aggregator:-
     * Then When a port is attached to/made up in master node, then role-played
     * by the D-LAG nodes will not be affected and sending low-priority event-
     * update message to update remote D-LAG node would be sufficient.
     * But port is attached to/made up in the Slave Node then it does make
     * the difference as the master-slave-selection is based on the number of
     * ports in-sync in an aggregator, so Master-slave-selection process
     * must be triggered, but if the Node where this event has happened
     * is itself the elector then Master-slave-selection algorithm should be
     * immediately triggered immediately.
     * if the Node where this event has occured is not the designated elector
     * then high-priority event-update pdu must be sent to initiate the 
     * master slave selection in the elector. */
    pAggEntry = pPortEntry->pAggEntry;
    if (((pAggEntry) && (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)) ||
        (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
    {
        if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                             MacAddr,
                                             LA_FALSE,
                                             LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);

            LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_INTF_UP;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
        else
        {
            LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
    }
}

/*****************************************************************************/
/* Function Name      : LaEnable.                                            */
/*                                                                           */
/* Description        : This function is called to enable the LA module      */
/*                      from the disabled state.                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaEnable (VOID)
{
    tCfaIfInfo          CfaIfInfo;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex;
    UINT1               u1OperStatus;
    tLaLacAggEntry     *pAggEntry = NULL;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT1               au1LocalMac[LA_MAC_ADDRESS_SIZE];
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = LA_INVALID_HW_AGG_IDX;
#endif

    LA_MEMSET (au1LocalMac, 0, LA_MAC_ADDRESS_SIZE);
    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
#ifdef SNMP_2_WANTED
    LA_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif

    LA_TRC (INIT_SHUT_TRC, "SYS: Enabling LA Module ... \n");

    if (LA_MODULE_STATUS == LA_ENABLED)
    {
        return LA_SUCCESS;
    }

    LA_PROTOCOL_ADMIN_STATUS () = LA_ENABLED;

    /* 
     * Configurations are restored before GO_ACTIVE/GO_STANDBY event is
     * received by protocol.Hence if LA enable is called before GO_ACTIVE/
     * GO_STANDBY event is received then store that in LaAdminStatus. In
     * LaRedMakeNodeActive based on the LaAdminstaus enable/disable LA.
     */
    if (LA_NODE_STATUS () == LA_NODE_IDLE)
    {
        return LA_SUCCESS;
    }

    LA_MODULE_STATUS = LA_ENABLED;

    if (LA_MEMCMP (&gLaGlobalInfo.LaSystem.SystemMacAddr,
                   &au1LocalMac, LA_MAC_ADDRESS_SIZE) == 0)
    {
        /* Make the MAC address for Port 1 as the System MAC address */
        LaCfaGetSysMacAddress (gLaGlobalInfo.LaSystem.SystemMacAddr);
#ifdef SNMP_2_WANTED
        SnmpNotifyInfo.pu4ObjectId = FsLaActorSystemID;
        SnmpNotifyInfo.u4OidLen = sizeof (FsLaActorSystemID) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = LaLock;
        SnmpNotifyInfo.pUnLockPointer = LaUnLock;
        SnmpNotifyInfo.u4Indices = 0;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%m",
                              gLaGlobalInfo.LaSystem.SystemMacAddr));
#endif
    }

    /* Copy the System MAC address to Aggregator and port. */
    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
#ifdef NPAPI_WANTED
        if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
        {
            if (LaFsLaHwCreateAggGroup (pAggEntry->u2AggIndex, &u2HwAggIndex)
                == FNP_FAILURE)
            {
                LaHandleAggCreateFailed (pAggEntry->u2AggIndex);
                LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                             "in HW failed\n", pAggEntry->u2AggIndex);
                return LA_SUCCESS;
            }
            LaFsLaHwSetSelectionPolicyBitList (pAggEntry->u2AggIndex,
                                               pAggEntry->AggConfigEntry.
                                               u4LinkSelectPolicyBitList);

            LaAggCreated (pAggEntry->u2AggIndex, u2HwAggIndex);
        }
#endif
        LA_MEMCPY (pAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        LaGetPortEntry (u2PortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }
        /* Since the port mac address is available after the cfa is
         * initialized the Sytem MacAddr in ActorAdminInfo is filled
         * Here.
         */
        LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        if (LaL2IwfGetPortOperStatus (LA_MODULE, u2PortIndex, &u1OperStatus)
            == L2IWF_FAILURE)
        {
            LA_TRC_ARG1 (INIT_SHUT_TRC, "Port: %u Port not found in CFA\n",
                         u2PortIndex);

            continue;
        }

        if (u1OperStatus == LA_OPER_DOWN)
        {
            pPortEntry->u1PortOperStatus = LA_OPER_DOWN;
            if (pPortEntry->pAggEntry != NULL)
            {
                pPortEntry->u4DownInBundleCount++;
                pPortEntry->u1PortDownReason = LA_IF_OPER_DOWN;
                LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC,
                                 "Port %d aggregated to port-channel %d "
                                 "moved to Down state due to Oper down indication\r\n",
                                 u2PortIndex,
                                 pPortEntry->pAggEntry->AggConfigEntry.
                                 u2ActorAdminKey);
                LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
                LaSyslogPrintAcivePorts (pPortEntry);
                LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                                    LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);
            }
            /* pPortEntry->u1PortAdminStatus = LA_ADMIN_DOWN; */
        }
        if (u1OperStatus == LA_OPER_UP)
        {
            pPortEntry->u1PortOperStatus = LA_OPER_UP;
        }

        if (LaCfaGetIfInfo ((UINT4) u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
        {
            LA_TRC_ARG1 (INIT_SHUT_TRC, "Cannot get CfaIfInfo: %u\n",
                         u2PortIndex);

            /* update the default link speed and duplexity */
            pPortEntry->u4LinkSpeed = CFA_ENET_SPEED_1G;
            pPortEntry->u4LinkHighSpeed = 0;
            pPortEntry->u4LinkDuplex = ETH_STATS_DUP_ST_FULLDUP;
            pPortEntry->u4Mtu = CFA_ENET_MTU;
            pPortEntry->i4PauseAdminMode = CFA_DEF_PAUSE_ADMIN_MODE;
        }
        else
        {
            LA_MEMCPY (pPortEntry->au1MacAddr, CfaIfInfo.au1MacAddr,
                       LA_MAC_ADDRESS_SIZE);
            pPortEntry->u4LinkSpeed = CfaIfInfo.u4IfSpeed;
            pPortEntry->u4LinkHighSpeed = CfaIfInfo.u4IfHighSpeed;
            pPortEntry->u4LinkDuplex = CfaIfInfo.u1DuplexStatus;
            pPortEntry->u4Mtu = CfaIfInfo.u4IfMtu;
            pPortEntry->u4RestoreMtu = CfaIfInfo.u4IfMtu;
            pPortEntry->i4PauseAdminMode = (INT4) CfaIfInfo.u1PauseAdminNode;
            pPortEntry->i4RestorePauseAdminMode
                = (INT4) CfaIfInfo.u1PauseAdminNode;
        }

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "Updated status of port %u \n", u2PortIndex);

        /* Initiate LACP protocol if configured as enable */
        LaChangeLacpMode (pPortEntry, LA_MODE_DISABLED, pPortEntry->LaLacpMode);

    }

    /* LA D-LAG functionality should be started in all the
     * Aggregators where D-LAG functionality is enabled */

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        LaActiveDLAGEnable ();
        LaGetNextAggEntry (NULL, &pAggEntry);
        while (pAggEntry != NULL)
        {
            if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
            {
                LaDLAGSetActorSystemIDToDLAG (pAggEntry);
            }
            LaGetNextAggEntry (pAggEntry, &pAggEntry);
        }                        /* End of while */
    }
#ifdef ICCH_WANTED
    else if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
    {
        LaActiveMCLAGEnable ();
        LaGetNextAggEntry (NULL, &pAggEntry);
        while (pAggEntry != NULL)
        {
            if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
            {
                LaDLAGSetActorSystemIDToDLAG (pAggEntry);
            }
            LaGetNextAggEntry (pAggEntry, &pAggEntry);
        }                        /* End of while */

    }
#endif
    else
    {
        LaDLAGScanLaListAndInitDLAG ();
    }

    LA_TRC (CONTROL_PLANE_TRC, "SYS: Enabled LA Module \n");
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleDisablePort.                                 */
/*                                                                           */
/* Description        : This function is called to disable all the state     */
/*                      machines for the port.                               */
/*                                                                           */
/* Input(s)           : Port Entry Index.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaHandleDisablePort (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            MacAddr = { 0 };

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 " SYS: Port: %u :: DISABLING Port\n", pPortEntry->u2PortIndex);

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    /* Initialise the variables for LACP state machines */
    pPortEntry->LaPortEnabled = LA_PORT_DISABLED;

    /* Call the state machines */
    LaLacRxMachine (pPortEntry, LA_RXM_EVENT_PORT_DISABLED, NULL);
    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_PORT_DISABLED);
    LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 " SYS: Port: %u :: Deleting Cache Entry\n",
                 pPortEntry->u2PortIndex);
    /* Delete all cache entries for the port */
    LaAggDeleteCacheEntry (pPortEntry);

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 " SYS: Port: %u :: Deleting Marker Entry\n",
                 pPortEntry->u2PortIndex);
    /* Delete the marker entry for the port */
    LaAggDeleteMarkerEntry (pPortEntry);

    /* Reset the DLAG parameters */
    pPortEntry->u1DLAGPeerReady = LA_FALSE;
    pPortEntry->u1DLAGMasterAck = LA_FALSE;

    /* when an aggregator port is disable in an aggregator:
     * If Only D-LAG status is enabled in Aggregator:-
     * Then when a member port is disabled in an aggregator, then low-priority
     * event-update message must be sent to notify the remote D-LAG node about
     * this event.
     * 
     * If D-LAG Redundancy feature is also enabled in Aggregator:-
     * Then When a member port is disabled a Slave Node, then role-played by
     * the D-LAG nodes will not be affected and sending low-priority event-
     * update message to update the remote nodes would be sufficient.
     * But if member port disabled is connected to Master Node then it does
     * make the difference as the master-slave-selection is based on the
     * number of ports in-sync in an aggregator, so Master-slave-selection
     * process must be triggered, but if the Node where this event has happened
     * is itself the elector then Master-slave-selection algorithm must be
     * immediately trigged immediately.
     * if the Node where this event has occured is not the designated elector
     * then high-priority event-update pdu must be sent to initiate the 
     * master slave selection in the elector. */
    pAggEntry = pPortEntry->pAggEntry;
    if (((pAggEntry) && (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)) ||
        (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
    {
        if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                             MacAddr,
                                             LA_FALSE,
                                             LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);

            LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
        else
        {
            LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
    }

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 " SYS: Port: %u :: Port DISABLED successfully\n",
                 pPortEntry->u2PortIndex);
}

/*****************************************************************************/
/* Function Name      : LaHandleDeletePort.                                  */
/*                                                                           */
/* Description        : This function is called to delete a physical port    */
/*                      entry.                                               */
/*                                                                           */
/* Input(s)           : Port Entry Index.                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : All memory pools.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : aLaLacAggInfo[], au4PortList[], apLaLacPortEntry[]   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_INVALID_VALUE - On receiving a NULL pointer   */
/*****************************************************************************/
INT4
LaHandleDeletePort (UINT2 u2PortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (LA_SYSTEM_CONTROL != LA_START)
    {
        LA_TRC (INIT_SHUT_TRC, "LA module is shut down.\n");
        return LA_FAILURE;
    }
    if ((u2PortIndex < LA_MIN_PORTS) || (u2PortIndex > LA_MAX_PORTS))
    {
        LA_TRC (INIT_SHUT_TRC, "Invalid Port Index value\n");
        return LA_ERR_INVALID_VALUE;
    }

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "PORT DOES NOT EXIST. Cannot delete Port\n");
        return LA_FAILURE;
    }

    if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
    {
        LaChangeLacpMode (pPortEntry, pPortEntry->LaLacpMode, LA_MODE_DISABLED);
    }

    if (pPortEntry->bDynAggSelect == LA_TRUE)
    {
        LA_SLL_DELETE (&(gLaGlobalInfo.LaDynReallocPortList),
                       &(pPortEntry->NextDefPortEntry));

        pPortEntry->pDefAggEntry->bDynPortRealloc = LA_FALSE;
    }

    /* Delete the Port Entry pointer from global info */
    LaDeleteFromPortTable (pPortEntry);

    LA_PORTENTRY_FREE_MEM_BLOCK (pPortEntry);

    /* Delete the Port Entry pointer from the array  of Port entry pointers
     * in the global info */

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "SYS: Port: %u :: Port DELETION successful\n", u2PortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeleteAllAggEntries.                               */
/*                                                                           */
/* Description        : This function is called to delete all the aggregator */
/*                      Entries                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaDeleteAllAggEntries (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4Val = 0;

    /* Get the first Aggregator entry */
    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        LaDeleteAggregator (pAggEntry);
        pAggEntry = NULL;
        i4Val = LaGetNextAggEntry (NULL, &pAggEntry);
        UNUSED_PARAM (i4Val);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaDisable.                                           */
/*                                                                           */
/* Description        : This function is called to disable the LA module     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaDisable (VOID)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex;
    tLaLacpMode         PrevLacpMode;
    tLaBoolean          LaDistributing;

    LA_TRC (INIT_SHUT_TRC, "SYS: Disabling LA Module \n");

    LA_PROTOCOL_ADMIN_STATUS () = LA_DISABLED;

    if (LA_MODULE_STATUS != LA_ENABLED)
    {
        LA_TRC (INIT_SHUT_TRC, "LA is not enabled. Cannot Disable\n");
        return LA_SUCCESS;
    }

    /* Before disabling LA D-LAG functionality should be stopped in all the
     * Aggregators where D-LAG functionality is enabled */
    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        LaActiveDLAGDisable ();
    }
#ifdef ICCH_WANTED
    else if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
    {
        LaActiveMCLAGDisable ();
    }
#endif
    else
    {
        LaDLAGScanLaListAndDeInitDLAG ();
    }

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        LaGetPortEntry (u2PortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }

        PrevLacpMode = pPortEntry->LaLacpMode;
        if (PrevLacpMode == LA_MODE_LACP)
        {
            /* Store the disstributing state before it gets
             * modified below */
            LaDistributing = pPortEntry->LaLacActorInfo.
                LaLacPortState.LaDistributing;

            /* Stop tick timer */
            if (LaStopTimer (&(pPortEntry->LaTickTimer)) == LA_FAILURE)
            {
                continue;
            }
            pPortEntry->LaPortEnabled = LA_PORT_DISABLED;

            LaLacRxMachine (pPortEntry, LA_RXM_EVENT_PORT_DISABLED, NULL);
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_PORT_DISABLED);
            LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);

            /* Delete all cache entries for the port */
            LaAggDeleteCacheEntry (pPortEntry);
            /* Delete the marker entry for the port */
            LaAggDeleteMarkerEntry (pPortEntry);

            LaL2IwfRemovePortFromPortChannel
                (pPortEntry->u2PortIndex, pPortEntry->pAggEntry->u2AggIndex);

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                LaFsLaHwRemovePortFromConfAggGroup (pPortEntry->pAggEntry->
                                                    u2AggIndex,
                                                    pPortEntry->u2PortIndex);
            }
#endif
#ifdef NPAPI_WANTED
            /* Port, which is removed from the LAGG, will be created as an
             * individual port in the higher layer modules under the
             * following scenarios -
             *   1) Port was a Standby port (it would NOT have been
             *      present in hardware)
             *   2) NPAPI mode is synchronous and removing the port
             *      from the LAGG in hardware was successful.
             * 
             * In case of asynchronous NPAPI mode the creation will be 
             * handled in the NPAPI callback */
            if ((LaDistributing == LA_FALSE) &&
                (pPortEntry->LaLacHwPortStatus != LA_HW_REM_PORT_FAILED_IN_LAGG)
                && (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG))

            {
                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);

            }
            else if (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_NOT_IN_LAGG)
            {
                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);
            }
#else /* NPAPI_WANTED */
            /* Create Physcial Port to Bridge */
            LaHlCreatePhysicalPort (pPortEntry);
#endif /* !NPAPI_WANTED */
        }
        else if (PrevLacpMode == LA_MODE_MANUAL)
        {
            /* Store the disstributing state before it gets
             * modified below */
            LaDistributing = pPortEntry->LaLacActorInfo.
                LaLacPortState.LaDistributing;
            LaDisableManualConfigOnPort (pPortEntry);
            LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC,
                             "Port %d aggregated to port-channel %d "
                             "moved to Down state due to port-channel Disable\r\n",
                             u2PortIndex,
                             pPortEntry->pAggEntry->AggConfigEntry.
                             u2ActorAdminKey);
            LaSyslogPrintAcivePorts (pPortEntry);
            LaL2IwfRemovePortFromPortChannel
                (pPortEntry->u2PortIndex, pPortEntry->pAggEntry->u2AggIndex);
            pPortEntry->u4DownInBundleCount++;
            pPortEntry->u1PortDownReason = LA_IF_OPER_DOWN;
            LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
            LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                                LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                LaFsLaHwRemovePortFromConfAggGroup (pPortEntry->pAggEntry->
                                                    u2AggIndex,
                                                    pPortEntry->u2PortIndex);
            }
#endif
#ifdef NPAPI_WANTED
            /* Port, which is removed from the LAGG, will be created as an
             * individual port in the higher layer modules under the
             * following scenarios -
             *   1) Port was a Standby port (it would NOT have been
             *      present in hardware)
             *   2) NPAPI mode is synchronous and removing the port
             *      from the LAGG in hardware was successful.
             * 
             * In case of asynchronous NPAPI mode the creation will be 
             * handled in the NPAPI callback */
            if ((LaDistributing == LA_FALSE) &&
                (pPortEntry->LaLacHwPortStatus != LA_HW_REM_PORT_FAILED_IN_LAGG)
                && (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG))
            {
                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);

            }
            else if (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_NOT_IN_LAGG)
            {
                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);
            }
#else /* NPAPI_WANTED */
            /* Create Physcial Port to Bridge */
            LaHlCreatePhysicalPort (pPortEntry);
#endif /* !NPAPI_WANTED */
        }
        else
        {
            /* Nothing to do for LA_MODE_DISABLED */
        }
        pPortEntry->LaLacpMode = PrevLacpMode;

        if (pPortEntry->pAggEntry == NULL)
        {
            pPortEntry->u1PortOperStatus = LA_OPER_DOWN;
        }
    }

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        LaGetPortEntry (u2PortIndex, &pPortEntry);

        if (pPortEntry != NULL)
        {
            /* Delete all cache entries for the port */
            LaAggDeleteCacheEntry (pPortEntry);

            /* Delete the marker entry for the port */
            LaAggDeleteMarkerEntry (pPortEntry);

        }
    }                            /* End of FOR LOOP */

    /* Disable the LA Status */
    LA_MODULE_STATUS = LA_DISABLED;

    /* Indicate standby node to clear all the dynamic information synced up. */
    LaRedSyncProtocolDisable ();
    LA_TRC (CONTROL_PLANE_TRC, "SYS:    ******* LA DISABLED *******\n");

#ifndef NPAPI_WANTED
    UNUSED_PARAM (LaDistributing);
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacHwControl.                                      */
/*                                                                           */
/* Description        : This function is called from LaLacUpdateMux(). This  */
/*                      function is useful in indicating to lower layer      */
/*                      hardware / interface about the mux state by calling  */
/*                      the callback functions provided by it. Presently,    */
/*                      the function just updates the event and calls mux.   */
/*                                                                           */
/* Input(s)           : pPortEntry, event                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of NULL port entry ptr. */
/*****************************************************************************/
INT4
LaLacHwControl (tLaLacPortEntry * pPortEntry, tLaHwEvent LaLacEvent)
{

    tLaLacAggConfigEntry *pAggConfigEntry = NULL;
    tLaLacAggEntry     *pAggEntry;
    tNotifyProtoToApp   NotifyProtoToApp;
    tLaNpTrapLogInfo    NpLogInfo;
#ifdef L2RED_WANTED
    tLaHwSyncInfo      *pLaHwEntry = NULL;
#endif
#ifdef NPAPI_WANTED
    UINT4               u4RetVal = FNP_SUCCESS;
    UINT2               u2HwAggIndex = LA_INVALID_HW_AGG_IDX;

    INT1               *piIfName = NULL;
    INT1               *piLagName = NULL;
#endif
    UINT2               u2AggIndex;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1LagName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1LagName, 0, CFA_MAX_PORT_NAME_LENGTH);
#ifdef NPAPI_WANTED
    piIfName = (INT1 *) &au1IfName[0];
    piLagName = (INT1 *) &au1LagName[0];
#endif
    MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));

    if (pPortEntry == NULL)
    {

        LA_TRC (INIT_SHUT_TRC, "Pointer to Port Entry NULL\n");
        return LA_ERR_NULL_PTR;
    }

    if ((pAggEntry = pPortEntry->pAggEntry) == NULL)
    {
        return LA_FAILURE;
    }
    pAggConfigEntry = &(pAggEntry->AggConfigEntry);
    u2AggIndex = pAggEntry->u2AggIndex;

    switch (LaLacEvent)
    {

        case LA_HW_EVENT_PORT_INIT:

            break;

        case LA_HW_EVENT_PORT_ATTACH:

            pAggEntry->u1AttachedPortCount++;

            break;

        case LA_HW_EVENT_PORT_DETACH:

            pAggEntry->u1AttachedPortCount--;
            break;

        case LA_HW_EVENT_ENABLE_COLLECTOR:

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                LaFsLaHwEnableCollection (u2AggIndex, pPortEntry->u2PortIndex);
            }
#endif
            break;

        case LA_HW_EVENT_DISABLE_COLLECTOR:

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                LaFsLaHwDisableCollection (u2AggIndex, pPortEntry->u2PortIndex);
            }
#endif

            break;

        case LA_HW_EVENT_ENABLE_DISTRIBUTOR:

            if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
                (LA_AA_DLAG_MASTER_DOWN == LA_FALSE))
            {
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n Global DLAG Enabled. Enable Distributor Skip \n");
                return LA_SUCCESS;

            }

            if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
            {
                /* Distributing port count will be incremented in LaActiveMCLAGHwControl for 
                   Slave */
                if (LA_AA_DLAG_ROLE_PLAYED == LA_MCLAG_SYSTEM_ROLE_MASTER)
                {
                    pAggEntry->u1DistributingPortCount++;

                }
            }
            else
            {
                pAggEntry->u1DistributingPortCount++;
            }

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                if (pAggEntry->u1DistributingPortCount == 1)
                {
                    if (LaFsLaHwCreateAggGroup (u2AggIndex, &u2HwAggIndex)
                        == FNP_FAILURE)
                    {
                        LaHandleAggCreateFailed (u2AggIndex);
                        LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                                     "in HW failed\n", u2AggIndex);
                        return LA_SUCCESS;
                    }

                    LaFsLaHwSetSelectionPolicyBitList
                        (u2AggIndex,
                         pAggConfigEntry->u4LinkSelectPolicyBitList);

                    LaAggCreated (u2AggIndex, u2HwAggIndex);
                }

                /* indicate hw to add a link to an agg. */
                if (LaFsLaHwAddLinkToAggGroup
                    (u2AggIndex, pPortEntry->u2PortIndex,
                     &u2HwAggIndex) == FNP_FAILURE)
                {
                    pPortEntry->u1PortDownReason = LA_HW_FAILURE;
                    LaHandleLinkAdditionFailed (u2AggIndex,
                                                pPortEntry->u2PortIndex);
#if (defined ( MSTP_WANTED) || defined(RSTP_WANTED) || defined(PVRST_WANTED) || defined (ERPS_WANTED))
                    LaUpdatePortChannelStatusToHw (u2AggIndex, LA_PORT_DISABLED,
                                                   LA_TRUE);
#endif
                    return LA_SUCCESS;
                }

                LaFsLaHwEnableDistribution (u2AggIndex,
                                            pPortEntry->u2PortIndex);
                CfaCliConfGetIfName (u2AggIndex, piLagName);
                CfaCliConfGetIfName (pPortEntry->u2PortIndex, piIfName);
                UtlTrcLog (LaCfaGetGlobalModTrc (), 1, "LACP",
                           "Port %s is made UP in Bundle for %s", piIfName,
                           piLagName);

                /* Update the actual staus of port-channel to h/w */
#if (defined ( MSTP_WANTED) || defined(RSTP_WANTED) || defined(PVRST_WANTED) || defined (ERPS_WANTED))
                LaUpdatePortChannelStatusToHw (u2AggIndex, LA_PORT_DISABLED,
                                               LA_TRUE);
#endif
                CfaCliConfGetIfName (u2AggIndex, piLagName);
                CfaCliConfGetIfName (pPortEntry->u2PortIndex, piIfName);
                UtlTrcLog (LaCfaGetGlobalModTrc (), 1, "LACP",
                           "Port %s is made Down in Bundle for %s", piIfName,
                           piLagName);

                /* This function is moved inside NP_PROGRAMMING_ALLOWED, 
                 * because in Synchronous NP case, this function should not be 
                 * executed in the Standy node.In the standby node, this 
                 * function is invoked from LA_NP_ADD_PORT_SUCCESS_INFO message 
                 * handler */

                LaLinkAddedToAgg (u2AggIndex, pPortEntry->u2PortIndex);
            }
            else
            {
                /* Port need to be added to port channel when 
                 * its state is moved to Distributing */
                LaLinkAddedToAgg (u2AggIndex, pPortEntry->u2PortIndex);
            }
#else /* if NPAPI_WANTED not defined */

            /*If NPAPI_WANTED is not defined, then function gets invoked in
             * both active and standby node */
            LaLinkAddedToAgg (u2AggIndex, pPortEntry->u2PortIndex);
#endif
            break;

        case LA_HW_EVENT_DISABLE_DISTRIBUTOR:

            if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
                (LA_AA_DLAG_MASTER_DOWN == LA_FALSE) &&
                /*1) The following variable is only TRUE when DLAG is waiting for
                 *   Master Reply. In that case port is not added in H/w.
                 *   So removal not necessary. (shutdown port when Master no ack)
                 *2) If the following is FALSE means it is not DLAG and normal LACP
                 and trying to Remove it becasue of DLAG enabled.
                 That time removal necessary */
                (pPortEntry->u1DLAGPeerReady == LA_TRUE))
            {
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n Global DLAG Enabled. Disable Distributor Skip \n");
                return LA_SUCCESS;
            }

            if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
            {
                /* Distributing port count will be decremented in LaActiveMCLAGHwControl for
                   Slave */
                if ((LA_AA_DLAG_ROLE_PLAYED == LA_MCLAG_SYSTEM_ROLE_MASTER) &&
                    (pAggEntry->u1DistributingPortCount != 0))
                {
                    pAggEntry->u1DistributingPortCount--;
                }
            }
            else
            {
                pAggEntry->u1DistributingPortCount--;
            }

            LaChangeAggSpeed (pAggEntry);
            if (pAggEntry->u1DistributingPortCount == 0)
            {
#ifdef L2RED_WANTED
                /* When LA_NP_DEL_INFO doesnt reach the standby node, At the 
                 * standby, all ports in the group will be in DOWN state 
                 * and distributingPortCount variable in the corresponding agg 
                 * is zero but pLaHwEntry->u1AggStatus = LA_ENABLE and 
                 * pLaHwEntry->u2HwAggIdx will have valid agg Id.
                 * 
                 * If the HW maintains one to one mapping between the HW agg 
                 * index and SW agg index (always same HwAggIdx for SwAggIdx), 
                 * then this scenario will not be an issue
                 * 
                 * If the HW reassigns the HwAggId to some other SwAggIdx then 
                 * this will be an issue during the HW Audit 
                 * There is a possibility for having same HwAggIdx to two or more 
                 * SwAggIdx, this happens if LA_NP_DEL_INFO doesnt reach the standby
                 * In the HwAudit, it reads the active HwAggIdx and gets the 
                 * corresponding SwAggIdx, here, since we have two SwAgg with same 
                 * HwAggIdx, we may return older SwAggIdx. It checks for the 
                 * distributingPortCount, which is zero, so it deletes the aggregator 
                 * from the HW. The problem here is, New AggIdx will be in up state 
                 * with ports in distributing state but Hw will have not have this 
                 * group.
                 * This is taken care by the following statements, here we reset the 
                 * HwAggIdx and AggStatus if distributingPortCount becomes zero, 
                 * without waiting for LA_NP_DEL_INFO 
                 */
                if ((u2AggIndex <= LA_MAX_PORTS) ||
                    (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
                    return LA_FAILURE;
                pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);
                if (pLaHwEntry != NULL)
                {
                    pLaHwEntry->u2HwAggIdx = LA_INVALID_HW_AGG_IDX;
                    pLaHwEntry->u1AggStatus = LA_DISABLED;
                }

#endif /* L2RED_WANTED */
                if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
                {
                    pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
                    pAggEntry->u4OperDownCount++;
                    LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC,
                                     "Line Protocol on Port Channel %d "
                                     "changed to OPER down\r\n",
                                     pAggEntry->AggConfigEntry.u2ActorAdminKey);
                    pAggEntry->i4DownReason = LA_IF_OPER_DOWN;
                    LaLinkStatusNotify (pAggEntry->u2AggIndex, LA_OPER_DOWN,
                                        pAggEntry->u1AggAdminStatus,
                                        LA_TRAP_PORTCHANNEL_OPER_DOWN);

                    /* Get the time of last oper change */
                    LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
                    /* Indicate to bridge */
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                 "\nLA: Line Protocol on Port Channel %d changed down",
                                 pAggEntry->AggConfigEntry.u2ActorAdminKey);
                    LaL2IwfLaHLPortOperIndication (u2AggIndex, LA_OPER_DOWN);

                }

                pAggEntry->bHlIndicated = LA_FALSE;
                LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                             "SYS: Port: %u :: AggIndex: %u OPER DOWN Indication to BRIDGE\n",
                             pPortEntry->u2PortIndex, u2AggIndex);
            }
            LaUpdateLlPortChannelStatus (pAggEntry);
#ifdef NPAPI_WANTED

            /* When FsLaHwAddLinkToAggGroup fails then the variable 
             * LaLacHwPortStatus is set to LA_HW_ADD_PORT_FAILED_IN_LAGG. In
             * the NP callback function we set the Operkey of this port to
             * zero. This makes the port to come out of the bundle, So we 
             * execute DISABLE_DISTRIBUTING switch case in the 
             * LaLacHwControl,here we try to delete the port from the HW
             * (which is not present in the HW), because the port is not in
             * the HW, delete call fails and the LaLacHwPortStatus is set
             * to LA_HW_REM_PORT_FAILED_IN_LAGG. To handle this,
             * Before initiating the PortDel NP call in the 
             * LaLacHwControl, Can we check for the status of this 
             * variable (LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG) 
             */

            if (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG)
            {
                if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                {
                    if (LaFsLaHwRemoveLinkFromAggGroup (u2AggIndex,
                                                        pPortEntry->u2PortIndex)
                        == FNP_FAILURE)
                    {
                        LaHandleLinkRemoveFailed (u2AggIndex,
                                                  pPortEntry->u2PortIndex);
                    }
                }
                if (u4RetVal == FNP_SUCCESS)
                {
                    if ((LaIssGetAsyncMode (L2_PROTO_LACP) == LA_NP_SYNC))
                    {
                        pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
                        LaRedSyncModifyPortsInfo
                            (LA_NP_DEL_PORT_SUCCESS_INFO, u2AggIndex,
                             pPortEntry->u2PortIndex);
                    }
                }
            }
            else
            {
                /* If the port is not present in the HW */
                pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

                LaRedSyncModifyPortsInfo (LA_NP_DEL_PORT_SUCCESS_INFO,
                                          u2AggIndex, pPortEntry->u2PortIndex);
            }

            /*The indication is send to applications that the port is
             *removed from the port channel*/

            MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));

            NotifyProtoToApp.LaNotify.u1OperStatus = LA_OPER_DOWN;
            NotifyProtoToApp.LaNotify.u1Action =
                LA_NOTIFY_PORT_IN_BUNDLE_STATUS;
            /*SystemId in pLaParamInfo is unused for this indication */
            LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
#if (defined ( MSTP_WANTED) || defined(RSTP_WANTED) || defined(PVRST_WANTED) || defined (ERPS_WANTED))
            LaUpdatePortChannelStatusToHw (u2AggIndex,
                                           LA_PORT_DISABLED, LA_TRUE);
#endif
#endif /* NPAPI_WANTED */

            LaL2IwfRemoveActivePortFromPortChannel (pPortEntry->u2PortIndex,
                                                    u2AggIndex);
            /* LA_MCAST_CHANGE Ends */
            break;

        default:

            break;
    }

#ifndef NPAPI_WANTED
    UNUSED_PARAM (pAggConfigEntry);
    UNUSED_PARAM (NotifyProtoToApp);
#endif

    return LA_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaHandleNpCallbackEvents                             */
/*                                                                           */
/* Description        : */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaHandleNpCallbackEvents (tLaIntfMesg * pLaMsg)
{
    tLaNpCbMsg          LaNpCbMsg;
    tLaLacAggEntry     *pAggEntry;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaNpTrapLogInfo    NpLogInfo;
    unAsyncNpapi        NpParams;

    gLaCallSequence = LA_NP_CALLBACK_FLOW;

    MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));
    MEMSET (&LaNpCbMsg, 0, sizeof (tLaNpCbMsg));
    MEMCPY (&LaNpCbMsg, (tLaNpCbMsg *) & (pLaMsg->uLaIntfMsg.LaNpCbMsg),
            sizeof (tLaNpCbMsg));

    MEMSET (&NpParams, 0, sizeof (unAsyncNpapi));

    switch (LaNpCbMsg.u4NpCallId)
    {
        case AS_FS_LA_HW_CREATE_AGG_GROUP:

            MEMCPY (&NpParams.FsLaHwCreateAggGroup,
                    (tAsNpwFsLaHwCreateAggGroup *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup),
                    sizeof (tAsNpwFsLaHwCreateAggGroup));

            /* Success case of this API is not handled */
            if (NpParams.FsLaHwCreateAggGroup.rval == FNP_FAILURE)
            {
                LaHandleAggCreateFailed
                    (NpParams.FsLaHwCreateAggGroup.u2AggIndex);
            }
            else
            {
                LaAggCreated (NpParams.FsLaHwCreateAggGroup.u2AggIndex,
                              NpParams.FsLaHwCreateAggGroup.u2HwAggIndex);
            }
            break;

        case AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP:

            MEMCPY (&NpParams.FsLaHwAddLinkToAggGroup,
                    (tAsNpwFsLaHwAddLinkToAggGroup *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwAddLinkToAggGroup),
                    sizeof (tAsNpwFsLaHwAddLinkToAggGroup));

            if (NpParams.FsLaHwAddLinkToAggGroup.rval == FNP_SUCCESS)
            {
                LaLinkAddedToAgg (NpParams.FsLaHwAddLinkToAggGroup.u2AggIndex,
                                  NpParams.FsLaHwAddLinkToAggGroup.
                                  u2PortNumber);
            }
            else
            {
                LaHandleLinkAdditionFailed
                    (NpParams.FsLaHwAddLinkToAggGroup.u2AggIndex,
                     NpParams.FsLaHwAddLinkToAggGroup.u2PortNumber);

            }
            break;

        case AS_FS_LA_HW_DELETE_AGGREGATOR:

            MEMCPY (&NpParams.FsLaHwDeleteAggregator,
                    (tAsNpwFsLaHwDeleteAggregator *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwDeleteAggregator),
                    sizeof (tAsNpwFsLaHwDeleteAggregator));

            if (NpParams.FsLaHwDeleteAggregator.rval == FNP_FAILURE)
            {
                NpLogInfo.u4NpCallId = AS_FS_LA_HW_DELETE_AGGREGATOR;
                NpLogInfo.u2AggIndex =
                    NpParams.FsLaHwDeleteAggregator.u2AggIndex;
                LaNpFailSendTrapAndSyslog (&NpLogInfo);
            }
            else
            {
                if (LaGetAggEntry (NpParams.FsLaHwCreateAggGroup.u2AggIndex,
                                   &pAggEntry) == LA_SUCCESS)
                {
                    if (pAggEntry->HwAggStatus != LA_AGG_ADD_FAILED_IN_HW)
                        pAggEntry->HwAggStatus = LA_AGG_NOT_IN_HW;
                }
            }
            break;

        case AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:

            MEMCPY (&NpParams.FsLaHwRemoveLinkFromAggGroup,
                    (tAsNpwFsLaHwRemoveLinkFromAggGroup *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.
                       FsLaHwRemoveLinkFromAggGroup),
                    sizeof (tAsNpwFsLaHwRemoveLinkFromAggGroup));

            if (NpParams.FsLaHwRemoveLinkFromAggGroup.rval == FNP_SUCCESS)
            {
                LaGetPortEntry
                    (NpParams.FsLaHwRemoveLinkFromAggGroup.u2PortNumber,
                     &pPortEntry);
                if (pPortEntry != NULL)
                {
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

                    if ((pPortEntry->pAggEntry == NULL) ||
                        (LA_PROTOCOL_ADMIN_STATUS () == LA_DISABLED))
                    {
                        LaHlCreatePhysicalPort (pPortEntry);

                    }
                }
                if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
                {
                    LaRedSyncModifyPortsInfo (LA_NP_DEL_PORT_SUCCESS_INFO,
                                              NpParams.
                                              FsLaHwRemoveLinkFromAggGroup.
                                              u2AggIndex, NpParams.
                                              FsLaHwRemoveLinkFromAggGroup.
                                              u2PortNumber);
                }
            }
            else
            {
                LaHandleLinkRemoveFailed (NpParams.
                                          FsLaHwRemoveLinkFromAggGroup.
                                          u2AggIndex, NpParams.
                                          FsLaHwRemoveLinkFromAggGroup.
                                          u2PortNumber);
            }
            break;

        case AS_FS_LA_HW_SET_SELECTION_POLICY:

            MEMCPY (&NpParams.FsLaHwSetSelectionPolicy,
                    (tAsNpwFsLaHwSetSelectionPolicy *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwSetSelectionPolicy),
                    sizeof (tAsNpwFsLaHwSetSelectionPolicy));

            if (NpParams.FsLaHwSetSelectionPolicy.rval == FNP_FAILURE)
            {
                NpLogInfo.u4NpCallId = AS_FS_LA_HW_SET_SELECTION_POLICY;
                NpLogInfo.u2AggIndex =
                    NpParams.FsLaHwSetSelectionPolicy.u2AggIndex;
                NpLogInfo.u1SelectionPolicy =
                    NpParams.FsLaHwSetSelectionPolicy.u2AggIndex;
                LaNpFailSendTrapAndSyslog (&NpLogInfo);
            }
            break;

        case AS_FS_LA_HW_ENABLE_COLLECTION:

            MEMCPY (&NpParams.FsLaHwEnableCollection,
                    (tAsNpwFsLaHwEnableCollection *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwEnableCollection),
                    sizeof (tAsNpwFsLaHwEnableCollection));

            if (NpParams.FsLaHwEnableCollection.rval == FNP_FAILURE)
            {
                NpLogInfo.u4NpCallId = AS_FS_LA_HW_ENABLE_COLLECTION;
                NpLogInfo.u2AggIndex =
                    NpParams.FsLaHwEnableCollection.u2AggIndex;
                NpLogInfo.u2PortNumber =
                    NpParams.FsLaHwEnableCollection.u2PortNumber;
                LaNpFailSendTrapAndSyslog (&NpLogInfo);
            }
            break;

        case AS_FS_LA_HW_ENABLE_DISTRIBUTION:

            MEMCPY (&NpParams.FsLaHwEnableDistribution,
                    (tAsNpwFsLaHwEnableDistribution *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwEnableDistribution),
                    sizeof (tAsNpwFsLaHwEnableDistribution));

            if (NpParams.FsLaHwEnableDistribution.rval == FNP_FAILURE)
            {
                NpLogInfo.u4NpCallId = AS_FS_LA_HW_ENABLE_DISTRIBUTION;
                NpLogInfo.u2AggIndex =
                    NpParams.FsLaHwEnableDistribution.u2AggIndex;
                NpLogInfo.u2PortNumber =
                    NpParams.FsLaHwEnableDistribution.u2PortNumber;
                LaNpFailSendTrapAndSyslog (&NpLogInfo);
            }
            break;

        case AS_FS_LA_HW_DISABLE_COLLECTION:

            MEMCPY (&NpParams.FsLaHwDisableCollection,
                    (tAsNpwFsLaHwDisableCollection *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwDisableCollection),
                    sizeof (tAsNpwFsLaHwDisableCollection));

            if (NpParams.FsLaHwDisableCollection.rval == FNP_FAILURE)
            {
                NpLogInfo.u4NpCallId = AS_FS_LA_HW_DISABLE_COLLECTION;
                NpLogInfo.u2AggIndex =
                    NpParams.FsLaHwDisableCollection.u2AggIndex;
                NpLogInfo.u2PortNumber =
                    NpParams.FsLaHwDisableCollection.u2PortNumber;
                LaNpFailSendTrapAndSyslog (&NpLogInfo);
            }
            break;

        case AS_FS_LA_HW_SET_PORT_CHANNEL_STATUS:

            MEMCPY (&NpParams.FsLaHwSetPortChannelStatus,
                    (tAsNpwFsLaHwSetPortChannelStatus *)
                    & (LaNpCbMsg.LaAsyncNpCbParams.FsLaHwSetPortChannelStatus),
                    sizeof (tAsNpwFsLaHwSetPortChannelStatus));

            if (NpParams.FsLaHwSetPortChannelStatus.rval == FNP_FAILURE)
            {
                NpLogInfo.u4NpCallId = AS_FS_LA_HW_SET_PORT_CHANNEL_STATUS;
                NpLogInfo.u2AggIndex =
                    NpParams.FsLaHwSetPortChannelStatus.u2AggIndex;
                NpLogInfo.u2Inst = NpParams.FsLaHwSetPortChannelStatus.u2Inst;
                NpLogInfo.u1StpState =
                    NpParams.FsLaHwSetPortChannelStatus.u1StpState;
                LaNpFailSendTrapAndSyslog (&NpLogInfo);
            }
            break;
    }
    gLaCallSequence = LA_PROTCOL_FLOW;
    return LA_SUCCESS;
}
#endif /*NPAPI_WANTED */
/*****************************************************************************/
/* Function Name      : LaUpdateLlPortChannelStatus.                         */
/*                                                                           */
/* Description        : This function updates CFA with the Port Channel      */
/*                      information link Operational status, Speed and       */
/*                      Port Channel Mac Address                             */
/*                                                                           */
/* Input(s)           : Pointer to Aggregator Entry.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaUpdateLlPortChannelStatus (tLaLacAggEntry * pAggEntry)
{
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2IfIndex;
    UINT1               u1Flag = LA_FALSE;
    UINT1               u1TrapStatus = 0;
    INT4                i4Status = CFA_FAILURE;

    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    u2IfIndex = pAggEntry->u2AggIndex;
    i4Status = LaCfaGetIfInfo ((UINT4) u2IfIndex, &CfaIfInfo);

    if (CfaIfInfo.u1IfOperStatus != pAggEntry->u1AggOperStatus)
    {
        u1Flag = LA_TRUE;
    }
#ifdef ICCH_WANTED
    /* MC-LAG PortChannel OperStatus Change is notified to ICCH/VLAN
     * so as to change the FDB Table accordingly */

    if ((pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED) && (u1Flag == LA_TRUE))
    {
        if (pAggEntry->u1AggOperStatus == CFA_IF_DOWN)
        {
            LaIcchProcessMclagOperDown (pAggEntry->u2AggIndex);
        }
        else if (pAggEntry->u1AggOperStatus == CFA_IF_UP)
        {
            LaVlanProcessMclagOperUp (pAggEntry->u2AggIndex);
        }
    }
#endif
    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    CfaIfInfo.u1IfOperStatus = pAggEntry->u1AggOperStatus;
    LA_MEMCPY (CfaIfInfo.au1MacAddr,
               pAggEntry->AggConfigEntry.AggMacAddr, LA_MAC_ADDRESS_SIZE);

    CfaIfInfo.u4IfSpeed = pAggEntry->u4AggSpeed;
    /*If the High speed is 0,then update ifHighSpeed to ifSpeed */
    if (pAggEntry->u4AggHighSpeed == 0)
    {
        CfaIfInfo.u4IfHighSpeed = (pAggEntry->u4AggSpeed / CFA_1MB);
    }
    else
    {
        CfaIfInfo.u4IfHighSpeed = pAggEntry->u4AggHighSpeed;
    }
    LaCfaSetIfInfo (IF_OPER_STATUS, (UINT4) u2IfIndex, &CfaIfInfo);

    LaCfaSetIfInfo (IF_SPEED, (UINT4) u2IfIndex, &CfaIfInfo);
    if (u1Flag == LA_TRUE)
    {
        if (LaCfaGetLinkTrapEnabledStatus (u2IfIndex, &u1TrapStatus) ==
            CFA_SUCCESS)
        {
            if (u1TrapStatus == CFA_ENABLED)
            {
                if (pAggEntry->u1AggOperStatus == CFA_IF_DOWN)
                {
                    LaLinkStatusNotify ((UINT4) u2IfIndex,
                                        pAggEntry->u1AggOperStatus,
                                        pAggEntry->u1AggAdminStatus,
                                        LA_TRAP_PORTCHANNEL_DOWN);
                }
                else
                {
                    LaLinkStatusNotify ((UINT4) u2IfIndex,
                                        pAggEntry->u1AggOperStatus,
                                        pAggEntry->u1AggAdminStatus,
                                        LA_TRAP_PORTCHANNEL_UP);
                }
            }
        }
    }
    UNUSED_PARAM (i4Status);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangeAggSpeed()                                   */
/*                                                                           */
/* Description        : Calculates the Aggregator Speed. Called when a port  */
/*                      becomes distributing in a port channel, or goes out  */
/*                      of distributing state.                               */
/*                                                                           */
/* Input(s)           : Pointer to Aggregator Entry.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaChangeAggSpeed (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DistPortSpeed = 0;
    UINT4               u4DistPortHighSpeed = 0;
    UINT2               u2DistPortCount = 0;

    pAggEntry->u4AggSpeed = 0;
    pAggEntry->u4AggHighSpeed = 0;

    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

    while (pPortEntry != NULL)
    {
        if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing == LA_TRUE)
        {
            u4DistPortSpeed = pPortEntry->u4LinkSpeed;
            u4DistPortHighSpeed = pPortEntry->u4LinkHighSpeed;
            u2DistPortCount++;
        }
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }

    /* This logic depends on the link speed of each port number of 
     * distributing ports in the aggregation. 
     * For 100Mpbs links, if the number exceeds 40 then high speed is used
     * because if 40*100 mbps links are aggregated then the threshold value
     * of the UINT4 variable is reached. 
     * For 1Gbps links, if the number exceeds 4 then high speed is used
     * because if 4*1000 mbps links are aggregated then the threshold value
     * of the UINT4 variable is reached. */
    if (u4DistPortSpeed < LA_32BIT_MAX)
    {
        if ((u4DistPortSpeed <= CFA_ENET_SPEED_100M) && (u2DistPortCount <= 40))
        {
            pAggEntry->u4AggSpeed = u2DistPortCount * (u4DistPortSpeed);
            pAggEntry->u4AggHighSpeed = (pAggEntry->u4AggSpeed / CFA_1MB);
        }

        else if ((u4DistPortSpeed <= CFA_ENET_SPEED_1G) &&
                 (u2DistPortCount <= 4))
        {
            pAggEntry->u4AggSpeed = u2DistPortCount * (u4DistPortSpeed);
            pAggEntry->u4AggHighSpeed = (pAggEntry->u4AggSpeed / CFA_1MB);
        }

        else
        {
            pAggEntry->u4AggSpeed = LA_32BIT_MAX;
            pAggEntry->u4AggHighSpeed = u2DistPortCount *
                (u4DistPortSpeed / 1000000);
        }
    }
    else
    {
        pAggEntry->u4AggSpeed = LA_32BIT_MAX;
        pAggEntry->u4AggHighSpeed = u2DistPortCount * (u4DistPortHighSpeed);

    }
}

/*****************************************************************************/
/* Function Name      : LaCalculateAggOperStatus                             */
/*                                                                           */
/* Description        : Calculates the Aggregator operational status.        */
/*                      Called when a port becomes distributing in a port    */
/*                      channel, or goes out of distributing state.          */
/*                                                                           */
/* Input(s)           : Pointer to Aggregator Entry, PortChannel AdminStatus */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaCalculateAggOperStatus (tLaLacAggEntry * pAggEntry, UINT1 u1AggAdminStatus)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (u1AggAdminStatus == CFA_IF_UP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
                LA_TRUE)
            {
                pAggEntry->u1AggOperStatus = LA_OPER_UP;
                pAggEntry->u4OperUpCount++;
                pAggEntry->i4DownReason = LA_NONE;
                LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
                LaLinkStatusNotify (pAggEntry->u2AggIndex, LA_OPER_UP,
                                    LA_ADMIN_UP, LA_TRAP_PORTCHANNEL_OPER_UP);
                break;
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
    else
    {
        if (pAggEntry->u1AggOperStatus == LA_OPER_UP)
        {
            pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
#ifdef ICCH_WANTED
            /* Initiate handling for Admin Down if  the interface belongs
             * to MC-LAG port-channel.
             */
            if (LA_MCLAG_ENABLED == pAggEntry->u1MCLAGStatus)
            {
                LaIcchProcessMclagOperDown (pAggEntry->u2AggIndex);
            }
#endif
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCalculateAggMemberOperStatus                       */
/*                                                                           */
/* Description        : Calculates the oper status of the ports in the       */
/*                      bundle when the port channel is enabled              */
/*                      When the port channel is enabled again after         */
/*                      disabling, system paramters of the port channel      */
/*                      (eg: MTU)might be changed.This function calculates   */
/*                      the oper status of the ports in bundle based on the  */
/*                      new parameters                                       */
/*                                                                           */
/* Input(s)           : Pointer to Aggregator Entry, PortChannel AdminStatus */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*****************************************************************************/
INT4
LaCalculateAggMemberOperStatus (tLaLacAggEntry * pAggEntry,
                                UINT1 u1AggAdminStatus)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (u1AggAdminStatus == CFA_IF_UP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

        while (pPortEntry != NULL)
        {
            if (pPortEntry->LaLacpMode == LA_MODE_MANUAL)
            {
                LaSelectManualAggGroup (pAggEntry);
            }
            else if (pPortEntry->LaLacpMode == LA_MODE_LACP)
            {

                LaSelectLacpAggGroup (pAggEntry);
            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaReleaseCacheMemBlock                               */
/*                                                                           */
/* Description        : This function gives the memory block back to the pool*/
/*                                                                           */
/* Input(s)           : pLaHashEntry.                                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaReleaseCacheMemBlock (tLaHashNode * pLaHashEntry)
{
    if (LA_CACHEENTRY_FREE_MEM_BLOCK (pLaHashEntry) != LA_MEM_SUCCESS)
    {
        LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "SYS: Cache Entry Memory Block Release FAILED\n");
    }
}

/*****************************************************************************/
/* Function Name      : LaAggCreated                                         */
/*                                                                           */
/* Description        : This function gives the memory block back to the pool*/
/*                                                                           */
/* Input(s)           : pLaHashEntry.                                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaAggCreated (UINT2 u2AggIndex, UINT2 u2HwAggIndex)
{
    tLaLacAggEntry     *pAggEntry;
    tLaLacPortEntry    *pPortEntry = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
#endif

    /* In Asynchronous NPAPI mode of operation, VCM Mapping, VLAN Cpy properties
     * about the aggregator, should be given only when the NPAPI operation
     * succeeds. Hence this fn will be skipped in the normal protocol flow, 
     * but will be invoked from the  NPAPI callback flow. */

    if ((LaIssGetAsyncMode (L2_PROTO_LACP) == LA_NP_ASYNC) &&
        (gLaCallSequence != LA_NP_CALLBACK_FLOW))
    {
        return LA_SUCCESS;
    }

    if (LaGetAggEntry (u2AggIndex, &pAggEntry) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    pAggEntry->HwAggStatus = LA_AGG_IN_HW;

    if (pAggEntry->LaLacpMode == LA_MODE_LACP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        /*This is done to restart the statemachine for other ports in the group.
         *  If a port gets added into the bundle and an agg is created 
         *  successfully, then we restart the statemachine for other port in the
         *  group, which already failed to get added into the HW because of Agg
         *  create failure/Add Link failure */

        while (pPortEntry != NULL)
        {
            if (pPortEntry->LaLacActorInfo.u2IfKey == 0)
            {
                pPortEntry->LaLacActorInfo.u2IfKey =
                    pPortEntry->LaLacActorAdminInfo.u2IfKey;

                pPortEntry->NttFlag = LA_TRUE;
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (pAggEntry->u1DistributingPortCount == 0)
        {

            /* Assumptions -
             * 1) VCM will remove it's port-channel-to-context mapping
             * only after the port-channel is deleted in LA.
             * 
             * 2) Also the port-channel will never be known to LA unless
             * it is mapped to some context.
             * 
             * Hence the below call should never fail
             * */
            if (LA_FAILURE ==
                LaVcmGetContextInfoFromIfIndex (u2AggIndex, &u4ContextId,
                                                &u2LocalPortId))
            {
                LA_TRC_ARG1 (INIT_SHUT_TRC, "Get context id "
                             "from Ifindex %d failed\n", u2AggIndex);
                LaHandleAggCreateFailed (u2AggIndex);
                return LA_FAILURE;
            }
            if (VcmMapPortToContextInHw (u4ContextId, u2AggIndex) ==
                VCM_FAILURE)
            {
                LA_TRC_ARG1 (INIT_SHUT_TRC, "Mapping aggregator %d to "
                             "context failed\n", u2AggIndex);

                LaHandleAggCreateFailed (u2AggIndex);
                return LA_FAILURE;
            }
        }
#ifdef LA_HW_TRUNK_SUPPORTED
        VlanCopyPortPropertiesToHw (u2AggIndex, u2AggIndex, u4ContextId);
        VlanCopyPortUcastPropertiesToHw (u2AggIndex);
        VlanCopyPortMcastPropertiesToHw (u2AggIndex);
        PbbCopyPortPropertiesToHw (u2AggIndex, u2AggIndex);
#ifdef CN_WANTED
        CnApiCopyPortPropertiesToHw (u2AggIndex);
#endif /* CN_WANTED */
#endif /* LA_HW_TRUNK_SUPPORTED */

        /* Fist port in port channel has become active. Syncup
         * the hwaggid returned by FsLaHwAddLinkToAggGroup fn
         * to standby. */
        LaRedSyncModifyAggPortsInfo (LA_NP_ADD_INFO, u2AggIndex,
                                     (tLaHwId) u2HwAggIndex);
    }
#endif
    UNUSED_PARAM (u2HwAggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLinkAddedToAgg                                     */
/*                                                                           */
/* Description        : This function gives the memory block back to the pool*/
/*                                                                           */
/* Input(s)           : pLaHashEntry.                                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaLinkAddedToAgg (UINT2 u2AggIndex, UINT2 u2PortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;

    /* CFA, L2Iwf indication about the link should be given only when the 
     * NPAPI operations is successfully completed. Hence for Asynchronous 
     * NPAPI, this indication will be given only in the NP CallBack function*/

    if ((LaIssGetAsyncMode (L2_PROTO_LACP) == LA_NP_ASYNC) &&
        (gLaCallSequence != LA_NP_CALLBACK_FLOW))
    {
        return LA_SUCCESS;
    }

    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (pPortEntry->u1PortOperStatus != LA_OPER_UP)
    {
        /* Port Link status is down. Link addition to Agg will 
         * be done when oper up indication for port received.
         * so return */
        return LA_SUCCESS;
    }

    if (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG)
    {
        pPortEntry->LaLacHwPortStatus = LA_HW_PORT_ADDED_IN_LAGG;
    }

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));

        /* The indication is send to applications that the port is
         * added to the port channel*/
        NotifyProtoToApp.LaNotify.u1OperStatus = LA_OPER_UP;
        NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_PORT_IN_BUNDLE_STATUS;
        NotifyProtoToApp.LaNotify.LaInfo.u2IfIndex = u2AggIndex;
        LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
        LaRedSyncModifyPortsInfo (LA_NP_ADD_PORT_SUCCESS_INFO, u2AggIndex,
                                  u2PortIndex);
    }
    LaChangeAggSpeed (pAggEntry);
    /* Make the OPer Status for the Aggregator UP */
    if (!pAggEntry->bHlIndicated)
    {
        if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
        {
            pAggEntry->u1AggOperStatus = LA_OPER_UP;
            pAggEntry->u4OperUpCount++;
            pAggEntry->i4DownReason = LA_NONE;
            LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
            LaLinkStatusNotify (pAggEntry->u2AggIndex, LA_OPER_UP, LA_ADMIN_UP,
                                LA_TRAP_PORTCHANNEL_OPER_UP);
            /* Indicate to bridge */
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\nLA: Line Protocol on "
                         "Port Channel %d changed up",
                         pAggEntry->AggConfigEntry.u2ActorAdminKey);
            LaUpdateLlPortChannelStatus (pAggEntry);
            LaL2IwfLaHLPortOperIndication (u2AggIndex, LA_OPER_UP);
            pAggEntry->bHlIndicated = LA_TRUE;
        }
    }
    else
    {
        LaUpdateLlPortChannelStatus (pAggEntry);
    }
    /*The indication is given to the higher layer modules only after
     * the Port Speed is updated in LA module as well as in CFA module.*/
    LA_UNLOCK ();
    LaL2IwfAddActivePortToPortChannel (u2PortIndex, u2AggIndex);
    LA_LOCK ();
    LA_TRC_ARG2 (CONTROL_PLANE_TRC, "SYS: Port: %u :: AggIndex: %u OPER UP "
                 "indication to BRIDGE\n", u2PortIndex, u2AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleRxCtrlFrame                                  */
/*                                                                           */
/* Description        : This  function  receives the  incoming  LA  control  */
/*                      frame from the CFA Module. Then the Pkt is enqueued  */
/*                      to LA Control taskand API exits with a return value  */
/*                      LA_CONTROL.  The  invoking  process  should  return  */
/*                      without error.                         */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the received buffer                */
/*                      u2PortIndex - Interface Index of port/interface      */
/*                      u1FrameType - Ether Frame Type                  */
/*                                                                           */
/* Output(s)          : *pu1AggPortIndex - Interface index of the portchannel*/
/*                       or port.                                            */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DATA    & (*pu2AggPortIndex) (for Data frames)    */
/*                      LA_DROP    - Drop the frame                          */
/*                      LA_CONTROL - For LACP frames                         */
/*****************************************************************************/
INT4
LaHandleRxCtrlFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                     UINT2 *pu2AggPortIndex, UINT1 u1FrameType)
{
    UNUSED_PARAM (pu2AggPortIndex);
    UNUSED_PARAM (u1FrameType);

    if (LaEnqueueControlFrame (pBuf, u2PortIndex) == LA_FAILURE)
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                     ALL_FAILURE_TRC,
                     "LaHandleRxCtrlFrame: Port %d:"
                     "Posting ControlFrame to LA Controltask Failed\n",
                     u2PortIndex);
        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | BUFFER_TRC
                         | ALL_FAILURE_TRC,
                         "LaHandleRxCtrlFrame: Port %d: "
                         "Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);
        }
    }
    return LA_CONTROL;
}

/*****************************************************************************/
/* Function Name      : LaHandleRxDataFrame                                  */
/*                                                                           */
/* Description        : This function receives the incoming Data frame       */
/*             from  CFA Module. If the received packet  is a       */
/*             data pkt meant for higher layers, this API consults  */
/*             LA Database and returns an aggregated port index     */
/*             for the physical portindex passed in the parameter   */
/*             pAggIndex. It returns with a value LA_SUCCESS in     */
/*             such case. The CFA should then pass the pkt to the   */
/*             higher layers along with the aggregated port index.  */
/*                                                                           */
/*                      If the value returned is LA_FAILURE then the port is */
/*                      not in collecting state. The CFA should discard the  */
/*                      frame. LA_FAILURE is returned for error conditions   */
/*                      also.                                             */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the received buffer                */
/*                      u2PortIndex - Interface Index of port/interface      */
/*                      u1FrameType - Ether Frame Type                  */
/*                                                                           */
/* Output(s)          : *pu1AggPortIndex - Interface index of the portchannel*/
/*                       or port.                                            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DATA    & (*pu2AggPortIndex) (for Data frames)    */
/*                      LA_DROP    - Drop the frame                          */
/*                      LA_CONTROL - For LACP frames                         */
/*****************************************************************************/

INT4
LaHandleRxDataFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                     UINT2 *pu2AggPortIndex, UINT1 u1FrameType)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaMacAddr          LaDestMacAddress;
    UINT2               u2AggIndex;
    tLaCallBackArgs     LaCallBackArgs;
    INT4                i4RetVal = LA_FAILURE;
    INT4                i4IndepMode = LA_DISABLED;

    UNUSED_PARAM (u1FrameType);

    LA_MEMSET (&LaCallBackArgs, 0, sizeof (tLaCallBackArgs));
    LA_MEMSET (&LaDestMacAddress, 0, sizeof (tLaMacAddr));

    LA_LOCK ();

    /* This is a data frame */
    LA_TRC_ARG1 (DATA_PATH_TRC,
                 "LaHandleRxDataFrame: Port %d : Data Pkt received\n",
                 u2PortIndex);

    /* Getting the pointer to the Port Entry corresponding to this port */
    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) ||
         (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)) &&
        (NULL != pPortEntry) &&
        (LaActiveDLAGCheckIsLocalPort (pPortEntry->u2PortIndex) != LA_SUCCESS))
    {
        if (L2IwfGetPortChannelForPort
            ((UINT4) pPortEntry->u2PortIndex, pu2AggPortIndex) != L2IWF_FAILURE)
        {

            LA_UNLOCK ();
            return LA_DATA;

        }
    }

    if ((pPortEntry == NULL) || (pPortEntry->LaLacpMode == LA_MODE_DISABLED))
    {
        *pu2AggPortIndex = u2PortIndex;

        LA_UNLOCK ();
        return LA_DATA;
    }

    /* If independent mode is enabled and when there is no partner Collecting
     * bit will not be set and hence pass the data traffic through the port */

    nmhGetFsLaNoPartnerIndep (&i4IndepMode);

    if ((i4IndepMode == LA_ENABLED) &&
        (pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting == LA_FALSE))
    {
        *pu2AggPortIndex = u2PortIndex;

        LA_UNLOCK ();
        return LA_DATA;
    }

    if (pPortEntry->pAggEntry != NULL)
    {
        u2AggIndex = pPortEntry->pAggEntry->u2AggIndex;

        LaCallBackArgs.u2Port = pPortEntry->u2PortIndex;
        /* Verify with Application's state before accepting incoming frames */
        i4RetVal = LaCustCallBack (LA_PKT_RX_EVENT, &LaCallBackArgs);

        if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting == LA_TRUE)
            && (i4RetVal == LA_SUCCESS) &&
            (LaCallBackArgs.u2PortState == LA_OPER_UP) &&
            (pPortEntry->pAggEntry->u1AggOperStatus == LA_OPER_UP))
        {
            *pu2AggPortIndex = u2AggIndex;

            LA_UNLOCK ();
            return LA_DATA;
        }
        else
        {
            /* If port is not in Collecting state, then discard frame.
             * Increment corresponding statistics */
            LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "LaHandleRxDataFrame: Port: %d:"
                         "COLLECTING NOT Enabled !! DISCARDING FRAME\n",
                         pPortEntry->u2PortIndex);
            if (LA_COPY_FROM_CRU_BUF
                (pBuf, LaDestMacAddress, LA_DESTADDR_OFFSET,
                 LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
            {
                LA_UNLOCK ();
                return LA_DROP;
            }
            LaIncrementStatsOnCollectorDiscard (pBuf, pPortEntry,
                                                &LaDestMacAddress);
        }
    }
    LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                 "LaHandleRxDataFrame: Port %d "
                 "FATAL ERROR :\n", u2PortIndex);

    LA_UNLOCK ();
    return LA_DROP;
}

/*****************************************************************************/
/* Function Name      : LaRegisterWithPacketHandler                          */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      LA control packets.                                  */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaRegisterWithPacketHandler ()
{
    tProtocolInfo       LaProtocolInfo;

    static tMacAddr     LaMac = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };

    /* Register with packet handler to receive control packets */
    MEMSET (&LaProtocolInfo, 0, sizeof (tProtocolInfo));

    /* Packet should come through CFA, set the callback to NULL */
    LaProtocolInfo.ProtocolFnPtr = NULL;
    LaProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    MEMCPY (&LaProtocolInfo.ProtRegTuple.MacInfo.MacAddr, &LaMac,
            ETHERNET_ADDR_SIZE);
    MEMSET (LaProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE);
    LaProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = CFA_ENET_SLOW_PROTOCOL;
    LaProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;

    /* Fill the subtypes */
    LaProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = LA_LACP_SUBTYPE;
    LaProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFFFF;
    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_LA_LACP, &LaProtocolInfo) != CFA_SUCCESS)
    {
        return LA_FAILURE;
    }

    /* Register for LA marker packets. Only the Subtype need to be changed as
     * other qualifiers are same*/
    LaProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = LA_MARKER_SUBTYPE;
    LaProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFFFF;
    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_LA_MARKER, &LaProtocolInfo) != CFA_SUCCESS)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAssignMempoolIds                                   */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaAssignMempoolIds (VOID)
{
    /* tLaCtrlMesg */
    LA_CTRLMESG_POOL_ID = LAMemPoolIds[MAX_LA_CONTROL_MESG_SIZING_ID];

    /* tLaIntfMesg */
    LA_INTFMESG_POOL_ID = LAMemPoolIds[MAX_LA_INTF_MESG_SIZING_ID];

    /* tLaLacPortEntry */
    LA_PORTENTRY_POOL_ID = LAMemPoolIds[MAX_LA_PORTS_IN_SYS_SIZING_ID];

    /* tLaLacAggEntry */
    LA_AGGENTRY_POOL_ID = LAMemPoolIds[MAX_LA_AGG_IN_SYS_SIZING_ID];

    /* tLaCacheEntry */
    LA_CACHEENTRY_POOL_ID = LAMemPoolIds[MAX_LA_CACHE_ENTRIES_SIZING_ID];

    /* tLaMarkerEntry */
    LA_MARKERENTRY_POOL_ID = LAMemPoolIds[MAX_LA_MARKER_ENTRIES_SIZING_ID];

#if defined (DLAG_WANTED) || defined (ICCH_WANTED)
    /* tLaDLAGRemoteAggEntry */
    LA_DLAGREMOTEAGGENTRY_POOL_ID =
        LADlagMemPoolIds[MAX_LA_DLAG_REMOTE_AGG_ENTRIES_SIZING_ID];

    /* tLaDLAGRemotePortEntry */
    LA_DLAGREMOTEPORTENTRY_POOL_ID =
        LADlagMemPoolIds[MAX_LA_DLAG_REMOTE_PORT_ENTRIES_SIZING_ID];

    /* tLaConsPortEntry */
    LA_DLAGCONSPORTENTRY_POOL_ID =
        LADlagMemPoolIds[MAX_LA_DLAG_CONS_PORTS_IN_SYS_SIZING_ID];
#endif
}

/*****************************************************************************/
/* Function Name      : LaHandleRecoveryTmrExpiry                            */
/*                                                                           */
/* Description        : This function is used to call the recovery mechanism */
/*                      when Recovery timer is expired                       */
/*                                                                           */
/* Input (s)          : pPortEntry - Port entry structure                    */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer.      */
/*****************************************************************************/
INT4
LaHandleRecoveryTmrExpiry (tLaLacPortEntry * pPortEntry)
{
    tLaParamInfo       *pLaParamInfo = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacAggConfigEntry *pLaLacAggConfigEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    UINT1               au1IfName[24];
    UINT1               au1Date[LA_CACHE_TABLE_SIZE];
    INT1               *piIfName = NULL;

    MEMSET (au1IfName, 0, 24);
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    MEMSET (au1Date, LA_INIT_VAL, LA_CACHE_TABLE_SIZE);

    if (pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaHandleRecoveryTmrExpiry: Null pointer is passed.\n");

        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pPortEntry->pAggEntry;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "SYS: Recovery Timer Expired for Port %d \r\n",
                 pPortEntry->u2PortIndex);
    if ((pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
        && (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED)
        && (pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaLacpActivity ==
            LA_PASSIVE) && (pPortEntry->u1InPassiveDefState == LA_TRUE))
    {
        return LA_SUCCESS;
    }

    /* when the port recovery timer expiry state,trigger the recovery mechanism
       only if the port is in one of the following
     */
    /* If the port Receive state machine is in Defaulted state. */
    if (((pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
         && (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED) &&
         (pPortEntry->u4DefaultedStateRecTrgrdCount <
          pPortEntry->u4DefaultedStateThreshold)) ||
        /* If the Actor Key is 0 because of H/w failure */
        ((pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE) &&
         (pPortEntry->LaLacActorInfo.u2IfKey == 0) &&
         (pPortEntry->u4HardwareFailureRecTrgrdCount <
          pPortEntry->u4HardwareFailureRecThreshold)) ||
        /* If the State machine stuck in same state counter
           exceeds the threshold value */
        ((pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_THRSHLD_EXCEED) &&
         (pPortEntry->u4SameStateCount >= pPortEntry->u4SameStateRecThreshold)))
    {

        if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_THRSHLD_EXCEED)
        {
            pPortEntry->u4SameStateCount = 0;
        }

        if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_THRSHLD_EXCEED)
        {
            LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateRecTime));
            LA_GET_SYS_TIME (&(pPortEntry->u4SameStateRecTime));
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) LA_SYSLOG_ID,
                          "Reason for Recovery is Same state for Port= %u",
                          pPortEntry->u2PortIndex));
            CfaCliConfGetIfName (pPortEntry->u2PortIndex, piIfName);
            LaUtilTicksToDate (pPortEntry->u4ErrStateRecTime, au1Date);
            if (pPortEntry->u1RecState == LA_SYNC_STATE)
            {
                LA_SYS_TRC_ARG5 (ALL_FAILURE_TRC,
                                 " Same state error recovery has been triggered for port-channel :%d "
                                 "%s  State - Sync "
                                 "Time of last recovery %s "
                                 " Threshold Value %d Recovery Time duration %d Action - LA_NOTIFY_RECOVERY \r\n",
                                 pAggEntry->AggConfigEntry.u2ActorAdminKey,
                                 piIfName, au1Date,
                                 pPortEntry->u4SameStateRecThreshold,
                                 (gu4RecoveryTime * SYS_TIME_TICKS_IN_A_SEC));
            }
            else if (pPortEntry->u1RecState == LA_COLLECTING_STATE)
            {
                LA_SYS_TRC_ARG5 (ALL_FAILURE_TRC,
                                 " Same state error recovery has been triggered for port-channel :%d "
                                 "%s  State - Collecting "
                                 " Time of last recovery %s "
                                 "Threshold Value %d Recovery Time duration %d Action - LA_NOTIFY_RECOVERY \r\n",
                                 pAggEntry->AggConfigEntry.u2ActorAdminKey,
                                 piIfName, au1Date,
                                 pPortEntry->u4SameStateRecThreshold,
                                 (gu4RecoveryTime * SYS_TIME_TICKS_IN_A_SEC));
            }
            else if (pPortEntry->u1RecState == LA_AGGREGATION_STATE)
            {
                LA_SYS_TRC_ARG5 (ALL_FAILURE_TRC,
                                 " Same state error recovery has been triggered for port-channel :%d "
                                 "%s  State -  Aggregation "
                                 "Time of last recovery %s "
                                 " Threshold Value %d Recovery Time duration %d Action - LA_NOTIFY_RECOVERY \r\n",
                                 pAggEntry->AggConfigEntry.u2ActorAdminKey,
                                 piIfName, au1Date,
                                 pPortEntry->u4SameStateRecThreshold,
                                 (gu4RecoveryTime * SYS_TIME_TICKS_IN_A_SEC));
            }
        }

        else if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
        {
            (pPortEntry->u4DefaultedStateRecTrgrdCount)++;
            LA_GET_SYS_TIME (&(pPortEntry->u4DefStateRecTime));
            LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateRecTime));
            CfaCliConfGetIfName (pPortEntry->u2PortIndex, piIfName);
            LaUtilTicksToDate (pPortEntry->u4ErrStateRecTime, au1Date);
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) LA_SYSLOG_ID,
                          "Reason for Recovery is Port Defaulted for Port= %u",
                          pPortEntry->u2PortIndex));
            LA_SYS_TRC_ARG5 (ALL_FAILURE_TRC,
                             "Defaulted state error recovery has been triggered for port-channel :%d port %s time of last recovery %s "
                             " Threshold Value %d Recovery Time duration %d Action - LA_NOTIFY_RECOVERY\r\n",
                             pAggEntry->AggConfigEntry.u2ActorAdminKey,
                             piIfName, au1Date,
                             pPortEntry->u4DefaultedStateThreshold,
                             (gu4RecoveryTime * SYS_TIME_TICKS_IN_A_SEC));

        }

        else if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE)
        {
            (pPortEntry->u4HardwareFailureRecTrgrdCount)++;
            LA_GET_SYS_TIME (&(pPortEntry->u4HwFailRecTime));
            LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateRecTime));
            CfaCliConfGetIfName (pPortEntry->u2PortIndex, piIfName);
            LaUtilTicksToDate (pPortEntry->u4ErrStateRecTime, au1Date);
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, (UINT4) LA_SYSLOG_ID,
                          "Reason for Recovery is H/w Failure for Port= %u",
                          pPortEntry->u2PortIndex));
            LA_SYS_TRC_ARG5 (ALL_FAILURE_TRC,
                             "Hardware failure error recovery has been triggered for port-channel :%d port %s "
                             " time of last recovery %s Threshold Value %d Recovery Time duration %d Action - LA_NOTIFY_RECOVERY\r\n",
                             pAggEntry->AggConfigEntry.u2ActorAdminKey,
                             piIfName, au1Date,
                             pPortEntry->u4HardwareFailureRecThreshold,
                             (gu4RecoveryTime * SYS_TIME_TICKS_IN_A_SEC));

        }

        pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
        pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);
        NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_RECOVERY;
        pLaParamInfo->u2IfIndex = pPortEntry->u2PortIndex;
        pLaLacAggConfigEntry = &(pAggEntry->AggConfigEntry);
        pLaParamInfo->u2IfKey = pLaLacAggConfigEntry->u2ActorAdminKey;
        LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);

        /* Increment the Recovery triggered count */
        pPortEntry->u4RecTrgdCount++;
        gu4RecTrgdCount++;

    }
    else
    {
        if (((pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
             && (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED) &&
             (pPortEntry->u4DefaultedStateRecTrgrdCount
              >= pPortEntry->u4DefaultedStateThreshold)) ||
            ((pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE) &&
             (pPortEntry->LaLacActorInfo.u2IfKey == 0) &&
             (pPortEntry->u4HardwareFailureRecTrgrdCount
              >= pPortEntry->u4HardwareFailureRecThreshold)))
        {
            switch (gu4RecThresholdExceedAction)
            {
                case LA_ACT_SHUTDOWN:
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) LA_SYSLOG_ID,
                                  "Recovery trigger for Port : %u has exceeded the threshold.Performing admin down action",
                                  pPortEntry->u2PortIndex));
                    pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);
                    NotifyProtoToApp.LaNotify.u1Action =
                        LA_ERROR_REC_THRESHOLD_EXCEEDED;
                    pLaParamInfo->u2IfIndex = pPortEntry->u2PortIndex;
                    pLaLacAggConfigEntry = &(pAggEntry->AggConfigEntry);
                    pLaParamInfo->u2IfKey =
                        pLaLacAggConfigEntry->u2ActorAdminKey;
                    LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
                    break;
                case LA_ACT_NONE:
                    pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) LA_SYSLOG_ID,
                                  "Recovery trigger for Port : %u has exceeded the threshold.Performing no action",
                                  pPortEntry->u2PortIndex));
                    break;
                default:
                    break;
            }
            if (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
            {
                pPortEntry->u4DefaultedStateRecTrgrdCount = 0;
            }
            else
            {
                pPortEntry->u4HardwareFailureRecTrgrdCount = 0;
            }
            if (pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaLacpActivity ==
                LA_PASSIVE)
            {
                pPortEntry->u1InPassiveDefState = LA_TRUE;
            }
            if (gu4RecThresholdExceedAction == LA_ACT_SHUTDOWN)
            {
                pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_DISABLED;
            }
        }

    }

    return LA_SUCCESS;
}

#endif /*_LASYS_C */
