/*****************************************************************************/
/* Copyright (C) 2014 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/* $Id: lamclag.c,v 1.36.2.1 2018/03/19 14:20:24 siva Exp $                                                    */
/*                                                                           */
/* License Aricent Inc., 2001-2014                                           */
/*****************************************************************************/
/*    FILE  NAME            : lamclag.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Distributed Link Aggregation                   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 31 Mar 2014                                    */
/*    AUTHOR                : Active-Active DLAG team                        */
/*    DESCRIPTION           : This file contains function definitions added  */
/*                            for supporting Multi chassis Link Aggregation.  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 Mar 2014/          Initial Create.                          */
/*            DISS Team                                                      */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"
/*****************************************************************************/
/* Function Name      : LaActiveMCLAGUpdateRemoteMCLAGNodeList.              */
/*                                                                           */
/* Description        : This function is called by the Active Active MC-LAG  */
/*                      Rx Message Handler function to update the remote     */
/*                      aggregator info list in Master node.                 */
/*                                                                           */
/* Input(s)           : RemoteSysMac                                         */
/*                                  - Remote System Mac address              */
/*                      u2RemoteSysPriority                                  */
/*                                  - Remote System Prioirty.                */
/*                      pu1LinearBuf                                         */
/*                                  - MC-LAG Data Received from Remote MC-LAG*/
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node  */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaActiveMCLAGUpdateRemoteMCLAGNodeList (tLaMacAddr RemoteSysMac,
                                        UINT2 u2RemoteSysPriority,
                                        UINT1 *pu1LinearBuf, INT4 i4SllAction)
{
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4PrevMtu = 0;
    UINT4               u4PrevSpeed = 0;
    UINT4               u4PrevHighSpeed = 0;
    UINT4               u4Key = 0;
    UINT2               u2Val = 0;
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1Val = 0;

    LA_MEMSET (&CfaIfInfo, 0, sizeof (CfaIfInfo));
    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGUpdateRemoteMCLAGNodeList: NULL POINTER"
                "/EMPTY BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    /* Copy Port channel Key */
    LA_GET_4BYTE (u4Key, pu1PktBuf);

    /* Get the Aggregation Entry */
    if (LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry) == LA_FAILURE)
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "Failed to get Agg Entry for key %d \n", u4Key);
    }
    if (pAggEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "LaActiveMCLAGUpdateRemoteMCLAGNodeList: Port-Channel %d not exist",
                     u4Key);
        return LA_ERR_NULL_PTR;
    }

    if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_FALSE)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGUpdateRemoteMCLAGNodeList: MC-LAG is disabled\r\n");
        return LA_SUCCESS;
    }
    /* Search Remote MC-LAG Node Info List if this is Master */
    LaDLAGGetRemoteAggEntryBasedOnMac (RemoteSysMac, pAggEntry,
                                       &pRemoteAggEntry);

    /* port channel removal indication received from remote node */

    if (i4SllAction == LA_DLAG_DEL_REM_NODE_INFO)
    {
        if (pRemoteAggEntry == NULL)
        {
            return LA_SUCCESS;
        }

        LA_TRC (CONTROL_PLANE_TRC,
                "\n LaActiveMCLAGUpdateRemoteMCLAGNodeList: Remote Node is Deleted \n");
        LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);

        /* Call the consolidation logic as this node is deleted */
        LaActiveMCLAGChkForBestConsInfo (pAggEntry);

        LaActiveMCLAGSortOnStandbyConstraints (pAggEntry);

        return LA_SUCCESS;
    }

    if (pRemoteAggEntry == NULL)
    {
        /* If Remote MC-LAG node Entry is not found 
         * - Allocate Memory for Remote MC-LAG Aggregator
         * - Initialize with Default Values */

        if (LaActiveDLAGValidateSourceMacAddress ((UINT1 *) RemoteSysMac) !=
            LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGProcessDSU:ERROR: Remote MAC is not authorized.\n");
            return LA_FAILURE;
        }

        LA_TRC (CONTROL_PLANE_TRC,
                ("LaActiveDLAGValidateSourceMacAddress:: remote mac is authorised \r\n"));

        if ((LaDLAGCreateRemoteAggregator (RemoteSysMac, &pRemoteAggEntry))
            != LA_SUCCESS)
        {
            return LA_FAILURE;
        }

        /* This is used whether a full Update PDU is sent or
           only update PDU can be sent */

        if (LaDLAGAddToRemoteAggTable (pAggEntry, pRemoteAggEntry) ==
            LA_FAILURE)
        {
            LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "Failure in adding Remote D-LAG Node entry to given aggregator remote D-LAG info table\n");
            return LA_FAILURE;
        }

        pRemoteAggEntry->pAggEntry = pAggEntry;

        LaGetSlotIdFromMacAddress (RemoteSysMac, &(LaDLAGTxInfo.u4SlotId));
        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE;
        LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
    }

    /* Update Remote MC-LAG Node System Priority */
    pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority = u2RemoteSysPriority;

    /* Reset the Remote MC-LAG Node Keep Alive Count
     * as We have received a Valid PDU */
    pRemoteAggEntry->u4DLAGKeepAliveCount = 0;

    /* Initialize update required as LA_FALE */
    pRemoteAggEntry->u1UpdateRequired = LA_FALSE;

    /* Extract MTU from PDU */
    LA_GET_2BYTE (u2Val, pu1PktBuf);

    u4PrevMtu = pRemoteAggEntry->u4Mtu;
    pRemoteAggEntry->u4Mtu = (UINT4) u2Val;

    if (u4PrevMtu != pRemoteAggEntry->u4Mtu)
    {
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;
    }

    /* Extract Speed from PDU */

    LA_GET_1BYTE (u1Val, pu1PktBuf);
    LaActiveDLAGGetPortSpeedFromPdu (u1Val, &CfaIfInfo);

    /* Reference port speed in which ports are verified for 
       aggregation */
    u4PrevSpeed = pRemoteAggEntry->u4PortSpeed;

    pRemoteAggEntry->u4PortSpeed = CfaIfInfo.u4IfSpeed;

    if (u4PrevSpeed != pRemoteAggEntry->u4PortSpeed)
    {
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;

    }

    /* Reference port high speed in which ports are verified for 
       aggregation */
    u4PrevHighSpeed = pRemoteAggEntry->u4PortHighSpeed;
    pRemoteAggEntry->u4PortHighSpeed = CfaIfInfo.u4IfHighSpeed;

    if (u4PrevHighSpeed != pRemoteAggEntry->u4PortHighSpeed)
    {
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;
    }

    pu1PktBuf += LA_AA_DLAG_DSU_RESERVED_FIELD1_LEN;

    /* Update the Remote Port List Info */
    LaActiveMCLAGUpdateRemotePortList (pRemoteAggEntry, pu1PktBuf, i4SllAction);

    /* Do not process PDU's if remote port-channel is down */
    if (pRemoteAggEntry->u1AdminStatus == CFA_IF_DOWN)
    {
        return LA_SUCCESS;
    }

    /* RBTree addtion and consolidation done here */
    LaActiveMCLAGUpdateConsolidatedList (pRemoteAggEntry, i4SllAction);

    /* Added for HA support */
#ifdef L2RED_WANTED
    /*This function sends trigger message to standby 
       to add MC-LAG remote aggregator entry  */
    LaDLAGRedSendAddRemoteAggorPortEntry (pAggEntry);
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGUpdateRemotePortList.                   */
/*                                                                           */
/* Description        : This function is called to update the DLAG           */
/*                      remote port info list.                               */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote aggregator entry whose      */
/*                                        remote port info list needs to be  */
/*                                        updated.                           */
/*                      pu1LinearBuf - MC-LAG Data Received from Remote      */
/*                                     MC-LAG node on Distributing port.     */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node/ */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaActiveMCLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                   UINT1 *pu1LinearBuf, INT4 i4SllAction)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4RemotePortIndex = 0;
    UINT4               u4LocalPortIndex = 0;
    UINT4               u4SlotId = 0;
    UINT2               u2PrevPriority = LA_INIT_VAL;
    UINT2               u2Val = 0;
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1Val = 0;
    UINT1               u1NoOfRemotePortsEntry = 0;
    UINT1               u1PrevSyncState = LA_INIT_VAL;
    UINT1               u1PrevBundleState = LA_INIT_VAL;
    UINT1               u1Count = 0;

    pu1PktBuf = pu1LinearBuf;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }
    if ((pRemoteAggEntry->pAggEntry == NULL) || (pu1LinearBuf == NULL))
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pRemoteAggEntry->pAggEntry;

    /* Copy remote port info length */
    LA_GET_1BYTE (u1NoOfRemotePortsEntry, pu1PktBuf);

    if (i4SllAction == LA_DLAG_DEL_REM_PORT_INFO)
    {
        /* Get PortId and SlotId and put it u4RemotePortIndex
         * Structure: | MSB | Slot Id | PortIndex | 
         * Bit      : 0     1         16          32  */
        LA_GET_4BYTE (u4SlotId, pu1PktBuf);
        LA_GET_4BYTE (u4RemotePortIndex, pu1PktBuf);
        u4SlotId = u4SlotId << 2;
        u4SlotId = u4SlotId | LA_MSB_BIT_SET;
        u4RemotePortIndex = u4RemotePortIndex + u4SlotId;
        /* Search Remote MC-LAG Node Info List */
        LaDLAGGetRemotePortEntry ((UINT2) u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            return LA_SUCCESS;
        }

        LaDLAGDeleteRemotePort (pRemoteAggEntry, pRemotePortEntry);

        return LA_SUCCESS;
    }

    for (u1Count = 0; u1Count < u1NoOfRemotePortsEntry; u1Count++)
    {
        /* Get PortId and SlotId and put it u4RemotePortIndex
         * Structure: | MSB | Slot Id | PortIndex | 
         * Bit      : 0     1         16          32  */
        LA_GET_4BYTE (u4SlotId, pu1PktBuf);
        LA_GET_4BYTE (u4RemotePortIndex, pu1PktBuf);
        u4SlotId = u4SlotId << 2;
        u4SlotId = u4SlotId | LA_MSB_BIT_SET;
        u4RemotePortIndex = u4RemotePortIndex + u4SlotId;

        /* Search Remote MC-LAG Node Info List */
        LaDLAGGetRemotePortEntry (u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            /* If Remote Port Entry is not found 
             * - Allocate Memory for Remote Port Entry 
             * - Initialize with Default Values */

            if ((LaDLAGCreateRemotePortEntry (u4RemotePortIndex,
                                              &pRemotePortEntry)) != LA_SUCCESS)
            {
                return LA_FAILURE;
            }

            u4SlotId = (u4SlotId & (UINT4) (~LA_MSB_BIT_SET));
            u4SlotId = u4SlotId >> 2;
            pRemotePortEntry->u4SlotId = u4SlotId;
            pRemotePortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
            LaDLAGAddToRemotePortTable (pRemoteAggEntry, pRemotePortEntry);

            /* Delete port from HL */
            LaActiveDLAGGetIfIndexFromBridgeIndex (u4RemotePortIndex,
                                                   &u4LocalPortIndex);

            LaGetPortEntry ((UINT2) u4LocalPortIndex, &pPortEntry);

            if (pPortEntry != NULL)
            {
                pPortEntry->pAggEntry = pAggEntry;
                LaHlDeletePhysicalPort (pPortEntry);
            }

            /* First check whether entry is already in RBTree */

            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);

            if (pConsPortEntry == NULL)
            {

                /* The port is inserted in RB Tree hereitself. Because
                 * We are skipping the call to LaActiveMCLAGUpdateConsolidatedList
                 * if Remote port-channel is down, to avoid consolidation. But the 
                 * addition of port entries are only done in the functions called
                 * by LaActiveMCLAGUpdateConsolidatedList . If a port 
                 * is added in port-channel after remote port-channel is down, this will 
                 * not be added in RBTree until the port-channel is up. So the addition is 
                 * done here. This will add the port's irrespective of bundle state, 
                 * So the addition of port's in respective functions which adds the 
                 * port in RBTree is no more needed. */

                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);
                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }

                pConsPortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;

                if (pRemoteAggEntry->u1AdminStatus == CFA_IF_DOWN)
                {
                    /* If remote port-channel is down, the consolidation state
                     * of port to be shown as DOWN irrespective of bundle state */
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
                }

                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);

            }
        }

        /* Remote Port Bundle State */
        LA_GET_1BYTE (u1Val, pu1PktBuf);

        /* Initialize with LA_FALSE. If any changes this is set as LA_TRUE
           So that Consolidation logic is used */

        pRemotePortEntry->u1UpdateRequired = LA_FALSE;

        u1PrevBundleState = pRemotePortEntry->u1BundleState;
        pRemotePortEntry->u1BundleState = u1Val;

        if (u1PrevBundleState != u1Val)
        {
            pRemotePortEntry->u1UpdateRequired = LA_TRUE;
        }

        /* Remote Port Sync State */
        LA_GET_1BYTE (u1Val, pu1PktBuf);

        u1PrevSyncState = pRemotePortEntry->u1SyncState;
        pRemotePortEntry->u1SyncState = u1Val;

        if (u1PrevSyncState != u1Val)
        {
            pRemotePortEntry->u1UpdateRequired = LA_TRUE;

        }

        u2PrevPriority = pRemotePortEntry->u2Priority;

        /* If the priority value of the port is changed, the
         * consolidation logic to be repeated to identiy the 
         * active port list of the port channel
         */

        LA_GET_2BYTE (u2Val, pu1PktBuf);
        pRemotePortEntry->u2Priority = u2Val;
        if (u2PrevPriority != u2Val)
        {
            pRemotePortEntry->u1UpdateRequired = LA_TRUE;
            LaActiveDLAGGetPortEntry (u4RemotePortIndex, &pConsPortEntry,
                                      pAggEntry);
            if (pConsPortEntry == NULL)
            {
                continue;
            }
            /*Removing from consolidation Tree to  change Priority 
             * else Consolidation tree will get corrupted */
            if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
            {
                LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                         pConsPortEntry);

                pConsPortEntry->u2Priority = pRemotePortEntry->u2Priority;
                /*Read the Port Entry to consolidation RBTree */

                /* Strip off MSB and right shift in case MSB is set */
                if (LA_MSB_BIT_SET == (u4SlotId & LA_MSB_BIT_SET))
                {
                    u4SlotId = (u4SlotId & (UINT4) (~LA_MSB_BIT_SET));
                    u4SlotId = u4SlotId >> 2;
                }
                pConsPortEntry->u4SlotId = u4SlotId;
                pConsPortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                LaActiveDLAGAddToConsPortListTree (pAggEntry, pConsPortEntry);
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }
            else
            {
                pConsPortEntry->u2Priority = pRemotePortEntry->u2Priority;
            }
        }
        if (pRemotePortEntry->u1UpdateRequired == LA_TRUE)
        {
            LaActiveMCLAGUpdateConsolidatedList (pRemoteAggEntry,
                                                 LA_DLAG_CPY_REM_NODE_INFO);
        }

    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGFormDSU.                                */
/*                                                                           */
/* Description        : This function is called to form a DLAG LACPDU        */
/*                      for periodic sync message or for event update message*/
/*                      from Slave nodes                                     */
/*                                                                           */
/* Input(s)           : pAggEntry - Port channel which is trying to send     */
/*                                  MC-LAG PDU.                              */
/*                      pu1LinearBuf - Pointer to the linear OUT buffer.     */
/*                      pLaDLAGTxInfo -                                      */
/*                               NULL - Periodic-sync PDU                    */
/*                               info to be embedded PDU - Event-update      */
/*                                                         MC-LAG pdu.       */
/*                     pu4DataLength - Pointer to the length of  formed PDU  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaActiveMCLAGFormDSU (tLaLacAggEntry * pAggEntry, UINT1 **ppu1LinearBuf,
                      tLaDLAGTxInfo * pLaDLAGTxInfo, UINT4 *pu4DataLength)
{
    tLaLacPortEntry    *pRefPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    tIssPortCtrlSpeed   IssPortCtrlSpeed = 0;
    tCfaIfInfo          IfInfo;
    UINT4               u4Val = 0;
    UINT4               u4Count = 0;
    UINT4               u4DataPadLen = 0;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT2               u2Val = 0;
    UINT1               u1BundleState = 0;
    UINT1               u1Val = 0;
    UINT1               u1PortCount = 0;
    UINT1               u1PortChannelCount = 0;
    UINT1               u1DSUType = 0;
    UINT1              *pu1PortInfoLenField = NULL;
    UINT1              *pu1NoOfAggInfoLenField = NULL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               au1SwitchMacAddr[LA_MAC_ADDRESS_SIZE] = { 0 };
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bIsSyncPdu = LA_FALSE;

    if ((pAggEntry == NULL) && (pLaDLAGTxInfo != NULL))
    {
        /* For Sync PDU pAggentry and pLaDLAGTxInfo will be 
         * passed as NULL. Return NULL ptr only if Event PDU is called
         * with pAggEntry as NULL */
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGFormDSU: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pu1BpduStart = pu1BpduEnd = gLaGlobalInfo.gau1DLAGPdu;

    /* Copy Destination Address */
    LA_MEMCPY (pu1BpduEnd, &gLaSlowProtAddr, LA_MAC_ADDRESS_SIZE);
    pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    /* Get Source Address from Distributing port from CFA */

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        while (u4Count <
               LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount)
        {
            OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                     GlobalDLAGDistributingOperPortList,
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }

            u4Count++;
            if (LaCfaGetIfInfo (u4DLAGDistributingIfIndex, &IfInfo) !=
                CFA_SUCCESS)
            {
                u4DLAGDistributingIfIndex++;
                continue;
            }
            /* distributing port is valid */
            break;
        }
        /* Copy Source Address */
        LA_MEMCPY (pu1BpduEnd, IfInfo.au1MacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;
        LA_MEMCPY (au1SwitchMacAddr, gLaGlobalInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);
    }
    else
    {
        /* Fill the switch Mac as source Mac */
        LaActiveDLAGGetSwitchMacAddress (au1SwitchMacAddr);
        LA_MEMCPY (pu1BpduEnd, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;
    }

    /* Copy Protocol Type : SLOW */
    u2Val = (UINT2) LA_SLOW_PROT_TYPE;
    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

    /* Copy Protocol Sub Type : LACP */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACPDU_SUB_TYPE);

    /* Copy Version Number */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACP_VERSION);

    /* Fill TLV Type = DSU */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_DLAG_DSU_INFO);

    /* If Null then Periodic Sync PDU, set DSUType to zero, If Not then
     * Event Update PDU copy the DSUType from pLaDLAGTxInfo to PDU */

    if (pLaDLAGTxInfo != NULL)
    {
        u1DSUType = pLaDLAGTxInfo->u1DSUType;
    }
    else
    {
        bIsSyncPdu = LA_TRUE;
    }

    /* Fill DSU Sub type */
    LA_PUT_1BYTE (pu1BpduEnd, u1DSUType);

    /* Fill DSU Info Lenght : [From DSU subtype - System Prioirty] */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_DSU_INFO_LEN);

    /* Copy System ID : source mac */
    LA_MEMCPY (pu1BpduEnd, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE);
    pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    /* Copy System Priority */
    u2Val = gLaGlobalInfo.LaSystem.u2SystemPriority;
    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

    /* Mark the No Of port-channel info filed
     * incase of Sync PDU which will be filled 
     * latter when scanning all the port-channels.
     */
    if (bIsSyncPdu == LA_TRUE)
    {
        LaGetNextAggEntry (NULL, &pAggEntry);
    }

    pu1NoOfAggInfoLenField = pu1BpduEnd;
    /* Increment the field */
    pu1BpduEnd += 1;

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    do
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            /* Event PDU/Sync PDU */
            if ((pAggEntry->u1ConfigPortCount == 0) && (bIsSyncPdu == LA_TRUE))
            {
                LaGetNextAggEntry (pAggEntry, &pAggEntry);
                continue;
            }

            u1PortChannelCount++;

            /* Copy Port Channel Key */
            u4Val = (UINT4) pAggEntry->AggConfigEntry.u2ActorAdminKey;
            LA_PUT_4BYTE (pu1BpduEnd, u4Val);

            /* Fill the MTU of port-channel */
            u2Val = (UINT2) pAggEntry->u4Mtu;
            LA_PUT_2BYTE (pu1BpduEnd, u2Val);

            /* Fill the reference speed of port-channel */
            LaActiveDLAGFillPortSpeedInPdu (pAggEntry, &IssPortCtrlSpeed);

            u1Val = (UINT1) IssPortCtrlSpeed;
            LA_PUT_1BYTE (pu1BpduEnd, u1Val);

            /* Skip Reserved Field */
            pu1BpduEnd += LA_AA_DLAG_DSU_RESERVED_FIELD1_LEN;

            /* Marker Pointer for Number of Remote Port Info field 
             * This field is filled at the end of this function */
            pu1PortInfoLenField = pu1BpduEnd;
            pu1BpduEnd += 1;

            /* If Tx Event-update message to be sent is
             * for Port detatched from Aggregator then
             * Only detached port index will be sent */
            if ((u1DSUType == LA_DLAG_HIGH_PRIO_EVENT_PORT_DETATCH_FROM_AGG) ||
                (u1DSUType == LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG))
            {
                u1PortCount++;

                u4Val = 0;
                /* Copy Slot Info */
                LaActiveDLAGGetSlotIdFromBridgeIndex (pLaDLAGTxInfo->
                                                      u4PortIndex, &u4Val);
                LA_PUT_4BYTE (pu1BpduEnd, u4Val);

                /* Copy Port Info */
                u4Val = 0;
                LaActiveDLAGGetBridgeIndexFromIfIndex (pLaDLAGTxInfo->
                                                       u4PortIndex, &u4Val);
                LA_PUT_4BYTE (pu1BpduEnd, u4Val);

                break;
            }
            else if ((u1DSUType == LA_DLAG_HIGH_PRIO_EVENT_INTF_UP) ||
                     (u1DSUType == LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN))
            {
                /* If port channel is admin down/up , just indicate
                   master to update port-isolation table. No need to
                   fill oper status because the handling can be achieved
                   using port-Channel admin key and event type alone.
                 */

            }
            else
            {
                /* Can be a Database Update Type. */
                /* Copy Port List info */
                pRefPortEntry = pNextPortEntry = NULL;
                LaGetNextAggPortEntry (pAggEntry, pRefPortEntry,
                                       &pNextPortEntry);
                while (pNextPortEntry != NULL)
                {
                    u1PortCount++;
                    /* Copy Port Info */
                    u4Val = 0;
                    LaActiveDLAGGetSlotIdFromBridgeIndex ((UINT4)
                                                          pNextPortEntry->
                                                          u2PortIndex, &u4Val);
                    LA_PUT_4BYTE (pu1BpduEnd, u4Val);

                    u4Val = 0;
                    LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4)
                                                           pNextPortEntry->
                                                           u2PortIndex, &u4Val);

                    LA_PUT_4BYTE (pu1BpduEnd, u4Val);

                    u4Val = (UINT4) pNextPortEntry->u2PortIndex;
                    if ((LaGetPortBundleState (u4Val, &u1BundleState)) !=
                        LA_SUCCESS)
                    {
                        return LA_FAILURE;
                    }

                    LA_PUT_1BYTE (pu1BpduEnd, u1BundleState);

                    CfaGetIfOperStatus ((UINT4) pNextPortEntry->u2PortIndex,
                                        &u1Val);

                    LA_PUT_1BYTE (pu1BpduEnd, u1Val);
                    /* Fill Priority */
                    u2Val = pNextPortEntry->LaLacActorInfo.u2IfPriority;
                    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

                    pRefPortEntry = pNextPortEntry;
                    pNextPortEntry = NULL;
                    if ((NULL != pAggEntry) && (bIsSyncPdu == LA_TRUE))
                    {
                        pAggEntry->u4DLAGPeriodicSyncPduTxCount++;
                    }
                    LaGetNextAggPortEntry (pAggEntry, pRefPortEntry,
                                           &pNextPortEntry);
                }
            }

            LA_PUT_1BYTE (pu1PortInfoLenField, u1PortCount);
            /* Reset port counter */
            u1PortCount = 0;
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    while ((pAggEntry != NULL) && (bIsSyncPdu == LA_TRUE));

    LA_PUT_1BYTE (pu1NoOfAggInfoLenField, u1PortChannelCount);

    /* Put 4 byte as FCS */
    u4Val = 0;
    LA_PUT_4BYTE (pu1BpduEnd, u4Val);

    *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

    if (*pu4DataLength < LA_LACPDU_SIZE)
    {
        u4DataPadLen = LA_LACPDU_SIZE - (*pu4DataLength);
        LA_MEMSET (pu1BpduEnd, LA_INIT_VAL, u4DataPadLen);
        pu1BpduEnd = pu1BpduEnd + u4DataPadLen;
        *pu4DataLength = LA_LACPDU_SIZE;
    }

    *ppu1LinearBuf = gLaGlobalInfo.gau1DLAGPdu;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGProcessMCLAGPdu.                        */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      to processs Active-Active MC-LAG periodic-sync       */
/*                      and event-update PDU's                               */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving MC-LAG PDU.    */
/*                                                                           */
/*                      pu1LinearBuf - MC-LAG Data Received from Remote      */
/*                      MC-LAG node on Distributing port.                    */
/*                                                                           */
/*                      u4ByteCount - Length of the pu1LinearBuf.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaActiveMCLAGProcessMCLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf,
                              UINT4 u4ByteCount)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            LaSenderMac;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaBufChainHeader  *pMsgBuf = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaPortList        *pLocalDistributePortList = NULL;
    tLaMacAddr          RemoteMacAddr = { 0 };
    UINT1               au1SwitchMacAddr[LA_MAC_ADDRESS_SIZE] = { 0 };
    INT4                i4SllAction = LA_DLAG_CPY_REM_NODE_INFO;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    UINT4               u4Key = 0;
    UINT2               u2RemotePriority = 0;
    UINT1               u1DSUInfoLength = 0;
    UINT1               u1DSUSubType = 0;
    UINT1               u1NoOfPortChannel = 0;
    UINT1              *pu1PktBuf = NULL;
    BOOL1               bResult = OSIX_FALSE;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (BUFFER_TRC, "LaActiveMCLAGProcessMCLAGPdu: Linear Buffer "
                " Pointer is NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "LaActiveMCLAGProcessMCLAGPdu: MC-LAG PDU processing...");

    /* Starting from DSU subtype */
    pu1PktBuf = pu1LinearBuf + LA_DLAG_DSU_SUBTYPE_OFFSET + 1;

    /* Copy DSU sub type */
    LA_GET_1BYTE (u1DSUSubType, pu1PktBuf);

    /* Copy DSU info length */
    LA_GET_1BYTE (u1DSUInfoLength, pu1PktBuf);

    if ((u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE) ||
        (u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE))
    {

        if (u1DSUInfoLength != LA_AA_DLAG_ADD_DEL_INFO_LEN)
        {
            return LA_FAILURE;

        }
    }
    else
    {
        if (u1DSUInfoLength != LA_AA_DLAG_DSU_INFO_LEN)
        {
            return LA_FAILURE;

        }
        /* Copy MC-LAG PDU sender MAC */
        LA_MEMCPY (LaSenderMac, pu1PktBuf, LA_MAC_ADDRESS_SIZE);

        if (LA_USE_DIST_PORT_OR_NOT
            (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) ==
            LA_TRUE)
        {
            LA_MEMCPY (au1SwitchMacAddr, gLaGlobalInfo.LaSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
        }
        else
        {
            LaActiveDLAGGetSwitchMacAddress (au1SwitchMacAddr);
        }
        /* Added to drop the packets received from the same mac address */
        if (LA_MEMCMP (LaSenderMac, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE) == 0)
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveMCLAGProcessMCLAGPdu: Dropping DSU PDU Received. Pdu received from Same System");
            return LA_SUCCESS;
        }
    }

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        /* Allocating memory for the local port list */
        pLocalDistributePortList =
            (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

        /* If NULL, return */
        if (pLocalDistributePortList == NULL)
        {
            LA_TRC (ALL_FAILURE_TRC,
                    "Error in Allocating memory for bitlist\n");
            return LA_SUCCESS;
        }

        /* Initializing the local distributing port list */
        MEMSET (pLocalDistributePortList, 0, sizeof (tLaPortList));

        /* Copying the DLAG distributing port list to a local port list */
        LA_MEMCPY (pLocalDistributePortList,
                   LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingOperPortList,
                   sizeof (tLaPortList));

        /* If the received port is not distributing port, drop the PDU received */
        OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                 GlobalDLAGDistributingOperPortList,
                                 u2RecvdPortIndex, LA_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            /* Reset the local distributing port list for the received port index */
            OSIX_BITLIST_RESET_BIT ((*pLocalDistributePortList),
                                    u2RecvdPortIndex, sizeof (tLaPortList));
        }
        else
        {
            FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveMCLAGProcessMCLAGPdu: Dropping DSU PDU Received. Pdu received on non distribute"
                    "port\n");
            return LA_SUCCESS;
        }

        /* Scan the local distributing port list and forward the MC-LAG PDU 
         * on the remaining ports in the port list*/

        while (u4Count <
               (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount - 1))
        {
            OSIX_BITLIST_IS_BIT_SET ((*pLocalDistributePortList),
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            if ((pMsgBuf = LA_ALLOC_CRU_BUF (u4ByteCount, 0)) == NULL)
            {
                LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "LaActiveMCLAGProcessMCLAGPdu: Buffer Allocation failed\n");
                FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
                return LA_FAILURE;
            }
            /* Copy from Linear buffer to CRU buffer */
            LA_COPY_OVER_CRU_BUF (pMsgBuf, pu1LinearBuf, 0, u4ByteCount);

            /* Forward the MC-LAG PDU */
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveMCLAGProcessMCLAGPdu: Handing over LACPDU to Lower Layer...\n");
            LaHandOverOutFrame (pMsgBuf, (UINT2) u4DLAGDistributingIfIndex,
                                NULL);
            u4DLAGDistributingIfIndex++;
            u4Count++;
        }

        FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);

    }

    if (u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE)
    {
        /* Copy Port channel No's */
        LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

        LA_GET_4BYTE (u4Key, pu1PktBuf);
        /* Get the Aggregation Entry */
        if (LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry) == LA_FAILURE)
        {

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Failed to get Agg Entry for key %d \n", u4Key);
        }
        if ((pAggEntry == NULL) || (u1NoOfPortChannel != 1))
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "LaActiveMCLAGProcessMCLAGPdu: Dropping DSU PDU Received. Port-Channel =%d not exist \n",
                         u4Key);
            return LA_FAILURE;
        }

        if (LaIcchGetRolePlayed () == LA_ICCH_SLAVE)
        {
            /* Process Add-Del PDU and program H/w */
            LaActiveMCLAGGetAddDeletePortFromDSU (pAggEntry, pu1PktBuf,
                                                  LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE);
            pAggEntry->u4DLAGEventUpdatePduRxCount++;
        }
        return LA_SUCCESS;
    }

    if (u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        LaActiveMCLAGProcessPeriodicUpdateDSU (pu1PktBuf);
        return LA_SUCCESS;
    }

    if (LaIcchGetRolePlayed () == LA_ICCH_SLAVE)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGProcessMCLAGPdu: System Role is Slave. Dropping Sync/Event PDU \n");
        return LA_FAILURE;
    }

    /* Get the Remote System MAC and Priority */

    LA_MEMCPY (RemoteMacAddr, pu1PktBuf, LA_MAC_ADDRESS_SIZE);

    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    LA_GET_2BYTE (u2RemotePriority, pu1PktBuf);

    /* MC-LAG Periodic sync PDU's, Just Update the Remote Node List
     * reset the keep alive counters maintained for remote Nodes */

    if (u1DSUSubType == 0)
    {
        /* If it is a Sync PDU we need to loop for the whole PDU. 
         */
        LaActiveMCLAGProcessSyncPDU (RemoteMacAddr, u2RemotePriority, pu1PktBuf,
                                     i4SllAction);

        return LA_SUCCESS;
    }

    /*  
     *  In case of Event PDU's get the Key here itself. Then keep the pointer to the *
     *  same place where it was there. So that LaActiveMCLAGUpdateRemoteMCLAGNodeList  *
     *  will take start taking the Key from that place.
     */

    /* Copy Port channel No's */
    LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

    /* Copy Port channel Key */
    LA_GET_4BYTE (u4Key, pu1PktBuf);

    /* Get the Aggregation Entry */
    if (LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry) == LA_FAILURE)
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "Failed to get Agg Entry for key %d \n", u4Key);
    }

    if ((pAggEntry == NULL) || (u1NoOfPortChannel != 1))
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "LaActiveMCLAGProcessMCLAGPdu: Dropping DSU PDU Received. Port-Channel =%d not exist \n",
                     u4Key);
        return LA_ERR_NULL_PTR;
    }

    /* Move back the pointer to 4 byte */

    pu1PktBuf -= 4;

    /* MC-LAG Event-update PDU's */
    switch (u1DSUSubType)
    {
            /* High priority event update message */
        case LA_DLAG_HIGH_PRIO_EVENT_PO_UP:

            LaActiveMCLAGUpdateRemoteMCLAGNodeList (RemoteMacAddr,
                                                    u2RemotePriority, pu1PktBuf,
                                                    i4SllAction);
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_PO_DOWN:

            i4SllAction = LA_DLAG_DEL_REM_NODE_INFO;

            /* Search Remote MC-LAG Node Info List */
            LaDLAGGetRemoteAggEntryBasedOnMac (RemoteMacAddr, pAggEntry,
                                               &pRemoteAggEntry);

            LaActiveMCLAGUpdateRemoteMCLAGNodeList (RemoteMacAddr,
                                                    u2RemotePriority, pu1PktBuf,
                                                    i4SllAction);
            break;

        case LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG:    /* Fall through */
            i4SllAction = LA_DLAG_DEL_REM_PORT_INFO;
        case LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE:

            LaActiveMCLAGUpdateRemoteMCLAGNodeList (RemoteMacAddr,
                                                    u2RemotePriority, pu1PktBuf,
                                                    i4SllAction);
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_INTF_UP:

            LaMCLAGUpdateRemoteLagAdminStatus (RemoteMacAddr, pAggEntry,
                                               CFA_IF_UP);
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN:

            LaMCLAGUpdateRemoteLagAdminStatus (RemoteMacAddr, pAggEntry,
                                               CFA_IF_DOWN);
            break;

        default:
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveMCLAGProcessMCLAGPdu: Invalid MC-LAG Event update PDU"
                    " Received dropping.\n");
            return LA_FAILURE;
    }

    /* MC-LAG valid Event update message */
    pAggEntry->u4DLAGEventUpdatePduRxCount++;
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGFormAddDeleteDSU                        */
/*                                                                           */
/* Description        : This function is called to form a Add delete MC-LAG  */
/*                      PDU from the RBTrees. First Delete Ports are filled  */
/*                      followed by Add ports. After filling it in PDU       */
/*                      Recently changed flag is set to FALSE.               */
/*                                                                           */
/* Input(s)           : pAggEntry     - Port channel which is to send        */
/*                                      MC-LAG PDU.                          */
/*                      pu1LinearBuf  - Pointer to the linear OUT buffer.    */
/*                      pLaDLAGTxInfo - Info to be embedded PDU - Add-Delete */
/*                                      MC-LAG PDU.                          */
/*                      pu4DataLength - Pointer to length of the formed PDU. */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGFormAddDeleteDSU (tLaLacAggEntry * pAggEntry,
                               UINT1 **ppu1LinearBuf,
                               tLaDLAGTxInfo * pLaDLAGTxInfo,
                               UINT4 *pu4DataLength)
{
    tCfaIfInfo          IfInfo;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pDelPortEntry = NULL;
    UINT4               u4Val = 0;
    UINT4               u4Count = 0;
    UINT4               u4DataPadLen = 0;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT2               u2Val = 0;
    UINT1              *pu1TmpField = NULL;
    UINT1              *pu1NoOfAggInfoLenField = NULL;
    UINT1              *pu1PiMaskInfo = NULL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               u1DSUType = 0;
    UINT1               u1AddPort = 0;
    UINT1               u1DeletePort = 0;
    UINT1               u1PortChannelCount = 0;
    UINT1               au1SwitchMacAddr[LA_MAC_ADDRESS_SIZE] = { 0 };
    UINT1               u1RecentlyChanged = LA_FALSE;
    UINT1               u1PiMask = VLAN_PI_NOT_REQUIRED;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bIsPeriodicUpdatePdu = LA_FALSE;

    if ((pAggEntry == NULL) && (pLaDLAGTxInfo == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGFormAddDeleteDSU: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "\n Entering LaActiveMCLAGFormAddDeleteDSU \n");

    LA_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pu1BpduStart = pu1BpduEnd = gLaGlobalInfo.gau1DLAGPdu;

    /* Copy Destination Address */
    LA_MEMCPY (pu1BpduEnd, &gLaSlowProtAddr, LA_MAC_ADDRESS_SIZE);
    pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    /* Get Source Address from Distributing port from CFA */

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        while (u4Count <
               LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount)
        {
            OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                     GlobalDLAGDistributingOperPortList,
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            u4Count++;

            if (LaCfaGetIfInfo (u4DLAGDistributingIfIndex, &IfInfo) !=
                CFA_SUCCESS)
            {
                u4DLAGDistributingIfIndex++;
                continue;
            }

            /* distributing port is valid */
            break;
        }
        /* Copy Source Address */
        LA_MEMCPY (pu1BpduEnd, IfInfo.au1MacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;
    }
    else
    {

        /* Fill the switch Mac as source Mac */
        LaActiveDLAGGetSwitchMacAddress (au1SwitchMacAddr);
        LA_MEMCPY (pu1BpduEnd, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    }

    /* Copy Protocol Type : SLOW */
    u2Val = (UINT2) LA_SLOW_PROT_TYPE;
    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

    /* Copy Protocol Sub Type : LACP */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACPDU_SUB_TYPE);

    /* Copy Version Number */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACP_VERSION);

    /* Fill TLV Type = DSU */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_DLAG_DSU_INFO);

    /* Fill ADD_DEL Type */
    if (pLaDLAGTxInfo->u1DSUType ==
        LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        u1DSUType = LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE;
        bIsPeriodicUpdatePdu = LA_TRUE;
    }
    else if ((pLaDLAGTxInfo->u1DSUType == LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE))
    {
        u1DSUType = LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE;
        bIsPeriodicUpdatePdu = LA_TRUE;
        u1RecentlyChanged = LA_TRUE;
    }
    else
    {
        u1DSUType = LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE;
        u1RecentlyChanged = LA_TRUE;
    }

    LA_PUT_1BYTE (pu1BpduEnd, u1DSUType);

    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_ADD_DEL_INFO_LEN);

    if (bIsPeriodicUpdatePdu == LA_TRUE)
    {
        LaGetNextAggEntry (NULL, &pAggEntry);
    }

    pu1NoOfAggInfoLenField = pu1BpduEnd;
    /* Increment the field */
    pu1BpduEnd += 1;

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    do
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            u1DeletePort = 0;
            u1AddPort = 0;

            if (bIsPeriodicUpdatePdu == LA_TRUE)
            {
                RBTreeCount (pAggEntry->LaDLAGConsInfoTable.
                             LaDLAGConsolidatedList, &u4Count);

                if (u4Count == 0)
                {
                    /* No ports are in that Agg Entry consolidated List
                       Skip it */
                    LaGetNextAggEntry (pAggEntry, &pAggEntry);
                    continue;
                }
            }

            u1PortChannelCount++;

            /* Copy Port Channel Key */
            u4Val = (UINT4) pAggEntry->AggConfigEntry.u2ActorAdminKey;
            LA_PUT_4BYTE (pu1BpduEnd, u4Val);

            /*  Update Delete PortList DSU Type */
            LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_DSU_DELETE_PORT_LIST);

            /* Temp ptr for storing port no field which is filled later */
            pu1TmpField = pu1BpduEnd;
            pu1BpduEnd += 1;

            if (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
            {
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n ADD_DEL_UPDATE_PDU: Delete ports:");
            }
            else if (pLaDLAGTxInfo->u1DSUType ==
                     LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE)
            {
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n FULL_UPDATE_ON_NEW_NODE:Delete ports:");
            }

            /********************************************************************
             * The Event update PDU can be sent for
             * 1. recpetion of new node addition indication
             * 2. change in the existing port channel properties.
             *
             * For the first case (New node addition), all the port-channel Add
             * member ports can be sent in the PDU. This can be indentified by the macro
             * LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE
             *
             * In the second scenerio, only the updat port related information alone
             * can be sent. This can be indentified by the macro LA_AA_DLAG_ADD_DEL_UPDATE_ONLY
             *
             * IN the third scenerio, only Add port list info will be sent for all 
             * port-channel available which is periodic update and can be identified
             * by macro LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE.
             *
             **********************************************************************/

            if (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
            {
                LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
                while (pConsPortEntry != NULL)
                {
                    if ((pConsPortEntry->u1PortStateFlag !=
                         LA_AA_DLAG_PORT_IN_ADD)
                        && (pConsPortEntry->u1RecentlyChanged == LA_TRUE))
                    {
                        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d, ",
                                     pConsPortEntry->u4PortIndex);
                        u4Val = pConsPortEntry->u4PortIndex;
                        if ((u4Val & LA_MSB_BIT_SET) == LA_MSB_BIT_SET)
                        {
                            /* Strip out the Slot Id */
                            LA_PUT_1BYTE (pu1BpduEnd, LA_ICCH_SLAVE);
                            /* Strip out the port Id */
                            u4Val = u4Val & LA_PORT_ID_MASK;
                        }
                        else
                        {
                            LA_PUT_1BYTE (pu1BpduEnd, LA_ICCH_MASTER);
                            u4Val = u4Val | LA_MSB_BIT_SET;
                        }

                        u1RecentlyChanged = LA_TRUE;
                        LA_PUT_4BYTE (pu1BpduEnd, u4Val);
                        /* ReSet the RecentChangedFlag */
                        pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                        /* Check if port is removed from peer port-channel 
                           Delete from Cons entry and release the memory allocated for it */

                        LaActiveMCLAGMasterProcessAddDeleteLinks (pAggEntry,
                                                                  pConsPortEntry->
                                                                  u4PortIndex,
                                                                  (UINT1)
                                                                  LA_AA_DLAG_PORT_IN_DELETE);

                        if (pConsPortEntry->u1PortStateFlag ==
                            LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY)
                        {
                            pDelPortEntry = pConsPortEntry;
                            LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                                          &pConsPortEntry,
                                                          pAggEntry);
                            LaActiveDLAGRemovePortEntry (pAggEntry,
                                                         pDelPortEntry);
                            LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                                     pDelPortEntry);
                            LaActiveDLAGDeleteConsPortEntry (pDelPortEntry);

                            pDelPortEntry = NULL;
                            u1DeletePort++;
                            continue;
                        }
                        u1DeletePort++;
                    }
                    LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                                  &pConsPortEntry, pAggEntry);
                }
            }
            LA_TRC (CONTROL_PLANE_TRC, "\n");
            LA_PUT_1BYTE (pu1TmpField, (UINT1) u1DeletePort);

            /*  Update Add PortList DSU Type */
            LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_DSU_ADD_PORT_LIST);
            /* Temp ptr for storing port no field which is filled later */
            pu1TmpField = pu1BpduEnd;
            pu1BpduEnd += 1;
            pu1PiMaskInfo = pu1BpduEnd;
            pu1BpduEnd += 1;
            /* Loop for RBTrees */

            if (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
            {
                LA_TRC (CONTROL_PLANE_TRC, "\n ADD_DEL_UPDATE_PDU: Add ports:");
            }
            else if (pLaDLAGTxInfo->u1DSUType ==
                     LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
            {
                LA_TRC (CONTROL_PLANE_TRC, "\n PERIODIC_UPDATE :Add ports:");
            }
            else
            {
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n FULL_UPDATE_ON_NEW_NODE: Add ports:");
            }

            LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pConsPortEntry,
                                                      pAggEntry);
            while (pConsPortEntry != NULL)
            {
                if ((pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
                    && (pConsPortEntry->u1RecentlyChanged == LA_TRUE))
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d, ",
                                 pConsPortEntry->u4PortIndex);
                    u4Val = pConsPortEntry->u4PortIndex;
                    if (LaActiveDLAGCheckIsLocalPort
                        ((UINT2) (pConsPortEntry->u4PortIndex)) != LA_SUCCESS)
                    {
                        /* Strip out the Slot Id */
                        LA_PUT_1BYTE (pu1BpduEnd, LA_ICCH_SLAVE);
                        /* Strip out the port Id */
                        u4Val = u4Val & LA_PORT_ID_MASK;
                    }
                    else
                    {
                        LA_PUT_1BYTE (pu1BpduEnd, LA_ICCH_MASTER);
                        u4Val = u4Val | LA_MSB_BIT_SET;
                    }
                    LA_PUT_4BYTE (pu1BpduEnd, u4Val);
                    /* ReSet the RecentChangedFlag */
                    pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                    u1RecentlyChanged = LA_TRUE;
                    LaActiveMCLAGMasterProcessAddDeleteLinks (pAggEntry,
                                                              pConsPortEntry->
                                                              u4PortIndex,
                                                              (UINT1)
                                                              LA_AA_DLAG_PORT_IN_ADD);
                    u1AddPort++;
                }
                /* Fill all the ports when Initial Update */
                if ((pLaDLAGTxInfo->u1DSUType ==
                     LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE)
                    || (pLaDLAGTxInfo->u1DSUType ==
                        LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE))
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d, ",
                                 pConsPortEntry->u4PortIndex);
                    u4Val = pConsPortEntry->u4PortIndex;
                    if (LaActiveDLAGCheckIsLocalPort
                        ((UINT2) (pConsPortEntry->u4PortIndex)) != LA_SUCCESS)
                    {
                        /* Strip out the Slot Id */
                        LA_PUT_1BYTE (pu1BpduEnd, LA_ICCH_SLAVE);
                        /* Strip out the port Id */
                        u4Val = u4Val & LA_PORT_ID_MASK;
                    }
                    else
                    {
                        LA_PUT_1BYTE (pu1BpduEnd, LA_ICCH_MASTER);
                        u4Val = u4Val | LA_MSB_BIT_SET;
                    }

                    LA_PUT_4BYTE (pu1BpduEnd, u4Val);
                    if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
                    {
                        LaActiveMCLAGMasterProcessAddDeleteLinks (pAggEntry,
                                                                  pConsPortEntry->
                                                                  u4PortIndex,
                                                                  (UINT1)
                                                                  LA_AA_DLAG_PORT_IN_ADD);
                        u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                    u1AddPort++;
                }

                LaActiveDLAGGetNextPortEntryFromConsTree (pConsPortEntry,
                                                          &pConsPortEntry,
                                                          pAggEntry);
            }
            if ((u1RecentlyChanged == LA_TRUE)
                && (LA_MCLAG_PEER_EXISTS == LA_TRUE))
            {
                LaMCLAGCheckMasterSlaveLinkAvail (pAggEntry, &u1PiMask);
                LaVlanUpdatePortIsolationForMcLag (pAggEntry->u2AggIndex,
                                                   u1PiMask);
            }

            LA_PUT_1BYTE (pu1PiMaskInfo, u1PiMask);
            LA_PUT_1BYTE (pu1TmpField, (UINT1) u1AddPort);

            if ((u1AddPort == 0) && (u1DeletePort == 0)
                && (bIsPeriodicUpdatePdu == LA_FALSE))
            {
                /* If no ports are there no need to send Update 
                   message */
                return LA_FAILURE;
            }

            if (bIsPeriodicUpdatePdu == LA_FALSE)
            {
#ifdef NPAPI_WANTED
                /* Update the actual status of port-channel to h/w */
                LaUpdatePortChannelStatusToHw (pAggEntry->u2AggIndex,
                                               LA_PORT_DISABLED, LA_TRUE);
#endif
            }
            else if (NULL != pAggEntry)
            {
                pAggEntry->u4DLAGPeriodicSyncPduTxCount++;
            }
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);

    }
    while ((pAggEntry != NULL) && (bIsPeriodicUpdatePdu == LA_TRUE));

    /* Fill port-channel no's */
    LA_PUT_1BYTE (pu1NoOfAggInfoLenField, u1PortChannelCount);

    /* Put 4 byte as FCS */
    u4Val = 0;

    LA_PUT_4BYTE (pu1BpduEnd, u4Val);

    *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

    if (*pu4DataLength < LA_LACPDU_SIZE)
    {
        u4DataPadLen = LA_LACPDU_SIZE - (*pu4DataLength);
        LA_MEMSET (pu1BpduEnd, LA_INIT_VAL, u4DataPadLen);
        pu1BpduEnd = pu1BpduEnd + u4DataPadLen;
        *pu4DataLength = LA_LACPDU_SIZE;
    }

    *ppu1LinearBuf = gLaGlobalInfo.gau1DLAGPdu;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  LaActiveMCLAGGetAddDeletePortFromDSU                */
/*                                                                           */
/* Description        :  This function is called to process the AddDelete    */
/*                       Active MC-LAG PDU and subsequently calls the H/w    */
/*                       functions to program the ports.                     */
/*                                                                           */
/* Input(s)           : pAggEntry -  Aggregator entry for which the          */
/*                                    PDU received                           */
/*                      pu1PktBuf -  MC-LAG Data Received from Remote MC-LAG */
/*                      node on Distributing port. with LA Header stripped.  */
/*                                                                           */
/*                      u1PduType - Type of PDU.Can be Update /Periodic PDU. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGGetAddDeletePortFromDSU (tLaLacAggEntry * pAggEntry,
                                      UINT1 *pu1PktBuf, UINT1 u1PduType)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pDelPortEntry = NULL;
    UINT4               u4PortIndex = 0;
    UINT4               u4PortBrgIndex = 0;
    UINT1               u1DSUAddSubType = 0;
    UINT1               u1DSUDeleteSubType = 0;
    UINT1               u1AddCount = 0;
    UINT1               u1DeleteCount = 0;
    UINT1               u1PiMask = 0;
    UINT1               u1State = 0;
    INT4                i4RetStatus = 0;

#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = 0;
#endif

    if ((pAggEntry == NULL) || (pu1PktBuf == NULL))
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_FALSE)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC,
                     "MC-LAG is disabled on the port channel %d\r\n",
                     pAggEntry->AggConfigEntry.u2ActorAdminKey);
        return LA_SUCCESS;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "\n LaActiveMCLAGGetAddDeletePortFromDSU: Add/Del PDU received \n");

    /* Get Delete PortList field */
    LA_GET_1BYTE (u1DSUDeleteSubType, pu1PktBuf);

    if (u1DSUDeleteSubType != LA_AA_DLAG_DSU_DELETE_PORT_LIST)
    {
        return LA_FAILURE;
    }

    /* Get Delete Port Count */
    LA_GET_1BYTE (u1DeleteCount, pu1PktBuf);

    /* Delete Ports */
    while (u1DeleteCount != 0)
    {
        LA_GET_1BYTE (u1State, pu1PktBuf);
        LA_GET_4BYTE (u4PortBrgIndex, pu1PktBuf);

        LaActiveDLAGGetIfIndexFromBridgeIndex (u4PortBrgIndex, &u4PortIndex);

        if ((u4PortIndex != 0) && (u1State == LA_ICCH_SLAVE))
        {
            LaGetPortEntry ((UINT2) u4PortIndex, &pPortEntry);

            i4RetStatus = LaActiveDLAGCheckIsLocalPort ((UINT2) u4PortIndex);

            if ((i4RetStatus == LA_SUCCESS) &&
                (pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry) &&
                (pPortEntry->u1DLAGMasterAck == LA_TRUE))
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Master Removed Port =%d \n",
                             u4PortIndex);
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n Setting the MasterAck and Sync to FALSE \n");

                pPortEntry->u1DLAGMasterAck = LA_FALSE;
                /* Don't Set Peer Ready to FALSE */

                /*  If distributing is set to TRUE then only a Sync bit reset
                 *  is needed.
                 */
                if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
                    LA_TRUE)
                {

                    pPortEntry->LaLacActorInfo.LaLacPortState.
                        LaSynchronization = LA_FALSE;
                }

                /* Convey this info with Peer */
                pPortEntry->NttFlag = LA_TRUE;
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

                LaActiveMCLAGHwControl (pPortEntry,
                                        LA_HW_EVENT_DISABLE_DISTRIBUTOR);

            }
        }

        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (u4PortBrgIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry != NULL)
        {
            LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
            LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);
        }

#ifdef NPAPI_WANTED
        if ((LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            && (u4PortBrgIndex != 0) && (u1State == LA_ICCH_SLAVE))
        {
            if (LaFsLaHwDlagRemoveLinkFromAggGroup (pAggEntry->u2AggIndex,
                                                    (UINT2) u4PortBrgIndex) !=
                FNP_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                             "FsLaHwDlagRemoveLinkFromAggGroup: Failed to "
                             "configure remote ports in H/w for: port-"
                             "channel: %u\n", pAggEntry->u2AggIndex);
            }
        }

        if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
        {
            /* Since Port is removed from H/w Change the Status */
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

            if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
            {
                LaHlCreatePhysicalPort (pPortEntry);

                /* Make the port status as Forwarding in H/w */
                LaUpdatePortChannelStatusToHw (pPortEntry->u2PortIndex, LA_TRUE,
                                               LA_STANDBY_INDICATION);
            }

        }
#else
        if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
        {
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

            if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
            {
                LaHlCreatePhysicalPort (pPortEntry);
            }
        }
#endif
        u1DeleteCount--;
    }

    /* Get Add PortList field */
    LA_GET_1BYTE (u1DSUAddSubType, pu1PktBuf);

    if (u1DSUAddSubType != LA_AA_DLAG_DSU_ADD_PORT_LIST)
    {
        return LA_FAILURE;
    }

    /* Get Add Port Count */
    LA_GET_1BYTE (u1AddCount, pu1PktBuf);
    LA_GET_1BYTE (u1PiMask, pu1PktBuf);
    /* Get mask info */

    if (u1PiMask != VLAN_PI_NOT_REQUIRED)
    {
        if ((u1PiMask == VLAN_PI_MASK) || (u1PiMask == VLAN_PI_UNMASK))
        {
            LaVlanUpdatePortIsolationForMcLag (pAggEntry->u2AggIndex, u1PiMask);
        }
    }
    /* Add Ports */
    while (u1AddCount != 0)
    {
        LA_GET_1BYTE (u1State, pu1PktBuf);
        LA_GET_4BYTE (u4PortBrgIndex, pu1PktBuf);

        LaActiveDLAGGetIfIndexFromBridgeIndex (u4PortBrgIndex, &u4PortIndex);

        if ((u4PortIndex != 0) && (u1State == LA_ICCH_SLAVE))
        {
            /* Currently all ports are programmed in H/w */
            LaGetPortEntry ((UINT2) u4PortIndex, &pPortEntry);

            i4RetStatus = LaActiveDLAGCheckIsLocalPort ((UINT2) u4PortIndex);

            if ((i4RetStatus == LA_SUCCESS) &&
                (pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry) &&
                (pPortEntry->u1DLAGMasterAck == LA_FALSE))
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "\n Master Acknowledged Port =%d \n", u4PortIndex);
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n Setting the MasterAck and Sync to TRUE \n");
                pPortEntry->u1DLAGMasterAck = LA_TRUE;

                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_TRUE;

                pPortEntry->NttFlag = LA_TRUE;
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
                /* LaMuxStateAttached (pPortEntry); */
                LaActiveMCLAGHwControl (pPortEntry,
                                        LA_HW_EVENT_ENABLE_DISTRIBUTOR);
            }
        }

        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (u4PortBrgIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Create Port Entry and Insert in PortList and Consolidated Tree */
            i4RetStatus = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                           u4PortBrgIndex, 0);

            if (i4RetStatus == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }
            if (NULL != pConsPortEntry)
            {
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
                pConsPortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                /*Adding the newly created Port to the PortList */
                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                pConsPortEntry->u1NodeState = u1State;
            }
#ifdef NPAPI_WANTED
            if ((LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                && (u4PortBrgIndex != 0) && (u1State == LA_ICCH_SLAVE))
            {
                if (LaFsLaHwDlagAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                                   (UINT2) u4PortBrgIndex,
                                                   &u2HwAggIndex) !=
                    FNP_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                                 "FsLaHwDlagAddLinkToAggGroup: Failed to "
                                 "configure remote ports in H/w for: port-"
                                 "channel: %u\n", pAggEntry->u2AggIndex);
                }
            }
#endif

        }

        /* Set the Recent change as TRUE */
        if (NULL != pConsPortEntry)
        {
            pConsPortEntry->u1RecentlyChanged = LA_TRUE;
        }

        u1AddCount--;
    }

    if (u1PduType == LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        pConsPortEntry = NULL;
        LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                      &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
            {
                pConsPortEntry->u1RecentlyChanged = LA_FALSE;

            }
            else if (pConsPortEntry->u1RecentlyChanged == LA_FALSE)
            {
                pDelPortEntry = pConsPortEntry;

                /* Search Remote MC-LAG Node Info List */

                i4RetStatus =
                    LaActiveDLAGCheckIsLocalPort ((UINT2) pConsPortEntry->
                                                  u4PortIndex);
#ifdef NPAPI_WANTED
                if ((LA_SUCCESS == i4RetStatus) &&
                    (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE) &&
                    (pConsPortEntry->u4PortIndex != 0))
                {
                    i4RetStatus = LaFsLaHwDlagRemoveLinkFromAggGroup
                        (pAggEntry->u2AggIndex,
                         (UINT2) pConsPortEntry->u4PortIndex);
                    if (i4RetStatus != FNP_SUCCESS)
                    {
                        LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                                     "LaHwDlagRemoveLinkFromAggGroup: Failed to "
                                     "configure remote ports in H/w for: port-"
                                     "channel: %u\n", pAggEntry->u2AggIndex);
                    }
                }

                if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
                {
                    /* Since Port is removed from H/w Change the Status */
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

                    if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
                    {
                        LaHlCreatePhysicalPort (pPortEntry);

                        /* Make the port status as Forwarding in H/w */
                        LaUpdatePortChannelStatusToHw (pPortEntry->u2PortIndex,
                                                       LA_TRUE,
                                                       LA_STANDBY_INDICATION);
                    }

                }
#else
                if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
                {
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

                    if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
                    {
                        LaHlCreatePhysicalPort (pPortEntry);
                    }
                }
#endif

                LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                              &pConsPortEntry, pAggEntry);
                LaActiveDLAGRemovePortEntry (pAggEntry, pDelPortEntry);
                LaActiveDLAGDeleteConsPortEntry (pDelPortEntry);
                pDelPortEntry = NULL;
                continue;

            }

            LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                          &pConsPortEntry, pAggEntry);

        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGUpdateConsolidatedList                   */
/*                                                                           */
/* Description        : This function will check whether RBTree update is    */
/*                      needed. If needed it will move the port entries to   */
/*                      correspoding RBTrees. It will call the consolidation */
/*                      logic and will check whether Update PDU is required  */
/*                      or not.                                              */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - RemoteAggEntry for which the Sync/ */
/*                                         Event PDU received                */
/*                      i4SllAction - If Remote port is deleted              */
/*                                    this will have DEL_REMOTE_PORT so that */
/*                                    consolidation explicitly takes place   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGUpdateConsolidatedList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                     INT4 i4SllAction)
{
    tLaDLAGSpeedPropInfo TmpProperties;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1SortingNeeded = LA_FALSE;

    LA_MEMSET (&TmpProperties, 0, sizeof (tLaDLAGSpeedPropInfo));

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    /* Get Aggregator entry from Remote */
    pAggEntry = pRemoteAggEntry->pAggEntry;

    if (pAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC, "Aggregation pointer is " "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }
    /* Check for Speed/MTU properties with Consolidated Info */
    TmpProperties.u4Mtu = pRemoteAggEntry->u4Mtu;
    TmpProperties.u4ConsRefSpeed = pRemoteAggEntry->u4PortSpeed;
    TmpProperties.u4ConsRefHighSpeed = pRemoteAggEntry->u4PortHighSpeed;
    LA_MEMCPY (TmpProperties.BestInfoMac,
               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    i4RetVal = LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry, TmpProperties);

    if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
    {
        LaActiveMCLAGUpdateTreeOnPropSame (pRemoteAggEntry, &u1SortingNeeded);
    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
    {
        LaActiveMCLAGUpdateTreeOnPropGreater (pRemoteAggEntry,
                                              &u1SortingNeeded);

    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
    {
        LaActiveMCLAGUpdateTreeOnPropLesser (pRemoteAggEntry, &u1SortingNeeded);
    }
    else
    {
        LaActiveMCLAGChkForBestConsInfo (pAggEntry);
        u1SortingNeeded = LA_TRUE;

    }

    if ((u1SortingNeeded == LA_TRUE)
        || (i4SllAction == LA_DLAG_DEL_REM_PORT_INFO))
    {
        /* If sorting needed call consolidation logic */
        LaActiveMCLAGSortOnStandbyConstraints (pAggEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGSortOnStandbyConstraints                 */
/*                                                                           */
/* Description        : This function is used to sort the port entries       */
/*                      present in Add and Standby ports RBTree and          */
/*                      select the maximum ports allowed based on priority.  */
/*                                                                           */
/* Input(s)           : pAggEntry- Master Aggregator entry for which         */
/*                                 consolidation  neede                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/

INT4
LaActiveMCLAGSortOnStandbyConstraints (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pTmpConsPortEntry = NULL;
    tLaBoolean          bUpdateDSURequired = LA_FALSE;
    UINT4               u4Count = 0;
    UINT4               u4Index = 0;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (tLaDLAGTxInfo));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGSortOnStandbyConstraints: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    /* Get the number of ports in Add List Tree */
    LA_TRC (CONTROL_PLANE_TRC,
            "\n --------------------------------------------------------------------*\n\tConsolidated List");
    /* Add Ports which are in ADD and STANDBY state to Consolidation Tree 
     * To Sort it according to Priority */
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        if ((pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
            || (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_STANDBY))
        {
            /*Add Port Entry to Consolidation Tree */
            LaActiveDLAGAddToConsPortListTree (pAggEntry, pConsPortEntry);

            /* If Port is moved from STANDBY to ADD state Set the recently 
             * changed Flag */
            if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_STANDBY)
            {
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }

            /* All Ports in Consolidation Tree should be in ADD state */
            pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
        }
        else
        {
            /* Port is down now .Remove from Consolidation tree */
            LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry, pConsPortEntry);
        }

        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    /* Get the Total number of Port Entry in Consolidation Logic */
    RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
                 &u4Count);

    /* If Total Number of Port Entry in consolidation is less or 
     * Equal to Maximum allowed Ports, then Consolidation is over*/
    if (u4Count < pAggEntry->u1MaxPortsToAttach)
    {
        if (gLaGlobalInfo.u4TraceOption != 0)
        {
            LaDLAGActivePrintAllTreePorts (pAggEntry);
        }

        bUpdateDSURequired = LaActiveDLAGChkAddDelDSURequired (pAggEntry);

        if (bUpdateDSURequired == LA_TRUE)
        {
            LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_ADD_DEL_UPDATE_ONLY;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }

        return LA_SUCCESS;
    }

    /* Port Entries in Consolidation Tree is more than Maximum allowed Ports,
     * Remove the extra Ports according to Prioirty*/
    pTmpConsPortEntry = NULL;
    LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pTmpConsPortEntry,
                                              pAggEntry);

    /* Scanning the Port Entry from consolidation Tree till Maximum 
     * allowed Ports, No need to do anything as they are already in ADD
     * state, remaining Ports needs to be moved to STANDY state */
    for (u4Index = 1; u4Index <= pAggEntry->u1MaxPortsToAttach; u4Index++)
    {
        /*GetNext Consolidation Entry */
        if (pTmpConsPortEntry != NULL)
        {
            LaActiveDLAGGetNextPortEntryFromConsTree (pTmpConsPortEntry,
                                                      &pTmpConsPortEntry,
                                                      pAggEntry);
        }
        else
        {
            LA_TRC (CONTROL_PLANE_TRC, "\n Consolidated Tree Corrupted\n");
            return LA_FAILURE;
        }
    }

    /* Moving Ports to Standby Tree and Removing from Conslidation Tree */
    for (u4Index = (UINT4) (pAggEntry->u1MaxPortsToAttach + 1);
         u4Index <= u4Count; u4Index++)
    {
        if (pTmpConsPortEntry != NULL)
        {
            /* Incase Port is now standby previous it was in ADD state
             * Set the Recent change as TRUE so that remove indication is sent */
            pTmpConsPortEntry->u1RecentlyChanged = LA_TRUE;

            pTmpConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_STANDBY;
            LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                     pTmpConsPortEntry);

            /*GetNext Consolidation Entry */
            LaActiveDLAGGetNextPortEntryFromConsTree (pTmpConsPortEntry,
                                                      &pTmpConsPortEntry,
                                                      pAggEntry);
        }
    }

    if (gLaGlobalInfo.u4TraceOption != 0)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "\n -----------------------------------------------------------------\n");
        LA_TRC (CONTROL_PLANE_TRC, "\n After consolidation Ports\n");

        LaDLAGActivePrintAllTreePorts (pAggEntry);
        LA_TRC (CONTROL_PLANE_TRC,
                "\n -----------------------------------------------------------------\n");
    }

    bUpdateDSURequired = LaActiveDLAGChkAddDelDSURequired (pAggEntry);

    if (bUpdateDSURequired == LA_TRUE)
    {
        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_ADD_DEL_UPDATE_ONLY;
        LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGMasterUpdateConsolidatedList            */
/*                                                                           */
/* Description        : This function will be called by Master if Master     */
/*                      has local port channel member ports.                 */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which Master ports   */
/*                                  participating in LAG are there           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGMasterUpdateConsolidatedList (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGSpeedPropInfo TmpProperties;
    INT4                i4RetVal = 0;

    LA_MEMSET (&TmpProperties, 0, sizeof (tLaDLAGSpeedPropInfo));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGMasterUpdateConsolidatedList"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    TmpProperties.u4Mtu = pAggEntry->u4Mtu;
    TmpProperties.u4ConsRefSpeed = pAggEntry->u4PortSpeed;
    TmpProperties.u4ConsRefHighSpeed = pAggEntry->u4PortHighSpeed;
    LA_MEMCPY (TmpProperties.BestInfoMac, gLaGlobalInfo.LaSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    /*
     * If Master has local ports and it is partipating the port channel then
     * the state machine is running for the local ports and the result of it
     * the consolidation logic will be updated the update message can be transmitted
     * from the Master in terms of ADD and/or Delete PortList to the Slaves.
     */

    i4RetVal = LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry, TmpProperties);

    if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
    {
        LaActiveMCLAGMasterUpdateTreeOnPropSame (pAggEntry);

    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
    {
        LaActiveMCLAGMasterUpdateTreeOnPropGreater (pAggEntry);
    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
    {
        LaActiveMCLAGMasterUpdateTreeOnPropLesser (pAggEntry);
    }
    else
    {
        LaActiveMCLAGChkForBestConsInfo (pAggEntry);
    }

    LaActiveMCLAGSortOnStandbyConstraints (pAggEntry);

    return LA_SUCCESS;
}

/**********************************************************************************/
/* Function Name      : LaActiveMCLAGChkForBestConsInfo                            */
/*                                                                                */
/* Description        : This function is called whenever                          */
/*                      - The Node is Deleted                                     */
/*                      - there is a change in the received information which     */
/*                        triggers to calculate the best information              */
/*                                                                                */
/*                      The Logic is to loop for all the remote entries to find   */
/*                      the best one. If best among the remote is then compare    */
/*                      with the existing Master information provided there is    */
/*                      local port information present for the Master. .          */
/*                                                                                */
/*                                                                                */
/* Input(s)           : pAggEntry - Aggregator entry                              */
/*                                                                                */
/*                                                                                */
/* Output(s)          : None                                                      */
/*                                                                                */
/*                                                                                */
/* Return Value(s)    : LA_SUCCESS - On Success                                   */
/*                      LA_FAILURE - On failure                                   */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed               */
/*                                                                                */
/**********************************************************************************/

INT4
LaActiveMCLAGChkForBestConsInfo (tLaLacAggEntry * pAggEntry)
{

    tLaDLAGRemoteAggEntry *pTmpRemAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pCheckRemAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pBestRemAggEntry = NULL;
    tLaDLAGSpeedPropInfo TmpProperties;
    INT4                i4RetVal = 0;
    UINT1               u1SortingNeeded = LA_FALSE;
    UINT1               u1NoOfRemoteEntries = 0;
    tLaBoolean          bAllNodesEqual = LA_FALSE;

    LA_MEMSET (&TmpProperties, 0, sizeof (tLaDLAGSpeedPropInfo));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGChkForBestConsInfo: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;

    }

    LA_TRC (CONTROL_PLANE_TRC, "\n Selecting Best Info out of All Nodes \n");

    /* Select for best port-channel */
    /* Get the first and second remote Aggregation Entry */
    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pTmpRemAggEntry);
    /* Assume Best is first */
    pBestRemAggEntry = pTmpRemAggEntry;
    pCheckRemAggEntry = pTmpRemAggEntry;

    u1NoOfRemoteEntries++;

    /*
     * The best information is selected by doing comparision on the remote
     * information stored in the Master.
     */

    while (pTmpRemAggEntry != NULL)
    {
        pTmpRemAggEntry = NULL;
        /* Get Next entry of current entry */
        LaDLAGGetNextRemoteAggEntry (pAggEntry, pCheckRemAggEntry,
                                     &pTmpRemAggEntry);
        /* Assign this as checking entry */
        pCheckRemAggEntry = pTmpRemAggEntry;
        if (pCheckRemAggEntry == NULL)
        {
            /*exit the loop with Best Agg entry */
            if (u1NoOfRemoteEntries == 1)
            {
                /* Only one remote present */
                bAllNodesEqual = LA_TRUE;
            }

            pTmpRemAggEntry = NULL;
            continue;
        }
        if ((pBestRemAggEntry->u4PortSpeed == pCheckRemAggEntry->u4PortSpeed) &&
            (pBestRemAggEntry->u4PortHighSpeed ==
             pCheckRemAggEntry->u4PortHighSpeed)
            && (pBestRemAggEntry->u4Mtu == pCheckRemAggEntry->u4Mtu))
        {
            /* All are having same Info */
            /* Keep Best as first. Retain it and get the next node  */
            bAllNodesEqual = LA_TRUE;
            continue;
        }

        if ((pBestRemAggEntry->u4PortSpeed > pCheckRemAggEntry->u4PortSpeed) ||
            ((pBestRemAggEntry->u4PortSpeed == pCheckRemAggEntry->u4PortSpeed)
             && (pBestRemAggEntry->u4PortHighSpeed >
                 pCheckRemAggEntry->u4PortHighSpeed))
            ||
            ((pBestRemAggEntry->u4PortSpeed == pCheckRemAggEntry->u4PortSpeed)
             && (pBestRemAggEntry->u4PortHighSpeed ==
                 pCheckRemAggEntry->u4PortHighSpeed)
             && (pBestRemAggEntry->u4Mtu > pCheckRemAggEntry->u4Mtu)))
        {
            /*Best is first. Retain it and get the next node  */
        }
        else
        {
            /* second is best for checking */
            pBestRemAggEntry = pCheckRemAggEntry;
        }
        u1NoOfRemoteEntries++;
    }

    if (((pAggEntry->u1ConfigPortCount != 0) &&
         /* If Master particpates in port-channel check with Aggentry MTU,Speed
            only if atleast one port is configured
            so that it will have some speed */
         (pBestRemAggEntry != NULL)
         && (pBestRemAggEntry->u4PortSpeed == pAggEntry->u4PortSpeed)
         && (pBestRemAggEntry->u4PortHighSpeed == pAggEntry->u4PortHighSpeed)
         && (pBestRemAggEntry->u4Mtu == pAggEntry->u4Mtu))
        && (bAllNodesEqual == LA_TRUE))
    {
        bAllNodesEqual = LA_TRUE;
        LA_TRC (CONTROL_PLANE_TRC, "\n All Nodes having same MTU \n");

        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
            pBestRemAggEntry->u4PortSpeed;
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
            pBestRemAggEntry->u4PortHighSpeed;
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
            pBestRemAggEntry->u4Mtu;
        LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.BestInfoMac,
                   pBestRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        LaActiveDLAGPrintBestConsInfo (pAggEntry);

        LA_TRC (CONTROL_PLANE_TRC, "\n Cons Info changed by Best\n");

        /* Loop for all port-channels and select the ports */
        LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pTmpRemAggEntry);
        while (pTmpRemAggEntry != NULL)
        {
            pTmpRemAggEntry->u1UpdateRequired = LA_TRUE;
            LaActiveMCLAGUpdateTreeOnPropSame (pTmpRemAggEntry,
                                               &u1SortingNeeded);
            pTmpRemAggEntry->u1UpdateRequired = LA_FALSE;
            LaDLAGGetNextRemoteAggEntry (pAggEntry, pTmpRemAggEntry,
                                         &pTmpRemAggEntry);
        }

        /* Call for Master ports also */
        LaActiveMCLAGMasterUpdateTreeOnPropSame (pAggEntry);
    }

    else
    {

        bAllNodesEqual = LA_FALSE;
        if ((pBestRemAggEntry != NULL)
            && (((pAggEntry->u1ConfigPortCount != 0) &&
                 ((pBestRemAggEntry->u4PortSpeed > pAggEntry->u4PortSpeed)
                  || (pBestRemAggEntry->u4PortHighSpeed >
                      pAggEntry->u4PortHighSpeed)
                  || (pBestRemAggEntry->u4Mtu > pAggEntry->u4Mtu))) ||
                (pAggEntry->u1ConfigPortCount == 0)))
        {
            /* Update the Consolidated Info from Best Remote Entry */
            LA_TRC (CONTROL_PLANE_TRC, "\n Remote Node Is Best \n");
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
                pBestRemAggEntry->u4PortSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
                pBestRemAggEntry->u4PortHighSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                pBestRemAggEntry->u4Mtu;
            LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                       BestInfoMac,
                       pBestRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
        }
        else
        {
            /* Master itself best */
            LA_TRC (CONTROL_PLANE_TRC, "\n Master is best \n");
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
                pAggEntry->u4PortSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
                pAggEntry->u4PortHighSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                pAggEntry->u4Mtu;
            LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                       BestInfoMac, gLaGlobalInfo.LaSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);

        }

        LaActiveDLAGPrintBestConsInfo (pAggEntry);

        LA_TRC (CONTROL_PLANE_TRC, "\n Cons Info changed by Best Node\n");

        LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pTmpRemAggEntry);
        while (pTmpRemAggEntry != NULL)
        {
            TmpProperties.u4Mtu = pTmpRemAggEntry->u4Mtu;
            TmpProperties.u4ConsRefSpeed = pTmpRemAggEntry->u4PortSpeed;
            TmpProperties.u4ConsRefHighSpeed = pTmpRemAggEntry->u4PortHighSpeed;
            LA_MEMCPY (TmpProperties.BestInfoMac,
                       pTmpRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
            i4RetVal =
                LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry,
                                                       TmpProperties);

            /* Race contion can happen ? */
            pTmpRemAggEntry->u1UpdateRequired = LA_TRUE;

            if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
            {
                LaActiveMCLAGUpdateTreeOnPropSame (pTmpRemAggEntry,
                                                   &u1SortingNeeded);
            }
            else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
            {
                LaActiveMCLAGUpdateTreeOnPropGreater (pTmpRemAggEntry,
                                                      &u1SortingNeeded);

            }
            else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
            {
                LaActiveMCLAGUpdateTreeOnPropLesser (pTmpRemAggEntry,
                                                     &u1SortingNeeded);
            }

            pTmpRemAggEntry->u1UpdateRequired = LA_FALSE;
            LaDLAGGetNextRemoteAggEntry (pAggEntry, pTmpRemAggEntry,
                                         &pTmpRemAggEntry);
        }

        /* Check for Master ports */

        TmpProperties.u4Mtu = pAggEntry->u4Mtu;
        TmpProperties.u4ConsRefSpeed = pAggEntry->u4PortSpeed;
        TmpProperties.u4ConsRefHighSpeed = pAggEntry->u4PortHighSpeed;
        LA_MEMCPY (TmpProperties.BestInfoMac,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        i4RetVal =
            LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry, TmpProperties);

        if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
        {
            LaActiveMCLAGMasterUpdateTreeOnPropSame (pAggEntry);

        }
        else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
        {
            LaActiveMCLAGMasterUpdateTreeOnPropGreater (pAggEntry);
        }
        else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
        {
            LaActiveMCLAGMasterUpdateTreeOnPropLesser (pAggEntry);
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGUpdateTreeOnPropSame                     */
/*                                                                           */
/* Description        : This function will be called by Remote Consolidated  */
/*                      List when receiving a DLAG PDU with same MTU/Speed   */
/*                      with the best Info. It will check whether any port   */
/*                      properties changed(such as priority/bundle state)and */
/*                      moves the ports to corresponding state accordingly   */
/*                      and sets the SortingReq as TRUE. So that             */
/*                      consolidation takes place.                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote Aggregator for which PDU    */
/*                                        received.                          */
/*                      pu1SortingReq - Whether Selection Logic to be        */
/*                                        triggered.                         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu1SortingReq - As TRUE or FALSE                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveMCLAGUpdateTreeOnPropSame (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                   UINT1 *pu1SortingReq)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGUpdateTreeOnPropSame "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Remote Called */
    pAggEntry = pRemoteAggEntry->pAggEntry;

    LA_TRC (CONTROL_PLANE_TRC, "\n Remote MTU or Speed Same \n");

    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);

    while (pRemotePortEntry != NULL)
    {
        if ((pRemotePortEntry->u1UpdateRequired == LA_TRUE)
            || (pRemoteAggEntry->u1UpdateRequired == LA_TRUE))
        {
            *pu1SortingReq = LA_TRUE;    /* Whether consolidation needed */

            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n u1UpdateRequired is TRUE =%d\n",
                         pRemotePortEntry->u4PortIndex);
            /* First check whether entry is already in RBTree */
            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);

            if (pConsPortEntry == NULL)
            {
                /* Create Port Entry and Insert in PortList and Consolidated Tree */
                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);
                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }

                if (NULL != pConsPortEntry)
                {
                    pConsPortEntry->u4InstanceId =
                        LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                    pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                    LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
            }

            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Up In bundle =%d\n",
                             pRemotePortEntry->u4PortIndex);
                if (pConsPortEntry != NULL)
                {
                    /* If it is in ADD state no need to set the flag */
                    if (pConsPortEntry->u1PortStateFlag !=
                        LA_AA_DLAG_PORT_IN_ADD)
                    {
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
                }
            }
            else
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                             pRemotePortEntry->u4PortIndex);
                if (pConsPortEntry != NULL)
                {
                    /* Down in bundle is Present in PortList RBTree in ADD State,
                       move it to DELETE State. */
                    if (pConsPortEntry->u1PortStateFlag ==
                        LA_AA_DLAG_PORT_IN_ADD)
                    {
                        /* Changing Port State From ADD to DELETE */
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

                }
            }
        }                        /* UpdateRequired ends */

        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }                            /* while ends */

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGMasterUpdateTreeOnPropSame               */
/*                                                                           */
/* Description        : This function is called by Master if Master          */
/*                      particpates in port-channel when MTU/Speed is same   */
/*                      with best info.It will check whether any port        */
/*                      properties changed(such as priority/Bundle state)and */
/*                      moves the ports to corresponding State accordingly   */
/*                      and after that consolidation takes place.            */
/* Input(s)           : pAggEntry -  Aggregator for which ports are added    */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveMCLAGMasterUpdateTreeOnPropSame (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;
    UINT4               u4BrgPortIndex = 0;
    UINT1               u1BundleState = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGMasterUpdateTreeOnPropSame: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "Master MTU Or Speed Same:\n ");
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

    while (pPortEntry != NULL)
    {
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);
        LaGetPortBundleState ((UINT4) pPortEntry->u2PortIndex, &u1BundleState);
        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Create Port Entry and Insert in PortList and Consolidated Tree */
            i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                        u4BrgPortIndex,
                                                        pPortEntry->
                                                        LaLacActorInfo.
                                                        u2IfPriority);

            if (i4RetVal == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            if (NULL != pConsPortEntry)
            {
                pConsPortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                /*Adding the newly created Port to the PortList */
                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }
        }

        if (u1BundleState == LA_PORT_UP_IN_BNDL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Up In bundle =%d\n",
                         pPortEntry->u2PortIndex);
            if (pConsPortEntry != NULL)
            {
                /* If it is in ADD state no need to set the flag */
                if (pConsPortEntry->u1PortStateFlag != LA_AA_DLAG_PORT_IN_ADD)
                {
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
            }
        }
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                         pPortEntry->u2PortIndex);
            if (pConsPortEntry != NULL)
            {
                /* Down in bundle is Present in PortList RBTree in ADD State,
                   move it to DELETE State. */
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    /* Changing Port State From ADD to DELETE */
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
            }
        }

        /* Continue to next port */
        pConsPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);

    }

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGUpdateTreeOnPropGreater                   */
/*                                                                            */
/* Description        : This function will be called by Remote Consolidated   */
/*                      List when receiving a DLAG PDU with Greater MTU/Speed */
/*                      with the Best Info. It will move all the ports from   */
/*                      Add & Standby State to delete State. After that only  */
/*                      Ports that belongs to this port-channel are moved to  */
/*                      Add State and sets the SortingReq as TRUE. So that    */
/*                      consolidation takes place.                            */
/*                                                                            */
/*                                                                            */
/* Input(s)           : pRemoteAggEntry - Remote Aggregator for which PDU     */
/*                                        received.                           */
/*                      pu1SortingReq - Whether Selection Logic to be         */
/*                                        triggered.                          */
/*                                                                            */
/*                                                                            */
/* Output(s)          : pu1SortingReq - As TRUE or FALSE                      */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS - On Success                               */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed           */
/*                                                                            */
/******************************************************************************/

INT4
LaActiveMCLAGUpdateTreeOnPropGreater (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                      UINT1 *pu1SortingReq)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;
    UINT1               u1BundleState = LA_PORT_DOWN;
    UINT2               u2FlagPortCount = 0;
    UINT2               u2FlagDeletePortCount = 0;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGUpdateTreeOnPropGreater"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Remote Called */
    pAggEntry = pRemoteAggEntry->pAggEntry;

    LA_TRC (CONTROL_PLANE_TRC, "\n MTU or Speed greater \n");
    LA_TRC (CONTROL_PLANE_TRC, "\n Move all the ports to Delete State \n");
    /* Get the Port Entry and move all Port State to DELETE */
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        /* Incase Port is in STANDBY state no need to give indication,
         * If it is in DELETE State its not changed at all*/
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
        {
            /* Set the RecentChangedFlag,
             * Using this flag,Ports which are already in ADD state
             * can be identified, if it belongs to the same Greater Aggregation,
             * Port needs to be moved back to ADD state, resetting the 
             * recently changed flag */
            pConsPortEntry->u1RecentlyChanged = LA_TRUE;
        }
        pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    pConsPortEntry = NULL;
    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);

    while (pRemotePortEntry != NULL)
    {
        if ((pRemotePortEntry->u1UpdateRequired == LA_TRUE)
            || (pRemoteAggEntry->u1UpdateRequired == LA_TRUE))
        {
            *pu1SortingReq = LA_TRUE;
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "\n Update Required is TRUE Port=%d\n",
                         pRemotePortEntry->u4PortIndex);

            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);

            if (pConsPortEntry == NULL)
            {
                /* Port Entry is not present, creating new Port Entry */
                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);
                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }

                if (NULL != pConsPortEntry)
                {
                    pConsPortEntry->u4InstanceId =
                        LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                    pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                    /*Adding the newly created Port to the PortList */
                    LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
            }

            /* Incase of Bundle State is UP Port needs to be in ADD State,
             * If Bundle State is DOWN, Port Should be in DELETE State */
            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Up =%d\n",
                             pRemotePortEntry->u4PortIndex);

                if (pConsPortEntry != NULL)
                {
                    /* Set the RecentChangedFlag and move Port to ADD State */
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

                    /* Port State can Either be in DELETE/NOT_IN_ANY_LIST/STANDBY.
                     * Reverse the Recently Changed Flag, If Port State is 
                     * NOT_IN_ANY_LIST Flag definitely will be false and needs to be set,
                     * If Port State is in DELETE if its previuos state is ADD it will be set,
                     * else It will be false, so reverse it */
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;

                }
            }
            else
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Down =%d\n",
                             pRemotePortEntry->u4PortIndex);
                if ((pConsPortEntry != NULL) &&
                    (pConsPortEntry->u1PortStateFlag ==
                     LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

                }
            }

        }
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }
    pConsPortEntry = NULL;
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        u2FlagPortCount++;
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
        {
            u2FlagDeletePortCount++;
        }
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }
    if (u2FlagPortCount == u2FlagDeletePortCount)
    {
        pConsPortEntry = NULL;
        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            if (LaActiveDLAGCheckIsLocalPort
                ((UINT2) pConsPortEntry->u4PortIndex) == LA_SUCCESS)

            {
                LaGetPortBundleState (pConsPortEntry->u4PortIndex,
                                      &u1BundleState);
                if (u1BundleState == LA_PORT_UP_IN_BNDL)
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
                    if (LaGetPortEntry
                        ((UINT2) pConsPortEntry->u4PortIndex,
                         &pPortEntry) == LA_SUCCESS)
                    {
                        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                            u4ConsRefSpeed = pPortEntry->u4LinkSpeed;
                        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                            u4ConsRefHighSpeed = pPortEntry->u4LinkHighSpeed;
                        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                            pPortEntry->u4Mtu;
                        LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.
                                   LaDLAGCompProp.BestInfoMac,
                                   pAggEntry->DLAGSystem.SystemMacAddr,
                                   LA_MAC_ADDRESS_SIZE);

                    }

                }
            }
            LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                          pAggEntry);

        }

    }

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGMasterUpdateTreeOnPropGreater              */
/*                                                                             */
/* Description        : This function is called by Master if Master also       */
/*                      particpates in port-channel when MTU/Speed is changed  */
/*                      greater with best info.It will move all the ports from */
/*                      ADD & STANDBY State to DELETE State,After that only    */
/*                      Ports that belongs to this port-channel are moved to   */
/*                      ADD State.After that consolidation takes place.        */
/*                                                                             */
/* Input(s)           : pAggEntry -  Aggregator for which ports are added      */
/*                                                                             */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*                                                                             */
/*******************************************************************************/

INT4
LaActiveMCLAGMasterUpdateTreeOnPropGreater (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4BrgPortIndex = 0;
    UINT1               u1BundleState = LA_PORT_DOWN;
    UINT2               u2FlagPortCount = 0;
    UINT2               u2FlagDeletePortCount = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGMasterUpdateTreeOnPropGreater"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "Master MTU Or Speed Greater:\n ");

    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        /* Incase Port is in STANDBY state no need to give indication,
         * If it is in DELETE State its not changed at all*/
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
        {
            /* Set the RecentChangedFlag */
            pConsPortEntry->u1RecentlyChanged = LA_TRUE;
        }
        pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    pConsPortEntry = NULL;
    pPortEntry = NULL;            /* Get First Entry */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        LaGetPortBundleState ((UINT4) pPortEntry->u2PortIndex, &u1BundleState);

        /* Incase of Bundle State is UP Port needs to be in ADD State,
         * If Bundle State is DOWN, Port Should be in DELETE State */
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);

        LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Port Entry is not present, creating new Port Entry */
            i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                        u4BrgPortIndex,
                                                        pPortEntry->
                                                        LaLacActorInfo.
                                                        u2IfPriority);

            if (i4RetVal == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            if (NULL != pConsPortEntry)
            {
                pConsPortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                /*Adding the newly created Port to the PortList */
                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }
        }

        if (u1BundleState == LA_PORT_UP_IN_BNDL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Up =%d\n",
                         (UINT4) pPortEntry->u2PortIndex);
            if (pConsPortEntry != NULL)
            {
                /* Set the RecentChangedFlag and move Port to ADD State */
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

                /* Port State can Either be in DELETE or NOT_IN_ANY_LIST.
                 * Reverse the Recently Changed Flag, If Port State is 
                 * NOT_IN_ANY_LIST Flag definitely will be false and needs to be set,
                 * If Port State is in DELETE if its previuos state is ADD it will be set,
                 * else It will be false, so reverse it */
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }
        }                        /* Else Already all Ports are in DELETE state, so do Nothing */
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                         pPortEntry->u2PortIndex);

            if ((pConsPortEntry != NULL) &&
                (pConsPortEntry->u1PortStateFlag ==
                 LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
            {
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

            }
        }

        /* Continue to next port */
        pConsPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }
    pConsPortEntry = NULL;
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        u2FlagPortCount++;
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
        {
            u2FlagDeletePortCount++;
        }
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }
    if (u2FlagPortCount == u2FlagDeletePortCount)
    {
        LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
        while (pRemotePortEntry != NULL)
        {
            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                          &pConsPortEntry, pAggEntry);
                if (pConsPortEntry != NULL)
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

                    pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                        u4ConsRefSpeed = pRemoteAggEntry->u4PortSpeed;
                    pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                        u4ConsRefHighSpeed = pRemoteAggEntry->u4PortHighSpeed;
                    pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                        pRemoteAggEntry->u4Mtu;
                    LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                               BestInfoMac,
                               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                               LA_MAC_ADDRESS_SIZE);
                }
            }
            pConsPortEntry = NULL;

            LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                          &pRemotePortEntry);
        }
    }
    return LA_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : LaActiveMCLAGUpdateTreeOnPropLesser                     */
/*                                                                             */
/* Description        : This function will be called by Remote Consolidated    */
/*                      List when receiving a DLAG PDU with Lesser  MTU/Speed  */
/*                      with the Best Info. It will move all the ports of this */
/*                      Remote channel from ADD & STANDBY State to DELETE State*/
/*                      and sets the SortingReq as TRUE. So that consolidation */
/*                      takes place.                                           */
/*                                                                             */
/*                                                                             */
/* Input(s)           : pRemoteAggEntry - Remote Aggregator for which PDU      */
/*                                        received.                            */
/*                      pu1SortingReq - Whether Selection Logic to be          */
/*                                        triggered.                           */
/*                                                                             */
/*                                                                             */
/* Output(s)          : pu1SortingReq - As TRUE or FALSE                       */
/*                                                                             */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*                                                                             */
/*******************************************************************************/

INT4
LaActiveMCLAGUpdateTreeOnPropLesser (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                     UINT1 *pu1SortingReq)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;
    UINT2               u2FlagPortCount = 0;
    UINT2               u2FlagDeletePortCount = 0;
    UINT1               u1BundleState = LA_PORT_DOWN;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGUpdateTreeOnPropLesser"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pRemoteAggEntry->pAggEntry;

    LA_TRC (CONTROL_PLANE_TRC, "\n MTU or Speed Lesser \n");
    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        /* First check whether entry is already in RBTree */
        if ((pRemotePortEntry->u1UpdateRequired == LA_TRUE)
            || (pRemoteAggEntry->u1UpdateRequired == LA_TRUE))
        {
            *pu1SortingReq = LA_TRUE;    /* Whether consolidation needed */
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "\n Update Required is TRUE Port=%d\n",
                         pRemotePortEntry->u4PortIndex);
            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);
            if (pConsPortEntry == NULL)
            {
                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);

                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }
                if (NULL != pConsPortEntry)
                {
                    pConsPortEntry->u4InstanceId =
                        LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                    pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                    LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
            }

            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Up =%d\n",
                             pRemotePortEntry->u4PortIndex);

                if (pConsPortEntry != NULL)
                {
                    /* If it is not in ADD state no need to set the flag */
                    if (pConsPortEntry->u1PortStateFlag ==
                        LA_AA_DLAG_PORT_IN_ADD)
                    {
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
                }
            }
            else
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Down =%d\n",
                             pRemotePortEntry->u4PortIndex);

                if ((pConsPortEntry != NULL) &&
                    (pConsPortEntry->u1PortStateFlag ==
                     LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

                }
            }
        }                        /* Update required ends */
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }
    pConsPortEntry = NULL;
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        u2FlagPortCount++;
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
        {
            u2FlagDeletePortCount++;
        }
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }
    if (u2FlagPortCount == u2FlagDeletePortCount)
    {
        LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
        while (pRemotePortEntry != NULL)
        {
            LaGetPortBundleState (pRemotePortEntry->u4PortIndex,
                                  &u1BundleState);
            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                          &pConsPortEntry, pAggEntry);
                if (pConsPortEntry != NULL)
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

                    pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                        u4ConsRefSpeed = pRemoteAggEntry->u4PortSpeed;
                    pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                        u4ConsRefHighSpeed = pRemoteAggEntry->u4PortHighSpeed;
                    pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                        pRemoteAggEntry->u4Mtu;
                    LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                               BestInfoMac,
                               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                               LA_MAC_ADDRESS_SIZE);
                }
            }
            pConsPortEntry = NULL;

            LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                          &pRemotePortEntry);
        }
    }
    /* while ends */
    return LA_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : LaActiveMCLAGMasterUpdateTreeOnPropLesser               */
/*                                                                             */
/* Description        : This function is called by Master if Master also       */
/*                      particpates in port-channel when MTU/Speed is changed  */
/*                      greater with best info.It will move all the ports from */
/*                      of this port-channel from ADD & STANDBY State to DELETE*/
/*                      State, After that consolidation takes place.           */
/*                                                                             */
/* Input(s)           : pAggEntry -  Aggregator for which ports are added      */
/*                                                                             */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*                                                                             */
/*******************************************************************************/

INT4
LaActiveMCLAGMasterUpdateTreeOnPropLesser (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4BrgPortIndex = 0;
    UINT1               u1BundleState = 0;
    INT4                i4RetVal = LA_FAILURE;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGMasterUpdateTreeOnPropLesser: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "\n Master MTU or Speed Lesser \n");
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        /* First check whether entry is already in RBTree */
        LaGetPortBundleState ((UINT4) pPortEntry->u2PortIndex, &u1BundleState);

        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);
        LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Create Port Entry and Insert in PortList and Consolidated Tree */

            i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                        u4BrgPortIndex,
                                                        pPortEntry->
                                                        LaLacActorInfo.
                                                        u2IfPriority);
            if (i4RetVal == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            if (NULL != pConsPortEntry)
            {
                pConsPortEntry->u4InstanceId = LA_MC_LAGG_DEFAULT_INSTANCE_ID;
                pConsPortEntry->b1LaggDistType = LA_MC_LAGG_ENABLED;
                /*Adding the newly created Port to the PortList */
                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }
        }

        if (u1BundleState == LA_PORT_UP_IN_BNDL)
        {
            if (pConsPortEntry != NULL)
            {
                /* If it is not in ADD state no need to set the flag */
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
            }
        }
        else
        {

            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                         pPortEntry->u2PortIndex);
            if ((pConsPortEntry != NULL) &&
                (pConsPortEntry->u1PortStateFlag ==
                 LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
            {
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

            }
        }

        /* Else down in bundle ports. Don't do anything */
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGMasterProcessAddDeleteLinks              */
/*                                                                           */
/* Description        : This function programs the ports in H/w while        */
/*                      giving the PDU to Slaves by Master.                  */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS- On Success                               */
/*                      LA_ERR_NULL_PTR - If Null pointer received           */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveMCLAGMasterProcessAddDeleteLinks (tLaLacAggEntry * pAggEntry,
                                          UINT4 u4PortBrgIndex,
                                          UINT1 u1PortStateFlag)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    INT4                i4RetStatus = 0;
    UINT4               u4PortIndex = 0;
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = 0;
    BOOL1               bRemoteFlag = LA_FALSE;
#endif

#ifdef NPAPI_WANTED
    /* If the port is a remote port, NP programming should not be allowed.
     * The hardware entry for the remote port would not be available */
    if (LA_MSB_BIT_SET == (u4PortBrgIndex & LA_MSB_BIT_SET))
    {
        bRemoteFlag = LA_TRUE;
    }
#endif
    LA_TRC (CONTROL_PLANE_TRC, "\n Master programs Add/Del list in H/w \n");

    LaActiveDLAGGetIfIndexFromBridgeIndex ((UINT2) u4PortBrgIndex,
                                           &u4PortIndex);

    if (u4PortIndex != 0)
    {
        LaGetPortEntry ((UINT2) u4PortIndex, &pPortEntry);

        i4RetStatus = LaActiveDLAGCheckIsLocalPort ((UINT2) u4PortIndex);

        if ((i4RetStatus == LA_SUCCESS) &&
            (pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry))
        {
            if ((u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
                && (pPortEntry->u1DLAGMasterAck == LA_TRUE))
            {
                pPortEntry->u1DLAGMasterAck = LA_FALSE;
                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_FALSE;
            }

            if ((u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                && (pPortEntry->u1DLAGMasterAck == LA_FALSE))
            {
                pPortEntry->u1DLAGMasterAck = LA_TRUE;
                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_TRUE;
            }

            /* Convey this info with Peer */
            pPortEntry->NttFlag = LA_TRUE;
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

            if (u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
            {
                LaActiveMCLAGHwControl (pPortEntry,
                                        LA_HW_EVENT_DISABLE_DISTRIBUTOR);
            }
            else
            {
                LaActiveMCLAGHwControl (pPortEntry,
                                        LA_HW_EVENT_ENABLE_DISTRIBUTOR);
            }
        }
    }

#ifdef NPAPI_WANTED
    if (bRemoteFlag != LA_TRUE)
    {
        if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
        {
            if (u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
            {
                if (LaFsLaHwDlagRemoveLinkFromAggGroup (pAggEntry->u2AggIndex,
                                                        (UINT2) u4PortBrgIndex)
                    != FNP_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                                 "FsLaHwDlagRemoveLinkFromAggGroup: Failed to "
                                 "configure remote ports in H/w for: port-"
                                 "channel: %u\n", pAggEntry->u2AggIndex);
                }
                if ((pPortEntry != NULL)
                    && (pPortEntry->pAggEntry == pAggEntry))
                {
                    /* Since Port is removed from H/w Change the Status */
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
                }
            }

            if (u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
            {
                if (LaFsLaHwDlagAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                                   (UINT2) u4PortBrgIndex,
                                                   &u2HwAggIndex) !=
                    FNP_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                                 "FsLaHwDlagAddLinkToAggGroup: Failed to "
                                 "configure remote ports in H/w for: port-"
                                 "channel: %u\n", pAggEntry->u2AggIndex);
                }
            }
        }
    }
#endif
    return LA_SUCCESS;

}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGMasterDownIndication                      */
/*                                                                            */
/* Description        : This function called whenever there is a indication   */
/*                      from MBSM that Master is Down. So all the Slaves will */
/*                      start sending LACP PDU with Default Global Mac,       */
/*                      Priority instead of DLAG configured MAc,Priority      */
/*                                                                            */
/* Input(s)           : pAggEntry - Aggregator entry in which DLAG Enabled    */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/

INT4
LaActiveMCLAGMasterDownIndication ()
{
    LA_TRC (CONTROL_PLANE_TRC, "\n Master Down Indication \n");

    /* Master Down is set to TRUE */
    if (LA_AA_DLAG_MASTER_DOWN == LA_TRUE)
    {
        /* Already master is in Down */
        return LA_SUCCESS;

    }

    LaActiveMCLAGDisable ();

    /* 1) This done here becasue LaDLAGResetActorSystemID,
     *    LaDLAGResetActorSystemPriority will call LaHwControl(DISABLE_DISTRIBUTOR)
     *    for ports that are in port-channel. This will try to behave as normal
     *    scenario. Until that time we have to keep Master as Up so that there
     *    it will not try to act as normal remove scenario.
     * 2) When port moves Distributing will take time(based on timeout) 
     *    +2 seconds (wait time) we will attain normal scenario. */

    LA_AA_DLAG_MASTER_DOWN = LA_TRUE;

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGMasterUpIndication                        */
/*                                                                            */
/* Description        : This function called whenever there is a indication   */
/*                      from MBSM that Master is UP. So all the Slaves will   */
/*                      start sending LACP PDU with Configured DLAG MAC,      */
/*                                                                            */
/* Input(s)           : None                                                  */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/

INT4
LaActiveMCLAGMasterUpIndication ()
{

    LA_TRC (CONTROL_PLANE_TRC,
            "\nLaActiveMCLAGMasterUpIndication: Master Up Indication \n");

    if (LA_AA_DLAG_MASTER_DOWN == LA_FALSE)
    {
        /* Already master is Up */
        return LA_SUCCESS;

    }

    LaActiveMCLAGEnable ();

    /* This is done here because when Master comes up 
     * normal LACP ports needs to be flushed and removed from
     * H/L. So for not disturbing that we are making the global
     * variable false here */
    LA_AA_DLAG_MASTER_DOWN = LA_FALSE;

    return LA_SUCCESS;

}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGHwControl                                 */
/*                                                                            */
/* Description        : This function is useful in indicating to lower layer  */
/*                      hardware / interface about the mux state by calling   */
/*                      the callback functions provided by it. Presently,     */
/*                      the function just updates the event and calls mux.    */
/*                                                                            */
/* Input(s)           : pPortEntry - Pointer to PortEntry                     */
/*                      LaLacEvent - Enable/Disable Distributor               */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/
INT4
LaActiveMCLAGHwControl (tLaLacPortEntry * pPortEntry, UINT1 LaLacEvent)
{

#ifdef NPAPI_WANTED
    tLaLacAggConfigEntry *pAggConfigEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    tLaParamInfo       *pLaParamInfo = NULL;
#endif
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaNpTrapLogInfo    NpLogInfo;
#ifdef L2RED_WANTED
    tLaHwSyncInfo      *pLaHwEntry = NULL;
#endif
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = LA_INVALID_HW_AGG_IDX;
#endif
    UINT2               u2AggIndex;
    UINT2               au2ActivePortArray[LA_MAX_PORTS_PER_AGG];

    MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));
    LA_MEMSET (au2ActivePortArray, 0, sizeof (au2ActivePortArray));

#ifdef NPAPI_WANTED
    pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);
#endif

    if (pPortEntry == NULL)
    {

        LA_TRC (INIT_SHUT_TRC, "Pointer to Port Entry NULL\n");
        return LA_ERR_NULL_PTR;
    }

    if ((pAggEntry = pPortEntry->pAggEntry) == NULL)
    {
        return LA_FAILURE;
    }

#ifdef NPAPI_WANTED
    pAggConfigEntry = &(pAggEntry->AggConfigEntry);
#endif
    u2AggIndex = pAggEntry->u2AggIndex;

    switch (LaLacEvent)
    {
        case LA_HW_EVENT_ENABLE_DISTRIBUTOR:

            if (LaLacGetMCLAGActivePorts
                (u2AggIndex, au2ActivePortArray,
                 &pAggEntry->u1DistributingPortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "SYS:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                if (pAggEntry->u1DistributingPortCount == 1)
                {
                    if (LaFsLaHwCreateAggGroup (u2AggIndex, &u2HwAggIndex)
                        == FNP_FAILURE)
                    {
                        LaHandleAggCreateFailed (u2AggIndex);
                        LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                                     "in HW failed\n", u2AggIndex);
                        return LA_SUCCESS;
                    }

                    LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                                 "Success in LaActiveDLAGControl\n",
                                 u2AggIndex);

                    FsLaHwSetSelectionPolicyBitList
                        (u2AggIndex,
                         pAggConfigEntry->u4LinkSelectPolicyBitList);

                    LaAggCreated (u2AggIndex, u2HwAggIndex);
                }

                /* This function is moved inside NP_PROGRAMMING_ALLOWED,
                 * because in Synchronous NP case, this function should not be
                 * executed in the Standy node.In the standby node, this
                 * function is invoked from LA_NP_ADD_PORT_SUCCESS_INFO message
                 * handler */

                LaActiveMCLAGLinkAddedToAgg (u2AggIndex,
                                             pPortEntry->u2PortIndex);
            }
#else /* if NPAPI_WANTED not defined */

            /*If NPAPI_WANTED is not defined, then function gets invoked in
             * both active and standby node */
            LaActiveMCLAGLinkAddedToAgg (u2AggIndex, pPortEntry->u2PortIndex);
#endif
            break;
        case LA_HW_EVENT_DISABLE_DISTRIBUTOR:

            if (LaLacGetMCLAGActivePorts
                (u2AggIndex, au2ActivePortArray,
                 &pAggEntry->u1DistributingPortCount) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "SYS:LaLacGetActivePorts function FAILED for Agg %d\n",
                             u2AggIndex);
                return LA_FAILURE;
            }

            LaActiveMCLAGChangeAggSpeed (pAggEntry);

            if (pAggEntry->u1DistributingPortCount == 0)
            {
#ifdef L2RED_WANTED
                /* When LA_NP_DEL_INFO doesnt reach the standby node, At the
                 * standby, all ports in the group will be in DOWN state
                 * and distributingPortCount variable in the corresponding agg
                 * is zero but pLaHwEntry->u1AggStatus = LA_ENABLE and
                 * pLaHwEntry->u2HwAggIdx will have valid agg Id.
                 *
                 * If the HW maintains one to one mapping between the HW agg
                 * index and SW agg index (always same HwAggIdx for SwAggIdx),
                 * then this scenario will not be an issue
                 *
                 * If the HW reassigns the HwAggId to some other SwAggIdx then
                 * this will be an issue during the HW Audit
                 * There is a possibility for having same HwAggIdx to two or more
                 * SwAggIdx, this happens if LA_NP_DEL_INFO doesnt reach the standby
                 * In the HwAudit, it reads the active HwAggIdx and gets the
                 * corresponding SwAggIdx, here, since we have two SwAgg with same
                 * HwAggIdx, we may return older SwAggIdx. It checks for the
                 * distributingPortCount, which is zero, so it deletes the aggregator
                 * from the HW. The problem here is, New AggIdx will be in up state
                 * with ports in distributing state but Hw will have not have this
                 * group.
                 * This is taken care by the following statements, here we reset the
                 * HwAggIdx and AggStatus if distributingPortCount becomes zero,
                 * without waiting for LA_NP_DEL_INFO
                 */
                if ((u2AggIndex <= LA_MAX_PORTS) ||
                    (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
                    return LA_FAILURE;
                pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);
                if (pLaHwEntry != NULL)
                {
                    pLaHwEntry->u2HwAggIdx = LA_INVALID_HW_AGG_IDX;
                    pLaHwEntry->u1AggStatus = LA_DISABLED;
                }

#endif /* L2RED_WANTED */
                if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
                {
                    pAggEntry->u1AggOperStatus = LA_OPER_DOWN;

                    /* Get the time of last oper change */
                    LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
                    pAggEntry->u4OperDownCount++;
                    pAggEntry->i4DownReason = LA_IF_OPER_DOWN;
                    LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC,
                                     "\nLine Protocol on Port Channel %d changed to OPER DOWN\r\n",
                                     pAggEntry->AggConfigEntry.u2ActorAdminKey);
                    LaLinkStatusNotify (pAggEntry->u2AggIndex, LA_OPER_DOWN,
                                        pAggEntry->u1AggAdminStatus,
                                        LA_TRAP_PORTCHANNEL_OPER_DOWN);
                    /* Indicate to bridge */
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                 "\nLA: Line Protocol on Port Channel %d changed down",
                                 pAggEntry->AggConfigEntry.u2ActorAdminKey);

                    LaL2IwfLaHLPortOperIndication (u2AggIndex, LA_OPER_DOWN);

                }
                pAggEntry->bHlIndicated = LA_FALSE;
                LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                             "SYS: Port: %u :: AggIndex: %u OPER DOWN Indication to BRIDGE\n",
                             pPortEntry->u2PortIndex, u2AggIndex);
            }
            LaUpdateLlPortChannelStatus (pAggEntry);

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {

                /* When FsLaHwAddLinkToAggGroup fails then the variable
                 * LaLacHwPortStatus is set to LA_HW_ADD_PORT_FAILED_IN_LAGG. In
                 * the NP callback function we set the Operkey of this port to
                 * zero. This makes the port to come out of the bundle, So we
                 * execute DISABLE_DISTRIBUTING switch case in the
                 * LaLacHwControl,here we try to delete the port from the HW
                 * (which is not present in the HW), because the port is not in
                 * the HW, delete call fails and the LaLacHwPortStatus is set
                 * to LA_HW_REM_PORT_FAILED_IN_LAGG. To handle this,
                 * Before initiating the PortDel NP call in the
                 * LaLacHwControl, Can we check for the status of this
                 * variable (LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG)
                 */

                /*The indication is send to applications that the port is
                 *removed from the port channel*/

                MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
                pLaParamInfo->u2IfIndex = pPortEntry->u2PortIndex;

                pLaParamInfo->u2IfKey = pPortEntry->LaLacActorInfo.u2IfKey;
                NotifyProtoToApp.LaNotify.u1OperStatus = LA_OPER_DOWN;
                NotifyProtoToApp.LaNotify.u1Action =
                    LA_NOTIFY_PORT_IN_BUNDLE_STATUS;
                /*SystemId in pLaParamInfo is unused for this indication */
                LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
            }
#endif /* NPAPI_WANTED */

            LaL2IwfRemoveActivePortFromPortChannel (pPortEntry->u2PortIndex,
                                                    u2AggIndex);
            /* LA_MCAST_CHANGE Ends */
            break;

        default:

            break;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveLinkAddedToAgg                               */
/*                                                                           */
/* Description        : This function gives indication to HL and Lower Layer */
/*                                                                           */
/* Input(s)           : u2AggIndex - Port-channel index .                    */
/*                      u2PortIndex - Port index                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

INT4
LaActiveMCLAGLinkAddedToAgg (UINT2 u2AggIndex, UINT2 u2PortIndex)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;

    /* CFA, L2Iwf indication about the link should be given only when the
     * NPAPI operations is successfully completed. Hence for Asynchronous
     * NPAPI, this indication will be given only in the NP CallBack function*/

    if ((LaIssGetAsyncMode (L2_PROTO_LACP) == LA_NP_ASYNC) &&
        (gLaCallSequence != LA_NP_CALLBACK_FLOW))
    {
        return LA_SUCCESS;
    }

    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry != NULL)
    {
        pPortEntry->LaLacHwPortStatus = LA_HW_PORT_ADDED_IN_LAGG;
    }

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));

        /* The indication is send to applications that the port is
         * added to the port channel*/
        NotifyProtoToApp.LaNotify.u1OperStatus = LA_OPER_UP;
        NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_PORT_IN_BUNDLE_STATUS;
        NotifyProtoToApp.LaNotify.LaInfo.u2IfIndex = u2AggIndex;
        LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
        LaRedSyncModifyPortsInfo (LA_NP_ADD_PORT_SUCCESS_INFO, u2AggIndex,
                                  u2PortIndex);
    }

    LaActiveMCLAGChangeAggSpeed (pAggEntry);

    /* Make the OPer Status for the Aggregator UP */
    if (!pAggEntry->bHlIndicated)
    {
        if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
        {
            pAggEntry->u1AggOperStatus = LA_OPER_UP;
            LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
            pAggEntry->u4OperUpCount++;
            pAggEntry->i4DownReason = LA_NONE;
            LaLinkStatusNotify (u2AggIndex, LA_OPER_UP, LA_ADMIN_UP,
                                LA_TRAP_PORTCHANNEL_OPER_UP);
            /* Indicate to bridge */
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\nLA: Line Protocol on "
                         "Port Channel %d changed up",
                         pAggEntry->AggConfigEntry.u2ActorAdminKey);
            LaUpdateLlPortChannelStatus (pAggEntry);
            LaL2IwfLaHLPortOperIndication (u2AggIndex, LA_OPER_UP);
            pAggEntry->bHlIndicated = LA_TRUE;
        }
    }
    else
    {
        LaUpdateLlPortChannelStatus (pAggEntry);
    }
    /*The indication is given to the higher layer modules only after
     * the Port Speed is updated in LA module as well as in CFA module.*/
    LaL2IwfAddActivePortToPortChannel (u2PortIndex, u2AggIndex);
    LA_TRC_ARG2 (CONTROL_PLANE_TRC, "SYS: Port: %u :: AggIndex: %u OPER UP "
                 "indication to BRIDGE\n", u2PortIndex, u2AggIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGChangeAggSpeed                           */
/*                                                                           */
/* Description        : Calculates the Aggregator Speed. Called when a port  */
/*                      becomes distributing in a port channel, or goes out  */
/*                      of distributing state.                               */
/*                                                                           */
/* Input(s)           : pAggEntrypAggEntry- Pointer to Aggregator Entry.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaActiveMCLAGChangeAggSpeed (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DistPortSpeed = 0;
    UINT4               u4DistPortHighSpeed = 0;
    UINT2               u2DistPortCount = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGChangeAggSpeed: "
                "NULL POINTER.\n");
        return;
    }

    pAggEntry->u4AggSpeed = 0;
    pAggEntry->u4AggHighSpeed = 0;

    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

    while (pPortEntry != NULL)
    {
        if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
             LA_TRUE)
            && (pPortEntry->LaLacActorInfo.LaLacPortState.
                LaSynchronization == LA_TRUE))
        {
            u4DistPortSpeed = pPortEntry->u4LinkSpeed;
            u4DistPortHighSpeed = pPortEntry->u4LinkHighSpeed;
            u2DistPortCount++;
        }
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }

    /* This logic depends on the link speed of each port number of
     * distributing ports in the aggregation.
     * For 100Mpbs links, if the number exceeds 40 then high speed is used
     * because if 40*100 mbps links are aggregated then the threshold value
     * of the UINT4 variable is reached.
     * For 1Gbps links, if the number exceeds 4 then high speed is used
     * because if 4*1000 mbps links are aggregated then the threshold value
     * of the UINT4 variable is reached. */
    if (u4DistPortSpeed < LA_32BIT_MAX)
    {
        if ((u4DistPortSpeed <= CFA_ENET_SPEED_100M) && (u2DistPortCount <= 40))
        {
            pAggEntry->u4AggSpeed = u2DistPortCount * (u4DistPortSpeed);
            pAggEntry->u4AggHighSpeed =
                u2DistPortCount * (u4DistPortSpeed / 1000000);
        }

        else if ((u4DistPortSpeed <= CFA_ENET_SPEED_1G) &&
                 (u2DistPortCount <= 4))
        {
            pAggEntry->u4AggSpeed = u2DistPortCount * (u4DistPortSpeed);
            pAggEntry->u4AggHighSpeed =
                u2DistPortCount * (u4DistPortSpeed / 1000000);
        }

        else
        {
            pAggEntry->u4AggSpeed = LA_32BIT_MAX;
            pAggEntry->u4AggHighSpeed = u2DistPortCount *
                (u4DistPortSpeed / 1000000);
        }
    }
    else
    {
        pAggEntry->u4AggSpeed = LA_32BIT_MAX;
        pAggEntry->u4AggHighSpeed = u2DistPortCount * (u4DistPortHighSpeed);

    }

}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGCalculateAggOperStatus                   */
/*                                                                           */
/* Description        : This function will give the Aggregator Oper status   */
/*                      based on the ports that are in Sync & Dist           */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregator Entry.             */
/*                      u1AggAdminStatus - Admin status of port-chanenl      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaActiveMCLAGCalculateAggOperStatus (tLaLacAggEntry * pAggEntry,
                                     UINT1 u1AggAdminStatus)
{

    tLaLacPortEntry    *pPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveMCLAGCalculateAggOperStatus: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Make the AggOperStatus as Down if UP
       It will be made as Up in the following Loop  */
    if (pAggEntry->u1AggOperStatus == LA_OPER_UP)
    {
        pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
    }

    if (u1AggAdminStatus == CFA_IF_UP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
                 LA_TRUE) &&
                (pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization ==
                 LA_TRUE))
            {
                pAggEntry->u1AggOperStatus = LA_OPER_UP;
                break;
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
    else
    {
        if (pAggEntry->u1AggOperStatus == LA_OPER_UP)
        {
            pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGInitParams                              */
/*                                                                           */
/* Description        : This function initializes the global variables used  */
/*                      by active MC-LAG                                     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.GlobalDLAGInfo                         */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*****************************************************************************/

INT4
LaActiveMCLAGInitParams ()
{

    /* Initialize the DLAG System and Master Down
     * and Role played */

    LA_MCLAG_SYSTEM_STATUS = LA_MCLAG_DISABLED;

    LA_AA_DLAG_MASTER_DOWN = LA_TRUE;

    /* Initialize the global DLAG params */

    LA_MEMSET (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr), 0,
               sizeof (tLaMacAddr));

    LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority =
        LA_DEFAULT_SYSTEM_PRIORITY;

    LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingIfIndex = 0;

    LA_MEMSET ((LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingOperPortList), 0,
               sizeof (tLaPortList));
    LA_MEMSET ((LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingPortList), 0,
               sizeof (tLaPortList));

    /* Global timer related Initializations */

    LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime =
        LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME;

    LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr.LaTmrFlag = LA_TMR_STOPPED;

    LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr.pEntry =
        (VOID *) (&(LA_DLAG_GLOBAL_INFO));

    LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr.LaAppTimer.u4Data =
        LA_AA_MCLAG_PERIODIC_TMR_TYPE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGScanLaListAndInitMCLAG.                 */
/*                                                                           */
/* Description        : This function is called if Global MC-LAG is enabled  */
/*                      to copy the Global MC-LAG properties as their System */
/*                      properties and Start the MC-LAG functionlity in all  */
/*                      the Aggregators in the List.                         */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaActiveMCLAGScanLaListAndInitMCLAG (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if ((pAggEntry->u1MCLAGStatus) == LA_MCLAG_ENABLED)
        {
            /* Init Active-Active MC-LAG in All Nodes */
            LaActiveCopyGlobalAndInitMCLAG (pAggEntry);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
}

/*****************************************************************************/
/* Function Name      : LaActiveCopyGlobalAndInitMCLAG                       */
/*                                                                           */
/* Description        : This function is called if Global MC-LAG is enabled  */
/*                      to copy the Global MC-LAG properties as their System */
/*                      properties and Start the MC-LAG functionlity in that */
/*                      Aggregator entry                                     */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which MC-LAG enabled */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaActiveCopyGlobalAndInitMCLAG (tLaLacAggEntry * pAggEntry)
{

    if (pAggEntry == NULL)
    {
        return LA_ERR_NULL_PTR;
    }

    /* Disable redundancy if enabled */

    pAggEntry->u1DLAGRedundancy = LA_DLAG_REDUNDANCY_OFF;

    if (LaMCLAGInit (pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }

    pAggEntry->u1MCLAGStatus = LA_MCLAG_ENABLED;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMCLAGInit                                          */
/*                                                                           */
/* Description        : This function is used to initialize the MC-LAG       */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which MC-LAG feature */
/*                                  should be initiated.                     */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaMCLAGInit (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT1               au1LocalMac[LA_MAC_ADDRESS_SIZE];

    LA_MEMSET (au1LocalMac, 0, LA_MAC_ADDRESS_SIZE);

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaMCLAGInit: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }
    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));
    /*Get the switch MAC */
    LaCfaGetSysMacAddress (au1LocalMac);

    /* If MC-LAG is enabled in port channel without enabling in the system.The port channel
     * will take the switch MAC as MC-LAG System identifier.After enabling MC-LAG globally 
     * if the port-channel System Identifier is switch MAC,then we need to copy the Global 
     * MC-LAG System Identifier to the MC-LAG port channel*/

    if (LA_MEMCMP (&pAggEntry->DLAGSystem.SystemMacAddr,
                   &au1LocalMac, LA_MAC_ADDRESS_SIZE) == 0)
    {
        LA_MEMCPY (pAggEntry->DLAGSystem.SystemMacAddr,
                   &LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);
    }

    /* Copy MC-LAG System Priority to Actor */
    if ((LaDLAGChangeActorSystemPriorityToDLAG (pAggEntry)) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaMCLAGInit: LaDLAGChangeActorSystemPriorityToDLAG failed.\n");
        return LA_FAILURE;
    }

    pAggEntry->AggConfigEntry.ActorSystem.u2SystemPriority =
        pAggEntry->DLAGSystem.u2SystemPriority;

    /* Change Actor System Priority in Ports */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
            = pAggEntry->DLAGSystem.u2SystemPriority;
        pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority
            = pAggEntry->DLAGSystem.u2SystemPriority;

        LA_MEMCPY (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            if (pPortEntry->LaLacpMode == LA_MODE_LACP)
            {
                LaLacRxMachine (pPortEntry,
                                LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
            }
        }
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }

    /* Assign the Role played from Global variable */
    pAggEntry->u1MCLAGRolePlayed = LA_AA_DLAG_ROLE_PLAYED;

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGEnable                                    */
/*                                                                            */
/* Description        : This function is called to Enable the functionalites  */
/*                      when Global DLAG status is enabled                    */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
INT4
LaActiveMCLAGEnable ()
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT1               au1LocalMac[LA_MAC_ADDRESS_SIZE];

    LA_MEMSET (au1LocalMac, 0, LA_MAC_ADDRESS_SIZE);

    if (LA_MEMCMP (&LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                   &au1LocalMac, LA_MAC_ADDRESS_SIZE) == 0)
    {
        /* Make the MAC address for MCLAG as the System MAC address */
        LaCfaGetSysMacAddress (LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.
                               SystemMacAddr);
    }

    if ((LA_SLL_COUNT (LA_AGG_SLL) != 0) &&
        (LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime != 0))
    {
        if ((LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr)))
            != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStartTimer failed for Global MC-LAG Periodic Sync Timer\n");
            return LA_FAILURE;
        }
    }

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (FNP_FAILURE == LaFsLaHwDlagStatus (LA_MCLAG_ENABLED))
        {
            LA_TRC (INIT_SHUT_TRC, "LaActiveMCLAGEnable FAILED\n");
            return LA_FAILURE;
        }
    }
#endif

    LaGetNextAggEntry (NULL, &pAggEntry);
    LaActiveMCLAGScanLaListAndInitMCLAG ();
    while (pAggEntry != NULL)
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);
        }
        if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
            (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER))
        {
            /* Create RBTree for DLAG Total port List */
            pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList =
                LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                        (tLaDLAGConsPortEntry,
                                         LaDLAGConsPortInfo),
                                        LaCmpPortPriority);
        }

        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            /* Send high-priority event-update message to remote MC-LAG nodes */
            LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);

            /* Create RBTree for DLAG Remote Aggregator Entry */
            pAggEntry->LaDLAGRemoteAggInfoTree =
                LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                        (tLaDLAGRemoteAggEntry,
                                         LaDLAGRemoteAggNode),
                                        LaMacCmpForLaDLAGRemoteAggEntry);
            /* Create RBTree for DLAG Add port List */
            pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList =
                LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                        (tLaDLAGConsPortEntry,
                                         LaDLAGPortInfo), LaCmpPortNo);
        }
        if (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
        {
            LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }                            /* End of while */

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGTxPeriodicSyncOrAddDelPdu               */
/*                                                                           */
/* Description        : This function is called on MC-LAG periodic sync timer*/
/*                      expiry to send MC-LAG Periodic sync/update PDU.      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGTxPeriodicSyncOrAddDelPdu ()
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4Count = 0;
    tLaBoolean          bSyncPduRequired = LA_FALSE;
    tLaBoolean          bUpdatePduRequired = LA_FALSE;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    LaGetNextAggEntry (NULL, &pAggEntry);

    /* Check any of port-channel contains atleast one port
     * then we can send periodic sync PDU */

    while (pAggEntry != NULL)
    {
        if ((pAggEntry->LaLacpMode == LA_MODE_LACP) &&
            (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
        {
            if (pAggEntry->u1ConfigPortCount > 0)
            {
                bSyncPduRequired = LA_TRUE;
            }

            /* Get the Total number of Port Entry in Consolidation Logic */
            RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
                         &u4Count);

            if (u4Count != 0)
            {
                bUpdatePduRequired = LA_TRUE;
            }

            if ((bSyncPduRequired == LA_TRUE)
                && (bUpdatePduRequired == LA_TRUE))
            {
                break;
            }
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    if ((LaIcchGetRolePlayed () == LA_ICCH_SLAVE) &&
        (bSyncPduRequired == LA_TRUE))
    {
        /* If System Role is Master then No need to send Periodic 
         * Sync PDU */

        /* This sends a consolidated Periodic Sync Pdu */

        if ((LaActiveMCLAGTxDSU (NULL, NULL) != LA_SUCCESS))
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveMCLAGTxPeriodicSyncOrAddDelPdu: MC-LAG Periodic Sync PDU"
                    " Tx failed for System : \n");
        }
    }
    else if ((LaIcchGetRolePlayed () == LA_ICCH_MASTER) &&
             (bUpdatePduRequired == LA_TRUE))
    {
        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE;

        if ((LaActiveMCLAGTxDSU (NULL, &LaDLAGTxInfo) != LA_SUCCESS))
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveMCLAGTxPeriodicSyncOrAddDelPdu: MC-LAG Periodic Update PDU"
                    " Tx failed for System : \n");
        }

    }

    if ((LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr))) !=
        LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: LaStartTimer failed for Global MC-LAG Periodic Sync Timer\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGTxDSU                                   */
/*                                                                           */
/* Description        : This function Forms and Sends a MC-LAG.              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregator entry              */
/*                      pLaDLAGTxInfo - Type of Pdu                          */
/*                      If NULL - Periodic Sync Pdu                          */
/*                      Else    - Periodic Updated Pdu                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/

INT4
LaActiveMCLAGTxDSU (tLaLacAggEntry * pAggEntry, tLaDLAGTxInfo * pLaDLAGTxInfo)
{
#ifdef NPAPI_WANTED
    tIcchInfo           IcchInfo;
#endif
#ifndef ICCH_WANTED
    tLaBufChainHeader  *pMsgBuf = NULL;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
#endif
    UINT4               u4DataLength = 0;
    UINT1              *pu1LinearBuf = NULL;
#ifndef ICCH_WANTED
    UINT1               u1OperStatus = CFA_IF_UP;
    BOOL1               bResult = OSIX_FALSE;
#endif
    BOOL1               bDlagMsgToMaster = OSIX_FALSE;

    LA_TRC (CONTROL_PLANE_TRC, "Entering LaActiveMCLAGTxDSU. \r\n");
#ifdef NPAPI_WANTED
    LA_MEMSET (&IcchInfo, 0, sizeof (tIcchInfo));
#endif
    if (pLaDLAGTxInfo == NULL)
    {
        /* Explicilty pass the pointer for pAggEntry and pLaDLAGTxInfo as
         * NULL as this is Full consolidated periodic Sync PDU */
        if ((LaActiveMCLAGFormDSU (NULL, &pu1LinearBuf, NULL, &u4DataLength)) !=
            LA_SUCCESS)
        {

            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveMCLAGFormDSU failed. \n");
            return LA_FAILURE;
        }
        bDlagMsgToMaster = OSIX_TRUE;
    }
    else if (pLaDLAGTxInfo->u1DSUType ==
             LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        /* Explicilty pass the pointer for pAggEntry as NULL 
         * for this full consolidated periodic Update Pdu */
        if ((LaActiveMCLAGFormAddDeleteDSU
             (NULL, &pu1LinearBuf, pLaDLAGTxInfo, &u4DataLength)) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveMCLAGTxDSU:LaActiveMCLAGFormAddDeleteDSU() failed. \n");
            return LA_FAILURE;
        }
        bDlagMsgToMaster = OSIX_FALSE;
    }

    else if ((pAggEntry != NULL)
             && (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER))
    {
        if ((pLaDLAGTxInfo->u1DSUType ==
             LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE)
            || (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
            || (pLaDLAGTxInfo->u1DSUType == LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN))

        {
            /* Separte LACPU for Add/Del message */
            if ((LaActiveMCLAGFormAddDeleteDSU
                 (pAggEntry, &pu1LinearBuf, pLaDLAGTxInfo,
                  &u4DataLength)) != LA_SUCCESS)
            {
                LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "LaActiveMCLAGTxDSU:LaActiveMCLAGFormAddDeleteDSU() failed. \n");

                return LA_FAILURE;
            }
        }
        else
        {
            /* Other Sync PDU's and Event PDU's. Avoid it as this is Master */
            return LA_SUCCESS;

        }
        pAggEntry->u4DLAGEventUpdatePduTxCount++;
        bDlagMsgToMaster = OSIX_FALSE;
    }
    else if ((pAggEntry != NULL)
             && (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_SLAVE))
        /* other Event update Messages.Sent only by Slaves */
    {
        if ((LaActiveMCLAGFormDSU
             (pAggEntry, &pu1LinearBuf, pLaDLAGTxInfo,
              &u4DataLength)) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveMCLAGTxDSU: LaActiveMCLAGFormDSU() failed.\n");
            return LA_FAILURE;
        }
        pAggEntry->u4DLAGEventUpdatePduTxCount++;
        bDlagMsgToMaster = OSIX_TRUE;
    }
#ifdef ICCH_WANTED
    LaIcchSendDSUToPeer (pu1LinearBuf, u4DataLength);
    UNUSED_PARAM (bDlagMsgToMaster);
#else
    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        while (u4Count <
               LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount)
        {
            OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                     GlobalDLAGDistributingOperPortList,
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            u4Count++;
            /* Check If Distributing port is operationally UP,
             * If not then packet transmission attempt need not
             * be made on the failed distributing port, continue scanning the
             * remaining distribution port list*/

            CfaGetIfOperStatus (u4DLAGDistributingIfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_DOWN)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "LaActiveMCLAGTxDSU: MC-LAG PDU Tx is not possible as Distributing"
                             " port %d configured for this port channel is down.\n",
                             u4DLAGDistributingIfIndex);
                u4DLAGDistributingIfIndex++;
                continue;
            }

            /* Allocate memory for CRU buffer */
            if ((pMsgBuf = LA_ALLOC_CRU_BUF (u4DataLength, 0)) == NULL)
            {
                LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "LaActiveMCLAGTxDSU: Buffer Allocation failed\n");
                return LA_FAILURE;
            }

            /* Copy from Linear buffer to CRU buffer */
            LA_COPY_OVER_CRU_BUF (pMsgBuf, pu1LinearBuf, 0, u4DataLength);

            LA_MEMSET (gLaGlobalInfo.gau1DLAGPdu, LA_INIT_VAL,
                       LA_MAX_DLAG_PDU_SIZE);
            /* Send the MC-LAG PDU */
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveMCLAGTxDSU: Handing over LACPDU to Lower Layer...\n");
            LaHandOverOutFrame (pMsgBuf, (UINT2) u4DLAGDistributingIfIndex,
                                NULL);
            u4DLAGDistributingIfIndex++;
        }
    }
    else
    {
#ifdef NPAPI_WANTED

        if (bDlagMsgToMaster == OSIX_FALSE)
        {
            if ((pLaDLAGTxInfo != NULL) &&
                (pLaDLAGTxInfo->u1DSUType ==
                 LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE) &&
                (pLaDLAGTxInfo->u4SlotId != 0))
            {

                IcchInfo.u4SlotId = pLaDLAGTxInfo->u4SlotId;
            }
            else
            {
                IcchInfo.u4SlotId = ICCH_TO_ALL_SLOTS;
            }
        }
        else
        {
            IcchInfo.u4SlotId = 0;
        }

        IcchInfo.pu1Data = pu1LinearBuf;
        IcchInfo.u2PktLength = u4DataLength;
        IcchInfo.u2AppId = ICCH_LA;

        ICCHHandOverTxFrame (&IcchInfo);
#else
        UNUSED_PARAM (bDlagMsgToMaster);

#endif

    }
#endif /* ICCH_WANTED */
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGTxEventDSU                               */
/*                                                                           */
/* Description        : This function Forms and Sends Event message of type  */
/*                      Add/Del or DB update                                 */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     MC-LAG PDU.                            */
/*                      pLaDLAGTxInfo - Tx Info.                             */
/*                         - Pointer to TxInfo Structure, to be sent in      */
/*                           update PDU.                                     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On NULL Ptr                        */
/*****************************************************************************/
INT4
LaActiveMCLAGTxEventDSU (tLaLacAggEntry * pAggEntry,
                         tLaDLAGTxInfo * pLaDLAGTxInfo)
{

    if ((pAggEntry == NULL) || (pLaDLAGTxInfo == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGTxEventDSU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    if ((LaActiveMCLAGTxDSU (pAggEntry, pLaDLAGTxInfo) != LA_SUCCESS))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGTxEventDSU: MC-LAG EventPDU"
                " Tx failed for System : \n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGKeepAlive                                */
/*                                                                           */
/* Description        : This function called when expiry of Periodic timer   */
/*                      and increments the keep alive count and verifies the */
/*                      keep alive count is reached the maximum. If maximum  */
/*                      reached then the node is deleted from the database   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/

INT4
LaActiveMCLAGKeepAlive ()
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if ((pAggEntry->LaLacpMode == LA_MODE_LACP) &&
            (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
        {
            /* Call Keep Alive for each Agg Entry */
            LaDLAGKeepAlive (pAggEntry);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGProcessSyncPDU                          */
/*                                                                           */
/* Description        : This function  called to process the periodic sync   */
/*                      Pdu. This traverse through entire Pdu and updates    */
/*                      MC-LAG database.                                     */
/*                                                                           */
/* Input(s)           : RemoteSysMac                                         */
/*                                  - Remote System Mac address              */
/*                      u2RemoteSysPriority                                  */
/*                                  - Remote System Prioirty.                */
/*                      pu1LinearBuf                                         */
/*                                  - MCLAG Data Received from Remote MC-LAG */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node  */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGProcessSyncPDU (tLaMacAddr RemoteSysMac,
                             UINT2 u2RemoteSysPriority, UINT1 *pu1LinearBuf,
                             INT4 i4SllAction)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4Key = 0;
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1NoOfPortChannel = 0;
    UINT1               u1NoOfPorts = 0;
    UINT1               u1KeyFieldOffSet = 0;
    UINT1              *pu1TmpBuf = NULL;

    /* Mark the pointer in Local ponter */

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGProcessSyncPDU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

    while (u1NoOfPortChannel != 0)
    {
        pu1TmpBuf = pu1PktBuf;
        /* Copy Port channel Key */
        LA_GET_4BYTE (u4Key, pu1TmpBuf);

        /* Get the Aggregation Entry */
        LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry);

        if (pAggEntry != NULL)
        {
            pAggEntry->u4DLAGPeriodicSyncPduRxCount++;
        }

        LaActiveMCLAGUpdateRemoteMCLAGNodeList (RemoteSysMac,
                                                u2RemoteSysPriority, pu1PktBuf,
                                                i4SllAction);

        /* Since this is Local buffer we need to traverse through entire PDU
         * Logic.
         * 1) Port channel ports off set is 9 fileds from current position.
         *    Move the pointer to 9th position.(Current+ Key 3 + Mtu 2 +Speed 2+ Reserv 2)
         * 2) Since port no's can vary take the port no's field.
         *    Each port will have fixed data 8 bytes
         *    [Port no(4)+ Bundle state(1) + Sync(1)+prioirty(2)].
         *
         * 3) Next port-channel Key will be available at position (No of ports * 8).
         *    Move the pointer there. We will get the next port-channel Key.
         */
        pu1PktBuf += LA_AA_DLAG_SYNC_PDU_NO_OF_PORTS_OFFSET;

        LA_GET_1BYTE (u1NoOfPorts, pu1PktBuf);

        u1KeyFieldOffSet = (UINT1)
            (u1NoOfPorts * (UINT1) LA_AA_MCLAG_SYNC_PDU_PER_PORT_INFO_LEN);

        pu1PktBuf += u1KeyFieldOffSet;

        u1NoOfPortChannel--;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveMCLAGProcessPeriodicUpdateDSU                */
/*                                                                           */
/* Description        : This function  called to process the periodic sync   */
/*                      Update PDU from Master.This traverse through entire  */
/*                      Pdu and updates MC-LAG database.                     */
/*                                                                           */
/* Input(s)           : pu1LinearBuf                                         */
/*                                  - MC-LAG Data Received from Remote MC-LAG*/
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveMCLAGProcessPeriodicUpdateDSU (UINT1 *pu1LinearBuf)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT1              *pu1PktBuf = NULL;
    UINT4               u4Key = 0;
    UINT1               u1NoOfPortChannel = 0;
    UINT1               u1NoOfPorts = 0;
    UINT1               u1KeyFieldOffSet = 0;

    /* Mark the pointer in Local ponter */

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveMCLAGProcessPeriodicUpdateDSU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

    while (u1NoOfPortChannel != 0)
    {
        LA_GET_4BYTE (u4Key, pu1PktBuf);
        /* Get the Aggregation Entry */
        if (LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry) == LA_FAILURE)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "Failed to get Agg Entry for key %d \n", u4Key);
        }
        if (pAggEntry != NULL)
        {
            if (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_SLAVE)

            {
                /* Process Add-Del PDU and program H/w */
                LaActiveMCLAGGetAddDeletePortFromDSU (pAggEntry, pu1PktBuf,
                                                      LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE);
                pAggEntry->u4DLAGPeriodicSyncPduRxCount++;
            }
        }
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "LaActiveMCLAGProcessPeriodicUpdateDSU: Port-Channel %d not exist \n",
                         u4Key);
        }

        /* Get Delete Type field */
        pu1PktBuf += LA_AA_DLAG_ADD_OR_DEL_FIELD_LEN;

        LA_GET_1BYTE (u1NoOfPorts, pu1PktBuf);

        u1KeyFieldOffSet = (UINT1)
            ((u1NoOfPorts * (UINT1) LA_AA_DLAG_PORT_NUM_LEN) +
             (u1NoOfPorts * (UINT1) LA_AA_MCLAG_PORT_STATE_FIELD_LEN));

        pu1PktBuf += u1KeyFieldOffSet;

        /* Get Add Type field */
        pu1PktBuf += LA_AA_DLAG_ADD_OR_DEL_FIELD_LEN;

        LA_GET_1BYTE (u1NoOfPorts, pu1PktBuf);

        pu1PktBuf += LA_AA_MCLAG_PI_MASK_FIELDLEN;
        u1KeyFieldOffSet = (UINT1)
            ((u1NoOfPorts * (UINT1) LA_AA_DLAG_PORT_NUM_LEN) +
             (u1NoOfPorts * (UINT1) LA_AA_MCLAG_PORT_STATE_FIELD_LEN));

        pu1PktBuf += u1KeyFieldOffSet;

        u1NoOfPortChannel--;
    }
    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveMCLAGDisable                                  */
/*                                                                            */
/* Description        : This function is called to Disable the functionlities */
/*                      when Global MC-LAG Status is Disabled                 */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
INT4
LaActiveMCLAGDisable ()
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetNextAggEntry (pAggEntry, &pAggEntry);

    /* Since we are disabling Active MC-LAG in the entire system
     * we need to flush all the entries in H/w + HL
     * 1) We need to call the Disable distributor for the
     *    acknowledged port and clear H/L.
     * 2) Delete Aggregator in H/w as this contains n number of
     *    entriesof remote ports which we don't know.
     * 3) Enable normal LAG functionilty by resetting sys ID &
     *    Sys Priority
     */

    while (pAggEntry != NULL)
    {
        if (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
        {
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);

            while (pPortEntry != NULL)
            {
                if (pPortEntry->u1DLAGMasterAck == LA_TRUE)
                {
                    /* The ports that are Acknolwdged by Master
                       only was given to H/L . So remove those
                       ports. So that Dist portcount also reduced */
                    LaActiveMCLAGHwControl (pPortEntry,
                                            LA_HW_EVENT_DISABLE_DISTRIBUTOR);
                }

                if (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG)
                {
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
                }

                LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);

                /* Since this will behave as normal scenario in LACP
                   Creating Agg Group will be done in ENABLE_DISTRIBUTOR in LaLacHwControl */
            }

            LaMCLAGDeInit (pAggEntry);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

#ifdef NPAPI_WANTED
    /* Delete all the entries from H/w */
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (FNP_FAILURE == LaFsLaHwDlagStatus (LA_DLAG_DISABLED))
        {
            LA_TRC (INIT_SHUT_TRC, "LaActiveMCLAGEnable FAILED\n");
            return LA_FAILURE;
        }
    }
#endif

    /* Stop the global timer */
    if ((LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr))) !=
        LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: LaStopTimer failed for Global MC-LAG "
                "Periodic Sync Timer\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaMCLAGProcessPeerUpEvent                             */
/*                                                                            */
/* Description        : This function is called to process the PEER_UP        */
/*                      notification in LA                                    */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
VOID
LaMCLAGProcessPeerUpEvent (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4IcclIfIndex = 0;
    UINT4               u4Count = 0;
    UINT1               u1AggOperStatus = LA_OPER_DOWN;
    /* When peer up indication is coming to LA, forcefully program the
     * ICCL to forwarding state irrepective of loop avoiding protocol
     * running*/
    if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
    {
        LaIcchGetIcclIfIndex (&u4IcclIfIndex);
        LaGetAggEntry ((UINT2) u4IcclIfIndex, &pAggEntry);
        if (pAggEntry != NULL)
        {
            u1AggOperStatus = pAggEntry->u1AggOperStatus;
        }
        if (u1AggOperStatus == LA_OPER_UP)
        {
            L2IwfSetInstPortState (RST_DEFAULT_INSTANCE,
                                   u4IcclIfIndex, AST_PORT_STATE_FORWARDING);
#ifdef NPAPI_WANTED
            LaUpdatePortChannelStatusToHw ((UINT2) u4IcclIfIndex,
                                           AST_PORT_STATE_FORWARDING, LA_FALSE);
#endif
        }
    }

    /* Start timer during peer up if it is not already running */
    if ((LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr.LaTmrFlag !=
         LA_TMR_RUNNING) && (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED))
    {
        if (LaStartTimer (&(LA_DLAG_GLOBAL_INFO.
                            GlobalMCLAGPeriodicSyncTmr)) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStartTimer failed for Global MC-LAG Periodic Sync"
                    " Timer\n");
        }
    }

    /* When peer is up, the MC-LAG system identifiers should be copied to
     * Actor System info for the MCLAG portchannels */
    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
            (LaIcchGetRolePlayed () == LA_ICCH_MASTER))
        {
            RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
                         &u4Count);
            if (u4Count == 0)
            {
                LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
            }
        }

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }                            /* End of while */
    return;
}

/******************************************************************************/
/* Function Name      : LaMCLAGProcessPeerDownEvent                           */
/*                                                                            */
/* Description        : This function is called to process the PEER_UP        */
/*                      notification in LA                                    */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
VOID
LaMCLAGProcessPeerDownEvent (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pNextConsPortEntry = NULL;
    UINT4               u4Count = 0;

    /* Stop timer during peer down if it is already running */
    if ((LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr.LaTmrFlag ==
         LA_TMR_RUNNING) && (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED))
    {
        if (LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr))
            != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStopTimer failed for Global MC-LAG Periodic Sync"
                    " Timer\n");
        }
    }

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {

        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
            while (pPortEntry != NULL)
            {
                pPortEntry->u1DLAGMasterAck = LA_TRUE;
                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_TRUE;
                LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
            }
        }

        /* Destroy RBTree of MC-LAG Remote Aggregator Entry */
        if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
            (LaIcchGetRolePlayed () == LA_ICCH_MASTER))
        {
            RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
                         &u4Count);

            if (u4Count == 0)
            {
                LaGetNextAggEntry (pAggEntry, &pAggEntry);
                continue;
            }

            LaDLAGDeleteAllRemoteAggEntries (pAggEntry);
            LaActiveDLAGGetNextPortEntry (NULL, &pNextConsPortEntry, pAggEntry);
            while (pNextConsPortEntry != NULL)
            {
                pConsPortEntry = pNextConsPortEntry;
                LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
                LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                              &pNextConsPortEntry, pAggEntry);
            }
            LaActiveDLAGGetNextPortEntryFromConsTree (NULL,
                                                      &pNextConsPortEntry,
                                                      pAggEntry);
            while (pNextConsPortEntry != NULL)
            {
                pConsPortEntry = pNextConsPortEntry;
                LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                         pConsPortEntry);
                LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);

                LaActiveDLAGGetNextPortEntryFromConsTree (pConsPortEntry,
                                                          &pNextConsPortEntry,
                                                          pAggEntry);
            }

        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }                            /* End of while */
    return;
}

/*****************************************************************************/
/* Function Name      : LaMCLAGSnmpLowGetFirstValidRemotePortChannelIndex    */
/*                                                                           */
/* Description        : This function returns the first index of MC-LAG      */
/*                      Remote Aggregator Table. This function is called     */
/*                      with these output parameters initialized to zero     */
/*                                                                           */
/* Input(s)          :  None                                                 */
/* Output(s)          : Port Channel interface index and Remote System Id    */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaMCLAGSnmpLowGetFirstValidRemotePortChannelIndex (INT4
                                                   *pi4FsLaPortChannelIfIndex,
                                                   tMacAddr *
                                                   pFsLaDLAGRemotePortChannelSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            break;
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    *pi4FsLaPortChannelIfIndex = pAggEntry->u2AggIndex;

    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    LA_MEMCPY (pFsLaDLAGRemotePortChannelSystemID,
               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaMCLAGSnmpLowGetNextValidRemotePortChannelIndex           */
/* Description :  This function returns the next index of MC-LAG Remote      */
/*                Aggregator Table. If this function is passed with NULL     */
/*                values, then first index will be returned                  */
/*                                                                           */
/* Input       :  PortChannelIfIndex and Remote Port Channel System ID       */
/*                                                                           */
/* Output      :  Next PortChannelIfIndex and Next Remote Port               */
/*                Channel System ID                                          */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaMCLAGSnmpLowGetNextValidRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                                  INT4
                                                  *pi4NextFsLaPortChannelIfIndex,
                                                  tMacAddr
                                                  FsLaDlagRemotePortChannelSysID,
                                                  tMacAddr *
                                                  pNextFsLaDLAGRemotePortChannelSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pNextRemoteAggEntry = NULL;

    if ((i4FsLaPortChannelIfIndex < 0)
        || (i4FsLaPortChannelIfIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    if (LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry) !=
        LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    while (pAggEntry != NULL)
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            break;
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    /* Get the Remote Aggregator entry */

    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDlagRemotePortChannelSysID,
                                       pAggEntry, &pRemoteAggEntry);

    do
    {
        /* Get the next remote aggreator entry 
         * if the entry is present, then retrive the index values and return*/
        if (LaDLAGGetNextRemoteAggEntry
            (pAggEntry, pRemoteAggEntry, &pNextRemoteAggEntry) == LA_SUCCESS)
        {
            if (NULL != pAggEntry)
            {
                *pi4NextFsLaPortChannelIfIndex = pAggEntry->u2AggIndex;
            }
            MEMCPY (pNextFsLaDLAGRemotePortChannelSystemID,
                    pNextRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                    LA_MAC_ADDRESS_SIZE);
            return LA_SUCCESS;
        }
        /* If there are more than one Port Channnel present, get the next 
         * aggregator entry. Set the remote aggregator entry to NULL. 
         * So that in the next iteration it will return the first index
         * of the remote port channel*/
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
        pRemoteAggEntry = NULL;
    }
    while (pAggEntry != NULL);    /* Iterate until the port channel entry is NULL */

    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaMCLAGSnmpLowGetFirstValidRemotePortIndex           */
/*                                                                           */
/* Description        : This function gets the first valid index of the      */
/*                      MC-LAG Remote Port Table. This function is called    */
/*                      with these output parameters initialized to zero     */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : Port Channel Index, Remote Port Channel System ID    */
/*                      RemotePortIndex                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaMCLAGSnmpLowGetFirstValidRemotePortIndex (INT4 *pi4FsLaPortChannelIfIndex,
                                            tMacAddr *
                                            pFsLaDLAGRemotePortChannelSystemID,
                                            INT4 *pi4FsLaDLAGRemotePortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);

    while (pAggEntry != NULL)
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            break;
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    if (pRemotePortEntry == NULL)
    {
        return LA_FAILURE;
    }

    *pi4FsLaPortChannelIfIndex = pAggEntry->u2AggIndex;
    LA_MEMCPY (pFsLaDLAGRemotePortChannelSystemID,
               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);
    *pi4FsLaDLAGRemotePortIndex = (INT4) pRemotePortEntry->u4PortIndex;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaMCLAGSnmpLowGetNextValidRemotePortIndex                   */
/* Description :  This function returns the next index of the MC-LAG Remote   */
/*                Port Table. If this function is passed with NULL           */
/*                values, then first index will be returned                  */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex,FsLaDlagRemotePortChannelSysID */
/*                i4FsLaDLAGRemotePortIndex                                  */
/*                                                                           */
/* Output      :  pi4NextFsLaPortChannelIfIndex,                             */
/*                pNextFsLaDLAGRemotePortChannelSystemId,                    */
/*                pi4NextFsLaDLAGRemotePortIndex                             */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaMCLAGSnmpLowGetNextValidRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                           INT4 *pi4NextFsLaPortChannelIfIndex,
                                           tMacAddr
                                           FsLaDlagRemotePortChannelSysID,
                                           tMacAddr *
                                           pNextFsLaDLAGRemotePortChannelSystemID,
                                           INT4 i4FsLaDLAGRemotePortIndex,
                                           INT4 *pi4NextFsLaDLAGRemotePortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGRemotePortEntry *pNextRemotePortEntry = NULL;

    if ((i4FsLaPortChannelIfIndex < 0)
        || (i4FsLaPortChannelIfIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    /* Get the aggregator entry */
    if (LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry) !=
        LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    while (pAggEntry != NULL)
    {
        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            break;
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    /* Get the remote  aggregator entry */
    if (LaDLAGGetRemoteAggEntryBasedOnMac
        (FsLaDlagRemotePortChannelSysID, pAggEntry,
         &pRemoteAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }

    LaDLAGGetRemotePortEntry ((UINT4) i4FsLaDLAGRemotePortIndex,
                              pRemoteAggEntry, &pRemotePortEntry);
    do
    {
        do
        {
            if (LaDLAGGetNextRemotePortEntry
                (pRemoteAggEntry, pRemotePortEntry,
                 &pNextRemotePortEntry) == LA_SUCCESS)
            {
                *pi4NextFsLaPortChannelIfIndex = pAggEntry->u2AggIndex;
                MEMCPY (pNextFsLaDLAGRemotePortChannelSystemID,
                        pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                        LA_MAC_ADDRESS_SIZE);
                *pi4NextFsLaDLAGRemotePortIndex =
                    (INT4) pNextRemotePortEntry->u4PortIndex;
                return LA_SUCCESS;
            }
            LaDLAGGetNextRemoteAggEntry (pAggEntry, pRemoteAggEntry,
                                         &pRemoteAggEntry);
            pRemotePortEntry = NULL;
        }
        while (pRemoteAggEntry != NULL);

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
        pRemotePortEntry = NULL;
        pRemoteAggEntry = NULL;
    }
    while (pAggEntry != NULL);

    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaMCLAGCheckMasterSlaveLinkAvail                     */
/*                                                                           */
/* Description        : This function programs the ports in H/w while        */
/*                      giving the PDU to Slaves by Master.                  */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS- On Success                               */
/*                      LA_ERR_NULL_PTR - If Null pointer received           */
/*                                                                           */
/*****************************************************************************/

VOID
LaMCLAGCheckMasterSlaveLinkAvail (tLaLacAggEntry * pAggEntry, UINT1 *pu1PiMask)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    UINT1               u1IsSlave = LA_FALSE;
    UINT1               u1IsMaster = LA_FALSE;

    LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        if ((pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD) &&
            ((pAggEntry->u1AggOperStatus == LA_OPER_UP)))
        {
            if ((pConsPortEntry->u4PortIndex & LA_MSB_BIT_SET) ==
                LA_MSB_BIT_SET)
            {
                u1IsSlave = LA_TRUE;
            }
            else
            {
                u1IsMaster = LA_TRUE;
            }
        }
        LaActiveDLAGGetNextPortEntryFromConsTree (pConsPortEntry,
                                                  &pConsPortEntry, pAggEntry);

    }                            /* End of while */
    if ((u1IsSlave == LA_TRUE) && (u1IsMaster == LA_TRUE))
    {
        *pu1PiMask = VLAN_PI_MASK;
    }
    else
    {
        *pu1PiMask = VLAN_PI_UNMASK;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : LaMCLAGUpdateRemoteLagAdminStatus                    */
/*                                                                           */
/* Description        : This function triggers consolidation logic based on  */
/*                      the admin status of the remote port-channel.         */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                      RemoteSysMac  - Remote System Mac address            */
/*                      u1AdminStatus - Admin Status of remote port-channel  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS- On Success                               */
/*                      LA_FAILURE  - On Failure                             */
/*                      LA_ERR_NULL_PTR - If Null pointer received           */
/*                                                                           */
/*****************************************************************************/
INT4
LaMCLAGUpdateRemoteLagAdminStatus (tLaMacAddr RemoteSysMac,
                                   tLaLacAggEntry * pAggEntry,
                                   UINT1 u1AdminStatus)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaMCLAGUpdateRemoteLagAdminStatus: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }
    /* Search Remote MC-LAG Node Info List if this is Master */
    LaDLAGGetRemoteAggEntryBasedOnMac (RemoteSysMac, pAggEntry,
                                       &pRemoteAggEntry);

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaMCLAGUpdateRemoteLagAdminStatus: NULL POINTER.\n");
        return LA_FAILURE;
    }

    if (u1AdminStatus == CFA_IF_UP)
    {
        /* Set the update required flag so that consolidation takes place */
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;
        pRemoteAggEntry->u1AdminStatus = CFA_IF_UP;
        LaActiveMCLAGUpdateConsolidatedList (pRemoteAggEntry,
                                             LA_DLAG_CPY_REM_NODE_INFO);
        return LA_SUCCESS;
    }

    pRemoteAggEntry->u1AdminStatus = CFA_IF_DOWN;
    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                  &pConsPortEntry, pAggEntry);

        if ((pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL) &&
            (pConsPortEntry != NULL))
        {
            /* If Admin Status is CFA_IF_DOWN move all the ports from
               ADD to DELETE state and send indication. */

            if (u1AdminStatus == CFA_IF_DOWN)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "\n Port Moved to Down In bundle =%d\r\n",
                             pRemotePortEntry->u4PortIndex);
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    /* Changing Port State From ADD to DELETE */
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
                }
            }

        }
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }                            /* while ends */

    /* Sort the ports based on current port state */
    LaActiveDLAGSortOnStandbyConstraints (pAggEntry);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMCLAGDeInit                                        */
/*                                                                           */
/* Description        : This function is used to de-initialize the MC-LAG    */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which MC-LAG feature */
/*                                  should be stopped.                       */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaMCLAGDeInit (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    INT4                i4RetVal = LA_SUCCESS;
    tLaDLAGTxInfo       LaDLAGTxInfo;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaMCLAGDeInit: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    /* Stop MC-LAG Periodic sync timer */
    if ((LaStopTimer (&pAggEntry->DLAGPeriodicSyncTmr)) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaMCLAGDeInit: LaStopTimer failed for MC-LAG "
                "Periodic Sync Timer\n");
        i4RetVal = LA_FAILURE;
    }

    /* Reset Actor System ID to Global System ID */
    if (LaDLAGResetActorSystemID (pAggEntry) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaMCLAGDeInit: LaDLAGResetActorSystemID failed.\n");
        i4RetVal = LA_FAILURE;
    }

    /* Reset Actor System Priority to Global System ID */
    if (LaDLAGResetActorSystemPriority (pAggEntry) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaMCLAGDeInit: LaDLAGResetActorSystemPriority failed.\n");
        i4RetVal = LA_FAILURE;
    }

    LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);

    /* Delete all remote aggregator entries */
    LaDLAGDeleteAllRemoteAggEntries (pAggEntry);

    /* Destroy RBTree of DLAG Remote Aggregator Entry */
    LaDLAGRemoteAggInfoTreeDestroy (pAggEntry->LaDLAGRemoteAggInfoTree,
                                    NULL, 0);
    pAggEntry->LaDLAGRemoteAggInfoTree = NULL;

    if ((pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED) &&
        (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_SLAVE))
    {
        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
            LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);
            LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);

        }
    }

    if ((pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED) &&
        (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER))
    {
        LaActiveDLAGDeleteEntriesInAllTree (pAggEntry);

        LaActiveDLAGDestroyPortListTree (pAggEntry->LaDLAGConsInfoTable.
                                         LaDLAGConsolidatedList, NULL, 0);
        pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList = NULL;
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "\n Free Units of CONS Entry POOL when shutdown = %d \n",
                     MemGetFreeUnits (LA_DLAGCONSPORTENTRY_POOL_ID));
    }

    LaActiveDLAGDestroyPortListTree (pAggEntry->LaDLAGConsInfoTable.
                                     LaDLAGPortList, NULL, 0);
    pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList = NULL;

    /* Change The Node Role Played as None */
    pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_NONE;

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : LaIcchProcessMclagOperDown                           */
/*                                                                           */
/* Description        : This function handles the MCLAG PortChannel down event*/
/*                      Based on Whether the role of the node is Master/Slave*/
/*                      ,The MC-LAG PortChannel down will be handled.        */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
LaIcchProcessMclagOperDown (UINT4 u4IfIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT1               u1IsRemoteMclagPortUp = LA_FALSE;

    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return;
    }
    /* When Mc-LAG PortChanel goes Down,
     * 1) Master Node : We check the Consolidation table and check whether
     * a remote port-channel is UP in bundle. If the remote port-channel is
     * up we program the FDB with ICCL port in NP. If remote port-channel is 
     * down we flush the FDB Entry both on NP and on SW_LEARNING database.
     *
     * 2) Slave Node: We frame and send a Check Port Status to the Master and 
     * program the FDB on the port-channel on the ICCL. The Master Node should
     * send back a port-down message incase the port is Down. On reception of
     * the Port-Down message the FDB Entries will be flushed.
     */

    LaApiIsRemoteMclagUp (u4IfIndex, &u1IsRemoteMclagPortUp);

    if (u1IsRemoteMclagPortUp == LA_TRUE)
    {
        LaVlanIcchProcessMclagDown (u4IfIndex, LA_PORT_UP_IN_BNDL);
    }
    else
    {
        LaVlanIcchProcessMclagDown (u4IfIndex, LA_PORT_DOWN);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaIcchProcessMclagOperDownAtMaster                   */
/*                                                                           */
/* Description        : This function process the MC-LAG PortChannel down at */
/*                      Master Node. Based on oper-status of the port-channel*/
/*                      in the peer node actions will be taken.              */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                      pAggEntry -AggEntry                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
LaIcchProcessMclagOperDownAtMaster (UINT4 u4IfIndex, tLaLacAggEntry * pAggEntry)
{
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    if (pAggEntry == NULL)
    {
        return;
    }

    pRemoteAggEntry = (tLaDLAGRemoteAggEntry *) RBTreeGetFirst
        (pAggEntry->LaDLAGRemoteAggInfoTree);

    if (pRemoteAggEntry == NULL)
    {
        return;
    }

    if (LA_SUCCESS != LaDLAGGetNextRemotePortEntry (pRemoteAggEntry,
                                                    NULL, &pRemotePortEntry))
    {
        return;
    }

    if (pRemotePortEntry == NULL)
    {
        return;
    }
    else if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
    {
        LaVlanIcchProcessMclagDown (u4IfIndex, LA_PORT_UP_IN_BNDL);
    }
    else
    {
        LaVlanIcchProcessMclagDown (u4IfIndex, LA_PORT_DOWN);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaIcchProcessMclagOperDownAtSlave                    */
/*                                                                           */
/* Description        : This function process the MC-LAG PortChannel down at */
/*                      Slave Node. The PortStatus will be checked on the    */
/*                      peer node. If Interface is not present the then fdb  */
/*                      entries learnt on the PortChannel  will be flushed  */
/*                      else the same would be programmed as if learnt on    */
/*                      ICCL till the MC-LAG PortChannel comes up.           */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface index of the MC-LAG PortChannel.*/
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
LaIcchProcessMclagOperDownAtSlave (UINT4 u4IfIndex)
{
    LA_UNLOCK ();
    LaVlanIcchCheckPortStatus (u4IfIndex);
    LA_LOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : LaLacGetMCLAGActivePorts.                            */
/*                                                                           */
/* Description        : This function is called from LaActiveMCLAGHwControl  */
/*                      to get the port indices of all the ports selected    */
/*                      for a particular aggregator.                         */
/*                                                                           */
/* Input(s)           : The aggregator index.                                */
/*                                                                           */
/* Output(s)          : The port indices filled in the Linear buffer,        */
/*                      count of the number of ports.                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer.      */
/*****************************************************************************/
INT4
LaLacGetMCLAGActivePorts (UINT2 u2AggIndex, UINT2 *pLinearBuf,
                          UINT1 *pu1PortCount)
{

    tLaLacPortEntry    *pPortNode = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;
    tLaCallBackArgs     LaCallBackArgs;

    if ((pLinearBuf == NULL) || (pu1PortCount == NULL))
    {

        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "LaLacGetMCLAGActivePorts: "
                "NULL pointer passed\r\n");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&LaCallBackArgs, 0, sizeof (tLaCallBackArgs));

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC,
                "LaLacGetMCLAGActivePorts: Aggregation entry not present\r\n");

        return LA_FAILURE;
    }

    *pu1PortCount = 0;

    /* This function will update the ports which are up in bundle 
       with sync bit set. The port status to be up. 
     */
    LA_SLL_SCAN (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry), pPortNode,
                 tLaLacPortEntry *)
    {
        LaCallBackArgs.u2Port = pPortNode->u2PortIndex;
        i4RetVal = LaCustCallBack (LA_GET_ACTIVE_PORT_EVENT, &LaCallBackArgs);

        if ((pPortNode->OperIndividual == LA_FALSE) &&
            (pPortNode->LaLacActorInfo.LaLacPortState.LaSynchronization ==
             LA_TRUE) &&
            (pPortNode->LaLacActorInfo.LaLacPortState.LaDistributing ==
             LA_TRUE) &&
            (pPortNode->LaPortEnabled == LA_PORT_ENABLED) &&
            (i4RetVal == LA_SUCCESS) &&
            (LaCallBackArgs.u2PortState == LA_OPER_UP))
        {
            *pLinearBuf = pPortNode->u2PortIndex;
            ++pLinearBuf;
            ++(*pu1PortCount);
        }
    }

    return LA_SUCCESS;
}
