/* $Id: latxm.c,v 1.10 2011/09/12 07:13:23 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : latxm.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Link Aggregation Transmit Machine module       */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains contains functions for the  */
/*                            Transmit State Machine                         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaLacTxLacpdu.                                       */
/*                                                                           */
/* Description        : This function is called LaLacTxMachine() to form and */
/*                      hand over a LACPDU packet to Control Parser to be    */
/*                      sent out.                                            */
/*                                                                           */
/* Input(s)           : pPortEntry                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL port pointer  */
/*****************************************************************************/
INT4
LaLacTxLacpdu (tLaLacPortEntry * pPortEntry)
{
    UINT1               aLinearBuf[LA_SLOW_PROTOCOL_SIZE];
    tLaBufChainHeader  *pMsgBuf;
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;

    /* Transmit only in Active node. Otherwise just set
     * the u4HoldCount = 0 and return. */
    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        pPortEntry->u4HoldCount = 0;
        return LA_SUCCESS;
    }

    LA_MEMSET (aLinearBuf, 0, LA_SLOW_PROTOCOL_SIZE);

    if (LaLacFormLacpdu (pPortEntry, aLinearBuf) != LA_SUCCESS)
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "TXM: Port: %u :: LaLacFormLacpdu() failed\n",
                     pPortEntry->u2PortIndex);
        return LA_FAILURE;
    }
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    if ((LA_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) &&
        (LA_HR_CHECK_STDY_ST_PKT (aLinearBuf)))
    {
        /* steady state pkt validation succeed here. send the pkt 
         * to RM. */
        if (LaRedHRSendStdyStPkt (aLinearBuf, LA_LACPDU_SIZE,
                                  pPortEntry->u2PortIndex,
                                  pPortEntry->u4PeriodicTime) == LA_FAILURE)
        {
            LA_TRC (ALL_FAILURE_TRC, "LA: Sending steady state packet to "
                    "RM failed \r\n");
            return LA_FAILURE;
        }
        return LA_SUCCESS;
    }
#endif
    if ((pMsgBuf = LA_ALLOC_CRU_BUF (LA_LACPDU_SIZE, 0)) == NULL)
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "TXM: Port: %u :: Buffer Allocation failed\n",
                     pPortEntry->u2PortIndex);
        return LA_FAILURE;
    }

    LA_COPY_OVER_CRU_BUF (pMsgBuf, aLinearBuf, 0, LA_SLOW_PROTOCOL_SIZE);
    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "TXM: Port: %u :: Handing over LACPDU to Lower Layer...\n",
                 pPortEntry->u2PortIndex);

    LaHandOverOutFrame (pMsgBuf, pPortEntry->u2PortIndex, NULL);

    /* Update Tx Stats */
    ++(pPortEntry->LaPortStats.u4LacpduTx);

    /* Get the AggIfEntry pointer */
    LA_GET_AGGIFENTRY (pPortEntry, pLaLacAggIfEntry);

    if (pLaLacAggIfEntry == NULL)
    {
        return LA_FAILURE;
    }

    /* Increment the Aggregator If Statistics if port is attached */
    if (pPortEntry->LaAggAttached == LA_TRUE)
    {

        /* We increment all the statistics negatively for the Aggregator,
         * because these frames have already been counted once, at the
         * port level */
        pLaLacAggIfEntry->i4AggOutOctets += -(LA_SLOW_PROTOCOL_SIZE);
        (pLaLacAggIfEntry->i4AggOutFrames)--;
        (pLaLacAggIfEntry->i4AggOutMcastPkts)--;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacTxMachine                                       */
/*                                                                           */
/* Description        : This function is called when certain events occur.   */
/*                      When a LACPDU is received, this function is called   */
/*                      - with the receiving port's port entry and received  */
/*                      LACPDU info.                                         */
/*                      - with each of other ports' port entry and the       */
/*                      received LACPDU to check whether this LACPDU info    */
/*                      was earlier received by any other port.              */
/*                                                                           */
/* Input(s)           : Port Entry, An Event                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL port pointer  */
/*****************************************************************************/
INT4
LaLacTxMachine (tLaLacPortEntry * pPortEntry, tLaTxSmEvent LaEvent)
{
    if (pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TXM: Port Entry Pointer NULL");
        return LA_ERR_NULL_PTR;
    }

    switch (LaEvent)
    {

        case LA_TXM_EVENT_PORT_INIT:
            /* Intentional fall-through */

        case LA_TXM_EVENT_PORT_DISABLED:

            pPortEntry->NttFlag = LA_FALSE;
            pPortEntry->u4HoldCount = 0;
            break;

        case LA_TXM_EVENT_PORT_ENABLED:

            pPortEntry->NttFlag = LA_FALSE;
            break;

        case LA_TXM_EVENT_NTT:

            if (pPortEntry->PTxSmState != LA_PTXM_STATE_NO_PERIODIC)
            {
                if (pPortEntry->u4HoldCount < MAX_TX_PER_INTERVAL)
                {
                    if (LaLacTxLacpdu (pPortEntry) == LA_SUCCESS)
                    {

                        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                     "TXM: Port: %u :: LACPDU transmitted out\n",
                                     pPortEntry->u2PortIndex);

                        (pPortEntry->u4HoldCount)++;
                        pPortEntry->NttFlag = LA_FALSE;
                    }
                }
                else
                {
                    /* Already  3 PDUs transmitted in the 
                     * fast periodic interval (1 sec) 
                     * so postpone transmission till the 
                     * end of this fast periodic interval (tick timer expiry).
                     */
                    LA_TRC (CONTROL_PLANE_TRC,
                            "\nLA: BPDU Tx postponed - limit!!");
                    pPortEntry->NttFlag = LA_TRUE;
                }
            }
            else
            {
                pPortEntry->NttFlag = LA_FALSE;
            }
            break;

        case LA_TXM_EVENT_TICK:

            /* Decrement the hold count */
            if (pPortEntry->u4HoldCount > 0)
            {
                pPortEntry->u4HoldCount = 0;
                if (pPortEntry->NttFlag == LA_TRUE)
                {
                    if (pPortEntry->PTxSmState != LA_PTXM_STATE_NO_PERIODIC)
                    {
                        if (LaLacTxLacpdu (pPortEntry) != LA_SUCCESS)
                        {
                            LA_TRC (CONTROL_PLANE_TRC,
                                    "TXM: LACPDU transmission failed"
                                    "in Tick Timer expiry () failed \n");

                            return LA_FAILURE;
                        }
                    }
                }
                pPortEntry->NttFlag = LA_FALSE;
            }

            break;

        case LA_TXM_EVENT_TX_DATA:
            if ((pPortEntry->NttFlag == LA_TRUE) &&
                (pPortEntry->LaLacpEnabled == LA_LACP_ENABLED) &&
                (pPortEntry->LaPortEnabled == LA_PORT_ENABLED) &&
                ((pPortEntry->LaLacActorInfo.LaLacPortState.
                  LaLacpActivity == LA_ACTIVE)
                 || (pPortEntry->LaLacPartnerInfo.LaLacPortState.
                     LaLacpActivity == LA_ACTIVE)) &&
                (pPortEntry->LaLacpMode == LA_MODE_LACP))
            {

                if (LaLacTxLacpdu (pPortEntry) == LA_SUCCESS)
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                 "TXM: Port: %u :: LACPDU transmitted out\n",
                                 pPortEntry->u2PortIndex);

                    (pPortEntry->u4HoldCount)++;
                    pPortEntry->NttFlag = LA_FALSE;
                }
            }
            pPortEntry->NttFlag = LA_FALSE;
            break;

        default:

            break;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacPeriodicTxMachine                               */
/*                                                                           */
/* Description        : This function is called during initialisation and    */
/*                      disabling of a port and after expiry of Tick Timer.  */
/*                      This function checks whether the PeriodicAfter timer */
/*                      has expired and whether periodic transmission of     */
/*                      LACPDUs will take place.                             */
/*                                                                           */
/* Input(s)           : Port Entry pointer, Event.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL port pointer  */
/*****************************************************************************/
INT4
LaLacPeriodicTxMachine (tLaLacPortEntry * pPortEntry, tLaPTxSmEvent LaEvent)
{
    switch (LaEvent)
    {
        case LA_PTXM_EVENT_PORT_INIT:
            /* This event is received when BEGIN = TRUE */
            LaLacPTxSmMakeNoPeriodic (pPortEntry);
            break;

        case LA_PTXM_EVENT_VARS_CHANGED:
            /* (LACP_Enabled == FALSE) or (port_enabled == FALSE) 
             * or (Actor_Oper_Port_State.LACP_Activity
             * == PASSIVE && Partner_Oper_Port_State.LACP_Activity == PASSIVE) 
             */
            if ((pPortEntry->LaLacpMode != LA_MODE_LACP) ||
                (pPortEntry->LaPortEnabled == LA_PORT_DISABLED) ||
                (pPortEntry->LaLacpEnabled == LA_LACP_DISABLED) ||
                ((pPortEntry->LaLacActorInfo.
                  LaLacPortState.LaLacpActivity == LA_PASSIVE) &&
                 (pPortEntry->LaLacPartnerInfo.
                  LaLacPortState.LaLacpActivity == LA_PASSIVE)))
            {
                if (pPortEntry->PTxSmState != LA_PTXM_STATE_NO_PERIODIC)
                {
                    LaLacPTxSmMakeNoPeriodic (pPortEntry);
                }
            }
            else
            {
                switch (pPortEntry->PTxSmState)
                {
                    case LA_PTXM_STATE_NO_PERIODIC:
                        LaLacPTxSmMakeFastPeriodic (pPortEntry);
                        break;
                    case LA_PTXM_STATE_FAST_PERIODIC:
                        if (pPortEntry->LaLacPartnerInfo.
                            LaLacPortState.LaLacpTimeout == LA_LONG_TIMEOUT)
                        {
                            LaLacPTxSmMakePeriodicTx (pPortEntry);
                        }
                        break;
                    case LA_PTXM_STATE_SLOW_PERIODIC:
                        if (pPortEntry->LaLacPartnerInfo.
                            LaLacPortState.LaLacpTimeout == LA_SHORT_TIMEOUT)
                        {
                            LaLacPTxSmMakePeriodicTx (pPortEntry);
                        }
                        break;
                    default:
                        break;
                }
            }
            break;

        case LA_PTXM_EVENT_PTMR_EXPIRED:
            /* Periodic Timer Expired */
            if (pPortEntry->PTxSmState != LA_PTXM_STATE_NO_PERIODIC)
            {
                LaLacPTxSmMakePeriodicTx (pPortEntry);
            }
            break;

        default:
            /* Unknown Event received */
            return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacPTxSmMakeNoPeriodic                             */
/*                                                                           */
/* Description        : This function is called to make the port not to      */
/*                      transmit any LACPDU.This function also checks whether*/
/*                      any transition required for making the port to       */
/*                      transmit fast LACPDU's.                              */
/*                                                                           */
/* Input(s)           : Port Entry pointer                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL port pointer  */
/*****************************************************************************/

INT4
LaLacPTxSmMakeNoPeriodic (tLaLacPortEntry * pPortEntry)
{
    if (LaStopTimer (&(pPortEntry->PeriodicTmr)) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    pPortEntry->PTxSmState = LA_PTXM_STATE_NO_PERIODIC;

    if ((pPortEntry->LaPortEnabled == LA_PORT_ENABLED) &&
        (pPortEntry->LaLacpMode == LA_MODE_LACP) &&
        (pPortEntry->LaLacpEnabled == LA_LACP_ENABLED) &&
        ((pPortEntry->LaLacActorInfo.LaLacPortState.
          LaLacpActivity == LA_ACTIVE)
         ||
         (pPortEntry->LaLacPartnerInfo.LaLacPortState.
          LaLacpActivity == LA_ACTIVE)))
    {
        if (LaLacPTxSmMakeFastPeriodic (pPortEntry) != LA_SUCCESS)
        {
            return LA_FAILURE;
        }
    }

    /* Donot change the partner sync bit when the rx machine
     * is in defaulted state, since defaulted state 
     * indicates that there can be no disaggrement.
     */
    else if ((pPortEntry->LaLacPartnerInfo.
              LaLacPortState.LaSynchronization == LA_TRUE) &&
             (pPortEntry->InDefaulted == LA_FALSE))
    {
        pPortEntry->LaLacPartnerInfo.
            LaLacPortState.LaSynchronization = LA_FALSE;
        LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_PARTNER_NO_SYNC);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacPTxSmMakeFastPeriodic                           */
/*                                                                           */
/* Description        : This function is called to transmit Fast Lacpdu based*/
/*                      on the timeout value.                                */
/*                                                                           */
/* Input(s)           : Port Entry pointer                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaLacPTxSmMakeFastPeriodic (tLaLacPortEntry * pPortEntry)
{
    /* FAST_PERIODIC state (IEEE802.3-2000 section 43.4.13 )  */

    pPortEntry->u4PeriodicTime = LA_FAST_PERIODIC_TIME;
    if (LaStartTimer (&(pPortEntry->PeriodicTmr)) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    pPortEntry->PTxSmState = LA_PTXM_STATE_FAST_PERIODIC;

    /* Start the periodic timer as per the Partner Lacp_Timeout value */
    if (pPortEntry->LaLacPartnerInfo.
        LaLacPortState.LaLacpTimeout == LA_LONG_TIMEOUT)
    {
        return (LaLacPTxSmMakePeriodicTx (pPortEntry));
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacPTxSmMakeSlowPeriodic                           */
/*                                                                           */
/* Description        : This function is called to transmit Slow Lacpdu based*/
/*                      on the timeout value.                                */
/*                                                                           */
/* Input(s)           : Port Entry pointer                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaLacPTxSmMakeSlowPeriodic (tLaLacPortEntry * pPortEntry)
{
    /* This function implements SLOW_PERIODIC 
     * state (IEEE802.3-2000 section 43.4.13 )  
     */

    pPortEntry->u4PeriodicTime = LA_SLOW_PERIODIC_TIME;

    /* Start the periodic timer as per the Lacp_Timeout value */
    if (LaStartTimer (&(pPortEntry->PeriodicTmr)) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    pPortEntry->PTxSmState = LA_PTXM_STATE_SLOW_PERIODIC;
    if (pPortEntry->LaLacPartnerInfo.
        LaLacPortState.LaLacpTimeout == LA_SHORT_TIMEOUT)
    {
        return (LaLacPTxSmMakePeriodicTx (pPortEntry));
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacPTxSmMakePeriodicTx                             */
/*                                                                           */
/* Description        : This function is called to make the decision whether */
/*                      the port to be transmit FAST/SLOW/NO LACPDU's        */
/*                                                                           */
/* Input(s)           : Port Entry pointer                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaLacPTxSmMakePeriodicTx (tLaLacPortEntry * pPortEntry)
{
    /* Set NTT & Try to transmit LACP pdu at the earliest using Tx Machine */

    pPortEntry->NttFlag = LA_TRUE;
    if (LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    pPortEntry->PTxSmState = LA_PTXM_STATE_PERIODIC_TX;
    /* Go to FAST_PERIODIC or SLOW_PERIODIC depending on LACP_Timeout 
     * of partner oper values 
     */
    if (pPortEntry->LaLacPartnerInfo.
        LaLacPortState.LaLacpTimeout == LA_SHORT_TIMEOUT)
    {
        return (LaLacPTxSmMakeFastPeriodic (pPortEntry));
    }
    else
    {
        return (LaLacPTxSmMakeSlowPeriodic (pPortEntry));
    }
}

/* end of file */
