/*****************************************************************************/
/* $Id: laapi.c,v 1.65 2018/01/11 11:18:53 siva Exp $                     */
/*    FILE  NAME            : laapi.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Interfaces with external Modules            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 07 April 2005                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains control parser & multiplexer*/
/*                            functions for LA. It contains all the interface*/
/*                            functions with lower and upper layers.         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 APRIL 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*****************************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/
/* Function Name      : LaHandleOutgoingFrame                                */
/*                                                                           */
/* Description        : If LA is enabled in the system the L2IWF should invoke */
/*                      this API to get the actual physical port on which the*/
/*                      frame has to be sent.The LA Distributor module select*/
/*                      the link based on Distribution Algorithm.            */
/*                                                                           */
/*                      If the return value is LA_SUCCESS, then pu2PortIndex */
/*                      contains the physical port on which CFA should send  */
/*                      this Frame.                                          */
/* Input(s)           : pBuf - Pointer to the data buffer to be transmitted  */
/*                      u2AggIfIndex - Logical Index given by the higher mods*/
/*                      u2Protocol - Protocol of the data in the buffer      */
/*                      u1EncapType - Encapsulation type to be used          */
/*                                                                           */
/* Output(s)          : *pu2PortIndex - Physical port on which to send frame.*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*          LA_SUCCESS - if corresponding phy. portindex could be found. CFA */
/*                       uses this port to tx frame.                         */
/*          LA_FAILURE - If error/port not found. CFA should drop the frame. */
/*          LA_IGNORE  - Do nothing and exit                                 */
/*****************************************************************************/
INT4
LaHandleOutgoingFrame (tLaBufChainHeader * pBuf, UINT2 u2AggIndex,
                       UINT2 u2Protocol, UINT1 u1EncapType, UINT2 *pu2PortIndex)
{
    UINT2               u2SelectedPortIndex;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4LocalPortIndex = 0;
    UINT2               u2PortIndex;
    UINT1               u1IfType = LA_ETHERNET_TYPE;
    tCfaIfInfo          CfaIfInfo;

    UNUSED_PARAM (u1EncapType);

    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    LA_LOCK ();

    if (LA_MODULE_STATUS != LA_ENABLED)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }

    if (pBuf == NULL)
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                     "LaHandleOutgoingFrame: AggIndex %u :"
                     "Outgoing CRU Buffer - Null Pointer\n", u2AggIndex);
        LA_UNLOCK ();
        return LA_IGNORE;
    }

    LA_PKT_DUMP (DUMP_TRC, pBuf, 16, "CTRL: Dumping Outgoing frame header..\n");
    if (LaCfaGetIfInfo (u2AggIndex, &CfaIfInfo) == CFA_SUCCESS)
    {
        u1IfType = CfaIfInfo.u1IfType;
    }

    LA_TRC_ARG1 (CONTROL_PLANE_TRC | DATA_PATH_TRC,
                 "Called  LaHandleOutgoingFrame for AggIndex %d\n", u2AggIndex);

    if (u1IfType == LA_PORT_CHANNEL_TYPE)
    {
        LaGetAggEntry (u2AggIndex, &pAggEntry);

        if ((pAggEntry == NULL) || (pAggEntry->u1AggOperStatus == LA_OPER_DOWN))
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "LaHandleOutgoingFrame: AggIndex %d"
                         "Null Pointer for AggEntry or AggIndex Oper down\n",
                         u2AggIndex);
            LA_UNLOCK ();
            return LA_FAILURE;
        }
        if (u2Protocol == CFA_PROT_BPDU)
        {
            LaGetNextAggDistributingPort (u2AggIndex, 0, &u2PortIndex);

            if (u2PortIndex == 0)
            {
                if (((LA_DLAG_SYSTEM_STATUS == LA_ENABLED)) ||
                    (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
                {
                    if ((pAggEntry->u1DLAGRolePlayed ==
                         LA_DLAG_SYSTEM_ROLE_MASTER) ||
                        (pAggEntry->u1MCLAGRolePlayed ==
                         LA_MCLAG_SYSTEM_ROLE_MASTER))
                    {
                        /* Get the Remote ports of this port channel */
                        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry,
                                                      pAggEntry);
                        while (pConsPortEntry != NULL)
                        {
                            LaActiveDLAGGetIfIndexFromBridgeIndex
                                (pConsPortEntry->u4PortIndex,
                                 &u4LocalPortIndex);
                            LaGetPortEntry ((UINT2) u4LocalPortIndex,
                                            &pPortEntry);
                            if ((pPortEntry != NULL)
                                && (pConsPortEntry->u1PortStateFlag ==
                                    LA_AA_DLAG_PORT_IN_ADD))
                            {

                                *pu2PortIndex = (UINT2) u4LocalPortIndex;
                                LA_UNLOCK ();
                                return LA_SUCCESS;

                            }

                            LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                                          &pConsPortEntry,
                                                          pAggEntry);
                        }
                    }

                }

                LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                             "LaHandleOutgoingFrame: AggIndex %d"
                             "No Port in the Portchannel, so discard packet\n",
                             u2AggIndex);
                LA_UNLOCK ();
                return LA_FAILURE;
            }
            else
            {
                *pu2PortIndex = u2PortIndex;
                LA_UNLOCK ();
                return LA_SUCCESS;
            }
        }
        if (LaAggSelectLink (pBuf, u2AggIndex, &u2SelectedPortIndex) !=
            LA_SUCCESS)
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "LaHandleOutgoingFrame:AggIndex %d:"
                         "LaAggSelectLink function FAILED\n", u2AggIndex);
            LA_UNLOCK ();
            return LA_FAILURE;
        }
        LaGetPortEntry (u2SelectedPortIndex, &pPortEntry);

        if (NULL == pPortEntry)
        {
            LA_TRC_ARG2 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "LaHandleOutgoingFrame: AggIndex %d:"
                         "Selected Port Index %u is invalid; Discarding frame..\n",
                         u2AggIndex, u2SelectedPortIndex);
            LA_UNLOCK ();
            return LA_FAILURE;
        }

        if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing == LA_TRUE)
        {

            *pu2PortIndex = pPortEntry->u2PortIndex;
            LA_UNLOCK ();
            return LA_SUCCESS;
        }
        /* Port state is not Distributing, so discard frame. */

        LA_TRC_ARG2 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                     "LaHandleOutgoingFrame: AggIndex %u:"
                     "DISTRIBUTING not Enabled"
                     "in the port %d Discarding Frame\r\n", u2SelectedPortIndex,
                     pPortEntry->u2PortIndex);

        /* Incrementing the Aggregator If Statistics */
        LaIncrementStatsOnDistributorDiscard (pBuf, pPortEntry);
    }
    else
    {
        LaGetPortEntry (u2AggIndex, &pPortEntry);

        if ((pPortEntry == NULL)
            || (pPortEntry->LaLacpMode != LA_MODE_DISABLED))
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "LaHandleOutgoingFrame:AggIndex %d:"
                         "Port Entry is NULL or Lacp mode is disabled\n",
                         u2AggIndex);
            LA_UNLOCK ();
            return LA_FAILURE;
        }
        else
        {
            *pu2PortIndex = u2AggIndex;
            LA_UNLOCK ();
            return LA_SUCCESS;
        }
    }
    LA_UNLOCK ();
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaCreatePort                                         */
/*                                                                           */
/* Description        : This function is invoked from CFA when a physical    */
/*                      port is created                                      */
/*                                                                           */
/* Input(s)           : u2PortIndex -Port on which it is recieved            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaCreatePort (UINT2 u2PortIndex)
{
    tLaIntfMesg        *pMesg = NULL;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "Called LaCreatePort for Port %d\n", u2PortIndex);

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LA is not started\n");
        return LA_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMesg =
         (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaCreatePort: Intf Mesg ALLOC_MEM_BLOCK FAILED\n",
                     u2PortIndex);
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaCreatePort: Intf Mesg ALLOC_MEM_BLOCK FAILED"));
        return LA_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    pMesg->u2MesgType = LA_CREATE_PORT_MSG;
    pMesg->u2PortIndex = u2PortIndex;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "LaCreatePort: Port %d: "
                     "LA_Enqueue Control Message FAILED\n", u2PortIndex);
        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {

            LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaCreatePort: Port %d: "
                         "LA_FREE_MEM_BLOCK FAILED\n", u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    L2_SYNC_TAKE_SEM ();

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMapPort                                            */
/*                                                                           */
/* Description        : This function is invoked from VCM when a physical    */
/*                      port is mappeed to a context                         */
/*                                                                           */
/* Input(s)           : u2PortIndex -Port on which it is recieved            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaMapPort (UINT2 u2PortIndex)
{
    tLaIntfMesg        *pMesg = NULL;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "Called LaMapPort for Port %d\n", u2PortIndex);

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LA is not started\n");
        return LA_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMesg =
         (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaMapPort: Intf Mesg ALLOC_MEM_BLOCK FAILED\n",
                     u2PortIndex);
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaMapPort: Intf Mesg ALLOC_MEM_BLOCK FAILED"));
        return LA_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    pMesg->u2MesgType = LA_MAP_PORT_MSG;
    pMesg->u2PortIndex = u2PortIndex;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "LaMapPort: Port %d: "
                     "LA_Enqueue Control Message FAILED\n", u2PortIndex);
        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {

            LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaMapPort: Port %d: "
                         "LA_FREE_MEM_BLOCK FAILED\n", u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    L2MI_SYNC_TAKE_SEM ();

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeletePort                                         */
/*                                                                           */
/* Description        : This function is invoked from CFA when a physical    */
/*                      port is deleted                                      */
/*                                                                           */
/* Input(s)           : u2PortIndex -Port on which it is recieved            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaDeletePort (UINT2 u2PortIndex)
{
    tLaIntfMesg        *pMesg = NULL;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "Called LaDeletePort for Port %d\n", u2PortIndex);

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LA is not started\n");
        return LA_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMesg =
         (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaDeletePort: Intf Mesg ALLOC_MEM_BLOCK FAILED\n",
                     u2PortIndex);
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaDeletePort: Intf Mesg ALLOC_MEM_BLOCK FAILED"));
        return LA_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    pMesg->u2MesgType = LA_DELETE_PORT_MSG;
    pMesg->u2PortIndex = u2PortIndex;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "LaDeletePort: Port %d: "
                     "LA_Enqueue Control Message FAILED\n", u2PortIndex);

        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {
            LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaDeletePort: Port %d: "
                         "LA_FREE_MEM_BLOCK FAILED\n", u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    /* This semaphore is to Sync with the LA control event sent above.
     * The code should not continue unless the above event is processed */
    LA_TAKE_PROTOCOL_SEMAPHORE (gLaSyncSemId);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaUnmapPort                                          */
/*                                                                           */
/* Description        : This function is invoked from VCM when a physical    */
/*                      port is unmapped from a context.                     */
/*                                                                           */
/* Input(s)           : u2PortIndex -Port on which it is recieved            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaUnmapPort (UINT2 u2PortIndex)
{
    tLaIntfMesg        *pMesg = NULL;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "Called LaUnmapPort for Port %d\n", u2PortIndex);

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LA is not started\n");
        return LA_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMesg =
         (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaUnmapPort: Intf Mesg ALLOC_MEM_BLOCK FAILED\n",
                     u2PortIndex);
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaUnmapPort: Intf Mesg ALLOC_MEM_BLOCK FAILED"));
        return LA_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    pMesg->u2MesgType = LA_UNMAP_PORT_MSG;
    pMesg->u2PortIndex = u2PortIndex;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "LaUnmapPort: Port %d: "
                     "LA_Enqueue Control Message FAILED\n", u2PortIndex);

        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {
            LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaUnmapPort: Port %d: "
                         "LA_FREE_MEM_BLOCK FAILED\n", u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    L2MI_SYNC_TAKE_SEM ();

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaUpdatePortStatus                                   */
/*                                                                           */
/* Description        : This function is invoked from CFA to indicate link   */
/*                      status changes                                       */
/*                                                                           */
/* Input(s)           : u2PortIndex -Port on which it is recieved            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaUpdatePortStatus (UINT2 u2PortIndex, UINT1 u1PortOperStatus)
{
    tLaIntfMesg        *pMesg = NULL;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "Called LaUpdatePortStatus for Port %d\n", u2PortIndex);

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LA is not started\n");
        return LA_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMesg =
         (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaUpdatePortStatus: Intf Mesg ALLOC_MEM_BLOCK FAILED\n",
                     u2PortIndex);
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaUpdatePortStatus: Intf Mesg ALLOC_MEM_BLOCK FAILED"));
        return LA_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    if (u1PortOperStatus == LA_OPER_UP)
    {
        pMesg->u2MesgType = LA_ENABLE_PORT_MSG;
    }
    else if (u1PortOperStatus == LA_OPER_DOWN)
    {
        pMesg->u2MesgType = LA_DISABLE_PORT_MSG;
    }

    pMesg->u2PortIndex = u2PortIndex;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "LaUpdatePortStatus: Port %d: "
                     "LA_Enqueue Control Message FAILED\n", u2PortIndex);

        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {
            LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaUpdatePortStatus: Port %d: "
                         "LA_FREE_MEM_BLOCK FAILED\n", u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetLaEnableStatus.                                 */
/*                                                                           */
/* Description        : This function is called by either the CFA or PNAC    */
/*                      module when they require to know if Link Aggregation */
/*                      module is enabled or not.                            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaEnabled.                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LaEnabled - If Link Aggregation is enabled.          */
/*                      LaDisabled -If Link Aggregation is disabled.         */
/*****************************************************************************/
tLaEnable
LaGetLaEnableStatus (VOID)
{
    if (LA_SYSTEM_CONTROL != LA_START)
    {

        LA_TRC (INIT_SHUT_TRC, "LA System is SHUTDOWN.\n");
        return LA_DISABLED;
    }

    /* return (gLaGlobalInfo.LaEnabled); */
    return (LA_MODULE_STATUS);
}

/*****************************************************************************/
/* Function Name      : LaUpdatePortChannelAdminStatus.                      */
/*                                                                           */
/* Description        : This function is called by CFA when the Port Channel */
/*                      admin status changes.                                */
/*                      in the status of a physical port.                    */
/*                                                                           */
/* Input(s)           : Port Channel Index, Admin Status.                    */
/*                                                                           */
/* Output(s)          : Returns the Operational Status since LA controls     */
/*                      it.                                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaUpdatePortChannelAdminStatus (UINT2 u2IfIndex, UINT1 u1AdminStatus,
                                UINT1 *pu1OperStatus)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            MacAddr = { 0 };
    tCfaIfInfo          IfInfo;
    UINT1               u1PrevAggOperStatus;

    LA_LOCK ();

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));
    LA_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    /* Check if the Port Channel already exists */
    LaGetAggEntry (u2IfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        LA_UNLOCK ();

        /* Port Channel doesnot exist */
        *pu1OperStatus = CFA_IF_DOWN;
        return LA_FAILURE;
    }
    if (pAggEntry->u1AggAdminStatus != u1AdminStatus)
    {
        if (u1AdminStatus == CFA_IF_UP)
        {
            /* Fill the MTU value in to the aggregator entry and also to
             * the structures of individual member ports of the aggregation.
             */
            if (LaCfaGetIfInfo (u2IfIndex, &IfInfo) != CFA_SUCCESS)
            {
                LA_UNLOCK ();
                return LA_FAILURE;
            }
            else
            {
                LaFillAggMtu (u2IfIndex, IfInfo.u4IfMtu);
                LaFillAggPauseAdminMode (u2IfIndex, IfInfo.u1PauseAdminNode);
            }
        }
        else
        {
#ifdef ICCH_WANTED
            if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
            {
                VlanDeleteFdbEntries (u2IfIndex, VLAN_OPTIMIZE);
            }
#endif
            if (pAggEntry->HwAggStatus == LA_AGG_ADD_FAILED_IN_HW)
            {
                if (pAggEntry->LaLacpMode == LA_MODE_LACP)
                {
                    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
                    while (pPortEntry != NULL)
                    {
                        pPortEntry->LaLacActorInfo.u2IfKey =
                            pPortEntry->LaLacActorAdminInfo.u2IfKey;

                        pPortEntry->NttFlag = LA_TRUE;
                        LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

                        LaGetNextAggPortEntry (pAggEntry, pPortEntry,
                                               &pPortEntry);
                    }
                }
                pAggEntry->HwAggStatus = LA_AGG_NOT_IN_HW;
            }
#ifdef ICCH_WANTED
            if (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
            {
                LaIcchProcessMclagOperDown (pAggEntry->u2AggIndex);
            }
#endif
        }

        if ((pAggEntry->u1AggAdminStatus == LA_ADMIN_UP) &&
            (u1AdminStatus == LA_ADMIN_DOWN))
        {
            pAggEntry->i4DownReason = LA_IF_ADMIN_DOWN;
            pAggEntry->u4OperDownCount++;
            LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "Line Protocol on Port Channel %d "
                             "changed to Admin down\r\n",
                             pAggEntry->AggConfigEntry.u2ActorAdminKey);
            LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
            LaLinkStatusNotify (pAggEntry->u2AggIndex, LA_OPER_DOWN,
                                pAggEntry->u1AggAdminStatus,
                                LA_TRAP_PORTCHANNEL_OPER_DOWN);

        }
        /* Change the interface status from down to up if needed */
        pAggEntry->u1AggAdminStatus = u1AdminStatus;
        if ((u1AdminStatus == LA_ADMIN_UP)
            && (pAggEntry->u1AggOperStatus == LA_OPER_DOWN))
        {
            pAggEntry->i4DownReason = LA_IF_OPER_DOWN;
        }
        u1PrevAggOperStatus = pAggEntry->u1AggOperStatus;

        /* This global variable is used to control LACP PDU negotiation once the 
         * Port Channel Interface is made admin down */
        /* If gu4LagAdminDownNoNeg is LA_TRUE, LACP PDU negotiation will be
         * stopped. If it is LA_FALSE, LACP PDU negotiation can happen through 
         * the member ports though port channel interface is down*/
        if (gu4LagAdminDownNoNeg == LA_TRUE)
        {
            LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
            LaCalculateAggOperStatus (pAggEntry, u1AdminStatus);
            while (pPortEntry != NULL)
            {
                if ((u1AdminStatus == LA_ADMIN_UP) &&
                    (pPortEntry->u1PortOperStatus == LA_OPER_UP))
                {
                    LaHandleEnablePort (pPortEntry);
                }
                if ((u1AdminStatus == LA_ADMIN_DOWN) &&
                    (pPortEntry->u1PortOperStatus == LA_OPER_UP))

                {
                    LaHandleDisablePort (pPortEntry);
                    if (gu4PartnerConfig == LA_ENABLED)
                    {
                        /* When Independent mode is enabled, the ports operate as individual
                         *            ports and these ports will become visible to higher layer protocols,
                         *                       when no partner in the switch */
                        pPortEntry->LaLacPartnerInfo.LaLacPortState.
                            LaSynchronization = LA_FALSE;

                        pPortEntry->LaLacActorInfo.LaLacPortState.LaDefaulted =
                            LA_TRUE;
                        pPortEntry->LaRxmState = LA_RXM_STATE_DEFAULTED;
                        pPortEntry->InDefaulted = LA_TRUE;

                        pAggEntry->UsingPartnerAdminInfo = LA_FALSE;

                        LaL2IwfRemovePortFromPortChannel (pPortEntry->
                                                          u2PortIndex,
                                                          pPortEntry->
                                                          pAggEntry->
                                                          u2AggIndex);

                        LaHlCreatePhysicalPort (pPortEntry);
                    }
                }
                LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
            }
        }

        LaCalculateAggOperStatus (pAggEntry, u1AdminStatus);
        LaCalculateAggMemberOperStatus (pAggEntry, u1AdminStatus);

        if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
            (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED) &&
            (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE))
        {
            /* When port-channel state is modified the H/L indication
             * is given based on the port's Distrbuting state.
             * Incase of Active-Active DLAG shut and no shut of 
             * of port-channel in Slaves should not trigere H/L indication
             * unless it is approved by Master( Sync,Distributing is TRUE)
             * Otherwise H/L indication should not be given.
             * For thus purpose check the no of ports that are in 
             * (Sync & Distributing state). */

            LaActiveDLAGCalculateAggOperStatus (pAggEntry, u1AdminStatus);
        }
#ifdef ICCH_WANTED
        else if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
                 (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_SLAVE))
        {
            LaActiveMCLAGCalculateAggOperStatus (pAggEntry, u1AdminStatus);
        }
#endif

        if (pAggEntry->u1AggOperStatus != u1PrevAggOperStatus)
        {
            LaUpdateLlPortChannelStatus (pAggEntry);
            LaL2IwfLaHLPortOperIndication (u2IfIndex,
                                           pAggEntry->u1AggOperStatus);
        }

        if (((pAggEntry) && (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)) ||
            (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
        {
            if (u1AdminStatus == CFA_IF_UP)
            {
                LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_INTF_UP;
            }
            else
            {
                LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN;
            }

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 MacAddr,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);

            }
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
    }

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
        (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER))

    {
        /* Call consolidation if this port-channel is Master */
        LaActiveDLAGMasterUpdateConsolidatedList (pAggEntry);

    }
#ifdef ICCH_WANTED
    else if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
             (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER))
    {
        /* Call consolidation if this port-channel is Master */
        LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
    }
#endif
    if (pAggEntry->u1AggOperStatus == LA_OPER_UP)
    {
        *pu1OperStatus = CFA_IF_UP;
    }
    else
    {
        *pu1OperStatus = CFA_IF_DOWN;
    }

    LA_UNLOCK ();

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIsLaStarted.                                       */
/*                                                                           */
/* Description        : This function is called by either the CFA/ PNAC or   */
/*                      Bridge module to check if the LA Module is started or*/
/*                      not.                                                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaSystemControl.                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - if the module is started.                  */
/*                      LA_FALSE - if the module is shutdown.                */
/*****************************************************************************/
tLaBoolean
LaIsLaStarted (VOID)
{
    if (LA_SYSTEM_CONTROL == LA_START)
    {
        LA_TRC (INIT_SHUT_TRC, "LA Module is Started.\n");
        return LA_TRUE;
    }

    LA_TRC (INIT_SHUT_TRC, "LA Module is Shutdown.\n");

    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaUpdateRestoreMtu                                   */
/* Description        : When the Mtu of a physical port is changed ,this     */
/*                      routine is called to update the restore mtu for the  */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           : u2IfIndex - Index of the physical port               */
/*                      u4IfMtu  -  The MTU of the port                      */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
LaUpdateRestoreMtu (UINT2 u2IfIndex, UINT4 u4IfMtu)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LA_LOCK ();

    LaGetPortEntry (u2IfIndex, &pPortEntry);

    if (pPortEntry != NULL)
    {
        pPortEntry->u4RestoreMtu = u4IfMtu;
    }
    LA_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : LaUpdatePauseAdminMode                               */
/* Description        : When the pause admin mode of a physical port is      */
/*                      changed ,this routine is called to update the        */
/*                      previously configured pasue admin mode for the port. */
/*                                                                           */
/* Input(s)           : u2IfIndex           - Index of the physical port     */
/*                      i4IfPauseAdminMode  - Pause Admin Mode of the port   */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
LaUpdatePauseAdminMode (UINT2 u2IfIndex, UINT4 i4IfPauseAdminMode)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LA_LOCK ();

    LaGetPortEntry (u2IfIndex, &pPortEntry);

    if (pPortEntry != NULL)
    {
        pPortEntry->i4RestorePauseAdminMode = i4IfPauseAdminMode;
    }
    LA_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : LaUpdateAggPauseAdminMode                            */
/* Description        : When the pause admin mode of a port-channel is       */
/*                      changed ,this routine is called to update the        */
/*                      previously configured pasue admin mode for the       */
/*                      port-channel.                                        */
/*                                                                           */
/* Input(s)           : u2IfIndex           - Index of the physical port     */
/*                      i4AggPauseAdminMode - Pause Admin Mode of the port   */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
LaUpdateAggPauseAdminMode (UINT2 u2IfIndex, UINT4 i4AggPauseAdminMode)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_LOCK ();

    LaGetAggEntry (u2IfIndex, &pAggEntry);

    if (pAggEntry != NULL)
    {
        pAggEntry->i4PauseAdminMode = i4AggPauseAdminMode;
    }
    LA_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : LaUpdateActorPortChannelMac                          */
/* Description        : When the MacAddress of a physical port is changed,   */
/*                      this routine is called to update the MacAddress      */
/*                      for the port Channel.                                */
/*                                                                           */
/* Input(s)           : u2IfIndex - Index of the physical port               */
/*                      SetValFsLaPortChannelAdminMacAddress  -              */
/*                      The MTU of the port                                 */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - If the MacAddress updated for PortChannel  */
/*                      LA_FALSE - Failed to update the Macaddress           */
/*****************************************************************************/
INT4
LaUpdateActorPortChannelMac (UINT2 u2IfIndex,
                             tMacAddr SetValFsLaPortChannelAdminMacAddress)
{
    tLaLacAggEntry     *pAggEntry;

    LA_LOCK ();
    if (LaGetAggEntry (u2IfIndex, &pAggEntry) == LA_FAILURE)
    {
        LA_UNLOCK ();
        return LA_FALSE;
    }
    LA_MEMCPY (pAggEntry->AggConfigEntry.AggMacAddr,
               SetValFsLaPortChannelAdminMacAddress, LA_MAC_ADDRESS_SIZE);
    LA_UNLOCK ();
    return LA_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaGetLacpPortState                                 */
/*                                                                           */
/*     DESCRIPTION      : This is an utility function used                   */
/*                        to get the PortOperState for the port.             */
/*                                                                           */
/*     INPUT            : u2IfIndex -  specifies the port Number.            */
/*                        pLaLacPortState - specifies the Port State         */
/*                        u4Value   -  specifies the value as ACTOR/PARTNER  */
/*                                                                           */
/*     OUTPUT           : pLaLacPortState - Pointer which stores the current */
/*                                          Port OperState                   */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
LaGetLacpPortState (UINT2 u2IfIndex, tLaLacPortState ** pLaLacPortState,
                    UINT4 u4Value)
{
    LA_LOCK ();
    LaUtilGetPortState (u2IfIndex, pLaLacPortState, u4Value);
    LA_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaIsPortInPortChannel                              */
/*                                                                           */
/*     DESCRIPTION     : This routine checks whether the port is a member of  */
/*                      any Port Channel and if so returns SUCCESS.          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex -  specifies the port Number.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : LA_SUCCESS/LA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
LaIsPortInPortChannel (UINT4 u4IfIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;

    LA_LOCK ();
    if (LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry) != LA_SUCCESS)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }

    pLaLacAggEntry = pPortEntry->pAggEntry;

    if (pLaLacAggEntry == NULL)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }

    if ((*pLaLacAggEntry).u2AggIndex == (UINT2) u4IfIndex)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }
    LA_UNLOCK ();
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetNextAggPort                                     */
/*                                                                           */
/* Description        : This function is used to get the next port which is  */
/*                      configured in the aggregator port list.              */
/*                                                                           */
/* Input(s)           : u2AggIndex - PortChannel IfIndex                     */
/*                      u2Port  - PortIndex                                  */
/*                                                                           */
/* Output(s)          : pu2NextPort -Pointer to the next port.               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaGetNextAggPort (UINT2 u2AggIndex, UINT2 u2Port, UINT2 *pu2NextPort)
{
    INT4                i4OutCome = 0;

    LA_LOCK ();
    i4OutCome = LaUtilGetNextAggPort (u2AggIndex, u2Port, pu2NextPort);
    if (i4OutCome == LA_FAILURE)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }
    LA_UNLOCK ();
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaSetDefaultPropForAggregator                        */
/*                                                                           */
/* Description        : This function is used to set default properties for  */
/*                      an aggregator.                                       */
/*                                                                           */
/* Input(s)           : u2AggIndex - PortChannel IfIndex                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaSetDefaultPropForAggregator (UINT2 u2AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_LOCK ();

    if (LaGetAggEntry (u2AggIndex, &pAggEntry) == LA_FAILURE)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }

    pAggEntry->u1MaxPortsToAttach = LA_MAX_PORTS_PER_AGG;

    LA_UNLOCK ();
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleIncomingFrame                                */
/*                                                                           */
/* Description        : This function  receives the incoming frame  from the */
/*                      CFA Module.  This function classified received frame */
/*                      Frametype.                          */
/*                      If the received  packet is  an LACP control  packet, */
/*                      it is enqueued to  LA Control taskand API exits with */
/*                      a  return value  LA_CONTROL.  The  invoking  process */
/*                      should return without error.                         */
/*                                                                           */
/*                      If it is  a data pkt  meant for  higher layers, this */
/*                      API  consults  LA Database and returns an aggregated */
/*                      port index  for the physical portindex passed in the */
/*                      parameter   pAggIndex.   It  returns  with  a  value */
/*                      LA_SUCCESS  in such  case.  The CFA should then pass */
/*                      the  pkt  to  the  higher  layers  along  with   the */
/*                      aggregated port index.                               */
/*                                         */
/*                      If the value returned is LA_FAILURE then the port is */
/*                      not in collecting  state. The CFA should discard the */
/*                      frame. LA_FAILURE  is returned for error  conditions */
/*                      also.                                                */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the received buffer                */
/*                      u2PortIndex - Interface Index of port/interface      */
/*                                                                           */
/* Output(s)          : *pu1AggPortIndex - Interface index of the portchannel*/
/*                       or port.                                            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DATA    & (*pu2AggPortIndex) (for Data frames)    */
/*                      LA_DROP    - Drop the frame                          */
/*                      LA_CONTROL - For LACP frames                         */
/*****************************************************************************/

INT4
LaHandleIncomingFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                       UINT2 *pu2AggPortIndex)
{
    tMacAddr            DestAddr;

    if ((LaIsLaCtrlFrame (pBuf, DestAddr, *pu2AggPortIndex)) == LA_TRUE)
    {
        return (LaHandleReceivedFrame (pBuf, u2PortIndex, pu2AggPortIndex,
                                       L2_LA_CTRL_FRAME));
    }
    else
    {
        return (LaHandleReceivedFrame (pBuf, u2PortIndex, pu2AggPortIndex,
                                       BRG_DATA_FRAME));
    }
}

/*****************************************************************************/
/* Function Name      : LaHandleReceivedFrame                                */
/*                                                                           */
/* Description        : This function  receives the incoming frame  from the */
/*                      CFA Module. This expect the frameType from the caller*/
/*                      function. Then,It verifies the received frame type   */
/*                      If the received packet is  an                        */
/*                      LACP  packet it is  enqueued to  LA Control task and */
/*                      API exits with a return value LA_CONTROL.The invoking*/
/*                      process should return without error.                 */
/*                                                                           */
/*                      If it is a data pkt  meant for  higher layers, this  */
/*                      API consults  LA Database and returns an aggregated  */
/*                      port index for the physical portindex passed in the  */
/*                      parameter  pAggIndex. It  returns  with  a  value    */
/*                      LA_SUCCESS in such case. The CFA should then pass    */
/*                      the  pkt  to  the  higher layers  along with  the    */
/*                      aggregated port index.                               */
/*                                         */
/*                      If the value returned is LA_FAILURE then the port is */
/*                      not in collecting  state. The CFA should discard the */
/*                      frame. LA_FAILURE  is returned for error  conditions */
/*                      also.                                                */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the received buffer                */
/*                      u2PortIndex - Interface Index of port/interface      */
/*                                                                           */
/* Output(s)          : *pu1AggPortIndex - Interface index of the portchannel*/
/*                       or port.                                            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DATA    & (*pu2AggPortIndex) (for Data frames)    */
/*                      LA_DROP    - Drop the frame                          */
/*                      LA_CONTROL - For LACP frames                         */
/*****************************************************************************/

INT4
LaHandleReceivedFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                       UINT2 *pu2AggPortIndex, UINT1 u1FrameType)
{
    UINT1               u1IfType = 0;
    UINT1               u1ProtType = 0;
    if (LaGetLaEnableStatus () != LA_ENABLED)
    {
        *pu2AggPortIndex = u2PortIndex;
        return LA_DATA;
    }

    if (pBuf == NULL)
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     ("LaHandleRxDataFrame : Port %d: "
                      "CRU Buffer received - Null Pointer\n"), u2PortIndex);
        return LA_IGNORE;
    }

    /* Checking if an entry exists corresponding to this port index */

    CfaGetIfaceType (u2PortIndex, &u1IfType);
    if (u1IfType != CFA_PSEUDO_WIRE)
    {
        if ((LA_DLAG_SYSTEM_STATUS == LA_ENABLED)
            && (u1FrameType == L2_LA_CTRL_FRAME) &&
            (LA_USE_DIST_PORT_OR_NOT
             (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) ==
             LA_FALSE))
        {
            if (CRU_BUF_Copy_FromBufChain
                (pBuf, (UINT1 *) &u1ProtType, LA_TLV_TYPE_OFFSET,
                 1) == CRU_FAILURE)
            {
                return LA_IGNORE;
            }

            /* Checking if an entry exists corresponding to this port index */
            if ((u1ProtType != LA_DLAG_DSU_INFO) &&
                (u2PortIndex < LA_MIN_PORTS || u2PortIndex > LA_MAX_PORTS))
            {
                LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                             ALL_FAILURE_TRC,
                             "LaHandleRxDataFrame: Port %u: "
                             "Invalid Port Index Discard the frame...\n",
                             u2PortIndex);
                return LA_DROP;
            }

        }
    }

    if (u1FrameType == L2_LA_CTRL_FRAME)
    {
        return (LaHandleRxCtrlFrame
                (pBuf, u2PortIndex, pu2AggPortIndex, u1FrameType));
    }
    else
    {
        return (LaHandleRxDataFrame
                (pBuf, u2PortIndex, pu2AggPortIndex, u1FrameType));
    }
}

/*****************************************************************************/
/* Function Name      : LaCallBackRegister                                   */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT4
LaCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = LA_SUCCESS;

    LA_LOCK ();

    switch (u4Event)
    {
            /* Currently all events having same prototype */
        case LA_PKT_RX_EVENT:
            LA_CALLBACK[u4Event].pLaPktRxCallBack = pFsCbInfo->pLaPktRxCallBack;
            break;

        case LA_PKT_TX_EVENT:
            LA_CALLBACK[u4Event].pLaPktTxCallBack = pFsCbInfo->pLaPktTxCallBack;
            break;

        case LA_GET_DISTRIB_PORT_EVENT:
            LA_CALLBACK[u4Event].pLaGetDistribPortCallBack
                = pFsCbInfo->pLaGetDistribPortCallBack;
            break;

        case LA_GET_ACTIVE_PORT_EVENT:
            LA_CALLBACK[u4Event].pLaGetActivePortCallBack
                = pFsCbInfo->pLaGetActivePortCallBack;
            break;

        default:
            i4RetVal = LA_FAILURE;
            break;
    }

    LA_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : LaIsLaCtrlFrame                                      */
/*                                                                           */
/* Description        : This function checks whether the given frame is      */
/*                      an LACP control frame.                               */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pBuf     - Incoming frame                            */
/*                      DestAddr - Destination address in the frame          */
/*                      u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE                                              */
/*                      LA_FALSE                                             */
/*****************************************************************************/

INT4
LaIsLaCtrlFrame (tCRU_BUF_CHAIN_DESC * pBuf, tMacAddr DestAddr, UINT2 u4IfIndex)
{
    if (L2IwfIsLaCtrlFrame (pBuf, DestAddr, (UINT4) u4IfIndex) == L2IWF_TRUE)
    {
        return LA_TRUE;
    }

    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaInit                                               */
/*                                                                           */
/* Description        : This is an API to initialise the LA module           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaInit (VOID)
{
    LA_LOCK ();
    if (LaHandleInit () == LA_FAILURE)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }
    LA_UNLOCK ();
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaShutDown                                           */
/*                                                                           */
/* Description        : This is an API to shutdown the LA module             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaShutDown (VOID)
{
    LA_LOCK ();
    if (LaHandleShutDown () == LA_FAILURE)
    {
        LA_UNLOCK ();
        return LA_FAILURE;
    }
    LA_UNLOCK ();
    return LA_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaAsyncNpUpdateStatus                                */
/*                                                                           */
/* Description        : This function will be invoked when the asynchronous  */
/*                      hardware updation is completed to indicate the status*/
/*                      to LA module. It posts a message to the LA task to   */
/*                      handle the hardware operation status.                */
/*                                                                           */
/* Input(s)           : u4NpCallId - Unique number that identifies the source*/
/*                                   NPAPI call.                             */
/*                      NpParams   - Union consiting of a structure for each */
/*                                   asynchronous NPAPI. It contains the     */
/*                                   parameters that were passed to the NPAPI*/
/*                                   identified by give NpCallId.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
VOID
LaAsyncNpUpdateStatus (UINT4 u4NpCallId, unAsyncNpapi * NpParams)
{
    tLaIntfMesg        *pMesg = NULL;

    if (LA_SYSTEM_CONTROL == LA_SHUTDOWN)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LA is not started\n");
        return;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMesg =
         (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "LaAsyncNpUpdateStatus:Intf Msg ALLOC_MEM_BLOCK FAILED\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaAsyncNpUpdateStatus:Intf Msg ALLOC_MEM_BLOCK FAILED"));
        return;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    pMesg->u2MesgType = LA_NP_CALLBACK;
    pMesg->uLaIntfMsg.LaNpCbMsg.u4NpCallId = u4NpCallId;
    MEMCPY (&(pMesg->uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams),
            NpParams, sizeof (unAsyncNpapi));

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LaAsyncNpUpdateStatus: LA_Enqueue Message FAILED\n");
        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {
            LA_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "LaAsyncNpUpdateStatus:LA_FREE_MEM_BLOCK FAILED\n");
        }
        return;
    }
    if (LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaAsyncNpUpdateStatus:"
                "SendEvent FAILED\n");
        /* Cannot undo enqueue.. */
    }
}

#endif /*NPAPI_WANTED */

/*****************************************************************************/
/* Function Name      : LaNotifyL2Vpn                                        */
/*                                                                           */
/* Description        : This function notifies the MPLS L2VPN module about   */
/*                      operstatus of the AC ports on which DLAG is enabled  */
/*                                                                           */
/* Input(s)           : pAggEntry  - pointer to tAggEntry Entry              */
/*                      u1DlagNodeStatus - Status to be communicated         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/

PUBLIC VOID
LaNotifyL2Vpn (tLaLacAggEntry * pAggEntry, UINT1 u1DlagNodeStatus)
{
#ifdef MPLS_WANTED
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tDlagAcStatusMsg    DlagAcStatusMsg;
    UINT1               u1DlagPortCount = 0;

    MEMSET (&DlagAcStatusMsg, 0, sizeof (tDlagAcStatusMsg));

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tDlagAcStatusMsg), 0)) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "LaNotifyL2Vpn :pBuf Memory allocation failed\n");
        return;
    }

    DlagAcStatusMsg.u1DlagPswStatus = u1DlagNodeStatus;

    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while (pTmpPortEntry != NULL)
    {
        if (u1DlagPortCount < LA_MAX_DLAG_PORTS)
        {
            DlagAcStatusMsg.au2PortIndex[u1DlagPortCount++] =
                pTmpPortEntry->u2PortIndex;
        }
        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }

    if (u1DlagPortCount == 0)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaNotifyL2Vpn:"
                "SendEvent FAILED: As no ports are associated with the "
                "Aggregator entry\n");
    }
    else
    {
        if (u1DlagPortCount > LA_MAX_DLAG_PORTS)
        {
            CRU_BUF_Release_MsgBufChain ((tOsixMsg *) (VOID *) pBuf, FALSE);
            return;
        }

        DlagAcStatusMsg.u1NoOfPorts = u1DlagPortCount;
        CRU_BUF_Copy_OverBufChain (pBuf,
                                   (UINT1 *) (VOID *) (&DlagAcStatusMsg),
                                   0, sizeof (tDlagAcStatusMsg));

        L2VpnNotifyAcUpDown ((tDlagAcStatusMsg *) pBuf);
    }
    CRU_BUF_Release_MsgBufChain ((tOsixMsg *) (VOID *) pBuf, FALSE);
#else
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (u1DlagNodeStatus);
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LaSetMacAddressToSlotId                          */
/*                                                                          */
/*    Description        : This function sets the MacAddress mapping        */
/*                         for the given SlotId                                */
/*                                                                          */
/*    Input(s)           : SlotId,MacAddress                                */
/*                                                                          */
/*    Output(s)          : NONE                                             */
/*                                                                          */
/*    Returns            : LA_SUCCESS or LA_FAILURE                         */
/****************************************************************************/
INT4
LaSetMacAddressToSlotId (tMacAddr DestMacAddr, UINT4 u4SlotId)
{
#ifdef MBSM_WANTED
    return (MbsmSetMacAddressToSlotId (DestMacAddr, u4SlotId));
#else
    UNUSED_PARAM (u4SlotId);
    UNUSED_PARAM (DestMacAddr);
    return LA_SUCCESS;
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LaGetMacAddressFromSlotId                        */
/*                                                                          */
/*    Description        : This function returns the MacAddress of the      */
/*                          given SlotId                                      */
/*                                                                          */
/*    Input(s)           : SlotId                                           */
/*                                                                          */
/*    Output(s)          : MacAddress                                       */
/*                                                                          */
/*    Returns            : LA_SUCCESS or LA_FAILURE                         */
/****************************************************************************/
INT4
LaGetMacAddressFromSlotId (UINT4 u4SlotId, tMacAddr * pDestMacAddr)
{
#ifdef MBSM_WANTED
    return (MbsmGetMacAddressFromSlotId (u4SlotId, pDestMacAddr));
#else
    UNUSED_PARAM (u4SlotId);
    UNUSED_PARAM (pDestMacAddr);
    return LA_SUCCESS;
#endif

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LaGetSlotIdFromMacAddress                        */
/*                                                                          */
/*    Description        : This function returns the Slot Identifier of the */
/*                         given SlotId                                     */
/*                                                                          */
/*    Input(s)           : SlotId                                           */
/*                                                                          */
/*    Output(s)          : MacAddress                                       */
/*                                                                          */
/*    Returns            : LA_SUCCESS or LA_FAILURE                         */
/****************************************************************************/
INT4
LaGetSlotIdFromMacAddress (tMacAddr MacAddr, UINT4 *pu4SlotId)
{
#ifdef MBSM_WANTED
    return (MbsmGetSlotIdFromMacAddress (MacAddr, pu4SlotId));
#else
    UNUSED_PARAM (pu4SlotId);
    UNUSED_PARAM (MacAddr);
    return LA_SUCCESS;
#endif

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LaGetDissMasterSlotId                          */
/*                                                                          */
/*    Description        : This function returns the Master SlotId          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : pu4SlotId - SlotId of Master Node                */
/*                                                                          */
/*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     */
/****************************************************************************/
VOID
LaGetDissMasterSlotId (UINT4 *pu4SlotId)
{
#ifdef MBSM_WANTED
    MbsmGetDissMasterSlotId (pu4SlotId);
#else
    UNUSED_PARAM (pu4SlotId);
#endif

}

/*****************************************************************************/
/* Function Name      : LaGetShowCmdOutputAndCalcChkSum                      */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
LaGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (LaCliGetShowCmdOutputToFile ((UINT1 *) LA_AUDIT_FILE_ACTIVE) !=
            LA_SUCCESS)
        {
            LA_TRC (ALL_FAILURE_TRC, "GetShRunFile Failed\n");
            return LA_FAILURE;
        }
        if (LaCliCalcSwAudCheckSum
            ((UINT1 *) LA_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != LA_SUCCESS)
        {
            LA_TRC (ALL_FAILURE_TRC, "CalcSwAudChkSum Failed\n");
            return LA_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (LaCliGetShowCmdOutputToFile ((UINT1 *) LA_AUDIT_FILE_STDBY) !=
            LA_SUCCESS)
        {
            LA_TRC (ALL_FAILURE_TRC, "GetShRunFile Failed\n");
            return LA_FAILURE;
        }
        if (LaCliCalcSwAudCheckSum
            ((UINT1 *) LA_AUDIT_FILE_STDBY, pu2SwAudChkSum) != LA_SUCCESS)
        {
            LA_TRC (ALL_FAILURE_TRC, "CalcSwAudChkSum Failed\n");
            return LA_FAILURE;
        }
    }
    else
    {
        LA_TRC (ALL_FAILURE_TRC, "Node is neither Active nor Standby\n");
        return LA_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaApiMCLAGMasterInit                                 */
/*                                                                           */
/* Description        : This funcion inits the MASTER configuration in MCLAG */
/*                      It is being invoked from HB module during master-    */
/*                      slave election                                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaApiMCLAGMasterInit ()
{
#ifdef ICCH_WANTED
    if (LaActiveMCLAGMasterUpIndication () == LA_FAILURE)
    {
        LA_TRC (ALL_FAILURE_TRC, "LaActiveMCLAGMasterUpIndication failed\n");
    }
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : LaApiIsMclagInterface                                */
/*                                                                           */
/* Description        : This function is used to check if the given input    */
/*                      interface corresponds to MC-LAG.                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : *pu1IsMclagEnabled - True if u4IfIndex is MC-LAG     */
/*                                           interface else false            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
VOID
LaApiIsMclagInterface (UINT4 u4IfIndex, UINT1 *pu1IsMclagEnabled)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    *pu1IsMclagEnabled = OSIX_FALSE;

    /* To check if the given interface index corresponds to MC-LAG
     * interface.
     */
    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);
    if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
    {
        *pu1IsMclagEnabled = OSIX_TRUE;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : LaApiDisableStpOnMclagInt                            */
/*                                                                           */
/* Description        : This function is used to disable spanning tree on    */
/*                      MC-LAG interfaces.                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
LaApiDisableStpOnMclagInt (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);

    do
    {
        if (pAggEntry == NULL)
        {
            break;
        }

        if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
        {
            /*Disable STP on MC-LAG interface */
            LaAstDisableStpOnPort (pAggEntry->u2AggIndex);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    while (1);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetMCLAGSystemStatus                               */
/*                                                                           */
/* Description        : This function is used to get the MCLAG system status */
/*                      whether MCLAG is enabled/disbaled in the system      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1MCLAGSystemStatus                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DISABLED - If MCLAG is disabled in the system     */
/*                      LA_ENABLED  - If MCLAG is enabled in the system      */
/*                                                                           */
/*****************************************************************************/
UINT1
LaGetMCLAGSystemStatus (VOID)
{
    return LA_MCLAG_SYSTEM_STATUS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaApiIsMemberofICCL                                */
/*                                                                           */
/*     DESCRIPTION      : This routine checks whether the port is a member   */
/*                        of ICCL interface.                                 */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex -  Interface index of port.              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
LaApiIsMemberofICCL (UINT4 u4IfIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    UINT4               u4IcclIfIndex = 0;

    LA_LOCK ();
    if (LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry) != LA_SUCCESS)
    {
        LA_UNLOCK ();
        return OSIX_FAILURE;
    }

    pLaLacAggEntry = pPortEntry->pAggEntry;

    if (pLaLacAggEntry == NULL)
    {
        LA_UNLOCK ();
        return OSIX_FAILURE;
    }

    LaIcchGetIcclIfIndex (&u4IcclIfIndex);

    if (pLaLacAggEntry->u2AggIndex != (UINT2) u4IcclIfIndex)
    {
        LA_UNLOCK ();
        return OSIX_FAILURE;
    }

    LA_UNLOCK ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaApiIsMemberofMcLag                               */
/*                                                                           */
/*     DESCRIPTION      : This routine checks whether the port is a member   */
/*                        of MC-LAG enabled port-channel.                    */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4IfIndex -  Interface index of port.              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
LaApiIsMemberofMcLag (UINT4 u4IfIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    LA_LOCK ();
    if (LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry) != LA_SUCCESS)
    {
        LA_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) != LA_FALSE)
    {
        LA_UNLOCK ();
        return OSIX_SUCCESS;
    }

    LA_UNLOCK ();
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaUpdateActorPortChannelAdminKey                     */
/*                                                                           */
/* Description        : This function is used to get the admin key from      */
/*                      the given port-channel index.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - If index of the port-channel             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu2AdminKey - pointer to fill the admin key          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/

INT4
LaUpdateActorPortChannelAdminKey (UINT4 u4IfIndex, UINT2 *pu2AdminKey)
{
    tLaLacAggEntry     *pAggEntry;

    LA_LOCK ();
    if (LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry) == LA_FAILURE)
    {
        LA_UNLOCK ();
        return LA_FALSE;
    }
    *pu2AdminKey = pAggEntry->AggConfigEntry.u2ActorAdminKey;
    LA_UNLOCK ();
    return LA_TRUE;

}

/*****************************************************************************/
/* Function Name      : LaGetAggIndexFromAdminKey                            */
/*                                                                           */
/* Description        : This function is used to get the port-channel index  */
/*                      from the given admin key.                            */
/*                                                                           */
/* Input(s)           : u2AdminKey - admin key of the port-channel           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu4IfIndex - pointer to fill the port-channel index  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/

INT4
LaGetAggIndexFromAdminKey (UINT2 u2AdminKey, UINT4 *pu4IfIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    LaGetAggEntryByKey (u2AdminKey, &pAggEntry);

    LA_LOCK ();
    if (pAggEntry == NULL)
    {
        LA_UNLOCK ();
        return LA_FALSE;
    }

    *pu4IfIndex = (UINT4) pAggEntry->u2AggIndex;
    LA_UNLOCK ();
    return LA_TRUE;
}

/*****************************************************************************/
/* Function Name      : LaApiDisableLldpOnIcclPorts                          */
/*                                                                           */
/* Description        : This function is used to disable LLDP on ICCL ports  */
/*                                                                           */
/* Input(s)           : u4AggIndex - ICCL port-channel index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/

UINT4
LaApiDisableLldpOnIcclPorts (UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;

    LA_LOCK ();

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

    while (pPortEntry != NULL)
    {
        if (LA_TRUE != LaLLdpUpdateLLdpStatusOnIccl
            (pPortEntry->LaLacActorInfo.u2IfIndex, LA_AGG_CAPABLE))
        {
            LA_UNLOCK ();
            return LA_FALSE;
        }
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }

    LA_UNLOCK ();
    return LA_TRUE;
}

/*****************************************************************************/
/* Function Name      : LaApiGetOperStatusonAggIndex                         */
/*                                                                           */
/* Description        : This function is used to get OperStatus of AggIndex  */
/*                                                                           */
/* Input(s)           : u4AggIndex - AggIndex                                */
/*                      pu1Status - OperStatus                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/
UINT4
LaApiGetOperStatusonAggIndex (UINT4 u4AggIndex, UINT1 *pu1Status)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_LOCK ();
    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    if (NULL == pAggEntry)
    {
        LA_UNLOCK ();
        return LA_FALSE;
    }

    *pu1Status = pAggEntry->u1AggOperStatus;

    LA_UNLOCK ();
    return LA_TRUE;
}

/*****************************************************************************/
/* Function Name      : LaIcchInitProperties                                 */
/*                                                                           */
/* Description        : This function is used to initialize ICCL properties  */
/*                      Sets the ICCL LACP mode as Manual mode               */
/*                                                                           */
/* Input(s)           : u4IcclIfIndex - ICCL interface index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaIcchInitProperties (UINT4 u4IcclIfIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    LA_LOCK ();
    LaGetAggEntry ((UINT2) u4IcclIfIndex, &pAggEntry);
    if (pAggEntry != NULL)
    {
        pAggEntry->LaLacpMode = LA_MODE_MANUAL;
    }
    LA_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : LaApiIsRemoteMclagUp                                 */
/*                                                                           */
/* Description        : This function is used to check if the remote mclag   */
/*                      interface is UP or not.                              */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : *pu1IsMclagEnabled - True if remote MCLAG is up      */
/*                                           else false                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaApiIsRemoteMclagUp (UINT4 u4IfIndex, UINT1 *pu1IsRemoteMclagUp)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;

    *pu1IsRemoteMclagUp = LA_FALSE;

    /* To check if the given interface index corresponds to MC-LAG
     * interface.
     */
    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);

    if (pAggEntry->u1MCLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
    {
        pRemoteAggEntry = (tLaDLAGRemoteAggEntry *) RBTreeGetFirst
            (pAggEntry->LaDLAGRemoteAggInfoTree);

        if (pRemoteAggEntry == NULL)
        {
            return;
        }

        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);

        while (pRemotePortEntry != NULL)
        {
            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                *pu1IsRemoteMclagUp = LA_TRUE;
                break;
            }
            LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                          &pRemotePortEntry);
        }
    }
    else if (pAggEntry->u1MCLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)
    {
        LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                      &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            if ((pConsPortEntry->u1NodeState == LA_ICCH_MASTER) &&
                (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD))
            {
                *pu1IsRemoteMclagUp = LA_TRUE;
                break;
            }
            LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                          &pConsPortEntry, pAggEntry);
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : LaCheckMclagSystemId                                 */
/*                                                                           */
/* Description        : This function is used to test the mc-lag             */
/*                      system-identifier                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaCheckMclagSystemId ()
{
    if (gLaGlobalInfo.bIsMCLAGGlobalSysIDConfigured == LA_TRUE)
    {
        return LA_SUCCESS;
    }
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaCheckMclagSystemIdForPortChannel                   */
/*                                                                           */
/* Description        : This function is used to test the mc-lag             */
/*                      system-identifier in port-channel                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaCheckMclagSystemIdForPortChannel (UINT4 u4IfIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_LOCK ();

    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);

    if ((NULL != pAggEntry)
        && ((pAggEntry->bIsMCLAGSystemIdConfigured) == LA_TRUE))
    {
        LA_UNLOCK ();
        return LA_SUCCESS;
    }

    LA_UNLOCK ();
    return LA_FAILURE;
}

 /***************************************************************************
  *
  *    Function Name       : LaIcchApiTestICCLParams
  *
  *    Description         : This function checks ICCL paramters.If it is already 
  *                          use throws error message.
  *
  *
  *    Input(s)            : u2Status -LA_ENABLED/LA_DISABLED
  *
  *    Output(s)           : None
  *
  *    Global Var Referred : None
  *
  *    Global Var Modified : None
  *
  *    Use of Recursion    : None.
  *
  *    Exceptions or Operating
  *    System Error Handling    : None.
  *
  *
  *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
  *
  *****************************************************************************/

#ifdef ICCH_WANTED
INT4
LaIcchApiTestICCLParams (INT4 i4Status)
{

    UINT1               au1IcchInterface[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4IcclIpAddress = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IcchVlan = 0;
    UINT2               u2InstanceId = 0;
    CHR1               *pIcclIntfName = NULL;

    /*Before enabling MC-LAG check whether ICCL VLAN ID or port-channel is
     *already in use if ICCL settings is not a startup configurations*/
#ifndef ICCL_STARTUP_WANTED

    if (i4Status == LA_START)
    {

        MEMSET (au1IcchInterface, 0, CFA_MAX_PORT_NAME_LENGTH);

        pIcclIntfName = IcchApiGetIcclIpInterface (u2InstanceId);

        if (pIcclIntfName != NULL)
        {
            STRNCPY (au1IcchInterface, pIcclIntfName,
                     MEM_MAX_BYTES (STRLEN (pIcclIntfName),
                                    CFA_MAX_PORT_NAME_LENGTH - 1));
        }

        u4IcchVlan = IcchApiGetIcclVlanId (u2InstanceId);

        /*During MSR ICCL port-channel and VLAN will be created.Skip
         *the check if MSR is in progress.Otherwise the ICCL VLAN ID
         *and port-channel is used for other purpose.Hence block
         *configuring mc-lag.*/

        if (MsrIsMibRestoreInProgress () == MSR_FALSE)
        {
            if (CfaGetInterfaceIndexFromName (au1IcchInterface, &u4IfIndex)
                == OSIX_SUCCESS)
            {
                CLI_SET_ERR (CLI_LA_ICCL_PORT_CHNL_USED_ERR);
                return OSIX_FAILURE;
            }

            if ((VlanApiIsVlanExists ((tVlanId) u4IcchVlan) == VLAN_TRUE))
            {
                CLI_SET_ERR (CLI_LA_ICCL_VLAN_USED_ERR);
                return OSIX_FAILURE;

            }
        }

        u4IcclIpAddress = IcchApiGetIcclIpAddr (u2InstanceId);

        /*Check if ICCH IP is already in use. */
        if (SNMP_FAILURE == CfaIpIfIsLocalNet (u4IcclIpAddress))
        {
            CLI_SET_ERR (CLI_LA_ICCH_IP_USED);
            return OSIX_FAILURE;
        }

    }

#endif
    return OSIX_SUCCESS;
}
#endif
/***************************************************************************
*
*    Function Name       : LaHbResumeProtocolOperation
*
*    Description         : This function send an event to HB module when
*                          MC-LAG system control status is set as LA_START.
*
*
*    Input(s)            : None
*
*    Output(s)           : Post HB_MCLAG_START event to HB module
*
*    Global Var Referred : None
*
*    Global Var Modified : None
*
*    Use of Recursion    : None.
*
*    Exceptions or Operating
*    System Error Handling    : None.
*
*
*    Returns             : None
*
*****************************************************************************/

VOID
LaHbResumeProtocolOperation ()
{
    /*Send an event to HB to enable mc-lag functionality.

       1.Create ICCL port-channel,VLAN and IVR.
       2.Set ICCH properties.
       3.Disable STP on ICCL ports.
       4.Disable LLDP on ICCL ports
       5.Create TCP socket */

#ifdef ICCH_WANTED
    HbApiSendEvtFromMCLAG (HB_MCLAG_START);
#endif
    return;
}

/***************************************************************************
*
*    Function Name       : LaIsMclagStarted
*
*    Description         : This function is used to check whether MC-LAG
*                          functionality is started.
*
*    Input(s)            : None
*
*    Output(s)           : None
*
*    Global Var Referred : gLaMclagSystemControl
*
*    Global Var Modified : None
*
*    Use of Recursion    : None.
*
*    Exceptions or Operating
*    System Error Handling    : None.
*
*
*    Returns             : LA_TRUE when gLaMclagSystemControl is LA_START
*                          LA_FALSE when gLaMclagSystemControl is LA_SHUTDOWN
*
*****************************************************************************/
tLaBoolean
LaIsMclagStarted (VOID)
{
    if (gLaMclagSystemControl == LA_START)
    {
        LA_TRC (INIT_SHUT_TRC, "MC-LAG Module is Started.\n");
        return LA_TRUE;
    }

    LA_TRC (INIT_SHUT_TRC, "MC-LAG Module is Shutdown.\n");

    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaNotifyMclagShutdownComplete                        */
/*                                                                           */
/* Description        : This function is used to notify the MCLAG shutdown   */
/*                      completion to LA module                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
LaNotifyMclagShutdownComplete (VOID)
{
    gu1LaMclagShutDownInProgress = OSIX_FALSE;
    return;
}

/* End of File */
