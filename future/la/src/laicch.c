/***********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: laicch.c,v 1.13 2015/12/30 13:37:06 siva Exp $
 *
 * Description     : This file contains LA ICCH related routines
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : laicch.c                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : LA Module                                        */
/*  MODULE NAME           : LA                                               */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 31 Mar 2015                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/
#include "lahdrs.h"
/*****************************************************************************/
/* Function Name      : LaIcchRegisterWithICCH                               */
/*                                                                           */
/* Description        : Registers LA module with ICCH to send and receive    */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then LA_SUCCESS         */
/*                      Otherwise LA_FAILURE                               */
/*****************************************************************************/
INT4
LaIcchRegisterWithICCH (VOID)
{
    tIcchRegParams        IcchRegParams;

    MEMSET (&IcchRegParams, 0, sizeof (tIcchRegParams));

    IcchRegParams.u4EntId = ICCH_LA_APP_ID;
    IcchRegParams.pFnRcvPkt = LaIcchHandleUpdateEvents;

    if (LaIcchRegisterProtocols (&IcchRegParams) == ICCH_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIcchDeRegisterWithICCH                             */
/*                                                                           */
/* Description        : De-Registers LA module with ICCH.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if de-registration is success then LA_SUCCESS        */
/*                      Otherwise LA_FAILURE                                 */
/*****************************************************************************/
INT4
LaIcchDeRegisterWithICCH (VOID)
{
    tIcchRegParams        IcchRegParams;

    MEMSET (&IcchRegParams, 0, sizeof (tIcchRegParams));

    IcchRegParams.u4EntId = ICCH_LA_APP_ID;

    if (LaIcchDeRegisterProtocols (&IcchRegParams) == ICCH_FAILURE)
    {
        return LA_FAILURE;
    }

     return LA_SUCCESS;
 }



/*****************************************************************************/
/* Function Name      : LaIcchHandleUpdateEvents                             */
/*                                                                           */
/* Description        : This function will be invoked by the ICCH module to  */
/*                      pass events  to LA module                            */
/*                                                                           */
/* Input(s)           : u1Event - Event given by ICCH module.                */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                ICCH_MESSAGE.                              */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS - If the message is enq'ed successfully */
/*                      ICCH_FAILURE otherwise                               */
/*****************************************************************************/
INT4
LaIcchHandleUpdateEvents (UINT1 u1Event, tIcchMsg * pData, UINT2 u2DataLen)
{
    tLaIntfMesg        *pLaIntfMesg = NULL;

    if ((u1Event == ICCH_MESSAGE) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to LA task. */
        return ICCH_FAILURE;
    }

    if ((LA_INITIALIZED () == LA_FALSE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        /* LA is not initialized and hence the mempool for interface
         * messages and the queue will not be present. Hence return
         * from this place. */
        return ICCH_FAILURE;
    }

    if ((pLaIntfMesg = (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID))
        == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaIcchHandleUpdateEvents: Ctrl Mesg ALLOC_MEM_BLOCK "
                     "FAILED for event\n", u1Event);
        return ICCH_FAILURE;

    }

    LA_MEMSET (pLaIntfMesg, 0, sizeof (tLaIntfMesg));

    pLaIntfMesg->u2MesgType = LA_ICCH_MSG;

    pLaIntfMesg->uLaIntfMsg.IcchFrame.u1Event = u1Event;
    pLaIntfMesg->uLaIntfMsg.IcchFrame.pFrame = pData;
    pLaIntfMesg->uLaIntfMsg.IcchFrame.u2Length = u2DataLen;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pLaIntfMesg) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaIcchHandleUpdateEvents:"
                "ICCH Message enqueue FAILED\n");

        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pLaIntfMesg)
            != LA_MEM_SUCCESS)
        {

            LA_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "LaIcchHandleUpdateEvents: LA_FREE_MEM_BLOCK FAILED\n");
        }

        return ICCH_FAILURE;
    }

    /* Sent the event to LA CTRL Task. */
    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    return ICCH_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaIcchProcessUpdateMsg                               */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the ICCH module. This function invokes          */
/*                      appropriate functions to handle the update message.  */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the LA Queue message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaIcchProcessUpdateMsg (tLaIntfMesg * pMsg)
{
    tLaLacAggEntry       *pAggEntry = NULL;
    tIcchProtoAck        ProtoAck;
    UINT4                u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tIcchProtoAck));

    switch (pMsg->uLaIntfMsg.IcchFrame.u1Event)
    {
        case ICCH_INITIATE_BULK_UPDATES:
            LA_TRC (CONTROL_PLANE_TRC,
                    "Received INITIATE_BULK_UPDATES from lower layer "
                    "module \n");
            LaIcchSendBulkReqMsg ();
            break;

        case ICCH_MESSAGE:
            /* Read the sequence number from the ICCH Header */
            ICCH_PKT_GET_SEQNUM (pMsg->uLaIntfMsg.IcchFrame.pFrame, &u4SeqNum);
            /* Remove the ICCH Header */
            ICCH_STRIP_OFF_ICCH_HDR (pMsg->uLaIntfMsg.IcchFrame.pFrame,
                    pMsg->uLaIntfMsg.IcchFrame.u2Length);

            ProtoAck.u4AppId = ICCH_LA_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            LaIcchHandleUpdates (pMsg->uLaIntfMsg.IcchFrame.pFrame,
                    pMsg->uLaIntfMsg.IcchFrame.u2Length);
            ICCH_FREE (pMsg->uLaIntfMsg.IcchFrame.pFrame);

            /* Sending ACK to ICCH */
            IcchApiSendProtoAckToICCH (&ProtoAck);
            break;

        case ICCH_PEER_UP:
            LA_MCLAG_PEER_EXISTS = LA_TRUE;
            LaMCLAGProcessPeerUpEvent ();
            IcchReleaseMemoryForMsg ((UINT1 *) pMsg->uLaIntfMsg.IcchFrame.pFrame);
            break;

        case ICCH_PEER_DOWN:
            LA_MCLAG_PEER_EXISTS = LA_FALSE;
            LaMCLAGProcessPeerDownEvent ();
            IcchReleaseMemoryForMsg ((UINT1 *) pMsg->uLaIntfMsg.IcchFrame.pFrame);
            break;

        case GO_MASTER:
            if (IcchApiGetPeerAddress () != 0)
            {
                LaActiveMCLAGMasterUpIndication ();
            }
            LA_AA_DLAG_ROLE_PLAYED = LA_MCLAG_SYSTEM_ROLE_MASTER;
            LA_AA_DLAG_MASTER_DOWN = LA_FALSE;
            LaGetNextAggEntry (NULL, &pAggEntry);

            while (pAggEntry != NULL)
            {
                if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
                {
                    pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_MASTER;
                    if (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList
                        == NULL)
                    {
                        /* Create RBTree for DLAG Total port List */
                        pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList =
                        LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                                (tLaDLAGConsPortEntry,
                                                 LaDLAGConsPortInfo),
                                                LaCmpPortPriority);
                    }
                    LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
                }                    
                LaGetNextAggEntry (pAggEntry, &pAggEntry);
            }
            break;

        case GO_SLAVE:
            LA_AA_DLAG_ROLE_PLAYED = LA_MCLAG_SYSTEM_ROLE_SLAVE;
            LA_AA_DLAG_MASTER_DOWN = LA_FALSE;
            LaGetNextAggEntry (NULL, &pAggEntry);

            while (pAggEntry != NULL)
            {
                if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
                {
                    pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_SLAVE;
                    if (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList
                            != NULL)
                    {
                        LaActiveDLAGDeleteEntriesInAllTree (pAggEntry);

                        LaActiveDLAGDestroyPortListTree (
                                pAggEntry->LaDLAGConsInfoTable.
                                LaDLAGConsolidatedList, NULL, 0);
                        pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList
                                = NULL;
                    }
                }
                LaGetNextAggEntry (pAggEntry, &pAggEntry);
            }
            break;

        default:
            break;
    } /* End of switch */
    return;
}

/*****************************************************************************/
/* Function Name      : LaIcchHandleUpdates                                  */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      ICCH module. This function invokes appropriate fns.  */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to tIcchMsg.                          */
/*                      u2DataLen - Data length.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaIcchHandleUpdates (tIcchMsg * pMsg, UINT2 u2DataLen)
{
    tIcchProtoEvt       ProtoEvt;
    UINT1               au1LinearPdu[LA_AA_DLAG_PDU_SIZE] = { 0 };
    UINT1              *pu1LinearBuf = NULL;                             
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2IcchMsgLen = 0;;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));

    pu1LinearBuf = au1LinearPdu;
    ProtoEvt.u4AppId = ICCH_LA_APP_ID;
    ProtoEvt.u4Event = ICCH_BULK_UPDT_ABORT;

    u2MinLen = LA_ICCH_MSGTYPE_FIELD_SIZE + LA_ICCH_LEN_FIELD_SIZE;

    LA_ICCH_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    if (u1MsgType == LA_ICCH_BULK_UPD_TAIL_MESSAGE)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                         "Bulk update tail message received, which "
                         "indicates that bulk updates are completed "
                         "in LA module. It should be indicated to "
                         "ICCH modules. \n");
        ProtoEvt.u4Error = ICCH_NONE;
        ProtoEvt.u4Event = ICCH_PROTOCOL_BULK_UPDT_COMPLETION;
        LaIcchHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u1MsgType == LA_ICCH_BULK_UPD_REQUEST)
    {
        LaIcchSendBulkUpdates ();
    }

    while ((u2OffSet + u2MinLen) < u2DataLen)
    {
        /*shiju*/
        /*LA_ICCH_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);*/

        LA_ICCH_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* doesnot have room for type and length field itself.
             * Skip to next attribute */
            u2OffSet += u2Length;
            continue;
        }

        u2IcchMsgLen = u2Length - u2MinLen;

        switch (u1MsgType)
        {
           /* Processing LA sync DSU message */
           case LA_ICCH_SYNC_DSU:
                    LA_ICCH_GET_N_BYTE (pMsg, pu1LinearBuf, &u2OffSet, u2IcchMsgLen);
                    LaActiveMCLAGProcessMCLAGPdu (0, pu1LinearBuf, u2IcchMsgLen);
                    break;

        } /* End of switch */
    }
}

/*****************************************************************************/
/* Function Name      : LaIcchSendBulkUpdates                                */
/*                                                                           */
/* Description        : This function sends bulk updates to the remote node. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaIcchSendBulkUpdates (VOID)
{
    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "Bulk Update is not applicable for LA! \n");

    LaIcchSetBulkUpdatesStatus (ICCH_LA_APP_ID);

    /* Send the tail msg to indicate the completion of Bulk
     * update process.
     */
    LaIcchSendBulkUpdTailMsg ();
    return;

}

/*****************************************************************************/
/* Function Name      : LaIcchSyncBulkLaEntries                              */
/*                                                                           */
/* Description        : This function will send vlan update                  */
/*                      syncup messages to remote node in bulk.              */
/*                                                                           */
/* Input(s)           : u4Context   - Virtual ContextId                      */
/*                      ppMsg - pointer to pointer to IcchMsg.               */
/*                                                                           */
/* Output(s)          : *ppMsg - Updated pointer to IcchMsg after bulk update*/
/*                      pu2Offset - Updated offset.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                         */
/*****************************************************************************/
INT4
LaIcchSyncBulkLaEntries (tIcchMsg **ppMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM(pu2OffSet);
    UNUSED_PARAM(ppMsg);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIcchSendBulkReqMsg                                 */
/*                                                                           */
/* Description        : This function sends bulk request message to the      */
/*                      remote node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/

INT4
LaIcchSendBulkReqMsg (VOID)
{
    tIcchMsg            *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT4               u4SyncMsgType = 0;
    UINT4               u4Offset = 0;
    UINT2               u2BufSize = 0;

    ProtoEvt.u4AppId = ICCH_LA_APP_ID;
    ProtoEvt.u4Event = ICCH_BULK_UPDT_ABORT;

    u2BufSize = LA_ICCH_SYNC_MSG_TYPE_SIZE + LA_ICCH_LEN_FIELD_SIZE;

    LA_TRC (CONTROL_PLANE_TRC, "LA ICCH: Send Bulk Req\n");
    /* Allocate memory for data to be sent to ICCH. This memory will be freed by
     * ICCH after Txmitting  */
    if ((pMsg = ICCH_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "ICCH alloc. failed\n");
        ProtoEvt.u4Error = ICCH_MEMALLOC_FAIL;
        LaIcchHandleProtocolEvent (&ProtoEvt);
        return (LA_FAILURE);
    }

    /* Fill the message type. */
    u4SyncMsgType = ICCH_BULK_UPDATE_MSG;
    LA_ICCH_PUT_4_BYTE (pMsg, &u4Offset, u4SyncMsgType);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    LA_ICCH_PUT_2_BYTE (pMsg, &u4Offset, ICCH_BULK_UPDT_REQ_MSG);

    if (LaIcchSendMsgToIcch (pMsg, u2BufSize) == LA_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        LaIcchHandleProtocolEvent (&ProtoEvt);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIcchSendBulkUpdTailMsg                             */
/*                                                                           */
/* Description        : This function will send the tail msg to the slave    */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaIcchSendBulkUpdTailMsg (VOID)
{
    tIcchMsg         *pMsg = NULL;
    tIcchProtoEvt       ProtoEvt;
    UINT4               u4Offset = 0;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = ICCH_LA_APP_ID;
    ProtoEvt.u4Event = ICCH_BULK_UPDT_ABORT;

    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Node is not active. Bulk Update tail msg "
                "not sent.\n");
        return LA_SUCCESS;
    }

    u2BufSize = LA_ICCH_SYNC_MSG_TYPE_SIZE + LA_ICCH_LEN_FIELD_SIZE;

    /* Allocate memory for data to be sent to ICCH. This memory will be freed by
     * ICCH after Txmitting  */
    if ((pMsg = ICCH_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "ICCH alloc failed\n");
        ProtoEvt.u4Error = ICCH_MEMALLOC_FAIL;
        LaIcchHandleProtocolEvent (&ProtoEvt);
        return (LA_FAILURE);
    }

    /* Form a bulk update tail message.

     *          <--------4 Byte--------><---2 Byte--->
     *************************************************
     *          *                      *             *
     * ICCH Hdr * LA_BULK_UPD_TAIL_MSG * Msg Length  *
     *          *                      *             *
     *************************************************

     * The ICCH Hdr shall be included by ICCH.
     */

    /* Fill the message type. */
    LA_ICCH_PUT_4_BYTE (pMsg, &u4Offset, ICCH_BULK_UPDT_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    LA_ICCH_PUT_2_BYTE (pMsg, &u4Offset, ICCH_BULK_UPDT_TAIL_MSG);
    if (LaIcchSendMsgToIcch (pMsg, u2BufSize) == LA_FAILURE)
    {
        ProtoEvt.u4Error = ICCH_SENDTO_FAIL;
        LaIcchHandleProtocolEvent (&ProtoEvt);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIcchSendDSUToPeer                                  */
/*                                                                           */
/* Description        : This function will send LA DSU to remote node        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaIcchSendDSUToPeer (UINT1 *pu1LinearBuf, UINT4 u4DataLength)
{
    tIcchMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgLen = 0;

    /* Form a ICCH sync message with DSU
     *
     * |---------------------------------|
     * |Msg. Type |   Length  |   DSU    |
     * | (1 byte) | (2 bytes) | (N bytes)|
     * |---------------------------------|
     * The ICCH Hdr shall be included by ICCH
     */
    u2MsgLen = LA_ICCH_MSGTYPE_FIELD_SIZE + LA_ICCH_LEN_FIELD_SIZE +
               u4DataLength;

    if ((pMsg = ICCH_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        LA_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                 "ICCH alloc failed. LA addition sync up message"
                 "not sent \n");
        return LA_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (u2MsgLen));
    LA_TRC (CONTROL_PLANE_TRC, "Sending LA DSU to peer node\n");

    LA_ICCH_PUT_1_BYTE (pMsg, &u2OffSet, LA_ICCH_SYNC_DSU);
    LA_ICCH_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    LA_ICCH_PUT_N_BYTE (pMsg, pu1LinearBuf, &u2OffSet, u4DataLength);

    LaIcchSendMsgToIcch (pMsg, u2MsgLen);
    
    return LA_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : LaIcchCheckAndSendMsg                                */
/*                                                                           */
/* Description        : This function will check if the room size exists in  */
/*                      the input buffer pIcchMsg.                           */
/*                      If yes, the buffer is sent to ICCH.                  */
/*                      else, return LA_SUCCESS.                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaIcchCheckAndSendMsg (tIcchMsg **ppIcchMsg, UINT2 u2MsgLen, UINT2 *pu2OffSet)
{
    tIcchProtoEvt ProtoEvt;
    UINT2         u2BufSize = LA_ICCH_MAX_MSG_SIZE;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));


    if ((u2BufSize - *pu2OffSet) < u2MsgLen)
    {
        /* no room for the current message */
        LaIcchSendMsgToIcch (*ppIcchMsg, *pu2OffSet);

        *ppIcchMsg = ICCH_ALLOC_TX_BUF (u2BufSize);

        if (*ppIcchMsg == NULL)
        {
            LA_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC, 
                    "ICCH alloc failed. Bulk updates not sent\n");
            ProtoEvt.u4Error = ICCH_MEMALLOC_FAIL;
            LaIcchHandleProtocolEvent (&ProtoEvt);
            return LA_FAILURE;
        }
        *pu2OffSet = 0;

    }

    return LA_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : LaIcchSendMsgToIcch                                     */
/*                                                                            */
/*  Description     : This function enqueues the update messages to the ICCH  */
/*                    module.                                                 */
/*                                                                            */
/*  Input(s)        : pMsg  - Message to be sent to ICCH module               */
/*                    u2Len - Length of the Message                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : LA_SUCCESS/LA_FAILURE                                   */
/******************************************************************************/
INT4
LaIcchSendMsgToIcch (tIcchMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal;

    u4RetVal = LaIcchEnqMsgToIcchFromAppl (pMsg, u2Len, ICCH_LA_APP_ID,
                                         ICCH_LA_APP_ID);

    if (u4RetVal != ICCH_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Enqueuing of Messages from Appl to Icch failed \n");

        ICCH_FREE (pMsg);
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/******************************************************************************/
/*  Function Name      : LaSetIcclProperties                                  */
/*                                                                            */
/*  Description        : This function is called when LA module is started    */
/*                       to create ICCL interface and to set other properties */ 
/*                       for the ICCL link.                                   */
/*                                                                            */
/*  Input(s)           : None                                                 */
/*                                                                            */
/*  Output(s)          : None                                                 */
/*                                                                            */
/*  Global Variables   : None                                                 */
/*  Referred                                                                  */
/*                                                                            */
/*  Global variables   : None                                                 */
/*  Modified                                                                  */
/*                                                                            */
/*  Use of Recursion   : None                                                 */
/*                                                                            */
/*  Returns            : LA_SUCCESS/LA_FAILURE                                */
/*                                                                            */
/******************************************************************************/
INT4
LaSetIcclProperties (VOID)
{
    UINT4   u4IcclIfIndex = 0;

    /* Create ICCL interface */

    LaCfaCreateIcchIPInterface (ICCH_DEFAULT_INST);

    if (VlanApiSetIcchPortProperties() != VLAN_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "!!!LaSetIcclProperties:  VlanApiSetIcchPortProperties Failure !!!\r\n");
        return LA_FAILURE;
    }

    LaIcchGetIcclIfIndex (&u4IcclIfIndex);

    /* Disable STP, LLDP on ICCL interface */
    if (LaAstDisableStpOnPort (u4IcclIfIndex) != OSIX_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "!!!LaSetIcclProperties: Disabling STP on port Failed!!!\r\n");
        return LA_FAILURE;
    }

    if (LaApiDisableLldpOnIcclPorts (u4IcclIfIndex) != LA_TRUE)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "!!!LaSetIcclProperties: Disabling LLDP on port is Failed!!!\r\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}



