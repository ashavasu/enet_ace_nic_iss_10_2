/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: lambsm.c,v 1.20 2014/12/09 12:46:05 siva Exp $*/
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : lambsm.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LA module                                      */
/*    MODULE NAME           : LA module Card Updation                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 21 Jan 2005                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            LA module.                                     */
/*---------------------------------------------------------------------------*/

#include "lahdrs.h"

/*****************************************************************************/
/* Function Name      : LaMbsmUpdateCardInsertion                            */
/*                                                                           */
/* Description        : This function programs the HW with the LA software   */
/*                      configuration.                                       */
/*                                                                           */
/*                      This function will be called from the LA module      */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmPortInfo - Structure containing the PortList,  */
/*                                       Starting Index and the port count.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
LaMbsmUpdateCardInsertion (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2AggIndex = 0;
    UINT2               u2PortNo = 1;
    UINT2               au2ActivePortArray[LA_MAX_PORTS_PER_AGG];
    UINT1               u1PortCount = 0;
    UINT1               u1SelectionPolicy = 0;

    UNUSED_PARAM (pPortInfo);
    LA_MEMSET (au2ActivePortArray, 0, sizeof (au2ActivePortArray));

    /* Program the HW for LA. Only For Card Update Message */
    if (0 == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (LaFsLaMbsmHwInit (pSlotInfo) == FNP_FAILURE)
        {
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaFsLaMbsmHwInit:"
                    "  Programming the HW for LA failed\n");
            return MBSM_FAILURE;
        }
    }

    for (u2AggIndex = LA_MIN_AGG_INDEX;
         u2AggIndex <= LA_MAX_AGG_INDEX; u2AggIndex++)
    {
        LaGetAggEntry (u2AggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            /* If the Aggregate Entry in the SW is not present,
             * do not create the entry in the HW.
             */
            continue;
        }

        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

        while (pPortEntry != NULL)
        {
            LaFsLaMbsmHwAddPortToConfAggGroup (u2AggIndex,
                                               pPortEntry->u2PortIndex,
                                               pSlotInfo);
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

        /* Create the trunk entry in the HW. */
        if (LaFsLaMbsmHwCreateAggGroup (u2AggIndex, pSlotInfo) == FNP_FAILURE)
        {
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LaFsLaMbsmHwCreateAggGroup: Creating a trunk entry in the HW failed\n");
            return MBSM_FAILURE;
        }

        /* Get the active ports for this trunk entry. */
        if (LaLacGetActivePorts (u2AggIndex, au2ActivePortArray,
                                 &u1PortCount) != LA_SUCCESS)
        {
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
               "LaLacGetActivePorts: Getting active ports for trunk entry failed\n");  
            return MBSM_FAILURE;
        }

        while (u1PortCount)
        {
            u2PortNo = au2ActivePortArray[u1PortCount - 1];

            /* Add the active ports to this link in the HW. */
            if (LaFsLaMbsmHwAddLinkToAggGroup (u2AggIndex, u2PortNo,
                                               pSlotInfo) == FNP_FAILURE)
            {
                LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "LaFsLaMbsmHwAddLinkToAggGroup: Adding active ports to the link in the HW failed\n");
                return MBSM_FAILURE;
            }
            u1PortCount--;
        }

        /* Set the selection policy for the AggIndex. */
        u1SelectionPolicy = (UINT1) pAggEntry->AggConfigEntry.LinkSelectPolicy;
        if (LaFsLaMbsmHwSetSelectionPolicy (u2AggIndex, u1SelectionPolicy,
                                            pSlotInfo) == FNP_FAILURE)
        {
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "LaFsLaMbsmHwSetSelectionPolicy: Setting the selection policy for the AggIndex failed\n");
            return MBSM_FAILURE;
        }

    }

    LA_TRC (CONTROL_PLANE_TRC, "LaMbsmUpdateCardInsertion programmed the HW with the LA software and returns success\n");    

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMbsmProcessUpdateMessage                           */
/*                                                                           */
/* Description        : This function constructs the LA Q Mesg and calls the */
/*                      LA CONTROL event to process this Mesg.               */
/*                                                                           */
/*                      This function will be called from the LA module      */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tLaIntfMesg        *pMesg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL != (UINT1) LA_START))
    {
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    /* Allocate Control Message buffer from the pool */
    if ((pMesg = (tLaIntfMesg *)
         LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID)) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "LaEnqueueControlFrame: Ctrl Mesg ALLOC_MEM_BLOCK FAILED\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaEnqueueControlFrame: Ctrl Mesg ALLOC_MEM_BLOCK FAILED"));
        return MBSM_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaIntfMesg));

    if (!(pMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg);
        return MBSM_FAILURE;
    }
    
    LA_MEMSET (pMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg, 0, sizeof (tMbsmProtoMsg));

    pMesg->u2MesgType = (UINT2) i4Event;
    MEMCPY (pMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
            sizeof (tMbsmProtoMsg));

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pMesg) == LA_OSIX_SUCCESS)
    {
        if (LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT) != LA_OSIX_SUCCESS)
        {
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaEnqueueControlFrame:"
                    " SendEvent of Control Frame FAILED\n");

            MEM_FREE (pMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg);
            LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg);

            return MBSM_FAILURE;
        }
        return MBSM_SUCCESS;
    }
    LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
            "LaEnqueueControlFrame:" "LA_Enqueue Control Message FAILED\n");

    MEM_FREE (pMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg);
    LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pMesg);

    return MBSM_FAILURE;
}
