/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                     */
/* $Id: lactrlpm.c,v 1.39 2010/10/20 11:15:25 prabuc Exp                    */
/* Licensee Aricent Inc., 2001-2002                                         */
/*****************************************************************************/
/*    FILE  NAME            : lactrlpm.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Control Parser/Multiplexer Module           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains control parser & multiplexer*/
/*                            functions for LA. It contains all the interface*/
/*                            functions with lower and upper layers.         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*****************************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaHandOverOutFrame                                   */
/*                                                                           */
/* Description        : This function hands over the outgoing frames, which  */
/*                      it receives from the Aggregator Multiplexer or LAC   */
/*                      module, to the CFA module.                           */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the data buffer to be transmitted  */
/*                      u2PortIndex - The interface/port index               */
/*                      *pPktParams - Pointer to the structure having the    */
/*                      frame parameters, which need to be passed to the CFA */
/*                      module. This will be passed as NULL for locally      */
/*                      generated MarkerPDUs and LACPDUs.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaHandOverOutFrame (tLaBufChainHeader * pBuf,
                    UINT2 u2PortIndex, tLaPktParams * pPktParams)
{
    UINT4               u4PktSize;
    UINT2               u2Protocol;
    UINT1               u1EncapType;

    if (pBuf == NULL)
    {
        LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "CTRL: Null Pointer\n");
        return LA_FAILURE;
    }

/* For locally generated Marker PDUs and LACPDUs, the value of pPktParams is
 * null. Hence fill in the values as below before handing over the frame to
 * PNAC or CFA */

    if (pPktParams == NULL)
    {
        u4PktSize = LA_SLOW_PROTOCOL_SIZE;
        u2Protocol = LA_SLOW_PROTOCOL;
        u1EncapType = LA_ENCAP_NONE;

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "CTRL: Port: %d ::Sending Out LA PDU...\n", u2PortIndex);
    }
    else
    {
        u4PktSize = pPktParams->u4PktSize;
        u2Protocol = pPktParams->u2Protocol;
        u1EncapType = pPktParams->u1EncapType;

        LA_TRC_ARG1 (DATA_PATH_TRC,
                     "CTRL: Port: %d ::Sending Out Data Pkt...\n", u2PortIndex);
    }

    if (LA_NODE_ACTIVE == LA_NODE_STATUS ())
    {
        LaL2IwfHandleOutgoingPktOnPort (pBuf, u2PortIndex, u4PktSize,
                                        u2Protocol, u1EncapType);
    }
    else
    {
        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "CTRL: CRU Buffer Release FAILED\n");
        }

    }

    LA_TRC_ARG1 (DATA_PATH_TRC,
                 "CTRL: Port: %d Outgoing Frame handed over CFA module\n",
                 u2PortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaEnqueueControlFrame                                */
/*                                                                           */
/* Description        : This function is invoked from LaHandleIncomingFrame  */
/*                      to send the LACP frame to the LA Control task.       */
/*                                                                           */
/* Input(s)           : pBuf -The Control Frame                              */
/*                      u2PortIndex -Port on which it is recieved            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaEnabled.                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaEnqueueControlFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex)
{
    tLaCtrlMesg        *pMesg = NULL;

    if ((LA_INITIALIZED () != TRUE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "La is not started\n");
        return LA_FAILURE;
    }

    if (u2PortIndex > LA_MAX_PORTS)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaEnqueueControlFrame:"
                     "Port %u :Invalid Port Index\n", u2PortIndex);
        return LA_FAILURE;
    }

    LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 "Called LaEnqueueControlFrame for Port %d\n", u2PortIndex);

    /* Allocate Control Message buffer from the pool */
    if ((pMesg =
         (tLaCtrlMesg *) LA_ALLOC_MEM_BLOCK (LA_CTRLMESG_POOL_ID)) == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaEnqueueControlFrame: Ctrl Mesg ALLOC_MEM_BLOCK FAILED\n",
                     u2PortIndex);
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, LA_SYSLOG_ID,
                      "LaEnqueueControlFrame: Ctrl Mesg ALLOC_MEM_BLOCK FAILED"));
        return LA_FAILURE;
    }

    LA_MEMSET (pMesg, 0, sizeof (tLaCtrlMesg));

    pMesg->u4MesgType = LA_CONTROL_FRAME;

    pMesg->CtrlFrame.u2PortIndex = u2PortIndex;
    pMesg->CtrlFrame.pFrame = pBuf;

    if (LA_ENQUEUE_MESSAGE (LA_CTRLQ_ID, pMesg) != LA_OSIX_SUCCESS)
    {

        LA_TRC_ARG1 (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "LaEnqueueControlFrame: Port %d :"
                     "LA_Enqueue Control Message FAILED\n", u2PortIndex);

        if (LA_FREE_MEM_BLOCK (LA_CTRLMESG_POOL_ID, (UINT1 *) pMesg)
            != LA_MEM_SUCCESS)
        {

            LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaEnqueueControlFrame: Port %d:"
                         "LA_FREE_MEM_BLOCK FAILED\n", u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_SEND_EVENT (LA_TASK_ID, LA_PKT_ENQ_EVENT);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIsThisLAControlPacket                              */
/*                                                                           */
/* Description        : This function is invoked from LaHandleIncomingFrame  */
/*                      to check if the received frame is LACP Frame or not  */
/*                                                                           */
/* Input(s)           : pBuf -The Received Frame.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - If the frame is an LACP/Marker Frame.      */
/*                      LA_FALSE - If the frame is a data frame.             */
/*****************************************************************************/
INT4
LaIsThisLAControlPacket (tLaBufChainHeader * pBuf,
                         tLaMacAddr * pLaDestMacAddress)
{
    UINT2               u2ProtType = 0;
    UINT1               u1SubType = 0;
    UINT1               au1LaSlowMcastAddr[] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };

    /* We compare the Destination Address of the received frame in order to
     * determine if the packet is a data frame or is a LA frame (i.e. LACPDU or
     * Marker PDU). Here, we assume that the Destination Address field located
     * in the first 6 bytes of the frame is located linearly in the CRU buffer.
     */
    LA_TRC (CONTROL_PLANE_TRC, "Called LaIsThisLAControlPacket");

    if (LA_COPY_FROM_CRU_BUF
        (pBuf, (UINT1 *) pLaDestMacAddress, LA_DESTADDR_OFFSET,
         LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
    {
        LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | BUFFER_TRC |
                ALL_FAILURE_TRC,
                "LaIsThisLAControlPacket: "
                "Frame Copying From CRU Buffer Failed\n");

        return LA_FALSE;
    }

    /* The following check is made to determine if the received frame is a Mac
     * Client frame or an LACPDU*/

    if ((LA_MEMCMP (pLaDestMacAddress, au1LaSlowMcastAddr, LA_ETH_ADDR_SIZE))
        == 0)
    {

        /* this is a LACP frame */

        if ((LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u2ProtType,
                                   LA_PROTOCOL_TYPE_OFFSET,
                                   2) == LA_CRU_FAILURE)
            ||
            (LA_COPY_FROM_CRU_BUF
             (pBuf, (UINT1 *) &u1SubType, LA_PROTOCOL_SUBTYPE_OFFSET,
              1) == LA_CRU_FAILURE))
        {

            LA_TRC (DATA_PATH_TRC | CONTROL_PLANE_TRC | BUFFER_TRC |
                    ALL_FAILURE_TRC,
                    "LaIsThisLAControlPacket: "
                    "Frame Copying From CRU Buffer Failed\n");
            return LA_FALSE;
        }
        u2ProtType = LA_NTOHS (u2ProtType);

        /* Check the Type and sub type */
        if ((u2ProtType == LA_SLOW_PROT_TYPE) &&
            ((u1SubType == LA_LACP_SUBTYPE) ||
             (u1SubType == LA_MARKER_SUBTYPE)))
        {

            LA_TRC (CONTROL_PLANE_TRC, "LaIsThisLAControlPacket:"
                    "Received an LA Control Frame\n");

            return LA_TRUE;
        }
    }

    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaHandlePktEnqEvent                                  */
/*                                                                           */
/* Description        : This function is called whenever the task receives   */
/*                      a Pkt Enqueue event. This event is posted if         */
/*                      the lower layer (or CFA) receives an LACP frame.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : LA_CONTROL_MESG_Q and LA_CONTROL_MESG_MEM_POOL       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaHandlePktEnqEvent ()
{
    tLaCtrlMesg        *pMesg = NULL;

    /* Event received, dequeue messages */

    while (LA_DEQUEUE_MESSAGES (LA_CTRLQ_ID, &pMesg) == LA_OSIX_SUCCESS)
    {

        if (pMesg->u4MesgType == LA_CONTROL_FRAME)
        {
            UINT2               u2PortIndex = pMesg->CtrlFrame.u2PortIndex;
            tLaBufChainHeader  *pBuf;

            pBuf = (tLaBufChainHeader *) pMesg->CtrlFrame.pFrame;

            LA_LOCK ();

            LaProcessLacpFrame (pBuf, u2PortIndex);

            LA_UNLOCK ();
        }
        else
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                         "LaHandlePktEnqEvent:"
                         "Unknown Control Message (Type %d) received\n",
                         pMesg->u4MesgType);
        }

        /* Now release the buffer to pool */

        LA_FREE_MEM_BLOCK (LA_CTRLMESG_POOL_ID, (UINT1 *) pMesg);
    }
}

/*****************************************************************************/
/* Function Name      : LaHandleCtrlEvents                                   */
/*                                                                           */
/* Description        : This function is called whenever the task receives   */
/*                      a CONTROL event. A CONTROL event is posted if there  */
/*                      lower layer (CFA or PNAC) needs to indicate any      */
/*                      interface creation or deletion or oper status change */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : LA_CONTROL_MESG_Q and LA_CONTROL_MESG_MEM_POOL       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaHandleCtrlEvents ()
{
    tLaIntfMesg        *pIntfMesg = NULL;
    tMacAddr            MacAddr = { 0 };
    UINT2               u2PortIndex;
    INT4                i4RetVal;
    tCfaIfInfo          CfaIfInfo;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;
    tLaPortList         DefaultDLAGPortList;
    UINT1               u1BridgedIfaceStatus;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    INT4                i4Result = 0;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    UINT4               u4MasterSlotId = 0;
#endif

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));
    LA_MEMSET (&DefaultDLAGPortList, 0, sizeof (tLaPortList));

    /* Event received, dequeue messages */

    while (LA_DEQUEUE_MESSAGES (LA_INTFQ_ID, &pIntfMesg) == LA_OSIX_SUCCESS)
    {
        if (pIntfMesg == NULL)
            continue;

        LA_LOCK ();

        u2PortIndex = pIntfMesg->u2PortIndex;

        switch (pIntfMesg->u2MesgType)
        {
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:

                i4ProtoId =
                    pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg->
                    i4ProtoCookie;
                pSlotInfo =
                    &(pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg->
                      MbsmSlotInfo);
                pPortInfo =
                    &(pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg->
                      MbsmPortInfo);
                i4Result = LaMbsmUpdateCardInsertion (pPortInfo, pSlotInfo);
                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;

                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = i4Result;

                /* Incase of Active-Active DLAG if Master
                 * is Up then we need to Send the DLAG system
                 * Parameters in LACP PDU to Peer. */

                MbsmGetDissMasterSlotId (&u4MasterSlotId);
                if ((MbsmProtoAckMsg.i4SlotId == (INT4) u4MasterSlotId) &&
                    (MbsmProtoAckMsg.i4SlotId != IssGetSwitchid ()))
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                 " MASTER [%d] ATTACHED... \r\n",
                                 MbsmProtoAckMsg.i4SlotId);
                    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
                    {
                        LA_TRC (CONTROL_PLANE_TRC, " ENABLE DLAG... \r\n");

                        LaActiveDLAGMasterUpIndication ();
                    }

                }

                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MEM_FREE (pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg);

                break;

            case MBSM_MSG_CARD_REMOVE:

                i4ProtoId =
                    pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg->
                    i4ProtoCookie;
                pSlotInfo =
                    &(pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg->
                      MbsmSlotInfo);
                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;

                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;

                /* Incase of Active-Active DLAG if Maste is Down.
                 * We should act like normal LACP is which it
                 * communicates with Peer using its Default 
                 * Parameters used for LACP instead if DLAG
                 * System Parameters */

                if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
                {
                    LaActiveDLAGHandleCardDelete (MbsmProtoAckMsg.i4SlotId);
                }

                LaSnoopNotifyCardRemovalEvent ();

                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MEM_FREE (pIntfMesg->uLaIntfMsg.MbsmCardUpdate.pMbsmProtoMsg);

                break;
#endif

            case LA_CREATE_PORT_MSG:
                if (LaCfaGetIfInfo (u2PortIndex, &CfaIfInfo) == CFA_SUCCESS)
                {
                    if (CfaIfInfo.u1IfType == CFA_ENET)
                    {
                        LaHandleCreatePort (u2PortIndex);
                    }
                    else if (CfaIfInfo.u1IfType == CFA_LAGG)
                    {
                        LaCreatePortChannelInterface (u2PortIndex);
                    }
                }

                L2_SYNC_GIVE_SEM ();
                break;

            case LA_DELETE_PORT_MSG:
                LaGetPortEntry (u2PortIndex, &pPortEntry);
                if (pPortEntry != NULL)
                {
                    LaHandleDeletePort (u2PortIndex);
                }
                else
                {
                    /* This should be a Port-channel */
                    LaDeletePortChannelInterface (u2PortIndex);
                }

                LA_RELEASE_PROTOCOL_SEMAPHORE (gLaSyncSemId);

                break;

            case LA_MAP_PORT_MSG:
                if (LaCfaGetIfInfo (u2PortIndex, &CfaIfInfo) == CFA_SUCCESS)
                {
                    if (CfaIfInfo.u1IfType == CFA_ENET)
                    {
                        LaHandleCreatePort (u2PortIndex);
                    }
                    else if (CfaIfInfo.u1IfType == CFA_LAGG)
                    {
                        LaCreatePortChannelInterface (u2PortIndex);
                    }
                }

                L2MI_SYNC_GIVE_SEM ();
                break;

            case LA_UNMAP_PORT_MSG:
                LaGetPortEntry (u2PortIndex, &pPortEntry);
                if (pPortEntry != NULL)
                {
                    LaHandleDeletePort (u2PortIndex);
                }
                else
                {
                    /* This should be a Port-channel */
                    LaDeletePortChannelInterface (u2PortIndex);
                }

                L2MI_SYNC_GIVE_SEM ();
                break;

            case LA_ENABLE_PORT_MSG:
                if (LaGetLaEnableStatus () == LA_ENABLED)
                {
                    if ((i4RetVal = LaGetPortEntry (u2PortIndex, &pPortEntry))
                        == LA_SUCCESS)
                    {
                        if (pPortEntry->u1PortOperStatus != CFA_IF_UP)
                        {
                            pPortEntry->u1PortOperStatus = CFA_IF_UP;
                            /* This global variable is used to control LACP PDU negotiation 
                             * once the Port Channel Interface is made admin down */
                            /* If gu4LagAdminDownNoNeg is LA_TRUE, LACP PDU negotiation 
                             * will be stopped. If it is LA_FALSE, LACP PDU negotiation
                             * can happen through the member ports though port channel 
                             * interface is down*/

                            if (LaActiveDLAGCheckIsLocalPort
                                (pPortEntry->u2PortIndex) != LA_SUCCESS)
                            {

                                if (LaCfaGetIfInfo
                                    (pPortEntry->u2PortIndex,
                                     &CfaIfInfo) != CFA_SUCCESS)
                                {
                                    LA_TRC_ARG1 (CONTROL_PLANE_TRC |
                                                 ALL_FAILURE_TRC,
                                                 "Retrieval of port %d properties failed!\n",
                                                 pPortEntry->u2PortIndex);

                                    break;
                                }

                                pPortEntry->u4LinkSpeed = CfaIfInfo.u4IfSpeed;
                                pPortEntry->u4LinkHighSpeed =
                                    CfaIfInfo.u4IfHighSpeed;
                                pPortEntry->u4LinkDuplex =
                                    CfaIfInfo.u1DuplexStatus;
                                pPortEntry->u4Mtu = CfaIfInfo.u4IfMtu;
                                pPortEntry->i4PauseAdminMode =
                                    (INT4) CfaIfInfo.u1PauseAdminNode;

                                LaL2IwfLaHLPortOperIndication
                                    (pPortEntry->u2PortIndex, CFA_IF_UP);

                                break;

                            }

                            if (gu4LagAdminDownNoNeg == LA_TRUE)
                            {
                                if ((pPortEntry->pAggEntry != NULL) &&
                                    (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
                                    && (pPortEntry->pAggEntry->
                                        u1AggAdminStatus == LA_ADMIN_UP))
                                {
                                    LaHandleEnablePort (pPortEntry);
                                }
                                else
                                {
                                    LaL2IwfLaHLPortOperIndication
                                        (pPortEntry->u2PortIndex, CFA_IF_UP);
                                }
                            }
                            else
                            {
                                if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
                                {
                                    LaHandleEnablePort (pPortEntry);
                                }
                                else
                                {
                                    LaL2IwfLaHLPortOperIndication
                                        (pPortEntry->u2PortIndex, CFA_IF_UP);
                                }
                            }
                            /* Add the port index to D-LAG distributing oper port list */
                            LaGetNextAggEntry (NULL, &pAggEntry);
                            while (pAggEntry != NULL)
                            {
                                OSIX_BITLIST_IS_BIT_SET (pAggEntry->
                                                         DLAGDistributingPortList,
                                                         u2PortIndex,
                                                         LA_PORT_LIST_SIZE,
                                                         bResult);
                                if (bResult == OSIX_TRUE)
                                {
                                    OSIX_BITLIST_SET_BIT (pAggEntry->
                                                          DLAGDistributingOperPortList,
                                                          u2PortIndex,
                                                          LA_PORT_LIST_SIZE);
                                    pAggEntry->
                                        u4DLAGDistributingPortListCount++;
                                }
                                LaGetNextAggEntry (pAggEntry, &pAggEntry);
                            }

                            if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) ||
                                (LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry)
                                 == LA_TRUE))
                            {

                                OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                                         GlobalDLAGDistributingPortList,
                                                         u2PortIndex,
                                                         LA_PORT_LIST_SIZE,
                                                         bResult);

                                if (bResult == OSIX_TRUE)
                                {

                                    OSIX_BITLIST_SET_BIT (LA_DLAG_GLOBAL_INFO.
                                                          GlobalDLAGDistributingOperPortList,
                                                          u2PortIndex,
                                                          LA_PORT_LIST_SIZE)
                                        LA_DLAG_GLOBAL_INFO.
                                        u4GlobalDLAGDistributingPortListCount++;

                                }

                            }
                            /* For Router port over LAG.
                             * To notify the port up status to kernel */
                            CfaGetIfBridgedIfaceStatus ((UINT4) u2PortIndex,
                                                        &u1BridgedIfaceStatus);
                            if (u1BridgedIfaceStatus == CFA_DISABLED)
                            {
                                LaL2IwfLaHLPortOperIndication (u2PortIndex,
                                                               CFA_IF_UP);
                            }

                            /* Port Oper Status change needs to be synced
                             * with the Standby
                             */
                            LaRedSyncUpPortOperStatus
                                (pPortEntry->u2PortIndex,
                                 pPortEntry->u1PortOperStatus);

                        }
#ifndef PNAC_WANTED
                        else
                        {
                            /* When PNAC is not enabled, during bridge port type change
                             * port is internally deleted and created. In LA port is created with
                             * port oper status UP (as the port is already up in CFA).
                             * Hence when port oper status UP indication is send from CFA,
                             * LA needs to send port oper UP indication to upper layer
                             * modules such as STP.
                             * */
                            LaL2IwfLaHLPortOperIndication (u2PortIndex,
                                                           CFA_IF_UP);
                        }
#endif
                    }
                }
                else
                {
                    LaL2IwfLaHLPortOperIndication (u2PortIndex, CFA_IF_UP);
                }

                break;

            case LA_DISABLE_PORT_MSG:
                if (LaGetLaEnableStatus () == LA_ENABLED)
                {
                    if ((i4RetVal = LaGetPortEntry (u2PortIndex, &pPortEntry))
                        == LA_SUCCESS)
                    {
                        if (pPortEntry->u1PortOperStatus != CFA_IF_DOWN)
                        {
                            pPortEntry->u1PortOperStatus = CFA_IF_DOWN;

                            /* Port Oper Status change needs to be synced
                             * with the Standby Node
                             */
                            LaRedSyncUpPortOperStatus
                                (pPortEntry->u2PortIndex,
                                 pPortEntry->u1PortOperStatus);

                            if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
                            {
                                if (pPortEntry->LaLacActorInfo.u2IfKey == 0)
                                {
                                    pPortEntry->LaLacActorInfo.u2IfKey =
                                        pPortEntry->LaLacActorAdminInfo.u2IfKey;

                                    pPortEntry->NttFlag = LA_TRUE;
                                    LaLacTxMachine (pPortEntry,
                                                    LA_TXM_EVENT_NTT);
                                }
                                LaHandleDisablePort (pPortEntry);
                            }
                            else
                            {
                                /* Scan and check if failed port is distribution 
                                 * port for any port channel: If yes then remote
                                 * aggregator entries maintained should be freed. */
                                LaGetNextAggEntry (NULL, &pAggEntry);
                                while (pAggEntry != NULL)
                                {
                                    OSIX_BITLIST_IS_BIT_SET (pAggEntry->
                                                             DLAGDistributingOperPortList,
                                                             u2PortIndex,
                                                             LA_PORT_LIST_SIZE,
                                                             bResult);
                                    if (bResult == OSIX_TRUE)
                                    {
                                        OSIX_BITLIST_RESET_BIT (pAggEntry->
                                                                DLAGDistributingOperPortList,
                                                                u2PortIndex,
                                                                LA_PORT_LIST_SIZE);
                                        pAggEntry->
                                            u4DLAGDistributingPortListCount--;
                                    }

                                    if ((LA_DLAG_SYSTEM_STATUS ==
                                         LA_DLAG_ENABLED)
                                        &&
                                        (LA_MEMCMP
                                         (pAggEntry->
                                          DLAGDistributingOperPortList,
                                          DefaultDLAGPortList,
                                          LA_PORT_LIST_SIZE) == 0))
                                    {
                                        LaDLAGDeleteAllRemoteAggEntries
                                            (pAggEntry);
                                        if (pAggEntry->u1DLAGRedundancy ==
                                            LA_DLAG_REDUNDANCY_ON)
                                        {
                                            LaDLAGTriggerMSSelectionProcess
                                                (pAggEntry,
                                                 MacAddr,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_DIST_INTF_DOWN);
                                        }
                                    }
                                    LaGetNextAggEntry (pAggEntry, &pAggEntry);
                                }

                                if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
                                    ||
                                    (LaIsMCLAGEnabledOnAgg
                                     (pPortEntry->pAggEntry) == LA_TRUE))
                                {

                                    OSIX_BITLIST_IS_BIT_SET
                                        (LA_DLAG_GLOBAL_INFO.
                                         GlobalDLAGDistributingOperPortList,
                                         u2PortIndex, LA_PORT_LIST_SIZE,
                                         bResult);

                                    if (bResult == OSIX_TRUE)
                                    {
                                        OSIX_BITLIST_RESET_BIT
                                            (LA_DLAG_GLOBAL_INFO.
                                             GlobalDLAGDistributingOperPortList,
                                             u2PortIndex,
                                             LA_PORT_LIST_SIZE)
                                            LA_DLAG_GLOBAL_INFO.
                                            u4GlobalDLAGDistributingPortListCount--;

                                    }

                                }

                            }

                            /* Indicate to Higher Layers */
                            LaL2IwfLaHLPortOperIndication
                                (pPortEntry->u2PortIndex, CFA_IF_DOWN);
                        }
                    }
                }
                else
                {
                    /* This is possible, if port oper status message is 
                     * posted to LA, and before LA process it LA module is
                     * shutdown/disabled. Since the message is posted to LA, 
                     * this information has to reach the higher layers. */
                    LaL2IwfLaHLPortOperIndication (u2PortIndex, CFA_IF_DOWN);
                }

                break;
#ifdef ICCH_WANTED
            case LA_ICCH_MSG:
                LaIcchProcessUpdateMsg (pIntfMesg);
                break;
#endif /* ICCH_WANTED */

#ifdef L2RED_WANTED
            case LA_RM_FRAME:
                LaRedHandleRmEvents (pIntfMesg);
                break;
#endif
#ifdef NPAPI_WANTED
            case LA_NP_CALLBACK:
                LaHandleNpCallbackEvents (pIntfMesg);
                break;
#endif
            default:
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "LaHandleCtrlEvents:"
                             "Unknown Control Message (Type %d) received\n",
                             pIntfMesg->u2MesgType);
        }

        /* Now release the buffer to pool */
        LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pIntfMesg);
        LA_UNLOCK ();
    }
}

/*****************************************************************************/
/* Function Name      : LaProcessLacpFrame                                   */
/*                                                                           */
/* Description        : This function is called by LA when it receives a new */
/*                      LACPDU.                                              */
/*                                                                           */
/* Input(s)           : u2PortIndex-interface index of the port.             */
/*                      pBuf - LACP frame to be processed                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaProcessLacpFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex)
{
    tLaMacAddr          LaDestMacAddress;
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2Type;
    UINT1               u1Subtype;
    UINT4               u4ByteCount;
    UINT1               au1LinearPdu[LA_AA_DLAG_PDU_SIZE] = { 0 };
    UINT1              *pu1LinearPdu = NULL;
    UINT1              *pu1Buffer = NULL;
    UINT1               u1TLVType = 0;

    if (LaGetLaEnableStatus () != LA_ENABLED)
    {
        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                         ALL_FAILURE_TRC,
                         "LaHandlePktEnqEvent %d :"
                         "Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);
        }

        return LA_FAILURE;
    }

    if (pBuf == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     ("LaProcessLacpFrame:Port %d : CRU Buffer received - Null Pointer\n"),
                     u2PortIndex);
        return LA_FAILURE;
    }

    LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 "Called LaProcessLacpFrame for Port %d", u2PortIndex);

    /* when ISSU Miantenace Mode is in progress
     * drop all the received PDUs */
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                         ALL_FAILURE_TRC,
                         "LaHandlePktEnqEvent %d :"
                         "Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);
        }

        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "LaProcessLacpFrame:Port %d :Dropping LACP frames"
                     "during ISSU Maintenance Mode \n", u2PortIndex);

        return LA_FAILURE;
    }

    u4ByteCount = LA_GET_CRU_BUF_VALID_BYTE_COUNT (pBuf);
    if (u4ByteCount > LA_MAX_ETH_FRAME_SIZE)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "LaProcessLacpFrame:Port %d :"
                     "Invalid length of Ethernet Frame; Discarding frame...\n",
                     u2PortIndex);

        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                         ALL_FAILURE_TRC,
                         "LaProcessLacpFrame:Port %d :"
                         "Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);
        }
        return LA_FAILURE;
    }

    LA_PKT_DUMP (DUMP_TRC, pBuf, u4ByteCount,
                 "LaProcessLacpFrame:"
                 "Dumping LACP frame received from lower layer...\n");

    /* Storing the address of the Linear Buffer in another pointer */
    pu1LinearPdu = au1LinearPdu;

    /* We compare the Destination Address of the received frame in order to
     * determine if the packet is a data frame or is a LA frame (i.e. LACPDU or
     * Marker PDU). Here, we assume that the Destination Address field located in
     * the first 6 bytes of the frame is located linearly in the CRU buffer */

    if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) LaDestMacAddress, LA_OFFSET,
                              LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                     ALL_FAILURE_TRC,
                     "LaProcessLacpFrame: Port %u "
                     "Received Frame Copying From CRU Buffer Failed\n",
                     u2PortIndex);

        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                         ALL_FAILURE_TRC,
                         "LaProcessLacpFrame: Port %u"
                         " Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);

        }

        return LA_FAILURE;
    }

    if (LaCopyPduBufInfo (pBuf, &pu1LinearPdu, u4ByteCount) != LA_SUCCESS)
    {
        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaProcessLacpFrame: Port %d "
                         "Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);
        }

        return LA_FAILURE;
    }
    /* Store the pointer to the linear buffer in another pointer, which will
     * be used if the received frame happens to be a Marker Pdu (i.e.
     * neither a data frame nor LACPDU) */
    pu1Buffer = pu1LinearPdu;

    u1TLVType = (UINT1) *(pu1Buffer + LA_TLV_TYPE_OFFSET);

    if (u1TLVType != LA_DLAG_DSU_INFO)
    {

        /* Getting the pointer to the Port Entry corresponding to this port */
        if (LaGetPortEntry (u2PortIndex, &pPortEntry) == LA_FAILURE)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "LaProcessLacpFrame:Port %d :"
                         "Frame received on Invalid Port...\n", u2PortIndex);

            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                             ALL_FAILURE_TRC,
                             "LaProcessLacpFrame:Port %d :"
                             "Received frame CRU Buffer Release Failed\n",
                             u2PortIndex);
            }

            return LA_FAILURE;
        }

        if (pPortEntry == NULL)
        {
            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                             ALL_FAILURE_TRC,
                             "LaHandlePktEnqEvent %d :"
                             "Received frame CRU Buffer Release Failed\n",
                             u2PortIndex);
            }
            return LA_FAILURE;
        }

        /* Status of LACP on a particular port */
        if (pPortEntry->LaPortEnabled == LA_PORT_DISABLED)
        {
            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                             ALL_FAILURE_TRC,
                             "LaHandlePktEnqEvent %d :"
                             "Received frame CRU Buffer Release Failed\n",
                             u2PortIndex);
            }

            return LA_FAILURE;
        }

        /* If not D-LAG PDU, then AggIf entry should be present for LACPDU
         * and Marker PDU */
        LA_GET_AGGIFENTRY (pPortEntry, pLaLacAggIfEntry);
        if (pLaLacAggIfEntry == NULL)
        {
            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                             ALL_FAILURE_TRC,
                             "LaHandlePktEnqEvent %d :"
                             "Received frame CRU Buffer Release Failed\n",
                             u2PortIndex);
            }
            return LA_FAILURE;
        }
    }

    pu1Buffer += LA_PROTOCOL_TYPE_OFFSET;
    LA_GET_2BYTE (u2Type, pu1Buffer);

    if (u2Type == LA_SLOW_PROT_TYPE)
    {
        /* The received frame is a Slow Protocol PDU */
        LA_GET_1BYTE (u1Subtype, pu1Buffer);
        pu1Buffer += LA_PDU_VERSION_NUM_SIZE;

        if (u1Subtype == LA_LACP_SUBTYPE)
        {
            /* D-LAG PDU Processing */
            if (u1TLVType == LA_DLAG_DSU_INFO)
            {
                if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
                {

                    LaActiveDLAGProcessDLAGPdu (u2PortIndex, pu1LinearPdu,
                                                u4ByteCount);
                }
#ifdef ICCH_WANTED
                else if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
                {
                    LaActiveMCLAGProcessMCLAGPdu (u2PortIndex, pu1LinearPdu,
                                                  u4ByteCount);
                }
#endif
                else
                {
                    LaDLAGProcessDLAGPdu (u2PortIndex, pu1LinearPdu);

                }
            }
            else
            {
                /* Passing the pointer to the Tlv Type: Actor Information field of
                 * the received frame */
                if (LA_MODE_LACP == pPortEntry->LaLacpMode)
                {
                    LaLacReceivePduInfo (pPortEntry, pu1Buffer);
                }

                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "LaProcessLacpFrame: Port %d: LACPDU Handed Over to Rx Machine\n",
                             u2PortIndex);
            }

            if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                             "LaProcessLacpFrame: Port %d"
                             "Received frame CRU Buffer Release Failed\n",
                             u2PortIndex);

                return LA_FAILURE;
            }
        }                        /* End of LACPDU handling */
        else
        {
            /* Passing the pointer to the Destination Address field */
            if (LaAggParseFrame (pu1LinearPdu, pPortEntry,
                                 pLaLacAggIfEntry) == LA_UNKNOWN)
            {

                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "LaProcessLacpFrame: Port %d "
                             "Received Frame of UNKNOWN Slow Protocol SUBTYPE\n",
                             u2PortIndex);

                if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                                 ALL_FAILURE_TRC,
                                 "LaProcessLacpFrame: Port %d "
                                 "Received frame CRU Buffer Release Failed\n",
                                 u2PortIndex);
                }
                /* L2-ISS This clause has already been handled before enqueing to LA */

                /* Pass the frame to Bridge module (i.e. Passing frames with
                 * unsuppported Slow Protocols to the MAC Client (as defined in
                 * Clause 43B.5 */

                /* Incrementing the number of Unknown PDUs Received */
                (pPortEntry->LaPortStats.u4UnknownRx)++;

                /* Increment the Aggregator if Statistics */
                /* All the statistics are negatively incremented for the
                 * Aggregator, because these frames have already been counted
                 * once, at the port level */
                (pLaLacAggIfEntry->i4AggInUnknownProtos)--;

            }                    /* End of unknown frame processing */
            else
            {

                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "LaProcessLacpFrame: Port %d"
                             "Non-LACPDU handed over to the Aggregator module\n",
                             u2PortIndex);

                if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC | BUFFER_TRC |
                                 ALL_FAILURE_TRC,
                                 "LaProcessLacpFrame: Port %d "
                                 "Received frame CRU Buffer Release Failed\n",
                                 u2PortIndex);

                    return LA_FAILURE;
                }
            }

        }                        /* End of else (non-LACPDU) */

    }                            /* End of Slow Protocol Type frames processing */
    else
    {

        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                     ALL_FAILURE_TRC, "LaProcessLacpFrame: Port %d"
                     ": Received Frame is of UNKNOWN TYPE\n", u2PortIndex);

        if (pPortEntry != NULL)
        {
            /* Increment the number of Unknown PDUs Received */
            (pPortEntry->LaPortStats.u4UnknownRx)++;
        }

        /* Incrementing the Aggregator If statistics */
        /* All the statistics are negatively incremented for the
         * Aggregator, because these frames have already been counted
         * once, at the port level */

        if (pLaLacAggIfEntry)
        {
            (pLaLacAggIfEntry->i4AggInUnknownProtos)--;
        }

        if (LA_RELEASE_CRU_BUF (pBuf, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC |
                         BUFFER_TRC | ALL_FAILURE_TRC,
                         "LaProcessLacpFrame: Port %d"
                         "Received frame CRU Buffer Release Failed\n",
                         u2PortIndex);
        }

        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/* End of File */
