/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : lal2wr.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Aggregator module                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains L2IWF module routines       */
/*                            used in LA.                                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 October 2007/        Initial Create.                        */
/*            L2 Team                                                        */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/
/* Function Name      : LaL2IwfAddActivePortToPortChannel                    */
/*                                                                           */
/* Description        : This function calls the L2IWF module to add a port   */
/*                      a port-channel.                                      */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                      u2AggId - Aggregator Id                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfAddActivePortToPortChannel (UINT2 u2IfIndex, UINT2 u2AggId)
{
    return (L2IwfAddActivePortToPortChannel (u2IfIndex, u2AggId));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfRemoveActivePortFromPortChannel               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to remove a port*/
/*                      from a port-channel.                                 */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                      u2AggId - Aggregator Id                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfRemoveActivePortFromPortChannel (UINT2 u2IfIndex, UINT2 u2AggId)
{
    return (L2IwfRemoveActivePortFromPortChannel (u2IfIndex, u2AggId));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfGetPortChannelForPort                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to remove a port*/
/*                      from a port-channel.                                 */
/*                                                                           */
/* Input(s)           : u2IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : u2AggId - Aggregator Id                              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfGetPortChannelForPort (UINT2 u2IfIndex, UINT2 *pu2AggId)
{
    return (L2IwfGetPortChannelForPort (u2IfIndex, pu2AggId));
}

/*****************************************************************************/
/* Function Name      : LaL2IwfGetBridgeMode                                 */
/*                                                                           */
/* Description        : This function returns the Bridge mode configured     */
/*                      in L2IWF                                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                                                                           */
/* Output(s)          : u4BridgeMode -  Bridge Mode                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode));
}
