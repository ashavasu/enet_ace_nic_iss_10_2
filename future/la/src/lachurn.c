/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : lachurn.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREGATION                               */
/*    MODULE NAME           : Link Aggregation Churn Detection Machine       */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains functions for the Churn     */
/*                            Detection Machine.                             */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/                    Initial Create.              */
/*            Bridge Team                                                    */
/*****************************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaLacUpdateActorChurnState.                          */
/*                                                                           */
/* Description        : This function is called from LaChurnDetection()to    */
/*                      update the Actor's Churn Detection machine           */
/*                                                                           */
/* Input(s)           : Pointer to the Port Entry.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacUpdateActorChurnState (tLaLacPortEntry * pPortEntry)
{
    if (pPortEntry->LaLagIdMatched == LA_TRUE)
    {
        /* In Sync */
        if (pPortEntry->LaActorChurnState == ACTOR_CHURN_MONITOR)
        {
            /* Stop the Churn Timer */
            pPortEntry->ActorChurnTmr = 0;
        }

        /* The Actor enters into NO_ACTOR_CHURN state whenever IN_SYNC */
        pPortEntry->LaActorChurnState = NO_ACTOR_CHURN;

    }
    else
    {                            /* Out of Sync */

        if (pPortEntry->LaActorChurnState == ACTOR_CHURN_MONITOR)
        {

            if (LaLacCheckTimerExpiry (&(pPortEntry->ActorChurnTmr)))
            {

                pPortEntry->LaActorChurnState = ACTOR_CHURN;
                /* update actor churn count */
                ++(pPortEntry->LaPortDebug.u4ActorChangeCount);
            }

        }
        else if (pPortEntry->LaActorChurnState == NO_ACTOR_CHURN)
        {

            /* Start the Churn Monitor Timer */
            pPortEntry->ActorChurnTmr = LA_CHURN_DETECTION_TICKS;
            pPortEntry->LaActorChurnState = ACTOR_CHURN_MONITOR;
        }
    }
}

/*****************************************************************************/
/* Function Name      : LaLacUpdatePartnerChurnState.                        */
/*                                                                           */
/* Description        : This function is called from LaChurnDetection()to    */
/*                      update the Partner's Churn Detection machine         */
/*                                                                           */
/* Input(s)           : Pointer to the Port Entry.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacUpdatePartnerChurnState (tLaLacPortEntry * pPortEntry)
{

    if (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization)
    {
        /* In Sync */
        if (pPortEntry->LaPartnerChurnState == PARTNER_CHURN_MONITOR)
        {
            /* Stop the Churn Timer */
            pPortEntry->PartnerChurnTmr = 0;
        }

        /* The Partner enters into NO_ACTOR_CHURN state whenever IN_SYNC */
        pPortEntry->LaPartnerChurnState = NO_PARTNER_CHURN;

    }
    else
    {                            /* Out of Sync */

        if (pPortEntry->LaPartnerChurnState == PARTNER_CHURN_MONITOR)
        {

            if (LaLacCheckTimerExpiry (&(pPortEntry->PartnerChurnTmr)))
            {

                pPortEntry->LaPartnerChurnState = PARTNER_CHURN;
                /* update partner churn count */
                ++(pPortEntry->LaPortDebug.u4PartnerChangeCount);
            }

        }
        else if (pPortEntry->LaPartnerChurnState == NO_CHURN)
        {

            /* Start the Churn Monitor Timer */
            pPortEntry->PartnerChurnTmr = LA_CHURN_DETECTION_TICKS;
            pPortEntry->LaPartnerChurnState = PARTNER_CHURN_MONITOR;
        }
    }
}

/*****************************************************************************/
/* Function Name      : LaLacUpdatePartnerChurnState.                        */
/*                                                                           */
/* Description        : This function implements the churn detection machine */
/*                      which is common for both the Actor and the Partner.  */
/*                      This state machine function must be called from      */
/*                      LaLacTickTmrExpiry() only after                      */
/*                      LaLacMuxControlMachine() has been called which will  */
/*                      update the  Synchronization bit                      */
/*                                                                           */
/* Input(s)           : Pointer to the Port Entry, Event                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL port pointer  */
/*****************************************************************************/
INT4
LaLacChurnDetection (tLaLacPortEntry * pPortEntry, tLaLacEvent LaLacEvent)
{
    if (pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "CHURN: Port Entry Pointer NULL\n");
        return LA_ERR_NULL_PTR;
    }

    switch (LaLacEvent)
    {

        case LA_CHURN_EVENT_PORT_ENABLED:

            /* Start the Churn Monitor Timer */
            pPortEntry->ActorChurnTmr = LA_CHURN_DETECTION_TICKS;
            pPortEntry->LaActorChurnState = ACTOR_CHURN_MONITOR;
            pPortEntry->LaPartnerChurnState = PARTNER_CHURN_MONITOR;
            break;

        case LA_CHURN_EVENT_PORT_DISABLED:

            pPortEntry->ActorChurnTmr = 0;
            break;

        case LA_CHURN_EVENT_TICK:

            LaLacUpdateActorChurnState (pPortEntry);
            LaLacUpdatePartnerChurnState (pPortEntry);
            break;

        default:
            break;
    }
    return LA_SUCCESS;
}

/* end of file */
