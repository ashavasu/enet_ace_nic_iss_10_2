/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: lared.c,v 1.92 2017/11/08 13:19:13 siva Exp $
* Description: Protocol Low Level Routines
*********************************************************************/

#include "lahdrs.h"

#ifdef NPAPI_WANTED
#include "larednp.h"
#endif
#ifdef CLI_WANTED
#include "cli.h"
#include "lacli.h"
#endif
static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);
/*****************************************************************************/
/* Function Name      : LaRedSyncUpPartnerInfo                               */
/*                                                                           */
/* Description        : This function:                                       */
/*                           - gets the information to be synced up for the  */
/*                             given port                                    */
/*                           - constructs the RM message and                 */
/*                           - sends it to Redundancy Manager.               */
/*                                                                           */
/* Input(s)           : pPort          - Pointer to the port entry.          */
/*                      pLaLacpduInfo  - ParnterInfo to be synced up.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpPartnerInfo (tLaLacPortEntry * pPortEntry,
                        tLaLacpduInfo * pLaLacpduInfo)
{
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    tRmMsg             *pMsg;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT4               u4SyncMsgType;

    if ((LA_IS_STANDBY_UP () == LA_FALSE) || (RmGetNodeState () != RM_ACTIVE))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE +
        LA_SYNC_PORT_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    /* 
     * This is a Sync message, and the size of the information to be sent for
     * a port is constant and is given by LA_SYNC_PORT_MSG_SIZE. In this case
     * information for one port needs to be sent and hence the size if 
     * LA_SYNC_PORT_MSG_SIZE.
     */
    u2SyncMsgLen = LA_SYNC_PORT_MSG_SIZE;

    /* Fill the message type. */
    u4SyncMsgType = LA_LACP_SYNC_MSG;
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4SyncMsgType);

    /* Fill the number of size of port information to be synced up. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* 
     * Copy the Port's (ID, ActorKey) and the partner informatin from the 
     * received pdu into the RM buf.
     */
    LaRedFormSyncMsgForPort (pPortEntry, &(pLaLacpduInfo->LaLacActorInfo),
                             &u4Offset, &pMsg);

    LaRedCopyPartnerInfoIntoRMBuf (&(pPortEntry->LaLacActorInfo), &u4Offset,
                                   &pMsg);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaRedSyncUpPortOperStatus                            */
/*                                                                           */
/* Description        : This function sends the port oper status to the      */
/*                      Standby.                                             */
/*                                                                           */
/* Input(s)           : u2PortIndex   - Port Id.                             */
/*                      u1PortOperStatus - Port Oper Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpPortOperStatus (UINT2 u2PortIndex, UINT1 u1PortOperStatus)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT4               u4PortIndex = (UINT4) u2PortIndex;

    if (RmGetNodeState () != RM_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        return LA_SUCCESS;
    }

    if ((LA_IS_STANDBY_UP () == LA_FALSE) || (RmGetNodeState () != RM_ACTIVE))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE +
        LA_PORT_OPER_STATUS_MSG_SIZE;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    u2SyncMsgLen = LA_PORT_OPER_STATUS_MSG_SIZE;

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_PORT_OPER_STATUS_MSG);
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the port information to be synced up. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);
    LA_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PortOperStatus);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncUpWaitWhileTmrExpiry                        */
/*                                                                           */
/* Description        : This function sends the wait while timer's expiry    */
/*                      message to LA in standby node.                       */
/*                                                                           */
/* Input(s)           : u2PortIndex   - Port Id.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpWaitWhileTmrExpiry (UINT2 u2PortIndex)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT4               u4PortIndex = (UINT4) u2PortIndex;

    if ((LA_IS_STANDBY_UP () == LA_FALSE) || (RmGetNodeState () != RM_ACTIVE))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE +
        LA_WAIT_WHILE_TMR_EXP_MSG_SIZE;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    u2SyncMsgLen = LA_WAIT_WHILE_TMR_EXP_MSG_SIZE;

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_WAIT_WHILE_TMR_EXP_MSG);
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the port information to be synced up. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncUpCurrWhileTmrExpiry                        */
/*                                                                           */
/* Description        : This function sends the current while timer's split  */
/*                      interval timer expiry message to LA in standby node. */
/*                                                                           */
/* Input(s)           : u2PortIndex   - Port Id.                             */
/*                      u1TimerCount  - Number of split intervals expired.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpCurrWhileTmrExpiry (UINT2 u2PortIndex, UINT1 u1TimerCount)
{
    tRmMsg             *pMsg;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT4               u4PortIndex = (UINT4) u2PortIndex;

    if (LA_IS_STANDBY_UP () == LA_FALSE)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE +
        LA_CURR_SPLIT_TMR_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    u2SyncMsgLen = LA_CURR_SPLIT_TMR_MSG_SIZE;

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_CURR_SPLIT_TMR_EXP_MSG);
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Fill the number of number of port information to be synced up. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);
    LA_RM_PUT_1_BYTE (pMsg, &u4Offset, u1TimerCount);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncProtocolDisable                             */
/*                                                                           */
/* Description        : This function will be invoked whenever the module is */
/*                      is disabled. This function will send clean syncup    */
/*                      entries message to standby node, so that the dynamic */
/*                      informationed synced up will be removed by the       */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncProtocolDisable (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2BufSize;

    if (RmGetNodeState () != RM_ACTIVE)
    {
        /*Only Active node will send the SyncUp message */
        return LA_SUCCESS;
    }

    if (LA_IS_STANDBY_UP () == LA_FALSE)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_CLEAN_SYNCUP_ENTRIES_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, LA_CLEAN_SYNCUP_ENTRIES_MSG);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSendMsgToRm                                      */
/*                                                                           */
/* Description        : This function constructs the RM message from the     */
/*                      given linear buf and sends it to Redundancy Manager. */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if sync msg is sent then LA_SUCCESS                  */
/*                      Otherwise LA_FAILURE                                 */
/*****************************************************************************/
INT4
LaRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{

    /* Call the API provided by RM to send the data to RM */
    if (LaRmEnqMsgToRmFromAppl (pMsg, u2BufSize, RM_LA_APP_ID, RM_LA_APP_ID)
        == RM_FAILURE)
    {
        LA_TRC (LA_TRC_FLAG, "Enq to RM from appl failed\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);

        return LA_FAILURE;
    }

    return (LA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : LaRedTrigHigherLayer                                 */
/*                                                                           */
/* Description        : This function will send the given event to the next  */
/*                      higher layer (can be STP or VLAN).                   */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type. L2_INITIATE_BULK_UPDATES  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedTrigHigherLayer (UINT1 u1TrigType)
{
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
    LaAstHandleUpdateEvents (u1TrigType, NULL, 0);
#else
#ifdef VLAN_WANTED
    LaVlanRedHandleUpdateEvents (u1TrigType, NULL, 0);
#else
#ifdef LLDP_WANTED
    LaLldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    if ((LA_NODE_STATUS () == LA_NODE_ACTIVE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))
    {
        /* Send an event to RM to notify that sync up is completed */
        if (RmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            LA_TRC (LA_RED_TRC, "LA send event L2_SYNC_UP_COMPLETED to "
                    "RM failed\n");
        }
    }
#endif /* LLDP_WANTED */
#endif
#endif
}

/*****************************************************************************/
/* Function Name      : LaRedHandleRmEvents                                  */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pLaMsg - Pointer to the  input buffer.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleRmEvents (tLaIntfMesg * pLaMsg)
{
    tRmNodeInfo        *pData = NULL;
    tLaNodeStatus       LaPrevNodeState = LA_NODE_IDLE;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pLaMsg->uLaIntfMsg.RmFrame.u1Event)
    {
        case GO_ACTIVE:

            if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
            {
                break;
            }

            LaPrevNodeState = LA_NODE_STATUS ();
            LaRedMakeNodeActive ();
            LA_TRC (LA_RED_TRC, "Receieved GO_ACTIVE event from RM\n");
            if (LA_BULK_REQ_RECD () == LA_TRUE)
            {
                LA_BULK_REQ_RECD () = LA_FALSE;
                gLaRedGlobalInfo.u2BulkUpdNextPort = LA_MIN_PORTS;
                LaRedHandleBulkUpdateEvent ();
            }
            if (LaPrevNodeState == LA_NODE_IDLE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }
            LaRmHandleProtocolEvent (&ProtoEvt);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            LA_TRC (LA_RED_TRC, "Receieved RM_CONFIG_RESTORE_COMPLETE"
                    " event from RM\n");
            if (LA_NODE_STATUS () == LA_NODE_IDLE)
            {
                if (LaRmGetNodeState () == RM_STANDBY)
                {
                    LA_NODE_STATUS () = LA_NODE_STANDBY;
                    LaRedMakeNodeStandbyFromIdle ();
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                LaRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case GO_STANDBY:
            LA_TRC (LA_RED_TRC, "Receieved GO_STANDBY event from RM\n");
            if (LA_NODE_STATUS () == LA_NODE_STANDBY)
            {
                break;
            }
            if (LA_NODE_STATUS () == LA_NODE_IDLE)
            {
                return;
            }
            else if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
            {
                LaRedHandleGoStandbyEvent ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                LaRmHandleProtocolEvent (&ProtoEvt);
            }
            LA_BULK_REQ_RECD () = LA_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
#ifdef NPAPI_WANTED
            if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
            {
                LaRedPerformAudit ();
            }
#endif
            break;

        case RM_STANDBY_UP:
            LA_TRC (LA_RED_TRC, "Receieved RM_STANDBY_UP event from " "RM\n");
            pData = (tRmNodeInfo *) pLaMsg->uLaIntfMsg.RmFrame.pFrame;
            LA_NUM_STANDBY_NODES () = pData->u1NumStandby;
            LaRmReleaseMemoryForMsg ((UINT1 *) pData);
            LaRedHandleStandByUpEvent (NULL);
            break;

        case RM_STANDBY_DOWN:
            LA_TRC (LA_RED_TRC, "Receieved RM_STANDBY_DOWN event from RM\n");
            pData = (tRmNodeInfo *) pLaMsg->uLaIntfMsg.RmFrame.pFrame;
            LA_NUM_STANDBY_NODES () = pData->u1NumStandby;
            LaRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_COMPLETE_SYNC_UP:
#if !defined (CFA_WANTED) && !defined (EOAM_WANTED) && !defined (PNAC_WANTED)
            LaRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
#endif
            break;

        case L2_COMPLETE_SYNC_UP:
            LaRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        case L2_INITIATE_BULK_UPDATES:
            LA_TRC (LA_RED_TRC, "Receieved L2_INITIATE_BULK_UPDATES event "
                    "from PNAC\n");
            LaRedSendBulkReq ();
            break;

        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM ((tRmMsg *) (pLaMsg->uLaIntfMsg.RmFrame.pFrame),
                               &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR ((tRmMsg *) (pLaMsg->uLaIntfMsg.RmFrame.pFrame),
                                 pLaMsg->uLaIntfMsg.RmFrame.u2Length);

            ProtoAck.u4AppId = RM_LA_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            LA_TRC (LA_RED_TRC, "Receieved RM_MESSAGE event from RM\n");
            LaRedHandleSyncUpMessage
                ((tRmMsg *) pLaMsg->uLaIntfMsg.RmFrame.pFrame,
                 pLaMsg->uLaIntfMsg.RmFrame.u2Length);

            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (pLaMsg->uLaIntfMsg.RmFrame.pFrame));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            LaRedHandleDynSyncAudit ();
            break;

        default:
            LA_TRC (LA_RED_TRC, "Receieved Unknown event from RM\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedMakeNodeStandbyFromIdle.                        */
/*                                                                           */
/* Description        : This function makes the node Standby from Idle state */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaRedMakeNodeStandbyFromIdle (VOID)
{
    if (LA_SYSTEM_CONTROL != LA_START)
    {
        LA_INIT_NUM_STANDBY_NODES ();
        LA_TRC (LA_RED_TRC, "LA Node made standby and La is not started.\n");
        return LA_SUCCESS;
    }

    LA_INIT_NUM_STANDBY_NODES ();

    /*
     * Note: Even when the node is in Idle state, configurations
     * can be made. Framework has to take care of it.
     * Hence check the protocol admin status and enable/disable 
     * the LA module accordingly.
     */
    if (LA_PROTOCOL_ADMIN_STATUS () == LA_ENABLED)
    {
        /* According to configurations LA module is enabled,
         * hence enable LA here. */
        LaEnable ();
    }

    LA_TRC (LA_RED_TRC, "LA Node made STANDBY from IDLE state.\n");
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedMakeNodeActive.                                 */
/*                                                                           */
/* Description        : This function brings up the standby card to the      */
/*                      same state as the ACTIVE card. It will first call    */
/*                      LaEnable to bring up LA module.                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaRedMakeNodeActive (VOID)
{
    tLaRedSyncedUpInfo *pSyncUpEntry = NULL;
    UINT2               u2Port;

    if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
    {
        return LA_SUCCESS;
    }

    if (LA_SYSTEM_CONTROL != LA_START)
    {
        LA_NODE_STATUS () = LA_NODE_ACTIVE;
        LA_RM_GET_NUM_STANDBY_NODES_UP ();
        LA_TRC (LA_RED_TRC, "LA Node made active and La is not started.\n");
        return LA_SUCCESS;
    }

    if (LA_NODE_STATUS () == LA_NODE_IDLE)
    {
        /* Make the node as active. */
        LA_NODE_STATUS () = LA_NODE_ACTIVE;
        LA_RM_GET_NUM_STANDBY_NODES_UP ();

        /* Node is coming up first time. Hence initialize the
         * hardware. */
#ifdef NPAPI_WANTED
        if (FNP_FAILURE == LaFsLaHwInit ())
        {
            LA_TRC (INIT_SHUT_TRC, "FsLaHwInit() FAILED\n");
            return LA_FAILURE;
        }
#endif

        /* Note: Even when the node is in Idle state, configurations
         * can be made. Framework has to take care of it.
         * Hence check the protocol admin status and enable/disable 
         * the LA module accordingly. */
        if (LA_PROTOCOL_ADMIN_STATUS () == LA_ENABLED)
        {
            /* According to configurations LA module is enabled,
             * hence enable LA here. */
            LaEnable ();
        }

        LA_TRC (LA_RED_TRC, "LA Node made ACTIVE from IDLE state.\n");
        return LA_SUCCESS;
    }

    if (LA_NODE_STATUS () == LA_NODE_STANDBY)
    {
        LA_NODE_STATUS () = LA_NODE_ACTIVE;
        LA_RM_GET_NUM_STANDBY_NODES_UP ();

        if (LA_PROTOCOL_ADMIN_STATUS () == LA_ENABLED)
        {
            /* Call the D-LAG init function when the standby node
             *  becomes active */
            LaDLAGScanLaListAndInitDLAG ();
        }
        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            /*
             * Send LACPDU from all ports on which LACP is enabled
             * to prevent partner timeout
             */
            LaRedSendPduOnAllPort ();
            /*
             * Start the protocol timers here
             */
            LaRedStartTimers ();
        }
#ifdef NPAPI_WANTED
        /* Since we have not synchoronized LA NPAPI shadow table, we are 
         * populating it using the information available in LA s/w.
         */
        if (IssGetStackingModel () != ISS_CTRL_PLANE_STACKING_MODEL)
        {
            if (LaRedUpdateNpapiDB () == LA_FAILURE)
            {
                LA_TRC (LA_RED_TRC | ALL_FAILURE_TRC, "Not able to update LA"
                        " NPAPI Database during switchover from "
                        "Standby-->Active\n");
                return LA_FAILURE;
            }
        }
#endif
        for (u2Port = LA_MIN_PORTS; u2Port <= LA_MAX_PORTS; u2Port++)
        {
            /* Remove the stored information from the SyncUpTable. */
            pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2Port);

            /* Store the AggId as zero. */
            pSyncUpEntry->ReceivedTime = 0;
            pSyncUpEntry->u1TimerCount = 0;
            pSyncUpEntry->u2PortActorKey = 0;
        }
        LA_TRC (LA_RED_TRC, "LA Node made ACTIVE from Standby state.\n");
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedRcvPktFromRm                                    */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the LA  */
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event sent then LA_SUCCESS    */
/*                      Otherwise LA_FAILURE                                 */
/*****************************************************************************/
INT4
LaRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tLaIntfMesg        *pBuf = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to LA task. */
        return RM_FAILURE;
    }

    if (u1Event == RM_FILE_TRANSFER_COMPLETE)
    {
        return RM_FAILURE;
    }

    if ((LA_INITIALIZED () == LA_FALSE) || (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        /* LA is not initialized and hence the mempool for interface
         * messages and the queue will not be present. Hence return
         * from this place. */
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            LaRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    if ((pBuf = (tLaIntfMesg *) LA_ALLOC_MEM_BLOCK (LA_INTFMESG_POOL_ID))
        == NULL)
    {
        LA_TRC_ARG1 (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "LaRedRcvPktFromRm: Ctrl Mesg ALLOC_MEM_BLOCK "
                     "FAILED for event\n", u1Event);
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            LaRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;

    }

    LA_MEMSET (pBuf, 0, sizeof (tLaCtrlMesg));

    pBuf->u2MesgType = LA_RM_FRAME;

    pBuf->uLaIntfMsg.RmFrame.u1Event = u1Event;
    pBuf->uLaIntfMsg.RmFrame.pFrame = pData;
    pBuf->uLaIntfMsg.RmFrame.u2Length = u2DataLen;

    if (LA_ENQUEUE_MESSAGE (LA_INTFQ_ID, pBuf) != LA_OSIX_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "LaRcvPktFromRm:"
                " RM Message enqueue FAILED\n");

        if (LA_FREE_MEM_BLOCK (LA_INTFMESG_POOL_ID, (UINT1 *) pBuf)
            != LA_MEM_SUCCESS)
        {

            LA_TRC (INIT_SHUT_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "LaRedRcvPktFromRm: LA_FREE_MEM_BLOCK FAILED\n");
        }

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            LaRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;

    }

    /* Sent the event to LA CTRL Task. */
    LA_SEND_EVENT (LA_TASK_ID, LA_CONTROL_EVENT);

    return RM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaRegisterWithRM                                     */
/*                                                                           */
/* Description        : Registers LA with RM by providing an application     */
/*                      ID for LA and a call back function to be called      */
/*                      whenever RM needs to send an event to LA.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then LA_SUCCESS           */
/*                      Otherwise LA_FAILURE                                 */
/*****************************************************************************/
INT4
LaRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_LA_APP_ID;
    RmRegParams.pFnRcvPkt = LaRedRcvPktFromRm;
    if (LaRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        LA_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                "LaRegisterWithRM: Registration with RM FAILED\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaDeRegisterWithRM                                   */
/*                                                                           */
/* Description        : Deregisters LA with RM                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then LA_SUCCESS         */
/*                      Otherwise LA_FAILURE                                 */
/*****************************************************************************/
INT4
LaRedDeRegisterWithRM (VOID)
{
    if (LaRmDeRegisterProtocols () == RM_FAILURE)
    {
        LA_TRC (INIT_SHUT_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                "LaRedDeRegisterWithRM: Registration with RM FAILED\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaFormSyncMsgForPort                                 */
/*                                                                           */
/* Description        : This function fills the information to be synced for */
/*                      a given port.                                        */
/*                                                                           */
/* Input(s)           : pPortEntry    - Pointer to the PortEntry             */
/*                      pPartnerInfo  - Pointer PartnerInfomation to be      */
/*                                      synced up.                           */
/*                      pu4Offset     - Offset in the give buffer from where */
/*                                      information needs to be filled.      */
/*                      ppMsg         - Pointer to the buffer where the      */
/*                                      information needs to be filled.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaRedFormSyncMsgForPort (tLaLacPortEntry * pPortEntry,
                         tLaLacIfInfo * pPartnerInfo,
                         UINT4 *pu4Offset, tRmMsg ** ppMsg)
{

    UINT4               u4PortIndex;
    UINT2               u2AggKey;

    /* Get the AggKey. */
    u2AggKey = LA_GET_AGGID_FROM_PORTENTRY (pPortEntry);

    u4PortIndex = (UINT4) (pPortEntry->u2PortIndex);

    /* Store the IfIndex of the port, Actor Key in the given buffer. */
    LA_RM_PUT_4_BYTE (*ppMsg, pu4Offset, u4PortIndex);
    LA_RM_PUT_2_BYTE (*ppMsg, pu4Offset, u2AggKey);

    /* Copy the Partner Info from the received LACPDU in the buffer. */
    LaRedCopyPartnerInfoIntoRMBuf (pPartnerInfo, pu4Offset, (ppMsg));

    LA_RM_PUT_1_BYTE (*ppMsg, pu4Offset, (pPortEntry->u1TimerCount));
}

/*****************************************************************************/
/* Function Name      : LaRedCopyPartnerInfoIntoRMBuf.                        */
/*                                                                           */
/* Description        : This function is called from LaFormSyncMsgForPort()  */
/*                      to copy the Actor and Partner information into the   */
/*                      linear buffer.                                       */
/*                                                                           */
/* Input(s)           : Pointer to the If Info structure, Pointer to the     */
/*                      RM buffer.                                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedCopyPartnerInfoIntoRMBuf (tLaLacIfInfo * pLaLacIfInfo,
                               UINT4 *pu4Offset, tRmMsg ** ppRmBuffer)
{
    UINT1               u1Val = 0;
    UINT2               u2Val = 0;

    if ((pLaLacIfInfo == NULL) || (ppRmBuffer == NULL))
    {
        return LA_FAILURE;
    }

    u2Val = pLaLacIfInfo->LaSystem.u2SystemPriority;
    LA_RM_PUT_2_BYTE (*ppRmBuffer, pu4Offset, u2Val);

    LA_RM_PUT_N_BYTE (*ppRmBuffer, (pLaLacIfInfo->LaSystem.SystemMacAddr),
                      pu4Offset, LA_MAC_ADDRESS_SIZE);

    u2Val = pLaLacIfInfo->u2IfKey;
    LA_RM_PUT_2_BYTE (*ppRmBuffer, pu4Offset, u2Val);

    u2Val = pLaLacIfInfo->u2IfPriority;
    LA_RM_PUT_2_BYTE (*ppRmBuffer, pu4Offset, u2Val);

    u2Val = pLaLacIfInfo->u2IfIndex;
    LA_RM_PUT_2_BYTE (*ppRmBuffer, pu4Offset, u2Val);

    /* Fill the Port State Bit */
    u1Val = 0;

    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaExpired));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaDefaulted));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaDistributing));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaCollecting));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaSynchronization));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaAggregation));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaLacpTimeout));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaLacpActivity));

    LA_RM_PUT_1_BYTE (*ppRmBuffer, pu4Offset, u1Val);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedStoreSyncMsgforPort                             */
/*                                                                           */
/* Description        : This function validates the Port sync information    */
/*                      fron the given buffer and stores it in the           */
/*                      corresponding entry of SyncUpTable.                  */
/*                                                                           */
/* Input(s)           : u2Port           - port Identifier.                  */
/*                      pLaPortSyncInfo  - Pointer to the memory containing  */
/*                                         dynamic port information.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.SyncUpTable.                        */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedStoreSyncMsgforPort (UINT2 u2Port, tLaRedSyncedUpInfo * pLaPortSyncInfo)
{
    tLaRedSyncedUpInfo *pSyncUpEntry = NULL;

    if ((u2Port == 0) || (u2Port > LA_MAX_PORTS))
        return;
    /* Get the entry for this port in LaSyncUpTable. */
    pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2Port);

    pSyncUpEntry->u2PortActorKey = pLaPortSyncInfo->u2PortActorKey;
    pSyncUpEntry->u1TimerCount = pLaPortSyncInfo->u1TimerCount;
    LA_GET_SYS_TIME (&(pSyncUpEntry->ReceivedTime));

    return;
}

/*****************************************************************************/
/* Function Name      : LaRedLacStandbyConstruct.                            */
/*                                                                           */
/* Description        : This function will construct the data structs        */
/*                      from the synced up data and it will call the RXM     */
/*                      as if a PDU was received from the connected partner. */
/*                                                                           */
/* Input(s)           : pPort          - Pointer to the port entry           */
/*                      pLaLacpduInfo  - Pointer to LACPDU info              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaRedLacStandbyConstruct (tLaLacPortEntry * pPort,
                          tLaLacpduInfo * pLaLacpduInfo)
{
    tLaSysTime          CurrentTime;

    /*
     * Need to mimic the reception of LACPDU from partner.  Actor info in
     * the LACPDU (from partner) should be taken from synced up partner info.
     * Partner info in the LACPDU (from partner) should be taken from port's 
     * actor info.
     */

    LA_MEMCPY (&(pLaLacpduInfo->LaLacPartnerInfo), &(pPort->LaLacActorInfo),
               sizeof (tLaLacIfInfo));

    /* Collector max delay is not filled  now. Should we fill zero or 
     * should we sync up this from ACTIVE card ? PENDING. */

    /* debug info - update the system time to mimic as if the pdu is 
     * received now.*/
    LA_GET_SYS_TIME (&CurrentTime);
    pPort->LaPortDebug.LaLastRxTime = CurrentTime;

    /* Call the Rx Machine with LAC_RECEIVED event */
    LaLacRxMachine (pPort, LA_RXM_EVENT_PDU_RXD, pLaLacpduInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedStartTimers                                     */
/*                                                                           */
/* Description        : This function starts the timers when the Standby node*/
/*                      becomes Active                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedStartTimers ()
{
    tLaLacPortEntry    *pPortEntry;
    tLaRedSyncedUpInfo *pSyncUpEntry;
    UINT2               u2PortIndex;

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        LaGetPortEntry (u2PortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            if (LaStartTimer (&(pPortEntry->LaTickTimer)) == LA_FAILURE)
            {
                return;
            }
        }
        /* Assume LaEnable had checked the PNAC status and updated the
         * u1OperStatus.
         */

        if ((LA_OPER_DOWN == pPortEntry->u1PortOperStatus) ||
            (pPortEntry->LaLacpMode != LA_MODE_LACP) ||
            (pPortEntry->LaLacpEnabled != LA_LACP_ENABLED))
        {
            continue;
        }

        /* Get the Entry for this port in the SyncUpTable. */
        pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2PortIndex);

        if (((pPortEntry->LaRxmState == LA_RXM_STATE_CURRENT) ||
             (pPortEntry->LaRxmState == LA_RXM_STATE_EXPIRED)) &&
            (pSyncUpEntry->u1TimerCount != 0))
        {
            LaRedReStartCurrTimer (pPortEntry, pSyncUpEntry->ReceivedTime,
                                   pSyncUpEntry->u1TimerCount);
        }
        else
        {
            /* if Active crashes/ goes down, immediately after the standby comes
             * up, means, active didnt sene even a single syncup for curr timer,
             * then when standby becomes active since pSyncUpEntry->u1TimerCount
             * is zero it wont start the curr timer,so port wont come out of the
             * group
             * To take care this condition, RX machine is invoked with 
             * CURRENT_WHILE_EXP message */

            if (((pPortEntry->LaRxmState == LA_RXM_STATE_CURRENT) ||
                 (pPortEntry->LaRxmState == LA_RXM_STATE_EXPIRED)) &&
                (pSyncUpEntry->u1TimerCount == 0))
            {

                /* since the Curr timer exp is not yet received, lets set the
                 * receivedTime to 0 */
                pSyncUpEntry->ReceivedTime = 0;
                LaRedReStartCurrTimer (pPortEntry, pSyncUpEntry->ReceivedTime,
                                       pSyncUpEntry->u1TimerCount);
            }
        }
        if ((pPortEntry->LaMuxState == LA_MUX_STATE_WAITING))
        {
            if (LaStartTimer (&pPortEntry->WaitWhileTmr) == LA_FAILURE)
            {
                return;
            }
        }

        if (LaStartTimer (&(pPortEntry->PeriodicTmr)) == LA_FAILURE)
        {
            return;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaRedReStartCurrTimer.                               */
/*                                                                           */
/* Description        : This function adjusts the current split interval     */
/*                      timer based on the dynamic information synced up     */
/*                      time, the current time and the u1TimerCount value    */
/*                      synced up for this port.                             */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the port entry.              */
/*                      SyncedTime - Dynamic info sync up time for this port.*/
/*                      u1TimerCount - Split intervals expired count synced  */
/*                                     up for this port.                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedReStartCurrTimer (tLaLacPortEntry * pPortEntry, tLaSysTime SyncedTime,
                       UINT1 u1TimerCount)
{
    tLaSysTime          CurrentTime;
    UINT4               u4DiffTime;
    UINT4               u4LeftOutDuration;

    LA_GET_SYS_TIME (&CurrentTime);

    u4DiffTime = (CurrentTime - SyncedTime) / SYS_TIME_TICKS_IN_A_SEC;

    if (u1TimerCount <= 2)
    {
        pPortEntry->u1TimerCount = u1TimerCount;
        if (LaStopTimer (&pPortEntry->CurrentWhileTmr) == LA_FAILURE)
        {
            return;
        }

        if (u4DiffTime >= pPortEntry->u4CurrentWhileTime)
        {
            u4LeftOutDuration = 0;
        }
        else
        {
            u4LeftOutDuration = (pPortEntry->u4CurrentWhileTime - u4DiffTime);
        }
        if (u4LeftOutDuration == 0)
        {
            /* This split interval is over. */
            if (u1TimerCount == 2)
            {
                /* Means Last split interval has expired. Hence give current
                 * while timer expiry indication to the Receive state 
                 * machine. */
                LaLacRxMachine (pPortEntry,
                                LA_RXM_EVENT_CURRENT_WHILE_EXP, NULL);
                pPortEntry->u1TimerCount = 0;
            }
            else
            {
                /* u1TimerCount < 2, one split interval expiry sync up message
                 * is lost during switch over.*/
                pPortEntry->u1TimerCount = (UINT1) (u1TimerCount + 1);
                if (LA_START_TMR
                    (LA_TIMER_LIST_ID,
                     &pPortEntry->CurrentWhileTmr.LaAppTimer,
                     (pPortEntry->u4CurrentWhileTime * SYS_TIME_TICKS_IN_A_SEC))
                    != LA_TMR_SUCCESS)
                {
                    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "TMR: Starting Timer FAILED\n");
                    return;
                }
                LA_TRC (CONTROL_PLANE_TRC, "TMR: Started Timer \n");
                (pPortEntry->CurrentWhileTmr).LaTmrFlag = LA_TMR_RUNNING;
            }
        }
        else
        {
            /* Start the split interval for the left out duration. */

            if (LA_START_TMR (LA_TIMER_LIST_ID,
                              &pPortEntry->CurrentWhileTmr.LaAppTimer,
                              (u4LeftOutDuration * SYS_TIME_TICKS_IN_A_SEC))
                != LA_TMR_SUCCESS)
            {
                LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "TMR: Starting Timer FAILED\n");
                return;
            }
            LA_TRC (CONTROL_PLANE_TRC, "TMR: Started Timer \n");
            (pPortEntry->CurrentWhileTmr).LaTmrFlag = LA_TMR_RUNNING;
        }
    }

}

/*****************************************************************************/
/* Function Name      : LaRedHandleSyncUpMessage.                            */
/*                                                                           */
/* Description        : This function stores the information present in the  */
/*                      message in LaSyncUpTable.                            */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      u2Length - Length of buffer.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleSyncUpMessage (tRmMsg * pMesg, UINT2 u2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4RedMsgType = 0;
    UINT4               u4Offset = 0;
    UINT4               u2MesgSize = 0;
    INT4                i4RetVal;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* No need to do null pointer check for pMesg as it is done
     * in LaRedRcvPktFromRm. */
    if (u2Length < (LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE))
    {
        /* Message size is very less to process. */
        return;
    }

    /* Store length of buffer in u2MesgSize, so that it can be used for
     * LA_NP_SYNC_MSG type messages. */
    u2MesgSize = u2Length;

    /* Get the Message type. */
    LA_RM_GET_4_BYTE (pMesg, &u4Offset, u4RedMsgType);
    u2Length -= LA_SYNC_MSG_TYPE_SIZE;

    switch (u4RedMsgType)
    {
        case LA_LACP_SYNC_MSG:
            LA_TRC (LA_RED_TRC, "Received Dynamic LACP sync msg. \n");
            LaRedProcessLacpSyncMsg (pMesg, &u4Offset, &u2Length);
            break;

        case LA_CURR_SPLIT_TMR_EXP_MSG:
            LA_TRC (CONTROL_PLANE_TRC, "Received CurrentWhile Timer Exp "
                    "sync msg.\n");
            LaRedProcessCurrSplitTmrExp (pMesg, &u4Offset, &u2Length);
            break;

        case LA_CLEAN_SYNCUP_ENTRIES_MSG:
            LA_TRC (LA_RED_TRC, "Received Clean SyncUp Entries msg. \n");
            LaRedCleanSyncUpEntries ();
            break;

        case LA_NP_SYNC_MSG:
            if (LA_NODE_STATUS () != LA_NODE_STANDBY)
            {
                /* Not a Standby node, hence don't process the received 
                 * sync message. */
                return;
            }

            LA_TRC (CONTROL_PLANE_TRC, "Received Np Sync Up msg. \n");
            while ((u2MesgSize - u4Offset) >
                   (LA_RED_LENGTH_SIZE + LA_NP_SUB_TYPE_SIZE))
            {
                i4RetVal = LaRedStoreSyncUpInfoForOneAgg (pMesg, &u4Offset);
                if (i4RetVal == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                    break;
                }
            }
            break;
            /* Added for handling DLAG sync messages */
        case LA_DLAG_SYNC_MSG:
            if (LA_NODE_STATUS () != LA_NODE_STANDBY)
            {
                /* Not a Standby node, hence don't process the received
                 * sync message. */
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }
            LA_TRC (LA_RED_TRC, "Received Dynamic D-LAG sync msg. \n");
            if ((u2MesgSize - (UINT2) u4Offset) >
                (LA_RED_LENGTH_SIZE + DLAG_SUB_TYPE_SIZE))
            {
                i4RetVal = LaRedProcessDLAGSyncMsg (pMesg, &u4Offset);
                if (i4RetVal == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                    break;
                }
            }
            break;
        case LA_BULK_REQ_MSG:
            if (LA_IS_STANDBY_UP () == LA_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                LA_BULK_REQ_RECD () = LA_TRUE;
                break;
            }

            LA_BULK_REQ_RECD () = LA_FALSE;
            /* On recieving LA_BULK_REQ_MSG, Bulk updation process should be
             * restarted.
             */
            gLaRedGlobalInfo.u2BulkUpdNextPort = LA_MIN_PORTS;
            LaRedHandleBulkUpdateEvent ();

            break;

        case LA_BULK_UPD_TAIL_MSG:
            LA_TRC (LA_RED_TRC,
                    "[STANDBY]: Received Bulk Update Tail Message \r\n");
            /* On receiving LA_BULK_UPD_TAIL_MSG, give indication to RM. 
             * RM gives the trigger to higher layers
             * (STP/VLAN) inorder to send the Bulk Req msg. \n");
             */
#ifdef NPAPI_WANTED
            if (gLaRedGlobalInfo.u1LaPortHwStatusSyncUp ==
                LA_PORT_HW_STATUS_NOT_RCVD)
            {
                LaRedSelfUpdatePortHwStatus ();
            }
            else
            {
                gLaRedGlobalInfo.u1LaPortHwStatusSyncUp =
                    LA_PORT_HW_STATUS_NOT_RCVD;
            }
#endif
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ProtoEvt.u4Error = RM_NONE;
            LaRmHandleProtocolEvent (&ProtoEvt);
            break;

        case LA_WAIT_WHILE_TMR_EXP_MSG:
            LA_TRC (CONTROL_PLANE_TRC,
                    "Received WaitWhileTimer Expiry msg. \n");
            LaRedProcessWaitWhileTmrExp (pMesg, &u4Offset, &u2Length);
            break;

        case LA_PORT_OPER_STATUS_MSG:
            LA_TRC (LA_RED_TRC, "Received Port oper status syncup msg. \n");
            LaRedProcessPortOperStatus (pMesg, &u4Offset, &u2Length);
            break;

#ifdef NPAPI_WANTED
        case LA_PORT_HW_STATUS_SYNCUP_MSG:
            LA_TRC (LA_RED_TRC, "Received Port HW Status SyncUp msg. \n");
            LaRedProcessPortHwStatusSyncUpMsg (pMesg, &u4Offset, &u2Length);
            gLaRedGlobalInfo.u1LaPortHwStatusSyncUp = LA_PORT_HW_STATUS_RCVD;
            break;
#endif
            /* HITLESS RESTART */
        case LA_HR_STDY_ST_PKT_REQ:
            LA_TRC (LA_RED_TRC, "Received Steady State Pkt Request msg. \n");
            LaRedHRProcStdyStPktReq ();
            break;

        default:
            break;
    }

}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaRedSelfUpdatePortHwStatus                          */
/*                                                                           */
/* Description        : This function updates the LaLacHwPortStatus for all  */
/*                      ports by self,this happens if ACTIVE, doesn't send   */
/*                      LA_PORT_HW_STATUS_SYNCUP_MSG                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tLaLacPortEntry                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : tLaLacPortEntry                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedSelfUpdatePortHwStatus ()
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaHwSyncInfo      *pLaHwEntry = NULL;
    UINT4               u4IfIndex;
    UINT1               u1BundleState;

    for (u4IfIndex = LA_MIN_PORTS; u4IfIndex <= LA_MAX_PORTS; u4IfIndex++)
    {

        if ((LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry)) != LA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "PortEntry Not found for IfIndex [%d]\n", u4IfIndex);
            continue;
        }

        if ((pAggEntry = pPortEntry->pAggEntry) == NULL)
        {

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "pAggEntry Not found for IfIndex [%d]\n", u4IfIndex);
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
            continue;
        }
        pLaHwEntry = LA_RED_GET_AGG_ENTRY (pAggEntry->u2AggIndex);
        if (pLaHwEntry == NULL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "pLaHwEntry Not found for AggIndex [%d]\n",
                         pAggEntry->u2AggIndex);
            continue;
        }

        LaGetPortBundleState (u4IfIndex, &u1BundleState);
        if (u1BundleState == LA_PORT_UP_IN_BNDL &&
            pLaHwEntry->u2HwAggIdx != LA_INVALID_HW_AGG_IDX)
        {
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_ADDED_IN_LAGG;
        }
        else
        {
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
        }
    }                            /* End of FOR Statement */
}

/*****************************************************************************/
/* Function Name      : LaRedProcessPortHwStatusSyncUpMsg                    */
/*                                                                           */
/* Description        : This function updates the LaLacHwPortStatus of all   */
/*                      PortEntry                                            */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tLaLacPortEntry                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : tLaLacPortEntry                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedProcessPortHwStatusSyncUpMsg (tRmMsg * pMesg, UINT4 *pu4Offset,
                                   UINT2 *pu2Length)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2MesgSize = 0;
    UINT1               u1PortState = 0;

    if (LA_NODE_STATUS () != LA_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received 
         * sync message. */
        return;
    }

    /* Get the Message size encoded by the LA module in Active node. */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length -= LA_RED_LENGTH_SIZE;

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        return;
    }

    while (*pu2Length >= LA_PORT_HW_STATUS_SYNCUP_MSG_SIZE)
    {
        LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4IfIndex);
        LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1PortState);
        LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                     "Received Port's HW State SyncUP exp msg for "
                     "Port :%d and State = %d\n", u4IfIndex, u1PortState);
        LaRedHandlePortHwStatusSyncUp (u4IfIndex, u1PortState);
        *pu2Length -= LA_PORT_HW_STATUS_SYNCUP_MSG_SIZE;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedHandlePortHwStatusSyncUp                        */
/*                                                                           */
/* Description        : This function updates the LaLacHwPortStatus of all   */
/*                      PortEntry                                            */
/*                                                                           */
/* Input(s)           : Port number and the Hw Status                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tLaLacPortEntry                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : tLaLacPortEntry                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandlePortHwStatusSyncUp (UINT4 u4IfIndex, UINT1 u1RcvdPortHwStatus)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if ((LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry)) != LA_SUCCESS)
    {
        LA_TRC_ARG1 (LA_RED_TRC, "Received Port IfIndex [%d] is invalid",
                     u4IfIndex);
        return;
    }

    if (u1RcvdPortHwStatus == pPortEntry->LaLacHwPortStatus)
    {
        /* No change in the HW status */
        return;
    }

    switch (u1RcvdPortHwStatus)
    {
        case LA_HW_PORT_NOT_IN_LAGG:

            switch (pPortEntry->LaLacHwPortStatus)
            {
                case LA_HW_PORT_ADDED_IN_LAGG:
                    /* LA_NP_DEL_PORT_SUCCESS_INFO is missing */

                    if (pPortEntry->pAggEntry == NULL)
                    {
                        pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                        LaHlCreatePhysicalPort (pPortEntry);
                        break;
                    }

                    LaRedDelSyncPortEntry (LA_NP_DEL_PORT_SUCCESS_INFO,
                                           pPortEntry->pAggEntry->u2AggIndex,
                                           (UINT2) u4IfIndex);
                    break;

                case LA_HW_REM_PORT_FAILED_IN_LAGG:
                case LA_HW_ADD_PORT_FAILED_IN_LAGG:

                    /* This should not happen */

                    LA_TRC_ARG2 (LA_RED_TRC, "Received Port's Hw Status "
                                 "[%d] indicates more than one msg loss, "
                                 "current status [%d]", u1RcvdPortHwStatus,
                                 pPortEntry->LaLacHwPortStatus);
                    pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                    break;

                default:
                    break;
            }
            break;

        case LA_HW_PORT_ADDED_IN_LAGG:

            switch (pPortEntry->LaLacHwPortStatus)
            {
                case LA_HW_PORT_NOT_IN_LAGG:
                case LA_HW_ADD_PORT_FAILED_IN_LAGG:
                case LA_HW_REM_PORT_FAILED_IN_LAGG:
                    /*LA_NP_ADD_PORT_SUCCESS_INFO is missing */

                    LaRedAddSyncPortEntry (LA_NP_ADD_PORT_SUCCESS_INFO,
                                           pPortEntry->pAggEntry->u2AggIndex,
                                           (UINT2) u4IfIndex);
                    break;

                default:
                    break;
            }
            break;

        case LA_HW_REM_PORT_FAILED_IN_LAGG:

            switch (pPortEntry->LaLacHwPortStatus)
            {
                case LA_HW_PORT_NOT_IN_LAGG:
                    pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                    break;

                case LA_HW_PORT_ADDED_IN_LAGG:
                    /*LA_NP_DEL_PORT_FAILURE_INFO is missing */

                    if (pPortEntry->pAggEntry == NULL)
                    {
                        pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                        break;
                    }

                    LaRedDelSyncPortEntry (LA_NP_DEL_PORT_FAILURE_INFO,
                                           pPortEntry->pAggEntry->u2AggIndex,
                                           (UINT2) u4IfIndex);
                    break;

                case LA_HW_ADD_PORT_FAILED_IN_LAGG:

                    /* This should not happen */

                    LA_TRC_ARG2 (LA_RED_TRC, "Received Port's Hw Status "
                                 "[%d] indicates more than one msg loss, "
                                 "current status [%d]", u1RcvdPortHwStatus,
                                 pPortEntry->LaLacHwPortStatus);
                    pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                    break;

                default:
                    break;
            }
            break;

        case LA_HW_ADD_PORT_FAILED_IN_LAGG:

            switch (pPortEntry->LaLacHwPortStatus)
            {
                case LA_HW_PORT_NOT_IN_LAGG:

                    /*LA_NP_ADD_PORT_FAILURE_INFO is missing */

                    if (pPortEntry->pAggEntry == NULL)
                    {
                        pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                        break;
                    }
                    LaRedAddSyncPortEntry (LA_NP_ADD_PORT_FAILURE_INFO,
                                           pPortEntry->pAggEntry->u2AggIndex,
                                           (UINT2) u4IfIndex);
                    break;

                case LA_HW_PORT_ADDED_IN_LAGG:

                    /* LA_NP_DEL_PORT_SUCCESS_INFO is missing */
                    if (pPortEntry->pAggEntry != NULL)
                    {
                        LaRedDelSyncPortEntry (LA_NP_DEL_PORT_SUCCESS_INFO,
                                               pPortEntry->pAggEntry->
                                               u2AggIndex, (UINT2) u4IfIndex);
                    }
                    pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                    break;

                case LA_HW_REM_PORT_FAILED_IN_LAGG:

                    /* This should not happen */

                    LA_TRC_ARG2 (LA_RED_TRC, "Received Port's Hw Status [%d]"
                                 "indicates more than one msg loss, current "
                                 "status [%d]", u1RcvdPortHwStatus,
                                 pPortEntry->LaLacHwPortStatus);
                    pPortEntry->LaLacHwPortStatus = u1RcvdPortHwStatus;
                    break;

                default:
                    break;
            }
            break;
    }
}
#endif /* NPAPI_WANTED */
/*****************************************************************************/
/* Function Name      : LaRedProcessLacpSyncMsg.                             */
/*                                                                           */
/* Description        : This function process the received Lacp sync up msg  */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.SyncUpTable.                        */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedProcessLacpSyncMsg (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tLaRedSyncedUpInfo  LaPortSyncInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal;
    UINT2               u2MesgSize = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    MEMSET (&LaPortSyncInfo, 0, sizeof (tLaRedSyncedUpInfo));

    if (LA_NODE_STATUS () != LA_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received 
         * sync message. */
        return;
    }

    /* Get the Message size encoded by the LA module in Active node. */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length -= LA_RED_LENGTH_SIZE;

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    while (*pu2Length >= LA_SYNC_PORT_MSG_SIZE)
    {
        i4RetVal = LaRedGetPortInfoFromRMMesg (pMesg, pu4Offset,
                                               &u4IfIndex, &LaPortSyncInfo);
        if (i4RetVal == LA_SUCCESS)
        {
            LaRedStoreSyncMsgforPort ((UINT2) u4IfIndex, &LaPortSyncInfo);
        }
        else
        {
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
        *pu2Length -= LA_SYNC_PORT_MSG_SIZE;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : LaRedProcessPortOperStatus.                          */
/*                                                                           */
/* Description        : This function process the received Port Oper Status  */
/*                      SyncUp  msg.                                         */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedProcessPortOperStatus (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = 0;
    UINT2               u2MesgSize = 0;
    UINT1               u1PortOperStatus = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (LA_NODE_STATUS () != LA_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received 
         * sync message. */
        return;
    }

    /* Get the Message size encoded by the LA module in Active node. */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length -= LA_RED_LENGTH_SIZE;

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    while (*pu2Length >= LA_PORT_OPER_STATUS_MSG_SIZE)
    {
        LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4IfIndex);
        LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1PortOperStatus);
        LA_TRC_ARG2 (LA_RED_TRC, "Received port oper sync up msg for "
                     "Port :%d Oper value :%d\n", u4IfIndex, u1PortOperStatus);
        LaRedHandlePortOperStatus (u4IfIndex, u1PortOperStatus);
        L2IwfSetBridgePortOperStatus (u4IfIndex, u1PortOperStatus);
        *pu2Length -= LA_PORT_OPER_STATUS_MSG_SIZE;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedHandlePortOperStatus.                           */
/*                                                                           */
/* Description        : This function calls the respective routines depending*/
/*                      upon the Oper Status                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex  - IfIndex of the port.                    */
/*                      u1PortOperStatus - Port Oper Status                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandlePortOperStatus (UINT4 u4IfIndex, UINT1 u1PortOperStatus)
{
    tRmProtoEvt         ProtoEvt;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaRedSyncedUpInfo *pSyncUpEntry = NULL;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Check the validity of the port. */
    if ((u4IfIndex < LA_MIN_PORTS) || (u4IfIndex > LA_MAX_PORTS))
    {
        /* Invalid port. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (LA_MODULE_STATUS == LA_ENABLED)
    {
        if ((LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry)) == LA_SUCCESS)
        {
            if (pPortEntry->u1PortOperStatus != u1PortOperStatus)
            {
                pPortEntry->u1PortOperStatus = u1PortOperStatus;
                if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
                {
                    if (u1PortOperStatus == CFA_IF_UP)
                    {
                        if (gu4LagAdminDownNoNeg == LA_TRUE)
                        {
                            if ((pPortEntry->pAggEntry != NULL) &&
                                (pPortEntry->pAggEntry->
                                 u1AggAdminStatus == LA_ADMIN_UP))
                            {

                                LaHandleEnablePort (pPortEntry);
                            }
                        }
                        else
                        {
                            LaHandleEnablePort (pPortEntry);
                        }
                    }
                    else if (u1PortOperStatus == CFA_IF_DOWN)
                    {
                        LaHandleDisablePort (pPortEntry);
                        pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u4IfIndex);
                        pSyncUpEntry->ReceivedTime = 0;
                        pSyncUpEntry->u2PortActorKey = 0;
                        pSyncUpEntry->u1TimerCount = 0;
                    }
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : LaRedProcessWaitWhileTmrExp.                         */
/*                                                                           */
/* Description        : This function process the received WaitWhile  tmr    */
/*                      exp msg.                                             */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedProcessWaitWhileTmrExp (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = 0;
    UINT2               u2MesgSize = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (LA_NODE_STATUS () != LA_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received 
         * sync message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Get the Message size encoded by the LA module in Active node. */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length -= LA_RED_LENGTH_SIZE;

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    while (*pu2Length >= LA_WAIT_WHILE_TMR_EXP_MSG_SIZE)
    {
        LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4IfIndex);
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Received WaitWhile Tmr exp msg for "
                     "Port :%d\n", u4IfIndex);
        LaRedHandleWaitWhileTmrExp (u4IfIndex);
        *pu2Length -= LA_WAIT_WHILE_TMR_EXP_MSG_SIZE;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedProcessCurrSplitTmrExp.                         */
/*                                                                           */
/* Description        : This function process the received curr split tmr    */
/*                      exp msg and stores the info in LaSyncUpTable.        */
/*                                                                           */
/* Input(s)           : pMesg    - Pointer to the sync up message.           */
/*                      pu4Offset - Pointer to the offset in the msg.        */
/*                      pu2Length - Length of buffer.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.SyncUpTable.                        */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedProcessCurrSplitTmrExp (tRmMsg * pMesg, UINT4 *pu4Offset, UINT2 *pu2Length)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = 0;
    UINT2               u2MesgSize = 0;
    UINT1               u1Count = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (LA_NODE_STATUS () != LA_NODE_STANDBY)
    {
        /* Not a Standby node, hence don't process the received 
         * sync message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Get the Message size encoded by the LA module in Active node. */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2MesgSize);
    *pu2Length -= LA_RED_LENGTH_SIZE;

    if (*pu2Length != u2MesgSize)
    {
        /* Data corruption, hence ignore the whole sync up message. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    while (*pu2Length >= LA_CURR_SPLIT_TMR_MSG_SIZE)
    {
        LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4IfIndex);
        LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1Count);
        LA_TRC_ARG2 (LA_RED_TRC, "Received CurrWhileSplit Tmr exp msg for "
                     "Port :%d and Count = %d\n", u4IfIndex, u1Count);
        LaRedHandleCurrWhileTmrExp (u4IfIndex, u1Count);
        *pu2Length -= LA_CURR_SPLIT_TMR_MSG_SIZE;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedGetPortInfoFromRMMesg.                          */
/*                                                                           */
/* Description        : This function reads the dynamic information for a    */
/*                      port  and stores timer count and the actor key       */
/*                      in the given pointer  pLaPortSyncInfo.The partner    */
/*                      info will be immediately applied.                    */
/*                                                                           */
/* Input(s)           : pRmBuf   - Pointer to the sync up message.           */
/*                      pu4Offset - Pointer to the offset from where the     */
/*                                  read should start.                       */
/*                                                                           */
/* Output(s)          : pu4Offset - Pointer to the offset that contains the  */
/*                                  next port information.                   */
/*                      pu4IfIndex - Pointer to the port identifier.         */
/*                      pLaPortSyncInfo - Pointer containing the info        */
/*                                            to be stored in LaSyncUpTable. */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/
INT4
LaRedGetPortInfoFromRMMesg (tRmMsg * pRmBuf, UINT4 *pu4Offset,
                            UINT4 *pu4IfIndex,
                            tLaRedSyncedUpInfo * pLaPortSyncInfo)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacpduInfo       LaLacpduInfo;
    tLaLacIfInfo       *pPartnerInfo = NULL;
    UINT1               u1Val = 0;
    UINT1               u1ActorVal = 0;
    tLaLacAggEntry     *pAggEntry = NULL;

    /* Get the port Identifier. */
    LA_RM_GET_4_BYTE (pRmBuf, pu4Offset, (*pu4IfIndex));

    if ((*pu4IfIndex < LA_MIN_PORTS) || (*pu4IfIndex > LA_MAX_PORTS))
    {
        /* Invalid port, hence return failure. Move the offset, so that
         * it points to the next port sync message.*/
        *pu4Offset += LA_SYNC_PORT_MSG_SIZE - LA_PORT_ID_SIZE;
        return LA_FAILURE;
    }

    LaGetPortEntry ((UINT2) *pu4IfIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return LA_FAILURE;
    }

    pAggEntry = pPortEntry->pAggEntry;

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    /* Get the AggIndex */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset, (pLaPortSyncInfo->u2PortActorKey));

    /* Now get the partner Info from the Sync Up message and fill it in
     * the actor portion of the LaLacpduInfo.
     * */
    pPartnerInfo = &(LaLacpduInfo.LaLacActorInfo);

    /* Get the system Priority */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset,
                      (pPartnerInfo->LaSystem.u2SystemPriority));

    LA_RM_GET_N_BYTE (pRmBuf, (pPartnerInfo->LaSystem.SystemMacAddr),
                      pu4Offset, LA_MAC_ADDRESS_SIZE);

    /* Get the Actor Key */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset, pPartnerInfo->u2IfKey);

    /* Get the actor port priority */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset, (pPartnerInfo->u2IfPriority));

    /* Get the actor port index/number */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset, (pPartnerInfo->u2IfIndex));

    /* Get the actor port state */
    LA_RM_GET_1_BYTE (pRmBuf, pu4Offset, u1Val);

    pPartnerInfo->LaLacPortState.LaLacpActivity = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaLacpTimeout = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaAggregation = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaSynchronization = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaCollecting = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaDistributing = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaDefaulted = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pPartnerInfo->LaLacPortState.LaExpired = u1Val & 0x01;

    LA_RM_GET_1_BYTE (pRmBuf, pu4Offset, (pLaPortSyncInfo->u1TimerCount));
    LaRedLacStandbyConstruct (pPortEntry, &LaLacpduInfo);
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset,
                      (pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority));

    LA_RM_GET_N_BYTE (pRmBuf,
                      (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr),
                      pu4Offset, LA_MAC_ADDRESS_SIZE);

    /* Get the Actor Key */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset, pPortEntry->LaLacActorInfo.u2IfKey);

    /* Get the actor port priority */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset,
                      (pPortEntry->LaLacActorInfo.u2IfPriority));

    /* Get the actor port index/number */
    LA_RM_GET_2_BYTE (pRmBuf, pu4Offset,
                      (pPortEntry->LaLacActorInfo.u2IfIndex));

    /* Get the actor port state */
    LA_RM_GET_1_BYTE (pRmBuf, pu4Offset, u1ActorVal);

    pPortEntry->LaLacActorInfo.LaLacPortState.LaLacpActivity =
        u1ActorVal & 0x01;
    u1ActorVal = (UINT1) (u1ActorVal >> 1);

    pPortEntry->LaLacActorInfo.LaLacPortState.LaLacpTimeout = u1ActorVal & 0x01;
    u1ActorVal = (UINT1) (u1ActorVal >> 1);

    pPortEntry->LaLacActorInfo.LaLacPortState.LaAggregation = u1ActorVal & 0x01;
    u1ActorVal = (UINT1) (u1ActorVal >> 4);

    pPortEntry->LaLacActorInfo.LaLacPortState.LaDefaulted = u1ActorVal & 0x01;
    u1ActorVal = (UINT1) (u1ActorVal >> 1);

    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = u1ActorVal & 0x01;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedHandleGoStandbyEvent.                           */
/*                                                                           */
/* Description        : This function handles the GO_STANDBY event from RM.  */
/*                      If node is active, then restart LA module, else just */
/*                      update the node status.                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleGoStandbyEvent (VOID)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2PortIndex = 0;

    /* 
     * Make the node status to Standby , otherwise
     * hardware init/deinit will be called.
     */

    LA_NODE_STATUS () = LA_NODE_STANDBY;
    LA_INIT_NUM_STANDBY_NODES ();
#ifdef NPAPI_WANTED
    /* Stop audit task by changing the u1IsAuditStopped variable to 
     * LA_TRUE. */
    LA_RED_IS_AUDIT_STOP () = LA_TRUE;
#endif
    if (LA_PROTOCOL_ADMIN_STATUS () == LA_ENABLED)
    {
        /*
         * Disable the module inorder to flush the dynamic information
         */
        LaDisable ();
        /* When the module is disabled, lacp disable function will be called  for
         * those ports which are in aggregation. This will result in disabling
         * the DISTRIBUTOR capability of the port and will set the 
         * LaLacHwPortStatus as LA_HW_PORT_NOT_IN_LAGG if it is an Active node.
         * Then the Active node will send a dynamic update to the standby node
         * which will update the LaLacHwPortStatus in Standby node. But during
         * force-switchover, module disable will be called only in Standby node
         * and as a result, LaLacHwPortStatus will not be resetted to
         * LA_HW_PORT_NOT_IN_LAGG. Hence it is set explicitly for all the ports.
         */
        for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
        {
            LaGetPortEntry (u2PortIndex, &pPortEntry);
            if (pPortEntry == NULL)
            {
                continue;
            }

            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
        }
        /* According to configurations LA module is enabled,
         * hence enable LA here. */
        LaEnable ();
    }
}

/*****************************************************************************/
/* Function Name      : LaRedHandleStandByUpEvent.                           */
/*                                                                           */
/* Description        : This function handles the STANDBY UP event from RM.  */
/*                      If node is active, then it increments the            */
/*                      u1NumPeersUp and calls LaRedHandleBulkReqEvent       */
/*                                                                           */
/* Input(s)           : pvPeerId - PeerId to which the BulkUpdate has to be  */
/*                      sent.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleStandByUpEvent (VOID *pvPeerId)
{
    UNUSED_PARAM (pvPeerId);

    if (LA_BULK_REQ_RECD () == LA_TRUE)
    {
#if !defined (CFA_WANTED) && !defined (EOAM_WANTED) && !defined (PNAC_WANTED)
        LA_BULK_REQ_RECD () = LA_FALSE;
        /* Bulk request msg is recieved before RM_STANDBY_UP event.
         * So we are sending bulk updates now.
         */
        gLaRedGlobalInfo.u2BulkUpdNextPort = LA_MIN_PORTS;
        LaRedHandleBulkUpdateEvent ();
#endif
    }
}

/*****************************************************************************/
/* Function Name      : LaRedSendBulkReq.                                    */
/*                                                                           */
/* Description        : This function sends a bulk request to the active     */
/*                      node in the system.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSendBulkReq (VOID)
{

    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SyncMsgType;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE;

    LA_TRC (LA_RED_TRC, "LA RED: Send Bulk Req\n");
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return (LA_FAILURE);
    }

    /* Fill the message type. */
    u4SyncMsgType = LA_BULK_REQ_MSG;
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4SyncMsgType);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, LA_BULK_REQ_MSG);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedHandleBulkReqEvent.                             */
/*                                                                           */
/* Description        : This function gets the dynamic information from all  */
/*                      ports whose Receive sem is in CURRENT state.         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleBulkReqEvent (VOID *pvPeerId)
{
    /* Send Bulk Update messages to the standby. */
    UINT2               u2PortIndex = 0;
    UINT2               u2StartIfIndex = 0;
    UINT2               u2BulkUpdPortCnt = 0;
    UNUSED_PARAM (pvPeerId);

    LA_TRC (LA_RED_TRC, "LA RED: Received Bulk Req\n");

    if ((LA_SYSTEM_CONTROL == LA_SHUTDOWN) ||
        (LA_MODULE_STATUS == LA_DISABLED) || (LA_INITIALIZED () != TRUE))
    {
        /* LA completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        LaRmSetBulkUpdatesStatus (RM_LA_APP_ID);
        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        LaRedSendBulkUpdateTailMsg ();

        return;
    }

    u2BulkUpdPortCnt = LA_RED_NO_OF_PORTS_PER_SUB_UPDATE;
    u2StartIfIndex = gLaRedGlobalInfo.u2BulkUpdNextPort;

    for (u2PortIndex = gLaRedGlobalInfo.u2BulkUpdNextPort;
         u2PortIndex <= LA_MAX_PORTS && u2BulkUpdPortCnt > 0;
         u2PortIndex++, u2BulkUpdPortCnt--)
    {
        /* Update u2BulkUpdNextPort, to resume the next sub bulk update from
         * where the previous sub bulk update left it out.
         */
        gLaRedGlobalInfo.u2BulkUpdNextPort++;
    }
    LaRedPortOperStatusBulkSyncup (u2StartIfIndex);

    /*Sync Up the learnt partner information */
    LaRedLacpBulkSyncup (u2StartIfIndex);

    LaRedCurrWhileTmrBulkSyncUp (u2StartIfIndex);

    LaRedWaitWhileTmrBulkSyncUp (u2StartIfIndex);

#ifdef NPAPI_WANTED
    LaRedPortHwStatusBulkSyncUp (u2StartIfIndex);
#endif
    if (gLaRedGlobalInfo.u2BulkUpdNextPort <= LA_MAX_PORTS)
    {
        /* Send an event to start the next sub bulk update.
         */
        LA_SEND_EVENT (LA_TASK_ID, LA_RED_BULK_UPD_EVENT);
    }
    else
    {
        /* Sync Up information at the NP level. */
        LaRedAggBulkSyncUp ();

        /* Sync Up the D-LAG information */
        LaDLAGRedAggBulkSyncUp ();

        /* LA completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        LaRmSetBulkUpdatesStatus (RM_LA_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        LaRedSendBulkUpdateTailMsg ();
    }

    return;
}

/******************************************************************************/
/* Function Name      : LaRedHandleWaitWhileTmrExp.                           */
/*                                                                            */
/* Description        : This function sets the ReadyN variable for the given  */
/*                      port to True and checks if the ReadyN variable for    */
/*                      all the ports configured to the same aggregator is    */
/*                      True.If so, for all those ports with value as         */
/*                      SELECTED, the Ready variable is set as True and the   */
/*                      function to attach the port to the aggregator is      */
/*                      called.                                               */
/*                                                                            */
/* Input(s)           : u4IfIndex  - IfIndex of the port.                     */
/*                                                                            */
/* Output(s)          : None.                                                 */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : gLaRedGlobalInfo.                                     */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : None.                                                 */
/******************************************************************************/
VOID
LaRedHandleWaitWhileTmrExp (UINT4 u4IfIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Check the validity of the port. */
    if ((u4IfIndex < LA_MIN_PORTS) || (u4IfIndex > LA_MAX_PORTS))
    {
        /* Invalid port. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    if (LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_WAIT_WHILE_EXP)
        != LA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "TMR: Port: %d WaitWhile timer Expiry"
                     "Handling FAILED\n", pPortEntry->u2PortIndex);
    }
}

/*****************************************************************************/
/* Function Name      : LaRedHandleCurrWhileTmrExp.                          */
/*                                                                           */
/* Description        : This function stores the received time and given     */
/*                      count in syncup entry for the given ifindex. If the  */
/*                      count has reached LA_CURRWHILE_SPLIT_INTERVALS value */
/*                      then this funcion clears the sync up entry for the   */
/*                      given ifindex and calls the corresponding state      */
/*                      machine routine.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex  - IfIndex of the port.                    */
/*                      u1Count - No of time the curr while split interval   */
/*                                timer has expired.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaRedGlobalInfo.                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.SyncUpTable.                        */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleCurrWhileTmrExp (UINT4 u4IfIndex, UINT1 u1Count)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaRedSyncedUpInfo *pSyncUpEntry;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Check the validity of the port. */
    if ((u4IfIndex < LA_MIN_PORTS) || (u4IfIndex > LA_MAX_PORTS))
    {
        /* Invalid port. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u4IfIndex);

    if (u1Count == LA_CURRWHILE_SPLIT_INTERVALS)
    {
        /* Current while timer has expired and hence clear the 
         * information for this port. */
        pSyncUpEntry->ReceivedTime = 0;
        pSyncUpEntry->u1TimerCount = 0;
        pSyncUpEntry->u2PortActorKey = 0;

        LaLacRxMachine (pPortEntry, LA_RXM_EVENT_CURRENT_WHILE_EXP, NULL);
        return;
    }

    /* One of the split interval of the curentwhile timer has expired,
     * hence store the received time and the count value. */
    LA_GET_SYS_TIME (&(pSyncUpEntry->ReceivedTime));
    pSyncUpEntry->u1TimerCount = u1Count;
    return;
}

/*****************************************************************************/
/* Function Name      : LaRedCleanSyncUpEntries.                             */
/*                                                                           */
/* Description        : This function cleans up LaSyncUpTable.               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.LaSyncUpTable.                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedCleanSyncUpEntries (VOID)
{
    tLaRedSyncedUpInfo *pSyncUpEntry = NULL;
    UINT2               u2Port;
#ifdef NPAPI_WANTED
    UINT2               u2AggIndex;
    UINT2               u2MaxAggId;
#endif

    if (LA_NODE_STATUS () != LA_NODE_STANDBY)
    {
        /* Node is not a standby node, hence don't process the 
         * received sync message. */
        return;
    }

    for (u2Port = LA_MIN_PORTS; u2Port <= LA_MAX_PORTS; u2Port++)
    {
        /* Remove the stored information from the SyncUpTable. */
        pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2Port);

        /* Store the AggId as zero. */
        pSyncUpEntry->ReceivedTime = 0;
        pSyncUpEntry->u1TimerCount = 0;
        pSyncUpEntry->u2PortActorKey = 0;
    }

#ifdef NPAPI_WANTED
    /* 
     * Clean up sw aggid to hw agg id mapping.
     */
    u2MaxAggId = LA_MAX_PORTS + LA_HW_MAX_AGG_GRPS;
    for (u2AggIndex = (LA_MAX_PORTS + 1); u2AggIndex <= u2MaxAggId;
         u2AggIndex++)
    {
        LaRedDelSyncAggEntry (u2AggIndex);
    }
#endif
}

/*****************************************************************************/
/* Function Name      : LaRedSendPduOnAllPort.                               */
/*                                                                           */
/* Description        : This function sends LACPDU on ports that are         */
/*                      operationally up and where LACP is enabled.          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedSendPduOnAllPort (VOID)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;

    while (LaGetNextPortEntry (pPortEntry, &pNextPortEntry) == LA_SUCCESS)
    {
        pPortEntry = pNextPortEntry;
        /* Send LACPDU on ports that are enabled and where LACP is 
         * enabled. */
        if ((pPortEntry->LaLacpMode == LA_MODE_LACP) &&
            (pPortEntry->LaLacpEnabled == LA_LACP_ENABLED) &&
            (pPortEntry->LaPortEnabled == LA_PORT_ENABLED))
        {
            if (LaLacTxLacpdu (pPortEntry) == LA_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "TXM: Port: %u :: LACPDU transmitted out\n",
                             pPortEntry->u2PortIndex);

                (pPortEntry->u4HoldCount)++;
                pPortEntry->NttFlag = LA_FALSE;
            }

        }
    }
}

/*****************************************************************************/
/* Function Name      : LaRedGetNodeStateFromRm.                             */
/*                                                                           */
/* Description        : This function gets Node status from RM and stores in */
/*                      gLaRedGlobalInfo.NodeStatus.                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.NodeStatus.                         */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedGetNodeStateFromRm (VOID)
{
    UINT4               u4NodeState;
    u4NodeState = LaRmGetNodeState ();

    switch (u4NodeState)
    {
        case RM_ACTIVE:
            LA_NODE_STATUS () = LA_NODE_ACTIVE;
            break;
        case RM_STANDBY:
            LA_NODE_STATUS () = LA_NODE_STANDBY;
            break;
        default:
            LA_NODE_STATUS () = LA_NODE_IDLE;
            break;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedSyncModifyAggPortsInfo.                         */
/*                                                                           */
/* Description        : This function will be called when a port is added or */
/*                      removed from hardware aggregation. This function     */
/*                      will form rm message containing the change done in hw*/
/*                      and sends it to standby node.                        */
/*                      Note: This function will be called only in active    */
/*                      node.                                                */
/*                                                                           */
/* Input(s)           : u2LaNpSubType - Type used to indicate add/remove     */
/*                                      port to hw aggregation.              */
/*                      u2AggIndex - Aggregator Id.                          */
/*                      u2PortNum  - Port Id that was added/removed from hw  */
/*                                   aggregation.                            */
/*                      pHwInfo    - Hw aggregation related info.            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS /LA_FAILURE                               */
/*****************************************************************************/
INT4
LaRedSyncModifyAggPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex,
                             tLaHwId HwInfo)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2SyncMsgLen;
    UINT2               u2BufSize;
    UINT4               u4AggIndex;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if ((LA_IS_STANDBY_UP () == LA_FALSE) || (RmGetNodeState () != RM_ACTIVE))
    {
        /*Only the Active node needs to send the SyncUp message */
        return LA_SUCCESS;
    }

    /* Store this information in gLaRedGlobalInfo.aLaRedHwAggEntry,
     * so that in bulk update this information can be sent to
     * the standby node. */

    if (u2LaNpSubType == LA_NP_DEL_INFO)
    {
        LaRedDelSyncAggEntry (u2AggIndex);
        /* 
         * u2SyncMsgLen = Size of sw AggId.
         */
        u2SyncMsgLen = LA_PORT_ID_SIZE;
    }
    else
    {
        LaRedAddSyncAggEntry (u2AggIndex, (UINT2) HwInfo);
        /* 
         * u2SyncMsgLen = Size of sw AggId + Size of HwInfo to be synced.
         */
        u2SyncMsgLen = LA_PORT_ID_SIZE + LA_NP_HWINFO_SIZE;
    }

    if (LA_IS_STANDBY_UP () == LA_FALSE)
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    u2BufSize = (UINT2) (LA_SYNC_MSG_TYPE_SIZE + LA_NP_SUB_TYPE_SIZE +
                         LA_RED_LENGTH_SIZE + u2SyncMsgLen);

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return (LA_FAILURE);
    }

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_NP_SYNC_MSG);

    /* Add Information, hence set the sub type as Add info. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2LaNpSubType);

    /* Fill the number of size of port information to be synced up. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Now fill the sw Agg Id, Hw AggId, and the port that is added to the
     * aggregator. */
    u4AggIndex = (UINT4) u2AggIndex;
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4AggIndex);

    if (u2LaNpSubType == LA_NP_ADD_INFO)
    {
        /* When the first port becomes active in aggregation, the aggregation 
         * will be created in hw and hence hw aggid will be present. When the 
         * last active port in an aggregation goes out aggregation that 
         * aggregator is removed from aggregator. In this case no hw aggid 
         * will be present. */

        /* In this HwInfo is nothing but HwAggId. Hence just send it as a 
         * UINT4 val. */
        LA_RM_PUT_4_BYTE (pMsg, &u4Offset, (UINT4) HwInfo);
    }

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncModifyPortsInfo.                            */
/*                                                                           */
/* Description        : This function will be called when a port is added or */
/*                      removed from bundle in the hardware. This function   */
/*                      will form rm message containing the change done in hw*/
/*                      and sends it to standby node.                        */
/*                      Note: This function will be called only in active    */
/*                      node.                                                */
/*                                                                           */
/* Input(s)           : u2LaNpSubType - Type used to indicate add/remove     */
/*                                      port to hw aggregation.              */
/*                      u2AggIndex - Aggregator Id.                          */
/*                      u2PortNum  - Port Id that was added/removed from hw  */
/*                                   aggregation.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS /LA_FAILURE                               */
/*****************************************************************************/
INT4
LaRedSyncModifyPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex,
                          UINT2 u2PortNum)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize;
    UINT4               u4AggIndex = (UINT4) u2AggIndex;
    UINT4               u4PortNum = (UINT4) u2PortNum;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if ((LA_NODE_STATUS () != LA_NODE_ACTIVE)
        || (LA_IS_STANDBY_UP () == LA_FALSE))
    {
        /*Only the Active node needs to send the SyncUp message */
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    /* These messages has the AggIndex and the PortIndex */
    u2SyncMsgLen = LA_PORT_ID_SIZE + LA_PORT_ID_SIZE;

    u2BufSize = (UINT2) (LA_SYNC_MSG_TYPE_SIZE + LA_NP_SUB_TYPE_SIZE +
                         LA_RED_LENGTH_SIZE + u2SyncMsgLen);

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return (LA_FAILURE);
    }

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_NP_SYNC_MSG);

    /* Add Information, hence set the sub type as Add info. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2LaNpSubType);

    /* Fill the number of size of port information to be synced up. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);

    /* Now fill the sw Agg Id, Hw AggId, and the port that is added to the
     * aggregator. */
    switch (u2LaNpSubType)
    {
        case LA_NP_ADD_PORT_SUCCESS_INFO:
        case LA_NP_ADD_PORT_FAILURE_INFO:
        case LA_NP_DEL_PORT_SUCCESS_INFO:
        case LA_NP_DEL_PORT_FAILURE_INFO:

            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4AggIndex);
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortNum);

            if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        default:
            RM_FREE (pMsg);
            break;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedStoreSyncUpInfoForOneAgg.                       */
/*                                                                           */
/* Description        : This function will be called when a port add/remove  */
/*                      from hw aggregation message is received. This fn     */
/*                      stores the information in sync up table.             */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                                                                           */
/* Output(s)          : pu2Length - number of bytes written                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedStoreSyncUpInfoForOneAgg (tRmMsg * pMesg, UINT4 *pu4Offset)
{
    UINT4               u4AggIndex = 0;
    UINT4               u4HwIndex;
#ifdef NPAPI_WANTED
    UINT4               u4PortNum;
#endif
    UINT2               u2LaNpSubType = 0;
    UINT2               u2NpMesgLength = 0;

    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2LaNpSubType);
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2NpMesgLength);

    switch (u2LaNpSubType)
    {
        case LA_NP_DEL_INFO:

            if (u2NpMesgLength != LA_PORT_ID_SIZE)
            {
                return LA_FAILURE;
            }
            /* Get the sw Agg Id. */
            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);
            LaRedDelSyncAggEntry ((UINT2) u4AggIndex);
            break;

        case LA_NP_ADD_INFO:

            if (u2NpMesgLength != (LA_PORT_ID_SIZE + LA_NP_HWINFO_SIZE))
            {
                return LA_FAILURE;
            }
            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);
            /* Get the hw agg Id.  This information will be used auditing. */
            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4HwIndex);
            LaRedAddSyncAggEntry ((UINT2) u4AggIndex, (UINT2) u4HwIndex);
            break;
#ifdef NPAPI_WANTED
        case LA_NP_ADD_PORT_SUCCESS_INFO:
        case LA_NP_ADD_PORT_FAILURE_INFO:
            if (u2NpMesgLength != (LA_PORT_ID_SIZE + LA_PORT_ID_SIZE))
            {
                return LA_FAILURE;
            }

            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);
            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4PortNum);
            LaRedAddSyncPortEntry (u2LaNpSubType, (UINT2) u4AggIndex,
                                   (UINT2) u4PortNum);
            break;

        case LA_NP_DEL_PORT_SUCCESS_INFO:
        case LA_NP_DEL_PORT_FAILURE_INFO:

            if (u2NpMesgLength != (LA_PORT_ID_SIZE + LA_PORT_ID_SIZE))
            {
                return LA_FAILURE;
            }

            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);
            LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4PortNum);
            LaRedDelSyncPortEntry (u2LaNpSubType, (UINT2) u4AggIndex,
                                   (UINT2) u4PortNum);
            break;
#endif
        default:
            return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedProcessDLAGSyncMsg      .                       */
/*                                                                           */
/* Description        : This function process the D-LAG sync message         */
/*                      received from Active node                            */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedProcessDLAGSyncMsg (tRmMsg * pMesg, UINT4 *pu4Offset)
{
    UINT2               u2LaDLAGSubType = 0;
    UINT2               u2DLAGMesgLength = 0;

    /* Get the D-LAG subtype */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2LaDLAGSubType);

    /* Get the length of the D-LAG sync message */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2DLAGMesgLength);

    switch (u2LaDLAGSubType)
    {
        case LA_DLAG_RED_ADD_AGG_INFO:
            /* Called for adding D-LAG Remote Aggregator entry */
            LaDLAGRedProcessAddRemAggEntry (pMesg, pu4Offset);
            break;
        case LA_DLAG_RED_DEL_AGG_INFO:
            /* Called for deleting  D-LAG Remote Aggregator entry */
            LaDLAGRedProcessDelRemAggEntry (pMesg, pu4Offset);
            break;
        case LA_DLAG_RED_DEL_REM_PORT_INFO:
            /* Called for deleting  D-LAG Remote Port entry */
            LaDLAGRedProcessDelRemPortEntry (pMesg, pu4Offset);
            break;
        default:
            return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedCurrWhileTmrBulkSyncUp.                         */
/*                                                                           */
/* Description        : This function syncs up the CurrWhile Timer exp on    */
/*                      getting the Bulk Request message.                    */
/*                                                                           */
/* Input(s)           : u2StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedCurrWhileTmrBulkSyncUp (UINT2 u2StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2PortIndex = 0;
    UINT4               u4PortIndex = 0;
    UINT2               u2BulkUpdPortCnt;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BulkUpdPortCnt = LA_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u2PortIndex = u2StartIfIndex;
         u2PortIndex <= LA_MAX_PORTS && u2BulkUpdPortCnt > 0;
         u2PortIndex++, u2BulkUpdPortCnt--)
    {
        if (LaGetPortEntry (u2PortIndex, &pPortEntry) != LA_SUCCESS)
        {
            continue;
        }

        /* Check the LaRxmState of the port and if it is 
         * LA_RXM_STATE_DEFAULTED just send a current while timer
         * expiry sync up to standby node.
         */
        if (pPortEntry->LaRxmState != LA_RXM_STATE_DEFAULTED)
        {
            continue;
        }

        if (u2BufSize - u4Offset < LA_CURR_SPLIT_TMR_MSG_SIZE)
        {
            /* There is no enough space to fill the information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                 * size of update information so far written. Hence update 
                 * length field at offset u4SyncLengthOffset with the value 
                 * u2SyncMsgLen. */
                LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used as buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = LA_RM_OFFSET;
            u2SyncMsgLen = 0;

            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }

            /* Fill the message type. */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_CURR_SPLIT_TMR_EXP_MSG);

            /* Offset in the buffer where the Length of the Sync Up info
             * needs to be present. */
            u4SyncLengthOffset = u4Offset;

            /* Fill the number of size of  information to be 
             * synced up. Here only zero will be filled. The following
             * macro is called to move the pointer by 2 bytes.*/
            LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
        }

        /* Fill the number of number of port information to be synced up. */
        u4PortIndex = (UINT4) u2PortIndex;
        LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);
        LA_RM_PUT_1_BYTE (pMsg, &u4Offset, LA_CURRWHILE_SPLIT_INTERVALS);

        u2SyncMsgLen += LA_CURR_SPLIT_TMR_MSG_SIZE;
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    /* This is to find the memory required for syncing LaRedCurrWhileTmrBulkSyncUp */
    if (u1MemEstFlag == LA_LACP_SYNC_MSG)
    {
        u4BulkUnitSize =
            sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT1) +
            sizeof (UINT4) + sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "LA", (CHR1 *) "tLaLacPortEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done for LA_LACP_SYNC_MSG, so setting the next 
         * bulk message  */
        u1MemEstFlag = LA_CURR_SPLIT_TMR_EXP_MSG;
    }

}

/*****************************************************************************/
/* Function Name      : LaRedWaitWhileTmrBulkSyncUp.                         */
/*                                                                           */
/* Description        : This function syncs up the WaitWhile Timer exp       */
/*                      getting the Bulk Request message.                    */
/*                                                                           */
/* Input(s)           : u2StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedWaitWhileTmrBulkSyncUp (UINT2 u2StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BulkUpdPortCnt;
    UINT4               u4PortIndex;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BulkUpdPortCnt = LA_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u2PortIndex = u2StartIfIndex;
         u2PortIndex <= LA_MAX_PORTS && u2BulkUpdPortCnt > 0;
         u2PortIndex++, u2BulkUpdPortCnt--)
    {
        if (LaGetPortEntry (u2PortIndex, &pPortEntry) != LA_SUCCESS)
        {
            continue;
        }

        /* Check the LaMuxState of the port and generate wait while
         * timer expiry message for ports that are in any of these 
         * three states (Attached/Collecting/Distributing).
         */
        if ((pPortEntry->LaLacpMode != LA_MODE_LACP) ||
            (pPortEntry->u1PortOperStatus != LA_OPER_UP) ||
            ((pPortEntry->LaMuxState != LA_MUX_STATE_ATTACHED) &&
             (pPortEntry->LaMuxState != LA_MUX_STATE_COLLECTING) &&
             (pPortEntry->LaMuxState != LA_MUX_STATE_DISTRIBUTING)))
        {
            continue;
        }

        if (u2BufSize - u4Offset < LA_WAIT_WHILE_TMR_EXP_MSG_SIZE)
        {
            /* There is no enough space to fill the information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                 * size of update information so far written. Hence update 
                 * length field at offset u4SyncLengthOffset with the value 
                 * u2SyncMsgLen. */
                LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used as buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }

            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = LA_RM_OFFSET;
            u2SyncMsgLen = 0;

            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }

            /* Fill the message type. */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_WAIT_WHILE_TMR_EXP_MSG);

            /* Offset in the buffer where the Length of the Sync Up info
             * needs to be present. */
            u4SyncLengthOffset = u4Offset;

            /* Fill the number of size of  information to be 
             * synced up. Here only zero will be filled. The following
             * macro is called to move the pointer by 2 bytes.*/
            LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
        }

        /* Fill the number of number of port information to be synced up. */
        u4PortIndex = (UINT4) u2PortIndex;
        LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);

        u2SyncMsgLen += LA_WAIT_WHILE_TMR_EXP_MSG_SIZE;
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    /* This is to find the memory required for syncing LaRedWaitWhileTmrBulkSyncUp */
    if (u1MemEstFlag == LA_CURR_SPLIT_TMR_EXP_MSG)
    {
        u4BulkUnitSize =
            sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT2) +
            sizeof (UINT4) + sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "LA", (CHR1 *) "tLaLacPortEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done for LA_CURR_SPLIT_TMR_EXP_MSG, so setting the next 
         * bulk message  */
        u1MemEstFlag = LA_WAIT_WHILE_TMR_EXP_MSG;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedPortOperStatusBulkSyncup                        */
/*                                                                           */
/* Description        : This function sync  up the port oper status  on      */
/*                      getting the Bulk Request message.                    */
/*                                                                           */
/* Input(s)           : u2StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedPortOperStatusBulkSyncup (UINT2 u2StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BulkUpdPortCnt;
    UINT4               u4PortIndex;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BulkUpdPortCnt = LA_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u2PortIndex = u2StartIfIndex;
         u2PortIndex <= LA_MAX_PORTS && u2BulkUpdPortCnt > 0;
         u2PortIndex++, u2BulkUpdPortCnt--)
    {
        if (LaGetPortEntry (u2PortIndex, &pPortEntry) != LA_SUCCESS)
        {
            continue;
        }

        if (pPortEntry->u1PortOperStatus != CFA_IF_UP)
        {
            continue;
        }

        if (u2BufSize - u4Offset < LA_PORT_OPER_STATUS_MSG_SIZE)
        {
            /* There is no enough space to fill the information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                 * size of update information so far written. Hence update 
                 * length field at offset u4SyncLengthOffset with the value 
                 * u2SyncMsgLen. */
                LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used as buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = LA_RM_OFFSET;
            u2SyncMsgLen = 0;

            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }

            /* Fill the message type. */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_PORT_OPER_STATUS_MSG);

            /* Offset in the buffer where the Length of the Sync Up info
             * needs to be present. */
            u4SyncLengthOffset = u4Offset;

            /* Fill the number of size of  information to be 
             * synced up. Here only zero will be filled. The following
             * macro is called to move the pointer by 2 bytes.*/
            LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
        }

        /* Fill the port oper status information to be synced up. */
        u4PortIndex = (UINT4) u2PortIndex;
        LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4PortIndex);
        LA_RM_PUT_1_BYTE (pMsg, &u4Offset, pPortEntry->u1PortOperStatus);

        u2SyncMsgLen += LA_PORT_OPER_STATUS_MSG_SIZE;
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    /* This is to find the memory required for syncing LaRedPortOperStatusBulkSyncup */
    if ((u1MemEstFlag == 1) && (LA_HR_STATUS () != LA_HR_STATUS_DISABLE))

    {
        u4BulkUnitSize =
            sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT1) +
            sizeof (UINT4) + sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "LA", (CHR1 *) "tLaLacPortEntry",
                                    u4BulkUnitSize);
        /* Initilal Memory estimation is done and set the flag as LA_PORT_OPER_STATUS_MSG
         * next bulk message*/
        u1MemEstFlag = LA_PORT_OPER_STATUS_MSG;
    }

}

/*****************************************************************************/
/* Function Name      : LaRedLacpBulkSyncup                                  */
/*                                                                           */
/* Description        : This function sync  up the partner information of    */
/*                      all ports on getting the Bulk Request message.       */
/*                                                                           */
/* Input(s)           : u2StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedLacpBulkSyncup (UINT2 u2StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BulkUpdPortCnt;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BulkUpdPortCnt = LA_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u2PortIndex = u2StartIfIndex;
         u2PortIndex <= LA_MAX_PORTS && u2BulkUpdPortCnt > 0;
         u2PortIndex++, u2BulkUpdPortCnt--)
    {
        if ((LaGetPortEntry (u2PortIndex, &pPortEntry) != LA_SUCCESS)
            || (pPortEntry->pAggEntry == NULL))
        {
            continue;
        }

        /* Get the dynamic information for the ports. If the Receive state
         * machine is in CURRENT state, then get the partner information
         * from the port and store it in the given buffer. */
        if (pPortEntry->LaRxmState != LA_RXM_STATE_CURRENT)
        {
            if (!
                ((pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED)
                 && (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_DOWN)
                 && (pPortEntry->LaLacpMode != LA_MODE_DISABLED)))
            {
                continue;
            }
        }

        if (u2BufSize - u4Offset < LA_SYNC_PORT_MSG_SIZE)
        {
            /* There is no enough space to fill this port information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                 * size of update information so far written. Hence update 
                 * length field at offset u4SyncLengthOffset with the value 
                 * u2SyncMsgLen. */
                LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used as buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = LA_RM_OFFSET;
            u2SyncMsgLen = 0;

            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                return;
            }

            /* Fill the message type. */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_LACP_SYNC_MSG);

            /* Offset in the buffer where the Length of the Sync Up info
             * needs to be present. */
            u4SyncLengthOffset = u4Offset;

            /* Fill the number of size of  information to be 
             * synced up. Here only zero will be filled. The following
             * macro is called to move the pointer by 2 bytes.*/
            LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
        }

        /* 
         * Copy the Port's (ID, ActorKey, Partner_Oper_Port_State.Sync) and
         * the partner information from the received pdu into the linear buf.
         */
        LaRedFormSyncMsgForPort (pPortEntry, &(pPortEntry->LaLacPartnerInfo),
                                 &u4Offset, &pMsg);

        LaRedCopyPartnerInfoIntoRMBuf (&(pPortEntry->LaLacActorInfo), &u4Offset,
                                       &pMsg);

        u2SyncMsgLen += LA_SYNC_PORT_MSG_SIZE;
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    /* This is to find the memory required for syncing LaRedLacpBulkSyncup */
    if (u1MemEstFlag == LA_PORT_OPER_STATUS_MSG)
    {
        u4BulkUnitSize = sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT2) +
            sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "LA", (CHR1 *) "tLaLacPortEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done for LA_PORT_OPER_STATUS_MSG, so set the  
         * next bulk message*/
        u1MemEstFlag = LA_LACP_SYNC_MSG;
    }
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaRedPortHwStatusBulkSyncUp.                         */
/*                                                                           */
/* Description        : This function syncs up the port's Hw status  on      */
/*                      getting the Bulk Request message.                    */
/*                                                                           */
/* Input(s)           : u2StartIfIndex                                       */
/*                                                                           */
/* Output(s)          : Sends RM message to the standby node                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedPortHwStatusBulkSyncUp (UINT2 u2StartIfIndex)
{
    /* Send Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4SyncLengthOffset = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2SyncMsgLen = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BulkUpdPortCnt;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    u2BulkUpdPortCnt = LA_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u2PortIndex = u2StartIfIndex;
         u2PortIndex <= LA_MAX_PORTS && u2BulkUpdPortCnt > 0;
         u2PortIndex++, u2BulkUpdPortCnt--)
    {
        if (LaGetPortEntry (u2PortIndex, &pPortEntry) != LA_SUCCESS)
        {
            continue;
        }

        if (u2BufSize - u4Offset < LA_PORT_HW_STATUS_SYNCUP_MSG_SIZE)
        {
            /* There is no enough space to fill the information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Send the existing message to RM. */
                /* Offset of the Length field for update information is
                 * stored in u4SyncLengthOffset. u2SyncMsgLen contains the
                 * size of update information so far written. Hence update 
                 * length field at offset u4SyncLengthOffset with the value 
                 * u2SyncMsgLen. */
                LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used as buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = LA_RM_OFFSET;
            u2SyncMsgLen = 0;

            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }

            /* Fill the message type. */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_PORT_HW_STATUS_SYNCUP_MSG);

            /* Offset in the buffer where the Length of the Sync Up info
             * needs to be present. */
            u4SyncLengthOffset = u4Offset;

            /* Fill the number of size of  information to be 
             * synced up. Here only zero will be filled. The following
             * macro is called to move the pointer by 2 bytes.*/
            LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2SyncMsgLen);
        }

        /* 
         * Copy the Port's HW status into the linear buffer
         */

        LA_RM_PUT_4_BYTE (pMsg, &u4Offset, ((UINT4) u2PortIndex));
        LA_RM_PUT_1_BYTE (pMsg, &u4Offset, pPortEntry->LaLacHwPortStatus);

        u2SyncMsgLen += LA_PORT_HW_STATUS_SYNCUP_MSG_SIZE;
    }

    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Fill the message length at proper offset. */
        LA_RM_PUT_2_BYTE (pMsg, &u4SyncLengthOffset, u2SyncMsgLen);

        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    /* This is to find the memory required for syncing LaRedPortHwStatusBulkSyncUp */
    if (u1MemEstFlag == LA_WAIT_WHILE_TMR_EXP_MSG)
    {
        u4BulkUnitSize = sizeof (UINT2) + sizeof (UINT2) + sizeof (UINT2) +
            sizeof (UINT1) + sizeof (UINT4) + sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "LA", (CHR1 *) "tLaLacPortEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done for LA_WAIT_WHILE_TMR_EXP_MSG, so set the  
         * next bulk message*/
        u1MemEstFlag = LA_PORT_HW_STATUS_SYNCUP_MSG;
    }

}
#endif /* NPAPI_WANTED */
/*****************************************************************************/
/* Function Name      : LaRedAggBulkSyncUp.                                  */
/*                                                                           */
/* Description        : This function syncs up the information stored in     */
/*                      LaHwEntry to standby.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedAggBulkSyncUp (VOID)
{
    /* Send NP level Bulk Update messages to the standby. */
    tRmMsg             *pMsg = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2AggMsgSize = 0;
    UINT2               u2BufSize = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if (pAggEntry->u1DistributingPortCount == 0)
        {
            /* Since no port is active in this aggregation, the aggregation
             * will not be known to hw, hence move to next aggregator.*/
            LaGetNextAggEntry (pAggEntry, &pAggEntry);
            continue;
        }

        /* Get the size of message this aggregator will require. */
        u2AggMsgSize = LaRedGetAggBulkMsgSize (pAggEntry->u2AggIndex);

        if (u2BufSize - u4Offset < u2AggMsgSize)
        {
            /* There is no enough space to fill this agg information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used a buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }
            }

            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = 0;

            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }

            /* Fill the message type. */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_NP_SYNC_MSG);

        }

        /* Fill the agg information for this aggregation. */
        LaRedSyncInfoForOneAgg (pMsg, &u4Offset, pAggEntry->u2AggIndex);

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    /* This is to find the memory required for syncing LaRedAggBulkSyncUp */
    if (u1MemEstFlag == LA_PORT_HW_STATUS_SYNCUP_MSG)
    {
        u4BulkUnitSize = sizeof (UINT4);
        /* updating the sizing structure for the data which are stored in file */
        IssSzUpdateSizingInfoForHR ((CHR1 *) "LA", (CHR1 *) "tLaLacAggEntry",
                                    u4BulkUnitSize);
        /* Memory estimation are done for LA_PORT_HW_STATUS_SYNCUP_MSG, so set the  
         * next bulk message*/
        u1MemEstFlag = LA_NP_SYNC_MSG;
    }
    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      :  LaDLAGRedAggBulkSyncUp                              */
/*                                                                           */
/* Description        : This function send  DLAG bulk sync informaton        */
/*                      to standby.                                          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaDLAGRedAggBulkSyncUp (VOID)
{
    tRmMsg             *pMsg = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = 0;
    UINT2               u2AggMsgSize = 0;
    UINT2               u2BufSize = 0;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        /* Get the size of message this aggregator will require. */
        u2AggMsgSize = LaRedGetAggBulkMsgSize (pAggEntry->u2AggIndex);

        if ((u2BufSize - (UINT2) u4Offset) < u2AggMsgSize)
        {
            /* There is no enough space to fill this agg information
             * in to the buffer. Hence send the existing message and
             * construct new message to fill the information of other 
             * ports.*/
            if (pMsg != NULL)
            {
                /* Note u4Offset contains the number of bytes written in to
                 * the buffer. Hence can be used a buffer size here. */
                if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
                {
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    LaRmHandleProtocolEvent (&ProtoEvt);
                }
            }
            /* Reset the offset, size fields so that they can be used in the
             * new buffer. */
            u4Offset = 0;
            u2BufSize = LA_BULK_SPIT_MSG_SIZE;

            /* Allocate memory for data to be sent to RM. This memory 
             * will be freed by RM after Txmitting  */
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                LaRmHandleProtocolEvent (&ProtoEvt);
                return;
            }
            /* If the D-LAG status is disabled, return */
            if (pAggEntry->u1DLAGStatus != LA_DLAG_ENABLED)
            {
                RM_FREE (pMsg);
                return;
            }
            /* Fill the message type. the message type will be LA_DLAG_SYNC_MSG */
            LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_DLAG_SYNC_MSG);
        }

        /* Fill the DLAG sync information for this aggregator */
        LaDLAGRedSyncInfoPerAgg (pMsg, &u4Offset, pAggEntry->u2AggIndex);
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
    /* Send the last message. */
    if (pMsg != NULL)
    {
        /* Note u4Offset contains the number of bytes written in to
         * the buffer. Hence can be used a buffer size here. */
        if (LaRedSendMsgToRm (pMsg, (UINT2) u4Offset) == LA_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            LaRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedSendAddRemoteAggorPortEntry                  */
/*                                                                           */
/* Description        : This function sends trigger message to standby       */
/*                      to add D-LAG remote aggregator entry                 */
/*                                                                           */
/* Input(s)           : Aggregatore entry                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS, LA_FAILURE                               */
/*****************************************************************************/
INT4
LaDLAGRedSendAddRemoteAggorPortEntry (tLaLacAggEntry * pAggEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2BufSize = 0;

    if (LA_IS_STANDBY_UP () == LA_FALSE)
    {
        LA_TRC (LA_RED_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }
    if (pAggEntry == NULL)
        return LA_FAILURE;
    /* Maximum Aggregator message size */
    u2BufSize = LA_DLAG_MAX_AGG_MSG_SIZE + sizeof (tLaPortList);

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }
    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_DLAG_SYNC_MSG);

    /* Sync up the Information with standby for each aggregator */
    LaDLAGRedSyncInfoPerAgg (pMsg, &u4Offset, pAggEntry->u2AggIndex);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedSendDelRemoteAggEntry                       */
/*                                                                           */
/* Description        : This function sends trigger message to standby       */
/*                      to delete D-LAG remote aggregator entry              */
/*                                                                           */
/* Input(s)           : Aggregator Entry                                     */
/*                      Remote Aggregator entry                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT4
LaDLAGRedSendDelRemoteAggEntry (tLaLacAggEntry * pAggEntry,
                                tLaDLAGRemoteAggEntry * pRemAggEntry)
{
    tRmMsg             *pMsg = NULL;
    tLaMacAddr          LaMacAddr = { 0 };
    UINT4               u4Offset = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2AggMsgLen = 0;
    UINT4               u4AggIndex = 0;

    if (LA_IS_STANDBY_UP () == LA_FALSE)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    if ((pAggEntry == NULL) || (pRemAggEntry == NULL))
        return LA_FAILURE;

    u2BufSize = LA_RED_LENGTH_SIZE + LA_DLAG_SYNC_MSG_SIZE +
        DLAG_SUB_TYPE_SIZE + LA_DLAG_DEL_AGG_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }
    /* 
     * This is a Sync message, and the size of the information to be sent for
     * the aggregator is constant and is given by LA_DLAG_DEL_AGG_MSG_SIZE
     * which equals to 10 [(Port Channel Index (4)+ MAC address (6)] */
    u2AggMsgLen = LA_DLAG_DEL_AGG_MSG_SIZE;

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_DLAG_SYNC_MSG);

    /* Fill the subtype as Delete Agg Info. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, LA_DLAG_RED_DEL_AGG_INFO);

    /* Fill the length field. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2AggMsgLen);

    /* Copy the  port channel index */
    u4AggIndex = (UINT4) pAggEntry->u2AggIndex;
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4AggIndex);

    /* Copy the remote D-LAG system mac address */
    LA_MEMCPY (LaMacAddr, pRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    LA_RM_PUT_N_BYTE (pMsg, LaMacAddr, &u4Offset, LA_MAC_ADDRESS_SIZE);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedSendDelRemotePortEntry                      */
/*                                                                           */
/* Description        : This function sends trigger message to standby       */
/*                      to delete  D-LAG remote port entry                   */
/*                                                                           */
/* Input(s)           : Remote Aggregator entry                              */
/*                      Remote Port entry                                    */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT4
LaDLAGRedSendDelRemotePortEntry (tLaDLAGRemoteAggEntry * pRemAggEntry,
                                 tLaDLAGRemotePortEntry * pRemPortEntry)
{
    tRmMsg             *pMsg = NULL;
    tLaMacAddr          LaMacAddr = { 0 };
    UINT4               u4Offset = 0;
    UINT4               u4Val = 0;
    UINT2               u2BufSize = 0;
    UINT4               u4AggIndex = 0;
    UINT2               u2MsgLen = 0;

    if (LA_IS_STANDBY_UP () == LA_FALSE)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LA: Red sync up msgs can not be sent if"
                " the standby node is down.\n");
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return LA_SUCCESS;
    }

    if ((pRemAggEntry == NULL) || (pRemPortEntry == NULL))
        return LA_FAILURE;

    u2BufSize = LA_RED_LENGTH_SIZE + LA_DLAG_SYNC_MSG_SIZE +
        DLAG_SUB_TYPE_SIZE + LA_DLAG_DEL_PORT_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }
    /* 
     * This is a Sync message, and the size of the information to be sent for
     * the for the remote port entry  is constant and is given by LA_DLAG_DEL_PORT_MSG_SIZE
     * which equals to 14 [(Port Channel Index (4)+ MAC address (6) + Remote Port Index(4)] */
    u2MsgLen = LA_DLAG_DEL_PORT_MSG_SIZE;

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_DLAG_SYNC_MSG);

    /* Fill the subtype as Delete Remote Port Info. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, LA_DLAG_RED_DEL_REM_PORT_INFO);

    /* Fill the length field. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    if (pRemAggEntry->pAggEntry == NULL)
    {
        RM_FREE (pMsg);
        return LA_FAILURE;
    }
    /* Copy the aggregator index */
    u4AggIndex = (UINT4) pRemAggEntry->pAggEntry->u2AggIndex;
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4AggIndex);

    /* Copy the remote D-LAG system priority */
    LA_MEMCPY (LaMacAddr, pRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    LA_RM_PUT_N_BYTE (pMsg, LaMacAddr, &u4Offset, LA_MAC_ADDRESS_SIZE);

    /* Fill the remote port index */
    u4Val = pRemPortEntry->u4PortIndex;
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Val);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedGetAggBulkMsgSize.                            */
/*                                                                           */
/* Description        : This function finds out the size of information to   */
/*                      be synced up for a given aggregator.                 */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggregator Id.                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Size of message.                                     */
/*****************************************************************************/
UINT2
LaRedGetAggBulkMsgSize (UINT2 u2AggIndex)
{
    UINT2               u2AggMsgSize = 0;

    UNUSED_PARAM (u2AggIndex);

    /* 
     * Size = sizeof NP subtype field + length field + sizeof aggId 
     *        + sizeof HwAggId 
     */
    u2AggMsgSize =
        LA_NP_SUB_TYPE_SIZE + LA_RED_LENGTH_SIZE + 2 * LA_PORT_ID_SIZE;

    return u2AggMsgSize;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncInfoForOneAgg.                            */
/*                                                                           */
/* Description        : This function gets the active ports in hw and the    */
/*                      hw agg id and writes them into the given input buf.  */
/*                      Note: No hw read is done here.                       */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                      u2AggIndex - Aggregator Id.                          */
/*                                                                           */
/* Output(s)          : pMsg - Rm buffer after filling the information.      */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  next write has to be done.               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedSyncInfoForOneAgg (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 u2AggIndex)
{
    tLaHwSyncInfo      *pLaHwEntry = NULL;
    UINT4               u2AggMsgLen = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4AggIndex;

    if ((u2AggIndex <= LA_MAX_PORTS) ||
        (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
        return;

    pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);

    /* Fill the subtype as Add Info. */
    LA_RM_PUT_2_BYTE (pMsg, pu4Offset, LA_NP_ADD_INFO);

    u4MsgLenOffset = *pu4Offset;

    /* First put the Message length for this message. This will be
     * zero first. At the end of this function, the correct value will
     * be inserted. */
    LA_RM_PUT_2_BYTE (pMsg, pu4Offset, u2AggMsgLen);

    u4AggIndex = (UINT4) u2AggIndex;
    LA_RM_PUT_4_BYTE (pMsg, pu4Offset, u4AggIndex);
    u2AggMsgLen += LA_PORT_ID_SIZE;

    u4AggIndex = (UINT4) (pLaHwEntry->u2HwAggIdx);
    LA_RM_PUT_4_BYTE (pMsg, pu4Offset, u4AggIndex);
    u2AggMsgLen += LA_PORT_ID_SIZE;

    /* Fill the length field. */
    LA_RM_PUT_2_BYTE (pMsg, &u4MsgLenOffset, u2AggMsgLen);

    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedSyncInfoPerAgg                              */
/*                                                                           */
/* Description        : This function is used for sync up of remote          */
/*                      aggregatpr information  with the standby             */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                      u2AggIndex - Aggregator Id.                          */
/*                                                                           */
/* Output(s)          : pMsg - Rm buffer after filling the information.      */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  next write has to be done.               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaDLAGRedSyncInfoPerAgg (tRmMsg * pMsg, UINT4 *pu4Offset, UINT2 u2AggIndex)
{
    tLaMacAddr          LaMacAddr = { 0 };
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    UINT4               u2AggMsgLen = 0;
    UINT4               u4MsgLenOffset = 0;
    UINT4               u4PortCountOffset = 0;
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;
    UINT1               u1PortCount = 0;
    UINT4               u4AggIndex;

    if ((u2AggIndex <= LA_MAX_PORTS) ||
        (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
        return;
    /* Get the aggregator entry using aggIndex */
    LaGetAggEntry (u2AggIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return;
    }
    /* Fill the subtype as LA_DLAG_RED_ADD_AGG_INFO */
    LA_RM_PUT_2_BYTE (pMsg, pu4Offset, LA_DLAG_RED_ADD_AGG_INFO);

    u4MsgLenOffset = *pu4Offset;

    /* First put the Message length for this message. This will be
     * zero first. At the end of this function, the correct value will
     * be inserted. */
    LA_RM_PUT_2_BYTE (pMsg, pu4Offset, u2AggMsgLen);

    u4AggIndex = (UINT4) u2AggIndex;
    /* Fill the aggregator index */
    LA_RM_PUT_4_BYTE (pMsg, pu4Offset, u4AggIndex);
    u2AggMsgLen += LA_FOUR_BYTE;

    LA_RM_PUT_N_BYTE (pMsg, pAggEntry->DLAGDistributingOperPortList,
                      pu4Offset, sizeof (tLaPortList));
    u2AggMsgLen += sizeof (tLaPortList);

    u4Val = pAggEntry->u4DLAGDistributingPortListCount;
    LA_RM_PUT_4_BYTE (pMsg, pu4Offset, u4Val);
    u2AggMsgLen += LA_FOUR_BYTE;

    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
    while (pRemoteAggEntry != NULL)
    {
        /* Copy the remote D-LAG system MAC address */
        LA_MEMCPY (LaMacAddr, pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        LA_RM_PUT_N_BYTE (pMsg, LaMacAddr, pu4Offset, LA_MAC_ADDRESS_SIZE);
        u2AggMsgLen += LA_MAC_ADDRESS_SIZE;

        /* Copy the remote D-LAG system priority */
        u2Val = pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority;
        LA_RM_PUT_2_BYTE (pMsg, pu4Offset, u2Val);
        u2AggMsgLen += LA_TWO_BYTE;

        /* Copy D-LAG keep alive count */
        u4Val = pRemoteAggEntry->u4DLAGKeepAliveCount;
        LA_RM_PUT_4_BYTE (pMsg, pu4Offset, u4Val);
        u2AggMsgLen += LA_FOUR_BYTE;

        /* Copy Role Played */
        u1Val = pRemoteAggEntry->u1DLAGRolePlayed;
        LA_RM_PUT_1_BYTE (pMsg, pu4Offset, u1Val);
        u2AggMsgLen += LA_ONE_BYTE;

        /* Copy the port count filed address, skip port count field */
        u4PortCountOffset = *pu4Offset;
        LA_RM_PUT_1_BYTE (pMsg, pu4Offset, u1PortCount);

        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
        while (pRemotePortEntry != NULL)
        {
            u1PortCount++;
            /* Copy Remote Port Index */
            u4Val = pRemotePortEntry->u4PortIndex;
            LA_RM_PUT_4_BYTE (pMsg, pu4Offset, u4Val);
            u2AggMsgLen += LA_FOUR_BYTE;

            /* Copy  Remote Port Bundle state */
            u1Val = pRemotePortEntry->u1BundleState;
            LA_RM_PUT_1_BYTE (pMsg, pu4Offset, u1Val);
            u2AggMsgLen += LA_ONE_BYTE;

            /* Copy  Remote Port Sync state */
            u1Val = pRemotePortEntry->u1SyncState;
            LA_RM_PUT_1_BYTE (pMsg, pu4Offset, u1Val);
            u2AggMsgLen += LA_ONE_BYTE;

            LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                          &pRemotePortEntry);
        }
        /* Copy the number of Ports count */
        LA_RM_PUT_1_BYTE (pMsg, &u4PortCountOffset, u1PortCount);
        u2AggMsgLen += LA_ONE_BYTE;

        LaDLAGGetNextRemoteAggEntry (pAggEntry, pRemoteAggEntry,
                                     &pRemoteAggEntry);
    }
    /* Fill the aggregator role played field, since this value
     * of this object is assigned dynamically, we have to send the Role Played field*/
    u1Val = pAggEntry->u1DLAGRolePlayed;
    LA_RM_PUT_1_BYTE (pMsg, pu4Offset, u1Val);
    u2AggMsgLen += LA_ONE_BYTE;

    /* Fill the length field. */
    LA_RM_PUT_2_BYTE (pMsg, &u4MsgLenOffset, u2AggMsgLen);
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaRedPerformAudit.                                   */
/*                                                                           */
/* Description        : This function spawns the audit task. Audit task      */
/*                      audits the information present in sw and the         */
/*                      information present in hw and syncs hw with that of  */
/*                      sw.                                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedPerformAudit (VOID)
{
    INT4                i4RetVal;

    /* Set the audit stop flag to false. Based on this flag audit task will 
     * decide wheather to run or not. */
    LA_RED_IS_AUDIT_STOP () = LA_FALSE;

    if (LA_RED_AUDIT_TSKID () == LA_INIT_VAL)
    {
        /* Doing audit for the first time */
        i4RetVal = LA_CREATE_TASK (LA_AUDIT_TASK,
                                   LA_AUDIT_TASK_PRIORITY,
                                   OSIX_DEFAULT_STACK_SIZE,
                                   (OsixTskEntry) LaRedAuditTaskMain,
                                   0, &LA_RED_AUDIT_TSKID ());

        if (i4RetVal != OSIX_SUCCESS)
        {
            LA_TRC (ALL_FAILURE_TRC, "LA AUDIT Task creation failed. \n");

            LA_RED_IS_AUDIT_STOP () = LA_TRUE;
            return;
        }

    }
}

/*****************************************************************************/
/* Function Name      : LaRedAuditTaskMain.                                  */
/*                                                                           */
/* Description        : This function audits the information present in sw   */
/*                      and the information present in hw and syncs hw with  */
/*                      that of sw.                                          */
/*                                                                           */
/* Input(s)           : pi1Param - Unused parameter.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedAuditTaskMain (INT1 *pi1Param)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    tLaRedPortsInSwAgg  aLaRedPortsInAgg[LA_MAX_PORTS_PER_AGG];
    tLaHwInfo           HwInfo;
    tLaHwInfo          *pHwInfo;
    INT4                i4RetVal;
    UINT2               u2Index = 0;
    UINT2               u2AggIndex;
    INT4                i4HwAggIndex = -1;
    UINT2               u2Port;

    UNUSED_PARAM (pi1Param);

    /* If GetFirst and GetNext Aggregator information is possible in Hw, then
     * make this loop as loop on existing aggregations in Hw. And in this as
     * once this loop is over, findout the sw aggregations that are not 
     * covered in this loop and program those aggregations in Hw. */

    /* For every aggregator comparison take  lock before starting comparison 
     * and release lock once the comparison and the corresponding action is 
     * over. */
    LA_LOCK ();
    if (LA_RED_IS_AUDIT_STOP () == LA_TRUE)
    {
        LA_RED_AUDIT_TSKID () = 0;
        LA_UNLOCK ();
        return;
    }

    pHwInfo = &HwInfo;
    MEMSET (pHwInfo, 0, sizeof (tLaHwInfo));

    LA_HW_LOOP_ON_AGGREGATORS (i4HwAggIndex, pHwInfo)
    {
        i4RetVal = LaRedGetSwAggIndex ((UINT2) i4HwAggIndex, &u2AggIndex);

        if (i4RetVal == LA_FAILURE)
        {
            /* Agg may have ports in HW, instead of deleting the Agg from the
             * HW, we can update the HwAggIdx into the datastructure in the SW
             * This may happen if LA_NP_ADD_INFO message doesnt reach the 
             * standby */
            if (LaRedUpdateSwFromAggInHw (i4HwAggIndex, pHwInfo) != LA_SUCCESS)
            {
                /* There is no sw index for the corresponding hw index,
                 * hence delete this aggregation from hw. */

                LaRedDeleteAggregatorFromHw ((UINT2) i4HwAggIndex);
                LA_UNLOCK ();
                LA_LOCK ();
                if (LA_RED_IS_AUDIT_STOP () == LA_TRUE)
                {
                    LA_RED_AUDIT_TSKID () = 0;
                    LA_UNLOCK ();
                    return;
                }
            }
            continue;
        }

        LaGetAggEntry (u2AggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            /* Aggregation doesn't exist in software 
             * active(in DISTRIBUTING state) ports is zero. 
             * And the aggregation is active in hardware. */

            LaRedDeleteAggregatorFromHw ((UINT2) i4HwAggIndex);
            LA_UNLOCK ();
            LA_LOCK ();
            if (LA_RED_IS_AUDIT_STOP () == LA_TRUE)
            {
                LA_RED_AUDIT_TSKID () = 0;
                LA_UNLOCK ();
                return;
            }
            continue;
        }

        if (pAggEntry->u1DistributingPortCount == 0)
        {
            if (pHwInfo->u1NumPorts != 0)
            {
                LA_GET_NEXT_PORT_FROM_HWAGG (u2Index, u2Port, pHwInfo)
                {
                    /* Port is in hardware aggregation, but not in software. Hence
                     * remove the port from hardware aggregation. */

                    LaFsLaHwRemoveLinkFromAggGroup (u2AggIndex, u2Port);
                }
            }
            LA_UNLOCK ();
            LA_LOCK ();
            if (LA_RED_IS_AUDIT_STOP () == LA_TRUE)
            {
                LA_RED_AUDIT_TSKID () = 0;
                LA_UNLOCK ();
                return;
            }
            continue;
        }

        LA_MEMSET (aLaRedPortsInAgg, 0,
                   (LA_MAX_PORTS_PER_AGG * sizeof (tLaRedPortsInSwAgg)));

        /* There are some active ports for this port channel in sw. */

        u2Index = 0;
        pPortEntry = NULL;

        /* Find out the active ports and store it in the aLaRedPortsInAgg for 
         * the corresponding aggregation. */
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);

        while (pNextPortEntry != NULL)
        {
            if (pNextPortEntry->LaLacActorInfo.
                LaLacPortState.LaDistributing == LA_TRUE)
            {
                aLaRedPortsInAgg[u2Index].u2Port = pNextPortEntry->u2PortIndex;
                aLaRedPortsInAgg[u2Index].u2HwSync = LA_FALSE;
                u2Index++;
            }

            pPortEntry = pNextPortEntry;
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);
        }

        /* Sync this aggregation information with hardware. */
        LaRedSyncAggPortListWithHw (pAggEntry->u2AggIndex, aLaRedPortsInAgg,
                                    u2Index, (UINT1)
                                    pAggEntry->AggConfigEntry.LinkSelectPolicy,
                                    pHwInfo);

        /* This aggregator is audited, hence mark it. */
        pAggEntry->u1IsAggAudited = LA_TRUE;

        LA_UNLOCK ();
        LA_LOCK ();
        if (LA_RED_IS_AUDIT_STOP () == LA_TRUE)
        {
            LA_RED_AUDIT_TSKID () = 0;
            LA_UNLOCK ();
            return;
        }
    }

    /* Now already the lock is taken, hence release the lock after while 
     * loop. */
    /* Max number of aggregations that are not present in hw, but in sw can 
     * be 1 or 2. Hence complete the following loop and then unlock. */
    LaGetNextAggEntry (NULL, &pAggEntry);

    while (pAggEntry != NULL)
    {
        /* Find out the aggregators that are not in hw, but present in sw
         * and program them in hw. For this it is better to add a flag in 
         * TmpAggEntry to check that whether it is synced with hw or not.*/
        if ((pAggEntry->u1DistributingPortCount != 0) &&
            (pAggEntry->u1IsAggAudited == LA_FALSE))
        {
            /* This aggregator is in sw, but not in hw. Hence program this into
             * hardware. */
            LaRedAddAggregatorToHw (pAggEntry);

        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    /* If Active crashes before sending LA_NP_DEL_PORT_SUCCESS_INFO
     * message, then it will not be detetced by the HW Audit, because
     * port will not be in HW and Distributing will also be FALSE for that
     * port, but the LaLacHwPortStatus is set to LA_HW_PORT_ADDED_IN_LAGG 
     * Following Loop detects this scenario and makes the LaLacHePortStatus
     * to LA_HW_PORT_NOT_IN_LAGG*/

    for (u2Index = LA_MIN_PORTS; u2Index <= LA_MAX_PORTS; u2Index++)
    {
        LaGetPortEntry (u2Index, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry->LaLacActorInfo.
            LaLacPortState.LaDistributing == LA_FALSE &&
            pPortEntry->LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG)
        {

            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
            if (pPortEntry->pAggEntry == NULL)
            {
                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);
            }
        }
    }

    LA_RED_AUDIT_TSKID () = 0;
    LA_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : LaRedUpdateSwFromAggInHw                             */
/*                                                                           */
/* Description        : This function updates the Agg datastructure in the   */
/*                      SW from the info in the HW                           */
/*                                                                           */
/* Input(s)           : AggIndex & Hw infofor the agg                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaRedUpdateSwFromAggInHw (INT4 i4HwAggIndex, tLaHwInfo * pHwInfo)
{
    tLaLacPortEntry    *pPortEntry = NULL, *pNextPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaHwSyncInfo      *pLaHwEntry = NULL;
    tLaIntfMesg         LaMsg;    /* Added to send a NP success message to 
                                   LaHandleNPCallbackEvent function */
    UINT2               u2HwAggIndex = (UINT2) i4HwAggIndex;
    UINT1               portIdx;

    if (pHwInfo->u1NumPorts == 0)
    {
        /* if aggregator doesnt have any ports in it, then it will be deleted 
         * from the Hw because we cant find its corresponding SwAggIndex.
         * If any of the port is in distributing state, then Hw Audit will 
         * create the agg in HW and add ports into it */
        return LA_SUCCESS;
    }

    LaGetPortEntry (pHwInfo->au4LPortList[0], &pPortEntry);
    if (pPortEntry == NULL)
    {
        return LA_FAILURE;
    }

    if ((pAggEntry = pPortEntry->pAggEntry) == NULL)
    {
        return LA_FAILURE;
    }

    pLaHwEntry = LA_RED_GET_AGG_ENTRY (pAggEntry->u2AggIndex);
    if (pLaHwEntry == NULL)
    {
        return LA_FAILURE;
    }

    pLaHwEntry->u2HwAggIdx = u2HwAggIndex;
    pLaHwEntry->u1AggStatus = LA_ENABLED;

    /* Enable Aggregator */

    MEMSET (&LaMsg, 0, sizeof (tLaIntfMesg));
    LaMsg.uLaIntfMsg.LaNpCbMsg.u4NpCallId = AS_FS_LA_HW_CREATE_AGG_GROUP;
    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup.rval =
        FNP_SUCCESS;
    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup.u2AggIndex
        = pAggEntry->u2AggIndex;
    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup.
        u2HwAggIndex = pLaHwEntry->u2HwAggIdx;
    LaHandleNpCallbackEvents (&LaMsg);

    /*Now, check for the port status, if the port is in HW and in SW if it is 
     * not in LA_HW_PORT_ADDED_IN_LAGG then update the SW status */

    for (portIdx = 0; portIdx < pHwInfo->u1NumPorts; portIdx++)
    {
        LaGetPortEntry (pHwInfo->au4LPortList[portIdx], &pPortEntry);
        if (pPortEntry == NULL)
        {
            continue;
        }
        if (pPortEntry->LaLacActorInfo.
            LaLacPortState.LaDistributing == LA_TRUE &&
            pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG)
        {
            LaRedAddSyncPortEntry (LA_NP_ADD_PORT_SUCCESS_INFO,
                                   pAggEntry->u2AggIndex,
                                   pHwInfo->au4LPortList[portIdx]);
        }
    }

    pPortEntry = NULL;
    LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);
    while (pNextPortEntry != NULL)
    {
        if (pNextPortEntry->LaLacActorInfo.
            LaLacPortState.LaDistributing == LA_TRUE &&
            pNextPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG)
        {
            /* indicate hw to add a link to an agg. */
            if (LaFsLaHwAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                           pNextPortEntry->u2PortIndex,
                                           &u2HwAggIndex) == FNP_FAILURE)
            {                    /* For Handling Sync NP case */
                LaHandleLinkAdditionFailed (pAggEntry->u2AggIndex,
                                            pNextPortEntry->u2PortIndex);
            }
            /* In case of synchronus NP case, NP success/
             * failure is handled here */
            LaLinkAddedToAgg (pAggEntry->u2AggIndex,
                              pNextPortEntry->u2PortIndex);

        }
        pPortEntry = pNextPortEntry;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);
    }

    /* This aggregator is audited, hence mark it. */
    pAggEntry->u1IsAggAudited = LA_TRUE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedAddAggregatorToHw.                              */
/*                                                                           */
/* Description        : This function adds the aggregator to the hardware.   */
/*                      This function will be called if in audit, an active  */
/*                      aggregator is found in sw, but not in hw.            */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to sw agg entry.                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedAddAggregatorToHw (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    tLaLacAggConfigEntry *pAggConfigEntry = NULL;
    tLaHwSyncInfo      *pLaHwEntry = NULL;
    UINT2               u2HwAggId;

    LaFsLaRedHwNpDeleteAggregator (pAggEntry->u2AggIndex);

    if (LaFsLaHwCreateAggGroup (pAggEntry->u2AggIndex, &u2HwAggId) ==
        FNP_FAILURE)
    {
        return;
    }

    pAggConfigEntry = &(pAggEntry->AggConfigEntry);
    pLaHwEntry = LA_RED_GET_AGG_ENTRY (pAggEntry->u2AggIndex);

    if (LaFsLaHwSetSelectionPolicyBitList (pAggEntry->u2AggIndex,
                                           pAggConfigEntry->
                                           u4LinkSelectPolicyBitList) ==
        FNP_FAILURE)
    {
        return;
    }

    /* Find out the active ports and store it in the aLaHwAggEntry for the
     * corresponding aggregation. */
    LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);

    /* If adding a list of ports is possible for a chipset, then form the 
     * portlist and directly program the chipset instead adding the ports
     * one by one to the hw aggregator. */
    while (pNextPortEntry != NULL)
    {
        if (pNextPortEntry->LaLacActorInfo.
            LaLacPortState.LaDistributing == LA_TRUE)
        {

            if (LaFsLaHwAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                           pNextPortEntry->u2PortIndex,
                                           &u2HwAggId) == FNP_FAILURE)
            {
                LaHandleLinkAdditionFailed (pAggEntry->u2AggIndex,
                                            pNextPortEntry->u2PortIndex);

            }

            /* In case of synchronus NP case, NP success/
             * failure is handled here */
            LaLinkAddedToAgg (pAggEntry->u2AggIndex,
                              pNextPortEntry->u2PortIndex);
        }

        pPortEntry = pNextPortEntry;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);
    }

    pLaHwEntry->u2HwAggIdx = u2HwAggId;
    pLaHwEntry->u1AggStatus = LA_ENABLED;
    return;
}

/*****************************************************************************/
/* Function Name      : LaRedDeleteAggregatorFromHw.                         */
/*                                                                           */
/* Description        : This function removes the aggregator from the        */
/*                      hardware. This function will be called if in audit,  */
/*                      an active aggregator is found in hw, but not in sw.  */
/*                                                                           */
/* Input(s)           : u2HwAggIndex - HwIndex of the aggregator.            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedDeleteAggregatorFromHw (UINT2 u2HwAggIndex)
{
    tLaHwSyncInfo      *pLaHwEntry;

    pLaHwEntry = &(gLaRedGlobalInfo.aLaRedHwAggInfo[u2HwAggIndex]);

    /* For certain devices we have to remove all physical ports from
     * aggregator and then only we can delete the aggregator. For
     * such devices, get the hw information and then remove the ports
     * from aggregator and then delete aggregator. */
    LaFsLaHwCleanAndDeleteAggregator (u2HwAggIndex);

    pLaHwEntry->u2HwAggIdx = LA_INVALID_HW_AGG_IDX;
    pLaHwEntry->u1AggStatus = LA_DISABLED;
}

/*****************************************************************************/
/* Function Name      : LaRedGetSwAggIndex.                                  */
/*                                                                           */
/* Description        : This function gets the sw agg index for the given    */
/*                      hw agg index.                                        */
/*                                                                           */
/* Input(s)           : u2HwAggIndex - Hw Agg Id.                            */
/*                                                                           */
/* Output(s)          : pu2SwAggIndex - Sw Aggregator Id for the given hw    */
/*                                      hw agg id.                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedGetSwAggIndex (UINT2 u2HwAggIndex, UINT2 *pu2SwAggIndex)
{
    tLaHwSyncInfo      *pLaHwEntry = NULL;
    UINT2               u2AggIndex;
    UINT2               u2MaxAggId;

    /* In case of bcm 
     * *pu2SwAggIndex = u2HwAggIndex + LA_HW_MIN_AGG_INDEX;
     */
    u2MaxAggId = (LA_MAX_PORTS + 1) + LA_HW_MAX_AGG_GRPS;
    for (u2AggIndex = (LA_MAX_PORTS + 1); u2AggIndex <= u2MaxAggId;
         u2AggIndex++)
    {
        pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);
        if ((pLaHwEntry->u1AggStatus == LA_ENABLED) &&
            (pLaHwEntry->u2HwAggIdx == (UINT1) u2HwAggIndex))
        {
            *pu2SwAggIndex = u2AggIndex;
            return LA_SUCCESS;
        }
    }
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncAggPortListWithHw.                          */
/*                                                                           */
/* Description        : This function syncs the hardware and software for    */
/*                      for the given aggregation.                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggregator Id.                          */
/*                      pSwPortList - Active ports in sw aggregation.        */
/*                      u2SyncMsgLen - No. of active ports in sw aggregation */
/*                      u1LinkSelectPolicy - Selection policy in sw agg.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE.                           */
/*****************************************************************************/
VOID
LaRedSyncAggPortListWithHw (UINT2 u2AggIndex,
                            tLaRedPortsInSwAgg * pSwPortList,
                            UINT2 u2SwNumPorts, UINT4 u4LinkSelectPolicyBitList,
                            tLaHwInfo * pHwInfo)
{

    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2Port;
    UINT2               u2SyncNumPorts = 0;
    UINT2               u2NumPorts = 0;
    UINT2               u2Index = 0;
    UINT2               u2HwAggIndex;

    if (FsRedGetSelPolicy (pHwInfo) != u4LinkSelectPolicyBitList)
    {
        /* Set the port channel status to disabled */
        LaUpdatePortChannelStatusToHw (u2AggIndex, LA_PORT_DISABLED, LA_FALSE);

        /* Selection policy in hardware and software for this aggregation is
         * different. */
        LaFsLaHwSetSelectionPolicyBitList (u2AggIndex,
                                           u4LinkSelectPolicyBitList);

        /* Update the actual staus of port-channel to h/w */
        LaUpdatePortChannelStatusToHw (u2AggIndex, LA_PORT_DISABLED, LA_TRUE);
    }

    LA_GET_NEXT_PORT_FROM_HWAGG (u2Index, u2Port, pHwInfo)
    {
        if (LaRedIsHwPortInSwPortList (u2Port, pSwPortList) == LA_FALSE)
        {
            /* Port is in hardware aggregation, but not in software. Hence
             * remove the port from hardware aggregation. */
            if (LaFsLaHwRemoveLinkFromAggGroup (u2AggIndex, u2Port) ==
                FNP_FAILURE)
            {
                LaHandleLinkRemoveFailed (u2AggIndex, u2Port);
            }
            else
            {                    /* If Sync NP call Succeeds */
                if ((LaIssGetAsyncMode (L2_PROTO_LACP) == LA_NP_SYNC))
                {

                    LaGetPortEntry (u2Port, &pPortEntry);
                    if (pPortEntry != NULL)
                    {
                        pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
                    }
                }
            }

            continue;
        }
        else
        {
            /* If port is in the HW & Sw, then check for LaLacHwPortStatus
             * to verify the Message loss, LA_NP_ADD_PORT_SUCCESS_INFO */

            LaGetPortEntry (u2Port, &pPortEntry);
            if (pPortEntry != NULL)
            {
                if (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG)
                {
                    LaRedAddSyncPortEntry (LA_NP_ADD_PORT_SUCCESS_INFO,
                                           u2AggIndex, u2Port);
                }
            }
        }
        /* Port is in hw as well as in sw aggregation. */
        u2SyncNumPorts++;
    }

    if (u2SyncNumPorts != u2SwNumPorts)
    {
        for (u2NumPorts = 0; u2NumPorts < u2SwNumPorts; u2NumPorts++)
        {
            if (pSwPortList[u2NumPorts].u2HwSync == LA_FALSE)
            {

                /* This port is not present in hardware, but is in software. 
                 * Hence add the port in hardware too. */
                if (LaFsLaHwAddLinkToAggGroup (u2AggIndex,
                                               pSwPortList[u2NumPorts].u2Port,
                                               &u2HwAggIndex) == FNP_FAILURE)

                {
                    LaHandleLinkAdditionFailed (u2AggIndex,
                                                pSwPortList[u2NumPorts].u2Port);

                }

                /* In case of synchronus NP case, NP success/
                 * failure is handled here */
                LaLinkAddedToAgg (u2AggIndex, pSwPortList[u2NumPorts].u2Port);
            }
        }
    }

    /* Update the NPAPI DB with the port used as DLF, MC, IPMC in H/W
     */
    LaFsLaRedHwNpUpdateDlfMcIpmcPort (u2AggIndex);

    return;
}

/*****************************************************************************/
/* Function Name      : LaRedIsHwPortInSwPortList.                           */
/*                                                                           */
/* Description        : This function check wheather the given port is       */
/*                      present in given port list or not. If the port is    */
/*                      pesent in the list, then the HwSync flag of the      */
/*                      corresponding entry is set to true.                  */
/*                                                                           */
/* Input(s)           : u2Port - Port Id.                                    */
/*                      pSwPortList - List of active ports in sw aggregator. */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE / LA_FALSE.                                  */
/*****************************************************************************/
INT4
LaRedIsHwPortInSwPortList (UINT2 u2Port, tLaRedPortsInSwAgg * pSwPortList)
{
    UINT2               u2Index;

    for (u2Index = 0; u2Index < LA_MAX_PORTS_PER_AGG; u2Index++)
    {
        if (pSwPortList[u2Index].u2Port == u2Port)
        {
            /* This port is present in hw as well as in sw aggregation. */
            pSwPortList[u2Index].u2HwSync = LA_TRUE;
            return LA_TRUE;
        }
    }
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : FsRedGetSelPolicy.                                   */
/*                                                                           */
/* Description        : This function is used to get the selection policy    */
/*                      from LA NPAPI database                               */
/*                                                                           */
/* Input(s)           : pHwInfo - Aggregator H/W information                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Selection policy of aggregator                       */
/*****************************************************************************/
UINT1
FsRedGetSelPolicy (tLaHwInfo * pHwInfo)
{
    return (pHwInfo->u4SelPolicyBitList);
}

/*****************************************************************************/
/* Function Name      : LaRedUpdateNpapiDB.                                  */
/*                                                                           */
/* Description        : This function is used to synchorinize the LA NPAPI   */
/*                      data structures using the information available in   */
/*                      standby node s/w                                     */
/*                                                                           */
/* Input(s)           : u2Port - Port Id.                                    */
/*                      pSwPortList - List of active ports in sw aggregator. */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedUpdateNpapiDB (VOID)
{
    tLaLacAggConfigEntry *pAggConfigEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tPortList          *pLaConfigPorts = NULL;
    tPortList          *pLaActivePorts = NULL;

    /* Node moving from standby to active state,hence hardware needs to be
     * initialised.
     */
    if (FNP_FAILURE == LaFsLaRedHwInit ())
    {
        LA_TRC (INIT_SHUT_TRC, "FsLaRedHwInit() FAILED during "
                "switchover Standby --> Active\n");
        return LA_FAILURE;
    }

    pLaConfigPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pLaConfigPorts == NULL)
    {
        LA_TRC (INIT_SHUT_TRC, "Error in Allocating memory for bitlist\n");
        return LA_FAILURE;
    }

    pLaActivePorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pLaActivePorts == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList (*pLaConfigPorts);
        return LA_FAILURE;
    }

    LaGetNextAggEntry (NULL, &pAggEntry);

    while (pAggEntry != NULL)
    {
        LA_MEMSET (*pLaConfigPorts, 0, sizeof (tPortList));
        LA_MEMSET (*pLaActivePorts, 0, sizeof (tPortList));

        pAggConfigEntry = &(pAggEntry->AggConfigEntry);

        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

        while (pPortEntry != NULL)
        {
            OSIX_BITLIST_SET_BIT ((*pLaConfigPorts), pPortEntry->u2PortIndex,
                                  sizeof (tPortList));

            if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing
                == LA_TRUE)
            {
                OSIX_BITLIST_SET_BIT ((*pLaActivePorts),
                                      pPortEntry->u2PortIndex,
                                      sizeof (tPortList));
            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

        /* LA NPAPI shadow table maintains Configured ports, Active ports,
         * LA selection policy for every aggregator.So we are populating 
         * it using current information in s/w.
         */
        if (LaFsLaRedHwUpdateDBForAggregator (pAggEntry->u2AggIndex,
                                              *pLaConfigPorts, *pLaActivePorts,
                                              (UINT1) pAggConfigEntry->
                                              LinkSelectPolicy) != FNP_SUCCESS)

        {
            LA_TRC_ARG1 (ALL_FAILURE_TRC, "FAILED to update LA NPAPI DB "
                         "during switchover from Standby --> Active in "
                         "aggregator %d\n", pAggEntry->u2AggIndex);
        }

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    FsUtilReleaseBitList (*pLaConfigPorts);
    FsUtilReleaseBitList (*pLaActivePorts);
    return LA_SUCCESS;
}
#endif

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaRedAddSyncPortEntry.                               */
/*                                                                           */
/* Description        : This function will be called when port gets added    */
/*                      into the HW. This function invokes NpCallBack        */
/*                      function                                             */
/*                                                                           */
/* Input(s)           : u2LaNpSubType - LA_NP_ADD_PORT_SUCCESS_INFO          */
/*                                      LA_NP_ADD_PORT_FAILURE_INFO          */
/*                      u2AggIndex - Id of the aggregator.                   */
/*                      u2PortNum  - Port successfully added to the HW       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedAddSyncPortEntry (UINT2 u2LaNpSubType, UINT2 u2AggIndex, UINT2 u2PortNum)
{
    tLaIntfMesg         LaMsg;

    /* In case of ASYNC NP, When Standby receives LA_NP_ADD_PORT_SUCCESS_INFO
     * message, it invokes LaHandleNpCallbackEvents with
     * AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP success information 
     * When Standby receives LA_NP_ADD_PORT_FAILURE_INFO, it invokes 
     * LaHandleNpCallbackEvents with Failure information*/

    MEMSET (&LaMsg, 0, sizeof (tLaIntfMesg));

    LaMsg.uLaIntfMsg.LaNpCbMsg.u4NpCallId = AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP;

    if (u2LaNpSubType == LA_NP_ADD_PORT_SUCCESS_INFO)
    {
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwAddLinkToAggGroup.
            rval = FNP_SUCCESS;
    }
    else
    {
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwAddLinkToAggGroup.
            rval = FNP_FAILURE;
    }
    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwAddLinkToAggGroup.
        u2AggIndex = u2AggIndex;
    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwAddLinkToAggGroup.
        u2PortNumber = u2PortNum;

    LaHandleNpCallbackEvents (&LaMsg);

    return;
}

/*****************************************************************************/
/* Function Name      : LaRedDelSyncPortEntry.                               */
/*                                                                           */
/* Description        : This function will be called when port gets deleted  */
/*                      from the HW and when phy port to be created.         */
/*                      This function invokes the NpCallBack function        */
/*                                                                           */
/* Input(s)           : u2LaNpSubType - LA_NP_DEL_PORT_SUCCESS_INFO          */
/*                                      LA_NP_DEL_PORT_FAILURE_INFO          */
/*                      u2AggIndex - Aggregatore Index                       */
/*                      u2PortNum  - Port successfully added to the HW       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                  */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedDelSyncPortEntry (UINT2 u2LaNpSubType, UINT2 u2AggIndex, UINT2 u2PortNum)
{
    tLaIntfMesg         LaMsg;    /* Added to send a NP success message 
                                   to LaHandleNPCallbackEvent function */

    /* In case of ASYNC NP, When Standby receives
     * LA_NP_DEL_PORT_SUCCESS_INFO message, it invokes
     * LaHandleNpCallbackEvents with AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP
     * success information 
     * When Standby receives LA_NP_DEL_PORT_FAILURE_INFO, it invokes
     * LaHandleNpCallbackEvents with failure information */

    MEMSET (&LaMsg, 0, sizeof (tLaIntfMesg));

    LaMsg.uLaIntfMsg.LaNpCbMsg.u4NpCallId =
        AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP;
    if (u2LaNpSubType == LA_NP_DEL_PORT_SUCCESS_INFO)
    {
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.
            FsLaHwRemoveLinkFromAggGroup.rval = FNP_SUCCESS;
    }
    else
    {
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.
            FsLaHwRemoveLinkFromAggGroup.rval = FNP_FAILURE;
    }

    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.
        FsLaHwRemoveLinkFromAggGroup.u2AggIndex = u2AggIndex;
    LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.
        FsLaHwRemoveLinkFromAggGroup.u2PortNumber = u2PortNum;

    LaHandleNpCallbackEvents (&LaMsg);
    return;
}
#endif
/*****************************************************************************/
/* Function Name      : LaRedAddSyncAggEntry.                                */
/*                                                                           */
/* Description        : This function will be called when first port becomes */
/*                      active in an aggregator. This function will store    */
/*                      the HW agg index and the status in                   */
/*                      gLaRedGlobalInfo.aLaRedHwAggInfo                     */
/*                                                                           */
/* Input(s)           : u2AggIndex - Id of the aggregator.                   */
/*                      u2HwIndex  - Id of the aggregator in the hw.         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.aLaRedHwAggInfo                     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedAddSyncAggEntry (UINT2 u2AggIndex, UINT2 u2HwIndex)
{
    tLaHwSyncInfo      *pLaHwEntry;
#ifdef NPAPI_WANTED
    tLaIntfMesg         LaMsg;    /* Added to send a NP success message to LaHandleNPCallbackEvent function */
#endif
    if ((u2AggIndex <= LA_MAX_PORTS) ||
        (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
        return;

    pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);

    pLaHwEntry->u2HwAggIdx = u2HwIndex;
    pLaHwEntry->u1AggStatus = LA_ENABLED;
#ifdef NPAPI_WANTED
    if (LA_NODE_STATUS () == LA_NODE_STANDBY)
    {
        /* In case of ASYNC NP, When Standby receives LA_NP_ADD_INFO message, it invokes LaHandleNpCallbackEvents with AGG create 
         * success information */
        MEMSET (&LaMsg, 0, sizeof (tLaIntfMesg));
        LaMsg.uLaIntfMsg.LaNpCbMsg.u4NpCallId = AS_FS_LA_HW_CREATE_AGG_GROUP;
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup.rval =
            FNP_SUCCESS;
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup.
            u2AggIndex = u2AggIndex;
        LaMsg.uLaIntfMsg.LaNpCbMsg.LaAsyncNpCbParams.FsLaHwCreateAggGroup.
            u2HwAggIndex = u2HwIndex;
        LaHandleNpCallbackEvents (&LaMsg);
    }
#endif /* NPAPI_WANTED */
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedProcessAddRemAggEntry                       */
/*                                                                           */
/* Description        : This function process the D-LAG sync message         */
/*                      received from the active node and adds the DLAG      */
/*                      Remote Aggregator Entry                              */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaDLAGRedProcessAddRemAggEntry (tRmMsg * pMesg, UINT4 *pu4Offset)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tMacAddr            MacAddr = { 0 };
    tLaPortList         DLAGDistributingOperPortList;
    UINT4               u4AggIndex = 0;
    UINT4               u4Val = 0;
    UINT4               u4RemotePortIndex = 0;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;
    UINT1               u1NoOfRemotePortsEntry = 0;
    UINT1               u1Count = 0;

    MEMSET (&DLAGDistributingOperPortList, 0, sizeof (tLaPortList));
    /* Get the port channel index */
    LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);

    /* Get the aggregator entry using port channel index */
    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If the aggregator entry is null, return */
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    /* Get the D-LAG distributing operational port list */
    LA_RM_GET_N_BYTE (pMesg, DLAGDistributingOperPortList, pu4Offset,
                      sizeof (tLaPortList));
    MEMCPY (pAggEntry->DLAGDistributingOperPortList,
            DLAGDistributingOperPortList, sizeof (tLaPortList));

    /* Get the D-LAG distributing port list count */
    LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4Val);
    pAggEntry->u4DLAGDistributingPortListCount = u4Val;

    /* Get the remote mac address which is of 6 bytes */
    LA_RM_GET_N_BYTE (pMesg, MacAddr, pu4Offset, LA_MAC_ADDRESS_SIZE);

    /* Get the remote aggregator entry based on the MAC address */
    LaDLAGGetRemoteAggEntryBasedOnMac (MacAddr, pAggEntry, &pRemoteAggEntry);

    if (pRemoteAggEntry == NULL)
    {
        /* If Remote D-LAG node Entry is not found
         * - Allocate Memory for Remote D-LAG Aggregator
         * - Initialize with Default Values */
        if ((LaDLAGCreateRemoteAggregator (MacAddr, &pRemoteAggEntry)) !=
            LA_SUCCESS)
        {
            return LA_FAILURE;
        }

        pRemoteAggEntry->pAggEntry = pAggEntry;
        /* Add to Remote Aggregator table */
        if (LaDLAGAddToRemoteAggTable (pAggEntry, pRemoteAggEntry) ==
            LA_FAILURE)
        {
            LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "Failure in adding Remote D-LAG Node entry to given aggregator remote D-LAG info table\n");
            return LA_FAILURE;

        }

    }
    /* Get the remote system priority */
    LA_RM_GET_2_BYTE (pMesg, pu4Offset, u2Val);
    pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority = u2Val;

    /* Get the remote keep alive count */
    LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4Val);
    pRemoteAggEntry->u4DLAGKeepAliveCount = u4Val;

    /* Get the remote role played */
    LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1Val);
    pRemoteAggEntry->u1DLAGRolePlayed = u1Val;

    /* Get the number of remote ports */
    LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1NoOfRemotePortsEntry);

    for (u1Count = 0; u1Count < u1NoOfRemotePortsEntry; u1Count++)
    {
        /* Copy First port Index */
        LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4RemotePortIndex);
        /* Search Remote D-LAG Node Info List */
        LaDLAGGetRemotePortEntry (u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            /* If Remote Port Entry is not found
             * - Allocate Memory for Remote Port Entry
             * - Initialize with Default Values */
            if ((LaDLAGCreateRemotePortEntry (u4RemotePortIndex,
                                              &pRemotePortEntry)) != LA_SUCCESS)
            {
                return LA_FAILURE;
            }
            /* Add to Remote port table */
            LaDLAGAddToRemotePortTable (pRemoteAggEntry, pRemotePortEntry);
        }
        /* Get the remote port bundle state */
        LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1Val);
        pRemotePortEntry->u1BundleState = u1Val;

        /* Get the remote port sync state */
        LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1Val);
        pRemotePortEntry->u1SyncState = u1Val;
    }
    /* Get the role played by the aggregator */
    LA_RM_GET_1_BYTE (pMesg, pu4Offset, u1Val);
    pAggEntry->u1DLAGRolePlayed = u1Val;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedProcessDelRemAggEntry                       */
/*                                                                           */
/* Description        : This function process the D-LAG sync message from    */
/*                      active node and deletes the remote                   */
/*                      aggregator entry                                     */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaDLAGRedProcessDelRemAggEntry (tRmMsg * pMesg, UINT4 *pu4Offset)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tMacAddr            MacAddr = { 0 };
    UINT4               u4AggIndex = 0;

    /* Get the port channel index */
    LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);

    /* Get the aggregator entry using port channel index */
    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If the aggregator entry is null, return */
    if (pAggEntry == NULL)
    {
        return LA_SUCCESS;
    }
    /* Get the remote mac address which is of 6 bytes */
    LA_RM_GET_N_BYTE (pMesg, MacAddr, pu4Offset, LA_MAC_ADDRESS_SIZE);

    /* Get the remote aggregator entry based on the MAC address */
    LaDLAGGetRemoteAggEntryBasedOnMac (MacAddr, pAggEntry, &pRemoteAggEntry);

    /* If the remote aggregator entry */
    if (pRemoteAggEntry != NULL)
    {
        /* Delete the remote aggregator entry */
        LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGRedProcessDelRemPortEntry                         */
/*                                                                           */
/* Description        : This function process the D-LAG sync message         */
/*                                                                           */
/* Input(s)           : pMsg - Rm buffer.                                    */
/*                      pu4Offset - pointer to the offset from where the     */
/*                                  write has to be done.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaDLAGRedProcessDelRemPortEntry (tRmMsg * pMesg, UINT4 *pu4Offset)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tMacAddr            MacAddr = { 0 };
    UINT4               u4AggIndex = 0;
    UINT4               u4RemotePortIndex = 0;

    /* Get the port channel index */
    LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4AggIndex);

    /* Get the aggregator entry using port channel index */
    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If the aggregator entry is null, return */
    if (pAggEntry == NULL)
    {
        return LA_SUCCESS;
    }

    LA_RM_GET_N_BYTE (pMesg, MacAddr, pu4Offset, LA_MAC_ADDRESS_SIZE);

    /* Get the remote aggregator entry based on the MAC address */
    LaDLAGGetRemoteAggEntryBasedOnMac (MacAddr, pAggEntry, &pRemoteAggEntry);

    /* If the remote aggregator entry is null, return */
    if (pRemoteAggEntry == NULL)
    {
        return LA_SUCCESS;
    }

    /* Get the remote port index */
    LA_RM_GET_4_BYTE (pMesg, pu4Offset, u4RemotePortIndex);

    /* Get the remote port entry using the remote port index */
    LaDLAGGetRemotePortEntry (u4RemotePortIndex, pRemoteAggEntry,
                              &pRemotePortEntry);

    /* If the remote port entry is null, return */
    if (pRemotePortEntry != NULL)
    {
        /* Delete the remote aggregator entry */
        LaDLAGDeleteRemotePort (pRemoteAggEntry, pRemotePortEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedDelSyncAggEntry.                                */
/*                                                                           */
/* Description        : This function will be called when the number of      */
/*                      active ports in an aggregator becomes zero. This     */
/*                      function will initialize the HW index for this       */
/*                      aggregator in gLaRedGlobalInfo.aLaRedHwAggInfo       */
/*                                                                           */
/* Input(s)           : u2AggIndex - Id of the aggregator.                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gLaRedGlobalInfo.aLaRedHwAggInfo                     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedDelSyncAggEntry (UINT2 u2AggIndex)
{
    tLaHwSyncInfo      *pLaHwEntry;

    if ((u2AggIndex <= LA_MAX_PORTS) ||
        (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
        return;

    pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);

    pLaHwEntry->u2HwAggIdx = LA_INVALID_HW_AGG_IDX;
    pLaHwEntry->u1AggStatus = LA_DISABLED;
}

/*****************************************************************************/
/* Function Name      : LaRedHandleBulkUpdateEvent.                          */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      LaRedHandleBulkReqEvent is triggered.                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaNodeStatus.                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleBulkUpdateEvent (VOID)
{
    if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
    {
        LaRedHandleBulkReqEvent (NULL);
    }
    else
    {
        /* Only Active node can process bulk request and can
         * send the bulk update.*/
        LA_BULK_REQ_RECD () = LA_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : LaRedSendBulkUpdateTailMsg                           */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_LA_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        LA_TRC (LA_RED_TRC, "LA: Node is not active. Bulk Update tail msg "
                "not sent.\n");
        return LA_SUCCESS;
    }

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
        return (LA_FAILURE);
    }

    /* Form a bulk update tail message. 

     *        <--------4 Byte--------><---2 Byte--->
     *********************************************** 
     *        *                      *             *
     * RM Hdr * LA_BULK_UPD_TAIL_MSG * Msg Length  *
     *        *                      *             * 
     ***********************************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, LA_BULK_UPD_TAIL_MSG);
    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        LaRmHandleProtocolEvent (&ProtoEvt);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRestartModule                                      */
/*                                                                           */
/* Description        : This function will restart LA module.                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRestartModule (VOID)
{
    LA_LOCK ();

    if (LaHandleShutDown () != LA_SUCCESS)
    {
        LA_TRC (ALL_FAILURE_TRC, "LA shutdown failed during restart for "
                "applying configuration database\n");
        LA_UNLOCK ();
        return LA_FAILURE;
    }

    /* Node will move to IDLE state in this function */
    if (LaHandleInit () != LA_SUCCESS)
    {
        LA_TRC (ALL_FAILURE_TRC, "LA start failed during restart for "
                "applying configuration database\n");
        LA_UNLOCK ();
        return LA_FAILURE;
    }

    LA_UNLOCK ();

    return LA_SUCCESS;
}

#ifdef CLI_WANTED
VOID
LaRedReadSyncUpTable (tCliHandle CliHandle)
{
    UINT2               u2PortIndex;
    tLaRedSyncedUpInfo *pSyncUpEntry = NULL;

    for (u2PortIndex = 1; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2PortIndex);
        if (pSyncUpEntry->u2PortActorKey != 0)
        {
            LaRedShowPortSyncInfo (CliHandle, u2PortIndex, pSyncUpEntry);
        }
    }
}

VOID
LaRedPrintSyncUpInfoForPort (tCliHandle CliHandle, UINT2 u2AggIndex)
{
    tLaRedSyncedUpInfo *pSyncUpEntry = NULL;
    UINT2               u2Port;

    for (u2Port = 1; u2Port <= LA_MAX_PORTS; u2Port++)
    {
        pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2Port);
        if (pSyncUpEntry->u2PortActorKey == u2AggIndex)
        {
            LaRedShowPortSyncInfo (CliHandle, u2Port, pSyncUpEntry);
            return;
        }
    }                            /* End of FOR LOOP */

    CliPrintf (CliHandle, "\r%% Invalid Port-channel\r\n");
    return;
}

VOID
LaRedShowPortSyncInfo (tCliHandle CliHandle, UINT2 u2Port,
                       tLaRedSyncedUpInfo * pSyncUpEntry)
{
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[20];

    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, 20);
    if ((u2Port == 0) || (u2Port > LA_MAX_PORTS))
        return;
    pSyncUpEntry = LA_GET_SYNCUP_ENTRY (u2Port);

    LaCfaCliGetIfName ((UINT4) u2Port, (INT1 *) au1Temp);

    CliPrintf (CliHandle, "\r\nActor Information for Port : ");
    CliPrintf (CliHandle, "%s\r\n", au1Temp);
    CliPrintf (CliHandle, "\r-------------\r\n");
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\rChannel Group : %d\r\n",
               pSyncUpEntry->u2PortActorKey);
    CliPrintf (CliHandle, "\rPseudo port-channel = Po%d\r\n",
               pSyncUpEntry->u2PortActorKey);

    CliPrintf (CliHandle, "\rCurrentWhile Split Interval Tmr Count = %d\r\n",
               pSyncUpEntry->u1TimerCount);

    CliPrintf (CliHandle, "\r\n");

}
#endif
/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : LaRedHRProcStdyStPktReq                              */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      LA to the RM by setting the periodic timeout value   */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHRProcStdyStPktReq (VOID)
{
    tLaLacPortEntry    *PortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    UINT1               u1TailFlag = 0;

    if (LA_MODULE_STATUS == LA_DISABLED)
    {
        /* Module is disabled. So peroidic timers will not be running.
         * TO move next moduls, send tail msg here */
        LA_TRC (LA_RED_TRC, "Module is disabled; sending steady state "
                "tail msg.\n");
        LaRedHRSendStdyStTailMsg ();
        return;
    }

    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        LA_TRC (LA_RED_TRC, "LA: Node is not active. Processing of steady  "
                "state pkt request failed.\n");
        return;
    }
    if (LA_HR_STATUS () == LA_HR_STATUS_DISABLE)
    {
        LA_TRC (LA_RED_TRC, "LA: Hitless restart is not enabled. Processing "
                "of steady state pkt request failed.\n");
        return;
    }
    LA_TRC (LA_RED_TRC, "LA: starts sending steady state packets to RM.\n");

    /* This flag is set to indicate the LA timer expiry handler that steady 
     * state request had received from RM */

    LA_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;

    /* the periodic timer is stopped and restarted with 0 seconds.
     * When restarting this timer with 0 seconds, it is immediately
     * expires and the periodic PDU will be sent. This periodic PDU
     * (in hitless restart case, it is called as steady state packet)
     * is hooked, and it is sent to RM. */

    while (LaGetNextPortEntry (PortEntry, &pNextPortEntry) == LA_SUCCESS)
    {
        if ((pNextPortEntry->LaLacpMode == LA_MODE_LACP) &&
            (pNextPortEntry->LaLacpEnabled == LA_LACP_ENABLED) &&
            (pNextPortEntry->LaPortEnabled == LA_PORT_ENABLED))
        {
            if (pNextPortEntry->PeriodicTmr.LaTmrFlag == LA_TMR_RUNNING)
            {
                if ((LaStopTimer (&(pNextPortEntry->PeriodicTmr))) ==
                    LA_FAILURE)
                {
                    return;
                }
                if (LA_START_TMR (LA_TIMER_LIST_ID,
                                  &pNextPortEntry->PeriodicTmr.LaAppTimer,
                                  0 /*Duration */ ) != LA_TMR_SUCCESS)
                {
                    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "TMR: Starting Timer FAILED\n");
                }
                if (u1TailFlag == 0)
                {
                    u1TailFlag = LA_HR_STDY_ST_PKT_TAIL;
                }
            }
        }
        PortEntry = pNextPortEntry;
    }
    /* This steady state tail msg sending is done in case of LA periodic
     * timers are not running. */
    if (u1TailFlag != LA_HR_STDY_ST_PKT_TAIL)
    {
        LaRedHRSendStdyStTailMsg ();
        LA_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaRedHRSendStdyStPkt                                 */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
LaRedHRSendStdyStPkt (UINT1 *pu1LinBuf, UINT4 u4PktLen, UINT2 u2Port,
                      UINT4 u4TimeOut)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BufSize = 0;

    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        LA_TRC (LA_RED_TRC, "LA: Node is not active. Steady state pkt is not "
                "not sent to RM.\n");
        return LA_SUCCESS;
    }
    if (LA_HR_STATUS () == LA_HR_STATUS_DISABLE)
    {
        LA_TRC (LA_RED_TRC, "LA: Hitless restart is not enabled. Steady state "
                "pkt is not sent to RM.\n");
        return LA_SUCCESS;
    }

    /* Forming  steady state packet

       <- 24B --><-------  4B -- -------><-- 2B -->< 2B -><-- 4B -->< 124 B >
       ____________________________________________________________________
       |        |                       |         |      |         |        |
       | RM Hdr | LR_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
       |________|_______________________|_________|______|_________|________|

       * The RM Hdr shall be included by RM.
     */

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE    /* length of the message type */
        + LA_RED_LENGTH_SIZE    /* length of the message length */
        + sizeof (UINT2)        /* length for the port field */
        + sizeof (UINT4)        /* length for the Timeout filed */
        + u4PktLen;                /* buffer length */
    /* Allocate memory for data to be sent to RM. This memory will be freed 
     * by RM */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    u2MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_HR_STDY_ST_PKT_MSG);
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2Port);
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, u4TimeOut);
    /* copying the packet into the RM steady state msg */
    LA_RM_PUT_N_BYTE (pMsg, pu1LinBuf, &u4Offset, u4PktLen);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        LA_TRC (ALL_FAILURE_TRC, "LA: steady state pkt sending to RM is "
                "failed\n");
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedHRSendStdyStTailMsg                             */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
LaRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = LA_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if (LA_NODE_STATUS () != LA_NODE_ACTIVE)
    {
        LA_TRC (LA_RED_TRC, "LA: Node is not active. Steady state "
                "tail msg is not sent to RM.\n");
        return LA_SUCCESS;
    }
    if (LA_HR_STATUS () == LA_HR_STATUS_DISABLE)
    {
        LA_TRC (LA_RED_TRC, "LA: Hitless restart is not enabled. Steady state "
                "tail msg is not sent to RM.\n");
        return LA_SUCCESS;
    }

    LA_TRC (LA_RED_TRC, "LA: sending steady state tail msg to RM.\n");

    u2BufSize = LA_SYNC_MSG_TYPE_SIZE + LA_RED_LENGTH_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "RM alloc failed\n");
        return (LA_FAILURE);
    }

    /* Form a steady state tail message. 

     *         <--------4 Byte--------><---2 Byte--->
     _______________________________________________
     |        |                        |             |
     | RM Hdr | LA_HR_STDY_ST_PKT_TAIL | Msg Length  |
     |________|________________________|_____________|

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    LA_RM_PUT_4_BYTE (pMsg, &u4Offset, LA_HR_STDY_ST_PKT_TAIL);
    LA_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (LaRedSendMsgToRm (pMsg, u2BufSize) == LA_FAILURE)
    {
        LA_TRC (ALL_FAILURE_TRC, "LA: steady state tail msg sending is "
                "failed\n");
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaRedHandleDynSyncAudit (VOID)
{
    /*On receiving this event, la should execute show cmd and calculate checksum */
    LaExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : LaExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_LA_APP_ID;
    UINT2               u2ChkSum = 0;

    LA_UNLOCK ();
    if (LaGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == LA_FAILURE)
    {
        LA_LOCK ();
        LA_TRC (ALL_FAILURE_TRC, "Checksum of calculation failed for LA\n");
        return;
    }

    if (LaRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == LA_FAILURE)
    {
        LA_LOCK ();
        LA_TRC (ALL_FAILURE_TRC, "Sending checkum to RM failed\n");
        return;
    }
    LA_LOCK ();
    return;
}
