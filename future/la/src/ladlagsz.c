/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ladlagsz.c,v 1.1 2016/01/11 12:51:28 siva Exp $
 *
 * Description: Sizing files for DLAG.
 *******************************************************************/

#define _LADLAGSZ_C
#include "lahdrs.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  LaDlagSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < LA_DLAG_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsLADlagSizingParams[i4SizingId].u4StructSize,
                          FsLADlagSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(LADlagMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            LaDlagSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   LaDlagSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsLADlagSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, LADlagMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  LaDlagSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < LA_DLAG_MAX_SIZING_ID; i4SizingId++) {
        if(LADlagMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( LADlagMemPoolIds[ i4SizingId] );
            LADlagMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
