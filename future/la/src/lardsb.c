#include "lahdrs.h"
#ifdef CLI_WANTED
#include "lacli.h"
#endif

/*****************************************************************************/
/* Function Name      : LaRedSyncUpPartnerInfo                                */
/*                                                                           */
/* Description        : This function:                                       */
/*                           - gets the information to be synced up for the  */
/*                             given port                                    */
/*                           - constructs the RM message and                 */
/*                           - sends it to Redundancy Manager.               */
/*                                                                           */
/* Input(s)           : pPort          - Pointer to the port entry.          */
/*                      pLaLacpduInfo  - ParnterInfo to be synced up.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpPartnerInfo (tLaLacPortEntry * pPortEntry,
                        tLaLacpduInfo * pLaLacpduInfo)
{
    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (pLaLacpduInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncUpCurrWhileTmrExpiry                        */
/*                                                                           */
/* Description        : This function sends the current while timer expiry   */
/*                      message to LA in standby node.                       */
/*                                                                           */
/* Input(s)           : u2PortIndex   - Port Id.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpCurrWhileTmrExpiry (UINT2 u2PortIndex, UINT1 u1TimerCount)
{
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (u1TimerCount);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncUpWaitWhileTmrExpiry                        */
/*                                                                           */
/* Description        : This function sends the wait while timer expiry      */
/*                      message to LA in standby node.                       */
/*                                                                           */
/* Input(s)           : u2PortIndex   - Port Id.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpWaitWhileTmrExpiry (UINT2 u2PortIndex)
{
    UNUSED_PARAM (u2PortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncUpPortOperStatus.                           */
/*                                                                           */
/* Description        : This function sends the port oper status to the      */
/*                      Standby.                                             */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index                             */
/*                      u1PortOperStatus - Port Oper Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE.                             */
/*****************************************************************************/
INT4
LaRedSyncUpPortOperStatus (UINT2 u2PortIndex, UINT1 u1PortOperStatus)
{
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (u1PortOperStatus);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRegisterWithRM                                     */
/*                                                                           */
/* Description        : Creates a queue for receiving messages from RM and   */
/*                      registers LA with RM.                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then LA_SUCCESS           */
/*                      Otherwise LA_FAILURE                                 */
/*****************************************************************************/
INT4
LaRedRegisterWithRM (VOID)
{
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeRegisterWithRM                                   */
/*                                                                           */
/* Description        : It deregisters with RM                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*****************************************************************************/
INT4
LaRedDeRegisterWithRM (VOID)
{
    return LA_SUCCESS;
}

INT4
LaRedSyncProtocolDisable (VOID)
{
    return LA_SUCCESS;
}

INT4
LaRedSyncModifyAggPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex,
                             tLaHwId HwInfo)
{
    UNUSED_PARAM (u2LaNpSubType);
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (HwInfo);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRedSyncModifyPortsInfo.                            */
/*                                                                           */
/* Description        : This function will be called when a port is added or */
/*                      removed from bundle in the hardware. This function   */
/*                      will form rm message containing the change done in hw*/
/*                      and sends it to standby node.                        */
/*                      Note: This function will be called only in active    */
/*                      node.                                                */
/*                                                                           */
/* Input(s)           : u2LaNpSubType - Type used to indicate add/remove     */
/*                                      port to hw aggregation.              */
/*                      u2AggIndex - Aggregator Id.                          */
/*                      u2PortNum  - Port Id that was added/removed from hw  */
/*                                   aggregation.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS /LA_FAILURE                               */
/*****************************************************************************/
INT4
LaRedSyncModifyPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex,
                          UINT2 u2PortNum)
{
    UNUSED_PARAM (u2LaNpSubType);
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u2PortNum);
    return LA_SUCCESS;
}

#ifdef CLI_WANTED
VOID
LaRedReadSyncUpTable (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    return;
}

VOID
LaRedPrintSyncUpInfoForPort (tCliHandle CliHandle, UINT2 u2Port)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u2Port);
    return;
}
#endif
