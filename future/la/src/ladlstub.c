/*****************************************************************************/
/* Copyright (C) 2015 Aricent Inc . All Rights Reserved                      */
/* $Id: ladlstub.c,v 1.3 2017/11/08 13:19:13 siva Exp $                        */
/* License Aricent Inc., 2001-2002                                           */
/*****************************************************************************/
/*    FILE  NAME            : ladlstub.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Distributed Link Aggregation                   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DESCRIPTION           : This file contains stub function definitions   */
/*                            for supporting Distributed Link Aggregation    */
/*                            and Multi chassis Link Aggregation.            */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/
/* Function Name      : LaDLAGInit                                           */
/*                                                                           */
/* Description        : This function is used to initialize the D-LAG        */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which D-LAG feature  */
/*                                  should be initiated.                     */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGInit (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeInit                                         */
/*                                                                           */
/* Description        : This function is used to de-initialize the D-LAG     */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which D-LAG feature  */
/*                                  should be stopped.                       */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGDeInit (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGSetActorSystemIDToDLAG                         */
/*                                                                           */
/* Description        : This function will copy the DLAG System ID to        */
/*                      AggActorsystemID and updates the configured port     */
/*                      entries                                              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGSetActorSystemIDToDLAG (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGResetActorSystemID                             */
/*                                                                           */
/* Description        : This function will resets the  AggActorsystemID to   */
/*                      Global System ID and updates the configured port     */
/*                      entries                                              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaSystem                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGResetActorSystemID (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGChangeActorSystemPriorityToDLAG                */
/*                                                                           */
/* Description        : This function will copy the DLAG System priority to  */
/*                      AggActorsystem priority and updates the configured   */
/*                      port entries.                                        */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGChangeActorSystemPriorityToDLAG (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGChangeAttachedPortActorSysLAGIDToDLAG          */
/*                                                                           */
/* Description        : This function will copy the D-LAG System priority to */
/*                      newly attached port & Actor system priority and      */
/*                      copy D-LAG system ID to port actor system ID.        */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to port table entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGChangeAttachedPortActorSysLAGIDToDLAG (tLaLacPortEntry * pPortEntry)
{
    UNUSED_PARAM (pPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGResetPortActorSysLAGID                         */
/*                                                                           */
/* Description        : This function will resets the  AggActorsystemID to   */
/*                      Global System ID and resets the AggActorsystem       */
/*                      priority to Global System priority for the port entry*/
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to port table entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGResetPortActorSysLAGID (tLaLacPortEntry * pPortEntry)
{
    UNUSED_PARAM (pPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGResetActorSystemPriority                       */
/*                                                                           */
/* Description        : This function will resets the AggActorsystem         */
/*                      priority to Global System priority and updates the   */
/*                      configured port entries                              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaSystem                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGResetActorSystemPriority (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGCreateRemoteAggregator                         */
/*                                                                           */
/* Description        : This function is used to allocate memory for the     */
/*                      Remote D-LAG node port channel info                  */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : ppRetAggEntry - Allocated Remote Aggregator entry    */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory allocation is failed. */
/*****************************************************************************/
INT4
LaDLAGCreateRemoteAggregator (tMacAddr MacAddr,
                              tLaDLAGRemoteAggEntry ** ppRetRemoteAggEntry)
{
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (ppRetRemoteAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGCreateRemotePortEntry                          */
/*                                                                           */
/* Description        : This function is used to allocate memory for the     */
/*                      Remote port Entry                                    */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : u4RemotePortIndex - Remote Port Index for which new  */
/*                                          entry is to be created.          */
/*                      ppRetRemotePortEntry - pointer to the allocate       */
/*                                             remote port entry             */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory allocation is failed. */
/*****************************************************************************/
INT4
LaDLAGCreateRemotePortEntry (UINT4 u4RemotePortIndex,
                             tLaDLAGRemotePortEntry ** ppRetRemotePortEntry)
{
    UNUSED_PARAM (u4RemotePortIndex);
    UNUSED_PARAM (ppRetRemotePortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteRemoteAggregator                         */
/*                                                                           */
/* Description        : This function is used to delete the remote           */
/*                      aggregator entry from a remote aggregator info list. */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry from which remote       */
/*                                  pRemAggEntry should be found and deleted */
/*                      pRemAggEntry - Remote Aggregator entry to be deleted */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory de allocation is      */
/*                                           failed.                         */
/*****************************************************************************/
INT4
LaDLAGDeleteRemoteAggregator (tLaLacAggEntry * pAggEntry,
                              tLaDLAGRemoteAggEntry * pRemAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pRemAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteRemotePort                               */
/*                                                                           */
/* Description        : This function is used to delete the remote port      */
/*                      entry from a remote port info list.                  */
/*                                                                           */
/* Input(s)           : pRemAggEntry - Remote Aggregator entry from which    */
/*                                     pRemPortEntry should be found and     */
/*                                     deleted.                              */
/*                      pRemPortEntry - Remote port entry to be deleted      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory de allocation is      */
/*                                           failed.                         */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGDeleteRemotePort (tLaDLAGRemoteAggEntry * pRemAggEntry,
                        tLaDLAGRemotePortEntry * pRemPortEntry)
{
    UNUSED_PARAM (pRemAggEntry);
    UNUSED_PARAM (pRemPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGUpdateRemoteDLAGNodeList.                      */
/*                                                                           */
/* Description        : This function is called by the D-LAG Rx Message      */
/*                      Handler function to update the remote aggregator     */
/*                      info list.                                           */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel whose remote aggregator  */
/*                                     info list needs to be updated         */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                      node on Distributing port. with LA Header stripped.  */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node/ */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGUpdateRemoteDLAGNodeList (tLaLacAggEntry * pAggEntry, UINT1 *pu1LinearBuf,
                                INT4 i4SllAction)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pu1LinearBuf);
    UNUSED_PARAM (i4SllAction);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGUpdateRemotePortList.                          */
/*                                                                           */
/* Description        : This function is called by the D-LAG update remote   */
/*                      Aggregator info function to update the corresponding */
/*                      remote port info list.                               */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote aggregator entry whose      */
/*                                        remote port info list needs to be  */
/*                                        updated.                           */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                                     node on Distributing port.            */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node/ */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                            UINT1 *pu1LinearBuf, INT4 i4SllAction)
{
    UNUSED_PARAM (pRemoteAggEntry);
    UNUSED_PARAM (pu1LinearBuf);
    UNUSED_PARAM (i4SllAction);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteAllRemoteAggEntries.                     */
/*                                                                           */
/* Description        : This function is called to delete all the remote     */
/*                      aggregator Entries                                   */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to aggregator whose remote       */
/*                      D-LAG list needs to be deleted                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGDeleteAllRemoteAggEntries (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteAllRemotePortEntries.                    */
/*                                                                           */
/* Description        : This function is called to delete all the remote     */
/*                      port Entries                                         */
/*                                                                           */
/* Input(s)           : pRemAggEntry - Pointer to Remote Aggreator whose     */
/*                                     remote port list needs to be deleted  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGDeleteAllRemotePortEntries (tLaDLAGRemoteAggEntry * pRemAggEntry)
{
    UNUSED_PARAM (pRemAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTxPeriodicSyncPdu.                             */
/*                                                                           */
/* Description        : This function is called on D-LAG periodic sync timer */
/*                      expiry to send D-LAG Periodic sync PDU.              */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     D-LAG Periodic sync PDU               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGTxPeriodicSyncPdu (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTxEventUpdatePdu.                              */
/*                                                                           */
/* Description        : This function Forms and Sends Event-update PDU.      */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     event-update PDU.                     */
/*                      pLaDLAGTxInfo - Tx Info to be used in event-update   */
/*                                      PDU construction.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGTxEventUpdatePdu (tLaLacAggEntry * pAggEntry,
                        tLaDLAGTxInfo * pLaDLAGTxInfo)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pLaDLAGTxInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTxDSU.                                         */
/*                                                                           */
/* Description        : This function Forms and Sends D-LAG PDU.             */
/*                      This function will be called from Tx event-update    */
/*                      Tx periodic-sync message.                            */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     D-LAG PDU.                            */
/*                      pLaDLAGTxInfo - Tx Info.                             */
/*                         - NULL: for periodic sync PDU Tx info.            */
/*                         - Pointer to TxInfo Structure, to be sent in      */
/*                           update PDU.                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGTxDSU (tLaLacAggEntry * pAggEntry, tLaDLAGTxInfo * pLaDLAGTxInfo)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pLaDLAGTxInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetRemoteAggEntryBasedOnMac                    */
/*                                                                           */
/* Description        : This function returns the pointer to the Remote      */
/*                      D-LAG info node matching Remote System Source Mac    */
/*                      address in remote aggregator info list.              */
/*                                                                           */
/*                                                                           */
/* Input(s)           : LaDLAGRemoteNodeMacAddr - Remote Agg Entry index,    */
/*                                                Remote node Source Mac     */
/*                                                Address.                   */
/*                      pAggEntry - Aggregator Entry in which Remote Aggr    */
/*                                  Entry needs to be searched.              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : ppRemoteAggEntry - pointer to the Remote Aggregator  */
/*                                         entry in remote aggr info list    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaDLAGGetRemoteAggEntryBasedOnMac (tLaMacAddr LaDLAGRemoteNodeMacAddr,
                                   tLaLacAggEntry * pAggEntry,
                                   tLaDLAGRemoteAggEntry ** ppRemoteAggEntry)
{
    UNUSED_PARAM (LaDLAGRemoteNodeMacAddr);
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (ppRemoteAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetRemotePortEntry                             */
/*                                                                           */
/* Description        : This function returns the pointer to the Remote      */
/*                      Port entry of a D-LAG remote node.                   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4RemotePortIndex - Index of Remote Port to be       */
/*                                          searched (search key)            */
/*                      pRemoteAggEntry - Pointer to remote Aggregator entry */
/*                                        in whose remote port info list     */
/*                                        given remote port entry should be  */
/*                                        searched.                          */
/*                                                                           */
/* Output(s)          : ppRemotePortEntry - pointer to the found remote port */
/*                                          node in remote port info list    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
LaDLAGGetRemotePortEntry (UINT4 u4RemotePortIndex,
                          tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                          tLaDLAGRemotePortEntry ** ppRemotePortEntry)
{
    UNUSED_PARAM (u4RemotePortIndex);
    UNUSED_PARAM (pRemoteAggEntry);
    UNUSED_PARAM (ppRemotePortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetNextRemoteAggEntry                          */
/*                                                                           */
/* Description        : This function returns pointer to the next remote     */
/*                      D-LAG Node entry.                                    */
/*                      If this function called with NULL as pRemoteAggEntry */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which remote node    */
/*                                  info list is maintained                  */
/*                      pRemoteAggEntry - Remote Aggr Entry pointer for      */
/*                                        which next node should be returned */
/*                                                                           */
/* Output(s)          : pNextRemoteAggEntry - pointer to the next Node       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaDLAGGetNextRemoteAggEntry (tLaLacAggEntry * pAggEntry,
                             tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                             tLaDLAGRemoteAggEntry ** pNextRemoteAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pRemoteAggEntry);
    UNUSED_PARAM (pNextRemoteAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetNextRemotePortEntry                         */
/*                                                                           */
/* Description        : This function returns pointer to the next remote     */
/*                      Port entry of a D-LAG remote node.                   */
/*                      This function returns pointer to the first port      */
/*                      entry as the next node when pRemotePortEntry passed  */
/*                      is NULL.                                             */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote Aggr list in which remote   */
/*                                        port list is maintained.           */
/*                      pRemotePortEntry - pointer to the current remote     */
/*                                         port entry whose next remote      */
/*                                         port entry is to be returned      */
/*                                                                           */
/* Output(s)          : pNextRemotePortEntry - pointer to the next remote    */
/*                                             port entry found              */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGGetNextRemotePortEntry (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                              tLaDLAGRemotePortEntry * pRemotePortEntry,
                              tLaDLAGRemotePortEntry ** pNextRemotePortEntry)
{
    UNUSED_PARAM (pRemoteAggEntry);
    UNUSED_PARAM (pRemotePortEntry);
    UNUSED_PARAM (pNextRemotePortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGKeepAlive                                      */
/*                                                                           */
/* Description        : This function increments keep alive counter for      */
/*                      each D-LAG remote node after each D-LAG periodic     */
/*                      sync timer expiry and checks if keep alive counter   */
/*                      has reached maximum, Keep alive count configured     */
/*                      for the port channe, If found true then that remote  */
/*                      D-LAG node entry will be removed                     */
/*                      from the remote D-LAG node info list.                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - pointer to the aggregator entry.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGKeepAlive (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowValidateRemotePortChannelIndex                */
/* Input       :  i4FsLaPortChannelIfIndex,                                  */
/*                FsLaDlagRemotePortChannelSysID                          */
/*                                                                           */
/* Description : This function validates the indexes of remote               */
/*               port channel table                                          */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex, FsLaDlagRemotePortChannelSysID*/
/* Output      :  None                                                       */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowValidateRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                             tMacAddr
                                             FsLaDlagRemotePortChannelSysID)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (FsLaDlagRemotePortChannelSysID);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGSnmpLowGetFirstValidRemotePortChannelIndex     */
/*                                                                           */
/* Description        : This function returns the first index of D-LAG Remote*/
/*                      Aggregator Table. This function is called            */
/*                      with these output parameters initialized to zero     */
/*                                                                           */
/* Input(s)          :  None                                                 */
/* Output(s)          : Port Channel interface index and Remote System Id    */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetFirstValidRemotePortChannelIndex (INT4
                                                  *pi4FsLaPortChannelIfIndex,
                                                  tMacAddr *
                                                  pFsLaDLAGRemotePortChannelSystemID)
{
    UNUSED_PARAM (pi4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pFsLaDLAGRemotePortChannelSystemID);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowGetNextValidRemotePortChannelIndex            */
/* Description :  This function returns the next index of D-LAG Remote       */
/*                Aggregator Table. If this function is passed with NULL     */
/*                values, then first index will be returned                  */
/*                                                                           */
/* Input       :  PortChannelIfIndex and Remote Port Channel System ID       */
/*                                                                           */
/* Output      :  Next PortChannelIfIndex and Next Remote Port               */
/*                Channel System ID                                          */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetNextValidRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                                 INT4
                                                 *pi4NextFsLaPortChannelIfIndex,
                                                 tMacAddr
                                                 FsLaDlagRemotePortChannelSysID,
                                                 tMacAddr *
                                                 pNextFsLaDLAGRemotePortChannelSystemID)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pi4NextFsLaPortChannelIfIndex);
    UNUSED_PARAM (FsLaDlagRemotePortChannelSysID);
    UNUSED_PARAM (pNextFsLaDLAGRemotePortChannelSystemID);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowValidateRemotePortIndex                       */
/* Description :  This function validates the indexes of the D-LAG remote    */
/*                port table                                                 */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex,FsLaDlagRemotePortChannelSysID */
/*                i4FsLaDLAGRemotePortIndex                                  */
/*                                                                           */
/* Output      :  None                                                       */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowValidateRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr FsLaDlagRemotePortChannelSysID,
                                      INT4 i4FsLaDLAGRemotePortIndex)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (FsLaDlagRemotePortChannelSysID);
    UNUSED_PARAM (i4FsLaDLAGRemotePortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGSnmpLowGetFirstValidRemotePortIndex            */
/*                                                                           */
/* Description        : This function gets the first valid index of the      */
/*                      D-LAG Remote Port Table. This function is called     */
/*                      with these output parameters initialized to zero     */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : Port Channel Index, Remote Port Channel System ID    */
/*                      RemotePortIndex                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetFirstValidRemotePortIndex (INT4 *pi4FsLaPortChannelIfIndex,
                                           tMacAddr *
                                           pFsLaDLAGRemotePortChannelSystemID,
                                           INT4 *pi4FsLaDLAGRemotePortIndex)
{
    UNUSED_PARAM (pi4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pFsLaDLAGRemotePortChannelSystemID);
    UNUSED_PARAM (pi4FsLaDLAGRemotePortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowGetNextValidRemotePortIndex                   */
/* Description :  This function returns the next index of the D-LAG Remote   */
/*                Port Table. If this function is passed with NULL           */
/*                values, then first index will be returned                  */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex,FsLaDlagRemotePortChannelSysID */
/*                i4FsLaDLAGRemotePortIndex                                  */
/*                                                                           */
/* Output      :  pi4NextFsLaPortChannelIfIndex,                             */
/*                pNextFsLaDLAGRemotePortChannelSystemId,                    */
/*                pi4NextFsLaDLAGRemotePortIndex                             */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetNextValidRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                          INT4 *pi4NextFsLaPortChannelIfIndex,
                                          tMacAddr
                                          FsLaDlagRemotePortChannelSysID,
                                          tMacAddr *
                                          pNextFsLaDLAGRemotePortChannelSystemID,
                                          INT4 i4FsLaDLAGRemotePortIndex,
                                          INT4 *pi4NextFsLaDLAGRemotePortIndex)
{
    UNUSED_PARAM (i4FsLaPortChannelIfIndex);
    UNUSED_PARAM (pi4NextFsLaPortChannelIfIndex);
    UNUSED_PARAM (FsLaDlagRemotePortChannelSysID);
    UNUSED_PARAM (pNextFsLaDLAGRemotePortChannelSystemID);
    UNUSED_PARAM (i4FsLaDLAGRemotePortIndex);
    UNUSED_PARAM (pi4NextFsLaDLAGRemotePortIndex);
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaDLAGAddToRemoteAggTable                            */
/*                                                                           */
/* Description        : This function adds a Remote D-LAG Node entry to      */
/*                      given aggregator remote D-LAG info table             */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator to which remote D-LAG node to */
/*                                  be added.                                */
/*                      pRemoteAggEntry - Remote Aggregator entry to be      */
/*                                        added.                             */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaDLAGAddToRemoteAggTable (tLaLacAggEntry * pAggEntry,
                           tLaDLAGRemoteAggEntry * pRemoteAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pRemoteAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGAddToRemotePortTable                           */
/*                                                                           */
/* Description        : This function adds a Remote port entry to remote     */
/*                      port info list maintained in a remote aggregator     */
/*                      info table                                           */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - pointer to the Remote Aggr info    */
/*                                        list in which remote port info     */
/*                                        list is maintained to which given  */
/*                                        port entry needs to be added.      */
/*                      pRemotePortEntry - pointer to the port entry to be   */
/*                                         added.                            */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGAddToRemotePortTable (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                            tLaDLAGRemotePortEntry * pRemotePortEntry)
{
    UNUSED_PARAM (pRemoteAggEntry);
    UNUSED_PARAM (pRemotePortEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteFromRemoteAggTable                       */
/*                                                                           */
/* Description        : This function deletes a D-LAG node entry from Remote */
/*                      Remote Node Info List maintained in pAggEntry        */
/*                                                                           */
/* Input(s)           : pAggEntry - pointer to Aggr Entry in which           */
/*                                  pRemoteAggEntry node should be found and */
/*                                  deleted.                                 */
/*                      pRemoteAggEntry - pointer to the remote Aggr entry   */
/*                                        that needs to be deleted           */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGDeleteFromRemoteAggTable (tLaLacAggEntry * pAggEntry,
                                tLaDLAGRemoteAggEntry * pRemoteAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pRemoteAggEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteFromRemotePortTable                      */
/*                                                                           */
/* Description        : This function deletes a remote port entry from       */
/*                      Remote D-LAG port info list.                         */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - pointer to the Remote Aggr Entry   */
/*                                        in which pRemotePortEntry should   */
/*                                        be found and deleted.              */
/*                      pRemotePortEntry - pointer to the remote port entry  */
/*                                         that needs to be deleted.         */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGDeleteFromRemotePortTable (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                 tLaDLAGRemotePortEntry * pRemotePortEntry)
{
    UNUSED_PARAM (pRemoteAggEntry);
    UNUSED_PARAM (pRemotePortEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGFormDSU.                                       */
/*                                                                           */
/* Description        : This function is called to form a LACPDU from the    */
/*                      particular LaDLAGTxDSU to form D-LAG PDU.            */
/*                                                                           */
/* Input(s)           : pAggEntry - Port channel which is trying to send     */
/*                                  D-LAG PDU.                               */
/*                      pu1LinearBuf - Pointer to the linear OUT buffer.     */
/*                      pLaDLAGTxInfo :                                      */
/*                               NULL - Periodic-sync PDU                    */
/*                               info to be embedded PDU - Event-update      */
/*                                                         D-LAG pdu.        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGFormDSU (tLaLacAggEntry * pAggEntry, UINT1 *pu1LinearBuf,
               tLaDLAGTxInfo * pLaDLAGTxInfo)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pu1LinearBuf);
    UNUSED_PARAM (pLaDLAGTxInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGInitForcefulLacpTx.                            */
/*                                                                           */
/* Description        : This function is called to sent LACP PDU's on all    */
/*                      aggregator ports configured for an Aggregator with   */
/*                      actor synchronization bit set to desired value       */
/*                      either IN_SYNC/OUT_OF_SYNC                           */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel Index.                   */
/*                      LaSynchronization - Synchronization bit value        */
/*                                          Possible values are IN_SYNC/     */
/*                                          OUT_OF_SYNC                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGInitForcefulLacpTx (tLaLacAggEntry * pAggEntry,
                          tLaBoolean LaSynchronization)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (LaSynchronization);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGProcessDLAGPdu.                                */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      to processs D-LAG periodic-sync and event-update     */
/*                      PDU's                                                */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                      node on Distributing port.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGProcessDLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf)
{
    UNUSED_PARAM (u2RecvdPortIndex);
    UNUSED_PARAM (pu1LinearBuf);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGELectorSelectionProcess                        */
/*                                                                           */
/* Description        : This function is called on MS Selection timer expiry */
/*                      to processs Perform Elector selection                */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
VOID
LaDLAGElectorSelectionProcess (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGScanLaListAndInitDLAG.                         */
/*                                                                           */
/* Description        : This function is called to scan the aggregator list  */
/*                      and initialize the D-LAG functionality if D-LAG      */
/*                      status is enabled.                                   */
/*                                                                           */
/* Called From        :  LaEnable ()                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaDLAGScanLaListAndInitDLAG (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGScanLaListAndDeInitDLAG.                       */
/*                                                                           */
/* Description        : This function is called to scan the aggregator list  */
/*                      and De-initialize the D-LAG functionality if D-LAG   */
/*                      status is enabled.                                   */
/*                                                                           */
/* Called From        :  LaDisable ()                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaDLAGScanLaListAndDeInitDLAG (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTriggerMSSelectionProcess.                     */
/*                                                                           */
/* Description        : This function is called to start the master slave    */
/*                      selection algorithm directly if Master slave         */
/*                      selection timer is zero, else if not zero then timer */
/*                      is started Master slave selection algorithm will be  */
/*                      called inside LaTimerHandler, on expiry of this      */
/*                      timer.                                               */
/*                                                                           */
/* Input(s)           : pAggEntry  - Pointer to Port-channel entry.          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGTriggerMSSelectionProcess (tLaLacAggEntry * pAggEntry,
                                 tMacAddr PeerMacAddr,
                                 tLaBoolean IsRemoteElector, UINT1 u1Trigger)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (PeerMacAddr);
    UNUSED_PARAM (IsRemoteElector);
    UNUSED_PARAM (u1Trigger);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGProcessDLAGPdu.                          */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      to processs Active-Active D-LAG periodic-sync        */
/*                      and event-update PDU's                               */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                                                                           */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                      node on Distributing port.                           */
/*                                                                           */
/*                      u4ByteCount - Length of the pu1LinearBuf.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaActiveDLAGProcessDLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf,
                            UINT4 u4ByteCount)
{
    UNUSED_PARAM (u2RecvdPortIndex);
    UNUSED_PARAM (pu1LinearBuf);
    UNUSED_PARAM (u4ByteCount);
    return LA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    FUNCTION NAME    : LaActiveDLAGFillPortSpeedInPdu                     */
/*                                                                          */
/*    DESCRIPTION      : This function is used to fill the Speed of         */
/*                       Port in Active DLAG PDU                            */
/*                                                                          */
/*    INPUT            : pAggEntry - Pointer to AggEntry,                   */
/*                       pIssPortCtrlSpeed - pointer to Speed variable      */
/*                                                                          */
/*    OUTPUT           : None                                               */
/*                                                                          */
/*    RETURNS          : Speed of Aggregator                                */
/*                                                                          */
/****************************************************************************/

VOID
LaActiveDLAGFillPortSpeedInPdu (tLaLacAggEntry * pAggEntry,
                                tIssPortCtrlSpeed * pIssPortCtrlSpeed)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pIssPortCtrlSpeed);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    FUNCTION NAME    : LaActiveDLAGGetPortSpeedFromPdu                     */
/*                                                                           */
/*    DESCRIPTION      : This function is used to extract the Speed of       */
/*                       Port from Active DLAG PDU.                          */
/*                                                                           */
/*    INPUT            : u1Speed - Speed value extracted from DLAG Pdu       */
/*                       pIfInfo - CFA structure to fill Speed.              */
/*                                                                           */
/*    OUTPUT           : IfInfo containg Speed & HighSpeed                   */
/*                                                                           */
/*    RETURNS          : None                                                */
/*                                                                           */
/****************************************************************************/

VOID
LaActiveDLAGGetPortSpeedFromPdu (UINT1 u1Speed, tCfaIfInfo * pIfInfo)
{
    UNUSED_PARAM (u1Speed);
    UNUSED_PARAM (pIfInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGSortOnStandbyConstraints                 */
/*                                                                           */
/* Description        : This function is used to sort the port entries       */
/*                      present in Add and Standby ports RBTree and          */
/*                      select the maximum ports allowed based on priority.  */
/*                                                                           */
/* Input(s)           : pAggEntry- Master Aggregator entry for which         */
/*                                 consolidation  neede                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/

INT4
LaActiveDLAGSortOnStandbyConstraints (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGChkUpdateAggEntryConsInfo                      */
/*                                                                           */
/* Description        : This function is used to compare Best Info of the    */
/*                      received PDU (MTU, speed ) with the current          */
/*                      information and store the best information.          */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry which is having best    */
/*                                   consolidated info                       */
/*                                                                           */
/*                      RecvInfo - Compare info which is having speed,MTU */
/*                                    of port-channel                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_AA_DLAG_MTU_AND_SPEED_SAME -                      */
/*                          -  If MTU and speed same with Best Info          */
/*                      LA_AA_DLAG_MTU_OR_SPEED_GREATER -                    */
/*                          - If MTU or Speed greater with Best Info         */
/*                      LA_AA_DLAG_MTU_OR_SPEED_LESSER -                     */
/*                          - If MTU or Speed Lesser with Best Info          */
/*                      LA_FALSE- If Best Info going to be modified by       */
/*                                  previous best                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGChkUpdateAggEntryConsInfo (tLaLacAggEntry * pAggEntry,
                                       tLaDLAGSpeedPropInfo RecvInfo)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (RecvInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetPortEntry                             */
/*                                                                           */
/* Description        : This function returns the pointer to Port Entry      */
/*                      from PortList  RBTree if exists.                     */
/*                                                                           */
/* Input(s)           : u4RemotPortIndex - Port Index of the port            */
/*                      ppConsPortEntry - Pointer to Cons port entry         */
/*                      pAggEntry -Aggregator entry in which RBTree exits    */
/*                                                                           */
/* Output(s)          : ppConsPortEntry - Pointer to the Cons port entry     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - if port entry is NULL                   */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGGetPortEntry (UINT4 u4RemotPortIndex,
                          tLaDLAGConsPortEntry ** ppConsPortEntry,
                          tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (u4RemotPortIndex);
    UNUSED_PARAM (ppConsPortEntry);
    UNUSED_PARAM (pAggEntry);
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGCreateConsPortEntry                      */
/*                                                                           */
/* Description        : This function creates consolidated port entry for    */
/*                      a given port  by allocating memory for it.           */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port Index of the port                 */
/*                      ppConsPortEntry - Pointer to Cons port entry         */
/*                      u2Priority - Priority of port                        */
/*                                                                           */
/* Output(s)          : ppConsPortEntry - Pointer to the Cons port entry     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_MEM_FAILURE- if memory allocation fails       */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGCreateConsPortEntry (tLaDLAGConsPortEntry ** ppConsPortEntry,
                                 UINT4 u4PortIndex, UINT2 u2Priority)
{
    UNUSED_PARAM (ppConsPortEntry);
    UNUSED_PARAM (u4PortIndex);
    UNUSED_PARAM (u2Priority);
    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGDeleteConsPortEntry                      */
/*                                                                           */
/* Description        : This function Delets consolidated port entry for     */
/*                      a given port by Releasing memory for it.             */
/*                                                                           */
/* Input(s)           : ppConsPortEntry - Pointer to Cons port entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_MEM_FAILURE- if memory release fails          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGDeleteConsPortEntry (tLaDLAGConsPortEntry * pConsPortEntry)
{
    UNUSED_PARAM (pConsPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGAddPortEntry                             */
/*                                                                           */
/* Description        : This function adds a consolidated port entry to      */
/*                      given aggregator Add RBTree.                         */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                      port entry that to added             */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGAddPortEntry (tLaLacAggEntry * pAggEntry,
                          tLaDLAGConsPortEntry * pConsPortEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pConsPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGAddToConsPortListTree                    */
/*                                                                           */
/* Description        : This function adds a consolidated port entry to      */
/*                      given aggregator Consolidated RBTree.                */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                      port entry that to added             */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGAddToConsPortListTree (tLaLacAggEntry * pAggEntry,
                                   tLaDLAGConsPortEntry * pConsPortEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pConsPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGRemovePortEntry                          */
/*                                                                           */
/* Description        : This function removes a Port entry from              */
/*                      given aggregator port list RBTree.                   */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                       port entry that to be removed       */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGRemovePortEntry (tLaLacAggEntry * pAggEntry,
                             tLaDLAGConsPortEntry * pConsPortEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pConsPortEntry);
    return LA_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : LaActiveDLAGGetNextPortEntry                           */
/*                                                                             */
/* Description        : This function returns the pointer to Next Port         */
/*                      Entry for the given port if Entry is Present           */
/*                      Else it returns NULL.                                  */
/*                      If This function is called with NULL it will get the   */
/*                      first port entry from PortTable                        */
/*                                                                             */
/* Input(s)           : pConsPortEntry- Pointer to the current port entry      */
/*                      ppNextConsPortEntry - Pointer to Next port entry       */
/*                      pAggEntry -Aggregator entry in which RBTree exits      */
/*                                                                             */
/* Output(s)          : ppNextConsPortEntry - Pointer to the Next port entry   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/* Global Variables                                                            */
/* Modified           : None.                                                  */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On success                                */
/*                      LA_FAILURE - if port entry is NULL                     */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*******************************************************************************/

INT4
LaActiveDLAGGetNextPortEntry (tLaDLAGConsPortEntry * pConsPortEntry,
                              tLaDLAGConsPortEntry **
                              ppNextConsPortEntry, tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pConsPortEntry);
    UNUSED_PARAM (ppNextConsPortEntry);
    UNUSED_PARAM (pAggEntry);
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaActiveMasterDLAGUpdateConsolidatedList             */
/*                                                                           */
/* Description        : This function will be called by Master if Master     */
/*                      has local port channel member ports.                 */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which Master ports   */
/*                                  participating in LAG are there           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGMasterUpdateConsolidatedList (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/*********************************************************************************/
/* Function Name      : LaDLAGRemoteAggInfoTreeDestroy                           */
/*                                                                               */
/* Description        : This routine is used to destroy the RBTree               */
/*                                                                               */
/* Input(s)           : u4Arg    - Offset for free function                      */
/*                      freeFn   - Free Function required for RBTree Destruction */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : Pointer to RBTree                                        */
/*                                                                               */
/*********************************************************************************/

VOID
LaActiveDLAGDestroyPortListTree (tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg)
{
    UNUSED_PARAM (Tree);
    UNUSED_PARAM (FreeFn);
    UNUSED_PARAM (u4Arg);
    return;
}

/*******************************************************************************/
/* Function Name      : LaActiveDLAGGetNextPortEntryFromConsTree               */
/*                                                                             */
/* Description        : This function returns the pointer to Next Consolidated */
/*                      port entry for the given port entry from Consolidated  */
/*                      RBTree. Else it returns NULL.                          */
/*                      If This function is called with NULL it will get the   */
/*                      first port entry from Standby RBTree.                  */
/*                                                                             */
/* Input(s)           : pConsPortEntry - Pointer to the current port entry     */
/*                      ppNextConsPortEntry - Pointer to Next port entry       */
/*                      pAggEntry - Aggregator entry in which RBTree exits     */
/*                                                                             */
/* Output(s)          : ppNextConsPortEntry - Pointer to the Next port entry   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/* Global Variables                                                            */
/* Modified           : None.                                                  */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On success                                */
/*                      LA_FAILURE - if port entry is NULL                     */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*******************************************************************************/

INT4
LaActiveDLAGGetNextPortEntryFromConsTree (tLaDLAGConsPortEntry * pConsPortEntry,
                                          tLaDLAGConsPortEntry **
                                          ppNextConsPortEntry,
                                          tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pConsPortEntry);
    UNUSED_PARAM (ppNextConsPortEntry);
    UNUSED_PARAM (pAggEntry);
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGRemovePortEntryFromAddListTree           */
/*                                                                           */
/* Description        : This function removes a consolidated port entry from */
/*                      given aggregator Consolidated list RBTree.           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                       port entry that to be removed       */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGRemovePortEntryFromConsTree (tLaLacAggEntry * pAggEntry,
                                         tLaDLAGConsPortEntry * pConsPortEntry)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (pConsPortEntry);
    return LA_SUCCESS;
}

/**********************************************************************************/
/* Function Name      : LaActiveDLAGRemoveRemotePortFromPortList                  */
/*                                                                                */
/* Description        : This function removes a consolidated port entry from      */
/*                      given aggregator Add/Standby/Delete list RBTree.          */
/*                      If the port is in ADD list it is moved to Delete list and */
/*                      the flag set so that it will sent in DEL Pdu. At the same */
/*                      time the memory also released. If it is in Standby/Delete */
/*                      RBTree, no indication is required.                        */
/*                                                                                */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists       */
/*                                                                                */
/*                      u4RemotePortIndex - Remote port Index                     */
/* Global Variables                                                               */
/* Referred           : None                                                      */
/* Global Variables                                                               */
/* Modified           : None.                                                     */
/*                                                                                */
/* Output(s)          : None                                                      */
/*                                                                                */
/* Return Value(s)    : LA_SUCCESS - On Success                                   */
/*                      LA_FAILURE - On failure                                   */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed               */
/*                                                                                */
/*********************************************************************************/

INT4
LaActiveDLAGRemoveRemotePortFromPortList (tLaLacAggEntry * pAggEntry,
                                          UINT4 u4RemotePortIndex)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (u4RemotePortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGChkAddDelDSURequired                     */
/*                                                                           */
/* Description        : This function checks whether any of the ports        */
/*                      RecentlyChanged Flag is set in Add & Standby &       */
/*                      Delete RBTrees. If flag is set for atleast one port  */
/*                      return LA_TRUE so taht Add/Del PDU is triggered.     */
/*                      Else LA_FALSE is returned so that no PDU is triggered*/
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator in which RBTrees are there    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - If atleast one port flag is set            */
/*                      LA_FALSE - If none of ports flag is set              */
/*                      LA_ERR_NULL_PTR - If Null pointer received           */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGChkAddDelDSURequired (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaDLAGActivePrintAllTreePorts                        */
/*                                                                           */
/* Description        : This function prints all the ports of All RBTrees.   */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaDLAGActivePrintAllTreePorts (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGDeleteEntriesInAllTree                   */
/*                                                                           */
/* Description        : This function is called when DLAG is deinitialized   */
/*                      to delete all the ports entries in All RBTrees       */
/*                      and releases the memory allocated for it.            */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaActiveDLAGDeleteEntriesInAllTree (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGHwControl                                 */
/*                                                                            */
/* Description        : This function is useful in indicating to lower layer  */
/*                      hardware / interface about the mux state by calling   */
/*                      the callback functions provided by it. Presently,     */
/*                      the function just updates the event and calls mux.    */
/*                                                                            */
/* Input(s)           : pPortEntry - Pointer to PortEntry                     */
/*                      LaLacEvent - Enable/Disable Distributor               */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/
INT4
LaActiveDLAGHwControl (tLaLacPortEntry * pPortEntry, UINT1 LaLacEvent)
{
    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (LaLacEvent);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGInitParams                               */
/*                                                                           */
/* Description        : This function initializes the global variables used  */
/*                      by active D-LAG                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.GlobalDLAGInfo                         */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*****************************************************************************/

INT4
LaActiveDLAGInitParams ()
{
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveCopyGlobalAndInitDLAG                        */
/*                                                                           */
/* Description        : This function is called if Global DLAG is enabled    */
/*                      to copy the Global DLAG properties as their System   */
/*                      properties and Start the DLAG functionlity in that   */
/*                      Aggregator entry                                     */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which DLAG enabled   */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaActiveCopyGlobalAndInitDLAG (tLaLacAggEntry * pAggEntry)
{
    UNUSED_PARAM (pAggEntry);
    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGDisable                                   */
/*                                                                            */
/* Description        : This function is called to Disable the functionlities */
/*                      when Global DLAG Status is Disabled                   */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
INT4
LaActiveDLAGDisable ()
{
    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGEnable                                    */
/*                                                                            */
/* Description        : This function is called to Enable the functionalites  */
/*                      when Global DLAG status is enabled                    */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
INT4
LaActiveDLAGEnable ()
{
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGTxPeriodicSyncOrAddDelPdu                */
/*                                                                           */
/* Description        : This function is called on D-LAG periodic sync timer */
/*                      expiry to send D-LAG Periodic sync/update PDU.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGTxPeriodicSyncOrAddDelPdu ()
{
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGKeepAlive                                */
/*                                                                           */
/* Description        : This function called when expiry of Periodic timer   */
/*                      and increments the keep alive count and verifies the */
/*                      keep alive count is reached the maximum. If maximum  */
/*                      reached then the node is deleted from the database   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/

INT4
LaActiveDLAGKeepAlive ()
{
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetBridgeIndexFromIfIndex                */
/*                                                                           */
/* Description        : This function converts the CFA Index to              */
/*                      Bridge Index by referring Interface Map Table        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pu4BrgIndex - pointer to Bridge Index                */
/*                                                                           */
/* Return Value(s)    : LA_FAILURE - On Failure                              */
/*                      LA_SUCCESS - On Success                              */
/*****************************************************************************/
INT4
LaActiveDLAGGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4BrgIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetIfIndexFromBridgeIndex                */
/*                                                                           */
/* Description        : This function converts the Bridge Index to           */
/*                      CFA Index by referring CFA Interface Map Table       */
/*                                                                           */
/* Input(s)           : u4BrgIndex - Bridge index                            */
/*                                                                           */
/* Output(s)          : pu4IfIndex - Pointer to Interface Index              */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Succes                               */
/*                      KA_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
LaActiveDLAGGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4BrgIndex);
    UNUSED_PARAM (pu4IfIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetSlotIdFromBridgeIndex                 */
/*                                                                           */
/* Description        : This function converts the Bridge Index to           */
/*                      CFA Index by referring CFA Interface Map Table       */
/*                                                                           */
/* Input(s)           : u4BrgIndex - Bridge index                            */
/*                                                                           */
/* Output(s)          : pu4SlotId - Pointer to Slot id                       */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Succes                               */
/*                      KA_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
LaActiveDLAGGetSlotIdFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4SlotId)
{
    UNUSED_PARAM (u4BrgIndex);
    UNUSED_PARAM (pu4SlotId);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetRolePlayed                            */
/*                                                                           */
/* Description        : This function Gets the DLAG role to be played        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
LaActiveDLAGGetRolePlayed (VOID)
{
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGCheckIsLocalPort                         */
/*                                                                           */
/* Description        : This function checks if the port is local or Remote  */
/*                                                                           */
/* Input(s)           : u2PortNumber - Port Number                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_FAILURE- If port is remote port                   */
/*                      LA_SUCCESS- If port is Local port                    */
/*****************************************************************************/
INT4
LaActiveDLAGCheckIsLocalPort (UINT2 u2PortNumber)
{
    UNUSED_PARAM (u2PortNumber);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetSwitchMacAddress                      */
/*                                                                           */
/* Description        : This function is to get Switch mac-address           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1HwAddr - Pointer to store switch Mac-address      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaActiveDLAGGetSwitchMacAddress (UINT1 *pu1HwAddr)
{
    UNUSED_PARAM (pu1HwAddr);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGValidateSourceMacAddress                 */
/*                                                                           */
/* Description        : This function is to validate source  mac-address     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1HwAddr - Pointer to Hw Mac-address                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
LaActiveDLAGValidateSourceMacAddress (UINT1 *pu1HwAddr)
{
    UNUSED_PARAM (pu1HwAddr);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGSystemPriority                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG system priority        */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaDLAGSystemPriority - DLAG System Priority value*/
/*                        CliHandle              - CLI Handler               */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG System priority needs to be   */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGSystemPriority (tCliHandle CliHandle, UINT4 u4LaDLAGSystemPriority,
                             UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4LaDLAGSystemPriority);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGSystemPriority                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MC-LAG system priority      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaMCLAGSystemPriority - MC-LAG System Priority   */
/*                                                  value*/
/*                        CliHandle               - CLI Handler              */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       MC-LAG System priority needs to be  */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGSystemPriority (tCliHandle CliHandle,
                               UINT4 u4LaMCLAGSystemPriority, UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4LaMCLAGSystemPriority);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGSystemID                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG system ID              */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG System ID needs to be         */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGSystemID (tCliHandle CliHandle, tMacAddr SystemID,
                       UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (SystemID);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGSystemID                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MCLAG system ID             */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       MC-LAG System ID needs to be        */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGSystemID (tCliHandle CliHandle, tMacAddr SystemID,
                         UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (SystemID);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGPeriodicSyncTime                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG periodic sync time     */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4PeriodicSyncTime- DLAG Periodic Sync time value  */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG periodic-sync time needs to be*/
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGPeriodicSyncTime (tCliHandle CliHandle, UINT4 u4PeriodicSyncTime,
                               UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4PeriodicSyncTime);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetGlobalDLAGPeriodicSyncTime                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global DLAG periodic sync   */
/*                        time                                               */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4PeriodicSyncTime- DLAG Periodic Sync time value  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGGlobalPeriodicSyncTime (tCliHandle CliHandle,
                                     UINT4 u4PeriodicSyncTime)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4PeriodicSyncTime);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGGlobalSystemID                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global DLAG system ID       */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGGlobalSystemID (tCliHandle CliHandle, tMacAddr SystemID)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (SystemID);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetDLAGGlobalSystemID                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global MC-LAG system ID     */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetDLAGGlobalSystemID (tCliHandle CliHandle, tMacAddr SystemID)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (SystemID);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGGlobalPeriodicTime                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MC-LAG global periodic      */
/*                        sync. time                                         */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaMCLAGPeriodicTime   - MC-LAG periodic          */
/*                                                  sync. time               */
/*                        CliHandle               - CLI handler              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGGlobalPeriodicTime (tCliHandle CliHandle,
                                   UINT4 u4LaMCLAGPeriodicTime)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4LaMCLAGPeriodicTime);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGGlobalSystemPriority                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG Global system priority */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaDLAGSystemPriority - DLAG System Priority value*/
/*                        CliHandle              - CLI Handler               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGGlobalSystemPriority (tCliHandle CliHandle,
                                   UINT4 u4LaDLAGSystemPriority)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4LaDLAGSystemPriority);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGGlobalSystemPriority                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MCLAG Global system priority*/
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaMCLAGSystemPriority - MCLAG System Priority    */
/*                                                  value                    */
/*                        CliHandle               - CLI Handler              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGGlobalSystemPriority (tCliHandle CliHandle,
                                     UINT4 u4LaMCLAGSystemPriority)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4LaMCLAGSystemPriority);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGGlobalConfigureDistributePort                */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Global D-LAG          */
/*                        distributing port                                  */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGDistributingPortIndex - DLAG Distirbuting    */
/*                                                      port index           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureGlobalDistributePort (tCliHandle CliHandle,
                                     UINT4 u4DLAGDistributingPortIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4DLAGDistributingPortIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGMSSelectionWaitTime                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG Master Selection Wait  */
/*                        time                                               */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4MSSelectionWaitTime- DLAG MS Selection Wait Time */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG master-slave-selection wait   */
/*                                       time needs to be configured.        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGMSSelectionWaitTime (tCliHandle CliHandle,
                                  UINT4 u4MSSelectionWaitTime, UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4MSSelectionWaitTime);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGConfigureDistributePort                      */
/*                                                                           */
/*     DESCRIPTION      : This function configures the D-LAG distributing    */
/*                        port                                               */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGDistributingPortIndex - DLAG Distirbuting    */
/*                                                      port index           */
/*                        CliHandle  - CLI Handler                           */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG distributing port needs to be */
/*                                       configured.                         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureDistributePort (tCliHandle CliHandle,
                               UINT4 u4DLAGDistributingPortIndex,
                               UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4DLAGDistributingPortIndex);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGConfigureDistributePortList                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures the D-LAG distributing    */
/*                        port List                                          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGDistributingPortIndex - DLAG Distirbuting    */
/*                                                      port index           */
/*                        CliHandle  - CLI Handler                           */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG distributing port needs to be */
/*                                       configured.                         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureDistributePortList (tCliHandle CliHandle,
                                   UINT1 *pu1Ports, UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pu1Ports);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGConfigureGlobalDistributePortList            */
/*                                                                           */
/*     DESCRIPTION      : This function configures the D-LAG distributing    */
/*                        port List                                          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : pu1Ports - List of ports to be configured.         */
/*                                                                           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureGlobalDistributePortList (tCliHandle CliHandle, UINT1 *pu1Ports)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pu1Ports);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGStatus                                */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables D-LAG functionality */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGStatus - DLAG status (enable/disable)        */
/*                        CliHandle    - CLI Handler                         */
/*                        u4AggIndex   - AggIndex of port-channel in which   */
/*                                       D-LAG functionality needs to be     */
/*                                       enabled/disabled.                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGStatus (tCliHandle CliHandle, UINT4 u4DLAGStatus, UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4DLAGStatus);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables MC-LAG functionality*/
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4MCLAGStatus - MCLAG status (enable/disable)      */
/*                        CliHandle     - CLI Handler                        */
/*                        u4AggIndex    - AggIndex of port-channel in whih   */
/*                                        MC-LAG functionality needs to be   */
/*                                        enabled/disabled.                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGStatus (tCliHandle CliHandle, UINT4 u4MCLAGStatus,
                       UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4MCLAGStatus);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetMCLAGSystemStatus                             */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables MC-LAG functionality*/
/*                         globally in system                                */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4MCLAGStatus - MC-LAG status (enable/disable)     */
/*                        CliHandle    - CLI Handler                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetMCLAGSystemStatus (tCliHandle CliHandle, UINT4 u4MCLAGStatus)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4MCLAGStatus);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGRedundancy                            */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables D-LAG redundancy    */
/*                        feature                                            */
/*                                                                           */
/*     INPUT            : u4RedundancyFeat - D-LAG redundancy feature(ON/OFF)*/
/*                        CliHandle        - CLI Handler                     */
/*                        u4AggIndex       - AggIndex of port-channel in     */
/*                                           whic D-LAG redundancy feature   */
/*                                           needs to be switched ON/OFF.    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGRedundancy (tCliHandle CliHandle, UINT4 u4RedundancyFeat,
                         UINT4 u4AggIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4RedundancyFeat);
    UNUSED_PARAM (u4AggIndex);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGSystemStatus                          */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables D-LAG functionality */
/*                         globally in system                                */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGStatus - DLAG status (enable/disable)        */
/*                        CliHandle    - CLI Handler                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGSystemStatus (tCliHandle CliHandle, UINT4 u4DLAGStatus)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4DLAGStatus);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGShowCounters                                 */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Displays per portchannel DLAG counter information  */
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaDLAGShowCounters (tCliHandle CliHandle, INT4 i4Key)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Key);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGShowCounters                                */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Displays per portchannel MC-LAG counter information*/
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaMCLAGShowCounters (tCliHandle CliHandle, INT4 i4Key)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Key);
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGShowDetail                                  */
/*                                                                           */
/*     DESCRIPTION      : Displays MCLAG counter information per portchannel */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4Key -  specifies the portchannel index,          */
/*                        u1IsConsolidated - specifies consolidated or       */
/*                                           detailed info.                  */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaMCLAGShowDetail (tCliHandle CliHandle, INT4 i4Key, UINT1 u1IsConsolidated)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Key);
    UNUSED_PARAM (u1IsConsolidated);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGCalculateAggOperStatus                   */
/*                                                                           */
/* Description        : This function will give the Aggregator Oper status   */
/*                      based on the ports that are in Sync & Dist           */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregator Entry.             */
/*                      u1AggAdminStatus - Admin status of port-chanenl      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaActiveDLAGCalculateAggOperStatus (tLaLacAggEntry * pAggEntry,
                                    UINT1 u1AggAdminStatus)
{
    UNUSED_PARAM (pAggEntry);
    UNUSED_PARAM (u1AggAdminStatus);
    return LA_SUCCESS;
}
