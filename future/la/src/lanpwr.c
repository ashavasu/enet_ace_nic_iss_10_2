/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: lanpwr.c,v 1.5 2014/06/13 12:55:52 siva Exp $
 *
 * Description:This file contains the wrapper for
 *              for Hardware API's w.r.t LA
 *              <Part of dual node stacking model>
 *
 *******************************************************************/

#ifndef __LA_NP_WR_C__
#define __LA_NP_WR_C__

#include "lahdrs.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : LaNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tLaNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
LaNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tLaNpModInfo       *pLaNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pLaNpModInfo = &(pFsHwNp->LaNpModInfo);

    if (NULL == pLaNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_LA_HW_CREATE_AGG_GROUP:
        {
            tLaNpWrFsLaHwCreateAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwCreateAggGroup;
            u1RetVal =
                FsLaHwCreateAggGroup ((UINT2) pEntry->u4AggIndex,
                                      pEntry->pu2HwAggId);
            break;
        }
        case FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
        {
            tLaNpWrFsLaHwAddLinkToAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwAddLinkToAggGroup;
            u1RetVal =
                FsLaHwAddLinkToAggGroup ((UINT2) pEntry->u4AggIndex,
                                         (UINT2) pEntry->u4PortNumber,
                                         pEntry->pu2HwAggId);
            break;
        }
        case FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP:
        {
            tLaNpWrFsLaHwDlagAddLinkToAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwDlagAddLinkToAggGroup;
            u1RetVal =
                FsLaHwDlagAddLinkToAggGroup ((UINT2) pEntry->u4AggIndex,
                                             (UINT2) pEntry->u4PortNumber,
                                             pEntry->pu2HwAggId);
            break;
        }

        case FS_LA_HW_SET_SELECTION_POLICY:
        {
            tLaNpWrFsLaHwSetSelectionPolicy *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwSetSelectionPolicy;
            u1RetVal =
                FsLaHwSetSelectionPolicy ((UINT2) pEntry->u4AggIndex,
                                          pEntry->u1SelectionPolicy);
            break;
        }
        case FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST:
        {
            tLaNpWrFsLaHwSetSelectionPolicyBitList *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwSetSelectionPolicyBitList;
            u1RetVal =
                FsLaHwSetSelectionPolicyBitList ((UINT2) pEntry->u4AggIndex,
                                                 pEntry->
                                                 u4SelectionPolicyBitList);
            break;
        }
        case FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
        {
            tLaNpWrFsLaHwRemoveLinkFromAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwRemoveLinkFromAggGroup;
            u1RetVal =
                FsLaHwRemoveLinkFromAggGroup ((UINT2) pEntry->u4AggIndex,
                                              (UINT2) pEntry->u4PortNumber);
            break;
        }
        case FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP:
        {
            tLaNpWrFsLaHwDlagRemoveLinkFromAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwDlagRemoveLinkFromAggGroup;
            u1RetVal =
                FsLaHwDlagRemoveLinkFromAggGroup ((UINT2) pEntry->u4AggIndex,
                                                  (UINT2) pEntry->u4PortNumber);
            break;
        }
        case FS_LA_HW_DELETE_AGGREGATOR:
        {
            tLaNpWrFsLaHwDeleteAggregator *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwDeleteAggregator;
            u1RetVal = FsLaHwDeleteAggregator ((UINT2) pEntry->u4AggIndex);
            break;
        }
        case FS_LA_HW_INIT:
        {
            u1RetVal = FsLaHwInit ();
            break;
        }
        case FS_LA_HW_DE_INIT:
        {
            u1RetVal = FsLaHwDeInit ();
            break;
        }
        case FS_LA_HW_ENABLE_COLLECTION:
        {
            tLaNpWrFsLaHwEnableCollection *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwEnableCollection;
            u1RetVal =
                FsLaHwEnableCollection ((UINT2) pEntry->u4AggIndex,
                                        (UINT2) pEntry->u4PortNumber);
            break;
        }
        case FS_LA_HW_ENABLE_DISTRIBUTION:
        {
            tLaNpWrFsLaHwEnableDistribution *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwEnableDistribution;
            u1RetVal =
                FsLaHwEnableDistribution ((UINT2) pEntry->u4AggIndex,
                                          (UINT2) pEntry->u4PortNumber);
            break;
        }
        case FS_LA_HW_DISABLE_COLLECTION:
        {
            tLaNpWrFsLaHwDisableCollection *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwDisableCollection;
            u1RetVal =
                FsLaHwDisableCollection ((UINT2) pEntry->u4AggIndex,
                                         (UINT2) pEntry->u4PortNumber);
            break;
        }
        case FS_LA_HW_SET_PORT_CHANNEL_STATUS:
        {
            tLaNpWrFsLaHwSetPortChannelStatus *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwSetPortChannelStatus;
            u1RetVal =
                FsLaHwSetPortChannelStatus ((UINT2) pEntry->u4AggIndex,
                                            pEntry->u2Inst, pEntry->u1StpState);
            break;
        }
        case FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        {
            tLaNpWrFsLaHwAddPortToConfAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwAddPortToConfAggGroup;
            u1RetVal =
                FsLaHwAddPortToConfAggGroup ((UINT2) pEntry->u4AggIndex,
                                             (UINT2) pEntry->u4PortNumber);
            break;
        }
        case FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP:
        {
            tLaNpWrFsLaHwRemovePortFromConfAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwRemovePortFromConfAggGroup;
            u1RetVal =
                FsLaHwRemovePortFromConfAggGroup ((UINT2) pEntry->u4AggIndex,
                                                  (UINT2) pEntry->u4PortNumber);
            break;
        }

        case FS_LA_HW_DLAG_STATUS:
        {
            tLaNpWrFsLaHwDlagStatus *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwDlagStatus;
            u1RetVal = FsLaHwDlagStatus ((UINT2) pEntry->u1DlagStatus);
            break;
        }

#ifdef L2RED_WANTED
        case FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR:
        {
            tLaNpWrFsLaHwCleanAndDeleteAggregator *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaHwCleanAndDeleteAggregator;
            u1RetVal =
                FsLaHwCleanAndDeleteAggregator ((UINT2) pEntry->u4HwAggIndex);
            break;
        }
        case FS_LA_GET_NEXT_AGGREGATOR:
        {
            tLaNpWrFsLaGetNextAggregator *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaGetNextAggregator;
            pEntry->i4NextHwAggIndex =
                FsLaGetNextAggregator (pEntry->i4HwAggIndex, pEntry->pHwInfo);
            break;
        }
        case FS_LA_RED_HW_INIT:
        {
            u1RetVal = FsLaRedHwInit ();
            break;
        }
        case FS_LA_RED_HW_NP_DELETE_AGGREGATOR:
        {
            tLaNpWrFsLaRedHwNpDeleteAggregator *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaRedHwNpDeleteAggregator;
            u1RetVal = FsLaRedHwNpDeleteAggregator ((UINT2) pEntry->u4AggIndex);
            break;
        }
        case FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR:
        {
            tLaNpWrFsLaRedHwUpdateDBForAggregator *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaRedHwUpdateDBForAggregator;
            u1RetVal =
                FsLaRedHwUpdateDBForAggregator ((UINT2) pEntry->u4AggIndex,
                                                pEntry->ConfigPorts,
                                                pEntry->ActivePorts,
                                                pEntry->u1SelectionPolicy);
            break;
        }
        case FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT:
        {
            tLaNpWrFsLaRedHwNpUpdateDlfMcIpmcPort *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaRedHwNpUpdateDlfMcIpmcPort;
            u1RetVal =
                FsLaRedHwNpUpdateDlfMcIpmcPort ((UINT2) pEntry->u4AggIndex);
            break;
        }
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED
        case FS_LA_MBSM_HW_CREATE_AGG_GROUP:
        {
            tLaNpWrFsLaMbsmHwCreateAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwCreateAggGroup;
            u1RetVal =
                FsLaMbsmHwCreateAggGroup ((UINT2) pEntry->u4AggIndex,
                                          pEntry->pSlotInfo);
            break;
        }
        case FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP:
        {
            tLaNpWrFsLaMbsmHwAddPortToConfAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwAddPortToConfAggGroup;
            u1RetVal =
                FsLaMbsmHwAddPortToConfAggGroup ((UINT2) pEntry->u4AggIndex,
                                                 (UINT2) pEntry->u4PortNumber,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP:
        {
            tLaNpWrFsLaMbsmHwAddLinkToAggGroup *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwAddLinkToAggGroup;
            u1RetVal =
                FsLaMbsmHwAddLinkToAggGroup ((UINT2) pEntry->u4AggIndex,
                                             (UINT2) pEntry->u4PortNumber,
                                             pEntry->pSlotInfo);
            break;
        }
        case FS_LA_MBSM_HW_SET_SELECTION_POLICY:
        {
            tLaNpWrFsLaMbsmHwSetSelectionPolicy *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwSetSelectionPolicy;
            u1RetVal =
                FsLaMbsmHwSetSelectionPolicy ((UINT2) pEntry->u4AggIndex,
                                              pEntry->u1SelectionPolicy,
                                              pEntry->pSlotInfo);
            break;
        }
        case FS_LA_MBSM_HW_INIT:
        {
            tLaNpWrFsLaMbsmHwInit *pEntry = NULL;
            pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwInit;
            u1RetVal = FsLaMbsmHwInit (pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __LA_NP_WR_C__ */
