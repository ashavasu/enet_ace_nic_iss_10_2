#ifndef _LACONFIG_C
#define _LACONFIG_C
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: laconfig.c,v 1.75 2016/06/22 10:17:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : laconfig.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREGATION                               */
/*    MODULE NAME           : Link Aggregation module init/deinit            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains init, deinit functions      */
/*                            for Link Aggregation module                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"
extern UINT4        FsLaPortActorResetAdminState[12];
extern UINT4        FsLaPortMode[12];
extern UINT4        Dot3adAggPortActorAdminKey[11];

/*****************************************************************************/
/* Function Name      : LaEnableLacpOnPort                                   */
/*                                                                           */
/* Description        : This function is called from LaChangeLacpMode to     */
/*                      enable LACP on a particular port.                    */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry *pPortEntry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
/* LACP is enabled from LACP_DISABLED mode only */
INT4
LaEnableLacpOnPort (tLaLacPortEntry * pPortEntry)
{
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSNMP_OCTET_STRING_TYPE ActorAdminState;
    UINT1               u1AdminState = 0;
    LA_MEMSET (&SnmpNotifyInfo, LA_INIT_VAL, sizeof (tSnmpNotifyInfo));
    LA_MEMSET (&ActorAdminState, LA_INIT_VAL, sizeof (tSNMP_OCTET_STRING_TYPE));
#endif
    /* We want to run lacp protocol on this port */
    pPortEntry->LaLacpMode = LA_MODE_LACP;

    pPortEntry->LaLacActorInfo.u2IfKey =
        pPortEntry->LaLacActorAdminInfo.u2IfKey;
    pPortEntry->LaLacActorInfo.LaLacPortState =
        pPortEntry->LaLacActorAdminInfo.LaLacPortState;

    /* Make the port aggregatable */
    pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaAggregation = LA_TRUE;

#ifdef SNMP_2_WANTED
    /* Send Notification to MSR for saving the ActorAdminPortState */
    SnmpNotifyInfo.pu4ObjectId = FsLaPortActorResetAdminState;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsLaPortActorResetAdminState) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LaLock;
    SnmpNotifyInfo.pUnLockPointer = LaUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    ActorAdminState.pu1_OctetList = &u1AdminState;
    ActorAdminState.i4_Length = sizeof (UINT1);

    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                          pPortEntry->u2PortIndex, &ActorAdminState));
#endif

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
         (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER))
    {
        LaActiveDLAGMasterUpdateConsolidatedList (pPortEntry->pAggEntry);
    }
#ifdef ICCH_WANTED
    else if ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
             (LA_AA_DLAG_ROLE_PLAYED == LA_ICCH_MASTER))
    {
        LaActiveMCLAGMasterUpdateConsolidatedList (pPortEntry->pAggEntry);
    }
#endif

    if ((pPortEntry->pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED) ||
        (LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE))
    {
        LaDLAGChangeAttachedPortActorSysLAGIDToDLAG (pPortEntry);
    }

    /* Initialise the state machines */
    LaInitPort (pPortEntry);

    /* This global variable is used to control LACP PDU negotiation once the 
     * Port Channel Interface is made admin down */
    /* If gu4LagAdminDownNoNeg is LA_TRUE, LACP PDU negotiation will be
     * stopped. If it is LA_FALSE, LACP PDU negotiation can happen through 
     * the member ports though port channel interface is down*/
    if (gu4LagAdminDownNoNeg == LA_FALSE)
    {
        if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
        {
            LaHandleEnablePort (pPortEntry);
        }
    }
    else
    {
        if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
            (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP))
        {
            LaHandleEnablePort (pPortEntry);
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDisableLacpOnPort                                  */
/*                                                                           */
/* Description        : This function is called from LaChangeLacpMode to     */
/*                      disable LACP on a particular port.                   */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry *pPortEntry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaDisableLacpOnPort (tLaLacPortEntry * pPortEntry)
{
    /* Stop tick timer */
    if (LaStopTimer (&(pPortEntry->LaTickTimer)) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    pPortEntry->LaPortEnabled = LA_PORT_DISABLED;

    /* If D-LAG status is enabled, reset the port properties */
    if ((pPortEntry->pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED) ||
        (LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE))
    {
        LaDLAGResetPortActorSysLAGID (pPortEntry);
    }

    LaLacRxMachine (pPortEntry, LA_RXM_EVENT_PORT_DISABLED, NULL);
    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_PORT_DISABLED);
    LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);

    /* Delete all cache entries for the port */
    LaAggDeleteCacheEntry (pPortEntry);
    /* Delete the marker entry for the port */
    LaAggDeleteMarkerEntry (pPortEntry);

    /* Make the port non-aggregatable */
    pPortEntry->LaLacActorInfo.LaLacPortState.LaAggregation = LA_FALSE;
    pPortEntry->LaLacpMode = LA_MODE_DISABLED;

    pPortEntry->LaLacActorInfo.u2IfKey = 0;
    pPortEntry->LaLacActorAdminInfo.u2IfIndex = pPortEntry->u2PortIndex;

    pPortEntry->LaLacActorInfo.LaLacPortState =
        gLaGlobalInfo.LaPortDefaultState;

    pPortEntry->LaLacActorAdminInfo.LaLacPortState =
        gLaGlobalInfo.LaPortDefaultState;
    pPortEntry->LaLacPartnerInfo.LaLacPortState =
        gLaGlobalInfo.LaPartnerDefaultState;

    pPortEntry->LaLacPartnerAdminInfo.LaLacPortState =
        gLaGlobalInfo.LaPartnerDefaultState;

    pPortEntry->LaLacPartnerInfo.LaSystem = gLaGlobalInfo.LaPartnerSystem;
    pPortEntry->LaLacPartnerAdminInfo.LaSystem = gLaGlobalInfo.LaPartnerSystem;
    pPortEntry->LaLacPartnerInfo.u2IfKey = 0;
    pPortEntry->LaLacPartnerInfo.u2IfPriority =
        LA_DEFAULT_PARTNER_PORT_PRIORITY;
    pPortEntry->LaLacPartnerInfo.u2IfIndex = 0;
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaEnableManualConfigOnPort                           */
/*                                                                           */
/* Description        : This function is called from LaChangeLacpMode to     */
/*                      Enable Manual Aggregation on a particular port.      */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry *pPortEntry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/

INT4
LaEnableManualConfigOnPort (tLaLacPortEntry * pPortEntry)
{
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSNMP_OCTET_STRING_TYPE ActorAdminState;
    UINT1               u1AdminState = 0;
    LA_MEMSET (&SnmpNotifyInfo, LA_INIT_VAL, sizeof (tSnmpNotifyInfo));
    LA_MEMSET (&ActorAdminState, LA_INIT_VAL, sizeof (tSNMP_OCTET_STRING_TYPE));
#endif

    pPortEntry->LaLacpMode = LA_MODE_MANUAL;

    pPortEntry->LaLacActorInfo.u2IfKey =
        pPortEntry->LaLacActorAdminInfo.u2IfKey;
    pPortEntry->LaLacActorInfo.LaLacPortState =
        pPortEntry->LaLacActorAdminInfo.LaLacPortState;

    /* Make the port aggregatable */
    pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaAggregation = LA_TRUE;
    /* Make the partner aggregatable */
    pPortEntry->LaLacPartnerAdminInfo.LaLacPortState.LaAggregation = LA_TRUE;
    pPortEntry->pAggEntry->AggConfigEntry.AggOrIndividual = LA_TRUE;

    /* Make the Partner Admin Key same as the Actor Admin Key */
    pPortEntry->LaLacPartnerAdminInfo.u2IfKey =
        pPortEntry->LaLacActorAdminInfo.u2IfKey;

    /* Make the Partner Admin Port same as the Actor Admin Port */
    pPortEntry->LaLacPartnerAdminInfo.u2IfIndex =
        pPortEntry->LaLacActorAdminInfo.u2IfIndex;

#ifdef SNMP_2_WANTED
    /* Send Notification to MSR for saving the ActorAdminPortState */
    SnmpNotifyInfo.pu4ObjectId = FsLaPortActorResetAdminState;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsLaPortActorResetAdminState) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LaLock;
    SnmpNotifyInfo.pUnLockPointer = LaUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    ActorAdminState.pu1_OctetList = &u1AdminState;
    ActorAdminState.i4_Length = sizeof (UINT1);

    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                          pPortEntry->u2PortIndex, &ActorAdminState));
#endif

    /* Initialise the state machines */
    LaInitPort (pPortEntry);

    /* 
     * Although In Manual Mode no LACPDU is transmitted
     * the following code will take care of enabling 
     * the State machines based on the port-channel
     * Admin status when gu4LagAdminDownNoNeg is LA_TRUE. 
     * So that it avoids the mismatch of states in port
     * channel and members of port-channel.
     */

    if (gu4LagAdminDownNoNeg == LA_FALSE)
    {
        if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
        {
            LaHandleEnablePort (pPortEntry);
        }
    }
    else
    {
        if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
            (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP))
        {
            LaHandleEnablePort (pPortEntry);
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDisableManualConfigOnPort                          */
/*                                                                           */
/* Description        : This function is called from LaChangeLacpMode to     */
/*                      Disable Manual Aggregation on a particular port.     */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry *pPortEntry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/

INT4
LaDisableManualConfigOnPort (tLaLacPortEntry * pPortEntry)
{
    LaDisableLacpOnPort (pPortEntry);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangeLacpMode                                     */
/*                                                                           */
/* Description        : This function is called from Low level routine to    */
/*                      Change the LACP mode on the particular Port.         */
/*                      The mode can be changed from LacpEnable to           */
/*                      LacpDisable, or to ManualAggregation and vice versa. */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry *pPortEntry                     */
/*                      tLaLacpMode      OldLacpMode                         */
/*                      tLaLacpMode      NewLacpMode                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/

/* This function will be called from the low level routine.
 * In the low level routine, the NewLacpMode will be assigned to the 
 * port. */
INT4
LaChangeLacpMode (tLaLacPortEntry * pPortEntry,
                  tLaLacpMode OldLacpMode, tLaLacpMode NewLacpMode)
{
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tLaBoolean          LaDistributing = LA_TRUE;
#ifdef SNMP_2_WANTED
    tSNMP_OCTET_STRING_TYPE PortAdminState;
    UINT1               u1SetActivity = 0x00;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSnmpNotifyInfo     SnmpNotifyPortStateInfo;

    LA_MEMSET (&SnmpNotifyInfo, LA_INIT_VAL, sizeof (tSnmpNotifyInfo));
    LA_MEMSET (&SnmpNotifyPortStateInfo, LA_INIT_VAL, sizeof (tSnmpNotifyInfo));

    SnmpNotifyInfo.pu4ObjectId = Dot3adAggPortActorAdminKey;
    SnmpNotifyInfo.u4OidLen =
        sizeof (Dot3adAggPortActorAdminKey) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LaLock;
    SnmpNotifyInfo.pUnLockPointer = LaUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;


    SnmpNotifyPortStateInfo.pu4ObjectId = FsLaPortActorResetAdminState;
    SnmpNotifyPortStateInfo.u4OidLen =
        sizeof (FsLaPortActorResetAdminState) / sizeof (UINT4);
    SnmpNotifyPortStateInfo.u4SeqNum = 0;
    SnmpNotifyPortStateInfo.u1RowStatus = FALSE;
    SnmpNotifyPortStateInfo.pLockPointer = LaLock;
    SnmpNotifyPortStateInfo.pUnLockPointer = LaUnLock;
    SnmpNotifyPortStateInfo.u4Indices = 1;
    SnmpNotifyPortStateInfo.i1ConfStatus = SNMP_SUCCESS; 
    PortAdminState.i4_Length = 1;
    PortAdminState.pu1_OctetList = &u1SetActivity;
#endif

    if (LA_MODULE_STATUS == LA_DISABLED)
    {
        if ((NewLacpMode == LA_MODE_DISABLED)
            && (OldLacpMode != LA_MODE_DISABLED))
        {
            if (OldLacpMode == LA_MODE_MANUAL)
            {
                 /* Reset the LacpMode in ActorAdminInfo to default value when
                  * lacp mode changed from LA_MODE_MANUAL to LA_MODE_DISABLED */
                  pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaLacpActivity =
                        gLaGlobalInfo.LaPortDefaultState.LaLacpActivity;
            }

            LaRemovePortFromAggregator (pPortEntry);
            pPortEntry->LaLacActorAdminInfo.u2IfKey = 0;
#ifdef SNMP_2_WANTED
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  (INT4) pPortEntry->u2PortIndex, 0));
            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyPortStateInfo, "%i %s",
                              (INT4) pPortEntry->u2PortIndex, &PortAdminState));
#endif
        }
        pPortEntry->LaLacpMode = NewLacpMode;
        return LA_SUCCESS;
    }
    pLaLacAggEntry = pPortEntry->pAggEntry;

    if (pLaLacAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    switch (OldLacpMode)
    {
        case LA_MODE_MANUAL:
            if (NewLacpMode == LA_MODE_LACP)
            {
                LaDisableManualConfigOnPort (pPortEntry);
                LaEnableLacpOnPort (pPortEntry);
            }
            else if (NewLacpMode == LA_MODE_DISABLED)
            {
                /* Store the disstributing state before it gets
                 * modified below */
                LaDistributing = pPortEntry->LaLacActorInfo.
                    LaLacPortState.LaDistributing;

                if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
                     (pPortEntry->u1DLAGMasterAck == LA_TRUE))
                {
                    /* In case of remove port from port-channel, before getting ACK
                     * from Master, the information is removed from LA local database and
                     * indicated the same to the higher layer. only NP programming alone
                     * is done after getting ACK from Master.
                     * */
                    LaActiveDLAGHwControl (pPortEntry,
                                           LA_HW_EVENT_DISABLE_DISTRIBUTOR);

                }
#ifdef ICCH_WANTED
                else if ((LaIsMCLAGEnabledOnAgg (pLaLacAggEntry) == LA_TRUE) &&
                         (pPortEntry->u1DLAGMasterAck == LA_TRUE))
                {
                    LaActiveMCLAGHwControl (pPortEntry,
                                            LA_HW_EVENT_DISABLE_DISTRIBUTOR);
                }
#endif

                LaDisableManualConfigOnPort (pPortEntry);

                /* Reset the LacpMode in ActorAdminInfo to default value when 
                 * lacp mode changed from LA_MODE_MANUAL to LA_MODE_DISABLED */
                pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaLacpActivity =
                    gLaGlobalInfo.LaPortDefaultState.LaLacpActivity;

                /* Indicate Vlan to remove port properties from Hardware */
                LaL2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                                  pPortEntry->pAggEntry->
                                                  u2AggIndex);

#ifdef NPAPI_WANTED
                if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                {
                    LaFsLaHwRemovePortFromConfAggGroup (pPortEntry->pAggEntry->
                                                        u2AggIndex,
                                                        pPortEntry->
                                                        u2PortIndex);
                }
#endif

                /* Remove Port from PortChannel */
                LaRemovePortFromAggregator (pPortEntry);
                pPortEntry->LaLacActorAdminInfo.u2IfKey = 0;
#ifdef SNMP_2_WANTED
                SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                      (INT4) pPortEntry->u2PortIndex, 0));
                SNMP_MSR_NOTIFY_CFG ((SnmpNotifyPortStateInfo, "%i %s",
                              (INT4) pPortEntry->u2PortIndex, &PortAdminState));
#endif

#ifdef NPAPI_WANTED
                /* Port, which is removed from the LAGG, will be created as an
                 * individual port in the higher layer modules under the
                 * following scenarios -
                 *   1) Port was a Standby port (it would NOT have been
                 *      present in hardware)
                 *   2) NPAPI mode is synchronous and removing the port
                 *      from the LAGG in hardware was successful.
                 * 
                 * In case of asynchronous NPAPI mode the creation will be 
                 * handled in the NPAPI callback */
                if ((LaDistributing == LA_FALSE) &&
                    (pPortEntry->LaLacHwPortStatus !=
                     LA_HW_REM_PORT_FAILED_IN_LAGG) &&
                    (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG))
                    /* LaLAcHwPortStatus is added to takecare following 
                     * scenario:
                     * If the Remove port HW calls fails, then we should not
                     * create physical port, to do this, we are checking for 
                     * HWPortStatus != LA_HW_REM_PORT_FAILED_IN_LAGG 
                     * In the standby, when LA_NP_DEL_PORT_FAILURE_INFO doesnt 
                     * reach it,SW audit takes 40 seconds to detect out-of-sync.
                     * In the mean time, if "no channel-group" is executed,then 
                     * physical port should not be created, so the HWPortStatus
                     * is checked for LA_HW_PORT_ADDED_IN_LAGG */

                {
                    /* Create Physcial Port to Bridge */
                    LaHlCreatePhysicalPort (pPortEntry);

                }
                else if (pPortEntry->LaLacHwPortStatus ==
                         LA_HW_PORT_NOT_IN_LAGG)
                    /* This gets executed in both SYNC and ASYNC mode.
                     * In the Sync mode, this is done to ensure the port 
                     * removal is done successfully
                     * In the Async mode, this condition executes only in the 
                     * standby node. When the LA is disabled for the port (no 
                     * Channel-group command is executed), then Active deletes
                     * the port from the HW, and sends a RM message in the NP
                     * callback function. As part of normal flow, CLI also 
                     * syncs this conf with the standby through a RM message.
                     * These RM messages reaches the Standby node (through CLI 
                     * task & LA task), Since LA task has the highest priority,
                     * RM message sent in the NP callback function gets 
                     * executed first. In the NP callback function we check for
                     * the pPortEntry->pAggEntry == NULL to create a physcial
                     * port, this will not be true because the RM message
                     * received through CLI task is not yet executed, which 
                     * makes this NULL.
                     *
                     * To handle this scenario, in the NP callback function,we 
                     * set the LaLacHwPortStatus to LA_HE_PORT_NOT_IN_LAGG
                     * and the physical port is created in the CLI flow (i.e,
                     * this flow) */
                {
                    /* Create Physcial Port to Bridge */
                    LaHlCreatePhysicalPort (pPortEntry);
                }
#else /* NPAPI_WANTED */

                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);

#endif /* !NPAPI_WANTED */
            }
            break;

        case LA_MODE_LACP:
            if (NewLacpMode == LA_MODE_MANUAL)
            {
                LaDisableLacpOnPort (pPortEntry);
                LaEnableManualConfigOnPort (pPortEntry);
            }
            else if (NewLacpMode == LA_MODE_DISABLED)
            {
                /* Store the disstributing state before it gets
                 * modified below */
                LaDistributing = pPortEntry->LaLacActorInfo.
                    LaLacPortState.LaDistributing;

                if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
                     (pPortEntry->u1DLAGMasterAck == LA_TRUE)) 
                {
                    /* In case of remove port from port-channel, before getting ACK
                     * from Master, the information is removed from LA local database and
                     * indicated the same to the higher layer. only NP programming alone
                     * is done after getting ACK from Master.
                     * */
                    LaActiveDLAGHwControl (pPortEntry,
                                           LA_HW_EVENT_DISABLE_DISTRIBUTOR);

                }
#ifdef ICCH_WANTED
                else if ((LaIsMCLAGEnabledOnAgg (pLaLacAggEntry) == LA_TRUE) &&
                         (pPortEntry->u1DLAGMasterAck == LA_TRUE))
                {
                    LaActiveMCLAGHwControl (pPortEntry,
                                            LA_HW_EVENT_DISABLE_DISTRIBUTOR);
                }
#endif

                LaDisableLacpOnPort (pPortEntry);
                /* Indicate Vlan to remove port properties from Hardware */
                LaL2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                                  pPortEntry->pAggEntry->
                                                  u2AggIndex);

#ifdef NPAPI_WANTED
                if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                {

                    LaFsLaHwRemovePortFromConfAggGroup (pPortEntry->pAggEntry->
                                                        u2AggIndex,
                                                        pPortEntry->
                                                        u2PortIndex);
                }
#endif
                /* Remove Port from PortChannel */
                LaRemovePortFromAggregator (pPortEntry);
                pPortEntry->LaLacActorAdminInfo.u2IfKey = 0;
#ifdef SNMP_2_WANTED
                SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                      (INT4) pPortEntry->u2PortIndex, 0));
#endif
#ifdef NPAPI_WANTED
                /* Port, which is removed from the LAGG, will be created as an
                 * individual port in the higher layer modules under the
                 * following scenarios -
                 *   1) Port was a Standby port (it would NOT have been
                 *      present in hardware)
                 *   2) NPAPI mode is synchronous and removing the port
                 *      from the LAGG in hardware was successful.
                 * 
                 * In case of asynchronous NPAPI mode the creation will be 
                 * handled in the NPAPI callback */
                if ((LaDistributing == LA_FALSE) &&
                    (pPortEntry->LaLacHwPortStatus !=
                     LA_HW_REM_PORT_FAILED_IN_LAGG) &&
                    (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG))
                {
                    /* Create Physcial Port to Bridge */
                    LaHlCreatePhysicalPort (pPortEntry);

                }
                else if (pPortEntry->LaLacHwPortStatus ==
                         LA_HW_PORT_NOT_IN_LAGG)
                {
                    /* Create Physcial Port to Bridge */
                    LaHlCreatePhysicalPort (pPortEntry);
                }
#else /* NPAPI_WANTED */

                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);

#endif /* !NPAPI_WANTED */
            }
            break;

        case LA_MODE_DISABLED:
            if (NewLacpMode == LA_MODE_LACP)
            {
                /* Delete Physcial Port from Bridge and add to port-channel */
                if ((gu4LagAdminDownNoNeg == LA_TRUE) && (gu4PartnerConfig == LA_ENABLED))
                {
                    if (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP)
                    {
                        if (LaHlDeletePhysicalPort (pPortEntry) == LA_FAILURE)
                        {
                            return LA_FAILURE;
                        }
                    }
                }
                else
                {
                    if (LaHlDeletePhysicalPort (pPortEntry) == LA_FAILURE)
                    {
                        return LA_FAILURE;
                    }
                }
#ifdef NPAPI_WANTED
                if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                {

                    LaFsLaHwAddPortToConfAggGroup (pPortEntry->pAggEntry->
                                                   u2AggIndex,
                                                   pPortEntry->u2PortIndex);
                }
#endif

                LaEnableLacpOnPort (pPortEntry);
#ifdef SNMP_2_WANTED
                SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                      (INT4) pPortEntry->u2PortIndex,
                                      (INT4) pPortEntry->LaLacActorAdminInfo.
                                      u2IfKey));
#endif
            }
            else if (NewLacpMode == LA_MODE_MANUAL)
            {
#ifdef NPAPI_WANTED
                if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                {

                    LaFsLaHwAddPortToConfAggGroup (pPortEntry->pAggEntry->
                                                   u2AggIndex,
                                                   pPortEntry->u2PortIndex);
                }
#endif

                /* Delete Physcial Port from Bridge and add to port-channel */
                LaHlDeletePhysicalPort (pPortEntry);
                LaEnableManualConfigOnPort (pPortEntry);
            }
            break;
        default:
            return LA_FAILURE;
    }
#ifndef NPAPI_WANTED
    UNUSED_PARAM (LaDistributing);
#endif

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaChangeActorPortState                               */
/*                                                                           */
/* Description        : This function sets the actor's port state variables  */
/*                      This function is called by the low-level routines    */
/*                      and is used to either set or reset the port state    */
/*                      bits.                                                */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of the port whose port state has */
/*                                    to be changed                          */
/*                      u1PortStateVar - The Port state bit to be set or     */
/*                                       reset                               */
/*                      u1Set - Indicates the value to which the PortState   */
/*                                variable needs to be changed to (SET or    */
/*                                RESET)                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
LaChangeActorPortState (UINT2 u2PortIndex, UINT1 u1PortStateVar, UINT1 u1Set)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacPortState     PrevPartnerState;
    tLaSelect           PrevAggSelected;

    LaGetPortEntry (u2PortIndex, &pPortEntry);
    LA_MEMSET (&PrevPartnerState, LA_INIT_VAL, sizeof (tLaLacPortState));
    LA_MEMSET (&PrevAggSelected, LA_INIT_VAL, sizeof (tLaSelect));

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Null Pointer for Port %d \r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }

    PrevAggSelected = pPortEntry->AggSelected;
    PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;

    switch (u1PortStateVar)
    {

        case LA_LACP_ACTIVITY:
            /* Update the Actor's Admin and Oper values for Lacp Activity  */
            if (u1Set == LA_SET)
            {
                if (pPortEntry->
                    LaLacActorAdminInfo.LaLacPortState.LaLacpActivity ==
                    LA_ACTIVE)
                {
                    return LA_SUCCESS;
                }
                else
                {
                    pPortEntry->
                        LaLacActorAdminInfo.LaLacPortState.LaLacpActivity =
                        LA_ACTIVE;

                    if (LA_MODULE_STATUS == LA_ENABLED)
                    {
                        pPortEntry->
                            LaLacActorInfo.LaLacPortState.LaLacpActivity =
                            LA_ACTIVE;
                    }
                }
            }
            else if (u1Set == LA_RESET)
            {
                if (pPortEntry->
                    LaLacActorAdminInfo.LaLacPortState.LaLacpActivity ==
                    LA_PASSIVE)
                {
                    return LA_SUCCESS;
                }
                else
                {
                    pPortEntry->
                        LaLacActorAdminInfo.LaLacPortState.LaLacpActivity =
                        LA_PASSIVE;

                    if (LA_MODULE_STATUS == LA_ENABLED)
                    {
                        pPortEntry->
                            LaLacActorInfo.LaLacPortState.LaLacpActivity =
                            LA_PASSIVE;
                    }
                }
            }

            if ((LA_MODULE_STATUS == LA_ENABLED) &&
                (pPortEntry->LaLacpMode == LA_MODE_LACP))
            {
                /* Update the partner about this timeout change */
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
                LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);
            }
            LA_TRC (CONTROL_PLANE_TRC | MGMT_TRC,
                    "UTL: LacpActivity bit value changed SUCCESSFULLY\n");
            break;
        case LA_LACP_TIMEOUT:
            if (u1Set == LA_SET)
            {
                if (pPortEntry->
                    LaLacActorAdminInfo.LaLacPortState.LaLacpTimeout ==
                    LA_SHORT_TIMEOUT)
                {
                    return LA_SUCCESS;
                }
                else
                {
                    pPortEntry->
                        LaLacActorAdminInfo.LaLacPortState.LaLacpTimeout =
                        LA_SHORT_TIMEOUT;

                    if (LA_MODULE_STATUS == LA_ENABLED)
                    {
                        pPortEntry->
                            LaLacActorInfo.LaLacPortState.LaLacpTimeout =
                            LA_SHORT_TIMEOUT;
                    }
                }
            }
            else if (u1Set == LA_RESET)
            {
                if (pPortEntry->
                    LaLacActorAdminInfo.LaLacPortState.LaLacpTimeout ==
                    LA_LONG_TIMEOUT)
                {
                    return LA_SUCCESS;
                }
                else
                {
                    pPortEntry->
                        LaLacActorAdminInfo.LaLacPortState.LaLacpTimeout =
                        LA_LONG_TIMEOUT;

                    if (LA_MODULE_STATUS == LA_ENABLED)
                    {
                        pPortEntry->
                            LaLacActorInfo.LaLacPortState.LaLacpTimeout =
                            LA_LONG_TIMEOUT;
                    }
                }
            }

            if ((LA_MODULE_STATUS == LA_ENABLED) &&
                (pPortEntry->LaLacpMode == LA_MODE_LACP))
            {
                /* Update the partner about this timeout change */
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
            }
            LA_TRC (CONTROL_PLANE_TRC | MGMT_TRC,
                    "UTL: LacpTimeout bit value changed SUCCESSFULLY\n");
            break;
        case LA_AGGREGATION:
            if (u1Set == LA_SET)
            {
                if (pPortEntry->
                    LaLacActorAdminInfo.LaLacPortState.LaAggregation == LA_TRUE)
                {
                    return LA_SUCCESS;
                }
                else
                {
                    pPortEntry->
                        LaLacActorAdminInfo.LaLacPortState.LaAggregation =
                        LA_TRUE;

                    if (LA_MODULE_STATUS == LA_ENABLED)
                    {
                        pPortEntry->
                            LaLacActorInfo.LaLacPortState.LaAggregation =
                            LA_TRUE;
                    }
                }
            }
            else if (u1Set == LA_RESET)
            {
                if (pPortEntry->
                    LaLacActorAdminInfo.LaLacPortState.LaAggregation ==
                    LA_FALSE)
                {
                    return LA_SUCCESS;
                }
                else
                {
                    pPortEntry->
                        LaLacActorAdminInfo.LaLacPortState.LaAggregation =
                        LA_FALSE;

                    if (LA_MODULE_STATUS == LA_ENABLED)
                    {
                        pPortEntry->
                            LaLacActorInfo.LaLacPortState.LaAggregation =
                            LA_FALSE;
                    }
                }
            }
            if ((LA_MODULE_STATUS == LA_ENABLED)
                && (pPortEntry->LaLacpMode == LA_MODE_LACP))
            {
                pPortEntry->AggSelected = LA_UNSELECTED;
                pPortEntry->LaLacPartnerInfo.
                    LaLacPortState.LaSynchronization = LA_FALSE;
                /* Remove/Add Port form PortChannel when Individual/Aggregation is set  */
                LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected,
                                        PrevPartnerState);
            }

            break;
        default:
            return LA_SUCCESS;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangeActorAdminKey.                               */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Actor's key value.                               */
/*                                                                           */
/* Input(s)           : Port Index, Key value for the Port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangeActorAdminKey (UINT2 u2PortIndex, UINT2 u2PortKey)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tLaLacAggEntry     *pTmpAggEntry = NULL;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;

    SnmpNotifyInfo.pu4ObjectId = FsLaPortMode;
    SnmpNotifyInfo.u4OidLen = sizeof (FsLaPortMode) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = LaLock;
    SnmpNotifyInfo.pUnLockPointer = LaUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
#endif

    if (LaGetPortEntry (u2PortIndex, &pPortEntry) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Null Pointer for PortIndex %d \r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }

    if (LaGetAggEntryByKey (u2PortKey, &pLaLacAggEntry) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    if (pLaLacAggEntry == NULL)
    {
        /* create the port channel for this key before assigning to port */
        return LA_FAILURE;
    }
    else
    {
        /* NO of Ports configured for the aggregator in LA_MODE_MANUAL
         * should not exceed the configured MaxPorts value for that
         * aggregator */
        if ((pLaLacAggEntry->u1ConfigPortCount ==
             pLaLacAggEntry->u1MaxPortsToAttach) &&
            (pLaLacAggEntry->LaLacpMode == LA_MODE_MANUAL))
        {
            return LA_FAILURE;
        }

        /* Aggregator already exists */
        /* When no port is configured to the aggregator
         * allow assigning of port to the aggregator.
         * If there are more port assigned to this aggregator
         * check of the aggregator mode is not disabled. ie., the
         * first port assigned to the aggregator should have the
         * mode configured for it and it is the mode for the
         * aggregator.
         */
        if (pLaLacAggEntry->u1ConfigPortCount > 0)
        {
            if (pLaLacAggEntry->LaLacpMode == LA_MODE_DISABLED)
            {
                return LA_FAILURE;
            }
        }
        else
        {
            pLaLacAggEntry->LaLacpMode = LA_MODE_DISABLED;
        }
    }
    /* Remove this port from any previously configured aggregator */
    LaGetAggEntryBasedOnPort (u2PortIndex, &pTmpAggEntry);

    if (pTmpAggEntry != NULL)
    {
        if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
        {
            LaChangeLacpMode (pPortEntry,
                              pPortEntry->LaLacpMode, LA_MODE_DISABLED);
        }
        else
        {
            LaRemovePortFromAggregator (pPortEntry);
            pPortEntry->LaLacActorAdminInfo.u2IfKey = 0;
            pPortEntry->LaLacActorInfo.u2IfKey = 0;
        }
    }

    pPortEntry->LaLacActorAdminInfo.u2IfKey = u2PortKey;
    if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
    {
        pPortEntry->LaLacActorInfo.u2IfKey =
            pPortEntry->LaLacActorAdminInfo.u2IfKey;
    }

    if (pPortEntry->bDynAggSelect == LA_TRUE)
    {
        LaAddPortToAggregator (pLaLacAggEntry, pPortEntry);

        /* For dynamic aggregator, always enable LACP on the port
         * and set mode as active by default */
        pLaLacAggEntry->LaLacpMode = LA_MODE_LACP;
        if ((LaChangeLacpMode (pPortEntry, pPortEntry->LaLacpMode,
                               LA_MODE_LACP)) == LA_FAILURE)
        {
            return LA_FAILURE;
        }

        /* Set LACP mode as Active */
        if ((LaChangeActorPortState (pPortEntry->u2PortIndex,
                                     LA_LACP_ACTIVITY, LA_SET)) == LA_FAILURE)
        {
            return LA_FAILURE;
        }
#ifdef SNMP_2_WANTED
        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              (INT4) pPortEntry->u2PortIndex,
                              (INT4) LA_MODE_LACP));
#endif
    }
    else
    {
        /* Static - Add to configured aggregator */
        LaAddPortToAggregator (pLaLacAggEntry, pPortEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangeActorSystemPriority                          */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Actor's System Priority.                         */
/*                                                                           */
/* Input(s)           : Port Index, System priority                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangeActorSystemPriority (UINT2 u2PortIndex, UINT2 u2SystemPriority)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry;
    UINT2               u2Index;

    UNUSED_PARAM (u2PortIndex);

    gLaGlobalInfo.LaSystem.u2SystemPriority = u2SystemPriority;
    /* Change Actor System Priority in Aggregators */
    LaGetNextAggEntry (NULL, &pTmpAggEntry);
    while (pTmpAggEntry != NULL)
    {
        pTmpAggEntry->AggConfigEntry.
            ActorSystem.u2SystemPriority = u2SystemPriority;
        LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    }

    /* Change Actor System Priority in Ports */
    for (u2Index = 1; u2Index <= LA_MAX_PORTS; u2Index++)
    {
        LaGetPortEntry (u2Index, &pPortEntry);

        if (pPortEntry == NULL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Null Pointer for Port %d \r\n",
                         u2Index);
            continue;
        }

        if (pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
            != u2SystemPriority)
        {
            pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
                = u2SystemPriority;
            pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority
                = u2SystemPriority;

            if (LA_MODULE_STATUS == LA_ENABLED)
            {
                if (pPortEntry->LaLacpMode == LA_MODE_LACP)
                {
                    LaLacRxMachine (pPortEntry,
                                    LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
                }
            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangeActorPortPriority.                           */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Actor's Port Priority.                           */
/*                                                                           */
/* Input(s)           : Port Index, Key value for the Port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangeActorPortPriority (UINT2 u2PortIndex, UINT2 u2PortPriority)
{
    tLaLacPortEntry    *pPortEntry;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4BrgPortIndex = 0;

    tLaLacPortState     PrevPartnerState;
    tLaSelect           PrevAggSelected;

    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "PortIndex  %d  Not known to LA\r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }

    /* Update the actor's Admin and Oper values for the Key */
    if (pPortEntry->LaLacActorAdminInfo.u2IfPriority != u2PortPriority)
    {
        pPortEntry->LaLacActorAdminInfo.u2IfPriority = u2PortPriority;
        pPortEntry->LaLacActorInfo.u2IfPriority = u2PortPriority;

        if (LA_MODULE_STATUS == LA_DISABLED)
        {
            return LA_SUCCESS;
        }

        if (pPortEntry->LaLacpEnabled != LA_LACP_ENABLED)
        {
            return LA_SUCCESS;
        }

        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            PrevAggSelected = pPortEntry->AggSelected;
            PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;
            pPortEntry->AggSelected = LA_UNSELECTED;
            if (pPortEntry->InDefaulted == LA_FALSE)
            {
                pPortEntry->LaLacPartnerInfo.
                    LaLacPortState.LaSynchronization = LA_FALSE;
            }
            /* Remove/Add Port from PortChannel when Individual/Aggregation is set  */
            LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected,
                                    PrevPartnerState);
        }

        if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
             (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER)) ||
            ((LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE) &&
             (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER)))
        {
            LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                                   u2PortIndex,
                                                   &u4BrgPortIndex);

            LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry,
                                      pPortEntry->pAggEntry);

            if (pConsPortEntry != NULL)
            {
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    LaActiveDLAGRemovePortEntryFromConsTree (pPortEntry->
                                                             pAggEntry,
                                                             pConsPortEntry);

                    pConsPortEntry->u2Priority = pPortEntry->LaLacActorInfo.
                        u2IfPriority;
                    LaActiveDLAGAddToConsPortListTree (pPortEntry->pAggEntry,
                                                       pConsPortEntry);
                }
                else
                {
                    pConsPortEntry->u2Priority = pPortEntry->LaLacActorInfo.
                        u2IfPriority;
                }
            }

        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangeAggMacAddress.                               */
/*                                                                           */
/* Description        : This function is called by administrator(or) during  */
/*                      protocol operation to change Aggregator Mac Address  */
/*                                                                           */
/* Input(s)           : pLaLacAggEntry-Pointer to the portChannel            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/

INT4
LaChangeAggMacAddress (UINT4 u4IfIndex, tLaLacAggEntry * pLaLacAggEntry)
{
    tCfaIfInfo          CfaIfInfo;

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    if (LaCfaGetIfInfo (u4IfIndex, &CfaIfInfo) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    LA_MEMCPY (pLaLacAggEntry->AggConfigEntry.AggMacAddr,
               CfaIfInfo.au1MacAddr, LA_MAC_ADDRESS_SIZE);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaUpdateTableChangedTime.                            */
/*                                                                           */
/* Description        : This function is called by whenever a value in       */
/*                      Aggregator Table and Port Table changed.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaTablesLastChanged                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/

VOID
LaUpdateTableChangedTime (VOID)
{
    tLaSysTime          CurrentTime;

    LA_GET_SYS_TIME (&CurrentTime);

    gLaGlobalInfo.LaTablesLastChanged = CurrentTime;

    return;
}

/*****************************************************************************/
/* Function Name      : LaUpdateActorSystemID.                               */
/*                                                                           */
/* Description        : This function is called by administrator while       */
/*                      configuring AggActorsystemID.                        */
/*                                                                           */
/* Input(s)           : tLaMacAddr- MacAddr to be updated as SystemID        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/

INT4
LaUpdateActorSystemID (tLaMacAddr NewMacAddr)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pTmpAggEntry;
    UINT2               u2Index;
    UINT1               au1LocalMac[LA_MAC_ADDRESS_SIZE];

    LA_MEMSET (au1LocalMac, 0, LA_MAC_ADDRESS_SIZE);

    if (LA_MEMCMP (NewMacAddr, gLaGlobalInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE) == 0)
    {
        return LA_SUCCESS;
    }

    if (LA_MEMCMP (NewMacAddr, au1LocalMac, LA_MAC_ADDRESS_SIZE) == 0)
    {
        return LA_FAILURE;
    }

    LA_MEMCPY (gLaGlobalInfo.LaSystem.SystemMacAddr, NewMacAddr,
               LA_MAC_ADDRESS_SIZE);

    /* Change Actor System ID in Aggregators */
    /* Get First Aggregator in the System */
    LaGetNextAggEntry (NULL, &pTmpAggEntry);

    while (pTmpAggEntry != NULL)
    {
        if (pTmpAggEntry->u1MCLAGStatus != LA_MCLAG_ENABLED)
        {
            LA_MEMCPY (pTmpAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
                       NewMacAddr, LA_MAC_ADDRESS_SIZE);
        }
        LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    }

    /* Change Actor System ID in Ports */
    for (u2Index = 1; u2Index <= LA_MAX_PORTS; u2Index++)
    {
        LaGetPortEntry (u2Index, &pPortEntry);

        if (pPortEntry == NULL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Null Pointer for Port %d \r\n",
                         u2Index);
            continue;
        }

        if ((pPortEntry->pAggEntry != NULL) &&
            (pPortEntry->pAggEntry->u1MCLAGStatus != LA_MCLAG_ENABLED))
        {
            LA_MEMCPY (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                       NewMacAddr, LA_MAC_ADDRESS_SIZE);
            LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                       NewMacAddr, LA_MAC_ADDRESS_SIZE);

            if (LA_MODULE_STATUS == LA_ENABLED)
            {
                if (pPortEntry->LaLacpMode == LA_MODE_LACP)
                {
                    LaLacRxMachine (pPortEntry,
                                    LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
                }
            }
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangePortActorAdminPort                           */
/*                                                                           */
/* Description        : This function is called by administrator while       */
/*                      configuring Actor Admin Port value in LACP PDU's     */
/*                                                                           */
/* Input(s)           : u2Port - Current physical port                       */
/*                      u2AdminPort - Port to be set as Admin Port           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangePortActorAdminPort (UINT2 u2Port, UINT2 u2AdminPort)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacPortState     PrevPartnerState;
    tLaSelect           PrevAggSelected;

    LaGetPortEntry (u2Port, &pPortEntry);

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "PortIndex  %d  Not known to LA\r\n",
                     u2Port);
        return LA_FAILURE;
    }
    /* Update the actor's Admin and Oper values */
    if (pPortEntry->LaLacActorAdminInfo.u2IfIndex != u2AdminPort)
    {
        pPortEntry->LaLacActorAdminInfo.u2IfIndex = u2AdminPort;
        pPortEntry->LaLacActorInfo.u2IfIndex = u2AdminPort;
        if (LA_MODULE_STATUS == LA_DISABLED)
        {
            return LA_SUCCESS;
        }

        if (pPortEntry->LaLacpEnabled != LA_LACP_ENABLED)
        {
            return LA_SUCCESS;
        }
        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            PrevAggSelected = pPortEntry->AggSelected;
            PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;
            pPortEntry->AggSelected = LA_UNSELECTED;
            if (pPortEntry->InDefaulted == LA_FALSE)
            {
                pPortEntry->LaLacPartnerInfo.
                    LaLacPortState.LaSynchronization = LA_FALSE;
            }
            /* Remove/Add Port from PortChannel when Individual/Aggregation
             * is set 
             */
            LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected,
                                    PrevPartnerState);
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangePartnerPortPriority.                         */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Partner's Admin Port Priority.                   */
/*                                                                           */
/* Input(s)           : Port Index, PortPriority value for the Port          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangePartnerPortPriority (UINT2 u2PortIndex, UINT2 u2PortPriority)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "PortIndex  %d  Not known to LA\r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }
    pAggEntry = pPortEntry->pAggEntry;

    /* Update the partner's Admin values for the Priority */
    if (pPortEntry->LaLacPartnerAdminInfo.u2IfPriority != u2PortPriority)
    {
        pPortEntry->LaLacPartnerAdminInfo.u2IfPriority = u2PortPriority;
        if (LA_MODULE_STATUS == LA_DISABLED)
        {
            return LA_SUCCESS;
        }
        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            if (pPortEntry->InDefaulted == LA_TRUE)
            {
                pPortEntry->LaLacPartnerInfo.u2IfPriority = u2PortPriority;
                if (pPortEntry->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                    pPortEntry->AggSelected = LA_UNSELECTED;
                    LaLacMuxControlMachine (pPortEntry,
                                            LA_MUX_EVENT_UNSELECTED);
                    pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
                }
                LaLacpSelectionLogic (pPortEntry);
            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangePartnerPortKey.                              */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Partner's Admin Port Key.                        */
/*                                                                           */
/* Input(s)           : Port Index, Key value for the Port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangePartnerPortKey (UINT2 u2PortIndex, UINT2 u2PortKey)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "PortIndex  %d  Not known to LA\r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }
    pAggEntry = pPortEntry->pAggEntry;

    /* Update the partner's Admin values */
    if (pPortEntry->LaLacPartnerAdminInfo.u2IfKey != u2PortKey)
    {
        pPortEntry->LaLacPartnerAdminInfo.u2IfKey = u2PortKey;
        if (LA_MODULE_STATUS == LA_DISABLED)
        {
            return LA_SUCCESS;
        }
        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            if (pPortEntry->InDefaulted == LA_TRUE)
            {
                pPortEntry->LaLacPartnerInfo.u2IfKey = u2PortKey;
                if (pPortEntry->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                    pPortEntry->AggSelected = LA_UNSELECTED;
                    LaLacMuxControlMachine (pPortEntry,
                                            LA_MUX_EVENT_UNSELECTED);
                    pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
                }
                LaLacpSelectionLogic (pPortEntry);
            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangePartnerPortNumber.                           */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Partner's Admin Port Number.                     */
/*                                                                           */
/* Input(s)           : Port Index, Port Number value for the Port           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangePartnerPortNumber (UINT2 u2PortIndex, UINT2 u2PortNumber)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "PortIndex  %d  Not known to LA\r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }
    pAggEntry = pPortEntry->pAggEntry;

    /* Update the partner's Admin values for the Number */
    if (pPortEntry->LaLacPartnerAdminInfo.u2IfIndex != u2PortNumber)
    {
        pPortEntry->LaLacPartnerAdminInfo.u2IfIndex = u2PortNumber;
        if (LA_MODULE_STATUS == LA_DISABLED)
        {
            return LA_SUCCESS;
        }
        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            if (pPortEntry->InDefaulted == LA_TRUE)
            {
                pPortEntry->LaLacPartnerInfo.u2IfIndex = u2PortNumber;
                if (pPortEntry->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                    pPortEntry->AggSelected = LA_UNSELECTED;
                    LaLacMuxControlMachine (pPortEntry,
                                            LA_MUX_EVENT_UNSELECTED);
                    pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
                }
                LaLacpSelectionLogic (pPortEntry);
            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaChangePartnerSystemPriority                        */
/*                                                                           */
/* Description        : This function is called by administrator to change   */
/*                      the Partner's System Priority.                       */
/*                                                                           */
/* Input(s)           : Port Index, System priority                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaChangePartnerSystemPriority (UINT2 u2PortIndex, UINT2 u2SystemPriority)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "PortIndex  %d  Not known to LA\r\n",
                     u2PortIndex);
        return LA_FAILURE;
    }

    pAggEntry = pPortEntry->pAggEntry;
    /* Update the partner's Admin values */
    if (pPortEntry->LaLacPartnerAdminInfo.LaSystem.u2SystemPriority !=
        u2SystemPriority)
    {
        pPortEntry->LaLacPartnerAdminInfo.LaSystem.u2SystemPriority =
            u2SystemPriority;
        if (LA_MODULE_STATUS == LA_DISABLED)
        {
            return LA_SUCCESS;
        }
        if (pPortEntry->LaLacpMode == LA_MODE_LACP)
        {
            if (pPortEntry->InDefaulted == LA_TRUE)
            {
                pPortEntry->LaLacPartnerInfo.LaSystem.u2SystemPriority =
                    u2SystemPriority;
                if (pPortEntry->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                    pPortEntry->AggSelected = LA_UNSELECTED;
                    LaLacMuxControlMachine (pPortEntry,
                                            LA_MUX_EVENT_UNSELECTED);
                    pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
                }
                LaLacpSelectionLogic (pPortEntry);
            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaUpdatePartnerSystemID.                             */
/*                                                                           */
/* Description        : This function is called by administrator while       */
/*                      configuring PartnerSystemID.                         */
/*                                                                           */
/* Input(s)           : tLaMacAddr- MacAddr to be updated as SystemID        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apPortEntry                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaUpdatePartnerSystemID (UINT2 u2Index, tLaMacAddr NewMacAddr)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetPortEntry (u2Index, &pPortEntry);

    /* Change Partner System ID in Ports */

    if (pPortEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "Null Pointer for Port %d \r\n",
                     u2Index);
        return LA_FAILURE;
    }
    pAggEntry = pPortEntry->pAggEntry;

    LA_MEMCPY (pPortEntry->LaLacPartnerAdminInfo.LaSystem.SystemMacAddr,
               NewMacAddr, LA_MAC_ADDRESS_SIZE);

    if (pPortEntry->InDefaulted == LA_TRUE)
    {
        LA_MEMCPY (pPortEntry->LaLacPartnerInfo.LaSystem.SystemMacAddr,
                   NewMacAddr, LA_MAC_ADDRESS_SIZE);

        if (pPortEntry->AggSelected == LA_SELECTED)
        {
            pAggEntry->u1SelectedPortCount--;
            pPortEntry->AggSelected = LA_UNSELECTED;
            LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_UNSELECTED);
            pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
        }
        LaLacpSelectionLogic (pPortEntry);
    }
    return LA_SUCCESS;
}

#endif /* LACONFIG_C */
