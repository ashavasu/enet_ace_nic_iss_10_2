/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: lastdlow.c,v 1.69 2017/10/11 13:41:28 siva Exp $     */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : lastdlow.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Standard MIB Low Level Routines             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains low level routines for all  */
/*                            Standard MIB objects                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*****************************************************************************/

/*****************************************************************************/
/*                          INCLUDE FILES                                    */
/*****************************************************************************/
#include  "lahdrs.h"

#include  "fssnmp.h"
#include "iss.h"
#include "lacli.h"
#include "cli.h"
#include "fslacli.h"

INT4                LaCheckLaDynPortSpeed (UINT4 *pu4ErrorCode,
                                           tLaLacAggEntry * pAggEntry,
                                           UINT4 u4IfHighSpeed,
                                           INT4 i4Dot3adAggPortIndex);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot3adTablesLastChanged
Input       :  The Indices

The Object 
retValDot3adTablesLastChanged
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adTablesLastChanged (UINT4 *pu4RetValDot3adTablesLastChanged)
{
    *pu4RetValDot3adTablesLastChanged = gLaGlobalInfo.LaTablesLastChanged;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3adAggTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot3adAggTable
Input       :  The Indices
Dot3adAggIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhValidateIndexInstanceDot3adAggTable (INT4 i4Dot3adAggIndex)
{

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFirstIndexDot3adAggTable
Input       :  The Indices
Dot3adAggIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhGetFirstIndexDot3adAggTable (INT4 *pi4Dot3adAggIndex)
{
    if (LaSnmpLowGetFirstValidAggIndex (pi4Dot3adAggIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexDot3adAggTable
Input       :  The Indices
Dot3adAggIndex
nextDot3adAggIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetNextIndexDot3adAggTable (INT4 i4Dot3adAggIndex,
                               INT4 *pi4NextDot3adAggIndex)
{

    if (i4Dot3adAggIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    if (LaSnmpLowGetNextValidAggIndex (i4Dot3adAggIndex, pi4NextDot3adAggIndex)
        != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot3adAggMACAddress
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggMACAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggMACAddress (INT4 i4Dot3adAggIndex,
                           tMacAddr * pRetValDot3adAggMACAddress)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        LA_MEMCPY (pRetValDot3adAggMACAddress,
                   pAggEntry->AggConfigEntry.AggMacAddr, LA_MAC_ADDRESS_SIZE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot3adAggActorSystemPriority
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggActorSystemPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggActorSystemPriority (INT4 i4Dot3adAggIndex,
                                    INT4 *pi4RetValDot3adAggActorSystemPriority)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggActorSystemPriority =
            (INT4) pAggEntry->AggConfigEntry.ActorSystem.u2SystemPriority;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggActorSystemID
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggActorSystemID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggActorSystemID (INT4 i4Dot3adAggIndex,
                              tMacAddr * pRetValDot3adAggActorSystemID)
{

    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        LA_MEMCPY (pRetValDot3adAggActorSystemID,
                   pAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggAggregateOrIndividual
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggAggregateOrIndividual
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggAggregateOrIndividual (INT4 i4Dot3adAggIndex,
                                      INT4
                                      *pi4RetValDot3adAggAggregateOrIndividual)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        if (pAggEntry->AggConfigEntry.AggOrIndividual == LA_TRUE)
        {
            *pi4RetValDot3adAggAggregateOrIndividual = LA_SNMP_TRUE;
        }
        else
        {
            *pi4RetValDot3adAggAggregateOrIndividual = LA_SNMP_FALSE;
        }
        return SNMP_SUCCESS;

    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
Function    :  nmhGetDot3adAggActorAdminKey
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggActorAdminKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggActorAdminKey (INT4 i4Dot3adAggIndex,
                              INT4 *pi4RetValDot3adAggActorAdminKey)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggActorAdminKey =
            (INT4) pAggEntry->AggConfigEntry.u2ActorAdminKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggActorOperKey
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggActorOperKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggActorOperKey (INT4 i4Dot3adAggIndex,
                             INT4 *pi4RetValDot3adAggActorOperKey)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggActorOperKey =
            (INT4) pAggEntry->AggConfigEntry.u2ActorOperKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPartnerSystemID
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggPartnerSystemID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPartnerSystemID (INT4 i4Dot3adAggIndex,
                                tMacAddr * pRetValDot3adAggPartnerSystemID)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);

        LA_MEMCPY (pRetValDot3adAggPartnerSystemID,
                   pAggEntry->AggConfigEntry.PartnerSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPartnerSystemPriority
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggPartnerSystemPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPartnerSystemPriority (INT4 i4Dot3adAggIndex,
                                      INT4
                                      *pi4RetValDot3adAggPartnerSystemPriority)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPartnerSystemPriority =
            (INT4) pAggEntry->AggConfigEntry.PartnerSystem.u2SystemPriority;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPartnerOperKey
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggPartnerOperKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPartnerOperKey (INT4 i4Dot3adAggIndex,
                               INT4 *pi4RetValDot3adAggPartnerOperKey)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPartnerOperKey =
            (INT4) pAggEntry->AggConfigEntry.u2PartnerOperKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDot3adAggCollectorMaxDelay
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggCollectorMaxDelay
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggCollectorMaxDelay (INT4 i4Dot3adAggIndex,
                                  INT4 *pi4RetValDot3adAggCollectorMaxDelay)
{

    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggCollectorMaxDelay =
            pAggEntry->AggConfigEntry.CollectorMaxDelay;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot3adAggActorSystemPriority
Input       :  The Indices
Dot3adAggIndex

The Object 
setValDot3adAggActorSystemPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggActorSystemPriority (INT4 i4Dot3adAggIndex,
                                    INT4 i4SetValDot3adAggActorSystemPriority)
{
    /* if you change Agg system priority what should be done */
    LaChangeActorSystemPriority ((UINT2) i4Dot3adAggIndex,
                                 (UINT2) i4SetValDot3adAggActorSystemPriority);
    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot3adAggActorAdminKey
Input       :  The Indices
Dot3adAggIndex

The Object 
setValDot3adAggActorAdminKey
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggActorAdminKey (INT4 i4Dot3adAggIndex,
                              INT4 i4SetValDot3adAggActorAdminKey)
{
    UNUSED_PARAM (i4Dot3adAggIndex);
    UNUSED_PARAM (i4SetValDot3adAggActorAdminKey);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot3adAggCollectorMaxDelay
Input       :  The Indices
Dot3adAggIndex

The Object 
setValDot3adAggCollectorMaxDelay
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggCollectorMaxDelay (INT4 i4Dot3adAggIndex,
                                  INT4 i4SetValDot3adAggCollectorMaxDelay)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);
        if (pAggEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        pAggEntry->AggConfigEntry.CollectorMaxDelay =
            (UINT4) i4SetValDot3adAggCollectorMaxDelay;
        LaUpdateTableChangedTime ();
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot3adAggActorSystemPriority
Input       :  The Indices
Dot3adAggIndex

The Object 
testValDot3adAggActorSystemPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggActorSystemPriority (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot3adAggIndex,
                                       INT4
                                       i4TestValDot3adAggActorSystemPriority)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;

    if ((LaSnmpLowValidateAggIndex (i4Dot3adAggIndex)) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggActorSystemPriority < LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggActorSystemPriority > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if D-LAG Status is enabled in any of the
     * port channel if Yes this configuration should 
     * not be allowed. */
    LaGetNextAggEntry (NULL, &pTmpAggEntry);
    while (pTmpAggEntry != NULL)
    {
        if (pTmpAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
        {
            CLI_SET_ERR (CLI_LA_DLAG_STATUS_ENABLED);
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "D-LAG Status is enabled, In Po(%u)"
                         " port-channel, so System Priority cannot be "
                         "changed.\n", pTmpAggEntry->u2AggIndex);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggActorAdminKey
Input       :  The Indices
Dot3adAggIndex

The Object 
testValDot3adAggActorAdminKey
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggActorAdminKey (UINT4 *pu4ErrorCode, INT4 i4Dot3adAggIndex,
                                 INT4 i4TestValDot3adAggActorAdminKey)
{
    UNUSED_PARAM (i4Dot3adAggIndex);
    UNUSED_PARAM (i4TestValDot3adAggActorAdminKey);
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggCollectorMaxDelay
Input       :  The Indices
Dot3adAggIndex

The Object 
testValDot3adAggCollectorMaxDelay
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggCollectorMaxDelay (UINT4 *pu4ErrorCode, INT4 i4Dot3adAggIndex,
                                     INT4 i4TestValDot3adAggCollectorMaxDelay)
{
    if ((LaSnmpLowValidateAggIndex (i4Dot3adAggIndex)) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggCollectorMaxDelay < LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggCollectorMaxDelay > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot3adAggTable
 Input       :  The Indices
                Dot3adAggIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot3adAggTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3adAggPortListTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot3adAggPortListTable
Input       :  The Indices
Dot3adAggIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhValidateIndexInstanceDot3adAggPortListTable (INT4 i4Dot3adAggIndex)
{

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFirstIndexDot3adAggPortListTable
Input       :  The Indices
Dot3adAggIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3adAggPortListTable (INT4 *pi4Dot3adAggIndex)
{

    if (LaSnmpLowGetFirstValidAggIndex (pi4Dot3adAggIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexDot3adAggPortListTable
Input       :  The Indices
Dot3adAggIndex
nextDot3adAggIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3adAggPortListTable (INT4 i4Dot3adAggIndex,
                                       INT4 *pi4NextDot3adAggIndex)
{

    if (i4Dot3adAggIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    if (LaSnmpLowGetNextValidAggIndex (i4Dot3adAggIndex, pi4NextDot3adAggIndex)
        != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot3adAggPortListPorts
Input       :  The Indices
Dot3adAggIndex

The Object 
retValDot3adAggPortListPorts
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortListPorts (INT4 i4Dot3adAggIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDot3adAggPortListPorts)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaSnmpLowValidateAggIndex (i4Dot3adAggIndex) == LA_SUCCESS)
    {
        LaGetAggEntry ((UINT2) i4Dot3adAggIndex, &pAggEntry);

        pRetValDot3adAggPortListPorts->i4_Length = sizeof (tLaPortList);

        LA_MEMCPY (pRetValDot3adAggPortListPorts->pu1_OctetList,
                   &(pAggEntry->LaActivePorts),
                   pRetValDot3adAggPortListPorts->i4_Length);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : Dot3adAggPortTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot3adAggPortTable
Input       :  The Indices
Dot3adAggPortIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3adAggPortTable (INT4 i4Dot3adAggPortIndex)
{

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFirstIndexDot3adAggPortTable
Input       :  The Indices
Dot3adAggPortIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3adAggPortTable (INT4 *pi4Dot3adAggPortIndex)
{

    if (LaSnmpLowGetFirstValidPortIndex (pi4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexDot3adAggPortTable
Input       :  The Indices
Dot3adAggPortIndex
nextDot3adAggPortIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3adAggPortTable (INT4 i4Dot3adAggPortIndex,
                                   INT4 *pi4NextDot3adAggPortIndex)
{

    if (i4Dot3adAggPortIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    if (LaSnmpLowGetNextValidPortIndex (i4Dot3adAggPortIndex,
                                        pi4NextDot3adAggPortIndex) !=
        LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorSystemPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorSystemPriority (INT4 i4Dot3adAggPortIndex,
                                        INT4
                                        *pi4RetValDot3adAggPortActorSystemPriority)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortActorSystemPriority =
            (INT4) pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority;

        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorSystemID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorSystemID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorSystemID (INT4 i4Dot3adAggPortIndex,
                                  tMacAddr * pRetValDot3adAggPortActorSystemID)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        LA_MEMCPY (pRetValDot3adAggPortActorSystemID,
                   pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorAdminKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorAdminKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorAdminKey (INT4 i4Dot3adAggPortIndex,
                                  INT4 *pi4RetValDot3adAggPortActorAdminKey)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortActorAdminKey =
            (INT4) pPortEntry->LaLacActorAdminInfo.u2IfKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorOperKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorOperKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorOperKey (INT4 i4Dot3adAggPortIndex,
                                 INT4 *pi4RetValDot3adAggPortActorOperKey)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortActorOperKey =
            (INT4) pPortEntry->LaLacActorInfo.u2IfKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerAdminSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerAdminSystemPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerAdminSystemPriority (INT4 i4Dot3adAggPortIndex,
                                               INT4
                                               *pi4RetValDot3adAggPortPartnerAdminSystemPriority)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerAdminSystemPriority =
            (INT4) pPortEntry->LaLacPartnerAdminInfo.LaSystem.u2SystemPriority;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerOperSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerOperSystemPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerOperSystemPriority (INT4 i4Dot3adAggPortIndex,
                                              INT4
                                              *pi4RetValDot3adAggPortPartnerOperSystemPriority)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerOperSystemPriority =
            (INT4) pPortEntry->LaLacPartnerInfo.LaSystem.u2SystemPriority;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerAdminSystemID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerAdminSystemID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerAdminSystemID (INT4 i4Dot3adAggPortIndex,
                                         tMacAddr *
                                         pRetValDot3adAggPortPartnerAdminSystemID)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        LA_MEMCPY (pRetValDot3adAggPortPartnerAdminSystemID,
                   pPortEntry->LaLacPartnerAdminInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerOperSystemID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerOperSystemID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerOperSystemID (INT4 i4Dot3adAggPortIndex,
                                        tMacAddr *
                                        pRetValDot3adAggPortPartnerOperSystemID)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        LA_MEMCPY (pRetValDot3adAggPortPartnerOperSystemID,
                   pPortEntry->LaLacPartnerInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerAdminKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerAdminKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerAdminKey (INT4 i4Dot3adAggPortIndex,
                                    INT4 *pi4RetValDot3adAggPortPartnerAdminKey)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerAdminKey =
            (INT4) pPortEntry->LaLacPartnerAdminInfo.u2IfKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerOperKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerOperKey
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerOperKey (INT4 i4Dot3adAggPortIndex,
                                   INT4 *pi4RetValDot3adAggPortPartnerOperKey)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerOperKey =
            (INT4) pPortEntry->LaLacPartnerInfo.u2IfKey;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortSelectedAggID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortSelectedAggID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortSelectedAggID (INT4 i4Dot3adAggPortIndex,
                                  INT4 *pi4RetValDot3adAggPortSelectedAggID)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetAggEntryBasedOnPort ((UINT2) i4Dot3adAggPortIndex, &pAggEntry);
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        if ((pAggEntry == NULL) || ((pPortEntry->AggSelected != LA_SELECTED)
                                    /* && (pPortEntry->LaAggAttached != LA_STANDBY) */
            ))
        {
            *pi4RetValDot3adAggPortSelectedAggID = 0;

            return SNMP_SUCCESS;
        }
        *pi4RetValDot3adAggPortSelectedAggID = (INT4) pAggEntry->u2AggIndex;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortAttachedAggID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortAttachedAggID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortAttachedAggID (INT4 i4Dot3adAggPortIndex,
                                  INT4 *pi4RetValDot3adAggPortAttachedAggID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetAggEntryBasedOnPort ((UINT2) i4Dot3adAggPortIndex, &pAggEntry);
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        if ((pAggEntry == NULL) || (pPortEntry->LaAggAttached != LA_TRUE))
        {
            *pi4RetValDot3adAggPortAttachedAggID = 0;

            return SNMP_SUCCESS;
        }
        *pi4RetValDot3adAggPortAttachedAggID = (INT4) pAggEntry->u2AggIndex;

        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorPort
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorPort (INT4 i4Dot3adAggPortIndex,
                              INT4 *pi4RetValDot3adAggPortActorPort)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortActorPort =
            (INT4) pPortEntry->LaLacActorInfo.u2IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorPortPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorPortPriority (INT4 i4Dot3adAggPortIndex,
                                      INT4
                                      *pi4RetValDot3adAggPortActorPortPriority)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortActorPortPriority =
            pPortEntry->LaLacActorInfo.u2IfPriority;

        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerAdminPort
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerAdminPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerAdminPort (INT4 i4Dot3adAggPortIndex,
                                     INT4
                                     *pi4RetValDot3adAggPortPartnerAdminPort)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerAdminPort =
            (INT4) pPortEntry->LaLacPartnerAdminInfo.u2IfIndex;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerOperPort
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerOperPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerOperPort (INT4 i4Dot3adAggPortIndex,
                                    INT4 *pi4RetValDot3adAggPortPartnerOperPort)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerOperPort =
            (INT4) pPortEntry->LaLacPartnerInfo.u2IfIndex;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerAdminPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerAdminPortPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerAdminPortPriority (INT4 i4Dot3adAggPortIndex,
                                             INT4
                                             *pi4RetValDot3adAggPortPartnerAdminPortPriority)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerAdminPortPriority =
            (INT4) pPortEntry->LaLacPartnerAdminInfo.u2IfPriority;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerOperPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerOperPortPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerOperPortPriority (INT4 i4Dot3adAggPortIndex,
                                            INT4
                                            *pi4RetValDot3adAggPortPartnerOperPortPriority)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortPartnerOperPortPriority =
            (INT4) pPortEntry->LaLacPartnerInfo.u2IfPriority;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorAdminState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorAdminState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorAdminState (INT4 i4Dot3adAggPortIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValDot3adAggPortActorAdminState)
{
    tLaLacPortEntry    *pPortEntry;
    UINT1               u1PortState;

    if (LaSnmpLowValidatePortIndex ((UINT2) i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

    LaGetPortState (&(pPortEntry->LaLacActorAdminInfo.LaLacPortState),
                    &u1PortState);

    pRetValDot3adAggPortActorAdminState->pu1_OctetList[0] = u1PortState;
    pRetValDot3adAggPortActorAdminState->i4_Length = sizeof (u1PortState);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortActorOperState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortActorOperState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortActorOperState (INT4 i4Dot3adAggPortIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValDot3adAggPortActorOperState)
{

    tLaLacPortEntry    *pPortEntry;
    UINT1               u1PortState;
    if (LaSnmpLowValidatePortIndex ((UINT2) i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

    LaGetPortState (&(pPortEntry->LaLacActorInfo.LaLacPortState), &u1PortState);

    pRetValDot3adAggPortActorOperState->pu1_OctetList[0] = u1PortState;

    pRetValDot3adAggPortActorOperState->i4_Length = sizeof (u1PortState);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerAdminState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerAdminState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerAdminState (INT4 i4Dot3adAggPortIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValDot3adAggPortPartnerAdminState)
{

    tLaLacPortEntry    *pPortEntry;
    UINT1               u1PortState;
    if (LaSnmpLowValidatePortIndex ((UINT2) i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

    LaGetPortState (&(pPortEntry->LaLacPartnerAdminInfo.LaLacPortState),
                    &u1PortState);

    pRetValDot3adAggPortPartnerAdminState->pu1_OctetList[0] = u1PortState;
    pRetValDot3adAggPortPartnerAdminState->i4_Length = sizeof (u1PortState);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortPartnerOperState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortPartnerOperState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortPartnerOperState (INT4 i4Dot3adAggPortIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot3adAggPortPartnerOperState)
{

    tLaLacPortEntry    *pPortEntry;
    UINT1               u1PortState;
    if (LaSnmpLowValidatePortIndex ((UINT2) i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

    LaGetPortState (&(pPortEntry->LaLacPartnerInfo.LaLacPortState),
                    &u1PortState);

    pRetValDot3adAggPortPartnerOperState->pu1_OctetList[0] = u1PortState;
    pRetValDot3adAggPortPartnerOperState->i4_Length = sizeof (u1PortState);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortAggregateOrIndividual
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortAggregateOrIndividual
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortAggregateOrIndividual (INT4 i4Dot3adAggPortIndex,
                                          INT4
                                          *pi4RetValDot3adAggPortAggregateOrIndividual)
{

    tLaLacPortEntry    *pPortEntry;
    UINT1               u1AggOrInd = LA_INIT_VAL;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        u1AggOrInd =
            (UINT1) (pPortEntry->LaLacActorInfo.LaLacPortState.LaAggregation);

        if (u1AggOrInd == LA_FALSE)
        {
            *pi4RetValDot3adAggPortAggregateOrIndividual = LA_SNMP_FALSE;
        }
        else
        {
            *pi4RetValDot3adAggPortAggregateOrIndividual = LA_SNMP_TRUE;
        }

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot3adAggPortActorSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortActorSystemPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortActorSystemPriority (INT4 i4Dot3adAggPortIndex,
                                        INT4
                                        i4SetValDot3adAggPortActorSystemPriority)
{
    LaChangeActorSystemPriority ((UINT2) i4Dot3adAggPortIndex,
                                 (UINT2)
                                 i4SetValDot3adAggPortActorSystemPriority);
    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortActorAdminKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortActorAdminKey
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortActorAdminKey (INT4 i4Dot3adAggPortIndex,
                                  INT4 i4SetValDot3adAggPortActorAdminKey)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4PortChannelMtu = 0;
    UINT4               u4IcclIfIndex = 0;

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->LaLacActorAdminInfo.u2IfKey ==
        (UINT2) i4SetValDot3adAggPortActorAdminKey)
    {
        return SNMP_SUCCESS;
    }

    if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
    {
        CLI_SET_ERR (CLI_LA_KEY_CHANGE_ERR);
        return SNMP_FAILURE;
    }

    if (LaGetAggEntryByKey
        ((UINT2) i4SetValDot3adAggPortActorAdminKey, &pAggEntry) == LA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }

    if (LaCfaGetIfInfo (pAggEntry->u2AggIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "Retrieval of port-channel %d properties failed!\n",
                     pAggEntry->u2AggIndex);
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        return SNMP_FAILURE;
    }

    u4PortChannelMtu = CfaIfInfo.u4IfMtu;
    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "Retrieval of port %d properties failed!\n",
                     pPortEntry->u2PortIndex);
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        return SNMP_FAILURE;
    }

    /* Interface MTU should match with the Port channel MTU */

    if (CfaIfInfo.u4IfMtu != u4PortChannelMtu)
    {
        CLI_SET_ERR (CLI_LA_MTU_MISMATCH);
        return SNMP_FAILURE;
    }

    if (LaChangeActorAdminKey ((UINT2) i4Dot3adAggPortIndex,
                               (UINT2) i4SetValDot3adAggPortActorAdminKey)
        != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        return SNMP_FAILURE;
    }
    LaUpdateTableChangedTime ();

    LaIcchGetIcclIfIndex (&u4IcclIfIndex);

    /* Disable LLDP on ICCL interface */
    if (pAggEntry->u2AggIndex == u4IcclIfIndex)
    {
        if (LA_TRUE !=
            (LaLLdpUpdateLLdpStatusOnIccl
             ((UINT4) i4Dot3adAggPortIndex, LA_AGG_CAPABLE)))
        {
            LA_TRC (ALL_FAILURE_TRC,
                    "Disabling LLDP on ICCL port has failed\r\n");
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortPartnerAdminSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortPartnerAdminSystemPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortPartnerAdminSystemPriority (INT4 i4Dot3adAggPortIndex,
                                               INT4
                                               i4SetValDot3adAggPortPartnerAdminSystemPriority)
{
    if (LaChangePartnerSystemPriority ((UINT2) i4Dot3adAggPortIndex,
                                       (UINT2)
                                       i4SetValDot3adAggPortPartnerAdminSystemPriority)
        != LA_SUCCESS)
    {

        return SNMP_FAILURE;
    }

    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortPartnerAdminSystemID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortPartnerAdminSystemID
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortPartnerAdminSystemID (INT4 i4Dot3adAggPortIndex,
                                         tMacAddr
                                         SetValDot3adAggPortPartnerAdminSystemID)
{
    if (LaUpdatePartnerSystemID ((UINT2) i4Dot3adAggPortIndex,
                                 SetValDot3adAggPortPartnerAdminSystemID)
        != LA_SUCCESS)
    {
        LaUpdateTableChangedTime ();
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortPartnerAdminKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortPartnerAdminKey
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortPartnerAdminKey (INT4 i4Dot3adAggPortIndex,
                                    INT4 i4SetValDot3adAggPortPartnerAdminKey)
{
    if (LaChangePartnerPortKey ((UINT2) i4Dot3adAggPortIndex,
                                (UINT2) i4SetValDot3adAggPortPartnerAdminKey)
        != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortActorPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortActorPortPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortActorPortPriority (INT4 i4Dot3adAggPortIndex,
                                      INT4
                                      i4SetValDot3adAggPortActorPortPriority)
{
    tLaLacPortEntry    *pPortEntry;

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

    if (LaChangeActorPortPriority ((UINT2) i4Dot3adAggPortIndex,
                                   (UINT2)
                                   i4SetValDot3adAggPortActorPortPriority) !=
        LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortPartnerAdminPort
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortPartnerAdminPort
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortPartnerAdminPort (INT4 i4Dot3adAggPortIndex,
                                     INT4 i4SetValDot3adAggPortPartnerAdminPort)
{
    if (LaChangePartnerPortNumber ((UINT2) i4Dot3adAggPortIndex,
                                   (UINT2)
                                   i4SetValDot3adAggPortPartnerAdminPort) !=
        LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortPartnerAdminPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortPartnerAdminPortPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortPartnerAdminPortPriority (INT4 i4Dot3adAggPortIndex,
                                             INT4
                                             i4SetValDot3adAggPortPartnerAdminPortPriority)
{
    if (LaChangePartnerPortPriority ((UINT2) i4Dot3adAggPortIndex,
                                     (UINT2)
                                     i4SetValDot3adAggPortPartnerAdminPortPriority)
        != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    LaUpdateTableChangedTime ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortActorAdminState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortActorAdminState
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortActorAdminState (INT4 i4Dot3adAggPortIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValDot3adAggPortActorAdminState)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tSNMP_OCTET_STRING_TYPE PortState;
    UINT1               u1SetPortState;
    UINT4               u4SeqNum = 0;
    UINT1               u1SetActivity = 0x00;

    u1SetPortState = pSetValDot3adAggPortActorAdminState->pu1_OctetList[0];

    PortState.i4_Length = 1;
    PortState.pu1_OctetList = &u1SetActivity;

    LA_MEMSET (&SnmpNotifyInfo, LA_INIT_VAL, sizeof (tSnmpNotifyInfo));
    LA_MEMSET (&PortState, LA_INIT_VAL, sizeof (tSNMP_OCTET_STRING_TYPE));

    if ((u1SetPortState & LA_LACPACTIVITY_BITMASK))
    {
        LaChangeActorPortState ((UINT2) i4Dot3adAggPortIndex,
                                LA_LACP_ACTIVITY, LA_SET);
        LaUpdateTableChangedTime ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsLaPortActorResetAdminState,
                              u4SeqNum, FALSE, LaLock, LaUnLock, 1,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4Dot3adAggPortIndex,
                          &PortState));
    }

    if ((u1SetPortState & LA_LACPTIMEOUT_BITMASK))
    {
        LaChangeActorPortState ((UINT2) i4Dot3adAggPortIndex,
                                LA_LACP_TIMEOUT, LA_SET);
        LaUpdateTableChangedTime ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsLaPortActorResetAdminState,
                              u4SeqNum, FALSE, LaLock, LaUnLock, 1,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4Dot3adAggPortIndex,
                          &PortState));
    }

    if ((u1SetPortState & LA_AGGREGATION_BITMASK))
    {
        LaChangeActorPortState ((UINT2) i4Dot3adAggPortIndex,
                                LA_AGGREGATION, LA_SET);
        LaUpdateTableChangedTime ();
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDot3adAggPortPartnerAdminState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
setValDot3adAggPortPartnerAdminState
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDot3adAggPortPartnerAdminState (INT4 i4Dot3adAggPortIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValDot3adAggPortPartnerAdminState)
{
    UINT1               u1SetPortState;

    u1SetPortState = pSetValDot3adAggPortPartnerAdminState->pu1_OctetList[0];
    if ((u1SetPortState & LA_LACPACTIVITY_BITMASK))
    {
        LaChangePartnerPortState ((UINT2) i4Dot3adAggPortIndex,
                                  LA_LACP_ACTIVITY, LA_SET);
        LaUpdateTableChangedTime ();
    }

    if ((u1SetPortState & LA_LACPTIMEOUT_BITMASK))
    {
        LaChangePartnerPortState ((UINT2) i4Dot3adAggPortIndex,
                                  LA_LACP_TIMEOUT, LA_SET);
        LaUpdateTableChangedTime ();
    }

    if ((u1SetPortState & LA_AGGREGATION_BITMASK))
    {
        LaChangePartnerPortState ((UINT2) i4Dot3adAggPortIndex,
                                  LA_AGGREGATION, LA_SET);
        LaUpdateTableChangedTime ();
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortActorSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortActorSystemPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortActorSystemPriority (UINT4 *pu4ErrorCode,
                                           INT4 i4Dot3adAggPortIndex,
                                           INT4
                                           i4TestValDot3adAggPortActorSystemPriority)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggPortActorSystemPriority < LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggPortActorSystemPriority > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if D-LAG Status is enabled in any of the
     * port channel if Yes this configuration should 
     * not be allowed. */
    LaGetNextAggEntry (NULL, &pTmpAggEntry);
    while (pTmpAggEntry != NULL)
    {
        if (pTmpAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
        {
            CLI_SET_ERR (CLI_LA_DLAG_STATUS_ENABLED);
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "D-LAG Status is enabled, In Po(%u)"
                         " port-channel, so System Priority cannot be "
                         "changed.\n", pTmpAggEntry->u2AggIndex);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    }

    return SNMP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaCheckLaDynPortSpeed                                */
/*                                                                           */
/* Description        : This function is called to check                     */
/*                      portchannel speed                                    */
/*                                                                           */
/* Input(s)           : pu4ErrorCode                                         */
/*                      pAggEntry                                            */
/*                      u4IfHighSpeed                                        */
/*                      i4Dot3adAggPortIndex                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS - On success                            */
/*                      SNMP_FAILURE - On failure                            */
/*****************************************************************************/
INT4
LaCheckLaDynPortSpeed (UINT4 *pu4ErrorCode,
                       tLaLacAggEntry * pAggEntry,
                       UINT4 u4IfHighSpeed, INT4 i4Dot3adAggPortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    INT4                i4ConfPortCtrlSpeed;
    INT4                i4TestValIssPortCtrlSpeed;
    tCfaIfInfo          CfaIfInfo;

    /* When u1HwLaWithDiffPortSpeedSup is not supported,
     * LAG can't be created between ports that are operating in different speed
     */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_LA_WITH_DIFF_PORT_SPEED))
    {
        i4TestValIssPortCtrlSpeed = (INT4) u4IfHighSpeed;
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            if ((i4Dot3adAggPortIndex != pPortEntry->u2PortIndex))

            {
                if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) ==
                    CFA_SUCCESS)
                {
                    i4ConfPortCtrlSpeed = (INT4) CfaIfInfo.u4IfHighSpeed;
                    if (i4ConfPortCtrlSpeed != i4TestValIssPortCtrlSpeed)
                    {
                        CLI_SET_ERR (CLI_LA_NO_DIFF_SPEED);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }

                }
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

    }
    else
    {
        UNUSED_PARAM (i4ConfPortCtrlSpeed);
        UNUSED_PARAM (i4TestValIssPortCtrlSpeed);
        UNUSED_PARAM (CfaIfInfo);
        UNUSED_PARAM (pu4ErrorCode);
        UNUSED_PARAM (pAggEntry);
        UNUSED_PARAM (pPortEntry);
        UNUSED_PARAM (u4IfHighSpeed);
        UNUSED_PARAM (i4Dot3adAggPortIndex);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortActorAdminKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortActorAdminKey
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortActorAdminKey (UINT4 *pu4ErrorCode,
                                     INT4 i4Dot3adAggPortIndex,
                                     INT4 i4TestValDot3adAggPortActorAdminKey)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4AggContextId;
    UINT4               u4PortContextId;
    UINT4               u4PortChannelMtu;
    UINT2               u2LocalPort;
    UINT2               u2PnacAuthStatus = PNAC_PORTCNTRL_FORCEAUTHORIZED;
    UINT1               u1TunnelStatus = VLAN_TUNNEL_PROTOCOL_PEER;
    UINT1               u1Status = 0;
    UINT1               u1BridgedStatus = CFA_ENABLED;
    UINT1               u1PbPortType = 0;
    UINT1               u1PhyIfPortType = 0;
    UINT1               u1LogicalIfPortType = 0;
    UINT1               u1IfStatus = 0;

    if (LaActiveDLAGCheckIsLocalPort ((UINT2) i4Dot3adAggPortIndex)
        == LA_FAILURE)
    {
        CLI_SET_ERR (CLI_LA_REMOTE_PORT_CONFIG_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        if (VcmGetL2ModeExt () == VCM_MI_MODE)
        {
            CLI_SET_ERR (CLI_LA_PORT_UNMAP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        }
        else
        {
            CLI_SET_ERR (CLI_LA_INVALID_PORT);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggPortActorAdminKey < LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggPortActorAdminKey > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* When u1HwLaCreationAtCepPortSup is not supported.
     * LAG can't be created on CEP port.
     */
    if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_LAG_ON_CEP_PORT))
    {
        if (L2IwfGetPbPortType ((UINT4) i4Dot3adAggPortIndex,
                                &u1PbPortType) == L2IWF_SUCCESS)
        {
            if (u1PbPortType == L2IWF_CUSTOMER_EDGE_PORT)
            {
                CLI_SET_ERR (CLI_LA_NO_CUSTOMER_EDGE_PORT_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        UNUSED_PARAM (u1PbPortType);
    }

    LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPortEntry->LaLacActorAdminInfo.u2IfKey ==
        (UINT2) i4TestValDot3adAggPortActorAdminKey)
    {
        /* Same value */
        return SNMP_SUCCESS;
    }

    /* Port Lacp Mode should be disabled while changing PortActorAdminKey */
    if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
    {
        CLI_SET_ERR (CLI_LA_KEY_CHANGE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LaGetAggEntryByKey ((UINT2) i4TestValDot3adAggPortActorAdminKey,
                            &pAggEntry) == LA_FAILURE)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pAggEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LaCfaGetIfInfo (pAggEntry->u2AggIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "Retrieval of port-channel %d properties failed!\n",
                     pAggEntry->u2AggIndex);
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        return SNMP_FAILURE;
    }

    u4PortChannelMtu = CfaIfInfo.u4IfMtu;
    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    /* Interface MTU should match with the Port channel MTU */
    if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "Retrieval of port %d properties failed!\n",
                     pPortEntry->u2PortIndex);
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CfaIfInfo.u4IfMtu != u4PortChannelMtu)
    {
        CLI_SET_ERR (CLI_LA_MTU_MISMATCH);
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfBridgedIfaceStatus ((UINT4) pAggEntry->u2AggIndex,
                                &u1BridgedStatus);
    if (CfaIfInfo.u1BridgedIface != u1BridgedStatus)
    {
        CLI_SET_ERR (CLI_LA_INCOMPATIBLE_MODE);
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        return SNMP_FAILURE;
    }
    if (pAggEntry->bDynPortRealloc != pPortEntry->bDynAggSelect)
    {
        CLI_SET_ERR (CLI_LA_INVALID_DEF_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* In case of 802.1ad bridges(provider-core/provider-edge),
     * Enabling the protocol status on a port is not allowed
     * when the protocol tunnel status is set to Tunnel/Discard 
     * on a port.
     */
    LaL2IwfGetProtocolTunnelStatusOnPort ((UINT4) i4Dot3adAggPortIndex,
                                          L2_PROTO_LACP, &u1TunnelStatus);

    if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_PEER)
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LaDsGetStartedStatus () == DS_TRUE)
    {
        if (LaDsCheckDiffservPort (i4Dot3adAggPortIndex) != DS_SUCCESS)
        {
            /* Diffsrv Port should not be Lacp enabled */
            CLI_SET_ERR (CLI_LA_DIFFSRV_ENABLE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (LaPnacGetStartedStatus () == PNAC_TRUE)
    {
        LaL2IwfGetPortPnacAuthControl ((UINT2) i4Dot3adAggPortIndex,
                                       &u2PnacAuthStatus);
        if (u2PnacAuthStatus != PNAC_PORTCNTRL_FORCEAUTHORIZED)
        {
            CLI_SET_ERR (CLI_LA_PNAC_ENABLE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (LaVcmGetContextInfoFromIfIndex (pAggEntry->u2AggIndex,
                                        &u4AggContextId, &u2LocalPort)
        == VCM_FAILURE)
    {
        /* Agg does not belong to any context */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LaVcmGetContextInfoFromIfIndex ((UINT4) i4Dot3adAggPortIndex,
                                        &u4PortContextId, &u2LocalPort)
        == VCM_FAILURE)
    {
        /* Port does not belong to any context */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* For L3LAG, member port should not be in user configured Context.
     * Only the LAG should be in the specific context */
    CfaGetIfBridgedIfaceStatus ((UINT4) pAggEntry->u2AggIndex, &u1IfStatus);
    if ((u1IfStatus == CFA_DISABLED)
        && (u4AggContextId != L2IWF_DEFAULT_CONTEXT))
    {
        if (u4PortContextId != L2IWF_DEFAULT_CONTEXT)
        {
            CLI_SET_ERR (CLI_LA_MEMBER_CONTEXT_L3LAG_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    else if (u4PortContextId != u4AggContextId)
    {
        /* Agg and port do not belong to the same context */
        CLI_SET_ERR (CLI_LA_CONTEXT_MISMATCH);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u1IfStatus == CFA_ENABLED)
    {
        if (L2IwfGetPbPortType ((UINT4) i4Dot3adAggPortIndex,
                                &u1PhyIfPortType) != L2IWF_SUCCESS)
        {
            /*failure in fetching porttype for Physical port Index */
            return SNMP_FAILURE;
        }

        if (L2IwfGetPbPortType ((UINT4) pAggEntry->u2AggIndex,
                                &u1LogicalIfPortType) != L2IWF_SUCCESS)
        {
            /*failure in fetching porttype for logical port Index */
            return SNMP_FAILURE;
        }

        if (u1PhyIfPortType != u1LogicalIfPortType)
        {
            CLI_SET_ERR (CLI_LA_PORTTYPE_MISMATCH);
            /*Port channel port type doesnot match with physical port port type */
            return SNMP_FAILURE;
        }
    }
    LaL2IwfGetSispPortCtrlStatus ((UINT4) i4Dot3adAggPortIndex, &u1Status);

    if (u1Status == L2IWF_ENABLED)
    {
        CLI_SET_ERR (CLI_LA_SISP_CANNOT_BE_PO_MEMBER);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* In manual aggregation all ports will be aggregated once 
     * configured, so for manual aggregation, if the number of
     * ports already configured for this port-channel has reached
     * the maximum configurable ports per port-channel then return
     * failure. In case of normal aggregation using LACP, ports
     * that are configured above the max value will become standby
     * ports if ports 1 to "max-allowed" are up-in-bundle.
     */

    if ((pAggEntry->u1ConfigPortCount ==
         pAggEntry->u1MaxPortsToAttach) &&
        (pAggEntry->LaLacpMode == LA_MODE_MANUAL))
    {
        CLI_SET_ERR (CLI_LA_MAXPORT_MANUAL_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (LaCheckLaDynPortSpeed (pu4ErrorCode,
                               pAggEntry,
                               CfaIfInfo.u4IfHighSpeed,
                               i4Dot3adAggPortIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortPartnerAdminSystemPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortPartnerAdminSystemPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortPartnerAdminSystemPriority (UINT4 *pu4ErrorCode,
                                                  INT4 i4Dot3adAggPortIndex,
                                                  INT4
                                                  i4TestValDot3adAggPortPartnerAdminSystemPriority)
{

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggPortPartnerAdminSystemPriority <
         LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggPortPartnerAdminSystemPriority > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortPartnerAdminSystemID
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortPartnerAdminSystemID
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortPartnerAdminSystemID (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot3adAggPortIndex,
                                            tMacAddr
                                            TestValDot3adAggPortPartnerAdminSystemID)
{
    INT4                i4RetVal;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4RetVal =
         LaIsValidSystemID (TestValDot3adAggPortPartnerAdminSystemID)) ==
        LA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortPartnerAdminKey
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortPartnerAdminKey
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortPartnerAdminKey (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot3adAggPortIndex,
                                       INT4
                                       i4TestValDot3adAggPortPartnerAdminKey)
{
    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggPortPartnerAdminKey < LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggPortPartnerAdminKey > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortActorPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortActorPortPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortActorPortPriority (UINT4 *pu4ErrorCode,
                                         INT4 i4Dot3adAggPortIndex,
                                         INT4
                                         i4TestValDot3adAggPortActorPortPriority)
{

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggPortActorPortPriority < LA_SNMP_MIN_PRIORITY) ||
        (i4TestValDot3adAggPortActorPortPriority > LA_SNMP_MAX_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortPartnerAdminPort
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortPartnerAdminPort
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortPartnerAdminPort (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot3adAggPortIndex,
                                        INT4
                                        i4TestValDot3adAggPortPartnerAdminPort)
{

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* Since we dont know about partner values so we are accepting values 
     * from 0-65535 */
    if ((i4TestValDot3adAggPortPartnerAdminPort < LA_SNMP_MIN_VAL) ||
        (i4TestValDot3adAggPortPartnerAdminPort > LA_SNMP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortPartnerAdminPortPriority
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortPartnerAdminPortPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortPartnerAdminPortPriority (UINT4 *pu4ErrorCode,
                                                INT4 i4Dot3adAggPortIndex,
                                                INT4
                                                i4TestValDot3adAggPortPartnerAdminPortPriority)
{
    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot3adAggPortPartnerAdminPortPriority < LA_SNMP_MIN_PRIORITY)
        || (i4TestValDot3adAggPortPartnerAdminPortPriority >
            LA_SNMP_MAX_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortActorAdminState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortActorAdminState
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortActorAdminState (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot3adAggPortIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValDot3adAggPortActorAdminState)
{

    if (LaSnmpLowValidatePortIndex ((UINT2) i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Only the first 3 bits of octet string are settable. */
    if ((pTestValDot3adAggPortActorAdminState->i4_Length != sizeof (UINT1)) ||
        (pTestValDot3adAggPortActorAdminState->pu1_OctetList[0] &
         LA_LACPSTATE_RO_BITSMASK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2Dot3adAggPortPartnerAdminState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
testValDot3adAggPortPartnerAdminState
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Dot3adAggPortPartnerAdminState (UINT4 *pu4ErrorCode,
                                         INT4 i4Dot3adAggPortIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValDot3adAggPortPartnerAdminState)
{
    if (LaSnmpLowValidatePortIndex ((UINT2) i4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Only the first 3 bits of octet string are settable. */
    if ((pTestValDot3adAggPortPartnerAdminState->i4_Length != sizeof (UINT1)) ||
        (pTestValDot3adAggPortPartnerAdminState->pu1_OctetList[0] &
         LA_LACPSTATE_RO_BITSMASK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot3adAggPortTable
 Input       :  The Indices
                Dot3adAggPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot3adAggPortTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot3adAggPortStatsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot3adAggPortStatsTable
Input       :  The Indices
Dot3adAggPortIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3adAggPortStatsTable (INT4 i4Dot3adAggPortIndex)
{

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

    /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
Function    :  nmhGetFirstIndexDot3adAggPortStatsTable
Input       :  The Indices
Dot3adAggPortIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3adAggPortStatsTable (INT4 *pi4Dot3adAggPortIndex)
{

    if (LaSnmpLowGetFirstValidPortIndex (pi4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexDot3adAggPortStatsTable
Input       :  The Indices
Dot3adAggPortIndex
nextDot3adAggPortIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3adAggPortStatsTable (INT4 i4Dot3adAggPortIndex,
                                        INT4 *pi4NextDot3adAggPortIndex)
{

    if (i4Dot3adAggPortIndex < 0)
    {
        return (SNMP_FAILURE);
    }
    if (LaSnmpLowGetNextValidPortIndex (i4Dot3adAggPortIndex,
                                        pi4NextDot3adAggPortIndex) !=
        LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

    /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsLACPDUsRx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsLACPDUsRx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsLACPDUsRx (INT4 i4Dot3adAggPortIndex,
                                   UINT4 *pu4RetValDot3adAggPortStatsLACPDUsRx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsLACPDUsRx =
            pPortEntry->LaPortStats.u4LacpduRx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsMarkerPDUsRx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsMarkerPDUsRx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsMarkerPDUsRx (INT4 i4Dot3adAggPortIndex,
                                      UINT4
                                      *pu4RetValDot3adAggPortStatsMarkerPDUsRx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsMarkerPDUsRx =
            pPortEntry->LaPortStats.u4MarkerPduRx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsMarkerResponsePDUsRx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsMarkerResponsePDUsRx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsMarkerResponsePDUsRx (INT4 i4Dot3adAggPortIndex,
                                              UINT4
                                              *pu4RetValDot3adAggPortStatsMarkerResponsePDUsRx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsMarkerResponsePDUsRx =
            pPortEntry->LaPortStats.u4MarkerResponsePduRx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsUnknownRx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsUnknownRx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsUnknownRx (INT4 i4Dot3adAggPortIndex,
                                   UINT4 *pu4RetValDot3adAggPortStatsUnknownRx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsUnknownRx =
            pPortEntry->LaPortStats.u4UnknownRx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsIllegalRx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsIllegalRx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsIllegalRx (INT4 i4Dot3adAggPortIndex,
                                   UINT4 *pu4RetValDot3adAggPortStatsIllegalRx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsIllegalRx =
            pPortEntry->LaPortStats.u4IllegalRx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsLACPDUsTx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsLACPDUsTx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsLACPDUsTx (INT4 i4Dot3adAggPortIndex,
                                   UINT4 *pu4RetValDot3adAggPortStatsLACPDUsTx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsLACPDUsTx =
            pPortEntry->LaPortStats.u4LacpduTx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsMarkerPDUsTx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsMarkerPDUsTx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsMarkerPDUsTx (INT4 i4Dot3adAggPortIndex,
                                      UINT4
                                      *pu4RetValDot3adAggPortStatsMarkerPDUsTx)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsMarkerPDUsTx =
            pPortEntry->LaPortStats.u4MarkerPduTx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortStatsMarkerResponsePDUsTx
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortStatsMarkerResponsePDUsTx
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortStatsMarkerResponsePDUsTx (INT4 i4Dot3adAggPortIndex,
                                              UINT4
                                              *pu4RetValDot3adAggPortStatsMarkerResponsePDUsTx)
{

    tLaLacPortEntry    *pPortEntry = NULL;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pu4RetValDot3adAggPortStatsMarkerResponsePDUsTx =
            pPortEntry->LaPortStats.u4MarkerResponsePduTx;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : Dot3adAggPortDebugTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDot3adAggPortDebugTable
Input       :  The Indices
Dot3adAggPortIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot3adAggPortDebugTable (INT4 i4Dot3adAggPortIndex)
{
    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {

        return SNMP_FAILURE;
    }
}

/****************************************************************************
Function    :  nmhGetFirstIndexDot3adAggPortDebugTable
Input       :  The Indices
Dot3adAggPortIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot3adAggPortDebugTable (INT4 *pi4Dot3adAggPortIndex)
{
    if (LaSnmpLowGetFirstValidPortIndex (pi4Dot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexDot3adAggPortDebugTable
Input       :  The Indices
Dot3adAggPortIndex
nextDot3adAggPortIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot3adAggPortDebugTable (INT4 i4Dot3adAggPortIndex,
                                        INT4 *pi4NextDot3adAggPortIndex)
{
    if (i4Dot3adAggPortIndex < 0)

    {
        return (SNMP_FAILURE);
    }
    if (LaSnmpLowGetNextValidPortIndex
        (i4Dot3adAggPortIndex, pi4NextDot3adAggPortIndex) != LA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugRxState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugRxState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugRxState (INT4 i4Dot3adAggPortIndex,
                                 INT4 *pi4RetValDot3adAggPortDebugRxState)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValDot3adAggPortDebugRxState = pPortEntry->LaRxmState;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugLastRxTime
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugLastRxTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugLastRxTime (INT4 i4Dot3adAggPortIndex,
                                    UINT4
                                    *pu4RetValDot3adAggPortDebugLastRxTime)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugLastRxTime =
            pPortEntry->LaPortDebug.LaLastRxTime;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugMuxState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugMuxState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugMuxState (INT4 i4Dot3adAggPortIndex,
                                  INT4 *pi4RetValDot3adAggPortDebugMuxState)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4RetValDot3adAggPortDebugMuxState = pPortEntry->LaMuxState;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugMuxReason
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugMuxReason
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugMuxReason (INT4 i4Dot3adAggPortIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValDot3adAggPortDebugMuxReason)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        pRetValDot3adAggPortDebugMuxReason->i4_Length = LA_MUX_REASON_LEN;

        MEMCPY (pRetValDot3adAggPortDebugMuxReason->pu1_OctetList,
                (pPortEntry->LaPortDebug.au1LaMuxReasonStr), LA_MUX_REASON_LEN);

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugActorChurnState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugActorChurnState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugActorChurnState (INT4 i4Dot3adAggPortIndex,
                                         INT4
                                         *pi4RetValDot3adAggPortDebugActorChurnState)
{
    UNUSED_PARAM (i4Dot3adAggPortIndex);

    *pi4RetValDot3adAggPortDebugActorChurnState = LA_INIT_VAL;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugPartnerChurnState
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugPartnerChurnState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugPartnerChurnState (INT4 i4Dot3adAggPortIndex,
                                           INT4
                                           *pi4RetValDot3adAggPortDebugPartnerChurnState)
{
    UNUSED_PARAM (i4Dot3adAggPortIndex);

    *pi4RetValDot3adAggPortDebugPartnerChurnState = LA_INIT_VAL;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugActorChurnCount
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugActorChurnCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugActorChurnCount (INT4 i4Dot3adAggPortIndex,
                                         UINT4
                                         *pu4RetValDot3adAggPortDebugActorChurnCount)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugActorChurnCount =
            pPortEntry->LaPortDebug.u4ActorChurnCount;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugPartnerChurnCount
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugPartnerChurnCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugPartnerChurnCount (INT4 i4Dot3adAggPortIndex,
                                           UINT4
                                           *pu4RetValDot3adAggPortDebugPartnerChurnCount)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugPartnerChurnCount =
            pPortEntry->LaPortDebug.u4PartnerChurnCount;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugActorSyncTransitionCount
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugActorSyncTransitionCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugActorSyncTransitionCount (INT4 i4Dot3adAggPortIndex,
                                                  UINT4
                                                  *pu4RetValDot3adAggPortDebugActorSyncTransitionCount)
{

    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugActorSyncTransitionCount =
            pPortEntry->LaPortDebug.u4ActorSyncTransitionCount;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugPartnerSyncTransitionCount
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugPartnerSyncTransitionCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugPartnerSyncTransitionCount (INT4 i4Dot3adAggPortIndex,
                                                    UINT4
                                                    *pu4RetValDot3adAggPortDebugPartnerSyncTransitionCount)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugPartnerSyncTransitionCount =
            pPortEntry->LaPortDebug.u4PartnerSyncTransitionCount;

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugActorChangeCount
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugActorChangeCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugActorChangeCount (INT4 i4Dot3adAggPortIndex,
                                          UINT4
                                          *pu4RetValDot3adAggPortDebugActorChangeCount)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugActorChangeCount =
            pPortEntry->LaPortDebug.u4ActorChangeCount;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetDot3adAggPortDebugPartnerChangeCount
Input       :  The Indices
Dot3adAggPortIndex

The Object 
retValDot3adAggPortDebugPartnerChangeCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDot3adAggPortDebugPartnerChangeCount (INT4 i4Dot3adAggPortIndex,
                                            UINT4
                                            *pu4RetValDot3adAggPortDebugPartnerChangeCount)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaSnmpLowValidatePortIndex (i4Dot3adAggPortIndex) == LA_SUCCESS)
    {
        LaGetPortEntry ((UINT2) i4Dot3adAggPortIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {

            return SNMP_FAILURE;
        }
        *pu4RetValDot3adAggPortDebugPartnerChangeCount =
            pPortEntry->LaPortDebug.u4PartnerChangeCount;
    }

    return SNMP_SUCCESS;

}

/* Utility Routines for All Tables */

/****************************************************************************
 * Function    :  LaSnmpLowValidateAggIndex
 * Input       :  i4Dot3adIndex (AggIndex / PortIndex)
 *
 * Output      :  None
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/

INT4
LaSnmpLowValidateAggIndex (INT4 i4Dot3adIndex)
{
    tLaLacAggEntry     *pAggEntry;

    if ((i4Dot3adIndex < 0) || (i4Dot3adIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    if (LaGetAggEntry ((UINT2) i4Dot3adIndex, &pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;

}

/****************************************************************************
 * Function    :  LaSnmpLowGetFirstValidIndex
 * Input       :  None
 *
 * Output      :  pi4Dot3adFirstIndex (AggIndex / PortIndex)
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/

INT4
LaSnmpLowGetFirstValidAggIndex (INT4 *pi4Dot3adFirstIndex)
{

    tLaLacAggEntry     *pAggEntry;
    if (LaGetNextAggEntry (NULL, &pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    *pi4Dot3adFirstIndex = pAggEntry->u2AggIndex;
    return LA_SUCCESS;
}

/****************************************************************************
 * Function    :  LaSnmpLowGetNextValidIndex
 * Input       :  i4Dot3adIndex (AggIndex / PortIndex)
 *
 * Output      :  pi4Dot3adNextIndex (AggIndex / PortIndex)
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/

INT4
LaSnmpLowGetNextValidAggIndex (INT4 i4Dot3adIndex, INT4 *pi4Dot3adNextIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacAggEntry     *pNextLaLacAggEntry = NULL;

    if ((i4Dot3adIndex < 0) || (i4Dot3adIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    if (LaGetAggEntry ((UINT2) i4Dot3adIndex, &pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    if (LaGetNextAggEntry (pAggEntry, &pNextLaLacAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    *pi4Dot3adNextIndex = pNextLaLacAggEntry->u2AggIndex;
    return LA_SUCCESS;
}

/****************************************************************************
 * Function    :  LaSnmpLowValidatePortIndex
 * Input       :  i4Dot3adIndex (AggIndex / PortIndex)
 *
 * Output      :  None
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/
INT4
LaSnmpLowValidatePortIndex (INT4 i4Dot3adPortIndex)
{
    tLaLacPortEntry    *pPortEntry;

    if (i4Dot3adPortIndex > LA_16BIT_MAX)
    {
        return LA_FAILURE;
    }
    LaGetPortEntry ((UINT2) i4Dot3adPortIndex, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/****************************************************************************
 * Function    :  LaSnmpLowGetFirstValidPortIndex
 * Input       :  None
 *
 * Output      :  pi4Dot3adFirstIndex (AggIndex / PortIndex)
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/

INT4
LaSnmpLowGetFirstValidPortIndex (INT4 *pi4Dot3adFirstIndex)
{
    tLaLacPortEntry    *pPortEntry;

    if (LaGetNextPortEntry (NULL, &pPortEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    *pi4Dot3adFirstIndex = pPortEntry->u2PortIndex;
    return LA_SUCCESS;
}

/****************************************************************************
 * Function    :  LaSnmpLowGetNextValidPortIndex
 * Input       :  i4Dot3adIndex (AggIndex / PortIndex)
 *
 * Output      :  pi4Dot3adNextIndex (AggIndex / PortIndex)
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/

INT4
LaSnmpLowGetNextValidPortIndex (INT4 i4Dot3adIndex, INT4 *pi4Dot3adNextIndex)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacPortEntry    *pNextPortEntry;

    if (LaGetPortEntry ((UINT2) i4Dot3adIndex, &pPortEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    if (LaGetNextPortEntry (pPortEntry, &pNextPortEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    *pi4Dot3adNextIndex = (INT4) pNextPortEntry->u2PortIndex;
    return LA_SUCCESS;
}
