/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: ladlag.c,v 1.36 2017/11/08 13:19:13 siva Exp $                        */
/* License Aricent Inc., 2001-2002                                           */
/*****************************************************************************/
/*    FILE  NAME            : ladlag.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Distributed Link Aggregation                   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 15 Feb 2012                                    */
/*    AUTHOR                : DLAG team                                      */
/*    DESCRIPTION           : This file contains function definitions added  */
/*                            for supporting Distributed Link Aggregation    */
/*                            Multi chassi Link Aggregation.                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    15 Feb 2012/          Initial Create.                          */
/*            DLAG Team                                                      */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/
/* Function Name      : LaDLAGInit                                           */
/*                                                                           */
/* Description        : This function is used to initialize the D-LAG        */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which D-LAG feature  */
/*                                  should be initiated.                     */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGInit (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    UINT1               au1LocalMac[LA_MAC_ADDRESS_SIZE];

    LA_MEMSET (au1LocalMac, 0, LA_MAC_ADDRESS_SIZE);

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaDLAGInit: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }
    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if ((LA_MEMCMP (&LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                    &au1LocalMac, LA_MAC_ADDRESS_SIZE) == 0) &&
        (LA_MEMCMP (&pAggEntry->DLAGSystem.SystemMacAddr, &au1LocalMac,
                    LA_MAC_ADDRESS_SIZE) == 0))
    {
        LaCfaGetSysMacAddress (pAggEntry->DLAGSystem.SystemMacAddr);
    }
    else if (LA_MEMCMP (&pAggEntry->DLAGSystem.SystemMacAddr, &au1LocalMac,
                        LA_MAC_ADDRESS_SIZE) == 0)
    {
        LA_MEMCPY (&pAggEntry->DLAGSystem.SystemMacAddr,
                   &LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);
    }

    /* Copy D-LAG System ID to Actory System ID */
    if ((LaDLAGSetActorSystemIDToDLAG (pAggEntry)) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGInit: LaDLAGSetActorSystemIDToDLAG failed.\n");
        return LA_FAILURE;
    }
    /* Copy D-LAG System Priority to Actor */
    if ((LaDLAGChangeActorSystemPriorityToDLAG (pAggEntry)) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGInit: LaDLAGChangeActorSystemPriorityToDLAG failed.\n");
        return LA_FAILURE;
    }

    /* Change The Node Role Played as None */
    pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
    LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);

    if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
    {
        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            /* Change The Node Role Played as Master */
            pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
            pAggEntry->u4DLAGElectedAsMasterCount++;
        }

        /* Copy Source Mac as Elector System ID */
        LA_MEMCPY (pAggEntry->LaDLAGElector,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        pAggEntry->bLaDLAGIsDesignatedElector = LA_TRUE;

        if ((LaStartTimer (&(pAggEntry->DLAGMSSelectionWaitTmr))) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStartTimer failed for Master"
                    " Slave selection wait timer.\n");
            return LA_FAILURE;
        }

    }
    else
    {
        /* Assign the Role played from Global variable */
        if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
        {
            pAggEntry->u1DLAGRolePlayed = LA_AA_DLAG_ROLE_PLAYED;
        }

        if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
             (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)) ||
            ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
             (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER)))
        {
            if (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList == NULL)
            {
                /* Create RBTree for DLAG Total port List */
                pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList =
                    LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                            (tLaDLAGConsPortEntry,
                                             LaDLAGConsPortInfo),
                                            LaCmpPortPriority);
            }
        }
    }

    /* Send high-priority event-update message to remote D-LAG nodes */
    LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP;
    LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);

    /* Create RBTree for DLAG Remote Aggregator Entry */
    pAggEntry->LaDLAGRemoteAggInfoTree = LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                                                 (tLaDLAGRemoteAggEntry,
                                                                  LaDLAGRemoteAggNode),
                                                                 LaMacCmpForLaDLAGRemoteAggEntry);
    /* Create RBTree for DLAG Add port List */
    pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList =
        LaRBTreeCreateEmbedded (FSAP_OFFSETOF
                                (tLaDLAGConsPortEntry,
                                 LaDLAGPortInfo), LaCmpPortNo);

    if ((LA_DLAG_SYSTEM_STATUS == LA_ENABLED) &&
        (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER))
    {
        LaActiveDLAGMasterUpdateConsolidatedList (pAggEntry);
    }
#ifdef ICCH_WANTED
    else if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
             (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER))
    {
        LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
    }
#endif

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_DISABLED) &&
        (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_DISABLED))
    {
        /* Start D-LAG periodic sync timer */
        if ((LaStartTimer (&(pAggEntry->DLAGPeriodicSyncTmr))) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStartTimer failed for D-LAG Periodic Sync Timer\n");
            return LA_FAILURE;
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeInit                                         */
/*                                                                           */
/* Description        : This function is used to de-initialize the D-LAG     */
/*                      functionality.                                       */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which D-LAG feature  */
/*                                  should be stopped.                       */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGDeInit (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    INT4                i4RetVal = LA_SUCCESS;
    tLaDLAGTxInfo       LaDLAGTxInfo;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeInit: NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    /* Stop D-LAG Periodic sync timer */
    if ((LaStopTimer (&pAggEntry->DLAGPeriodicSyncTmr)) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeInit: LaStopTimer failed for D-LAG "
                "Periodic Sync Timer\n");
        i4RetVal = LA_FAILURE;
    }

    if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
    {
        if ((LaStopTimer (&pAggEntry->DLAGMSSelectionWaitTmr)) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaDLAGDeInit: LaStopTimer failed for D-LAG "
                    "MS Selection Wait Timer.\n");
            i4RetVal = LA_FAILURE;
        }

        /* Copy Source Mac as Elector System ID */
        LA_MEMSET (pAggEntry->LaDLAGElector, 0, LA_MAC_ADDRESS_SIZE);
        pAggEntry->bLaDLAGIsDesignatedElector = LA_FALSE;
    }

    /* Reset Actor System ID to Global System ID */
    if (LaDLAGResetActorSystemID (pAggEntry) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeInit: LaDLAGResetActorSystemID failed.\n");
        i4RetVal = LA_FAILURE;
    }

    /* Reset Actor System Priority to Global System ID */
    if (LaDLAGResetActorSystemPriority (pAggEntry) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeInit: LaDLAGResetActorSystemPriority failed.\n");
        i4RetVal = LA_FAILURE;
    }

    LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);

    /* Send high-priority event-update message to remote D-LAG nodes */
    LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_DOWN;
    LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);

    /* Delete all remote aggregator entries */
    LaDLAGDeleteAllRemoteAggEntries (pAggEntry);

    /* Destroy RBTree of DLAG Remote Aggregator Entry */
    LaDLAGRemoteAggInfoTreeDestroy (pAggEntry->LaDLAGRemoteAggInfoTree,
                                    NULL, 0);
    pAggEntry->LaDLAGRemoteAggInfoTree = NULL;

    if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
         (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)) ||
        ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
         (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_SLAVE)))
    {
        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
            LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);
            LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);

        }
    }

    if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
         (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)) ||
        ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
         (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER)))
    {
        LaActiveDLAGDeleteEntriesInAllTree (pAggEntry);

        LaActiveDLAGDestroyPortListTree (pAggEntry->LaDLAGConsInfoTable.
                                         LaDLAGConsolidatedList, NULL, 0);
        pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList = NULL;
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "\n Free Units of CONS Entry POOL when shutdown = %d \n",
                     MemGetFreeUnits (LA_DLAGCONSPORTENTRY_POOL_ID));
    }

    LaActiveDLAGDestroyPortListTree (pAggEntry->LaDLAGConsInfoTable.
                                     LaDLAGPortList, NULL, 0);
    pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList = NULL;

    /* Change The Node Role Played as None */
    pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
    pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_NONE;

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : LaDLAGSetActorSystemIDToDLAG                         */
/*                                                                           */
/* Description        : This function will copy the DLAG System ID to        */
/*                      AggActorsystemID and updates the configured port     */
/*                      entries                                              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGSetActorSystemIDToDLAG (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGSetActorSystemIDToDLAG: NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    if ((LA_MEMCMP (pAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
                    pAggEntry->DLAGSystem.SystemMacAddr,
                    LA_MAC_ADDRESS_SIZE)) == 0)
    {
        return LA_SUCCESS;
    }

    LA_MEMCPY (pAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
               pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        LA_MEMCPY (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            if (pPortEntry->LaLacpMode == LA_MODE_LACP)
            {
                LaLacRxMachine (pPortEntry,
                                LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
            }
        }

        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGResetActorSystemID                             */
/*                                                                           */
/* Description        : This function will resets the  AggActorsystemID to   */
/*                      Global System ID and updates the configured port     */
/*                      entries                                              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaSystem                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGResetActorSystemID (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGResetActorSystemID: NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    if ((LA_MEMCMP (pAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
                    gLaGlobalInfo.LaSystem.SystemMacAddr,
                    LA_MAC_ADDRESS_SIZE)) == 0)
    {
        return LA_SUCCESS;
    }

    LA_MEMCPY (pAggEntry->AggConfigEntry.ActorSystem.SystemMacAddr,
               gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        LA_MEMCPY (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        pPortEntry->LaLacActorInfo.u2IfIndex =
            pPortEntry->LaLacActorAdminInfo.u2IfIndex;

        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            if (pPortEntry->LaLacpMode == LA_MODE_LACP)
            {
                LaLacRxMachine (pPortEntry,
                                LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
            }
        }

        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGChangeActorSystemPriorityToDLAG                */
/*                                                                           */
/* Description        : This function will copy the DLAG System priority to  */
/*                      AggActorsystem priority and updates the configured   */
/*                      port entries.                                        */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGChangeActorSystemPriorityToDLAG (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGChangeActorSystemPriorityToDLAG:NULL POINTER.\r\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry->AggConfigEntry.ActorSystem.u2SystemPriority =
        pAggEntry->DLAGSystem.u2SystemPriority;

    /* Change Actor System Priority in Ports */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        if (pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
            != pAggEntry->DLAGSystem.u2SystemPriority)
        {
            pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
                = pAggEntry->DLAGSystem.u2SystemPriority;
            pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority
                = pAggEntry->DLAGSystem.u2SystemPriority;

            if (LA_MODULE_STATUS == LA_ENABLED)
            {
                if (pPortEntry->LaLacpMode == LA_MODE_LACP)
                {
                    LaLacRxMachine (pPortEntry,
                                    LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
                }
            }
        }
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGChangeAttachedPortActorSysLAGIDToDLAG          */
/*                                                                           */
/* Description        : This function will copy the D-LAG System priority to */
/*                      newly attached port & Actor system priority and      */
/*                      copy D-LAG system ID to port actor system ID.        */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to port table entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGChangeAttachedPortActorSysLAGIDToDLAG (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if ((pPortEntry == NULL) || (pPortEntry->pAggEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGChangeAttachedPortActorSysLAGIDToDLAG:"
                " NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pPortEntry->pAggEntry;

    if ((pAggEntry->u1DLAGStatus != LA_DLAG_ENABLED) &&
        (pAggEntry->u1MCLAGStatus != LA_MCLAG_ENABLED))
    {
        return LA_FAILURE;
    }

    if ((pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
         != pAggEntry->DLAGSystem.u2SystemPriority) ||
        (LA_MEMCMP (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                    pAggEntry->DLAGSystem.SystemMacAddr,
                    LA_MAC_ADDRESS_SIZE) != 0))
    {
        LA_MEMCPY (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   pAggEntry->DLAGSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
            = pAggEntry->DLAGSystem.u2SystemPriority;
        pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority
            = pAggEntry->DLAGSystem.u2SystemPriority;

        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            if (pPortEntry->LaLacpMode == LA_MODE_LACP)
            {
                LaLacRxMachine (pPortEntry,
                                LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGResetPortActorSysLAGID                         */
/*                                                                           */
/* Description        : This function will resets the  AggActorsystemID to   */
/*                      Global System ID and resets the AggActorsystem       */
/*                      priority to Global System priority for the port entry*/
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to port table entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGResetPortActorSysLAGID (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if ((pPortEntry == NULL) || (pPortEntry->pAggEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGResetPortActorSysLAGID:NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pPortEntry->pAggEntry;
    if ((pAggEntry->u1DLAGStatus != LA_DLAG_ENABLED) &&
        (pAggEntry->u1MCLAGStatus != LA_MCLAG_ENABLED))
    {
        return LA_FAILURE;
    }

    /* Change Actor System Priority in Ports */
    if ((pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
         != gLaGlobalInfo.LaSystem.u2SystemPriority) ||
        (LA_MEMCMP (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                    gLaGlobalInfo.LaSystem.SystemMacAddr,
                    LA_MAC_ADDRESS_SIZE) != 0))
    {
        LA_MEMCPY (pPortEntry->LaLacActorInfo.LaSystem.SystemMacAddr,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LA_MEMCPY (pPortEntry->LaLacActorAdminInfo.LaSystem.SystemMacAddr,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
            = gLaGlobalInfo.LaSystem.u2SystemPriority;
        pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority
            = gLaGlobalInfo.LaSystem.u2SystemPriority;

        if (LA_MODULE_STATUS == LA_ENABLED)
        {
            if (pPortEntry->LaLacpMode == LA_MODE_LACP)
            {
                LaLacRxMachine (pPortEntry,
                                LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
            }
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGResetActorSystemPriority                       */
/*                                                                           */
/* Description        : This function will resets the AggActorsystem         */
/*                      priority to Global System priority and updates the   */
/*                      configured port entries                              */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregation entry to be       */
/*                      updated                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaSystem                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGResetActorSystemPriority (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGResetActorSystemPriority: NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry->AggConfigEntry.ActorSystem.u2SystemPriority =
        gLaGlobalInfo.LaSystem.u2SystemPriority;

    /* Change Actor System Priority in Ports */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        if (pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
            != gLaGlobalInfo.LaSystem.u2SystemPriority)
        {
            pPortEntry->LaLacActorAdminInfo.LaSystem.u2SystemPriority
                = gLaGlobalInfo.LaSystem.u2SystemPriority;
            pPortEntry->LaLacActorInfo.LaSystem.u2SystemPriority
                = gLaGlobalInfo.LaSystem.u2SystemPriority;

            if (LA_MODULE_STATUS == LA_ENABLED)
            {
                if (pPortEntry->LaLacpMode == LA_MODE_LACP)
                {
                    LaLacRxMachine (pPortEntry,
                                    LA_RXM_EVENT_ACTOR_ADMIN_CHANGED, NULL);
                }
            }
        }
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGCreateRemoteAggregator                         */
/*                                                                           */
/* Description        : This function is used to allocate memory for the     */
/*                      Remote D-LAG node port channel info                  */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : ppRetAggEntry - Allocated Remote Aggregator entry    */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory allocation is failed. */
/*****************************************************************************/
INT4
LaDLAGCreateRemoteAggregator (tMacAddr MacAddr,
                              tLaDLAGRemoteAggEntry ** ppRetRemoteAggEntry)
{
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    /* Allocate a block for the remote aggregator entry */
    if (LA_DLAGREMOTEAGGENTRY_ALLOC_MEM_BLOCK (pRemoteAggEntry) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LA_DLAGREMOTEPORTENTRY_ALLOC_MEM_BLOCK FAILED\r\n");
        return LA_ERR_MEM_FAILURE;
    }

    LA_MEMSET (pRemoteAggEntry, 0, sizeof (tLaDLAGRemoteAggEntry));

    /* Remote Port List init */
    LA_SLL_INIT (&(pRemoteAggEntry->LaDLAGRemotePortList));
    pRemoteAggEntry->pRemotePortEntry = NULL;
    pRemoteAggEntry->pAggEntry = NULL;

    /* Update Remote D-LAG node System MAC Address */
    LA_MEMCPY (pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               MacAddr, LA_MAC_ADDRESS_SIZE);
    pRemoteAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
    pRemoteAggEntry->u4DLAGKeepAliveCount = 0;

    *ppRetRemoteAggEntry = pRemoteAggEntry;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGCreateRemotePortEntry                          */
/*                                                                           */
/* Description        : This function is used to allocate memory for the     */
/*                      Remote port Entry                                    */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : u4RemotePortIndex - Remote Port Index for which new  */
/*                                          entry is to be created.          */
/*                      ppRetRemotePortEntry - pointer to the allocate       */
/*                                             remote port entry             */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory allocation is failed. */
/*****************************************************************************/
INT4
LaDLAGCreateRemotePortEntry (UINT4 u4RemotePortIndex,
                             tLaDLAGRemotePortEntry ** ppRetRemotePortEntry)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    /* Allocate a block for the remote aggregator entry */
    if (LA_DLAGREMOTEPORTENTRY_ALLOC_MEM_BLOCK (pRemotePortEntry) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LA_DLAGREMOTEPORTENTRY_ALLOC_MEM_BLOCK FAILED\r\n");
        return LA_ERR_MEM_FAILURE;
    }

    LA_MEMSET (pRemotePortEntry, 0, sizeof (tLaDLAGRemotePortEntry));

    LA_SLL_INIT_NODE (&pRemotePortEntry->NextNode);

    /* Update Remote D-LAG node System MAC Address */
    pRemotePortEntry->u4PortIndex = u4RemotePortIndex;

    *ppRetRemotePortEntry = pRemotePortEntry;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteRemoteAggregator                         */
/*                                                                           */
/* Description        : This function is used to delete the remote           */
/*                      aggregator entry from a remote aggregator info list. */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry from which remote       */
/*                                  pRemAggEntry should be found and deleted */
/*                      pRemAggEntry - Remote Aggregator entry to be deleted */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory de allocation is      */
/*                                           failed.                         */
/*****************************************************************************/
INT4
LaDLAGDeleteRemoteAggregator (tLaLacAggEntry * pAggEntry,
                              tLaDLAGRemoteAggEntry * pRemAggEntry)
{
    /* Added for HA support */
#ifdef L2RED_WANTED
    /*This function sends trigger message to standby       
       to delete remote aggregator entry */
    LaDLAGRedSendDelRemoteAggEntry (pAggEntry, pRemAggEntry);
#endif

    LaDLAGDeleteAllRemotePortEntries (pRemAggEntry);
    LaDLAGDeleteFromRemoteAggTable (pAggEntry, pRemAggEntry);

    /* Release Mempool allocated for Remote Aggregator entry */
    if (LA_DLAGREMOTEAGGENTRY_FREE_MEM_BLOCK (pRemAggEntry) != LA_MEM_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LA_DLAGREMOTEAGGENTRY_FREE_MEM_BLOCK FAILED\n");

        return LA_ERR_MEM_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteRemotePort                               */
/*                                                                           */
/* Description        : This function is used to delete the remote port      */
/*                      entry from a remote port info list.                  */
/*                                                                           */
/* Input(s)           : pRemAggEntry - Remote Aggregator entry from which    */
/*                                     pRemPortEntry should be found and     */
/*                                     deleted.                              */
/*                      pRemPortEntry - Remote port entry to be deleted      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_MEM_FAILURE - if memory de allocation is      */
/*                                           failed.                         */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGDeleteRemotePort (tLaDLAGRemoteAggEntry * pRemAggEntry,
                        tLaDLAGRemotePortEntry * pRemPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    if ((pRemAggEntry == NULL) || (pRemAggEntry->pAggEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaDLAGDeleteRemotePort: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pRemAggEntry->pAggEntry;

    if ((LA_DLAG_SYSTEM_STATUS != LA_DLAG_ENABLED) &&
        (pAggEntry->u1MCLAGStatus != LA_MCLAG_ENABLED))
    {
#ifdef NPAPI_WANTED
        if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
        {
            if ((pAggEntry)
                && (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_OFF))
            {
                if (LaFsLaHwRemoveLinkFromAggGroup (pAggEntry->u2AggIndex,
                                                    pRemPortEntry->
                                                    u4PortIndex) != FNP_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC | ALL_FAILURE_TRC,
                                 "LaFsLaHwRemoveLinkFromAggGroup: Failed to "
                                 "remove remote ports configuration from H/w for:"
                                 " port-channel: %u\n", pAggEntry->u2AggIndex);
                }
            }
        }
#endif
    }
    /* Added for HA support */
#ifdef L2RED_WANTED
    /*This function sends trigger message to standby 
       to delete  D-LAG remote port entry */
    LaDLAGRedSendDelRemotePortEntry (pRemAggEntry, pRemPortEntry);
#endif

    LaDLAGDeleteFromRemotePortTable (pRemAggEntry, pRemPortEntry);

    if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
         (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)) ||
        ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
         (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER)))
    {
        LaActiveDLAGRemoveRemotePortFromPortList (pRemAggEntry->pAggEntry,
                                                  pRemPortEntry->u4PortIndex);
    }
    /* Release Mempool allocated for Remote Port entry */
    if (LA_DLAGREMOTEPORTENTRY_FREE_MEM_BLOCK (pRemPortEntry) != LA_MEM_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                "LA_DLAGREMOTEPORTENTRY_FREE_MEM_BLOCK FAILED\n");
        return LA_ERR_MEM_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGUpdateRemoteDLAGNodeList.                      */
/*                                                                           */
/* Description        : This function is called by the D-LAG Rx Message      */
/*                      Handler function to update the remote aggregator     */
/*                      info list.                                           */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel whose remote aggregator  */
/*                                     info list needs to be updated         */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                      node on Distributing port. with LA Header stripped.  */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node/ */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGUpdateRemoteDLAGNodeList (tLaLacAggEntry * pAggEntry, UINT1 *pu1LinearBuf,
                                INT4 i4SllAction)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            MacAddr;
    tLaBoolean          IsRemoteElector;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    UINT1              *pu1PktBuf = NULL;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if ((pAggEntry == NULL) || (pu1LinearBuf == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGFormDSU: NULL POINTER/EMPTY BUFFER PASSED\r\n");
        return LA_ERR_NULL_PTR;
    }
    pu1PktBuf = pu1LinearBuf;

    /* System ID and System Priority */
    LA_MEMCPY (MacAddr, pu1PktBuf, LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Search Remote D-LAG Node Info List */
    LaDLAGGetRemoteAggEntryBasedOnMac (MacAddr, pAggEntry, &pRemoteAggEntry);
    if (i4SllAction == LA_DLAG_DEL_REM_NODE_INFO)
    {
        if (pRemoteAggEntry == NULL)
        {
            return LA_SUCCESS;
        }

        IsRemoteElector = (tLaBoolean) pRemoteAggEntry->u1DLAGElector;

        LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);

        LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                         MacAddr,
                                         IsRemoteElector,
                                         LA_DLAG_MS_TRIGGER_ON_DEL_NODE);

        return LA_SUCCESS;
    }

    if (pRemoteAggEntry == NULL)
    {
        /* If Remote D-LAG node Entry is not found 
         * - Allocate Memory for Remote D-LAG Aggregator
         * - Initialize with Default Values */
        if ((LaDLAGCreateRemoteAggregator (MacAddr, &pRemoteAggEntry)) !=
            LA_SUCCESS)
        {
            return LA_FAILURE;
        }

        if (LaDLAGAddToRemoteAggTable (pAggEntry, pRemoteAggEntry) ==
            LA_FAILURE)
        {
            LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "Failure in adding Remote D-LAG Node entry to given aggregator remote D-LAG info table\n");
            return LA_FAILURE;
        }
        pRemoteAggEntry->pAggEntry = pAggEntry;

        /* Node ID Comparison */
        if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                             MacAddr,
                                             LA_FALSE,
                                             LA_DLAG_MS_TRIGGER_ON_NEW_NODE);

            /* Send PO_UP Acknowledge Message */
            LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP_ACK;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
    }

    /* Whether Remote D-LAG node is newly added/Already Exist
     * pRemoteAggEntry pointer @this place points to Remote
     * D-LAG node to be updated */

    /* Update Remote D-LAG Node System Priority */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority = u2Val;

    /* Update Remote D-LAG Node Role Played */
    LA_GET_1BYTE (u1Val, pu1PktBuf);
    pRemoteAggEntry->u1DLAGRolePlayed = u1Val;

    /* Reset the Remote D-LAG Node Keep Alive Count
     * as We have received a Valid PDU */
    pRemoteAggEntry->u4DLAGKeepAliveCount = 0;

    /* Skip Master length */
    pu1PktBuf += (2 * LA_MAC_ADDRESS_SIZE);

    if (i4SllAction == LA_DLAG_EVENT_RES_SYNC_INFO)
    {
        LA_MEMCPY (pAggEntry->LaDLAGElector, MacAddr, LA_MAC_ADDRESS_SIZE);
        pRemoteAggEntry->u1DLAGElector = LA_TRUE;
    }

    LA_GET_1BYTE (u1Val, pu1PktBuf);
    pRemoteAggEntry->u1RedEnabled = (BOOL1) u1Val;

    pu1PktBuf += LA_DLAG_DSU_RESERVED_FIELD1_LEN;

    /* Update the Remote Port List Info */
    LaDLAGUpdateRemotePortList (pRemoteAggEntry, pu1PktBuf, i4SllAction);

    /* Added for HA support */
#ifdef L2RED_WANTED
    /*This function sends trigger message to standby 
       to add D-LAG remote aggregator entry  */
    LaDLAGRedSendAddRemoteAggorPortEntry (pAggEntry);
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGUpdateRemotePortList.                          */
/*                                                                           */
/* Description        : This function is called by the D-LAG update remote   */
/*                      Aggregator info function to update the corresponding */
/*                      remote port info list.                               */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote aggregator entry whose      */
/*                                        remote port info list needs to be  */
/*                                        updated.                           */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                                     node on Distributing port.            */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node/ */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                            UINT1 *pu1LinearBuf, INT4 i4SllAction)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tMacAddr            MacAddr = { 0 };
    tLaBoolean          IsRemoteElector = LA_FALSE;
    UINT1              *pu1PktBuf = NULL;
    UINT4               u4RemotePortIndex = 0;
    UINT1               u1Val = 0;
    UINT1               u1NoOfRemotePortsEntry = 0;
    UINT1               u1PrevSyncState = LA_INIT_VAL;
    UINT1               u1PrevBundleState = LA_INIT_VAL;
    UINT1               i = 0;
    tLaBoolean          TriggerMSAlgo = LA_FALSE;

#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = 0;
#endif

    pu1PktBuf = pu1LinearBuf;
    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC | ALL_FAILURE_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }
    if ((pRemoteAggEntry->pAggEntry == NULL) || (pu1LinearBuf == NULL))
    {
        LA_TRC (BUFFER_TRC | ALL_FAILURE_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pRemoteAggEntry->pAggEntry;

    /* Copy remote port info length */
    LA_GET_1BYTE (u1NoOfRemotePortsEntry, pu1PktBuf);

    if (i4SllAction == LA_DLAG_DEL_REM_PORT_INFO)
    {
        /* Copy First port Index */
        LA_GET_4BYTE (u4RemotePortIndex, pu1PktBuf);
        /* Search Remote D-LAG Node Info List */
        LaDLAGGetRemotePortEntry ((UINT2) u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            return LA_SUCCESS;
        }
        LaDLAGDeleteRemotePort (pRemoteAggEntry, pRemotePortEntry);

        if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                             MacAddr,
                                             IsRemoteElector,
                                             LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);
        }
        return LA_SUCCESS;
    }

    for (i = 0; i < u1NoOfRemotePortsEntry; i++)
    {

        /* Copy First port Index */
        LA_GET_4BYTE (u4RemotePortIndex, pu1PktBuf);

        /* Search Remote D-LAG Node Info List */
        LaDLAGGetRemotePortEntry (u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            /* If Remote Port Entry is not found 
             * - Allocate Memory for Remote Port Entry 
             * - Initialize with Default Values */
            if ((LaDLAGCreateRemotePortEntry (u4RemotePortIndex,
                                              &pRemotePortEntry)) != LA_SUCCESS)
            {
                return LA_FAILURE;
            }

            LaDLAGAddToRemotePortTable (pRemoteAggEntry, pRemotePortEntry);

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                TriggerMSAlgo = LA_TRUE;
            }

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_OFF)
                {
                    if (LaFsLaHwAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                                   u4RemotePortIndex,
                                                   &u2HwAggIndex) !=
                        FNP_SUCCESS)
                    {
                        LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC | ALL_FAILURE_TRC,
                                     "FsLaHwAddLinkToAggGroup: Failed to "
                                     "configure remote ports in H/w for: port-"
                                     "channel: %u\n", pAggEntry->u2AggIndex);
                    }
                }
            }
#endif
        }

        /* Remote Port Bundle State */
        LA_GET_1BYTE (u1Val, pu1PktBuf);

        u1PrevBundleState = pRemotePortEntry->u1BundleState;
        pRemotePortEntry->u1BundleState = u1Val;

        if (u1PrevBundleState != u1Val)
        {
            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                TriggerMSAlgo = LA_TRUE;
            }
        }

        /* Remote Port Sync State */
        LA_GET_1BYTE (u1Val, pu1PktBuf);

        u1PrevSyncState = pRemotePortEntry->u1SyncState;
        pRemotePortEntry->u1SyncState = u1Val;

        if (u1PrevSyncState != u1Val)
        {
            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                TriggerMSAlgo = LA_TRUE;
            }
        }

    }

    if (TriggerMSAlgo == LA_TRUE)
    {
        LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                         MacAddr,
                                         IsRemoteElector,
                                         LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteAllRemoteAggEntries.                     */
/*                                                                           */
/* Description        : This function is called to delete all the remote     */
/*                      aggregator Entries                                   */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to aggregator whose remote       */
/*                      D-LAG list needs to be deleted                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGDeleteAllRemoteAggEntries (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGRemoteAggEntry *pRemAggEntry = NULL;
    tLaDLAGRemoteAggEntry RemAggEntry;

    /* Memset the Aggregator Remote entry structure */
    LA_MEMSET (&RemAggEntry, 0, sizeof (tLaDLAGRemoteAggEntry));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeleteAllRemoteAggEntries: NULL POINTER.\r\n");
        return LA_ERR_NULL_PTR;
    }

    /* Get the first remote Aggregation Entry */
    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemAggEntry);
    while (pRemAggEntry != NULL)
    {
        LA_MEMCPY (&RemAggEntry, pRemAggEntry, sizeof (tLaDLAGRemoteAggEntry));
        LaDLAGDeleteRemoteAggregator (pAggEntry, pRemAggEntry);

        pRemAggEntry = NULL;
        LaDLAGGetNextRemoteAggEntry (pAggEntry, &RemAggEntry, &pRemAggEntry);
    }

    /* Reset Remote Aggregator list to null */
    pAggEntry->pDLAGRemoteAggEntry = NULL;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteAllRemotePortEntries.                    */
/*                                                                           */
/* Description        : This function is called to delete all the remote     */
/*                      port Entries                                         */
/*                                                                           */
/* Input(s)           : pRemAggEntry - Pointer to Remote Aggreator whose     */
/*                                     remote port list needs to be deleted  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGDeleteAllRemotePortEntries (tLaDLAGRemoteAggEntry * pRemAggEntry)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    if (pRemAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaDLAGDeleteAllRemoteAggEntries: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }
    /* Get the first remote Aggregation Entry */
    LaDLAGGetNextRemotePortEntry (pRemAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        LaDLAGDeleteRemotePort (pRemAggEntry, pRemotePortEntry);
        pRemotePortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemAggEntry, NULL, &pRemotePortEntry);
    }

    /* Reset Remote Port list to NULL */
    LA_SLL_INIT (&(pRemAggEntry->LaDLAGRemotePortList));
    pRemAggEntry->pRemotePortEntry = NULL;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTxPeriodicSyncPdu.                             */
/*                                                                           */
/* Description        : This function is called on D-LAG periodic sync timer */
/*                      expiry to send D-LAG Periodic sync PDU.              */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     D-LAG Periodic sync PDU               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGTxPeriodicSyncPdu (tLaLacAggEntry * pAggEntry)
{
    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGTxPeriodicSyncPdu: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    if (pAggEntry->u1DLAGStatus != LA_DLAG_ENABLED)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "LaDLAGTxPeriodicSyncPdu: "
                "D-LAG Periodic Sync PDU Tx is blocked as D-LAG status is "
                "Disabled");
        return LA_FAILURE;
    }

    if ((LaDLAGTxDSU (pAggEntry, NULL)) != LA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "LaDLAGTxPeriodicSyncPdu: D-LAG Periodic Sync PDU"
                     " Tx failed for port-channel : %u\r\n",
                     pAggEntry->u2AggIndex);

        if ((LaStartTimer (&(pAggEntry->DLAGPeriodicSyncTmr))) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaDLAGTxPeriodicSyncPdu: LaStartTimer failed for D-LAG "
                    "Periodic Sync Timer\r\n");
        }

        return LA_FAILURE;
    }

    if ((LaStartTimer (&(pAggEntry->DLAGPeriodicSyncTmr))) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGTxPeriodicSyncPdu: LaStartTimer failed for D-LAG "
                "Periodic Sync Timer\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTxEventUpdatePdu.                              */
/*                                                                           */
/* Description        : This function Forms and Sends Event-update PDU.      */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     event-update PDU.                     */
/*                      pLaDLAGTxInfo - Tx Info to be used in event-update   */
/*                                      PDU construction.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGTxEventUpdatePdu (tLaLacAggEntry * pAggEntry,
                        tLaDLAGTxInfo * pLaDLAGTxInfo)
{
    if (pAggEntry == NULL || pLaDLAGTxInfo == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "LaDLAGTxEventUpdatePdu: "
                "Null pointer is passed\r\n");
        return LA_ERR_NULL_PTR;
    }

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        if ((LaActiveDLAGTxEventDSU (pAggEntry, pLaDLAGTxInfo)) != LA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "LaDLAGTxEventUpdatePdu: D-LAG Event Update PDU"
                         " Tx failed for port-channel : %u\n",
                         pAggEntry->u2AggIndex);
            return LA_FAILURE;
        }
        pAggEntry->u4DLAGEventUpdatePduTxCount++;

    }
#ifdef ICCH_WANTED
    else if (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE)
    {
        if ((LaActiveMCLAGTxEventDSU (pAggEntry, pLaDLAGTxInfo)) != LA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "LaDLAGTxEventUpdatePdu: D-LAG Event Update PDU"
                         " Tx failed for port-channel : %u\n",
                         pAggEntry->u2AggIndex);
            return LA_FAILURE;
        }
    }
#endif
    else
    {
        if ((LaDLAGTxDSU (pAggEntry, pLaDLAGTxInfo)) != LA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "LaDLAGTxEventUpdatePdu: D-LAG Event Update PDU"
                         " Tx failed for port-channel : %u\n",
                         pAggEntry->u2AggIndex);
            return LA_FAILURE;
        }
        pAggEntry->u4DLAGEventUpdatePduTxCount++;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGTxDSU.                                         */
/*                                                                           */
/* Description        : This function Forms and Sends D-LAG PDU.             */
/*                      This function will be called from Tx event-update    */
/*                      Tx periodic-sync message.                            */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     D-LAG PDU.                            */
/*                      pLaDLAGTxInfo - Tx Info.                             */
/*                         - NULL: for periodic sync PDU Tx info.            */
/*                         - Pointer to TxInfo Structure, to be sent in      */
/*                           update PDU.                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGTxDSU (tLaLacAggEntry * pAggEntry, tLaDLAGTxInfo * pLaDLAGTxInfo)
{
    tLaBufChainHeader  *pMsgBuf = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    UINT1               au1LinearBuf[LA_SLOW_PROTOCOL_SIZE] = { 0 };
    BOOL1               bResult = OSIX_FALSE;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaDLAGTxDSU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }
    /* Form D-LAG PDU */
    if ((LaDLAGFormDSU (pAggEntry, au1LinearBuf, pLaDLAGTxInfo)) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGTxDSU: LaDLAGFormDSU() failed.\n");
        return LA_FAILURE;
    }

    while (u4Count < pAggEntry->u4DLAGDistributingPortListCount)
    {
        OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingOperPortList,
                                 u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE,
                                 bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue the for loop */
            u4DLAGDistributingIfIndex++;
            continue;
        }
        u4Count++;
        /* Check If Distributing port is operationally UP,
         * If not then packet transmission attempt need not
         * be made on the failed distributing port, continue scanning the 
         * remaining distribution port list*/
        LaGetPortEntry ((UINT2) u4DLAGDistributingIfIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            u4DLAGDistributingIfIndex++;
            continue;
        }
        if (pPortEntry->u1PortOperStatus == CFA_IF_DOWN)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "LaDLAGTxDSU: D-LAG PDU Tx is not possible as Distributing"
                         " port %d configured for this port channel is down.\n",
                         u4DLAGDistributingIfIndex);
            u4DLAGDistributingIfIndex++;
            continue;
        }

        /* Allocate memory for CRU buffer */
        if ((pMsgBuf = LA_ALLOC_CRU_BUF (LA_LACPDU_SIZE, 0)) == NULL)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "LaDLAGTxDSU: Buffer Allocation failed\n");
            return LA_FAILURE;
        }

        /* Copy from Linear buffer to CRU buffer */
        LA_COPY_OVER_CRU_BUF (pMsgBuf, au1LinearBuf, 0, LA_SLOW_PROTOCOL_SIZE);

        /* Send the D-LAG PDU */
        LA_TRC (CONTROL_PLANE_TRC,
                "LaDLAGTxDSU: Handing over LACPDU to Lower Layer...\n");
        LaHandOverOutFrame (pMsgBuf, (UINT2) u4DLAGDistributingIfIndex, NULL);
        pAggEntry->u4DLAGPeriodicSyncPduTxCount++;
        u4DLAGDistributingIfIndex++;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetRemoteAggEntryBasedOnMac                    */
/*                                                                           */
/* Description        : This function returns the pointer to the Remote      */
/*                      D-LAG info node matching Remote System Source Mac    */
/*                      address in remote aggregator info list.              */
/*                                                                           */
/*                                                                           */
/* Input(s)           : LaDLAGRemoteNodeMacAddr - Remote Agg Entry index,    */
/*                                                Remote node Source Mac     */
/*                                                Address.                   */
/*                      pAggEntry - Aggregator Entry in which Remote Aggr    */
/*                                  Entry needs to be searched.              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : ppRemoteAggEntry - pointer to the Remote Aggregator  */
/*                                         entry in remote aggr info list    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaDLAGGetRemoteAggEntryBasedOnMac (tLaMacAddr LaDLAGRemoteNodeMacAddr,
                                   tLaLacAggEntry * pAggEntry,
                                   tLaDLAGRemoteAggEntry ** ppRemoteAggEntry)
{
    tLaDLAGRemoteAggEntry RemoteAggEntry;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LA_MEMSET (&RemoteAggEntry, 0, sizeof (tLaDLAGRemoteAggEntry));
    LA_MEMCPY (RemoteAggEntry.DLAGRemoteSystem.SystemMacAddr,
               LaDLAGRemoteNodeMacAddr, LA_MAC_ADDRESS_SIZE);
    if (pAggEntry != NULL)
    {
        pRemoteAggEntry = RBTreeGet (pAggEntry->LaDLAGRemoteAggInfoTree,
                                     (tRBElem *) & RemoteAggEntry);

        if (pRemoteAggEntry != NULL)
        {
            *ppRemoteAggEntry = pRemoteAggEntry;
            return LA_SUCCESS;
        }
    }
    *ppRemoteAggEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetRemotePortEntry                             */
/*                                                                           */
/* Description        : This function returns the pointer to the Remote      */
/*                      Port entry of a D-LAG remote node.                   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4RemotePortIndex - Index of Remote Port to be       */
/*                                          searched (search key)            */
/*                      pRemoteAggEntry - Pointer to remote Aggregator entry */
/*                                        in whose remote port info list     */
/*                                        given remote port entry should be  */
/*                                        searched.                          */
/*                                                                           */
/* Output(s)          : ppRemotePortEntry - pointer to the found remote port */
/*                                          node in remote port info list    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
LaDLAGGetRemotePortEntry (UINT4 u4RemotePortIndex,
                          tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                          tLaDLAGRemotePortEntry ** ppRemotePortEntry)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LA_SLL_SCAN (&(pRemoteAggEntry->LaDLAGRemotePortList), pRemotePortEntry,
                 tLaDLAGRemotePortEntry *)
    {
        if (pRemotePortEntry->u4PortIndex == u4RemotePortIndex)
        {
            *ppRemotePortEntry = pRemotePortEntry;
            return LA_SUCCESS;
        }
    }

    *ppRemotePortEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetNextRemoteAggEntry                          */
/*                                                                           */
/* Description        : This function returns pointer to the next remote     */
/*                      D-LAG Node entry.                                    */
/*                      If this function called with NULL as pRemoteAggEntry */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which remote node    */
/*                                  info list is maintained                  */
/*                      pRemoteAggEntry - Remote Aggr Entry pointer for      */
/*                                        which next node should be returned */
/*                                                                           */
/* Output(s)          : pNextRemoteAggEntry - pointer to the next Node       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaDLAGGetNextRemoteAggEntry (tLaLacAggEntry * pAggEntry,
                             tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                             tLaDLAGRemoteAggEntry ** pNextRemoteAggEntry)
{
    tLaDLAGRemoteAggEntry *pTmpRemoteAggEntry = NULL;

    if (pRemoteAggEntry == NULL)
    {

        pTmpRemoteAggEntry = (tLaDLAGRemoteAggEntry *) RBTreeGetFirst
            (pAggEntry->LaDLAGRemoteAggInfoTree);
    }
    else
    {

        pTmpRemoteAggEntry = (tLaDLAGRemoteAggEntry *) RBTreeGetNext
            (pAggEntry->LaDLAGRemoteAggInfoTree,
             (tRBElem *) pRemoteAggEntry, NULL);
    }

    if (pTmpRemoteAggEntry != NULL)
    {
        *pNextRemoteAggEntry = pTmpRemoteAggEntry;
        return LA_SUCCESS;
    }

    *pNextRemoteAggEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetNextRemotePortEntry                         */
/*                                                                           */
/* Description        : This function returns pointer to the next remote     */
/*                      Port entry of a D-LAG remote node.                   */
/*                      This function returns pointer to the first port      */
/*                      entry as the next node when pRemotePortEntry passed  */
/*                      is NULL.                                             */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote Aggr list in which remote   */
/*                                        port list is maintained.           */
/*                      pRemotePortEntry - pointer to the current remote     */
/*                                         port entry whose next remote      */
/*                                         port entry is to be returned      */
/*                                                                           */
/* Output(s)          : pNextRemotePortEntry - pointer to the next remote    */
/*                                             port entry found              */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGGetNextRemotePortEntry (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                              tLaDLAGRemotePortEntry * pRemotePortEntry,
                              tLaDLAGRemotePortEntry ** pNextRemotePortEntry)
{
    tLaDLAGRemotePortEntry *pTmpRemotePortEntry = NULL;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGGetNextRemotePortEntry:" " NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    if (pRemotePortEntry == NULL)
    {
        pTmpRemotePortEntry =
            (tLaDLAGRemotePortEntry *)
            LA_SLL_NEXT (&(pRemoteAggEntry->LaDLAGRemotePortList), NULL);
    }
    else
    {
        pTmpRemotePortEntry =
            (tLaDLAGRemotePortEntry *)
            LA_SLL_NEXT (&(pRemoteAggEntry->LaDLAGRemotePortList),
                         &pRemotePortEntry->NextNode);
    }
    if (pTmpRemotePortEntry != NULL)
    {
        *pNextRemotePortEntry = pTmpRemotePortEntry;
        return LA_SUCCESS;
    }

    *pNextRemotePortEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaDLAGKeepAlive                                      */
/*                                                                           */
/* Description        : This function increments keep alive counter for      */
/*                      each D-LAG remote node after each D-LAG periodic     */
/*                      sync timer expiry and checks if keep alive counter   */
/*                      has reached maximum, Keep alive count configured     */
/*                      for the port channe, If found true then that remote  */
/*                      D-LAG node entry will be removed                     */
/*                      from the remote D-LAG node info list.                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - pointer to the aggregator entry.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaDLAGKeepAlive (tLaLacAggEntry * pAggEntry)
{

    tLaBoolean          IsElectorDown = LA_FALSE;
    tMacAddr            MacAddr;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaDLAGKeepAlive: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
    while (pRemoteAggEntry != NULL)
    {
        pRemoteAggEntry->u4DLAGKeepAliveCount++;

        /* If Current Keep alive count has reached max Keep alive count
         * then the particular remote D-LAG node is down and Remote D-LAG
         * node entry can be removed form remote D-LAG node info list */
        if (pRemoteAggEntry->u4DLAGKeepAliveCount >=
            pAggEntry->u4DLAGMaxKeepAliveCount)
        {
            /* If D-LAG Redundancy feature is enabled, Re start the
             * Election */
            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                /* If Elector is Down then, Reset the Stored Elector ID to
                 * Node's Source Mac and I Am elector flag to true */
                if (LA_MEMCMP (pAggEntry->LaDLAGElector,
                               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                               LA_MAC_ADDRESS_SIZE) == 0)
                {
                    IsElectorDown = LA_TRUE;
                }
                /* If Elector is Down then, Reset the Stored Elector ID to
                 * Node's Source Mac and I Am elector flag to true */
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 MacAddr,
                                                 IsElectorDown,
                                                 LA_DLAG_MS_TRIGGER_ON_DEL_NODE);
            }

            /* Delete remote aggregator */
            LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);

            LA_TRC (CONTROL_PLANE_TRC, "\n KEEP ALIVE : Node is DOWN \n");
            if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
                (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER))
            {
                /* Trigger consolidation logic so that new Best info 
                   is selected */
                LaActiveDLAGChkForBestConsInfo (pAggEntry);
                LaActiveDLAGSortOnStandbyConstraints (pAggEntry);
            }
#ifdef ICCH_WANTED
            else if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
                     (pAggEntry->u1MCLAGRolePlayed ==
                      LA_MCLAG_SYSTEM_ROLE_MASTER))
            {
                /* Trigger consolidation logic so that new Best info 
                   is selected */
                LaActiveMCLAGChkForBestConsInfo (pAggEntry);
                LaActiveMCLAGSortOnStandbyConstraints (pAggEntry);
            }
#endif

            pRemoteAggEntry = NULL;
        }

        LaDLAGGetNextRemoteAggEntry (pAggEntry, pRemoteAggEntry,
                                     &pRemoteAggEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowValidateRemotePortChannelIndex                */
/* Input       :  i4FsLaPortChannelIfIndex,                                  */
/*                FsLaDlagRemotePortChannelSysID                          */
/*                                                                           */
/* Description : This function validates the indexes of remote               */
/*               port channel table                                          */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex, FsLaDlagRemotePortChannelSysID*/
/* Output      :  None                                                       */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowValidateRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                             tMacAddr
                                             FsLaDlagRemotePortChannelSysID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    if ((i4FsLaPortChannelIfIndex < 0)
        || (i4FsLaPortChannelIfIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (LaDLAGGetRemoteAggEntryBasedOnMac
        (FsLaDlagRemotePortChannelSysID, pAggEntry,
         &pRemoteAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGSnmpLowGetFirstValidRemotePortChannelIndex     */
/*                                                                           */
/* Description        : This function returns the first index of D-LAG Remote*/
/*                      Aggregator Table. This function is called            */
/*                      with these output parameters initialized to zero     */
/*                                                                           */
/* Input(s)          :  None                                                 */
/* Output(s)          : Port Channel interface index and Remote System Id    */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetFirstValidRemotePortChannelIndex (INT4
                                                  *pi4FsLaPortChannelIfIndex,
                                                  tMacAddr *
                                                  pFsLaDLAGRemotePortChannelSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    *pi4FsLaPortChannelIfIndex = pAggEntry->u2AggIndex;

    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    LA_MEMCPY (pFsLaDLAGRemotePortChannelSystemID,
               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowGetNextValidRemotePortChannelIndex            */
/* Description :  This function returns the next index of D-LAG Remote       */
/*                Aggregator Table. If this function is passed with NULL     */
/*                values, then first index will be returned                  */
/*                                                                           */
/* Input       :  PortChannelIfIndex and Remote Port Channel System ID       */
/*                                                                           */
/* Output      :  Next PortChannelIfIndex and Next Remote Port               */
/*                Channel System ID                                          */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetNextValidRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                                 INT4
                                                 *pi4NextFsLaPortChannelIfIndex,
                                                 tMacAddr
                                                 FsLaDlagRemotePortChannelSysID,
                                                 tMacAddr *
                                                 pNextFsLaDLAGRemotePortChannelSystemID)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pNextRemoteAggEntry = NULL;

    if ((i4FsLaPortChannelIfIndex < 0)
        || (i4FsLaPortChannelIfIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    if (LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry) !=
        LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    /* Get the Remote Aggregator entry */
    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDlagRemotePortChannelSysID,
                                       pAggEntry, &pRemoteAggEntry);
    do
    {
        /* Get the next remote aggreator entry 
         * if the entry is present, then retrive the index values and return*/
        if (LaDLAGGetNextRemoteAggEntry
            (pAggEntry, pRemoteAggEntry, &pNextRemoteAggEntry) == LA_SUCCESS)
        {
            *pi4NextFsLaPortChannelIfIndex = pAggEntry->u2AggIndex;
            MEMCPY (pNextFsLaDLAGRemotePortChannelSystemID,
                    pNextRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                    LA_MAC_ADDRESS_SIZE);
            return LA_SUCCESS;
        }
        /* If there are more than one Port Channnel present, get the next 
         * aggregator entry. Set the remote aggregator entry to NULL. 
         * So that in the next iteration it will return the first index
         * of the remote port channel*/
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
        pRemoteAggEntry = NULL;
    }
    while (pAggEntry != NULL);    /* Iterate until the port channel entry is NULL */

    return LA_FAILURE;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowValidateRemotePortIndex                       */
/* Description :  This function validates the indexes of the D-LAG remote    */
/*                port table                                                 */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex,FsLaDlagRemotePortChannelSysID */
/*                i4FsLaDLAGRemotePortIndex                                  */
/*                                                                           */
/* Output      :  None                                                       */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowValidateRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                      tMacAddr FsLaDlagRemotePortChannelSysID,
                                      INT4 i4FsLaDLAGRemotePortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    if ((i4FsLaPortChannelIfIndex < 0)
        || (i4FsLaPortChannelIfIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }

    LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    LaDLAGGetRemoteAggEntryBasedOnMac (FsLaDlagRemotePortChannelSysID,
                                       pAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (LaDLAGGetRemotePortEntry
        ((UINT4) i4FsLaDLAGRemotePortIndex, pRemoteAggEntry,
         &pRemotePortEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGSnmpLowGetFirstValidRemotePortIndex            */
/*                                                                           */
/* Description        : This function gets the first valid index of the      */
/*                      D-LAG Remote Port Table. This function is called     */
/*                      with these output parameters initialized to zero     */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : Port Channel Index, Remote Port Channel System ID    */
/*                      RemotePortIndex                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetFirstValidRemotePortIndex (INT4 *pi4FsLaPortChannelIfIndex,
                                           tMacAddr *
                                           pFsLaDLAGRemotePortChannelSystemID,
                                           INT4 *pi4FsLaDLAGRemotePortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    if (pRemotePortEntry == NULL)
    {
        return LA_FAILURE;
    }

    *pi4FsLaPortChannelIfIndex = pAggEntry->u2AggIndex;
    LA_MEMCPY (pFsLaDLAGRemotePortChannelSystemID,
               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);
    *pi4FsLaDLAGRemotePortIndex = (INT4) pRemotePortEntry->u4PortIndex;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function    :  LaDLAGSnmpLowGetNextValidRemotePortIndex                   */
/* Description :  This function returns the next index of the D-LAG Remote   */
/*                Port Table. If this function is passed with NULL           */
/*                values, then first index will be returned                  */
/*                                                                           */
/* Input       :  i4FsLaPortChannelIfIndex,FsLaDlagRemotePortChannelSysID */
/*                i4FsLaDLAGRemotePortIndex                                  */
/*                                                                           */
/* Output      :  pi4NextFsLaPortChannelIfIndex,                             */
/*                pNextFsLaDLAGRemotePortChannelSystemId,                    */
/*                pi4NextFsLaDLAGRemotePortIndex                             */
/* Returns     :  LA_SUCCESS / LA_FAILURE                                    */
/*****************************************************************************/

INT4
LaDLAGSnmpLowGetNextValidRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                          INT4 *pi4NextFsLaPortChannelIfIndex,
                                          tMacAddr
                                          FsLaDlagRemotePortChannelSysID,
                                          tMacAddr *
                                          pNextFsLaDLAGRemotePortChannelSystemID,
                                          INT4 i4FsLaDLAGRemotePortIndex,
                                          INT4 *pi4NextFsLaDLAGRemotePortIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGRemotePortEntry *pNextRemotePortEntry = NULL;

    if ((i4FsLaPortChannelIfIndex < 0)
        || (i4FsLaPortChannelIfIndex > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    /* Get the aggregator entry */
    if (LaGetAggEntry ((UINT2) i4FsLaPortChannelIfIndex, &pAggEntry) !=
        LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    /* Get the remote  aggregator entry */
    if (LaDLAGGetRemoteAggEntryBasedOnMac
        (FsLaDlagRemotePortChannelSysID, pAggEntry,
         &pRemoteAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }

    LaDLAGGetRemotePortEntry ((UINT4) i4FsLaDLAGRemotePortIndex,
                              pRemoteAggEntry, &pRemotePortEntry);
    do
    {
        do
        {
            if (LaDLAGGetNextRemotePortEntry
                (pRemoteAggEntry, pRemotePortEntry,
                 &pNextRemotePortEntry) == LA_SUCCESS)
            {
                *pi4NextFsLaPortChannelIfIndex = pAggEntry->u2AggIndex;
                MEMCPY (pNextFsLaDLAGRemotePortChannelSystemID,
                        pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                        LA_MAC_ADDRESS_SIZE);
                *pi4NextFsLaDLAGRemotePortIndex =
                    (INT4) pNextRemotePortEntry->u4PortIndex;
                return LA_SUCCESS;
            }
            LaDLAGGetNextRemoteAggEntry (pAggEntry, pRemoteAggEntry,
                                         &pRemoteAggEntry);
            pRemotePortEntry = NULL;
        }
        while (pRemoteAggEntry != NULL);

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
        pRemotePortEntry = NULL;
        pRemoteAggEntry = NULL;
    }
    while (pAggEntry != NULL);

    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaDLAGAddToRemoteAggTable                            */
/*                                                                           */
/* Description        : This function adds a Remote D-LAG Node entry to      */
/*                      given aggregator remote D-LAG info table             */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator to which remote D-LAG node to */
/*                                  be added.                                */
/*                      pRemoteAggEntry - Remote Aggregator entry to be      */
/*                                        added.                             */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaDLAGAddToRemoteAggTable (tLaLacAggEntry * pAggEntry,
                           tLaDLAGRemoteAggEntry * pRemoteAggEntry)
{
    UINT4               u4Status = RB_FAILURE;

    u4Status = RBTreeAdd (pAggEntry->LaDLAGRemoteAggInfoTree, pRemoteAggEntry);
    if (u4Status == RB_FAILURE)
    {
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGAddToRemotePortTable                           */
/*                                                                           */
/* Description        : This function adds a Remote port entry to remote     */
/*                      port info list maintained in a remote aggregator     */
/*                      info table                                           */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - pointer to the Remote Aggr info    */
/*                                        list in which remote port info     */
/*                                        list is maintained to which given  */
/*                                        port entry needs to be added.      */
/*                      pRemotePortEntry - pointer to the port entry to be   */
/*                                         added.                            */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGAddToRemotePortTable (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                            tLaDLAGRemotePortEntry * pRemotePortEntry)
{
    tLaDLAGRemotePortEntry *pCurRemPortEntry = NULL;
    tLaDLAGRemotePortEntry *pPrevRemPortEntry = NULL;

    /* Entry addition is done in assending order of Port Index Address */
    LA_SLL_SCAN (&(pRemoteAggEntry->LaDLAGRemotePortList), pCurRemPortEntry,
                 tLaDLAGRemotePortEntry *)
    {
        if (pCurRemPortEntry->u4PortIndex < pRemotePortEntry->u4PortIndex)
        {
            pPrevRemPortEntry = pCurRemPortEntry;
            continue;
        }
        break;
    }

    LA_SLL_INSERT (&(pRemoteAggEntry->LaDLAGRemotePortList),
                   &pPrevRemPortEntry->NextNode,
                   (tLaSllNode *) (VOID *) pRemotePortEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteFromRemoteAggTable                       */
/*                                                                           */
/* Description        : This function deletes a D-LAG node entry from Remote */
/*                      Remote Node Info List maintained in pAggEntry        */
/*                                                                           */
/* Input(s)           : pAggEntry - pointer to Aggr Entry in which           */
/*                                  pRemoteAggEntry node should be found and */
/*                                  deleted.                                 */
/*                      pRemoteAggEntry - pointer to the remote Aggr entry   */
/*                                        that needs to be deleted           */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGDeleteFromRemoteAggTable (tLaLacAggEntry * pAggEntry,
                                tLaDLAGRemoteAggEntry * pRemoteAggEntry)
{
    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeleteFromRemoteAggTable: " "NULL POINTER.\n");
        return;
    }
    RBTreeRem (pAggEntry->LaDLAGRemoteAggInfoTree, (tRBElem *) pRemoteAggEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGDeleteFromRemotePortTable                      */
/*                                                                           */
/* Description        : This function deletes a remote port entry from       */
/*                      Remote D-LAG port info list.                         */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - pointer to the Remote Aggr Entry   */
/*                                        in which pRemotePortEntry should   */
/*                                        be found and deleted.              */
/*                      pRemotePortEntry - pointer to the remote port entry  */
/*                                         that needs to be deleted.         */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGDeleteFromRemotePortTable (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                 tLaDLAGRemotePortEntry * pRemotePortEntry)
{
    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeleteAllRemoteAggEntries: " "NULL POINTER.\n");
        return;
    }

    LA_SLL_DELETE (&(pRemoteAggEntry->LaDLAGRemotePortList),
                   &pRemotePortEntry->NextNode);
    LA_SLL_INIT_NODE (&pRemotePortEntry->NextNode);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGFormDSU.                                       */
/*                                                                           */
/* Description        : This function is called to form a LACPDU from the    */
/*                      particular LaDLAGTxDSU to form D-LAG PDU.            */
/*                                                                           */
/* Input(s)           : pAggEntry - Port channel which is trying to send     */
/*                                  D-LAG PDU.                               */
/*                      pu1LinearBuf - Pointer to the linear OUT buffer.     */
/*                      pLaDLAGTxInfo :                                      */
/*                               NULL - Periodic-sync PDU                    */
/*                               info to be embedded PDU - Event-update      */
/*                                                         D-LAG pdu.        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaDLAGFormDSU (tLaLacAggEntry * pAggEntry, UINT1 *pu1LinearBuf,
               tLaDLAGTxInfo * pLaDLAGTxInfo)
{
    tLaLacPortEntry    *pRefPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT1              *pu1PortInfoLenField = NULL;
    UINT1              *pu1PktBuf = NULL;
    UINT4               u4Val = 0;
    UINT4               u4Count = 0;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT2               u2Val = 0;
    UINT1               u1BundleState = 0;
    UINT1               u1Val = 0;
    UINT1               u1PortCount = 0;
    UINT1               u1DSUType = 0;
    BOOL1               bResult = OSIX_FALSE;

    if ((pAggEntry == NULL) || (pu1LinearBuf == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGFormDSU: NULL POINTER/EMPTY " "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    /* Copy Destination Address */
    LA_MEMCPY (pu1PktBuf, &gLaSlowProtAddr, LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Get Source Address from Distributing port from CFA */

    while (u4Count < pAggEntry->u4DLAGDistributingPortListCount)
    {
        OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingOperPortList,
                                 u4DLAGDistributingIfIndex, LA_PORT_LIST_SIZE,
                                 bResult);
        if (bResult == OSIX_FALSE)
        {
            /* If the bit is not set, continue the for loop */
            u4DLAGDistributingIfIndex++;
            continue;
        }
        u4Count++;

        LaGetPortEntry ((UINT2) u4DLAGDistributingIfIndex, &pPortEntry);

        if (pPortEntry == NULL)
        {
            u4DLAGDistributingIfIndex++;
            continue;
        }
        u4DLAGDistributingIfIndex++;
    }

    /* Copy Source Address */
    if (pPortEntry != NULL)
    {
        LA_MEMCPY (pu1PktBuf, pPortEntry->au1MacAddr, LA_MAC_ADDRESS_SIZE);
    }
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Copy Protocol Type : SLOW */
    u2Val = (UINT2) LA_SLOW_PROT_TYPE;
    LA_PUT_2BYTE (pu1PktBuf, u2Val);

    /* Copy Protocol Sub Type : LACP */
    LA_PUT_1BYTE (pu1PktBuf, (UINT1) LACPDU_SUB_TYPE);

    /* Copy Version Number */
    LA_PUT_1BYTE (pu1PktBuf, (UINT1) LACP_VERSION);

    /* Fill TLV Type = DSU */
    LA_PUT_1BYTE (pu1PktBuf, (UINT1) LA_DLAG_DSU_INFO);

    /* If Null then Periodic Sync PDU, set DSUType to zero, If Not then
     * Event Update PDU copy the DSUType from pLaDLAGTxInfo to PDU */
    if (pLaDLAGTxInfo)
    {
        u1DSUType = pLaDLAGTxInfo->u1DSUType;
    }

    /* Fill DSU Sub type */
    LA_PUT_1BYTE (pu1PktBuf, u1DSUType);

    /* Fill DSU Info Lenght : excluding remote port list info */
    LA_PUT_1BYTE (pu1PktBuf, (UINT1) LA_DLAG_DSU_INFO_LEN);

    /* Copy Port Channel Key */
    u4Val = (UINT4) pAggEntry->AggConfigEntry.u2ActorAdminKey;
    LA_PUT_4BYTE (pu1PktBuf, u4Val);

    /* Copy System ID : source mac */
    LA_MEMCPY (pu1PktBuf, gLaGlobalInfo.LaSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Copy System Priority */
    u2Val = gLaGlobalInfo.LaSystem.u2SystemPriority;
    LA_PUT_2BYTE (pu1PktBuf, u2Val);

    /* Copy Role Played */
    u1Val = pAggEntry->u1DLAGRolePlayed;
    LA_PUT_1BYTE (pu1PktBuf, u1Val);

    if (u1DSUType == LA_DLAG_LOW_PRIO_EVENT_RES_SYNC)
    {
        /* Copy Slave Mac Address */
        LA_MEMCPY (pu1PktBuf, pLaDLAGTxInfo->LaDLAGSlaveMacAddr,
                   LA_MAC_ADDRESS_SIZE);
        pu1PktBuf += LA_MAC_ADDRESS_SIZE;

        /* Copy Elector Mac Address */
        LA_MEMCPY (pu1PktBuf, pAggEntry->LaDLAGElector, LA_MAC_ADDRESS_SIZE);
        pu1PktBuf += LA_MAC_ADDRESS_SIZE;
    }
    else
    {
        /* Skip master and Elector Mac address */
        pu1PktBuf += (2 * LA_MAC_ADDRESS_SIZE);
    }

    LA_PUT_1BYTE (pu1PktBuf, pAggEntry->u1DLAGRedundancy);

    /* Skip Reserved Field */
    pu1PktBuf += LA_DLAG_DSU_RESERVED_FIELD1_LEN;

    /* Marker Pointer for Number of Remote Port Info field 
     * This field is filled at the end of this function */
    pu1PortInfoLenField = pu1PktBuf;
    pu1PktBuf += 1;

    /* If Tx Event-update message to be sent is
     * for Port detatched from Aggregator then
     * Only detached port index will be sent */
    if ((u1DSUType == LA_DLAG_HIGH_PRIO_EVENT_PORT_DETATCH_FROM_AGG) ||
        (u1DSUType == LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG))
    {
        u1PortCount++;
        u4Val = pLaDLAGTxInfo->u4PortIndex;
        LA_PUT_4BYTE (pu1PktBuf, u4Val);
    }
    else
    {
        /* Copy Port List info */
        pRefPortEntry = pNextPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pRefPortEntry, &pNextPortEntry);
        while (pNextPortEntry != NULL)
        {
            u1PortCount++;
            /* Copy Port Info */
            u4Val = (UINT4) pNextPortEntry->u2PortIndex;
            LA_PUT_4BYTE (pu1PktBuf, u4Val);

            u4Val = (UINT4) pNextPortEntry->u2PortIndex;
            if ((LaGetPortBundleState (u4Val, &u1BundleState)) != LA_SUCCESS)
            {
                return LA_FAILURE;
            }

            LA_PUT_1BYTE (pu1PktBuf, u1BundleState);

            u1Val = pNextPortEntry->u1PortOperStatus;

            LA_PUT_1BYTE (pu1PktBuf, u1Val);

            pRefPortEntry = pNextPortEntry;
            pNextPortEntry = NULL;
            LaGetNextAggPortEntry (pAggEntry, pRefPortEntry, &pNextPortEntry);
        }
    }

    LA_PUT_1BYTE (pu1PortInfoLenField, u1PortCount);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGInitForcefulLacpTx.                            */
/*                                                                           */
/* Description        : This function is called to sent LACP PDU's on all    */
/*                      aggregator ports configured for an Aggregator with   */
/*                      actor synchronization bit set to desired value       */
/*                      either IN_SYNC/OUT_OF_SYNC                           */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel Index.                   */
/*                      LaSynchronization - Synchronization bit value        */
/*                                          Possible values are IN_SYNC/     */
/*                                          OUT_OF_SYNC                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGInitForcefulLacpTx (tLaLacAggEntry * pAggEntry,
                          tLaBoolean LaSynchronization)
{
    tLaLacPortEntry    *pRefPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGInitForcefulLacpTx: NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Copy Port List info */
    pRefPortEntry = pPortEntry = NULL;
    LaGetNextAggPortEntry (pAggEntry, pRefPortEntry, &pPortEntry);
    while (pPortEntry != NULL)
    {
        if (LaSynchronization == LA_TRUE)
        {
            LaMuxStateAttached (pPortEntry);
        }
        else
        {
            LaMuxStateDetached (pPortEntry);
        }
        pRefPortEntry = pPortEntry;
        pPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pRefPortEntry, &pPortEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGProcessDLAGPdu.                                */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      to processs D-LAG periodic-sync and event-update     */
/*                      PDU's                                                */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                      node on Distributing port.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGProcessDLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            LaSenderMac;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaBoolean          IsRemoteElector = LA_FALSE;
    tLaBufChainHeader  *pMsgBuf = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaPortList        *pLocalDistributePortList = NULL;
    UINT1              *pu1PktBuf = NULL;
    INT4                i4SllAction = LA_DLAG_CPY_REM_NODE_INFO;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    UINT4               u4Key = 0;
    UINT1               u1DSUInfoLength = 0;
    UINT1               u1DSUSubType = 0;
    BOOL1               bResult = OSIX_FALSE;

    UNUSED_PARAM (u1DSUInfoLength);

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                "LaDLAGProcessDLAGPdu: Linear Buffer "
                " Pointer is NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    /* Starting from DSU subtype */
    pu1PktBuf = pu1LinearBuf + LA_DLAG_DSU_SUBTYPE_OFFSET + 1;

    /* Copy DSU sub type */
    LA_GET_1BYTE (u1DSUSubType, pu1PktBuf);

    /* Copy DSU info length */
    LA_GET_1BYTE (u1DSUInfoLength, pu1PktBuf);

    /* Copy Port channel Key */
    LA_GET_4BYTE (u4Key, pu1PktBuf);

    /* Get the Aggregation Entry */
    if (LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGProcessDLAGPdu: Dropping DSU PDU Received");
        return LA_FAILURE;
    }
    /* Copy D-LAG PDU sender MAC */
    LA_MEMCPY (LaSenderMac, pu1PktBuf, LA_MAC_ADDRESS_SIZE);

    /* Added to drop the packets received from the same mac address */
    if (LA_MEMCMP (LaSenderMac, gLaGlobalInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE) == 0)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaDLAGProcessDLAGPdu: Dropping DSU PDU Received");
        return LA_SUCCESS;
    }
    /* If D-LAG status is disabled, drop the PDU received */
    if (pAggEntry->u1DLAGStatus == LA_DLAG_DISABLED)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaDLAGProcessDLAGPdu: Dropping DSU PDU Received");
        return LA_SUCCESS;
    }

    /* Allocating memory for the local port list */
    pLocalDistributePortList =
        (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

    /* If NULL, return */
    if (pLocalDistributePortList == NULL)
    {
        LA_TRC (ALL_FAILURE_TRC, "Error in Allocating memory for bitlist\n");
        return LA_SUCCESS;
    }

    /* Initializing the local distributing port list */
    MEMSET (pLocalDistributePortList, 0, sizeof (tLaPortList));

    /* Copying the DLAG distributing port list to a local port list */
    LA_MEMCPY (pLocalDistributePortList,
               pAggEntry->DLAGDistributingOperPortList, sizeof (tLaPortList));

    /* If the received port is not distributing port, drop the PDU received */
    OSIX_BITLIST_IS_BIT_SET (pAggEntry->DLAGDistributingOperPortList,
                             u2RecvdPortIndex, LA_PORT_LIST_SIZE, bResult);

    if (bResult == OSIX_TRUE)
    {
        /* Reset the local distributing port list for the received port index */
        OSIX_BITLIST_RESET_BIT ((*pLocalDistributePortList),
                                u2RecvdPortIndex, sizeof (tLaPortList));
    }
    else
    {
        FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
        LA_TRC (CONTROL_PLANE_TRC,
                "LaDLAGProcessDLAGPdu: Dropping DSU PDU Received");
        return LA_SUCCESS;
    }

    /* Scan the local distributing port list and forward the D-LAG PDU 
     * on the remaining ports in the port list*/
    if (pAggEntry->u4DLAGDistributingPortListCount != 0)
    {
        while (u4Count < (pAggEntry->u4DLAGDistributingPortListCount - 1))
        {
            OSIX_BITLIST_IS_BIT_SET ((*pLocalDistributePortList),
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            /* Allocate memory for CRU buffer */
            if ((pMsgBuf = LA_ALLOC_CRU_BUF (LA_LACPDU_SIZE, 0)) == NULL)
            {
                LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "LaDLAGTxDSU: Buffer Allocation failed\n");
                FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
                return LA_FAILURE;
            }
            /* Copy from Linear buffer to CRU buffer */
            LA_COPY_OVER_CRU_BUF (pMsgBuf, pu1LinearBuf, 0,
                                  LA_SLOW_PROTOCOL_SIZE);

            /* Forward the D-LAG PDU */
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaDLAGTxDSU: Handing over LACPDU to Lower Layer...\n");
            LaHandOverOutFrame (pMsgBuf, (UINT2) u4DLAGDistributingIfIndex,
                                NULL);
            pAggEntry->u4DLAGPeriodicSyncPduTxCount++;

            u4DLAGDistributingIfIndex++;
            u4Count++;
        }
    }

    /* D-LAG Periodic sync PDU's, Just Update the Remote Node List
     * reset the keep alive counters maintained for remote Nodes */
    if (u1DSUSubType == 0)
    {
        pAggEntry->u4DLAGPeriodicSyncPduRxCount++;
        LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);
        FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
        return LA_SUCCESS;
    }

    /* ---------------------------------------------------------------------- */
    /*            D-LAG Node ID Comparison: For Selecting Elector             */
    /* ---------------------------------------------------------------------- */
    /* On Receiving Certain Event update messages, if Redundancy feature      */
    /* is enabled then, D-LAG Node ID comparison method is applied to decide  */
    /* on the Elector, (i.e the Node among the D-LAG nodes which can possibly */
    /* do Master Slave Election as a wise man), Selection of Elector is made  */
    /* based on the D-LAG Node Source MAC which uniquely identifies a         */
    /* D-LAG Node, D-LAG Node with the least MAC address will be selected     */
    /* as the Elector.                                                        */
    /* ---------------------------------------------------------------------- */

    /* D-LAG Event-update PDU's */
    switch (u1DSUSubType)
    {
            /* High priority event update message */
        case LA_DLAG_HIGH_PRIO_EVENT_PO_UP:
            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            /* Node ID Comparison */
            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 LaSenderMac,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_NEW_NODE);

                /* Send PO_UP Acknowledge Message */
                LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP_ACK;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
            else
            {
                /* D-LAG, with Redundancy OFF */
                LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_PO_UP_ACK:

            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_RED_SWITCHED_OFF:

            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                /* Search Remote D-LAG Node Info List */
                LaDLAGGetRemoteAggEntryBasedOnMac (LaSenderMac, pAggEntry,
                                                   &pRemoteAggEntry);

                if (pRemoteAggEntry != NULL)
                {
                    IsRemoteElector =
                        (tLaBoolean) pRemoteAggEntry->u1DLAGElector;
                }

                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 LaSenderMac,
                                                 IsRemoteElector,
                                                 LA_DLAG_MS_TRIGGER_ON_DEL_NODE);

                LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_PO_DOWN:
            i4SllAction = LA_DLAG_DEL_REM_NODE_INFO;

            /* Search Remote D-LAG Node Info List */
            LaDLAGGetRemoteAggEntryBasedOnMac (LaSenderMac, pAggEntry,
                                               &pRemoteAggEntry);

            if (pRemoteAggEntry != NULL)
            {
                IsRemoteElector = (tLaBoolean) pRemoteAggEntry->u1DLAGElector;
            }

            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 LaSenderMac,
                                                 IsRemoteElector,
                                                 LA_DLAG_MS_TRIGGER_ON_DEL_NODE);

                LaDLAGTxInfo.u1DSUType = LA_DLAG_HIGH_PRIO_EVENT_PO_UP;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_INTF_UP:
            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 LaSenderMac,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);
            }
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN:

            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);
            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 LaSenderMac,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);
            }
            break;

        case LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG:    /* Fall through */
            i4SllAction = LA_DLAG_DEL_REM_PORT_INFO;
        case LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE:

            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_PORT_DETATCH_FROM_AGG:
            i4SllAction = LA_DLAG_DEL_REM_PORT_INFO;
            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 LaSenderMac,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);
            }
            break;

        case LA_DLAG_LOW_PRIO_EVENT_RES_SYNC:
            i4SllAction = LA_DLAG_EVENT_RES_SYNC_INFO;
            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
            {
                /* Apply Election Results */
                pu1PktBuf =
                    pu1LinearBuf + LA_DLAG_DSU_SUBTYPE_OFFSET +
                    LA_DLAG_MS_SELECTION_RESULT_OFFSET;
                LaDLAGApplyElectionResults (pAggEntry, pu1PktBuf);

                LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_RES_SYNC_ACK;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
            break;

        case LA_DLAG_LOW_PRIO_EVENT_RES_SYNC_ACK:
            LaDLAGUpdateRemoteDLAGNodeList (pAggEntry, pu1PktBuf, i4SllAction);

            if ((pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON) &&
                (pAggEntry->bLaDLAGIsDesignatedElector == LA_TRUE))
            {
                if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
                {
                    LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);
                }
                else
                {
                    /* change in role and hence port bundle state */
                    LaDLAGInitForcefulLacpTx (pAggEntry, LA_FALSE);
                }
                LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
            break;

        default:
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaDLAGProcessDLAGPdu: Invalid D-LAG Event update PDU"
                    " Received dropping.\n");
            FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
            return LA_FAILURE;
    }

    /* D-LAG valid Event update message */
    pAggEntry->u4DLAGEventUpdatePduRxCount++;
    FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGELectorSelectionProcess                        */
/*                                                                           */
/* Description        : This function is called on MS Selection timer expiry */
/*                      to processs Perform Elector selection                */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
VOID
LaDLAGElectorSelectionProcess (tLaLacAggEntry * pAggEntry)
{

    tLaDLAGRemoteAggEntry RemAggEntry;
    tLaDLAGRemoteAggEntry *pRemAggEntry = NULL;
    INT4                i4RetVal = LA_INIT_VAL;
    UINT1               u1PrevRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;

    LA_MEMSET (&RemAggEntry, 0, sizeof (tLaDLAGRemoteAggEntry));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "LaDLAGDeleteAllRemoteAggEntries: " "NULL POINTER.\n");
        return;
    }
    u1PrevRolePlayed = pAggEntry->u1DLAGRolePlayed;
    pAggEntry->bLaDLAGIsDesignatedElector = LA_TRUE;

    /* Get the first remote Aggregation Entry */
    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pRemAggEntry);
    while (pRemAggEntry != NULL)
    {
        if (pRemAggEntry->u1RedEnabled != LA_DLAG_REDUNDANCY_ON)
        {
            LA_MEMCPY (&RemAggEntry, pRemAggEntry,
                       sizeof (tLaDLAGRemoteAggEntry));
            pRemAggEntry = NULL;
            LaDLAGGetNextRemoteAggEntry (pAggEntry, &RemAggEntry,
                                         &pRemAggEntry);
            /* Nodes which are not enabled with Redundancy
             * Should not be considered in the Master Slave Selection */
            continue;
        }

        i4RetVal = LA_MEMCMP (gLaGlobalInfo.LaSystem.SystemMacAddr,
                              pRemAggEntry->DLAGRemoteSystem.
                              SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        if (i4RetVal > 0)
        {
            pAggEntry->bLaDLAGIsDesignatedElector = LA_FALSE;
            break;
        }

        LA_MEMCPY (&RemAggEntry, pRemAggEntry, sizeof (tLaDLAGRemoteAggEntry));
        pRemAggEntry = NULL;
        LaDLAGGetNextRemoteAggEntry (pAggEntry, &RemAggEntry, &pRemAggEntry);
    }

    if (pAggEntry->bLaDLAGIsDesignatedElector == LA_TRUE)
    {
        pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
        LA_MEMCPY (&(pAggEntry->LaDLAGElector),
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
        LaDLAGMSSelectionAlgorithm (pAggEntry, u1PrevRolePlayed);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaDLAGMSSelectionAlgorithm.                          */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      to processs Perform Master Slave selection algorithm */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaDLAGMSSelectionAlgorithm (tLaLacAggEntry * pAggEntry, UINT1 u1PrevRolePlayed)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaDLAGRemoteAggEntry RemoteAggEntry;
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pRemoteSlaveAggEntry = NULL;
    UINT4               u4SysPortInSyncCount = 0;
    UINT4               u4RemInSyncPortCount = 0;
    INT4                i4RetVal = 0;
    UINT2               u2SlavePriority = LA_INIT_VAL;
    UINT1               u1RolePlayed = pAggEntry->u1DLAGRolePlayed;
    BOOL1               b1IsRemoteSlave = LA_FALSE;
    BOOL1               b1MSSelectionPerformed = LA_FALSE;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));
    LA_MEMSET (&RemoteAggEntry, 0, sizeof (tLaDLAGRemoteAggEntry));

    /* Copying the Local configuration as Slave */
    LA_MEMCPY (LaDLAGTxInfo.LaDLAGSlaveMacAddr,
               gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);
    u2SlavePriority = gLaGlobalInfo.LaSystem.u2SystemPriority;
    pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_SLAVE;

    /* Get the Remote Aggregator Entry */
    LaDLAGGetNextRemoteAggEntry (pAggEntry, pRemoteAggEntry, &pRemoteAggEntry);
    if (pRemoteAggEntry == NULL)
    {
        pAggEntry->u1DLAGRolePlayed = u1RolePlayed;
        if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);
            pAggEntry->u4DLAGElectedAsMasterCount++;
            if (u1PrevRolePlayed != pAggEntry->u1DLAGRolePlayed)
                LaNotifyL2Vpn (pAggEntry, LA_AC_OPER_UP);

        }
        else
        {
            LaDLAGInitForcefulLacpTx (pAggEntry, LA_FALSE);
            pAggEntry->u4DLAGElectedAsSlaveCount++;
            if (u1PrevRolePlayed != pAggEntry->u1DLAGRolePlayed)
                LaNotifyL2Vpn (pAggEntry, LA_AC_OPER_DOWN);
        }
        return LA_SUCCESS;
    }
    while (pRemoteAggEntry != NULL)
    {
        if (pRemoteAggEntry->u1RedEnabled != LA_DLAG_REDUNDANCY_ON)
        {
            LA_MEMCPY (&RemoteAggEntry, pRemoteAggEntry,
                       sizeof (tLaDLAGRemoteAggEntry));
            LaDLAGGetNextRemoteAggEntry (pAggEntry, &RemoteAggEntry,
                                         &pRemoteAggEntry);
            /* Nodes which are not enabled with Redundancy
             * Should not be considered in the Master Slave Selection */
            continue;
        }
        b1MSSelectionPerformed = LA_TRUE;

        /*
         * MASTER SLAVE SELECTION ALGORITHM:-
         * --------------------------------
         * Master/slave selection will happen based on the following rules
         *
         * Rule 1:-
         * Always node with the minimum numerical value for system-priority
         * will be elected as master and if both the nodes are having minimum
         * numerical value for system-priority as equal then selection of master
         * will be done based on the rule 2 stated below.
         *
         * Rule 2:-
         * Always node with the maximum number ports having synchronization
         * status as IN_SYNC will be elected as Master and if both the nodes are
         * having max number of ports with synchronization status IN_SYNC as equal
         * then selection of master will be done based on the rule 3 stated below.
         *
         * Rule 3:-
         * Always node with the lesser node ID will be elected as master.
         * Since all the nodes participating in distribution must have unique
         * system ID Rule 3 will always yield positive result and breaks the
         * tie between competing nodes, So Rule 3 will act as final tie breaker
         * in case of Rule 1 and Rule 2 is not sufficient enough to make decision
         * of selecting master. 
         */

        if (u2SlavePriority <
            pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority)
        {
            pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
            LA_MEMCPY (LaDLAGTxInfo.LaDLAGSlaveMacAddr,
                       pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
            u2SlavePriority =
                pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority;
            b1IsRemoteSlave = LA_TRUE;
            pRemoteSlaveAggEntry = pRemoteAggEntry;
        }
        else if (u2SlavePriority ==
                 pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority)
        {
            if (b1IsRemoteSlave == LA_FALSE)
            {
                LaDLAGGetSysInSyncPortCount (pAggEntry, &u4SysPortInSyncCount);
            }
            else
            {
                LaDLAGGetRemSysInSyncPortCount (pRemoteSlaveAggEntry,
                                                &u4SysPortInSyncCount);
            }

            LaDLAGGetRemSysInSyncPortCount (pRemoteAggEntry,
                                            &u4RemInSyncPortCount);

            if (u4SysPortInSyncCount > u4RemInSyncPortCount)
            {
                pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
                LA_MEMCPY (LaDLAGTxInfo.LaDLAGSlaveMacAddr,
                           pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                           LA_MAC_ADDRESS_SIZE);
                u2SlavePriority =
                    pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority;
                b1IsRemoteSlave = LA_TRUE;
                pRemoteSlaveAggEntry = pRemoteAggEntry;
            }
            else if (u4SysPortInSyncCount == u4RemInSyncPortCount)
            {
                i4RetVal = LA_MEMCMP (LaDLAGTxInfo.LaDLAGSlaveMacAddr,
                                      pRemoteAggEntry->DLAGRemoteSystem.
                                      SystemMacAddr, LA_MAC_ADDRESS_SIZE);
                if (i4RetVal < 0)
                {
                    pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
                    LA_MEMCPY (LaDLAGTxInfo.LaDLAGSlaveMacAddr,
                               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
                               LA_MAC_ADDRESS_SIZE);
                    u2SlavePriority =
                        pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority;
                    b1IsRemoteSlave = LA_TRUE;
                    pRemoteSlaveAggEntry = pRemoteAggEntry;
                }
                else
                {
                    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "LaDLAGMSSelectionAlgorithm Source Mac of two D-LAG "
                            "Nodes cannot be same.\n");
                    return LA_FAILURE;
                }
            }
        }

        LA_MEMCPY (&RemoteAggEntry, pRemoteAggEntry,
                   sizeof (tLaDLAGRemoteAggEntry));
        LaDLAGGetNextRemoteAggEntry (pAggEntry, &RemoteAggEntry,
                                     &pRemoteAggEntry);
    }

    /* No Node Participates in MS Selection Algorithm
     * Current Node is the One and Only Master */
    if (b1MSSelectionPerformed != LA_TRUE)
    {
        pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
        LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);
    }

    if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
    {
        pAggEntry->u4DLAGElectedAsMasterCount++;
        /* If the previous role played is slave and the
         * new role played is master, send TRAP notification */
        if (u1PrevRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)
        {
            LaDLAGRoleChangeNotify (pAggEntry->u2AggIndex,
                                    LA_TRAP_DLAG_SLAVE_TO_MASTER);
            LaNotifyL2Vpn (pAggEntry, LA_AC_OPER_UP);
            pAggEntry->u4DLAGTrapTxCount++;
        }
    }
    else
    {
        pAggEntry->u4DLAGElectedAsSlaveCount++;
        /* If the previous role played is master and the
         * new role played is slave, send TRAP notification */
        if (u1PrevRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            LaDLAGRoleChangeNotify (pAggEntry->u2AggIndex,
                                    LA_TRAP_DLAG_MASTER_TO_SLAVE);
            LaNotifyL2Vpn (pAggEntry, LA_AC_OPER_DOWN);
            pAggEntry->u4DLAGTrapTxCount++;
        }
    }

    LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_RES_SYNC;
    LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetSysInSyncPortCount.                         */
/*                                                                           */
/* Description        : This function is called to get the number of ports   */
/*                      in sync in an Aggregator.                            */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel entry.                   */
/*                                                                           */
/* Output(s)          : u4SysPortInSyncCount - pointer to the variable in    */
/*                                             which calculated count of     */
/*                                             ports In-sync in aggregator   */
/*                                             should be stored.             */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGGetSysInSyncPortCount (tLaLacAggEntry * pAggEntry,
                             UINT4 *u4SysPortInSyncCount)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4Count = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "NULL POINTER\r\n");
        return LA_ERR_NULL_PTR;
    }

    if (pAggEntry->u1AggAdminStatus == LA_ENABLED)
    {
        /* Copy Port List info */
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            if (pPortEntry->u1PortOperStatus == CFA_IF_UP)
            {
                u4Count++;
            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
    *u4SysPortInSyncCount = u4Count;
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGGetRemSysInSyncPortCount.                      */
/*                                                                           */
/* Description        : This function is called to get the number of ports   */
/*                      in sync in a remote Aggregator entry stored.         */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry    - Pointer to stored remote        */
/*                                           Port-channel entry.             */
/*                                                                           */
/* Output(s)          : u4RemInSyncPortCount - pointer to the variable in    */
/*                                             which calculated count of     */
/*                                             ports In-sync in remote       */
/*                                             aggregator should be stored.  */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGGetRemSysInSyncPortCount (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                UINT4 *u4RemInSyncPortCount)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    UINT4               u4Count = 0;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Get the first remote Aggregation Entry */
    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        if (pRemotePortEntry->u1SyncState == CFA_IF_UP)
        {
            u4Count++;
        }
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }

    *u4RemInSyncPortCount = u4Count;
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGApplyElectionResults.                          */
/*                                                                           */
/* Description        : This function is called to apply the master slave    */
/*                      selection results sent from the elector.             */
/*                      This function is called on receiving low priority    */
/*                      results-sync event update pdu.                       */
/*                                                                           */
/* Called from        : LaDLAGProcessDLAGPdu () function.                    */
/* Input(s)           : pAggEntry  - Pointer to Aggregator in which results  */
/*                                   of master slave selection needs to be   */
/*                                   applied.                                */
/*                      pu1LinearBuf - Pointer to the received DSU and       */
/*                                     points to the Master Mac address in   */
/*                                     field (results).                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaDLAGApplyElectionResults (tLaLacAggEntry * pAggEntry, UINT1 *pu1LinearBuf)
{
    tMacAddr            LaDLAGElectorMacAddr;
    tMacAddr            LaDLAGSlaveMacAddr;
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1PrevRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    /* Read Slave Mac Address */
    LA_MEMCPY (LaDLAGSlaveMacAddr, pu1PktBuf, LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Read Elector Mac Address */
    LA_MEMCPY (LaDLAGElectorMacAddr, pu1PktBuf, LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    if ((LA_MEMCMP (gLaGlobalInfo.LaSystem.SystemMacAddr,
                    LaDLAGSlaveMacAddr, LA_MAC_ADDRESS_SIZE)) == 0)
    {
        u1PrevRolePlayed = pAggEntry->u1DLAGRolePlayed;
        pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_SLAVE;
        LaDLAGInitForcefulLacpTx (pAggEntry, LA_FALSE);
        pAggEntry->u4DLAGElectedAsSlaveCount++;
        /* If the previous role played is master and the 
         * new role played is slave, send TRAP notification */
        if (u1PrevRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            LaDLAGRoleChangeNotify (pAggEntry->u2AggIndex,
                                    LA_TRAP_DLAG_MASTER_TO_SLAVE);
            LaNotifyL2Vpn (pAggEntry, LA_AC_OPER_DOWN);

            pAggEntry->u4DLAGTrapTxCount++;
        }
    }
    else
    {
        /* In this release Ports connected to Slave will be
         * down-in-bundle, in the next release when support for
         * more than 2 D-LAG nodes are added ports connected to
         * Slave will be up-in-bundle */
        u1PrevRolePlayed = pAggEntry->u1DLAGRolePlayed;
        pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
        LaDLAGInitForcefulLacpTx (pAggEntry, LA_TRUE);
        pAggEntry->u4DLAGElectedAsMasterCount++;
        /* If the previous role played is slave and the 
         * new role played is master, send TRAP notification */
        if (u1PrevRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)
        {
            LaDLAGRoleChangeNotify (pAggEntry->u2AggIndex,
                                    LA_TRAP_DLAG_SLAVE_TO_MASTER);
            LaNotifyL2Vpn (pAggEntry, LA_AC_OPER_UP);
            pAggEntry->u4DLAGTrapTxCount++;
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGScanLaListAndInitDLAG.                         */
/*                                                                           */
/* Description        : This function is called to scan the aggregator list  */
/*                      and initialize the D-LAG functionality if D-LAG      */
/*                      status is enabled.                                   */
/*                                                                           */
/* Called From        :  LaEnable ()                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaDLAGScanLaListAndInitDLAG (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
        {
            LaDLAGInit (pAggEntry);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
}

/*****************************************************************************/
/* Function Name      : LaDLAGScanLaListAndDeInitDLAG.                       */
/*                                                                           */
/* Description        : This function is called to scan the aggregator list  */
/*                      and De-initialize the D-LAG functionality if D-LAG   */
/*                      status is enabled.                                   */
/*                                                                           */
/* Called From        :  LaDisable ()                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaDLAGScanLaListAndDeInitDLAG (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    /* If D-LAG status is enabled in Any of the port channel 
     * then D-LAG functionality should be stopped. */
    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
        {
            LaDLAGDeInit (pAggEntry);
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
}

/*****************************************************************************/
/* Function Name      : LaDLAGTriggerMSSelectionProcess.                     */
/*                                                                           */
/* Description        : This function is called to start the master slave    */
/*                      selection algorithm directly if Master slave         */
/*                      selection timer is zero, else if not zero then timer */
/*                      is started Master slave selection algorithm will be  */
/*                      called inside LaTimerHandler, on expiry of this      */
/*                      timer.                                               */
/*                                                                           */
/* Input(s)           : pAggEntry  - Pointer to Port-channel entry.          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaDLAGTriggerMSSelectionProcess (tLaLacAggEntry * pAggEntry,
                                 tMacAddr PeerMacAddr,
                                 tLaBoolean IsRemoteElector, UINT1 u1Trigger)
{
    INT4                i4RetVal = 0;
    UINT1               u1PrevRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    switch (u1Trigger)
    {
        case LA_DLAG_MS_TRIGGER_ON_NEW_NODE:
            /* If Timer is running, Then Node is waiting to select the elector
             * So update the Database and send Ack Message 
             * If Timer is not running, Elector alone has to process new nodes
             * So Other nodes Send Ack Message Alone */
            if (pAggEntry->DLAGMSSelectionWaitTmr.LaTmrFlag == LA_TMR_STOPPED)
            {
                if (pAggEntry->bLaDLAGIsDesignatedElector == LA_TRUE)
                {
                    /* If Timer is not running, Current node is the elector,
                     * Check the MAC address of the new node 
                     * If New Node has lower Mac Address, Reset the Elector bit
                     * and Send Ack Message alone 
                     * If Current Node has lower Mac Address, Restart 
                     * the MasterSlave Algo Timer to Perform the Master Slave Algorithm*/

                    i4RetVal = LA_MEMCMP (PeerMacAddr, pAggEntry->LaDLAGElector,
                                          LA_MAC_ADDRESS_SIZE);
                    if (i4RetVal < 0)
                    {
                        pAggEntry->bLaDLAGIsDesignatedElector = LA_FALSE;
                    }
                    else if ((i4RetVal > 0)
                             && (pAggEntry->bLaDLAGIsDesignatedElector ==
                                 LA_TRUE))
                    {
                        /* I Am Elector */
                        if ((LaStartTimer
                             (&(pAggEntry->DLAGMSSelectionWaitTmr))) !=
                            LA_SUCCESS)
                        {
                            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                    "TMR: LaStartTimer failed for Master"
                                    " Slave selection wait timer.\n");
                            return LA_FAILURE;
                        }
                    }

                }
            }
            break;

        case LA_DLAG_MS_TRIGGER_ON_DEL_NODE:
            if ((pAggEntry->bLaDLAGIsDesignatedElector == LA_TRUE) ||
                (IsRemoteElector == LA_TRUE))
            {
                if (pAggEntry->DLAGMSSelectionWaitTmr.LaTmrFlag ==
                    LA_TMR_STOPPED)
                {
                    if ((LaStartTimer (&(pAggEntry->DLAGMSSelectionWaitTmr))) !=
                        LA_SUCCESS)
                    {
                        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                "TMR: LaStartTimer failed for Master"
                                " Slave selection wait timer.\n");
                        return LA_FAILURE;
                    }
                }
            }
            break;

        case LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE:

            if ((pAggEntry->bLaDLAGIsDesignatedElector == LA_TRUE) &&
                (pAggEntry->DLAGMSSelectionWaitTmr.LaTmrFlag == LA_TMR_STOPPED))
            {
                u1PrevRolePlayed = pAggEntry->u1DLAGRolePlayed;
                pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_MASTER;
                LaDLAGMSSelectionAlgorithm (pAggEntry, u1PrevRolePlayed);
            }
            break;

        case LA_DLAG_MS_TRIGGER_ON_DIST_INTF_DOWN:
            if (pAggEntry->DLAGMSSelectionWaitTmr.LaTmrFlag == LA_TMR_STOPPED)
            {
                if ((LaStartTimer
                     (&(pAggEntry->DLAGMSSelectionWaitTmr))) != LA_SUCCESS)
                {
                    LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                            "TMR: LaStartTimer failed for Master"
                            " Slave selection wait timer.\n");
                    return LA_FAILURE;
                }
            }
            break;

        default:
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaDLAGTriggerMSSelectionProcess: Invalid D-LAG MS Trigger"
                    " - Ignored.\n");
            return LA_FAILURE;
    }

    return LA_SUCCESS;
}
