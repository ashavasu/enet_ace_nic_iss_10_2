/*****************************************************************************/
/* Copyright (C) 2014 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/* $Id: ladlagaa.c,v 1.11 2017/11/08 13:19:13 siva Exp $                                                     */
/*                                                                           */
/* License Aricent Inc., 2001-2014                                           */
/*****************************************************************************/
/*    FILE  NAME            : ladlagaa.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Distributed Link Aggregation                   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 31 Mar 2014                                    */
/*    AUTHOR                : Active-Active DLAG team                        */
/*    DESCRIPTION           : This file contains function definitions added  */
/*                            for supporting Distributed Link Aggregation    */
/*                            Multi chassi Link Aggregation.                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 Mar 2014/          Initial Create.                          */
/*            DISS Team                                                      */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/
/* Function Name      : LaActiveDLAGUpdateRemoteDLAGNodeList.                */
/*                                                                           */
/* Description        : This function is called by the Active Active D-LAG   */
/*                      Rx Message Handler function to update the remote     */
/*                      aggregator info list in Master node.                 */
/*                                                                           */
/* Input(s)           : RemoteSysMac                                         */
/*                                  - Remote System Mac address              */
/*                      u2RemoteSysPriority                                  */
/*                                  - Remote System Prioirty.                */
/*                      pu1LinearBuf                                         */
/*                                  - D-LAG Data Received from Remote D-LAG  */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node  */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaActiveDLAGUpdateRemoteDLAGNodeList (tLaMacAddr RemoteSysMac,
                                      UINT2 u2RemoteSysPriority,
                                      UINT1 *pu1LinearBuf, INT4 i4SllAction)
{
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4PrevMtu = 0;
    UINT4               u4PrevSpeed = 0;
    UINT4               u4PrevHighSpeed = 0;
    UINT4               u4Key = 0;
    UINT2               u2Val = 0;
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1Val = 0;

    LA_MEMSET (&CfaIfInfo, 0, sizeof (CfaIfInfo));
    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGUpdateRemoteDLAGNodeList: NULL POINTER"
                "/EMPTY BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    /* Copy Port channel Key */
    LA_GET_4BYTE (u4Key, pu1PktBuf);

    /* Get the Aggregation Entry */
    LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry);

    if (pAggEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "LaActiveDLAGUpdateRemoteDLAGNodeList: Port-Channel %d not exist",
                     u4Key);
        return LA_ERR_NULL_PTR;
    }

    /* Search Remote D-LAG Node Info List if this is Master */
    LaDLAGGetRemoteAggEntryBasedOnMac (RemoteSysMac, pAggEntry,
                                       &pRemoteAggEntry);

    /* port channel removal indication received from remote node */

    if (i4SllAction == LA_DLAG_DEL_REM_NODE_INFO)
    {
        if (pRemoteAggEntry == NULL)
        {
            return LA_SUCCESS;
        }

        LA_TRC (CONTROL_PLANE_TRC,
                "\n LaActiveDLAGUpdateRemoteDLAGNodeList: Remote Node is Deleted \n");
        LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);

        /* Call the consolidation logic as this node is deleted */
        LaActiveDLAGChkForBestConsInfo (pAggEntry);

        LaActiveDLAGSortOnStandbyConstraints (pAggEntry);

        return LA_SUCCESS;
    }

    if (pRemoteAggEntry == NULL)
    {
        /* If Remote D-LAG node Entry is not found 
         * - Allocate Memory for Remote D-LAG Aggregator
         * - Initialize with Default Values */

        if (LaActiveDLAGValidateSourceMacAddress ((UINT1 *) RemoteSysMac) !=
            LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGProcessDSU:ERROR: Remote MAC is not authorized.\n");
            return LA_FAILURE;
        }

        LA_TRC (CONTROL_PLANE_TRC,
                ("LaActiveDLAGValidateSourceMacAddress:: remote mac is authorised \r\n"));

        if ((LaDLAGCreateRemoteAggregator (RemoteSysMac, &pRemoteAggEntry))
            != LA_SUCCESS)
        {
            return LA_FAILURE;
        }

        /* This is used whether a full Update PDU is sent or
           only update PDU can be sent */

        if (LaDLAGAddToRemoteAggTable (pAggEntry, pRemoteAggEntry) ==
            LA_FAILURE)
        {
            LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);
            LA_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "Failure in adding Remote D-LAG Node entry to given aggregator remote D-LAG info table\n");
            return LA_FAILURE;
        }

        pRemoteAggEntry->pAggEntry = pAggEntry;

        LaGetSlotIdFromMacAddress (RemoteSysMac, &(LaDLAGTxInfo.u4SlotId));
        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE;
        LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
    }

    /* Update Remote D-LAG Node System Priority */
    pRemoteAggEntry->DLAGRemoteSystem.u2SystemPriority = u2RemoteSysPriority;

    /* Reset the Remote D-LAG Node Keep Alive Count
     * as We have received a Valid PDU */
    pRemoteAggEntry->u4DLAGKeepAliveCount = 0;

    /* Initialize update required as LA_FALE */
    pRemoteAggEntry->u1UpdateRequired = LA_FALSE;

    /* Extract MTU from PDU */
    LA_GET_2BYTE (u2Val, pu1PktBuf);

    u4PrevMtu = pRemoteAggEntry->u4Mtu;
    pRemoteAggEntry->u4Mtu = (UINT4) u2Val;

    if (u4PrevMtu != pRemoteAggEntry->u4Mtu)
    {
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;
    }

    /* Extract Speed from PDU */

    LA_GET_1BYTE (u1Val, pu1PktBuf);
    LaActiveDLAGGetPortSpeedFromPdu (u1Val, &CfaIfInfo);

    /* Reference port speed in which ports are verified for 
       aggregation */
    u4PrevSpeed = pRemoteAggEntry->u4PortSpeed;

    pRemoteAggEntry->u4PortSpeed = CfaIfInfo.u4IfSpeed;

    if (u4PrevSpeed != pRemoteAggEntry->u4PortSpeed)
    {
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;

    }

    /* Reference port high speed in which ports are verified for 
       aggregation */
    u4PrevHighSpeed = pRemoteAggEntry->u4PortHighSpeed;
    pRemoteAggEntry->u4PortHighSpeed = CfaIfInfo.u4IfHighSpeed;

    if (u4PrevHighSpeed != pRemoteAggEntry->u4PortHighSpeed)
    {
        pRemoteAggEntry->u1UpdateRequired = LA_TRUE;
    }

    pu1PktBuf += LA_AA_DLAG_DSU_RESERVED_FIELD1_LEN;

    /* Update the Remote Port List Info */
    LaActiveDLAGUpdateRemotePortList (pRemoteAggEntry, pu1PktBuf, i4SllAction);

    /* RBTree addtion and consolidation done here */
    LaActiveDLAGUpdateConsolidatedList (pRemoteAggEntry, i4SllAction);

    /* Added for HA support */
#ifdef L2RED_WANTED
    /*This function sends trigger message to standby 
       to add D-LAG remote aggregator entry  */
    LaDLAGRedSendAddRemoteAggorPortEntry (pAggEntry);
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGUpdateRemotePortList.                    */
/*                                                                           */
/* Description        : This function is called to update the DLAG           */
/*                      remote port info list.                               */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote aggregator entry whose      */
/*                                        remote port info list needs to be  */
/*                                        updated.                           */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                                     node on Distributing port.            */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node/ */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaActiveDLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                  UINT1 *pu1LinearBuf, INT4 i4SllAction)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4RemotePortIndex = 0;
    UINT4               u4LocalPortIndex = 0;
    UINT2               u2PrevPriority = LA_INIT_VAL;
    UINT2               u2Val = 0;
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1Val = 0;
    UINT1               u1NoOfRemotePortsEntry = 0;
    UINT1               u1PrevSyncState = LA_INIT_VAL;
    UINT1               u1PrevBundleState = LA_INIT_VAL;
    UINT1               u1Count = 0;

    pu1PktBuf = pu1LinearBuf;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }
    if ((pRemoteAggEntry->pAggEntry == NULL) || (pu1LinearBuf == NULL))
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pRemoteAggEntry->pAggEntry;

    /* Copy remote port info length */
    LA_GET_1BYTE (u1NoOfRemotePortsEntry, pu1PktBuf);

    if (i4SllAction == LA_DLAG_DEL_REM_PORT_INFO)
    {
        /* Copy First port Index */
        LA_GET_4BYTE (u4RemotePortIndex, pu1PktBuf);
        /* Search Remote D-LAG Node Info List */
        LaDLAGGetRemotePortEntry ((UINT2) u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            return LA_SUCCESS;
        }

        LaDLAGDeleteRemotePort (pRemoteAggEntry, pRemotePortEntry);

        return LA_SUCCESS;
    }

    for (u1Count = 0; u1Count < u1NoOfRemotePortsEntry; u1Count++)
    {

        /* Copy First port Index */
        LA_GET_4BYTE (u4RemotePortIndex, pu1PktBuf);

        /* Search Remote D-LAG Node Info List */
        LaDLAGGetRemotePortEntry (u4RemotePortIndex, pRemoteAggEntry,
                                  &pRemotePortEntry);
        if (pRemotePortEntry == NULL)
        {
            /* If Remote Port Entry is not found 
             * - Allocate Memory for Remote Port Entry 
             * - Initialize with Default Values */

            if ((LaDLAGCreateRemotePortEntry (u4RemotePortIndex,
                                              &pRemotePortEntry)) != LA_SUCCESS)
            {
                return LA_FAILURE;
            }

            LaDLAGAddToRemotePortTable (pRemoteAggEntry, pRemotePortEntry);

            /* Delete port from HL */
            LaActiveDLAGGetIfIndexFromBridgeIndex (u4RemotePortIndex,
                                                   &u4LocalPortIndex);

            LaGetPortEntry ((UINT2) u4LocalPortIndex, &pPortEntry);

            if (pPortEntry != NULL)
            {
                pPortEntry->pAggEntry = pAggEntry;
                LaHlDeletePhysicalPort (pPortEntry);
            }
        }

        /* Remote Port Bundle State */
        LA_GET_1BYTE (u1Val, pu1PktBuf);

        /* Initialize with LA_FALSE. If any changes this is set as LA_TRUE
           So that Consolidation logic is used */

        pRemotePortEntry->u1UpdateRequired = LA_FALSE;

        u1PrevBundleState = pRemotePortEntry->u1BundleState;
        pRemotePortEntry->u1BundleState = u1Val;

        if (u1PrevBundleState != u1Val)
        {
            pRemotePortEntry->u1UpdateRequired = LA_TRUE;
        }

        /* Remote Port Sync State */
        LA_GET_1BYTE (u1Val, pu1PktBuf);

        u1PrevSyncState = pRemotePortEntry->u1SyncState;
        pRemotePortEntry->u1SyncState = u1Val;

        if (u1PrevSyncState != u1Val)
        {
            pRemotePortEntry->u1UpdateRequired = LA_TRUE;

        }

        u2PrevPriority = pRemotePortEntry->u2Priority;

        /* If the priority value of the port is changed, the
         * consolidation logic to be repeated to identiy the 
         * active port list of the port channel
         */

        LA_GET_2BYTE (u2Val, pu1PktBuf);
        pRemotePortEntry->u2Priority = u2Val;
        if (u2PrevPriority != u2Val)
        {
            pRemotePortEntry->u1UpdateRequired = LA_TRUE;
            LaActiveDLAGGetPortEntry (u4RemotePortIndex, &pConsPortEntry,
                                      pAggEntry);
            if (pConsPortEntry == NULL)
            {
                continue;
            }
            /*Removing from consolidation Tree to  change Priority 
             * else Consolidation tree will get corrupted */
            if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
            {
                LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                         pConsPortEntry);

                pConsPortEntry->u2Priority = pRemotePortEntry->u2Priority;
                /*Read the Port Entry to consolidation RBTree */
                LaActiveDLAGAddToConsPortListTree (pAggEntry, pConsPortEntry);
            }
            else
            {
                pConsPortEntry->u2Priority = pRemotePortEntry->u2Priority;
            }
        }

    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGFormDSU.                                 */
/*                                                                           */
/* Description        : This function is called to form a DLAG LACPDU        */
/*                      for periodic sync message or for event update message*/
/*                      from Slave nodes                                     */
/*                                                                           */
/* Input(s)           : pAggEntry - Port channel which is trying to send     */
/*                                  D-LAG PDU.                               */
/*                      pu1LinearBuf - Pointer to the linear OUT buffer.     */
/*                      pLaDLAGTxInfo -                                      */
/*                               NULL - Periodic-sync PDU                    */
/*                               info to be embedded PDU - Event-update      */
/*                                                         D-LAG pdu.        */
/*                     pu4DataLength - Pointer to the length of  formed PDU  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaActiveDLAGFormDSU (tLaLacAggEntry * pAggEntry, UINT1 **ppu1LinearBuf,
                     tLaDLAGTxInfo * pLaDLAGTxInfo, UINT4 *pu4DataLength)
{
    tLaLacPortEntry    *pRefPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    tIssPortCtrlSpeed   IssPortCtrlSpeed = 0;
    tCfaIfInfo          IfInfo;
    UINT4               u4Val = 0;
    UINT4               u4Count = 0;
    UINT4               u4DataPadLen = 0;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT2               u2Val = 0;
    UINT1               u1BundleState = 0;
    UINT1               u1Val = 0;
    UINT1               u1PortCount = 0;
    UINT1               u1PortChannelCount = 0;
    UINT1               u1DSUType = 0;
    UINT1              *pu1PortInfoLenField = NULL;
    UINT1              *pu1NoOfAggInfoLenField = NULL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               au1SwitchMacAddr[LA_MAC_ADDRESS_SIZE] = { 0 };
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bIsSyncPdu = LA_FALSE;

    if ((pAggEntry == NULL) && (pLaDLAGTxInfo != NULL))
    {
        /* For Sync PDU pAggentry and pLaDLAGTxInfo will be 
         * passed as NULL. Return NULL ptr only if Event PDU is called
         * with pAggEntry as NULL */
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGFormDSU: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pu1BpduStart = pu1BpduEnd = gLaGlobalInfo.gau1DLAGPdu;

    /* Copy Destination Address */
    LA_MEMCPY (pu1BpduEnd, &gLaSlowProtAddr, LA_MAC_ADDRESS_SIZE);
    pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    /* Get Source Address from Distributing port from CFA */

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        while (u4Count <
               LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount)
        {
            OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                     GlobalDLAGDistributingOperPortList,
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }

            u4Count++;
            if (LaCfaGetIfInfo (u4DLAGDistributingIfIndex, &IfInfo) !=
                CFA_SUCCESS)
            {
                u4DLAGDistributingIfIndex++;
                continue;
            }
            /* distributing port is valid */
            break;
        }
        /* Copy Source Address */
        LA_MEMCPY (pu1BpduEnd, IfInfo.au1MacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;
        LA_MEMCPY (au1SwitchMacAddr, gLaGlobalInfo.LaSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);
    }
    else
    {
        /* Fill the switch Mac as source Mac */
        LaActiveDLAGGetSwitchMacAddress (au1SwitchMacAddr);
        LA_MEMCPY (pu1BpduEnd, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    }

    /* Copy Protocol Type : SLOW */
    u2Val = (UINT2) LA_SLOW_PROT_TYPE;
    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

    /* Copy Protocol Sub Type : LACP */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACPDU_SUB_TYPE);

    /* Copy Version Number */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACP_VERSION);

    /* Fill TLV Type = DSU */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_DLAG_DSU_INFO);

    /* If Null then Periodic Sync PDU, set DSUType to zero, If Not then
     * Event Update PDU copy the DSUType from pLaDLAGTxInfo to PDU */

    if (pLaDLAGTxInfo != NULL)
    {
        u1DSUType = pLaDLAGTxInfo->u1DSUType;
    }
    else
    {
        bIsSyncPdu = LA_TRUE;
    }

    /* Fill DSU Sub type */
    LA_PUT_1BYTE (pu1BpduEnd, u1DSUType);

    /* Fill DSU Info Lenght : [From DSU subtype - System Prioirty] */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_DSU_INFO_LEN);

    /* Copy System ID : source mac */
    LA_MEMCPY (pu1BpduEnd, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE);
    pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    /* Copy System Priority */
    u2Val = gLaGlobalInfo.LaSystem.u2SystemPriority;
    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

    /* Mark the No Of port-channel info filed
     * incase of Sync PDU which will be filled 
     * latter when scanning all the port-channels.
     */
    if (bIsSyncPdu == LA_TRUE)
    {

        LaGetNextAggEntry (NULL, &pAggEntry);
    }

    pu1NoOfAggInfoLenField = pu1BpduEnd;
    /* Increment the field */
    pu1BpduEnd += 1;

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    do
    {
        /* Event PDU/Sync PDU */
        if ((pAggEntry->u1ConfigPortCount == 0) && (bIsSyncPdu == LA_TRUE))
        {
            LaGetNextAggEntry (pAggEntry, &pAggEntry);
            continue;
        }

        u1PortChannelCount++;

        /* Copy Port Channel Key */
        u4Val = (UINT4) pAggEntry->AggConfigEntry.u2ActorAdminKey;
        LA_PUT_4BYTE (pu1BpduEnd, u4Val);

        /* Fill the MTU of port-channel */
        u2Val = (UINT2) pAggEntry->u4Mtu;
        LA_PUT_2BYTE (pu1BpduEnd, u2Val);

        /* Fill the reference speed of port-channel */
        LaActiveDLAGFillPortSpeedInPdu (pAggEntry, &IssPortCtrlSpeed);

        u1Val = (UINT1) IssPortCtrlSpeed;
        LA_PUT_1BYTE (pu1BpduEnd, u1Val);

        /* Skip Reserved Field */
        pu1BpduEnd += LA_AA_DLAG_DSU_RESERVED_FIELD1_LEN;

        /* Marker Pointer for Number of Remote Port Info field 
         * This field is filled at the end of this function */
        pu1PortInfoLenField = pu1BpduEnd;
        pu1BpduEnd += 1;

        /* If Tx Event-update message to be sent is
         * for Port detatched from Aggregator then
         * Only detached port index will be sent */
        if ((u1DSUType == LA_DLAG_HIGH_PRIO_EVENT_PORT_DETATCH_FROM_AGG) ||
            (u1DSUType == LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG))
        {
            u1PortCount++;
            u4Val = 0;
            LaActiveDLAGGetBridgeIndexFromIfIndex (pLaDLAGTxInfo->u4PortIndex,
                                                   &u4Val);
            LA_PUT_4BYTE (pu1BpduEnd, u4Val);
        }
        else
        {
            /* Can be a Database Update Type. */
            /* Copy Port List info */
            pRefPortEntry = pNextPortEntry = NULL;
            LaGetNextAggPortEntry (pAggEntry, pRefPortEntry, &pNextPortEntry);
            while (pNextPortEntry != NULL)
            {
                u1PortCount++;
                /* Copy Port Info */
                u4Val = 0;
                LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pNextPortEntry->
                                                       u2PortIndex, &u4Val);
                LA_PUT_4BYTE (pu1BpduEnd, u4Val);

                u4Val = (UINT4) pNextPortEntry->u2PortIndex;
                if ((LaGetPortBundleState (u4Val, &u1BundleState)) !=
                    LA_SUCCESS)
                {
                    return LA_FAILURE;
                }

                LA_PUT_1BYTE (pu1BpduEnd, u1BundleState);

                CfaGetIfOperStatus ((UINT4) pNextPortEntry->u2PortIndex,
                                    &u1Val);

                LA_PUT_1BYTE (pu1BpduEnd, u1Val);
                /* Fill Priority */
                u2Val = pNextPortEntry->LaLacActorInfo.u2IfPriority;
                LA_PUT_2BYTE (pu1BpduEnd, u2Val);

                pRefPortEntry = pNextPortEntry;
                pNextPortEntry = NULL;
                LaGetNextAggPortEntry (pAggEntry, pRefPortEntry,
                                       &pNextPortEntry);
            }
        }

        LA_PUT_1BYTE (pu1PortInfoLenField, u1PortCount);
        /* Reset port counter */
        u1PortCount = 0;
        LaGetNextAggEntry (pAggEntry, &pAggEntry);

    }
    while ((pAggEntry != NULL) && (bIsSyncPdu == LA_TRUE));

    LA_PUT_1BYTE (pu1NoOfAggInfoLenField, u1PortChannelCount);

    /* Put 4 byte as FCS */
    u4Val = 0;
    LA_PUT_4BYTE (pu1BpduEnd, u4Val);

    *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

    if (*pu4DataLength < LA_LACPDU_SIZE)
    {
        u4DataPadLen = LA_LACPDU_SIZE - (*pu4DataLength);
        LA_MEMSET (pu1BpduEnd, LA_INIT_VAL, u4DataPadLen);
        pu1BpduEnd = pu1BpduEnd + u4DataPadLen;
        *pu4DataLength = LA_LACPDU_SIZE;
    }

    *ppu1LinearBuf = gLaGlobalInfo.gau1DLAGPdu;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGProcessDLAGPdu.                          */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      to processs Active-Active D-LAG periodic-sync        */
/*                      and event-update PDU's                               */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel receiving D-LAG PDU.     */
/*                                                                           */
/*                      pu1LinearBuf - D-LAG Data Received from Remote D-LAG */
/*                      node on Distributing port.                           */
/*                                                                           */
/*                      u4ByteCount - Length of the pu1LinearBuf.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/

INT4
LaActiveDLAGProcessDLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf,
                            UINT4 u4ByteCount)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            LaSenderMac = { 0 };
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaBufChainHeader  *pMsgBuf = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaPortList        *pLocalDistributePortList = NULL;
    tLaMacAddr          RemoteMacAddr = { 0 };
    UINT1               au1SwitchMacAddr[LA_MAC_ADDRESS_SIZE] = { 0 };
    INT4                i4SllAction = LA_DLAG_CPY_REM_NODE_INFO;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    UINT4               u4Key = 0;
    UINT2               u2RemotePriority = 0;
    UINT1               u1DSUInfoLength = 0;
    UINT1               u1DSUSubType = 0;
    UINT1               u1NoOfPortChannel = 0;
    UINT1              *pu1PktBuf = NULL;
    BOOL1               bResult = OSIX_FALSE;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (BUFFER_TRC, "LaActiveDLAGProcessDLAGPdu: Linear Buffer "
                " Pointer is NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "LaActiveDLAGProcessDLAGPdu: DLAG PDU processing...");

    /* Starting from DSU subtype */
    pu1PktBuf = pu1LinearBuf + LA_DLAG_DSU_SUBTYPE_OFFSET + 1;

    /* Copy DSU sub type */
    LA_GET_1BYTE (u1DSUSubType, pu1PktBuf);

    /* Copy DSU info length */
    LA_GET_1BYTE (u1DSUInfoLength, pu1PktBuf);

    if ((u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE) ||
        (u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE))
    {

        if (u1DSUInfoLength != LA_AA_DLAG_ADD_DEL_INFO_LEN)
        {
            return LA_FAILURE;

        }
    }
    else
    {
        if (u1DSUInfoLength != LA_AA_DLAG_DSU_INFO_LEN)
        {
            return LA_FAILURE;

        }
        /* Copy D-LAG PDU sender MAC */
        LA_MEMCPY (LaSenderMac, pu1PktBuf, LA_MAC_ADDRESS_SIZE);

        if (LA_USE_DIST_PORT_OR_NOT
            (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) ==
            LA_TRUE)
        {
            LA_MEMCPY (au1SwitchMacAddr, gLaGlobalInfo.LaSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
        }
        else
        {
            LaActiveDLAGGetSwitchMacAddress (au1SwitchMacAddr);
        }
        /* Added to drop the packets received from the same mac address */
        if (LA_MEMCMP (LaSenderMac, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE) == 0)
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGProcessDLAGPdu: Dropping DSU PDU Received. Pdu received from Same System");
            return LA_SUCCESS;
        }
    }

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        /* Allocating memory for the local port list */
        pLocalDistributePortList =
            (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

        /* If NULL, return */
        if (pLocalDistributePortList == NULL)
        {
            LA_TRC (ALL_FAILURE_TRC,
                    "Error in Allocating memory for bitlist\n");
            return LA_SUCCESS;
        }

        /* Initializing the local distributing port list */
        MEMSET (pLocalDistributePortList, 0, sizeof (tLaPortList));

        /* Copying the DLAG distributing port list to a local port list */
        LA_MEMCPY (pLocalDistributePortList,
                   LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingOperPortList,
                   sizeof (tLaPortList));

        /* If the received port is not distributing port, drop the PDU received */
        OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                 GlobalDLAGDistributingOperPortList,
                                 u2RecvdPortIndex, LA_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            /* Reset the local distributing port list for the received port index */
            OSIX_BITLIST_RESET_BIT ((*pLocalDistributePortList),
                                    u2RecvdPortIndex, sizeof (tLaPortList));
        }
        else
        {
            FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGProcessDLAGPdu: Dropping DSU PDU Received. Pdu received on non distribute"
                    "port\n");
            return LA_SUCCESS;
        }

        /* Scan the local distributing port list and forward the D-LAG PDU 
         * on the remaining ports in the port list*/

        while (u4Count <
               (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount - 1))
        {
            OSIX_BITLIST_IS_BIT_SET ((*pLocalDistributePortList),
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            if ((pMsgBuf = LA_ALLOC_CRU_BUF (u4ByteCount, 0)) == NULL)
            {
                LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "LaActiveDLAGProcessDLAGPdu: Buffer Allocation failed\n");
                FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);
                return LA_FAILURE;
            }
            /* Copy from Linear buffer to CRU buffer */
            LA_COPY_OVER_CRU_BUF (pMsgBuf, pu1LinearBuf, 0, u4ByteCount);

            /* Forward the D-LAG PDU */
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGProcessDLAGPdu: Handing over LACPDU to Lower Layer...\n");
            LaHandOverOutFrame (pMsgBuf, (UINT2) u4DLAGDistributingIfIndex,
                                NULL);

            u4DLAGDistributingIfIndex++;
            u4Count++;
        }

        FsUtilReleaseBitList ((UINT1 *) pLocalDistributePortList);

    }

    if (u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE)
    {
        /* Copy Port channel No's */
        LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

        LA_GET_4BYTE (u4Key, pu1PktBuf);
        /* Get the Aggregation Entry */
        LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry);
        if ((pAggEntry == NULL) || (u1NoOfPortChannel != 1))
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "LaActiveDLAGProcessDLAGPdu: Dropping DSU PDU Received. Port-Channel =%d not exist \n",
                         u4Key);
            return LA_FAILURE;
        }

        if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)
        {
            /* Process Add-Del PDU and program H/w */
            LaActiveDLAGGetAddDeletePortFromDSU (pAggEntry, pu1PktBuf,
                                                 LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE);
        }

        pAggEntry->u4DLAGEventUpdatePduRxCount++;
        return LA_SUCCESS;
    }

    if (u1DSUSubType == LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        LaActiveDLAGProcessPeriodicUpdateDSU (pu1PktBuf);
        return LA_SUCCESS;
    }

    if (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_SLAVE)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGProcessDSU: System Role is Slave. Dropping Sync/Event PDU \n");
        return LA_FAILURE;
    }

    /* Get the Remote System MAC and Priority */

    LA_MEMCPY (RemoteMacAddr, pu1PktBuf, LA_MAC_ADDRESS_SIZE);

    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    LA_GET_2BYTE (u2RemotePriority, pu1PktBuf);

    /* D-LAG Periodic sync PDU's, Just Update the Remote Node List
     * reset the keep alive counters maintained for remote Nodes */

    if (u1DSUSubType == 0)
    {
        /* If it is a Sync PDU we need to loop for the whole PDU. 
         */
        LaActiveDLAGProcessSyncPDU (RemoteMacAddr, u2RemotePriority, pu1PktBuf,
                                    i4SllAction);

        return LA_SUCCESS;
    }

    /*  
     *  In case of Event PDU's get the Key here itself. Then keep the pointer to the *
     *  same place where it was there. So that LaActiveDLAGUpdateRemoteDLAGNodeList  *
     *  will take start taking the Key from that place.
     */

    /* Copy Port channel No's */
    LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

    /* Copy Port channel Key */
    LA_GET_4BYTE (u4Key, pu1PktBuf);

    /* Get the Aggregation Entry */
    LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry);

    if ((pAggEntry == NULL) || (u1NoOfPortChannel != 1))
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "LaActiveDLAGProcessDLAGPdu: Dropping DSU PDU Received. Port-Channel =%d not exist \n",
                     u4Key);
        return LA_ERR_NULL_PTR;
    }

    /* Move back the pointer to 4 byte */

    pu1PktBuf -= 4;

    /* D-LAG Event-update PDU's */
    switch (u1DSUSubType)
    {
            /* High priority event update message */
        case LA_DLAG_HIGH_PRIO_EVENT_PO_UP:

            LaActiveDLAGUpdateRemoteDLAGNodeList (RemoteMacAddr,
                                                  u2RemotePriority, pu1PktBuf,
                                                  i4SllAction);
            break;

        case LA_DLAG_HIGH_PRIO_EVENT_PO_DOWN:

            i4SllAction = LA_DLAG_DEL_REM_NODE_INFO;

            /* Search Remote D-LAG Node Info List */
            LaDLAGGetRemoteAggEntryBasedOnMac (RemoteMacAddr, pAggEntry,
                                               &pRemoteAggEntry);

            LaActiveDLAGUpdateRemoteDLAGNodeList (RemoteMacAddr,
                                                  u2RemotePriority, pu1PktBuf,
                                                  i4SllAction);
            break;

        case LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG:    /* Fall through */
            i4SllAction = LA_DLAG_DEL_REM_PORT_INFO;
        case LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE:

            LaActiveDLAGUpdateRemoteDLAGNodeList (RemoteMacAddr,
                                                  u2RemotePriority, pu1PktBuf,
                                                  i4SllAction);
            break;

        default:
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveDLAGProcessDLAGPdu: Invalid D-LAG Event update PDU"
                    " Received dropping.\n");
            return LA_FAILURE;
    }

    /* D-LAG valid Event update message */
    pAggEntry->u4DLAGEventUpdatePduRxCount++;

    return LA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    FUNCTION NAME    : LaActiveDLAGFillPortSpeedInPdu                     */
/*                                                                          */
/*    DESCRIPTION      : This function is used to fill the Speed of         */
/*                       Port in Active DLAG PDU                            */
/*                                                                          */
/*    INPUT            : pAggEntry - Pointer to AggEntry,                   */
/*                       pIssPortCtrlSpeed - pointer to Speed variable      */
/*                                                                          */
/*    OUTPUT           : None                                               */
/*                                                                          */
/*    RETURNS          : Speed of Aggregator                                */
/*                                                                          */
/****************************************************************************/

VOID
LaActiveDLAGFillPortSpeedInPdu (tLaLacAggEntry * pAggEntry,
                                tIssPortCtrlSpeed * pIssPortCtrlSpeed)
{
    if ((pAggEntry == NULL) || (pIssPortCtrlSpeed == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGFillPortSpeedInPdu : NULL POINTER " "ENTRY\n");
        return;
    }
    if (pAggEntry->u4PortSpeed == CFA_ENET_SPEED_10M)
    {
        *pIssPortCtrlSpeed = ISS_10MBPS;
    }
    else if (pAggEntry->u4PortSpeed == CFA_ENET_SPEED_1G)
    {
        *pIssPortCtrlSpeed = ISS_1GB;
    }
    else if (pAggEntry->u4PortSpeed == CFA_ENET_SPEED_2500M)
    {
        *pIssPortCtrlSpeed = ISS_2500MBPS;
    }
    else if (pAggEntry->u4PortSpeed == CFA_ENET_SPEED_100M)
    {
        *pIssPortCtrlSpeed = ISS_100MBPS;
    }
    else if ((pAggEntry->u4PortSpeed == CFA_ENET_MAX_SPEED_VALUE) &&
             (pAggEntry->u4PortHighSpeed == CFA_ENET_HISPEED_10G))
    {
        *pIssPortCtrlSpeed = ISS_10GB;
    }
    else if ((pAggEntry->u4PortSpeed == CFA_ENET_MAX_SPEED_VALUE) &&
             (pAggEntry->u4PortHighSpeed == CFA_ENET_HISPEED_40G))
    {
        *pIssPortCtrlSpeed = ISS_40GB;
    }
    else if ((pAggEntry->u4PortSpeed == CFA_ENET_MAX_SPEED_VALUE) &&
             (pAggEntry->u4PortHighSpeed == CFA_ENET_HISPEED_56G))
    {
        *pIssPortCtrlSpeed = ISS_56GB;
    }
    else
    {
        /* Port Speed is 0 */
        *pIssPortCtrlSpeed = 0;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    FUNCTION NAME    : LaActiveDLAGGetPortSpeedFromPdu                     */
/*                                                                           */
/*    DESCRIPTION      : This function is used to extract the Speed of       */
/*                       Port from Active DLAG PDU.                          */
/*                                                                           */
/*    INPUT            : u1Speed - Speed value extracted from DLAG Pdu       */
/*                       pIfInfo - CFA structure to fill Speed.              */
/*                                                                           */
/*    OUTPUT           : IfInfo containg Speed & HighSpeed                   */
/*                                                                           */
/*    RETURNS          : None                                                */
/*                                                                           */
/****************************************************************************/

VOID
LaActiveDLAGGetPortSpeedFromPdu (UINT1 u1Speed, tCfaIfInfo * pIfInfo)
{
    switch (u1Speed)
    {
        case ISS_10MBPS:
            pIfInfo->u4IfSpeed = CFA_ENET_SPEED_10M;
            pIfInfo->u4IfHighSpeed = (pIfInfo->u4IfSpeed / CFA_1MB);
            break;
        case ISS_100MBPS:
            pIfInfo->u4IfSpeed = CFA_ENET_SPEED_100M;
            pIfInfo->u4IfHighSpeed = (pIfInfo->u4IfSpeed / CFA_1MB);
            break;
        case ISS_1GB:
            pIfInfo->u4IfSpeed = CFA_ENET_SPEED_1G;
            pIfInfo->u4IfHighSpeed = (pIfInfo->u4IfSpeed / CFA_1MB);
            break;
        case ISS_2500MBPS:
            pIfInfo->u4IfSpeed = CFA_ENET_SPEED_2500M;
            pIfInfo->u4IfHighSpeed = (pIfInfo->u4IfSpeed / CFA_1MB);
            break;
        case ISS_10GB:
            pIfInfo->u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            pIfInfo->u4IfHighSpeed = CFA_ENET_HISPEED_10G;
            break;
        case ISS_40GB:
            pIfInfo->u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            pIfInfo->u4IfHighSpeed = CFA_ENET_HISPEED_40G;
            break;
        case ISS_56GB:
            pIfInfo->u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            pIfInfo->u4IfHighSpeed = CFA_ENET_HISPEED_56G;
            break;
        default:
            pIfInfo->u4IfSpeed = 0;
            pIfInfo->u4IfHighSpeed = 0;
            break;
    }

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGFormAddDeleteDSU                         */
/*                                                                           */
/* Description        : This function is called to form a Add delete DLAG    */
/*                      PDU from the RBTrees. First Delete Ports are filled  */
/*                      followed by Add ports. After filling it in PDU       */
/*                      Recently changed flag is set to FALSE.               */
/*                                                                           */
/* Input(s)           : pAggEntry - Port channel which is to send            */
/*                                  D-LAG PDU.                               */
/*                      pu1LinearBuf - Pointer to the linear OUT buffer.     */
/*                      pLaDLAGTxInfo :                                      */
/*                               info to be embedded PDU - Add-Delete        */
/*                                                         D-LAG pdu.        */
/*                      pu4DataLength - Pointer to length of the formed PDU. */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGFormAddDeleteDSU (tLaLacAggEntry * pAggEntry, UINT1 **ppu1LinearBuf,
                              tLaDLAGTxInfo * pLaDLAGTxInfo,
                              UINT4 *pu4DataLength)
{
    tCfaIfInfo          IfInfo;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pDelPortEntry = NULL;
    UINT4               u4Val = 0;
    UINT4               u4Count = 0;
    UINT4               u4DataPadLen = 0;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT2               u2Val = 0;
    UINT1              *pu1TmpField = NULL;
    UINT1              *pu1NoOfAggInfoLenField = NULL;
    UINT1              *pu1BpduStart = NULL;
    UINT1              *pu1BpduEnd = NULL;
    UINT1               u1DSUType = 0;
    UINT1               u1AddPort = 0;
    UINT1               u1DeletePort = 0;
    UINT1               u1PortChannelCount = 0;
    UINT1               au1SwitchMacAddr[LA_MAC_ADDRESS_SIZE] = { 0 };
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bIsPeriodicUpdatePdu = LA_FALSE;

    if ((pAggEntry == NULL) && (pLaDLAGTxInfo == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGFormAddDeleteDSU: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "\n Entering LaActiveDLAGFormAddDeleteDSU \n");

    LA_MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pu1BpduStart = pu1BpduEnd = gLaGlobalInfo.gau1DLAGPdu;

    /* Copy Destination Address */
    LA_MEMCPY (pu1BpduEnd, &gLaSlowProtAddr, LA_MAC_ADDRESS_SIZE);
    pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    /* Get Source Address from Distributing port from CFA */

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        while (u4Count <
               LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount)
        {
            OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                     GlobalDLAGDistributingOperPortList,
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            u4Count++;

            if (LaCfaGetIfInfo (u4DLAGDistributingIfIndex, &IfInfo) !=
                CFA_SUCCESS)
            {
                u4DLAGDistributingIfIndex++;
                continue;
            }

            /* distributing port is valid */
            break;
        }
        /* Copy Source Address */
        LA_MEMCPY (pu1BpduEnd, IfInfo.au1MacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;
    }
    else
    {

        /* Fill the switch Mac as source Mac */
        LaActiveDLAGGetSwitchMacAddress (au1SwitchMacAddr);
        LA_MEMCPY (pu1BpduEnd, au1SwitchMacAddr, LA_MAC_ADDRESS_SIZE);
        pu1BpduEnd += LA_MAC_ADDRESS_SIZE;

    }

    /* Copy Protocol Type : SLOW */
    u2Val = (UINT2) LA_SLOW_PROT_TYPE;
    LA_PUT_2BYTE (pu1BpduEnd, u2Val);

    /* Copy Protocol Sub Type : LACP */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACPDU_SUB_TYPE);

    /* Copy Version Number */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LACP_VERSION);

    /* Fill TLV Type = DSU */
    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_DLAG_DSU_INFO);

    /* Fill ADD_DEL Type */
    if (pLaDLAGTxInfo->u1DSUType ==
        LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        u1DSUType = LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE;
        bIsPeriodicUpdatePdu = LA_TRUE;
    }
    else
    {
        u1DSUType = LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE;
    }

    LA_PUT_1BYTE (pu1BpduEnd, u1DSUType);

    LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_ADD_DEL_INFO_LEN);

    if (bIsPeriodicUpdatePdu == LA_TRUE)
    {

        LaGetNextAggEntry (NULL, &pAggEntry);
    }

    pu1NoOfAggInfoLenField = pu1BpduEnd;
    /* Increment the field */
    pu1BpduEnd += 1;

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    do
    {
        u1DeletePort = 0;
        u1AddPort = 0;

        if (bIsPeriodicUpdatePdu == LA_TRUE)
        {

            RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
                         &u4Count);

            if (u4Count == 0)
            {
                /* No ports are in that Agg Entry consolidated List
                   Skip it */
                LaGetNextAggEntry (pAggEntry, &pAggEntry);
                continue;
            }
        }

        u1PortChannelCount++;

        /* Copy Port Channel Key */
        u4Val = (UINT4) pAggEntry->AggConfigEntry.u2ActorAdminKey;
        LA_PUT_4BYTE (pu1BpduEnd, u4Val);

        /*  Update Delete PortList DSU Type */
        LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_DSU_DELETE_PORT_LIST);

        /* Temp ptr for storing port no field which is filled later */
        pu1TmpField = pu1BpduEnd;
        pu1BpduEnd += 1;

        if (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
        {

            LA_TRC (CONTROL_PLANE_TRC, "\n ADD_DEL_UPDATE_PDU: Delete ports:");

        }
        else if (pLaDLAGTxInfo->u1DSUType ==
                 LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE)
        {

            LA_TRC (CONTROL_PLANE_TRC,
                    "\n FULL_UPDATE_ON_NEW_NODE:Delete ports:");

        }

        /********************************************************************
         * The Event update PDU can be sent for
         * 1. recpetion of new node addition indication
         * 2. change in the existing port channel properties.
         *
         * For the first case (New node addition), all the port-channel Add
         * member ports can be sent in the PDU. This can be indentified by the macro
         * LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE
         *
         * In the second scenerio, only the updat port related information alone
         * can be sent. This can be indentified by the macro LA_AA_DLAG_ADD_DEL_UPDATE_ONLY
         *
         * IN the third scenerio, only Add port list info will be sent for all 
         * port-channel available which is periodic update and can be identified
         * by macro LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE.
         *
         **********************************************************************/

        if (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
        {
            LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
            while (pConsPortEntry != NULL)
            {
                if ((pConsPortEntry->u1PortStateFlag != LA_AA_DLAG_PORT_IN_ADD)
                    && (pConsPortEntry->u1RecentlyChanged == LA_TRUE))
                {
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d, ",
                                 pConsPortEntry->u4PortIndex);
                    u4Val = pConsPortEntry->u4PortIndex;
                    LA_PUT_4BYTE (pu1BpduEnd, u4Val);
                    /* ReSet the RecentChangedFlag */
                    pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                    /* Check if port is removed from peer port-channel 
                       Delete from Cons entry and release the memory allocated for it */

                    LaActiveDLAGMasterProcessAddDeleteLinks (pAggEntry,
                                                             pConsPortEntry->
                                                             u4PortIndex,
                                                             (UINT1)
                                                             LA_AA_DLAG_PORT_IN_DELETE);

                    if (pConsPortEntry->u1PortStateFlag ==
                        LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY)
                    {
                        pDelPortEntry = pConsPortEntry;
                        LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                                      &pConsPortEntry,
                                                      pAggEntry);
                        LaActiveDLAGRemovePortEntry (pAggEntry, pDelPortEntry);
                        LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                                 pDelPortEntry);
                        LaActiveDLAGDeleteConsPortEntry (pDelPortEntry);

                        pDelPortEntry = NULL;
                        u1DeletePort++;
                        continue;
                    }

                    u1DeletePort++;
                }

                LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                              pAggEntry);
            }
        }
        LA_TRC (CONTROL_PLANE_TRC, "\n");
        LA_PUT_1BYTE (pu1TmpField, (UINT1) u1DeletePort);

        /*  Update Add PortList DSU Type */
        LA_PUT_1BYTE (pu1BpduEnd, (UINT1) LA_AA_DLAG_DSU_ADD_PORT_LIST);
        /* Temp ptr for storing port no field which is filled later */
        pu1TmpField = pu1BpduEnd;
        pu1BpduEnd += 1;

        /* Loop for RBTrees */

        if (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)
        {
            LA_TRC (CONTROL_PLANE_TRC, "\n ADD_DEL_UPDATE_PDU: Add ports:");
        }
        else if (pLaDLAGTxInfo->u1DSUType ==
                 LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
        {
            LA_TRC (CONTROL_PLANE_TRC, "\n PERIODIC_UPDATE :Add ports:");
        }
        else
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "\n FULL_UPDATE_ON_NEW_NODE: Add ports:");
        }

        LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pConsPortEntry,
                                                  pAggEntry);
        while (pConsPortEntry != NULL)
        {
            if ((pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY) &&
                (pConsPortEntry->u1RecentlyChanged == LA_TRUE))
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d, ",
                             pConsPortEntry->u4PortIndex);
                u4Val = pConsPortEntry->u4PortIndex;
                LA_PUT_4BYTE (pu1BpduEnd, u4Val);
                /* ReSet the RecentChangedFlag */
                pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                LaActiveDLAGMasterProcessAddDeleteLinks (pAggEntry,
                                                         pConsPortEntry->
                                                         u4PortIndex,
                                                         (UINT1)
                                                         LA_AA_DLAG_PORT_IN_ADD);
                u1AddPort++;
            }
            /* Fill all the ports when Initial Update */
            if ((pLaDLAGTxInfo->u1DSUType ==
                 LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE)
                || (pLaDLAGTxInfo->u1DSUType ==
                    LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE))
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d, ",
                             pConsPortEntry->u4PortIndex);
                u4Val = pConsPortEntry->u4PortIndex;
                LA_PUT_4BYTE (pu1BpduEnd, u4Val);
                if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
                {
                    LaActiveDLAGMasterProcessAddDeleteLinks (pAggEntry,
                                                             pConsPortEntry->
                                                             u4PortIndex,
                                                             (UINT1)
                                                             LA_AA_DLAG_PORT_IN_ADD);
                }
                pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                u1AddPort++;
            }

            LaActiveDLAGGetNextPortEntryFromConsTree (pConsPortEntry,
                                                      &pConsPortEntry,
                                                      pAggEntry);
        }

        LA_PUT_1BYTE (pu1TmpField, (UINT1) u1AddPort);

        if ((u1AddPort == 0) && (u1DeletePort == 0)
            && (bIsPeriodicUpdatePdu == LA_FALSE))
        {
            /* If no ports are there no need to send Update 
               message */
            return LA_FAILURE;
        }

#ifdef NPAPI_WANTED
        /* Update the actual status of port-channel to h/w */
        LaUpdatePortChannelStatusToHw (pAggEntry->u2AggIndex, LA_PORT_DISABLED,
                                       LA_TRUE);
#endif
        LaGetNextAggEntry (pAggEntry, &pAggEntry);

    }
    while ((pAggEntry != NULL) && (bIsPeriodicUpdatePdu == LA_TRUE));

    /* Fill port-channel no's */
    LA_PUT_1BYTE (pu1NoOfAggInfoLenField, u1PortChannelCount);

    /* Put 4 byte as FCS */
    u4Val = 0;

    LA_PUT_4BYTE (pu1BpduEnd, u4Val);

    *pu4DataLength = (UINT4) (pu1BpduEnd - pu1BpduStart);

    if (*pu4DataLength < LA_LACPDU_SIZE)
    {
        u4DataPadLen = LA_LACPDU_SIZE - (*pu4DataLength);
        LA_MEMSET (pu1BpduEnd, LA_INIT_VAL, u4DataPadLen);
        pu1BpduEnd = pu1BpduEnd + u4DataPadLen;
        *pu4DataLength = LA_LACPDU_SIZE;
    }

    *ppu1LinearBuf = gLaGlobalInfo.gau1DLAGPdu;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  LaActiveDLAGGetAddDeletePortFromDSU                 */
/*                                                                           */
/* Description        :  This function is called to process the AddDelete    */
/*                       Active D-LAG PDU and subsequently calls the H/w     */
/*                       functions to program the ports.                     */
/*                                                                           */
/* Input(s)           : pAggEntry -  Aggregator entry for which the          */
/*                                    PDU received                           */
/*                      pu1PktBuf -  D-LAG Data Received from Remote D-LAG   */
/*                      node on Distributing port. with LA Header stripped.  */
/*                                                                           */
/*                      u1PduType - Type of PDU.Can be Update /Periodic PDU. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGGetAddDeletePortFromDSU (tLaLacAggEntry * pAggEntry,
                                     UINT1 *pu1PktBuf, UINT1 u1PduType)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pDelPortEntry = NULL;
    UINT4               u4PortIndex = 0;
    UINT4               u4PortBrgIndex = 0;
    UINT1               u1DSUAddSubType = 0;
    UINT1               u1DSUDeleteSubType = 0;
    UINT1               u1AddCount = 0;
    UINT1               u1DeleteCount = 0;
    INT4                i4RetStatus = 0;

#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = 0;
#endif

    if ((pAggEntry == NULL) || (pu1PktBuf == NULL))
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "\n LaActiveDLAGGetAddDeletePortFromDSU: Add/Del PDU received \n");

    /* Get Delete PortList field */
    LA_GET_1BYTE (u1DSUDeleteSubType, pu1PktBuf);

    if (u1DSUDeleteSubType != LA_AA_DLAG_DSU_DELETE_PORT_LIST)
    {
        return LA_FAILURE;
    }

    /* Get Delete Port Count */
    LA_GET_1BYTE (u1DeleteCount, pu1PktBuf);

    /* Delete Ports */
    while (u1DeleteCount != 0)
    {
        LA_GET_4BYTE (u4PortBrgIndex, pu1PktBuf);

        LaActiveDLAGGetIfIndexFromBridgeIndex (u4PortBrgIndex, &u4PortIndex);

        if (u4PortIndex != 0)
        {
            LaGetPortEntry ((UINT2) u4PortIndex, &pPortEntry);

            i4RetStatus = LaActiveDLAGCheckIsLocalPort ((UINT2) u4PortIndex);

            if ((i4RetStatus == LA_SUCCESS) &&
                (pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry) &&
                (pPortEntry->u1DLAGMasterAck == LA_TRUE))
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Master Removed Port =%d \n",
                             u4PortIndex);
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n Setting the MasterAck and Sync to FALSE \n");

                pPortEntry->u1DLAGMasterAck = LA_FALSE;
                /* Don't Set Peer Ready to FALSE */
                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_FALSE;

                /* Convey this info with Peer */
                pPortEntry->NttFlag = LA_TRUE;
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

                LaActiveDLAGHwControl (pPortEntry,
                                       LA_HW_EVENT_DISABLE_DISTRIBUTOR);

            }
        }

        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (u4PortBrgIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry != NULL)
        {
            LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
            LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);
        }

#ifdef NPAPI_WANTED
        if ((LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            && (u4PortBrgIndex != 0))
        {
            if (LaFsLaHwDlagRemoveLinkFromAggGroup (pAggEntry->u2AggIndex,
                                                    (UINT2) u4PortBrgIndex) !=
                FNP_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                             "FsLaHwDlagRemoveLinkFromAggGroup: Failed to "
                             "configure remote ports in H/w for: port-"
                             "channel: %u\n", pAggEntry->u2AggIndex);
            }
        }

        if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
        {
            /* Since Port is removed from H/w Change the Status */
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

            if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
            {
                LaHlCreatePhysicalPort (pPortEntry);

                /* Make the port status as Forwarding in H/w */
                LaUpdatePortChannelStatusToHw (pPortEntry->u2PortIndex, LA_TRUE,
                                               LA_STANDBY_INDICATION);
            }

        }
#else
        if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
        {
            pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

            if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
            {
                LaHlCreatePhysicalPort (pPortEntry);
            }
        }
#endif
        u1DeleteCount--;
    }

    /* Get Add PortList field */
    LA_GET_1BYTE (u1DSUAddSubType, pu1PktBuf);

    if (u1DSUAddSubType != LA_AA_DLAG_DSU_ADD_PORT_LIST)
    {
        return LA_FAILURE;
    }

    /* Get Add Port Count */
    LA_GET_1BYTE (u1AddCount, pu1PktBuf);

    /* Add Ports */
    while (u1AddCount != 0)
    {
        LA_GET_4BYTE (u4PortBrgIndex, pu1PktBuf);

        LaActiveDLAGGetIfIndexFromBridgeIndex (u4PortBrgIndex, &u4PortIndex);

        if (u4PortIndex != 0)
        {
            /* Currently all ports are programmed in H/w */
            LaGetPortEntry ((UINT2) u4PortIndex, &pPortEntry);

            i4RetStatus = LaActiveDLAGCheckIsLocalPort ((UINT2) u4PortIndex);

            if ((i4RetStatus == LA_SUCCESS) &&
                (pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry) &&
                (pPortEntry->u1DLAGMasterAck == LA_FALSE))
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "\n Master Acknowledged Port =%d \n", u4PortIndex);
                LA_TRC (CONTROL_PLANE_TRC,
                        "\n Setting the MasterAck and Sync to TRUE \n");
                pPortEntry->u1DLAGMasterAck = LA_TRUE;

                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_TRUE;

                pPortEntry->NttFlag = LA_TRUE;
                LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
                /* LaMuxStateAttached (pPortEntry); */
                LaActiveDLAGHwControl (pPortEntry,
                                       LA_HW_EVENT_ENABLE_DISTRIBUTOR);
            }
        }

        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (u4PortBrgIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Create Port Entry and Insert in PortList and Consolidated Tree */
            i4RetStatus = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                           u4PortBrgIndex, 0);

            if (i4RetStatus == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

            /*Adding the newly created Port to the PortList */
            LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);

#ifdef NPAPI_WANTED
            if ((LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                && (u4PortBrgIndex != 0))
            {
                if (LaFsLaHwDlagAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                                   (UINT2) u4PortBrgIndex,
                                                   &u2HwAggIndex) !=
                    FNP_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                                 "FsLaHwDlagAddLinkToAggGroup: Failed to "
                                 "configure remote ports in H/w for: port-"
                                 "channel: %u\n", pAggEntry->u2AggIndex);
                }
            }
#endif

        }

        /* Set the Recent change as TRUE */
        pConsPortEntry->u1RecentlyChanged = LA_TRUE;

        u1AddCount--;
    }

    if (u1PduType == LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        pConsPortEntry = NULL;
        LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                      &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
            {
                pConsPortEntry->u1RecentlyChanged = LA_FALSE;

            }
            else if (pConsPortEntry->u1RecentlyChanged == LA_FALSE)
            {
                pDelPortEntry = pConsPortEntry;

                /* Search Remote D-LAG Node Info List */
#ifdef NPAPI_WANTED
                if ((LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                    && (pConsPortEntry->u4PortIndex != 0))
                {
                    if (LaFsLaHwDlagRemoveLinkFromAggGroup
                        (pAggEntry->u2AggIndex,
                         (UINT2) pConsPortEntry->u4PortIndex) != FNP_SUCCESS)
                    {
                        LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                                     "LaHwDlagRemoveLinkFromAggGroup: Failed to "
                                     "configure remote ports in H/w for: port-"
                                     "channel: %u\n", pAggEntry->u2AggIndex);
                    }
                }

                if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
                {
                    /* Since Port is removed from H/w Change the Status */
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

                    if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
                    {
                        LaHlCreatePhysicalPort (pPortEntry);

                        /* Make the port status as Forwarding in H/w */
                        LaUpdatePortChannelStatusToHw (pPortEntry->u2PortIndex,
                                                       LA_TRUE,
                                                       LA_STANDBY_INDICATION);
                    }

                }
#else
                if ((i4RetStatus == LA_SUCCESS) && (pPortEntry != NULL))
                {
                    pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;

                    if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
                    {
                        LaHlCreatePhysicalPort (pPortEntry);
                    }
                }
#endif

                LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                              &pConsPortEntry, pAggEntry);
                LaActiveDLAGRemovePortEntry (pAggEntry, pDelPortEntry);
                LaActiveDLAGDeleteConsPortEntry (pDelPortEntry);
                pDelPortEntry = NULL;
                continue;

            }

            LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                          &pConsPortEntry, pAggEntry);

        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGUpdateConsolidatedList                   */
/*                                                                           */
/* Description        : This function will check whether RBTree update is    */
/*                      needed. If needed it will move the port entries to   */
/*                      correspoding RBTrees. It will call the consolidation */
/*                      logic and will check whether Update PDU is required  */
/*                      or not.                                              */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - RemoteAggEntry for which the Sync/ */
/*                                         Event PDU received                */
/*                      i4SllAction - If Remote port is deleted              */
/*                                    this will have DEL_REMOTE_PORT so that */
/*                                    consolidation explicitly takes place   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGUpdateConsolidatedList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                    INT4 i4SllAction)
{
    tLaDLAGSpeedPropInfo TmpProperties;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1SortingNeeded = LA_FALSE;

    LA_MEMSET (&TmpProperties, 0, sizeof (tLaDLAGSpeedPropInfo));

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC, "Remote Aggregation pointer is "
                "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    /* Get Aggregator entry from Remote */
    pAggEntry = pRemoteAggEntry->pAggEntry;

    if (pAggEntry == NULL)
    {
        LA_TRC (BUFFER_TRC, "Aggregation pointer is " "NULL/Buffer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }
    /* Check for Speed/MTU properties with Consolidated Info */

    TmpProperties.u4Mtu = pRemoteAggEntry->u4Mtu;
    TmpProperties.u4ConsRefSpeed = pRemoteAggEntry->u4PortSpeed;
    TmpProperties.u4ConsRefHighSpeed = pRemoteAggEntry->u4PortHighSpeed;
    LA_MEMCPY (TmpProperties.BestInfoMac,
               pRemoteAggEntry->DLAGRemoteSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    i4RetVal = LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry, TmpProperties);

    if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
    {
        LaActiveDLAGUpdateTreeOnPropSame (pRemoteAggEntry, &u1SortingNeeded);
    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
    {
        LaActiveDLAGUpdateTreeOnPropGreater (pRemoteAggEntry, &u1SortingNeeded);

    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
    {
        LaActiveDLAGUpdateTreeOnPropLesser (pRemoteAggEntry, &u1SortingNeeded);
    }
    else
    {
        LaActiveDLAGChkForBestConsInfo (pAggEntry);
        u1SortingNeeded = LA_TRUE;

    }

    if ((u1SortingNeeded == LA_TRUE)
        || (i4SllAction == LA_DLAG_DEL_REM_PORT_INFO))
    {
        /* If sorting needed call consolidation logic */
        LaActiveDLAGSortOnStandbyConstraints (pAggEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGSortOnStandbyConstraints                 */
/*                                                                           */
/* Description        : This function is used to sort the port entries       */
/*                      present in Add and Standby ports RBTree and          */
/*                      select the maximum ports allowed based on priority.  */
/*                                                                           */
/* Input(s)           : pAggEntry- Master Aggregator entry for which         */
/*                                 consolidation  neede                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/

INT4
LaActiveDLAGSortOnStandbyConstraints (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaDLAGConsPortEntry *pTmpConsPortEntry = NULL;
    tLaBoolean          bUpdateDSURequired = LA_FALSE;
    UINT4               u4Count = 0;
    UINT4               u4Index = 0;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (tLaDLAGTxInfo));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGSortOnStandbyConstraints: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    /* Get the number of ports in Add List Tree */
    LA_TRC (CONTROL_PLANE_TRC,
            "\n --------------------------------------------------------------------*\n\tConsolidated List");
    /* Add Ports which are in ADD and STANDBY state to Consolidation Tree 
     * To Sort it according to Priority */
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        if ((pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
            || (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_STANDBY))
        {
            /*Add Port Entry to Consolidation Tree */
            LaActiveDLAGAddToConsPortListTree (pAggEntry, pConsPortEntry);

            /* If Port is moved from STANDBY to ADD state Set the recently 
             * changed Flag */
            if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_STANDBY)
            {
                pConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }

            /* All Ports in Consolidation Tree should be in ADD state */
            pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
        }
        else
        {
            /* Port is down now .Remove from Consolidation tree */
            LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry, pConsPortEntry);
        }

        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    /* Get the Total number of Port Entry in Consolidation Logic */
    RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
                 &u4Count);

    /* If Total Number of Port Entry in consolidation is less or 
     * Equal to Maximum allowed Ports, then Consolidation is over*/
    if (u4Count < pAggEntry->u1MaxPortsToAttach)
    {
        if (gLaGlobalInfo.u4TraceOption != 0)
        {
            LaDLAGActivePrintAllTreePorts (pAggEntry);
        }

        bUpdateDSURequired = LaActiveDLAGChkAddDelDSURequired (pAggEntry);

        if (bUpdateDSURequired == LA_TRUE)
        {
            LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_ADD_DEL_UPDATE_ONLY;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }

        return LA_SUCCESS;
    }

    /* Port Entries in Consolidation Tree is more than Maximum allowed Ports,
     * Remove the extra Ports according to Prioirty*/
    pTmpConsPortEntry = NULL;
    LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pTmpConsPortEntry,
                                              pAggEntry);

    /* Scanning the Port Entry from consolidation Tree till Maximum 
     * allowed Ports, No need to do anything as they are already in ADD
     * state, remaining Ports needs to be moved to STANDY state */
    for (u4Index = 1; u4Index <= pAggEntry->u1MaxPortsToAttach; u4Index++)
    {
        /*GetNext Consolidation Entry */
        if (pTmpConsPortEntry != NULL)
        {
            LaActiveDLAGGetNextPortEntryFromConsTree (pTmpConsPortEntry,
                                                      &pTmpConsPortEntry,
                                                      pAggEntry);
        }
        else
        {
            LA_TRC (CONTROL_PLANE_TRC, "\n Consolidated Tree Corrupted\n");
            return LA_FAILURE;
        }
    }

    /* Moving Ports to Standby Tree and Removing from Conslidation Tree */
    for (u4Index = (UINT4) (pAggEntry->u1MaxPortsToAttach + 1);
         u4Index <= u4Count; u4Index++)
    {
        if (pTmpConsPortEntry != NULL)
        {
            /* The RecentlyChanged flag can be set when the port is moving from
             * Add to standby
             * standby to Add
             * After updating the information , the flag can be reset to FALSE
             */
            if (pTmpConsPortEntry->u1RecentlyChanged == LA_TRUE)
            {
                pTmpConsPortEntry->u1RecentlyChanged = LA_FALSE;
            }
            else
            {
                /* Incase Port is now standby previous it was in ADD state
                 * Set the Recent change as TRUE so that remove indication is sent */
                pTmpConsPortEntry->u1RecentlyChanged = LA_TRUE;
            }

            pTmpConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_STANDBY;
            LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry,
                                                     pTmpConsPortEntry);

            /*GetNext Consolidation Entry */
            LaActiveDLAGGetNextPortEntryFromConsTree (pTmpConsPortEntry,
                                                      &pTmpConsPortEntry,
                                                      pAggEntry);
        }
    }

    if (gLaGlobalInfo.u4TraceOption != 0)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "\n -----------------------------------------------------------------\n");
        LA_TRC (CONTROL_PLANE_TRC, "\n After consolidation Ports\n");

        LaDLAGActivePrintAllTreePorts (pAggEntry);
        LA_TRC (CONTROL_PLANE_TRC,
                "\n -----------------------------------------------------------------\n");
    }

    bUpdateDSURequired = LaActiveDLAGChkAddDelDSURequired (pAggEntry);

    if (bUpdateDSURequired == LA_TRUE)
    {
        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_ADD_DEL_UPDATE_ONLY;
        LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDLAGChkUpdateAggEntryConsInfo                      */
/*                                                                           */
/* Description        : This function is used to compare Best Info of the    */
/*                      received PDU (MTU, speed ) with the current          */
/*                      information and store the best information.          */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry which is having best    */
/*                                   consolidated info                       */
/*                                                                           */
/*                      RecvInfo - Compare info which is having speed,MTU */
/*                                    of port-channel                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_AA_DLAG_MTU_AND_SPEED_SAME -                      */
/*                          -  If MTU and speed same with Best Info          */
/*                      LA_AA_DLAG_MTU_OR_SPEED_GREATER -                    */
/*                          - If MTU or Speed greater with Best Info         */
/*                      LA_AA_DLAG_MTU_OR_SPEED_LESSER -                     */
/*                          - If MTU or Speed Lesser with Best Info          */
/*                      LA_FALSE- If Best Info going to be modified by       */
/*                                  previous best                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGChkUpdateAggEntryConsInfo (tLaLacAggEntry * pAggEntry,
                                       tLaDLAGSpeedPropInfo RecvInfo)
{

    /* Received properties are comapred with the existing properties
     * The interest for the comparision is speed and MTU
     * 
     * If both speed and MTU can be changed, then the best information
     * can be selected based on the speed
     *
     * if speed alone is changed, then the comparision is based on the changed 
     * speed information and MTU (if required), after this the best information 
     * can be selected 
     *
     * If MTU alone is changed, then the comparision is based on the 
     * changed MTU information and speed (if required), after that
     * the best information can be selected 
     */

    if (((RecvInfo.u4ConsRefSpeed ==
          pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed)
         && (RecvInfo.u4ConsRefHighSpeed ==
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed)
         && (RecvInfo.u4Mtu ==
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu)))
    {
        /* Examples:
           4) (100 mbps == 100 mbps) && (100 = 100 ) && (1500 == 1500) */

        return LA_AA_DLAG_MTU_AND_SPEED_SAME;

    }

    if ((RecvInfo.u4ConsRefSpeed >
         pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed) ||
        /* Speed equal or greater */
        ((RecvInfo.u4ConsRefSpeed ==
          pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed)
         && (RecvInfo.u4ConsRefHighSpeed >
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed))
        ||
        /* Ref speed's equal and ref high speed  check */
        ((RecvInfo.u4ConsRefSpeed ==
          pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed)
         && (RecvInfo.u4ConsRefHighSpeed ==
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed)
         && (RecvInfo.u4Mtu >
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu))
        /* Ref speed's equal and ref high speed's equal and check Mtu */
        )
    {
        /* Examples:
           1)  1Gb > 100 Mbps (or)
           2) (u4Max == u4Max) && (56 Gb > 10 Gb) (or)
           3) (u4Max == u4Max) && (10 Gb == 10 Gb) && (1600 > 1500) (or)
         */

        /* Compare Info is best info. Change the Consolidated information */
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
            RecvInfo.u4ConsRefSpeed;
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
            RecvInfo.u4ConsRefHighSpeed;
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu = RecvInfo.u4Mtu;
        LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.BestInfoMac,
                   RecvInfo.BestInfoMac, LA_MAC_ADDRESS_SIZE);

        LaActiveDLAGPrintBestConsInfo (pAggEntry);
        return LA_AA_DLAG_MTU_OR_SPEED_GREATER;

    }

    /* Compare Info is not changed and it is less *
       Examples: 
       1) 100Mbps < 10Gbps (or)
       2) u4Max == u4Max but 10000(Mbps) < 56000(Mbps)
       3) u4Max == u4Max and 10000(Mbps) == 10000(Mbps) but 1000 <1600 */

    if ((LA_MEMCMP
         (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.BestInfoMac,
          RecvInfo.BestInfoMac, LA_MAC_ADDRESS_SIZE) == 0)
        &&
        ((RecvInfo.u4ConsRefSpeed !=
          pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed)
         || (RecvInfo.u4ConsRefHighSpeed !=
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed)
         || (RecvInfo.u4Mtu !=
             pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu)))
    {

        /*This is a special case.
           Example: Initially all are same MTU. In between it is changed a greater value.
           It again changed to normanl value (or) lower value.
           1) 1500 && 1500 && 1500
           2) 1500 && 1600 && 1500
           3) 1500 && 1200 && 1500 [OR] 1500 && 1500 && 1500 
           It is handled separetly */
        return LA_AA_DLAG_MTU_RCVD_FROM_PRV_BEST;

    }

    /* Speed/MTU is less with Best Info */
    return LA_AA_DLAG_MTU_OR_SPEED_LESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGGetPortEntry                             */
/*                                                                           */
/* Description        : This function returns the pointer to Port Entry      */
/*                      from PortList  RBTree if exists.                     */
/*                                                                           */
/* Input(s)           : u4RemotPortIndex - Port Index of the port            */
/*                      ppConsPortEntry - Pointer to Cons port entry         */
/*                      pAggEntry -Aggregator entry in which RBTree exits    */
/*                                                                           */
/* Output(s)          : ppConsPortEntry - Pointer to the Cons port entry     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - if port entry is NULL                   */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGGetPortEntry (UINT4 u4RemotPortIndex,
                          tLaDLAGConsPortEntry ** ppConsPortEntry,
                          tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry ConsPortEntry;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGGetPortEntry"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&ConsPortEntry, 0, sizeof (tLaDLAGConsPortEntry));
    ConsPortEntry.u4PortIndex = u4RemotPortIndex;
    *ppConsPortEntry =
        RBTreeGet (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
                   (tRBElem *) & ConsPortEntry);

    if (*ppConsPortEntry != NULL)
    {
        return LA_SUCCESS;
    }
    *ppConsPortEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGCreateConsPortEntry                      */
/*                                                                           */
/* Description        : This function creates consolidated port entry for    */
/*                      a given port  by allocating memory for it.           */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port Index of the port                 */
/*                      ppConsPortEntry - Pointer to Cons port entry         */
/*                      u2Priority - Priority of port                        */
/*                                                                           */
/* Output(s)          : ppConsPortEntry - Pointer to the Cons port entry     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_MEM_FAILURE- if memory allocation fails       */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGCreateConsPortEntry (tLaDLAGConsPortEntry ** ppConsPortEntry,
                                 UINT4 u4PortIndex, UINT2 u2Priority)
{
    tLaDLAGConsPortEntry *pTmpConsPortEntry = NULL;

    if (LA_DLAGCONSPORTENTRY_ALLOC_MEM_BLOCK (pTmpConsPortEntry) == NULL)
    {
        LA_TRC (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                "LA_DLAGCONSPORTENTRY_ALLOC_MEM_BLOCK FAILED\r\n");
        return LA_FAILURE;

    }

    LA_MEMSET (pTmpConsPortEntry, 0, sizeof (tLaDLAGConsPortEntry));

    pTmpConsPortEntry->u4PortIndex = u4PortIndex;
    pTmpConsPortEntry->u2Priority = u2Priority;
    pTmpConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_NOT_IN_ANY_LIST;

    *ppConsPortEntry = pTmpConsPortEntry;
    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGDeleteConsPortEntry                      */
/*                                                                           */
/* Description        : This function Delets consolidated port entry for     */
/*                      a given port by Releasing memory for it.             */
/*                                                                           */
/* Input(s)           : ppConsPortEntry - Pointer to Cons port entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_MEM_FAILURE- if memory release fails          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGDeleteConsPortEntry (tLaDLAGConsPortEntry * pConsPortEntry)
{

    if (pConsPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGDeleteConsPortEntry: NULL POINTER/EMPTY "
                "BUFFER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    if (LA_DLAGCONSPORTENTRY_FREE_MEM_BLOCK (pConsPortEntry) != LA_MEM_SUCCESS)
    {
        LA_TRC (INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                "LA_DLAGCONSPORTENTRY_FREE_MEM_BLOCK FAILED\n");
        return LA_ERR_MEM_FAILURE;

    }
    pConsPortEntry = NULL;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGAddPortEntry                             */
/*                                                                           */
/* Description        : This function adds a consolidated port entry to      */
/*                      given aggregator Add RBTree.                         */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                      port entry that to added             */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGAddPortEntry (tLaLacAggEntry * pAggEntry,
                          tLaDLAGConsPortEntry * pConsPortEntry)
{

    if ((pAggEntry == NULL) || (pConsPortEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGAddPortEntry"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    if (RBTreeAdd
        (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
         pConsPortEntry) == RB_FAILURE)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "\n Adding the Port Entry Failed: Port=%d \n",
                     pConsPortEntry->u4PortIndex);
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGAddToConsPortListTree                    */
/*                                                                           */
/* Description        : This function adds a consolidated port entry to      */
/*                      given aggregator Consolidated RBTree.                */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                      port entry that to added             */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGAddToConsPortListTree (tLaLacAggEntry * pAggEntry,
                                   tLaDLAGConsPortEntry * pConsPortEntry)
{
    if ((pAggEntry == NULL) || (pConsPortEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGAddToConsPortListTree "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    if (RBTreeGet
        (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
         pConsPortEntry) != NULL)
    {
        return LA_SUCCESS;

    }

    if (RBTreeAdd
        (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
         pConsPortEntry) == RB_FAILURE)
    {
        LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                     "\n  Add to CONS LIST FAILURE: Port=%d Priority=%d\n",
                     pConsPortEntry->u4PortIndex, pConsPortEntry->u2Priority);
        return LA_FAILURE;

    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGRemovePortEntry                          */
/*                                                                           */
/* Description        : This function removes a Port entry from              */
/*                      given aggregator port list RBTree.                   */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                       port entry that to be removed       */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*****************************************************************************/

INT4
LaActiveDLAGRemovePortEntry (tLaLacAggEntry * pAggEntry,
                             tLaDLAGConsPortEntry * pConsPortEntry)
{
    if ((pAggEntry == NULL) || (pConsPortEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGRemovePortEntry"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    if (RBTreeRem
        (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
         (tRBElem *) pConsPortEntry) == 0)
    {
        LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                     "\nRemove PORTLIST FAILED Port No=%d Priority=%d \n",
                     pConsPortEntry->u4PortIndex, pConsPortEntry->u2Priority);
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : LaActiveDLAGGetNextPortEntry                           */
/*                                                                             */
/* Description        : This function returns the pointer to Next Port         */
/*                      Entry for the given port if Entry is Present           */
/*                      Else it returns NULL.                                  */
/*                      If This function is called with NULL it will get the   */
/*                      first port entry from PortTable                        */
/*                                                                             */
/* Input(s)           : pConsPortEntry- Pointer to the current port entry      */
/*                      ppNextConsPortEntry - Pointer to Next port entry       */
/*                      pAggEntry -Aggregator entry in which RBTree exits      */
/*                                                                             */
/* Output(s)          : ppNextConsPortEntry - Pointer to the Next port entry   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/* Global Variables                                                            */
/* Modified           : None.                                                  */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On success                                */
/*                      LA_FAILURE - if port entry is NULL                     */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*******************************************************************************/

INT4
LaActiveDLAGGetNextPortEntry (tLaDLAGConsPortEntry * pConsPortEntry,
                              tLaDLAGConsPortEntry **
                              ppNextConsPortEntry, tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pTmpConsPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGGetNextPortEntry"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    if (pConsPortEntry == NULL)
    {
        pTmpConsPortEntry = (tLaDLAGConsPortEntry *) RBTreeGetFirst
            (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList);
    }
    else
    {
        pTmpConsPortEntry = (tLaDLAGConsPortEntry *) RBTreeGetNext
            (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
             (tRBElem *) pConsPortEntry, NULL);
    }
    if (pTmpConsPortEntry != NULL)
    {
        *ppNextConsPortEntry = pTmpConsPortEntry;
        return LA_SUCCESS;
    }

    *ppNextConsPortEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaActiveMasterDLAGUpdateConsolidatedList             */
/*                                                                           */
/* Description        : This function will be called by Master if Master     */
/*                      has local port channel member ports.                 */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which Master ports   */
/*                                  participating in LAG are there           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGMasterUpdateConsolidatedList (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGSpeedPropInfo TmpProperties;
    INT4                i4RetVal = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGMasterUpdateConsolidatedList"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    TmpProperties.u4Mtu = pAggEntry->u4Mtu;
    TmpProperties.u4ConsRefSpeed = pAggEntry->u4PortSpeed;
    TmpProperties.u4ConsRefHighSpeed = pAggEntry->u4PortHighSpeed;
    LA_MEMCPY (TmpProperties.BestInfoMac, gLaGlobalInfo.LaSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    /*
     * If Master has local ports and it is partipating the port channel then
     * the state machine is running for the local ports and the result of it
     * the consolidation logic will be updated the update message can be transmitted
     * from the Master in terms of ADD and/or Delete PortList to the Slaves.
     */

    i4RetVal = LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry, TmpProperties);

    if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
    {
        LaActiveDLAGMasterUpdateTreeOnPropSame (pAggEntry);

    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
    {
        LaActiveDLAGMasterUpdateTreeOnPropGreater (pAggEntry);
    }
    else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
    {
        LaActiveDLAGMasterUpdateTreeOnPropLesser (pAggEntry);
    }
    else
    {
        LaActiveDLAGChkForBestConsInfo (pAggEntry);
    }

    LaActiveDLAGSortOnStandbyConstraints (pAggEntry);

    return LA_SUCCESS;
}

/*********************************************************************************/
/* Function Name      : LaDLAGRemoteAggInfoTreeDestroy                           */
/*                                                                               */
/* Description        : This routine is used to destroy the RBTree               */
/*                                                                               */
/* Input(s)           : u4Arg    - Offset for free function                      */
/*                      freeFn   - Free Function required for RBTree Destruction */
/*                                                                               */
/* Output(s)          : None                                                     */
/*                                                                               */
/* Return Value(s)    : Pointer to RBTree                                        */
/*                                                                               */
/*********************************************************************************/

VOID
LaActiveDLAGDestroyPortListTree (tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg)
{
    if (Tree != NULL)
    {
        RBTreeDestroy (Tree, FreeFn, u4Arg);
    }
    return;
}

/*******************************************************************************/
/* Function Name      : LaActiveDLAGGetNextPortEntryFromConsTree               */
/*                                                                             */
/* Description        : This function returns the pointer to Next Consolidated */
/*                      port entry for the given port entry from Consolidated  */
/*                      RBTree. Else it returns NULL.                          */
/*                      If This function is called with NULL it will get the   */
/*                      first port entry from Standby RBTree.                  */
/*                                                                             */
/* Input(s)           : pConsPortEntry - Pointer to the current port entry     */
/*                      ppNextConsPortEntry - Pointer to Next port entry       */
/*                      pAggEntry - Aggregator entry in which RBTree exits     */
/*                                                                             */
/* Output(s)          : ppNextConsPortEntry - Pointer to the Next port entry   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/* Global Variables                                                            */
/* Modified           : None.                                                  */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On success                                */
/*                      LA_FAILURE - if port entry is NULL                     */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*******************************************************************************/

INT4
LaActiveDLAGGetNextPortEntryFromConsTree (tLaDLAGConsPortEntry * pConsPortEntry,
                                          tLaDLAGConsPortEntry **
                                          ppNextConsPortEntry,
                                          tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pTmpConsPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGGetNextPortEntryFromConsTree"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    if (pConsPortEntry == NULL)
    {
        pTmpConsPortEntry = (tLaDLAGConsPortEntry *) RBTreeGetFirst
            (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList);
    }
    else
    {
        pTmpConsPortEntry = (tLaDLAGConsPortEntry *) RBTreeGetNext
            (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
             (tRBElem *) pConsPortEntry, NULL);
    }
    if (pTmpConsPortEntry != NULL)
    {
        *ppNextConsPortEntry = pTmpConsPortEntry;
        return LA_SUCCESS;
    }

    *ppNextConsPortEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGRemovePortEntryFromAddListTree           */
/*                                                                           */
/* Description        : This function removes a consolidated port entry from */
/*                      given aggregator Consolidated list RBTree.           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists  */
/*                                                                           */
/*                      pConsPortEntry - pointer to Consolidated             */
/*                                       port entry that to be removed       */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGRemovePortEntryFromConsTree (tLaLacAggEntry * pAggEntry,
                                         tLaDLAGConsPortEntry * pConsPortEntry)
{
    if ((pAggEntry == NULL) || (pConsPortEntry == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGRemovePortEntryFromConsTree: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    if (RBTreeGet
        (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
         pConsPortEntry) == NULL)
    {
        /* Entry not present */
        return LA_SUCCESS;

    }

    if (RBTreeRem
        (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
         (tRBElem *) pConsPortEntry) == 0)
    {
        LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                     "\nRemove CONS Tree FAILED Port No=%d Prioirty=%d\n",
                     pConsPortEntry->u4PortIndex, pConsPortEntry->u2Priority);
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/**********************************************************************************/
/* Function Name      : LaActiveDLAGRemoveRemotePortFromPortList                  */
/*                                                                                */
/* Description        : This function removes a consolidated port entry from      */
/*                      given aggregator Add/Standby/Delete list RBTree.          */
/*                      If the port is in ADD list it is moved to Delete list and */
/*                      the flag set so that it will sent in DEL Pdu. At the same */
/*                      time the memory also released. If it is in Standby/Delete */
/*                      RBTree, no indication is required.                        */
/*                                                                                */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTree exists       */
/*                                                                                */
/*                      u4RemotePortIndex - Remote port Index                     */
/* Global Variables                                                               */
/* Referred           : None                                                      */
/* Global Variables                                                               */
/* Modified           : None.                                                     */
/*                                                                                */
/* Output(s)          : None                                                      */
/*                                                                                */
/* Return Value(s)    : LA_SUCCESS - On Success                                   */
/*                      LA_FAILURE - On failure                                   */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed               */
/*                                                                                */
/*********************************************************************************/

INT4
LaActiveDLAGRemoveRemotePortFromPortList (tLaLacAggEntry * pAggEntry,
                                          UINT4 u4RemotePortIndex)
{

    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4LocalPortIndex = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGRemoveRemotePortFromPortList: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LaActiveDLAGGetPortEntry (u4RemotePortIndex, &pConsPortEntry, pAggEntry);
    if (pConsPortEntry == NULL)
    {
        return LA_FAILURE;
    }

    LaActiveDLAGGetIfIndexFromBridgeIndex (u4RemotePortIndex,
                                           &u4LocalPortIndex);

    LaGetPortEntry ((UINT2) u4LocalPortIndex, &pPortEntry);

    if ((pPortEntry != NULL)
        && (LaActiveDLAGCheckIsLocalPort ((UINT2) u4RemotePortIndex)) !=
        LA_SUCCESS)
    {
        pPortEntry->pAggEntry = pAggEntry;
        LaL2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                          pPortEntry->pAggEntry->u2AggIndex);
        LaHlCreatePhysicalPort (pPortEntry);
    }

    if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
    {
        /* Port channel is removed from remote. Set the Recently changed flag */
        pConsPortEntry->u1RecentlyChanged = LA_TRUE;

        /* Set the action flag as Removed from channel
           so that it is removed from Cons Entry after sending PDU */
        pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY;
    }
    else
    {
        /* Delete the Port Entry from Tree and Release memory allocated for it */
        LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry, pConsPortEntry);
        LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
        LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);
        pConsPortEntry = NULL;
    }
    return LA_SUCCESS;
}

/**********************************************************************************/
/* Function Name      : LaActiveDLAGChkForBestConsInfo                            */
/*                                                                                */
/* Description        : This function is called whenever                          */
/*                      - The Node is Deleted                                     */
/*                      - there is a change in the received information which     */
/*                        triggers to calculate the best information              */
/*                                                                                */
/*                      The Logic is to loop for all the remote entries to find   */
/*                      the best one. If best among the remote is then compare    */
/*                      with the existing Master information provided there is    */
/*                      local port information present for the Master. .          */
/*                                                                                */
/*                                                                                */
/* Input(s)           : pAggEntry - Aggregator entry                              */
/*                                                                                */
/*                                                                                */
/* Output(s)          : None                                                      */
/*                                                                                */
/*                                                                                */
/* Return Value(s)    : LA_SUCCESS - On Success                                   */
/*                      LA_FAILURE - On failure                                   */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed               */
/*                                                                                */
/**********************************************************************************/

INT4
LaActiveDLAGChkForBestConsInfo (tLaLacAggEntry * pAggEntry)
{

    tLaDLAGRemoteAggEntry *pTmpRemAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pCheckRemAggEntry = NULL;
    tLaDLAGRemoteAggEntry *pBestRemAggEntry = NULL;
    tLaDLAGSpeedPropInfo TmpProperties;
    INT4                i4RetVal = 0;
    UINT1               u1SortingNeeded = LA_FALSE;
    UINT1               u1NoOfRemoteEntries = 0;
    tLaBoolean          bAllNodesEqual = LA_FALSE;

    LA_MEMSET (&TmpProperties, 0, sizeof (tLaDLAGSpeedPropInfo));

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGChkForBestConsInfo: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;

    }

    LA_TRC (CONTROL_PLANE_TRC, "\n Selecting Best Info out of All Nodes \n");

    /* Select for best port-channel */
    /* Get the first and second remote Aggregation Entry */
    LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pTmpRemAggEntry);
    /* Assume Best is first */
    pBestRemAggEntry = pTmpRemAggEntry;
    pCheckRemAggEntry = pTmpRemAggEntry;

    u1NoOfRemoteEntries++;

    /*
     * The best information is selected by doing comparision on the remote
     * information stored in the Master.
     */

    while (pTmpRemAggEntry != NULL)
    {
        pTmpRemAggEntry = NULL;
        /* Get Next entry of current entry */
        LaDLAGGetNextRemoteAggEntry (pAggEntry, pCheckRemAggEntry,
                                     &pTmpRemAggEntry);
        /* Assign this as checking entry */
        pCheckRemAggEntry = pTmpRemAggEntry;
        if (pCheckRemAggEntry == NULL)
        {
            /*exit the loop with Best Agg entry */
            if (u1NoOfRemoteEntries == 1)
            {
                /* Only one remote present */
                bAllNodesEqual = LA_TRUE;
            }

            pTmpRemAggEntry = NULL;
            continue;
        }
        if ((pBestRemAggEntry->u4PortSpeed == pCheckRemAggEntry->u4PortSpeed) &&
            (pBestRemAggEntry->u4PortHighSpeed ==
             pCheckRemAggEntry->u4PortHighSpeed)
            && (pBestRemAggEntry->u4Mtu == pCheckRemAggEntry->u4Mtu))
        {
            /* All are having same Info */
            /* Keep Best as first. Retain it and get the next node  */
            bAllNodesEqual = LA_TRUE;
            continue;
        }

        if ((pBestRemAggEntry->u4PortSpeed > pCheckRemAggEntry->u4PortSpeed) ||
            ((pBestRemAggEntry->u4PortSpeed == pCheckRemAggEntry->u4PortSpeed)
             && (pBestRemAggEntry->u4PortHighSpeed >
                 pCheckRemAggEntry->u4PortHighSpeed))
            ||
            ((pBestRemAggEntry->u4PortSpeed == pCheckRemAggEntry->u4PortSpeed)
             && (pBestRemAggEntry->u4PortHighSpeed ==
                 pCheckRemAggEntry->u4PortHighSpeed)
             && (pBestRemAggEntry->u4Mtu > pCheckRemAggEntry->u4Mtu)))
        {
            /*Best is first. Retain it and get the next node  */
        }
        else
        {
            /* second is best for checking */
            pBestRemAggEntry = pCheckRemAggEntry;
        }
        u1NoOfRemoteEntries++;
    }

    if (((pAggEntry->u1ConfigPortCount != 0) &&
         /* If Master particpates in port-channel check with Aggentry MTU,Speed
            only if atleast one port is configured
            so that it will have some speed */
         (pBestRemAggEntry != NULL)
         && (pBestRemAggEntry->u4PortSpeed == pAggEntry->u4PortSpeed)
         && (pBestRemAggEntry->u4PortHighSpeed == pAggEntry->u4PortHighSpeed)
         && (pBestRemAggEntry->u4Mtu == pAggEntry->u4Mtu))
        && (bAllNodesEqual == LA_TRUE))
    {
        bAllNodesEqual = LA_TRUE;
        LA_TRC (CONTROL_PLANE_TRC, "\n All Nodes having same MTU \n");

        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
            pBestRemAggEntry->u4PortSpeed;
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
            pBestRemAggEntry->u4PortHighSpeed;
        pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
            pBestRemAggEntry->u4Mtu;
        LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.BestInfoMac,
                   pBestRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
                   LA_MAC_ADDRESS_SIZE);

        LaActiveDLAGPrintBestConsInfo (pAggEntry);

        LA_TRC (CONTROL_PLANE_TRC, "\n Cons Info changed by Best\n");

        /* Loop for all port-channels and select the ports */
        LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pTmpRemAggEntry);
        while (pTmpRemAggEntry != NULL)
        {
            pTmpRemAggEntry->u1UpdateRequired = LA_TRUE;
            LaActiveDLAGUpdateTreeOnPropSame (pTmpRemAggEntry,
                                              &u1SortingNeeded);
            pTmpRemAggEntry->u1UpdateRequired = LA_FALSE;
            LaDLAGGetNextRemoteAggEntry (pAggEntry, pTmpRemAggEntry,
                                         &pTmpRemAggEntry);
        }

        /* Call for Master ports also */
        LaActiveDLAGMasterUpdateTreeOnPropSame (pAggEntry);
    }

    else
    {

        bAllNodesEqual = LA_FALSE;
        if ((pBestRemAggEntry != NULL)
            && (((pAggEntry->u1ConfigPortCount != 0) &&
                 ((pBestRemAggEntry->u4PortSpeed > pAggEntry->u4PortSpeed)
                  || (pBestRemAggEntry->u4PortHighSpeed >
                      pAggEntry->u4PortHighSpeed)
                  || (pBestRemAggEntry->u4Mtu > pAggEntry->u4Mtu))) ||
                (pAggEntry->u1ConfigPortCount == 0)))
        {
            /* Update the Consolidated Info from Best Remote Entry */
            LA_TRC (CONTROL_PLANE_TRC, "\n Remote Node Is Best \n");
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
                pBestRemAggEntry->u4PortSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
                pBestRemAggEntry->u4PortHighSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                pBestRemAggEntry->u4Mtu;
            LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                       BestInfoMac,
                       pBestRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
        }
        else
        {
            /* Master itself best */
            LA_TRC (CONTROL_PLANE_TRC, "\n Master is best \n");
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed =
                pAggEntry->u4PortSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefHighSpeed =
                pAggEntry->u4PortHighSpeed;
            pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu =
                pAggEntry->u4Mtu;
            LA_MEMCPY (pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                       BestInfoMac, gLaGlobalInfo.LaSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);

        }

        LaActiveDLAGPrintBestConsInfo (pAggEntry);

        LA_TRC (CONTROL_PLANE_TRC, "\n Cons Info changed by Best Node\n");

        LaDLAGGetNextRemoteAggEntry (pAggEntry, NULL, &pTmpRemAggEntry);
        while (pTmpRemAggEntry != NULL)
        {
            TmpProperties.u4Mtu = pTmpRemAggEntry->u4Mtu;
            TmpProperties.u4ConsRefSpeed = pTmpRemAggEntry->u4PortSpeed;
            TmpProperties.u4ConsRefHighSpeed = pTmpRemAggEntry->u4PortHighSpeed;
            LA_MEMCPY (TmpProperties.BestInfoMac,
                       pTmpRemAggEntry->DLAGRemoteSystem.SystemMacAddr,
                       LA_MAC_ADDRESS_SIZE);
            i4RetVal =
                LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry,
                                                       TmpProperties);

            /* Race contion can happen ? */
            pTmpRemAggEntry->u1UpdateRequired = LA_TRUE;

            if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
            {
                LaActiveDLAGUpdateTreeOnPropSame (pTmpRemAggEntry,
                                                  &u1SortingNeeded);
            }
            else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
            {
                LaActiveDLAGUpdateTreeOnPropGreater (pTmpRemAggEntry,
                                                     &u1SortingNeeded);

            }
            else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
            {
                LaActiveDLAGUpdateTreeOnPropLesser (pTmpRemAggEntry,
                                                    &u1SortingNeeded);
            }

            pTmpRemAggEntry->u1UpdateRequired = LA_FALSE;
            LaDLAGGetNextRemoteAggEntry (pAggEntry, pTmpRemAggEntry,
                                         &pTmpRemAggEntry);
        }

        /* Check for Master ports */

        TmpProperties.u4Mtu = pAggEntry->u4Mtu;
        TmpProperties.u4ConsRefSpeed = pAggEntry->u4PortSpeed;
        TmpProperties.u4ConsRefHighSpeed = pAggEntry->u4PortHighSpeed;
        LA_MEMCPY (TmpProperties.BestInfoMac,
                   gLaGlobalInfo.LaSystem.SystemMacAddr, LA_MAC_ADDRESS_SIZE);

        i4RetVal =
            LaActiveDLAGChkUpdateAggEntryConsInfo (pAggEntry, TmpProperties);

        if (i4RetVal == LA_AA_DLAG_MTU_AND_SPEED_SAME)
        {
            LaActiveDLAGMasterUpdateTreeOnPropSame (pAggEntry);

        }
        else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_GREATER)
        {
            LaActiveDLAGMasterUpdateTreeOnPropGreater (pAggEntry);
        }
        else if (i4RetVal == LA_AA_DLAG_MTU_OR_SPEED_LESS)
        {
            LaActiveDLAGMasterUpdateTreeOnPropLesser (pAggEntry);
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGUpdateTreeOnPropSame                     */
/*                                                                           */
/* Description        : This function will be called by Remote Consolidated  */
/*                      List when receiving a DLAG PDU with same MTU/Speed   */
/*                      with the best Info. It will check whether any port   */
/*                      properties changed(such as priority/bundle state)and */
/*                      moves the ports to corresponding state accordingly   */
/*                      and sets the SortingReq as TRUE. So that             */
/*                      consolidation takes place.                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pRemoteAggEntry - Remote Aggregator for which PDU    */
/*                                        received.                          */
/*                      pu1SortingReq - Whether Selection Logic to be        */
/*                                        triggered.                         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu1SortingReq - As TRUE or FALSE                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGUpdateTreeOnPropSame (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                  UINT1 *pu1SortingReq)
{
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGUpdateTreeOnPropSame "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Remote Called */
    pAggEntry = pRemoteAggEntry->pAggEntry;

    LA_TRC (CONTROL_PLANE_TRC, "\n Remote MTU or Speed Same \n");

    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        if ((pRemotePortEntry->u1UpdateRequired == LA_TRUE)
            || (pRemoteAggEntry->u1UpdateRequired == LA_TRUE))
        {
            *pu1SortingReq = LA_TRUE;    /* Whether consolidation needed */

            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n u1UpdateRequired is TRUE =%d\n",
                         pRemotePortEntry->u4PortIndex);
            /* First check whether entry is already in RBTree */
            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);

            if (pConsPortEntry == NULL)
            {
                /* Create Port Entry and Insert in PortList and Consolidated Tree */
                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);
                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }

                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
            }

            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Up In bundle =%d\n",
                             pRemotePortEntry->u4PortIndex);
                if (pConsPortEntry != NULL)
                {
                    /* If it is in ADD state no need to set the flag */
                    if (pConsPortEntry->u1PortStateFlag !=
                        LA_AA_DLAG_PORT_IN_ADD)
                    {
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
                }
            }
            else
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                             pRemotePortEntry->u4PortIndex);
                if (pConsPortEntry != NULL)
                {
                    /* Down in bundle is Present in PortList RBTree in ADD State,
                       move it to DELETE State. */
                    if (pConsPortEntry->u1PortStateFlag ==
                        LA_AA_DLAG_PORT_IN_ADD)
                    {
                        /* Changing Port State From ADD to DELETE */
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

                }
            }
        }                        /* UpdateRequired ends */

        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }                            /* while ends */

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGMasterUpdateTreeOnPropSame               */
/*                                                                           */
/* Description        : This function is called by Master if Master          */
/*                      particpates in port-channel when MTU/Speed is same   */
/*                      with best info.It will check whether any port        */
/*                      properties changed(such as priority/Bundle state)and */
/*                      moves the ports to corresponding State accordingly   */
/*                      and after that consolidation takes place.            */
/* Input(s)           : pAggEntry -  Aggregator for which ports are added    */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On Success                              */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed          */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGMasterUpdateTreeOnPropSame (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;
    UINT4               u4BrgPortIndex = 0;
    UINT1               u1BundleState = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGMasterUpdateTreeOnPropSame: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "Master MTU Or Speed Same:\n ");
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

    while (pPortEntry != NULL)
    {
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);
        LaGetPortBundleState ((UINT4) pPortEntry->u2PortIndex, &u1BundleState);
        /* First check whether entry is already in RBTree */
        LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Create Port Entry and Insert in PortList and Consolidated Tree */
            i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                        u4BrgPortIndex,
                                                        pPortEntry->
                                                        LaLacActorInfo.
                                                        u2IfPriority);

            if (i4RetVal == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            /*Adding the newly created Port to the PortList */
            LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);

        }

        if (u1BundleState == LA_PORT_UP_IN_BNDL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Up In bundle =%d\n",
                         pPortEntry->u2PortIndex);
            if (pConsPortEntry != NULL)
            {
                /* If it is in ADD state no need to set the flag */
                if (pConsPortEntry->u1PortStateFlag != LA_AA_DLAG_PORT_IN_ADD)
                {
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;
            }
        }
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                         pPortEntry->u2PortIndex);
            if (pConsPortEntry != NULL)
            {
                /* Down in bundle is Present in PortList RBTree in ADD State,
                   move it to DELETE State. */
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    /* Changing Port State From ADD to DELETE */
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
            }
        }

        /* Continue to next port */
        pConsPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);

    }

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGUpdateTreeOnPropGreater                   */
/*                                                                            */
/* Description        : This function will be called by Remote Consolidated   */
/*                      List when receiving a DLAG PDU with Greater MTU/Speed */
/*                      with the Best Info. It will move all the ports from   */
/*                      Add & Standby State to delete State. After that only  */
/*                      Ports that belongs to this port-channel are moved to  */
/*                      Add State and sets the SortingReq as TRUE. So that    */
/*                      consolidation takes place.                            */
/*                                                                            */
/*                                                                            */
/* Input(s)           : pRemoteAggEntry - Remote Aggregator for which PDU     */
/*                                        received.                           */
/*                      pu1SortingReq - Whether Selection Logic to be         */
/*                                        triggered.                          */
/*                                                                            */
/*                                                                            */
/* Output(s)          : pu1SortingReq - As TRUE or FALSE                      */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS - On Success                               */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed           */
/*                                                                            */
/******************************************************************************/

INT4
LaActiveDLAGUpdateTreeOnPropGreater (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                     UINT1 *pu1SortingReq)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGUpdateTreeOnPropGreater"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Remote Called */
    pAggEntry = pRemoteAggEntry->pAggEntry;

    LA_TRC (CONTROL_PLANE_TRC, "\n MTU or Speed greater \n");
    LA_TRC (CONTROL_PLANE_TRC, "\n Move all the ports to Delete State \n");
    /* Get the Port Entry and move all Port State to DELETE */
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        /* Incase Port is in STANDBY state no need to give indication,
         * If it is in DELETE State its not changed at all*/
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
        {
            /* Set the RecentChangedFlag,
             * Using this flag,Ports which are already in ADD state
             * can be identified, if it belongs to the same Greater Aggregation,
             * Port needs to be moved back to ADD state, resetting the 
             * recently changed flag */
            pConsPortEntry->u1RecentlyChanged = LA_TRUE;
        }
        pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    pConsPortEntry = NULL;
    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        if ((pRemotePortEntry->u1UpdateRequired == LA_TRUE)
            || (pRemoteAggEntry->u1UpdateRequired == LA_TRUE))
        {
            *pu1SortingReq = LA_TRUE;
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "\n Update Required is TRUE Port=%d\n",
                         pRemotePortEntry->u4PortIndex);

            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);

            if (pConsPortEntry == NULL)
            {
                /* Port Entry is not present, creating new Port Entry */
                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);
                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }

                /*Adding the newly created Port to the PortList */
                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);

            }

            /* Incase of Bundle State is UP Port needs to be in ADD State,
             * If Bundle State is DOWN, Port Should be in DELETE State */
            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Up =%d\n",
                             pRemotePortEntry->u4PortIndex);

                if (pConsPortEntry != NULL)
                {
                    /* Set the RecentChangedFlag and move Port to ADD State */
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

                    /* Port State can Either be in DELETE/NOT_IN_ANY_LIST/STANDBY.
                     * Reverse the Recently Changed Flag, If Port State is 
                     * NOT_IN_ANY_LIST Flag definitely will be false and needs to be set,
                     * If Port State is in DELETE if its previuos state is ADD it will be set,
                     * else It will be false, so reverse it */
                    if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
                    {
                        pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                    }
                    else
                    {
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }

                }
            }
            else
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Down =%d\n",
                             pRemotePortEntry->u4PortIndex);
                if ((pConsPortEntry != NULL) &&
                    (pConsPortEntry->u1PortStateFlag ==
                     LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

                }
            }

        }
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }
    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGMasterUpdateTreeOnPropGreater              */
/*                                                                             */
/* Description        : This function is called by Master if Master also       */
/*                      particpates in port-channel when MTU/Speed is changed  */
/*                      greater with best info.It will move all the ports from */
/*                      ADD & STANDBY State to DELETE State,After that only    */
/*                      Ports that belongs to this port-channel are moved to   */
/*                      ADD State.After that consolidation takes place.        */
/*                                                                             */
/* Input(s)           : pAggEntry -  Aggregator for which ports are added      */
/*                                                                             */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*                                                                             */
/*******************************************************************************/

INT4
LaActiveDLAGMasterUpdateTreeOnPropGreater (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4BrgPortIndex = 0;
    INT4                i4RetVal = LA_FAILURE;
    UINT1               u1BundleState = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGMasterUpdateTreeOnPropGreater"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "Master MTU Or Speed Greater:\n ");

    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        /* Incase Port is in STANDBY state no need to give indication,
         * If it is in DELETE State its not changed at all*/
        if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
        {
            /* Set the RecentChangedFlag */
            pConsPortEntry->u1RecentlyChanged = LA_TRUE;
        }
        pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    pConsPortEntry = NULL;
    pPortEntry = NULL;            /* Get First Entry */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        LaGetPortBundleState ((UINT4) pPortEntry->u2PortIndex, &u1BundleState);

        /* Incase of Bundle State is UP Port needs to be in ADD State,
         * If Bundle State is DOWN, Port Should be in DELETE State */
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);

        LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Port Entry is not present, creating new Port Entry */
            i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                        u4BrgPortIndex,
                                                        pPortEntry->
                                                        LaLacActorInfo.
                                                        u2IfPriority);

            if (i4RetVal == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            /*Adding the newly created Port to the PortList */
            LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
        }

        if (u1BundleState == LA_PORT_UP_IN_BNDL)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Up =%d\n",
                         (UINT4) pPortEntry->u2PortIndex);
            if (pConsPortEntry != NULL)
            {
                /* Set the RecentChangedFlag and move Port to ADD State */
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_ADD;

                /* Port State can Either be in DELETE or NOT_IN_ANY_LIST.
                 * Reverse the Recently Changed Flag, If Port State is 
                 * NOT_IN_ANY_LIST Flag definitely will be false and needs to be set,
                 * If Port State is in DELETE if its previuos state is ADD it will be set,
                 * else It will be false, so reverse it */
                if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
                {
                    pConsPortEntry->u1RecentlyChanged = LA_FALSE;
                }
                else
                {
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
            }
        }                        /* Else Already all Ports are in DELETE state, so do Nothing */
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                         pPortEntry->u2PortIndex);

            if ((pConsPortEntry != NULL) &&
                (pConsPortEntry->u1PortStateFlag ==
                 LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
            {
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

            }
        }

        /* Continue to next port */
        pConsPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }

    return LA_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : LaActiveDLAGUpdateTreeOnPropLesser                     */
/*                                                                             */
/* Description        : This function will be called by Remote Consolidated    */
/*                      List when receiving a DLAG PDU with Lesser  MTU/Speed  */
/*                      with the Best Info. It will move all the ports of this */
/*                      Remote channel from ADD & STANDBY State to DELETE State*/
/*                      and sets the SortingReq as TRUE. So that consolidation */
/*                      takes place.                                           */
/*                                                                             */
/*                                                                             */
/* Input(s)           : pRemoteAggEntry - Remote Aggregator for which PDU      */
/*                                        received.                            */
/*                      pu1SortingReq - Whether Selection Logic to be          */
/*                                        triggered.                           */
/*                                                                             */
/*                                                                             */
/* Output(s)          : pu1SortingReq - As TRUE or FALSE                       */
/*                                                                             */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*                                                                             */
/*******************************************************************************/

INT4
LaActiveDLAGUpdateTreeOnPropLesser (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                    UINT1 *pu1SortingReq)
{

    tLaDLAGRemotePortEntry *pRemotePortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4RetVal = LA_FAILURE;

    if (pRemoteAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGUpdateTreeOnPropLesser"
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    pAggEntry = pRemoteAggEntry->pAggEntry;

    LA_TRC (CONTROL_PLANE_TRC, "\n MTU or Speed Lesser \n");
    LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, NULL, &pRemotePortEntry);
    while (pRemotePortEntry != NULL)
    {
        /* First check whether entry is already in RBTree */
        if ((pRemotePortEntry->u1UpdateRequired == LA_TRUE)
            || (pRemoteAggEntry->u1UpdateRequired == LA_TRUE))
        {
            *pu1SortingReq = LA_TRUE;    /* Whether consolidation needed */
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "\n Update Required is TRUE Port=%d\n",
                         pRemotePortEntry->u4PortIndex);
            LaActiveDLAGGetPortEntry (pRemotePortEntry->u4PortIndex,
                                      &pConsPortEntry, pAggEntry);
            if (pConsPortEntry == NULL)
            {
                i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                            pRemotePortEntry->
                                                            u4PortIndex,
                                                            pRemotePortEntry->
                                                            u2Priority);

                if (i4RetVal == LA_FAILURE)
                {
                    /* Failed to create Port Entry */
                    return LA_FAILURE;
                }

                LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);

            }

            if (pRemotePortEntry->u1BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Up =%d\n",
                             pRemotePortEntry->u4PortIndex);

                if (pConsPortEntry != NULL)
                {
                    /* If it is not in ADD state no need to set the flag */
                    if (pConsPortEntry->u1PortStateFlag ==
                        LA_AA_DLAG_PORT_IN_ADD)
                    {
                        pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                    }
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
                }
            }
            else
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Bundle State is Down =%d\n",
                             pRemotePortEntry->u4PortIndex);

                if ((pConsPortEntry != NULL) &&
                    (pConsPortEntry->u1PortStateFlag ==
                     LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
                {
                    pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

                }
            }
        }                        /* Update required ends */
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaDLAGGetNextRemotePortEntry (pRemoteAggEntry, pRemotePortEntry,
                                      &pRemotePortEntry);
    }                            /* while ends */
    return LA_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : LaActiveDLAGMasterUpdateTreeOnPropLesser               */
/*                                                                             */
/* Description        : This function is called by Master if Master also       */
/*                      particpates in port-channel when MTU/Speed is changed  */
/*                      greater with best info.It will move all the ports from */
/*                      of this port-channel from ADD & STANDBY State to DELETE*/
/*                      State, After that consolidation takes place.           */
/*                                                                             */
/* Input(s)           : pAggEntry -  Aggregator for which ports are added      */
/*                                                                             */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/*                                                                             */
/* Return Value(s)    : LA_SUCCESS - On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer is passed            */
/*                                                                             */
/*******************************************************************************/

INT4
LaActiveDLAGMasterUpdateTreeOnPropLesser (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4BrgPortIndex = 0;
    UINT1               u1BundleState = 0;
    INT4                i4RetVal = LA_FAILURE;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGMasterUpdateTreeOnPropLesser: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    LA_TRC (CONTROL_PLANE_TRC, "\n Master MTU or Speed Lesser \n");
    LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
    while (pPortEntry != NULL)
    {
        /* First check whether entry is already in RBTree */
        LaGetPortBundleState ((UINT4) pPortEntry->u2PortIndex, &u1BundleState);

        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);
        LaActiveDLAGGetPortEntry (u4BrgPortIndex, &pConsPortEntry, pAggEntry);

        if (pConsPortEntry == NULL)
        {
            /* Create Port Entry and Insert in PortList and Consolidated Tree */

            i4RetVal = LaActiveDLAGCreateConsPortEntry (&pConsPortEntry,
                                                        u4BrgPortIndex,
                                                        pPortEntry->
                                                        LaLacActorInfo.
                                                        u2IfPriority);
            if (i4RetVal == LA_FAILURE)
            {
                /* Failed to create Port Entry */
                return LA_FAILURE;
            }

            /*Adding the newly created Port to the PortList */
            LaActiveDLAGAddPortEntry (pAggEntry, pConsPortEntry);
        }

        if (u1BundleState == LA_PORT_UP_IN_BNDL)
        {
            if (pConsPortEntry != NULL)
            {
                /* If it is not in ADD state no need to set the flag */
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    pConsPortEntry->u1RecentlyChanged = LA_TRUE;
                }
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;
            }
        }
        else
        {

            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Port Down In bundle =%d\n",
                         pPortEntry->u2PortIndex);
            if ((pConsPortEntry != NULL) &&
                (pConsPortEntry->u1PortStateFlag ==
                 LA_AA_DLAG_PORT_NOT_IN_ANY_LIST))
            {
                pConsPortEntry->u1PortStateFlag = LA_AA_DLAG_PORT_IN_DELETE;

            }
        }

        /* Else down in bundle ports. Don't do anything */
        /* Continue to next port */
        pConsPortEntry = NULL;
        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGChkAddDelDSURequired                     */
/*                                                                           */
/* Description        : This function checks whether any of the ports        */
/*                      RecentlyChanged Flag is set in Add & Standby &       */
/*                      Delete RBTrees. If flag is set for atleast one port  */
/*                      return LA_TRUE so taht Add/Del PDU is triggered.     */
/*                      Else LA_FALSE is returned so that no PDU is triggered*/
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator in which RBTrees are there    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - If atleast one port flag is set            */
/*                      LA_FALSE - If none of ports flag is set              */
/*                      LA_ERR_NULL_PTR - If Null pointer received           */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGChkAddDelDSURequired (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGChkAddDelDSURequired: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Scan the Port Entry RBTree and check if Recently Changed Flag is set */
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
        {
            return LA_TRUE;
        }
        LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                      pAggEntry);
    }

    /* No ports flag set */
    return LA_FALSE;

}

/*****************************************************************************/
/* Function Name      : LaDLAGActivePrintAllTreePorts                        */
/*                                                                           */
/* Description        : This function prints all the ports of All RBTrees.   */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaDLAGActivePrintAllTreePorts (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    UINT4               u4Count = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaDLAGActivePrintAllTreePorts: "
                "NULL POINTER.\n");
        return;
    }
    pConsPortEntry = NULL;

    LA_TRC (CONTROL_PLANE_TRC,
            "\n -----------------------------------------------------------------\n");
    RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGPortList, &u4Count);
    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "\n Count = %d Remote: All RBTree Ports: \n", u4Count);
    while (pConsPortEntry != NULL)
    {
        if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d-Set ",
                         pConsPortEntry->u4PortIndex);
        }
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d-Not Set ",
                         pConsPortEntry->u4PortIndex);

        }
        LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                      &pConsPortEntry, pAggEntry);

    }
    LA_TRC (CONTROL_PLANE_TRC, "\n");

    LA_TRC (CONTROL_PLANE_TRC,
            "\n -----------------------------------------------------------------\n");
    pConsPortEntry = NULL;
    RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
                 &u4Count);
    LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pConsPortEntry, pAggEntry);
    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "\n Count = %d Remote: Consolidated RBTree Ports:\n ",
                 u4Count);
    while (pConsPortEntry != NULL)
    {
        if (pConsPortEntry->u1RecentlyChanged == LA_TRUE)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d-Set ",
                         pConsPortEntry->u4PortIndex);
        }
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "%d-Not Set ",
                         pConsPortEntry->u4PortIndex);
        }

        LaActiveDLAGGetNextPortEntryFromConsTree (pConsPortEntry,
                                                  &pConsPortEntry, pAggEntry);
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "\n -----------------------------------------------------------------\n");
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGDeleteEntriesInAllTree                   */
/*                                                                           */
/* Description        : This function is called when DLAG is deinitialized   */
/*                      to delete all the ports entries in All RBTrees       */
/*                      and releases the memory allocated for it.            */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaActiveDLAGDeleteEntriesInAllTree (tLaLacAggEntry * pAggEntry)
{

    tLaDLAGConsPortEntry *pConsPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGDeleteEntriesInAllTree: "
                "NULL POINTER.\n");
        return;
    }

    LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pConsPortEntry, pAggEntry);

    LA_TRC (CONTROL_PLANE_TRC,
            "\n Delete all the ports in all RBTrees and Free the memory \n");
    while (pConsPortEntry != NULL)
    {
        LaActiveDLAGRemovePortEntryFromConsTree (pAggEntry, pConsPortEntry);
        LaActiveDLAGGetNextPortEntryFromConsTree (NULL, &pConsPortEntry,
                                                  pAggEntry);

    }

    LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
    while (pConsPortEntry != NULL)
    {
        LaActiveDLAGRemovePortEntry (pAggEntry, pConsPortEntry);
        LaActiveDLAGDeleteConsPortEntry (pConsPortEntry);
        pConsPortEntry = NULL;
        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);

    }

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Free Units in Consolidated Tree= %d \n",
                 MemGetFreeUnits (LA_DLAGCONSPORTENTRY_POOL_ID));
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGMasterProcessAddDeleteLinks              */
/*                                                                           */
/* Description        : This function programs the ports in H/w while        */
/*                      giving the PDU to Slaves by Master.                  */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which RBTrees there  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS- On Success                               */
/*                      LA_ERR_NULL_PTR - If Null pointer received           */
/*                                                                           */
/*****************************************************************************/

INT4
LaActiveDLAGMasterProcessAddDeleteLinks (tLaLacAggEntry * pAggEntry,
                                         UINT4 u4PortBrgIndex,
                                         UINT1 u1PortStateFlag)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    INT4                i4RetStatus = 0;
    UINT4               u4PortIndex = 0;
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = 0;
#endif

    LA_TRC (CONTROL_PLANE_TRC, "\n Master programs Add/Del list in H/w \n");

    LaActiveDLAGGetIfIndexFromBridgeIndex ((UINT2) u4PortBrgIndex,
                                           &u4PortIndex);

    if (u4PortIndex != 0)
    {
        LaGetPortEntry ((UINT2) u4PortIndex, &pPortEntry);

        i4RetStatus = LaActiveDLAGCheckIsLocalPort ((UINT2) u4PortIndex);

        if ((i4RetStatus == LA_SUCCESS) &&
            (pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry))
        {
            if ((u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
                && (pPortEntry->u1DLAGMasterAck == LA_TRUE))
            {
                pPortEntry->u1DLAGMasterAck = LA_FALSE;
                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_FALSE;
            }

            if ((u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                && (pPortEntry->u1DLAGMasterAck == LA_FALSE))
            {
                pPortEntry->u1DLAGMasterAck = LA_TRUE;
                pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization =
                    LA_TRUE;
            }

            /* Convey this info with Peer */
            pPortEntry->NttFlag = LA_TRUE;
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
        }

        if (u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
        {
            LaActiveDLAGHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
        }
        else
        {
            LaActiveDLAGHwControl (pPortEntry, LA_HW_EVENT_ENABLE_DISTRIBUTOR);
        }
    }

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (u1PortStateFlag == LA_AA_DLAG_PORT_IN_DELETE)
        {
            if (LaFsLaHwDlagRemoveLinkFromAggGroup (pAggEntry->u2AggIndex,
                                                    (UINT2) u4PortBrgIndex) !=
                FNP_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                             "FsLaHwDlagRemoveLinkFromAggGroup: Failed to "
                             "configure remote ports in H/w for: port-"
                             "channel: %u\n", pAggEntry->u2AggIndex);
            }
            if ((pPortEntry != NULL) && (pPortEntry->pAggEntry == pAggEntry))
            {
                /* Since Port is removed from H/w Change the Status */
                pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
            }
        }

        if (u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
        {
            if (LaFsLaHwDlagAddLinkToAggGroup (pAggEntry->u2AggIndex,
                                               (UINT2) u4PortBrgIndex,
                                               &u2HwAggIndex) != FNP_SUCCESS)
            {
                LA_TRC_ARG1 (DATA_CTRL_PLANE_TRC,
                             "FsLaHwDlagAddLinkToAggGroup: Failed to "
                             "configure remote ports in H/w for: port-"
                             "channel: %u\n", pAggEntry->u2AggIndex);
            }
        }
    }
#endif
    return LA_SUCCESS;

}

/******************************************************************************/
/* Function Name      : LaActiveDLAGMasterDownIndication                      */
/*                                                                            */
/* Description        : This function called whenever there is a indication   */
/*                      from MBSM that Master is Down. So all the Slaves will */
/*                      start sending LACP PDU with Default Global Mac,       */
/*                      Priority instead of DLAG configured MAc,Priority      */
/*                                                                            */
/* Input(s)           : pAggEntry - Aggregator entry in which DLAG Enabled    */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/

INT4
LaActiveDLAGMasterDownIndication ()
{
    LA_TRC (CONTROL_PLANE_TRC, "\n Master Down Indication \n");

    /* Master Down is set to TRUE */
    if (LA_AA_DLAG_MASTER_DOWN == LA_TRUE)
    {
        /* Already master is in Down */
        return LA_SUCCESS;

    }

    LaActiveDLAGDisable ();

    /* 1) This done here becasue LaDLAGResetActorSystemID,
     *    LaDLAGResetActorSystemPriority will call LaHwControl(DISABLE_DISTRIBUTOR)
     *    for ports that are in port-channel. This will try to behave as normal
     *    scenario. Until that time we have to keep Master as Up so that there
     *    it will not try to act as normal remove scenario.
     * 2) When port moves Distributing will take time(based on timeout) 
     *    +2 seconds (wait time) we will attain normal scenario. */

    LA_AA_DLAG_MASTER_DOWN = LA_TRUE;

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGMasterUpIndication                        */
/*                                                                            */
/* Description        : This function called whenever there is a indication   */
/*                      from MBSM that Master is UP. So all the Slaves will   */
/*                      start sending LACP PDU with Configured DLAG MAC,      */
/*                                                                            */
/* Input(s)           : None                                                  */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/

INT4
LaActiveDLAGMasterUpIndication ()
{

    LA_TRC (CONTROL_PLANE_TRC,
            "\nLaActiveDLAGMasterUpIndication: Master Up Indication \n");

    if (LA_AA_DLAG_MASTER_DOWN == LA_FALSE)
    {
        /* Already master is Up */
        return LA_SUCCESS;

    }

    LaActiveDLAGEnable ();

    /* This is done here because when Master comes up 
     * normal LACP ports needs to be flushed and removed from
     * H/L. So for not disturbing that we are making the global
     * variable false here */
    LA_AA_DLAG_MASTER_DOWN = LA_FALSE;

    return LA_SUCCESS;

}

/******************************************************************************/
/* Function Name      : LaActiveDLAGHwControl                                 */
/*                                                                            */
/* Description        : This function is useful in indicating to lower layer  */
/*                      hardware / interface about the mux state by calling   */
/*                      the callback functions provided by it. Presently,     */
/*                      the function just updates the event and calls mux.    */
/*                                                                            */
/* Input(s)           : pPortEntry - Pointer to PortEntry                     */
/*                      LaLacEvent - Enable/Disable Distributor               */
/*                                                                            */
/* Output(s)          : None                                                  */
/*                                                                            */
/*                                                                            */
/* Return Value(s)    : LA_SUCCESS- On Success                                */
/*                      LA_ERR_NULL_PTR - If Null pointer received            */
/*                                                                            */
/******************************************************************************/
INT4
LaActiveDLAGHwControl (tLaLacPortEntry * pPortEntry, UINT1 LaLacEvent)
{

#ifdef NPAPI_WANTED
    tLaLacAggConfigEntry *pAggConfigEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    tLaParamInfo       *pLaParamInfo = NULL;
#endif
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaNpTrapLogInfo    NpLogInfo;
#ifdef L2RED_WANTED
    tLaHwSyncInfo      *pLaHwEntry = NULL;
#endif
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = LA_INVALID_HW_AGG_IDX;
#endif
    UINT2               u2AggIndex;
    UINT2               au2ActivePortArray[LA_MAX_PORTS_PER_AGG];

    MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));
    LA_MEMSET (au2ActivePortArray, 0, sizeof (au2ActivePortArray));

#ifdef NPAPI_WANTED
    pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);
#endif

    if (pPortEntry == NULL)
    {

        LA_TRC (INIT_SHUT_TRC, "Pointer to Port Entry NULL\n");
        return LA_ERR_NULL_PTR;
    }

    if ((pAggEntry = pPortEntry->pAggEntry) == NULL)
    {
        return LA_FAILURE;
    }

#ifdef NPAPI_WANTED
    pAggConfigEntry = &(pAggEntry->AggConfigEntry);
#endif
    u2AggIndex = pAggEntry->u2AggIndex;

    switch (LaLacEvent)
    {
        case LA_HW_EVENT_ENABLE_DISTRIBUTOR:

            if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
            {
                if (LaLacGetActivePorts
                    (u2AggIndex, au2ActivePortArray,
                     &pAggEntry->u1DistributingPortCount) != LA_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                 "SYS:LaLacGetActivePorts function FAILED for Agg %d\n",
                                 u2AggIndex);
                    return LA_FAILURE;
                }
            }
            else
            {
                pAggEntry->u1DistributingPortCount++;
            }

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {
                if (pAggEntry->u1DistributingPortCount == 1)
                {
                    if (LaFsLaHwCreateAggGroup (u2AggIndex, &u2HwAggIndex)
                        == FNP_FAILURE)
                    {
                        LaHandleAggCreateFailed (u2AggIndex);
                        LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                                     "in HW failed\n", u2AggIndex);
                        return LA_SUCCESS;
                    }

                    LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                                 "Success in LaActiveDLAGControl\n",
                                 u2AggIndex);

                    FsLaHwSetSelectionPolicyBitList
                        (u2AggIndex,
                         pAggConfigEntry->u4LinkSelectPolicyBitList);

                    LaAggCreated (u2AggIndex, u2HwAggIndex);
                }

                /* This function is moved inside NP_PROGRAMMING_ALLOWED,
                 * because in Synchronous NP case, this function should not be
                 * executed in the Standy node.In the standby node, this
                 * function is invoked from LA_NP_ADD_PORT_SUCCESS_INFO message
                 * handler */

                LaActiveDLAGLinkAddedToAgg (u2AggIndex,
                                            pPortEntry->u2PortIndex);
            }
#else /* if NPAPI_WANTED not defined */

            /*If NPAPI_WANTED is not defined, then function gets invoked in
             * both active and standby node */
            LaActiveDLAGLinkAddedToAgg (u2AggIndex, pPortEntry->u2PortIndex);
#endif
            break;
        case LA_HW_EVENT_DISABLE_DISTRIBUTOR:

            if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
            {
                if (LaLacGetActivePorts
                    (u2AggIndex, au2ActivePortArray,
                     &pAggEntry->u1DistributingPortCount) != LA_SUCCESS)
                {
                    LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                                 "SYS:LaLacGetActivePorts function FAILED for Agg %d\n",
                                 u2AggIndex);
                    return LA_FAILURE;
                }
            }
            else
            {
                if (pAggEntry->u1DistributingPortCount != 0)
                {
                    pAggEntry->u1DistributingPortCount--;
                }
            }

            LaActiveDLAGChangeAggSpeed (pAggEntry);

            if (pAggEntry->u1DistributingPortCount == 0)
            {
#ifdef L2RED_WANTED
                /* When LA_NP_DEL_INFO doesnt reach the standby node, At the
                 * standby, all ports in the group will be in DOWN state
                 * and distributingPortCount variable in the corresponding agg
                 * is zero but pLaHwEntry->u1AggStatus = LA_ENABLE and
                 * pLaHwEntry->u2HwAggIdx will have valid agg Id.
                 *
                 * If the HW maintains one to one mapping between the HW agg
                 * index and SW agg index (always same HwAggIdx for SwAggIdx),
                 * then this scenario will not be an issue
                 *
                 * If the HW reassigns the HwAggId to some other SwAggIdx then
                 * this will be an issue during the HW Audit
                 * There is a possibility for having same HwAggIdx to two or more
                 * SwAggIdx, this happens if LA_NP_DEL_INFO doesnt reach the standby
                 * In the HwAudit, it reads the active HwAggIdx and gets the
                 * corresponding SwAggIdx, here, since we have two SwAgg with same
                 * HwAggIdx, we may return older SwAggIdx. It checks for the
                 * distributingPortCount, which is zero, so it deletes the aggregator
                 * from the HW. The problem here is, New AggIdx will be in up state
                 * with ports in distributing state but Hw will have not have this
                 * group.
                 * This is taken care by the following statements, here we reset the
                 * HwAggIdx and AggStatus if distributingPortCount becomes zero,
                 * without waiting for LA_NP_DEL_INFO
                 */
                if ((u2AggIndex <= LA_MAX_PORTS) ||
                    (u2AggIndex > LA_MAX_PORTS + LA_MAX_AGG))
                    return LA_FAILURE;
                pLaHwEntry = LA_RED_GET_AGG_ENTRY (u2AggIndex);
                if (pLaHwEntry != NULL)
                {
                    pLaHwEntry->u2HwAggIdx = LA_INVALID_HW_AGG_IDX;
                    pLaHwEntry->u1AggStatus = LA_DISABLED;
                }

#endif /* L2RED_WANTED */
                if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
                {
                    pAggEntry->u1AggOperStatus = LA_OPER_DOWN;

                    /* Get the time of last oper change */
                    LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
                    /* Indicate to bridge */
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                 "\nLA: Line Protocol on Port Channel %d changed down",
                                 pAggEntry->AggConfigEntry.u2ActorAdminKey);

                    LaL2IwfLaHLPortOperIndication (u2AggIndex, LA_OPER_DOWN);

                }
                pAggEntry->bHlIndicated = LA_FALSE;
                LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                             "SYS: Port: %u :: AggIndex: %u OPER DOWN Indication to BRIDGE\n",
                             pPortEntry->u2PortIndex, u2AggIndex);
            }
            LaUpdateLlPortChannelStatus (pAggEntry);

#ifdef NPAPI_WANTED
            if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
            {

                /* When FsLaHwAddLinkToAggGroup fails then the variable
                 * LaLacHwPortStatus is set to LA_HW_ADD_PORT_FAILED_IN_LAGG. In
                 * the NP callback function we set the Operkey of this port to
                 * zero. This makes the port to come out of the bundle, So we
                 * execute DISABLE_DISTRIBUTING switch case in the
                 * LaLacHwControl,here we try to delete the port from the HW
                 * (which is not present in the HW), because the port is not in
                 * the HW, delete call fails and the LaLacHwPortStatus is set
                 * to LA_HW_REM_PORT_FAILED_IN_LAGG. To handle this,
                 * Before initiating the PortDel NP call in the
                 * LaLacHwControl, Can we check for the status of this
                 * variable (LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG)
                 */

                /*The indication is send to applications that the port is
                 *removed from the port channel*/

                MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
                pLaParamInfo->u2IfIndex = pPortEntry->u2PortIndex;
                /*pLaParamInfo->u2IfKey = u2AggIndex; */

                pLaParamInfo->u2IfKey = pPortEntry->LaLacActorInfo.u2IfKey;
                NotifyProtoToApp.LaNotify.u1OperStatus = LA_OPER_DOWN;
                NotifyProtoToApp.LaNotify.u1Action =
                    LA_NOTIFY_PORT_IN_BUNDLE_STATUS;
                /*SystemId in pLaParamInfo is unused for this indication */
                LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
            }
#endif /* NPAPI_WANTED */

            LaL2IwfRemoveActivePortFromPortChannel (pPortEntry->u2PortIndex,
                                                    u2AggIndex);
            /* LA_MCAST_CHANGE Ends */
            break;

        default:

            break;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveLinkAddedToAgg                               */
/*                                                                           */
/* Description        : This function gives indication to HL and Lower Layer */
/*                                                                           */
/* Input(s)           : u2AggIndex - Port-channel index .                    */
/*                      u2PortIndex - Port index                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

INT4
LaActiveDLAGLinkAddedToAgg (UINT2 u2AggIndex, UINT2 u2PortIndex)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;

    /* CFA, L2Iwf indication about the link should be given only when the
     * NPAPI operations is successfully completed. Hence for Asynchronous
     * NPAPI, this indication will be given only in the NP CallBack function*/

    if ((LaIssGetAsyncMode (L2_PROTO_LACP) == LA_NP_ASYNC) &&
        (gLaCallSequence != LA_NP_CALLBACK_FLOW))
    {
        return LA_SUCCESS;
    }

    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry != NULL)
    {
        pPortEntry->LaLacHwPortStatus = LA_HW_PORT_ADDED_IN_LAGG;
    }

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));

        /* The indication is send to applications that the port is
         * added to the port channel*/
        NotifyProtoToApp.LaNotify.u1OperStatus = LA_OPER_UP;
        NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_PORT_IN_BUNDLE_STATUS;
        NotifyProtoToApp.LaNotify.LaInfo.u2IfIndex = u2AggIndex;
        LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
        LaRedSyncModifyPortsInfo (LA_NP_ADD_PORT_SUCCESS_INFO, u2AggIndex,
                                  u2PortIndex);
    }

    LaActiveDLAGChangeAggSpeed (pAggEntry);

    /* Make the OPer Status for the Aggregator UP */
    if (!pAggEntry->bHlIndicated)
    {
        if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
        {
            pAggEntry->u1AggOperStatus = LA_OPER_UP;
            LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
            /* Indicate to bridge */
            LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\nLA: Line Protocol on "
                         "Port Channel %d changed up",
                         pAggEntry->AggConfigEntry.u2ActorAdminKey);
            LaUpdateLlPortChannelStatus (pAggEntry);
            LaL2IwfLaHLPortOperIndication (u2AggIndex, LA_OPER_UP);
            pAggEntry->bHlIndicated = LA_TRUE;
        }
    }
    else
    {
        LaUpdateLlPortChannelStatus (pAggEntry);
    }
    /*The indication is given to the higher layer modules only after
     * the Port Speed is updated in LA module as well as in CFA module.*/
    LaL2IwfAddActivePortToPortChannel (u2PortIndex, u2AggIndex);
    LA_TRC_ARG2 (CONTROL_PLANE_TRC, "SYS: Port: %u :: AggIndex: %u OPER UP "
                 "indication to BRIDGE\n", u2PortIndex, u2AggIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGChangeAggSpeed                           */
/*                                                                           */
/* Description        : Calculates the Aggregator Speed. Called when a port  */
/*                      becomes distributing in a port channel, or goes out  */
/*                      of distributing state.                               */
/*                                                                           */
/* Input(s)           : pAggEntrypAggEntry- Pointer to Aggregator Entry.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaActiveDLAGChangeAggSpeed (tLaLacAggEntry * pAggEntry)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT4               u4DistPortSpeed = 0;
    UINT4               u4LocalPortIndex = 0;
    UINT4               u4DistPortHighSpeed = 0;
    UINT2               u2DistPortCount = 0;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGChangeAggSpeed: "
                "NULL POINTER.\n");
        return;
    }

    pAggEntry->u4AggSpeed = 0;
    pAggEntry->u4AggHighSpeed = 0;

    if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

        while (pPortEntry != NULL)
        {
            if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
                 LA_TRUE)
                && (pPortEntry->LaLacActorInfo.LaLacPortState.
                    LaSynchronization == LA_TRUE))
            {
                u4DistPortSpeed = pPortEntry->u4LinkSpeed;
                u4DistPortHighSpeed = pPortEntry->u4LinkHighSpeed;
                u2DistPortCount++;
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

    }

    if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
    {
        /* Get the Remote ports of this port channel */
        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            LaActiveDLAGGetIfIndexFromBridgeIndex (pConsPortEntry->u4PortIndex,
                                                   &u4LocalPortIndex);
            LaGetPortEntry ((UINT2) u4LocalPortIndex, &pPortEntry);
            if ((pPortEntry != NULL) &&
                (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD))
            {
                u4DistPortSpeed = pPortEntry->u4LinkSpeed;
                u4DistPortHighSpeed = pPortEntry->u4LinkHighSpeed;
                u2DistPortCount++;
            }

            LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                          pAggEntry);
        }
    }

    /* This logic depends on the link speed of each port number of
     * distributing ports in the aggregation.
     * For 100Mpbs links, if the number exceeds 40 then high speed is used
     * because if 40*100 mbps links are aggregated then the threshold value
     * of the UINT4 variable is reached.
     * For 1Gbps links, if the number exceeds 4 then high speed is used
     * because if 4*1000 mbps links are aggregated then the threshold value
     * of the UINT4 variable is reached. */
    if (u4DistPortSpeed < LA_32BIT_MAX)
    {
        if ((u4DistPortSpeed <= CFA_ENET_SPEED_100M) && (u2DistPortCount <= 40))
        {
            pAggEntry->u4AggSpeed = u2DistPortCount * (u4DistPortSpeed);
            pAggEntry->u4AggHighSpeed = 0;
        }

        else if ((u4DistPortSpeed <= CFA_ENET_SPEED_1G) &&
                 (u2DistPortCount <= 4))
        {
            pAggEntry->u4AggSpeed = u2DistPortCount * (u4DistPortSpeed);
            pAggEntry->u4AggHighSpeed = 0;
        }

        else
        {
            pAggEntry->u4AggSpeed = LA_32BIT_MAX;
            pAggEntry->u4AggHighSpeed = u2DistPortCount *
                (u4DistPortSpeed / 1000000);
        }
    }
    else
    {
        pAggEntry->u4AggSpeed = LA_32BIT_MAX;
        pAggEntry->u4AggHighSpeed = u2DistPortCount * (u4DistPortHighSpeed);

    }

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGPrintBestConsInfo                        */
/*                                                                           */
/* Description        : This function will print the values of               */
/*                      Best Consolidated Info                               */
/*                                                                           */
/* Input(s)           : pAggEntry -Pointer to Aggregator Entry.              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaActiveDLAGPrintBestConsInfo (tLaLacAggEntry * pAggEntry)
{
    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGPrintBestConsInfo: "
                "NULL POINTER.\n");
        return;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "\n -------------------------------------------------------------\n");

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Best Speed       : %d \n",
                 pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4ConsRefSpeed /
                 CFA_1MB);
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Best High Speed  : %d \n",
                 pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.
                 u4ConsRefHighSpeed);
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "\n Best Mtu         : %d \n",
                 pAggEntry->LaDLAGConsInfoTable.LaDLAGCompProp.u4Mtu);
    LA_TRC (CONTROL_PLANE_TRC,
            "\n -------------------------------------------------------------\n");

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGCalculateAggOperStatus                   */
/*                                                                           */
/* Description        : This function will give the Aggregator Oper status   */
/*                      based on the ports that are in Sync & Dist           */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregator Entry.             */
/*                      u1AggAdminStatus - Admin status of port-chanenl      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaActiveDLAGCalculateAggOperStatus (tLaLacAggEntry * pAggEntry,
                                    UINT1 u1AggAdminStatus)
{

    tLaLacPortEntry    *pPortEntry = NULL;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGCalculateAggOperStatus: "
                "NULL POINTER.\n");
        return LA_ERR_NULL_PTR;
    }

    /* Make the AggOperStatus as Down if UP
       It will be made as Up in the following Loop  */
    if (pAggEntry->u1AggOperStatus == LA_OPER_UP)
    {
        pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
    }

    if (u1AggAdminStatus == CFA_IF_UP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            if ((pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
                 LA_TRUE) &&
                (pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization ==
                 LA_TRUE))
            {
                pAggEntry->u1AggOperStatus = LA_OPER_UP;
                break;
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

        if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            /* Get the Remote ports of this port channel */
            LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
            while (pConsPortEntry != NULL)
            {
                if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
                {
                    pAggEntry->u1AggOperStatus = LA_OPER_UP;
                    break;
                }

                LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                              pAggEntry);
            }
        }

    }
    else
    {
        if (pAggEntry->u1AggOperStatus == LA_OPER_UP)
        {
            pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGInitParams                               */
/*                                                                           */
/* Description        : This function initializes the global variables used  */
/*                      by active D-LAG                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.GlobalDLAGInfo                         */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*****************************************************************************/

INT4
LaActiveDLAGInitParams ()
{

    /* Initialize the DLAG System and Master Down
     * and Role played */

    LA_DLAG_SYSTEM_STATUS = LA_DLAG_DISABLED;

    LA_AA_DLAG_MASTER_DOWN = LA_FALSE;

    LA_AA_DLAG_ROLE_PLAYED = LA_DLAG_DISABLED;

    /* Get the Role from DISS or NPSIM */
    LA_AA_DLAG_ROLE_PLAYED = LaActiveDLAGGetRolePlayed ();

    /* Initialize the global DLAG params */

    LA_MEMSET (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr), 0,
               sizeof (tLaMacAddr));

    LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority =
        LA_DEFAULT_SYSTEM_PRIORITY;

    LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingIfIndex = 0;

    LA_MEMSET ((LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingOperPortList), 0,
               sizeof (tLaPortList));
    LA_MEMSET ((LA_DLAG_GLOBAL_INFO.GlobalDLAGDistributingPortList), 0,
               sizeof (tLaPortList));

    /* Global timer related Initializations */
    LA_DLAG_GLOBAL_INFO.u4GlobalDLAGPeriodicSyncTime =
        LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME;

    LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr.LaTmrFlag = LA_TMR_STOPPED;

    LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr.pEntry =
        (VOID *) (&(LA_DLAG_GLOBAL_INFO));

    LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr.LaAppTimer.u4Data =
        LA_AA_DLAG_PERIODIC_TMR_TYPE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGScanLaListAndInitDLAG.                   */
/*                                                                           */
/* Description        : This function is called if Global DLAG is enabled    */
/*                      to copy the Global DLAG properties as their System   */
/*                      properties and Start the DLAG functionlity in all    */
/*                      the Aggregators in the List.                         */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaActiveDLAGScanLaListAndInitDLAG (VOID)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        /* Init Active-Active DLAG in All Nodes */
        LaActiveCopyGlobalAndInitDLAG (pAggEntry);

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }
}

/*****************************************************************************/
/* Function Name      : LaActiveCopyGlobalAndInitDLAG                        */
/*                                                                           */
/* Description        : This function is called if Global DLAG is enabled    */
/*                      to copy the Global DLAG properties as their System   */
/*                      properties and Start the DLAG functionlity in that   */
/*                      Aggregator entry                                     */
/*                                                                           */
/* Input(s)           : pAggEntry - Aggregator entry in which DLAG enabled   */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
LaActiveCopyGlobalAndInitDLAG (tLaLacAggEntry * pAggEntry)
{

    if (pAggEntry == NULL)
    {
        return LA_ERR_NULL_PTR;
    }

    /* Copy the Global Mac as DLAG Mac and Actor System Mac for connecting with Peer */
    LA_MEMCPY (pAggEntry->DLAGSystem.SystemMacAddr,
               LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);

    pAggEntry->DLAGSystem.u2SystemPriority =
        LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority;

    /* Disable redundancy if enabled */

    pAggEntry->u1DLAGRedundancy = LA_DLAG_REDUNDANCY_OFF;

    /* Stop D-LAG Periodic sync timer */
    if (LaStopTimer (&pAggEntry->DLAGPeriodicSyncTmr) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: LaStopTimer failed for DLAGPeriodicSyncTmr\n");
        return LA_FAILURE;
    }

    if (LaDLAGInit (pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }

    pAggEntry->u1DLAGStatus = LA_DLAG_ENABLED;

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGDisable                                   */
/*                                                                            */
/* Description        : This function is called to Disable the functionlities */
/*                      when Global DLAG Status is Disabled                   */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
INT4
LaActiveDLAGDisable ()
{
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = LA_INVALID_HW_AGG_IDX;
#endif
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;

    LaGetNextAggEntry (pAggEntry, &pAggEntry);

    /* Since we are disabling Active-DLAG in the entire system
     * we need to flush all the entries in H/w + HL 
     * 1) We need to call the Disable distributor for the
     *    acknowledged port and clear H/L.
     * 2) Delete Aggregator in H/w as this contains n number of
     *    entriesof remote ports which we don't know.
     * 3) Enable normal LAG functionilty by resetting sys ID &
     *    Sys Priority
     */

    while ((pAggEntry != NULL) && (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED))
    {

        LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);

        while (pPortEntry != NULL)
        {
            if (pPortEntry->u1DLAGMasterAck == LA_TRUE)
            {
                /* The ports that are Acknolwdged by Master
                   only was given to H/L . So remove those
                   ports. So that Dist portcount also reduced */
                LaActiveDLAGHwControl (pPortEntry,
                                       LA_HW_EVENT_DISABLE_DISTRIBUTOR);
            }

            if (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG)
            {
                pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }

        /* Reset the Distributing port count to 0 and indicate to Hl and Ll */

        if (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            pAggEntry->u1DistributingPortCount = 0;

            if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
            {
                pAggEntry->u1AggOperStatus = LA_OPER_DOWN;

                /* Get the time of last oper change */
                LA_GET_SYS_TIME (&(pAggEntry->AggTimeOfLastOperChange));
                /* Indicate to bridge */
                LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                             "\nDLAG: Line Protocol on Port Channel %d changed down",
                             pAggEntry->AggConfigEntry.u2ActorAdminKey);

                LaL2IwfLaHLPortOperIndication (pAggEntry->u2AggIndex,
                                               LA_OPER_DOWN);

            }
            pAggEntry->bHlIndicated = LA_FALSE;
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "DLAG: AggIndex: %u OPER DOWN Indication to BRIDGE\n",
                         pAggEntry->u2AggIndex);
            LaUpdateLlPortChannelStatus (pAggEntry);
        }

        /* Since this will behave as normal scenario in LACP
           Creating Agg Group will be done in ENABLE_DISTRIBUTOR in LaLacHwControl */

        LaDLAGDeInit (pAggEntry);

        /* Copy the GLobal Distributing port list, Periodic sysn time for port channel */
        LA_MEMSET ((pAggEntry->DLAGSystem.SystemMacAddr), LA_INIT_VAL,
                   sizeof (tLaMacAddr));
        pAggEntry->DLAGSystem.u2SystemPriority =
            LA_DLAG_DEFAULT_SYSTEM_PRIORITY;

        pAggEntry->u1DLAGStatus = LA_DLAG_DISABLED;

#ifdef NPAPI_WANTED
        /* Delete all the entries from H/w */
        if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
        {
            if (pAggEntry != NULL)
            {
                LaFsLaHwDeleteAggregator (pAggEntry->u2AggIndex);
                /* Aggregator is removed from hw, syncup this info with
                 * standby node.*/
                LaRedSyncModifyAggPortsInfo (LA_NP_DEL_INFO,
                                             pAggEntry->u2AggIndex, 0);
                LaFsLaHwCreateAggGroup (pAggEntry->u2AggIndex, &u2HwAggIndex);
                LaFsLaHwSetSelectionPolicyBitList (pAggEntry->u2AggIndex,
                                                   pAggEntry->AggConfigEntry.
                                                   u4LinkSelectPolicyBitList);

                LaAggCreated (pAggEntry->u2AggIndex, u2HwAggIndex);
            }
        }
#endif
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

#ifdef NPAPI_WANTED
    /* Delete all the entries from H/w */
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (FNP_FAILURE == LaFsLaHwDlagStatus (LA_DLAG_DISABLED))
        {
            LA_TRC (INIT_SHUT_TRC, "LaActiveDLAGEnable FAILED\n");
            return LA_FAILURE;
        }
    }
#endif

    /* Stop the global timer */
    if ((LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr))) !=
        LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: LaStopTimer failed for Global D-LAG "
                "Periodic Sync Timer\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/******************************************************************************/
/* Function Name      : LaActiveDLAGEnable                                    */
/*                                                                            */
/* Description        : This function is called to Enable the functionalites  */
/*                      when Global DLAG status is enabled                    */
/*                                                                            */
/* Input(s)           : None                                                  */
/* Output(s)          : None                                                  */
/* Return Value(s)    : None                                                  */
/******************************************************************************/
INT4
LaActiveDLAGEnable ()
{

    if (LA_SLL_COUNT (LA_AGG_SLL) != 0)
    {
        /* If no port-channels present no need of starting timer */
        LaActiveDLAGScanLaListAndInitDLAG ();

        if ((LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr))) !=
            LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "TMR: LaStartTimer failed for Global D-LAG Periodic Sync Timer\n");
            return LA_FAILURE;
        }
    }

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (FNP_FAILURE == LaFsLaHwDlagStatus (LA_DLAG_ENABLED))
        {
            LA_TRC (INIT_SHUT_TRC, "LaActiveDLAGEnable FAILED\n");
            return LA_FAILURE;
        }
    }
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGTxPeriodicSyncOrAddDelPdu                */
/*                                                                           */
/* Description        : This function is called on D-LAG periodic sync timer */
/*                      expiry to send D-LAG Periodic sync/update PDU.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGTxPeriodicSyncOrAddDelPdu ()
{

    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4Count = 0;
    tLaBoolean          bSyncPduRequired = LA_FALSE;
    tLaBoolean          bUpdatePduRequired = LA_FALSE;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    LaGetNextAggEntry (NULL, &pAggEntry);

    /* Check any of port-channel contains atleast one port
     * then we can send periodic sync PDU */

    while (pAggEntry != NULL)
    {
        if (pAggEntry->u1ConfigPortCount > 0)
        {
            bSyncPduRequired = LA_TRUE;
        }

        /* Get the Total number of Port Entry in Consolidation Logic */
        RBTreeCount (pAggEntry->LaDLAGConsInfoTable.LaDLAGConsolidatedList,
                     &u4Count);

        if (u4Count != 0)
        {
            bUpdatePduRequired = LA_TRUE;
        }

        if ((bSyncPduRequired == LA_TRUE) && (bUpdatePduRequired == LA_TRUE))
        {
            break;
        }
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    if ((LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_SLAVE) &&
        (bSyncPduRequired == LA_TRUE))
    {
        /* If System Role is Master then No need to send Periodic 
         * Sync PDU */

        /* This sends a consolidated Periodic Sync Pdu */

        if ((LaActiveDLAGTxDSU (NULL, NULL) != LA_SUCCESS))
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGTxPeriodicSyncOrAddDelPdu: D-LAG Periodic Sync PDU"
                    " Tx failed for System : \n");
        }
    }

    else if ((LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER) &&
             (bUpdatePduRequired == LA_TRUE))
    {
        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE;

        if ((LaActiveDLAGTxDSU (NULL, &LaDLAGTxInfo) != LA_SUCCESS))
        {
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGTxPeriodicSyncOrAddDelPdu: D-LAG Periodic Update PDU"
                    " Tx failed for System : \n");
        }

    }

    if ((LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr))) !=
        LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "TMR: LaStartTimer failed for Global D-LAG Periodic Sync Timer\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGTxDSU                                    */
/*                                                                           */
/* Description        : This function Forms and Sends a D-LAG.               */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to Aggregator entry              */
/*                      pLaDLAGTxInfo - Type of Pdu                          */
/*                      If NULL - Periodic Sync Pdu                          */
/*                      Else    - Periodic Updated Pdu                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/

INT4
LaActiveDLAGTxDSU (tLaLacAggEntry * pAggEntry, tLaDLAGTxInfo * pLaDLAGTxInfo)
{
#ifdef NPAPI_WANTED
    tIcchInfo           IcchInfo;
#endif
    tLaBufChainHeader  *pMsgBuf = NULL;
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4Count = 0;
    UINT4               u4DataLength = 0;
    UINT1              *pu1LinearBuf = NULL;
    UINT1               u1OperStatus = CFA_IF_UP;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bDlagMsgToMaster = OSIX_FALSE;

    LA_TRC (CONTROL_PLANE_TRC, "Entering LaActiveDLAGTxDSU. \n");
#ifdef NPAPI_WANTED
    LA_MEMSET (&IcchInfo, 0, sizeof (tIcchInfo));
#endif

    if (pLaDLAGTxInfo == NULL)
    {
        /* Explicilty pass the pointer for pAggEntry and pLaDLAGTxInfo as
         * NULL as this is Full consolidated periodic Sync PDU */
        if ((LaActiveDLAGFormDSU (NULL, &pu1LinearBuf, NULL, &u4DataLength)) !=
            LA_SUCCESS)
        {

            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveDLAGFormDSU failed. \n");
            return LA_FAILURE;
        }
        bDlagMsgToMaster = OSIX_TRUE;
    }
    else if (pLaDLAGTxInfo->u1DSUType ==
             LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE)
    {
        /* Explicilty pass the pointer for pAggEntry as NULL 
         * for this full consolidated periodic Update Pdu */
        if ((LaActiveDLAGFormAddDeleteDSU
             (NULL, &pu1LinearBuf, pLaDLAGTxInfo, &u4DataLength)) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveDLAGTxDSU:LaActiveDLAGFormAddDeleteDSU() failed. \n");
            return LA_FAILURE;
        }
        bDlagMsgToMaster = OSIX_FALSE;
    }

    else if ((pAggEntry != NULL)
             && (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER))
    {
        if (((pLaDLAGTxInfo->u1DSUType ==
              LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE)
             || (pLaDLAGTxInfo->u1DSUType == LA_AA_DLAG_ADD_DEL_UPDATE_ONLY)))
        {
            /* Separte LACPU for Add/Del message */
            if ((LaActiveDLAGFormAddDeleteDSU
                 (pAggEntry, &pu1LinearBuf, pLaDLAGTxInfo,
                  &u4DataLength)) != LA_SUCCESS)
            {
                LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "LaActiveDLAGTxDSU:LaActiveDLAGFormAddDeleteDSU() failed. \n");

                return LA_FAILURE;
            }
        }
        else
        {
            /* Other Sync PDU's and Event PDU's. Avoid it as this is Master */
            return LA_SUCCESS;

        }
        bDlagMsgToMaster = OSIX_FALSE;
    }
    else if ((pAggEntry != NULL)
             && (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE))
        /* other Event update Messages.Sent only by Slaves */
    {
        if ((LaActiveDLAGFormDSU
             (pAggEntry, &pu1LinearBuf, pLaDLAGTxInfo,
              &u4DataLength)) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "LaActiveDLAGTxDSU: LaActiveDLAGFormDSU() failed.\n");
            return LA_FAILURE;
        }
        bDlagMsgToMaster = OSIX_TRUE;
    }

    if (LA_USE_DIST_PORT_OR_NOT
        (LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount) == LA_TRUE)
    {
        while (u4Count <
               LA_DLAG_GLOBAL_INFO.u4GlobalDLAGDistributingPortListCount)
        {
            OSIX_BITLIST_IS_BIT_SET (LA_DLAG_GLOBAL_INFO.
                                     GlobalDLAGDistributingOperPortList,
                                     u4DLAGDistributingIfIndex,
                                     LA_PORT_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                /* If the bit is not set, continue the for loop */
                u4DLAGDistributingIfIndex++;
                continue;
            }
            u4Count++;
            /* Check If Distributing port is operationally UP,
             * If not then packet transmission attempt need not
             * be made on the failed distributing port, continue scanning the
             * remaining distribution port list*/

            CfaGetIfOperStatus (u4DLAGDistributingIfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_DOWN)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "LaActiveDLAGTxDSU: D-LAG PDU Tx is not possible as Distributing"
                             " port %d configured for this port channel is down.\n",
                             u4DLAGDistributingIfIndex);
                u4DLAGDistributingIfIndex++;
                continue;
            }

            /* Allocate memory for CRU buffer */
            if ((pMsgBuf = LA_ALLOC_CRU_BUF (u4DataLength, 0)) == NULL)
            {
                LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                        "LaActiveDLAGTxDSU: Buffer Allocation failed\n");
                return LA_FAILURE;
            }

            /* Copy from Linear buffer to CRU buffer */
            LA_COPY_OVER_CRU_BUF (pMsgBuf, pu1LinearBuf, 0, u4DataLength);

            LA_MEMSET (gLaGlobalInfo.gau1DLAGPdu, LA_INIT_VAL,
                       LA_MAX_DLAG_PDU_SIZE);
            /* Send the D-LAG PDU */
            LA_TRC (CONTROL_PLANE_TRC,
                    "LaActiveDLAGTxDSU: Handing over LACPDU to Lower Layer...\n");
            LaHandOverOutFrame (pMsgBuf, (UINT2) u4DLAGDistributingIfIndex,
                                NULL);

            u4DLAGDistributingIfIndex++;
        }
    }
    else
    {
#ifdef NPAPI_WANTED

        if (bDlagMsgToMaster == OSIX_FALSE)
        {
            if ((pLaDLAGTxInfo != NULL) &&
                (pLaDLAGTxInfo->u1DSUType ==
                 LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE) &&
                (pLaDLAGTxInfo->u4SlotId != 0))
            {

                IcchInfo.u4SlotId = pLaDLAGTxInfo->u4SlotId;
            }
            else
            {
                IcchInfo.u4SlotId = ICCH_TO_ALL_SLOTS;
            }
        }
        else
        {
            IcchInfo.u4SlotId = 0;
        }

        IcchInfo.pu1Data = pu1LinearBuf;
        IcchInfo.u2PktLength = u4DataLength;
        IcchInfo.u2AppId = ICCH_LA;

        ICCHHandOverTxFrame (&IcchInfo);
#else
        UNUSED_PARAM (bDlagMsgToMaster);
#endif

    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGTxEventDSU                               */
/*                                                                           */
/* Description        : This function Forms and Sends Event message of type  */
/*                      Add/Del or DB update                                 */
/*                                                                           */
/* Input(s)           : pAggEntry    - Port-channel which needs to transmit  */
/*                                     D-LAG PDU.                            */
/*                      pLaDLAGTxInfo - Tx Info.                             */
/*                         - Pointer to TxInfo Structure, to be sent in      */
/*                           update PDU.                                     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On NULL Ptr                        */
/*****************************************************************************/
INT4
LaActiveDLAGTxEventDSU (tLaLacAggEntry * pAggEntry,
                        tLaDLAGTxInfo * pLaDLAGTxInfo)
{

    if ((pAggEntry == NULL) || (pLaDLAGTxInfo == NULL))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGTxEventDSU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    if ((LaActiveDLAGTxDSU (pAggEntry, pLaDLAGTxInfo) != LA_SUCCESS))
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGTxEventDSU: D-LAG EventPDU"
                " Tx failed for System : \n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGKeepAlive                                */
/*                                                                           */
/* Description        : This function called when expiry of Periodic timer   */
/*                      and increments the keep alive count and verifies the */
/*                      keep alive count is reached the maximum. If maximum  */
/*                      reached then the node is deleted from the database   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/

INT4
LaActiveDLAGKeepAlive ()
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        /* Call Keep Alive for each Agg Entry */
        LaDLAGKeepAlive (pAggEntry);
        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGProcessSyncPDU                           */
/*                                                                           */
/* Description        : This function  called to process the periodic sync   */
/*                      Pdu. This traverse through entire Pdu and updates    */
/*                      D-LAG database.                                      */
/*                                                                           */
/* Input(s)           : RemoteSysMac                                         */
/*                                  - Remote System Mac address              */
/*                      u2RemoteSysPriority                                  */
/*                                  - Remote System Prioirty.                */
/*                      pu1LinearBuf                                         */
/*                                  - D-LAG Data Received from Remote D-LAG  */
/*                      i4SllAction - Action to be performed on remote aggr  */
/*                                    info list. Actions can be delete node  */
/*                                    delete remote port from a node/update  */
/*                                    list.                                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGProcessSyncPDU (tLaMacAddr RemoteSysMac,
                            UINT2 u2RemoteSysPriority, UINT1 *pu1LinearBuf,
                            INT4 i4SllAction)
{
    UINT1              *pu1PktBuf = NULL;
    UINT1               u1NoOfPortChannel = 0;
    UINT1               u1NoOfPorts = 0;
    UINT1               u1KeyFieldOffSet = 0;

    /* Mark the pointer in Local ponter */

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGProcessSyncPDU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

    while (u1NoOfPortChannel != 0)
    {

        LaActiveDLAGUpdateRemoteDLAGNodeList (RemoteSysMac, u2RemoteSysPriority,
                                              pu1PktBuf, i4SllAction);

        /* Since this is Local buffer we need to traverse through entire PDU
         * Logic.
         * 1) Port channel ports off set is 9 fileds from current position.
         *    Move the pointer to 9th position.(Current+ Key 3 + Mtu 2 +Speed 2+ Reserv 2)
         * 2) Since port no's can vary take the port no's field.
         *    Each port will have fixed data 8 bytes
         *    [Port no(4)+ Bundle state(1) + Sync(1)+prioirty(2)].
         *
         * 3) Next port-channel Key will be available at position (No of ports * 8).
         *    Move the pointer there. We will get the next port-channel Key.
         */
        pu1PktBuf += LA_AA_DLAG_SYNC_PDU_NO_OF_PORTS_OFFSET;

        LA_GET_1BYTE (u1NoOfPorts, pu1PktBuf);

        u1KeyFieldOffSet = (UINT1)
            (u1NoOfPorts * (UINT1) LA_AA_DLAG_SYNC_PDU_PER_PORT_INFO_LEN);

        pu1PktBuf += u1KeyFieldOffSet;

        u1NoOfPortChannel--;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGProcesscPeriodicUpdateDSU                */
/*                                                                           */
/* Description        : This function  called to process the periodic sync   */
/*                      Update PDU from Master.This traverse through entire  */
/*                      Pdu and updates D-LAG database.                      */
/*                                                                           */
/* Input(s)           : pu1LinearBuf                                         */
/*                                  - D-LAG Data Received from Remote D-LAG  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/

INT4
LaActiveDLAGProcessPeriodicUpdateDSU (UINT1 *pu1LinearBuf)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT1              *pu1PktBuf = NULL;
    UINT4               u4Key = 0;
    UINT1               u1NoOfPortChannel = 0;
    UINT1               u1NoOfPorts = 0;
    UINT1               u1KeyFieldOffSet = 0;

    /* Mark the pointer in Local ponter */

    if (pu1LinearBuf == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC,
                "LaActiveDLAGProcessPeriodicUpdateDSU: Null pointer is passed.\n");
        return LA_ERR_NULL_PTR;
    }

    pu1PktBuf = pu1LinearBuf;

    LA_GET_1BYTE (u1NoOfPortChannel, pu1PktBuf);

    while (u1NoOfPortChannel != 0)
    {
        LA_GET_4BYTE (u4Key, pu1PktBuf);
        /* Get the Aggregation Entry */
        LaGetAggEntryByKey ((UINT2) u4Key, &pAggEntry);
        if (pAggEntry != NULL)
        {
            if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE)

            {
                /* Process Add-Del PDU and program H/w */
                LaActiveDLAGGetAddDeletePortFromDSU (pAggEntry, pu1PktBuf,
                                                     LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE);
            }
        }
        else
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "LaActiveDLAGProcessPeriodicUpdateDSU: Port-Channel %d not exist \n",
                         u4Key);
        }

        /* Get Delete Type field */
        pu1PktBuf += LA_AA_DLAG_ADD_OR_DEL_FIELD_LEN;

        LA_GET_1BYTE (u1NoOfPorts, pu1PktBuf);

        u1KeyFieldOffSet = (UINT1)
            (u1NoOfPorts * (UINT1) LA_AA_DLAG_PORT_NUM_LEN);

        pu1PktBuf += u1KeyFieldOffSet;

        /* Get Add Type field */
        pu1PktBuf += LA_AA_DLAG_ADD_OR_DEL_FIELD_LEN;

        LA_GET_1BYTE (u1NoOfPorts, pu1PktBuf);

        u1KeyFieldOffSet = (UINT1)
            (u1NoOfPorts * (UINT1) LA_AA_DLAG_PORT_NUM_LEN);

        pu1PktBuf += u1KeyFieldOffSet;

        u1NoOfPortChannel--;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaActiveDLAGHandleCardDelete                         */
/*                                                                           */
/* Description        : This function called when expiry of Global Periodic  */
/*                      timer to check any of Remote DLAG Node is Down. This */
/*                      is called for every aggregator in the System.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On NULL Ptr                        */
/*****************************************************************************/
INT4
LaActiveDLAGHandleCardDelete (INT4 i4SlotId)
{
    tLaBoolean          bUpdateDSURequired = LA_FALSE;
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            RemoteSlotMac = { 0 };
    tLaDLAGRemoteAggEntry *pRemoteAggEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT1               au1ZeroMac[LA_MAC_ADDRESS_SIZE];
    UINT4               u4MasterSlotId = 0;

    MEMSET (au1ZeroMac, 0, LA_MAC_ADDRESS_SIZE);

    LaGetDissMasterSlotId (&u4MasterSlotId);

    if ((i4SlotId == (INT4) u4MasterSlotId) && (i4SlotId != IssGetSwitchid ()))
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "MASTER [%d] DETACHED... \r\n",
                     i4SlotId);
        if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
        {

            LaActiveDLAGMasterDownIndication ();
        }
    }

    if (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_MASTER
        && (i4SlotId != IssGetSwitchid ()))
    {
        LaGetMacAddressFromSlotId ((UINT4) i4SlotId, &RemoteSlotMac);

        if (MEMCMP (au1ZeroMac, RemoteSlotMac, LA_MAC_ADDRESS_SIZE) == 0)
        {
            LA_TRC (CONTROL_PLANE_TRC, "LaActiveDLAGHandleCardDelete"
                    "Remote Mac is Empty.\n");
            return LA_FAILURE;
        }

        LaGetNextAggEntry (NULL, &pAggEntry);

        while (pAggEntry != NULL)
        {
            /* Search Remote D-LAG Node Info List */
            LaDLAGGetRemoteAggEntryBasedOnMac (RemoteSlotMac, pAggEntry,
                                               &pRemoteAggEntry);

            if (pRemoteAggEntry != NULL)
            {
                /* Delete remote aggregator */
                LaDLAGDeleteRemoteAggregator (pAggEntry, pRemoteAggEntry);

                if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
                    (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER))
                {
                    /* Trigger consolidation logic so that new Best info
                       is selected */
                    LaActiveDLAGChkForBestConsInfo (pAggEntry);
                    LaActiveDLAGSortOnStandbyConstraints (pAggEntry);

                    bUpdateDSURequired =
                        LaActiveDLAGChkAddDelDSURequired (pAggEntry);

                    if (bUpdateDSURequired == LA_TRUE)
                    {
                        LaDLAGTxInfo.u1DSUType = LA_AA_DLAG_ADD_DEL_UPDATE_ONLY;
                        LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
                    }
                }
                pRemoteAggEntry = NULL;
            }
            LaGetNextAggEntry (pAggEntry, &pAggEntry);
        }
    }

    return LA_SUCCESS;
}
