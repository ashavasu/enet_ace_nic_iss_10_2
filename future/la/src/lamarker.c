/*****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: lamarker.c,v 1.17 2014/06/26 10:48:36 siva Exp $
* Licensee Aricent Inc., 2001-2002
*****************************************************************************/
/*    FILE  NAME            : lamarker.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Link Aggregation                               */
/*    MODULE NAME           : LA Marker protocol                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains functions regarding the use */
/*                            of Marker protocol i.e.Marker Generator, Marker*/
/*                            Receiver, Marker Responder.                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaAggGenerateMarkerPdu                               */
/*                                                                           */
/* Description        : This function is called by the LAC module whenever a */
/*                      a port needs to be detached from an Aggregator and   */
/*                      a conversation needs to shifted from one link to     */
/*                      another. This functions forms the Marker PDU and     */
/*                      sends it out on the port/link specified.             */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the Port Entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.pLaCacheTable                          */
/*                      gLaGlobalInfo.pLaMarkerTable                         */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggGenerateMarkerPdu (tLaLacPortEntry * pPortEntry)
{
    tLaBufChainHeader  *pMarkerRequestPdu = NULL;
    tLaMacAddr          LaSrcMacAddress;
    UINT2               u2AggIndex = 0;
    UINT4               u4TransactionId = 0;
    UINT4               u4CacheHashIndex = 0;
    tLaCacheEntry      *pLaCacheEntry = NULL;
    tLaMarkerEntry     *pLaMarkerEntry = NULL;
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;

    LA_MEMSET (&LaSrcMacAddress, 0, sizeof (tLaMacAddr));
    if (pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "MARK: Null Pointer\n");
        return LA_FAILURE;
    }

    /* Getting the Hardware Address of the port which is to be filled the Source
     * Address field of the Marker PDU */
    LA_MEMCPY (&LaSrcMacAddress, pPortEntry->au1MacAddr, LA_MAC_ADDRESS_SIZE);

    /* Get the Aggregator Index corresponding to this port */
    if (pPortEntry->pAggEntry == NULL)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MARK: Port %d not attached to any Aggregator\n",
                     pPortEntry->u2PortIndex);
        return LA_FAILURE;
    }
    u2AggIndex = pPortEntry->pAggEntry->u2AggIndex;

    /* Get the Next unique Transaction Id to tbe used for the Marker PDU */
    LaAggGetNextTransactionId (u2AggIndex, &u4TransactionId);

    /* Now allocate a CRU buffer message chain for this Marker PDU */
    if ((pMarkerRequestPdu = LA_ALLOC_CRU_BUF (LA_MARKER_PDU_SIZE, LA_OFFSET))
        == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "MARK: Marker Request CRU Buffer Allocation FAILED\n");
        return LA_FAILURE;
    }

    if (LaAggFormMarkerRequestPdu (&LaSrcMacAddress,
                                   pPortEntry->u2PortIndex,
                                   u4TransactionId,
                                   pMarkerRequestPdu) != LA_SUCCESS)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "MARK: Forming Marker PDU FAILED\n");
        if (LA_RELEASE_CRU_BUF (pMarkerRequestPdu, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Request CRU Buffer Release FAILED\n");
        }
        return LA_FAILURE;
    }

    /* We need to update the bMarkerStatus field in the CacheEntry Table for all
     * entries matching this PortIndex on which the Marker PDU is being sent out */

    LA_HASH_SCAN_TBL (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex)
    {
        pLaCacheEntry = (tLaCacheEntry *) LA_HASH_GET_FIRST_BUCKET_NODE
            (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex);

        while (pLaCacheEntry != NULL)
        {
            if (pLaCacheEntry->u2PortIndex == pPortEntry->u2PortIndex)
            {
                pLaCacheEntry->bMarkerStatus = LA_TRUE;
            }

            pLaCacheEntry = (tLaCacheEntry *) LA_HASH_GET_NEXT_BUCKET_NODE
                (gLaGlobalInfo.pLaCacheTable,
                 u4CacheHashIndex, &pLaCacheEntry->NextCacheEntry);
        }
    }

    /* We need to add an entry in the MarkerEntry Table for this PortIndex */

    if (LA_MARKERENTRY_ALLOC_MEM_BLOCK (pLaMarkerEntry) == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "MARK: Marker Entry Memory Block Allocation FAILED\n");
        if (LA_RELEASE_CRU_BUF (pMarkerRequestPdu, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Request CRU Buffer Release FAILED\n");
        }
        return LA_FAILURE;
    }

    LA_MEMSET (pLaMarkerEntry, 0, sizeof (tLaMarkerEntry));

    pLaMarkerEntry->u4TransactionId = u4TransactionId;

    pLaMarkerEntry->LaMarkerTmr.pEntry = (VOID *) pPortEntry;
    pLaMarkerEntry->LaMarkerTmr.LaAppTimer.u4Data = LA_MARKER_TMR_TYPE;
    pLaMarkerEntry->LaMarkerTmr.LaTmrFlag = LA_TMR_STOPPED;

    if (LaStartTimer (&(pLaMarkerEntry->LaMarkerTmr)) != LA_SUCCESS)
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MARK: Starting Marker Timer for Port %d FAILED\n",
                     pPortEntry->u2PortIndex);
        if (LA_RELEASE_CRU_BUF (pMarkerRequestPdu, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Request CRU Buffer Release FAILED\n");
        }
        if (LA_MARKERENTRY_FREE_MEM_BLOCK (pLaMarkerEntry) != LA_MEM_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Entry Memory Block Release FAILED\n");
        }
        pLaMarkerEntry = NULL;

        return LA_FAILURE;
    }

    pPortEntry->pLaMarkerEntry = pLaMarkerEntry;

    /* This function is called to ensure that no more than 5 frames are sent in
     * any one-second period */

    if (LaAggMaintainMarkerRate (u2AggIndex, LA_MARKER_GET) == LA_SUCCESS)
    {
        LaAggMultiplexFrame (pMarkerRequestPdu, pPortEntry, NULL, LA_FALSE);

        /* Increment the number of Marker PDUs Transmitted */
        (pPortEntry->LaPortStats.u4MarkerPduTx)++;

        /* Increment the Aggregator If Statistics */
        LaGetAggIfEntry (u2AggIndex, &pLaLacAggIfEntry);

        if (pLaLacAggIfEntry == NULL)
        {
            return LA_FAILURE;
        }

        if (pPortEntry->LaAggAttached == LA_TRUE)
        {

            /* We increment all the statistics negatively for the Aggregator,
             * because these frames have already been counted once, at the
             * port level */
            pLaLacAggIfEntry->i4AggOutOctets += -(LA_SLOW_PROTOCOL_SIZE);
            (pLaLacAggIfEntry->i4AggOutFrames)--;
            (pLaLacAggIfEntry->i4AggOutMcastPkts)--;
        }
    }                            /* End of LaAggMaintainMarkerRate function returning Success */
    else
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "MARK: LaAggMaintainMarkerRate function FAILED\n");

        if (LA_RELEASE_CRU_BUF (pMarkerRequestPdu, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Request CRU Buffer Release FAILED\n");
        }

        if (LaStopTimer (&(pLaMarkerEntry->LaMarkerTmr)) != LA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "MARK: Stopping Marker Timer for Port %d FAILED\n",
                         pPortEntry->u2PortIndex);
        }

        if (LA_MARKERENTRY_FREE_MEM_BLOCK (pLaMarkerEntry) != LA_MEM_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Entry Memory Block Release FAILED\n");
        }

        pLaMarkerEntry = pPortEntry->pLaMarkerEntry = NULL;

        return LA_FAILURE;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "MARK: Marker Request PDU generated SUCESSFULLY\n");

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggFormMarkerRequestPdu                            */
/*                                                                           */
/* Description        : This function is called by the GenerateMarkerPdu     */
/*                      function. This function forms the linear PDU, copies */
/*                      it into a CRU Buffer and returns the pointer to the  */
/*                      CRU buffer.                                          */
/*                                                                           */
/* Input(s)           : pLaSrcmacAddr - Pointer to the Source MAC Address to */
/*                                      be used in the Marker PDU            */
/*                     u2PortIndex - Index of the port                       */
/*                     u4TransactionId - TransactionId of the Marker Request */
/*                                        to be used in the Marker PDU       */
/*                     pMarkerRequestPdu - Pointer to the Marker Request Pdu */
/*                                         which is to be sent               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggFormMarkerRequestPdu (tLaMacAddr * pLaSrcMacAddress,
                           UINT2 u2PortIndex, UINT4 u4TransactionId,
                           tLaBufChainHeader * pMarkerRequestPdu)
{
    UINT1               au1Pkt[LA_MARKER_PDU_SIZE];
    UINT1              *pu1Pkt;
    UINT2               u2SpmaWord;
    UINT2               u2Pad;
    UINT4               u4SpmaDword;
    UINT2               u2SpType;
    UINT1               u1MarkerSubtype;

    if (pLaSrcMacAddress == NULL || pMarkerRequestPdu == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "MARK: Null Pointer\n");
        return LA_FAILURE;
    }

    pu1Pkt = (UINT1 *) au1Pkt;

    /* Filling in the various Marker fields into the Marker PDU buffer */

    LA_MEMSET (pu1Pkt, 0, LA_MARKER_PDU_SIZE);

    /* Getting the Slow Protocols Multicast Address from the structure */
    u4SpmaDword = LA_SLOW_PROT_ADDR_DWORD;
    u2SpmaWord = LA_SLOW_PROT_ADDR_WORD;
    u2SpType = LA_SLOW_PROT_TYPE;

    /* Filling in the Slow Protocol Multicast Address in the linear buffer */
    LA_PUT_4BYTE (pu1Pkt, u4SpmaDword);
    LA_PUT_2BYTE (pu1Pkt, u2SpmaWord);

    /* Filling in the Address of the port on which the Marker PDU has to be sent, as
     * the Source Address in the Marker Request PDU linear buffer */
    LA_MEMCPY (pu1Pkt, pLaSrcMacAddress, LA_MAC_ADDRESS_SIZE);
    pu1Pkt += LA_MAC_ADDRESS_SIZE;

    /* Filling in the Slow Protocol Type field in the linear buffer */
    LA_PUT_2BYTE (pu1Pkt, u2SpType);

    /* Filling in the Subtype field which indicates the Slow Protocol being
     * encapsulated in the linear buffer */
    u1MarkerSubtype = LA_MARKER_SUBTYPE;
    LA_PUT_1BYTE (pu1Pkt, u1MarkerSubtype);

    /* Filling in the Marker Protocol Version Number in the linear buffer */
    *pu1Pkt = LA_MARKER_VERSION_NUM;
    pu1Pkt += 1;

    /* Filling in the TlvType as Marker Request PDU, in the linear buffer */
    *pu1Pkt = LA_MARKER_INFORMATION;
    pu1Pkt += 1;

    /* Filling in the Marker Information Length in the linear buffer */
    *pu1Pkt = LA_MARKER_INFO_LENGTH;
    pu1Pkt += 1;

    /* Filling in the Requester port index in the linear buffer */
    LA_PUT_2BYTE (pu1Pkt, u2PortIndex);

    /* Filling in the Requester System Id encoded as a MAC Address */
    LA_MEMCPY (pu1Pkt, &(gLaGlobalInfo.LaSystem.SystemMacAddr),
               LA_MAC_ADDRESS_SIZE);
    pu1Pkt += LA_MAC_ADDRESS_SIZE;

    /* Filling in the Requester Transaction Id in the linear buffer */
    LA_PUT_4BYTE (pu1Pkt, u4TransactionId);

    /* Filling in the Pad field (zeroes) for 2 bytes in the linear buffer */
    u2Pad = LA_MARKER_PAD;
    LA_PUT_2BYTE (pu1Pkt, u2Pad);

    /* Filling in the Terminator Type field in the linear buffer */
    *pu1Pkt = LA_MARKER_TERMINATOR_TYPE;
    pu1Pkt += 1;

    /* Filling in the Terminator length in the linear buffer */
    *pu1Pkt = LA_MARKER_TERMINATOR_LENGTH;
    pu1Pkt += 1;

    /* The next 90 octets are Reserved field, to be filled in as zeroes. These have
     * already been memset as zeroes. The last 4 bytes are for FCS which will be
     * filled in by the underlying MAC layer (i.e. CFA) */

    /* Now copy the Marker PDU from the linear buffer au1Pkt into the CRU buffer */
    if (LA_COPY_OVER_CRU_BUF (pMarkerRequestPdu, au1Pkt, LA_OFFSET,
                              LA_MARKER_PDU_SIZE) == LA_CRU_FAILURE)
    {
        LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "MARK: Marker Request PDU Copy Over CRU Buffer FAILED\n");
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggRespondToMarkerRequest                          */
/*                                                                           */
/* Description        : This function is called by the Aggregator Parser     */
/*                      whenever a Marker PDU has been received and a Marker */
/*                      Response PDU needs to be generated                   */
/*                                                                           */
/* Input(s)           : pMarkerRequestPdu - Pointer to the Marker Request Pdu*/
/*                      pPortEntry - Pointer to the port entry          */
/*                      pLaLacAggIfEntry - Pointer to the Aggregator If entry*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaLacPortEntry.aLaLacPortEntry                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggRespondToMarkerRequest (UINT1 *pMarkerRequestPdu,
                             tLaLacPortEntry * pPortEntry,
                             tLaLacAggIfEntry * pLaLacAggIfEntry)
{

    UINT1              *pMarkerPdu;
    tLaBufChainHeader  *pMarkerResponsePdu = NULL;
    tLaMacAddr          LaSrcMacAddress;

    LA_MEMSET (&LaSrcMacAddress, 0, sizeof (tLaMacAddr));

    if (pMarkerRequestPdu == NULL || pPortEntry == NULL ||
        pLaLacAggIfEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "MARK: Null Pointer\n");
        return LA_FAILURE;
    }

    pMarkerPdu = pMarkerRequestPdu;

    /* Getting the Hardware Address of the port which is to be filled the Source
     * Address field of the Marker PDU */
    LA_MEMCPY (&LaSrcMacAddress, pPortEntry->au1MacAddr, LA_MAC_ADDRESS_SIZE);

    pMarkerPdu += LA_ETH_ADDR_OFFSET;
    LA_MEMCPY (pMarkerPdu, &LaSrcMacAddress, LA_MAC_ADDRESS_SIZE);
    pMarkerPdu += LA_MAC_ADDRESS_SIZE;

    /* Incrementing the pointer by 3 bytes in order to update the 
     * Marker Version Number */

    pMarkerPdu += LA_MARKER_VER_OFFSET;
    *pMarkerPdu = LA_MARKER_VERSION_NUM;
    pMarkerPdu++;

    *pMarkerPdu = LA_MARKER_RESPONSE_INFORMATION;

    /* Allocating a CRU Buffer for the Marker Response PDU */
    if ((pMarkerResponsePdu = LA_ALLOC_CRU_BUF (LA_MARKER_PDU_SIZE, LA_OFFSET))
        == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "MARK: Marker Response CRU Buffer Allocation FAILED\n");
        return LA_FAILURE;
    }

    /* Copying the Marker Response PDU into a CRU Buffer */
    if (LA_COPY_OVER_CRU_BUF (pMarkerResponsePdu, pMarkerRequestPdu, LA_OFFSET,
                              LA_MARKER_PDU_SIZE) == LA_CRU_FAILURE)
    {

        LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "MARK: Marker Response PDU Copy Over CRU Buffer FAILED\n");

        if (LA_RELEASE_CRU_BUF (pMarkerResponsePdu, LA_FALSE) != LA_CRU_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                    "MARK: Marker Response CRU Buffer Release FAILED\n");
        }

        return LA_FAILURE;
    }

    /* Pass the Marker Response Pdu to the Aggregator Multiplexer function */
    LaAggMultiplexFrame (pMarkerResponsePdu, pPortEntry, NULL, LA_FALSE);

    /* Increment the number of Marker Response PDUs Transmitted */
    (pPortEntry->LaPortStats.u4MarkerResponsePduTx)++;

    if (pPortEntry->LaAggAttached == LA_TRUE)
    {

        /* We increment all the statistics negatively for the Aggregator,
         * because these frames have already been counted once,
         * at the port level */
        pLaLacAggIfEntry->i4AggOutOctets += -(LA_SLOW_PROTOCOL_SIZE);
        (pLaLacAggIfEntry->i4AggOutFrames)--;
        (pLaLacAggIfEntry->i4AggOutMcastPkts)--;
    }

    LA_TRC (CONTROL_PLANE_TRC,
            "MARK: Marker Response PDU generated SUCESSFULLY\n");

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggReceiveMarkerResponse                           */
/*                                                                           */
/* Description        : This function is called by the Aggregator Parser     */
/*                      whenever a Marker Response PDU has been received.    */
/*                      This performs the ncessary processing of deleting    */
/*                      entries from Cache and Markertables & then informs   */
/*                      the LAC module to indicate the port on which the     */
/*                      Marker Response has been received by calling Purge   */
/*                      Entry function.                                      */
/*                                                                           */
/* Input(s)           : pMarkerResponsePdu - Pointer to the Marker Response  */
/*                                           PDU                             */
/*                      u2PortIndex - Index of the port on which the Marker  */
/*                                    Response PDU was received              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggReceiveMarkerResponse (UINT1 *pMarkerResponsePdu,
                            tLaLacPortEntry * pPortEntry)
{
    tLaMarkerEntry     *pLaMarkerEntry = NULL;
    UINT2               u2RequesterPort;
    UINT4               u4ReqTransId;

    if (pMarkerResponsePdu == NULL || pPortEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "MARK: Null Pointer\n");
        return LA_FAILURE;
    }

    pMarkerResponsePdu += LA_MARKER_REQUESTER_PORT_OFFSET;
    LA_GET_2BYTE (u2RequesterPort, pMarkerResponsePdu);

    pMarkerResponsePdu += LA_ETH_ADDR_SIZE;
    LA_GET_4BYTE (u4ReqTransId, pMarkerResponsePdu);

    /* Calculating the Hash Index in order to get the pointer to the entry in the
     * Marker Hash Table corresponding to the port on which the Marker Response was
     * received */

    pLaMarkerEntry = pPortEntry->pLaMarkerEntry;

    if (pLaMarkerEntry == NULL)
    {
        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "MARK: Delayed Marker Response; Marker Entry already Purged\n");
        return LA_FAILURE;
    }

    if ((u4ReqTransId == pLaMarkerEntry->u4TransactionId) &&
        (u2RequesterPort == pPortEntry->u2PortIndex))
    {

        if (pLaMarkerEntry->LaMarkerTmr.LaTmrFlag != LA_TMR_STOPPED)
        {
            if (LaStopTimer (&(pLaMarkerEntry->LaMarkerTmr)) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "MARK: Stopping Marker Timer for Port %d FAILED\n",
                             pPortEntry->u2PortIndex);
                return LA_FAILURE;
            }
        }

        if (LaAggPurgeEntry (pPortEntry) != LA_SUCCESS)
        {
            LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                    "MARK: LaAggPurgeEntry function FAILED\n");
            return LA_FAILURE;
        }

    }                            /* End of Request and Response Match check */
    else
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MARK: Marker Response received DOES NOT BELONG to Port %d\n",
                     pPortEntry->u2PortIndex);
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaAggMaintainMarkerRate                              */
/*                                                                           */
/* Description        : This function is called when a Marker PDU is being   */
/*                      generated. It ensures that not more than 5 Marker    */
/*                      PDUs are generated in any one-second period. For this*/
/*                      purpose a 1-second Rate Timer is being used.         */
/*                                                                           */
/* Input(s)           : u2AggIndex - Index of the Aggregator                 */
/*                      u1Action - Can be either SET or GET.                 */
/*                      SET-> On timer expiry ; GET-> On Marker Generation   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAggMaintainMarkerRate (UINT2 u2AggIndex, UINT1 u1Action)
{
    UINT1               u1MarkerRateCount;
    tLaLacAggEntry     *pLaLacAggInfo;

    LaGetAggEntry (u2AggIndex, &pLaLacAggInfo);
    if (pLaLacAggInfo == NULL)
    {
        return LA_FAILURE;
    }

    if (u1Action == LA_MARKER_SET)
    {
        pLaLacAggInfo->u1MarkerRateCount = 0;
    }                            /* End of Action SET */
    else
    {

        u1MarkerRateCount = pLaLacAggInfo->u1MarkerRateCount;

        if (u1MarkerRateCount >= LA_MAX_MARKER_RATE_COUNT)
        {
            /* More than 5 Marker PDUs cannot be sent in any 1 second period */
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "MARK: Marker Rate Count EXCEEDED by Aggregator %d\n",
                         u2AggIndex);
            return LA_FAILURE;
        }

        /* If timer is not running, start the timer */
        if (pLaLacAggInfo->LaMarkerRateTmr.LaTmrFlag == LA_TMR_STOPPED)
        {

            if (LaStartTimer (&(pLaLacAggInfo->LaMarkerRateTmr)) != LA_SUCCESS)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "MARK: Starting MarkerRateTimer for Agg %d FAILED\n",
                             u2AggIndex);
                return LA_FAILURE;
            }
        }
        pLaLacAggInfo->u1MarkerRateCount = ++u1MarkerRateCount;

    }                            /* End of Action GET */

    return LA_SUCCESS;
}

/* End of File */
