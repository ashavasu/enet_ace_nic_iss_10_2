/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: lamux.c,v 1.104 2017/12/26 11:04:22 siva Exp $ */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : lamux.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Link Aggregation Mux and Selection Control     */
/*                            module                                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains contains functions for the  */
/*                            Mux Control and Selection Logic                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"
#include "aclcli.h"
extern UINT4        IfMainBrgPortType[12];
extern UINT4        gu4AclOverLa;
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaLacMuxMachineInit.                                 */
/*                                                                           */
/* Description        : The Mux Control Machine init chooses the state       */
/*                      transition of the MuxControlMachine based on         */
/*                      current state of the port.                           */
/*                                                                           */
/* Input(s)           : An event.                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
LaLacMuxMachineInit (VOID)
{
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_PORT_INIT].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_UNSELECTED].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_SELECTED].pAction = LaMuxStateWaiting;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_STANDBY].pAction = LaMuxStateWaiting;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_PARTNER_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_PARTNER_NO_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_PARTNER_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_PARTNER_NO_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_WAIT_WHILE_EXP].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DETACHED]
        [LA_MUX_EVENT_READY_SET].pAction = NULL;

    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_PORT_INIT].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_UNSELECTED].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_SELECTED].pAction = LaMuxStateAttached;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING][LA_MUX_EVENT_STANDBY].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_PARTNER_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_PARTNER_NO_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_PARTNER_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_PARTNER_NO_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_WAIT_WHILE_EXP].pAction = LaMuxWaitWhileExpWaiting;
    LA_MUX_MACHINE[LA_MUX_STATE_WAITING]
        [LA_MUX_EVENT_READY_SET].pAction = LaMuxStateAttached;

    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_PORT_INIT].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_UNSELECTED].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED][LA_MUX_EVENT_SELECTED].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_STANDBY].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_PARTNER_SYNC].pAction = LaMuxStateCollecting;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_PARTNER_NO_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_PARTNER_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_PARTNER_NO_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_WAIT_WHILE_EXP].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_ATTACHED]
        [LA_MUX_EVENT_READY_SET].pAction = NULL;

    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_PORT_INIT].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_UNSELECTED].pAction = LaMuxStateAttached;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_SELECTED].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_STANDBY].pAction = LaMuxStateAttached;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_PARTNER_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_PARTNER_NO_SYNC].pAction = LaMuxStateAttached;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_PARTNER_COLLECTING].pAction = LaMuxStateDistributing;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_PARTNER_NO_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_WAIT_WHILE_EXP].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_COLLECTING]
        [LA_MUX_EVENT_READY_SET].pAction = NULL;

    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_PORT_INIT].pAction = LaMuxStateDetached;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_UNSELECTED].pAction = LaMuxStateCollecting;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_SELECTED].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_STANDBY].pAction = LaMuxStateCollecting;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_PARTNER_SYNC].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_PARTNER_NO_SYNC].pAction = LaMuxStateCollecting;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_PARTNER_COLLECTING].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_PARTNER_NO_COLLECTING].pAction = LaMuxStateCollecting;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_WAIT_WHILE_EXP].pAction = NULL;
    LA_MUX_MACHINE[LA_MUX_STATE_DISTRIBUTING]
        [LA_MUX_EVENT_READY_SET].pAction = NULL;
}

/*****************************************************************************/
/* Function Name      : LaLacMuxControlMachine.                              */
/*                                                                           */
/* Description        : The Mux Control Machine  attaches and detaches a     */
/*                      physical port to and from an aggregator and turns the*/
/*                      distribution and collecting on or off as required by */
/*                      the selection and match logic.                       */
/*                                                                           */
/* Input(s)           : pPortEntry, An event.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaLacMuxControlMachine (tLaLacPortEntry * pPortEntry, tLaMuxSmEvent MuxEvent)
{
    tLaMuxSmState       MuxState;
    INT4                i4RetVal;

    MuxState = pPortEntry->LaMuxState;

    if (LA_MUX_MACHINE[MuxState][MuxEvent].pAction == NULL)
    {
        /* No Action to take for the event - State combination */
        return LA_SUCCESS;
    }
    i4RetVal = (*LA_MUX_MACHINE[MuxState][MuxEvent].pAction) (pPortEntry);
    if (i4RetVal != LA_SUCCESS)
    {
        /* Failure occured in the action routine */
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacMuxStateDetached.                               */
/*                                                                           */
/* Description        : The Mux Control Machine detaches a physical port from*/
/*                       an aggregator.                                      */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaMuxStateDetached (tLaLacPortEntry * pPortEntry)
{
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-MUX: Port %d DETACHED\n",
                 pPortEntry->u2PortIndex);
    if (pPortEntry->LaAggAttached == LA_TRUE)
    {
        /* Port is detached from Agg only when moving from ATTACHED state */
        LaLacHwControl (pPortEntry, LA_HW_EVENT_PORT_DETACH);
        pPortEntry->LaAggAttached = LA_FALSE;

        /* Delete the Cache entries */
        LaAggDeleteCacheEntry (pPortEntry);

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "MUX: Port: %u :: Port DETACHED\n",
                     pPortEntry->u2PortIndex);
        /* Update the Port List */
        LaLacUpdatePortList (pPortEntry, LA_PORT_DETACH);

        LaUpdateTableChangedTime ();
        /* Update Agg Stats */
        LaLacUpdateStats (pPortEntry, LA_PORT_DETACH);
    }

    if (LaStopTimer (&pPortEntry->WaitWhileTmr) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization = LA_FALSE;

    if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing == LA_TRUE)
    {
        /*Disable Distribution on the port */
        LaLacHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
    }

    pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing = LA_FALSE;

    if (pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting == LA_TRUE)
    {
        LaLacHwControl (pPortEntry, LA_HW_EVENT_DISABLE_COLLECTOR);
    }

    pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting = LA_FALSE;
    pPortEntry->LaMuxState = LA_MUX_STATE_DETACHED;
    pPortEntry->bReadyN = LA_FALSE;
    pPortEntry->bReady = LA_FALSE;
    if ((LA_PROTOCOL_ADMIN_STATUS () != LA_ENABLED) &&
        (pPortEntry->u1PortDownReason == LA_NONE))
    {
        pPortEntry->u4DownInBundleCount++;
        pPortEntry->u1PortDownReason = LA_IF_OPER_DOWN;
        if (pPortEntry->pAggEntry != NULL)
        {
            LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC, "Port %d aggregated to "
                             "port-channel %d moved to Down state "
                             "due to port-channel Disable",
                             pPortEntry->u2PortIndex,
                             pPortEntry->pAggEntry->AggConfigEntry.
                             u2ActorAdminKey);
        }
        LaSyslogPrintAcivePorts (pPortEntry);
        LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
        LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                            LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);

    }
    if ((pPortEntry->u1PortOperStatus != CFA_IF_DOWN) &&
        (pPortEntry->u1PortDownReason == LA_NONE) &&
        (LA_PROTOCOL_ADMIN_STATUS () == LA_ENABLED))
    {
        pPortEntry->u4DownInBundleCount++;
        if (pPortEntry->pAggEntry != NULL)
        {
            LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC, "Port %d aggregated to "
                             "port-channel %d moved to Down state",
                             pPortEntry->u2PortIndex,
                             pPortEntry->pAggEntry->AggConfigEntry.
                             u2ActorAdminKey);
        }
        LaSyslogPrintAcivePorts (pPortEntry);
        LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
        LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                            LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);
    }

    pPortEntry->NttFlag = LA_TRUE;

    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

    if ((pPortEntry->AggSelected == LA_SELECTED) ||
        (pPortEntry->AggSelected == LA_STANDBY))
    {
        return (LaMuxStateWaiting (pPortEntry));
    }
    MEMSET (pPortEntry->LaPortDebug.au1LaMuxReasonStr, 0, LA_MUX_REASON_LEN);

    SNPRINTF ((CHR1 *) (pPortEntry->LaPortDebug.au1LaMuxReasonStr),
              LA_MUX_REASON_LEN, "LA-MUX: Port %d DETACHED",
              pPortEntry->u2PortIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacMuxStateWaiting.                                */
/*                                                                           */
/* Description        : This function moves a port from detached state to    */
/*                       Waiting State and start waitwhile timer.            */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaMuxStateWaiting (tLaLacPortEntry * pPortEntry)
{
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-MUX: Port %d WAITING\n",
                 pPortEntry->u2PortIndex);
    /* Check if manual mode of aggregation - if so dont start WaitWhileTimer 
     * so that we will move to ATTACHED state immediately.
     */
    pPortEntry->LaMuxState = LA_MUX_STATE_WAITING;

    /* Do not use waitwhile timer :
     *        - for manual aggregation mode 
     */
    if ((pPortEntry->LaLacpMode == LA_MODE_LACP) &&
        (pPortEntry->u4WaitWhileTime != 0))
    {
        if (LaStartTimer (&pPortEntry->WaitWhileTmr) == LA_FAILURE)
        {
            return LA_FAILURE;
        }
        return LA_SUCCESS;
    }

    pPortEntry->bReadyN = LA_TRUE;
    /* Check if Ready is TRUE - to go to ATTACHED state */
    if ((pPortEntry->AggSelected == LA_SELECTED)
        && (LaMuxCheckIsReady (pPortEntry) == LA_TRUE))
    {
        return (LaMuxStateAttached (pPortEntry));
    }
    MEMSET (pPortEntry->LaPortDebug.au1LaMuxReasonStr, 0, LA_MUX_REASON_LEN);

    SNPRINTF ((CHR1 *) (pPortEntry->LaPortDebug.au1LaMuxReasonStr),
              LA_MUX_REASON_LEN, "LA-MUX: Port %d WAITING",
              pPortEntry->u2PortIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMuxCheckIsReady.                                   */
/*                                                                           */
/* Description        : This function sets the Ready Variable to true and    */
/*                      call the muxmachine to move into attached state.     */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - On success                                 */
/*                      LA_FALSE - On Failure                                */
/*****************************************************************************/
tLaBoolean
LaMuxCheckIsReady (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;

    pAggEntry = pPortEntry->pAggEntry;
    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while (pTmpPortEntry != NULL)
    {
        if (((pTmpPortEntry->AggSelected == LA_SELECTED) ||
             (pTmpPortEntry->AggSelected == LA_STANDBY))
            && (pTmpPortEntry->WaitWhileTmr.LaTmrFlag == LA_TMR_RUNNING))
        {
            return LA_FALSE;
        }
        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }
    /* Ready is TRUE. So find all the ports for which
     * Ready is not true and trigger the Mux Machine for those
     * ports.
     */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);

    while (pTmpPortEntry != NULL)
    {
        if (pTmpPortEntry->AggSelected == LA_SELECTED)
        {
            /* If Ready is already true, do not call Mux Machine */
            if (pTmpPortEntry->bReady != LA_TRUE)
            {
                pTmpPortEntry->bReady = LA_TRUE;
                if (pTmpPortEntry != pPortEntry)
                {
                    LaLacMuxControlMachine (pTmpPortEntry,
                                            LA_MUX_EVENT_READY_SET);
                }
            }
        }
        else if (pTmpPortEntry->AggSelected == LA_STANDBY)
        {
            pTmpPortEntry->bReady = LA_FALSE;
        }

        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }

    return LA_TRUE;
}

/******************************************************************************/
/* Function Name      : LaMuxAnyPortWaitWhileTimerRunning                      */
/*                                                                             */
/* Description        : This function is called for checking any of the port's */
/*                      Wait While Timer is running.                           */
/*                                                                             */
/* Input(s)           : pPortEntry.                                            */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Referred           : None                                                   */
/*                                                                             */
/* Global Variables                                                            */
/* Modified           : None.                                                  */
/*                                                                             */
/* Return Value(s)    : LA_TRUE - On success                                   */
/*                      LA_FALSE - On Failure                                  */
/*****************************************************************************/
tLaBoolean
LaMuxAnyPortWaitWhileTimerRunning (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;

    if ((pPortEntry == NULL) || (pPortEntry->pAggEntry == NULL))
    {
        return LA_FALSE;

    }

    pAggEntry = pPortEntry->pAggEntry;
    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while (pTmpPortEntry != NULL)
    {
        if (pTmpPortEntry->WaitWhileTmr.LaTmrFlag == LA_TMR_RUNNING)
        {
            /* One of the Ports Wait While Timer Running */
            return LA_TRUE;
        }
        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }

    /* None of ports Wait Timer is Running */
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaMuxWaitWhileExpWaiting.                            */
/*                                                                           */
/* Description        : This function makes the port to move from            */
/*                      waiting state to attached state                      */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaMuxWaitWhileExpWaiting (tLaLacPortEntry * pPortEntry)
{
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-MUX: Port %d WAITWHILE_EXP_WAITING\n",
                 pPortEntry->u2PortIndex);

    pPortEntry->bReadyN = LA_TRUE;
    if (LaMuxCheckIsReady (pPortEntry) != LA_TRUE)
    {
        return LA_SUCCESS;
    }
    if (pPortEntry->AggSelected == LA_SELECTED)
    {
        return (LaMuxStateAttached (pPortEntry));
    }
    MEMSET (pPortEntry->LaPortDebug.au1LaMuxReasonStr, 0, LA_MUX_REASON_LEN);

    SNPRINTF ((CHR1 *) (pPortEntry->LaPortDebug.au1LaMuxReasonStr),
              LA_MUX_REASON_LEN, "LA-MUX: Port %d WAITWHILE_EXP_WAITING",
              pPortEntry->u2PortIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMuxStateAttached.                                  */
/*                                                                           */
/* Description        : This function makes the port to move from            */
/*                      collectingstate to attached state (or)               */
/*                      attahed state to collecting state                    */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaMuxStateAttached (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaDLAGTxInfo       LaDLAGTxInfo;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-MUX: Port %d ATTACHED\n",
                 pPortEntry->u2PortIndex);

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (pPortEntry->LaAggAttached == LA_FALSE)
    {
        /*It's not already attached */
        /*Update the actor sync transition count */
        ++(pPortEntry->LaPortDebug.u4ActorSyncTransitionCount);
        /* Update the Port List */
        LaLacUpdatePortList (pPortEntry, LA_PORT_ATTACH);
        /* Update Agg stats */
        LaLacUpdateStats (pPortEntry, LA_PORT_ATTACH);

        LaUpdateTableChangedTime ();

        LaLacHwControl (pPortEntry, LA_HW_EVENT_PORT_ATTACH);
        pPortEntry->LaAggAttached = LA_TRUE;
    }

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) ||
        (LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE))
    {
        /* The scenerio where if no Partner is available for the port channel,
         * Actor will makes its port channel status as up based on the
         * default information i.e., the Actor information can be 
         * considered as Partner information. In this process the 
         * port channel goes down and then up. This information can be 
         * propogated to all the Slaves using DLAG update messages
         */

        if (pPortEntry->u1DLAGMasterAck == LA_TRUE)
        {
            LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE;
            LaDLAGTxEventUpdatePdu (pPortEntry->pAggEntry, &LaDLAGTxInfo);
        }

        pPortEntry->u1DLAGPeerReady = LA_FALSE;
        pPortEntry->u1DLAGMasterAck = LA_FALSE;

        if (pPortEntry->pAggEntry->u1DLAGRolePlayed ==
            LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            /* If Port is previously acknowledged by Master
             * we need to remove it. And wait for approval */
            LaActiveDLAGMasterUpdateConsolidatedList (pPortEntry->pAggEntry);
        }
#ifdef ICCH_WANTED
        else if (pPortEntry->pAggEntry->u1MCLAGRolePlayed ==
                 LA_MCLAG_SYSTEM_ROLE_MASTER)
        {
            LaActiveMCLAGMasterUpdateConsolidatedList (pPortEntry->pAggEntry);
        }
#endif
    }

    /* If D-LAG redundancy feature is enabled and If a port is attached to
     * port-channel which is selected as Slave, then its port synchroniztion
     * state should not be changed here */
    pAggEntry = pPortEntry->pAggEntry;
    if ((pAggEntry) && (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON) &&
        (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE))
    {
        LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LaMuxStateAttached: port %u sync "
                     "state will not be changed to in-sync as the port belogs"
                     "to slave D-LAG Node.\n", pPortEntry->u2PortIndex);
    }
    else
    {
        pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization = LA_TRUE;
    }

    if (pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting == LA_TRUE)
    {
        LaLacHwControl (pPortEntry, LA_HW_EVENT_DISABLE_COLLECTOR);
        /* LaMuxDisableCollecting(pPortEntry->u2PortIndex); */
        pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting = LA_FALSE;
    }

    pPortEntry->LaMuxState = LA_MUX_STATE_ATTACHED;

    pPortEntry->NttFlag = LA_TRUE;
    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

    if ((pPortEntry->AggSelected == LA_SELECTED) &&
        (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization ==
         LA_TRUE))
    {
        LaMuxStateCollecting (pPortEntry);
    }
    else if ((pPortEntry->AggSelected == LA_UNSELECTED) ||
             (pPortEntry->AggSelected == LA_STANDBY))
    {
        LaMuxStateDetached (pPortEntry);
    }
    MEMSET (pPortEntry->LaPortDebug.au1LaMuxReasonStr, 0, LA_MUX_REASON_LEN);

    SNPRINTF ((CHR1 *) (pPortEntry->LaPortDebug.au1LaMuxReasonStr),
              LA_MUX_REASON_LEN, "LA-MUX: Port %d ATTACHED",
              pPortEntry->u2PortIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaMuxStateCollecing.                                 */
/*                                                                           */
/* Description        : This function makes the port to move from            */
/*                      collecting state to distributing state (or)          */
/*                      distributing state to collecting state               */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaMuxStateCollecting (tLaLacPortEntry * pPortEntry)
{
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-MUX: Port %d COLLECTING\n",
                 pPortEntry->u2PortIndex);
    if (pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting == LA_FALSE)
    {
        pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting = LA_TRUE;
        LaLacHwControl (pPortEntry, LA_HW_EVENT_ENABLE_COLLECTOR);
        LaUpdateTableChangedTime ();
    }

    if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing == LA_TRUE)
    {
        /*Disable Distribution on the port */
        pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing = LA_FALSE;
        if ((pPortEntry->u1PortOperStatus != CFA_IF_DOWN) &&
            (pPortEntry->pAggEntry != NULL) &&
            (pPortEntry->pAggEntry->u1AggOperStatus != CFA_IF_DOWN) &&
            (LA_PROTOCOL_ADMIN_STATUS () == LA_ENABLED))
        {
            pPortEntry->u4DownInBundleCount++;
            pPortEntry->u1PortDownReason = LA_NO_LACP_RCVD;
            if (pPortEntry->pAggEntry != NULL)
            {
                LA_SYS_TRC_ARG4 (CONTROL_PLANE_TRC, "Port %d aggregated to "
                                 "port-channel %d moved to Down state since LACP BPDUs not received/ "
                                 "Port %d aggregated to port-channel %d moved to Down state\r\n",
                                 pPortEntry->u2PortIndex,
                                 pPortEntry->pAggEntry->AggConfigEntry.
                                 u2ActorAdminKey, pPortEntry->u2PortIndex,
                                 pPortEntry->pAggEntry->AggConfigEntry.
                                 u2ActorAdminKey);
            }
            LaSyslogPrintAcivePorts (pPortEntry);
            LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
            LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                                LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);
        }

        LaLacHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
        /* Delete all cache entries for the port */
        LaAggDeleteCacheEntry (pPortEntry);
        LaUpdateTableChangedTime ();
    }

    pPortEntry->LaMuxState = LA_MUX_STATE_COLLECTING;
    pPortEntry->NttFlag = LA_TRUE;
    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);

    if ((pPortEntry->AggSelected == LA_SELECTED) &&
        (pPortEntry->LaLacPartnerInfo.
         LaLacPortState.LaSynchronization == LA_TRUE) &&
        (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting == LA_TRUE))
    {
        LaMuxStateDistributing (pPortEntry);
    }
    else if ((pPortEntry->AggSelected == LA_UNSELECTED) ||
             /*SELECTED is FALSE */
             (pPortEntry->AggSelected == LA_STANDBY) ||    /*Port is in STANDBY */
             (pPortEntry->LaLacPartnerInfo.
              LaLacPortState.LaSynchronization == LA_FALSE))
    {
        LaMuxStateAttached (pPortEntry);
    }
    MEMSET (pPortEntry->LaPortDebug.au1LaMuxReasonStr, 0, LA_MUX_REASON_LEN);

    SNPRINTF ((CHR1 *) (pPortEntry->LaPortDebug.au1LaMuxReasonStr),
              LA_MUX_REASON_LEN, "LA-MUX: Port %d COLLECTING",
              pPortEntry->u2PortIndex);

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaMuxStateDistributing.                              */
/*                                                                           */
/* Description        : This function makes the port to be in distributing.  */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaMuxStateDistributing (tLaLacPortEntry * pPortEntry)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-MUX: Port %d DISTRIBUTING\n",
                 pPortEntry->u2PortIndex);
    if (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing != LA_TRUE)
    {
        pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing = LA_TRUE;
        pPortEntry->u4UpInBundleCount++;
        pPortEntry->u1PortDownReason = LA_NONE;
        /* Stop the recovery timer if it is running */

        if ((pPortEntry->RecoveryTmr.LaTmrFlag == LA_TMR_RUNNING) &&
            (pPortEntry->LaRxmState != LA_RXM_STATE_DEFAULTED) &&
            (pPortEntry->u1RecoveryTmrReason != LA_REC_ERR_HW_FAILURE))
        {
            if (LaStopTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
            {
                return LA_FAILURE;
            }
            pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
        }

        /* Initialize the Same State count to 0 */
        pPortEntry->u4SameStateCount = 0;
        LaLacHwControl (pPortEntry, LA_HW_EVENT_ENABLE_DISTRIBUTOR);
        LaUpdateTableChangedTime ();
        LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
        LaSyslogPrintAcivePorts (pPortEntry);
        LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_UP, LA_ADMIN_UP,
                            LA_TRAP_PORT_UP);
    }
    pPortEntry->LaMuxState = LA_MUX_STATE_DISTRIBUTING;

    /* Set NTT is Not specified in STANDARD IEEE802.3-2000 page 1258 Fig 43-13.
     * independent control but specified in coupled control.
     * Here NTT should be set, otherwise partner will not know
     * that Distributing is enabled here.
     */

    pAggEntry = pPortEntry->pAggEntry;

    if (((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
         (pPortEntry->u1DLAGMasterAck == LA_FALSE) &&
         (LA_AA_DLAG_MASTER_DOWN != LA_TRUE)) ||
        ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
         (pPortEntry->u1DLAGMasterAck == LA_FALSE) &&
         (LA_AA_DLAG_MASTER_DOWN != LA_TRUE)))
    {
        pPortEntry->u1DLAGPeerReady = LA_TRUE;
        LaDLAGTxInfo.u1DSUType = LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE;
        LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        pPortEntry->LaLacActorInfo.LaLacPortState.LaSynchronization = LA_FALSE;

        if (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER)
        {
            LaActiveDLAGMasterUpdateConsolidatedList (pAggEntry);
        }
#ifdef ICCH_WANTED
        else if (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER)
        {
            LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
        }
#endif
    }

    pPortEntry->NttFlag = LA_TRUE;
    LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
    MEMSET (pPortEntry->LaPortDebug.au1LaMuxReasonStr, 0, LA_MUX_REASON_LEN);

    SNPRINTF ((CHR1 *) (pPortEntry->LaPortDebug.au1LaMuxReasonStr),
              LA_MUX_REASON_LEN, "LA-MUX: Port %d DISTRIBUTING",
              pPortEntry->u2PortIndex);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCreatePortChannelInterface.                        */
/*                                                                           */
/* Description        : This function is called from CFA whenever a          */
/*                       portchannel is created.                             */
/*                                                                           */
/* Input(s)           : u4IfIndex- logical index in CFA                      */
/*                      tCfaIfInfo- CFA Parameters for the logical port      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaCreatePortChannelInterface (UINT4 u4IfIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT1              *pu1KeyStr;
    UINT4               u4KeyStrLen = 0;
    INT4                i4Key = 0;
    UINT2               u2AdminKey = 0;
    UINT1               au1Str[] = "po";
    tCfaIfInfo          CfaIfInfo;
#ifdef NPAPI_WANTED
    UINT2               u2HwAggIndex = LA_INVALID_HW_AGG_IDX;
#endif

    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    if (LaIsLaStarted () != LA_TRUE)
    {
        return LA_FAILURE;
    }

    if (LaCfaGetIfInfo ((UINT2) u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        /* Cannot get Interface Name */
        return LA_FAILURE;
    }

    /* Check if the Port Channel already exists */
    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);

    if (pAggEntry != NULL)
    {
        /* Port Channel Already exists */
        return LA_SUCCESS;
    }
    /* Create the aggregator entry */
    /* First find out the AggActorAdminKey from ifAlias */
    if (STRNCMP (CfaIfInfo.au1IfName, au1Str, 2) != 0)
    {
        return LA_FAILURE;
    }
    pu1KeyStr = &(CfaIfInfo.au1IfName[2]);
    u4KeyStrLen = STRLEN (pu1KeyStr);

    /* If the following characters are not numeric,
     * return failure.  */
    while (u4KeyStrLen != 0)
    {
        if (!ISDIGIT (*pu1KeyStr))
        {
            return LA_FAILURE;
        }
        u4KeyStrLen--;
        pu1KeyStr++;
    }
    pu1KeyStr = &(CfaIfInfo.au1IfName[2]);
    i4Key = ATOL (pu1KeyStr);
    /* If the value is not within size UINT2, return */
    if ((i4Key < 0) || (i4Key > LA_16BIT_MAX))
    {
        return LA_FAILURE;
    }
    u2AdminKey = (UINT2) i4Key;

    /* Create an aggregator with default values */
    if (LaCreateAggregator (u2AdminKey, u4IfIndex, &pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }

    LaAddToAggTable (pAggEntry);
    /* Assign MAC address for the aggregator */
    if (LaChangeAggMacAddress (u4IfIndex, pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }
    /* For Update the Port Channel Mac address in CFA */
    pAggEntry->u4AggSpeed = CfaIfInfo.u4IfSpeed;
    pAggEntry->u4Mtu = CfaIfInfo.u4IfMtu;
    LaUpdateLlPortChannelStatus (pAggEntry);
#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (LaFsLaHwCreateAggGroup (u4IfIndex, &u2HwAggIndex) == FNP_FAILURE)
        {
            LaHandleAggCreateFailed (u4IfIndex);
            LA_TRC_ARG1 (INIT_SHUT_TRC, "Creating aggregator %d "
                         "in HW failed\n", u4IfIndex);
            return LA_SUCCESS;
        }
        LaFsLaHwSetSelectionPolicyBitList (u4IfIndex,
                                           pAggEntry->AggConfigEntry.
                                           u4LinkSelectPolicyBitList);

        LaAggCreated (u4IfIndex, u2HwAggIndex);
    }
#endif

    /* Incase a port-channel is created call to
     * Init DLAG if Global status is enabled */
    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        if (LA_SLL_COUNT (LA_AGG_SLL) == 1)
        {
            /* If this is first port-channel then start the global periodic sync timer */
            if (LaStartTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr))
                != LA_SUCCESS)
            {
                return LA_FAILURE;
            }
        }

        LaActiveCopyGlobalAndInitDLAG (pAggEntry);

    }
#ifdef ICCH_WANTED
    else if ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
             (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
    {
        LaActiveCopyGlobalAndInitMCLAG (pAggEntry);
    }
#endif
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeletePortChannelInterface.                        */
/*                                                                           */
/* Description        : This function is called from CFA whenever a          */
/*                       portchannel is deleted.                             */
/*                                                                           */
/* Input(s)           : u4IfIndex - PortChannel (logical)Index in CFA.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaDeletePortChannelInterface (UINT4 u4IfIndex)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pDefPortEntry = NULL;
    tLaBoolean          LaDistributing = LA_TRUE;
#ifdef NPAPI_WANTED
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId;
#endif
    UINT4               u4IcclIfIndex = 0;
    INT4                i4RetVal = LA_SUCCESS;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    if (LaIsLaStarted () != LA_TRUE)
    {
        return LA_FAILURE;
    }

    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        /* Interface doesnt exist */
        return LA_SUCCESS;
    }
    if (pAggEntry->bDynPortRealloc == LA_TRUE)
    {
        /* Remove default port from the aggregator */
        if (LaGetDefPortEntryForAgg (pAggEntry, &pDefPortEntry) == LA_FAILURE)
        {
            return LA_FAILURE;
        }

        /* Reset the PortEntry flags */
        pDefPortEntry->bDynAggSelect = LA_FALSE;
        pDefPortEntry->pDefAggEntry = NULL;
        pAggEntry->LaLacpMode = LA_MODE_DISABLED;
        /* Disable LACP on the default port so that all ports
         * in the port-channel are removed from the port-channel */
        if ((LaChangeLacpMode (pDefPortEntry, pDefPortEntry->LaLacpMode,
                               LA_MODE_DISABLED)) == LA_FAILURE)
        {
            return LA_FAILURE;
        }

        /* Reset the AggEntry flags */
        pAggEntry->bDynPortRealloc = LA_FALSE;
        /* Delete Port from Dynamic realloc port list */
        LA_SLL_DELETE (&(gLaGlobalInfo.LaDynReallocPortList),
                       &(pDefPortEntry->NextDefPortEntry));
    }
    else
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            if (LA_MODULE_STATUS == LA_ENABLED)
            {
                LaDistributing = pPortEntry->LaLacActorInfo.
                    LaLacPortState.LaDistributing;

                if (pPortEntry->LaLacpMode == LA_MODE_LACP)
                {
                    LaDisableLacpOnPort (pPortEntry);
                }
                else if (pPortEntry->LaLacpMode == LA_MODE_MANUAL)
                {
                    LaDisableManualConfigOnPort (pPortEntry);
                }
                LaL2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                                  pAggEntry->u2AggIndex);

#ifdef NPAPI_WANTED
                if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
                {

                    LaFsLaHwRemovePortFromConfAggGroup (pAggEntry->u2AggIndex,
                                                        pPortEntry->
                                                        u2PortIndex);
                }
#endif
            }
            if (pPortEntry->pAggEntry != NULL)
            {
                LaDeleteFromAggPortTable (pPortEntry->pAggEntry, pPortEntry);
                pPortEntry->pAggEntry->u1ConfigPortCount--;
            }
            pPortEntry->pAggEntry = NULL;

            if ((pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED) &&
                (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_ADDED_IN_LAGG))
            {
                /* Incase DLAG enabled and port-channel deleted
                   Change the port status to Not in H/w */
                pPortEntry->LaLacHwPortStatus = LA_HW_PORT_NOT_IN_LAGG;
            }

            /* Reset the lacp mode, key and actor state when the port channel
             * is deleted.
             */
            pPortEntry->LaLacpMode = LA_MODE_DISABLED;
            pPortEntry->LaLacActorInfo.u2IfKey = 0;
            pPortEntry->LaLacActorAdminInfo.u2IfKey = 0;

            pPortEntry->LaLacActorInfo.LaLacPortState =
                gLaGlobalInfo.LaPortDefaultState;
            pPortEntry->LaLacActorInfo.u2IfPriority = LA_DEFAULT_PORT_PRIORITY;
            pPortEntry->LaLacActorAdminInfo.u2IfPriority =
                LA_DEFAULT_PORT_PRIORITY;
            LA_MEMSET (&(pPortEntry->LaPortStats), 0, sizeof (tLaPortStats));

#ifdef NPAPI_WANTED
            /* Port, which is removed from the LAGG, will be created as an
             * individual port in the higher layer modules under the
             * following scenarios -
             *   1) Port was a Standby port (it would NOT have been
             *      present in hardware)
             *   2) NPAPI mode is synchronous and removing the port
             *      from the LAGG in hardware was successful.
             * 
             * In case of asynchronous NPAPI mode the creation will be 
             * handled in the NPAPI callback */
            if ((LaDistributing == LA_FALSE) &&
                (pPortEntry->LaLacHwPortStatus != LA_HW_REM_PORT_FAILED_IN_LAGG)
                && (pPortEntry->LaLacHwPortStatus != LA_HW_PORT_ADDED_IN_LAGG))
            {
                /* LaLAcHwPortStatus is added to takecare following 
                 * scenario:
                 * If the Remove port HW calls fails, then we should not
                 * create physical port, to do this, we are checking for 
                 * HWPortStatus != LA_HW_REM_PORT_FAILED_IN_LAGG 
                 * In the standby, when LA_NP_DEL_PORT_FAILURE_INFO doesnt 
                 * reach it,SW audit takes 40 seconds to detect out-of-sync.
                 * In the mean time, if "no channel-group" is executed,then 
                 * physical port should not be created, so the HWPortStatus
                 * is checked for LA_HW_PORT_ADDED_IN_LAGG */

                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);

            }
            else if (pPortEntry->LaLacHwPortStatus == LA_HW_PORT_NOT_IN_LAGG)
            {
                /* This gets executed in both SYNC and ASYNC mode.
                 * In the Sync mode, this is done to ensure the port 
                 * removal is done successfully
                 * In the Async mode, this condition executes only in the 
                 * standby node. When the LA is disabled for the port (no 
                 * Channel-group command is executed), then Active deletes
                 * the port from the HW, and sends a RM message in the NP
                 * callback function. As part of normal flow, CLI also 
                 * syncs this conf with the standby through a RM message.
                 * These RM messages reaches the Standby node (through CLI 
                 * task & LA task), Since LA task has the highest priority,
                 * RM message sent in the NP callback function gets 
                 * executed first. In the NP callback function we check for
                 * the pPortEntry->pAggEntry == NULL to create a physcial
                 * port, this will not be true because the RM message
                 * received through CLI task is not yet executed, which 
                 * makes this NULL.
                 *
                 * To handle this scenario, in the NP callback function,we 
                 * set the LaLacHwPortStatus to LA_HE_PORT_NOT_IN_LAGG
                 * and the physical port is created in the CLI flow (i.e,
                 * this flow) */

                /* Create Physcial Port to Bridge */
                LaHlCreatePhysicalPort (pPortEntry);
            }
#else /* NPAPI_WANTED */
            /* Create Physcial Port to Bridge */
            LaHlCreatePhysicalPort (pPortEntry);
#endif /* !NPAPI_WANTED */
            LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        }

        if (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)
        {
            LaDLAGDeInit (pAggEntry);
        }
    }

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        if (pAggEntry != NULL)
        {
            LaVcmGetContextInfoFromIfIndex (pAggEntry->u2AggIndex, &u4ContextId,
                                            &u2LocalPortId);
            VcmUnMapPortFromContextInHw (u4ContextId, pAggEntry->u2AggIndex);
            LaFsLaHwDeleteAggregator (pAggEntry->u2AggIndex);
            /* Aggregator is removed from hw, syncup this info with
             * standby node.*/
            LaRedSyncModifyAggPortsInfo (LA_NP_DEL_INFO, pAggEntry->u2AggIndex,
                                         0);
        }
    }
#endif /* NPAPI_WANTED */

    LaIssFreeAggregatorMac (u4IfIndex);

    LaDeleteFromAggTable (pAggEntry);

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
        (LA_SLL_COUNT (LA_AGG_SLL) == 0))
    {
        /* If this is Last port channel
         * Stop the Global Sync timer 
         */

        if (LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalDLAGPeriodicSyncTmr)) !=
            LA_SUCCESS)
        {
            i4RetVal = LA_FAILURE;
        }
    }
    if ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
        (LA_SLL_COUNT (LA_AGG_SLL) == 0))
    {
        /* If this is Last port channel
         * Stop the Global Sync timer 
         */

        if (LaStopTimer (&(LA_DLAG_GLOBAL_INFO.GlobalMCLAGPeriodicSyncTmr)) !=
            LA_SUCCESS)
        {
            i4RetVal = LA_FAILURE;
        }
    }

    /* If the port-channel is ICCL interface then,
       reset the ICCL interface index as zero in 
       ICCH module.
     */
    LaIcchGetIcclIfIndex (&u4IcclIfIndex);

    if (u4IcclIfIndex == (UINT4) pAggEntry->u2AggIndex)
    {
        LaIcchSetIcclIfIndex (LA_INIT_VAL);
    }

    /* Release the Aggregator entry */
    if (LA_AGGENTRY_FREE_MEM_BLOCK (pAggEntry) != LA_MEM_SUCCESS)
    {

        LA_TRC (INIT_SHUT_TRC, "LA_AGGENTRY_FREE_MEM_BLOCK FAILED\n");
        return LA_ERR_MEM_FAILURE;
    }

#ifndef NPAPI_WANTED
    UNUSED_PARAM (LaDistributing);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : LaHlCreatePhysicalPort.                              */
/*                                                                           */
/* Description        : This function is called whenever a port is removed   */
/*                       from the portchannel                                */
/*                                                                           */
/* Input(s)           : pPortEntry                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaHlCreatePhysicalPort (tLaLacPortEntry * pPortEntry)
{
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               u1PortType = 0;

    LA_MEMSET (&SnmpNotifyInfo, LA_INIT_VAL, sizeof (tSnmpNotifyInfo));

    if (pPortEntry == NULL)
    {
        return LA_FAILURE;
    }

    /* Set the default port type for the port based on bridge mode. */
    LaL2IwfPbSetDefPortType (pPortEntry->u2PortIndex);

    if (L2IwfGetPbPortType (pPortEntry->u2PortIndex, &u1PortType)
        == L2IWF_FAILURE)
    {
        return LA_FAILURE;
    }

    /* This SNMP notify is not required for redundancy. So passing 
     * sequence number as 0*/
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfMainBrgPortType, u4SeqNum,
                          FALSE, NULL, NULL, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pPortEntry->u2PortIndex,
                      u1PortType));

    /* Need to send Physical port create indication to upper layer modules
     * when LA module status is enabled. Because when phy port is made
     * member of port-channel, the physical port is not deleted if module
     * status is disable */
    if (LaGetLaEnableStatus () == LA_ENABLED)
    {
        LaL2IwfCreatePortIndicationFromLA (pPortEntry->u2PortIndex,
                                           pPortEntry->u1PortOperStatus);
    }

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        CfaFsCfaHwClearStats (pPortEntry->u2PortIndex);
    }
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHlDeletePhysicalPort.                              */
/*                                                                           */
/* Description        : This function is called whenever a port is added     */
/*                       to the portchannel                                  */
/*                                                                           */
/* Input(s)           : pPortEntry                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaHlDeletePhysicalPort (tLaLacPortEntry * pPortEntry)
{
    UINT1               u1PortType = 0;
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1TempPauseAdminNode = 0;

    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    LA_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    /* Check if the ACL filter is attached with the physical port */
    if (gu4AclOverLa == 1)
    {
        if (AclValidateMemberPort (pPortEntry->u2PortIndex) != LA_SUCCESS)
        {
            return LA_FAILURE;
        }
    }

    /* This SNMP notify is not required for redundancy. So passing 
     * sequence number as 0*/
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IfMainBrgPortType, u4SeqNum,
                          FALSE, NULL, NULL, 1, SNMP_SUCCESS)
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pPortEntry->u2PortIndex,
                          u1PortType));

    LaL2IwfPbSetPcPortTypeToPhyPort ((UINT4)
                                     (pPortEntry->pAggEntry->u2AggIndex),
                                     pPortEntry->u2PortIndex);

    LaL2IwfDeleteAndAddPortToPortChannel (pPortEntry->u2PortIndex,
                                          pPortEntry->pAggEntry->u2AggIndex);

    /* Getting the Port-channel related params */
    if (LaCfaGetIfInfo (pPortEntry->pAggEntry->u2AggIndex, &CfaIfInfo)
        == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    u1TempPauseAdminNode = CfaIfInfo.u1PauseAdminNode;
    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    /* Getting the Port related params */
    if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    /* Copying the Pause admin mode of Port-Channel to Port */
    CfaIfInfo.u1PauseAdminNode = u1TempPauseAdminNode;

    LaCfaSetIfInfo (IF_PAUSE_ADMIN_MODE, (UINT4) pPortEntry->u2PortIndex,
                    &CfaIfInfo);

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        CfaFsCfaHwClearStats (pPortEntry->u2PortIndex);
    }
#endif

    /* In the absence of CFA, the above functions must be suitably ported
     * to the target platform. The functionality that needs to be ported
     * is to update the Operational status of this port as DOWN in the
     * higher layer modules such as Bridge, VLAN and spanning tree modules
     * such as RSTP/MSTP. Indication must then be given to delete the port
     * in these above-mentioned higher layer modules. Then the hardware
     * statistics for the port should be reset */

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCreateAggregator                                   */
/*                                                                           */
/* Description        : This function is used to allocate memory for the     */
/*                      port channel and indicate the higher layers that the */
/*                      portchannel is created                               */
/*                                                                           */
/* Input(s)           : u2AdminKey-key for which the portchannel is created  */
/*                      u4Index- IfIndex(CFA) to which the port channel is   */
/*                       mapped.                                             */
/*                                                                           */
/* Output(s)          : ppRetAggEntry - Allocated Aggregator entry           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

/* Creation and deletion of aggregator*/
INT4
LaCreateAggregator (UINT2 u2AdminKey, UINT4 u4Index,
                    tLaLacAggEntry ** ppRetAggEntry)
{
    tLaLacAggEntry     *pAggEntry;

    if (LA_FREE_AGGS == LA_INIT_VAL)
    {
        return LA_FAILURE;
    }

    /* Allocate a block for the aggregator entry */
    if (LA_AGGENTRY_ALLOC_MEM_BLOCK (pAggEntry) == NULL)
    {

        LA_TRC (INIT_SHUT_TRC, "LA_AGGENTRY_ALLOC_MEM_BLOCK FAILED\r\n");
        return LA_ERR_MEM_FAILURE;
    }

    LA_MEMSET (pAggEntry, 0, sizeof (tLaLacAggEntry));

    LA_SLL_INIT_NODE (&pAggEntry->NextNode);

    LA_SLL_INIT (&pAggEntry->ConfigPortSll);

    pAggEntry->u2AggIndex = (UINT2) u4Index;

    pAggEntry->LaMarkerRateTmr.LaAppTimer.u4Data = LA_MARKER_RATE_TMR_TYPE;
    pAggEntry->LaMarkerRateTmr.pEntry = (VOID *) pAggEntry;
    pAggEntry->LaMarkerRateTmr.LaTmrFlag = LA_TMR_STOPPED;

    pAggEntry->u1AggOperStatus = LA_OPER_DOWN;
    pAggEntry->u1AggAdminStatus = LA_ADMIN_DOWN;
    pAggEntry->i4DownReason = LA_IF_ADMIN_DOWN;
    pAggEntry->u4OperDownCount++;

    pAggEntry->LaLinkUpDownTrapEnable = LA_TRAP_DISABLED;
    pAggEntry->LaLacpMode = LA_MODE_DISABLED;
    pAggEntry->u4Mtu = CFA_ENET_MTU;
    pAggEntry->i4PauseAdminMode = CFA_DEF_PAUSE_ADMIN_MODE;

    /* AggConfig configurations */
    pAggEntry->AggConfigEntry.u2ActorAdminKey = u2AdminKey;
    pAggEntry->AggConfigEntry.u2ActorOperKey = u2AdminKey;

    pAggEntry->AggConfigEntry.AggOrIndividual = LA_FALSE;

    LA_MEMCPY (&(pAggEntry->AggConfigEntry.ActorSystem),
               &(gLaGlobalInfo.LaSystem), sizeof (tLaSystem));
    pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_DEFAULT_LINK_SELECT_POLICY;
    pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList = LA_SELECT_POLICY_DEF;
    pAggEntry->u1MaxPortsToAttach = LA_MAX_PORTS_PER_AGG;

    pAggEntry->u1IsAggAudited = LA_FALSE;

    /* D-LAG configurations */
    pAggEntry->u1DLAGStatus = LA_DLAG_DISABLED;
    pAggEntry->u1MCLAGStatus = LA_MCLAG_DISABLED;
    pAggEntry->u1DLAGRedundancy = LA_DLAG_REDUNDANCY_OFF;
    pAggEntry->u4DLAGDistributingIfIndex = 0;
    LA_MEMSET ((pAggEntry->DLAGSystem.SystemMacAddr), 0, sizeof (tLaMacAddr));
    pAggEntry->DLAGSystem.u2SystemPriority = LA_DLAG_DEFAULT_SYSTEM_PRIORITY;
    pAggEntry->u1DLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
    pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_NONE;
    pAggEntry->u4DLAGMaxKeepAliveCount = LA_DLAG_MAX_KEEP_ALIVE_COUNT;
    pAggEntry->u4DLAGPeriodicSyncPduTxCount = 0;
    pAggEntry->u4DLAGPeriodicSyncPduRxCount = 0;
    pAggEntry->u4DLAGEventUpdatePduTxCount = 0;
    pAggEntry->u4DLAGEventUpdatePduRxCount = 0;
    pAggEntry->u4DLAGElectedAsMasterCount = 0;
    pAggEntry->u4DLAGElectedAsSlaveCount = 0;
    pAggEntry->u4DLAGTrapTxCount = 0;
    pAggEntry->pDLAGRemoteAggEntry = NULL;
    pAggEntry->u4DLAGDistributingPortListCount = 0;

    pAggEntry->u4DLAGPeriodicSyncTime = LA_DLAG_DEFAULT_PERIODIC_SYNC_TIME;
    pAggEntry->u4DLAGMSSelectionWaitTime =
        LA_DLAG_DEFAULT_MS_SELECTION_WAIT_TIME;
    pAggEntry->u4MCLAGPeriodicSyncTime = LA_DLAG_DEFAULT_PERIODIC_SYNC_TIME;

    pAggEntry->DLAGPeriodicSyncTmr.LaTmrFlag = LA_TMR_STOPPED;
    pAggEntry->DLAGMSSelectionWaitTmr.LaTmrFlag = LA_TMR_STOPPED;
    pAggEntry->MCLAGPeriodicSyncTmr.LaTmrFlag = LA_TMR_STOPPED;

    pAggEntry->DLAGPeriodicSyncTmr.pEntry = (VOID *) pAggEntry;
    pAggEntry->DLAGMSSelectionWaitTmr.pEntry = (VOID *) pAggEntry;
    pAggEntry->MCLAGPeriodicSyncTmr.pEntry = (VOID *) pAggEntry;

    pAggEntry->DLAGPeriodicSyncTmr.LaAppTimer.u4Data =
        LA_DLAG_PERIODIC_TMR_TYPE;
    pAggEntry->DLAGMSSelectionWaitTmr.LaAppTimer.u4Data =
        LA_DLAG_MS_SELECTION_WAIT_TMR_TYPE;

    LA_MEMSET (&(pAggEntry->LaDLAGConsInfoTable), 0, sizeof (tLaDLAGConsInfo));

    *ppRetAggEntry = pAggEntry;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeleteAggregator                                   */
/*                                                                           */
/* Description        : This function is used to allocate memory for the     */
/*                      port channel and indicate the higher layers that the */
/*                      portchannel is created                               */
/*                                                                           */
/* Input(s)           : u2AdminKey-key for which the portchannel is created  */
/*                      u4Index- IfIndex(CFA) to which the port channel is   */
/*                       mapped.                                             */
/*                                                                           */
/* Output(s)          : ppRetAggEntry - Allocated Aggregator entry           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaDeleteAggregator (tLaLacAggEntry * pAggEntry)
{
    UINT4               u4IcclIfIndex = 0;

    /* Instead of indicating lower layer (CFA) alone this func will indicate to
     * Cfa and from there to L2 */

#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        LaFsLaHwDeleteAggregator (pAggEntry->u2AggIndex);
        /* Aggregator is removed from hw, syncup this info with
         ** standby node.*/
        LaRedSyncModifyAggPortsInfo (LA_NP_DEL_INFO, pAggEntry->u2AggIndex, 0);
    }
#endif /* NPAPI_WANTED */
    LaCfaDeletePortChannel ((UINT4) pAggEntry->u2AggIndex);

    /* If the port-channel is ICCL interface then,
       reset the ICCL interface index as zero in 
       ICCH module.
     */
    LaIcchGetIcclIfIndex (&u4IcclIfIndex);

    if (u4IcclIfIndex == (UINT4) pAggEntry->u2AggIndex)
    {
        LaIcchSetIcclIfIndex (LA_INIT_VAL);
    }

    /* Delete all remote aggregator entries */
    LaDLAGDeleteAllRemoteAggEntries (pAggEntry);

    LaDeleteFromAggTable (pAggEntry);

    /* Release Mempool allocated for the Aggregator entry */
    if (LA_AGGENTRY_FREE_MEM_BLOCK (pAggEntry) != LA_MEM_SUCCESS)
    {

        LA_TRC (INIT_SHUT_TRC, "LA_AGGENTRY_FREE_MEM_BLOCK FAILED\n");
        return LA_ERR_MEM_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaAddPortToAggregator                                */
/*                                                                           */
/* Description        : This function is used to add Port to the aggregator  */
/*                      configured portlist.                                 */
/*                                                                           */
/* Input(s)           : pAggEntry - pointer to the Aggregator to which the   */
/*                       port should be added.                               */
/*                      pPortEntry - Pointer to the port which is to be added*/
/*                       to the aggregator entry.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaAddPortToAggregator (tLaLacAggEntry * pAggEntry, tLaLacPortEntry * pPortEntry)
{
    UINT2               u2PortIndex = pPortEntry->u2PortIndex;

    /* Aggregator parameter changes when a port is added to an aggregator */

    if (pAggEntry->u1ConfigPortCount == 0)
    {
        pAggEntry->u2FirstPort = u2PortIndex;
    }
    LaAddToAggPortTable (pAggEntry, pPortEntry);
    pAggEntry->u1ConfigPortCount++;

    /* port parameter changes when it is added to an aggregator */
    pPortEntry->pAggEntry = pAggEntry;

    /* if this parameter mismatch then the port should be admin down */
    if (pPortEntry->LaLacpMode != pAggEntry->LaLacpMode)
    {
        /* This mismatch should not be there */
        return LA_SUCCESS;
    }

    if (pPortEntry->LaLacpMode != LA_MODE_DISABLED)
    {
        LaChangeLacpMode (pPortEntry, LA_MODE_DISABLED, pPortEntry->LaLacpMode);
    }

    return LA_SUCCESS;
}

/**************************************************************************** 
 * Function Name      : LaAddDynPortToAggregator
 *
 * Description        : This function is used to add dynamically selected
 *                      Port to the aggregator portlist. This not does not
 *                      change the Lacp Mode
 *
 * Input(s)           : pAggEntry - pointer to the Aggregator to which the
 *                       port should be added.
 *                      pPortEntry - Pointer to the port which is to be added
 *                       to the aggregator entry.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : LA_SUCCESS - On success
 *                      LA_FAILURE - On Failure
 *****************************************************************************/

INT4
LaAddDynPortToAggregator (tLaLacAggEntry * pAggEntry,
                          tLaLacPortEntry * pPortEntry)
{
    UINT2               u2PortIndex = pPortEntry->u2PortIndex;

    /* Aggregator parameter changes when a port is added to an aggregator */

    if (pAggEntry->u1ConfigPortCount == 0)
    {
        pAggEntry->u2FirstPort = u2PortIndex;
        pAggEntry->AggConfigEntry.u2ActorOperKey =
            pPortEntry->LaLacActorAdminInfo.u2IfKey;
    }
    LaAddToAggPortTable (pAggEntry, pPortEntry);
    pAggEntry->u1ConfigPortCount++;

    /* port parameter changes when it is added to an aggregator */
    pPortEntry->pAggEntry = pAggEntry;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaRemovePortFromAggregator                           */
/*                                                                           */
/* Description        : This function is used to remove port from the        */
/*                       aggregator.                                         */
/*                                                                           */
/* Input(s)           :  pPortEntry - Pointer to the port which is to be     */
/*                       removed from the aggregator entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

VOID
LaRemovePortFromAggregator (tLaLacPortEntry * pPortEntry)
{
    tLaDLAGTxInfo       LaDLAGTxInfo;
    tMacAddr            MacAddr = { 0 };
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4BrgPortIndex = 0;

    LA_MEMSET (&LaDLAGTxInfo, 0, sizeof (LaDLAGTxInfo));

    pAggEntry = pPortEntry->pAggEntry;

    if (pAggEntry == NULL)
    {
        return;

    }

    LaDLAGTxInfo.u4PortIndex = (UINT4) pPortEntry->u2PortIndex;

    LaDeleteFromAggPortTable (pPortEntry->pAggEntry, pPortEntry);
    pPortEntry->pAggEntry->u1ConfigPortCount--;

    if (pPortEntry->pAggEntry->u1ConfigPortCount == 0)
    {
        pPortEntry->pAggEntry->AggConfigEntry.u2ActorOperKey =
            pPortEntry->pAggEntry->AggConfigEntry.u2ActorAdminKey;
        /* Reset the Aggregator Ref Speed */
        pPortEntry->pAggEntry->u4PortSpeed = 0;
        pPortEntry->pAggEntry->u4PortHighSpeed = 0;

    }
    LA_MEMSET (&(pPortEntry->LaPortStats), 0, sizeof (tLaPortStats));

    /* Reset the counters if port is removed from port-channel */
    pPortEntry->u4ErrorDetectCount = 0;
    pPortEntry->u4RecTrgdCount = 0;
    pPortEntry->u4DefaultedStateRecTrgrdCount = 0;
    pPortEntry->u1InPassiveDefState = LA_FALSE;

    /* If Only D-LAG status is enabled in Aggregator:-
     * Then when a port is detatched from an aggregator, then low-priority
     * event-update message must be sent to notify the remote D-LAG node about
     * this event, so that remote D-LAG node can free the resources allocated
     * to maintain the detatched port information.
     * 
     * If D-LAG Redundancy feature is also enabled in Aggregator:-
     * Then When a port is detatched from a Slave Node, then role-played by
     * the D-LAG nodes will not be affected and sending low-priority event-
     * update message to free the resources in remote nodes would be sufficient.
     * But port is detatched from the Master Node then it does make the
     * difference as the master-slave-selection is based on the number of 
     * ports in-sync in an aggregator, so Master-slave-selection process
     * must be triggered, but if the Node where this event has happened
     * is itself the elector then a low-priority event-update message should be
     * sent to update the remote D-LAG node followed by Master-slave-selection
     * algorithm.
     * if the Node where this event has occured is not the designated elector
     * then high-priority event-update pdu must be sent to initiate the 
     * master slave selection in the elector. */
    if (((pAggEntry) && (pAggEntry->u1DLAGStatus == LA_DLAG_ENABLED)) ||
        (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE))
    {
        if (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            if (pAggEntry->bLaDLAGIsDesignatedElector == LA_TRUE)
            {
                LaDLAGTxInfo.u4PortIndex = (UINT4) pPortEntry->u2PortIndex;
                LaDLAGTxInfo.u1DSUType =
                    LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);

                LaDLAGTriggerMSSelectionProcess (pAggEntry,
                                                 MacAddr,
                                                 LA_FALSE,
                                                 LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE);

            }
            else
            {
                LaDLAGTxInfo.u1DSUType =
                    LA_DLAG_HIGH_PRIO_EVENT_PORT_DETATCH_FROM_AGG;
                LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
            }
        }
        else
        {
            LaDLAGTxInfo.u1DSUType =
                LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG;
            LaDLAGTxEventUpdatePdu (pAggEntry, &LaDLAGTxInfo);
        }
    }

    /* Delete the port from RBTree and release the memory for that port */
    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
        (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_MASTER) &&
        (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_OFF))

    {
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);
        LaActiveDLAGRemoveRemotePortFromPortList (pAggEntry, u4BrgPortIndex);
        LaActiveDLAGMasterUpdateConsolidatedList (pAggEntry);
    }
#ifdef ICCH_WANTED
    else if ((pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED) &&
             (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_MASTER))
    {
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->
                                               u2PortIndex, &u4BrgPortIndex);
        LaActiveDLAGRemoveRemotePortFromPortList (pAggEntry, u4BrgPortIndex);
        LaActiveMCLAGMasterUpdateConsolidatedList (pAggEntry);
    }
#endif
    pPortEntry->pAggEntry = NULL;

    return;
}

/*****************************************************************************/
/* Function Name      : LaSelectLacpAggGroup                                 */
/*                                                                           */
/* Description        : This is the core selection logic called for making   */
/*                       the port to be SELECTED for the portchannel in      */
/*                       LACP mode .                                         */
/*                                                                           */
/* Input(s)           : pAggEntry-AggEntry for which the ports to be SELECTED*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaSelectLacpAggGroup (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *apSelectedPortEntry[LA_MAX_PORTS];
    tLaSystem          *pAggActorSystem;
    tLaSystem          *pAggPartnerSystem;
    tLaSystem          *pPortPartnerSystem;
    UINT2               u2Index = 0;
    UINT2               u2TotalCount = 0;
    tLaBoolean          bAllPortsReady = LA_TRUE;
    tLaLacPortEntry    *pRefPortEntry = NULL;

    pAggActorSystem = &(pAggEntry->AggConfigEntry.ActorSystem);
    pAggPartnerSystem = &(pAggEntry->AggConfigEntry.PartnerSystem);

    /* Find out the reference port if we are going to aggregate based
     * on the partner default value.
     */
    if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
    {
        LaSelectRefPortForAdminInfo (pAggEntry, &pTmpPortEntry);
        if (pTmpPortEntry == NULL)
        {
            return LA_SUCCESS;
        }
        pRefPortEntry = pTmpPortEntry;

        if (LaUnSelectGroupIfRefPortLAGChanged (pTmpPortEntry) == LA_TRUE)
        {
            pPortPartnerSystem = &(pTmpPortEntry->LaLacPartnerInfo.LaSystem);

            /* copy the partner LAG ID as the Aggregator's partner LAG ID */
            LA_MEMCPY (pAggPartnerSystem, pPortPartnerSystem,
                       sizeof (tLaSystem));
            /* copy the partner key for the port to this aggregator */
            pAggEntry->AggConfigEntry.u2PartnerOperKey =
                pTmpPortEntry->LaLacPartnerInfo.u2IfKey;

            /* copy the port speed & LA mode (LACP or MANUAL) as the
             * aggregator speed so that all other ports attaching to
             * this aggregator later  can be verified for this requirement.
             */
            pAggEntry->u4PortSpeed = pTmpPortEntry->u4LinkSpeed;
            pAggEntry->u4PortHighSpeed = pTmpPortEntry->u4LinkHighSpeed;
            pAggEntry->LaLacpMode = pTmpPortEntry->LaLacpMode;
            pAggEntry->AggConfigEntry.AggOrIndividual =
                ((pTmpPortEntry->LaLacPartnerInfo.
                  LaLacPortState.LaAggregation == LA_TRUE) &&
                 (pTmpPortEntry->LaLacActorInfo.
                  LaLacPortState.LaAggregation == LA_TRUE))
                ? LA_TRUE : LA_FALSE;
        }
        apSelectedPortEntry[u2Index] = pTmpPortEntry;
        u2Index++;
    }

    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while ((pTmpPortEntry != NULL) && (u2Index < LA_MAX_PORTS))
    {
        /* Take the configured aggregator entry and check if the LAG ID in it
         * matches with LAG ID in the port entry
         */
        pPortPartnerSystem = &(pTmpPortEntry->LaLacPartnerInfo.LaSystem);

        if (pTmpPortEntry->bPartnerActive == LA_TRUE)
        {
            LaGetPortEntry (pTmpPortEntry->u2PortIndex, &pPortEntry);
            if (pTmpPortEntry->LaLacPartnerInfo.u2IfKey == 0)
            {
                pPortEntry->u1PortDownReason = LA_PARTNER_KEY_ZERO;
            }

            else if (pTmpPortEntry->i4PauseAdminMode !=
                     pAggEntry->i4PauseAdminMode)
            {
                pPortEntry->u1PortDownReason = LA_PAUSEMODE_MISMATCH;
            }

            else if ((pAggEntry->u1SelectedPortCount == 0) &&
                     ((LA_SYSTEM_IDS_EQUAL ((*pAggActorSystem),
                                            (*pPortPartnerSystem)) == LA_TRUE)
                      && (pTmpPortEntry->LaLacActorInfo.u2IfKey ==
                          pTmpPortEntry->LaLacPartnerInfo.u2IfKey)))

            {
                pPortEntry->u1PortDownReason = LA_LOOPBACK_CONNECTION;
            }
            else if (LA_SYSTEM_IDS_EQUAL
                     ((*pAggPartnerSystem), (*pPortPartnerSystem)) != LA_TRUE)
            {
                pPortEntry->u1PortDownReason = LA_PARTNER_ID_MISMATCH;
            }

            else if (pAggEntry->AggConfigEntry.u2PartnerOperKey !=
                     pTmpPortEntry->LaLacPartnerInfo.u2IfKey)
            {
                pPortEntry->u1PortDownReason = LA_PARTNER_KEY_MISMATCH;
            }

            else if (pAggEntry->u4PortSpeed != pTmpPortEntry->u4LinkSpeed)
            {
                pPortEntry->u1PortDownReason = LA_SPEED_MISMATCH;
            }

            else if (pAggEntry->u4Mtu != pTmpPortEntry->u4Mtu)
            {
                pPortEntry->u1PortDownReason = LA_MTU_MISMATCH;
            }

            if ((u2Index == 0) &&
                (pTmpPortEntry->LaPortEnabled == LA_PORT_ENABLED) &&
                (pAggEntry->u1SelectedPortCount == 0) &&
                /* Do not permit aggregation of ports that are in 
                 * loop back connections within my system.
                 * ie., My Aggregator should not allow its own ports
                 * to get aggregated when they are connected together.
                 */
                (!((LA_SYSTEM_IDS_EQUAL ((*pAggActorSystem),
                                         (*pPortPartnerSystem)) == LA_TRUE) &&
                   (pTmpPortEntry->LaLacActorInfo.u2IfKey ==
                    pTmpPortEntry->LaLacPartnerInfo.u2IfKey))) &&
                (pTmpPortEntry->u4Mtu == pAggEntry->u4Mtu) &&
                (pTmpPortEntry->i4PauseAdminMode
                 == pAggEntry->i4PauseAdminMode) &&
                /* Below check for 0 values for key and port identifier
                 * in other than defaulted state done for ANVL LACP
                 * 4.5 and 4.11 */
                ((pTmpPortEntry->LaLacPartnerInfo.u2IfKey != 0) &&
                 (pTmpPortEntry->LaLacPartnerInfo.u2IfIndex != 0)))
            {
                /* copy the partner LAG ID as the Aggregator's partner LAG ID */
                LA_MEMCPY (pAggPartnerSystem, pPortPartnerSystem,
                           sizeof (tLaSystem));

                /* copy the partner key for the port to this aggregator */
                pAggEntry->AggConfigEntry.u2PartnerOperKey =
                    pTmpPortEntry->LaLacPartnerInfo.u2IfKey;

                /* copy the port speed & LA mode (LACP or MANUAL) as the aggregator 
                 * speed so that all other ports attaching to this aggregator later 
                 * can be verified for this requirement.
                 */
                pAggEntry->u4PortSpeed = pTmpPortEntry->u4LinkSpeed;
                pAggEntry->u4PortHighSpeed = pTmpPortEntry->u4LinkHighSpeed;
                pAggEntry->LaLacpMode = pTmpPortEntry->LaLacpMode;
                /* Copy the Mtu value that should be restored */
                pAggEntry->AggConfigEntry.AggOrIndividual =
                    ((pTmpPortEntry->LaLacPartnerInfo.
                      LaLacPortState.LaAggregation == LA_TRUE) &&
                     (pTmpPortEntry->LaLacActorInfo.
                      LaLacPortState.LaAggregation == LA_TRUE))
                    ? LA_TRUE : LA_FALSE;

                apSelectedPortEntry[u2Index] = pTmpPortEntry;
                u2Index++;
            }
            else if ((pTmpPortEntry->LaPortEnabled == LA_PORT_ENABLED) &&
                     (LA_SYSTEM_IDS_EQUAL ((*pAggPartnerSystem),
                                           (*pPortPartnerSystem)) == LA_TRUE) &&
                     (pAggEntry->AggConfigEntry.u2PartnerOperKey ==
                      pTmpPortEntry->LaLacPartnerInfo.u2IfKey) &&
                     (pAggEntry->AggConfigEntry.AggOrIndividual == LA_TRUE) &&
                     (pAggEntry->u4PortSpeed == pTmpPortEntry->u4LinkSpeed) &&
                     ((pAggEntry->u4PortHighSpeed ==
                       pTmpPortEntry->u4LinkHighSpeed)
                      || (pAggEntry->u4PortSpeed < LA_32BIT_MAX))
                     && (pAggEntry->LaLacpMode == pTmpPortEntry->LaLacpMode)
                     && (pTmpPortEntry->LaLacActorInfo.LaLacPortState.
                         LaAggregation == LA_TRUE)
                     && (pTmpPortEntry->LaLacPartnerInfo.LaLacPortState.
                         LaAggregation == LA_TRUE)
                     && (pTmpPortEntry->u4Mtu == pAggEntry->u4Mtu) &&
                     (pTmpPortEntry->i4PauseAdminMode =
                      pAggEntry->i4PauseAdminMode) &&
                     /* Below check for 0 values for key and port identifier
                      * in other than defaulted state done for ANVL LACP
                      * 4.5 and 4.11 */
                     ((pTmpPortEntry->LaLacPartnerInfo.u2IfKey != 0) &&
                      (pTmpPortEntry->LaLacPartnerInfo.u2IfIndex != 0)))
            {
                apSelectedPortEntry[u2Index] = pTmpPortEntry;
                u2Index++;
            }
        }
        else if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
        {
            if ((pTmpPortEntry != pRefPortEntry)
                && (pTmpPortEntry->LaPortEnabled == LA_PORT_ENABLED) &&
                (LA_SYSTEM_IDS_EQUAL ((*pAggPartnerSystem),
                                      (*pPortPartnerSystem)) == LA_TRUE) &&
                (pAggEntry->AggConfigEntry.u2PartnerOperKey ==
                 pTmpPortEntry->LaLacPartnerInfo.u2IfKey) &&
                (pAggEntry->AggConfigEntry.AggOrIndividual == LA_TRUE) &&
                (pAggEntry->u4PortSpeed == pTmpPortEntry->u4LinkSpeed) &&
                (pAggEntry->LaLacpMode == pTmpPortEntry->LaLacpMode) &&
                (pTmpPortEntry->LaLacActorInfo.
                 LaLacPortState.LaAggregation == LA_TRUE) &&
                (pTmpPortEntry->LaLacPartnerInfo.
                 LaLacPortState.LaAggregation == LA_TRUE) &&
                (pTmpPortEntry->u4Mtu == pAggEntry->u4Mtu) &&
                (pTmpPortEntry->i4PauseAdminMode
                 == pAggEntry->i4PauseAdminMode))
            {
                apSelectedPortEntry[u2Index] = pTmpPortEntry;
                u2Index++;
            }
        }

        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }

    if (u2Index == 0)
    {
        /* None of the ports are there in the active list i.e., None of 
         * the ports selected this aggregator 
         */
        return LA_SUCCESS;
    }
    u2TotalCount = u2Index;
    if (u2TotalCount <= pAggEntry->u1MaxPortsToAttach)
    {
        /* All the ports can become selected
         */

        for (u2Index = 0; u2Index < u2TotalCount; u2Index++)
        {
            if ((apSelectedPortEntry[u2Index]->AggSelected == LA_UNSELECTED) ||
                (apSelectedPortEntry[u2Index]->AggSelected == LA_STANDBY))
            {

                if ((apSelectedPortEntry[u2Index]->AggSelected == LA_STANDBY) &&
                    (LaMuxAnyPortWaitWhileTimerRunning
                     (apSelectedPortEntry[u2Index]) == LA_TRUE))
                {

                    bAllPortsReady = LA_FALSE;

                }

                apSelectedPortEntry[u2Index]->AggSelected = LA_SELECTED;
                if (pAggEntry->u4StandByPortCount >= 1)
                {
                    pAggEntry->u4StandByPortCount--;
                }
                pAggEntry->u1SelectedPortCount++;
                if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
                {
                    apSelectedPortEntry[u2Index]->PortUsingPartnerAdminInfo
                        = LA_TRUE;
                }

                if (!bAllPortsReady)
                {
                    /* Not all ports are Ready(Wait While Expired).
                       Move to next Port in Selected List.
                     */
                    continue;

                }

                /*
                   Two state transition caused by LA_MUX_EVENT_SELECTED
                   In DETACHED -> WAITING . Wait while timer will not run for any port.
                   In WAITING  -> ATTACHED . Only move if No Wait while timer is running.

                 */
                if ((apSelectedPortEntry[u2Index]->LaMuxState ==
                     LA_MUX_STATE_WAITING) && (bAllPortsReady == LA_TRUE))
                {
                    /* Standby port moving to ATTACHED. Set the bReady to TRUE */
                    apSelectedPortEntry[u2Index]->bReady = LA_TRUE;

                }

                LaLacMuxControlMachine (apSelectedPortEntry[u2Index],
                                        LA_MUX_EVENT_SELECTED);

            }
        }
    }
    else
    {
        /* Depending on standby constraints, 
         * choose if the port is to be selected or standby 
         */
        LaSortOnStandbyConstraints (apSelectedPortEntry, u2TotalCount);

        /* Now the top pAggEntry->u1MaxPortsToAttach number of elements
         * (or ports) in the sorted array gives me the links that are to
         * be active. Rest of the links are to be made standby.
         * First change the active links to standby links and then make
         * new links to be active
         */
        for (u2Index = pAggEntry->u1MaxPortsToAttach;
             ((u2Index < u2TotalCount)
              && (u2Index <
                  LA_MAX_PORTS)) /* pAggEntry->u1SelectedPortCount */ ;
             u2Index++)
        {
            if (apSelectedPortEntry[u2Index]->AggSelected != LA_STANDBY)
            {
                if (apSelectedPortEntry[u2Index]->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                }
                /* This link is now to be made as standby */
                apSelectedPortEntry[u2Index]->AggSelected = LA_STANDBY;
                pAggEntry->u4StandByPortCount++;
                apSelectedPortEntry[u2Index]->u1PortDownReason =
                    LA_HOT_STANDBY_ERR;

                LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC,
                                 "Port %d aggregated to port-channel %d moved to Hot-Standby mode",
                                 u2Index,
                                 apSelectedPortEntry[u2Index]->pAggEntry->
                                 AggConfigEntry.u2ActorAdminKey);
                LA_GET_SYS_TIME (&
                                 (apSelectedPortEntry[u2Index]->
                                  u4BundStChgTime));
                LaSyslogPrintAcivePorts (apSelectedPortEntry[u2Index]);
                LaLinkStatusNotify (apSelectedPortEntry[u2Index]->u2PortIndex,
                                    LA_OPER_DOWN, LA_ADMIN_DOWN,
                                    LA_TRAP_PORT_DOWN);

                if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
                {
                    apSelectedPortEntry[u2Index]->PortUsingPartnerAdminInfo
                        = LA_TRUE;
                }

                /* Trigger the MUX state machine to move to WAITING state */
                if (LaLacMuxControlMachine (apSelectedPortEntry[u2Index],
                                            LA_MUX_EVENT_STANDBY) != LA_SUCCESS)
                {
                    return LA_FAILURE;
                }
#ifdef NPAPI_WANTED
                /*Indicate Standby port state to h/w. */
                LaUpdatePortChannelStatusToHw (apSelectedPortEntry[u2Index]->
                                               u2PortIndex, LA_PORT_DISABLED,
                                               LA_STANDBY_INDICATION);
#endif
            }
        }
        for (u2Index = 0; u2Index < pAggEntry->u1MaxPortsToAttach; u2Index++)
        {
            /* Change the AggSelected variable of those ports that are
             * not already in selected state. Donot do anything if the
             * port is already in selected state.
             */
            if (apSelectedPortEntry[u2Index]->AggSelected != LA_SELECTED)
            {

                /* Check for Wait While Timer running for any port.
                   If Yes don't call Mux Machine let it expire to do its work.
                   If No(i.e it can be a standby port trying to become as active port
                   whose wait while timer would have already expired).
                   Call the Mux Machine.
                 */

                if ((apSelectedPortEntry[u2Index]->AggSelected == LA_STANDBY) &&
                    (LaMuxAnyPortWaitWhileTimerRunning
                     (apSelectedPortEntry[u2Index]) == LA_TRUE))
                {

                    bAllPortsReady = LA_FALSE;

                }
                /* This link is now to be made as Selected */
                apSelectedPortEntry[u2Index]->AggSelected = LA_SELECTED;
                pAggEntry->u1SelectedPortCount++;
                if (pAggEntry->u4StandByPortCount >= 1)
                {
                    pAggEntry->u4StandByPortCount--;
                }

                if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
                {
                    apSelectedPortEntry[u2Index]->PortUsingPartnerAdminInfo
                        = LA_TRUE;
                }

                if (!bAllPortsReady)
                {
                    /* Not all ports are Ready(Wait While Expired). 
                       Move to next Port in Selected List.
                     */
                    continue;

                }

                /*
                   Two state transition caused by LA_MUX_EVENT_SELECTED
                   In DETACHED -> WAITING . Wait while timer will not run for any port.
                   In WAITING  -> ATTACHED . Only move if No Wait while timer is running.
                 */
                if ((apSelectedPortEntry[u2Index]->LaMuxState ==
                     LA_MUX_STATE_WAITING) && (bAllPortsReady == LA_TRUE))
                {
                    /* Standby port moving to ATTACHED. Set the bReady to TRUE */
                    apSelectedPortEntry[u2Index]->bReady = LA_TRUE;

                }

                /* Trigger the MUX state machine to attach to aggregator */
                if (LaLacMuxControlMachine (apSelectedPortEntry[u2Index],
                                            LA_MUX_EVENT_SELECTED) !=
                    LA_SUCCESS)
                {
                    return LA_FAILURE;
                }

            }
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaSelectManualAggGroup                               */
/*                                                                           */
/* Description        : This is the core selection logic called for making   */
/*                       the port to be SELECTED for the portchannel in      */
/*                       MANUAL mode .                                       */
/*                                                                           */
/* Input(s)           : pAggEntry- AggEntry for which the ports to be SELECTED*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/

INT4
LaSelectManualAggGroup (tLaLacAggEntry * pAggEntry)
{
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaSelect           PrevAggSelected;

    UNUSED_PARAM (PrevAggSelected);

    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while (pTmpPortEntry != NULL)
    {
        PrevAggSelected = pTmpPortEntry->AggSelected;

        if (pTmpPortEntry->LaPortEnabled == LA_PORT_ENABLED)
        {
            if (pTmpPortEntry->AggSelected == LA_UNSELECTED)
            {
                if ((pAggEntry->u1SelectedPortCount == 0) &&
                    (pTmpPortEntry->LaLacActorInfo.
                     LaLacPortState.LaAggregation == LA_TRUE) &&
                    (pTmpPortEntry->u4Mtu == pAggEntry->u4Mtu) &&
                    (pTmpPortEntry->i4PauseAdminMode
                     == pAggEntry->i4PauseAdminMode))
                {
                    pAggEntry->u4PortSpeed = pTmpPortEntry->u4LinkSpeed;
                    pAggEntry->u4PortHighSpeed = pTmpPortEntry->u4LinkHighSpeed;
                    pAggEntry->LaLacpMode = pTmpPortEntry->LaLacpMode;
                    pTmpPortEntry->AggSelected = LA_SELECTED;
                    pAggEntry->u1SelectedPortCount++;
                    if (pAggEntry->u4StandByPortCount >= 1)
                    {
                        pAggEntry->u4StandByPortCount--;
                    }

                    LaLacMuxControlMachine (pTmpPortEntry,
                                            LA_MUX_EVENT_SELECTED);
                }
                else if ((pAggEntry->u4PortSpeed == pTmpPortEntry->u4LinkSpeed)
                         && ((pAggEntry->u4PortHighSpeed ==
                              pTmpPortEntry->u4LinkHighSpeed)
                             || (pAggEntry->u4PortSpeed < LA_32BIT_MAX))
                         && (pAggEntry->LaLacpMode == pTmpPortEntry->LaLacpMode)
                         && (pTmpPortEntry->LaLacActorInfo.LaLacPortState.
                             LaAggregation == LA_TRUE)
                         && (pTmpPortEntry->u4Mtu == pAggEntry->u4Mtu) &&
                         (pTmpPortEntry->i4PauseAdminMode
                          == pAggEntry->i4PauseAdminMode))
                {
                    pTmpPortEntry->AggSelected = LA_SELECTED;
                    pAggEntry->u1SelectedPortCount++;
                    if (pAggEntry->u4StandByPortCount >= 1)
                    {
                        pAggEntry->u4StandByPortCount--;
                    }
                    LaLacMuxControlMachine (pTmpPortEntry,
                                            LA_MUX_EVENT_SELECTED);
                }
            }
        }
        else
        {
            if (pTmpPortEntry->AggSelected == LA_SELECTED)
            {
                pTmpPortEntry->AggSelected = LA_UNSELECTED;
                pAggEntry->u1SelectedPortCount--;
                LaLacMuxControlMachine (pTmpPortEntry, LA_MUX_EVENT_UNSELECTED);

#ifdef NPAPI_WANTED
                /*Indicate Standby port state to h/w. */
                LaUpdatePortChannelStatusToHw (pTmpPortEntry->u2PortIndex,
                                               LA_PORT_DISABLED,
                                               LA_STANDBY_INDICATION);
#endif
            }
        }
        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaSortOnStandbyConstraints                           */
/*                                                                           */
/* Description        : This function is used to sort the portEntry          */
/*                       based on standby/selected constraints               */
/*                                                                           */
/* Input(s)           : ppPortEntry- array of PortEntry which is configured  */
/*                      for the portchannel.                                 */
/*                      u2PortCount- Number of ports in the array.           */
/*                                                                           */
/* Output(s)          : ppPortEntry - sorted portEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
LaSortOnStandbyConstraints (tLaLacPortEntry ** ppPortEntry, UINT2 u2PortCount)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacPortEntry    *pBestPortEntry;
    tLaLacPortEntry    *pCheckPortEntry;
    tLaLacIfInfo       *pBestActorInfo;
    tLaLacIfInfo       *pCheckActorInfo;
    tLaLacIfInfo       *pBestPartnerInfo;
    tLaLacIfInfo       *pCheckPartnerInfo;
    tLaBoolean          bRetVal;
    UINT2               u2CmpIndex;
    UINT2               u2Index;

    /* The no of ports whose AggSelected is TRUE is more than 
     * LA_MAX_PORTS_PER_AGG. Select the active ports whose PortStandBy
     * is to be made LA_FALSE 
     */
    pPortEntry = ppPortEntry[0];

    /* Find whether the ACTOR or the PARTNER has higher SYSTEM ID */
    bRetVal =
        LA_ACTOR_HAS_HIGHER_SYSTEM_ID (pPortEntry->LaLacActorInfo.LaSystem,
                                       pPortEntry->LaLacPartnerInfo.LaSystem);

    if (bRetVal == LA_TRUE)
    {
        /* Actor System ID is higher than the Partner System ID */

        for (u2Index = 0; u2Index < u2PortCount; u2Index++)
        {

            for (u2CmpIndex = (UINT1) (u2Index + 1); u2CmpIndex < u2PortCount;
                 u2CmpIndex++)
            {

                pBestPortEntry = ppPortEntry[u2Index];
                pCheckPortEntry = ppPortEntry[u2CmpIndex];

                pBestActorInfo = &(pBestPortEntry->LaLacActorInfo);
                pCheckActorInfo = &(pCheckPortEntry->LaLacActorInfo);

                /* If Check Port has higher port ID, make it the Best Port */

                if (LA_CHECK_PORT_HAS_HIGHER_PORT_ID
                    (pCheckActorInfo, pBestActorInfo) == LA_TRUE)
                {

                    ppPortEntry[u2Index] = pCheckPortEntry;
                    ppPortEntry[u2CmpIndex] = pBestPortEntry;

                }
            }

            /* apAggSelectedPortEntry[u2Index]->PortStandBy = LA_FALSE; */
        }

    }
    else
    {
        /* Partner System ID is higher than the Actor System ID */

        for (u2Index = 0; u2Index < u2PortCount; u2Index++)
        {

            for (u2CmpIndex = (UINT1) (u2Index + 1); u2CmpIndex < u2PortCount;
                 u2CmpIndex++)
            {

                pBestPortEntry = ppPortEntry[u2Index];
                pCheckPortEntry = ppPortEntry[u2CmpIndex];

                pBestPartnerInfo = &(pBestPortEntry->LaLacPartnerInfo);
                pCheckPartnerInfo = &(pCheckPortEntry->LaLacPartnerInfo);

                /* If Check Port has higher port ID, make it the Best Port */
                if (LA_CHECK_PORT_HAS_HIGHER_PORT_ID
                    (pCheckPartnerInfo, pBestPartnerInfo) == LA_TRUE)
                {
                    ppPortEntry[u2Index] = pCheckPortEntry;
                    ppPortEntry[u2CmpIndex] = pBestPortEntry;
                }
            }
            /* apAggSelectedPortEntry[u2Index]->PortStandBy = LA_FALSE; */
        }
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaSelectRefPortForAdminInfo                          */
/*                                                                           */
/* Description        : This function is used to find a reference port       */
/*                      for aggregation from partner admin informations      */
/*                                                                           */
/* Input(s)           : pAggEntry - pointer to the Aggregator to which the   */
/*                       port should be added.                               */
/*                                                                           */
/* Output(s)          : ppPortEntry - Reference port entry.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaSelectRefPortForAdminInfo (tLaLacAggEntry * pAggEntry,
                             tLaLacPortEntry ** ppPortEntry)
{
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacPortEntry    *pRefPortEntry = NULL;
    tLaSystem          *pAggActorSystem;
    tLaSystem          *pPortPartnerSystem;

    pAggActorSystem = &(pAggEntry->AggConfigEntry.ActorSystem);
    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while (pTmpPortEntry != NULL)
    {
        pPortPartnerSystem = &(pTmpPortEntry->LaLacPartnerInfo.LaSystem);
        if ((pTmpPortEntry->LaPortEnabled == LA_PORT_ENABLED) &&
            /* Do not permit aggregation of ports that are in
             * loop back connections within my system.
             * ie., My Aggregator should not allow its own ports
             * to get aggregated when they are connected together.
             * Also the refernce port's mtu should be same as the port
             * channel's mtu
             */
            (!((LA_SYSTEM_IDS_EQUAL ((*pAggActorSystem),
                                     (*pPortPartnerSystem)) == LA_TRUE) &&
               (pTmpPortEntry->LaLacActorInfo.u2IfKey ==
                pTmpPortEntry->LaLacPartnerInfo.u2IfKey))) &&
            (pAggEntry->u4Mtu == pTmpPortEntry->u4Mtu) &&
            (pAggEntry->i4PauseAdminMode == pTmpPortEntry->i4PauseAdminMode))
        {
            if (pRefPortEntry == NULL)
            {
                pRefPortEntry = pTmpPortEntry;
            }
            else if ((pRefPortEntry->LaLacPartnerAdminInfo.u2IfPriority
                      > pTmpPortEntry->LaLacPartnerAdminInfo.u2IfPriority)
                     ||
                     ((pRefPortEntry->LaLacPartnerAdminInfo.u2IfPriority ==
                       pTmpPortEntry->LaLacPartnerAdminInfo.u2IfPriority) &&
                      (pRefPortEntry->u2PortIndex >
                       pTmpPortEntry->u2PortIndex)))
            {
                pRefPortEntry = pTmpPortEntry;
            }
        }
        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }
    /* NOTE: pRefPortEntry returned can also be NULL - when none of the ports are
     * suitable for attaching to the aggregator
     */
    *ppPortEntry = pRefPortEntry;
    return;
}

/*****************************************************************************/
/* Function Name      : LaSelectRefPortForAdminInfo                          */
/*                                                                           */
/* Description        : This function unselects all the links from the       */
/*                      aggregator when selection logic is applied with      */
/*                      partner admin values.                                */
/*                                                                           */
/* Input(s)           : pPortEntry - Reference Port Entry                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*****************************************************************************/
tLaBoolean
LaUnSelectGroupIfRefPortLAGChanged (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    UINT1               AggOrIndividual = LA_TRUE;
    tLaSystem          *pPortPartnerSystem;
    tLaSystem          *pAggPartnerSystem;

    pAggEntry = pPortEntry->pAggEntry;
    if (pAggEntry->u1SelectedPortCount == 0)
    {
        return LA_TRUE;
    }
    pAggPartnerSystem = &(pAggEntry->AggConfigEntry.PartnerSystem);
    pPortPartnerSystem = &(pPortEntry->LaLacPartnerInfo.LaSystem);

    if ((LA_SYSTEM_IDS_EQUAL ((*pAggPartnerSystem),
                              (*pPortPartnerSystem)) == LA_TRUE) &&
        (pAggEntry->AggConfigEntry.u2PartnerOperKey ==
         pPortEntry->LaLacPartnerInfo.u2IfKey) &&
        (pAggEntry->u4PortSpeed == pPortEntry->u4LinkSpeed) &&
        (pAggEntry->LaLacpMode == pPortEntry->LaLacpMode))
    {
        AggOrIndividual = ((pPortEntry->LaLacPartnerInfo.
                            LaLacPortState.LaAggregation == LA_TRUE) &&
                           (pPortEntry->LaLacActorInfo.
                            LaLacPortState.LaAggregation == LA_TRUE))
            ? LA_TRUE : LA_FALSE;
        if (pAggEntry->AggConfigEntry.AggOrIndividual == AggOrIndividual)
        {
            /* Return FALSE when 
             * 1) this reference port is aggregatable and
             *    aggregator is aggregatable or
             * 2) this reference port is individual and
             *    this is already selected in the group.
             */
            if ((AggOrIndividual == LA_TRUE) ||
                ((AggOrIndividual == LA_FALSE) &&
                 (pPortEntry->AggSelected == LA_SELECTED)))
            {
                return LA_FALSE;
            }
        }
    }

    /* Unselect all the ports in the aggregator */
    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while ((pTmpPortEntry != NULL) && (pAggEntry->u1SelectedPortCount > 0))
    {
        if ((pTmpPortEntry->PortUsingPartnerAdminInfo == LA_TRUE) &&
            (pTmpPortEntry->AggSelected == LA_SELECTED))
        {
            pAggEntry->u1SelectedPortCount--;
            pTmpPortEntry->AggSelected = LA_UNSELECTED;
            LaLacMuxControlMachine (pTmpPortEntry, LA_MUX_EVENT_UNSELECTED);
            pTmpPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
        }
        LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
    }
    return LA_TRUE;
}

/* end of file */
