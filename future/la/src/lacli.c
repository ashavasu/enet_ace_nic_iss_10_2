
/* SOURCE FILE HEADER :
 *
 * $Id: lacli.c,v 1.213 2018/02/05 10:29:23 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : lacli.c                                          |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Vibha R                                          |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : LA                                               |
 * |                                                                           |
 * |  MODULE NAME           : LA configuration                                 |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           | 
 * |                          fsla.mib, stdla.mib                              |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __LACLI_C__
#define __LACLI_C__

#include "lahdrs.h"
#include "lacli.h"
#include "fslacli.h"
#include "stdlacli.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_la_cmd                                 */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the LA module as      */
/*                        defined in lacmd.h                                 */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_la_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[LA_CLI_MAX_COMMANDS];
    INT1                argno = 0;

    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Key = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    INT4                i4Key = 0;

    UINT4               u4LacpMode = 0;
    UINT4               u4DefPort = 0;
#ifdef DLAG_WANTED
    UINT4               u4DistributePort = 0;
    UINT1               au1MemberPorts[LA_PORT_LIST_SIZE];
#endif
    UINT1               u1IsActiveLacp = 0;
    UINT1               u1IsDetail = 0;
#if (defined DLAG_WANTED) || (defined ICCH_WANTED)
    UINT1               u1IsConsolidated = 0;
#endif
    tMacAddr            SystemID;

    /* If LA is not Started, then all commands are considered as invalid,
     * except Start/Shutdown command
     */
    if ((u4Command != CLI_LA_SYSTEMCONTROL) &&
        (u4Command != CLI_LA_NO_SYSTEMSHUT) &&
        (LA_SYSTEM_CONTROL == LA_SHUTDOWN))
    {
        if (u4Command != CLI_LA_SYSTEMSHUT)
        {
            CliPrintf (CliHandle, "\r%% Port-channel is shutdown\r\n");
        }
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store LA_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == LA_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, LaLock, LaUnLock);

    LA_LOCK ();

    switch (u4Command)
    {
        case CLI_LA_SYSTEMSHUT:
            i4RetStatus = LaSetSystemShut (CliHandle, LA_SHUTDOWN);
            /* This command shutsdown the LA module */
            break;

        case CLI_LA_NO_SYSTEMSHUT:
            i4RetStatus = LaSetSystemShut (CliHandle, LA_START);
            /* This command starts the LA module */
            break;

        case CLI_LA_SYSTEMCONTROL:
            i4RetStatus =
                LaSetSystemControl (CliHandle, CLI_PTR_TO_U4 (args[0]));
            /* This command enables or disables LA module */
            break;

        case CLI_LA_SYSTEM_PRIORITY:
            i4RetStatus = LaSetSystemPriority (CliHandle, *(args[0]));
            break;

        case CLI_LA_SYSTEM_ID:

            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            StrToMac ((UINT1 *) args[0], SystemID);
            i4RetStatus = LaSetSystemID (CliHandle, SystemID);
            break;

        case CLI_LA_NO_SYSTEM_PRIORITY:
            i4RetStatus =
                LaSetSystemPriority (CliHandle, LA_DEFAULT_SYSTEM_PRIORITY);
            break;

        case CLI_LA_NO_SYSTEM_ID:

            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            LaCfaGetSysMacAddress (SystemID);
            i4RetStatus = LaSetSystemID (CliHandle, SystemID);
            break;

        case CLI_LA_POLICY:
            i4RetStatus = LaSetLaPolicy (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                         CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_NO_POLICY:
            i4RetStatus =
                LaSetLaPolicy (CliHandle, CLI_PTR_TO_U4 (args[0]),
                               LA_SELECT_POLICY_DEF);
            break;

        case CLI_LA_TIMEOUT:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = LaSetTimeout (CliHandle, u4IfIndex,
                                        CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_LA_NO_TIMEOUT:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = LaSetTimeout (CliHandle, u4IfIndex, LA_LONG_TIMEOUT);
            break;

        case CLI_LA_ACTOR_PORTPRIORITY:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                LaSetActorPortPriority (CliHandle, u4IfIndex, *(args[0]));
            break;

        case CLI_LA_ACTOR_ADMIN_PORT:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                LaSetActorAdminPort (CliHandle, u4IfIndex, *(args[0]));
            break;

        case CLI_LA_NO_ACTOR_PORTPRIORITY:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                LaSetActorPortPriority (CliHandle, u4IfIndex,
                                        LA_DEFAULT_PORT_PRIORITY);
            break;

        case CLI_LA_PORT_ACTOR_MODE:

            if (CLI_PTR_TO_U4 (args[1]) == LA_CLI_ACTIVE)
            {
                u4LacpMode = (UINT4) LA_MODE_LACP;
                u1IsActiveLacp = LA_TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[1]) == LA_CLI_PASSIVE)
            {
                u4LacpMode = (UINT4) LA_MODE_LACP;
                u1IsActiveLacp = LA_FALSE;
            }
            else if (CLI_PTR_TO_U4 (args[1]) == LA_CLI_MANUAL)
            {
                u4LacpMode = (UINT4) LA_MODE_MANUAL;
                u1IsActiveLacp = LA_FALSE;
            }
            u4IfIndex = CLI_GET_IFINDEX ();

            /* When u1HwLaCreateOnPort is not supported, 
             * LAG creation is possible with front ports only */
            if (ISS_HW_SUPPORTED !=
                IssGetHwCapabilities (ISS_HW_LA_CREATE_ON_ALL_PORT))
            {
                if (((u4IfIndex > ISS_HW_FRONT_PORTS_P1_To_P4)
                     && (u4IfIndex != ISS_HW_SFP_PORTS_P7_P8))
                    && (u4IfIndex != ISS_HW_SFP_PORTS_P7_P8 + 1))
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Target Limitation:LAG can be created only between the front ports (P1 to P4)"
                               "and the two ports mapped to 6.5Gbps interface (P7 and P8)\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            i4RetStatus = LaSetPortActorMode (CliHandle, u4IfIndex,
                                              (UINT2) (*(args[0])),
                                              u4LacpMode, u1IsActiveLacp);
            break;

        case CLI_LA_PORT_MODE_DISABLED:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = LaSetActorPortModeDisabled (CliHandle, u4IfIndex);
            break;

        case CLI_LA_PORT_WAIT_TIME:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = LaSetActorPortWaitTime (CliHandle, u4IfIndex,
                                                  *(args[0]));
            break;

        case CLI_LA_NO_PORT_WAIT_TIME:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = LaSetActorPortWaitTime (CliHandle, u4IfIndex,
                                                  LA_PORT_WAITTIME_DEF);
            break;

        case CLI_LA_PORT_ADMIN_KEY:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = LaSetPortActorAdminKeyLacpMode
                (CliHandle, u4IfIndex, CLI_PTR_TO_U4 (args[0]),
                 CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_LA_PORTCHANNEL_DEF_PORT:

            u4DefPort = CLI_PTR_TO_U4 (args[0]);
            u4IfIndex = (UINT2) CLI_GET_IFINDEX ();

            i4RetStatus = LaAttachDefaultPortToAggregator (CliHandle,
                                                           u4DefPort,
                                                           u4IfIndex);
            break;

        case CLI_LA_PORTCHANNEL_NO_DEF_PORT:

            u4IfIndex = (UINT2) CLI_GET_IFINDEX ();

            i4RetStatus = LaAttachDefaultPortToAggregator (CliHandle, 0,
                                                           u4IfIndex);
            break;

        case CLI_LA_PORTCHANNEL_MAX_PORTS:

            u4IfIndex = (UINT2) CLI_GET_IFINDEX ();

            i4RetStatus = LaSetActorPortChannelMaxPorts (CliHandle,
                                                         *(args[0]), u4IfIndex);
            break;

#ifdef DLAG_WANTED
        case CLI_LA_DLAG_SYSTEM_PRIORITY:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaDLAGSetDLAGSystemPriority (CliHandle, *(args[0]),
                                                       u4IfIndex);
            break;

        case CLI_LA_DLAG_NO_SYSTEM_PRIORITY:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaDLAGSetDLAGSystemPriority (CliHandle,
                                                       LA_DLAG_DEFAULT_SYSTEM_PRIORITY,
                                                       u4IfIndex);
            break;

        case CLI_LA_DLAG_SYSTEM_ID:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            StrToMac ((UINT1 *) args[0], SystemID);

            i4RetStatus =
                LaDLAGSetDLAGSystemID (CliHandle, SystemID, u4IfIndex);
            break;

        case CLI_LA_DLAG_NO_SYSTEM_ID:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            i4RetStatus =
                LaDLAGSetDLAGSystemID (CliHandle, SystemID, u4IfIndex);
            break;

        case CLI_LA_DLAG_PERIODIC_SYNC_TIME:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaDLAGSetDLAGPeriodicSyncTime (CliHandle, *(args[0]),
                                                         u4IfIndex);
            break;

        case CLI_LA_DLAG_MS_SELECTION_WAIT_TIME:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                LaDLAGSetDLAGMSSelectionWaitTime (CliHandle, *(args[0]),
                                                  u4IfIndex);
            break;

        case CLI_LA_DLAG_DISTRIBUTE_PORT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaDLAGConfigureDistributePort (CliHandle,
                                                         CLI_PTR_TO_U4 (args
                                                                        [0]),
                                                         u4IfIndex);
            break;
        case CLI_LA_DLAG_NO_DISTRIBUTE_PORT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                LaDLAGConfigureDistributePort (CliHandle, u4DistributePort,
                                               u4IfIndex);
            break;

        case CLI_LA_DLAG_DISTRIBUTE_PORT_LIST:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            CLI_MEMSET (au1MemberPorts, 0, LA_PORT_LIST_SIZE);
            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     au1MemberPorts, LA_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid" "Port(s)\r\n");
                    break;
                }
            }
            i4RetStatus = LaDLAGConfigureDistributePortList (CliHandle,
                                                             au1MemberPorts,
                                                             u4IfIndex);
            break;

        case CLI_LA_DLAG_NO_DISTRIBUTE_PORT_LIST:
            CLI_MEMSET (au1MemberPorts, 0, LA_PORT_LIST_SIZE);
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                LaDLAGConfigureDistributePortList (CliHandle, au1MemberPorts,
                                                   u4IfIndex);
            break;

        case CLI_LA_DLAG_STATUS:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaDLAGSetDLAGStatus (CliHandle,
                                               CLI_PTR_TO_U4 (args[0]),
                                               u4IfIndex);
            break;

        case CLI_LA_DLAG_GLOBAL_STATUS:

            i4RetStatus =
                LaDLAGSetDLAGSystemStatus (CliHandle, CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_LA_DLAG_GLOBAL_DISTRIBUTE_PORT_LIST:

            CLI_MEMSET (au1MemberPorts, 0, LA_PORT_LIST_SIZE);
            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     au1MemberPorts, LA_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid" "Port(s)\r\n");
                    break;
                }
            }
            i4RetStatus = LaDLAGConfigureGlobalDistributePortList (CliHandle,
                                                                   au1MemberPorts);
            break;

        case CLI_LA_DLAG_GLOBAL_NO_DISTRIBUTE_PORT_LIST:
            CLI_MEMSET (au1MemberPorts, 0, LA_PORT_LIST_SIZE);

            i4RetStatus =
                LaDLAGConfigureGlobalDistributePortList (CliHandle,
                                                         au1MemberPorts);
            break;

        case CLI_LA_DLAG_GLOBAL_SYSTEM_PRIORITY:

            i4RetStatus =
                LaDLAGSetDLAGGlobalSystemPriority (CliHandle, *(args[0]));
            break;

        case CLI_LA_DLAG_GLOBAL_NO_SYSTEM_PRIORITY:
            i4RetStatus =
                LaDLAGSetDLAGGlobalSystemPriority (CliHandle,
                                                   LA_DLAG_DEFAULT_SYSTEM_PRIORITY);
            break;

        case CLI_LA_DLAG_GLOBAL_SYSTEM_ID:

            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            StrToMac ((UINT1 *) args[0], SystemID);

            i4RetStatus = LaDLAGSetDLAGGlobalSystemID (CliHandle, SystemID);
            break;

        case CLI_LA_DLAG_GLOBAL_NO_SYSTEM_ID:
            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            i4RetStatus = LaDLAGSetDLAGGlobalSystemID (CliHandle, SystemID);
            break;

        case CLI_LA_DLAG_GLOBAL_PERIODIC_SYNC_TIME:
            i4RetStatus =
                LaDLAGSetDLAGGlobalPeriodicSyncTime (CliHandle, *(args[0]));
            break;

        case CLI_LA_DLAG_GLOBAL_NO_PERIODIC_SYNC_TIME:
            i4RetStatus =
                LaDLAGSetDLAGGlobalPeriodicSyncTime (CliHandle,
                                                     LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME);
            break;

        case CLI_LA_DLAG_GLOBAL_DISTRIBUTE_PORT:
            i4RetStatus = LaDLAGConfigureGlobalDistributePort (CliHandle,
                                                               CLI_PTR_TO_U4
                                                               (args[0]));

            break;
        case CLI_LA_DLAG_GLOBAL_NO_DISTRIBUTE_PORT:
            i4RetStatus =
                LaDLAGConfigureGlobalDistributePort (CliHandle,
                                                     u4DistributePort);
            break;
        case CLI_LA_DLAG_REDUNDANCY:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaDLAGSetDLAGRedundancy (CliHandle,
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   u4IfIndex);
            break;
#endif /* DLAG_WANTED */
        case CLI_SHOW_LA_PORTCHANNEL_INFO:

            u4Key = CLI_PTR_TO_U4 (args[0]);
            u4CmdType = CLI_PTR_TO_U4 (args[1]);

            if (u4CmdType == LA_SHOW_REDUNDANCY)
            {
                if (u4Key == 0)
                {
                    LaRedReadSyncUpTable (CliHandle);
                }
                else
                {
                    LaRedPrintSyncUpInfoForPort (CliHandle, (UINT2) u4Key);
                }
            }
            else
            {
                LaShowPortChannelInfo (CliHandle, (UINT2) u4Key,
                                       (UINT1) u4CmdType);
            }
            break;

        case CLI_SHOW_LA_PORT_INFO:

            LaShowIntEtherChannelInfo (CliHandle, u4IfIndex);
            break;

        case CLI_SHOW_LACP_INFO:

            u4CmdType = CLI_PTR_TO_U4 (args[0]);

            if (u4CmdType == CLI_SHOW_LACP_COUNTERS)
            {
                LaShowLacpCounters (CliHandle, CLI_PTR_TO_I4 (args[1]));
            }
            else if (u4CmdType == CLI_SHOW_LACP_NEIGHBOR)
            {
                if ((UINT1 *) (args[2]) != NULL)
                {
                    u1IsDetail = 1;
                }
                LaShowLacpNeighbor (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                    u1IsDetail);
            }
            break;

#ifdef DLAG_WANTED
        case CLI_SHOW_LA_DLAG_INFO:

            u1IsConsolidated = LA_FALSE;

            LaDLAGShowDetail (CliHandle, CLI_PTR_TO_I4 (args[0]),
                              u1IsConsolidated);
            break;

        case CLI_SHOW_LA_DLAG_CONSOLIDATED:

            u1IsConsolidated = LA_TRUE;

            LaDLAGShowDetail (CliHandle, CLI_PTR_TO_I4 (args[0]),
                              u1IsConsolidated);

            break;

        case CLI_SHOW_LA_DLAG_COUNTERS:
            LaDLAGShowCounters (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
#endif /* DLAG_WANTED */

        case CLI_LA_SET_TRACE_LEVEL:

            i4RetStatus = LaCliSetTraceLevel (CliHandle,
                                              CLI_PTR_TO_I4 (args[0]),
                                              CLI_ENABLE);
            break;

        case CLI_LA_RESET_TRACE_LEVEL:

            i4RetStatus = LaCliSetTraceLevel (CliHandle,
                                              CLI_PTR_TO_I4 (args[0]),
                                              CLI_DISABLE);
            break;

        case CLI_LA_NO_PARTNER_INDEP:

            i4RetStatus =
                LaSetNoPartnerIndep (CliHandle, CLI_PTR_TO_U4 (args[0]));
            /* This command enables or disables independent when no partner
             * in the switch */
            break;
        case CLI_LA_ERR_TIME_SET:
            LaSetErrorRecTime (CliHandle, (INT4) *(args[0]));
            break;

        case CLI_LA_ERR_THRESHOLD_SET:
            LaSetErrorRecThreshold (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_ERR_THRESHOLD_RESET:
            LaSetErrorRecThreshold (CliHandle, LA_DEFAULT_REC_THRESHOLD);
            break;

#ifdef ICCH_WANTED
        case CLI_LA_MCLAG_GLOBAL_STATUS:

            i4RetStatus =
                LaSetMCLAGSystemStatus (CliHandle, CLI_PTR_TO_U4 (args[0]));

            break;
        case CLI_LA_MCLAG_GLOBAL_SYSTEM_ID:

            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            StrToMac ((UINT1 *) args[0], SystemID);

            i4RetStatus = LaMCLAGSetDLAGGlobalSystemID (CliHandle, SystemID);
            break;

        case CLI_LA_MCLAG_GLOBAL_SYSTEM_PRIORITY:

            i4RetStatus =
                LaMCLAGSetMCLAGGlobalSystemPriority (CliHandle, *(args[0]));
            break;

        case CLI_LA_MCLAG_GLOBAL_PERIODIC_SYNC_TIME:

            i4RetStatus =
                LaMCLAGSetMCLAGGlobalPeriodicTime (CliHandle, *(args[0]));
            break;

        case CLI_LA_MCLAG_GLOBAL_NO_PERIODIC_SYNC_TIME:

            i4RetStatus =
                LaMCLAGSetMCLAGGlobalPeriodicTime (CliHandle,
                                                   LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME);
            break;
        case CLI_LA_MCLAG_GLOBAL_NO_SYSTEM_ID:
            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            i4RetStatus = LaMCLAGSetDLAGGlobalSystemID (CliHandle, SystemID);
            break;

        case CLI_LA_MCLAG_GLOBAL_NO_SYSTEM_PRIORITY:
            i4RetStatus =
                LaMCLAGSetMCLAGGlobalSystemPriority (CliHandle,
                                                     LA_DLAG_DEFAULT_SYSTEM_PRIORITY);
            break;
        case CLI_LA_MCLAG_STATUS:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaMCLAGSetMCLAGStatus (CliHandle,
                                                 CLI_PTR_TO_U4 (args[0]),
                                                 u4IfIndex);
            break;

        case CLI_LA_MCLAG_SYSTEM_ID:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            StrToMac ((UINT1 *) args[0], SystemID);

            i4RetStatus =
                LaMCLAGSetMCLAGSystemID (CliHandle, SystemID, u4IfIndex);
            break;

        case CLI_LA_MCLAG_SYSTEM_PRIORITY:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaMCLAGSetMCLAGSystemPriority (CliHandle, *(args[0]),
                                                         u4IfIndex);
            break;

        case CLI_LA_MCLAG_NO_SYSTEM_ID:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            LA_MEMSET (SystemID, 0, LA_MAC_ADDRESS_SIZE);
            i4RetStatus =
                LaMCLAGSetMCLAGSystemID (CliHandle, SystemID, u4IfIndex);
            break;

        case CLI_LA_MCLAG_NO_SYSTEM_PRIORITY:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = LaMCLAGSetMCLAGSystemPriority (CliHandle,
                                                         LA_DLAG_DEFAULT_SYSTEM_PRIORITY,
                                                         u4IfIndex);
            break;

        case CLI_SHOW_LA_MCLAG_CONSOLIDATED:

            u1IsConsolidated = LA_TRUE;

            LaMCLAGShowDetail (CliHandle, CLI_PTR_TO_I4 (args[0]),
                               u1IsConsolidated);

            break;

        case CLI_SHOW_LA_MCLAG_INFO:

            u1IsConsolidated = LA_FALSE;

            LaMCLAGShowDetail (CliHandle, CLI_PTR_TO_I4 (args[0]),
                               u1IsConsolidated);
            break;
        case CLI_SHOW_LA_MCLAG_COUNTERS:
            LaMCLAGShowCounters (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
#endif
        case CLI_LA_PORT_DEF_STATE_THRESHOLD_SET:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            LaSetPortDefaultedThreshold (CliHandle, u4IfIndex,
                                         CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_DEF_STATE_THRESHOLD_SET:
            LaSetDefaultedThreshold (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_HW_FAILURE_REC_THRESHOLD_SET:
            LaSetHwFailureRecThreshold (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_SAME_STATE_REC_THRESHOLD_SET:
            LaSetSameStateRecThreshold (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_PORT_HW_FAILURE_REC_THRESHOLD_SET:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            LaSetPortHwFailureRecThreshold (CliHandle, u4IfIndex,
                                            CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_PORT_SAME_STATE_REC_THRESHOLD_SET:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            LaSetPortSameStateRecThreshold (CliHandle, u4IfIndex,
                                            CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_LA_REC_THRESH_EXCEED_ACT_SET:
            LaSetRecThresholdExceedAction (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_LA_MCLAG_CLEAR_COUNTER:
            LaMCLAGClearCounters (CliHandle);
            break;

        case CLI_LACP_CLEAR_COUNTERS:
            if (args[0] == NULL)
            {
                LaClearStatistics (CliHandle);
            }
            else
            {
                LaPortChannelClearStatistics (CliHandle,
                                              CLI_PTR_TO_I4 (args[0]));
            }
            break;
        case CLI_LA_MCLAG_NO_SYSTEMSHUT:
            i4RetStatus = LaMCLAGSystemControl (CliHandle, LA_START);
            break;

        case CLI_LA_MCLAG_SYSTEMSHUT:
            i4RetStatus = LaMCLAGSystemControl (CliHandle, LA_SHUTDOWN);
            break;

        case CLI_LA_ENHANCED_LB_STATUS:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CLI_FAILURE;
            if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
                == SNMP_SUCCESS)
            {
                if (nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key)
                    == SNMP_SUCCESS)
                {
                    if (((args[0]) != NULL) &&
                        ((*args[0]) == LA_ENHANCED_LB_ENABLE))
                    {
                        /* need to convert to key */
                        i4RetStatus = LaSetLaPolicy (CliHandle, i4Key,
                                                     LA_SELECT_ENHANCED);
                    }
                    else
                    {
                        /* need to convert to key */
                        i4RetStatus = LaSetLaPolicy (CliHandle, i4Key,
                                                     LA_SELECT_POLICY_DEF);
                    }
                }
            }
            break;

        case CLI_LA_RANDOMIZED_LB_STATUS:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CLI_FAILURE;
            if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
                == SNMP_SUCCESS)
            {
                if (nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key)
                    == SNMP_SUCCESS)
                {
                    if (((args[0]) != NULL) &&
                        ((*args[0]) == LA_RANDOMIZED_LB_ENABLE))
                    {
                        /* need to convert to key */
                        i4RetStatus = LaSetLaPolicy (CliHandle, i4Key,
                                                     LA_SELECT_RANDOMIZED);
                    }
                    else
                    {
                        /* need to convert to key */
                        i4RetStatus = LaSetLaPolicy (CliHandle, i4Key,
                                                     LA_SELECT_POLICY_DEF);
                    }
                }
            }
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_LA_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", LaCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    LA_UNLOCK ();

    return i4RetStatus;

}

/***************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : LaSetSystemShut                                   */
/*                                                                          */
/*     DESCRIPTION      : This function starts or shuts  the La 
 *                        System Control                                    */
/*                                                                          */
/*     INPUT            : i4LaStatus - LA_START/LA_SHUTDOWN                 */
/*                        CliHandle  - CLI Handler                          */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/****************************************************************************/
INT4
LaSetSystemShut (tCliHandle CliHandle, INT4 i4LaStatus)
{
    UINT4               u4ErrCode;
    INT4                i4LaEnableFlag;

    if (i4LaStatus == LA_START)
    {
        i4LaEnableFlag = LA_ENABLED;
    }
    else
    {
        i4LaEnableFlag = LA_DISABLED;
    }

    if (nmhTestv2FsLaSystemControl (&u4ErrCode, i4LaStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaSystemControl (i4LaStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4LaEnableFlag == LA_ENABLED)
    {

        if (nmhTestv2FsLaStatus (&u4ErrCode, i4LaEnableFlag) == SNMP_SUCCESS)
        {
            if (nmhSetFsLaStatus (i4LaEnableFlag) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        /* On execution of "no shutdown port-channel" LA module should be
         * started and enabled */

    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : LaSetSystemControl                                */
/*                                                                          */
/*     DESCRIPTION      : This function sets the La System Control          */
/*                                                                          */
/*     INPUT            : i4LaStatus - LA_START/LA_SHUTDOWN                 */
/*                        CliHandle  - CLI Handler                          */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/****************************************************************************/
INT4
LaSetSystemControl (tCliHandle CliHandle, INT4 i4LaStatus)
{
    UINT4               u4ErrCode;
    INT4                i4LaEnableFlag;

    if (i4LaStatus == LA_START)
    {
        i4LaEnableFlag = LA_ENABLED;
    }
    else
    {
        i4LaEnableFlag = LA_DISABLED;
    }

    if (nmhTestv2FsLaStatus (&u4ErrCode, i4LaEnableFlag) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsLaStatus (i4LaEnableFlag) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : LaSetNoPartnerIndep                               */
/*                                                                          */
/*     DESCRIPTION      : This function enables/disables Independent mode   */
/*                        when no partner in the switch.                    */
/*                                                                          */
/*     INPUT            : i4LaIndepMode - LA_ENABLED/LA_DISABLED            */
/*                        CliHandle  - CLI Handler                          */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/****************************************************************************/
INT4
LaSetNoPartnerIndep (tCliHandle CliHandle, INT4 i4LaIndepMode)
{
    UINT4               u4ErrCode = 0;
    INT4                i4LaEnableIndepFlag = 0;

    if (i4LaIndepMode == LA_ENABLED)
    {
        i4LaEnableIndepFlag = LA_ENABLED;
    }
    else if (i4LaIndepMode == LA_DISABLED)
    {
        i4LaEnableIndepFlag = LA_DISABLED;
    }

    if (nmhTestv2FsLaNoPartnerIndep (&u4ErrCode, i4LaEnableIndepFlag) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaNoPartnerIndep (i4LaEnableIndepFlag) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetSystemID                                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the LACP system ID              */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetSystemID (tCliHandle CliHandle, tMacAddr SystemID)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsLaActorSystemID (&u4ErrCode, SystemID) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaActorSystemID (SystemID) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetSystemPriority                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Actor Port Priority and the */
/*                        Actor Port System Priority.                        */
/*                                                                           */
/*     INPUT            : i4LaSystemPriority- System Priority value          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetSystemPriority (tCliHandle CliHandle, INT4 i4LaSystemPriority)
{
    UINT4               u4ErrCode;
    INT4                i4IfIndex;
    INT4                i4AggIndex = 0;

    /* when we set the ActorSystemPriority for any physical port or 
     * port channel, the ActorSystemPriority in all the agg port entries
     * in AggPortTable and in all the agg entries in the AggTable are
     * set with the given value.Also the global lacp system priority is
     * updated with the given value.So its enough to set the ActorSystemPriority
     * for a single physical port or port channel
     */
    if (nmhGetFirstIndexDot3adAggPortTable (&i4IfIndex) == SNMP_SUCCESS)
    {
        if (nmhTestv2Dot3adAggPortActorSystemPriority (&u4ErrCode,
                                                       i4IfIndex,
                                                       i4LaSystemPriority)
            != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetDot3adAggPortActorSystemPriority (i4IfIndex,
                                                    i4LaSystemPriority)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else
    {
        if (nmhGetFirstIndexDot3adAggTable (&i4AggIndex) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%% No Port-channel/Port available\r\n");
            return (CLI_FAILURE);
        }

        if (nmhTestv2Dot3adAggActorSystemPriority (&u4ErrCode,
                                                   i4AggIndex,
                                                   i4LaSystemPriority)
            != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetDot3adAggActorSystemPriority (i4AggIndex, i4LaSystemPriority)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetPolicy                                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the La Policy                   */
/*                                                                           */
/*     INPUT            :  i4Key, i4SelPolicy                                */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetLaPolicy (tCliHandle CliHandle, INT4 i4Key, INT4 i4SelPolicy)
{
    tLaLacAggEntry     *pLaLacAggEntry;
    UINT4               u4ErrCode;
    INT4                i4Index;
    UINT4               u4ChannelIfIndex;

    if (i4Key != 0)
    {
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
            return (CLI_FAILURE);
        }
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }
    else
    {
        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%% No Port-channel available\r\n");
            return (CLI_SUCCESS);
        }

    }

    do
    {
        u4ChannelIfIndex = i4Index;
        if (nmhTestv2FsLaPortChannelSelectionPolicyBitList
            (&u4ErrCode, u4ChannelIfIndex, i4SelPolicy) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsLaPortChannelSelectionPolicyBitList (u4ChannelIfIndex,
                                                         i4SelPolicy) !=
            SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (i4Key != 0)
        {
            break;
        }

    }
    while (nmhGetNextIndexDot3adAggTable ((INT4) u4ChannelIfIndex, &i4Index)
           != SNMP_FAILURE);

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetTimeout                                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port Lacp Timeout           */
/*                                                                           */
/*     INPUT            : u4Timeout - Long/short                             */
/*                        u4IfaceId, CliHandle CLIHandler                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetTimeout (tCliHandle CliHandle, UINT4 u4IfaceId, UINT4 u4Timeout)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tSNMP_OCTET_STRING_TYPE PortAdminState;
    UINT1               u1SetTimeout = LA_LACPTIMEOUT_BITMASK;
    UINT1               u1State;
    UINT1               u1Value;

    LaGetPortEntry ((UINT2) u4IfaceId, &pPortEntry);

    if (pPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        return CLI_FAILURE;
    }

    PortAdminState.i4_Length = 1;
    PortAdminState.pu1_OctetList = &u1SetTimeout;

    LaGetPortState (&(pPortEntry->LaLacActorAdminInfo.LaLacPortState),
                    &u1State);

    u1Value = (UINT1) (u1State & LA_LACPTIMEOUT_BITMASK);
    if (u4Timeout == LA_SHORT_TIMEOUT)
    {
        if (u1Value)
        {
            /* no need to change */
        }
        else
        {
            nmhSetDot3adAggPortActorAdminState (u4IfaceId, &PortAdminState);
        }
    }
    else if (u4Timeout == LA_LONG_TIMEOUT)
    {
        if (u1Value)
        {
            nmhSetFsLaPortActorResetAdminState (u4IfaceId, &PortAdminState);
        }
        else
        {
            /* no need to change it */
        }
    }
    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaActorPortPriority                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Actor Port Priority and the */
/*                        Actor Port System Priority.                        */
/*                                                                           */
/*     INPUT            : i4PortPriority- Port Priority value , u4IfIndex    */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetActorPortPriority (tCliHandle CliHandle, UINT4 u4IfaceId,
                        INT4 i4PortPriority)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot3adAggPortActorPortPriority (&u4ErrCode, u4IfaceId,
                                                 i4PortPriority) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetDot3adAggPortActorPortPriority (u4IfaceId,
                                              i4PortPriority) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaActorAdminPort                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Actor Admin Port which will */
/*                        be filled in LACP PDUs.                            */
/*                                                                           */
/*     INPUT            : i4AdminPort   - Admin Port                         */
/*                        u4IfaceId                                          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetActorAdminPort (tCliHandle CliHandle, UINT4 u4IfaceId, INT4 i4AdminPort)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsLaPortActorAdminPort (&u4ErrCode, (INT4) u4IfaceId,
                                         (INT4) i4AdminPort) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_INVALID_PORT);
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaPortActorAdminPort (u4IfaceId, i4AdminPort) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetPortActorMode                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the LacpMode and LacpActivity   */
/*                        for the port(s) specified. LacpActivity has meaning*/
/*                        only when LacpMode is LacpEnable.                  */
/*                                                                           */
/*     INPUT            : u4LacpMode , u1IsActiveLacp, u4IfaceId, u2Key      */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success / Failure                                  */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetPortActorMode (tCliHandle CliHandle, UINT4 u4IfaceId, UINT2 u2Key,
                    UINT4 u4LacpMode, UINT1 u1IsActiveLacp)
{

    UINT4               u4ErrCode = 0;
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1Value;
    UINT1               u1State;
    UINT1               u1SetActivity = LA_LACPACTIVITY_BITMASK;
    tLaLacPortState    *pu1PortState;
    tLaBoolean          bAllow = LA_FALSE;
    tLaLacAggEntry     *pAggEntry;
    tLaLacPortEntry    *pPortEntry = NULL;
    tSNMP_OCTET_STRING_TYPE PortAdminState;
    UINT1               u1AggBridgedState = CFA_ENABLED;
    UINT1               u1PortBridgedState = CFA_DISABLED;

    /* Key checking */
    if (nmhTestv2Dot3adAggPortActorAdminKey (&u4ErrCode, u4IfaceId,
                                             u2Key) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (LaGetAggEntryByKey (u2Key, &pAggEntry) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        return (CLI_FAILURE);
    }

    /* Checking whether any ACL is associated to the interface */
    if (AclEnabledForPort (u4IfaceId) == CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Configuration not permitted. Remove filters associated to the port before adding to port-channel.\n\n");
        return (CLI_FAILURE);
    }

    LaGetPortEntry ((UINT2) u4IfaceId, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return CLI_FAILURE;
    }

    /* protocol mismatch check */
    if (pAggEntry != NULL)
    {
        CfaGetIfBridgedIfaceStatus ((UINT4) pAggEntry->u2AggIndex,
                                    &u1AggBridgedState);
        /*ICCL should not accept LACP mode links */
        LaIcchGetIcclIfIndex (&u4IcclIfIndex);
        if ((u4LacpMode == LA_MODE_LACP) &&
            (pAggEntry->u2AggIndex == u4IcclIfIndex))
        {
            CLI_SET_ERR (CLI_LA_PORT_LACP_MODE);
            return (CLI_FAILURE);
        }
        /* MC-LAG should not accept non-LACP mode links */
        if ((pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED) &&
            (u4LacpMode != LA_MODE_LACP))
        {
            CLI_SET_ERR (CLI_LA_PORT_NON_LACP_MODE);
            return (CLI_FAILURE);
        }
    }

    CfaGetIfBridgedIfaceStatus (u4IfaceId, &u1PortBridgedState);
    if (u1AggBridgedState != u1PortBridgedState)
    {
        CLI_SET_ERR (CLI_LA_INCOMPATIBLE_MODE);
        return (CLI_FAILURE);
    }
    if (pPortEntry->bDynAggSelect == LA_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r%% This interface is configured as default port for a "
                   "port channel.  It cannot be added as a static member to "
                   "the channel group\r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsLaPortMode (&u4ErrCode, (INT4) u4IfaceId, (INT4) u4LacpMode)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    /* The user input LACP mode must be validated for compatibility
     * with respect to the other ports that are members of the port-channel 
     * */

    /* If this is a new entry allow setting of the ports */

    if (pAggEntry == NULL)
    {
        bAllow = LA_TRUE;
    }
    /* Allow set of the variables */

    /* If no other ports are members of the port channel , then allow
     * setting of the user input LACP mode */

    else if (pAggEntry->u1ConfigPortCount == 0)
    {
        bAllow = LA_TRUE;
    }

    /* if the number of ports previously configured in the port channel
     * is 1, and is also same as the port currently configured by the 
     * user than allow setting of any mode as per user input */

    else if ((pAggEntry->u1ConfigPortCount == 1) &&
             (LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry) ==
              LA_SUCCESS) && (pPortEntry != NULL)
             && (pPortEntry->u2PortIndex == (UINT2) u4IfaceId))
    {
        bAllow = LA_TRUE;
    }
    /* Allow set of the variables */

    /* LACP disabled can always be set on a port , irrespective of the 
     * other members in the port-channel */

    else if ((u4LacpMode != (UINT4) LA_MODE_DISABLED) &&
             (pAggEntry->LaLacpMode != u4LacpMode))
    {
        bAllow = LA_FALSE;
        CliPrintf (CliHandle,
                   "\r%% Channel protocol mismatch for this interface "
                   "in group %d : this interface can not be added to "
                   "the channel group \r\n", u2Key);
        return (CLI_FAILURE);
        /* dont allow setting of variables */
    }
    else if (u4LacpMode == (UINT4) LA_MODE_MANUAL)
    {
        bAllow = LaCalculateAllow (CliHandle, u4IfaceId, u2Key);
    }
    else
    {

        bAllow = LA_TRUE;
        /* Allow setting of varaibles */
    }

    if (bAllow == LA_TRUE)
    {
        if (nmhSetDot3adAggPortActorAdminKey (u4IfaceId, u2Key) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        LaUtilGetPortState ((UINT2) u4IfaceId, &pu1PortState, LA_CLI_ACTOR);
        if (pu1PortState == NULL)
        {
            return CLI_FAILURE;
        }

        LaGetPortState (pu1PortState, &u1State);
        u1Value = (UINT1) (u1State & LA_LACPACTIVITY_BITMASK);

        PortAdminState.i4_Length = 1;
        PortAdminState.pu1_OctetList = &u1SetActivity;

        if (u1IsActiveLacp == LA_TRUE)
        {
            nmhSetDot3adAggPortActorAdminState (u4IfaceId, &PortAdminState);
        }
        else if (u1IsActiveLacp == LA_FALSE)
        {
            if (u1Value)
            {
                nmhSetFsLaPortActorResetAdminState (u4IfaceId, &PortAdminState);
            }
            else
            {
                /* no need to change */
            }
        }
        if (nmhSetFsLaPortMode (u4IfaceId, u4LacpMode) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : LaAttachDefaultPortToAggregator
 *
 *     DESCRIPTION      : This function attaches a default port to a given
 *                        aggregator
 *
 *     INPUT            : CliHandle - CLI Handler
 *                        u4IfaceId - Default IfIndex to be set (0 to reset)
 *                        u4AggIndex - AggIndex for which default port needs
 *                        to be set
 *
 *     OUTPUT           : NONE
 *
 *     RETURNS          : Success / Failure
 *
 *****************************************************************************/

INT4
LaAttachDefaultPortToAggregator (tCliHandle CliHandle, UINT4 u4IfaceId,
                                 UINT4 u4AggIndex)
{
    INT4                i4AggIfIndex = 0;
    UINT4               u4Error = 0;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if (nmhTestv2FsLaPortChannelDefaultPortIndex (&u4Error, i4AggIfIndex,
                                                  (INT4) u4IfaceId)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaPortChannelDefaultPortIndex (i4AggIfIndex, (INT4) u4IfaceId)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaActorPortModeDisabled                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Aggregation mode in         */
/*                        the port as Disabled.                              */
/*                                                                           */
/*     INPUT            : u4IfaceId, u4LacpMode                              */
/*                        CliHandle CLI Handler                              */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetActorPortModeDisabled (tCliHandle CliHandle, UINT4 u4IfaceId)
{
    UINT4               u4ErrCode;
    tLaLacPortEntry    *pPortEntry = NULL;

    if (nmhTestv2FsLaPortMode (&u4ErrCode, (INT4) u4IfaceId, LA_MODE_DISABLED)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    LaGetPortEntry ((UINT2) u4IfaceId, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (pPortEntry->bDynAggSelect == LA_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r%% This command cannot be used on ports configured "
                   "as default member of an aggregator\r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaPortMode (u4IfaceId, LA_MODE_DISABLED) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetActorPortWaitTime                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the aggregation wait time for   */
/*                         ports                                             */
/*                                                                           */
/*     INPUT            : u4IfaceId, u4WaitTime - Wait Time value            */
/*                        CliHandle CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Sucess /Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetActorPortWaitTime (tCliHandle CliHandle, UINT4 u4IfaceId, UINT4 u4WaitTime)
{

    UINT4               u4Errcode;

    /* Set the wait time in centiseconds */
    u4WaitTime = u4WaitTime * 100;

    if (nmhTestv2FsLaPortAggregateWaitTime (&u4Errcode, u4IfaceId,
                                            u4WaitTime) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaPortAggregateWaitTime (u4IfaceId, u4WaitTime) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SetLaActorPortChannelMac                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Mac Address for the         */
/*                         PortChannel                                       */
/*                                                                           */
/*     INPUT            : u4MacSelPolicy - DYNAMIC / FORCE AGG_MAC           */
/*                        PortChannelMac - Force Aggregation MAC address     */
/*                        CliHandle CLI Handler                              */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/ Failure                                   */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetActorPortChannelMac (tCliHandle CliHandle, UINT4 u4IfIndex,
                          UINT4 u4MacSelPolicy, tMacAddr PortChannelMac)
{
    UINT4               u4Errcode;
    UNUSED_PARAM (CliHandle);
    /* First set the Port Channel MAC Address. */

    if (u4MacSelPolicy != LA_AGGMAC_DYNAMIC)
    {
        nmhTestv2FsLaPortChannelAdminMacAddress (&u4Errcode,
                                                 (INT4) u4IfIndex,
                                                 PortChannelMac);
        nmhSetFsLaPortChannelAdminMacAddress (u4IfIndex, PortChannelMac);
    }

    /* Set the MAC selection policy */
    /* The MAC address must be set before testing the selection policy
     * since the MAC address for the Aggregation is validated below */

    nmhTestv2FsLaPortChannelMacSelection (&u4Errcode, (INT4) u4IfIndex,
                                          (INT4) u4MacSelPolicy);
    nmhSetFsLaPortChannelMacSelection (u4IfIndex, u4MacSelPolicy);

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetPortActorAdminKeyLacpMode                     */
/*                                                                           */
/*     DESCRIPTION      : This function configures the ActorAdmin key and/or */
/*                        LACP mode for the given port                       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4IfaceId - Interface Index                        */
/*                        u4AdminKey - Actor AdminKey                        */
/*                        u4LacpMode - LACP Mode                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetPortActorAdminKeyLacpMode (tCliHandle CliHandle, UINT4 u4IfaceId,
                                UINT4 u4AdminKey, UINT4 u4ConfigMode)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortState    *pu1PortState;
    tSNMP_OCTET_STRING_TYPE PortAdminState;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4ErrCode = 0;
    UINT4               u4LacpMode = 0;
    UINT1               u1IsActiveLacp = LA_FALSE;
    UINT1               u1Value = 0;
    UINT1               u1State = 0;
    UINT1               u1SetActivity = LA_LACPACTIVITY_BITMASK;

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));

    if (nmhValidateIndexInstanceDot3adAggPortTable ((INT4) u4IfaceId) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port Number\r\n");
        return CLI_FAILURE;
    }

    LaGetPortEntry ((UINT2) u4IfaceId, &pPortEntry);
    if (pPortEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (LaGetAggEntryByKey ((UINT2) u4AdminKey, &pAggEntry) == LA_FAILURE)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        return CLI_FAILURE;
    }
    if (pAggEntry == NULL)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        return CLI_FAILURE;
    }

    switch (u4ConfigMode)
    {
        case LA_CLI_ACTIVE:
            u4LacpMode = (UINT4) LA_MODE_LACP;
            u1IsActiveLacp = LA_TRUE;
            break;
        case LA_CLI_PASSIVE:
            u4LacpMode = (UINT4) LA_MODE_LACP;
            u1IsActiveLacp = LA_FALSE;
            break;
        default:
            break;
    }

    if (pPortEntry->bDynAggSelect != LA_TRUE)
    {
        CliPrintf (CliHandle, "\r%% Port admin-key can be configured "
                   "only for ports that select aggregator dynamically\r\n");
        return CLI_FAILURE;
    }
    /* Key checking */
    if (u4AdminKey != 0)
    {
        if (pAggEntry->bDynPortRealloc != pPortEntry->bDynAggSelect)
        {
            CLI_SET_ERR (CLI_LA_ADMIN_KEY_DEF_PORT_ERR);
            return CLI_FAILURE;
        }

        if (pPortEntry->pAggEntry == pAggEntry)
        {
            /* Same value */
            return CLI_SUCCESS;
        }

        if (LaCfaGetIfInfo (pPortEntry->u2PortIndex, &CfaIfInfo) != CFA_SUCCESS)
        {
            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Retrieval of port %d properties failed!\n",
                         pPortEntry->u2PortIndex);
            CLI_SET_ERR (CLI_LA_INVALID_PORT);
            return CLI_FAILURE;
        }

        /* Interface MTU should match with the Port channel MTU */

        if (CfaIfInfo.u4IfMtu != pAggEntry->u4Mtu)
        {
            CLI_SET_ERR (CLI_LA_MTU_MISMATCH);
            return CLI_FAILURE;
        }

        /* Set LACP mode to LA_MODE_DISABLED before changing PortAdminKey */
        if (nmhSetFsLaPortMode (u4IfaceId, LA_MODE_DISABLED) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Disabling LACP Mode failed\r\n");
            return CLI_FAILURE;
        }
        if (nmhTestv2Dot3adAggPortActorAdminKey (&u4ErrCode, u4IfaceId,
                                                 (UINT2) u4AdminKey) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid Actor Admin Key. Disabled "
                       "LACP on port.\r\n");
            return CLI_FAILURE;
        }
    }

    if (u4LacpMode != 0)
    {
        if (nmhTestv2FsLaPortMode (&u4ErrCode, u4IfaceId, u4LacpMode) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid LACP Mode\r\n");
            return CLI_FAILURE;
        }
    }

    if (u4AdminKey != 0)
    {
        if (nmhSetDot3adAggPortActorAdminKey (u4IfaceId, (UINT2) u4AdminKey) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Error while setting Port Actor Admin "
                       "Key. Disabled LACP on port.\r\n");
            return CLI_FAILURE;
        }
    }

    if (u4LacpMode != 0)
    {
        /*  If the mode is configured then invoke the function 
         *  LaChangeActorPortState to set the appropriate state, before
         *  setting the LACP Mode */
        LaUtilGetPortState ((UINT2) u4IfaceId, &pu1PortState, LA_CLI_ACTOR);
        if (pu1PortState == NULL)
        {
            return CLI_FAILURE;
        }
        LaGetPortState (pu1PortState, &u1State);
        u1Value = u1State & LA_LACPACTIVITY_BITMASK;
        PortAdminState.i4_Length = 1;
        PortAdminState.pu1_OctetList = &u1SetActivity;

        if (u1IsActiveLacp == LA_TRUE)
        {
            if (u1Value == 0)
            {
                nmhSetDot3adAggPortActorAdminState (u4IfaceId, &PortAdminState);
            }
        }
        else if (u1IsActiveLacp == LA_FALSE)
        {
            if (u1Value != 0)
            {
                nmhSetFsLaPortActorResetAdminState (u4IfaceId, &PortAdminState);
            }
        }
        /* Set LACP mode */
        if (nmhSetFsLaPortMode (u4IfaceId, u4LacpMode) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%% LACP Mode Setting failed\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

#ifdef DLAG_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGSystemPriority                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG system priority        */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaDLAGSystemPriority - DLAG System Priority value*/
/*                        CliHandle              - CLI Handler               */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG System priority needs to be   */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGSystemPriority (tCliHandle CliHandle, UINT4 u4LaDLAGSystemPriority,
                             UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGSystemPriority (&u4ErrCode, i4AggIfIndex,
                                                     (INT4)
                                                     u4LaDLAGSystemPriority)) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaPortChannelDLAGSystemPriority (i4AggIfIndex,
                                                 (INT4) u4LaDLAGSystemPriority)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG system priority set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGSystemID                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG system ID              */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG System ID needs to be         */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGSystemID (tCliHandle CliHandle, tMacAddr SystemID,
                       UINT4 u4AggIndex)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGSystemID (&u4ErrCode, i4AggIfIndex,
                                               SystemID)) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetFsLaPortChannelDLAGSystemID (i4AggIfIndex, SystemID))
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG system mac address set routine failed\r\n");
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGPeriodicSyncTime                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG periodic sync time     */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4PeriodicSyncTime- DLAG Periodic Sync time value  */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG periodic-sync time needs to be*/
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGPeriodicSyncTime (tCliHandle CliHandle, UINT4 u4PeriodicSyncTime,
                               UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGPeriodicSyncTime (&u4ErrCode, i4AggIfIndex,
                                                       (INT4)
                                                       u4PeriodicSyncTime)) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaPortChannelDLAGPeriodicSyncTime (i4AggIfIndex,
                                                    (INT4) u4PeriodicSyncTime))
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG periodic sync time set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetGlobalDLAGPeriodicSyncTime                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global DLAG periodic sync   */
/*                        time                                               */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4PeriodicSyncTime- DLAG Periodic Sync time value  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGGlobalPeriodicSyncTime (tCliHandle CliHandle,
                                     UINT4 u4PeriodicSyncTime)
{
    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaDLAGPeriodicSyncTime (&u4ErrCode,
                                            u4PeriodicSyncTime)) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG Global periodic sync time Test routine failed\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaDLAGPeriodicSyncTime (u4PeriodicSyncTime)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG Global periodic sync time set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGGlobalSystemID                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global DLAG system ID       */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGGlobalSystemID (tCliHandle CliHandle, tMacAddr SystemID)
{

    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaDLAGSystemID (&u4ErrCode, SystemID)) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetFsLaDLAGSystemID (SystemID)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG Global system mac address set routine failed\r\n");
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGGlobalSystemPriority                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG Global system priority */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaDLAGSystemPriority - DLAG System Priority value*/
/*                        CliHandle              - CLI Handler               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGGlobalSystemPriority (tCliHandle CliHandle,
                                   UINT4 u4LaDLAGSystemPriority)
{
    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaDLAGSystemPriority (&u4ErrCode, (INT4)
                                          u4LaDLAGSystemPriority)) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaDLAGSystemPriority ((INT4) u4LaDLAGSystemPriority)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG Global system priority set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGGlobalConfigureDistributePort                */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Global D-LAG          */
/*                        distributing port                                  */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGDistributingPortIndex - DLAG Distirbuting    */
/*                                                      port index           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureGlobalDistributePort (tCliHandle CliHandle,
                                     UINT4 u4DLAGDistributingPortIndex)
{
    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaDLAGDistributingPortIndex (&u4ErrCode, (INT4)
                                                 u4DLAGDistributingPortIndex))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaDLAGDistributingPortIndex
         ((INT4) u4DLAGDistributingPortIndex)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r %%D-LAG Global Configure Distribute Port routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGMSSelectionWaitTime                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the DLAG Master Selection Wait  */
/*                        time                                               */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4MSSelectionWaitTime- DLAG MS Selection Wait Time */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG master-slave-selection wait   */
/*                                       time needs to be configured.        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGMSSelectionWaitTime (tCliHandle CliHandle,
                                  UINT4 u4MSSelectionWaitTime, UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGMSSelectionWaitTime
         (&u4ErrCode, i4AggIfIndex, u4MSSelectionWaitTime)) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaPortChannelDLAGMSSelectionWaitTime
         (i4AggIfIndex, (INT4) u4MSSelectionWaitTime)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%D-LAG master selection wait time set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGConfigureDistributePort                      */
/*                                                                           */
/*     DESCRIPTION      : This function configures the D-LAG distributing    */
/*                        port                                               */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGDistributingPortIndex - DLAG Distirbuting    */
/*                                                      port index           */
/*                        CliHandle  - CLI Handler                           */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG distributing port needs to be */
/*                                       configured.                         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureDistributePort (tCliHandle CliHandle,
                               UINT4 u4DLAGDistributingPortIndex,
                               UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4AggIfIndex = 0;

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGDistributingPortIndex (&u4ErrCode,
                                                            i4AggIfIndex,
                                                            u4DLAGDistributingPortIndex))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaPortChannelDLAGDistributingPortIndex (i4AggIfIndex,
                                                         u4DLAGDistributingPortIndex))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGConfigureDistributePortList                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures the D-LAG distributing    */
/*                        port List                                          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGDistributingPortIndex - DLAG Distirbuting    */
/*                                                      port index           */
/*                        CliHandle  - CLI Handler                           */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       D-LAG distributing port needs to be */
/*                                       configured.                         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureDistributePortList (tCliHandle CliHandle,
                                   UINT1 *pu1Ports, UINT4 u4AggIndex)
{
    tSNMP_OCTET_STRING_TYPE PortListVal;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4AggIfIndex = 0;
    UINT1               au1PortList[LA_PORT_LIST_SIZE];

    MEMSET (&au1PortList, 0, LA_PORT_LIST_SIZE);
    MEMSET (&PortListVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    PortListVal.pu1_OctetList = &au1PortList[0];
    PortListVal.i4_Length = LA_PORT_LIST_SIZE;

    MEMCPY (PortListVal.pu1_OctetList, pu1Ports, LA_PORT_LIST_SIZE);

    if (nmhTestv2FsLaPortChannelDLAGDistributingPortList (&u4ErrCode,
                                                          i4AggIfIndex,
                                                          &PortListVal) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhSetFsLaPortChannelDLAGDistributingPortList (i4AggIfIndex,
                                                        &PortListVal))
        != SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGConfigureGlobalDistributePortList            */
/*                                                                           */
/*     DESCRIPTION      : This function configures the D-LAG distributing    */
/*                        port List                                          */
/*                                                                           */
/*                                                                           */
/*     INPUT            : pu1Ports - List of ports to be configured.         */
/*                                                                           */
/*                        CliHandle  - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGConfigureGlobalDistributePortList (tCliHandle CliHandle, UINT1 *pu1Ports)
{
    tSNMP_OCTET_STRING_TYPE PortListVal;
    UINT4               u4ErrCode = 0;
    UINT1               au1PortList[LA_PORT_LIST_SIZE];

    MEMSET (&au1PortList, 0, LA_PORT_LIST_SIZE);
    MEMSET (&PortListVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    PortListVal.pu1_OctetList = &au1PortList[0];
    PortListVal.i4_Length = LA_PORT_LIST_SIZE;

    MEMCPY (PortListVal.pu1_OctetList, pu1Ports, LA_PORT_LIST_SIZE);

    if (nmhTestv2FsLaDLAGDistributingPortList (&u4ErrCode,
                                               &PortListVal) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if ((nmhSetFsLaDLAGDistributingPortList (&PortListVal)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Distributing Port List Set failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGStatus                                */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables D-LAG functionality */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGStatus - DLAG status (enable/disable)        */
/*                        CliHandle    - CLI Handler                         */
/*                        u4AggIndex   - AggIndex of port-channel in which   */
/*                                       D-LAG functionality needs to be     */
/*                                       enabled/disabled.                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGStatus (tCliHandle CliHandle, UINT4 u4DLAGStatus, UINT4 u4AggIndex)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGStatus (&u4ErrCode, i4AggIfIndex,
                                             (INT4) u4DLAGStatus))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaPortChannelDLAGStatus (i4AggIfIndex, u4DLAGStatus))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGRedundancy                            */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables D-LAG redundancy    */
/*                        feature                                            */
/*                                                                           */
/*     INPUT            : u4RedundancyFeat - D-LAG redundancy feature(ON/OFF)*/
/*                        CliHandle        - CLI Handler                     */
/*                        u4AggIndex       - AggIndex of port-channel in     */
/*                                           whic D-LAG redundancy feature   */
/*                                           needs to be switched ON/OFF.    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGRedundancy (tCliHandle CliHandle, UINT4 u4RedundancyFeat,
                         UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4AggIfIndex = 0;

    LaGetAggEntry (u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelDLAGRedundancy (&u4ErrCode, i4AggIfIndex,
                                                 (INT4) u4RedundancyFeat))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaPortChannelDLAGRedundancy (i4AggIfIndex, u4RedundancyFeat))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGSetDLAGSystemStatus                          */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables D-LAG functionality */
/*                         globally in system                                */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4DLAGStatus - DLAG status (enable/disable)        */
/*                        CliHandle    - CLI Handler                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaDLAGSetDLAGSystemStatus (tCliHandle CliHandle, UINT4 u4DLAGStatus)
{

    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaDLAGSystemStatus (&u4ErrCode, (INT4) u4DLAGStatus))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaDLAGSystemStatus ((INT4) u4DLAGStatus)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% DLAG System Status set failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGShowCounters                                 */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Displays per portchannel DLAG counter information  */
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaDLAGShowCounters (tCliHandle CliHandle, INT4 i4Key)
{
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tSNMP_OCTET_STRING_TYPE DLAGDistributePorts;
    tLaPortList        *pDistributePorts = NULL;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    UINT4               u4TxCount = 0;
    UINT4               u4RxCount = 0;
    UINT4               u4ElectedAsMasterCount = 0;
    UINT4               u4ElectedAsSlaveCount = 0;
    UINT4               u4PagingStatus = 0;
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1               u1flag = 1;

    pDistributePorts =
        (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

    if (pDistributePorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pDistributePorts, 0, sizeof (tLaPortList));
    MEMSET (&DLAGDistributePorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    DLAGDistributePorts.pu1_OctetList = *pDistributePorts;
    DLAGDistributePorts.i4_Length = sizeof (tLaPortList);

    if (i4Key == 0)
    {
        /* No Port Channel index is given by user. So,we get the
         * first interface from the table. */
        u1flag = 0;
        /* This flag is used to indicate that all details of all
         * the existing port-channels must be displayed */

        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
            return (CLI_SUCCESS);
        }
    }
    else
    {
        /* Get the Aggregator entry */
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
            return (CLI_SUCCESS);
        }
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }
    do
    {
        nmhGetDot3adAggActorAdminKey (i4Index, &i4Key);
        CliPrintf (CliHandle, "\n\rD-LAG Statistics : Channel Group %d\r\n",
                   i4Key);

        CliPrintf (CliHandle, "\r%s\r\n",
                   "--------------------------------------------\r\n");

        nmhGetFsLaPortChannelDLAGElectedAsMasterCount (i4Index,
                                                       &u4ElectedAsMasterCount);
        CliPrintf (CliHandle,
                   "\rElected As Master Count          :  %d\r\n",
                   u4ElectedAsMasterCount);
        nmhGetFsLaPortChannelDLAGElectedAsSlaveCount (i4Index,
                                                      &u4ElectedAsSlaveCount);
        CliPrintf (CliHandle,
                   "\rElected As Slave Count           :  %d\r\n\n",
                   u4ElectedAsSlaveCount);

        nmhGetFsLaPortChannelDLAGDistributingPortList (i4Index,
                                                       &DLAGDistributePorts);

        CliPrintf (CliHandle, "\rD-LAG Distributing Ports         ");

        if (CliOctetToIfName (CliHandle, ":", &DLAGDistributePorts,
                              LA_MAX_PORTS, LA_PORT_LIST_SIZE, 0,
                              &u4PagingStatus,
                              LA_DLAG_CLI_MAX_PORTS_PER_LINE) == CLI_FAILURE)
        {
            FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
            return CLI_FAILURE;
        }

        nmhGetFsLaPortChannelDLAGPeriodicSyncPduTxCount (i4Index, &u4TxCount);

        nmhGetFsLaPortChannelDLAGPeriodicSyncPduRxCount (i4Index, &u4RxCount);
        CliPrintf (CliHandle,
                   "\rPeriodic Sync PDU Tx Count       :  %d\r\n", u4TxCount);
        CliPrintf (CliHandle,
                   "\rPeriodic Sync PDU Rx Count       :  %d\r\n", u4RxCount);

        nmhGetFsLaPortChannelDLAGEventUpdatePduTxCount (i4Index, &u4TxCount);

        nmhGetFsLaPortChannelDLAGEventUpdatePduRxCount (i4Index, &u4RxCount);
        CliPrintf (CliHandle,
                   "\rEvent Update PDU Tx Count        :  %d\r\n", u4TxCount);
        CliPrintf (CliHandle,
                   "\rEvent Update PDU Rx Count        :  %d\r\n", u4RxCount);

        nmhGetFsLaPortChannelTrapTxCount (i4Index, &u4TxCount);

        CliPrintf (CliHandle,
                   "\rPort-channel Traps Tx Count      :  %d\r\n", u4TxCount);

        CliPrintf (CliHandle, "\n");
        u1Quit = (UINT1) CliPrintf (CliHandle, "\r");
        if (u1Quit == CLI_FAILURE)
        {
            break;
        }
        i4PrevIndex = i4Index;

    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS) && (u1flag == 0));

    FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaDLAGShowDetail                                   */
/*                                                                           */
/*     DESCRIPTION      : Displays DLAG counter information per portchannel  */
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaDLAGShowDetail (tCliHandle CliHandle, INT4 i4Key, UINT1 u1IsConsolidated)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tMacAddr            BaseMac;
    tMacAddr            PrevBaseMac;
    tMacAddr            RemoteBaseMac;
    tSNMP_OCTET_STRING_TYPE DLAGDistributePorts;
    tLaPortList        *pDistributePorts = NULL;
    UINT4               u4DLAGPeriodicSyncTime = 0;
    UINT4               u4DLAGMSSelectionWaitTime = 0;
    UINT4               u4PagingStatus = 0;
    UINT4               u4DLAGRemoteChannelSpeed = 0;
    UINT4               u4DLAGRemoteChannelHiSpeed = 0;
    UINT4               u4CfaRemotePortIndex = 0;
    UINT4               u4Count = 0;
    INT4                i4DLAGRemoteChannelMtu = 0;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4DLAGSystemPriority = 0;
    INT4                i4DLAGRolePlayed = 0;
    INT4                i4DLAGStatus = 0;
    INT4                i4DLAGRedundancy = 0;
    INT4                i4DLAGMaxKeepAliveCount = 0;
    INT4                i4PortChannelIndex = 0;
    INT4                i4RemotePortChannelIndex = 0;
    INT4                i4RemotePortIndex = 0;
    INT4                i4PrevRemotePortIndex = 0;
    INT4                i4DLAGRemotePriority = 0;
    INT4                i4DLAGRemoteRolePlayed = 0;
    INT4                i4DLAGRemoteKeepAliveCount = 0;
    INT4                i4RemotePortBundleState = 0;
    INT4                i4RemotePortSyncStatus = 0;
    INT4                i4RemotePortPriority = 0;
    INT4                i4RetValue = 0;
    INT1                i1RetValue = 0;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY] = { 0 };
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1              *pifName = NULL;
    UINT1              *pu1Tmp = NULL;
    UINT1               u1flag = 1;

    pDistributePorts =
        (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

    if (pDistributePorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (*pDistributePorts, 0, sizeof (tLaPortList));
    MEMSET (&DLAGDistributePorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    DLAGDistributePorts.pu1_OctetList = *pDistributePorts;
    DLAGDistributePorts.i4_Length = sizeof (tLaPortList);

    nmhGetFsLaDLAGSystemStatus (&i4DLAGStatus);
    i1RetValue = nmhGetFsLaDLAGSystemID ((tMacAddr *) & BaseMac);
    nmhGetFsLaDLAGSystemPriority (&i4DLAGSystemPriority);
    nmhGetFsLaDLAGPeriodicSyncTime (&u4DLAGPeriodicSyncTime);
    nmhGetFsLaDLAGRolePlayed (&i4DLAGRolePlayed);
    nmhGetFsLaDLAGDistributingPortList (&DLAGDistributePorts);

    CliPrintf (CliHandle, "\n\rD-LAG Configuration for the System:\r\n");
    CliPrintf (CliHandle, "\r%s\r\n",
               "---------------------------------------------------");
    /* Obtain all the D-LAG parameters */
    if (i4DLAGStatus == LA_DLAG_ENABLED)
    {
        CliPrintf (CliHandle,
                   "\rDLAG status                            : Enabled\r\n");
    }
    else if (i4DLAGStatus == LA_DLAG_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\rDLAG status                            : Disabled\r\n");
    }

    switch (i4DLAGRolePlayed)
    {
        case LA_DLAG_SYSTEM_ROLE_MASTER:
            CliPrintf (CliHandle,
                       "\rRole Played                            : Master\n");
            break;
        case LA_DLAG_SYSTEM_ROLE_BACKUP_MASTER:
            CliPrintf (CliHandle,
                       "\rRole Played                            : Backup-Master\n");
            break;
        case LA_DLAG_SYSTEM_ROLE_SLAVE:
            CliPrintf (CliHandle,
                       "\rRole Played                            : Slave\n");
            break;
        case LA_DLAG_SYSTEM_ROLE_NONE:
            CliPrintf (CliHandle,
                       "\rRole Played                            : None\n");
            break;
        default:
            break;
    }

    CliPrintf (CliHandle, "\rD-LAG Distributing Ports               ");

    if (CliOctetToIfName (CliHandle, ":", &DLAGDistributePorts,
                          LA_MAX_PORTS, LA_PORT_LIST_SIZE, 0,
                          &u4PagingStatus,
                          LA_DLAG_CLI_MAX_PORTS_PER_LINE) == CLI_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
        return CLI_FAILURE;
    }

    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
    pu1Tmp = &au1Temp[0];
    CliPrintf (CliHandle, "\rD-LAG system MAC                       : ");
    PrintMacAddress (BaseMac, pu1Tmp);
    CliPrintf (CliHandle, "%s\r\n", pu1Tmp);

    CliPrintf (CliHandle,
               "\rD-LAG system priority                  : %d\r\n",
               i4DLAGSystemPriority);
    CliPrintf (CliHandle,
               "\rD-LAG periodic sync time               : %d seconds\r\n",
               u4DLAGPeriodicSyncTime);

    CliPrintf (CliHandle,
               "\rMaximum keep alive count               : %d\r\n",
               LA_DLAG_MAX_KEEP_ALIVE_COUNT);

    CliPrintf (CliHandle, "\n");

    if (i4Key == 0)
    {
        /* No Port Channel index is given by user. So,we get the
         * first interface from the table. */
        u1flag = 0;
        /* This flag is used to indicate that all details of all
         * the existing port-channels must be displayed */

        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
            return (CLI_SUCCESS);
        }
    }
    else
    {
        /* Get the Aggregator entry */
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
            return (CLI_SUCCESS);
        }
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }

    if ((u1IsConsolidated == LA_TRUE) &&
        (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_SLAVE)
        && (i4DLAGStatus == LA_DLAG_ENABLED))
    {
        CliPrintf (CliHandle, "\r\n%s",
                   "Consolidated Info present only in the Master Node\r\n");
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetDot3adAggActorAdminKey (i4Index, &i4Key);
        MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
        pifName = &au1Temp[0];

        MEMSET (BaseMac, 0, LA_MAC_ADDRESS_SIZE);
        MEMSET (RemoteBaseMac, 0, LA_MAC_ADDRESS_SIZE);
        MEMSET (PrevBaseMac, 0, LA_MAC_ADDRESS_SIZE);

        /* Obtain all the D-LAG parameters */
        nmhGetFsLaPortChannelDLAGSystemID (i4Index, (tMacAddr *) & BaseMac);
        nmhGetFsLaPortChannelDLAGSystemPriority (i4Index,
                                                 &i4DLAGSystemPriority);
        nmhGetFsLaPortChannelDLAGPeriodicSyncTime (i4Index,
                                                   &u4DLAGPeriodicSyncTime);
        nmhGetFsLaPortChannelDLAGMSSelectionWaitTime (i4Index,
                                                      &u4DLAGMSSelectionWaitTime);
        nmhGetFsLaPortChannelDLAGRolePlayed (i4Index, &i4DLAGRolePlayed);
        nmhGetFsLaPortChannelDLAGStatus (i4Index, &i4DLAGStatus);
        nmhGetFsLaPortChannelDLAGRedundancy (i4Index, &i4DLAGRedundancy);
        nmhGetFsLaPortChannelDLAGMaxKeepAliveCount (i4Index,
                                                    &i4DLAGMaxKeepAliveCount);
        nmhGetFsLaPortChannelDLAGDistributingPortList (i4Index,
                                                       &DLAGDistributePorts);

        CliPrintf (CliHandle,
                   "\n\rD-LAG Configuration : Channel Group %d\r\n", i4Key);

        CliPrintf (CliHandle, "\r%s\r\n",
                   "-------------------------------------------------------");

        if (i4DLAGStatus == LA_DLAG_ENABLED)
        {
            CliPrintf (CliHandle,
                       "\rDLAG status                            : Enabled\r\n");
        }
        else if (i4DLAGStatus == LA_DLAG_DISABLED)
        {
            CliPrintf (CliHandle,
                       "\rDLAG status                            : Disabled\r\n");
        }

        if (i4DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            CliPrintf (CliHandle,
                       "\rD-LAG Redundancy                       : On \r\n");
        }
        if (i4DLAGRedundancy == LA_DLAG_REDUNDANCY_OFF)
        {
            CliPrintf (CliHandle,
                       "\rD-LAG Redundancy                       : Off \r\n");
        }

        if (i4DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
        {
            switch (i4DLAGRolePlayed)
            {
                case LA_DLAG_SYSTEM_ROLE_MASTER:
                    CliPrintf (CliHandle,
                               "\rRole Played                            : Master\n");
                    break;
                case LA_DLAG_SYSTEM_ROLE_BACKUP_MASTER:
                    CliPrintf (CliHandle,
                               "\rRole Played                            : Backup-Master\n");
                    break;
                case LA_DLAG_SYSTEM_ROLE_SLAVE:
                    CliPrintf (CliHandle,
                               "\rRole Played                            : Slave\n");
                    break;
                case LA_DLAG_SYSTEM_ROLE_NONE:
                    CliPrintf (CliHandle,
                               "\rRole Played                            : None\n");
                    break;
                default:
                    break;
            }
            CliPrintf (CliHandle, "\rD-LAG Distributing Ports               ");

            if (CliOctetToIfName (CliHandle, ":", &DLAGDistributePorts,
                                  LA_MAX_PORTS, LA_PORT_LIST_SIZE, 0,
                                  &u4PagingStatus,
                                  LA_DLAG_CLI_MAX_PORTS_PER_LINE) ==
                CLI_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
                return CLI_FAILURE;
            }

            MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
            pu1Tmp = &au1Temp[0];
            CliPrintf (CliHandle,
                       "\rD-LAG system MAC                       : ");
            PrintMacAddress (BaseMac, pu1Tmp);
            CliPrintf (CliHandle, "%s\r\n", pu1Tmp);

            CliPrintf (CliHandle,
                       "\rD-LAG system priority                  : %d\r\n",
                       i4DLAGSystemPriority);
            CliPrintf (CliHandle,
                       "\rD-LAG periodic sync time               : %d milli-seconds\r\n",
                       u4DLAGPeriodicSyncTime);
            if (LA_DLAG_SYSTEM_STATUS != LA_DLAG_ENABLED)
            {
                CliPrintf (CliHandle,
                           "\rD-LAG master slave selection wait time : %d milli-seconds\r\n",
                           u4DLAGMSSelectionWaitTime);
            }

            CliPrintf (CliHandle,
                       "\rMaximum keep alive count               : %d\r\n",
                       i4DLAGMaxKeepAliveCount);
            CliPrintf (CliHandle, "\n");
        }

        i4RetValue = LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry);

        if (pLaLacAggEntry != NULL)
        {
            CliPrintf (CliHandle,
                       "\rD-LAG Maximum number of Ports allowed  : %d\r\n",
                       pLaLacAggEntry->u1MaxPortsToAttach);
        }

        if (u1IsConsolidated == LA_TRUE)
        {
            pConsPortEntry = NULL;

            if (pLaLacAggEntry == NULL)
            {
                CliPrintf (CliHandle, "\r\n");
                i4PrevIndex = i4Index;
                continue;
            }

            CliPrintf (CliHandle, "\r\n%s", "Consolidated Ports Info:-     ");
            CliPrintf (CliHandle, "\r\n%s\r\n", "----------------------------");

            RBTreeCount (pLaLacAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
                         &u4Count);

            if (u4Count == 0)
            {
                CliPrintf (CliHandle, "\r\n");
                i4PrevIndex = i4Index;
                continue;
            }

            /* display LACP information for the port */
            CliPrintf (CliHandle, "\r\n%26s%8s\r\n", "LACP Port", "Port");
            CliPrintf (CliHandle, "%-9s%-8s%-13s%-9s\r\n",
                       "Port", "State", "Priority", "Property");
            CliPrintf (CliHandle,
                       "\r\n------------------------------------------\r\n");

            LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry,
                                          pLaLacAggEntry);

            while (pConsPortEntry != NULL)
            {
                LaActiveDLAGGetIfIndexFromBridgeIndex (pConsPortEntry->
                                                       u4PortIndex,
                                                       &u4CfaRemotePortIndex);
                LaCfaCliGetIfName (u4CfaRemotePortIndex, (INT1 *) pifName);
                CliPrintf (CliHandle, "\r\n%-9s", pifName);

                switch (pConsPortEntry->u1PortStateFlag)
                {
                    case LA_AA_DLAG_PORT_IN_ADD:
                        CliPrintf (CliHandle, "%-11s", "Bundle");
                        break;
                    case LA_AA_DLAG_PORT_IN_DELETE:
                        CliPrintf (CliHandle, "%-11s", "Down");
                        break;
                    case LA_AA_DLAG_PORT_IN_STANDBY:
                        CliPrintf (CliHandle, "%-11s", "Standby");
                        break;
                    case LA_AA_DLAG_PORT_NOT_IN_ANY_LIST:
                        CliPrintf (CliHandle, "%-11s", "NotInAny");
                        break;
                    case LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY:
                        CliPrintf (CliHandle, "%-11s", "Removed");
                        break;
                    default:
                        break;
                }
                CliPrintf (CliHandle, "%-13d", pConsPortEntry->u2Priority);
                if (LaActiveDLAGCheckIsLocalPort ((UINT2) u4CfaRemotePortIndex)
                    == LA_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-9s", "Remote");
                }
                else
                {
                    CliPrintf (CliHandle, "%-9s", "Local");
                }
                LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                              &pConsPortEntry, pLaLacAggEntry);
            }
            CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            /* Remote Port Channel entry */
            MEMSET (PrevBaseMac, 0, LA_MAC_ADDRESS_SIZE);
            CliPrintf (CliHandle, "\r\n%s",
                       "D-LAG Neighbor Nodes Info :-     ");
            CliPrintf (CliHandle, "\r\n%s\r\n", "----------------------------");
            while (nmhGetNextIndexFsLaDLAGRemotePortChannelTable
                   (i4Index, &i4PortChannelIndex,
                    PrevBaseMac, &BaseMac) == SNMP_SUCCESS)

            {
                if (i4Index != i4PortChannelIndex)
                    break;

                MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                pu1Tmp = &au1Temp[0];

                CliPrintf (CliHandle, "\r\nSystem MAC                : ");

                PrintMacAddress (BaseMac, pu1Tmp);
                CliPrintf (CliHandle, "%s \r\n", pu1Tmp);

                nmhGetFsLaDLAGRemotePortChannelSystemPriority
                    (i4PortChannelIndex, BaseMac, &i4DLAGRemotePriority);
                nmhGetFsLaDLAGRemotePortChannelRolePlayed (i4PortChannelIndex,
                                                           BaseMac,
                                                           &i4DLAGRemoteRolePlayed);
                nmhGetFsLaDLAGRemotePortChannelKeepAliveCount
                    (i4PortChannelIndex, BaseMac, &i4DLAGRemoteKeepAliveCount);

                nmhGetFsLaDLAGRemotePortChannelSpeed (i4PortChannelIndex,
                                                      BaseMac,
                                                      &u4DLAGRemoteChannelSpeed);

                nmhGetFsLaDLAGRemotePortChannelHighSpeed (i4PortChannelIndex,
                                                          BaseMac,
                                                          &u4DLAGRemoteChannelHiSpeed);

                nmhGetFsLaDLAGRemotePortChannelMtu (i4PortChannelIndex, BaseMac,
                                                    &i4DLAGRemoteChannelMtu);

                CliPrintf (CliHandle, "\rSystem priority           : %d\r\n",
                           i4DLAGRemotePriority);
                CliPrintf (CliHandle, "\rChannel Group             : %d\r\n",
                           i4Key);

                if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
                {
                    i4DLAGRemoteRolePlayed = LA_DLAG_SYSTEM_ROLE_SLAVE;
                }

                switch (i4DLAGRemoteRolePlayed)
                {
                    case LA_DLAG_SYSTEM_ROLE_MASTER:
                        CliPrintf (CliHandle,
                                   "\rRole Played               : Master\r\n");
                        break;

                    case LA_DLAG_SYSTEM_ROLE_BACKUP_MASTER:
                        CliPrintf (CliHandle,
                                   "\rRole Played               : Backup-Master\r\n");
                        break;

                    case LA_DLAG_SYSTEM_ROLE_SLAVE:
                        CliPrintf (CliHandle,
                                   "\rRole Played               : Slave\r\n");
                        break;

                    case LA_DLAG_SYSTEM_ROLE_NONE:
                        CliPrintf (CliHandle,
                                   "\rRole Played               : None\r\n");
                        break;

                    default:
                        break;
                }

                CliPrintf (CliHandle,
                           "\rCurrent Keep Alive Count  : %d\r\n",
                           i4DLAGRemoteKeepAliveCount);

                if (u4DLAGRemoteChannelSpeed >= CFA_ENET_MAX_SPEED_VALUE)
                {
                    if (u4DLAGRemoteChannelHiSpeed == CFA_ENET_HISPEED_10G)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 10 Gbps\r\n");
                    }
                    else if (u4DLAGRemoteChannelHiSpeed == CFA_ENET_HISPEED_40G)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 40 Gbps\r\n");
                    }

                    else if (u4DLAGRemoteChannelHiSpeed == CFA_ENET_HISPEED_56G)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 56 Gbps\r\n");
                    }

                }
                else
                {
                    if (u4DLAGRemoteChannelSpeed == CFA_ENET_SPEED_10M)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 10 Mbps\r\n");
                    }
                    else if (u4DLAGRemoteChannelSpeed == CFA_ENET_SPEED_1G)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 1 Gbps\r\n");
                    }
                    else if (u4DLAGRemoteChannelSpeed == CFA_ENET_SPEED_2500M)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 2.5 Gbps\r\n");
                    }
                    else if (u4DLAGRemoteChannelSpeed == CFA_ENET_SPEED_100M)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 100 Mbps\r\n");
                    }
                    else if (u4DLAGRemoteChannelSpeed == CFA_ENET_SPEED)
                    {
                        CliPrintf (CliHandle,
                                   "\rSpeed                     : 100 Mbps\r\n");
                    }
                }

                CliPrintf (CliHandle, "\rMTU                       : %d\r\n",
                           i4DLAGRemoteChannelMtu);
                CliPrintf (CliHandle, "\n");

                CliPrintf (CliHandle, "\r%s\r\n", "Remote Ports Info :-");
                MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                pifName = &au1Temp[0];

                i4PrevRemotePortIndex = 0;
                while (nmhGetNextIndexFsLaDLAGRemotePortTable
                       (i4PortChannelIndex, &i4RemotePortChannelIndex,
                        BaseMac, &RemoteBaseMac,
                        i4PrevRemotePortIndex,
                        &i4RemotePortIndex) == SNMP_SUCCESS)
                {
                    if ((i4PortChannelIndex != i4RemotePortChannelIndex) ||
                        (MEMCMP (BaseMac, RemoteBaseMac, LA_MAC_ADDRESS_SIZE) !=
                         0))
                        break;

                    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                    LaActiveDLAGGetIfIndexFromBridgeIndex ((UINT4)
                                                           i4RemotePortIndex,
                                                           &u4CfaRemotePortIndex);

                    LaCfaCliGetIfName (u4CfaRemotePortIndex, (INT1 *) pifName);

                    i4RemotePortBundleState = i4RemotePortSyncStatus = 0;
                    nmhGetFsLaDLAGRemotePortBundleState
                        (i4RemotePortChannelIndex, RemoteBaseMac,
                         i4RemotePortIndex, &i4RemotePortBundleState);

                    nmhGetFsLaDLAGRemotePortSyncStatus
                        (i4RemotePortChannelIndex, RemoteBaseMac,
                         i4RemotePortIndex, &i4RemotePortSyncStatus);

                    nmhGetFsLaDLAGRemotePortPriority
                        (i4RemotePortChannelIndex, RemoteBaseMac,
                         i4RemotePortIndex, &i4RemotePortPriority);

                    CliPrintf (CliHandle, "\rIf Index : %-5s, ", pifName);
                    switch (i4RemotePortBundleState)
                    {
                        case LA_PORT_UP_IN_BNDL:
                            CliPrintf (CliHandle, "State : Up in Bundle, ");
                            /* Up in Bundle ports */
                            break;

                        case LA_PORT_STANDBY:
                            CliPrintf (CliHandle, "State : Hot standby, ");
                            /* Stand by ports */
                            break;

                        case LA_PORT_DOWN:
                            CliPrintf (CliHandle,
                                       "State : Down, Not in Bundle, ");
                            /* Down ports */
                            break;

                        case LA_PORT_UP_INDIVIDUAL:
                            CliPrintf (CliHandle, "State : Up, Independent, ");
                            /* Up in Individual */
                            break;

                        default:
                            break;
                    }

                    switch (i4RemotePortSyncStatus)
                    {
                        case LA_DLAG_PORT_IN_SYNC:
                            CliPrintf (CliHandle, "Sync State: In Sync, ");
                            break;

                        case LA_DLAG_PORT_OUT_OF_SYNC:
                            CliPrintf (CliHandle, "Sync State: Out of Sync, ");
                            break;

                        default:
                            break;
                    }

                    CliPrintf (CliHandle, "Priority: %d ",
                               i4RemotePortPriority);
                    CliPrintf (CliHandle, "\n");
                    i4PrevRemotePortIndex = i4RemotePortIndex;
                }

                MEMCPY (PrevBaseMac, BaseMac, LA_MAC_ADDRESS_SIZE);
                CliPrintf (CliHandle, "\n");
            }
        }
        CliPrintf (CliHandle, "\n");
        u1Quit = (UINT1) CliPrintf (CliHandle, "\r");
        if (u1Quit == CLI_FAILURE)
        {
            break;
        }
        i4PrevIndex = i4Index;
    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS) && (u1flag == 0));
    FsUtilReleaseBitList ((UINT1 *) pDistributePorts);

    UNUSED_PARAM (i4RetValue);
    UNUSED_PARAM (i1RetValue);
    return (CLI_SUCCESS);
}
#endif /* DLAG_WANTED */
#ifdef ICCH_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGSystemPriority                      */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MC-LAG system priority      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaMCLAGSystemPriority - MC-LAG System Priority   */
/*                                                  value*/
/*                        CliHandle               - CLI Handler              */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       MC-LAG System priority needs to be  */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGSystemPriority (tCliHandle CliHandle,
                               UINT4 u4LaMCLAGSystemPriority, UINT4 u4AggIndex)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelMCLAGSystemPriority (&u4ErrCode, i4AggIfIndex,
                                                      (INT4)
                                                      u4LaMCLAGSystemPriority))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaPortChannelMCLAGSystemPriority (i4AggIfIndex,
                                                  (INT4)
                                                  u4LaMCLAGSystemPriority) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%MC-LAG system priority set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGSystemID                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MCLAG system ID             */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                        u4AggIndex   - AggIndex of port-channel for which  */
/*                                       MC-LAG System ID needs to be        */
/*                                       configured.                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGSystemID (tCliHandle CliHandle, tMacAddr SystemID,
                         UINT4 u4AggIndex)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelMCLAGSystemID (&u4ErrCode, i4AggIfIndex,
                                                SystemID)) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetFsLaPortChannelMCLAGSystemID (i4AggIfIndex, SystemID))
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%MC-LAG system mac address set routine failed\r\n");
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetDLAGGlobalSystemID                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Global MC-LAG system ID     */
/*                                                                           */
/*     INPUT            : SystemID - Global System ID                        */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetDLAGGlobalSystemID (tCliHandle CliHandle, tMacAddr SystemID)
{

    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaMCLAGSystemID (&u4ErrCode, SystemID)) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetFsLaMCLAGSystemID (SystemID)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%MC-LAG Global system mac address set routine failed\r\n");
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGGlobalPeriodicTime                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MC-LAG global periodic      */
/*                        sync. time                                         */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaMCLAGPeriodicTime   - MC-LAG periodic          */
/*                                                  sync. time               */
/*                        CliHandle               - CLI handler              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGGlobalPeriodicTime (tCliHandle CliHandle,
                                   UINT4 u4LaMCLAGPeriodicTime)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaMCLAGPeriodicSyncTime (&u4ErrCode,
                                            u4LaMCLAGPeriodicTime) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaMCLAGPeriodicSyncTime (u4LaMCLAGPeriodicTime) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%Configuring MC-LAG Global system periodic sync time failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGGlobalSystemPriority                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the MCLAG Global system priority*/
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4LaMCLAGSystemPriority - MCLAG System Priority    */
/*                                                  value                    */
/*                        CliHandle               - CLI Handler              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGGlobalSystemPriority (tCliHandle CliHandle,
                                     UINT4 u4LaMCLAGSystemPriority)
{
    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaMCLAGSystemPriority (&u4ErrCode, (INT4)
                                           u4LaMCLAGSystemPriority)) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaMCLAGSystemPriority ((INT4) u4LaMCLAGSystemPriority)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%MC-LAG Global system priority set routine failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGSetMCLAGStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables MC-LAG functionality*/
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4MCLAGStatus - MCLAG status (enable/disable)      */
/*                        CliHandle     - CLI Handler                        */
/*                        u4AggIndex    - AggIndex of port-channel in whih   */
/*                                        MC-LAG functionality needs to be   */
/*                                        enabled/disabled.                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaMCLAGSetMCLAGStatus (tCliHandle CliHandle, UINT4 u4MCLAGStatus,
                       UINT4 u4AggIndex)
{

    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4AggIfIndex = 0;
    UINT4               u4ErrCode = 0;

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if ((nmhTestv2FsLaPortChannelMCLAGStatus (&u4ErrCode, i4AggIfIndex,
                                              (INT4) u4MCLAGStatus))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaPortChannelMCLAGStatus (i4AggIfIndex, u4MCLAGStatus))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetMCLAGSystemStatus                             */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables MC-LAG functionality*/
/*                         globally in system                                */
/*                                                                           */
/*                                                                           */
/*     INPUT            : u4MCLAGStatus - MC-LAG status (enable/disable)     */
/*                        CliHandle    - CLI Handler                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
LaSetMCLAGSystemStatus (tCliHandle CliHandle, UINT4 u4MCLAGStatus)
{

    UINT4               u4ErrCode = 0;

    if ((nmhTestv2FsLaMCLAGSystemStatus (&u4ErrCode, (INT4) u4MCLAGStatus))
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsLaMCLAGSystemStatus ((INT4) u4MCLAGStatus)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% MCLAG System Status set failed\r\n");
        return CLI_FAILURE;
    }
    if (u4MCLAGStatus == LA_ENABLED)
    {
        CliPrintf (CliHandle, "Configuration on MC-LAG interfaces shall be "
                   "done uniformly across MC-LAG nodes\r\n");
    }

    return CLI_SUCCESS;
}
#endif /* ICCH_WANTED */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaShowPortChannelInfo                              */
/*                                                                           */
/*     DESCRIPTION      : Displays portchannel specific information          */
/*                                                                           */
/*     INPUT            : u2PortChannelIndex -specifies the portchannel index*/
/*                        u1CmdType - display format Number                  */
/*                                                                           */
/*     OUTPUT           : pLaLacPortState - Pointer which stores the current */
/*                                          Port OperState                   */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
LaShowPortChannelInfo (tCliHandle CliHandle, UINT2 u2PortChannelIndex,
                       UINT1 u1CmdType)
{

    INT4                i4Index = 0;
    UINT1               u1flag = 1;
    tLaLacAggEntry     *pLaLacAggEntry;
    UINT4               u2ChannelIfIndex;
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1               u1LoopCount = 0;
    UINT1               u1IsShowSummary = LA_FALSE;
    UINT4               u4IcclIfIndex = 0;
    INT4                i4IsMCLAGEnabled = LA_DISABLED;
#ifdef ICCH_WANTED
    INT4                i4PortChannelMCLAGStatus = LA_MCLAG_DISABLED;
    INT1                i1MclagLoopCount = 0;
    INT1                i1IcclLoopCount = 0;
#endif

    if ((u1CmdType == 0) || (u1CmdType == LA_SHOW_DETAIL)
        || (u1CmdType == LA_SHOW_SUMMARY))
    {
        u1IsShowSummary = LA_TRUE;
    }

    if (u2PortChannelIndex == 0)
    {
        /* No Port Channel index is given by user. We get the first interface
         * from the table.
         */
        /* display the default summary details even though there won't
         * be any portchannel configured in the switch */

        u1flag = 0;
        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            if (u1IsShowSummary == LA_FALSE)
            {
                return;
            }
        }
    }
    else
    {

        /* Key is given by user. Obtain the interface index based on key */
        if (LaGetAggEntryByKey (u2PortChannelIndex, &pLaLacAggEntry) !=
            LA_SUCCESS)
        {
            if ((u1IsShowSummary == LA_FALSE) || (u2PortChannelIndex != 0))
            {

                CliPrintf (CliHandle, "\r%% Invalid Port-channel\r\n");

                return;
            }
        }
        /* Get the Aggreation Index only when there is a
         * port channel configured in the switch */
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }

    /*Get MCLAG Global status */
    nmhGetFsLaMCLAGSystemStatus (&i4IsMCLAGEnabled);
    LaIcchGetIcclIfIndex (&u4IcclIfIndex);
    do
    {
        if ((u1CmdType != LA_SHOW_ICCL)
#ifdef ICCH_WANTED
            && (u4IcclIfIndex != (UINT4) i4Index))
#else
            )
#endif
        {
            u1LoopCount++;
        }
        u2ChannelIfIndex = (UINT2) i4Index;
        /* display LA Protocol and global configuration details for 
         * the CLI command "show etherchannel" */
        if (!u1CmdType)
        {
            /* display LA Global configuration details one time */
            if (u1LoopCount == 1)
            {
                ShowLaGlobalInfo (CliHandle);
            }
            /* display LA Protocol details for all the Port Channels */
            u1Quit = (UINT1) ShowLaProtocol (CliHandle,
                                             (UINT4) u2ChannelIfIndex,
                                             u1LoopCount);
        }

        switch (u1CmdType)
        {
            case LA_SHOW_LOADBALANCE:
                u1Quit =
                    (UINT1) ShowLaLoadBalance (CliHandle,
                                               (UINT4) u2ChannelIfIndex,
                                               u1LoopCount);
                break;

            case LA_SHOW_PROTOCOL:
                u1Quit = (UINT1) ShowLaProtocol (CliHandle,
                                                 (UINT4) u2ChannelIfIndex,
                                                 u1LoopCount);
                break;

            case LA_SHOW_PORTCHANNEL:

                /* display LA Global configuration details one time */
                if (u1LoopCount == 1)
                {
                    ShowLaGlobalInfo (CliHandle);
                }
                /* display Portchannel details for all the Port Channels */
                u1Quit =
                    (UINT1) ShowLaPortChannel (CliHandle,
                                               (UINT4) u2ChannelIfIndex,
                                               u1CmdType);
                break;

            case LA_SHOW_SUMMARY:
#ifdef ICCH_WANTED
                if (u4IcclIfIndex != u2ChannelIfIndex)
#endif
                {
                    u1Quit = (UINT1) ShowLaSummary (CliHandle,
                                                    (UINT4) u2ChannelIfIndex,
                                                    u1LoopCount, u1CmdType);
                }
                break;

            case LA_SHOW_PORT:
                u1Quit = (UINT1) ShowLaPort (CliHandle,
                                             (UINT4) u2ChannelIfIndex,
                                             u1CmdType);
                break;

            case LA_SHOW_DETAIL:
                /* display LA Global configuration details one time */
                if (u1LoopCount == 1)
                {
                    ShowLaGlobalInfo (CliHandle);
                }
                /* display the Ports details which are binded to the
                 * particular Port Channel */
                u1Quit = (UINT1) ShowLaPort (CliHandle,
                                             (UINT4) u2ChannelIfIndex,
                                             u1CmdType);

                if (u1Quit == CLI_FAILURE)
                {
                    break;
                }
                /* display the Port channel details for the particular 
                 * Port Channel */
                u1Quit =
                    (UINT1) ShowLaPortChannel (CliHandle,
                                               (UINT4) u2ChannelIfIndex,
                                               u1CmdType);
                break;

#ifdef ICCH_WANTED
            case LA_SHOW_MCLAG:
                /*Display etherchannel summary for MCLAG ports alone */
                if (i4IsMCLAGEnabled == LA_ENABLED)
                {
                    nmhGetFsLaPortChannelMCLAGStatus ((INT4) u2ChannelIfIndex,
                                                      &i4PortChannelMCLAGStatus);
                }

                if (i4PortChannelMCLAGStatus == LA_MCLAG_ENABLED)
                {

                    i1MclagLoopCount++;
                    u1Quit = (UINT1) ShowLaSummary (CliHandle,
                                                    (UINT4) u2ChannelIfIndex,
                                                    (UINT1) i1MclagLoopCount,
                                                    u1CmdType);

                }

                break;
            case LA_SHOW_ICCL:
                /*Display etherchannel summary for ICCL ports alone */
                if (u4IcclIfIndex == u2ChannelIfIndex)
                {
                    i1IcclLoopCount++;
                    u1Quit = (UINT1) ShowLaSummary (CliHandle,
                                                    (UINT4) u2ChannelIfIndex,
                                                    (UINT1) i1IcclLoopCount,
                                                    u1CmdType);
                }
                break;
#endif

        }

        if ((u1flag == 1) || (u1Quit == CLI_FAILURE))
        {
            break;
        }
    }
    while (nmhGetNextIndexDot3adAggTable (u2ChannelIfIndex, &i4Index)
           != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaShowIntEtherChannelInfo                          */
/*                                                                           */
/*     DESCRIPTION      : Displays portchannel specific information          */
/*                                                                           */
/*     INPUT            : u4IfIndex,  CliHandle CLIHandler                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
LaShowIntEtherChannelInfo (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1               u1IsShowCommand = LA_SHOW_INT_ETHERCHANNEL;
    UINT1               u1PortState;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ChannelIfIndex = 0;
    INT4                i4PortMode = 0;
    INT4                i4AdminKey = 0;
    INT4                i4OperKey = 0;

    INT4                i4BundleState = 0;
    INT4                i4PortPriority = 0;
    INT4                i4WaitTime = 0;
    INT4                i4Key = 0;
    INT4                i4AdminPort = 0;
    tLaLacAggEntry     *pLaLacAggEntry;
    tLaLacPortState    *pPortOperState;

    tSNMP_OCTET_STRING_TYPE RetPortState;

    RetPortState.pu1_OctetList = &u1PortState;

    /* No Port index is given by user. So, display the details of 
     * all the Port Channels and all the Ports 
     * which are binded to the port channel */
    if (u4IfIndex == 0)
    {

        if (nmhGetFirstIndexDot3adAggTable ((INT4 *) &u4IfIndex) !=
            SNMP_SUCCESS)
        {
            return;
        }
        /* display all Ports details which are binded to the particular
         * port channel */
        do
        {
            CliPrintf (CliHandle, "\r\n");
            u4ChannelIfIndex = u4IfIndex;
            u1Quit = (UINT1) ShowLaPort (CliHandle, u4ChannelIfIndex,
                                         u1IsShowCommand);

            if (u1Quit == CLI_FAILURE)
            {
                break;
            }
        }
        while (nmhGetNextIndexDot3adAggTable ((INT4) u4ChannelIfIndex,
                                              (INT4 *) &u4IfIndex) !=
               SNMP_FAILURE);

        /* display all Port Channel details */
        u4IfIndex = 0;
        if (nmhGetFirstIndexDot3adAggTable ((INT4 *) &u4IfIndex) !=
            SNMP_SUCCESS)
        {
            return;
        }

        do
        {
            u4ChannelIfIndex = u4IfIndex;
            u1Quit = (UINT1) ShowLaPortChannel (CliHandle, u4ChannelIfIndex,
                                                u1IsShowCommand);
            if (u1Quit == CLI_FAILURE)
            {
                break;
            }
        }
        while (nmhGetNextIndexDot3adAggTable ((INT4) u4ChannelIfIndex,
                                              (INT4 *) &u4IfIndex) !=
               SNMP_FAILURE);
    }
    /* display Port details for the particular port */
    else
    {

        if (LaGetAggEntryBasedOnPort ((UINT2) u4IfIndex,
                                      &pLaLacAggEntry) != LA_SUCCESS)
        {
            /* display the following Information if the given port was
             * not binded with any port-channel configured in the switch */
            CliPrintf (CliHandle,
                       "\r\n%% Etherchannel not enabled on this port\r\n");
            return;
        }
        u4ChannelIfIndex = (UINT4) pLaLacAggEntry->u2AggIndex;
        nmhGetDot3adAggActorAdminKey ((INT4) u4ChannelIfIndex, &i4Key);
        nmhGetFsLaPortBundleState ((INT4) u4IfIndex, &i4BundleState);
        nmhGetFsLaPortMode ((INT4) u4IfIndex, &i4PortMode);
        nmhGetDot3adAggPortActorPortPriority ((INT4) u4IfIndex,
                                              &i4PortPriority);
        nmhGetFsLaPortAggregateWaitTime ((INT4) u4IfIndex,
                                         (UINT4 *) &i4WaitTime);
        nmhGetFsLaPortActorAdminPort ((INT4) u4IfIndex, &i4AdminPort);
        LaUtilGetPortState ((UINT2) u4IfIndex, &pPortOperState, LA_CLI_ACTOR);
        nmhGetDot3adAggPortActorAdminKey ((INT4) u4IfIndex, &i4AdminKey);
        nmhGetDot3adAggPortActorOperKey ((INT4) u4IfIndex, &i4OperKey);
        nmhGetDot3adAggPortActorOperState ((INT4) u4IfIndex, &RetPortState);
        if (pPortOperState == NULL)
        {
            return;
        }

        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        LaCfaCliGetIfName (u4IfIndex, (INT1 *) au1Temp);

        /* Lock before Print */

        /* Print all the Objects */

        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "\r\nPort : ");
        CliPrintf (CliHandle, "%s\r\n", au1Temp);
        CliPrintf (CliHandle, "\r-------------\r\n");

        CliPrintf (CliHandle, "\r\n");

        if (i4BundleState == LA_PORT_UP_IN_BNDL)
        {
            CliPrintf (CliHandle, "Port State = Up in Bundle\r\n");
            /* Up in Bundle ports */
        }
        else if (i4BundleState == LA_PORT_STANDBY)
        {
            CliPrintf (CliHandle, "Port State = Hot standby\r\n");
            /* Stand by ports */
        }
        else if (i4BundleState == LA_PORT_DOWN)
        {
            CliPrintf (CliHandle, "Port State = Down, Not in Bundle\r\n");
            /* Down ports */
        }
        else if (i4BundleState == LA_PORT_UP_INDIVIDUAL)
        {
            CliPrintf (CliHandle, "Port State = Up, Independent \r\n");
            /* Up in Individual */
        }

        CliPrintf (CliHandle, "\rChannel Group :  %d\r\n", i4Key);
        if (i4PortMode == LA_MODE_DISABLED)
        {
            CliPrintf (CliHandle, "Mode : Disabled\r\n");
        }
        else if (i4PortMode == LA_MODE_MANUAL)
        {
            CliPrintf (CliHandle, "Mode : On\r\n");
        }
        else if (i4PortMode == LA_MODE_LACP)
        {
            CliPrintf (CliHandle, "Mode : Active\r\n");
        }

        /* Pseudo port-channel displays the configured port-channel information
           and the port-channel displays the active port-channel information
           (Operationally up in bundle ports) */

        if (i4BundleState == LA_PORT_UP_IN_BNDL)
        {
            CliPrintf (CliHandle, "Port-channel = Po%d\r\n", i4Key);
        }
        else
        {
            CliPrintf (CliHandle, "Port-channel = Null\r\n");
        }
        CliPrintf (CliHandle, "Pseudo port-channel = Po%d\r\n", i4Key);

        CliPrintf (CliHandle, "LACP port-priority  = %d\r\n", i4PortPriority);
        CliPrintf (CliHandle, "LACP Wait-time  = %d secs \r\n",
                   (i4WaitTime / 100));
        CliPrintf (CliHandle, "LACP Admin Port = %d \r\n", i4AdminPort);

        if (pPortOperState->LaLacpActivity == LA_ACTIVE)
        {
            CliPrintf (CliHandle, "LACP Activity : Active \r\n");
        }
        else if (pPortOperState->LaLacpActivity == LA_PASSIVE)
        {
            CliPrintf (CliHandle, "LACP Activity : Passive \r\n");

        }
        if (pPortOperState->LaLacpTimeout == LA_SHORT_TIMEOUT)
        {
            CliPrintf (CliHandle, "LACP Timeout : Short\r\n");
        }
        else if (pPortOperState->LaLacpTimeout == LA_LONG_TIMEOUT)
        {
            CliPrintf (CliHandle, "LACP Timeout : Long\r\n");
        }

        if (pPortOperState->LaAggregation == LA_TRUE)
        {
            CliPrintf (CliHandle, "\r\nAggregation State : Aggregation, ");
        }
        else if (pPortOperState->LaAggregation == LA_FALSE)
        {
            CliPrintf (CliHandle, "\r\nAggregation State : Individual, ");
        }

        if (pPortOperState->LaSynchronization == LA_TRUE)
        {
            CliPrintf (CliHandle, "Sync, ");
        }

        if (pPortOperState->LaCollecting == LA_TRUE)
        {
            CliPrintf (CliHandle, "Collecting, ");
        }

        if (pPortOperState->LaDistributing == LA_TRUE)
        {
            CliPrintf (CliHandle, "Distributing, ");
        }

        if (pPortOperState->LaDefaulted == LA_TRUE)
        {
            CliPrintf (CliHandle, "Defaulted ");
        }

        if (pPortOperState->LaExpired == LA_TRUE)
        {
            CliPrintf (CliHandle, "Expired ");
        }

        u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");

        if (u1Quit == CLI_FAILURE)
        {
            return;
        }

        /* display LACP information for the port */
        CliPrintf (CliHandle, "\r\n%26s%8s%6s%7s%8s\r\n",
                   "LACP Port", "Admin", "Oper", "Port", "Port");
        CliPrintf (CliHandle, "%-9s%-8s%-12s%-7s%-7s%-8s%-8s",
                   "Port", "State", "Priority", "Key", "Key", "Number",
                   "State");
        CliPrintf (CliHandle,
                   "\r\n------------------------------------------"
                   "---------------------------\r\n");
        CliPrintf (CliHandle, "%-9s", au1Temp);
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        if (i4BundleState == LA_PORT_UP_IN_BNDL)
        {
            CliPrintf (CliHandle, "%-8s", "Bundle");
            /* Up in Bundle ports */
        }
        if (i4BundleState == LA_PORT_STANDBY)
        {
            CliPrintf (CliHandle, "%-8s", "Standby");
            /* Stand by ports */
        }
        if (i4BundleState == LA_PORT_DOWN)
        {
            CliPrintf (CliHandle, "%-8s", "Down");
            /* Down ports */
        }
        if (i4BundleState == LA_PORT_UP_INDIVIDUAL)
        {
            CliPrintf (CliHandle, "%-8s", "Indep");
            /* Up in Individual */
        }

        CliPrintf (CliHandle, "%-12d", i4PortPriority);

        CliPrintf (CliHandle, "%-7d", i4AdminKey);

        CliPrintf (CliHandle, "%-7d", i4OperKey);

        CliPrintf (CliHandle, "0x%-6x", u4IfIndex);

        u1Quit =
            (UINT1) CliPrintf (CliHandle, "0x%-6x\r\n",
                               RetPortState.pu1_OctetList[0]);

        if (u1Quit == CLI_FAILURE)
        {
            return;
        }

    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaLoadBalance                                  */
/*                                                                           */
/*     DESCRIPTION      : Displays portchannel load-balancing information    */
/*                                                                           */
/*     INPUT            : u4IfIndex -  specifies the portchannel index,      */
/*                        u1LoopCount - Count which refers the no of time    */
/*                                      this function called.                */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
ShowLaLoadBalance (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1LoopCount)
{
    INT4                i4PortSelPolicy = 0;
    INT4                i4Key = 0;

    if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetFsLaPortChannelSelectionPolicyBitList ((INT4) u4IfIndex,
                                                 &i4PortSelPolicy);
    nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key);

    /* Release the Lock here ... */

    if (u1LoopCount == 1)
    {
        CliPrintf (CliHandle, "\n\r%38s\r\n", "Channel Group Listing");
        CliPrintf (CliHandle, "\r%38s\r\n", "---------------------");
    }

    CliPrintf (CliHandle, "\rGroup : %d \r\n", i4Key);
    CliPrintf (CliHandle, "\r----------\r\n");

    if (i4PortSelPolicy == LA_SELECT_SRC_MAC)
    {
        CliPrintf (CliHandle, "Source MAC Address \r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_DST_MAC)
    {
        CliPrintf (CliHandle, "Destination MAC Address \r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_SRC_DST_MAC)
    {
        CliPrintf (CliHandle, "Source and Destination MAC Address\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_SRC_IP)
    {
        CliPrintf (CliHandle, "Source IP Address \r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_DST_IP)
    {
        CliPrintf (CliHandle, "Destination IP Address \r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_SRC_DST_IP)
    {
        CliPrintf (CliHandle, "Source and Destination IP Address\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_VLAN_ID)
    {
        CliPrintf (CliHandle, "Vlan ID\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_SA_DA_IP_PORT_PROTO)
    {
        CliPrintf (CliHandle,
                   "SRC,DST IP's,SRC,DST MAC's,Protocol,L4 Src and Dst\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_SERVICE_INSTANCE)
    {
        CliPrintf (CliHandle, "Service instance\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_MAC_SRC_VID)
    {
        CliPrintf (CliHandle, "Source Mac VID\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_MAC_DST_VID)
    {
        CliPrintf (CliHandle, "Destination Mac VID\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_MAC_SRC_DST_VID)
    {
        CliPrintf (CliHandle, "Source and Destination Mac VID\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_DST_IP6)
    {
        CliPrintf (CliHandle, "Destination IP6\r\n");
    }

    if (i4PortSelPolicy == LA_SELECT_SRC_IP6)
    {
        CliPrintf (CliHandle, "Source IP6\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_L3_PROTOCOL)
    {
        CliPrintf (CliHandle, "Select L3 Protocol\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_DST_L4_PORT)
    {
        CliPrintf (CliHandle, "Destination L4 Port\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_SRC_L4_PORT)
    {
        CliPrintf (CliHandle, "Source L4 Port\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_MPLS_VC_LABEL)
    {
        CliPrintf (CliHandle, "Mpls VC Label\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_MPLS_TUNNEL_LABEL)
    {
        CliPrintf (CliHandle, "Mpls Tunnel Label\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_MPLS_VC_TUNNEL)
    {
        CliPrintf (CliHandle, "Mpls VC Tunnel\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_ENHANCED)
    {
        CliPrintf (CliHandle, "Enhanced Hashing\r\n");
    }
    if (i4PortSelPolicy == LA_SELECT_RANDOMIZED)
    {
        CliPrintf (CliHandle, "Randomized Load Balance\r\n");
    }

    /* Lock here again... */

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaProtocol                                     */
/*                                                                           */
/*     DESCRIPTION      : Displays portchannel protocol information          */
/*                                                                           */
/*     INPUT            : u4IfIndex -  specifies the portchannel index,      */
/*                        u1LoopCount - Count which refers the no of time    */
/*                                      this function called.                */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ShowLaProtocol (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1LoopCount)
{
    INT4                i4PortChannelMode = 0;
    INT4                i4Key = 0;
    UINT1               u1BridgeStatus = CFA_ENABLED;

    if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);
    nmhGetFsLaPortChannelMode ((INT4) u4IfIndex, &i4PortChannelMode);
    nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key);

    /*Release the Lock here .. */

    if (u1LoopCount == 1)
    {
        CliPrintf (CliHandle, "\n\r%38s\r\n", "Channel Group Listing");
        CliPrintf (CliHandle, "\r%38s\r\n", "---------------------");
    }

    CliPrintf (CliHandle, "\rGroup : %d \r\n", i4Key);
    CliPrintf (CliHandle, "\r----------\r\n");
    if (u1BridgeStatus != CFA_ENABLED)
    {
        CliPrintf (CliHandle, "\rGroup Status : L3 \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rGroup Status : L2 \r\n");
    }

    CliPrintf (CliHandle, "\rProtocol : ");

    switch (i4PortChannelMode)
    {
        case LA_MODE_LACP:
            CliPrintf (CliHandle, "LACP\r\n");
            break;
        case LA_MODE_MANUAL:
            CliPrintf (CliHandle, "Manual\r\n");
            break;
        case LA_MODE_DISABLED:
            CliPrintf (CliHandle, "Disabled\r\n");
            break;
        default:
            CliPrintf (CliHandle, "- \r\n");
            break;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaPortChannel                                  */
/*                                                                           */
/*     DESCRIPTION      : Displays portchannel information                   */
/*                                                                           */
/*     INPUT            : u4IfIndex -  specifies the portchannel index,      */
/*                        u1IsShowCommand - specifies the user input show    */
/*                                          command name                     */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ShowLaPortChannel (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1IsShowCommand)
{
    UINT4               u4PortChannelUpCount = 0;
    UINT4               u4PortChannelDownCount = 0;
    UINT4               u4OperChgTimeStamp = 0;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];
#ifdef ICCH_WANTED
    INT4                i4IsMCLAGEnabled = LA_DISABLED;
    INT4                i4PortChannelMCLAGStatus = LA_MCLAG_DISABLED;
#endif
    INT4                i4Key;
    INT4                i4BundleState;
    INT4                i4PortMode;
    INT4                i4DefaultPort = 0;
    INT4                i4LaAggMaxPorts = 0;
    INT4                i4PortChannelMode = 0;
    INT4                i4DownReason = 0;
    UINT1              *pu1Temp = NULL;
    UINT2               u2Port;
    UINT2               u2NextPort;
    UINT2               u2Count;
    UINT2               u2PortCount = 0;
    UINT2               u2Index;
    UINT2               au2Ports[LA_MAX_PORTS];
    UINT1               u1Agginuse = 0;
    UINT1               u1Standby = 0;
    UINT1               u1Disabled = 0;
    UINT1               au1Tmp[MAX_LEN_TEMP_ARRAY];
    UINT1               u1IsDisplay = LA_TRUE;
    UINT1               u1BridgeStatus = CFA_ENABLED;
    UINT1               au1Date[LA_REQUIRED_BUF_SIZE];
    tMacAddr            BaseMac;
    tLaLacAggEntry     *pAggEntry = NULL;

    if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);
    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1Tmp, 0, MAX_LEN_TEMP_ARRAY);
    MEMSET (au1Date, 0, LA_REQUIRED_BUF_SIZE);

    pu1Temp = &au1Tmp[0];

    /* Get all the Mib Objects */
    nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key);
#ifdef ICCH_WANTED
    nmhGetFsLaMCLAGSystemStatus (&i4IsMCLAGEnabled);
    if (i4IsMCLAGEnabled == LA_ENABLED)
    {
        nmhGetFsLaPortChannelMCLAGStatus ((INT4) u4IfIndex,
                                          &i4PortChannelMCLAGStatus);
    }
#endif
    /* Don't display Group and Port-Channel heading information for the
     * CLI commands "show interface etherchannel" and "show etherchannel
     * details" */

    if ((u1IsShowCommand == LA_SHOW_DETAIL) ||
        (u1IsShowCommand == LA_SHOW_INT_ETHERCHANNEL))
    {
        u1IsDisplay = LA_FALSE;
    }

    u2Port = 0;
    u2NextPort = 0;
    LaUtilGetNextAggPort ((UINT2) u4IfIndex, u2Port, &u2NextPort);
    LA_MEMSET (au2Ports, 0, sizeof (au2Ports));
    u2Count = 0;

    while ((u2NextPort != 0) && (u2Count < LA_MAX_PORTS))
    {
        u2Port = u2NextPort;
        au2Ports[u2Count] = u2Port;
        u2Count++;
        LaUtilGetNextAggPort ((UINT2) u4IfIndex, u2Port, &u2NextPort);
        u2PortCount = u2Count;
    }

    for (u2Index = 0; ((u2Index < u2Count) &&
                       (u2Index < LA_MAX_PORTS)); u2Index++)
    {
        nmhGetFsLaPortMode (au2Ports[u2Index], &i4PortMode);
        if (i4PortMode != LA_MODE_DISABLED)
        {
            nmhGetFsLaPortBundleState (au2Ports[u2Index], &i4BundleState);
            if (i4BundleState == LA_PORT_UP_IN_BNDL)
            {
                u1Agginuse++;
                /*in Port channel, Agg-in-use */
            }
            else if (i4BundleState == LA_PORT_STANDBY)
            {
                u1Standby++;
                /*Hot-standby  */
            }
        }
        else if (i4PortMode == LA_MODE_DISABLED)
        {
            u1Disabled++;
            /*disabled */
        }
    }

    nmhGetDot3adAggMACAddress ((INT4) u4IfIndex, (tMacAddr *) & BaseMac);

    nmhGetFsLaPortChannelDefaultPortIndex ((INT4) u4IfIndex, &i4DefaultPort);

    nmhGetFsLaPortChannelMaxPorts ((INT4) u4IfIndex, &i4LaAggMaxPorts);

    nmhGetFsLaPortChannelUpCount ((INT4) u4IfIndex, &u4PortChannelUpCount);

    nmhGetFsLaPortChannelDownCount ((INT4) u4IfIndex, &u4PortChannelDownCount);

    nmhGetFsLaPortChannelOperChgTimeStamp ((INT4) u4IfIndex,
                                           &u4OperChgTimeStamp);

    nmhGetFsLaPortChannelDownReason ((INT4) u4IfIndex, &i4DownReason);

    /* Print all the Values of the Objects.. */

    if (u1IsDisplay == LA_TRUE)
    {
        CliPrintf (CliHandle, "\n\r%38s\r\n", "Channel Group Listing");
        CliPrintf (CliHandle, "\r%38s\r\n", "---------------------");
        CliPrintf (CliHandle, "\rGroup : %d \r\n", i4Key);
        CliPrintf (CliHandle, "\r----------\r\n");
        if (u1BridgeStatus != CFA_ENABLED)
        {
            CliPrintf (CliHandle, "\r\nGroup State : L3 ");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nGroup State : L2 ");
        }
        CliPrintf (CliHandle, "\n\r%43s\r\n", "Port-channels in the group:");
        CliPrintf (CliHandle, "\r%43s\r\n", "---------------------------");
    }
    /* display the following port-channel details for all the options
     * in the "show etherchannel" and "show interfaces etherchannel"
     * CLI command */

    CliPrintf (CliHandle, "\n\r%s : Po%d\r\n ", "Port-channel", i4Key);
    CliPrintf (CliHandle, "\r%s\r\n", "-------------------");

    CliPrintf (CliHandle, "\n\rNumber of Ports = %d\r\n", u2PortCount);

    if (u1Standby > 0)
    {
        CliPrintf (CliHandle, "\rHotStandBy port = %d\r\n", u1Standby);
    }
    else
    {
        CliPrintf (CliHandle, "\rHotStandBy port = null\r\n");
    }
    if (u1Agginuse > 0)
    {
        CliPrintf (CliHandle, "\rPort state = Port-channel Ag-Inuse \r\n");
    }
    else if (u1Disabled < u2Count)
    {
        CliPrintf (CliHandle, "\rPort state = Port-channel Ag-Not-Inuse \r\n");
    }
    if (u1Disabled == u2Count)
    {
        CliPrintf (CliHandle, "\rPort state = Port-channel "
                   "Ag-Not-Inuse Down\r\n");
    }
    nmhGetFsLaPortChannelMode ((INT4) u4IfIndex, &i4PortChannelMode);
    switch (i4PortChannelMode)
    {
        case LA_MODE_LACP:
            CliPrintf (CliHandle, "\rProtocol = LACP\r\n");
            break;
        case LA_MODE_DISABLED:
            CliPrintf (CliHandle, "\rProtocol = Disabled\r\n");
            break;
        case LA_MODE_MANUAL:
            CliPrintf (CliHandle, "\rProtocol = Manual\r\n");
            break;
        default:
            CliPrintf (CliHandle, "\rProtocol = -\r\n");
            break;
    }

    CliPrintf (CliHandle, "\rAggregator-MAC ");
    PrintMacAddress (BaseMac, pu1Temp);
    CliPrintf (CliHandle, " %s\r\n", pu1Temp);

    if (i4DefaultPort == 0)
    {
        CliPrintf (CliHandle, "\rDefault Port = None");
    }
    else
    {
        CfaCliGetIfName ((UINT4) i4DefaultPort, (INT1 *) au1Temp);
        CliPrintf (CliHandle, "\rDefault Port = %s", au1Temp);
    }

    CliPrintf (CliHandle, "\rMaximum number of Ports  = %d\r\n",
               i4LaAggMaxPorts);

    CliPrintf (CliHandle, "\rPort-channel operational Up Count = %d\r\n",
               u4PortChannelUpCount);

    CliPrintf (CliHandle, "\rPort-channel operational Down Count = %d\r\n",
               u4PortChannelDownCount);

    LaUtilTicksToDate (u4OperChgTimeStamp, au1Date);
    CliPrintf (CliHandle, "\rOperational change timestamp = %s\r\n", au1Date);
    if (i4DownReason == LA_IF_OPER_DOWN)
    {
        CliPrintf (CliHandle, "\rPort-channel is operationally Down\r\n");
    }
    else if (i4DownReason == LA_IF_ADMIN_DOWN)
    {
        CliPrintf (CliHandle, "\rPort-channel is Admin Down\r\n");
    }

#ifdef ICCH_WANTED
    if (LA_MCLAG_ENABLED == i4PortChannelMCLAGStatus)
    {
        CliPrintf (CliHandle, "\rMC-LAG is Enabled on the Port-Channel\r\n");
    }
    else if (LA_MCLAG_DISABLED == i4PortChannelMCLAGStatus)
    {
        CliPrintf (CliHandle, "\rMC-LAG is Disabled on the Port-Channel\r\n");
    }
#endif
    LaGetAggEntryByKey ((UINT2) i4Key, &pAggEntry);

    if (pAggEntry != NULL)
    {
        CliPrintf (CliHandle, "\n\rPort-Channel Mtu         = %d\r\n",
                   pAggEntry->u4Mtu);
        CliPrintf (CliHandle, "\rPort-Channel Speed       = %d Mbps\r\n",
                   (pAggEntry->u4AggSpeed / CFA_1MB));
        CliPrintf (CliHandle, "\rPort-Channel High Speed  = %d Mbps\r\n",
                   pAggEntry->u4AggHighSpeed);
        CliPrintf (CliHandle, "\rPort-Channel Member Ports Speed = %d Mbps\r\n",
                   (pAggEntry->u4PortSpeed / CFA_1MB));
        CliPrintf (CliHandle,
                   "\rPort-Channel Member Ports High Speed  = %d Mbps\r\n",
                   pAggEntry->u4PortHighSpeed);
    }
    CliPrintf (CliHandle, "\n\r");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaSummary                                      */
/*                                                                           */
/*     DESCRIPTION      : Displays portchannel information in summary        */
/*                                                                           */
/*     INPUT            : u4IfIndex -  specifies the portchannel index,      */
/*                        u1LoopCount - Count which refers the no of time    */
/*                                      this function called.                */
/*                        u1CmdType - Input command type                      */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ShowLaSummary (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1LoopCount,
               UINT1 u1CmdType)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;
    INT4                i4Key;
    INT4                i4PortMode;
    INT4                i4Channel_Mode;
    INT4                i4BundleState = 0;
    INT4                i4TmpIndex = 0;
    INT4                i4PortChannelMCLAGStatus = 0;
    UINT4               u4IcclIfIndex = 0;
    UINT1              *pu1Temp = NULL;
    UINT2               u2Index;
    UINT2               u2Port;
    UINT2               u2NextPort;
    UINT2               u2Count;
    UINT2               au2Ports[LA_MAX_PORTS];
    UINT2               u2ChannelIfIndex;
    UINT1               u1Agginuse;
    UINT1               u1Standby;
    UINT1               u1Down;
    UINT1               u1Indi;
    UINT1               u1Disabled;
    UINT1               au1Temp[LA_CLI_MAX_TEMP];
    UINT1               au1State[LA_CLI_MAX_STATE];
    UINT1               u1ChannelCount = 0;
    UINT1               u1BridgeStatus = CFA_ENABLED;
    UINT1               au1FlagDsp[LA_CLI_MAX_FLAG_DSP];
    UINT1              *au1FlagDspTemp = NULL;
    UINT1               u1OperStatus = 0;
    UINT1               u1AdminState = 0;

    if (u1LoopCount == 1)
    {
        ShowLaGlobalInfo (CliHandle);
    }

    if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    LaGetAggEntry ((UINT2) u4IfIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return CLI_FAILURE;
    }

    MEMSET (au1Temp, 0, LA_CLI_MAX_TEMP);
    MEMSET (au1State, 0, LA_CLI_MAX_STATE);
    MEMSET (au1FlagDsp, 0, LA_CLI_MAX_FLAG_DSP);
    pu1Temp = au1Temp;
    au1FlagDspTemp = au1FlagDsp;
    LaIcchGetIcclIfIndex (&u4IcclIfIndex);
    if (u1LoopCount == 1)
    {
        {
            i4TmpIndex = (INT4) u4IfIndex;

            do
            {
                nmhGetFsLaPortChannelMCLAGStatus ((INT4) i4TmpIndex,
                                                  &i4PortChannelMCLAGStatus);
                if ((u1CmdType == LA_SHOW_ICCL) &&
                    ((UINT4) i4TmpIndex == u4IcclIfIndex))
                {
                    u1ChannelCount++;
                    break;
                }
                else if ((u1CmdType == LA_SHOW_MCLAG) &&
                         (i4PortChannelMCLAGStatus == LA_MCLAG_ENABLED))
                {
                    u1ChannelCount++;
                }
                else if (((UINT4) i4TmpIndex != u4IcclIfIndex) &&
                         (u1CmdType != LA_SHOW_ICCL) &&
                         (u1CmdType != LA_SHOW_MCLAG))
                {
                    u1ChannelCount++;
                }
                u2ChannelIfIndex = (UINT2) i4TmpIndex;
            }
            while (nmhGetNextIndexDot3adAggTable (u2ChannelIfIndex,
                                                  &i4TmpIndex) != SNMP_FAILURE);
        }

        CliPrintf (CliHandle, "\n\rFlags: \r\n"
                   "\rD - down         P - in port-channel\r\n"
                   "\rI - stand-alone  H - Hot-standby (LACP only)\r\n"
                   "\rE - ErrDisabled\r\n"
                   "\rU - in-use       d - default port\r\n"
                   "\rR - Layer3\r\n"
                   "\rAD - Admin Down      AU - Admin Up\r\n"
                   "\rOD - Operative Down  OU - Operative Up\r\n");

        CliPrintf (CliHandle, "\r\nNumber of channel-groups in use: %d\r\n",
                   u1ChannelCount);
        CliPrintf (CliHandle, "\rNumber of aggregators: %d\r\n",
                   u1ChannelCount);
        CliPrintf (CliHandle, "\r\n%-7s%-16s%-11s%-5s\r\n",
                   "Group", "Port-channel", "Protocol", "Ports");
        CliPrintf (CliHandle,
                   "\r------------------------------------------"
                   "-------------------------------\r\n");
    }

    if (u4IfIndex)
    {
        u1Agginuse = 0;
        u1Down = 0;
        u1Standby = 0;
        u1Indi = 0;
        u1Disabled = 0;

        u2Port = 0;
        u2NextPort = 0;
        LaUtilGetNextAggPort ((UINT2) u4IfIndex, u2Port, &u2NextPort);
        LA_MEMSET (au2Ports, 0, sizeof (au2Ports));
        u2Count = 0;

        while ((u2NextPort != 0) && (u2Count < LA_MAX_PORTS))
        {
            u2Port = u2NextPort;
            au2Ports[u2Count] = u2Port;
            u2Count++;
            LaUtilGetNextAggPort ((UINT2) u4IfIndex, u2Port, &u2NextPort);
        }
        CfaGetCdbPortAdminStatus (u4IfIndex, &u1AdminState);
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
        if ((u1AdminState == CFA_IF_UP) && (u1OperStatus == CFA_IF_UP))
        {
            SNPRINTF ((CHR1 *) au1FlagDspTemp, LA_CLI_MAX_FLAG_DSP, "[AU,OU]");
        }
        else if ((u1AdminState == CFA_IF_UP) && (u1OperStatus == CFA_IF_DOWN))
        {
            SNPRINTF ((CHR1 *) au1FlagDspTemp, LA_CLI_MAX_FLAG_DSP, "[AU,OD]");
        }
        else if ((u1AdminState == CFA_IF_DOWN) && (u1OperStatus == CFA_IF_DOWN))
        {
            SNPRINTF ((CHR1 *) au1FlagDspTemp, LA_CLI_MAX_FLAG_DSP, "[AD,OD]");
        }
        else
        {
            SNPRINTF ((CHR1 *) au1FlagDspTemp, LA_CLI_MAX_FLAG_DSP, "[AD,OU]");
        }

        for (u2Index = 0; ((u2Index < u2Count) &&
                           (u2Index < LA_MAX_PORTS)); u2Index++)
        {
            nmhGetFsLaPortMode (au2Ports[u2Index], &i4PortMode);

            if (i4PortMode != LA_MODE_DISABLED)
            {
                nmhGetFsLaPortBundleState (au2Ports[u2Index], &i4BundleState);

                if (i4BundleState == LA_PORT_UP_IN_BNDL)
                {
                    /* Up in Bundle ports */
                    u1Agginuse++;
                }
                if (i4BundleState == LA_PORT_STANDBY)
                {
                    /* Stand by ports */
                    u1Standby++;
                }
                if (i4BundleState == LA_PORT_DOWN)
                {
                    /* Down ports */
                    u1Down++;
                }
                if (i4BundleState == LA_PORT_UP_INDIVIDUAL)
                {
                    /* Up in Individual */
                    u1Indi++;
                }
            }
            else
            {
                u1Disabled++;
            }
        }

        if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
            == SNMP_SUCCESS)
        {
            LaCfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);

            nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key);

            nmhGetFsLaPortChannelMode ((INT4) u4IfIndex, &i4Channel_Mode);

            CliPrintf (CliHandle, "%-6d ", i4Key);

            if (u1BridgeStatus == CFA_ENABLED)
            {

                if (pAggEntry->u1AggOperStatus == LA_OPER_DOWN)
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(D)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
                else if (u1Agginuse > 0)
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(U)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
                else if (u1Indi > 0)
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(I)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
                else if ((u1Down == u2Count) || (u1Disabled > 0))
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(D)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
            }
            else
            {
                if (pAggEntry->u1AggOperStatus == LA_OPER_DOWN)
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(RD)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
                else if (u1Agginuse > 0)
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(RU)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
                else if (u1Indi > 0)
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(RI)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }
                else if ((u1Down == u2Count) || (u1Disabled > 0))
                {
                    SNPRINTF ((CHR1 *) pu1Temp, LA_CLI_MAX_TEMP, "Po%-d(RD)%s",
                              i4Key, au1FlagDspTemp);
                    CliPrintf (CliHandle, "%-16s", pu1Temp);
                }

            }

            switch (i4Channel_Mode)
            {
                case LA_MODE_LACP:
                    CliPrintf (CliHandle, "%-11s", "LACP");
                    break;
                case LA_MODE_MANUAL:
                    CliPrintf (CliHandle, "%-11s", "Manual");
                    break;
                case LA_MODE_DISABLED:
                    CliPrintf (CliHandle, "%-11s", "Disabled");
                    break;
                default:
                    CliPrintf (CliHandle, "%-11s", "Unknown");
                    break;
            }

            for (u2Index = 0; ((u2Index < u2Count) &&
                               (u2Index < LA_MAX_PORTS)); u2Index++)
            {
                MEMSET (au1Temp, 0, LA_CLI_MAX_TEMP);
                MEMSET (au1State, 0, LA_CLI_MAX_STATE);

                LaCfaCliGetIfName ((UINT4) au2Ports[u2Index], (INT1 *) pu1Temp);

                if (LaGetPortEntry (au2Ports[u2Index], &pPortEntry)
                    == LA_FAILURE)
                {
                    continue;
                }

                if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
                {
                    nmhGetFsLaPortBundleState (au2Ports[u2Index],
                                               &i4BundleState);
                    if ((pPortEntry->AggSelected == LA_SELECTED)
                        && (pPortEntry->LaLacActorInfo.LaLacPortState.
                            LaDistributing == LA_TRUE)
                        && (i4BundleState == LA_PORT_UP_IN_BNDL))
                    {
                        CLI_STRNCPY (au1State, "(P", STRLEN ("(P"));
                        au1State[STRLEN ("(P")] = '\0';
                    }
                    else if (pPortEntry->AggSelected == LA_STANDBY)
                    {
                        CLI_STRNCPY (au1State, "(H", STRLEN ("(H"));
                        au1State[STRLEN ("(H")] = '\0';
                    }
                    else if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
                    {
                        CLI_STRNCPY (au1State, "(I", STRLEN ("(I"));
                        au1State[STRLEN ("(I")] = '\0';
                    }
                    else
                    {
                        CLI_STRNCPY (au1State, "(D", STRLEN ("(D"));
                        au1State[STRLEN ("(D")] = '\0';
                    }
                    if (pPortEntry->pDefAggEntry != NULL)
                    {
                        CLI_STRCAT (au1State, "d)");
                    }
                    else
                    {
                        CLI_STRCAT (au1State, ")");
                    }
                }
                else
                {
                    CLI_STRNCPY (au1State, "(D)", STRLEN ("(D)"));
                    au1State[STRLEN ("(D)")] = '\0';
                }

                if ((pPortEntry->u1PortOperStatus == LA_OPER_DOWN) &&
                    (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_DISABLED) &&
                    (gu4RecThresholdExceedAction == LA_ACT_SHUTDOWN))
                {
                    CLI_STRNCPY (au1State, "(DE)", STRLEN ("(DE)"));
                    au1State[STRLEN ("(DE)")] = '\0';
                }

                if ((u2Index % 4) != 0)
                {
                    CliPrintf (CliHandle, ",%s%s", pu1Temp, au1State);
                }
                else
                {
                    /* For first interface New line is not required
                     */
                    if (u2Index == 0)
                    {

                        CliPrintf (CliHandle, "%s%s", pu1Temp, au1State);
                    }
                    else
                    {

                        CliPrintf (CliHandle,
                                   "\r\n%-34s%s%s", " ", pu1Temp, au1State);
                    }
                }

            }

            /* Provide a New line after Last port
             */
            CliPrintf (CliHandle, "\r\n");

        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaPort                                         */
/*                                                                           */
/*     DESCRIPTION      : Displays per port portchannel information          */
/*                                                                           */
/*     INPUT            : u4IfIndex -  specifies the portchannel index,      */
/*                        u1IsShowCommand - specifies the user input show    */
/*                                          command name                     */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ShowLaPort (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1IsShowCommand)
{
    tSNMP_OCTET_STRING_TYPE RetPortState;
    tSNMP_OCTET_STRING_TYPE RecSameState;
    UINT1               u1PortState;
    tLaLacPortState    *pPortOperState;
    UINT4               u4UpInBundleCount = 0;
    UINT4               u4DownInBundleCount = 0;
    UINT4               u4BundStChgTimeStamp = 0;
    UINT4               u4ErrStateDetTimeStamp = 0;
    UINT4               u4ErrStateRecTimeStamp = 0;
    UINT4               u4DefStateRecCount = 0;
    UINT4               u4SameStateRecCount = 0;
    UINT4               u4HwFailRecCount = 0;
    UINT4               u4DefStateRecTimeStamp = 0;
    UINT4               u4SameStateRecTimeStamp = 0;
    UINT4               u4HwFailRecTimeStamp = 0;
    INT4                i4RecTrigReason = 0;
    INT4                i4DownInBundleReason = 0;
    INT4                i4Key;
    INT4                i4BundleState;
    INT4                i4PortMode;
    INT4                i4AdminPort;
    INT4                i4PortPriority;
    INT4                i4WaitTime;
    INT4                i4AdminKey;
    INT4                i4OperKey;
    UINT2               u2Port;
    UINT2               u2NextPort;
    UINT2               u2Count;
    UINT2               u2Index;
    UINT2               au2Ports[LA_MAX_PORTS];
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1RecSameState[1];
    UINT1               u1IsShowPortChannel = LA_FALSE;
    UINT1               au1Date[LA_REQUIRED_BUF_SIZE];
    UINT1               u1State = 0;
    UINT1               u1RecSameState = 0;
    tLaLacPortEntry    *pPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    LA_MEMSET (&CfaIfInfo, LA_INIT_VAL, sizeof (tCfaIfInfo));
    LA_MEMSET (au1RecSameState, LA_INIT_VAL, sizeof (UINT1));
    RecSameState.pu1_OctetList = &au1RecSameState[0];

    if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

    RetPortState.pu1_OctetList = &u1PortState;

    u2Port = 0;
    u2NextPort = 0;

    LaUtilGetNextAggPort ((UINT2) u4IfIndex, u2Port, &u2NextPort);
    LA_MEMSET (au2Ports, 0, sizeof (au2Ports));

    /* Get all the Ports which are binded with the
     * particular port channel */

    u2Count = 0;

    while ((u2NextPort != 0) && (u2Count < LA_MAX_PORTS))
    {
        u2Port = u2NextPort;
        au2Ports[u2Count] = u2Port;
        u2Count++;

        LaUtilGetNextAggPort ((UINT2) u4IfIndex, u2Port, &u2NextPort);
    }

    /* If no ports are binded with portchannel then don't display
     * any port or group information for the "show etherchannel port"
     * CLI command */

    if ((u1IsShowCommand == LA_SHOW_PORT) && (u2Count == 0))
    {
        return CLI_SUCCESS;
    }

    /* Don't display Group Information for the "show interfaces 
     * etherchannel" CLI command */
    if ((u1IsShowCommand != LA_SHOW_INT_ETHERCHANNEL) && (u2Count != 0))
    {
        ShowLaGroupInfo (CliHandle, u4IfIndex, u1IsShowPortChannel);
    }
    /* display Group and Port channel Heading Info for the "show 
     * etherchannel detail" CLI command to display port channel details. 
     * This can be valid only when there is no port binded with the
     * particular port channel */
    else if ((u1IsShowCommand == LA_SHOW_DETAIL) && (u2Count == 0))
    {
        u1IsShowPortChannel = LA_TRUE;
        ShowLaGroupInfo (CliHandle, u4IfIndex, u1IsShowPortChannel);
    }

    for (u2Index = 0; ((u2Index < u2Count) &&
                       (u2Index < LA_MAX_PORTS)); u2Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        nmhGetFsLaPortBundleState (au2Ports[u2Index], &i4BundleState);
        nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key);
        nmhGetFsLaPortMode (au2Ports[u2Index], &i4PortMode);
        nmhGetDot3adAggPortActorPortPriority (au2Ports[u2Index],
                                              &i4PortPriority);
        nmhGetFsLaPortAggregateWaitTime (au2Ports[u2Index],
                                         (UINT4 *) &i4WaitTime);
        nmhGetFsLaPortActorAdminPort (au2Ports[u2Index], &i4AdminPort);

        nmhGetFsLaPortUpInBundleCount (au2Ports[u2Index], &u4UpInBundleCount);

        nmhGetFsLaPortDownInBundleCount (au2Ports[u2Index],
                                         &u4DownInBundleCount);

        nmhGetFsLaPortBundStChgTimeStamp (au2Ports[u2Index],
                                          &u4BundStChgTimeStamp);

        nmhGetFsLaPortDownInBundleReason (au2Ports[u2Index],
                                          &i4DownInBundleReason);

        nmhGetFsLaPortErrStateDetTimeStamp (au2Ports[u2Index],
                                            &u4ErrStateDetTimeStamp);

        nmhGetFsLaPortErrStateRecTimeStamp (au2Ports[u2Index],
                                            &u4ErrStateRecTimeStamp);

        nmhGetFsLaPortRecTrigReason (au2Ports[u2Index], &i4RecTrigReason);

        nmhGetFsLaPortDefStateRecCount (au2Ports[u2Index], &u4DefStateRecCount);

        nmhGetFsLaPortSameStateRecCount (au2Ports[u2Index],
                                         &u4SameStateRecCount);

        nmhGetFsLaPortHwFailRecCount (au2Ports[u2Index], &u4HwFailRecCount);

        nmhGetFsLaPortDefStateRecTimeStamp (au2Ports[u2Index],
                                            &u4DefStateRecTimeStamp);

        nmhGetFsLaPortSameStateRecTimeStamp (au2Ports[u2Index],
                                             &u4SameStateRecTimeStamp);

        nmhGetFsLaPortHwFailRecTimeStamp (au2Ports[u2Index],
                                          &u4HwFailRecTimeStamp);

        nmhGetFsLaPortRecState (au2Ports[u2Index], &RecSameState);

        LaGetPortEntry ((UINT2) au2Ports[u2Index], &pPortEntry);

        LaUtilGetPortState (au2Ports[u2Index], &pPortOperState, LA_CLI_ACTOR);
        if (pPortOperState == NULL)
        {
            continue;
        }
        LaCfaCliGetIfName ((UINT4) au2Ports[u2Index], (INT1 *) au1Temp);

        CliPrintf (CliHandle, "\r\nPort : ");
        CliPrintf (CliHandle, "%s\r\n", au1Temp);
        CliPrintf (CliHandle, "\r-------------\r\n");
        CliPrintf (CliHandle, "\r\n");

        if (i4BundleState == LA_PORT_UP_IN_BNDL)
        {
            CliPrintf (CliHandle, "\rPort State = Up in Bundle\r\n");
            /* Up in Bundle ports */
        }
        else if (i4BundleState == LA_PORT_STANDBY)
        {
            CliPrintf (CliHandle, "\rPort State = Hot standby\r\n");
            /* Stand by ports */
        }
        else if (i4BundleState == LA_PORT_DOWN)
        {
            CliPrintf (CliHandle, "\rPort State = Down, Not in Bundle\r\n");
            /* Down ports */

            if (pPortEntry != NULL)
            {
                if (pPortEntry->u1PortOperStatus == LA_OPER_DOWN)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Oper status of the port is down\r\n");
                }
                else if (i4DownInBundleReason == LA_MTU_MISMATCH)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Mismatch in MTU between physical port and port-channel\r\n");
                }
                else if (i4DownInBundleReason == LA_SPEED_MISMATCH)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Mismatch in speed between ports\r\n");
                }
                else if ((i4DownInBundleReason == LA_PARTNER_KEY_ZERO)
                         && (pPortEntry->bPartnerActive == LA_TRUE))
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Received partner key as zero\r\n");
                }
                else if (i4DownInBundleReason == LA_PAUSEMODE_MISMATCH)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Pause-admin mode mismatch between port and aggregator\r\n");
                }
                else if (i4DownInBundleReason == LA_LOOPBACK_CONNECTION)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Aggregation of ports that are in loopback connections in the same system\r\n");
                }
                else if (i4DownInBundleReason == LA_PARTNER_KEY_MISMATCH)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Partner key mismatch\r\n");
                }
                else if (i4DownInBundleReason == LA_PARTNER_ID_MISMATCH)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Partner id mismatch\r\n");
                }
                else if (i4DownInBundleReason == LA_HW_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Failure in Hardware programming\r\n");
                }
                else if (i4DownInBundleReason == LA_HOT_STANDBY_ERR)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : Port in Hot-Standby mode\r\n");
                }
                else if (i4DownInBundleReason == LA_NO_LACP_RCVD)
                {
                    CliPrintf (CliHandle,
                               "\rReason for port-down : No LACP packet received\r\n");
                }

            }
        }
        else if (i4BundleState == LA_PORT_UP_INDIVIDUAL)
        {
            CliPrintf (CliHandle, "\rPort State = Up, Independent \r\n");
            /* Up in Individual */
        }

        CliPrintf (CliHandle, "\rChannel Group : %d\r\n", i4Key);
        if (i4PortMode == LA_MODE_DISABLED)
        {
            CliPrintf (CliHandle, "\rMode : Disabled\r\n");
        }
        else if (i4PortMode == LA_MODE_MANUAL)
        {
            CliPrintf (CliHandle, "\rMode : On\r\n");
        }
        else if (i4PortMode == LA_MODE_LACP)
        {
            CliPrintf (CliHandle, "\rMode : Active\r\n");
        }

        /* Pseudo port-channel displays the configured port-channel information
           and the port-channel displays the active port-channel information
           (Operationally up in bundle ports) */

        if (i4BundleState == LA_PORT_UP_IN_BNDL)
        {
            CliPrintf (CliHandle, "\rPort-channel = Po%d\r\n", i4Key);
        }
        else
        {
            CliPrintf (CliHandle, "\rPort-channel = Null\r\n");
        }
        CliPrintf (CliHandle, "\rPseudo port-channel = Po%d\r\n", i4Key);
        CliPrintf (CliHandle, "\rPort Up-In-Bundle Count = %d\r\n",
                   u4UpInBundleCount);
        CliPrintf (CliHandle, "\rPort Down-In-Bundle Count = %d\r\n",
                   u4DownInBundleCount);

        LaUtilTicksToDate (u4BundStChgTimeStamp, au1Date);
        CliPrintf (CliHandle, "\rPort state change Timestamp = %s\r\n",
                   au1Date);

        CliPrintf (CliHandle, "\rLACP port-priority  = %d\r\n", i4PortPriority);
        CliPrintf (CliHandle, "\rLACP Wait-time  = %d secs \r\n",
                   (i4WaitTime / 100));
        CliPrintf (CliHandle, "\rLACP Port Identifier = %d \r\n", i4AdminPort);

        if (pPortOperState->LaLacpActivity == LA_ACTIVE)
        {
            CliPrintf (CliHandle, "\rLACP Activity : Active \r\n");
        }
        else if (pPortOperState->LaLacpActivity == LA_PASSIVE)
        {
            CliPrintf (CliHandle, "\rLACP Activity : Passive \r\n");

        }
        if (pPortOperState->LaLacpTimeout == LA_SHORT_TIMEOUT)
        {
            CliPrintf (CliHandle, "\rLACP Timeout : Short\r\n");
        }
        else if (pPortOperState->LaLacpTimeout == LA_LONG_TIMEOUT)
        {
            CliPrintf (CliHandle, "\rLACP Timeout : Long\r\n");
        }
        if (pPortEntry != NULL)
        {
            if (i4RecTrigReason == LA_REC_ERR_DEFAULTED)
            {
                if (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED)
                {
                    CliPrintf (CliHandle,
                               "\rLACP Error State : LACPDU not received from Peer node\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\rLACP Error State : LACP negotiating and DEFAULTED error set\r\n");
                }
                LaUtilTicksToDate (u4ErrStateDetTimeStamp, au1Date);
                CliPrintf (CliHandle, "\rError Detected Timestamp = %s\r\n",
                           au1Date);
                LaUtilTicksToDate (u4ErrStateRecTimeStamp, au1Date);
                CliPrintf (CliHandle, "\rError Recovery Timestamp = %s\r\n",
                           au1Date);
                CliPrintf (CliHandle, "\rPort Defaulted error Count = %d\r\n",
                           u4DefStateRecCount);
                LaUtilTicksToDate (u4DefStateRecTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "\rDefaulted state recovery Timestamp = %s\r\n",
                           au1Date);

            }
            else if (i4RecTrigReason == LA_REC_ERR_HW_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\rLACP Error State : LACP H/w Programming Failure \r\n");
                LaUtilTicksToDate (u4ErrStateDetTimeStamp, au1Date);
                CliPrintf (CliHandle, "\rError Detected Timestamp = %s\r\n",
                           au1Date);
                LaUtilTicksToDate (u4ErrStateRecTimeStamp, au1Date);
                CliPrintf (CliHandle, "\rError Recovery Timestamp = %s\r\n",
                           au1Date);
                CliPrintf (CliHandle, "\rPort H/w Failure Count = %d\r\n",
                           u4HwFailRecCount);
                LaUtilTicksToDate (u4HwFailRecTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "\rH/w Failure recovery Timestamp = %s\r\n",
                           au1Date);
            }
            else if (i4RecTrigReason == LA_REC_ERR_SAME_STATE)
            {
                LaGetPortState (&
                                (pPortEntry->LaLacActorAdminInfo.
                                 LaLacPortState), &u1State);
                LaGetSameState (u1State, &u1RecSameState);
                if (u1RecSameState == LA_SYNC_STATE)
                {
                    CliPrintf (CliHandle,
                               "\rLACP Error State : LACP Port Stuck in Sync State \r\n");
                }
                if (u1RecSameState == LA_COLLECTING_STATE)
                {
                    CliPrintf (CliHandle,
                               "\rLACP Error State : LACP Port Stuck in Collecting State \r\n");
                }
                if (u1RecSameState == LA_DEFAULTED_STATE)
                {
                    CliPrintf (CliHandle,
                               "\rLACP Error State : LACP Port Stuck in Defaulted State \r\n");

                }
                if (u1RecSameState == LA_AGGREGATION_STATE)
                {
                    CliPrintf (CliHandle,
                               "\rLACP Error State : LACP Port Stuck in Aggregation State \r\n");
                }
                LaUtilTicksToDate (u4ErrStateDetTimeStamp, au1Date);
                CliPrintf (CliHandle, "\rError Detected Timestamp = %s\r\n",
                           au1Date);
                LaUtilTicksToDate (u4ErrStateRecTimeStamp, au1Date);
                CliPrintf (CliHandle, "\rError Recovery Timestamp = %s\r\n",
                           au1Date);
                CliPrintf (CliHandle, "\rPort Same State Count = %d\r\n",
                           u4SameStateRecCount);
                LaUtilTicksToDate (u4SameStateRecTimeStamp, au1Date);
                CliPrintf (CliHandle,
                           "\rSame state recovery Timestamp = %s\r\n", au1Date);

            }
            else
            {
                CliPrintf (CliHandle, "\rLACP Error State : None \r\n");
            }
        }

        if (pPortOperState->LaAggregation == LA_TRUE)
        {
            CliPrintf (CliHandle, "\r\nAggregation State : Aggregation, ");
        }
        else if (pPortOperState->LaAggregation == LA_FALSE)
        {
            CliPrintf (CliHandle, "\r\nAggregation State : Individual, ");
        }

        if (pPortOperState->LaSynchronization == LA_TRUE)
        {
            CliPrintf (CliHandle, "Sync, ");
        }

        if (pPortOperState->LaCollecting == LA_TRUE)
        {
            CliPrintf (CliHandle, "Collecting, ");
        }

        if (pPortOperState->LaDistributing == LA_TRUE)
        {
            CliPrintf (CliHandle, "Distributing, ");
        }

        if (pPortOperState->LaDefaulted == LA_TRUE)
        {
            CliPrintf (CliHandle, "Defaulted ");
        }

        if (pPortOperState->LaExpired == LA_TRUE)
        {
            CliPrintf (CliHandle, "Expired ");
        }

        u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }

    for (u2Index = 0; u2Index < u2Count; u2Index++)
    {

        nmhGetFsLaPortBundleState (au2Ports[u2Index], &i4BundleState);
        nmhGetDot3adAggPortActorPortPriority (au2Ports[u2Index],
                                              &i4PortPriority);
        nmhGetDot3adAggPortActorAdminKey (au2Ports[u2Index], &i4AdminKey);
        nmhGetDot3adAggPortActorOperKey (au2Ports[u2Index], &i4OperKey);
        nmhGetDot3adAggPortActorOperState (au2Ports[u2Index], &RetPortState);
        LaCfaCliGetIfName ((UINT4) au2Ports[u2Index], (INT1 *) au1Temp);

        if (u2Index == 0)
        {
            /* display LACP information for the port */
            CliPrintf (CliHandle, "\r\n%26s%8s%6s%7s%8s\r\n",
                       "LACP Port", "Admin", "Oper", "Port", "Port");
            CliPrintf (CliHandle, "%-9s%-8s%-12s%-7s%-7s%-8s%-8s",
                       "Port", "State", "Priority", "Key", "Key", "Number",
                       "State");
            CliPrintf (CliHandle,
                       "\r\n------------------------------------------"
                       "---------------------------\r\n");
        }

        CliPrintf (CliHandle, "%-9s", au1Temp);
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        if (i4BundleState == LA_PORT_UP_IN_BNDL)
        {
            CliPrintf (CliHandle, "%-8s", "Bundle");
            /* Up in Bundle ports */
        }
        if (i4BundleState == LA_PORT_STANDBY)
        {
            CliPrintf (CliHandle, "%-8s", "Standby");
            /* Stand by ports */
        }
        if (i4BundleState == LA_PORT_DOWN)
        {
            CliPrintf (CliHandle, "%-8s", "Down");
            /* Down ports */
        }
        if (i4BundleState == LA_PORT_UP_INDIVIDUAL)
        {
            CliPrintf (CliHandle, "%-8s", "Indep");
            /* Up in Individual */
        }

        CliPrintf (CliHandle, "%-12d", i4PortPriority);
        CliPrintf (CliHandle, "%-7d", i4AdminKey);
        CliPrintf (CliHandle, "%-7d", i4OperKey);
        CliPrintf (CliHandle, "0x%-6x", au2Ports[u2Index]);

        u1Quit =
            (UINT1) CliPrintf (CliHandle, "0x%-6x\r\n",
                               RetPortState.pu1_OctetList[0]);

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }

    }
    return (u1Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaShowLacpCounters                                 */
/*                                                                           */

/*     DESCRIPTION      : Displays per portchannel LACP counter information  */
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaShowLacpCounters (tCliHandle CliHandle, INT4 i4Key)
{
    UINT1               u1flag = 1;
    tLaLacAggEntry     *pLaLacAggEntry;
    INT4                i4PrevIndex;
    UINT1               u1Quit = CLI_SUCCESS;
    UINT2               u2Port = 0;
    UINT2               u2NextPort = 0;
    UINT2               u2Count;
    UINT2               au2Ports[LA_MAX_PORTS];
    UINT4               u4ErrDetected = 0;
    UINT4               u4RecTrgd = 0;
    INT4                i4IllegalRx;
    INT4                i4UnknownRx;
    INT4                i4LacpTx;
    INT4                i4RespPDUTx;
    INT4                i4RespPDURx;
    INT4                i4LacpRx;
    INT4                i4MarkPDURx;
    INT4                i4MarkPDUTx;
    UINT2               u2Index;
    UINT1              *pifName = NULL;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY];
    INT4                i4Index;

    pifName = &au1Temp[0];
    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);

    CliPrintf (CliHandle, "\r\n%15s%17s%20s%11s%20s",
               "LACPDUs", "Marker", "Marker Response", "LACPDUs",
               "Error States");

    CliPrintf (CliHandle, "\r\n%-7s%-6s%-10s%-6s%-10s%-7s%-10s%-15s%-10s\r\n",
               "Port", "Sent", "Recv", "Sent", "Recv", "Sent", "Recv",
               "Pkts Err", "Detd Trgd");

    CliPrintf (CliHandle,
               "\r-----------------------------------------------------------"
               "------------------------\r\n");

    if (i4Key == 0)
    {
        /* No Port Channel index is given by user. So,we get the 
         * first interface from the table. */
        u1flag = 0;
        /* This flag is used to indicate that all details of all
         * the existing port-channels must be displayed */

        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        /* Key is given by user. Obtain the interface index based on key */
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            return (CLI_SUCCESS);
        }

        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }

    do
    {
        nmhGetDot3adAggActorAdminKey (i4Index, &i4Key);
        u2Port = 0;
        u2NextPort = 0;
        LA_MEMSET (au2Ports, 0, sizeof (au2Ports));

        if (LaUtilGetNextAggPort ((UINT2) i4Index, u2Port, &u2NextPort)
            == LA_FAILURE)
        {
            /* don't display any lacp information when there is no Port
             * binded with Port-Channel */
            if (i4Index != 0)
            {
                CliPrintf (CliHandle, "\rChannel group: %d\r\n", i4Key);
                CliPrintf (CliHandle, "\r------------------\r\n");
                CliPrintf (CliHandle, "\rNo interfaces aggregated in "
                           "the channel group\r\n");
            }
        }
        else
        {
            /* If Key is given by user. display the following Information */
            u2Count = 0;

            while ((u2NextPort != 0) && (u2Count < LA_MAX_PORTS))
            {
                u2Port = u2NextPort;
                au2Ports[u2Count] = u2Port;
                u2Count++;

                LaUtilGetNextAggPort ((UINT2) i4Index, u2Port, &u2NextPort);
            }

            for (u2Index = 0; ((u2Index < u2Count) &&
                               (u2Index < LA_MAX_PORTS)); u2Index++)
            {
                MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                LaCfaCliGetIfName ((UINT4) au2Ports[u2Index], (INT1 *) pifName);
                /* LACPDUs */
                nmhGetDot3adAggPortStatsLACPDUsTx ((INT4) au2Ports[u2Index],
                                                   (UINT4 *) &i4LacpTx);
                nmhGetDot3adAggPortStatsLACPDUsRx ((INT4) au2Ports[u2Index],
                                                   (UINT4 *) &i4LacpRx);
                /* MarkerPDUs */
                nmhGetDot3adAggPortStatsMarkerPDUsTx ((INT4) au2Ports[u2Index],
                                                      (UINT4 *) &i4MarkPDUTx);
                nmhGetDot3adAggPortStatsMarkerPDUsRx ((INT4) au2Ports[u2Index],
                                                      (UINT4 *) &i4MarkPDURx);
                /* Marker Response PDUs */
                nmhGetDot3adAggPortStatsMarkerResponsePDUsRx ((INT4)
                                                              au2Ports[u2Index],
                                                              (UINT4 *)
                                                              &i4RespPDURx);
                nmhGetDot3adAggPortStatsMarkerResponsePDUsTx ((INT4)
                                                              au2Ports[u2Index],
                                                              (UINT4 *)
                                                              &i4RespPDUTx);

                /* Errors */
                nmhGetDot3adAggPortStatsUnknownRx ((INT4) au2Ports[u2Index],
                                                   (UINT4 *) &i4UnknownRx);
                nmhGetDot3adAggPortStatsIllegalRx ((INT4) au2Ports[u2Index],
                                                   (UINT4 *) &i4IllegalRx);
                nmhGetFsLaPortErrStateDetCount ((INT4) au2Ports[u2Index],
                                                &u4ErrDetected);

                nmhGetFsLaPortErrStateRecCount ((INT4) au2Ports[u2Index],
                                                &u4RecTrgd);

                if (u2Index == 0)
                {
                    CliPrintf (CliHandle, "\rChannel group: %d\r\n", i4Key);
                    CliPrintf (CliHandle, "\r------------------\r\n");
                }

                CliPrintf (CliHandle, "%-7s  ", pifName);

                CliPrintf (CliHandle, "%-6d", i4LacpTx);
                CliPrintf (CliHandle, "%-10d", i4LacpRx);
                CliPrintf (CliHandle, "%-6d", i4MarkPDUTx);
                CliPrintf (CliHandle, "%-10d", i4MarkPDURx);
                CliPrintf (CliHandle, "%-6d", i4RespPDURx);
                CliPrintf (CliHandle, "%-10d", i4RespPDUTx);
                CliPrintf (CliHandle, "%-6d", i4UnknownRx);
                CliPrintf (CliHandle, "%-10d", i4IllegalRx);
                CliPrintf (CliHandle, "%-6d", u4ErrDetected);
                CliPrintf (CliHandle, "%-10d", u4RecTrgd);

                CliPrintf (CliHandle, "\r\n");
            }

            u1Quit = (UINT1) CliPrintf (CliHandle, "\r");

            if (u1Quit == CLI_FAILURE)
            {
                break;
            }
        }
        i4PrevIndex = i4Index;
    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS) && (u1flag == 0));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaShowLacpNeighbor                                 */
/*                                                                           */
/*     DESCRIPTION      : Displays per portchannel LACP neighbor information */
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        u1IsDetail - specifies the "Detail" option in the  */
/*                                     Lacp neighbor show command            */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
LaShowLacpNeighbor (tCliHandle CliHandle, INT4 i4Key, UINT1 u1IsDetail)
{
    UINT1               u1flag = 1;
    tLaLacAggEntry     *pLaLacAggEntry;
    UINT1               u1Quit = CLI_SUCCESS;
    INT4                i4PrevIndex;
    INT4                i4OperKey;
    UINT1               u1RetOperKey;
    UINT1               u1RetPortState;
    UINT1               u1RetAddr;
    UINT1               u1RetPrio;
    UINT1               u1PortState;
    INT4                i4PortPriority;
    UINT2               u2Port = 0;
    UINT2               u2NextPort = 0;
    UINT2               u2Count;
    UINT2               au2Ports[LA_MAX_PORTS];
    UINT2               u2Index;
    UINT1              *pifName = NULL;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY];
    INT4                i4Index;
    tLaMacAddr          Address;
    UINT1               au1String[20];
    tSNMP_OCTET_STRING_TYPE RetPortState;
    tLaLacPortState    *pPortOperState;

    pifName = &au1Temp[0];
    RetPortState.pu1_OctetList = &u1PortState;

    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
    MEMSET (au1String, 0, 20);

    CliPrintf (CliHandle,
               "\r\nFlags: \r\n"
               "A - Device is in Active mode \r\n"
               "P - Device is in Passive mode \r\n");

    if (i4Key == 0)
    {
        /* No Port Channel index is given by user. We get the first interface
         * from the table.
         */
        u1flag = 0;
        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        /* Key is given by user. Obtain the interface index based on key */
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            return (CLI_SUCCESS);
        }
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }
    /* don't display any lacp information when there is no Port
     * binded with any Port-Channel */
    if (LaUtilGetNextAggPort ((UINT2) i4Index, u2Port, &u2NextPort)
        == LA_FAILURE)
    {
        /* If Key is given by user. display the following Information */
        if (i4Key != 0)
        {

            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        u2Port = 0;
        u2NextPort = 0;
        nmhGetDot3adAggActorAdminKey (i4Index, &i4Key);

        LaUtilGetNextAggPort ((UINT2) i4Index, u2Port, &u2NextPort);

        LA_MEMSET (au2Ports, 0, sizeof (au2Ports));
        u2Count = 0;

        while ((u2NextPort != 0) && (u2Count < LA_MAX_PORTS))
        {
            u2Port = u2NextPort;
            au2Ports[u2Count] = u2Port;
            u2Count++;
            LaUtilGetNextAggPort ((UINT2) i4Index, u2Port, &u2NextPort);
        }

        for (u2Index = 0; ((u2Index < LA_MAX_PORTS)
                           && (u2Index < u2Count)); u2Index++)
        {
            MEMSET (au1String, 0, 20);
            MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);

            LaCfaCliGetIfName ((UINT4) au2Ports[u2Index], (INT1 *) pifName);

            if (LaIsPortPartnerPresent (au2Ports[u2Index]) == LA_TRUE)
            {
                u1RetAddr
                    =
                    nmhGetDot3adAggPortPartnerOperSystemID ((INT4)
                                                            au2Ports[u2Index],
                                                            &Address);

                LaUtilGetPortState (au2Ports[u2Index], &pPortOperState,
                                    LA_CLI_PARTNER);
                if (pPortOperState == NULL)
                {
                    continue;
                }

                u1RetPrio = nmhGetDot3adAggPortPartnerOperPortPriority
                    ((INT4) au2Ports[u2Index], &i4PortPriority);

                u1RetOperKey = nmhGetDot3adAggPortPartnerOperKey
                    ((INT4) au2Ports[u2Index], &i4OperKey);

                u1RetPortState = nmhGetDot3adAggPortPartnerOperState
                    ((INT4) au2Ports[u2Index], &RetPortState);

                if (u2Index == 0)
                {
                    CliPrintf (CliHandle,
                               "\n\rChannel group %d neighbors \r\n", i4Key);

                }
                CliPrintf (CliHandle, "\n\rPort %-7s\r\n", pifName);
                CliPrintf (CliHandle, "----------\r\n");

                if (u1RetAddr != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%-32s: "
                               "UNKNOWN\r\n", "Partner System ID");
                }
                else
                {
                    CLI_CONVERT_MAC_TO_DOT_STR ((UINT1 *) &Address, au1String);
                    CliPrintf (CliHandle, "%-32s: %s\r\n",
                               "Partner System ID", au1String);
                }

                CliPrintf (CliHandle, "\r%-32s:", "Flags");

                if (pPortOperState->LaLacpActivity == LA_ACTIVE)
                {
                    CliPrintf (CliHandle, " A\r\n");
                }
                else if (pPortOperState->LaLacpActivity == LA_PASSIVE)
                {
                    CliPrintf (CliHandle, " P\r\n");
                }

                if (u1RetPrio != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-32s: UNKNOWN\r\n",
                               "LACP Partner Port Priority");
                }
                else
                {
                    CliPrintf (CliHandle, "%-32s: %d\r\n",
                               "LACP Partner Port Priority", i4PortPriority);
                }

                if (u1RetOperKey != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-32s: UNKNOWN\r\n",
                               "LACP Partner Oper Key");
                }
                else
                {
                    CliPrintf (CliHandle, "%-32s: %d\r\n",
                               "LACP Partner Oper Key", i4OperKey);
                }

                if (u1RetPortState != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-32s: UNKNOWN\r\n",
                               "LACP Partner Port State");
                }
                else
                {
                    CliPrintf (CliHandle, "%-32s: 0x%x\r\n",
                               "LACP Partner Port State",
                               RetPortState.pu1_OctetList[0]);
                }

                u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");

                if (u1IsDetail == 1)
                {
                    CliPrintf (CliHandle, "\rPort State Flags Decode\r\n");
                    CliPrintf (CliHandle, "\r------------------------\r\n");

                    if (pPortOperState->LaLacpActivity == LA_ACTIVE)
                    {
                        CliPrintf (CliHandle, "LACP Activity : Active\r\n");
                    }
                    else if (pPortOperState->LaLacpActivity == LA_PASSIVE)
                    {
                        CliPrintf (CliHandle, "LACP Activity : Passive\r\n");
                    }
                    if (pPortOperState->LaLacpTimeout == LA_SHORT_TIMEOUT)
                    {
                        CliPrintf (CliHandle, "LACP Timeout : Short\r\n");
                    }
                    else if (pPortOperState->LaLacpTimeout == LA_LONG_TIMEOUT)
                    {
                        CliPrintf (CliHandle, "LACP Timeout : Long\r\n");
                    }

                    if (pPortOperState->LaAggregation == LA_TRUE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAggregation State : Aggregation, ");
                    }
                    else if (pPortOperState->LaAggregation == LA_FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAggregation State : Individual, ");
                    }

                    if (pPortOperState->LaSynchronization == LA_TRUE)
                    {
                        CliPrintf (CliHandle, "Sync, ");
                    }

                    if (pPortOperState->LaCollecting == LA_TRUE)
                    {
                        CliPrintf (CliHandle, "Collecting, ");
                    }

                    if (pPortOperState->LaDistributing == LA_TRUE)
                    {
                        CliPrintf (CliHandle, "Distributing, ");
                    }

                    if (pPortOperState->LaDefaulted == LA_TRUE)
                    {
                        CliPrintf (CliHandle, "Defaulted ");
                    }

                    if (pPortOperState->LaExpired == LA_TRUE)
                    {
                        CliPrintf (CliHandle, "Expired ");
                    }

                    u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");
                }

            }
            else
            {

                if (u2Index == 0)
                {
                    CliPrintf (CliHandle,
                               "\n\rChannel group %d neighbors \r\n", i4Key);

                }
                CliPrintf (CliHandle, "\n\rPort %-7s\r\n", pifName);
                CliPrintf (CliHandle, "----------\r\n");

                CliPrintf (CliHandle,
                           "\rPartner Information is not known \r\n");
            }

            if (u1Quit == CLI_FAILURE)
            {
                return (CLI_SUCCESS);
            }
        }

        i4PrevIndex = i4Index;
    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS) && (u1flag == 0));

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

#ifdef ICCH_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGShowCounters                                */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Displays per portchannel MC-LAG counter information*/
/*                                                                           */
/*     INPUT            : i4Key -  specifies the portchannel index,          */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaMCLAGShowCounters (tCliHandle CliHandle, INT4 i4Key)
{
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    UINT4               u4TxCount = 0;
    UINT4               u4RxCount = 0;
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1               u1flag = 1;

    if (i4Key == 0)
    {
        /* No Port Channel index is given by user. So,we get the
         * first interface from the table. */
        u1flag = 0;
        /* This flag is used to indicate that all details of all
         * the existing port-channels must be displayed */

        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        /* Get the Aggregator entry */
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            return (CLI_SUCCESS);
        }
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }
    do
    {
        LaGetAggEntry ((UINT2) i4Index, &pLaLacAggEntry);
        if (pLaLacAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
        {
            nmhGetDot3adAggActorAdminKey (i4Index, &i4Key);
            CliPrintf (CliHandle,
                       "\n\rMC-LAG Statistics : Channel Group %d\r\n", i4Key);

            CliPrintf (CliHandle, "\r%s\r\n",
                       "--------------------------------------------\r\n");

            CliPrintf (CliHandle, "\rMC-LAG ICCL Ports         ");

            nmhGetFsLaPortChannelMCLAGPeriodicSyncPduTxCount (i4Index,
                                                              &u4TxCount);

            nmhGetFsLaPortChannelMCLAGPeriodicSyncPduRxCount (i4Index,
                                                              &u4RxCount);
            CliPrintf (CliHandle,
                       "\rPeriodic Sync PDU Tx Count       :  %d\r\n",
                       u4TxCount);
            CliPrintf (CliHandle,
                       "\rPeriodic Sync PDU Rx Count       :  %d\r\n",
                       u4RxCount);

            nmhGetFsLaPortChannelMCLAGEventUpdatePduTxCount (i4Index,
                                                             &u4TxCount);

            nmhGetFsLaPortChannelMCLAGEventUpdatePduRxCount (i4Index,
                                                             &u4RxCount);
            CliPrintf (CliHandle,
                       "\rEvent Update PDU Tx Count        :  %d\r\n",
                       u4TxCount);
            CliPrintf (CliHandle,
                       "\rEvent Update PDU Rx Count        :  %d\r\n",
                       u4RxCount);

            CliPrintf (CliHandle, "\n");
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r");
            if (u1Quit == CLI_FAILURE)
            {
                break;
            }
        }
        i4PrevIndex = i4Index;
    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS) && (u1flag == 0));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGShowDetail                                  */
/*                                                                           */
/*     DESCRIPTION      : Displays MCLAG counter information per portchannel */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4Key -  specifies the portchannel index,          */
/*                        u1IsConsolidated - specifies consolidated or       */
/*                                           detailed info.                  */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaMCLAGShowDetail (tCliHandle CliHandle, INT4 i4Key, UINT1 u1IsConsolidated)
{
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tMacAddr            BaseMac;
    tMacAddr            PrevBaseMac;
    tMacAddr            RemoteBaseMac;
    UINT4               u4Value = 0;
    UINT4               u4CfaRemotePortIndex = 0;
    UINT4               u4Count = 0;
    UINT4               u4PortIndex = 0;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4MCLAGStatus = 0;
    INT4                i4PortChannelIndex = 0;
    INT4                i4RemotePortChannelIndex = 0;
    INT4                i4RemotePortIndex = 0;
    INT4                i4PrevRemotePortIndex = 0;
    INT4                i4RemotePortBundleState = 0;
    INT4                i4RemotePortSyncStatus = 0;
    INT4                i4RemotePortSlotIndex = 0;
    INT4                i4RemotePortPriority = 0;
    INT4                i4MCLAGSystemPriority = 0;
    INT4                i4Value = 0;
    INT1                i1RetValue = 0;
    INT4                i4RetValue = 0;
    UINT4               u4DefPort = 1;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY] = { 0 };
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1              *pifName = NULL;
    UINT1              *pu1Tmp = NULL;
    UINT1               u1flag = 1;
    UINT1               u1IfType = 0;
    UINT1               u1RemoteFlag = OSIX_FALSE;

    nmhGetFsLaMCLAGSystemStatus (&i4MCLAGStatus);
    i1RetValue = nmhGetFsLaMCLAGSystemID ((tMacAddr *) & BaseMac);

    CliPrintf (CliHandle, "\n\rMC-LAG Configuration for the System:\r\n");
    CliPrintf (CliHandle, "\r%s\r\n",
               "---------------------------------------------------");
    /* Obtain all the MC-LAG parameters */
    if (i4MCLAGStatus == LA_MCLAG_ENABLED)
    {
        CliPrintf (CliHandle,
                   "\rMC-LAG status                           : Enabled\r\n");
    }
    else if (i4MCLAGStatus == LA_MCLAG_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\rMC-LAG status                           : Disabled\r\n");
    }

    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
    pu1Tmp = &au1Temp[0];
    CliPrintf (CliHandle, "\rMC-LAG system MAC                       : ");
    PrintMacAddress (BaseMac, pu1Tmp);
    CliPrintf (CliHandle, "%s\r\n", pu1Tmp);

    nmhGetFsLaMCLAGSystemPriority (&i4Value);

    CliPrintf (CliHandle,
               "\rMC-LAG system priority                  : %d\r\n", i4Value);

    nmhGetFsLaMCLAGPeriodicSyncTime (&u4Value);

    CliPrintf (CliHandle,
               "\rMC-LAG periodic sync time               : %d seconds\r\n",
               u4Value);

    CliPrintf (CliHandle,
               "\rMaximum keep alive count                : %d\r\n",
               LA_DLAG_MAX_KEEP_ALIVE_COUNT);

    CliPrintf (CliHandle, "\n");

    if (i4Key == 0)
    {
        /* No Port Channel index is given by user. So,we get the
         * first interface from the table. */
        u1flag = 0;
        /* This flag is used to indicate that all details of all
         * the existing port-channels must be displayed */

        if (nmhGetFirstIndexDot3adAggTable (&i4Index) != SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        /* Get the Aggregator entry */
        if (LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry) != LA_SUCCESS)
        {
            CliPrintf (CliHandle, "\rNo interfaces configured in "
                       "the channel group\r\n");
            return (CLI_SUCCESS);
        }
        i4Index = (INT4) pLaLacAggEntry->u2AggIndex;
    }

    if ((u1IsConsolidated == LA_TRUE) &&
        (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_SLAVE)
        && (i4MCLAGStatus == LA_MCLAG_ENABLED))
    {
        CliPrintf (CliHandle, "\r\n%s",
                   "Consolidated Info present only in the Master Node\r\n");
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsLaPortChannelMCLAGStatus (i4Index, &i4MCLAGStatus);
        if (i4MCLAGStatus != LA_MCLAG_ENABLED)
        {
            i4PrevIndex = i4Index;
            continue;
        }

        u1RemoteFlag = OSIX_FALSE;
        nmhGetDot3adAggActorAdminKey (i4Index, &i4Key);
        MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
        pifName = &au1Temp[0];

        MEMSET (BaseMac, 0, LA_MAC_ADDRESS_SIZE);
        MEMSET (RemoteBaseMac, 0, LA_MAC_ADDRESS_SIZE);
        MEMSET (PrevBaseMac, 0, LA_MAC_ADDRESS_SIZE);

        /* Obtain all the MC-LAG parameters */
        nmhGetFsLaPortChannelMCLAGSystemID (i4Index, (tMacAddr *) & BaseMac);
        nmhGetFsLaPortChannelMCLAGSystemPriority (i4Index,
                                                  &i4MCLAGSystemPriority);
        nmhGetFsLaPortChannelMCLAGStatus (i4Index, &i4MCLAGStatus);

        CliPrintf (CliHandle,
                   "\n\rMC-LAG Configuration : Channel Group %d\r\n", i4Key);

        CliPrintf (CliHandle, "\r%s\r\n",
                   "-------------------------------------------------------");

        if (i4MCLAGStatus == LA_MCLAG_ENABLED)
        {
            CliPrintf (CliHandle,
                       "\rMC-LAG status                           : Enabled\r\n");
        }
        else if (i4MCLAGStatus == LA_MCLAG_DISABLED)
        {
            CliPrintf (CliHandle,
                       "\rMC-LAG status                           : Disabled\r\n");
        }

        CliPrintf (CliHandle, "\rsystem MAC for port channel             : ");
        PrintMacAddress (BaseMac, pu1Tmp);
        CliPrintf (CliHandle, "%s\r\n", pu1Tmp);

        CliPrintf (CliHandle,
                   "\rsystem priority for port channel        : %d\r\n",
                   i4MCLAGSystemPriority);

        i4RetValue = LaGetAggEntryByKey ((UINT2) i4Key, &pLaLacAggEntry);

        if (pLaLacAggEntry != NULL)
        {
            CliPrintf (CliHandle,
                       "\rMC-LAG Maximum number of Ports allowed  : %d\r\n",
                       pLaLacAggEntry->u1MaxPortsToAttach);
        }

        if (u1IsConsolidated == LA_TRUE)
        {
            pConsPortEntry = NULL;

            if (pLaLacAggEntry == NULL)
            {
                CliPrintf (CliHandle, "\r\n");
                i4PrevIndex = i4Index;
                continue;
            }

            CliPrintf (CliHandle, "\r\n%s", "Consolidated Ports Info:     ");
            CliPrintf (CliHandle, "\r\n%s\r\n", "----------------------------");

            RBTreeCount (pLaLacAggEntry->LaDLAGConsInfoTable.LaDLAGPortList,
                         &u4Count);

            if (u4Count == 0)
            {
                CliPrintf (CliHandle, "\r\n");
                i4PrevIndex = i4Index;
                continue;
            }

            /* display LACP information for the port */
            CliPrintf (CliHandle, "\r\n%26s%8s\r\n", "LACP Port", "Port");
            CliPrintf (CliHandle, "%-9s%-8s%-13s%-9s",
                       "Port", "State", "Priority", "Property");
            CliPrintf (CliHandle,
                       "\r\n------------------------------------------");

            LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry,
                                          pLaLacAggEntry);

            while (pConsPortEntry != NULL)
            {
                /* If MSB of consolidated port entry is non zero, the entry belongs
                 * to remote node */
                if (LA_MSB_BIT_SET ==
                    ((pConsPortEntry->u4PortIndex) & LA_MSB_BIT_SET))
                {
                    u1RemoteFlag = OSIX_TRUE;

                    u4PortIndex = pConsPortEntry->u4PortIndex;
                    u4PortIndex = u4PortIndex & LA_PORT_ID_MASK;

                    /* Incase the local port with respect to the remote
                     * port is not present in the system we are 
                     * displaying the port information using the port-type
                     * of the first port present in the system*/

                    LaCfaCliGetIfName (u4DefPort, (INT1 *) pifName);
                    SPRINTF ((CHR1 *) pifName +
                             (LA_MC_LAGG_PORT_NUM_OFFSET * sizeof (UINT1)),
                             "%d   ", u4PortIndex);

                    /* GetIfIndex from u4PortIndex (for slave node ) */
                    /* GetIfName from IfIndex (for slave node ) */
                }
                else
                {
                    LaActiveDLAGGetIfIndexFromBridgeIndex (pConsPortEntry->
                                                           u4PortIndex,
                                                           &u4CfaRemotePortIndex);
                    LaCfaCliGetIfName (u4CfaRemotePortIndex, (INT1 *) pifName);
                }
                CliPrintf (CliHandle, "\r\n%-9s", pifName);

                switch (pConsPortEntry->u1PortStateFlag)
                {
                    case LA_AA_DLAG_PORT_IN_ADD:
                        CliPrintf (CliHandle, "%-8s", "Bundle");
                        break;
                    case LA_AA_DLAG_PORT_IN_DELETE:
                        CliPrintf (CliHandle, "%-8s", "Down");
                        break;
                    case LA_AA_DLAG_PORT_IN_STANDBY:
                        CliPrintf (CliHandle, "%-8s", "Standby");
                        break;
                    case LA_AA_DLAG_PORT_NOT_IN_ANY_LIST:
                        CliPrintf (CliHandle, "%-8s", "NotInAny");
                        break;
                    case LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY:
                        CliPrintf (CliHandle, "%-8s", "Removed");
                        break;
                    default:
                        break;
                }
                CliPrintf (CliHandle, "%-13d", pConsPortEntry->u2Priority);
                if (u1RemoteFlag == OSIX_TRUE)
                {
                    CliPrintf (CliHandle, "%-9s", "Remote");
                }
                else
                {
                    CliPrintf (CliHandle, "%-9s", "Local");
                }

                LaActiveDLAGGetNextPortEntry (pConsPortEntry,
                                              &pConsPortEntry, pLaLacAggEntry);
            }

            CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            /* Remote Port Channel entry */
            MEMSET (PrevBaseMac, 0, LA_MAC_ADDRESS_SIZE);
            CliPrintf (CliHandle, "\r\n%s",
                       "MC-LAG Neighbor Nodes Info :     ");
            CliPrintf (CliHandle, "\r\n%s\r\n", "----------------------------");
            if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
            {
                while (nmhGetNextIndexFsLaMCLAGRemotePortChannelTable
                       (i4Index, &i4PortChannelIndex,
                        PrevBaseMac, &BaseMac) == SNMP_SUCCESS)

                {
                    if (i4Index != i4PortChannelIndex)
                        break;

                    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                    pu1Tmp = &au1Temp[0];

                    CliPrintf (CliHandle, "\r\nSystem MAC                : ");

                    PrintMacAddress (BaseMac, pu1Tmp);
                    CliPrintf (CliHandle, "%s \r\n", pu1Tmp);

                    nmhGetFsLaMCLAGRemotePortChannelSystemPriority
                        (i4PortChannelIndex, BaseMac, &i4Value);
                    CliPrintf (CliHandle,
                               "\rSystem priority           : %d\r\n", i4Value);
                    CliPrintf (CliHandle,
                               "\rChannel Group             : %d\r\n", i4Key);

                    nmhGetFsLaMCLAGRemotePortChannelRolePlayed
                        (i4PortChannelIndex, BaseMac, &i4Value);
                    if (LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED)
                    {
                        i4Value = LA_DLAG_SYSTEM_ROLE_SLAVE;
                    }

                    switch (i4Value)
                    {
                        case LA_DLAG_SYSTEM_ROLE_MASTER:
                            CliPrintf (CliHandle,
                                       "\rRole Played               : Master\r\n");
                            break;

                        case LA_DLAG_SYSTEM_ROLE_SLAVE:
                            CliPrintf (CliHandle,
                                       "\rRole Played               : Slave\r\n");
                            break;

                        case LA_DLAG_SYSTEM_ROLE_NONE:
                            CliPrintf (CliHandle,
                                       "\rRole Played               : None\r\n");
                            break;

                        default:
                            break;
                    }

                    nmhGetFsLaMCLAGRemotePortChannelKeepAliveCount
                        (i4PortChannelIndex, BaseMac, &i4Value);

                    CliPrintf (CliHandle,
                               "\rCurrent Keep Alive Count  : %d\r\n", i4Value);

                    nmhGetFsLaMCLAGRemotePortChannelSpeed (i4PortChannelIndex,
                                                           BaseMac, &u4Value);
                    if (u4Value >= CFA_ENET_MAX_SPEED_VALUE)
                    {

                        nmhGetFsLaMCLAGRemotePortChannelHighSpeed
                            (i4PortChannelIndex, BaseMac, &u4Value);
                        if (u4Value == CFA_ENET_HISPEED_10G)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 10 Gbps\r\n");
                        }
                        else if (u4Value == CFA_ENET_HISPEED_40G)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 40 Gbps\r\n");
                        }

                        else if (u4Value == CFA_ENET_HISPEED_56G)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 56 Gbps\r\n");
                        }

                    }
                    else
                    {
                        if (u4Value == CFA_ENET_SPEED_10M)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 10 Mbps\r\n");
                        }
                        else if (u4Value == CFA_ENET_SPEED_1G)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 1 Gbps\r\n");
                        }
                        else if (u4Value == CFA_ENET_SPEED_2500M)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 2.5 Gbps\r\n");
                        }
                        else if (u4Value == CFA_ENET_SPEED_100M)
                        {
                            CliPrintf (CliHandle,
                                       "\rSpeed                     : 100 Mbps\r\n");
                        }
                    }

                    nmhGetFsLaMCLAGRemotePortChannelMtu (i4PortChannelIndex,
                                                         BaseMac, &i4Value);
                    CliPrintf (CliHandle,
                               "\rMTU                       : %d\r\n", i4Value);
                    CliPrintf (CliHandle, "\n");

                    CliPrintf (CliHandle, "\r%s\r\n", "Remote Ports Info :");
                    MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                    pifName = &au1Temp[0];

                    i4PrevRemotePortIndex = 0;
                    while (nmhGetNextIndexFsLaMCLAGRemotePortTable
                           (i4PortChannelIndex, &i4RemotePortChannelIndex,
                            BaseMac, &RemoteBaseMac,
                            i4PrevRemotePortIndex,
                            &i4RemotePortIndex) == SNMP_SUCCESS)
                    {
                        if ((i4PortChannelIndex != i4RemotePortChannelIndex) ||
                            (MEMCMP
                             (BaseMac, RemoteBaseMac,
                              LA_MAC_ADDRESS_SIZE) != 0))
                            break;

                        MEMSET (au1Temp, 0, MAX_LEN_TEMP_ARRAY);
                        u4PortIndex = (UINT4) i4RemotePortIndex;

                        /* Masking done to fetch remote port index */
                        u4PortIndex = u4PortIndex & LA_PORT_ID_MASK;
                        CfaGetIfType (u4PortIndex, &u1IfType);
                        /* Incase the local port with respect to the remote
                         * port is not present in the system we are 
                         * displaying the port information using the port-type
                         * of the first port present in the system*/
                        LaCfaCliGetIfName (u4DefPort, (INT1 *) pifName);
                        SPRINTF ((CHR1 *) pifName +
                                 (LA_MC_LAGG_PORT_NUM_OFFSET * sizeof (UINT1)),
                                 "%d   ", u4PortIndex);

                        i4RemotePortBundleState = i4RemotePortSyncStatus = 0;
                        nmhGetFsLaMCLAGRemotePortBundleState
                            (i4RemotePortChannelIndex, RemoteBaseMac,
                             i4RemotePortIndex, &i4RemotePortBundleState);

                        nmhGetFsLaMCLAGRemotePortSyncStatus
                            (i4RemotePortChannelIndex, RemoteBaseMac,
                             i4RemotePortIndex, &i4RemotePortSyncStatus);

                        nmhGetFsLaMCLAGRemotePortPriority
                            (i4RemotePortChannelIndex, RemoteBaseMac,
                             i4RemotePortIndex, &i4RemotePortPriority);
                        nmhGetFsLaMCLAGRemotePortSlotIndex
                            (i4RemotePortChannelIndex, RemoteBaseMac,
                             i4RemotePortIndex, &i4RemotePortSlotIndex);

                        CliPrintf (CliHandle, "\rIf Index : %-5s, ", pifName);
                        switch (i4RemotePortBundleState)
                        {
                            case LA_PORT_UP_IN_BNDL:
                                CliPrintf (CliHandle, "State : Up in Bundle, ");
                                /* Up in Bundle ports */
                                break;

                            case LA_PORT_STANDBY:
                                CliPrintf (CliHandle, "State : Hot standby, ");
                                /* Stand by ports */
                                break;

                            case LA_PORT_DOWN:
                                CliPrintf (CliHandle,
                                           "State : Down, Not in Bundle, ");
                                /* Down ports */
                                break;

                            case LA_PORT_UP_INDIVIDUAL:
                                CliPrintf (CliHandle,
                                           "State : Up, Independent, ");
                                /* Up in Individual */
                                break;

                            default:
                                break;
                        }

                        switch (i4RemotePortSyncStatus)
                        {
                            case LA_DLAG_PORT_IN_SYNC:
                                CliPrintf (CliHandle, "Sync State: In Sync, ");
                                break;

                            case LA_DLAG_PORT_OUT_OF_SYNC:
                                CliPrintf (CliHandle,
                                           "Sync State: Out of Sync, ");
                                break;

                            default:
                                break;
                        }

                        CliPrintf (CliHandle, "Priority: %d ",
                                   i4RemotePortPriority);

                        CliPrintf (CliHandle, "Slot Index: %d ",
                                   i4RemotePortSlotIndex);
                        CliPrintf (CliHandle, "\n");
                        i4PrevRemotePortIndex = i4RemotePortIndex;
                    }

                    MEMCPY (PrevBaseMac, BaseMac, LA_MAC_ADDRESS_SIZE);
                    CliPrintf (CliHandle, "\n");
                }
            }
        }
        CliPrintf (CliHandle, "\n");
        u1Quit = (UINT1) CliPrintf (CliHandle, "\r");
        if (u1Quit == CLI_FAILURE)
        {
            break;
        }
        i4PrevIndex = i4Index;
    }
    while ((nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4Index) ==
            SNMP_SUCCESS) && (u1flag == 0));
    UNUSED_PARAM (i4RetValue);
    UNUSED_PARAM (i1RetValue);
    return (CLI_SUCCESS);
}
#endif /* ICCH_WANTED */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaCalculateAllow                                   */
/*                                                                           */
/*     DESCRIPTION      : This is an utility function called from            */
/*                        LaPortActorMode, for validating the values passed  */
/*                        from cli Interface Manager.                        */
/*                                                                           */
/*     INPUT            : u4IfaceId - Interface Identifier                   */
/*                        u2Key     - PortChannel Number                     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : LA_TRUE/LA_FALSE                                   */
/*                                                                           */
/*****************************************************************************/

INT4
LaCalculateAllow (tCliHandle CliHandle, UINT4 u4IfaceId, UINT2 u2Key)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2Index = 0;
    UINT2               u2ConfCount = 0;
    UINT2               u4Count = 0;
    UINT2               au2ConfPorts[LA_MAX_PORTS];
    LA_MEMSET (au2ConfPorts, 0, sizeof (UINT2));

    if (LaGetAggEntryByKey (u2Key, &pAggEntry) != LA_SUCCESS)
    {
        CLI_SET_ERR (CLI_LA_PORT_CHANNEL_ERR);
        return (CLI_FAILURE);
    }

    if (pAggEntry == NULL)
    {
        u4Count = 1;
        return LA_TRUE;

    }
    else
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);

        /* To get member-ports of the port-channel */
        while (1)
        {
            if (u2Index >= LA_MAX_PORTS)
            {
                break;
            }
            if (pPortEntry == NULL)
            {
                au2ConfPorts[u2Index] = 0;
                break;
            }
            else
            {
                au2ConfPorts[u2Index] = pPortEntry->u2PortIndex;
                u2Index++;
            }
            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }

    while (u4Count < u2Index)
    {
        /* Checking if the user-input interface is already a member port
         * of this port channel */

        if (u4IfaceId == au2ConfPorts[u2ConfCount])
        {
            u2ConfCount++;
            break;
        }
        u4Count++;
    }

    if ((pAggEntry->u1ConfigPortCount >=
         pAggEntry->u1MaxPortsToAttach) && u2ConfCount == 0)
    {
        CliPrintf (CliHandle,
                   "\r%%This port cannot belong to this  port channel"
                   " as there are already %d members in the port channel "
                   "configured for manual aggregation \r\n",
                   pAggEntry->u1MaxPortsToAttach);
        return LA_FALSE;
    }
    else
    {
        return LA_TRUE;
    }

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaGlobalInfo                                   */
/*                                                                           */
/*     DESCRIPTION      : Displays LA global information                     */
/*                                                                           */
/*     INPUT            : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
ShowLaGlobalInfo (tCliHandle CliHandle)
{
    UINT4               u4Value = 0;
    INT4                i4IfIndex = 0;
    INT4                i4Index = 0;
    INT4                i4SystemPrio;
    INT4                i4RetVal = 0;
    INT4                i4Flag = LA_FALSE;
    tMacAddr            SystemID;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY];

    LA_MEMSET (au1Temp, 0, sizeof (au1Temp));

    nmhGetFsLaStatus (&i4RetVal);

    /* Release the Lock ... */

    if (i4RetVal == LA_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nPort-channel Module Admin Status is "
                   "enabled\r\n");

        /* Get the module Operational status. */
        nmhGetFsLaOperStatus (&i4RetVal);
        if (i4RetVal == LA_ENABLED)
        {
            CliPrintf (CliHandle, "\rPort-channel Module Oper Status is "
                       "enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rPort-channel Module Oper Status is "
                       " disabled\r\n");
        }

        /* Get the recovery action to be performed on exceeding the recovery threshold */
        nmhGetFsLaRecThresholdExceedAction (&i4RetVal);
        if (i4RetVal == LA_ACT_NONE)
        {
            CliPrintf (CliHandle,
                       "\rPort-channel recovery action on exceeding Threshold is"
                       " None\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\rPort-channel recovery action on exceeding Threshold is"
                       " Shutdown\r\n");
        }

        /* Get the status of the port when partner is not available */
        nmhGetFsLaNoPartnerIndep (&i4RetVal);
        if (i4RetVal == LA_ENABLED)
        {
            CliPrintf (CliHandle, "\rPort-channel Independent mode is "
                       "enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rPort-channel Independent mode is "
                       "disabled\r\n");
        }
        nmhGetFsLaActorSystemID ((tMacAddr *) & SystemID);
        PrintMacAddress (SystemID, au1Temp);
        CliPrintf (CliHandle, "\rPort-channel System Identifier is  %s\r\n",
                   au1Temp);
        /* System priority value is set on a per port basis,
         * however all the ports have the same system-priority
         * value. So first get the first valid port index for LA
         * and get the corresponding system priority value.
         */
        if (nmhGetFirstIndexDot3adAggPortTable (&i4IfIndex) == SNMP_SUCCESS)
        {
            if (nmhGetDot3adAggPortActorSystemPriority
                (i4IfIndex, &i4SystemPrio) == SNMP_SUCCESS)
            {
                i4Flag = LA_TRUE;
            }
        }
        else if (nmhGetFirstIndexDot3adAggTable (&i4Index) == SNMP_SUCCESS)
        {
            /* if no ports are created but port channel(s) are
             * present get the actor system priority from the
             * first port channel entry in the agg table.
             */
            if (nmhGetDot3adAggActorSystemPriority (i4Index, &i4SystemPrio)
                == SNMP_SUCCESS)
            {
                i4Flag = LA_TRUE;
            }
        }
        if (i4Flag == LA_TRUE)
        {
            CliPrintf (CliHandle, "\rLACP System Priority: %d\r\n",
                       i4SystemPrio);
        }
        if (nmhGetFsLaRecTmrDuration (&u4Value) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rLACP Error Recovery Time: %u\r\n",
                       u4Value);
        }

        if (nmhGetFsLaRecThreshold (&u4Value) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rLACP Error Recovery Threshold: %u\r\n",
                       u4Value);
        }

        if (nmhGetFsLaTotalErrRecCount (&u4Value) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rLACP Recovery Triggered count: %u\r\n",
                       u4Value);
        }

        if (nmhGetFsLaDefaultedStateThreshold (&u4Value) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\rLACP Error Recovery Threshold for Defaulted State : %u\r\n",
                       u4Value);
        }

        if (nmhGetFsLaHardwareFailureRecThreshold (&u4Value) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\rLACP Error Recovery Threshold for Hardware Failure : %u\r\n",
                       u4Value);
        }

        if (nmhGetFsLaSameStateRecThreshold (&u4Value) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rLACP Same state threshold : %u\r\n",
                       u4Value);
        }

    }
    else
    {
        CliPrintf (CliHandle, "\r\nPort-channel Module Admin Status is "
                   "disabled\r\n");
        CliPrintf (CliHandle, "\rPort-channel Module Oper Status is "
                   "disabled\r\n");
    }
    /* Lock here again.. */

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowLaGroupInfo                                    */
/*                                                                           */
/*     DESCRIPTION      : Displays LA Group information                      */
/*                                                                           */
/*     INPUT            : u4IfIndex - PortChannel Number                     */
/*                        u1IsShowCommand - specifies the user input show    */
/*                                          command name                     */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
ShowLaGroupInfo (tCliHandle CliHandle, UINT4 u4IfIndex,
                 UINT1 u1IsShowPortChannel)
{
    INT4                i4Key = 0;
    INT4                i4PortMode = 0;
    UINT1               u1BridgeStatus = CFA_ENABLED;

    if (nmhValidateIndexInstanceDot3adAggTable ((INT4) u4IfIndex)
        == SNMP_FAILURE)
    {
        return;
    }

    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);
    nmhGetDot3adAggActorAdminKey ((INT4) u4IfIndex, &i4Key);
    nmhGetFsLaPortChannelMode ((INT4) u4IfIndex, &i4PortMode);

    /* Don't display the following Info for the "show etherchannel
     * CLI command. This can be valid only when there is no port
     * binded with the given port-channel*/

    if (u1IsShowPortChannel == LA_FALSE)
    {
        CliPrintf (CliHandle, "\n\r%38s\r\n", "Channel Group Listing");
        CliPrintf (CliHandle, "\r%38s", "---------------------");
    }

    CliPrintf (CliHandle, "\r\nGroup: %d \r\n", i4Key);
    CliPrintf (CliHandle, "\r----------\r\n");

    CliPrintf (CliHandle, "\rProtocol :");
    switch (i4PortMode)
    {
        case LA_MODE_LACP:
            CliPrintf (CliHandle, "LACP\r\n");
            break;
        case LA_MODE_MANUAL:
            CliPrintf (CliHandle, "Manual\r\n");
            break;
        case LA_MODE_DISABLED:
            CliPrintf (CliHandle, "Disabled\r\n");
            break;
        default:
            CliPrintf (CliHandle, "Unknown\r\n");
            break;
    }
    /* display the Port Channel info for the "show etherchannel
     * CLI command. This can be valid only when there is no port
     * binded with the given port-channel*/
    if (u1IsShowPortChannel == LA_FALSE)
    {
        CliPrintf (CliHandle, "\n\r%35s\r\n", "Ports in the Group");
        CliPrintf (CliHandle, "\r%35s\r\n", "------------------");
    }
    else
    {
        CliPrintf (CliHandle, "\n\r%43s\r\n", "Port-channels in the group:");
        CliPrintf (CliHandle, "\r%43s\r\n", "---------------------------");
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowRunningConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : Displays LA Configuration                          */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UNUSED_PARAM (u4Module);
    CliRegisterLock (CliHandle, LaLock, LaUnLock);
    LA_LOCK ();

    if (LaShowRunningConfigScalars (CliHandle) == CLI_SUCCESS)
    {
        LaShowRunningConfigTables (CliHandle);
        LaShowRunningConfigInterface (CliHandle);
    }

    LA_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LaShowRunningConfigScalars                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in LA        */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
LaShowRunningConfigScalars (tCliHandle CliHandle)
{
    INT4                i4SystemControl = 0;
    INT4                i4LaStatus = 0;
    INT4                i4IfIndex = 0;
    INT4                i4AggIndex = 0;
    INT4                i4SystemPrio = 0;
    INT4                i4LaIndepMode = LA_DISABLED;
    INT4                i4DLAGSystemPriority = 0;
    INT4                i4Value;
    UINT4               u4Value;
#if (defined DLAG_WANTED) || (defined ICCH_WANTED)
    tMacAddr            BaseMac;
#endif
#ifdef ICCH_WANTED
    UINT4               u4MCLAGPeriodicSyncTime = 0;
    INT4                i4MCLAGSystemPriority = 0;
    INT4                i4MCLAGStatus = 0;
    INT4                i4MCLAGSystemStatus = 0;
#endif
    tLaPortList        *pDistributePorts = NULL;
    tMacAddr            MacAddr;
    tMacAddr            DefaultNvRamMacAddr;
    tMacAddr            DefaultMacAddr = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    tSNMP_OCTET_STRING_TYPE DLAGDistributePorts;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY];
    UINT1              *pu1Temp = NULL;
#ifdef DLAG_WANTED
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4PagingStatus = 0;
    UINT4               u4DLAGPeriodicSyncTime = 0;
    UINT1               u1EntryFound = OSIX_FALSE;
    INT4                i4DLAGDistributingPort = 0;
    INT4                i4DLAGStatus = 0;
    INT1                ai1Temp[CFA_MAX_PORT_NAME_LENGTH];
    BOOL1               bResult = OSIX_FALSE;
#endif

    pDistributePorts =
        (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

    if (pDistributePorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    LA_MEMSET (au1Temp, 0, sizeof (au1Temp));
    LA_MEMSET (MacAddr, 0, sizeof (tMacAddr));
    LA_MEMSET (&DefaultNvRamMacAddr, 0, sizeof (tMacAddr));

    LA_MEMSET (*pDistributePorts, 0, sizeof (tLaPortList));
    LA_MEMSET (&DLAGDistributePorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    DLAGDistributePorts.pu1_OctetList = *pDistributePorts;
    DLAGDistributePorts.i4_Length = sizeof (tLaPortList);

    pu1Temp = &au1Temp[0];

    CliPrintf (CliHandle, "\r\n");

    nmhGetFsLaSystemControl (&i4SystemControl);

    if (i4SystemControl != LA_START)
    {
        CliPrintf (CliHandle, "shutdown port-channel\r\n");
        FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
        return CLI_FAILURE;

    }
    nmhGetFsLaStatus (&i4LaStatus);
    if (i4LaStatus != LA_DISABLED)
    {
        CliPrintf (CliHandle, "set port-channel enable\r\n");
    }

    nmhGetFsLaNoPartnerIndep (&i4LaIndepMode);
    if (i4LaIndepMode != LA_DISABLED)
    {
        CliPrintf (CliHandle, "set port-channel independentmode enable\r\n");
    }

#ifdef DLAG_WANTED
    nmhGetFsLaDLAGSystemStatus (&i4DLAGStatus);

    if (i4DLAGStatus == LA_ENABLED)
    {
        nmhGetFsLaDLAGSystemStatus (&i4DLAGStatus);

        if (i4DLAGStatus != LA_DLAG_DISABLED)
        {
            CliPrintf (CliHandle, "set d-lag enable\r\n");
        }
        if ((nmhGetFsLaDLAGDistributingPortIndex
             (&i4DLAGDistributingPort)) == SNMP_SUCCESS)
        {
            if (i4DLAGDistributingPort != 0)
            {
                CfaCliConfGetIfName ((UINT4) i4DLAGDistributingPort, ai1Temp);
                CliPrintf (CliHandle, "d-lag distribute-port %-15s\r\n",
                           ai1Temp);
            }
        }
        /* Display the D-LAG periodic sync time configuration */
        if ((nmhGetFsLaDLAGPeriodicSyncTime
             (&u4DLAGPeriodicSyncTime)) == SNMP_SUCCESS)
        {
            if (u4DLAGPeriodicSyncTime != LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME)
            {
                CliPrintf (CliHandle, "d-lag periodic-sync-time %d\r\n",
                           u4DLAGPeriodicSyncTime);
            }
        }
        /* Display the D-LAG System Identifier configuration */
        if ((nmhGetFsLaDLAGSystemID ((tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
        {
            /* Display the command, if the system mac id is not zero */
            if ((LA_MEMCMP (&BaseMac, DefaultMacAddr, LA_MAC_ADDRESS_SIZE)) !=
                0)
            {
                PrintMacAddress (BaseMac, pu1Temp);
                CliPrintf (CliHandle, "d-lag system-identifier %s\r\n",
                           pu1Temp);
            }
        }
        /* Display the D-LAG system priority configuration */
        if ((nmhGetFsLaDLAGSystemPriority (&i4DLAGSystemPriority)) ==
            SNMP_SUCCESS)
        {
            if (i4DLAGSystemPriority != LA_DLAG_DEFAULT_SYSTEM_PRIORITY)
            {
                CliPrintf (CliHandle, "d-lag system-priority %d\r\n",
                           i4DLAGSystemPriority);
            }
        }
        if ((nmhGetFsLaDLAGDistributingPortList (&DLAGDistributePorts))
            == SNMP_SUCCESS)
        {
            for (; u4DLAGDistributingIfIndex <= LA_PORT_LIST_SIZE * 8;
                 u4DLAGDistributingIfIndex++)
            {
                OSIX_BITLIST_IS_BIT_SET (DLAGDistributePorts.pu1_OctetList,
                                         u4DLAGDistributingIfIndex,
                                         LA_PORT_LIST_SIZE, bResult);
                if (bResult == OSIX_FALSE)
                {
                    /* If the bit is not set, continue the for loop */
                    continue;
                }
                if ((UINT4) i4DLAGDistributingPort == u4DLAGDistributingIfIndex)
                {
                    OSIX_BITLIST_RESET_BIT (DLAGDistributePorts.pu1_OctetList,
                                            u4DLAGDistributingIfIndex,
                                            LA_PORT_LIST_SIZE);
                }
                else
                {
                    u1EntryFound = OSIX_TRUE;
                }
            }
            if (u1EntryFound == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "\rd-lag distribute-port-list");

                if ((CliConfOctetToIfName
                     (CliHandle, " ", NULL, &DLAGDistributePorts,
                      &u4PagingStatus)) == CLI_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
                    return CLI_FAILURE;
                }
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
#endif
#ifdef ICCH_WANTED

    /*Display MC-LAG system control status */
    if (nmhGetFsLaMCLAGSystemControl (&i4MCLAGSystemStatus) == SNMP_SUCCESS)
    {
        if (i4MCLAGSystemStatus == LA_START)
        {
            CliPrintf (CliHandle, "no shutdown mc-lag\r\n");
        }
    }

    nmhGetFsLaMCLAGSystemStatus (&i4MCLAGStatus);
    /* Display the MC-LAG periodic sync time configuration */

    if ((nmhGetFsLaMCLAGPeriodicSyncTime
         (&u4MCLAGPeriodicSyncTime)) == SNMP_SUCCESS)
    {
        if (u4MCLAGPeriodicSyncTime != LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME)
        {
            CliPrintf (CliHandle, "mc-lag periodic-sync-time %d\r\n",
                       u4MCLAGPeriodicSyncTime);
        }
    }
    /* Display the MC-LAG System Identifier configuration */
    if ((nmhGetFsLaMCLAGSystemID ((tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
    {
        /* Display the command, if the system mac id is not zero and not eqaul
         * to System MAC Address*/

        LaCfaGetSysMacAddress (DefaultNvRamMacAddr);
        if ((LA_MEMCMP (&BaseMac, DefaultMacAddr, LA_MAC_ADDRESS_SIZE) != 0))
        {

            /* If system identifier is  explicitly configured by the 
             * user then display the command.*/
            if (gLaGlobalInfo.bIsMCLAGGlobalSysIDConfigured == LA_TRUE)
            {
                PrintMacAddress (BaseMac, pu1Temp);
                CliPrintf (CliHandle, "mc-lag system-identifier %s\r\n",
                           pu1Temp);
            }
        }
    }
    /* Display the MC-LAG system priority configuration */
    if ((nmhGetFsLaMCLAGSystemPriority (&i4MCLAGSystemPriority)) ==
        SNMP_SUCCESS)
    {
        if (i4MCLAGSystemPriority != LA_DLAG_DEFAULT_SYSTEM_PRIORITY)
        {
            CliPrintf (CliHandle, "mc-lag system-priority %d\r\n",
                       i4MCLAGSystemPriority);
        }
    }
    if (i4MCLAGStatus == LA_ENABLED)
    {
        CliPrintf (CliHandle, "set mc-lag enable\r\n");
    }
#endif
    nmhGetFsLaActorSystemID (&MacAddr);

    LaCfaGetSysMacAddress (DefaultNvRamMacAddr);
    if ((LA_MEMCMP (MacAddr, DefaultMacAddr, LA_MAC_ADDRESS_SIZE) != 0) &&
        (LA_MEMCMP (MacAddr, DefaultNvRamMacAddr, LA_MAC_ADDRESS_SIZE) != 0))
    {
        PrintMacAddress (MacAddr, au1Temp);
        CliPrintf (CliHandle, "lacp system-identifier %s\r\n", au1Temp);
    }

    if (nmhGetFirstIndexDot3adAggPortTable (&i4IfIndex) == SNMP_SUCCESS)
    {

        /* All ports have same system-priority */

        nmhGetDot3adAggPortActorSystemPriority (i4IfIndex, &i4SystemPrio);

        if (i4SystemPrio != LA_DEFAULT_SYSTEM_PRIORITY)
        {
            if (i4DLAGSystemPriority != i4SystemPrio
                || 0 == i4DLAGSystemPriority)
            {
                CliPrintf (CliHandle, "lacp system-priority  %d\r\n",
                           i4SystemPrio);
            }
        }
    }

    else
    {
        if (nmhGetFirstIndexDot3adAggTable (&i4AggIndex) == SNMP_SUCCESS)
        {
            nmhGetDot3adAggActorSystemPriority (i4AggIndex, &i4SystemPrio);

            if (i4SystemPrio != LA_DEFAULT_SYSTEM_PRIORITY)
            {
                if (i4DLAGSystemPriority != i4SystemPrio
                    || 0 == i4DLAGSystemPriority)
                {
                    CliPrintf (CliHandle, "lacp system-priority  %d\r\n",
                               i4SystemPrio);
                }
            }
        }
    }

    FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
    if (nmhGetFsLaRecTmrDuration (&u4Value) == SNMP_SUCCESS)
    {
        if (u4Value != LA_DEFAULT_RECOVERY_TIME)
        {
            CliPrintf (CliHandle,
                       "\rset port-channel error-recovery-time %u\r\n",
                       u4Value);
        }
    }

    if (nmhGetFsLaRecThreshold (&u4Value) == SNMP_SUCCESS)
    {
        if (u4Value != LA_DEFAULT_REC_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "\rport-channel error-recovery-threshold  %u\r\n",
                       u4Value);
        }
    }

    if (nmhGetFsLaDefaultedStateThreshold (&u4Value) == SNMP_SUCCESS)
    {
        if (u4Value != LA_DEF_STATE_DEFAULT_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "\rport-channel defaulted-state-threshold %u\r\n",
                       u4Value);
        }
    }

    if (nmhGetFsLaHardwareFailureRecThreshold (&u4Value) == SNMP_SUCCESS)
    {
        if (u4Value != LA_DEFAULT_REC_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "\rport-channel hw-failure recovery-threshold %u\r\n",
                       u4Value);
        }
    }

    if (nmhGetFsLaSameStateRecThreshold (&u4Value) == SNMP_SUCCESS)
    {
        if (u4Value != LA_DEFAULT_REC_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "\rport-channel same-state recovery-threshold %u\r\n",
                       u4Value);
        }

    }

    if (nmhGetFsLaRecThresholdExceedAction (&i4Value) == SNMP_SUCCESS)
    {
        if (i4Value != LA_ACT_NONE)
        {
            CliPrintf (CliHandle,
                       "\rport-channel rec-threshold-exceed-action shutdown\r\n");
        }
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LaShowRunningConfigTables                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays  the table  objects in LA   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
LaShowRunningConfigTables (tCliHandle CliHandle)
{
    INT4                i4PortSelPolicy = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIndex = 0;
    INT4                i4Key = 0;
    INT1                i1OutCome;
    UINT1               au1Temp[MAX_LEN_TEMP_ARRAY];

    i1OutCome = nmhGetFirstIndexDot3adAggTable (&i4IfIndex);

    while (i1OutCome != SNMP_FAILURE)
    {
        LA_MEMSET (au1Temp, 0, sizeof (au1Temp));

        /*ChannelSelectionPolicy */

        nmhGetFsLaPortChannelSelectionPolicyBitList (i4IfIndex,
                                                     &i4PortSelPolicy);
        nmhGetDot3adAggActorAdminKey (i4IfIndex, &i4Key);

        if ((i4PortSelPolicy ^ LA_SELECT_POLICY_DEF) != 0)
        {
            if ((i4PortSelPolicy & LA_SELECT_SRC_DST_MAC) !=
                LA_SELECT_SRC_DST_MAC)
            {
                if ((i4PortSelPolicy & LA_SELECT_SRC_MAC) == LA_SELECT_SRC_MAC)
                {
                    STRCAT (au1Temp, "src-mac ");
                }
                if ((i4PortSelPolicy & LA_SELECT_DST_MAC) == LA_SELECT_DST_MAC)
                {
                    STRCAT (au1Temp, "dest-mac ");
                }
            }
            else
            {
                STRCAT (au1Temp, "src-dest-mac ");
            }
            if ((i4PortSelPolicy & LA_SELECT_SRC_DST_IP) !=
                LA_SELECT_SRC_DST_IP)
            {
                if ((i4PortSelPolicy & LA_SELECT_SRC_IP) == LA_SELECT_SRC_IP)
                {
                    STRCAT (au1Temp, "src-ip ");
                }
                if ((i4PortSelPolicy & LA_SELECT_DST_IP) == LA_SELECT_DST_IP)
                {
                    STRCAT (au1Temp, "dest-ip ");
                }
            }
            else
            {
                STRCAT (au1Temp, "src-dest-ip ");
            }
            if ((i4PortSelPolicy & LA_SELECT_VLAN_ID) == LA_SELECT_VLAN_ID)
            {
                STRCAT (au1Temp, "vlan-id ");
            }
            if ((i4PortSelPolicy & LA_SELECT_SA_DA_IP_PORT_PROTO) ==
                LA_SELECT_SA_DA_IP_PORT_PROTO)
            {
                STRCAT (au1Temp,
                        "SRC,DST IP's,SRC,DST MAC's,Protocol,L4 Src and Dst");
            }
            if ((i4PortSelPolicy & LA_SELECT_SERVICE_INSTANCE) ==
                LA_SELECT_SERVICE_INSTANCE)
            {
                STRCAT (au1Temp, "service-instance ");
            }
            if ((i4PortSelPolicy & LA_SELECT_MAC_SRC_DST_VID) !=
                LA_SELECT_MAC_SRC_DST_VID)
            {
                if ((i4PortSelPolicy & LA_SELECT_MAC_SRC_VID) ==
                    LA_SELECT_MAC_SRC_VID)
                {
                    STRCAT (au1Temp, "mac-src-vid ");
                }
                if ((i4PortSelPolicy & LA_SELECT_MAC_DST_VID) ==
                    LA_SELECT_MAC_DST_VID)
                {
                    STRCAT (au1Temp, "mac-dest-vid ");
                }
            }
            else
            {
                STRCAT (au1Temp, "mac-src-dest-vid ");
            }
            if ((i4PortSelPolicy & LA_SELECT_DST_IP6) == LA_SELECT_DST_IP6)
            {
                STRCAT (au1Temp, "dest-ip6 ");
            }
            if ((i4PortSelPolicy & LA_SELECT_SRC_IP6) == LA_SELECT_SRC_IP6)
            {
                STRCAT (au1Temp, "src-ip6 ");
            }
            if ((i4PortSelPolicy & LA_SELECT_L3_PROTOCOL) ==
                LA_SELECT_L3_PROTOCOL)
            {
                STRCAT (au1Temp, "l3-protocol ");
            }
            if ((i4PortSelPolicy & LA_SELECT_DST_L4_PORT) ==
                LA_SELECT_DST_L4_PORT)
            {
                STRCAT (au1Temp, "dest-l4-port ");
            }
            if ((i4PortSelPolicy & LA_SELECT_SRC_L4_PORT) ==
                LA_SELECT_SRC_L4_PORT)
            {
                STRCAT (au1Temp, "src-l4-port ");
            }
            if ((i4PortSelPolicy & LA_SELECT_MPLS_VC_LABEL) ==
                LA_SELECT_MPLS_VC_LABEL)
            {
                STRCAT (au1Temp, "mpls-vc-label ");
            }
            if ((i4PortSelPolicy & LA_SELECT_MPLS_TUNNEL_LABEL) ==
                LA_SELECT_MPLS_TUNNEL_LABEL)
            {
                STRCAT (au1Temp, "mpls-tunnel-label ");
            }
            if ((i4PortSelPolicy & LA_SELECT_MPLS_VC_TUNNEL) ==
                LA_SELECT_MPLS_VC_TUNNEL)
            {
                STRCAT (au1Temp, "mpls-vc-tunnel-label ");
            }

            if (((i4PortSelPolicy & LA_SELECT_SRC_DST_MAC) !=
                 LA_SELECT_SRC_DST_MAC) &&
                ((i4PortSelPolicy & LA_SELECT_ENHANCED) !=
                 LA_SELECT_ENHANCED) &&
                ((i4PortSelPolicy & LA_SELECT_RANDOMIZED) !=
                 LA_SELECT_RANDOMIZED))
            {
                CliPrintf (CliHandle, "port-channel load-balance %s %d\r\n",
                           au1Temp, i4Key);
            }

        }

        i4PrevIndex = i4IfIndex;

        i1OutCome = nmhGetNextIndexDot3adAggTable (i4PrevIndex, &i4IfIndex);

    }

    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : LaShowRunningConfigInterface                       */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface tables  in LA    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
LaShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextChannelIfIndex = 0;
    INT1                i1OutCome;
    INT4                i4AdminKey = 0;
    INT4                i4NextIfIndex = 0;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    i1OutCome = nmhGetFirstIndexDot3adAggTable (&i4IfIndex);

    while (i1OutCome != SNMP_FAILURE)
    {
        i4NextChannelIfIndex = i4IfIndex;

        nmhGetDot3adAggActorAdminKey (i4NextChannelIfIndex, &i4AdminKey);

        LA_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        LaShowRunningConfigPortChannelInterfaceDetails (CliHandle,
                                                        i4NextChannelIfIndex);

        CliRegisterLock (CliHandle, LaLock, LaUnLock);
        LA_LOCK ();

        i1OutCome =
            nmhGetNextIndexDot3adAggTable (i4NextChannelIfIndex, &i4IfIndex);
    }

    i1OutCome = nmhGetFirstIndexFsLaPortTable (&i4IfIndex);

    while (i1OutCome != SNMP_FAILURE)
    {
        if (LaGetAggEntryBasedOnPort ((UINT2) i4IfIndex,
                                      &pLaLacAggEntry) == LA_SUCCESS)
        {
            i4NextChannelIfIndex = (INT4) pLaLacAggEntry->u2AggIndex;
            MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
            LA_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            LaShowRunningConfigPhysicalInterfaceDetails (CliHandle,
                                                         i4IfIndex,
                                                         (UINT4)
                                                         i4NextChannelIfIndex);

            CliRegisterLock (CliHandle, LaLock, LaUnLock);
            LA_LOCK ();

            CliPrintf (CliHandle, "!\r\n");
        }

        i1OutCome = nmhGetNextIndexFsLaPortTable (i4IfIndex, &i4NextIfIndex);
        i4IfIndex = i4NextIfIndex;
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LaShowRunningConfigPhysicalInterfaceDetails        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the physical Interface details*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
LaShowRunningConfigPhysicalInterfaceDetails (tCliHandle CliHandle, INT4 i4Index,
                                             UINT4 u4ChannelIndex)
{
    INT4                i4SystemControl;
    tLaLacAggEntry     *pLaLacAggEntry;

    CliRegisterLock (CliHandle, LaLock, LaUnLock);
    LA_LOCK ();

    if (nmhValidateIndexInstanceDot3adAggPortTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetFsLaSystemControl (&i4SystemControl);

        /*Needed this check When called from Cfa... */
        if (i4SystemControl != LA_START)
        {
            LA_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        if (u4ChannelIndex != 0)
        {
            LaShowRunningConfigDisplayPortDetails (CliHandle, i4Index,
                                                   u4ChannelIndex);
        }
        else
        {
            if ((LaGetAggEntryBasedOnPort ((UINT2) i4Index,
                                           &pLaLacAggEntry)) != LA_SUCCESS)
            {
                LA_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return;
            }
            u4ChannelIndex = pLaLacAggEntry->u2AggIndex;
            {
                LaShowRunningConfigDisplayPortDetails (CliHandle, i4Index,
                                                       u4ChannelIndex);
            }
        }
    }
    LA_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LaShowRunningConfigDisplayPortDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays physical Interface details  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
LaShowRunningConfigDisplayPortDetails (tCliHandle CliHandle,
                                       INT4 i4Index, UINT4 u4ChannelIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    INT4                i4PortPriority = 0;
    UINT4               u4WaitTime = 0;
    UINT4               u4DefaultedStateThresh = 0;
    UINT4               u4HwFailureRecThresh = 0;
    UINT4               u4SameStateRecThresh = 0;
    INT4                i4PortMode = 0;
    INT4                i4AdminKey = 0;
    INT4                i4AdminPort = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName;
    UINT1               u1State = 0;
    UINT1               u1Value = 0;
    UINT1               u1ValTimeOut = 0;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    /* PortEntry */
    LaGetPortEntry ((UINT2) i4Index, &pPortEntry);

    if (pPortEntry == NULL)
    {
        return;
    }
    /*PortMode */
    nmhGetFsLaPortMode (i4Index, &i4PortMode);

    if (i4PortMode != LA_MODE_DISABLED)
    {

        LaGetPortState (&(pPortEntry->LaLacActorAdminInfo.LaLacPortState),
                        &u1State);

        u1Value = (UINT1) (u1State & LA_LACPACTIVITY_BITMASK);

    }
    u1ValTimeOut = (UINT1) (u1State & LA_LACPTIMEOUT_BITMASK);

    nmhGetDot3adAggActorAdminKey ((INT4) u4ChannelIndex, &i4AdminKey);
    nmhGetDot3adAggPortActorPortPriority (i4Index, &i4PortPriority);
    nmhGetFsLaPortAggregateWaitTime (i4Index, &u4WaitTime);
    nmhGetFsLaPortActorAdminPort (i4Index, &i4AdminPort);
    nmhGetFsLaPortDefaultedStateThreshold (i4Index, &u4DefaultedStateThresh);
    nmhGetFsLaPortHardwareFailureRecThreshold (i4Index, &u4HwFailureRecThresh);
    nmhGetFsLaPortSameStateRecThreshold (i4Index, &u4SameStateRecThresh);

    if (((pPortEntry->bDynAggSelect == LA_FALSE) &&
         ((i4PortMode != LA_MODE_DISABLED) ||
          (i4PortMode == LA_MODE_MANUAL) ||
          (i4PortMode == LA_MODE_LACP))) ||
        (pPortEntry->LaLacActorAdminInfo.u2IfKey != (UINT2) i4AdminKey) ||
        (i4PortPriority != LA_DEFAULT_PORT_PRIORITY) ||
        (u4WaitTime / 100 != LA_PORT_WAITTIME_DEF) ||
        (u1ValTimeOut != 0) || (i4AdminPort != i4Index) ||
        (u4SameStateRecThresh != LA_DEFAULT_REC_THRESHOLD) ||
        (u4HwFailureRecThresh != LA_DEFAULT_REC_THRESHOLD) ||
        (u4DefaultedStateThresh != LA_DEF_STATE_DEFAULT_THRESHOLD))
    {
        CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
    }
    if (pPortEntry->bDynAggSelect == LA_FALSE)
    {
        if (i4PortMode != LA_MODE_DISABLED)
        {
            CliPrintf (CliHandle, "channel-group %d mode", i4AdminKey);
        }

        if (i4PortMode == LA_MODE_MANUAL)
        {
            CliPrintf (CliHandle, " on\r\n");
        }
        else if (i4PortMode == LA_MODE_LACP)
        {
            if (u1Value != 0)
            {
                CliPrintf (CliHandle, " active\r\n");
            }
            else
            {
                CliPrintf (CliHandle, " passive\r\n");
            }
        }
    }
    else
    {
        if (pPortEntry->LaLacActorAdminInfo.u2IfKey != (UINT2) i4AdminKey)
        {
            CliPrintf (CliHandle, "lacp admin-key %d mode",
                       pPortEntry->LaLacActorAdminInfo.u2IfKey);

            if (u1Value != 0)
            {
                CliPrintf (CliHandle, " active\r\n");
            }
            else
            {
                CliPrintf (CliHandle, " passive\r\n");
            }
        }
    }

    /*PortPriority */

    if (i4PortPriority != LA_DEFAULT_PORT_PRIORITY)
    {
        CliPrintf (CliHandle, "lacp port-priority %d\r\n", i4PortPriority);

    }

    /*WaitTime */

    if (u4WaitTime / 100 != LA_PORT_WAITTIME_DEF)
    {
        CliPrintf (CliHandle, "lacp wait-time %d\r\n", u4WaitTime / 100);
    }

    /* Actor Admin PortState */

    LaGetPortState (&(pPortEntry->LaLacActorAdminInfo.LaLacPortState),
                    &u1State);

    u1ValTimeOut = (UINT1) (u1State & LA_LACPTIMEOUT_BITMASK);

    if (u1ValTimeOut != 0)
    {
        CliPrintf (CliHandle, "lacp timeout short\r\n");
    }

    if (i4AdminPort != i4Index)
    {
        CliPrintf (CliHandle, "lacp port-identifier %d\r\n", i4AdminPort);
    }

    if (u4DefaultedStateThresh != LA_DEF_STATE_DEFAULT_THRESHOLD)
    {
        CliPrintf (CliHandle, "defaulted-state-threshold %u\r\n",
                   u4DefaultedStateThresh);
    }

    if (u4HwFailureRecThresh != LA_DEFAULT_REC_THRESHOLD)
    {
        CliPrintf (CliHandle, "hw-failure recovery-threshold %u\r\n",
                   u4HwFailureRecThresh);
    }

    if (u4SameStateRecThresh != LA_DEFAULT_REC_THRESHOLD)
    {
        CliPrintf (CliHandle, "same-state recovery-threshold %u\r\n",
                   u4SameStateRecThresh);
    }
    CliPrintf (CliHandle, "! \r\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : LaShowRunningConfigPortChannelInterfaceDetails     */
/*                                                                           */
/*     DESCRIPTION      : This function displays Interface  objects in LA    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
LaShowRunningConfigPortChannelInterfaceDetails (tCliHandle CliHandle,
                                                INT4 i4Index)
{
    tMacAddr            BaseMac;
    tMacAddr            TempMacAddr;
    tMacAddr            DefaultNvRamMacAddr;
    tSNMP_OCTET_STRING_TYPE DLAGDistributePorts;
    tLaPortList        *pDistributePorts = NULL;

#ifdef ICCH_WANTED
    tLaLacAggEntry     *pAggEntry = NULL;
#endif
#ifdef DLAG_WANTED
    UINT4               u4DLAGDistributingIfIndex = 1;
    UINT4               u4PagingStatus = 0;
    UINT1               u1EntryFound = OSIX_FALSE;
    BOOL1               bResult = OSIX_FALSE;
#endif
    UINT4               u4DLAGPeriodicSyncTime = 0;
    UINT4               u4DLAGMSSelectionWaitTime = 0;
    INT4                i4DLAGDistributingPort = 0;
    INT4                i4DLAGSystemPriority = 0;
    INT4                i4DLAGRedundancy = 0;
    INT4                i4MCLAGStatus = 0;
    INT4                i4DefaultPort;
    INT4                i4LaAggMaxPorts;
    INT4                i4DLAGStatus = 0;
    INT4                i4PortSelPolicy = 0;
#ifdef ICCH_WANTED
    INT4                i4MCLAGSystemPriority = 0;
#endif
    UINT1              *pu1Temp = NULL;
    UINT1               au1TmpMac[MAX_LEN_TEMP_ARRAY];
    INT1                ai1Temp[CFA_MAX_PORT_NAME_LENGTH];

    INT1               *piIfName;
    INT1                i1Flag = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    pDistributePorts =
        (tLaPortList *) FsUtilAllocBitList (sizeof (tLaPortList));

    if (pDistributePorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return;
    }
    MEMSET (*pDistributePorts, 0, sizeof (tLaPortList));
    MEMSET (&DLAGDistributePorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    DLAGDistributePorts.pu1_OctetList = *pDistributePorts;
    DLAGDistributePorts.i4_Length = sizeof (tLaPortList);

    MEMSET (au1TmpMac, 0, MAX_LEN_TEMP_ARRAY);
    pu1Temp = &au1TmpMac[0];

    MEMSET (BaseMac, 0, LA_MAC_ADDRESS_SIZE);
    MEMSET (TempMacAddr, 0, LA_MAC_ADDRESS_SIZE);
    MEMSET (DefaultNvRamMacAddr, 0, LA_MAC_ADDRESS_SIZE);

    CliRegisterLock (CliHandle, LaLock, LaUnLock);
    LA_LOCK ();

    if (nmhValidateIndexInstanceDot3adAggTable (i4Index) == SNMP_SUCCESS)
    {

        /*PortChannelInterface Details */
        nmhGetFsLaPortChannelDefaultPortIndex (i4Index, &i4DefaultPort);
        nmhGetFsLaPortChannelMaxPorts (i4Index, &i4LaAggMaxPorts);
        nmhGetFsLaPortChannelSelectionPolicyBitList (i4Index, &i4PortSelPolicy);
#ifdef DLAG_WANTED
        /* Display the D-LAG distribute port */
        nmhGetFsLaPortChannelDLAGDistributingPortIndex (i4Index,
                                                        &i4DLAGDistributingPort);
        /* Display the D-LAG system priority command */
        nmhGetFsLaPortChannelDLAGSystemPriority (i4Index,
                                                 &i4DLAGSystemPriority);
        /* Display the D-LAG status  command */
        nmhGetFsLaPortChannelDLAGStatus (i4Index, &i4DLAGStatus);
        /* Display the D-LAG periodic sync time command */
        nmhGetFsLaPortChannelDLAGPeriodicSyncTime (i4Index,
                                                   &u4DLAGPeriodicSyncTime);
        /* Display the D-LAG master-slave selection wait time  command */
        nmhGetFsLaPortChannelDLAGMSSelectionWaitTime (i4Index,
                                                      &u4DLAGMSSelectionWaitTime);
        nmhGetFsLaPortChannelDLAGRedundancy (i4Index, &i4DLAGRedundancy);
#endif
#ifdef ICCH_WANTED
        /* Display the MC-LAG status  command */
        nmhGetFsLaPortChannelMCLAGStatus (i4Index, &i4MCLAGStatus);
#endif

        if ((i4DefaultPort != 0) || (i4LaAggMaxPorts != LA_MAX_PORTS_PER_AGG) ||
            (i4PortSelPolicy == LA_SELECT_ENHANCED) ||
            (i4PortSelPolicy == LA_SELECT_RANDOMIZED) ||
            (i4DLAGDistributingPort != 0) ||
            (i4DLAGSystemPriority != LA_DLAG_DEFAULT_SYSTEM_PRIORITY) ||
            (i4DLAGStatus == LA_DLAG_ENABLED) ||
            (i4MCLAGStatus == LA_MCLAG_ENABLED) ||
            (u4DLAGPeriodicSyncTime != LA_DLAG_DEFAULT_PERIODIC_SYNC_TIME) ||
            (u4DLAGMSSelectionWaitTime !=
             LA_DLAG_DEFAULT_MS_SELECTION_WAIT_TIME)
            || (i4DLAGRedundancy == LA_DLAG_REDUNDANCY_ON))
        {
            i1Flag = 1;
            CfaCliConfGetIfName ((UINT4) i4Index, piIfName);
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        }

        if (i4DefaultPort != 0)
        {
            if (CfaCliConfGetIfName ((UINT4) i4DefaultPort, ai1Temp) ==
                CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "default port %s\r\n", ai1Temp);
            }
        }

        if (nmhGetDot3adAggMACAddress (i4Index, (tMacAddr *) & BaseMac)
            == SNMP_FAILURE)
        {

            PrintMacAddress (BaseMac, pu1Temp);
            CliPrintf (CliHandle, "Aggregator-Mac %s\r\n", pu1Temp);
        }
        if (i4LaAggMaxPorts != LA_MAX_PORTS_PER_AGG)
        {
            CliPrintf (CliHandle, "port-channel max-ports %d\r\n",
                       i4LaAggMaxPorts);
        }
        if (i4PortSelPolicy == LA_SELECT_ENHANCED)
        {
            CliPrintf (CliHandle, "load-balance enhanced-mode enable\r\n");
        }
        if (i4PortSelPolicy == LA_SELECT_RANDOMIZED)
        {
            CliPrintf (CliHandle, "load-balance randomized enable\r\n");
        }
        if (i4DLAGStatus == LA_DLAG_ENABLED)
        {

#ifdef DLAG_WANTED
            if ((nmhGetFsLaDLAGSystemID ((tMacAddr *) & BaseMac)) ==
                SNMP_SUCCESS)
            {

                /* Display the command, if the system mac id is not zero */
                if ((LA_MEMCMP (&BaseMac, TempMacAddr, LA_MAC_ADDRESS_SIZE)) !=
                    0)
                {

                    /* D-LAG commands */
                    MEMSET (BaseMac, 0, LA_MAC_ADDRESS_SIZE);
                    /* Display the D-LAG distribute port */

                    if (i4DLAGDistributingPort != 0)
                    {
                        CfaCliConfGetIfName ((UINT4) i4DLAGDistributingPort,
                                             ai1Temp);
                        CliPrintf (CliHandle, "d-lag distribute-port %-15s\r\n",
                                   ai1Temp);
                    }

                    if ((nmhGetFsLaPortChannelDLAGDistributingPortList (i4Index,
                                                                        &DLAGDistributePorts))
                        == SNMP_SUCCESS)
                    {
                        for (;
                             u4DLAGDistributingIfIndex <= LA_PORT_LIST_SIZE * 8;
                             u4DLAGDistributingIfIndex++)
                        {
                            OSIX_BITLIST_IS_BIT_SET (DLAGDistributePorts.
                                                     pu1_OctetList,
                                                     u4DLAGDistributingIfIndex,
                                                     LA_PORT_LIST_SIZE,
                                                     bResult);
                            if (bResult == OSIX_FALSE)
                            {
                                /* If the bit is not set, continue the for loop */
                                continue;
                            }
                            if ((UINT4) i4DLAGDistributingPort ==
                                u4DLAGDistributingIfIndex)
                            {
                                OSIX_BITLIST_RESET_BIT (DLAGDistributePorts.
                                                        pu1_OctetList,
                                                        u4DLAGDistributingIfIndex,
                                                        LA_PORT_LIST_SIZE);
                            }
                            else
                            {
                                u1EntryFound = OSIX_TRUE;
                            }
                        }
                        if (u1EntryFound == OSIX_TRUE)
                        {
                            CliPrintf (CliHandle,
                                       "\rd-lag distribute-port-list");

                            if ((CliConfOctetToIfName
                                 (CliHandle, " ", NULL, &DLAGDistributePorts,
                                  &u4PagingStatus)) == CLI_FAILURE)
                            {
                                FsUtilReleaseBitList ((UINT1 *)
                                                      pDistributePorts);
                                LA_UNLOCK ();
                                CliUnRegisterLock (CliHandle);
                                return;
                            }
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                    MEMSET (au1TmpMac, 0, MAX_LEN_TEMP_ARRAY);
                    pu1Temp = &au1TmpMac[0];
                    /* Display the D-LAG System Identifier command */
                    if ((nmhGetFsLaPortChannelDLAGSystemID
                         (i4Index, (tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
                    {
                        /* Display the command, if the system mac id is not zero */
                        if ((LA_MEMCMP
                             (&BaseMac, TempMacAddr, LA_MAC_ADDRESS_SIZE)) != 0)
                        {
                            PrintMacAddress (BaseMac, pu1Temp);
                            CliPrintf (CliHandle,
                                       "d-lag system-identifier %s\r\n",
                                       pu1Temp);
                        }
                    }
                    if (i4DLAGSystemPriority != LA_DLAG_DEFAULT_SYSTEM_PRIORITY)
                    {
                        CliPrintf (CliHandle, "d-lag system-priority %d\r\n",
                                   i4DLAGSystemPriority);
                    }
                    if (i4DLAGStatus == LA_DLAG_ENABLED)
                    {
                        CliPrintf (CliHandle, "set d-lag enable\r\n");
                    }
                    if (u4DLAGPeriodicSyncTime !=
                        LA_DLAG_DEFAULT_PERIODIC_SYNC_TIME)
                    {
                        CliPrintf (CliHandle, "d-lag periodic-sync-time %d\r\n",
                                   u4DLAGPeriodicSyncTime);
                    }
                    if (u4DLAGMSSelectionWaitTime !=
                        LA_DLAG_DEFAULT_MS_SELECTION_WAIT_TIME)
                    {
                        CliPrintf (CliHandle,
                                   "d-lag master-slave-selection-wait-time %d\r\n",
                                   u4DLAGMSSelectionWaitTime);
                    }
                    if (i4DLAGRedundancy == LA_DLAG_REDUNDANCY_ON)
                    {
                        CliPrintf (CliHandle, "d-lag redundancy on\r\n");
                    }
                }
            }
#endif
        }
        else
        {
            MEMSET (au1TmpMac, 0, MAX_LEN_TEMP_ARRAY);

            pu1Temp = &au1TmpMac[0];
#ifdef ICCH_WANTED
            /* Display the MC-LAG System Identifier command */
            if ((nmhGetFsLaPortChannelMCLAGSystemID
                 (i4Index, (tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
            {
                LaCfaGetSysMacAddress (DefaultNvRamMacAddr);
                LaGetAggEntry ((UINT2) i4Index, &pAggEntry);

                /* Display the command, if the system mac id is not zero */

                if (((LA_MEMCMP (&BaseMac, TempMacAddr,
                                 LA_MAC_ADDRESS_SIZE)) != 0))

                {
                    /* Display the command only if the System identifier is 
                     * configured explicitly*/
                    if (pAggEntry != NULL)
                    {
                        if ((pAggEntry->bIsMCLAGSystemIdConfigured) == LA_TRUE)
                        {

                            PrintMacAddress (BaseMac, pu1Temp);
                            CliPrintf (CliHandle,
                                       "mc-lag system-identifier %s\r\n",
                                       pu1Temp);
                        }
                    }
                }
            }
            /* Display the MC-LAG system priority command */
            nmhGetFsLaPortChannelMCLAGSystemPriority (i4Index,
                                                      &i4MCLAGSystemPriority);

            if (i4MCLAGSystemPriority != LA_DLAG_DEFAULT_SYSTEM_PRIORITY)
            {
                CliPrintf (CliHandle, "mc-lag system-priority %d\r\n",
                           i4MCLAGSystemPriority);
            }
            if (i4MCLAGStatus == LA_MCLAG_ENABLED)
            {
                CliPrintf (CliHandle, "set mc-lag enable\r\n");
            }
#endif

        }
    }
    if (i1Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    FsUtilReleaseBitList ((UINT1 *) pDistributePorts);
    LA_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : LaSetActorPortChannelMaxPorts
 *
 *     DESCRIPTION      : This function configures maximum no of ports for a
 *                        given port channel.
 *
 *     INPUT            : CliHandle - CLI Handler
 *                        u4MaxPorts - maximum ports to be set.
 *                        u4AggIndex - AggIndex for which  maximum no of ports
 *                        needs to be set
 *
 *     OUTPUT           : NONE
 *
 *     RETURNS          : Success / Failure
 *
 *****************************************************************************/

INT4
LaSetActorPortChannelMaxPorts (tCliHandle CliHandle, UINT4 u4AggMaxPorts,
                               UINT4 u4AggIndex)
{
    INT4                i4AggIfIndex = 0;
    UINT4               u4Error = 0;
    tLaLacAggEntry     *pAggEntry = NULL;

    LaGetAggEntry ((UINT2) u4AggIndex, &pAggEntry);

    /* If AggEntry is not created then return failure */
    if (pAggEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Port channel\r\n");
        return CLI_FAILURE;
    }

    i4AggIfIndex = (INT4) pAggEntry->u2AggIndex;

    if (nmhTestv2FsLaPortChannelMaxPorts (&u4Error, i4AggIfIndex,
                                          (INT4) u4AggMaxPorts) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid number of ports "
                   "per port-channel\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsLaPortChannelMaxPorts (i4AggIfIndex, (INT4) u4AggMaxPorts)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : LaCliSetTraceLevel
 *
 *     DESCRIPTION      : This function configures maximum no of ports for a
 *                        given port channel.
 *
 *     INPUT            : CliHandle - CLI Handler
 *                        i4CliTraceVal - Trace value.
 *                        u1TraceFlag - Enable/Disable trace
 *
 *     OUTPUT           : NONE
 *
 *     RETURNS          : Success / Failure
 *
 *****************************************************************************/

INT4
LaCliSetTraceLevel (tCliHandle CliHandle, INT4 i4CliTraceVal, UINT1 u1TraceFlag)
{

    INT4                i4TraceVal = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;

    /* Get La Trace Option */
    nmhGetFsLaTraceOption (&i4TraceVal);

    /* Warning message to enable the all LA trace messages */
    if ((i4CliTraceVal == LA_ALL_TRC) && (u1TraceFlag == CLI_ENABLE))
    {
        i4RetVal = CliDisplayMessageAndUserPromptResponse
            ("This will enable all LACP traces ", 1, LaLock, LaUnLock);
        CliPrintf (CliHandle, "\r\n");
        if (i4RetVal != CLI_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4CliTraceVal;
    }
    else
    {
        i4TraceVal &= (~(i4CliTraceVal));
    }

    if (nmhTestv2FsLaTraceOption (&u4ErrorCode, i4TraceVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsLaTraceOption (i4TraceVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : IssLaShowDebugging
 *
 *     DESCRIPTION      : This function is used to display debug level for  
 *                        LACP module
 *
 *     INPUT            : CliHandle - CLI Handler
 *
 *     OUTPUT           : NONE
 *
 *     RETURNS          : NONE 
 *
 *****************************************************************************/

VOID
IssLaShowDebugging (tCliHandle CliHandle)
{
    INT4                i4TraceVal = 0;
    nmhGetFsLaTraceOption (&i4TraceVal);

    if (i4TraceVal == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rLA :");
    if ((i4TraceVal & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  LA initialization and shutdown debugging is on");
    }
    if ((i4TraceVal & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  LA management debugging is on");
    }
    if ((i4TraceVal & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  LA data path debugging is on");
    }
    if ((i4TraceVal & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  LA event debugging is on");
    }
    if ((i4TraceVal & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  LA packet dump debugging is on");
    }
    if ((i4TraceVal & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  LA all resources except buffers debugging is on");
    }
    if ((i4TraceVal & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  LA all failure debugging is on");
    }
    if ((i4TraceVal & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  LA buffer debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : LaCliGetShowCmdOutputToFile                          */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT4
LaCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    CHR1               *au1LaShowCmdList[LA_MAX_DYN_CMDS] =
        { "show interfaces etherchannel > ",
        "show etherchannel >> ",
        "show d-lag detail >> ",
        "show d-lag consolidated >> ",
        "show lacp neighbor detail >> "
    };
    INT4                i4Cmd = 0;
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return LA_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < LA_MAX_DYN_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile ((UINT1 *) pu1FileName,
                                       (UINT1 *) au1LaShowCmdList[i4Cmd]) ==
            CLI_FAILURE)
        {
            return LA_FAILURE;
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCliCalcSwAudCheckSum                               */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT4
LaCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[LA_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT2                i2ReadLen;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, LA_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return LA_FAILURE;
    }
    MEMSET (ai1Buf, 0, LA_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (LaCliReadLineFromFile (i4Fd, ai1Buf, LA_CLI_MAX_GROUPS_LINE_LEN,
                                  &i2ReadLen) != LA_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', LA_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    if (FileClose (i4Fd) < 0)
    {
        return LA_FAILURE;
    }

    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCliReadLineFromFile                                */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT1
LaCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (LA_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (LA_CLI_EOF);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetErrorRecTime                                  */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Sets the error recovery time                       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4ErrRecDuration - specifies the error recovery    */
/*                                           duration                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetErrorRecTime (tCliHandle CliHandle, INT4 i4ErrRecDuration)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsLaRecTmrDuration
        (&u4ErrCode, (UINT4) i4ErrRecDuration) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaRecTmrDuration ((UINT4) i4ErrRecDuration) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetErrorRecThreshold                             */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Sets the error recovery counter threshold          */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4ErrRecDuration - specifies the error recovery    */
/*                                           threshold                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetErrorRecThreshold (tCliHandle CliHandle, INT4 i4ErrThreshold)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsLaRecThreshold
        (&u4ErrCode, (UINT4) i4ErrThreshold) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaRecThreshold ((UINT4) i4ErrThreshold) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*******************************************************************************/
/*                                                                             */
/*     FUNCTION NAME    : LaSetPortDefaultedThreshold                          */
/*                                                                             */
/*                                                                             */
/*     DESCRIPTION      : Sets the maximum number of times that a port in      */
/*                          defaulted state can undergo error recovery         */
/*                                                                             */
/*     INPUT            : CliHandle - CLI Handler                              */
/*           i4FsLaPortIndex - Port Number on which the threshold */
/*                      is to be set                      */
/*                        i4PortDefThreshold - specifies the default state     */
/*                                               error recovery threshold      */
/*                                                                             */
/*     OUTPUT           : None                                                 */
/*                                                                             */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                             */
/*******************************************************************************/
INT4
LaSetPortDefaultedThreshold (tCliHandle CliHandle, UINT4 u4FsLaPortIndex,
                             INT4 i4PortDefThreshold)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaPortDefaultedStateThreshold
        (&u4ErrCode, (INT4) u4FsLaPortIndex,
         (UINT4) i4PortDefThreshold) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaPortDefaultedStateThreshold ((INT4) u4FsLaPortIndex,
                                               (UINT4) i4PortDefThreshold) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the Defaulted State Threshold"
                   "on this port\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetDefaultedThreshold                            */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Sets the maximum number of times recovery from     */
/*                        defaulted state can happen                         */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4DefThreshold - specifies the default state       */
/*                                               error recovery threshold    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetDefaultedThreshold (tCliHandle CliHandle, INT4 i4DefThreshold)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaDefaultedStateThreshold (&u4ErrCode,
                                              (UINT4) i4DefThreshold) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaDefaultedStateThreshold ((UINT4) i4DefThreshold) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Defaulted State Threshold\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetHwFailureRecThreshold                         */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Sets the maximum number of times recovery from     */
/*                        hardware failure can happen                        */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4HwFailureRecThreshold - specifies the hardware   */
/*                                       failure error recovery threshold    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetHwFailureRecThreshold (tCliHandle CliHandle, INT4 i4HwFailureRecThreshold)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaHardwareFailureRecThreshold (&u4ErrCode,
                                                  (UINT4)
                                                  i4HwFailureRecThreshold) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaHardwareFailureRecThreshold ((UINT4) i4HwFailureRecThreshold)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the Hardware"
                   "Failure Recovery Threshold\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*******************************************************************************/
/*                                                                             */
/*     FUNCTION NAME    : LaSetPortHwFailureRecThreshold                       */
/*                                                                             */
/*                                                                             */
/*     DESCRIPTION      : Sets the maximum number of times that a port in      */
/*                          defaulted state can undergo error recovery         */
/*                                                                             */
/*     INPUT            : CliHandle - CLI Handler                              */
/*                        u4FsLaPortIndex - Port Number on which the threshold */
/*                                           is to be set                      */
/*                        i4PortHwFailureRecThreshold - specifies the hardware */
/*                                      failure  error recovery threshold      */
/*                                                                             */
/*     OUTPUT           : None                                                 */
/*                                                                             */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                             */
/*******************************************************************************/
INT4
LaSetPortHwFailureRecThreshold (tCliHandle CliHandle, UINT4 u4FsLaPortIndex,
                                INT4 i4PortHwFailureRecThreshold)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaPortHardwareFailureRecThreshold (&u4ErrCode,
                                                      (INT4) u4FsLaPortIndex,
                                                      (UINT4)
                                                      i4PortHwFailureRecThreshold)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaPortHardwareFailureRecThreshold ((INT4) u4FsLaPortIndex,
                                                   (UINT4)
                                                   i4PortHwFailureRecThreshold)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the Hardware failure recovery"
                   "threshold on this port\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetSameStateRecThreshold                         */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Sets the maximum number of times a port stays in   */
/*                        the same state before triggering error recovery    */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4SameStateRecThreshold - specifies the same state */
/*                                              error recovery threshold    */
/*                                                                           */
                                                                                                                                                                                                                                                                                                                                                                           /*     OUTPUT           : None                                               *//*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetSameStateRecThreshold (tCliHandle CliHandle, INT4 i4SameStateRecThreshold)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaSameStateRecThreshold (&u4ErrCode,
                                            (UINT4) i4SameStateRecThreshold) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaSameStateRecThreshold ((UINT4) i4SameStateRecThreshold)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the Same"
                   "State Recovery Threshold\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*******************************************************************************/
/*                                                                             */
/*     FUNCTION NAME    : LaSetPortSameStateRecThreshold                       */
/*                                                                             */
/*     DESCRIPTION      : Sets the maximum number of times that a port stays in*/
/*                        same state before undergoing error recovery          */
/*                                                                             */
/*     INPUT            : CliHandle - CLI Handler                              */
/*                        u4FsLaPortIndex - Port Number on which the threshold */
/*                                           is to be set                      */
/*                        i4PortSameStateRecThreshold - specifies the same     */
/*                                              state recovery threshold       */
/*                                                                             */
/*     OUTPUT           : None                                                 */
/*                                                                             */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                             */
/*******************************************************************************/
INT4
LaSetPortSameStateRecThreshold (tCliHandle CliHandle, UINT4 u4FsLaPortIndex,
                                INT4 i4PortSameStateRecThreshold)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaPortSameStateRecThreshold
        (&u4ErrCode, (INT4) u4FsLaPortIndex,
         (UINT4) i4PortSameStateRecThreshold) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaPortSameStateRecThreshold ((INT4) u4FsLaPortIndex,
                                             (UINT4)
                                             i4PortSameStateRecThreshold) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the Same State recovery threshold"
                   "on this port\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaSetRecThresholdExceedAction                      */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Sets the action to be performed when recovery is   */
/*                        triggered after reaching the threshold             */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4RecThresholdExceedAction - specifies the action  */
/*                           to be performed when recovery is triggered after*/
/*                             reaching the threshold                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaSetRecThresholdExceedAction (tCliHandle CliHandle,
                               UINT4 u4RecThresholdExceedAction)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsLaRecThresholdExceedAction (&u4ErrCode,
                                               (INT4)
                                               u4RecThresholdExceedAction) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsLaRecThresholdExceedAction ((INT4) u4RecThresholdExceedAction)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set the action to be performed on exceeding"
                   "Recovery threshold\r\n");
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaMCLAGClearCounters                               */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Clears MC-LAG periodic and Event Tx and Rx         */
/*                        counters                                           */
/*     INPUT            : CliHandle - CliContext IDCliContext ID             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaMCLAGClearCounters (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4MclagClearCounter = LA_TRUE;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsLaMCLAGClearCounters (&u4ErrorCode, i4MclagClearCounter) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaMCLAGClearCounters (i4MclagClearCounter) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaClearStatistics                                  */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Clears the statistics of bridge,                   */
/*                   port channel and its member ports                  */
/*                                                                           */
/*     INPUT            : CliHandle - CliContext ID                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaClearStatistics (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = LA_INIT_VAL;
    INT4                i4ClearStatistics = LA_ENABLED;

    if (nmhTestv2FsLaClearStatistics (&u4ErrorCode, i4ClearStatistics) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaClearStatistics (i4ClearStatistics) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaPortChannelClearStatistics                       */
/*                                                                           */
/*                                                                           */
/*     DESCRIPTION      : Clears the statistics of bridge,                   */
/*                        port channel and its member ports                  */
/*                                                                           */
/*     INPUT            : CliHandle - CliContext ID                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
LaPortChannelClearStatistics (tCliHandle CliHandle, INT4 i4Key)
{
    UINT4               u4ErrorCode = LA_INIT_VAL;
    INT4                i4ClearStatistics = LA_ENABLED;

    if (nmhTestv2FsLaPortChannelClearStatistics (&u4ErrorCode, i4Key,
                                                 i4ClearStatistics) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsLaPortChannelClearStatistics (i4Key, i4ClearStatistics)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
*
*    Function Name       : LaMCLAGSystemControl
*
*    Description         : This function sets the MCLAG system control
*                          status.
*
*    Input(s)            : CliHandle -
*                          i4Status  - Status of MCLAG System control
*                                     1.LA_START
*                                     2.LA_SHUTDOWN
*
*    Output(s)           : None
*
*    Global Var Referred : None
*
*    Global Var Modified : gLaMclagSystemControl .
*
*    Use of Recursion    : None.
*
*    Exceptions or Operating
*    System Error Handling    : None.
*
*
*    Returns                  : CLI_SUCCESS/CLI_FALURE
*
*****************************************************************************/

INT4
LaMCLAGSystemControl (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsLaMCLAGSystemControl (&u4ErrorCode, i4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsLaMCLAGSystemControl (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#endif /* __LACLI_C__ */
