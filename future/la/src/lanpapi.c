/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: lanpapi.c,v 1.5 2014/06/13 12:55:52 siva Exp $
 *
 * Description: This file contains the HW wrapper changes for
 *              Link Aggregation module.
 *              < part of Dual Unit Stacking >
 *
 *******************************************************************/

#ifndef __LA_NP_API_C__
#define __LA_NP_API_C__

#include "lahdrs.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwCreateAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwCreateAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwCreateAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwCreateAggGroup (UINT2 u2AggIndex, UINT2 *pu2HwAggId)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwCreateAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_CREATE_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwCreateAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->pu2HwAggId = pu2HwAggId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwAddLinkToAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwAddLinkToAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwAddLinkToAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                           UINT2 *pu2HwAggId)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwAddLinkToAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_ADD_LINK_TO_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwAddLinkToAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;
    pEntry->pu2HwAggId = pu2HwAggId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwDlagAddLinkToAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwDlagAddLinkToAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwDlagAddLinkToAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwDlagAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                               UINT2 *pu2HwAggId)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwDlagAddLinkToAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_DLAG_ADD_LINK_TO_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwDlagAddLinkToAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;
    pEntry->pu2HwAggId = pu2HwAggId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwSetSelectionPolicy                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwSetSelectionPolicy
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwSetSelectionPolicy
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwSetSelectionPolicy (UINT2 u2AggIndex, UINT1 u1SelectionPolicy)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwSetSelectionPolicy *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_SET_SELECTION_POLICY,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwSetSelectionPolicy;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u1SelectionPolicy = u1SelectionPolicy;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwSetSelectionPolicyBitList                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwSetSelectionPolicyBitList
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwSetSelectionPolicyBitList
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwSetSelectionPolicyBitList (UINT2 u2AggIndex,
                                   UINT4 u4SelectionPolicyBitList)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwSetSelectionPolicyBitList *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_SET_SELECTION_POLICY_BIT_LIST,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwSetSelectionPolicyBitList;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4SelectionPolicyBitList = u4SelectionPolicyBitList;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwRemoveLinkFromAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwRemoveLinkFromAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwRemoveLinkFromAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwRemoveLinkFromAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwRemoveLinkFromAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwRemoveLinkFromAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwDlagRemoveLinkFromAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwDlagRemoveLinkFromAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwDlagRemoveLinkFromAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwDlagRemoveLinkFromAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwDlagRemoveLinkFromAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_DLAG_REMOVE_LINK_FROM_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwDlagRemoveLinkFromAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));

}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwDeleteAggregator                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwDeleteAggregator
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwDeleteAggregator
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwDeleteAggregator (UINT2 u2AggIndex)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwDeleteAggregator *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_DELETE_AGGREGATOR,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwDeleteAggregator;

    pEntry->u4AggIndex = u2AggIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwDeInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwEnableCollection                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwEnableCollection
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwEnableCollection
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwEnableCollection (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwEnableCollection *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_ENABLE_COLLECTION,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwEnableCollection;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwEnableDistribution                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwEnableDistribution
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwEnableDistribution
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwEnableDistribution (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwEnableDistribution *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_ENABLE_DISTRIBUTION,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwEnableDistribution;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwDisableCollection                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwDisableCollection
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwDisableCollection
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwDisableCollection (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwDisableCollection *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_DISABLE_COLLECTION,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwDisableCollection;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwSetPortChannelStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwSetPortChannelStatus
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwSetPortChannelStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwSetPortChannelStatus (UINT2 u2AggIndex, UINT2 u2Inst, UINT1 u1StpState)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwSetPortChannelStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_SET_PORT_CHANNEL_STATUS,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwSetPortChannelStatus;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u2Inst = u2Inst;
    pEntry->u1StpState = u1StpState;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwAddPortToConfAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwAddPortToConfAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwAddPortToConfAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwAddPortToConfAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwAddPortToConfAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_ADD_PORT_TO_CONF_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwAddPortToConfAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwRemovePortFromConfAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwRemovePortFromConfAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwRemovePortFromConfAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwRemovePortFromConfAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwRemovePortFromConfAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_REMOVE_PORT_FROM_CONF_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwRemovePortFromConfAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;

    return (NpUtilHwProgram (&FsHwNp));

}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwDlagStatus 
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes LaFsLaHwDlagStatus 
 *                                                                          
 *    Input(s)            : Arguments of LaFsLaHwDlagStatus 
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwDlagStatus (UINT1 u1DlagStatus)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwDlagStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_DLAG_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwDlagStatus;

    pEntry->u1DlagStatus = u1DlagStatus;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef L2RED_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaHwCleanAndDeleteAggregator                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaHwCleanAndDeleteAggregator
 *                                                                          
 *    Input(s)            : Arguments of FsLaHwCleanAndDeleteAggregator
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaHwCleanAndDeleteAggregator (UINT2 u2HwAggIndex)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaHwCleanAndDeleteAggregator *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_HW_CLEAN_AND_DELETE_AGGREGATOR,    /* Function/OpCode */
                         u2HwAggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaHwCleanAndDeleteAggregator;

    pEntry->u4HwAggIndex = u2HwAggIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaGetNextAggregator                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaGetNextAggregator
 *                                                                          
 *    Input(s)            : Arguments of FsLaGetNextAggregator
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

INT4
LaFsLaGetNextAggregator (INT4 i4HwAggIndex, tLaHwInfo * pHwInfo)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaGetNextAggregator *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_GET_NEXT_AGGREGATOR,    /* Function/OpCode */
                         i4HwAggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaGetNextAggregator;

    pEntry->i4HwAggIndex = i4HwAggIndex;
    pEntry->pHwInfo = pHwInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (LA_INVALID_HW_AGG_IDX);
    }
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaGetNextAggregator;
    return (pEntry->i4NextHwAggIndex);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaRedHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaRedHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsLaRedHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaRedHwInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_RED_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaRedHwNpDeleteAggregator                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaRedHwNpDeleteAggregator
 *                                                                          
 *    Input(s)            : Arguments of FsLaRedHwNpDeleteAggregator
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaRedHwNpDeleteAggregator (UINT2 u2AggIndex)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaRedHwNpDeleteAggregator *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_RED_HW_NP_DELETE_AGGREGATOR,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaRedHwNpDeleteAggregator;

    pEntry->u4AggIndex = u2AggIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaRedHwUpdateDBForAggregator                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaRedHwUpdateDBForAggregator
 *                                                                          
 *    Input(s)            : Arguments of FsLaRedHwUpdateDBForAggregator
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaRedHwUpdateDBForAggregator (UINT2 u2AggIndex, tPortList ConfigPorts,
                                  tPortList ActivePorts,
                                  UINT1 u1SelectionPolicy)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaRedHwUpdateDBForAggregator *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_RED_HW_UPDATE_D_B_FOR_AGGREGATOR,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         2);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaRedHwUpdateDBForAggregator;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->ConfigPorts = ConfigPorts;
    pEntry->ActivePorts = ActivePorts;
    pEntry->u1SelectionPolicy = u1SelectionPolicy;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaRedHwNpUpdateDlfMcIpmcPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaRedHwNpUpdateDlfMcIpmcPort
 *                                                                          
 *    Input(s)            : Arguments of FsLaRedHwNpUpdateDlfMcIpmcPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaRedHwNpUpdateDlfMcIpmcPort (UINT2 u2AggIndex)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaRedHwNpUpdateDlfMcIpmcPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_RED_HW_NP_UPDATE_DLF_MC_IPMC_PORT,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaRedHwNpUpdateDlfMcIpmcPort;

    pEntry->u4AggIndex = u2AggIndex;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* L2RED_WANTED */
#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaMbsmHwCreateAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaMbsmHwCreateAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaMbsmHwCreateAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaMbsmHwCreateAggGroup (UINT2 u2AggIndex, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaMbsmHwCreateAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_MBSM_HW_CREATE_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwCreateAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaMbsmHwAddPortToConfAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaMbsmHwAddPortToConfAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaMbsmHwAddPortToConfAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaMbsmHwAddPortToConfAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaMbsmHwAddPortToConfAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_MBSM_HW_ADD_PORT_TO_CONF_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwAddPortToConfAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaMbsmHwAddLinkToAggGroup                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaMbsmHwAddLinkToAggGroup
 *                                                                          
 *    Input(s)            : Arguments of FsLaMbsmHwAddLinkToAggGroup
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaMbsmHwAddLinkToAggGroup (UINT2 u2AggIndex, UINT2 u2PortNumber,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaMbsmHwAddLinkToAggGroup *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_MBSM_HW_ADD_LINK_TO_AGG_GROUP,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwAddLinkToAggGroup;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u4PortNumber = u2PortNumber;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaMbsmHwSetSelectionPolicy                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaMbsmHwSetSelectionPolicy
 *                                                                          
 *    Input(s)            : Arguments of FsLaMbsmHwSetSelectionPolicy
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaMbsmHwSetSelectionPolicy (UINT2 u2AggIndex, UINT1 u1SelectionPolicy,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaMbsmHwSetSelectionPolicy *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_MBSM_HW_SET_SELECTION_POLICY,    /* Function/OpCode */
                         u2AggIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwSetSelectionPolicy;

    pEntry->u4AggIndex = u2AggIndex;
    pEntry->u1SelectionPolicy = u1SelectionPolicy;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : LaFsLaMbsmHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsLaMbsmHwInit
 *                                                                          
 *    Input(s)            : Arguments of FsLaMbsmHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
LaFsLaMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tLaNpModInfo       *pLaNpModInfo = NULL;
    tLaNpWrFsLaMbsmHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_LA_MOD,    /* Module ID */
                         FS_LA_MBSM_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pLaNpModInfo = &(FsHwNp.LaNpModInfo);
    pEntry = &pLaNpModInfo->LaNpFsLaMbsmHwInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

#endif /* __LA_NP_API_C__ */
