/*****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: lautils.c,v 1.107 2017/11/16 14:26:55 siva Exp $
* Licensee Aricent Inc., 2001-2002
*****************************************************************************/
/*    FILE  NAME            : lautils.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREGATION                               */
/*    MODULE NAME           : Link Aggregation utility module.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains utility functions used by   */
/*                            the Link Aggregation module                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                       DESCRIPTION OF CHANGE              */
/*            MODIFIED BY                 FAULT REPORT NO                    */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/              Initial Create.                    */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#define LA_UTILS
#include "lahdrs.h"
#include "fscfacli.h"
#include "hb.h"
#include "pvrst.h"
UINT1               gau1LaPortBitMaskMap[LA_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};
INT4                LaHandleAggProgramFailed (UINT2 u2AggId);

/*****************************************************************************/
/* Function Name      : LaAggGetNextTransactionId                            */
/*                                                                           */
/* Description        : This function is called by the GenerateMarkerPDU     */
/*                      function, in order to get the next transaction Id to */
/*                      be used in the Marker Request PDU which is to be sent*/
/*                                                                           */
/* Input(s)           : u2AggIndex - The index of the aggregator             */
/*                                                                           */
/* Output(s)          : pu4TransactionId - The next transaction Id to be used*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaAggGetNextTransactionId (UINT2 u2AggIndex, UINT4 *pu4TransactionId)
{
    tLaLacAggEntry     *pAggEntry;
    UINT4               u4TId;

    LaGetAggEntry (u2AggIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return;
    }
    u4TId = pAggEntry->u4TransId;

    if (++u4TId > LA_MAX_MARKER_TRANS_ID)
    {
        u4TId = 0;
        ++u4TId;
    }

    *pu4TransactionId = u4TId;
    pAggEntry->u4TransId = u4TId;

}

/*****************************************************************************/
/* Function Name      : LaAggCacheEntryTableHashFn                           */
/*                                                                           */
/* Description        : This function generates a hash index value based on  */
/*                      the MAC Address (Source / Destination MAC) given as  */
/*                      the input to this function.                          */
/*                                                                           */
/* Input(s)           : pLaMacAddress - MAC Address, based on which the hash */
/*                                      index is calculated.                 */
/*                                                                           */
/* Output(s)          : pu4HashIndex - Hash Index value to be returned       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaAggCacheEntryTableHashFn (tLaMacAddr * pLaMacAddress, UINT4 *pu4HashIndex)
{
    UINT2               u2Val;

    LA_MEMCPY (&u2Val, ((UINT1 *) pLaMacAddress + 4), 2);
    u2Val = (UINT2) (LA_NTOHS (u2Val));
    *pu4HashIndex = (u2Val) % LA_CACHE_TABLE_SIZE;

}

/*****************************************************************************/
/* Function Name      : LaGetAggEntry                                        */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator  */
/*                      Info from the Global structure.                      */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - Index of Aggregator for whose entry, a  */
/*                                   pointer is to be returned               */
/*                                                                           */
/* Output(s)          : ppAggEntry - Address of the Pointer to the           */
/*                                       Aggregator Entry                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaAggList                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
LaGetAggEntry (UINT2 u2AggIndex, tLaLacAggEntry ** ppAggEntry)
{
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_SLL_SCAN (LA_AGG_SLL, pAggEntry, tLaLacAggEntry *)
    {
        if (pAggEntry->u2AggIndex == u2AggIndex)
        {
            *ppAggEntry = pAggEntry;
            return LA_SUCCESS;
        }
    }
    *ppAggEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaGetNextAggEntry                                    */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator  */
/*                      Info from the Global structure.                      */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Address of the Pointer to the            */
/*                                       Aggregator Entry                    */
/*                                                                           */
/* Output(s)          : pNextAggEntry - Address of the Pointer to the        */
/*                       Next Aggregator Entry(ie Next to pAggEntry)         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaAggList                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*                    : LA_FAILURE                                           */
/*****************************************************************************/

INT4
LaGetNextAggEntry (tLaLacAggEntry * pAggEntry, tLaLacAggEntry ** pNextAggEntry)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;
    if (pAggEntry == NULL)
    {
        pTmpAggEntry = (tLaLacAggEntry *) LA_SLL_NEXT (LA_AGG_SLL, NULL);
    }
    else
    {
        pTmpAggEntry = (tLaLacAggEntry *) LA_SLL_NEXT (LA_AGG_SLL,
                                                       &pAggEntry->NextNode);
    }

    if (pTmpAggEntry != NULL)
    {
        *pNextAggEntry = pTmpAggEntry;
        return LA_SUCCESS;
    }
    *pNextAggEntry = NULL;
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaGetAggEntryByKey                                   */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator  */
/*                      Info from the Global structure based on AdminKey     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2Key - Admin Key of the Aggregator                  */
/*                                                                           */
/* Output(s)          : ppRetAggEntry - Address of the Pointer to the        */
/*                       Aggregator Entry(Based on the AdminKey)             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*                    : LA_FAILURE                                           */
/*****************************************************************************/

INT4
LaGetAggEntryByKey (UINT2 u2Key, tLaLacAggEntry ** ppRetAggEntry)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;

    LaGetNextAggEntry (NULL, &pTmpAggEntry);
    while (pTmpAggEntry != NULL)
    {
        if (pTmpAggEntry->AggConfigEntry.u2ActorAdminKey == u2Key)
        {
            *ppRetAggEntry = pTmpAggEntry;
            return LA_SUCCESS;
        }
        LaGetNextAggEntry (pTmpAggEntry, &pTmpAggEntry);
    }
    *ppRetAggEntry = NULL;
    return LA_FAILURE;

}

/*****************************************************************************/
/* Function Name      : LaGetDefPortEntryForAgg                              */
/*                                                                           */
/* Description        : This function returns the default PortEntry for the  */
/*                      given AggEntry                                       */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry - Pointer to the Aggregator Entry          */
/*                                                                           */
/* Output(s)          : ppRetPortEntry - Address of the Pointer to the       */
/*                                       Default PortEntry (NULL, if none)   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS                                           */
/*                    : LA_FAILURE                                           */
/*****************************************************************************/
INT4
LaGetDefPortEntryForAgg (tLaLacAggEntry * pAggEntry,
                         tLaLacPortEntry ** ppRetPortEntry)
{
    tLaLacPortEntry    *pCheckPortEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;

    *ppRetPortEntry = NULL;

    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    UTL_SLL_OFFSET_SCAN (&(gLaGlobalInfo.LaDynReallocPortList),
                         pCheckPortEntry, pTmpPortEntry, tLaLacPortEntry *)
    {
        /* Check whether the Port's DefAggEntry is same as the given
         * port channel's AggEntry. If so, this is the Default port
         * for the port-channel */
        if (pCheckPortEntry->pDefAggEntry == pAggEntry)
        {
            *ppRetPortEntry = pCheckPortEntry;
            return LA_SUCCESS;
        }
    }

    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaAddToAggTable                                      */
/*                                                                           */
/* Description        : This function adds a AggEntry to the global AGGLIST  */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2Key - Admin Key of the Aggregator                  */
/*                                                                           */
/* Output(s)          : pAggEntry -  Pointer to the Aggregator Entry that    */
/*                      should be added to the global LaAggList.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaAggList                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaAddToAggTable (tLaLacAggEntry * pAggEntry)
{
    tLaLacAggEntry     *pTmpAggEntry = NULL;
    tLaLacAggEntry     *pPrevAggEntry = NULL;

    /* Entry addition is done in assending order of Aggregator Index */

    LA_SLL_SCAN (LA_AGG_SLL, pTmpAggEntry, tLaLacAggEntry *)
    {
        if (pTmpAggEntry->u2AggIndex < pAggEntry->u2AggIndex)
        {
            pPrevAggEntry = pTmpAggEntry;
            continue;
        }
        else
        {
            break;
        }
    }
    LA_SLL_INSERT (LA_AGG_SLL, &pPrevAggEntry->NextNode, &pAggEntry->NextNode);
    return;
}

/*****************************************************************************/
/* Function Name      : LaDeleteFromAggTable                                 */
/*                                                                           */
/* Description        : This function removes a AggEntry from the            */
/*                      global AGGLIST.This function is called whenever a    */
/*                      portchannel is deleted.                              */
/*                                                                           */
/* Input(s)           : pAggEntry -  Pointer to the Aggregator Entry that    */
/*                      should be added to the global LaAggList.             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaAggList                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDeleteFromAggTable (tLaLacAggEntry * pAggEntry)
{
    LA_SLL_DELETE (LA_AGG_SLL, &pAggEntry->NextNode);
    LA_SLL_INIT_NODE (&pAggEntry->NextNode);
    return;
}

/*****************************************************************************/
/* Function Name      : LaAddToAggPortTable                                  */
/*                                                                           */
/* Description        : This function adds a Port to the portlist of the     */
/*                      Aggregator.This function is called whenever a port   */
/*                      is assigned an adminkey                              */
/*                                                                           */
/* Input(s)           : pAggEntry -  Pointer to the Aggregator Entry.        */
/*                      pPortEntry  - Pointer to the port entry that should  */
/*                      added to the pAggEntry configured port list.         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaAddToAggPortTable (tLaLacAggEntry * pAggEntry, tLaLacPortEntry * pPortEntry)
{
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacPortEntry    *pPrevPortEntry = NULL;

    LA_SLL_SCAN (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                 pTmpPortEntry, tLaLacPortEntry *)
    {
        if (pTmpPortEntry->u2PortIndex < pPortEntry->u2PortIndex)
        {
            pPrevPortEntry = pTmpPortEntry;
            continue;
        }
        else
        {
            break;
        }
    }
    LA_SLL_INSERT (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                   &pPrevPortEntry->NextPortEntry, &pPortEntry->NextPortEntry);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetNextAggPortEntry                                */
/*                                                                           */
/* Description        : This function is used to get the next port from the  */
/*                      aggregator configured list.                          */
/*                                                                           */
/* Input(s)           : pAggEntry -  Pointer to the Aggregator Entry .       */
/*                      pPortEntry - Pointer to the portEntry                */
/*                                                                           */
/* Output(s)          : ppNextPortEntry-Pointer to the next port entry in the*/
/*                      aggregator configure port list.                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaGetNextAggPortEntry (tLaLacAggEntry * pAggEntry,
                       tLaLacPortEntry * pPortEntry,
                       tLaLacPortEntry ** ppNextPortEntry)
{
    if (pPortEntry == NULL)
    {
        *ppNextPortEntry = (tLaLacPortEntry *)
            LA_SLL_NEXT (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry), NULL);
    }
    else
    {
        *ppNextPortEntry = (tLaLacPortEntry *)
            LA_SLL_NEXT (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                         &pPortEntry->NextPortEntry);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetNextAggDistributingPortEntry                    */
/*                                                                           */
/* Description        : This function is used to get the next port for which */
/*                      the distributing is enabled.                         */
/*                                                                           */
/* Input(s)           : pAggEntry -  Pointer to the Aggregator Entry         */
/*                      pPortEntry - Pointer to the Distributing portEntry   */
/*                                                                           */
/* Output(s)          : ppNextPortEntry-Pointer to the next port entry in the*/
/*                      aggregator configure port list.                      */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaGetNextAggDistributingPortEntry (tLaLacAggEntry * pAggEntry,
                                   tLaLacPortEntry * pPortEntry,
                                   tLaLacPortEntry ** ppNextPortEntry)
{
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    *ppNextPortEntry = NULL;

    pTmpPortEntry = (tLaLacPortEntry *)
        LA_SLL_NEXT (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                     &pPortEntry->NextPortEntry);
    while (pTmpPortEntry != NULL)
    {
        if (pTmpPortEntry->LaLacActorInfo.
            LaLacPortState.LaDistributing == LA_TRUE)
        {
            break;
        }
        pTmpPortEntry = (tLaLacPortEntry *)
            LA_SLL_NEXT (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                         &pTmpPortEntry->NextPortEntry);
    }

    if (pTmpPortEntry == NULL)
    {
        return LA_FAILURE;
    }
    *ppNextPortEntry = pTmpPortEntry;
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaUtilGetNextAggPort                                 */
/*                                                                           */
/* Description        : This function is used to get the next port which is  */
/*                      configured in the aggregator port list.              */
/*                                                                           */
/* Input(s)           : u2AggIndex - PortChannel IfIndex                     */
/*                      u2Port  - PortIndex                                  */
/*                                                                           */
/* Output(s)          : pu2NextPort -Pointer to the next port.               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaUtilGetNextAggPort (UINT2 u2AggIndex, UINT2 u2Port, UINT2 *pu2NextPort)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;

    *pu2NextPort = 0;
    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    if (u2Port != 0)
    {
        LaGetPortEntry (u2Port, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return LA_FAILURE;
        }
    }
    LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);
    if (pNextPortEntry == NULL)
    {
        return LA_FAILURE;
    }
    *pu2NextPort = pNextPortEntry->u2PortIndex;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetNextAggDistributingPort                         */
/*                                                                           */
/* Description        : This function is used to get the next port for which */
/*                      the distributing is enabled.                         */
/*                                                                           */
/* Input(s)           : u2AggIndex - Aggregator IfIndex                      */
/*                      u2Port  - Distributing portIndex                     */
/*                                                                           */
/* Output(s)          : pu2NexPort Pointer to the next distributing port     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaGetNextAggDistributingPort (UINT2 u2AggIndex, UINT2 u2Port,
                              UINT2 *pu2NextPort)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaLacPortEntry    *pNextPortEntry = NULL;
    tLaCallBackArgs     LaCallBackArgs;
    INT4                i4RetVal = LA_FAILURE;

    LA_MEMSET (&LaCallBackArgs, 0, sizeof (tLaCallBackArgs));

    *pu2NextPort = 0;
    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    if (u2Port != 0)
    {
        LaGetPortEntry (u2Port, &pPortEntry);
        if (pPortEntry == NULL)
        {
            return LA_FAILURE;
        }
    }
    LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pNextPortEntry);
    while (pNextPortEntry != NULL)
    {
        LaCallBackArgs.u2Port = pNextPortEntry->u2PortIndex;
        i4RetVal = LaCustCallBack (LA_GET_DISTRIB_PORT_EVENT, &LaCallBackArgs);

        if ((pNextPortEntry->LaLacActorInfo.
             LaLacPortState.LaDistributing == LA_TRUE)
            && (i4RetVal == LA_SUCCESS) &&
            (LaCallBackArgs.u2PortState == LA_OPER_UP))
        {
            break;
        }
        LaGetNextAggPortEntry (pAggEntry, pNextPortEntry, &pNextPortEntry);
    }
    if (pNextPortEntry == NULL)
    {
        return LA_FAILURE;
    }
    *pu2NextPort = pNextPortEntry->u2PortIndex;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeleteFromAggPortTable                             */
/*                                                                           */
/* Description        : This function removes a Port from the portlist of the*/
/*                      Aggregator.This function is called whenever a port   */
/*                      lacp mode is disabled                                */
/*                                                                           */
/* Input(s)           : pAggEntry -  Pointer to the Aggregator Entry.        */
/*                      pPortEntry  - Pointer to the port entry that should  */
/*                      be removed from the pAggEntry configured port list.  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE.                               */
/*****************************************************************************/

INT4
LaDeleteFromAggPortTable (tLaLacAggEntry * pAggEntry,
                          tLaLacPortEntry * pPortEntry)
{
    tCfaIfInfo          CfaIfInfo;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif

    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* when the port is removed from the bundle , the interface MTU of
     * the port is restored to the original MTU of the port which the
     * user has given for the physical interface
     */
    CfaIfInfo.u4IfMtu = pPortEntry->u4RestoreMtu;

    /* when the port is removed from the bundle , the interface pause admin 
     * mode of the port is restored to the original pause admin mode of the 
     * port which the user has given for the physical interfacie*/
    CfaIfInfo.u1PauseAdminNode = (UINT1) pPortEntry->i4RestorePauseAdminMode;

    /* Restore the old MTU value */
    LaCfaSetIfInfo (IF_MTU, (UINT4) (pPortEntry->u2PortIndex), &CfaIfInfo);

    /* Restore the old Flow control value */
    LaCfaSetIfInfo (IF_PAUSE_ADMIN_MODE, (UINT4) (pPortEntry->u2PortIndex),
                    &CfaIfInfo);
#ifdef NPAPI_WANTED
    if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
    {
        /* Program the new MTU in h/w */
        if (FNP_FAILURE == CfaFsCfaHwSetMtu (pPortEntry->u2PortIndex,
                                             CfaIfInfo.u4IfMtu))
        {
            LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Failed to set MTU for port :%d\n",
                         pPortEntry->u2PortIndex);
        }
    }
#endif

    LA_SLL_DELETE (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                   &pPortEntry->NextPortEntry);
    LA_SLL_INIT_NODE (&pPortEntry->NextPortEntry);

#ifdef SNMP_2_WANTED
    /* Send notification for configuring the mtu of the individual 
     * interfaces */
    SnmpNotifyInfo.pu4ObjectId = IfMainMtu;
    SnmpNotifyInfo.u4OidLen = sizeof (IfMainMtu) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          pPortEntry->u2PortIndex, CfaIfInfo.u4IfMtu));
#endif

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetAggEntryBasedOnPort                             */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator  */
/*                      Information to which a port is assigned.             */
/*                                                                           */
/* Input(s)           : u2AggIndex - Index of Aggregator for whose entry, a  */
/*                                   pointer is to be returned               */
/*                                                                           */
/* Output(s)          : ppAggEntry - Address of the Pointer to the           */
/*                                   Aggregator Entry                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
LaGetAggEntryBasedOnPort (UINT2 u2PortIndex, tLaLacAggEntry ** ppAggEntry)
{
    tLaLacPortEntry    *pPortEntry;

    *ppAggEntry = NULL;
    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry != NULL)
    {
        *ppAggEntry = pPortEntry->pAggEntry;
        if (*ppAggEntry != NULL)
        {
            return LA_SUCCESS;
        }
    }
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaGetPortEntry                                       */
/*                                                                           */
/* Description        : This function returns the pointer to the Port entry  */
/*                      from the Global structure.                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of port for whose entry, a       */
/*                                    pointer is to be returned              */
/*                                                                           */
/* Output(s)          : ppPortEntry - Address of Pointer to Port Entry  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apLaLacPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/
INT4
LaGetPortEntry (UINT2 u2PortIndex, tLaLacPortEntry ** ppPortEntry)
{
    if ((u2PortIndex < LA_MIN_PORTS) || (u2PortIndex > LA_MAX_PORTS))
    {
        *ppPortEntry = NULL;
        return LA_FAILURE;
    }

    if ((*ppPortEntry =
         gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1]) == NULL)
    {
        *ppPortEntry = NULL;
        return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetNextPortEntry                                   */
/*                                                                           */
/* Description        : This function returns the pointer to the Next Port   */
/*                      entry from the Global structure.                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the Present PortEntry        */
/*                                                                           */
/* Output(s)          : ppNextPortEntry - Address of Pointer to NextPort Entry */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apLaLacPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaGetNextPortEntry (tLaLacPortEntry * pPortEntry,
                    tLaLacPortEntry ** ppNextPortEntry)
{
    UINT2               u2PortIndex;

    if (pPortEntry == NULL)
    {
        u2PortIndex = 0;
    }
    else
    {
        u2PortIndex = pPortEntry->u2PortIndex;
    }
    u2PortIndex++;

    *ppNextPortEntry = NULL;
    for (; u2PortIndex <= LA_MAX_PORTS; u2PortIndex++)
    {
        if ((gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1]) != NULL)
        {
            *ppNextPortEntry = gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1];
            return LA_SUCCESS;
        }
    }
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaAddToPortTable                                     */
/*                                                                           */
/* Description        : This function adds a port into the global structure  */
/*                      It is called whenever a new port is created          */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the PortEntry to be added    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apLaLacPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaAddToPortTable (tLaLacPortEntry * pPortEntry)
{
    UINT2               u2PortIndex;

    u2PortIndex = pPortEntry->u2PortIndex;

    if (u2PortIndex == 0 || u2PortIndex > LA_MAX_PORTS)
        return LA_FAILURE;

    if (gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1] != NULL)
    {
        return LA_FAILURE;
    }
    gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1] = pPortEntry;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaDeleteFromPortTable                                */
/*                                                                           */
/* Description        : This function removed a port into the globalstructure */
/*                      It is called whenever a new port is deleted          */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to the PortEntry to be deleted  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apLaLacPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/

INT4
LaDeleteFromPortTable (tLaLacPortEntry * pPortEntry)
{
    UINT2               u2PortIndex;

    u2PortIndex = pPortEntry->u2PortIndex;

    if (u2PortIndex == 0 || u2PortIndex > LA_MAX_PORTS)
        return LA_FAILURE;
    if (gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1] == NULL)
    {
        return LA_FAILURE;
    }

    gLaGlobalInfo.apLaLacPortEntry[u2PortIndex - 1] = NULL;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetAggConfigEntry                                  */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator  */
/*                      configuration entry through the Global structure.    */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - Index of the Aggregator for which the   */
/*                                   the pointer is to be returned           */
/*                                                                           */
/* Output(s)          : ppLaLacAggConfigEntry - Address of the Pointer to the*/
/*                                              Aggregator Config Entry      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.aLaLacAggInfo                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaGetAggConfigEntry (UINT2 u2AggIndex,
                     tLaLacAggConfigEntry ** ppLaLacAggConfigEntry)
{
    tLaLacAggEntry     *pAggEntry;

    LaGetAggEntry (u2AggIndex, &pAggEntry);

    if (pAggEntry != NULL)
    {
        *ppLaLacAggConfigEntry = &(pAggEntry->AggConfigEntry);
    }
    else
    {
        *ppLaLacAggConfigEntry = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LaGetAggIfEntry                                      */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator  */
/*                      interface entry through the Global structure.        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - Index of the Aggregator for which the   */
/*                                   the pointer is to be returned           */
/*                                                                           */
/* Output(s)          : ppLaLacAggIfEntry - Address of the Pointer to the    */
/*                                          Aggregator Interface Entry       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.aLaLacAggInfo                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaGetAggIfEntry (UINT2 u2AggIndex, tLaLacAggIfEntry ** ppLaLacAggIfEntry)
{
    tLaLacAggEntry     *pAggEntry;

    LaGetAggEntry (u2AggIndex, &pAggEntry);

    *ppLaLacAggIfEntry = &(pAggEntry->AggIfEntry);
    return;

}

/*****************************************************************************/
/* Function Name      : LaGetAggPortListEntry                                */
/*                                                                           */
/* Description        : This function returns the pointer to the Aggregator's*/
/*                      port list entry through the Global structure.        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - Index of aggregator for whose port list */
/*                                   entry, a pointer needs to be returned   */
/*                                                                           */
/* Output(s)          : ppLaLacAggPortListEntry - Address of the Pointer to  */
/*                                                the Aggregator Port list   */
/*                                                Entry                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.au4PortList                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaGetAggPortListEntry (UINT2 u2AggIndex, tLaPortList ** ppLaLacAggPortListEntry)
{
    tLaLacAggEntry     *pAggEntry;

    LaGetAggEntry (u2AggIndex, &pAggEntry);

    *ppLaLacAggPortListEntry = &(pAggEntry->LaActivePorts);

}

/*****************************************************************************/
/* Function Name      : LaGetLinkSelectionPolicy                             */
/*                                                                           */
/* Description        : This function returns the Aggregator LinkSelection   */
/*                      policy.                                              */
/*                                                                           */
/* Input(s)           : u2AggIndex - IfIndex of aggregator.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MAC_SRC/MAC_DST/MAC_SRC_DST/IP_SRC/IP_DST/IP_SRC_DST */
/*****************************************************************************/

INT4
LaGetLinkSelectPolicy (UINT2 u2AggIndex)
{
    tLaLacAggEntry     *pAggEntry;

    LaGetAggEntry (u2AggIndex, &pAggEntry);

    if (pAggEntry == NULL)
    {
        return LA_OTHER_OPTIONS;
    }

    return (pAggEntry->AggConfigEntry.LinkSelectPolicy);
}

/*****************************************************************************/
/* Function Name      : LaValidatePortEntry                                  */
/*                                                                           */
/* Description        : This function returns success if an entry with this  */
/*                      port index exists; else it returns failure.          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2PortIndex - The port index for which a check must  */
/*                                    be done to see if a valid entry exists */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.apLaLacPortEntry                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/
INT4
LaValidatePortEntry (UINT2 u2PortIndex)
{

    if ((u2PortIndex == 0) || (u2PortIndex > LA_MAX_PORTS))
        return LA_FAILURE;
    /* Validating the entry */
    if ((LA_GET_PORTENTRY (u2PortIndex)) == NULL)
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "UTL: Port %d does not exist\n", u2PortIndex);
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaValidateAggEntry                                   */
/*                                                                           */
/* Description        : This function returns success if an aggregator entry */
/*                      with this index exists; else it returns failure.     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2AggIndex - The Aggregator index for which a check  */
/*                                   must be done to see if a valid entry    */
/*                                   exists.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.aLaLacAggInfo                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/
INT4
LaValidateAggEntry (UINT2 u2AggIndex)
{
    tLaLacAggEntry     *pAggEntry;

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    /* Validating the entry */
    if (pAggEntry == NULL)
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "UTL: Agg %d does not exist\n", u2AggIndex);
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIsPortPartnerPresent                               */
/*                                                                           */
/* Description        : This function returns whether the partner for this   */
/*                      port is active/not .                                 */
/*                                                                           */
/* Input(s)           : u2PortIndex - PortIndex.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE- If Partner Present                          */
/*                      LA_FALSE- If Partner Not Present                     */
/*****************************************************************************/

tLaBoolean
LaIsPortPartnerPresent (UINT2 u2PortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry != NULL)
    {
        return (pPortEntry->bPartnerActive);
    }
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaIsPortInDefaulted                                  */
/*                                                                           */
/* Description        : This function returns whether the port is in         */
/*                            Defaulted state.                               */
/*                                                                           */
/* Input(s)           : u2PortIndex - PortIndex.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE- If Partner Present                          */
/*                      LA_FALSE- If Partner Not Present                     */
/*****************************************************************************/
tLaBoolean
LaIsPortInDefaulted (UINT2 u2PortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    LaGetPortEntry (u2PortIndex, &pPortEntry);
    if (pPortEntry != NULL)
    {
        return (pPortEntry->InDefaulted);
    }
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaGetPortState                                       */
/*                                                                           */
/* Description        : This function gets the actor's/partner's port state  */
/*                      variables encoded as individual bits within a single */
/*                      octet, and returns it as a 1 byte value.             */
/*                                                                           */
/* Input(s)           : pLaLacPortState - Pointer to the Actor's/Partner's   */
/*                                     Admin/Oper Information's Port state   */
/*                                                                           */
/* Output(s)          : pu1PortState - The port state of the corresponding   */
/*                                     entity                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaGetPortState (tLaLacPortState * pLaLacPortState, UINT1 *pu1PortState)
{
    UINT1               u1Val;

    u1Val = 0;
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaLacpActivity));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaLacpTimeout));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaAggregation));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaSynchronization));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaCollecting));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaDistributing));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaDefaulted));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacPortState->LaExpired));

    *pu1PortState = u1Val;

}

/*****************************************************************************/
/* Function Name      : LaChangePartnerPortState                             */
/*                                                                           */
/* Description        : This function sets the partner's port state variables*/
/*                      This function is called by the low-level routines    */
/*                      and is used to either set or reset the port state    */
/*                      bits.                                                */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of the port whose port state has */
/*                                    to be changed                          */
/*                      u1PortStateVar - The Port state bit to be set or     */
/*                                       reset                               */
/*                      u1Value - Indicates the value to which the PortState */
/*                                variable needs to be changed to (SET or    */
/*                                RESET)                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LaChangePartnerPortState (UINT2 u2PortIndex, UINT1 u1PortStateVar, UINT1 u1Set)
{
    tLaLacPortEntry    *pPortEntry;
    tLaLacAggEntry     *pAggEntry = NULL;

    if (LaGetPortEntry (u2PortIndex, &pPortEntry) == LA_FAILURE)
    {
        return;
    }

    pAggEntry = pPortEntry->pAggEntry;

    switch (u1PortStateVar)
    {
        case LA_LACP_ACTIVITY:
            /* Update the Partner's Admin and Oper values for Lacp Activity  */
            if (u1Set == LA_SET)
            {
                if (pPortEntry->
                    LaLacPartnerAdminInfo.LaLacPortState.LaLacpActivity ==
                    LA_ACTIVE)
                {
                    return;
                }
                else
                {
                    pPortEntry->
                        LaLacPartnerAdminInfo.LaLacPortState.LaLacpActivity =
                        LA_ACTIVE;
                    if (pPortEntry->InDefaulted == LA_TRUE)
                    {
                        pPortEntry->
                            LaLacPartnerInfo.LaLacPortState.LaLacpActivity =
                            LA_ACTIVE;
                    }
                }
            }
            else if (u1Set == LA_RESET)
            {
                if (pPortEntry->
                    LaLacPartnerAdminInfo.LaLacPortState.LaLacpActivity ==
                    LA_PASSIVE)
                {
                    return;
                }
                else
                {
                    pPortEntry->
                        LaLacPartnerAdminInfo.LaLacPortState.LaLacpActivity =
                        LA_PASSIVE;

                    if (pPortEntry->InDefaulted == LA_TRUE)
                    {
                        pPortEntry->
                            LaLacPartnerInfo.LaLacPortState.LaLacpActivity =
                            LA_PASSIVE;
                    }
                }
            }

            if ((LA_MODULE_STATUS == LA_ENABLED) &&
                (pPortEntry->LaLacpMode == LA_MODE_LACP) &&
                (pPortEntry->PortUsingPartnerAdminInfo == LA_TRUE))
            {
                /*  LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT); */
                LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);
            }

            LA_TRC (CONTROL_PLANE_TRC | MGMT_TRC,
                    "UTL: LacpPartnerActivity bit value changed SUCCESSFULLY\n");
            break;

        case LA_LACP_TIMEOUT:

            if (u1Set == LA_SET)
            {
                if (pPortEntry->
                    LaLacPartnerAdminInfo.LaLacPortState.LaLacpTimeout ==
                    LA_SHORT_TIMEOUT)
                {
                    return;
                }
                else
                {
                    pPortEntry->
                        LaLacPartnerAdminInfo.LaLacPortState.LaLacpTimeout =
                        LA_SHORT_TIMEOUT;

                    if (pPortEntry->InDefaulted == LA_TRUE)
                    {
                        pPortEntry->
                            LaLacPartnerInfo.LaLacPortState.LaLacpTimeout =
                            LA_SHORT_TIMEOUT;
                    }
                }
            }
            else if (u1Set == LA_RESET)
            {
                if (pPortEntry->
                    LaLacPartnerAdminInfo.LaLacPortState.LaLacpTimeout ==
                    LA_LONG_TIMEOUT)
                {
                    return;
                }
                else
                {
                    pPortEntry->
                        LaLacPartnerAdminInfo.LaLacPortState.LaLacpTimeout =
                        LA_LONG_TIMEOUT;

                    if (pPortEntry->InDefaulted == LA_TRUE)
                    {
                        pPortEntry->
                            LaLacPartnerInfo.LaLacPortState.LaLacpTimeout =
                            LA_LONG_TIMEOUT;
                    }
                }
            }

            if ((LA_MODULE_STATUS == LA_ENABLED) &&
                (pPortEntry->LaLacpMode == LA_MODE_LACP) &&
                (pPortEntry->PortUsingPartnerAdminInfo == LA_TRUE))
            {
                LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);
            }
            LA_TRC (CONTROL_PLANE_TRC | MGMT_TRC,
                    "UTL: LacpPartnerTimeout bit value changed SUCCESSFULLY\n");
            break;
        case LA_AGGREGATION:
            if (u1Set == LA_SET)
            {
                if (pPortEntry->
                    LaLacPartnerAdminInfo.LaLacPortState.LaAggregation
                    == LA_TRUE)
                {
                    return;
                }
                else
                {
                    pPortEntry->
                        LaLacPartnerAdminInfo.LaLacPortState.LaAggregation =
                        LA_TRUE;

                    if (pPortEntry->InDefaulted == LA_TRUE)
                    {
                        pPortEntry->
                            LaLacPartnerInfo.LaLacPortState.LaAggregation =
                            LA_TRUE;
                    }
                }
            }
            else if (u1Set == LA_RESET)
            {
                if (pPortEntry->
                    LaLacPartnerAdminInfo.LaLacPortState.LaAggregation ==
                    LA_FALSE)
                {
                    return;
                }
                else
                {
                    pPortEntry->
                        LaLacPartnerAdminInfo.LaLacPortState.LaAggregation =
                        LA_FALSE;

                    if (pPortEntry->InDefaulted == LA_TRUE)
                    {
                        pPortEntry->
                            LaLacPartnerInfo.LaLacPortState.LaAggregation
                            = LA_FALSE;
                    }
                }
            }
            if ((LA_MODULE_STATUS == LA_ENABLED)
                && (pPortEntry->LaLacpMode == LA_MODE_LACP) &&
                (pPortEntry->InDefaulted == LA_TRUE))
            {
                if (pPortEntry->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                    pPortEntry->AggSelected = LA_UNSELECTED;
                    LaLacMuxControlMachine (pPortEntry,
                                            LA_MUX_EVENT_UNSELECTED);
                    pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
                }
                LaLacpSelectionLogic (pPortEntry);
            }
            break;
        default:
            break;
    }
    return;

}

/*****************************************************************************/
/* Function Name      : LaIncrementStatsOnCollectorDiscard                   */
/*                                                                           */
/* Description        : This function increments all the aggregator If Entry */
/*                      related statistics when a frame is discarded by the  */
/*                      Frame Collector when port Collecting is FALSE.       */
/*                                                                           */
/* Input(s)           : pBuf - The pointer to the data buffer                */
/*                      pPortEntry - Pointer to the Port Entry          */
/*                      pLaDestMacAddress - Pointer to the Destination Mac   */
/*                                          Address extracted from pBuf      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/
INT4
LaIncrementStatsOnCollectorDiscard (tLaBufChainHeader * pBuf,
                                    tLaLacPortEntry * pPortEntry,
                                    tLaMacAddr * pLaDestMacAddress)
{
    UINT4               u4ByteCount;
    tLaLacAggIfEntry   *pLaLacAggIfEntry;

    /* For getting the octets of all the frames discarded */
    u4ByteCount = LA_GET_CRU_BUF_VALID_BYTE_COUNT (pBuf);

    pLaLacAggIfEntry = &(pPortEntry->pAggEntry->AggIfEntry);

    /* We increment all the statistics negatively for the Aggregator, because 
     * these frames have already been counted once, at the port level */

    if (pPortEntry->LaAggAttached == LA_TRUE)
    {

        pLaLacAggIfEntry->i4AggInOctets -= (u4ByteCount);

        (pLaLacAggIfEntry->i4AggInDiscards)--;
        (pLaLacAggIfEntry->i4AggInErrors)--;
        (pLaLacAggIfEntry->i4AggInFrames)--;

        if (LA_MEMCMP (pLaDestMacAddress, &(gLaBCastAddr), LA_MAC_ADDRESS_SIZE)
            == 0)
        {
            (pLaLacAggIfEntry->i4AggInBcastPkts)--;
        }
        else
        {
            if (LA_IS_MAC_MULTICAST (*(UINT1 *) pLaDestMacAddress))
            {
                (pLaLacAggIfEntry->i4AggInMcastPkts)--;
            }
            else
            {
                (pLaLacAggIfEntry->i4AggInUcastPkts)--;
            }
        }
    }
    LA_TRC (DATA_PATH_TRC,
            "UTL: Statistics updated on Collector Discard SUCCESSFULLY\n");
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIncrementStatsOnDistributorDiscard                 */
/*                                                                           */
/* Description        : This function increments all the aggregator If Entry */
/*                      related statistics when a frame is discarded by the  */
/*                      Frame Distributor when port Distributing is FALSE or */
/*                      when frames are discarded due to sending of Marker   */
/*                      PDU on the corresponding port.                       */
/*                                                                           */
/* Input(s)           : pBuf - The pointer to the data buffer                */
/*                      pPortEntry - Pointer to the port entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS / LA_FAILURE                              */
/*****************************************************************************/
INT4
LaIncrementStatsOnDistributorDiscard (tLaBufChainHeader * pBuf,
                                      tLaLacPortEntry * pPortEntry)
{
    UINT4               u4ByteCount;
    tLaMacAddr          LaDestMacAddress;
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;

    LA_MEMSET (&LaDestMacAddress, 0, sizeof (tLaMacAddr));
    if (LA_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &LaDestMacAddress, LA_OFFSET,
                              LA_ETH_ADDR_SIZE) == LA_CRU_FAILURE)
    {
        LA_TRC (DATA_PATH_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                "UTL: Outgoing Frame Copy from CRU Buffer FAILED\n");
        return LA_FAILURE;
    }

    /* For getting the octets of all the frames discarded */
    u4ByteCount = LA_GET_CRU_BUF_VALID_BYTE_COUNT (pBuf);

    if ((pPortEntry->pAggEntry == NULL)
        || (pPortEntry->OperIndividual == LA_TRUE))
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                     "UTL: Port %d not attached to any Aggregator\n",
                     pPortEntry->u2PortIndex);
        return LA_FAILURE;

    }
    pLaLacAggIfEntry = &(pPortEntry->pAggEntry->AggIfEntry);

    /* We increment all these statistics positively for the Aggregator, because 
     * these frames have not been counted at the port level */

    if (pPortEntry->LaAggAttached == LA_TRUE)
    {

        pLaLacAggIfEntry->i4AggOutOctets += (u4ByteCount);

        (pLaLacAggIfEntry->i4AggOutDiscards)++;
        (pLaLacAggIfEntry->i4AggOutErrors)++;
        (pLaLacAggIfEntry->i4AggOutFrames)++;

        if (!(LA_MEMCMP (&LaDestMacAddress, &gLaBCastAddr, LA_ETH_ADDR_SIZE)))
        {
            (pLaLacAggIfEntry->i4AggOutBcastPkts)++;
        }
        else
        {
            if (LA_IS_MAC_MULTICAST (*LaDestMacAddress))
            {
                (pLaLacAggIfEntry->i4AggOutMcastPkts)++;
            }
            else
            {
                (pLaLacAggIfEntry->i4AggOutUcastPkts)++;
            }
        }
    }
    LA_TRC (DATA_PATH_TRC,
            "UTL: Statistics updated on Distributor Discard SUCCESSFULLY\n");
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  LaCopyPduBufInfo                                    */
/*                                                                           */
/* Description        : This function is called from a function which        */
/*                      initialises the pointer ppu1ToLinearBuf to point to  */
/*                      an array. This function initializes the pointer to   */
/*                      either point to the start of the valid data byte if  */
/*                      CRU BUF is linear or it fills the array to which the */
/*                      pointer points.                                      */
/*                                                                           */
/* Input(s)           : Pointer to CRU buf chain header.                     */
/*                                                                           */
/* Output(s)          : Pointer to the linear buffer, which contains the PDU */
/*                      bytes. The linear buffer may be data buffer in the   */
/*                      CRU buf chain if the data buffer is linear and       */
/*                      contiguous.Otherwise, the data from the CRU buffer   */
/*                      is put in linear buffer that is passed   .           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*****************************************************************************/
INT4
LaCopyPduBufInfo (tLaBufChainHeader * pFromPduBuf, UINT1 **ppu1ToLinearBuf,
                  UINT4 u4ByteCount)
{
    UINT1              *pu1Ptr = NULL;

    if ((pFromPduBuf == NULL) || (ppu1ToLinearBuf == NULL))
    {

        LA_TRC (BUFFER_TRC, "UTILS: LACPDU BUFFER POINTER NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    pu1Ptr = LA_IS_CRU_BUF_LINEAR (pFromPduBuf, LA_OFFSET, u4ByteCount);

    if (pu1Ptr != NULL)
    {

        *ppu1ToLinearBuf = pu1Ptr;
        return LA_SUCCESS;
    }
    else
    {

        if (LA_COPY_FROM_CRU_BUF (pFromPduBuf, *ppu1ToLinearBuf, LA_OFFSET,
                                  u4ByteCount) == LA_CRU_FAILURE)
        {

            LA_TRC (BUFFER_TRC, "UTILS: Copy From CRU Buffer FAILED !!!\n");
            return LA_FAILURE;
        }

        return LA_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : LaCopyIntoLacpduInfo.                                */
/*                                                                           */
/* Description        : This function is called to store the most recently   */
/*                      received LACPDU bytes in the LaLacpduInfo structure  */
/*                      from the linear buffer. The pointer to the           */
/*                      LaLacpduInfo is passed, which is filled with LACPDU  */
/*                      info from the linear buffer.                         */
/*                                                                           */
/* Input(s)           : Pointer to the linear buffer containing LACPDU info, */
/*                      Pointer to the LaLacpduInfo structure.               */
/*                                                                           */
/* Output(s)          : The LACPDU info is filled from the linear buffer into*/
/*                      the LaLacpduInfo structure.                          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receiving a NULL buffer pointer */
/*****************************************************************************/
INT4
LaCopyIntoLacpduInfo (UINT1 *pu1LinearBuf, tLaLacpduInfo * pLaLacpduInfo)
{
    UINT1              *pu1PktBuf = pu1LinearBuf;

    UINT1               u1Val;
    UINT2               u2Val;

    if ((pu1LinearBuf == NULL) || (pLaLacpduInfo == NULL))
    {

        LA_TRC (BUFFER_TRC, "UTILS: Linear Buffer Pointer NULL !!!\n");
        return LA_ERR_NULL_PTR;
    }

    /* Copy from the start of Actor Information only */

    pu1PktBuf = pu1PktBuf + 1;

    /* Verify the Actor Information length */
    LA_GET_1BYTE (u1Val, pu1PktBuf);

    if (u1Val != LA_ACTOR_INFO_LEN)
    {

        LA_TRC (BUFFER_TRC, "UTILS: INCORRECT ACTOR INFO LENGTH\n");
        return LA_ERR_INVALID_VALUE;
    }

    /* Get the system Priority */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacActorInfo.LaSystem.u2SystemPriority = u2Val;

    LA_MEMCPY (pLaLacpduInfo->LaLacActorInfo.LaSystem.SystemMacAddr,
               pu1PktBuf, LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Get the Actor Key */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacActorInfo.u2IfKey = u2Val;

    /* Get the actor port priority */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacActorInfo.u2IfPriority = u2Val;

    /* Get the actor port index/number */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacActorInfo.u2IfIndex = u2Val;

    /* Get the actor port state */
    LA_GET_1BYTE (u1Val, pu1PktBuf);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaLacpActivity = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaLacpTimeout = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaAggregation = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaSynchronization =
        u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaCollecting = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaDistributing = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaDefaulted = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaExpired = u1Val & 0x01;

    /* shift the buffer pointer by 3 bytes to point to Partner TLV Type */
    pu1PktBuf = pu1PktBuf + 4;

    /* Verify the Partner Information length */
    LA_GET_1BYTE (u1Val, pu1PktBuf);

    if (u1Val != LA_PARTNER_INFO_LEN)
    {

        LA_TRC (BUFFER_TRC, "INCORRECT PARTNER INFO LENGTH\n");
        return LA_ERR_INVALID_VALUE;
    }
    /* Get the partner system Priority */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacPartnerInfo.LaSystem.u2SystemPriority = u2Val;

    LA_MEMCPY (pLaLacpduInfo->LaLacPartnerInfo.LaSystem.SystemMacAddr,
               pu1PktBuf, LA_MAC_ADDRESS_SIZE);
    pu1PktBuf += LA_MAC_ADDRESS_SIZE;

    /* Get the Partner Key */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacPartnerInfo.u2IfKey = u2Val;

    /* Get the Partner port priority */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacPartnerInfo.u2IfPriority = u2Val;

    /* Get the Partner port index/number */
    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->LaLacPartnerInfo.u2IfIndex = u2Val;

    /* Get the Partner port state */
    LA_GET_1BYTE (u1Val, pu1PktBuf);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaLacpActivity =
        u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaLacpTimeout = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaAggregation = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaSynchronization =
        u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaCollecting = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaDistributing =
        u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaDefaulted = u1Val & 0x01;
    u1Val = (UINT1) (u1Val >> 1);

    pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState.LaExpired = u1Val & 0x01;

    /* shift the buffer pointer by 3 bytes to point to Collector TLV_Type */
    pu1PktBuf = pu1PktBuf + 4;

    /* Verify the Collector Information length */
    LA_GET_1BYTE (u1Val, pu1PktBuf);

    if (u1Val != LA_COLLECTOR_INFO_LEN)
    {

        LA_TRC (BUFFER_TRC, "UTILS: INCORRECT COLLECTOR INFO LENGTH\n");
        return LA_ERR_INVALID_VALUE;
    }

    LA_GET_2BYTE (u2Val, pu1PktBuf);
    pLaLacpduInfo->CollectorMaxDelay = u2Val;

    /* shift the buffer pointer by 12 bytes to point to Terminator TLV_Type */
    pu1PktBuf = pu1PktBuf + 13;

    /* Verify the Terminator Information length */
    LA_GET_1BYTE (u1Val, pu1PktBuf);

    if (u1Val != LA_TERMINATOR_INFO_LEN)
    {

        LA_TRC (BUFFER_TRC, "UTILS: INCORRECT TERMINATOR INFO LENGTH\n");
        return LA_ERR_INVALID_VALUE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacCheckTimerExpiry.                               */
/*                                                                           */
/* Description        : This function checks if a particular timer has       */
/*                      expired or not. It returns a Boolean truth value if  */
/*                      the timer has expired,otherwise, it decrements the   */
/*                      timer counter and returns a Boolean false value.     */
/*                                                                           */
/* Input(s)           : Pointer to the timer counter.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - On timer expiry                            */
/*                      LA_FALSE - If the timer has not expired.             */
/*****************************************************************************/
tLaBoolean
LaLacCheckTimerExpiry (tLaTicks * pTimerCounter)
{
    if (pTimerCounter == NULL)
    {

        LA_TRC (CONTROL_PLANE_TRC, "UTILS: POINTER TO TIMER COUNTER NULL\n");
        return LA_ERR_NULL_PTR;
    }

    if (*pTimerCounter != LA_STOPPED)
    {
        *pTimerCounter -= 1;
        if (*pTimerCounter == LA_EXPIRED)
        {

            return LA_TRUE;
        }
    }
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      :  LaLacFormLacpdu.                                    */
/*                                                                           */
/* Description        : This function is called to form a LACPDU from the    */
/*                      particular LaLacPortEntry corresponding to a         */
/*                      port. The LACPDU is formed to be handed over to      */
/*                      Control Parser multiplexer.                          */
/*                                                                           */
/* Input(s)           : Pointer to the port entry whose LACPDU is to be      */
/*                      formed,                                              */
/*                      Pointer to the linear buffer which will contain the  */
/*                      LACPDU bytes.                                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_ERR_NULL_PTR - On receiving a NULL pointer        */
/*****************************************************************************/
INT4
LaLacFormLacpdu (tLaLacPortEntry * pPortEntry, UINT1 *pu1LinearBuf)
{
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;
    UINT2               u2InfoType = 0;

    if ((pPortEntry == NULL) || (pu1LinearBuf == NULL))
    {

        LA_TRC (CONTROL_PLANE_TRC, "UTILS: NULL POINTER PASSED\n");
        return LA_ERR_NULL_PTR;
    }

    if (pPortEntry->pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    /* Insert the 4 Most Significant bytes into the buffer */

    LA_MEMCPY (pu1LinearBuf, &gLaSlowProtAddr, LA_MAC_ADDRESS_SIZE);
    pu1LinearBuf += LA_MAC_ADDRESS_SIZE;

    LA_MEMCPY (pu1LinearBuf, pPortEntry->au1MacAddr, LA_MAC_ADDRESS_SIZE);
    pu1LinearBuf += LA_MAC_ADDRESS_SIZE;

    u2Val = LA_SLOW_PROT_TYPE;
    LA_PUT_2BYTE (pu1LinearBuf, u2Val);

    *pu1LinearBuf = LACPDU_SUB_TYPE;
    pu1LinearBuf += 1;

    *pu1LinearBuf = LACP_VERSION;
    pu1LinearBuf += 1;

    /* Fill Actor Information */
    *pu1LinearBuf = LA_ACTOR_INFO;
    pu1LinearBuf += 1;

    *pu1LinearBuf = LA_ACTOR_INFO_LEN;
    pu1LinearBuf += 1;

    if (LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED)
    {
        LaActiveDLAGGetBridgeIndexFromIfIndex ((UINT4) pPortEntry->u2PortIndex,
                                               &u4Val);
        pPortEntry->LaLacActorInfo.u2IfIndex = (UINT2) u4Val;

    }
    u2InfoType = LA_ACTOR_INFO;
    /* Copy the Actor's If Info into the buffer */
    LaCopyParticipantInfoIntoBuffer (&(pPortEntry->LaLacActorInfo),
                                     (&pu1LinearBuf), u2InfoType);

    /* Incrementby the number of reserved fields */
    pu1LinearBuf += 3;

    /* Fill Partner Info */
    *pu1LinearBuf = LA_PARTNER_INFO;
    pu1LinearBuf += 1;

    *pu1LinearBuf = LA_PARTNER_INFO_LEN;
    pu1LinearBuf += 1;
    u2InfoType = LA_PARTNER_INFO;

    /* Copy the partner's If Info into the buffer */
    LaCopyParticipantInfoIntoBuffer (&(pPortEntry->LaLacPartnerInfo),
                                     (&pu1LinearBuf), u2InfoType);

    /* Increment the number of reserved fields */
    pu1LinearBuf += 3;

    /* Fill Collector Info */
    *pu1LinearBuf = LA_COLLECTOR_INFO;
    pu1LinearBuf += 1;

    *pu1LinearBuf = LA_COLLECTOR_INFO_LEN;
    pu1LinearBuf += 1;

    u2Val = (UINT2) pPortEntry->pAggEntry->AggConfigEntry.CollectorMaxDelay;
    LA_PUT_2BYTE (pu1LinearBuf, u2Val);

    /* Increment the linear buffer by 12 to point to Terminator TLV_Type */
    pu1LinearBuf += 12;

    /* Fill Terminator Info */
    *pu1LinearBuf = LA_TERMINATOR_INFO;
    pu1LinearBuf += 1;

    *pu1LinearBuf = LA_TERMINATOR_INFO_LEN;
    pu1LinearBuf += 1;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaCopyParticipantInfoIntoBuffer.                     */
/*                                                                           */
/* Description        : This function is called from LaLacFormLacpdu() to    */
/*                      copy the Actor and Partner information into the      */
/*                      linear buffer.                                       */
/*                                                                           */
/* Input(s)           : Pointer to the If Info structure, Pointer to the     */
/*                      linear buffer.                                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
LaCopyParticipantInfoIntoBuffer (tLaLacIfInfo * pLaLacIfInfo,
                                 UINT1 **ppPduBuffer, UINT2 u2InfoType)
{
    UINT1               u1Val = 0;
    UINT2               u2Val = 0;
#ifdef ICCH_WANTED
    tLaLacAggEntry     *pAggEntry;
    UINT2               u2PortMaskIndex = 0;
    UINT2               u2MaskValue = 0;
#endif
    if ((pLaLacIfInfo == NULL) || (ppPduBuffer == NULL))
    {
        return LA_ERR_NULL_PTR;
    }
    u2Val = pLaLacIfInfo->LaSystem.u2SystemPriority;
    LA_PUT_2BYTE (*ppPduBuffer, u2Val);

    LA_MEMCPY (*ppPduBuffer, pLaLacIfInfo->LaSystem.SystemMacAddr,
               LA_MAC_ADDRESS_SIZE);
    *ppPduBuffer += LA_MAC_ADDRESS_SIZE;

    u2Val = pLaLacIfInfo->u2IfKey;
    LA_PUT_2BYTE (*ppPduBuffer, u2Val);

    u2Val = pLaLacIfInfo->u2IfPriority;
    LA_PUT_2BYTE (*ppPduBuffer, u2Val);

#ifdef ICCH_WANTED
    /*To avoid same port-number received from MC-LAG master and slave
     * nodes , logical port number will be used too fill LACPDU of slave node
     * ports.Check whether MCLAG is enabled in the system ,whether mc-lag is 
     * enabled in the port-channel,check whether node state is master.If all
     * conditions are true check whether we copy the actor info as we need to 
     * mask only the actor port number*/

    LaGetAggEntryByKey (pLaLacIfInfo->u2IfKey, &pAggEntry);

    /*Mask value will be given from HB module.It will be exchanged as part of HB message */
    u2MaskValue = HbApiGetPortMaskValue ();

    if ((LaGetMCLAGSystemStatus () == LA_TRUE) &&
        (LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
        (u2InfoType == LA_ACTOR_INFO))
    {
        /* Port index will be in the least significant byte.We add one byte 
         * mask value at the most significant byte.As the msak value differ between MC-LAG nodes
         * the logical port number generated will also differ  
         *---------------------------------------*
         *                    |                  *
         *   Mask Value       |   Port Index     *    
         *   0x80/0x40        |                  *
         * --------------------------------------*
         2nd  Byte               1st  Byte    */

        u2PortMaskIndex = pLaLacIfInfo->u2IfIndex | u2MaskValue;
        u2Val = u2PortMaskIndex;
    }

    else
#endif
    {
        u2Val = pLaLacIfInfo->u2IfIndex;
    }
    LA_PUT_2BYTE (*ppPduBuffer, u2Val);

    /* Fill the Port State Bit */
    u1Val = 0;

    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaExpired));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaDefaulted));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaDistributing));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaCollecting));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaSynchronization));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaAggregation));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaLacpTimeout));

    u1Val = (UINT1) (u1Val << 1);
    u1Val = (UINT1) (u1Val | (pLaLacIfInfo->LaLacPortState.LaLacpActivity));

    LA_PUT_1BYTE (*ppPduBuffer, u1Val);

    UNUSED_PARAM (u2InfoType);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacGetActivePorts.                                 */
/*                                                                           */
/* Description        : This function is called from LaAggGetValidPort()     */
/*                      to get the port indices of all the ports selected    */
/*                      for a particular aggregator.                         */
/*                                                                           */
/* Input(s)           : The aggregator index.                                */
/*                                                                           */
/* Output(s)          : The port indices filled in the Linear buffer,        */
/*                      count of the number of ports.                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On Failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer.      */
/*****************************************************************************/
INT4
LaLacGetActivePorts (UINT2 u2AggIndex, UINT2 *pLinearBuf, UINT1 *pu1PortCount)
{
    tLaLacPortEntry    *pPortNode;
    tLaLacAggEntry     *pAggEntry;
    INT4                i4RetVal = LA_FAILURE;
    UINT4               u4PortIndex = 0;
    tLaCallBackArgs     LaCallBackArgs;
    tLaDLAGConsPortEntry *pConsPortEntry = NULL;
    LA_TRC_ARG1 (DATA_PATH_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                 "LAG %d member ports are  \n", u2AggIndex);
    if ((pLinearBuf == NULL) || (pu1PortCount == NULL))
    {

        LA_TRC (DATA_PATH_TRC | ALL_FAILURE_TRC, "NULL pointer passed");
        return LA_ERR_NULL_PTR;
    }

    LA_MEMSET (&LaCallBackArgs, 0, sizeof (tLaCallBackArgs));

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }
    *pu1PortCount = 0;

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED))
    {
        *pu1PortCount = 0;
        /* Get the ports of this port channel */
        LaActiveDLAGGetNextPortEntry (NULL, &pConsPortEntry, pAggEntry);
        while (pConsPortEntry != NULL)
        {
            if (pConsPortEntry->u1PortStateFlag == LA_AA_DLAG_PORT_IN_ADD)
            {
                LaActiveDLAGGetIfIndexFromBridgeIndex (pConsPortEntry->
                                                       u4PortIndex,
                                                       &u4PortIndex);
                *pLinearBuf = (UINT2) u4PortIndex;
                ++pLinearBuf;
                ++(*pu1PortCount);
            }

            LaActiveDLAGGetNextPortEntry (pConsPortEntry, &pConsPortEntry,
                                          pAggEntry);
        }
    }

    LA_SLL_SCAN (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry), pPortNode,
                 tLaLacPortEntry *)
    {
        LaCallBackArgs.u2Port = pPortNode->u2PortIndex;
        i4RetVal = LaCustCallBack (LA_GET_ACTIVE_PORT_EVENT, &LaCallBackArgs);

        if ((pPortNode->OperIndividual == LA_FALSE) &&
            (pPortNode->LaLacActorInfo.LaLacPortState.LaDistributing ==
             LA_TRUE) && (i4RetVal == LA_SUCCESS) &&
            (LaCallBackArgs.u2PortState == LA_OPER_UP))
        {
            *pLinearBuf = pPortNode->u2PortIndex;
            ++pLinearBuf;
            ++(*pu1PortCount);
            LA_TRC_ARG1 (DATA_PATH_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                         "Port %d  \n", pPortNode->u2PortIndex);
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacTickTmrExpiry.                                  */
/*                                                                           */
/* Description        : This function is called from the LaTimerHandler() on */
/*                      expiry of the Tick Timer. This checks the state of   */
/*                      each state machine and acts accordingly.             */
/*                                                                           */
/* Input(s)           : pPortEntry.                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*****************************************************************************/
INT4
LaLacTickTmrExpiry (tLaLacPortEntry * pPortEntry)
{
    /* Reset the hold count */
    if (pPortEntry->u4HoldCount >= 3)
    {
        pPortEntry->u4HoldCount = 0;
        if (pPortEntry->NttFlag == LA_TRUE)
        {
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_TX_DATA);
        }
    }
    else
    {
        pPortEntry->u4HoldCount = 0;
    }
    if (LaStartTimer (&(pPortEntry->LaTickTimer)) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacPortChangeAggregation.                          */
/*                                                                           */
/* Description        : This function is called after a Marker response has  */
/*                      been received or waiting period for marker response  */
/*                      has timed out.                                       */
/*                                                                           */
/* Input(s)           : pLaLacportEntry                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer.      */
/*****************************************************************************/
/* This function is called after a Marker response has been received or
 * waiting period for the marker response has timed out */
INT4
LaLacPortChangeAggregation (tLaLacPortEntry * pPortEntry)
{
    if (pPortEntry == NULL)
    {

        LA_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                ("Port Entry Pointer NULL"));
        return LA_ERR_NULL_PTR;
    }

    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "UTILS: Port: %u :: Marker Response Received\n",
                 pPortEntry->u2PortIndex);

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaLacUpdateStats.                                    */
/*                                                                           */
/* Description        : This function is called whenever a physical port gets*/
/*                      attached to or detached from an aggregator.          */
/*                                                                           */
/* Input(s)           : pLaLacportEntry, LaPortStateFlag                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
/* Agg Interface Table updation */
INT4
LaLacUpdateStats (tLaLacPortEntry * pPortEntry,
                  tLaPortStateFlag LaPortStateFlag)
{
    tLaIfaceStats       LaIfaceStats;
    tLaIfaceStats      *pLaIfaceStats = NULL;
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;

    pLaIfaceStats = &LaIfaceStats;

    if (LaCfaGetIfCounters (pPortEntry->u2PortIndex, pLaIfaceStats)
        != CFA_SUCCESS)
    {
        return LA_FAILURE;
    }

    if (pPortEntry->pAggEntry == NULL)
    {
        return LA_SUCCESS;
    }

    pLaLacAggIfEntry = &(pPortEntry->pAggEntry->AggIfEntry);

    if (LaPortStateFlag == LA_PORT_ATTACH)
    {

        pLaLacAggIfEntry->i4AggInOctets -= pLaIfaceStats->u4InOctets;
        pLaLacAggIfEntry->i4AggInDiscards -= pLaIfaceStats->u4InDiscards;
        pLaLacAggIfEntry->i4AggInErrors -= pLaIfaceStats->u4InErrors;
        pLaLacAggIfEntry->i4AggInUnknownProtos -=
            pLaIfaceStats->u4InUnknownProtos;
        pLaLacAggIfEntry->i4AggInUcastPkts -= pLaIfaceStats->u4InUcastPkts;
        pLaLacAggIfEntry->i4AggInMcastPkts -= pLaIfaceStats->u4InMulticastPkts;
        pLaLacAggIfEntry->i4AggInBcastPkts -= pLaIfaceStats->u4InBroadcastPkts;
        pLaLacAggIfEntry->i4AggInFrames -=
            (pLaIfaceStats->u4InUcastPkts + pLaIfaceStats->u4InMulticastPkts +
             pLaIfaceStats->u4InBroadcastPkts);

        pLaLacAggIfEntry->i4AggOutOctets -= pLaIfaceStats->u4OutOctets;
        pLaLacAggIfEntry->i4AggOutDiscards -= pLaIfaceStats->u4OutDiscards;
        pLaLacAggIfEntry->i4AggOutErrors -= pLaIfaceStats->u4OutErrors;
        pLaLacAggIfEntry->i4AggOutUcastPkts -= pLaIfaceStats->u4OutUcastPkts;
        pLaLacAggIfEntry->i4AggOutMcastPkts -=
            pLaIfaceStats->u4OutMulticastPkts;
        pLaLacAggIfEntry->i4AggOutBcastPkts -=
            pLaIfaceStats->u4OutBroadcastPkts;
        pLaLacAggIfEntry->i4AggOutFrames -=
            (pLaIfaceStats->u4OutUcastPkts + pLaIfaceStats->u4OutMulticastPkts +
             pLaIfaceStats->u4OutBroadcastPkts);

    }
    else if (LaPortStateFlag == LA_PORT_DETACH)
    {

        pLaLacAggIfEntry->i4AggInOctets += pLaIfaceStats->u4InOctets;
        pLaLacAggIfEntry->i4AggInDiscards += pLaIfaceStats->u4InDiscards;
        pLaLacAggIfEntry->i4AggInErrors += pLaIfaceStats->u4InErrors;
        pLaLacAggIfEntry->i4AggInUnknownProtos +=
            pLaIfaceStats->u4InUnknownProtos;
        pLaLacAggIfEntry->i4AggInUcastPkts += pLaIfaceStats->u4InUcastPkts;
        pLaLacAggIfEntry->i4AggInMcastPkts += pLaIfaceStats->u4InMulticastPkts;
        pLaLacAggIfEntry->i4AggInBcastPkts += pLaIfaceStats->u4InBroadcastPkts;
        pLaLacAggIfEntry->i4AggInFrames +=
            (pLaIfaceStats->u4InUcastPkts + pLaIfaceStats->u4InMulticastPkts +
             pLaIfaceStats->u4InBroadcastPkts);

        pLaLacAggIfEntry->i4AggOutOctets += pLaIfaceStats->u4OutOctets;
        pLaLacAggIfEntry->i4AggOutDiscards += pLaIfaceStats->u4OutDiscards;
        pLaLacAggIfEntry->i4AggOutErrors += pLaIfaceStats->u4OutErrors;
        pLaLacAggIfEntry->i4AggOutUcastPkts += pLaIfaceStats->u4OutUcastPkts;
        pLaLacAggIfEntry->i4AggOutMcastPkts +=
            pLaIfaceStats->u4OutMulticastPkts;
        pLaLacAggIfEntry->i4AggOutBcastPkts +=
            pLaIfaceStats->u4OutBroadcastPkts;
        pLaLacAggIfEntry->i4AggOutFrames +=
            (pLaIfaceStats->u4OutUcastPkts + pLaIfaceStats->u4OutMulticastPkts +
             pLaIfaceStats->u4OutBroadcastPkts);
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacUpdatePortList                                  */
/*                                                                           */
/* Description        : This function is called whenever a physical port gets*/
/*                      attached to or detached from an aggregator.          */
/*                                                                           */
/* Input(s)           : pLaLacportEntry, LaPortStateFlag                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaLacUpdatePortList (tLaLacPortEntry * pPortEntry,
                     tLaPortStateFlag LaPortStateFlag)
{
    if (pPortEntry->pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (LaPortStateFlag == LA_PORT_ATTACH)
    {
        OSIX_BITLIST_SET_BIT (pPortEntry->pAggEntry->LaActivePorts,
                              pPortEntry->u2PortIndex, sizeof (tLaPortList));
    }
    else if (LaPortStateFlag == LA_PORT_DETACH)
    {
        OSIX_BITLIST_RESET_BIT (pPortEntry->pAggEntry->LaActivePorts,
                                pPortEntry->u2PortIndex, sizeof (tLaPortList));

    }

    return LA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : LaLinkStatusNotify.                                  */
/*                                                                           */
/* Description        : This function is called whenever Oper Status         */
/*                      for any aggregator is sent to bridge.                */
/*                                                                           */
/* Input(s)           : u2AggIndex, u1AggOperStatus.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :  LaLinkUpDownTrapEnable in LaAggInfo                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaLinkStatusNotify (UINT4 u4IfIndex, UINT1 u1OperStatus, UINT1 u1AdminStatus,
                    UINT1 u1TrapType)
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tLaLacAggConfigEntry *pLaLacAggConfigEntry = NULL;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    UINT4               u4SpecTrapType = 0;
    UINT4               u4GenTrapType = 0;
    UINT1               au1Buf[LA_OBJECT_NAME_LEN];

    UINT4               au4IfIndex[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 1 };
    UINT4               au4ActorAdminKey[] =
        { 1, 2, 840, 10006, 300, 43, 1, 1, 1, 1, 6 };
    UINT4               au4IfAdminStatus[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 7 };
    UINT4               au4IfOperStatus[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 8 };
    INT4                i4BundleState = 0;

    tNotifyProtoToApp   NotifyProtoToApp;
    tLaParamInfo       *pLaParamInfo = NULL;

    pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);
    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = 0;

    switch (u1TrapType)
    {
        case LA_TRAP_PORTCHANNEL_DOWN:
        case LA_TRAP_PORTCHANNEL_UP:

            /* For a IF LINK UP/DOWN Trap, ifIndex, ifAdminStatus and */
            /* ifOperStatus have to be notified through the TRAP. */

            /*Copy the ifIndex Oid */
            pOid = alloc_oid (SNMP_MAX_OID_LENGTH);

            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                return;
            }

            MEMCPY (pOid->pu4_OidList, au4IfIndex, sizeof (au4IfIndex));

            /* Concatenate the OID with the ifIndex value - table indexing */
/*            pOid->pu4_OidList[LA_TRAP_OID_LEN] = u4IfIndex;*/
            pOid->pu4_OidList[(sizeof (au4IfIndex) / sizeof (UINT4))] =
                u4IfIndex;
            pOid->u4_Length = (sizeof (au4IfIndex) / sizeof (UINT4)) + 1;

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifIndex */
            pVbList =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0, (INT4) u4IfIndex,
                                                         NULL, NULL,
                                                         pSnmpCnt64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                return;
            }
            /* Starting of Variable Binding */
            pStartVb = pVbList;

            /*Copy the ifAdminStatus Oid */
            pOid = alloc_oid (SNMP_MAX_OID_LENGTH);

            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            MEMCPY (pOid->pu4_OidList, au4IfAdminStatus,
                    sizeof (au4IfAdminStatus));

            /* Concatenate the OID with the ifIndex value - table indexing */
/*            pOid->pu4_OidList[LA_TRAP_OID_LEN] = u4IfIndex;*/
            pOid->pu4_OidList[(sizeof (au4IfAdminStatus) / sizeof (UINT4))] =
                u4IfIndex;
            pOid->u4_Length = (sizeof (au4IfAdminStatus) / sizeof (UINT4)) + 1;

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifAdminStatus*/
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         (INT4) u1AdminStatus,
                                                         NULL, NULL,
                                                         pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            /* Start of Next Variable Binding */
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "ifOperStatus");

            /*Copy the ifOperStatus Oid */

            pOid = alloc_oid (SNMP_MAX_OID_LENGTH);

            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            MEMCPY (pOid->pu4_OidList, au4IfOperStatus,
                    sizeof (au4IfOperStatus));

            /* Concatenate the OID with the ifIndex value - table indexing */
/*            pOid->pu4_OidList[LA_TRAP_OID_LEN] = u4IfIndex;*/
            pOid->pu4_OidList[(sizeof (au4IfOperStatus) / sizeof (UINT4))] =
                u4IfIndex;
            pOid->u4_Length = (sizeof (au4IfOperStatus) / sizeof (UINT4)) + 1;

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifOperStatus */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0, (INT4) u1OperStatus,
                                                         NULL, NULL,
                                                         pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            break;
        case LA_TRAP_PORT_UP:
        case LA_TRAP_PORT_DOWN:

            /* For Link UP/Down Trap, Below details have to be notified 
             * through Trap */
            /* Port-channel Number 
             * Port Bundle state 
             * Bundle state change Time 
             * Down in Bundle reason */

            /* For the SNMP Variable Binding of OID and associated Value
             * for port-channel number */
            pOid = alloc_oid (SNMP_MAX_OID_LENGTH);

            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                return;
            }

            MEMCPY (pOid->pu4_OidList, au4ActorAdminKey,
                    sizeof (au4ActorAdminKey));

            LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry);
            if (pPortEntry != NULL)
            {
                pLaLacAggEntry = pPortEntry->pAggEntry;
                if (pLaLacAggEntry != NULL)
                {
                    /* Concatenate the OID with the Agg index value - table indexing */
                    pOid->
                        pu4_OidList[(sizeof (au4ActorAdminKey) /
                                     sizeof (UINT4))] =
                        pLaLacAggEntry->u2AggIndex;
                    pOid->u4_Length =
                        (sizeof (au4ActorAdminKey) / sizeof (UINT4)) + 1;
                    pVbList =
                        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                                 SNMP_DATA_TYPE_INTEGER,
                                                                 0,
                                                                 (INT4)
                                                                 pLaLacAggEntry->
                                                                 AggConfigEntry.
                                                                 u2ActorAdminKey,
                                                                 NULL, NULL,
                                                                 pSnmpCnt64Type);
                }
            }
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                return;
            }
            /* Starting of Variable Binding */
            pStartVb = pVbList;

            /*Copy the fsLaPortBundleState Oid */
            SPRINTF ((CHR1 *) au1Buf, "fsLaPortBundleState");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifAdminStatus*/
            nmhGetFsLaPortBundleState ((INT4) u4IfIndex, &i4BundleState);
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0, i4BundleState, NULL,
                                                         NULL, pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            /* Start of Next Variable Binding */
            pVbList = pVbList->pNextVarBind;

            /*Copy the fsLaPortBundStChgTimeStamp Oid */
            SPRINTF ((CHR1 *) au1Buf, "fsLaPortBundStChgTimeStamp");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifOperStatus */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         (INT4) pPortEntry->
                                                         u4BundStChgTime, NULL,
                                                         NULL, pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            /*Copy the fsLaPortDownInBundleReason Oid */
            SPRINTF ((char *) au1Buf, "fsLaPortDownInBundleReason");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length++] = u4IfIndex;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifOperStatus */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         (INT4) pPortEntry->
                                                         u1PortDownReason, NULL,
                                                         NULL, pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            break;
        case LA_TRAP_PORTCHANNEL_OPER_DOWN:
            /* For port-Channel UP/DOWN trap, port-channel Down reason and 
             * Port- channel oper change Time stamp have to be notified through the TRAP. */

            /* Copy port-channel Down Reason OID */
            SPRINTF ((char *) au1Buf, "fsLaPortChannelOperChgTimeStamp");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                return;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifAdminStatus*/
            LaGetAggEntry (u4IfIndex, &pLaLacAggEntry);
            if (pLaLacAggEntry == NULL)
            {
                SNMP_FreeOid (pOid);
                return;

            }
            if (pLaLacAggEntry != NULL)
            {
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pLaLacAggEntry->u2AggIndex;
                }

                pVbList =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             pLaLacAggEntry->
                                                             AggTimeOfLastOperChange,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);
            }
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            /* Start of Next Variable Binding */
            pStartVb = pVbList;

            /* port-channel oper change timestamp */
            SPRINTF ((char *) au1Buf, "fsLaPortChannelDownReason");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifOperStatus */
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length++] =
                    pLaLacAggEntry->u2AggIndex;
            }
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         (INT4) pLaLacAggEntry->
                                                         i4DownReason, NULL,
                                                         NULL, pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            break;
        default:
            break;
    }
    if ((u1TrapType == LA_TRAP_PORTCHANNEL_UP) ||
        (u1TrapType == LA_TRAP_PORTCHANNEL_DOWN))
    {
        /* The following API sends the Trap info to the FutureSNMP Agent. */
        SNMP_AGT_RIF_Notify_Trap (NULL, u1TrapType, u4SpecTrapType, pStartVb);
    }
    else
    {
        SPRINTF ((char *) au1Buf, "fsFutureLaTraps");
        pEnterpriseOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
        if (pEnterpriseOid == NULL)
        {
            return;
        }
        if (u1TrapType == LA_TRAP_PORTCHANNEL_OPER_DOWN)
        {
            u4GenTrapType = 7;
            SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, ENTERPRISE_SPECIFIC,
                                      u4GenTrapType, pStartVb);
        }
        else
        {
            u4GenTrapType = 6;
            SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, ENTERPRISE_SPECIFIC,
                                      u4GenTrapType, pStartVb);
        }
    }

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    if ((u1TrapType == LA_TRAP_PORTCHANNEL_DOWN) ||
        (u1TrapType == LA_TRAP_PORTCHANNEL_UP))
    {
        LaGetAggConfigEntry ((UINT2) u4IfIndex, &pLaLacAggConfigEntry);

        if (pLaLacAggConfigEntry != NULL)
        {
            pLaParamInfo->u2IfKey = pLaLacAggConfigEntry->u2ActorAdminKey;
        }

        NotifyProtoToApp.LaNotify.u1OperStatus = u1OperStatus;
        NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_LINK_STATUS;
        LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AdminStatus);
    UNUSED_PARAM (u1OperStatus);
    UNUSED_PARAM (u1TrapType);
#endif /* SNMP_2_WANTED */

}

/*****************************************************************************/
/* Function Name      : LaErrorRecTrigNotify.                                */
/*                                                                           */
/* Description        : This function is called whenever there is a          */
/*                      Error recovery mechanism is triggered                */
/*                                                                           */
/* Input(s)           : u2AggIndex, u1AggOperStatus.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :  LaLinkUpDownTrapEnable in LaAggInfo                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaErrorRecTrigNotify (tLaLacPortEntry * pPortEntry, UINT1 u1RecoveryTmrReason,
                      UINT1 u1HwFailType)
{
#ifdef SNMP_2_WANTED
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;

    UINT4               au4ActorAdminKey[] =
        { 1, 2, 840, 10006, 300, 43, 1, 1, 1, 1, 6 };
    UINT4               u4ThresholdValue = 0;
    UINT4               u4RecTimeStamp = 0;
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[LA_OBJECT_NAME_LEN];

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    switch (u1RecoveryTmrReason)
    {
        case LA_REC_ERR_DEFAULTED:
        case LA_REC_ERR_HW_FAILURE:
        case LA_REC_ERR_SAME_STATE:
            /* For Error State Trap Following details have to be notified 
             * through Trap */
            /* Port-channel number 
             * H/W Failure Trap type for H/w Failure
             * State in which port stuck for Same state error
             * Failure recovery Timestamp 
             * HardwareFailure Threshold 
             * Recovery timer duaration 
             * Exceed ACtion */

            pOid = alloc_oid (SNMP_MAX_OID_LENGTH);

            if (pOid == NULL)
            {
                /* Release the memory Assigned for Entrerprise Oid */
                return;
            }

            /* Copy Adminkey OID */
            MEMCPY (pOid->pu4_OidList, au4ActorAdminKey,
                    sizeof (au4ActorAdminKey));
            pLaLacAggEntry = pPortEntry->pAggEntry;
            if (pLaLacAggEntry != NULL)
            {
                /* Concatenate the OID with the Admin key value - table indexing */
                pOid->
                    pu4_OidList[(sizeof (au4ActorAdminKey) / sizeof (UINT4))] =
                    pLaLacAggEntry->u2AggIndex;
                pOid->u4_Length =
                    (sizeof (au4ActorAdminKey) / sizeof (UINT4)) + 1;

                /* For the SNMP Variable Binding of OID and associated Value
                 * for ifIndex */
                pVbList =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (UINT4)
                                                             pLaLacAggEntry->
                                                             AggConfigEntry.
                                                             u2ActorAdminKey,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);
            }
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                return;
            }
            /* Starting of Variable Binding */
            pStartVb = pVbList;

            if (u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE)
            {
                SPRINTF ((char *) au1Buf, "fsLaHwFailTrapType");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }

                /* For the SNMP Variable Binding of OID and associated Value
                 * for ifAdminStatus*/
                if (pLaLacAggEntry != NULL)
                {
                    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                    {
                        pOid->pu4_OidList[pOid->u4_Length++] =
                            pLaLacAggEntry->u2AggIndex;
                    }
                    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                    {
                        pOid->pu4_OidList[pOid->u4_Length++] =
                            pPortEntry->u2PortIndex;
                    }
                    pVbList->pNextVarBind =
                        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                                 SNMP_DATA_TYPE_INTEGER,
                                                                 0,
                                                                 (UINT4)
                                                                 u1HwFailType,
                                                                 NULL, NULL,
                                                                 pSnmpCnt64Type);
                }
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                /* Start of Next Variable Binding */
                pVbList = pVbList->pNextVarBind;

            }
            if (u1RecoveryTmrReason == LA_REC_ERR_SAME_STATE)
            {
                SPRINTF ((char *) au1Buf, "fsLaPortRecState");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }

                /* For the SNMP Variable Binding of OID and associated Value
                 * for ifAdminStatus*/
                if (pLaLacAggEntry != NULL)
                {
                    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                    {
                        pOid->pu4_OidList[pOid->u4_Length++] =
                            pPortEntry->u2PortIndex;
                    }
                    pVbList->pNextVarBind =
                        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                                 SNMP_DATA_TYPE_INTEGER,
                                                                 0,
                                                                 (UINT4)
                                                                 pPortEntry->
                                                                 u1RecState,
                                                                 NULL, NULL,
                                                                 pSnmpCnt64Type);
                }
                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                /* Start of Next Variable Binding */
                pVbList = pVbList->pNextVarBind;
            }

            if (u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
            {
                SPRINTF ((char *) au1Buf, "fsLaPortDefStateRecTimeStamp");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }

                u4RecTimeStamp = pPortEntry->u4DefStateRecTime;
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pPortEntry->u2PortIndex;
                }
                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             u4RecTimeStamp,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);

                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                pVbList = pVbList->pNextVarBind;

                SPRINTF ((char *) au1Buf, "fsLaPortDefaultedStateThreshold");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                u4ThresholdValue = pPortEntry->u4DefaultedStateThreshold;
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pPortEntry->u2PortIndex;
                }
                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             u4ThresholdValue,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);

            }
            else if (u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE)
            {
                SPRINTF ((char *) au1Buf, "fsLaPortHwFailRecTimeStamp");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                u4RecTimeStamp = pPortEntry->u4HwFailRecTime;
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pPortEntry->u2PortIndex;
                }
                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             u4RecTimeStamp,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);

                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                pVbList = pVbList->pNextVarBind;

                SPRINTF ((char *) au1Buf,
                         "fsLaPortHardwareFailureRecThreshold");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }

                u4ThresholdValue = pPortEntry->u4HardwareFailureRecThreshold;
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pPortEntry->u2PortIndex;
                }
                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             u4ThresholdValue,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);
            }
            else
            {
                SPRINTF ((char *) au1Buf, "fsLaPortSameStateRecTimeStamp");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                u4RecTimeStamp = pPortEntry->u4SameStateRecTime;
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pPortEntry->u2PortIndex;
                }
                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             u4RecTimeStamp,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);

                if (pVbList->pNextVarBind == NULL)
                {
                    SNMP_FreeOid (pOid);
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                pVbList = pVbList->pNextVarBind;

                SPRINTF ((char *) au1Buf, "fsLaPortSameStateRecThreshold");
                pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
                if (pOid == NULL)
                {
                    LaMemFreeVarBindList (pStartVb);
                    return;
                }
                u4ThresholdValue = pPortEntry->u4SameStateRecThreshold;
                if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
                {
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pPortEntry->u2PortIndex;
                }
                pVbList->pNextVarBind =
                    (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                             SNMP_DATA_TYPE_INTEGER,
                                                             0,
                                                             (INT4)
                                                             u4ThresholdValue,
                                                             NULL, NULL,
                                                             pSnmpCnt64Type);
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifAdminStatus*/
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }
            /* Start of Next Variable Binding */
            pVbList = pVbList->pNextVarBind;

            /*Copy the ifOperStatus Oid */

            SPRINTF ((char *) au1Buf, "fsLaRecTmrDuration");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifOperStatus */
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length++] = pPortEntry->u2PortIndex;
            }
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         (INT4) gu4RecoveryTime,
                                                         NULL, NULL,
                                                         pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            /*Copy the ifOperStatus Oid */

            SPRINTF ((char *) au1Buf, "fsLaRecThresholdExceedAction");
            pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
            if (pOid == NULL)
            {
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            /* For the SNMP Variable Binding of OID and associated Value
             * for ifOperStatus */
            if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
            {
                pOid->pu4_OidList[pOid->u4_Length++] = pPortEntry->u2PortIndex;
            }
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         (INT4)
                                                         gu4RecThresholdExceedAction,
                                                         NULL, NULL,
                                                         pSnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                LaMemFreeVarBindList (pStartVb);
                return;
            }

            break;
    }
    if (u1RecoveryTmrReason == LA_REC_ERR_DEFAULTED)
    {
        u4SpecTrapType = 4;
    }
    else if (u1RecoveryTmrReason == LA_REC_ERR_HW_FAILURE)
    {
        u4SpecTrapType = 3;
    }
    else
    {
        u4SpecTrapType = 5;
    }
    SNMP_AGT_RIF_Notify_Trap (NULL, u1RecoveryTmrReason, u4SpecTrapType,
                              pStartVb);
#else
    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (u1RecoveryTmrReason);
    UNUSED_PARAM (u1HwFailType);
#endif
}

#if (defined DLAG_WANTED) || (defined ICCH_WANTED)
/*****************************************************************************/
/* Function Name      : LaDLAGRoleChangeNotify.                              */
/*                                                                           */
/* Description        : This function is called whenever there is a          */
/*                      role change from master to slave and slave           */
/*                      to master                                            */
/*                                                                           */
/* Input(s)           : u2AggIndex, u1AggOperStatus.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :  LaLinkUpDownTrapEnable in LaAggInfo                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
LaDLAGRoleChangeNotify (UINT2 u2AggIndex, UINT4 u4DLAGTrapType)
{
#ifdef SNMP_2_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];

    UINT4               au4PortChannelIndex[] =
        { 1, 3, 6, 1, 4, 1, 2076, 63, 2, 1, 1, 1 };

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = 0;

    /*Copy the ifIndex Oid */
    pOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        return;
    }

    MEMCPY (pOid->pu4_OidList, au4PortChannelIndex,
            sizeof (au4PortChannelIndex));

    /* Concatenate the OID with the  port channel index value - table indexing */
    /* pOid->pu4_OidList[LA_TRAP_OID_LEN] = u2AggIndex; */
    pOid->pu4_OidList[(sizeof (au4PortChannelIndex) / sizeof (UINT4))] =
        u2AggIndex;
    pOid->u4_Length = (sizeof (au4PortChannelIndex) / sizeof (UINT4)) + 1;

    /* For the SNMP Variable Binding of OID and associated Value
     * for AggIndex */
    pVbList =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (UINT4) u2AggIndex,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        return;
    }
    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /*Copy the fsLaDLAGTrapType  Oid */

    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);
    SPRINTF ((CHR1 *) au1Buf, "%s", "fsLaDLAGTrapType");

    pOid = LaMakeObjIdFromDotNew (((INT1 *) au1Buf));
    if (pOid == NULL)
    {
        LaMemFreeVarBindList (pStartVb);
        return;
    }
    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);

    /* Depending upon the Trap Type, send the message to the SNMP manager */
    if (u4DLAGTrapType == LA_TRAP_DLAG_MASTER_TO_SLAVE)
    {
        SPRINTF ((CHR1 *) au1Buf, "%d %s ", u2AggIndex,
                 "D-LAG ROLE CHANGED FROM MASTER TO SLAVE");
    }
    else if (u4DLAGTrapType == LA_TRAP_DLAG_SLAVE_TO_MASTER)
    {
        SPRINTF ((CHR1 *) au1Buf, "%d %s ", u2AggIndex,
                 "D-LAG ROLE CHANGED FROM SLAVE TO MASTER");
    }
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        LaMemFreeVarBindList (pStartVb);
        SNMP_FreeOid (pOid);
        return;
    }
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring,
                                                  NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        LaMemFreeVarBindList (pStartVb);
        return;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (NULL, u4DLAGTrapType, u4SpecTrapType, pStartVb);
#else
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (u4DLAGTrapType);
#endif /* SNMP_2_WANTED */

}

/****************************************************************************
 *
 *    FUNCTION NAME    : LaMacCmpForLaDLAGRemoteAggEntry
 *
 *    DESCRIPTION      : This routine is used to compare nodes of RBTree
 *                       tLaDLAGRemoteAggEntry (indexed by SystemMacAddr) 
 *                       in a particular LaDLAGRemoteAggEntry node.
 *
 *    INPUT            : *pE1 - pointer to DLAGRemoteAggEntry Node1
 *                       *pE2 - pointer to DLAGRemoteAggEntry Node2   
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if pE1 > pE2 
 *                       else returns -1
 *
 ****************************************************************************/
PUBLIC INT4
LaMacCmpForLaDLAGRemoteAggEntry (tRBElem * pDLAGRemoteAggEntryNode1,
                                 tRBElem * pDLAGRemoteAggEntryNode2)
{
    tLaDLAGRemoteAggEntry *pEntryA = NULL;
    tLaDLAGRemoteAggEntry *pEntryB = NULL;
    INT4                i4RetVal = LA_INIT_VAL;

    pEntryA = (tLaDLAGRemoteAggEntry *) pDLAGRemoteAggEntryNode1;
    pEntryB = (tLaDLAGRemoteAggEntry *) pDLAGRemoteAggEntryNode2;

    i4RetVal = LA_MEMCMP (pEntryA->DLAGRemoteSystem.SystemMacAddr,
                          pEntryB->DLAGRemoteSystem.SystemMacAddr,
                          LA_MAC_ADDRESS_SIZE);

    if (i4RetVal != 0)

    {
        if (i4RetVal < 0)

        {
            return -1;
        }

        else

        {
            return 1;
        }
    }
    return 0;
}

/******************************************************************************
 * Function Name      : LaDLAGRemoteAggInfoTreeDestroy
 *
 * Description        : This routine is used to destroy the RBTree
 *
 * Input(s)           : u4Arg    - Offset for free function
 *                      freeFn   - Free Function required for RBTree Destruction
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to RBTree
 *****************************************************************************/
VOID
LaDLAGRemoteAggInfoTreeDestroy (tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg)
{
    if (Tree != NULL)
    {
        RBTreeDestroy (Tree, FreeFn, u4Arg);
    }
    return;
}
#endif /* DLAG_WANTED or ICCH_WANTED */
#ifdef SNMP_2_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : LaMemFreeVarBindList                                      */
/*                                                                           */
/* Description  : Frees the memory allocated for SNMP VarBind list,          */
/*                by scanning the list                                       */
/*                                                                           */
/* Input        : pVarBindLst  : pointer to the Varbind list                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
LaMemFreeVarBindList (tSNMP_VAR_BIND * pVarBindLst)
{
    SNMP_AGT_FreeVarBindList (pVarBindLst);
}

#endif /* SNMP_2_WANTED */
/*****************************************************************************/
/* Function Name      : LaPrintIfInfo                                        */
/*                                                                           */
/* Description        : This function prints the Interface Info              */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry * pPortEntry                    */
/*                      tLaLacIfInfo    * pLacpduActorInfo                   */
/*                      tLacIfInfo      * pPortPartnerInfo                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :  None                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
#ifdef TRACE_WANTED
VOID
LaPrintIfInfo (tLaLacPortEntry * pPortEntry,
               tLaLacIfInfo * pLacpduActorInfo, tLaLacIfInfo * pPortPartnerInfo)
{
    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                 "RXM: Port: %u :: AggSelected is FALSE. LACPDU INFO MISMATCH.\n",
                 pPortEntry->u2PortIndex);

    LA_TRC (CONTROL_PLANE_TRC,
            "**************************************************************************\n");

    LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                 "     LacpduActorKey     : %u, PortPartnerKey     : %u\n",
                 pLacpduActorInfo->u2IfKey, pPortPartnerInfo->u2IfKey);

    LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                 "     LacpduActorIndex   : %u, PortPartnerIndex   : %u\n",
                 pLacpduActorInfo->u2IfIndex, pPortPartnerInfo->u2IfIndex);

    LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                 "     LacpduActorPriority: %u, PortPartnerPriority: %u\n",
                 pLacpduActorInfo->u2IfPriority,
                 pPortPartnerInfo->u2IfPriority);

    LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                 "     LacpduActorAggr    : %u, PortPartnerAggr    : %u\n",
                 pLacpduActorInfo->LaLacPortState.LaAggregation,
                 pPortPartnerInfo->LaLacPortState.LaAggregation);

    LA_TRC (CONTROL_PLANE_TRC,
            "**************************************************************************\n");

}

/*****************************************************************************/
/* Function Name      : LaPrintPortStates                                    */
/*                                                                           */
/* Description        : This function prints the Port State Info             */
/*                                                                           */
/* Input(s)           : tLaLacPortEntry * pPortEntry                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaPrintPortStates (tLaLacPortEntry * pPortEntry)
{
    LA_TRC_ARG4 (CONTROL_PLANE_TRC,
                 "MUX: Port: %u :: ActorSync  : %u, ActorColl  : %u, ActorDist  : %u\n",
                 pPortEntry->u2PortIndex,
                 pPortEntry->LaLacActorInfo.LaLacPortState.
                 LaSynchronization,
                 pPortEntry->LaLacActorInfo.LaLacPortState.LaCollecting,
                 pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing);

    LA_TRC_ARG4 (CONTROL_PLANE_TRC,
                 "MUX: Port: %u :: PartnerSync: %u, PartnerColl: %u, PartnerDist: %u\n",
                 pPortEntry->u2PortIndex,
                 pPortEntry->LaLacPartnerInfo.LaLacPortState.
                 LaSynchronization,
                 pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting,
                 pPortEntry->LaLacPartnerInfo.LaLacPortState.LaDistributing);

    LA_TRC_ARG2 (CONTROL_PLANE_TRC,
                 "MUX: Port: %u :: AggSelected: %u\n",
                 pPortEntry->u2PortIndex, pPortEntry->AggSelected);

    LA_TRC_ARG3 (CONTROL_PLANE_TRC,
                 "MUX: Port: %u :: RxmState   : %u, MuxState   : %u\n",
                 pPortEntry->u2PortIndex, pPortEntry->LaRxmState,
                 pPortEntry->LaMuxState);
}

/*****************************************************************************/
/* Function Name      : LaDisplayCacheTable                                  */
/*                                                                           */
/* Description        : This function prints the Cache Table Info            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaDisplayCacheTable (VOID)
{
    tLaCacheEntry      *pLaFirstCacheEntry = NULL;
    tLaCacheEntry      *pLaNextCacheEntry = NULL;
    UINT4               u4CacheHashIndex;

    LA_HASH_SCAN_TBL (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex)
    {
        pLaFirstCacheEntry = (tLaCacheEntry *) LA_HASH_GET_FIRST_BUCKET_NODE
            (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex);

        while (pLaFirstCacheEntry != NULL)
        {

            pLaNextCacheEntry = (tLaCacheEntry *) LA_HASH_GET_NEXT_BUCKET_NODE
                (gLaGlobalInfo.pLaCacheTable, u4CacheHashIndex,
                 &pLaFirstCacheEntry->NextCacheEntry);

            LA_TRC (DATA_PATH_TRC,
                    "*****************************************\n");
            LA_TRC (DATA_PATH_TRC,
                    "*****         CACHE ENTRY           *****\n");
            LA_TRC (DATA_PATH_TRC,
                    "*****************************************\n");
            LA_TRC_ARG1 (DATA_PATH_TRC, " Source Mac ::%x \n",
                         pLaFirstCacheEntry->SrcMacAddr);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Destination Mac ::%x \n",
                         pLaFirstCacheEntry->DestMacAddr);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Port Index is  ::%d\n",
                         pLaFirstCacheEntry->u2PortIndex);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Agg Index is  ::%d\n",
                         pLaFirstCacheEntry->u2AggIndex);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Vlan Id is  ::%d\n",
                         pLaFirstCacheEntry->VlanId);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Isid is  ::%u\n",
                         pLaFirstCacheEntry->u4Isid);

#ifdef MPLS_WANTED
            LA_TRC_ARG1 (DATA_PATH_TRC, " Mpls Vc Label is  ::%d\n",
                         pLaFirstCacheEntry->u4MplsVcLabel);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Mpls Tunnel Label is  ::%d\n",
                         pLaFirstCacheEntry->u4MplsTunnelLabel);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Mpls Vc Tunnel Label is  ::%d\n",
                         pLaFirstCacheEntry->u4MplsVcTunnelLabel);
#endif

            LA_TRC_ARG1 (DATA_PATH_TRC, " Last Access Time is  ::%d\n",
                         pLaFirstCacheEntry->LastAccessTime);

            LA_TRC_ARG1 (DATA_PATH_TRC, " Marker Status ::%d\n",
                         pLaFirstCacheEntry->bMarkerStatus);

            if (pLaFirstCacheEntry->LaCacheTmr.LaTmrFlag == LA_TMR_RUNNING)
            {
                LA_TRC (DATA_PATH_TRC, " Cache Timer is RUNNING\n");
                LA_TRC (DATA_PATH_TRC,
                        "*****************************************\n");
            }
            else
            {
                LA_TRC (DATA_PATH_TRC, " Cache Timer is NOT RUNNING\n");
                LA_TRC (DATA_PATH_TRC,
                        "*****************************************\n");
            }

            pLaFirstCacheEntry = pLaNextCacheEntry;

        }                        /* End of WHILE */
    }
}

#endif

/*****************************************************************************/
/* Function Name        : LaIsValidSystemID                                  */
/*                                                                           */
/* Description          : This function checks whether the systemID is valid */
/*                        or not                                             */
/*                                                                           */
/* Input(s)             : CheckMacAddr -Mac Address under consideration      */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : LA_SUCCESS - On success                            */
/*                        LA_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
LaIsValidSystemID (tLaMacAddr CheckMacAddr)
{
    UINT1               au1LocalMacAddr[LA_MAC_ADDRESS_SIZE];

    LA_MEMSET (au1LocalMacAddr, 0, LA_MAC_ADDRESS_SIZE);

    if (LA_MEMCMP (CheckMacAddr, gLaBCastAddr, LA_MAC_ADDRESS_SIZE) == 0)
    {
        return LA_FAILURE;
    }
    if (LA_IS_MAC_MULTICAST (CheckMacAddr[0]))
    {
        return LA_FAILURE;
    }
    if (LA_MEMCMP (CheckMacAddr, au1LocalMacAddr, LA_MAC_ADDRESS_SIZE) == 0)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;

}

 /*****************************************************************************/
/* Function Name        : LaIsValidPortState                                 */
/*                                                                           */
/* Description          : This function returns whether the Input portstate  */
/*                        valid or not                                       */
/*                                                                           */
/* Input(s)             : i4PortState- Input PortState                       */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : LA_SUCCESS - On Port is aggregatable               */
/*                        LA_FAILURE - On port is not aggregatable           */
/*****************************************************************************/

INT4
LaIsValidPortState (INT4 i4PortState)
{
    if ((i4PortState == LA_ACTIVITY) || (i4PortState == LA_TIMEOUT) ||
        (i4PortState == LA_AGGREGATE))
    {
        return LA_SUCCESS;
    }
    return LA_FAILURE;
}

 /*****************************************************************************/
/* Function Name      : LaLock                                               */
/*                                                                           */
/* Description        : This function is called to Lock the semaphore for    */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : ProtocolLocked is changed to TRUE                    */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS - On success                            */
/*                      SNMP_FAILURE - On failure of creation of semaphore   */
/*****************************************************************************/
INT4
LaLock ()
{
    if (gLaProtocolSemId != LA_INVALID_VAL)
    {
        LA_TAKE_PROTOCOL_SEMAPHORE (gLaProtocolSemId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaUnLock                                             */
/*                                                                           */
/* Description        : This function is called to Unlock the semaphore for  */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : ProtocolLocked is changed to FALSE                   */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS - On success                            */
/*                      SNMP_FAILURE - On failure bcoz of no semaphore.      */
/*****************************************************************************/
INT4
LaUnLock ()
{
    if (gLaProtocolSemId != LA_INVALID_VAL)
    {
        LA_RELEASE_PROTOCOL_SEMAPHORE (gLaProtocolSemId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/*****************************************************************************/
/* Function Name        : LaCustCallBack                                     */
/*                                                                           */
/* Description          : This function calls the registered function        */
/*                        pointer for specific events.                       */
/*                                                                           */
/* Input(s)             : u4Event - Event for which Call back is invoked     */
/*                        pLaCallBackArgs - Input parameters required for    */
/*                                          call back functions              */
/* Output(s)            : pLaCallBackArgs - Output parameters required for   */
/*                                          call back functions              */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : LA_SUCCESS/LA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
LaCustCallBack (UINT4 u4Event, tLaCallBackArgs * pLaCallBackArgs)
{
    INT4                i4RetVal = LA_SUCCESS;

    /* Set to Default values if Call Back is not registered */
    pLaCallBackArgs->u2PortState = LA_OPER_UP;

    switch (u4Event)
    {
        case LA_PKT_RX_EVENT:
            if (LA_CALLBACK[u4Event].pLaPktRxCallBack != NULL)
            {
                i4RetVal = LA_CALLBACK[u4Event].pLaPktRxCallBack
                    (pLaCallBackArgs->u2Port, &pLaCallBackArgs->u2PortState);
            }
            break;

        case LA_PKT_TX_EVENT:
            if (LA_CALLBACK[u4Event].pLaPktTxCallBack != NULL)
            {
                i4RetVal = LA_CALLBACK[u4Event].pLaPktTxCallBack
                    (pLaCallBackArgs->u2Port, &pLaCallBackArgs->u2PortState);
            }
            break;

        case LA_GET_DISTRIB_PORT_EVENT:
            if (LA_CALLBACK[u4Event].pLaGetDistribPortCallBack != NULL)
            {
                i4RetVal = LA_CALLBACK[u4Event].pLaGetDistribPortCallBack
                    (pLaCallBackArgs->u2Port, &pLaCallBackArgs->u2PortState);
            }
            break;

        case LA_GET_ACTIVE_PORT_EVENT:
            if (LA_CALLBACK[u4Event].pLaGetActivePortCallBack != NULL)
            {
                i4RetVal = LA_CALLBACK[u4Event].pLaGetActivePortCallBack
                    (pLaCallBackArgs->u2Port, &pLaCallBackArgs->u2PortState);
            }
            break;

        default:
            pLaCallBackArgs->u2PortState = LA_OPER_DOWN;
            i4RetVal = LA_FAILURE;
            break;
    }

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name        : LaFillAggrMtu                                      */
/*                                                                           */
/* Description          : This function configures the MTU values in a given */
/*                        aggregator and all its member ports.               */
/*                                                                           */
/* Input(s)             : u2AggIndex- IfIndex of the portchannel.            */
/*                      : u4Mtu- MTU value to be filled.                     */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : LA_SUCCESS                                         */
/*                                                                           */
/*****************************************************************************/

INT4
LaFillAggMtu (UINT2 u2AggIndex, UINT4 u4Mtu)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
#ifdef SNMP_2_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (pAggEntry->u4Mtu == u4Mtu)
    {
        /* No change in the MTU value */
        return LA_SUCCESS;
    }

    pAggEntry->u4Mtu = u4Mtu;

    CfaIfInfo.u4IfMtu = u4Mtu;

    LA_SLL_SCAN (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                 pTmpPortEntry, tLaLacPortEntry *)
    {
        pTmpPortEntry->u4Mtu = u4Mtu;
        LaCfaSetIfInfo (IF_MTU, (UINT4) (pTmpPortEntry->u2PortIndex),
                        &CfaIfInfo);

#ifdef SNMP_2_WANTED
        /* Send notification for configuring the mtu of the individual 
         * interfaces */
        SnmpNotifyInfo.pu4ObjectId = IfMainMtu;
        SnmpNotifyInfo.u4OidLen = sizeof (IfMainMtu) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NULL;
        SnmpNotifyInfo.pUnLockPointer = NULL;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              pTmpPortEntry->u2PortIndex, u4Mtu));
#endif
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : LaFillAggPauseAdminMode                            */
/*                                                                           */
/* Description          : This function configures the Pause Admin mode      */
/*                        values in a given aggregator and all its member    */
/*                        ports.                                             */
/*                                                                           */
/* Input(s)             : u2AggIndex- IfIndex of the portchannel.            */
/*                      : i4PauseAdminMode- Pause Admin Mode value to fill   */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : LA_SUCCESS/LA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
LaFillAggPauseAdminMode (UINT2 u2AggIndex, INT4 i4PauseAdminMode)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    LA_MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    LaGetAggEntry (u2AggIndex, &pAggEntry);
    if (pAggEntry == NULL)
    {
        return LA_FAILURE;
    }

    if (pAggEntry->i4PauseAdminMode != i4PauseAdminMode)
    {
        /* Change in the Pause Admin Mode value */
        pAggEntry->i4PauseAdminMode = i4PauseAdminMode;
    }

    CfaIfInfo.u1PauseAdminNode = (UINT1) i4PauseAdminMode;

    LA_SLL_SCAN (LA_GET_AGG_CONFIG_PORT_SLL (pAggEntry),
                 pTmpPortEntry, tLaLacPortEntry *)
    {
        pTmpPortEntry->i4PauseAdminMode = i4PauseAdminMode;
        LaCfaSetIfInfo (IF_PAUSE_ADMIN_MODE,
                        (UINT4) (pTmpPortEntry->u2PortIndex), &CfaIfInfo);

    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : LaUtilGetPortState                                 */
/*                                                                           */
/*     DESCRIPTION      : This is an utility function used by cli routines   */
/*                        to get the PortOperState for the port.             */
/*                                                                           */
/*     INPUT            : u2IfIndex -  specifies the port Number.            */
/*                        pLaLacPortState - specifies the Port State         */
/*                        u4Value   -  specifies the value as ACTOR/PARTNER  */
/*                                                                           */
/*     OUTPUT           : pLaLacPortState - Pointer which stores the current */
/*                                          Port OperState                   */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
LaUtilGetPortState (UINT2 u2IfIndex, tLaLacPortState ** pLaLacPortState,
                    UINT4 u4Value)
{
    tLaLacPortEntry    *pPortEntry;
    if (LaGetPortEntry (u2IfIndex, &pPortEntry) != LA_SUCCESS)
    {
        *pLaLacPortState = NULL;
        return;
    }
    if (u4Value == LA_ACTOR)
    {
        *pLaLacPortState = &pPortEntry->LaLacActorInfo.LaLacPortState;
    }
    if (u4Value == LA_PARTNER)
    {
        *pLaLacPortState = &pPortEntry->LaLacPartnerInfo.LaLacPortState;
    }

    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : LaUpdatePortChannelStatusToHw                        */
/*                                                                           */
/* Description        : This routine is called from LA to update the h/w     */
/*                      the actual status of the port-channel interface      */
/*                      retrieving from the L2Iwf common database.           */
/*                                                                           */
/* Input(s)           : u2AggId      - Index of the port channel             */
/*                      u1StpState   - Port state                            */
/*                      u1UpdateFlag - Flag to check, actual STP port state  */
/*                                     needs to be updated in h/w or not.    */
/*                                     updated in h/w or not.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/

INT4
LaUpdatePortChannelStatusToHw (UINT2 u2AggId, UINT1 u1StpState,
                               UINT1 u1UpdateFlag)
{
    tMstInstPortStateInfo TmpInstPortStateInfo[AST_MAX_INSTANCES];
    INT4                i4RetVal;
    UINT2               u2NumInst = 1;
    UINT2               u2Index;
    UINT2               au2ActivePortArray[LA_MAX_PORTS_PER_AGG];
    UINT1               u1PortCount = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1ProtocolId = 0;
    UINT4               u4ContextId = 0;
#if (defined(MSTP_WANTED) || defined(RSTP_WANTED))
    UINT2               u2LocalPort = 0;
    tLaLacAggEntry     *pAggEntry = NULL;
#endif

    if (u1UpdateFlag == LA_STANDBY_INDICATION)
    {
        /* Here LA directly updates the h/w about
         * the standby port to be in discarding state
         * Here Mst instance will be passed as invalid instance to
         * notify about this special case in h/w call.
         */
        if (u1StpState == LA_PORT_DISABLED)
        {
            u1StpState = AST_PORT_STATE_DISCARDING;

        }
        else
        {
            u1StpState = AST_PORT_STATE_FORWARDING;
        }

        i4RetVal = LaFsLaHwSetPortChannelStatus (u2AggId,
                                                 (AST_MAX_INSTANCES + 1),
                                                 u1StpState);
        if (i4RetVal == FNP_SUCCESS)
        {
            return LA_SUCCESS;
        }
        else
        {
            return LA_FAILURE;
        }
    }

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
        (LA_AA_DLAG_ROLE_PLAYED == LA_DLAG_SYSTEM_ROLE_SLAVE))
    {
        return LA_SUCCESS;
    }

    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return LA_FAILURE;
    }

    LA_MEMSET (au2ActivePortArray, 0, sizeof (au2ActivePortArray));

    if (u1StpState == LA_PORT_DISABLED)
    {
        u1StpState = AST_PORT_STATE_DISCARDING;
    }

    if (LaLacGetActivePorts (u2AggId, au2ActivePortArray,
                             &u1PortCount) != LA_SUCCESS)
    {
        LA_TRC_ARG1 (DATA_PATH_TRC | ALL_FAILURE_TRC,
                     "AGG:LaLacGetActivePorts function FAILED for Agg %d\n",
                     u2AggId);
        return LA_FAILURE;
    }

    if ((u1UpdateFlag == LA_TRUE) && (u1PortCount == 0))
    {
        /* No Active ports are existing in the Aggregator, so just reset 
         * the flag and set discarding for the aggregator.
         */
        u1UpdateFlag = LA_FALSE;
        u1StpState = AST_PORT_STATE_DISCARDING;
    }

    MEMSET (&TmpInstPortStateInfo[0], 0, sizeof (TmpInstPortStateInfo));

#if (defined(RSTP_WANTED) || defined(MSTP_WANTED) || defined(PVRST_WANTED))

    if (AstIsPvrstEnabledInContext (L2IWF_DEFAULT_CONTEXT) == AST_TRUE)
    {
        for (u2VlanId = 1; u2VlanId <= VLAN_DEV_MAX_VLAN_ID; u2VlanId++)
        {
            if ((L2IwfIsVlanActive (u2VlanId) == OSIX_TRUE) &&
                (LA_INVALID_VAL != L2IwfGetVlanInstMapping (u2VlanId)))
            {
                if (u1UpdateFlag == LA_TRUE)
                {
                    u1StpState = L2IwfGetVlanPortState (u2VlanId, u2AggId);
                }
                if (FNP_FAILURE ==
                    LaFsLaHwSetPortChannelStatus (u2AggId, u2VlanId,
                                                  u1StpState))
                {
                    LaHandleAggProgramFailed (u2AggId);
                }
            }

        }
        return LA_SUCCESS;
    }
    else
    {
        i4RetVal =
            L2IwfGetInstPortStateForPort (u2AggId, TmpInstPortStateInfo,
                                          &u2NumInst);
        if (i4RetVal == L2IWF_FAILURE)
        {
            return LA_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (u2VlanId);
    u2NumInst = 1;
#endif

    for (u2Index = 0; u2Index < u2NumInst; u2Index++)
    {
        if (u1UpdateFlag == LA_TRUE)
        {
            if ((L2IwfIsErpsStartedInContext (u4ContextId))
                &&
                (L2IwfGetPortStateCtrlOwner (u2AggId, &u1ProtocolId, OSIX_FALSE)
                 == L2IWF_SUCCESS) && (u1ProtocolId == ERPS_MODULE))
            {
                u1StpState = TmpInstPortStateInfo[u2Index].u1PortState;
            }
            else
            {

#if (defined(MSTP_WANTED) || defined(RSTP_WANTED))
                if (AstGetContextInfoFromIfIndex
                    (u2AggId, &u4ContextId, &u2LocalPort) == RST_FAILURE)
                {
                    /*Spanning tree is not enabled in the aggregator index u2AggId */

                    if (LaGetAggEntry (u2AggId, &pAggEntry) == LA_FAILURE)
                    {
                        return LA_FAILURE;
                    }

                    if (pAggEntry->u1AggAdminStatus == CFA_IF_UP)
                    {
                        u1StpState = AST_PORT_STATE_FORWARDING;
                    }
                }
                else
                {
                    u1StpState = TmpInstPortStateInfo[u2Index].u1PortState;
                }
#else
                u1StpState = AST_PORT_STATE_FORWARDING;
#endif
            }
            /* For RSTP/MSTP */
            LA_TRC_ARG2 (DATA_PATH_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                         "LAG %d STP state %d \n", u2AggId, u1StpState);
            if (FNP_FAILURE ==
                LaFsLaHwSetPortChannelStatus (u2AggId,
                                              TmpInstPortStateInfo[u2Index].
                                              u2Inst, u1StpState))
            {
                LaHandleAggProgramFailed (u2AggId);
            }
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleAggCreateFailed                              */
/*                                                                           */
/* Description        : This routine is called from LA whenever, addition    */
/*                      of aggregator to h/w is failed.                      */
/*                      When HW Add of LAG is failed, the peer has also      */
/*                      to be made down about the particular LAG.Peer is made*/
/*                      down by setting operkey as 0 for all the member ports*/
/*                      By this peer will come out of the aggregation since 0*/
/*                      is not a valid value for the key.                    */
/*                                                                           */
/* Input(s)           : u2AggId      - Index of the port channel             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/
INT4
LaHandleAggCreateFailed (UINT2 u2AggId)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaNpTrapLogInfo    NpLogInfo;

    MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));

    if (LaGetAggEntry (u2AggId, &pAggEntry) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    if (pAggEntry->LaLacpMode == LA_MODE_LACP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            pPortEntry->LaLacActorInfo.u2IfKey = 0;

            pPortEntry->NttFlag = LA_TRUE;
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
            if ((gu4RecoveryTime != 0) &&
                (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_NONE))

            {
                if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
                    (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP))
                {
                    pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_HW_FAILURE;
                    pPortEntry->u4ErrorDetectCount++;
                    LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateDetTime));
                    LaErrorRecTrigNotify (pPortEntry, LA_REC_ERR_HW_FAILURE,
                                          AS_FS_LA_HW_CREATE_AGG_GROUP);
                    if (LaStartTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
                    {
                        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                     "LA: Recovery Timer Start FAILED for Port %d \r\n",
                                     pPortEntry->u2PortIndex);
                        pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
                        return LA_FAILURE;
                    }

                }
            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
    pAggEntry->HwAggStatus = LA_AGG_ADD_FAILED_IN_HW;

    /* Send Trap and Syslog */
    NpLogInfo.u4NpCallId = AS_FS_LA_HW_CREATE_AGG_GROUP;
    NpLogInfo.u2AggIndex = u2AggId;
    LaNpFailSendTrapAndSyslog (&NpLogInfo);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleLinkAdditionFailed                           */
/*                                                                           */
/* Description        : This routine is called from LA whenever, addition    */
/*                      of link to an aggregator is failed in h/w.           */
/*                                                                           */
/* Input(s)           : u2AggId      - Index of the port channel             */
/*                      u2PortNumber - Index of the physical port            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/
INT4
LaHandleLinkAdditionFailed (UINT2 u2AggId, UINT2 u2PortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaNpTrapLogInfo    NpLogInfo;

    UNUSED_PARAM (u2AggId);

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    /* LaLacHwPortStatus variable is updated in both Active node and Standby node.
     * In the standby, Oper key of the port is not set to zero because to retry for 
     * bundling if take-over happens
     * Trap is also sent only by the active node */

    if (pPortEntry != NULL)
    {
        pPortEntry->LaLacHwPortStatus = LA_HW_ADD_PORT_FAILED_IN_LAGG;
    }

    if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
    {
        LaRedSyncModifyPortsInfo (LA_NP_ADD_PORT_FAILURE_INFO, u2AggId,
                                  u2PortIndex);

        if ((pPortEntry != NULL) && (pPortEntry->LaLacpMode == LA_MODE_LACP))
        {
            pPortEntry->LaLacActorInfo.u2IfKey = 0;

            pPortEntry->NttFlag = LA_TRUE;
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
        }

        MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));

        /* Send Trap and Syslog */
        NpLogInfo.u4NpCallId = AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP;
        NpLogInfo.u2AggIndex = u2AggId;
        NpLogInfo.u2PortNumber = u2PortIndex;
        LaNpFailSendTrapAndSyslog (&NpLogInfo);
    }
    if ((gu4RecoveryTime != 0) &&
        (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_NONE))
    {
        if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
            (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP))
        {
            pPortEntry->u4ErrorDetectCount++;
            LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateDetTime));
            LaErrorRecTrigNotify (pPortEntry, LA_REC_ERR_HW_FAILURE,
                                  AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP);
            if (LaStartTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "LA: Recovery Timer Start FAILED for Port %d \r\n",
                             pPortEntry->u2PortIndex);
                pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
                return LA_FAILURE;
            }

        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaHandleLinkRemoveFailed                             */
/*                                                                           */
/* Description        : This routine is called from LA whenever, Removing    */
/*                      of link to an aggregator is failed in h/w.           */
/*                                                                           */
/* Input(s)           : u2AggId      - Index of the port channel             */
/*                      u2PortNumber - Index of the physical port            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/
INT4
LaHandleLinkRemoveFailed (UINT2 u2AggId, UINT2 u2PortIndex)
{
    tLaLacPortEntry    *pPortEntry = NULL;
    tLaNpTrapLogInfo    NpLogInfo;

    UNUSED_PARAM (u2AggId);

    /* LaLacHwPortStatus variable is updated in both Active node and Standby node.
     * Trap is sent only by the active node */

    LaGetPortEntry (u2PortIndex, &pPortEntry);

    if (pPortEntry != NULL)
    {
        pPortEntry->LaLacHwPortStatus = LA_HW_REM_PORT_FAILED_IN_LAGG;
    }

    if (LA_NODE_STATUS () == LA_NODE_ACTIVE)
    {

        LaRedSyncModifyPortsInfo (LA_NP_DEL_PORT_FAILURE_INFO, u2AggId,
                                  u2PortIndex);

        MEMSET (&NpLogInfo, 0, sizeof (tLaNpTrapLogInfo));

        NpLogInfo.u4NpCallId = AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP;
        NpLogInfo.u2AggIndex = u2AggId;
        NpLogInfo.u2PortNumber = u2PortIndex;
        LaNpFailSendTrapAndSyslog (&NpLogInfo);
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaNpFailSendTrapAndSyslog                            */
/*                                                                           */
/* Description        : This routine is called from LA whenever, addition    */
/*                      of link to an aggregator is failed in h/w.           */
/*                                                                           */
/* Input(s)           : u2AggId      - Index of the port channel             */
/*                      u2PortNumber - Index of the physical port            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/
VOID
LaNpFailSendTrapAndSyslog (tLaNpTrapLogInfo * pNpLogInfo)
{
    LaNpFailSyslog (pNpLogInfo);
    LaNpFailNotify (pNpLogInfo);
}
#endif /*NPAPI_WANTED */
/****************************************************************************
 * Function    :  LaSnmpLowValidateAggIndex
 * Input       :  i4Dot3adIndex (AggIndex / PortIndex)
 *
 * Output      :  None
 * Returns     :  LA_SUCCESS / LA_FAILURE
 *****************************************************************************/
INT4
LaIsValidAggIndex (UINT2 u2IfIndex)
{
    tLaLacAggEntry     *pAggEntry;

    if (LaGetAggEntry (u2IfIndex, &pAggEntry) != LA_SUCCESS)
    {
        return LA_FAILURE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaGetPortBundleState                                 */
/*                                                                           */
/* Description        : This routine is called from LA whenever, to get the  */
/*                      bundle state of the port.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Index of the port                     */
/*                                                                           */
/* Output(s)          : *pui1PortBundleState - Bundle state                  */
/*                                             (PORT_UP_IN_BNDL /PORT_DOWN/  */
/*                                              PORT_STANDBY /               */
/*                                              PORT_UP_INDIVIDUAL)          */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/

INT4
LaGetPortBundleState (UINT4 u4IfIndex, UINT1 *pui1PortBundleState)
{

    tLaLacPortEntry    *pPortEntry;

    *pui1PortBundleState = LA_PORT_DOWN;

    if (LaGetPortEntry ((UINT2) u4IfIndex, &pPortEntry) == LA_SUCCESS)
    {
        if (pPortEntry->LaLacpMode == LA_MODE_DISABLED)
        {
            if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
            {
                *pui1PortBundleState = LA_PORT_UP_INDIVIDUAL;

            }
            else if (pPortEntry->u1PortOperStatus == LA_OPER_DOWN)
            {
                *pui1PortBundleState = LA_PORT_DOWN;
            }
        }
        else                    /* if( pPortEntry->LaLacpMode != LA_MODE_DISABLED) */
        {
            if (pPortEntry->u1PortOperStatus == LA_OPER_DOWN)
            {
                *pui1PortBundleState = LA_PORT_DOWN;
            }
            else if (pPortEntry->u1PortOperStatus == LA_OPER_UP)
            {
                if ((pPortEntry->AggSelected == LA_SELECTED) &&
                    (pPortEntry->LaLacActorInfo.
                     LaLacPortState.LaDistributing == LA_TRUE))
                {
                    *pui1PortBundleState = LA_PORT_UP_IN_BNDL;
                }
                else if (pPortEntry->AggSelected == LA_STANDBY)
                {
                    *pui1PortBundleState = LA_PORT_STANDBY;
                }
                else if ((pPortEntry->AggSelected == LA_UNSELECTED)
                         ||
                         ((pPortEntry->AggSelected == LA_SELECTED) &&
                          (pPortEntry->LaLacActorInfo.
                           LaLacPortState.LaDistributing != LA_TRUE)))
                {
                    *pui1PortBundleState = LA_PORT_DOWN;
                }
            }
        }
        return LA_SUCCESS;
    }
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaSetSelectionPolicy                                 */
/*                                                                           */
/* Description        : This function updates the selection policy for       */
/*                      load balance                                         */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry = Pointer to the Config entry table        */
/*                      i4SelectPolicyBitList = variable which holds multiple*/
/*                      bit set                                              */
/*                                                                           */
/* Output(s)          : NULL                                                 */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/

INT4
LaSetSelectionPolicy (tLaLacAggEntry * pAggEntry, INT4 i4SelectPolicyBitList)
{
    UINT4               u4Count;
    INT4                i4Temp;
    i4Temp = i4SelectPolicyBitList;

    if ((i4SelectPolicyBitList & LA_SELECT_SRC_MAC) == LA_SELECT_SRC_MAC)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_DST_MAC) == LA_SELECT_DST_MAC)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_DA;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_SRC_DST_MAC) ==
        LA_SELECT_SRC_DST_MAC)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_DA;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_SRC_IP) == LA_SELECT_SRC_IP)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_IP;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_DST_IP) == LA_SELECT_DST_IP)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_DA_IP;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_SRC_DST_IP) == LA_SELECT_SRC_DST_IP)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_DA_IP;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_VLAN_ID) == LA_SELECT_VLAN_ID)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_VLAN_ID;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_SERVICE_INSTANCE) ==
        LA_SELECT_SERVICE_INSTANCE)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_ISID;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_MAC_SRC_VID) ==
        LA_SELECT_MAC_SRC_VID)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_VID;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_MAC_DST_VID) ==
        LA_SELECT_MAC_DST_VID)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_DA_VID;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_MAC_SRC_DST_VID) ==
        LA_SELECT_MAC_SRC_DST_VID)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_DA_VID;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_DST_IP6) == LA_SELECT_DST_IP6)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_DA_IP6;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_SRC_IP6) == LA_SELECT_SRC_IP6)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_IP6;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_L3_PROTOCOL) ==
        LA_SELECT_L3_PROTOCOL)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_L3_PROTOCOL;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_DST_L4_PORT) ==
        LA_SELECT_DST_L4_PORT)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_DA_L4_PORT;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_SRC_L4_PORT) ==
        LA_SELECT_SRC_L4_PORT)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_SA_L4_PORT;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_MPLS_VC_LABEL) ==
        LA_SELECT_MPLS_VC_LABEL)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_MPLS_VC_LABEL;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_MPLS_TUNNEL_LABEL) ==
        LA_SELECT_MPLS_TUNNEL_LABEL)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_MPLS_TUNNEL_LABEL;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_MPLS_VC_TUNNEL) ==
        LA_SELECT_MPLS_VC_TUNNEL)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_MPLS_VC_TUNNEL;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_ENHANCED) == LA_SELECT_ENHANCED)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_ENHANCED;
    }
    if ((i4SelectPolicyBitList & LA_SELECT_RANDOMIZED) == LA_SELECT_RANDOMIZED)
    {
        pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_RANDOMIZED;
    }

    /* Find Number of bits set in an Integer */
    for (u4Count = 0; i4Temp; i4Temp >>= 1)
    {
        u4Count += i4Temp & 1;
    }
    if (u4Count > 1)
    {
        if ((i4SelectPolicyBitList != LA_SELECT_SRC_DST_MAC) &&
            (i4SelectPolicyBitList != LA_SELECT_SRC_DST_IP) &&
            (i4SelectPolicyBitList != LA_SELECT_MAC_SRC_DST_VID) &&
            (i4SelectPolicyBitList != LA_SELECT_MPLS_VC_TUNNEL))

        {
            pAggEntry->AggConfigEntry.LinkSelectPolicy = LA_OTHER_OPTIONS;
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaSetSelectionPolicyBitList                          */
/*                                                                           */
/* Description        : This function updates the BitList value when         */
/*                      configured from old Low level Routine                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pAggEntry = pointer to the Aggconfig entry           */
/*                      i4SetValFsLaPortChannelSelectionPolicy =             */
/*                      Selection policy Variable                            */
/*                                                                           */
/* Output(s)          : NULL                                                 */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/ LA_FAILURE                               */
/*****************************************************************************/

INT4
LaSetSelectionPolicyBitList (tLaLacAggEntry * pAggEntry,
                             INT4 i4SetValFsLaPortChannelSelectionPolicy)
{
    pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList = 0;
    switch (i4SetValFsLaPortChannelSelectionPolicy)
    {
        case LA_SA:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SRC_MAC;
            break;
        case LA_DA:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_DST_MAC;
            break;
        case LA_SA_DA:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SRC_DST_MAC;
            break;
        case LA_SA_IP:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SRC_IP;
            break;
        case LA_DA_IP:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_DST_IP;
            break;
        case LA_SA_DA_IP:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SRC_DST_IP;
            break;
        case LA_VLAN_ID:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_VLAN_ID;
            break;
        case LA_SA_DA_IP_PORT_PROTO:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SA_DA_IP_PORT_PROTO;
            break;
        case LA_ISID:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SERVICE_INSTANCE;
            break;
        case LA_SA_VID:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_MAC_SRC_VID;
            break;
        case LA_DA_VID:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_MAC_DST_VID;
            break;
        case LA_SA_DA_VID:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_MAC_SRC_DST_VID;
            break;
        case LA_DA_IP6:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_DST_IP6;
            break;
        case LA_SA_IP6:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SRC_IP6;
            break;
        case LA_L3_PROTOCOL:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_L3_PROTOCOL;
            break;
        case LA_DA_L4_PORT:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_DST_L4_PORT;
            break;
        case LA_SA_L4_PORT:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_SRC_L4_PORT;
            break;
        case LA_MPLS_VC_LABEL:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_MPLS_VC_LABEL;
            break;
        case LA_MPLS_TUNNEL_LABEL:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_MPLS_TUNNEL_LABEL;
            break;
        case LA_MPLS_VC_TUNNEL:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_MPLS_VC_TUNNEL;
            break;
        case LA_ENHANCED:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_ENHANCED;
            break;
        case LA_RANDOMIZED:
            pAggEntry->AggConfigEntry.u4LinkSelectPolicyBitList =
                LA_SELECT_RANDOMIZED;
            break;
        case LA_OTHER_OPTIONS:
            return LA_SUCCESS;
            break;
        default:
            return LA_FAILURE;
    }
    return LA_SUCCESS;
}

/******************************************************************************
 * Function Name      : LaRBTreeCreateEmbedded
 *
 * Description        : This routine is used to disable Semaphore created with 
 *                      RBTree
 *
 * Input(s)           : u4Offset - Offset 
 *                      Cmp      - Compare Function required for RBTree Creation
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Pointer to RBTree
 *****************************************************************************/
tRBTree
LaRBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp)
{
    tRBTree             RBTree;
    RBTree = RBTreeCreateEmbedded (u4Offset, Cmp);
    if (RBTree == NULL)
    {
        return NULL;
    }
    RBTreeDisableMutualExclusion (RBTree);
    return RBTree;
}

/***************************************************************************************
 *
 *    FUNCTION NAME    : LaCmpPortNo
 *
 *    DESCRIPTION      : This routine is used to compare the nodes of RBTree
 *                       based on port no in a particular remote port entry
 *
 *    INPUT            : *pConsPortEntry1 - pointer to Consolidated port entry Node1
 *                       *pConsPortEntry2 - pointer to Consolidated port entry Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if (pE1 port Num) > 
 *                       (pE2 port nNum)
 *                       else returns -1
 *                                                                                       *
 *****************************************************************************************/
PUBLIC INT4
LaCmpPortNo (tRBElem * pConsPortEntry1, tRBElem * pConsPortEntry2)
{
    tLaDLAGConsPortEntry *pEntryA = NULL;
    tLaDLAGConsPortEntry *pEntryB = NULL;

    pEntryA = (tLaDLAGConsPortEntry *) pConsPortEntry1;
    pEntryB = (tLaDLAGConsPortEntry *) pConsPortEntry2;

    /* RBTree Index is PortIndex */
    if (pEntryA->u4PortIndex < pEntryB->u4PortIndex)
    {
        return (-1);
    }
    if (pEntryA->u4PortIndex > pEntryB->u4PortIndex)
    {
        return (1);
    }

    return (0);
}

/***************************************************************************************
 *
 *    FUNCTION NAME    : LaCmpPortPriority
 *
 *    DESCRIPTION      : This routine is used to compare nodes of RBTree
 *                       Ports (indexed by port no and priority )
 *                       in a particular LAConsolidatedList tree node.
 *
 *    INPUT            : *pConsPortEntry1 - pointer to Consolidated port entry Node1
 *                       *pConsPortEntry2 - pointer to Consolidated port entry Node2
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : If equal returns 0, returns 1 if pE1 > pE2
 *                       else returns -1
 *
 *****************************************************************************************/

PUBLIC INT4
LaCmpPortPriority (tRBElem * pConsPortEntry1, tRBElem * pConsPortEntry2)
{
    tLaDLAGConsPortEntry *pEntryA = NULL;
    tLaDLAGConsPortEntry *pEntryB = NULL;

    pEntryA = (tLaDLAGConsPortEntry *) pConsPortEntry1;
    pEntryB = (tLaDLAGConsPortEntry *) pConsPortEntry2;

    /* Priority is less */
    if (pEntryA->u2Priority < pEntryB->u2Priority)
    {
        return (-1);
    }
    /* Priority is greater */
    if (pEntryA->u2Priority > pEntryB->u2Priority)
    {
        return (1);
    }
    /*  Priority equal. Check port No */
    if (pEntryA->u4PortIndex < pEntryB->u4PortIndex)
    {
        return (-1);
    }

    if (pEntryA->u4PortIndex > pEntryB->u4PortIndex)
    {
        return (1);
    }

    return (0);

}

/******************************************************************************
 *
 *    FUNCTION NAME    : LaIsMCLAGEnabledOnAgg
 *
 *    DESCRIPTION      : This routine is used to check if MCLAG is enabled on
 *                       a port-channel interface
 *
 *    INPUT            : *pAggEntry - Aggregation entry
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : LA_TRUE / LA_FALSE
 *
 ******************************************************************************/
UINT1
LaIsMCLAGEnabledOnAgg (tLaLacAggEntry * pAggEntry)
{
    if (pAggEntry != NULL)
    {
        if ((LA_MCLAG_SYSTEM_STATUS == LA_MCLAG_ENABLED) &&
            (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED))
        {
            return LA_TRUE;
        }
    }
    return LA_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function         :   LaGetSyslogLevel                                     */
/*                                                                           */
/* Description      :   Gets the Syslog level based on Input Trace type      */
/*                                                                           */
/* Input(s)         :   u2TraceType                                          */
/*                                                                           */
/* Output(s)        :   None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred         :   None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified         :   None                                                 */
/*                                                                           */
/* Return Value(s)  :   Syslog Level based on Trace type                     */
/*****************************************************************************/
UINT2
LaGetSyslogLevel (UINT2 u2TraceType)
{
    UINT2               u2SyslogLevel = 0;
    switch (u2TraceType)
    {
        case CONTROL_PLANE_TRC:
            u2SyslogLevel = SYSLOG_INFO_LEVEL;
            break;

        case ALL_FAILURE_TRC:
            u2SyslogLevel = SYSLOG_ALERT_LEVEL;
            break;

        default:
            break;
    }
    return u2SyslogLevel;
}

#ifdef EVPN_VXLAN_WANTED
/******************************************************************************
 *    FUNCTION NAME    : LaUtilEvpnGetMCLAGSystemID
 *    DESCRIPTION      : This routine is used to get MCLAGSystemID
 *    INPUT            : NONE
 *    OUTPUT           : pRetValEvpnMCLAGSystemID
 *                       pi4EvpnMCLAGSystemPriority
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 ******************************************************************************/
INT4
LaUtilEvpnGetMCLAGSystemID (tMacAddr * pRetValEvpnMCLAGSystemID,
                            INT4 *pi4EvpnMCLAGSystemPriority)
{
#ifdef ICCH_WANTED
    if ((nmhGetFsLaMCLAGSystemID (pRetValEvpnMCLAGSystemID)) == SNMP_SUCCESS)
    {
        if (nmhGetFsLaMCLAGSystemPriority (pi4EvpnMCLAGSystemPriority) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pRetValEvpnMCLAGSystemID);
    UNUSED_PARAM (pi4EvpnMCLAGSystemPriority);

    return SNMP_SUCCESS;
#endif
}
#endif /* EVPN_VXLAN_WANTED */
/******************************************************************************
    FUNCTION NAME    : LaUtilClearPortChannelCounters
    DESCRIPTION      : This routine is used to clear port channel 
                       and physical port counters
    INPUT            : u2AggIndex - Aggregate Index
    OUTPUT           : 
    RETURNS          : LA_SUCCESS / LA_FAILURE
******************************************************************************/
INT4
LaUtilClearPortChannelCounters (UINT2 u2AggIndex)
{
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;
    UINT2               u2Port = LA_INIT_VAL;
    UINT2               u2NextPort = LA_INIT_VAL;

    LaGetAggIfEntry (u2AggIndex, &pLaLacAggIfEntry);

    if (pLaLacAggIfEntry == NULL)
    {
        return LA_FAILURE;
    }

    pLaLacAggIfEntry->i4AggInOctets = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInDiscards = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInErrors = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInUnknownProtos = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInUcastPkts = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInFrames = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutOctets = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutDiscards = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutErrors = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutUcastPkts = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutFrames = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInMcastPkts = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggInBcastPkts = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutMcastPkts = LA_INIT_VAL;
    pLaLacAggIfEntry->i4AggOutBcastPkts = LA_INIT_VAL;

    u2Port = LA_INIT_VAL;
    u2NextPort = LA_INIT_VAL;

    while (LaUtilGetNextAggPort ((UINT2) u2AggIndex, u2Port, &u2NextPort)
           != LA_FAILURE)
    {
        LaGetPortEntry ((UINT2) u2NextPort, &pPortEntry);

        if (pPortEntry == NULL)
        {
            continue;
        }
        pPortEntry->LaPortStats.u4LacpduTx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4LacpduRx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4MarkerPduRx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4MarkerResponsePduRx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4UnknownRx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4IllegalRx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4LacpduTx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4MarkerPduTx = LA_INIT_VAL;
        pPortEntry->LaPortStats.u4MarkerResponsePduTx = LA_INIT_VAL;
        pPortEntry->LaPortDebug.u4ActorSyncTransitionCount = LA_INIT_VAL;
        pPortEntry->LaPortDebug.u4PartnerSyncTransitionCount = LA_INIT_VAL;
        pPortEntry->LaPortDebug.u4ActorChangeCount = LA_INIT_VAL;
        pPortEntry->LaPortDebug.u4PartnerChangeCount = LA_INIT_VAL;
        pPortEntry->u4ErrorDetectCount = LA_INIT_VAL;
        pPortEntry->u4RecTrgdCount = LA_INIT_VAL;
        u2Port = u2NextPort;

    }
    return LA_SUCCESS;
}

 /***************************************************************************
 *
 *    Function Name       : LaNoMclag
 *
 *    Description         : This function is used as factory reset function
 *                          for MC-LAG.It disables MC-LAG in the system and
 *                          resets all mc-lag configurations.
 *                          ICCL configurations will be removed and it will be
 *                          reset to default value.
 *
 *    Input(s)            : u4InstanceId -Instance identifier
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : LA_MCLAG_SYSTEM_STATUS - MC-LAG status(disabled)
 *                          LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem - Global system
 *                                                   identifier for MC-LAG.
 *                          LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority
 *                                                 - MC-LAG Global system priority.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *
 *    Returns                  : LA_SUCCESS/LA_FAILURE
 *
 *****************************************************************************/

#ifdef ICCH_WANTED
INT4
LaNoMclag (UINT4 u4InstanceId)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tMacAddr            DefaultMacAddr;

    UNUSED_PARAM (u4InstanceId);
    LA_MEMSET (DefaultMacAddr, 0, LA_MAC_ADDRESS_SIZE);
    /*When No MCLAG command is given below operations should be done*
     * 1. Disable MC-LAG in port-chanel
     * 2. Disable MC-LAG globally
     a.ICCL port-channel,IVR and VLAN should be deleted
     b.Reset MC_LAG configurations
     * 3. Reset ICCL parameters*/

    /*1. Disable MC-LAG in port-chanel */
    LaGetNextAggEntry (NULL, &pAggEntry);
    while (pAggEntry != NULL)
    {
        if (pAggEntry->u1MCLAGStatus == LA_MCLAG_ENABLED)
        {
            pAggEntry->u1MCLAGStatus = LA_MCLAG_DISABLED;
            pAggEntry->DLAGSystem.u2SystemPriority =
                LA_DLAG_DEFAULT_SYSTEM_PRIORITY;
            LA_MEMCPY ((pAggEntry->DLAGSystem.SystemMacAddr), DefaultMacAddr,
                       LA_MAC_ADDRESS_SIZE);
            if (LaDLAGDeInit (pAggEntry) != LA_SUCCESS)
            {
                return LA_FAILURE;
            }
            pAggEntry->u1MCLAGRolePlayed = LA_MCLAG_SYSTEM_ROLE_MASTER;
            /*Clear MC-LAG Counters */
            pAggEntry->u4DLAGPeriodicSyncPduTxCount = 0;
            pAggEntry->u4DLAGPeriodicSyncPduRxCount = 0;
            pAggEntry->u4DLAGEventUpdatePduTxCount = 0;
            pAggEntry->u4DLAGEventUpdatePduRxCount = 0;

        }

        LaGetNextAggEntry (pAggEntry, &pAggEntry);
    }

    /*2 .Disable MCLAG Globally */
    LA_MCLAG_SYSTEM_STATUS = LA_MCLAG_DISABLED;
    LA_MEMCPY ((LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.SystemMacAddr),
               DefaultMacAddr, LA_MAC_ADDRESS_SIZE);
    LA_DLAG_GLOBAL_INFO.GlobalDLAGCommonSystem.u2SystemPriority =
        LA_DLAG_DEFAULT_SYSTEM_PRIORITY;
    LA_DLAG_GLOBAL_INFO.u4GlobalMCLAGPeriodicSyncTime =
        LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME;
    LaActiveMCLAGDisable ();

    /*Stop Sending HB packets to the peer node.In Processing of the event ICCL
     *configurations will be deleted*/
    HbApiSendEvtFromMCLAG (HB_MCLAG_STOP_HB_MSG);

    /*3. Delete ICCL interface,IVR and VLAN .Reset ICCL parameters */
    IcchApiSendMCLAGStatus (MCLAG_DISABLED);

    LA_AA_DLAG_ROLE_PLAYED = LA_MCLAG_SYSTEM_ROLE_NONE;
    LA_AA_DLAG_MASTER_DOWN = LA_TRUE;

    HbApiSendEvtFromMCLAG (HB_MCLAG_SHUTDOWN);

    UNUSED_PARAM (u4InstanceId);
    return LA_SUCCESS;
}

#endif
 /*****************************************************************************/
 /* Function Name      : LaSyslogPrintAcivePorts.                             */
 /*                                                                           */
 /* Description        : This function is called whenever there is a change   */
 /*                      in Bundle state of ports aggregated to               */
 /*                      port-channel                                         */
 /*                                                                           */
 /* Input(s)           : pPortEntry                 .                         */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None.                                                */
 /*                                                                           */
 /* Return Value(s)    : None.                                                */
 /*****************************************************************************/
VOID
LaSyslogPrintAcivePorts (tLaLacPortEntry * pPortEntry)
{
    INT4                i4BundleState = 0;
    INT4                i4PortMode = 0;
    UINT2               u2Port = 0;
    UINT2               u2NextPort = 0;
    UINT2               u2Count = 0;
    UINT2               au2Ports[LA_MAX_PORTS];
    UINT2               u2Index = 0;
    tLaLacAggEntry     *pLaLacAggEntry = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *piIfName = NULL;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    pLaLacAggEntry = pPortEntry->pAggEntry;

    if (pLaLacAggEntry == NULL)
    {
        return;
    }
    LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC, "Port-Channel : %d \r\n",
                     pLaLacAggEntry->AggConfigEntry.u2ActorAdminKey);

    u2Port = 0;
    u2NextPort = 0;
    LaUtilGetNextAggPort (pLaLacAggEntry->u2AggIndex, u2Port, &u2NextPort);
    LA_MEMSET (au2Ports, 0, sizeof (au2Ports));
    u2Count = 0;

    while ((u2NextPort != 0) && (u2Count < LA_MAX_PORTS))
    {
        u2Port = u2NextPort;
        au2Ports[u2Count] = u2Port;
        u2Count++;
        LaUtilGetNextAggPort (pLaLacAggEntry->u2AggIndex, u2Port, &u2NextPort);
    }
    LA_SYS_TRC (CONTROL_PLANE_TRC, "Active ports \r\n");
    for (u2Index = 0; ((u2Index < u2Count) &&
                       (u2Index < LA_MAX_PORTS)); u2Index++)
    {
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        if (LaGetPortEntry (au2Ports[u2Index], &pPortEntry) == LA_FAILURE)
        {
            continue;
        }
        CfaCliConfGetIfName ((UINT4) au2Ports[u2Index], piIfName);
        nmhGetFsLaPortBundleState (au2Ports[u2Index], &i4BundleState);
        if (i4PortMode != LA_MODE_DISABLED)
        {
            if (i4BundleState == LA_PORT_UP_IN_BNDL)
            {
                LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC, "%s (Up-In-Bundle)  \r\n",
                                 piIfName);
            }
            else if (i4BundleState == LA_PORT_DOWN)

            {
                LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC, "%s (Down-In-Bundle)  \r\n",
                                 piIfName);

            }

            else if (i4BundleState == LA_PORT_STANDBY)
            {
                LA_SYS_TRC_ARG1 (CONTROL_PLANE_TRC, "%s%d (Hot-Standby) \r\n",
                                 piIfName);

            }
        }
    }
}

 /*****************************************************************************/
 /* Function Name      : LaUtilTicksToDate.                                   */
 /*                                                                           */
 /* Description        : This function converts the Ticks to Date Format      */
 /*                                                                           */
 /* Input(s)           : u4Ticks.                                             */
 /*                                                                           */
 /* Output(s)          : u1DateFormat - Formatted Date                        */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None.                                                */
 /*                                                                           */
 /* Return Value(s)    : None.                                                */
 /*****************************************************************************/
VOID
LaUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat)
{
    tUtlTm              tm;
    UINT1              *au1Months[] = {
        (UINT1 *) "Jan", (UINT1 *) "Feb", (UINT1 *) "Mar",
        (UINT1 *) "Apr", (UINT1 *) "May", (UINT1 *) "Jun",
        (UINT1 *) "Jul", (UINT1 *) "Aug", (UINT1 *) "Sep",
        (UINT1 *) "Oct", (UINT1 *) "Nov", (UINT1 *) "Dec"
    };
    UtlGetTimeForTicks (u4Ticks, &tm);
    SPRINTF ((CHR1 *) pu1DateFormat, "%u %s %u %02u:%02u:%02u",
             tm.tm_mday,
             au1Months[tm.tm_mon],
             tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

 /*****************************************************************************/
 /* Function Name      : LaGetSameState.                                      */
 /*                                                                           */
 /* Description        : This function fetches LACP port state when port      */
 /*                      stuck in same state.                                 */
 /*                                                                           */
 /* Input(s)           : u1PortStates.                                        */
 /*                                                                           */
 /* Output(s)          : u1RecSameState - Port State                          */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Referred           : None                                                 */
 /*                                                                           */
 /* Global Variables                                                          */
 /* Modified           : None.                                                */
 /*                                                                           */
 /* Return Value(s)    : None.                                                */
 /*****************************************************************************/
VOID
LaGetSameState (UINT1 u1PortState, UINT1 *pu1RecSameState)
{
    if ((u1PortState & LA_AGGREGATION_BITMASK) != LA_FALSE)
    {
        *pu1RecSameState = LA_AGGREGATION_STATE;
    }
    if ((u1PortState & LA_SYNC_BITMASK) != LA_FALSE)
    {
        *pu1RecSameState = LA_SYNC_STATE;
    }
    if ((u1PortState & LA_COLLECTING_BITMASK) != LA_FALSE)
    {
        *pu1RecSameState = LA_COLLECTING_STATE;
    }
}

/******************************************************************************
 *    FUNCTION NAME    : LaHandleAggProgramFailed
 *    DESCRIPTION      : This function is called from LA ,whenever the port
 *                 status of all the ports in the aggregator entry 
 *                 can't be populated
 *    INPUT            : u2AggId - u2AggId      - Index of the port channel
 *    OUTPUT           : NONE
 *    RETURNS          : LA_SUCCESS/LA_FAILURE
 ******************************************************************************/
INT4
LaHandleAggProgramFailed (UINT2 u2AggId)
{
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaLacPortEntry    *pPortEntry = NULL;

    if (LaGetAggEntry (u2AggId, &pAggEntry) == LA_FAILURE)
    {
        return LA_FAILURE;
    }
    if (pAggEntry->LaLacpMode == LA_MODE_LACP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pPortEntry);
        while (pPortEntry != NULL)
        {
            pPortEntry->LaLacActorInfo.u2IfKey = 0;

            pPortEntry->NttFlag = LA_TRUE;
            LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
            if ((gu4RecoveryTime != 0) &&
                (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_NONE))

            {
                if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
                    (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP))
                {
                    pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_HW_FAILURE;

                    if (LaStartTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
                    {
                        LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                     "LA: Recovery Timer Start FAILED for Port %d \r\n",
                                     pPortEntry->u2PortIndex);
                        pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
                        return LA_FAILURE;
                    }
                    pPortEntry->u4ErrorDetectCount++;
                }
            }

            LaGetNextAggPortEntry (pAggEntry, pPortEntry, &pPortEntry);
        }
    }
    return LA_SUCCESS;
}

/* end of file */
