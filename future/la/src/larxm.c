/*****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: larxm.c,v 1.44 2017/11/16 14:26:55 siva Exp $
* Licensee Aricent Inc., 2001-2002
*****************************************************************************/
/*    FILE  NAME            : larxm.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains contains function for the   */
/*                            Receive State Machine                          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include "lahdrs.h"

/*****************************************************************************/
PRIVATE INT4 LaLacDynamicAggregator PROTO ((tLaLacPortEntry *));
PRIVATE INT4 LaLacFindAndAttachNewAgg PROTO ((tLaLacPortEntry *));
PRIVATE INT4 LaLacFindBestAggregator PROTO ((tLaLacPortEntry *,
                                             tLaLacAggEntry **));
PRIVATE INT4 LaLacAttachPortToNewAgg PROTO ((tLaLacPortEntry *,
                                             tLaLacAggEntry *));

/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : LaLacIfSamePort.                                     */
/*                                                                           */
/* Description        : This function is called to check whether the         */
/*                      interface information contained in both the IfInfo   */
/*                      structures is from the same port. This function is   */
/*                      called from LaRxMachine() to check whether a Port    */
/*                      has moved to another aggregator.                     */
/*                                                                           */
/* Input(s)           : pLaLacIfInfo, pLaLacIfInfo.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - If the interface info matches              */
/*                      LA_FALSE - If the interface info does not match      */
/*****************************************************************************/
tLaBoolean
LaLacIfSamePort (tLaLacIfInfo * pLaLacAInfo, tLaLacIfInfo * pLaLacBInfo)
{
    if ((pLaLacAInfo->u2IfIndex == pLaLacBInfo->u2IfIndex) &&
        LA_SYSTEM_IDS_EQUAL (pLaLacAInfo->LaSystem, pLaLacBInfo->LaSystem))
    {
        return LA_TRUE;
    }
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaLacMatchIfInfo.                                    */
/*                                                                           */
/* Description        : This function is called to check if the interface    */
/*                      information in both the IfInfo structures is the     */
/*                      same. This function is called find out whether the   */
/*                      Actor has the correct information about the partner. */
/*                                                                           */
/* Input(s)           : Pointers to 2 IfInfo structures.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - If IfInfo matches                          */
/*                      LA_FALSE - If IfInfo does not match.                 */
/*****************************************************************************/
tLaBoolean
LaLacMatchIfInfo (tLaLacIfInfo * pAIfInfo, tLaLacIfInfo * pBIfInfo)
{
    if ((LA_SYSTEM_IDS_EQUAL (pAIfInfo->LaSystem, pBIfInfo->LaSystem)) &&
        (pAIfInfo->u2IfKey == pBIfInfo->u2IfKey) &&
        (pAIfInfo->u2IfPriority == pBIfInfo->u2IfPriority) &&
        (pAIfInfo->u2IfIndex == pBIfInfo->u2IfIndex) &&
        (pAIfInfo->LaLacPortState.LaAggregation ==
         pBIfInfo->LaLacPortState.LaAggregation))
    {
        return LA_TRUE;
    }
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : LaLacCopyIfInfo.                                     */
/*                                                                           */
/* Description        : This function is called to copy the contents of one  */
/*                      IfInfo structure to another.                         */
/*                                                                           */
/* Input(s)           : pFromIfInfo.                                         */
/*                                                                           */
/* Output(s)          : pToIfInfo.                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaLacCopyIfInfo (tLaLacIfInfo * pFromIfInfo, tLaLacIfInfo * pToIfInfo)
{
    pToIfInfo->LaSystem = pFromIfInfo->LaSystem;
    pToIfInfo->u2IfKey = pFromIfInfo->u2IfKey;
    pToIfInfo->u2IfPriority = pFromIfInfo->u2IfPriority;
    pToIfInfo->u2IfIndex = pFromIfInfo->u2IfIndex;
    pToIfInfo->LaLacPortState = pFromIfInfo->LaLacPortState;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacRecordActorAdminIfInfo.                         */
/*                                                                           */
/* Description        : This function is called to copy the Actor's Default  */
/*                      Interface Info into as the Actor's Operational Info  */
/*                      structure.                                           */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaLacRecordActorAdminIfInfo (tLaLacPortEntry * pPortEntry)
{
    LaLacCopyIfInfo (&(pPortEntry->LaLacActorAdminInfo),
                     &(pPortEntry->LaLacActorInfo));

    /* If the Aggregator associated with this Port is
     * failed while adding to the hardware, then the actor operkey
     * of this port should be set to zero */
    if (pPortEntry->pAggEntry != NULL)
    {
        if (pPortEntry->pAggEntry->HwAggStatus == LA_AGG_ADD_FAILED_IN_HW)
        {
            pPortEntry->LaLacActorInfo.u2IfKey = 0;
        }
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacRecordPartnerOperIfInfo                         */
/*                                                                           */
/* Description        : This function is called to copy the partner's        */
/*                      interface info received in the most recent LACPDU    */
/*                      as the Partner's Oper Info.                          */
/*                                                                           */
/* Input(s)           : pPortEntry , pLaLacpduInfo.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaLacRecordPartnerOperIfInfo (tLaLacpduInfo * pLaLacpduInfo,
                              tLaLacPortEntry * pPortEntry)
{
    tLaBoolean          PrevPartnerSyncFlag;

    tNotifyProtoToApp   NotifyProtoToApp;
    tLaParamInfo       *pLaParamInfo = NULL;
    tLaLacIfInfo        PartnerInfo;
    tLaLacIfInfo        PartnerNewInfo;

    MEMSET (&PartnerInfo, 0, sizeof (tLaLacIfInfo));
    MEMSET (&PartnerNewInfo, 0, sizeof (tLaLacIfInfo));

    PrevPartnerSyncFlag =
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization;

    pPortEntry->u2SyncUp =
        LA_RED_IS_PARTNR_ST_CHANGED (pLaLacpduInfo, pPortEntry);

    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    /*The existing information stored for the partner is compared with the
     * information contained in the newly received LACP PDU and if any of
     * the parameter has changed ,then notification is send to applications
     */

    /*The existing information stored about the partner's lacp parameters
     *is copied to PartnerInfo*/
    MEMCPY (&PartnerInfo, &(pPortEntry->LaLacPartnerInfo),
            sizeof (tLaLacIfInfo));

    /*The new information stored about the partner's lacp parameters
     *is copied to PartnerNewInfo*/
    MEMCPY (&PartnerNewInfo, &(pLaLacpduInfo->LaLacActorInfo),
            sizeof (tLaLacIfInfo));

    if (MEMCMP (&PartnerInfo, &PartnerNewInfo, sizeof (tLaLacIfInfo)) != 0)
    {
        /*If the new information or the partner is different from
         * the stored information then the indication is send to the
         * higher layer applications*/

        pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);

        MEMCPY (&(pLaParamInfo->SystemId.SystemMacAddr),
                &((pLaLacpduInfo->LaLacActorInfo).LaSystem.SystemMacAddr),
                sizeof (tMacAddr));

        pLaParamInfo->SystemId.u2SystemPriority =
            (pLaLacpduInfo->LaLacActorInfo).LaSystem.u2SystemPriority;

        pLaParamInfo->u2IfIndex = (pLaLacpduInfo->LaLacActorInfo).u2IfIndex;
        pLaParamInfo->u2IfKey = (pLaLacpduInfo->LaLacActorInfo).u2IfKey;

        NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_PARAMETER_CHANGE;

        LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
    }

    /* This routine performs the recordPDU procedure 
     * defined in STD IEEE802.3 - 2000, section 43.4.9 
     */
    LaLacCopyIfInfo (&(pLaLacpduInfo->LaLacActorInfo),
                     &(pPortEntry->LaLacPartnerInfo));

    pPortEntry->LaLacActorInfo.LaLacPortState.LaDefaulted = LA_FALSE;
    /* Partner Oper State is Synchronized if the Partner has correct 
     * information about the actor or the partner is individual */

    /* Check if the partner has the correct information about the Actor i.e.
     * Partner Oper State is synchronized or not */
    if ((((LaLacMatchIfInfo (&(pLaLacpduInfo->LaLacPartnerInfo),
                             &(pPortEntry->LaLacActorInfo)) == LA_TRUE)
          && (pLaLacpduInfo->LaLacActorInfo.
              LaLacPortState.LaSynchronization == LA_TRUE)) ||
         /* Check if Partner is individual */
         ((pLaLacpduInfo->LaLacActorInfo.
           LaLacPortState.LaAggregation == LA_FALSE) &&
          (pLaLacpduInfo->LaLacActorInfo.
           LaLacPortState.LaSynchronization == LA_TRUE))) &&
        /* The link will be actively maintained in 
         * the aggregation if either the Actor
         * or the Partner has ACTIVE participation in LACP */
        /* Check if the Partner is ACTIVE */
        ((pLaLacpduInfo->LaLacActorInfo.
          LaLacPortState.LaLacpActivity == LA_ACTIVE) ||
         /* Check if the Actor is ACTIVE and the Partner 
          * has correct information about it */
         ((pPortEntry->LaLacActorInfo.
           LaLacPortState.LaLacpActivity == LA_ACTIVE) &&
          (pLaLacpduInfo->LaLacPartnerInfo.
           LaLacPortState.LaLacpActivity == LA_ACTIVE))))
    {
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization = LA_TRUE;
    }
    else
    {
        pPortEntry->LaLacPartnerInfo.
            LaLacPortState.LaSynchronization = LA_FALSE;
        /* Mismatch between the actor information present in port and the
         * partner information from the received LACPU. Hence sync up this
         * information with standby node.*/
        pPortEntry->u2SyncUp = LA_TRUE;
    }

    /* If Partnet_Oper_Port_State.Synchronization value changes then we need to
     * sync the information with the standby node. */
    if (PrevPartnerSyncFlag !=
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization)
    {
        pPortEntry->u2SyncUp = LA_TRUE;
    }
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacRecordPartnerAdminIfInfo.                       */
/*                                                                           */
/* Description        : This function is called to record the partner's admin*/
/*                      info as the operational values for the partner. This */
/*                      will be done during port initialization for forming  */
/*                      LACPDU.                                              */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaLacRecordPartnerAdminIfInfo (tLaLacPortEntry * pPortEntry)
{
    LaLacCopyIfInfo (&(pPortEntry->LaLacPartnerAdminInfo),
                     &(pPortEntry->LaLacPartnerInfo));

    pPortEntry->LaLacActorInfo.LaLacPortState.LaDefaulted = LA_TRUE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacUpdateSelectedFromReceivedLacpdu                */
/*                                                                           */
/* Description        : This function is called to update the port's selected*/
/*                      variable by comparing the partner's IfInfo with the  */
/*                      actor's(which is the partner for this port)IfInfo    */
/*                      in the received LACPDU in the CURRENT state.         */
/*                                                                           */
/* Input(s)           : pPortEntry , pLaLacpduInfo.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
/*  In the RxmCurrent state, the port updates its selected variable by comparing
 *  the partner's IfInfo with the actor's IfInfo in Received LACPDU */
VOID
LaLacUpdateSelectedFromReceivedLacpdu (tLaLacpduInfo * pLaLacpduInfo,
                                       tLaLacPortEntry * pPortEntry)
{
    if (!(LaLacMatchIfInfo (&(pLaLacpduInfo->LaLacActorInfo),
                            &(pPortEntry->LaLacPartnerInfo))))
    {
        /* Actor Info in received PDU doesnt match with that stored 
         * in the port operational value,
         * so Selected is set to UNSELECTED.
         */
#ifdef TRACE_WANTED
        LaPrintIfInfo (pPortEntry, &(pLaLacpduInfo->LaLacActorInfo),
                       &(pPortEntry->LaLacPartnerInfo));
#endif

        /* Need to sync up changed partner information with standby node. */
        pPortEntry->u2SyncUp = LA_TRUE;
        pPortEntry->AggSelected = LA_UNSELECTED;
    }
}

/*****************************************************************************/
/* Function Name      : LaLacUpdateSelectedFromPartnerAdmin.                 */
/*                                                                           */
/* Description        : In the EXPIRED state this function is called to      */
/*                      update the partner's selected variable from the      */
/*                      partner admin interface information.                 */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
/*  In the RxmExpired state, the port updates its selected variable by comparing
 *  the partner's IfInfo with the partner's Admin IfInfo */
VOID
LaLacUpdateSelectedFromPartnerAdmin (tLaLacPortEntry * pPortEntry)
{
    if (!(LaLacMatchIfInfo (&(pPortEntry->LaLacPartnerAdminInfo),
                            &(pPortEntry->LaLacPartnerInfo))))
    {
        pPortEntry->AggSelected = LA_UNSELECTED;
    }
}

/*****************************************************************************/
/* Function Name      : LaLacUpdateNtt.                                      */
/*                                                                           */
/* Description        : This function updates the value of the NTT variable. */
/*                      The parameter values for the partner carried in the  */
/*                      received LACPDU are compared with the corresponding  */
/*                      operational vales for the Actor. If one or more of   */
/*                      values in the received LACPDU differ from the current*/
/*                      operational values, NTT is set to TRUE.              */
/*                                                                           */
/* Input(s)           : pPortEntry , ppLaLacpduInfo.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacUpdateNtt (tLaLacPortEntry * pPortEntry, tLaLacpduInfo * pLaLacpduInfo)
{
    tLaLacPortState    *pLacpduPartnerState = NULL;
    tLaLacPortState    *pActorState = NULL;

    pLacpduPartnerState = &(pLaLacpduInfo->LaLacPartnerInfo.LaLacPortState);
    pActorState = &(pPortEntry->LaLacActorInfo.LaLacPortState);

    /* If a mismatch is found between the Partner's knowledge about Actor (from
     * the last received LACPDU) and the Actor's info, an event EVENT_NTT is 
     * generated and transmit machine is called to send LACPDU. This LACPDU 
     * transmitted will contain the Partner's info as stored with the Actor
     * and not the Actor's info in the received LACPDU. This will result in 
     * generation of EVENT_NTT twice at both the ends during system 
     * initialization but will help to synchronize faster when the system is 
     * running. 
     */
    if (!(LaLacMatchIfInfo (&(pLaLacpduInfo->LaLacPartnerInfo),
                            &(pPortEntry->LaLacActorInfo))) ||
        (pLacpduPartnerState->LaLacpActivity != pActorState->LaLacpActivity) ||
        (pLacpduPartnerState->LaSynchronization !=
         pActorState->LaSynchronization) ||
        (pLacpduPartnerState->LaLacpTimeout != pActorState->LaLacpTimeout))
    {

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "RXM: Port: %u :: Calling TxMachine with EVENT NTT...\n",
                     pPortEntry->u2PortIndex);
        pPortEntry->NttFlag = LA_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : LaLacInitializeCurrentWhileTimer.                    */
/*                                                                           */
/* Description        : This function is called to initialise the Current    */
/*                      While timer by the Short Timeout Time or by Long     */
/*                      Timeout Time after a LACPDU has been received.       */
/*                                                                           */
/* Input(s)           : pPortEntry , LaLacpTimeout.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacInitializeCurrentWhileTimer (tLaLacPortEntry * pPortEntry,
                                  tLaLacpTimeout LaLacpTimeout)
{

    if (LaLacpTimeout == LA_SHORT_TIMEOUT)
    {
        pPortEntry->u4CurrentWhileTime = LA_SHORT_TIMEOUT_TIME;
    }
    else
    {
        pPortEntry->u4CurrentWhileTime = LA_LONG_TIMEOUT_TIME;
    }

    /* Start timer for one third of the u4CurrentWhileTime.
     * Whenever this timer expires increment u1TimerCount and send
     * a syncup message to standby node (in case of redundancy).
     * Once the u1TimerCount reaches 2 send currentwhileTimer expiry
     * event to the Receive sem. This approach will make the new 
     * active node to identify the current while timer expiry event
     * even if the currentwhiletimer expiry syncup message is missed.*/
    if ((pPortEntry->LaRxmState == LA_RXM_STATE_CURRENT) &&
        (pPortEntry->u1TimerCount != 0))
    {
        /* Initimate the standby node about the count reset. If there is
         * any change in the information from the received packet and the
         * packet that was receied before the present packet, then it will
         * be synced up with the standby node. Hence Combine u1TimerCount
         * reset information with it other wise send a dynamic sync up 
         * seperately. */
        pPortEntry->u2SyncUp = LA_TRUE;
    }

    pPortEntry->u1TimerCount = 0;
    pPortEntry->u4CurrentWhileTime = ((pPortEntry->u4CurrentWhileTime) /
                                      LA_CURRWHILE_SPLIT_INTERVALS);

    if (LaStartTimer (&pPortEntry->CurrentWhileTmr) == LA_FAILURE)
    {
        return;
    }
}

/*****************************************************************************/
/* Function Name      : LaLacRxmPortInitialize.                              */
/*                                                                           */
/* Description        : This function is called during initialisation of a   */
/*                      port from the Receive Machine when PORT_INIT event   */
/*                      is received by it.                                   */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaLacRxmPortInitialize (tLaLacPortEntry * pPortEntry)
{

    tLaLacAggEntry     *pAggEntry = NULL;

    pAggEntry = pPortEntry->pAggEntry;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-RXM: Port %d INITIALIZE\n",
                 pPortEntry->u2PortIndex);

    if (LaStopTimer (&pPortEntry->CurrentWhileTmr) == LA_FAILURE)
    {
        return LA_FAILURE;
    }

    if (pPortEntry->AggSelected != LA_UNSELECTED)
    {
        if (pPortEntry->pAggEntry == NULL)
        {
            return LA_FAILURE;
        }
        if (pPortEntry->AggSelected == LA_SELECTED)
        {
            pPortEntry->pAggEntry->u1SelectedPortCount--;
        }
        pPortEntry->AggSelected = LA_UNSELECTED;
        LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_UNSELECTED);
    }
    LaLacRecordActorAdminIfInfo (pPortEntry);
    LaLacRecordPartnerAdminIfInfo (pPortEntry);
    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_FALSE;

    if ((pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED) &&
        (pAggEntry->u1PortDisabledCounter > 0))
    {
        pAggEntry->u1PortDisabledCounter--;
    }

    pPortEntry->LaRxmState = LA_RXM_STATE_INITIALIZE;
    pPortEntry->InDefaulted = LA_FALSE;
    pPortEntry->b1PortMoved = LA_FALSE;

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacRxmPortDisabled.                                */
/*                                                                           */
/* Description        : This function is called during port deletion or      */
/*                      disabling to update the LaLagIdMatched variable.     */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
VOID
LaLacRxmPortDisabled (tLaLacPortEntry * pPortEntry)
{
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4Count1 = 0;
    UINT4               u4Count2 = 0;

    pAggEntry = pPortEntry->pAggEntry;
    pPortEntry->InDefaulted = LA_FALSE;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-RXM: Port %d PORT_DISABLED\n",
                 pPortEntry->u2PortIndex);

    if (LaStopTimer (&pPortEntry->CurrentWhileTmr) == LA_FAILURE)
    {
        return;
    }

    if (LaStopTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
    {
        return;
    }

    if (pAggEntry == NULL)
    {
        return;
    }
    pPortEntry->bPartnerActive = LA_FALSE;
    pPortEntry->u4SameStateCount = 0;
    pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization = LA_FALSE;

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
        (pPortEntry->u1DLAGMasterAck == LA_TRUE) &&
        (pAggEntry->u1DLAGRolePlayed == LA_DLAG_SYSTEM_ROLE_SLAVE))
    {
        /* Incase of Active D-LAG remove port from HL.
           Because following Call will move statemachine to
           LaMuxStateAttached which will set DLAG params as FALSE */
        LaActiveDLAGHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
    }
#ifdef ICCH_WANTED
    else if ((LaIsMCLAGEnabledOnAgg (pAggEntry) == LA_TRUE) &&
             (pPortEntry->u1DLAGMasterAck == LA_TRUE) &&
             (pAggEntry->u1MCLAGRolePlayed == LA_MCLAG_SYSTEM_ROLE_SLAVE))
    {
        LaActiveMCLAGHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
    }
#endif

    /* if the link goes down, treat it as device failure and remove
     * it from the aggregator. Reason is; if the downed link is still in 
     * aggregation and there are standby links, the standby cannot take 
     * part in aggregation unless one of the existing link is removed. So
     * if we do not remove the link from aggregation the purpose of hot-
     * stanby links will be lost. Disadvantage of this approach is 
     * re-aggregation of all(??) links whenever a link goes down 
     * temporarily.
     */
    if (pPortEntry->AggSelected != LA_UNSELECTED)
    {
        if (pPortEntry->AggSelected == LA_SELECTED)
        {
            pPortEntry->pAggEntry->u1SelectedPortCount--;
        }
        pPortEntry->AggSelected = LA_UNSELECTED;
        LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_UNSELECTED);
        pPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
    }
    pPortEntry->LaRxmState = LA_RXM_STATE_PORT_DISABLED;

    pAggEntry->u1PortDisabledCounter++;
    if (pPortEntry->u1PortOperStatus != LA_OPER_UP)
    {
        pPortEntry->u4DownInBundleCount++;
        pPortEntry->u1PortDownReason = LA_IF_OPER_DOWN;
        LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC,
                         "Port %d aggregated to port-channel %d moved to Down state "
                         "due to Operational down indication\r\n",
                         pPortEntry->u2PortIndex,
                         pAggEntry->AggConfigEntry.u2ActorAdminKey);
        LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
        LaSyslogPrintAcivePorts (pPortEntry);
        LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                            LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);
    }

    if (pPortEntry->LaLacpMode == LA_MODE_LACP)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
        while (pTmpPortEntry != NULL)
        {
            u4Count1++;
            if ((pTmpPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED)
                ||
                (pTmpPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED)
                ||
                (pTmpPortEntry->LaRxmState == LA_RXM_STATE_LACP_DISABLED)
                || (pTmpPortEntry->LaLacpMode != LA_MODE_LACP))
            {
                u4Count2++;
                LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry,
                                       &pTmpPortEntry);
            }
            else
            {
                break;
            }
        }
        if ((u4Count1 == u4Count2) && (gu4UsePartnerAdminInfo == LA_TRUE)
            && (gu4PartnerConfig == LA_DISABLED))
        {
            pAggEntry->UsingPartnerAdminInfo = LA_TRUE;
        }
        LaLacpSelectionLogic (pPortEntry);
    }
    else if (pPortEntry->LaLacpMode == LA_MODE_MANUAL)
    {
        LaSelectManualAggGroup (pPortEntry->pAggEntry);
    }

}

/*****************************************************************************/
/* Function Name      : LaLacRxmLacpDisabled.                                */
/*                                                                           */
/* Description        : This function is called for a port that is individual*/
/*                      to update it LagIdMatched variable to TRUE when the  */
/*                      port is enabled.                                     */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacRxmLacpDisabled (tLaLacPortEntry * pPortEntry)
{
    tLaSelect           PrevAggSelected;
    tLaLacPortState     PrevPartnerState;
    tLaLacAggEntry     *pAggEntry = NULL;

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-RXM: Port %d LACP_DISABLED\n",
                 pPortEntry->u2PortIndex);

    pPortEntry->bPartnerActive = LA_FALSE;
    pPortEntry->InDefaulted = LA_TRUE;
    pAggEntry = pPortEntry->pAggEntry;

    if (pPortEntry->LaLacpMode != LA_MODE_MANUAL)
    {
        PrevAggSelected = pPortEntry->AggSelected;
        PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;
        pPortEntry->AggSelected = LA_UNSELECTED;
        LaLacRecordPartnerAdminIfInfo (pPortEntry);
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaAggregation = LA_FALSE;
        pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_FALSE;

        if ((pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED) &&
            (pAggEntry->u1PortDisabledCounter > 0))
        {
            pAggEntry->u1PortDisabledCounter--;
        }

        pPortEntry->LaRxmState = LA_RXM_STATE_LACP_DISABLED;
        LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected, PrevPartnerState);
    }
    else if (pPortEntry->LaLacpMode == LA_MODE_MANUAL)
    {
        /* The Aggregation mode is manual */
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization = LA_TRUE;
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting = LA_TRUE;
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaDistributing = LA_TRUE;

        if ((pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED) &&
            (pAggEntry->u1PortDisabledCounter > 0))
        {
            pAggEntry->u1PortDisabledCounter--;
        }

        pPortEntry->LaRxmState = LA_RXM_STATE_LACP_DISABLED;
        LaSelectManualAggGroup (pPortEntry->pAggEntry);
    }
}

/*****************************************************************************/
/* Function Name      : LaLacRxmExpired.                                     */
/*                                                                           */
/* Description        : This function is called when in the CURRENT state,   */
/*                      the port's Current While timer expires.              */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacRxmExpired (tLaLacPortEntry * pPortEntry)
{

    tLaLacAggEntry     *pAggEntry = NULL;

    pAggEntry = pPortEntry->pAggEntry;
    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-RXM: Port %d EXPIRED\n",
                 pPortEntry->u2PortIndex);

    pPortEntry->InDefaulted = LA_FALSE;

    pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization = LA_FALSE;
    pPortEntry->LaLacPartnerInfo.LaLacPortState.LaLacpTimeout =
        LA_SHORT_TIMEOUT;

    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_TRUE;

    if ((pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED) &&
        (pAggEntry->u1PortDisabledCounter > 0))
    {
        pAggEntry->u1PortDisabledCounter--;
    }

    pPortEntry->LaRxmState = LA_RXM_STATE_EXPIRED;

    LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_PARTNER_NO_SYNC);

    LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);

    LaLacInitializeCurrentWhileTimer (pPortEntry, LA_SHORT_TIMEOUT);

}

/*****************************************************************************/
/* Function Name      : LaLacRxmDefaulted.                                   */
/*                                                                           */
/* Description        : This function is called when in the EXPIRED state,   */
/*                      the port fails to receive a LACPDU before the Current*/
/*                      While timer expires.                                 */
/*                                                                           */
/* Input(s)           : pPortEntry.                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacRxmDefaulted (tLaLacPortEntry * pPortEntry)
{
    tLaSelect           PrevAggSelected;
    tLaLacPortState     PrevPartnerState;

    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    UINT4               u4Count1 = 0;
    UINT4               u4Count2 = 0;
    UINT1               au1MacAddr[LA_MAC_ADDRESS_SIZE];

    tLaLacAggConfigEntry *pLaLacAggConfigEntry = NULL;
    tNotifyProtoToApp   NotifyProtoToApp;
    tLaParamInfo       *pLaParamInfo = NULL;
    MEMSET (&NotifyProtoToApp, 0, sizeof (tNotifyProtoToApp));
    MEMSET (au1MacAddr, 0, LA_MAC_ADDRESS_SIZE);

    LA_TRC_ARG1 (CONTROL_PLANE_TRC, "LA-RXM: Port %d DEFAULTED\n",
                 pPortEntry->u2PortIndex);

    pAggEntry = pPortEntry->pAggEntry;
    pPortEntry->bPartnerActive = LA_FALSE;
    PrevAggSelected = pPortEntry->AggSelected;
    PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;
    if (MEMCMP (pPortEntry->LaLacPartnerInfo.LaSystem.SystemMacAddr,
                au1MacAddr, LA_MAC_ADDRESS_SIZE) == 0)
    {
        pPortEntry->u1PortDownReason = LA_NONE;
    }
    else if ((MEMCMP (pPortEntry->LaLacPartnerInfo.LaSystem.SystemMacAddr,
                      au1MacAddr,
                      LA_MAC_ADDRESS_SIZE) != 0) &&
             (pPortEntry->u1PortDownReason == LA_NONE))
    {
        pPortEntry->u4DownInBundleCount++;
        /* pPortEntry->u1PortDownReason = LA_NO_LACP_RCVD; */
        if (pPortEntry->pAggEntry != NULL)
        {
            LA_SYS_TRC_ARG2 (CONTROL_PLANE_TRC, "Port %d aggregated to "
                             "port-channel %d moved to Down state since "
                             "LACP BPDUs not received\r\n",
                             pPortEntry->u2PortIndex,
                             pPortEntry->pAggEntry->AggConfigEntry.
                             u2ActorAdminKey);
        }
        LaSyslogPrintAcivePorts (pPortEntry);
        LA_GET_SYS_TIME (&(pPortEntry->u4BundStChgTime));
        LaLinkStatusNotify (pPortEntry->u2PortIndex, LA_OPER_DOWN,
                            LA_ADMIN_DOWN, LA_TRAP_PORT_DOWN);
    }

    LaLacUpdateSelectedFromPartnerAdmin (pPortEntry);
    LaLacRecordPartnerAdminIfInfo (pPortEntry);

    pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization = LA_TRUE;

    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_FALSE;
    pPortEntry->LaRxmState = LA_RXM_STATE_DEFAULTED;
    pPortEntry->InDefaulted = LA_TRUE;

    LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
    while (pTmpPortEntry != NULL)
    {
        u4Count1++;
        if ((pTmpPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED)
            ||
            (pTmpPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED)
            ||
            (pTmpPortEntry->LaRxmState == LA_RXM_STATE_LACP_DISABLED)
            || (pTmpPortEntry->LaLacpMode != LA_MODE_LACP))
        {
            u4Count2++;
            LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
        }
        else
        {
            break;
        }
    }
    if ((u4Count1 == u4Count2) && (gu4UsePartnerAdminInfo == LA_TRUE))
    {
        pAggEntry->UsingPartnerAdminInfo = LA_TRUE;
    }
    if (gu4PartnerConfig == LA_ENABLED)
    {
        /* When Independent mode is enabled, the ports operate as individual
           ports and these ports will become visible to higher layer protocols, 
           when no partner in the switch */
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization =
            LA_FALSE;

        pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_TRUE;
        pPortEntry->LaRxmState = LA_RXM_STATE_DEFAULTED;
        pPortEntry->InDefaulted = LA_TRUE;

        pAggEntry->UsingPartnerAdminInfo = LA_FALSE;

        LaL2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                          pPortEntry->pAggEntry->u2AggIndex);

        LaHlCreatePhysicalPort (pPortEntry);
    }

    if ((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
        (pPortEntry->u1DLAGMasterAck == LA_TRUE))
    {
        /* Incase of Active D-LAG remove port from HL.
           Because following Call will move statemachine to
           LaMuxStateAttached which will set DLAG params as FALSE */
        LaActiveDLAGHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
    }
#ifdef ICCH_WANTED
    else if ((LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE) &&
             (pPortEntry->u1DLAGMasterAck == LA_TRUE))
    {
        LaActiveMCLAGHwControl (pPortEntry, LA_HW_EVENT_DISABLE_DISTRIBUTOR);
    }
#endif

    LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected, PrevPartnerState);

    if (gu4PartnerConfig == LA_ENABLED)
    {
        if (gu4LagAdminDownNoNeg == LA_TRUE)
        {
            if ((LaStopTimer (&(pPortEntry->PeriodicTmr))) == LA_FAILURE)
            {
                return;
            }
        }
    }

    /* when the port enters the defaulted state,indication is 
     * send to applications */
    pLaParamInfo = &(NotifyProtoToApp.LaNotify.LaInfo);
    NotifyProtoToApp.LaNotify.u1Action = LA_NOTIFY_DEFAULTED;
    pLaParamInfo->u2IfIndex = pPortEntry->u2PortIndex;
    pLaLacAggConfigEntry = &(pAggEntry->AggConfigEntry);
    pLaParamInfo->u2IfKey = pLaLacAggConfigEntry->u2ActorAdminKey;
    LaCfaNotifyProtoToApp (LA_NOTIFY, NotifyProtoToApp);
    /* Start the recovery timer */

    if ((gu4RecoveryTime != 0) &&
        (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_NONE))
    {
        if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
            (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP))
        {
            pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_DEFAULTED;
            pPortEntry->u4ErrorDetectCount++;
            LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateDetTime));
            LaErrorRecTrigNotify (pPortEntry, LA_REC_ERR_DEFAULTED,
                                  LA_INIT_VAL);
            if (LaStartTimer (&pPortEntry->RecoveryTmr) == LA_FAILURE)
            {
                LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                             "RXM: Recovery Timer Start FAILED for Port %d \r\n",
                             pPortEntry->u2PortIndex);

                pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
                return;
            }

            pPortEntry->u4ErrorDetectCount++;
        }
    }

}

/*****************************************************************************/
/* Function Name      : LaLacRxmUpdateMachines                               */
/*                                                                           */
/* Description        : This function is called whenever there is a          */
/*                      transistion of the state Machines.This function is   */
/*                      the core function to update all state machines in LA */
/*                      if needed under all conditions.                      */
/*                                                                           */
/* Input(s)           : pPortEntry-Pointer to the port for which the state   */
/*                      machines ot be updated.                              */
/*                      PrevAggSelected-Previous  State of the Port          */
/*                      (SELECTED/UNSELECTED/STANDBY)                        */
/*                      PrevPartnerState-Previous Partner Port State.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

INT4
LaLacRxmUpdateMachines (tLaLacPortEntry * pPortEntry,
                        tLaSelect PrevAggSelected,
                        tLaLacPortState PrevPartnerState)
{
    UINT1               u1State = 0;
    if (pPortEntry->AggSelected == LA_UNSELECTED)
    {
        if (pPortEntry->AggSelected != PrevAggSelected)
        {
            if (pPortEntry->pAggEntry == NULL)
            {
                return LA_FAILURE;
            }
            if (PrevAggSelected == LA_SELECTED)
            {
                pPortEntry->pAggEntry->u1SelectedPortCount--;
            }
            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "LA-RXM: Port %d Selected = UNSELECTED",
                         pPortEntry->u2PortIndex);

            LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_UNSELECTED);
        }
        /* Call the selection logic to take care of the changes */
        LaLacpSelectionLogic (pPortEntry);
    }
    else if (PrevPartnerState.LaSynchronization
             != pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization)

    {
        /* We are moving to some other state. Reset the same state count */
        pPortEntry->u4SameStateCount = 0;
        pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;

        if (pPortEntry->LaLacPartnerInfo.
            LaLacPortState.LaSynchronization == LA_TRUE)
        {
            LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_PARTNER_SYNC);
        }
        else
        {
            LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_PARTNER_NO_SYNC);
        }
    }
    else if (PrevPartnerState.LaCollecting
             != pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting)
    {
        /* We are moving to some other state. Reset the same state count */
        pPortEntry->u4SameStateCount = 0;
        pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;

        if (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting == LA_TRUE)
        {
            LaLacMuxControlMachine (pPortEntry,
                                    LA_MUX_EVENT_PARTNER_COLLECTING);
        }
        else
        {
            LaLacMuxControlMachine (pPortEntry,
                                    LA_MUX_EVENT_PARTNER_NO_COLLECTING);
        }
    }
    if (PrevPartnerState.LaLacpActivity !=
        pPortEntry->LaLacPartnerInfo.LaLacPortState.LaLacpActivity)
    {
        LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);
    }
    else if (PrevPartnerState.LaLacpTimeout !=
             pPortEntry->LaLacPartnerInfo.LaLacPortState.LaLacpTimeout)
    {
        LaLacPeriodicTxMachine (pPortEntry, LA_PTXM_EVENT_VARS_CHANGED);
    }
    else
    {
        /* We are stuck in same state. Increment the counter only if our state is
           not distributing */
        if ((pPortEntry->u4SameStateRecThreshold != 0) &&
            (pPortEntry->LaLacActorInfo.LaLacPortState.LaDistributing ==
             LA_FALSE))
        {
            pPortEntry->u4SameStateCount++;
            LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateDetTime));
            LaGetPortState (&(pPortEntry->LaLacActorAdminInfo.LaLacPortState),
                            &u1State);
            LaGetSameState (u1State, &(pPortEntry->u1RecState));
            if (pPortEntry->u4SameStateCount >=
                pPortEntry->u4SameStateRecThreshold)
            {
                if ((gu4RecoveryTime != 0) &&
                    (pPortEntry->u1RecoveryTmrReason == LA_REC_ERR_NONE))
                {
                    if ((pPortEntry->u1PortOperStatus == LA_OPER_UP) &&
                        (pPortEntry->pAggEntry->u1AggAdminStatus ==
                         LA_ADMIN_UP))
                    {
                        pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_SAME_STATE;
                        pPortEntry->u4ErrorDetectCount++;
                        LA_GET_SYS_TIME (&(pPortEntry->u4ErrStateDetTime));
                        LaErrorRecTrigNotify (pPortEntry, LA_REC_ERR_SAME_STATE,
                                              LA_INIT_VAL);
                        if (LaStartTimer (&pPortEntry->RecoveryTmr) ==
                            LA_FAILURE)
                        {
                            LA_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                         "LA: Recovery Timer Start FAILED for Port %d \r\n",
                                         pPortEntry->u2PortIndex);
                            pPortEntry->u1RecoveryTmrReason = LA_REC_ERR_NONE;
                            return LA_FAILURE;
                        }
                        pPortEntry->u4ErrorDetectCount++;
                    }
                }
            }
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacRxmCurrent.                                     */
/*                                                                           */
/* Description        : This function is called when in the CURRENT state,   */
/*                      the port receives a LACPDU before the Current While  */
/*                      Timer expires.                                       */
/*                                                                           */
/* Input(s)           : pPortEntry , pLaLacpduInfo.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
LaLacRxmCurrent (tLaLacPortEntry * pPortEntry, tLaLacpduInfo * pLaLacpduInfo)
{
    tLaSelect           PrevAggSelected;
    tLaLacPortState     PrevPartnerState;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;

    pAggEntry = pPortEntry->pAggEntry;

    if (pAggEntry->UsingPartnerAdminInfo == LA_TRUE)
    {
        LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);
        while (pTmpPortEntry != NULL)
        {
            if ((pTmpPortEntry->PortUsingPartnerAdminInfo == LA_TRUE) &&
                (pTmpPortEntry->AggSelected != LA_UNSELECTED))
            {
                if (pTmpPortEntry->AggSelected == LA_SELECTED)
                {
                    pAggEntry->u1SelectedPortCount--;
                }
                pTmpPortEntry->AggSelected = LA_UNSELECTED;
                LaLacMuxControlMachine (pTmpPortEntry, LA_MUX_EVENT_UNSELECTED);
                pTmpPortEntry->PortUsingPartnerAdminInfo = LA_FALSE;
            }
            LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry, &pTmpPortEntry);
        }

        pAggEntry->UsingPartnerAdminInfo = LA_FALSE;
    }

    pPortEntry->InDefaulted = LA_FALSE;

    /** Resetting the count when port recovers from Defaulted State **/
    pPortEntry->u4DefaultedStateRecTrgrdCount = 0;
    if ((pPortEntry->LaLacActorAdminInfo.LaLacPortState.LaLacpActivity ==
         LA_PASSIVE) && (pPortEntry->u1InPassiveDefState != LA_FALSE))
    {
        pPortEntry->u1InPassiveDefState = LA_FALSE;
    }

    PrevPartnerState = pPortEntry->LaLacPartnerInfo.LaLacPortState;
    PrevAggSelected = pPortEntry->AggSelected;

    pPortEntry->bPartnerActive = LA_TRUE;

    LaLacUpdateSelectedFromReceivedLacpdu (pLaLacpduInfo, pPortEntry);

    LaLacUpdateNtt (pPortEntry, pLaLacpduInfo);

    LaLacInitializeCurrentWhileTimer (pPortEntry,
                                      pPortEntry->LaLacActorInfo.
                                      LaLacPortState.LaLacpTimeout);

    LaLacRecordPartnerOperIfInfo (pLaLacpduInfo, pPortEntry);

    pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_FALSE;
    pPortEntry->LaRxmState = LA_RXM_STATE_CURRENT;

    if ((((LA_DLAG_SYSTEM_STATUS == LA_DLAG_ENABLED) &&
          (pAggEntry->u1DLAGRedundancy == LA_DLAG_REDUNDANCY_OFF) &&
          (pPortEntry->u1DLAGPeerReady == LA_TRUE)) &&
         /* 1. If Master Ack not received then Dont process */
         ((pPortEntry->u1DLAGMasterAck == LA_FALSE) ||
          /* 2. Master Acknolwdged and we sent sync.
           *    We expect Partner send collecting
           *    else don't process it */
          ((pPortEntry->u1DLAGMasterAck == LA_TRUE) &&
           (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting ==
            LA_FALSE)
           && (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaSynchronization ==
               LA_TRUE))))
        ||
        (((LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE)
          && (pPortEntry->u1DLAGPeerReady == LA_TRUE))
         && ((pPortEntry->u1DLAGMasterAck == LA_FALSE)
             || ((pPortEntry->u1DLAGMasterAck == LA_TRUE)
                 && (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting ==
                     LA_FALSE)
                 && (pPortEntry->LaLacPartnerInfo.LaLacPortState.
                     LaSynchronization == LA_TRUE)))))
    {
        /* Don't change the Statemachine. This is done for
           following purposes 
           1) When a port moves "Up,In Bundle" we wait for Master reply.
           Until that time Peer should be in "down state" so that traffic
           is not sent on that peer port. So we send "out of sync" to 
           peer so that he is not moving any further states.
           Becasue of "Out of Sync" peer state machine can change our
           states. This can lead to toggling of bundle state
           So don't process PDU's.

           2) Also Keeping us "Up,In Bundle" until that time helps in
           "Selecting Active and Standby Links in Master" 

           3) Wait until we receive collecting from Peer with only sync set after Masker
           Acknowlegded. Else we will be in Distributing and we receive
           partner as no collecting and we move to diffrent states. 

           Note: The above variables are Reset in LaMuxStateAttached */

        LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                     "\n LACP PDU received and No updation Port = %d \n",
                     pPortEntry->u2PortIndex);
    }
    else
    {
        /* Information needs to be synced with the Standby node. */
        LaRedSyncUpPartnerInfo (pPortEntry, pLaLacpduInfo);
        pPortEntry->u2SyncUp = LA_FALSE;
        LaLacRxmUpdateMachines (pPortEntry, PrevAggSelected, PrevPartnerState);
    }

    if (pPortEntry->NttFlag == LA_TRUE)
    {
        LaLacTxMachine (pPortEntry, LA_TXM_EVENT_NTT);
    }

}

/*****************************************************************************/
/* Function Name      : LaLacRxMachine                                       */
/*                                                                           */
/* Description        : This function is called when certain events occur.   */
/*                      When a LACPDU is received, this function is called   */
/*                      - with the receiving port's port entry and received  */
/*                      LACPDU info.                                         */
/*                      - with each of other ports' port entry and the       */
/*                      received LACPDU to check whether this LACPDU info    */
/*                      was earlier received by any other port.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All variables in the GLobal structure are            */
/*                      initialised.                                         */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer.      */
/*****************************************************************************/
INT4
LaLacRxMachine (tLaLacPortEntry * pPortEntry,
                tLaRxSmEvent LaEvent, tLaLacpduInfo * pLaLacpduInfo)
{
    tLaSelect           PrevAggSelected;
    switch (LaEvent)
    {

        case LA_RXM_EVENT_PORT_INIT:

            LaLacRxmPortInitialize (pPortEntry);

            /* Continue unconditional transition */

        case LA_RXM_EVENT_PORT_DISABLED:

            LaLacRxmPortDisabled (pPortEntry);

            break;

        case LA_RXM_EVENT_PORT_ENABLED:

            if (pPortEntry->LaRxmState != LA_RXM_STATE_PORT_DISABLED)
            {
                break;
            }

            if ((pPortEntry->LaLacpMode == LA_MODE_LACP) &&
                (pPortEntry->LaLacpEnabled == LA_LACP_ENABLED))
            {
                LaLacRxmExpired (pPortEntry);
            }
            else
            {
                LaLacRxmLacpDisabled (pPortEntry);
            }

            break;

        case LA_RXM_EVENT_CURRENT_WHILE_EXP:

            if (pPortEntry->LaRxmState == LA_RXM_STATE_CURRENT)
            {
                LaLacRxmExpired (pPortEntry);
            }
            else if (pPortEntry->LaRxmState == LA_RXM_STATE_EXPIRED)
            {
                LaLacRxmDefaulted (pPortEntry);
            }
            break;

        case LA_RXM_EVENT_PDU_RXD:

            if ((pPortEntry->LaRxmState == LA_RXM_STATE_LACP_DISABLED) ||
                (pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED))
            {
                break;
            }
            if (gu4PartnerConfig == LA_ENABLED)
            {
                /* When Independent mode is enabled, the ports operate as 
                 * individual ports and these ports will become visible to 
                 * higher layer protocols, when no partner in the switch.
                 * Now when the first LACP PDU's is received in the ports 
                 * configured as part of port-channel, we need to delete the 
                 * physical port created */

                if (gu4LagAdminDownNoNeg == LA_TRUE)
                {
                    if (pPortEntry->pAggEntry->u1AggAdminStatus == LA_ADMIN_UP)
                    {
                        if ((pPortEntry->LaLacActorInfo.LaLacPortState.
                             LaDefaulted != LA_FALSE)
                            &&
                            ((pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED)
                             || (pPortEntry->LaRxmState ==
                                 LA_RXM_STATE_EXPIRED)))
                        {
                            LaHlDeletePhysicalPort (pPortEntry);
                        }
                        else if (LA_MEMCMP
                                 (&(pPortEntry->LaLacPartnerAdminInfo),
                                  &(pPortEntry->LaLacPartnerInfo),
                                  sizeof (tLaLacIfInfo)) == 0)
                        {
                            LaHlDeletePhysicalPort (pPortEntry);
                        }
                    }
                }

                else
                {
                    if ((pPortEntry->LaLacActorInfo.LaLacPortState.
                         LaDefaulted != LA_FALSE)
                        && (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED))
                    {
                        LaHlDeletePhysicalPort (pPortEntry);
                    }

                    else if (LA_MEMCMP (&(pPortEntry->LaLacPartnerAdminInfo),
                                        &(pPortEntry->LaLacPartnerInfo),
                                        sizeof (tLaLacIfInfo)) == 0)
                    {
                        LaHlDeletePhysicalPort (pPortEntry);
                    }
                }
            }

            LaLacRxmCurrent (pPortEntry, pLaLacpduInfo);

            break;

        case LA_RXM_EVENT_ACTOR_ADMIN_CHANGED:

            PrevAggSelected = pPortEntry->AggSelected;
            pPortEntry->AggSelected = LA_UNSELECTED;
            /* Donot change partner sync bit since there is no significance for 
             * this bit when aggregated using partner admin values (ie, when the
             *  partner is not present)*/
            if (pPortEntry->InDefaulted == LA_FALSE)
            {
                pPortEntry->LaLacPartnerInfo.
                    LaLacPortState.LaSynchronization = LA_FALSE;
            }
            if ((pPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED) ||
                (pPortEntry->LaRxmState == LA_RXM_STATE_LACP_DISABLED) ||
                (pPortEntry->LaRxmState == LA_RXM_STATE_INITIALIZE) ||
                (pPortEntry->LaLacpMode == LA_MODE_MANUAL))
            {
                break;
            }

            if (pPortEntry->AggSelected == LA_UNSELECTED)
            {
                if (pPortEntry->AggSelected != PrevAggSelected)
                {
                    if (pPortEntry->pAggEntry == NULL)
                    {
                        return LA_FAILURE;
                    }
                    if (PrevAggSelected == LA_SELECTED)
                    {
                        pPortEntry->pAggEntry->u1SelectedPortCount--;
                    }
                    LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                                 "LA-RXM: Port %d Selected = UNSELECTED",
                                 pPortEntry->u2PortIndex);

                    LaLacMuxControlMachine (pPortEntry,
                                            LA_MUX_EVENT_UNSELECTED);
                }
                /* Call the selection logic to take care of the changes */
                LaLacpSelectionLogic (pPortEntry);
            }

            break;

        case LA_RXM_EVENT_PARTNER_ADMIN_CHANGED:

            if ((pPortEntry->LaRxmState == LA_RXM_STATE_INITIALIZE) ||
                (pPortEntry->LaRxmState == LA_RXM_STATE_LACP_DISABLED) ||
                (pPortEntry->LaRxmState == LA_RXM_STATE_DEFAULTED))
            {
                pPortEntry->AggSelected = LA_UNSELECTED;
                LaLacRecordPartnerAdminIfInfo (pPortEntry);
                pPortEntry->LaLacActorInfo.LaLacPortState.LaExpired = LA_FALSE;
            }
            break;

        case LA_RXM_EVENT_PORT_MOVED:

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "RXM: Port: %u:: Port's Partner has moved. Initializing Port...\n",
                         pPortEntry->u2PortIndex);
            LaLacRxmPortInitialize (pPortEntry);

            break;

        default:

            LA_TRC_ARG1 (CONTROL_PLANE_TRC,
                         "RXM: Port: %u :: Default Event passed\n",
                         pPortEntry->u2PortIndex);
            break;
    }

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaLacReceivePduInfo.                                 */
/*                                                                           */
/* Description        : This function is called by the Control Parser to     */
/*                      forward a LACPDU received by it. This function also  */
/*                      checks if the partner port has moved i.e. the port   */
/*                      has received a LACPDU from a partner which was a     */
/*                      partner to another port.                             */
/*                                                                           */
/* Input(s)           : pPortEntry, pu1LinearBuf.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure.                             */
/*                      LA_ERR_NULL_PTR - On receipt of a NULL pointer       */
/*****************************************************************************/
INT4
LaLacReceivePduInfo (tLaLacPortEntry * pPortEntry, UINT1 *pu1LinearBuf)
{
    tLaLacpduInfo       LaLacpduInfo;    /* All fields from the linear buffer
                                           will be copied into this structure
                                           which will be passed around. */
    tLaLacAggIfEntry   *pLaLacAggIfEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacAggEntry     *pAggEntry = NULL;
    tLaBoolean          bSamePort;

#ifdef ICCH_WANTED
    UINT2               u2ActualPortValue = 0;
    UINT2               u2MaskValue = 0;
#endif

    MEMSET (&LaLacpduInfo, 0, sizeof (tLaLacpduInfo));
    MEMSET (&bSamePort, 0, sizeof (tLaBoolean));

    /* Copy from the linear buffer to the Lacpdu Info structure */
    if (LaCopyIntoLacpduInfo (pu1LinearBuf, &LaLacpduInfo) == LA_SUCCESS)
    {

        /* Increment the number of LACPDUs Received */
        (pPortEntry->LaPortStats.u4LacpduRx)++;

        /* Increment Aggregator If statistics if port is attached */
        if (pPortEntry->LaAggAttached == LA_TRUE)
        {

            /* All the statistics are negatively incremented for the Aggregator,
             * because these frames have already been counted once, at the
             * port level */
            /* Get the AggIfEntry pointer */
            LA_GET_AGGIFENTRY (pPortEntry, pLaLacAggIfEntry);
            if (pLaLacAggIfEntry != NULL)
            {
                pLaLacAggIfEntry->i4AggInOctets += -(LA_SLOW_PROTOCOL_SIZE);
                (pLaLacAggIfEntry->i4AggInFrames)--;
                (pLaLacAggIfEntry->i4AggInMcastPkts)--;
            }
        }
    }
    else
    {
        /* Increment the Illegal Pdus Rx and Aggregator If statistics */
        (pPortEntry->LaPortStats.u4IllegalRx)++;

        /* All the statistics are negatively incremented for the Aggregator,
         * because these frames have already been counted once, at the
         * port level */
        LA_GET_AGGIFENTRY (pPortEntry, pLaLacAggIfEntry);
        if (pLaLacAggIfEntry != NULL)
        {
            (pLaLacAggIfEntry->i4AggInErrors)--;
        }
        return LA_FAILURE;
    }

    /* debug info - get the system time */
    LA_GET_SYS_TIME (&(pPortEntry->LaPortDebug.LaLastRxTime));

    /*To avoid same port-number received from MC-LAG master and slave
     * nodes , logical port number will be used to  fill LACPDU of MC-LAG
     * ports.When LACPDU is received from peer ,logical port number will be 
     * in the partner port number,before sending to RX machine decrypt the logical 
     * port number to actual port number*/
#ifdef ICCH_WANTED
    /*Get the mask value form HB modeule */
    u2MaskValue = HbApiGetPortMaskValue ();

    if ((LaGetMCLAGSystemStatus () == LA_TRUE) &&
        (LaIsMCLAGEnabledOnAgg (pPortEntry->pAggEntry) == LA_TRUE))

    {
        /*UnMasking logic
         *
         * 1.Do XOR operation to get back the shifted port number.
         * Mask value will be either 0x8000 or 0x4000

         *---------------------------------------*
         *                    |                  *
         *    Mask value      |   Port Index     *
         *    removed         |                  *
         * --------------------------------------*
         2nd  Byte               1st Byte    */

        u2ActualPortValue =
            LaLacpduInfo.LaLacPartnerInfo.u2IfIndex ^ u2MaskValue;
        LaLacpduInfo.LaLacPartnerInfo.u2IfIndex = u2ActualPortValue;
    }

#endif
    pAggEntry = pPortEntry->pAggEntry;

    if (pAggEntry != NULL)
    {
        if (pAggEntry->u1PortDisabledCounter > 0)
        {
            LaGetNextAggPortEntry (pAggEntry, NULL, &pTmpPortEntry);

            while (pTmpPortEntry != NULL)
            {
                /*Check if any of the ports in the port-channel is in PORT_DISABLED
                 * state*/
                if (pTmpPortEntry->LaRxmState == LA_RXM_STATE_PORT_DISABLED)
                {
                    /*If the port is in port_disabled state and if the same port's 
                     *partner System ID and partner port number is seen in a LACPDU
                     *received on a different Port (port_moved is set to TRUE),this 
                     *indicates that the physical connectivity has changed,
                     *and causes the state machine to enter the INITIALIZE state.*/

                    bSamePort =
                        LaLacIfSamePort (&(pTmpPortEntry->LaLacPartnerInfo),
                                         &(LaLacpduInfo.LaLacActorInfo));

                    if (bSamePort == LA_TRUE)
                    {
                        pTmpPortEntry->b1PortMoved = LA_TRUE;

                        LaLacRxMachine (pTmpPortEntry, LA_RXM_EVENT_PORT_MOVED,
                                        &LaLacpduInfo);

                    }
                }
                LaGetNextAggPortEntry (pAggEntry, pTmpPortEntry,
                                       &pTmpPortEntry);

            }
        }
    }

    /* Call the Rx Machine with LAC_RECEIVED event */
    LaLacRxMachine (pPortEntry, LA_RXM_EVENT_PDU_RXD, &LaLacpduInfo);

    if (pPortEntry->u2SyncUp == LA_TRUE)
    {
        /* Information needs to be synced with the Standby node. */
        LaRedSyncUpPartnerInfo (pPortEntry, &LaLacpduInfo);
        pPortEntry->u2SyncUp = LA_FALSE;
    }

    return LA_SUCCESS;
}

/*****************************************************************************
 * Function Name      : LaLacpSelectionLogic
 *
 * Description        : This function will be invoked by the RXM State
 *                      Machine and by the Configuration module when either
 *                      the Actor/Partner LACP Admin Values are Changed. If
 *                      the bDynAggSelect is set to true then the best
 *                      Aggregator is selected and the Port will get
 *                      attached to the selected Aggregator. If the Flag is
 *                      not set the Port get attached to the configured
 *                      aggregator
 *                                                                      
 * Input(s)           : pPortEntry
 *
 * Output(s)          : None
 *
 * Return Value(s)    : LA_SUCCESS - On success
 *                      LA_FAILURE - On failure
 *                      LA_ERR_NULL_PTR - On receipt of a NULL pointer
 *****************************************************************************/
INT4
LaLacpSelectionLogic (tLaLacPortEntry * pPortEntry)
{
    /* If the pDynAggSelect field is set to true then LaLacDynamicAggregator
     * will be invoked to select the appropriate aggregator. */
    if (pPortEntry->bDynAggSelect == LA_TRUE)
    {
        /* After the Best Aggregator is selected the port will be removed from
         * previous aggregator and added to the selected aggregator */
        if (LaLacDynamicAggregator (pPortEntry) != LA_SUCCESS)
        {
            return LA_FAILURE;
        }
    }
    else
    {
        /* If the pDynAggSelect is not set then the port will be either selected
         * or unselected from the configured aggregator based on the outcome of 
         * the LACP exchanges. */
        LaSelectLacpAggGroup (pPortEntry->pAggEntry);
    }

    return LA_SUCCESS;
}

/*****************************************************************************
 * Function Name      : LaLacDynamicAggregator
 *
 * Description        : This function will be called when Dynamic selection
 *                      logic is needed. This function selects the best
 *                      aggregation for the Port and remove the port from
 *                      current aggregator and adds to the new Aggregator. If
 *                      the selected aggregator is default aggregator, all the
 *                      other dynamic ports will be checked for 
 *                      realignment
 *
 * Input(s)           : pPortEntry
 *
 * Output(s)          : None
 *
 * Return Value(s)    : LA_SUCCESS
 *****************************************************************************/
PRIVATE INT4
LaLacDynamicAggregator (tLaLacPortEntry * pPortEntry)
{
    tLaLacPortEntry    *pCheckPortEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;

    if (pPortEntry == NULL)
    {
        return LA_FAILURE;
    }

    LaLacFindAndAttachNewAgg (pPortEntry);
    LaSelectLacpAggGroup (pPortEntry->pAggEntry);

    /* If default Agg is chosen for pPortEntry, then check whether the other
     * ports in LaDynReallocPortList also needs realignment */
    if (pPortEntry->pAggEntry == pPortEntry->pDefAggEntry)
    {
        if (pPortEntry->bPartnerActive == LA_TRUE)
        {
            /* Partner is active and default aggregation is chosen.
             * Check whether other dynamic ports also can join this
             * aggregation */
            UTL_SLL_OFFSET_SCAN (&(gLaGlobalInfo.LaDynReallocPortList),
                                 pCheckPortEntry, pTmpPortEntry,
                                 tLaLacPortEntry *)
            {
                if ((pCheckPortEntry != pPortEntry) &&
                    (pCheckPortEntry->bPartnerActive == LA_TRUE) &&
                    (LA_IS_LAGG_ID_EQUAL (pCheckPortEntry, pPortEntry) ==
                     LA_TRUE))
                {
                    /* This port should also move to the new aggregator */
                    LaLacAttachPortToNewAgg (pCheckPortEntry,
                                             pPortEntry->pAggEntry);
                    LaSelectLacpAggGroup (pCheckPortEntry->pAggEntry);
                }
            }
        }
        else
        {
            /* Default Port's partner is not active => Move all remaining
             * member ports to next best aggregator */
            /* First port in the list will always be the default port */
            pCheckPortEntry = (tLaLacPortEntry *)
                LA_SLL_NEXT (LA_GET_AGG_CONFIG_PORT_SLL (pPortEntry->pAggEntry),
                             &pPortEntry->NextPortEntry);

            while (pCheckPortEntry != NULL)
            {
                LaLacFindAndAttachNewAgg (pCheckPortEntry);
                LaSelectLacpAggGroup (pCheckPortEntry->pAggEntry);

                pCheckPortEntry = (tLaLacPortEntry *)
                    LA_SLL_NEXT
                    (LA_GET_AGG_CONFIG_PORT_SLL (pPortEntry->pAggEntry),
                     &pPortEntry->NextPortEntry);
            }
        }
    }

    return LA_SUCCESS;
}

/*****************************************************************************
 * Function Name      : LaLacFindAndAttachNewAgg
 *
 * Description        : This function selects the best aggregation for the
 *                      Port and remove the port from current aggregator and
 *                      adds to the new Aggregator
 *
 * Input(s)           : pPortEntry
 *
 * Output(s)          : None
 *
 * Return Value(s)    : LA_SUCCESS
 *                      LA_FAILURE
 *****************************************************************************/
PRIVATE INT4
LaLacFindAndAttachNewAgg (tLaLacPortEntry * pPortEntry)
{
    tLaLacAggEntry     *pNewAggEntry = NULL;

    /* Find the appropriate aggregator for the port */
    LaLacFindBestAggregator (pPortEntry, &pNewAggEntry);

    /* Remove the port from old agggreagtor, if present, and 
     * attach to the new best aggregator */
    LaLacAttachPortToNewAgg (pPortEntry, pNewAggEntry);

    return LA_SUCCESS;
}

/*****************************************************************************
 * Function Name      : LaLacAttachPortToNewAgg
 *
 * Description        : This function removes the Port from the old aggregator
 *                      if present  and adds it to the given new aggregator
 *
 * Input(s)           : pPortEntry - Pointer to PortEntry
 *                      pNewAggEntry - Pointer to new Agg Entry
 *
 * Output(s)          : None
 *
 * Return Value(s)    : LA_SUCCESS
 *                      LA_FAILURE
 *****************************************************************************/
PRIVATE INT4
LaLacAttachPortToNewAgg (tLaLacPortEntry * pPortEntry,
                         tLaLacAggEntry * pNewAggEntry)
{
    if (pNewAggEntry == pPortEntry->pAggEntry)
    {
        /* Same aggregator selected again, do nothing */
        return LA_SUCCESS;
    }

    if (pPortEntry->pAggEntry != NULL)
    {
        if (pPortEntry->AggSelected == LA_SELECTED)
        {
            pPortEntry->pAggEntry->u1SelectedPortCount--;
            pPortEntry->AggSelected = LA_UNSELECTED;
            LaLacMuxControlMachine (pPortEntry, LA_MUX_EVENT_UNSELECTED);
        }
        /* Remove port from current aggregator */
        /* Indicate VLAN to remove port properties from Hardware */
        L2IwfRemovePortFromPortChannel (pPortEntry->u2PortIndex,
                                        pPortEntry->pAggEntry->u2AggIndex);

#ifdef NPAPI_WANTED
        if (LA_IS_NP_PROGRAMMING_ALLOWED () == LA_TRUE)
        {

            /* Remove port from the configured list in the Hardware */
            LaFsLaHwRemovePortFromConfAggGroup (pPortEntry->pAggEntry->
                                                u2AggIndex,
                                                pPortEntry->u2PortIndex);
        }
#endif

        /* Remove port from port-channel */
        LaRemovePortFromAggregator (pPortEntry);

        /* Indicate the Physical port to Higher layer by invoking
         * LaHlCreatePhysicalPort */
        LaHlCreatePhysicalPort (pPortEntry);
    }

    /* Add the Port to the selected aggregator  */
    LaAddDynPortToAggregator (pNewAggEntry, pPortEntry);

    /* Delete Physcial Port from Bridge */
    LaHlDeletePhysicalPort (pPortEntry);
#ifdef NPAPI_WANTED
    LaFsLaHwAddPortToConfAggGroup (pPortEntry->pAggEntry->u2AggIndex,
                                   pPortEntry->u2PortIndex);
#endif

    return LA_SUCCESS;
}

/*****************************************************************************
 * Function Name      : LaLacFindBestAggregator
 * 
 * Description        : This function selects the best aggregator for the
 *                      Port based on the Actor and Partner's System Id,
 *                      Priority and Port Admin Key
 *                      
 * Input(s)           : pLaLacPortEntry - Pointer to Port entry for which Agg
 *                                        needs to be selected
 *
 * Output(s)          : ppSelectAggConfigEntry - Pointer to the best aggregator
 *
 * Return Value(s)    : LA_SUCCESS - On success
 *                      LA_FAILURE - On failure
 *****************************************************************************/
PRIVATE INT4
LaLacFindBestAggregator (tLaLacPortEntry * pLaLacPortEntry,
                         tLaLacAggEntry ** ppSelectAggConfigEntry)
{
    tLaLacPortEntry    *pBestPortEntry = NULL;
    tLaLacPortEntry    *pCheckPortEntry = NULL;
    tLaLacPortEntry    *pTmpPortEntry = NULL;
    tLaLacIfInfo       *pBestActorInfo = NULL;
    tLaLacIfInfo       *pCheckActorInfo = NULL;

    /* When there is no partner alive, default aggregator should be 
     * selected */
    if (pLaLacPortEntry->bPartnerActive == LA_FALSE)
    {
        *ppSelectAggConfigEntry = pLaLacPortEntry->pDefAggEntry;
        return LA_SUCCESS;
    }

    /* Initialize */
    pBestPortEntry = pLaLacPortEntry;
    pBestActorInfo = &(pBestPortEntry->LaLacActorInfo);

    /* Scan the Port List to find the port that is aggregatable and has the 
     * least Port index and Port priority, and choose its default aggregator. */
    UTL_SLL_OFFSET_SCAN (&(gLaGlobalInfo.LaDynReallocPortList),
                         pCheckPortEntry, pTmpPortEntry, tLaLacPortEntry *)
    {
        /* Check if there is a valid Partner for the Port */
        if ((pCheckPortEntry == pLaLacPortEntry) ||
            (pCheckPortEntry->bPartnerActive == LA_FALSE))
        {
            continue;
        }

        pCheckActorInfo = &(pCheckPortEntry->LaLacActorInfo);

        /* Check whether Actor system ids, Actor key, Partner system ids and
         * Partner keys of the two ports are equal */
        if ((LA_IS_LAGG_ID_EQUAL (pCheckPortEntry, pBestPortEntry) ==
             LA_TRUE) &&
            /* Check if the port under consideration has higher port ID */
            (LA_CHECK_PORT_HAS_HIGHER_PORT_ID (pCheckActorInfo, pBestActorInfo)
             == LA_TRUE))
        {
            pBestPortEntry = pCheckPortEntry;
            pBestActorInfo = &(pBestPortEntry->LaLacActorInfo);
        }
    }
    /* End of SLL_SCAN */

    *ppSelectAggConfigEntry = pBestPortEntry->pDefAggEntry;
    return LA_SUCCESS;
}

/* end of file */
