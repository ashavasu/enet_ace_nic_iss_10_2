/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsladb.h,v 1.34 2017/10/16 09:40:59 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSLADB_H
#define _FSLADB_H

UINT1 FsLaPortChannelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsLaPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsLaHwFailTrapObjectsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsLaDLAGTrapObjectsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsLaDLAGRemotePortChannelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsLaDLAGRemotePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsLaMCLAGRemotePortChannelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsLaMCLAGRemotePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsla [] ={1,3,6,1,4,1,2076,63};
tSNMP_OID_TYPE fslaOID = {8, fsla};


UINT4 FsLaSystemControl [ ] ={1,3,6,1,4,1,2076,63,1,1};
UINT4 FsLaStatus [ ] ={1,3,6,1,4,1,2076,63,1,2};
UINT4 FsLaTraceOption [ ] ={1,3,6,1,4,1,2076,63,1,3};
UINT4 FsLaMaxPortsPerPortChannel [ ] ={1,3,6,1,4,1,2076,63,1,4};
UINT4 FsLaMaxPortChannels [ ] ={1,3,6,1,4,1,2076,63,1,5};
UINT4 FsLaOperStatus [ ] ={1,3,6,1,4,1,2076,63,1,6};
UINT4 FsLaActorSystemID [ ] ={1,3,6,1,4,1,2076,63,1,7};
UINT4 FsLaNoPartnerIndep [ ] ={1,3,6,1,4,1,2076,63,1,8};
UINT4 FsLaDLAGSystemStatus [ ] ={1,3,6,1,4,1,2076,63,1,9};
UINT4 FsLaDLAGSystemID [ ] ={1,3,6,1,4,1,2076,63,1,10};
UINT4 FsLaDLAGSystemPriority [ ] ={1,3,6,1,4,1,2076,63,1,11};
UINT4 FsLaDLAGPeriodicSyncTime [ ] ={1,3,6,1,4,1,2076,63,1,12};
UINT4 FsLaDLAGRolePlayed [ ] ={1,3,6,1,4,1,2076,63,1,13};
UINT4 FsLaDLAGDistributingPortIndex [ ] ={1,3,6,1,4,1,2076,63,1,14};
UINT4 FsLaDLAGDistributingPortList [ ] ={1,3,6,1,4,1,2076,63,1,15};
UINT4 FsLaMCLAGSystemStatus [ ] ={1,3,6,1,4,1,2076,63,1,16};
UINT4 FsLaMCLAGSystemID [ ] ={1,3,6,1,4,1,2076,63,1,17};
UINT4 FsLaMCLAGSystemPriority [ ] ={1,3,6,1,4,1,2076,63,1,18};
UINT4 FsLaMCLAGPeriodicSyncTime [ ] ={1,3,6,1,4,1,2076,63,1,19};
UINT4 FsLaRecTmrDuration [ ] ={1,3,6,1,4,1,2076,63,1,20};
UINT4 FsLaRecThreshold [ ] ={1,3,6,1,4,1,2076,63,1,21};
UINT4 FsLaTotalErrRecCount [ ] ={1,3,6,1,4,1,2076,63,1,22};
UINT4 FsLaDefaultedStateThreshold [ ] ={1,3,6,1,4,1,2076,63,1,23};
UINT4 FsLaHardwareFailureRecThreshold [ ] ={1,3,6,1,4,1,2076,63,1,24};
UINT4 FsLaSameStateRecThreshold [ ] ={1,3,6,1,4,1,2076,63,1,25};
UINT4 FsLaRecThresholdExceedAction [ ] ={1,3,6,1,4,1,2076,63,1,26};
UINT4 FsLaMCLAGClearCounters [ ] ={1,3,6,1,4,1,2076,63,1,27};
UINT4 FsLaClearStatistics [ ] ={1,3,6,1,4,1,2076,63,1,28};
UINT4 FsLaMCLAGSystemControl [ ] ={1,3,6,1,4,1,2076,63,1,29};
UINT4 FsLaPortChannelIfIndex [ ] ={1,3,6,1,4,1,2076,63,2,1,1,1};
UINT4 FsLaPortChannelGroup [ ] ={1,3,6,1,4,1,2076,63,2,1,1,2};
UINT4 FsLaPortChannelAdminMacAddress [ ] ={1,3,6,1,4,1,2076,63,2,1,1,3};
UINT4 FsLaPortChannelMacSelection [ ] ={1,3,6,1,4,1,2076,63,2,1,1,4};
UINT4 FsLaPortChannelMode [ ] ={1,3,6,1,4,1,2076,63,2,1,1,5};
UINT4 FsLaPortChannelPortCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,6};
UINT4 FsLaPortChannelActivePortCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,7};
UINT4 FsLaPortChannelSelectionPolicy [ ] ={1,3,6,1,4,1,2076,63,2,1,1,8};
UINT4 FsLaPortChannelDefaultPortIndex [ ] ={1,3,6,1,4,1,2076,63,2,1,1,9};
UINT4 FsLaPortChannelMaxPorts [ ] ={1,3,6,1,4,1,2076,63,2,1,1,10};
UINT4 FsLaPortChannelSelectionPolicyBitList [ ] ={1,3,6,1,4,1,2076,63,2,1,1,11};
UINT4 FsLaPortChannelDLAGDistributingPortIndex [ ] ={1,3,6,1,4,1,2076,63,2,1,1,12};
UINT4 FsLaPortChannelDLAGSystemID [ ] ={1,3,6,1,4,1,2076,63,2,1,1,13};
UINT4 FsLaPortChannelDLAGSystemPriority [ ] ={1,3,6,1,4,1,2076,63,2,1,1,14};
UINT4 FsLaPortChannelDLAGPeriodicSyncTime [ ] ={1,3,6,1,4,1,2076,63,2,1,1,15};
UINT4 FsLaPortChannelDLAGMSSelectionWaitTime [ ] ={1,3,6,1,4,1,2076,63,2,1,1,16};
UINT4 FsLaPortChannelDLAGRolePlayed [ ] ={1,3,6,1,4,1,2076,63,2,1,1,17};
UINT4 FsLaPortChannelDLAGStatus [ ] ={1,3,6,1,4,1,2076,63,2,1,1,18};
UINT4 FsLaPortChannelDLAGRedundancy [ ] ={1,3,6,1,4,1,2076,63,2,1,1,19};
UINT4 FsLaPortChannelDLAGMaxKeepAliveCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,20};
UINT4 FsLaPortChannelDLAGPeriodicSyncPduTxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,21};
UINT4 FsLaPortChannelDLAGPeriodicSyncPduRxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,22};
UINT4 FsLaPortChannelDLAGEventUpdatePduTxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,23};
UINT4 FsLaPortChannelDLAGEventUpdatePduRxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,24};
UINT4 FsLaPortChannelDLAGElectedAsMasterCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,25};
UINT4 FsLaPortChannelDLAGElectedAsSlaveCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,26};
UINT4 FsLaPortChannelTrapTxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,27};
UINT4 FsLaPortChannelDLAGDistributingPortList [ ] ={1,3,6,1,4,1,2076,63,2,1,1,28};
UINT4 FsLaPortChannelMCLAGStatus [ ] ={1,3,6,1,4,1,2076,63,2,1,1,29};
UINT4 FsLaPortChannelMCLAGSystemID [ ] ={1,3,6,1,4,1,2076,63,2,1,1,30};
UINT4 FsLaPortChannelMCLAGSystemPriority [ ] ={1,3,6,1,4,1,2076,63,2,1,1,31};
UINT4 FsLaPortChannelMCLAGRolePlayed [ ] ={1,3,6,1,4,1,2076,63,2,1,1,32};
UINT4 FsLaPortChannelMCLAGMaxKeepAliveCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,33};
UINT4 FsLaPortChannelMCLAGPeriodicSyncPduTxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,34};
UINT4 FsLaPortChannelMCLAGPeriodicSyncPduRxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,35};
UINT4 FsLaPortChannelMCLAGEventUpdatePduTxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,36};
UINT4 FsLaPortChannelMCLAGEventUpdatePduRxCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,37};
UINT4 FsLaPortChannelClearStatistics [ ] ={1,3,6,1,4,1,2076,63,2,1,1,38};
UINT4 FsLaPortChannelUpCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,39};
UINT4 FsLaPortChannelDownCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,40};
UINT4 FsLaPortChannelHotStandByPortsCount [ ] ={1,3,6,1,4,1,2076,63,2,1,1,41};
UINT4 FsLaPortChannelOperChgTimeStamp [ ] ={1,3,6,1,4,1,2076,63,2,1,1,42};
UINT4 FsLaPortChannelDownReason [ ] ={1,3,6,1,4,1,2076,63,2,1,1,43};
UINT4 FsLaPortIndex [ ] ={1,3,6,1,4,1,2076,63,3,1,1,1};
UINT4 FsLaPortMode [ ] ={1,3,6,1,4,1,2076,63,3,1,1,2};
UINT4 FsLaPortBundleState [ ] ={1,3,6,1,4,1,2076,63,3,1,1,3};
UINT4 FsLaPortActorResetAdminState [ ] ={1,3,6,1,4,1,2076,63,3,1,1,4};
UINT4 FsLaPortAggregateWaitTime [ ] ={1,3,6,1,4,1,2076,63,3,1,1,5};
UINT4 FsLaPortPartnerResetAdminState [ ] ={1,3,6,1,4,1,2076,63,3,1,1,6};
UINT4 FsLaPortActorAdminPort [ ] ={1,3,6,1,4,1,2076,63,3,1,1,7};
UINT4 FsLaPortRestoreMtu [ ] ={1,3,6,1,4,1,2076,63,3,1,1,8};
UINT4 FsLaPortSelectAggregator [ ] ={1,3,6,1,4,1,2076,63,3,1,1,9};
UINT4 FsLaPortErrStateDetCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,10};
UINT4 FsLaPortErrStateRecCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,11};
UINT4 FsLaPortDefaultedStateThreshold [ ] ={1,3,6,1,4,1,2076,63,3,1,1,12};
UINT4 FsLaPortHardwareFailureRecThreshold [ ] ={1,3,6,1,4,1,2076,63,3,1,1,13};
UINT4 FsLaPortSameStateRecThreshold [ ] ={1,3,6,1,4,1,2076,63,3,1,1,14};
UINT4 FsLaPortUpInBundleCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,15};
UINT4 FsLaPortDownInBundleCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,16};
UINT4 FsLaPortBundStChgTimeStamp [ ] ={1,3,6,1,4,1,2076,63,3,1,1,17};
UINT4 FsLaPortDownInBundleReason [ ] ={1,3,6,1,4,1,2076,63,3,1,1,18};
UINT4 FsLaPortErrStateDetTimeStamp [ ] ={1,3,6,1,4,1,2076,63,3,1,1,19};
UINT4 FsLaPortErrStateRecTimeStamp [ ] ={1,3,6,1,4,1,2076,63,3,1,1,20};
UINT4 FsLaPortRecTrigReason [ ] ={1,3,6,1,4,1,2076,63,3,1,1,21};
UINT4 FsLaPortDefStateRecCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,22};
UINT4 FsLaPortSameStateRecCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,23};
UINT4 FsLaPortHwFailRecCount [ ] ={1,3,6,1,4,1,2076,63,3,1,1,24};
UINT4 FsLaPortDefStateRecTimeStamp [ ] ={1,3,6,1,4,1,2076,63,3,1,1,25};
UINT4 FsLaPortSameStateRecTimeStamp [ ] ={1,3,6,1,4,1,2076,63,3,1,1,26};
UINT4 FsLaPortHwFailRecTimeStamp [ ] ={1,3,6,1,4,1,2076,63,3,1,1,27};
UINT4 FsLaPortRecState [ ] ={1,3,6,1,4,1,2076,63,3,1,1,28};
UINT4 FsLaTrapPortChannelIndex [ ] ={1,3,6,1,4,1,2076,63,4,1,1,1};
UINT4 FsLaTrapPortIndex [ ] ={1,3,6,1,4,1,2076,63,4,1,1,2};
UINT4 FsLaHwFailTrapType [ ] ={1,3,6,1,4,1,2076,63,4,1,1,3};
UINT4 FsLaDLAGTrapPortChannelIndex [ ] ={1,3,6,1,4,1,2076,63,4,2,1,1};
UINT4 FsLaDLAGTrapType [ ] ={1,3,6,1,4,1,2076,63,4,2,1,2};
UINT4 FsLaDLAGRemotePortChannelSystemID [ ] ={1,3,6,1,4,1,2076,63,6,1,1,1};
UINT4 FsLaDLAGRemotePortChannelSystemPriority [ ] ={1,3,6,1,4,1,2076,63,6,1,1,2};
UINT4 FsLaDLAGRemotePortChannelRolePlayed [ ] ={1,3,6,1,4,1,2076,63,6,1,1,3};
UINT4 FsLaDLAGRemotePortChannelKeepAliveCount [ ] ={1,3,6,1,4,1,2076,63,6,1,1,4};
UINT4 FsLaDLAGRemotePortChannelSpeed [ ] ={1,3,6,1,4,1,2076,63,6,1,1,5};
UINT4 FsLaDLAGRemotePortChannelHighSpeed [ ] ={1,3,6,1,4,1,2076,63,6,1,1,6};
UINT4 FsLaDLAGRemotePortChannelMtu [ ] ={1,3,6,1,4,1,2076,63,6,1,1,7};
UINT4 FsLaDLAGRemotePortIndex [ ] ={1,3,6,1,4,1,2076,63,7,1,1,1};
UINT4 FsLaDLAGRemotePortBundleState [ ] ={1,3,6,1,4,1,2076,63,7,1,1,2};
UINT4 FsLaDLAGRemotePortSyncStatus [ ] ={1,3,6,1,4,1,2076,63,7,1,1,3};
UINT4 FsLaDLAGRemotePortPriority [ ] ={1,3,6,1,4,1,2076,63,7,1,1,4};
UINT4 FsLaMCLAGRemotePortChannelSystemID [ ] ={1,3,6,1,4,1,2076,63,8,1,1,1};
UINT4 FsLaMCLAGRemotePortChannelSystemPriority [ ] ={1,3,6,1,4,1,2076,63,8,1,1,2};
UINT4 FsLaMCLAGRemotePortChannelRolePlayed [ ] ={1,3,6,1,4,1,2076,63,8,1,1,3};
UINT4 FsLaMCLAGRemotePortChannelKeepAliveCount [ ] ={1,3,6,1,4,1,2076,63,8,1,1,4};
UINT4 FsLaMCLAGRemotePortChannelSpeed [ ] ={1,3,6,1,4,1,2076,63,8,1,1,5};
UINT4 FsLaMCLAGRemotePortChannelHighSpeed [ ] ={1,3,6,1,4,1,2076,63,8,1,1,6};
UINT4 FsLaMCLAGRemotePortChannelMtu [ ] ={1,3,6,1,4,1,2076,63,8,1,1,7};
UINT4 FsLaMCLAGRemotePortIndex [ ] ={1,3,6,1,4,1,2076,63,9,1,1,1};
UINT4 FsLaMCLAGRemotePortSlotIndex [ ] ={1,3,6,1,4,1,2076,63,9,1,1,2};
UINT4 FsLaMCLAGRemotePortBundleState [ ] ={1,3,6,1,4,1,2076,63,9,1,1,3};
UINT4 FsLaMCLAGRemotePortSyncStatus [ ] ={1,3,6,1,4,1,2076,63,9,1,1,4};
UINT4 FsLaMCLAGRemotePortPriority [ ] ={1,3,6,1,4,1,2076,63,9,1,1,5};



tMbDbEntry fslaMibEntry[]= {

{{10,FsLaSystemControl}, NULL, FsLaSystemControlGet, FsLaSystemControlSet, FsLaSystemControlTest, FsLaSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLaStatus}, NULL, FsLaStatusGet, FsLaStatusSet, FsLaStatusTest, FsLaStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLaTraceOption}, NULL, FsLaTraceOptionGet, FsLaTraceOptionSet, FsLaTraceOptionTest, FsLaTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsLaMaxPortsPerPortChannel}, NULL, FsLaMaxPortsPerPortChannelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLaMaxPortChannels}, NULL, FsLaMaxPortChannelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLaOperStatus}, NULL, FsLaOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLaActorSystemID}, NULL, FsLaActorSystemIDGet, FsLaActorSystemIDSet, FsLaActorSystemIDTest, FsLaActorSystemIDDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLaNoPartnerIndep}, NULL, FsLaNoPartnerIndepGet, FsLaNoPartnerIndepSet, FsLaNoPartnerIndepTest, FsLaNoPartnerIndepDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaDLAGSystemStatus}, NULL, FsLaDLAGSystemStatusGet, FsLaDLAGSystemStatusSet, FsLaDLAGSystemStatusTest, FsLaDLAGSystemStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaDLAGSystemID}, NULL, FsLaDLAGSystemIDGet, FsLaDLAGSystemIDSet, FsLaDLAGSystemIDTest, FsLaDLAGSystemIDDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLaDLAGSystemPriority}, NULL, FsLaDLAGSystemPriorityGet, FsLaDLAGSystemPrioritySet, FsLaDLAGSystemPriorityTest, FsLaDLAGSystemPriorityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32768"},

{{10,FsLaDLAGPeriodicSyncTime}, NULL, FsLaDLAGPeriodicSyncTimeGet, FsLaDLAGPeriodicSyncTimeSet, FsLaDLAGPeriodicSyncTimeTest, FsLaDLAGPeriodicSyncTimeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaDLAGRolePlayed}, NULL, FsLaDLAGRolePlayedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLaDLAGDistributingPortIndex}, NULL, FsLaDLAGDistributingPortIndexGet, FsLaDLAGDistributingPortIndexSet, FsLaDLAGDistributingPortIndexTest, FsLaDLAGDistributingPortIndexDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLaDLAGDistributingPortList}, NULL, FsLaDLAGDistributingPortListGet, FsLaDLAGDistributingPortListSet, FsLaDLAGDistributingPortListTest, FsLaDLAGDistributingPortListDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsLaMCLAGSystemStatus}, NULL, FsLaMCLAGSystemStatusGet, FsLaMCLAGSystemStatusSet, FsLaMCLAGSystemStatusTest, FsLaMCLAGSystemStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaMCLAGSystemID}, NULL, FsLaMCLAGSystemIDGet, FsLaMCLAGSystemIDSet, FsLaMCLAGSystemIDTest, FsLaMCLAGSystemIDDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, "000000000000"},

{{10,FsLaMCLAGSystemPriority}, NULL, FsLaMCLAGSystemPriorityGet, FsLaMCLAGSystemPrioritySet, FsLaMCLAGSystemPriorityTest, FsLaMCLAGSystemPriorityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "32768"},

{{10,FsLaMCLAGPeriodicSyncTime}, NULL, FsLaMCLAGPeriodicSyncTimeGet, FsLaMCLAGPeriodicSyncTimeSet, FsLaMCLAGPeriodicSyncTimeTest, FsLaMCLAGPeriodicSyncTimeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaRecTmrDuration}, NULL, FsLaRecTmrDurationGet, FsLaRecTmrDurationSet, FsLaRecTmrDurationTest, FsLaRecTmrDurationDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "90"},

{{10,FsLaRecThreshold}, NULL, FsLaRecThresholdGet, FsLaRecThresholdSet, FsLaRecThresholdTest, FsLaRecThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FsLaTotalErrRecCount}, NULL, FsLaTotalErrRecCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsLaDefaultedStateThreshold}, NULL, FsLaDefaultedStateThresholdGet, FsLaDefaultedStateThresholdSet, FsLaDefaultedStateThresholdTest, FsLaDefaultedStateThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FsLaHardwareFailureRecThreshold}, NULL, FsLaHardwareFailureRecThresholdGet, FsLaHardwareFailureRecThresholdSet, FsLaHardwareFailureRecThresholdTest, FsLaHardwareFailureRecThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FsLaSameStateRecThreshold}, NULL, FsLaSameStateRecThresholdGet, FsLaSameStateRecThresholdSet, FsLaSameStateRecThresholdTest, FsLaSameStateRecThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FsLaRecThresholdExceedAction}, NULL, FsLaRecThresholdExceedActionGet, FsLaRecThresholdExceedActionSet, FsLaRecThresholdExceedActionTest, FsLaRecThresholdExceedActionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsLaMCLAGClearCounters}, NULL, FsLaMCLAGClearCountersGet, FsLaMCLAGClearCountersSet, FsLaMCLAGClearCountersTest, FsLaMCLAGClearCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaClearStatistics}, NULL, FsLaClearStatisticsGet, FsLaClearStatisticsSet, FsLaClearStatisticsTest, FsLaClearStatisticsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsLaMCLAGSystemControl}, NULL, FsLaMCLAGSystemControlGet, FsLaMCLAGSystemControlSet, FsLaMCLAGSystemControlTest, FsLaMCLAGSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsLaPortChannelIfIndex}, GetNextIndexFsLaPortChannelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelGroup}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelGroupGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelAdminMacAddress}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelAdminMacAddressGet, FsLaPortChannelAdminMacAddressSet, FsLaPortChannelAdminMacAddressTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 1, 0, NULL},

{{12,FsLaPortChannelMacSelection}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMacSelectionGet, FsLaPortChannelMacSelectionSet, FsLaPortChannelMacSelectionTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 1, 0, "1"},

{{12,FsLaPortChannelMode}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelPortCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelPortCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelActivePortCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelActivePortCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelSelectionPolicy}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelSelectionPolicyGet, FsLaPortChannelSelectionPolicySet, FsLaPortChannelSelectionPolicyTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "3"},

{{12,FsLaPortChannelDefaultPortIndex}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDefaultPortIndexGet, FsLaPortChannelDefaultPortIndexSet, FsLaPortChannelDefaultPortIndexTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "0"},

{{12,FsLaPortChannelMaxPorts}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMaxPortsGet, FsLaPortChannelMaxPortsSet, FsLaPortChannelMaxPortsTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "8"},

{{12,FsLaPortChannelSelectionPolicyBitList}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelSelectionPolicyBitListGet, FsLaPortChannelSelectionPolicyBitListSet, FsLaPortChannelSelectionPolicyBitListTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGDistributingPortIndex}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGDistributingPortIndexGet, FsLaPortChannelDLAGDistributingPortIndexSet, FsLaPortChannelDLAGDistributingPortIndexTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGSystemID}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGSystemIDGet, FsLaPortChannelDLAGSystemIDSet, FsLaPortChannelDLAGSystemIDTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGSystemPriority}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGSystemPriorityGet, FsLaPortChannelDLAGSystemPrioritySet, FsLaPortChannelDLAGSystemPriorityTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "32768"},

{{12,FsLaPortChannelDLAGPeriodicSyncTime}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGPeriodicSyncTimeGet, FsLaPortChannelDLAGPeriodicSyncTimeSet, FsLaPortChannelDLAGPeriodicSyncTimeTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "1000"},

{{12,FsLaPortChannelDLAGMSSelectionWaitTime}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGMSSelectionWaitTimeGet, FsLaPortChannelDLAGMSSelectionWaitTimeSet, FsLaPortChannelDLAGMSSelectionWaitTimeTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "0"},

{{12,FsLaPortChannelDLAGRolePlayed}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGRolePlayedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGStatus}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGStatusGet, FsLaPortChannelDLAGStatusSet, FsLaPortChannelDLAGStatusTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "2"},

{{12,FsLaPortChannelDLAGRedundancy}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGRedundancyGet, FsLaPortChannelDLAGRedundancySet, FsLaPortChannelDLAGRedundancyTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "2"},

{{12,FsLaPortChannelDLAGMaxKeepAliveCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGMaxKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, "3"},

{{12,FsLaPortChannelDLAGPeriodicSyncPduTxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGPeriodicSyncPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGPeriodicSyncPduRxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGPeriodicSyncPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGEventUpdatePduTxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGEventUpdatePduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGEventUpdatePduRxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGEventUpdatePduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGElectedAsMasterCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGElectedAsMasterCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGElectedAsSlaveCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGElectedAsSlaveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelTrapTxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelTrapTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDLAGDistributingPortList}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDLAGDistributingPortListGet, FsLaPortChannelDLAGDistributingPortListSet, FsLaPortChannelDLAGDistributingPortListTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelMCLAGStatus}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGStatusGet, FsLaPortChannelMCLAGStatusSet, FsLaPortChannelMCLAGStatusTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "2"},

{{12,FsLaPortChannelMCLAGSystemID}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGSystemIDGet, FsLaPortChannelMCLAGSystemIDSet, FsLaPortChannelMCLAGSystemIDTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "000000000000"},

{{12,FsLaPortChannelMCLAGSystemPriority}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGSystemPriorityGet, FsLaPortChannelMCLAGSystemPrioritySet, FsLaPortChannelMCLAGSystemPriorityTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "32768"},

{{12,FsLaPortChannelMCLAGRolePlayed}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGRolePlayedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelMCLAGMaxKeepAliveCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGMaxKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, "3"},

{{12,FsLaPortChannelMCLAGPeriodicSyncPduTxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGPeriodicSyncPduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelMCLAGPeriodicSyncPduRxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGPeriodicSyncPduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelMCLAGEventUpdatePduTxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGEventUpdatePduTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelMCLAGEventUpdatePduRxCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelMCLAGEventUpdatePduRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelClearStatistics}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelClearStatisticsGet, FsLaPortChannelClearStatisticsSet, FsLaPortChannelClearStatisticsTest, FsLaPortChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortChannelTableINDEX, 1, 0, 0, "2"},

{{12,FsLaPortChannelUpCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDownCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDownCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelHotStandByPortsCount}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelHotStandByPortsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelOperChgTimeStamp}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelOperChgTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortChannelDownReason}, GetNextIndexFsLaPortChannelTable, FsLaPortChannelDownReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortChannelTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortIndex}, GetNextIndexFsLaPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortMode}, GetNextIndexFsLaPortTable, FsLaPortModeGet, FsLaPortModeSet, FsLaPortModeTest, FsLaPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortBundleState}, GetNextIndexFsLaPortTable, FsLaPortBundleStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortActorResetAdminState}, GetNextIndexFsLaPortTable, FsLaPortActorResetAdminStateGet, FsLaPortActorResetAdminStateSet, FsLaPortActorResetAdminStateTest, FsLaPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortAggregateWaitTime}, GetNextIndexFsLaPortTable, FsLaPortAggregateWaitTimeGet, FsLaPortAggregateWaitTimeSet, FsLaPortAggregateWaitTimeTest, FsLaPortTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, "2"},

{{12,FsLaPortPartnerResetAdminState}, GetNextIndexFsLaPortTable, FsLaPortPartnerResetAdminStateGet, FsLaPortPartnerResetAdminStateSet, FsLaPortPartnerResetAdminStateTest, FsLaPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortActorAdminPort}, GetNextIndexFsLaPortTable, FsLaPortActorAdminPortGet, FsLaPortActorAdminPortSet, FsLaPortActorAdminPortTest, FsLaPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortRestoreMtu}, GetNextIndexFsLaPortTable, FsLaPortRestoreMtuGet, FsLaPortRestoreMtuSet, FsLaPortRestoreMtuTest, FsLaPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortSelectAggregator}, GetNextIndexFsLaPortTable, FsLaPortSelectAggregatorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortErrStateDetCount}, GetNextIndexFsLaPortTable, FsLaPortErrStateDetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortErrStateRecCount}, GetNextIndexFsLaPortTable, FsLaPortErrStateRecCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortDefaultedStateThreshold}, GetNextIndexFsLaPortTable, FsLaPortDefaultedStateThresholdGet, FsLaPortDefaultedStateThresholdSet, FsLaPortDefaultedStateThresholdTest, FsLaPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, "5"},

{{12,FsLaPortHardwareFailureRecThreshold}, GetNextIndexFsLaPortTable, FsLaPortHardwareFailureRecThresholdGet, FsLaPortHardwareFailureRecThresholdSet, FsLaPortHardwareFailureRecThresholdTest, FsLaPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, "5"},

{{12,FsLaPortSameStateRecThreshold}, GetNextIndexFsLaPortTable, FsLaPortSameStateRecThresholdGet, FsLaPortSameStateRecThresholdSet, FsLaPortSameStateRecThresholdTest, FsLaPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLaPortTableINDEX, 1, 0, 0, "5"},

{{12,FsLaPortUpInBundleCount}, GetNextIndexFsLaPortTable, FsLaPortUpInBundleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortDownInBundleCount}, GetNextIndexFsLaPortTable, FsLaPortDownInBundleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortBundStChgTimeStamp}, GetNextIndexFsLaPortTable, FsLaPortBundStChgTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortDownInBundleReason}, GetNextIndexFsLaPortTable, FsLaPortDownInBundleReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortErrStateDetTimeStamp}, GetNextIndexFsLaPortTable, FsLaPortErrStateDetTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortErrStateRecTimeStamp}, GetNextIndexFsLaPortTable, FsLaPortErrStateRecTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortRecTrigReason}, GetNextIndexFsLaPortTable, FsLaPortRecTrigReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortDefStateRecCount}, GetNextIndexFsLaPortTable, FsLaPortDefStateRecCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortSameStateRecCount}, GetNextIndexFsLaPortTable, FsLaPortSameStateRecCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortHwFailRecCount}, GetNextIndexFsLaPortTable, FsLaPortHwFailRecCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortDefStateRecTimeStamp}, GetNextIndexFsLaPortTable, FsLaPortDefStateRecTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortSameStateRecTimeStamp}, GetNextIndexFsLaPortTable, FsLaPortSameStateRecTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortHwFailRecTimeStamp}, GetNextIndexFsLaPortTable, FsLaPortHwFailRecTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaPortRecState}, GetNextIndexFsLaPortTable, FsLaPortRecStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLaPortTableINDEX, 1, 0, 0, NULL},

{{12,FsLaTrapPortChannelIndex}, GetNextIndexFsLaHwFailTrapObjectsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaHwFailTrapObjectsTableINDEX, 2, 0, 0, NULL},

{{12,FsLaTrapPortIndex}, GetNextIndexFsLaHwFailTrapObjectsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaHwFailTrapObjectsTableINDEX, 2, 0, 0, NULL},

{{12,FsLaHwFailTrapType}, GetNextIndexFsLaHwFailTrapObjectsTable, FsLaHwFailTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaHwFailTrapObjectsTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGTrapPortChannelIndex}, GetNextIndexFsLaDLAGTrapObjectsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaDLAGTrapObjectsTableINDEX, 1, 0, 0, NULL},

{{12,FsLaDLAGTrapType}, GetNextIndexFsLaDLAGTrapObjectsTable, FsLaDLAGTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaDLAGTrapObjectsTableINDEX, 1, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelSystemID}, GetNextIndexFsLaDLAGRemotePortChannelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelSystemPriority}, GetNextIndexFsLaDLAGRemotePortChannelTable, FsLaDLAGRemotePortChannelSystemPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelRolePlayed}, GetNextIndexFsLaDLAGRemotePortChannelTable, FsLaDLAGRemotePortChannelRolePlayedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelKeepAliveCount}, GetNextIndexFsLaDLAGRemotePortChannelTable, FsLaDLAGRemotePortChannelKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelSpeed}, GetNextIndexFsLaDLAGRemotePortChannelTable, FsLaDLAGRemotePortChannelSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelHighSpeed}, GetNextIndexFsLaDLAGRemotePortChannelTable, FsLaDLAGRemotePortChannelHighSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortChannelMtu}, GetNextIndexFsLaDLAGRemotePortChannelTable, FsLaDLAGRemotePortChannelMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaDLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaDLAGRemotePortIndex}, GetNextIndexFsLaDLAGRemotePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaDLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaDLAGRemotePortBundleState}, GetNextIndexFsLaDLAGRemotePortTable, FsLaDLAGRemotePortBundleStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaDLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaDLAGRemotePortSyncStatus}, GetNextIndexFsLaDLAGRemotePortTable, FsLaDLAGRemotePortSyncStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaDLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaDLAGRemotePortPriority}, GetNextIndexFsLaDLAGRemotePortTable, FsLaDLAGRemotePortPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaDLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelSystemID}, GetNextIndexFsLaMCLAGRemotePortChannelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelSystemPriority}, GetNextIndexFsLaMCLAGRemotePortChannelTable, FsLaMCLAGRemotePortChannelSystemPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelRolePlayed}, GetNextIndexFsLaMCLAGRemotePortChannelTable, FsLaMCLAGRemotePortChannelRolePlayedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelKeepAliveCount}, GetNextIndexFsLaMCLAGRemotePortChannelTable, FsLaMCLAGRemotePortChannelKeepAliveCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelSpeed}, GetNextIndexFsLaMCLAGRemotePortChannelTable, FsLaMCLAGRemotePortChannelSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelHighSpeed}, GetNextIndexFsLaMCLAGRemotePortChannelTable, FsLaMCLAGRemotePortChannelHighSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortChannelMtu}, GetNextIndexFsLaMCLAGRemotePortChannelTable, FsLaMCLAGRemotePortChannelMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaMCLAGRemotePortChannelTableINDEX, 2, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortIndex}, GetNextIndexFsLaMCLAGRemotePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsLaMCLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortSlotIndex}, GetNextIndexFsLaMCLAGRemotePortTable, FsLaMCLAGRemotePortSlotIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaMCLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortBundleState}, GetNextIndexFsLaMCLAGRemotePortTable, FsLaMCLAGRemotePortBundleStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaMCLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortSyncStatus}, GetNextIndexFsLaMCLAGRemotePortTable, FsLaMCLAGRemotePortSyncStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLaMCLAGRemotePortTableINDEX, 3, 0, 0, NULL},

{{12,FsLaMCLAGRemotePortPriority}, GetNextIndexFsLaMCLAGRemotePortTable, FsLaMCLAGRemotePortPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsLaMCLAGRemotePortTableINDEX, 3, 0, 0, NULL},
};
tMibData fslaEntry = { 127, fslaMibEntry };

#endif /* _FSLADB_H */

