/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: lasz.h,v 1.4 2016/01/11 12:49:28 siva Exp $
 *
 *******************************************************************/

enum {
    MAX_LA_AGG_IN_SYS_SIZING_ID,
    MAX_LA_CACHE_ENTRIES_SIZING_ID,
    MAX_LA_CONTROL_MESG_SIZING_ID,
    MAX_LA_INTF_MESG_SIZING_ID,
    MAX_LA_MARKER_ENTRIES_SIZING_ID,
    MAX_LA_PORTS_IN_SYS_SIZING_ID,
    LA_MAX_SIZING_ID
};


#ifdef  _LASZ_C
tMemPoolId LAMemPoolIds[ LA_MAX_SIZING_ID];
INT4  LaSizingMemCreateMemPools(VOID);
VOID  LaSizingMemDeleteMemPools(VOID);
INT4  LaSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LASZ_C  */
extern tMemPoolId LAMemPoolIds[ ];
extern INT4  LaSizingMemCreateMemPools(VOID);
extern VOID  LaSizingMemDeleteMemPools(VOID);
extern INT4  LaSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _LASZ_C  */


#ifdef  _LASZ_C
tFsModSizingParams FsLASizingParams [] = {
{ "tLaLacAggEntry", "MAX_LA_AGG_IN_SYS", sizeof(tLaLacAggEntry),MAX_LA_AGG_IN_SYS, MAX_LA_AGG_IN_SYS,0 },
{ "tLaCacheEntry", "MAX_LA_CACHE_ENTRIES", sizeof(tLaCacheEntry),MAX_LA_CACHE_ENTRIES, MAX_LA_CACHE_ENTRIES,0 },
{ "tLaCtrlMesg", "MAX_LA_CONTROL_MESG", sizeof(tLaCtrlMesg),MAX_LA_CONTROL_MESG, MAX_LA_CONTROL_MESG,0 },
{ "tLaIntfMesg", "MAX_LA_INTF_MESG", sizeof(tLaIntfMesg),MAX_LA_INTF_MESG, MAX_LA_INTF_MESG,0 },
{ "tLaMarkerEntry", "MAX_LA_MARKER_ENTRIES", sizeof(tLaMarkerEntry),MAX_LA_MARKER_ENTRIES, MAX_LA_MARKER_ENTRIES,0 },
{ "tLaLacPortEntry", "MAX_LA_PORTS_IN_SYS", sizeof(tLaLacPortEntry),MAX_LA_PORTS_IN_SYS, MAX_LA_PORTS_IN_SYS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _LASZ_C  */
extern tFsModSizingParams FsLASizingParams [];
#endif /*  _LASZ_C  */


