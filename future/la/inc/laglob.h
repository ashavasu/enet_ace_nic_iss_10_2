/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: laglob.h,v 1.24 2017/10/16 09:40:59 siva Exp $
 *
 * Description: This file contains the global structure used
 *              in Link Aggregation module.
 *
 *****************************************************************************/
#ifndef _LAGLOB_H
#define _LAGLOB_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : laglob.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains the global structure used   */ /*                            in Link Aggregation module.                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

#ifdef _LASYS_C
#include "lamacro.h"

tLaGlobalInfo         gLaGlobalInfo;
tLaSysControl         gLaSystemControl = LA_SHUTDOWN; /* Manager sets this field
                                                         to start or shut down
                                                         the LA module */
tLaMemPoolId          gLaCtrlMesgPoolId = LA_INVALID_VAL;
tLaQId                gLaCtrlQId = LA_INVALID_VAL;
tLaMemPoolId          gLaIntfMesgPoolId = LA_INVALID_VAL;
tLaQId                gLaIntfQId = LA_INVALID_VAL;
tLaTaskId             gLaTaskId = LA_INVALID_VAL;
tLaSemId              gLaProtocolSemId = LA_INVALID_VAL;
tLaSemId              gLaSyncSemId = LA_INVALID_VAL;
tLaNodeStatus         gLaNodeStatus; /* Can be LA_NODE_IDLE /
                                   * LA_NODE_ACTIVE/ LA_NODE_STANDBY / 
                                   * For Active Node it will be 
                                   * LA_ACTIVE_NODE, for standby node 
                                   * it will be LA_STANDBY_NODE.
                                   */
tLaCallBackEntry     gLaCallBack [LA_MAX_CALLBACK_EVENTS];
tLaCallSequence      gLaCallSequence = LA_PROTCOL_FLOW;
UINT1                gu1LaInitialized = FALSE; 
UINT4                gu4PartnerConfig = LA_DISABLED;
UINT4                gu4LagAdminDownNoNeg = LA_FALSE;
UINT4                gu4UsePartnerAdminInfo = LA_TRUE;
                                /* Configures whether port-channel should
                                   use default admin parameters for all the 
                                   ports when all the ports receive state 
                                   is in defaulted/port-disabled state. 
                                   If variable is LA_FALSE member ports will 
                                   not use default partner admin values and results 
                                   in "down, not in bundle" state. If variable is 
                                   LA_TRUE selection logic will be triggered to 
                                   use the default admin partner value's as Oper 
                                   partner values results in "up,in bundle" state. */

UINT4                gu4RecoveryTime = LA_DEFAULT_RECOVERY_TIME;
UINT4                gu4RecTrgdCount = LA_INIT_VAL;
                                     /* Configurable Recovery time.
                                      * This value is used for starting
                                      * the Recovery Timer. */
UINT4                gu4RecThreshold = LA_DEFAULT_REC_THRESHOLD;
                                     /* Configurable Recovery threshold value.
                                      * Configuring this value sets defaulted
                                      * state threshold,same state threshold,
                                      * hardware failure recovery threshold.*/

UINT4            gu4DefaultedStateThreshold = LA_DEF_STATE_DEFAULT_THRESHOLD;
                                        /*Configurable Recovery threshold value.
                                         *This value is used to limit the 
                                         *number of times error-recovery is 
                                         *undergone by a defaulted state port. */
UINT4                gu4SameStateRecThreshold = LA_DEFAULT_REC_THRESHOLD;
                                         /*Configurable Recovery threshold value.
                                          * This value is used for checking
                                          * whether the same state counter
                                          * has reached this value and to
                                          trigger recovery based on this */                                  
            
UINT4                gu4HardwareFailureRecThreshold = LA_DEFAULT_REC_THRESHOLD;
                                         /* Configurable Recovery threshold value.
                                          * This value is used to limit the number 
                                          * of times error-recovery is 
                                          * undergone during hardware failure */
UINT4            gu4RecThresholdExceedAction = LA_ACT_NONE;
                                        /*This variable is used to 
                                         *indicate the action to be 
                                         *performed on exceeding the 
                                         *recovery threshold */
UINT1                gu1DLAGSystemStatus = LA_DLAG_DISABLED;
                                /* This variable is used to enable
                                   the DLAG in System which will 
                                   enable in per port-channel 
                                   based on mandatory params */
                                    
UINT1                gu1MCLAGSystemStatus = LA_DISABLED;
                                /* This variable is used to enable
                                   the DLAG in System which will 
                                   enable in per port-channel 
                                   based on mandatory params */
                                    
UINT1                gu1AADLAGMasterDown = LA_FALSE;
                                /* This variable is used indicate 
                                 * Master down in the DLAG network */

UINT1               gu1AADLAGRolePlayed = LA_DLAG_SYSTEM_ROLE_NONE;
                                /* This variable is used in identify the Role 
                                 * of the DLAG node, value can be either
                                 * Master or Slave. */
                                    
                                    
UINT1               gu1MclagPeerExists = LA_FALSE;
                                 /* This variable is used to identify the Remote
                                  * MC-LAG node availability.
                                  */
UINT1               gu1MclagClearCounter = LA_FALSE;
                                 /* This variable is used to reset the 
                                  * counters when set to TRUE. */  
tLaSysControl       gLaMclagSystemControl = LA_START;
                                  /*This variable starts or shutdown the
                                    MC-LAG functionality in the system*/

UINT1 gu1LaMclagShutDownInProgress = OSIX_FALSE;

 
UINT1               gu1LacpClearCounter = LA_FALSE;
                                  /* This variable is used to reset the 
                                   * counters when set to TRUE. */  
  
#else
extern tLaGlobalInfo      gLaGlobalInfo;
extern tLaSysControl      gLaSystemControl;
extern tLaMemPoolId       gLaCtrlMesgPoolId;
extern tLaQId             gLaCtrlQId;
extern tLaMemPoolId       gLaIntfMesgPoolId;
extern tLaQId             gLaIntfQId;
extern tLaTaskId          gLaTaskId;
extern tLaSemId           gLaProtocolSemId;
extern tLaSemId           gLaSyncSemId;
 
extern tLaNodeStatus      gLaNodeStatus; 
extern tLaCallBackEntry   gLaCallBack [LA_MAX_CALLBACK_EVENTS];
extern tLaCallSequence    gLaCallSequence;
extern UINT1              gu1LaInitialized;
extern UINT4              gu4PartnerConfig;
extern UINT4              gu4UsePartnerAdminInfo;
extern UINT4              gu4LagAdminDownNoNeg;
extern UINT4              gu4RecoveryTime;
extern UINT4              gu4RecThreshold;
extern UINT4              gu4RecTrgdCount;
extern UINT4              gu4DefaultedStateThreshold;
extern UINT4              gu4SameStateRecThreshold;
extern UINT4              gu4HardwareFailureRecThreshold;
extern UINT4              gu4RecThresholdExceedAction;
extern UINT1              gu1DLAGSystemStatus;
extern UINT1              gu1MCLAGSystemStatus;
extern UINT1              gu1AADLAGMasterDown;
extern UINT1              gu1AADLAGRolePlayed;

extern UINT1 gu1LaMclagShutDownInProgress;
extern tFsModSizingInfo gFsLaSizingInfo;
extern tFsModSizingParams FsLaSizingParams [] ;
extern UINT1       gu1MclagPeerExists;
extern UINT1       gu1MclagClearCounter;
extern UINT1       gu1LacpClearCounter;
extern tLaSysControl       gLaMclagSystemControl;
#endif /*  _LASYS_C */


#endif
