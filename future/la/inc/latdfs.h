/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: latdfs.h,v 1.66 2017/11/16 14:26:54 siva Exp $
 *
 * Description: This file contains all typedefs and enums used
 *              in Link Aggregation module.
 *
 *****************************************************************************/
#ifndef _LATDFS_H
#define _LATDFS_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : latdfs.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains all typedefs and enums used */
/*                            Link Aggregation module.                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Enumerated Constants                                                      */
/*****************************************************************************/

typedef tTMO_HASH_TABLE     tLaHashTable;  
typedef tTMO_HASH_NODE      tLaHashNode; 

typedef tTMO_SLL            tLaSll;
typedef tTMO_SLL_NODE       tLaSllNode;

typedef tRBTree             tLaRBTree;
typedef tRBNodeEmbd         tLaRBNodeEmbd;

typedef tTmrAppTimer        tLaAppTimer;
typedef tTimerCfg           tLaTimerCfg; 
typedef tTimerListId        tLaTimerListId;

typedef tOsixTaskId         tLaTaskId;
typedef tOsixSysTime        tLaSysTime;  
typedef tOsixQId            tLaQId;   
typedef tOsixSemId          tLaSemId;

typedef tMemPoolId          tLaMemPoolId;  
typedef tMacAddr            tLaMacAddr;

/* Statistics counter in CFA */ 
typedef tIfCountersStruct   tLaIfaceStats;

/* Port list */
typedef UINT1  tLaPortList[LA_PORT_LIST_SIZE];

typedef UINT4  tLaTime;
typedef UINT4  tLaTicks;

/* Timer enums */
enum {
   LA_NOW = (tLaTime)0,
   LA_EXPIRED = (tLaTime)0,
   LA_STOPPED = (tLaTime)0
};

/* Admin Status */
enum {
   LA_ADMIN_UP = 1,
   LA_ADMIN_DOWN = 2
};
   
/* oper Status */
enum {
   LA_OPER_UP = CFA_IF_UP,
   LA_OPER_DOWN = CFA_IF_DOWN
};

enum {
   LA_RESET = LA_FALSE,
   LA_SET = LA_TRUE
};

/* Timer Types */
enum {
   LA_CACHE_TMR_TYPE    = 1,
   LA_MARKER_TMR_TYPE   = 2,
   LA_TICK_TMR_TYPE     = 3,
   LA_TX_SCHEDULER_TMR_TYPE = 4,
   LA_MARKER_RATE_TMR_TYPE = 5,
   LA_PERIODIC_TMR_TYPE = 6,
   LA_CURRENT_WHILE_TMR_TYPE =7,
   LA_WAIT_WHILE_TMR_TYPE=8,
   LA_HOLD_WHILE_TMR_TYPE=9,
   LA_DLAG_PERIODIC_TMR_TYPE=10,
   LA_DLAG_MS_SELECTION_WAIT_TMR_TYPE=11,
   LA_AA_DLAG_PERIODIC_TMR_TYPE = 12,
   LA_AA_MCLAG_PERIODIC_TMR_TYPE = 13,
   LA_RECOVERY_TMR_TYPE = 14,
   LA_MAX_TMR_TYPE
};      

enum {
   ETH_TEN_MBPS,
   ETH_HUNDRED_MBPS,
   ETH_THOUSAND_MBPS,
   ETH_TEN_GBPS,
   ETH_INVALID_RATE
};

enum {
   HALF_DUPLEX,
   FULL_DUPLEX
};

typedef enum {
   LA_SELECTED = 1,
   LA_STANDBY,
   LA_UNSELECTED,
   LA_OUT_OF_SYNC
} tLaSelect;

typedef enum {
    LA_NONE = 0,
    LA_IF_OPER_DOWN,
    LA_PARTNER_KEY_MISMATCH,
    LA_PARTNER_ID_MISMATCH,
    LA_PARTNER_KEY_ZERO,
    LA_SPEED_MISMATCH,
    LA_MTU_MISMATCH,
    LA_PAUSEMODE_MISMATCH,
    LA_LOOPBACK_CONNECTION,
    LA_HW_FAILURE,
    LA_HOT_STANDBY_ERR,
    LA_NO_LACP_RCVD
} tLaPortDownReason;

       /* The tx_scheduler timer checks ntt frequently, and transmits as needed
       * subject to the maximum transmission rate limitations imposed by the
       * Tx Machine. This is the approach to transmit scheduling.
       */  
#define  LA_TX_SCHEDULING_TICKS  (SYS_TIME_TICKS_IN_A_SEC / 5)
#define  LA_CFA_NET_IF_UPD   2

#define LA_IF_ADMIN_DOWN 2

enum {/*
       * All the timers operate in terms of LA_TICKS.
       */ 
       LA_FAST_PERIODIC_TICKS = 1,
       LA_SLOW_PERIODIC_TICKS = 30,
   
       LA_SHORT_TIMEOUT_TICKS = (3*LA_FAST_PERIODIC_TICKS),
       LA_LONG_TIMEOUT_TICKS  = (3*LA_SLOW_PERIODIC_TICKS),
   
       LA_AGGREGATE_WAIT_TICKS  = 2,
   
       LA_TX_INTERVAL_TICKS     = 1,

       LA_COLLECTOR_DELAY_TICKS = 2,
   
       LA_MARKER_RATE_TIMEOUT   = 1,
       LA_CACHE_TIMEOUT         = 20,
       LA_MARKER_TIMEOUT        = 30,
       LA_CHURN_DETECTION_TICKS = 60
};

enum {/* 
       * Maximum number of transmissions within LA_TX_INTERVAL_TICKS.
       */
      MAX_TX_PER_INTERVAL = 3
};



/* Churn Detection Machine States */
typedef enum {
   CHURN_MONITOR,
   CHURN,
   NO_CHURN
}tLaLacChurnState;


/* Actor Churn States */
enum {
   ACTOR_CHURN_MONITOR = CHURN_MONITOR,
   ACTOR_CHURN = CHURN,
   NO_ACTOR_CHURN = NO_CHURN
};


/* Partner Churn States */
enum {
   PARTNER_CHURN_MONITOR = CHURN_MONITOR,
   PARTNER_CHURN = CHURN,
   NO_PARTNER_CHURN = NO_CHURN
};


/* Timer flag to indicate whether a timer is running or is stopped */
typedef enum {
   LA_TMR_STOPPED = (tLaTime)0,
   LA_TMR_RUNNING = (tLaTime)1
}tLaTmrFlag;



/* mib */
typedef enum {
   LA_TRAP_ENABLED  = 1,
   LA_TRAP_DISABLED = 2 
}tLaTrapEnable;


typedef enum {
   LA_PORT_DISABLED = LA_FALSE,
   LA_PORT_ENABLED  = LA_TRUE
}tLaPortEnable;

typedef enum {
   LA_LACP_DISABLED = LA_FALSE,
   LA_LACP_ENABLED  = LA_TRUE
}tLaLacpEnable;


typedef enum {
   LA_UNAUTH = LA_FALSE,
   LA_AUTH   = LA_TRUE
}tLaAuthStatus;

/* Indicates Port is Aggregatable or Individual */
typedef enum {
   LA_INDIV = LA_FALSE,
   LA_AGG = LA_TRUE
}tLaAggOrIndiv;


typedef enum {
   LA_PORT_ATTACH,
   LA_PORT_DETACH
}tLaPortStateFlag;



#ifdef LA_UTILS
      tMacAddr gLaBCastAddr = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
      tMacAddr gLaSlowProtAddr = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x02};
#else
extern tMacAddr gLaBCastAddr;
extern tMacAddr gLaSlowProtAddr;
#endif


/*
 ******************************************************************************
 tLaLacEvent
 
 This Data structure defines the different events that trigger various State
 Event Machines associated with the Link Aggregation Protocol.
 ******************************************************************************
 */
typedef enum {
   LA_HW_EVENT_PORT_INIT = 0,
   LA_HW_EVENT_PORT_ATTACH,
   LA_HW_EVENT_PORT_DETACH,
   LA_HW_EVENT_ENABLE_COLLECTOR,
   LA_HW_EVENT_DISABLE_COLLECTOR,
   LA_HW_EVENT_ENABLE_DISTRIBUTOR,
   LA_HW_EVENT_DISABLE_DISTRIBUTOR
}tLaHwEvent;

typedef enum {
    LA_ACTIVITY_STATE = 0,
    LA_TIMEOUT_STATE,
    LA_AGGREGATION_STATE,
    LA_SYNC_STATE,
    LA_COLLECTING_STATE,
    LA_DISTRIBUTING_STATE,
    LA_DEFAULTED_STATE,
    LA_EXPIRED_STATE
}tLaPortState;

typedef enum {
    LA_CHURN_EVENT_PORT_ENABLED = 0,
    LA_CHURN_EVENT_PORT_DISABLED,
    LA_CHURN_EVENT_TICK
} tLaChurnEvent;

/*
 ******************************************************************************

 
 This Data structure defines the different states / Events of State Machines in LACP protocol.
 ******************************************************************************
 */

typedef enum {
   LA_MUX_EVENT_PORT_INIT             = 0, /* Port is initialising */
   LA_MUX_EVENT_UNSELECTED            = 1, /* Port is not selected
                                            * to attach to aggregator */

   LA_MUX_EVENT_SELECTED              = 2, /* Port is selected to attach 
                                            * to aggregator */
   LA_MUX_EVENT_STANDBY               = 3, /* Port is selected as 
                                            * standby link */
   LA_MUX_EVENT_PARTNER_SYNC          = 4, /* Partner has synced with me */
   LA_MUX_EVENT_PARTNER_NO_SYNC       = 5, /* Partner has gone 
                                            * out of sync */
   LA_MUX_EVENT_PARTNER_COLLECTING    = 6, /* Partner has entered 
                                            * collecting state 
                                            */
   LA_MUX_EVENT_PARTNER_NO_COLLECTING = 7, /* Partner has gone out of 
                                            * collecting state 
                                            */
   LA_MUX_EVENT_WAIT_WHILE_EXP        = 8, /* Wait while timer expired */
   LA_MUX_EVENT_READY_SET             = 9, /* Ready variable is active */

   LA_MUX_EVENT_COUNT                 = 10
}tLaMuxSmEvent;


typedef enum {
   LA_MUX_STATE_DETACHED     = 0,
   LA_MUX_STATE_WAITING      = 1,
   LA_MUX_STATE_ATTACHED     = 2,
   LA_MUX_STATE_COLLECTING   = 3,
   LA_MUX_STATE_DISTRIBUTING = 4,
   LA_MUX_STATE_COUNT        = 5
}tLaMuxSmState;

typedef enum {
   LA_RXM_EVENT_PORT_MOVED         = 0,
   LA_RXM_EVENT_PORT_INIT          = 1,
   LA_RXM_EVENT_PORT_DISABLED      = 2,
   LA_RXM_EVENT_PORT_ENABLED       = 3,
   LA_RXM_EVENT_CURRENT_WHILE_EXP  = 4,
   LA_RXM_EVENT_PDU_RXD            = 5,
   LA_RXM_EVENT_ACTOR_ADMIN_CHANGED = 6,
   LA_RXM_EVENT_PARTNER_ADMIN_CHANGED = 7
}tLaRxSmEvent;

typedef enum {
   LA_RXM_STATE_NULL = 0,
   LA_RXM_STATE_CURRENT,
   LA_RXM_STATE_EXPIRED,
   LA_RXM_STATE_DEFAULTED,
   LA_RXM_STATE_INITIALIZE, 
   LA_RXM_STATE_LACP_DISABLED,
   LA_RXM_STATE_PORT_DISABLED,
   LA_RXM_STATE_LACP_ENABLED,
   LA_RXM_STATE_INVALID
}tLaRxSmState;

typedef enum {
   LA_PTXM_EVENT_PORT_INIT       = 0,
   LA_PTXM_EVENT_DISABLE,
   LA_PTXM_EVENT_PTMR_EXPIRED,
   LA_PTXM_EVENT_VARS_CHANGED
}tLaPTxSmEvent;

typedef enum {
   LA_PTXM_STATE_NO_PERIODIC   = 0,
   LA_PTXM_STATE_FAST_PERIODIC = 1,
   LA_PTXM_STATE_SLOW_PERIODIC = 2,
   LA_PTXM_STATE_PERIODIC_TX   = 3
}tLaPTxSmState;

 
typedef enum {
   LA_TXM_EVENT_PORT_INIT     = 0,
   LA_TXM_EVENT_PORT_DISABLED = 1,
   LA_TXM_EVENT_PORT_ENABLED  = 2,
   LA_TXM_EVENT_NTT           = 3,
   LA_TXM_EVENT_TICK          = 4,
   LA_TXM_EVENT_TX_DATA       = 5
}tLaTxSmEvent;

typedef enum {
    LA_AGG_IN_HW = 0,
    LA_AGG_NOT_IN_HW,
    LA_AGG_ADD_FAILED_IN_HW
}tLaHwAggStatus;

typedef enum {
    LA_PROTCOL_FLOW = 0,
    LA_NP_CALLBACK_FLOW
}tLaCallSequence;
typedef UINT4 tLaNodeStatus;

typedef enum {
    LA_HW_PORT_NOT_IN_LAGG = 0, 
    LA_HW_PORT_ADDED_IN_LAGG,  
    LA_HW_REM_PORT_FAILED_IN_LAGG,
    LA_HW_ADD_PORT_FAILED_IN_LAGG
}tLaLacHwPortStatus;

/*
 ******************************************************************************
 tLaSystem          
 
 This is a globally unique identifier used to identify a System shall be the
 concatenation of a globally administered individual MAC address and the
 System Priority.
 ******************************************************************************
 */
typedef struct  {
   tLaMacAddr        SystemMacAddr;
   UINT2             u2SystemPriority;
}tLaSystem;

/* Port Statistics */
typedef struct {
   UINT4    u4LacpduRx;
   UINT4    u4MarkerPduRx;
   UINT4    u4MarkerResponsePduRx;
   UINT4    u4UnknownRx;
   UINT4    u4IllegalRx;
   UINT4    u4LacpduTx;
   UINT4    u4MarkerPduTx;
   UINT4    u4MarkerResponsePduTx;
}tLaPortStats;


/*****************************************************************************/
/* tLaAppTmrNode                                                             */
/*                                                                           */
/* This is the Timer Structure for the timers in the LA module               */
/*****************************************************************************/
typedef struct {
   tLaAppTimer   LaAppTimer; /* This is the tTmrAppTimer of the FSAP2. The 
                                u4Data field in the structure has to be 
                                initialised to the Timer Type (Cache timer, 
                                Marker Timer, etc.) while starting a timer */
   
   VOID          *pEntry;    /* This pointer will hold the port entry pointer
                                for marker entry and the cache entry pointer
                                for cache entry. Each time this pointer is 
                                initialised, it must be properly type cast */
   tLaTmrFlag    LaTmrFlag;  /* This flag is used to indicate whether a timer 
                                is running or is stopped. */
}tLaAppTmrNode;


/*
 ******************************************************************************
 tLaLacIfInfo

 This stucture stores interface information. This is common to Port as well as 
 Aggregator.
 ******************************************************************************
 */
typedef struct {
   tLaSystem               LaSystem;
   tLaLacPortState         LaLacPortState;
   UINT2                   u2IfIndex;
   UINT2                   u2IfKey;
   UINT2                   u2IfPriority;
   UINT2                   u2Reserved;
} tLaLacIfInfo;


/* Port Debug information */ 
typedef struct {
   tLaSysTime      LaLastRxTime;
   UINT4           u4ActorChurnCount;
   UINT4           u4PartnerChurnCount;
   UINT4           u4ActorSyncTransitionCount;
   UINT4           u4PartnerSyncTransitionCount;       
   UINT4           u4ActorChangeCount;
   UINT4           u4PartnerChangeCount;
   UINT1           au1LaMuxReasonStr[LA_MUX_REASON_LEN];
}tLaPortDebug;


/*****************************************************************************/
/* tLaMarkerEntry                                                            */
/*                                                                           */ 
/* This is the Marker Entry structure. There will be one Marker Entry per    */
/* Port.                                                                     */
/*****************************************************************************/
typedef struct {
   UINT4           u4TransactionId;
   tLaAppTmrNode   LaMarkerTmr;
}tLaMarkerEntry;


typedef struct LaLacPortEntry        tLaLacPortEntry;

typedef struct LaLacAggEntry         tLaLacAggEntry;

typedef struct LaDLAGRemoteAggEntry  tLaDLAGRemoteAggEntry;
typedef struct LaDLAGRemotePortEntry tLaDLAGRemotePortEntry;

typedef struct LaLacAggConfigEntry   tLaLacAggConfigEntry;
typedef struct LaLacAggIfEntry       tLaLacAggIfEntry;

/*****************************************************************************/
/* tLaLacPortEntry                                                           */
/*                                                                           */ 
/* This structure contains all Link Aggregation Control configuration        */
/* parameters for each Port.                                                 */
/*****************************************************************************/
struct LaLacPortEntry {
   tLaSllNode           NextPortEntry;      /* Next entry in the list */
   tLaSllNode           NextDefPortEntry;   /* Next entry in the default port
                                               list */
   tLaLacAggEntry      *pAggEntry;          /* 1. Pointer to the aggregator or 
                                               Port Channel entry selected
                                               based on the actor key
                                               configured.
                                               2.When ports are manually bound
                                               to an aggregator, this points to
                                               the configured aggregator. In
                                               that case, this port can Select 
                                               and Attach only to this 
                                               aggregator and pDefAggEntry will
                                               be NULL */

   tLaLacAggEntry      *pDefAggEntry;       /* Pointer to the default
                                               aggregator or port Channel
                                               entry to which this port is
                                               configured. When this is
                                               configured, this port can Select
                                               and Attach dynamically to a 
                                               different aggregator based on
                                               Partner info */

   tLaMarkerEntry       *pLaMarkerEntry;    /*Points to the Marker entry for 
                                               port */
   /* Actor LACPDU Information */ 
   tLaLacIfInfo         LaLacActorInfo;      /* Contains the Actor's 
                                                Operational information */  
   
   tLaLacIfInfo         LaLacActorAdminInfo; /* Contains The Actor's Admin
                                                information */
   /* Partner LACPDU Information */
   tLaLacIfInfo         LaLacPartnerInfo;    /* Contains the Partner's
                                                Operational information as in
                                                the most recently received
                                                LACPDU */

   tLaLacIfInfo         LaLacPartnerAdminInfo;  /* Contains the Partner's Admin
    * information */


  /* Timers */
   tLaAppTmrNode        LaTickTimer;      /* This is a 1 second timer. All other
                                             timing counters (except
                                             LaTxScheduler)function as multiples
                                             of this timer */
   tLaAppTmrNode        CurrentWhileTmr;  /* This timer is used to detect
                                           * whether received protocol info.
                                           * has expired. 
                                           * Set to 3 if Short 
                                           * Timeout or 90 if Long Timeout.
                                           */
   tLaAppTmrNode        PeriodicTmr;      /* Timer to ensure periodic
                                           * transmission of LACPDUs. Duration
                                           * can be LA_FAST_PERIODIC_TIME or
                                           * LA_SLOW_PERIODIC_TIME
                                           */
   tLaAppTmrNode        WaitWhileTmr;     /* This timer is started to 
                                           * count the waiting period for the
                                           * port before attaching to the 
                                           * aggregator.
                                           */
   tLaAppTmrNode        HoldWhileTmr;      /* One second timer to 
                                            * reset u4HoldCount.
                                            */
   tLaAppTmrNode        RecoveryTmr;       /* This timer is used for
                                              recovering the port from
                                              inconsistent state.
                                            */
   /* State of State Machines */
   tLaRxSmState         LaRxmState;          /* State of the Receive Machine */
   tLaMuxSmState        LaMuxState;          /* State of the Mux Control 
                                                Machine. */
   tLaPTxSmState        PTxSmState; /* Current State of 
                                         Periodic Tx Machine */
   /* Variables used by the State Machines */
   tLaBoolean           NttFlag;       /* Need to Transmit flag -
                                          Indicates a LACPDU is to
                                          be transmitted on the port.
                                        */
   tLaSelect            AggSelected;   /* States whether an Aggregator has
                                          been selected for the port */ 
   tLaBoolean           LaAggAttached;      /* TRUE if the local system 
                                               resources have attached the port
                                               to the aggregator */
   tLaBoolean           OperIndividual;     /* TRUE if the port is 
                                             * set aggregatable
                                             * but the port is operating as
                                             * individual link without attaching
                                             * to aggregator.
                                             */
   tLaBoolean          bPartnerActive;      /* TRUE if partner is present on 
                                             * the port. Else FALSE.
                                             */
   tLaBoolean          bReadyN;             /* TRUE if the WaitWhileTmr for
                                             * the port has expired. This
                                             * will be made FALSE 
                                             * in Mux machine DETACHED
                                             * state.
                                             */
   tLaBoolean          bReady;              /* Set to TRUE if the port
                                             * can attach to aggregator.
                                             */

   tLaBoolean          PortUsingPartnerAdminInfo; /* Set to TRUE 
                                                   * if port is aggregated 
                           * using Partner admin values
                                                   */
   
   tLaBoolean          InDefaulted;     /* Set to TRUE 
                                             * if port is in Defaulted state
                     */
   tLaBoolean           bDynAggSelect;      /* Set to TRUE if the Port can be
                                               reallocated dynamically to a
                                               different aggregator. Default is
                                               TRUE for 10 GE interfaces */

   tLaLacpMode          LaLacpMode;         /* The Port runs in either of the 3
                                               modes - ENABLED, DISABLED or
                                               MANUAL */
   tLaPortEnable        LaPortEnabled;      /* Indicates whether the Port is 
                                               operational or not */
   tLaLacpEnable        LaLacpEnabled;      /* Indicates whether the port is 
                                               operating the LACP. 
                                               Req. IEEE 802.3ad 43.4.8 - 2 
                                             */
 
   tLaPortStats         LaPortStats;
   
   tLaPortDebug         LaPortDebug;

   tLaLacHwPortStatus   LaLacHwPortStatus;

   UINT4                u4CurrentWhileTime; /* CurrentWhileTimer Value / 
                                               LA_RED_CURRWHILE_SPLIT_INTERVALS 
                                             */

   UINT4                u4PeriodicTime;    /* Duration with which 
                                            * the PeriodicTmr is started.
                                            * Values can be 
                                            * LA_FAST_PERIODIC_TIME or
                                            * LA_SLOW_PERIODIC_TIME.
                                            */
   UINT4                u4WaitWhileTime;   /* Configurable WaitWhile time.
                                            * This value is used for starting
                                            * the WaitWhileTmr.
                                            */
   UINT4                u4HoldWhileTime;

   UINT4                u4HoldCount;       /* No. of LACPDUs transmitted since
                                              HoldWhile was initialised */
      /* Link Speed and Duplexity */
   UINT4                u4LinkSpeed;

   UINT4                u4LinkHighSpeed;

   UINT4                u4LinkDuplex;

   UINT4                u4Mtu;             /* Should be same as the MTU value 
                                            *  of the aggregator
                                            */
   UINT4                u4SameStateCount; /* This variable maintains the
                                             the number of times the Mux state
                                             machine stuck in the same state.
                                           */
   UINT4                u4ErrorDetectCount; /* This variable maintains the
                                               number of times the Error is
                                               detected */
   UINT4                u4RecTrgdCount;    /* This variable maintains the
                                              number of times the recovery
                                              mechanism's are triggered */



   UINT4                u4RestoreMtu;      /* Value that should be restored
                                            * when the port goes out of 
                                            * the aggregation
                                            */

   UINT4                u4DefaultedStateThreshold;  /* This variable maintains 
                                                       the maximum number of times
                                                       a port in Defaulted State can
                                                       undergo error recovery */
   UINT4                u4DefaultedStateRecTrgrdCount; /* This variable maintains the
                                                       number of times the recovery
                                                       mechanism's are triggered 
                                                       for a defaulted state port*/
   UINT4                u4HardwareFailureRecThreshold; /* This variable maintains the
                                                         maximum number of times a port
                                                         can undergo error recovery 
                                                         after hardware failure */
   UINT4                u4HardwareFailureRecTrgrdCount; /* This variable maintains the
                                                           number of times the recovery
                                                           mechanism is triggered after
                                                           hardware failure */
   UINT4                u4SameStateRecThreshold;        /* This variable maintains the
                                                           maximum number of times a port
                                                           stays in the same before 
                                                           undergoing recovery */
   UINT4                u4UpInBundleCount;              /* This variable tracks the 
                                                           number of times port goes to 
                                                           Up-In-Bundle state */
   UINT4                u4DownInBundleCount;            /* This variable tracks the 
                                                           number of times port goes to
                                                           Down-in-Bundle state */
   UINT4                u4BundStChgTime;                /* This variable specifies the
                                                           timestamp when port bundle state
                                                           changes */
   UINT4                u4ErrStateDetTime;              /* variable specifies timestamp
                                                           when the port goes to error 
                                                           detected state */
   UINT4                u4ErrStateRecTime;              /* Variable specifies timestamp
                                                           when the port goes to error
                                                           triggered state */
   UINT4                u4DefStateRecTime;              /* This specifies the time of 
                                                           last recovery to overcome 
                                                           from the defaulted state */
   UINT4                u4SameStateRecTime;             /* This specifies the time of
                                                           last recovery to overcome
                                                           same state*/
   UINT4                u4HwFailRecTime;                /* This variable specifies the
                                                           time of last recovery to overcome
                                                           h/w failure */
 
   INT4                 i4PauseAdminMode;  /* Value of the individual port of 
                                              the aggregator */

   INT4                 i4RestorePauseAdminMode;  /* Value that should be 
                                                     restored when the port
                                                     goes out of the 
                                                     aggregator */
   
   /* Physical Port Information */
   UINT2                u2PortIndex;        /* Port Index */

   UINT2                u2SyncUp;
   /* Physical Port Status */
   UINT1                u1PortOperStatus;    /* CFA oper status UP / DOWN */
   UINT1                u1AggOperStatus;     /* Oper status of port 
                                              * as individual 
                                              * when not attached to aggregator
                                              */
   UINT1                u1TimerCount;
   UINT1                u1SyncState;
  
   UINT1                u1DLAGPeerReady;     /* This variable is set to
                                                 TRUE when Peer moves to
                                                 Collecting 
                                             */  
   UINT1                u1DLAGMasterAck;     
                                            /* confirms whether Add/Delete portlist 
                                             * information received for the result of 
                                             * DLAG sync messages.Based on this
                                             * this variable takes TRUE/FALSE */
   UINT1                au1MacAddr[LA_MAC_ADDRESS_SIZE];
   UINT1                u1RecState;
   UINT1                u1PortDownReason;
   UINT1                u1RecoveryTmrReason; /* This indicates the reason
                                                in which recovery timer is
                                                started. This can be
                                                1) Port moved to Defaulted state
                                                2) Port Actor Key is 0 because of
                                                H/w failure.
                                                3) Port stuck in a same state
                                                for longer duration.
                                              */
   UINT1            u1InPassiveDefState;      /*A flag to indicate that the 
                                                port is in LA passive and has 
                                                reached the recovery threshold*/
   BOOL1                b1PortMoved;           /*The variable is set to TRUE if port is moved.
                                                 The variable is set to FALSE,when 
                                                 INITIALISE state is called*/
    UINT1               au1Pad[3];
};


/*****************************************************************************/
/* tLaLacAggConfigEntry                                                      */
/*                                                                           */ 
/* This structure contains the Link Aggregation Control configuration        */
/* parameters for each Aggregator.                                           */
/*****************************************************************************/
struct  LaLacAggConfigEntry {
   
   tLaSystem       ActorSystem; /* Read/Writable */
   tLaSystem       PartnerSystem; /* Read only */
   tLaTicks                CollectorMaxDelay; /* Read/Writable */
   tLaLinkSelectPolicy     LinkSelectPolicy;
   UINT4                   u4LinkSelectPolicyBitList;
   UINT2                   u2ActorAdminKey; /* Read/Writable */
   tLaMacAddr              AggMacAddr; /* Read only */
   UINT2                   u2ActorOperKey; /* Read only */
   UINT2                   u2PartnerOperKey; /* Read only */
   UINT2                   u2AggIndex; /* Read only index */
   UINT1                   AggOrIndividual; /* Read only */
                                            /* Value TRUE means - 
                                               The port attached
                                               to this aggregator is
                                               capable of 
                                               Aggregation */
   UINT1                   u1Pad;
   
};


/*****************************************************************************/
/* tLaLacAggIfEntry                                                          */
/*                                                                           */ 
/* This structure contains the Interface Parameters for each Aggregator.     */
/*****************************************************************************/
struct LaLacAggIfEntry {
   tLaSllNode          NextAggIfEntry;
   INT1               ai1IfName[LA_MAX_STR_LENGTH];
   INT1               ai1IfDescr[LA_MAX_STR_LENGTH];
   INT4                i4AggInOctets;
   INT4                i4AggInDiscards;
   INT4                i4AggInErrors;
   INT4                i4AggInUnknownProtos;
   INT4                i4AggInUcastPkts;
   INT4                i4AggInFrames;
   INT4                i4AggOutOctets;
   INT4                i4AggOutDiscards;
   INT4                i4AggOutErrors;
   INT4                i4AggOutUcastPkts;
   INT4                i4AggOutFrames;
   INT4                i4AggInMcastPkts;
   INT4                i4AggInBcastPkts;
   INT4                i4AggOutMcastPkts;
   INT4                i4AggOutBcastPkts;
};

typedef struct LaDLAGTxInfo {

    tMacAddr LaDLAGSlaveMacAddr;
             /* This object is used to publish the result of Master slave
              * selection, this object contains the MAC address of the
              * D-LAG node elected as Slave.
              * If Mac-address of Node Receiving the Result sync event-update
              * message is equal to the address in this object, then that node 
              * should play the role of Slave, if not same then that node
              * can play the role of either backup-master/slave based on 
              * the value contained in the field next to this in DSU */

    tMacAddr LaDLAGBackupMasterMacAddr;
             /* This object is used to publish the result of Master slave
              * selection, this object contains the MAC address of the
              * D-LAG node elected as Back-up-master.
              * If Mac-address of Node Receiving the Result sync event-update
              * message is equal to the address in this object, then that node 
              * should play the role of backup-master, if not same then that can 
              * play the role of either master/slave, based on mac-address of
              * master contained in DSU */

    UINT4    u4PortIndex;
             /* This variable is used to send the index of the port which is
              * removed from the port channel */

    UINT4    u4SlotId;
             /* This variable is used to identify the Slot ID of remote Node */
    UINT1    u1DSUType;
             /* Type of the DSU, if zero then periodic-sync PDU,
              * else event-update PDU, subtypes of event-update PDU will
              * be encoded in the same field. */

   UINT1    au1Reserved[3];

} tLaDLAGTxInfo;

/************************************************************
    Consolidated Best properties info

************************************************************/


typedef struct LaDLAGSpeedPropInfo {

    UINT4           u4Mtu; 
                                /* Best port-channel Mtu out of All channels*/
    UINT4           u4ConsRefSpeed;
                                 /* Best consolidated Ref Speed */
    UINT4           u4ConsRefHighSpeed;
                                 /*Best consolidated Ref High Speed */
    tLaMacAddr      BestInfoMac;
                                 /* Best info's Switch Mac address */
    UINT1           au1Reserved[2];

} tLaDLAGSpeedPropInfo;

/*********************************************************
    Consolidated info

**********************************************************/
typedef struct LaDLAGConsInfo {

    tLaDLAGSpeedPropInfo LaDLAGCompProp;
                                /* Speed properties for comparing */

    tLaRBTree   LaDLAGPortList;
                                /* This RB tree contains all the port information
                                 * that are participating in Distributed LAG. */

    tLaRBTree   LaDLAGConsolidatedList;

                                /* This RB Tree is used for sorting Add &
                                   standby ports to decide the best links

                                   based on priority.
                                   Entries are sorted in priority order */
} tLaDLAGConsInfo;

typedef struct LaDLAGConsPortEntry {

    tLaRBNodeEmbd       LaDLAGConsPortInfo;
                             /* RB tree Node for Consolidated port entry */

    tLaRBNodeEmbd       LaDLAGPortInfo;
                            /* RB tree Node for Port Entry*/

    UINT4               u4PortIndex;         /* Port Index  */
    UINT4               u4SlotId;
    UINT4               u4InstanceId;
    UINT2               u2Priority;          /* Port Priority */

    UINT1               u1RecentlyChanged;   /* Used to check whether
                                                it is recently updated */

    UINT1               u1PortStateFlag; /* Specifies the Port state depending on the
                                            * Port State Value 
                                            * LA_AA_DLAG_PORT_NOT_IN_ANY_LIST      0
                                            * LA_AA_DLAG_PORT_IN_ADD               1
                                            * LA_AA_DLAG_PORT_IN_DELETE            2
                                            * LA_AA_DLAG_PORT_IN_STANDBY           3
                                            * LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY    4
                                            */
   UINT1               u1NodeState; /*Used to check whether it is master port or slave port */                
    BOOL1               b1LaggDistType;
    UINT1               au1Pad[2];
} tLaDLAGConsPortEntry;



struct LaLacAggEntry {
   tLaSllNode              NextNode;

   tLaSll                  ConfigPortSll;
   
   tLaLacAggConfigEntry    AggConfigEntry;
   
   tLaLacAggIfEntry        AggIfEntry;

   tLaAppTmrNode           LaMarkerRateTmr;
   
   tLaSysTime              TimeOfLastOperChange;
   
   tLaLacpMode             LaLacpMode;      /* The Aggregator runs in either 
           of the 3 modes - ENABLED,
           DISABLED or MANUAL */
   tLaSysTime              AggTimeOfLastOperChange;
  
   tLaPortList             LaActivePorts /* LaAggPortList */;
                                 /* Each array entry is a complete set of ports
                                  * currently associated with this Aggregator.
                                  * Each bit set in this list represents an
                                  * Actor Port member of this Link Aggregation.
                                  * This is the PortListTable of the MIB.
                                  */
                                   
   tLaSystem             DLAGSystem;
                         /* Holds the D-LAG common system Id and
                          * common system priority to be used for 
                          * communicating with the peer node when
                          * D-LAG feature is enabled.
                          */

   tLaAppTmrNode         DLAGPeriodicSyncTmr;
                         /* Timer entry for D-LAG periodic-sync timer
                          * used to control the D-LAG periodic-sync PDU's 
                          * transmission interval */

   tLaAppTmrNode         MCLAGPeriodicSyncTmr;
                         /* Timer entry for D-LAG periodic-sync timer
                          * used to control the D-LAG periodic-sync PDU's 
                          * transmission interval */
   tLaAppTmrNode         DLAGMSSelectionWaitTmr;
                         /* Timer entry for D-LAG Master slave selection
                          * wait timer which is used as delay timer to
                          * delay the execution of Master-slave-selection
                          * algorithm. This timer is started at the start
                          * of Master-slave-selection process and when this
                          * timer is running, D-LAG nodes participating
                          * in distribution exchanges information needed
                          * for election via event-update messages and
                          * Master-slave-selection algorithm is called
                          * immediately on  expiry of this timer to
                          * elect master/slave.
                          * This timer is started only when redundancy feature
                          * is enabled.
                          */

   tLaDLAGRemoteAggEntry  *pDLAGRemoteAggEntry; 
                         /* Head Node Pointer to the list maintained to store
                          * the port channel info of each D-LAG node. */

   tLaDLAGConsInfo       LaDLAGConsInfoTable;
                        /* Consolidated data structure which is used by 
                         * only DLAG Master when redundancy off 
                         * to decide the consolidated table */ 

   tLaRBTree               LaDLAGRemoteAggInfoTree;
                         /* This is an RBTree which contains 
                          * pointers to the pDLAGRemoteAggEntry maintained */

   tLaTrapEnable           LaLinkUpDownTrapEnable; /* To enable or disable trap
                                                    notification */
   tLaHwAggStatus          HwAggStatus;    /* Flag indicates, the status of
                                               Agg in HW, i.e, whether it is 
                                               present in HW or not */
   tLaBoolean            bLaDLAGIsDesignatedElector;
                         /* Holds the truth value about 
                          * "Am I the designated elector?"
                          * LA_TRUE - if this node has autority to run Master
                          * slave selection algorithm.
                          * LA_FALSE - if this node does not have autority to
                          * run Master slave selection algorithm. */


   tLaBoolean              RxEnabled;
   
   tLaBoolean              TxEnabled;

   tLaBoolean             UsingPartnerAdminInfo;

   tLaBoolean              bDynPortRealloc; /* Set to TRUE when ports can be
                                               dynamically reallocated to a
                                               different aggregator */
   tLaBoolean              bHlIndicated;    /* When the first becomes oper up,
                                               HL modules are given operup 
                                               indication and this flags is 
                                               set. By this higher layer 
                                               indications are avoided, when 
                                               further ports comes up */

   UINT4                   u4PortSpeed;     /* Reference Speed of the ports 
                                               attached to this aggregator.
                                               Port with this speed only
                                               can attach to this aggregator.
                                             */
   UINT4                   u4PortHighSpeed;  /*Reference High Speed of the port*/

   UINT4                   u4AggSpeed;      /* Combined Speed of the 
                                               distributing ports attached 
                                               to this aggregator 
                                   */
   UINT4                   u4AggHighSpeed;  /*Combined HighSpeed of the 
                                              distributing ports of the 
                                              aggregator*/

   UINT4                   u4Mtu;           /* MTU value of the aggregator.
                                             * Only ports with same MTU value
                                             * can be member of the aggregation.
                                             */ 
   UINT4                   u4TransId;

   UINT4                 u4DLAGDistributingIfIndex;
                         /* This holds the configured Distributing port
                          * interface index for a D-LAG node, on which
                          * periodic-sync/event-update messages will be
                          * sent/received. */
   tLaPortList            DLAGDistributingPortList;
                         /* This holds the configured Distributing port
                          * interface list for a D-LAG node, on which
                          * periodic-sync/event-update messages will be
                          * sent/received. */
   tLaPortList            DLAGDistributingOperPortList;
                         /* This holds the operational Distributing port
                          * list*/
   UINT4                 u4DLAGDistributingPortListCount;
                          /* This object is added for efficiently scanning
                           * the port list*/

   UINT4                 u4DLAGPeriodicSyncTime;
                         /* Configured D-LAG periodic sync timer value */

   UINT4                 u4MCLAGPeriodicSyncTime;
                         /* Configured D-LAG periodic sync timer value */

   UINT4                 u4DLAGMSSelectionWaitTime;
                         /* Configured D-LAG master slave selection wait
                          * timer value */

   UINT4                 u4DLAGPeriodicSyncPduTxCount;
                         /* statistics counter for Periodic-sync pdu's
                          * sent on Distributing port */

   UINT4                 u4DLAGPeriodicSyncPduRxCount;
                         /* statistics counter for Periodic-sync pdu's
                          * received on Distributing port */

   UINT4                 u4DLAGEventUpdatePduTxCount;
                         /* statistics counter for event-update pdu's
                          * sent on Distributing port */

   UINT4                 u4DLAGEventUpdatePduRxCount;
                         /* statistics counter for event-update pdu's
                          * received on Distributing port */

   UINT4                 u4DLAGElectedAsMasterCount;
                         /* statistics counter to count number of times 
                          * the port-channel has been elected as master */

   UINT4                 u4DLAGElectedAsSlaveCount;
                         /* statistics counter to count number of times 
                          * the port-channel has been elected as slave */

   UINT4                 u4DLAGTrapTxCount;
                         /* statistics counter for SNMP Trap pdu's
                          * sent from port-channel */

   UINT4                 u4DLAGMaxKeepAliveCount;
                         /* Maximum keep alive count for a D-LAG node,
                          * if keep alive count maintained for any of the
                          * D-LAG node reaches this Max value then that
                          * Remote D-LAG node will be treated as
                          * operationally down */

   INT4                    i4PauseAdminMode;/* Value of the aggregator */
   INT4                    i4DownReason;       /* This vaiable specifies reason
                                              for port-channel down */

   tMacAddr             LaDLAGElector;
                         /* Holds the current designated elector Mac address
                          * when D-LAG redudancy feature is enabled */
   UINT2                   u2AggIndex;
   
   UINT2                   u2FirstPort;
   
   UINT1                   u1MaxPortsToAttach;
   
   UINT1                   u1ConfigPortCount;
                                         /* Number of ports configured
                                          * to this aggregator.
                                          */
   UINT1                   u1SelectedPortCount;  
                                         /* Count of all ports that is  
                                          * having Selected = SELECTED
                                          */
   UINT1                   u1AttachedPortCount;
   
   UINT1                   u1DistributingPortCount;

   UINT1                 u1AggToBeCreated; /* Flag which indicates whether
                                                a new agg is to be created 
                                    in the hw. It is set when 
                                    a port is selected 
                                                as an aggregator for the 
                                    first time.
                                             */
   UINT1                   u1AggAdminStatus;
   
   UINT1                   u1AggOperStatus;
   
   UINT1                   u1MarkerRateCount;   

   UINT1                   u1IsAggAudited;

   UINT1                 u1DLAGStatus;
                         /* Represents the current D-LAG status,
                          * whether D-LAG feature is enabled/disabled */
                           
   UINT1                 u1MCLAGStatus;
                         /* Represents the current MC-LAG status,
                          * whether MC-LAG feature is enabled/disabled */
   UINT1                 u1DLAGRedundancy;
                         /* This is used to identify whether redundancy
                          * feature is enabled/disabled */
   UINT1                 u1DLAGRolePlayed;
                         /* This variable is used only when redundancy
                          * feature is enabled in a D-LAG node,
                          * This holds the current role played by the D-LAG
                          * node master/slave */
   UINT1                 u1MCLAGRolePlayed;
                         /* This variable is used only when redundancy
                          * feature is enabled in a D-LAG node,
                          * This holds the current role played by the D-LAG
                          * node master/slave */

  BOOL1                 bIsMCLAGSystemIdConfigured;
                        /* This variable is used to check whether System ID is
                         * configured explicitly by the user*/                         

  UINT1                 u1PortDisabledCounter;
                         /*This variable is used to count no of ports in 
                          *the port-channel which is in PORT_DISABLED      
                          *state.It is used for optimizing port_moved 
                          *implementation*/

  UINT1                u1PortChannelClearCounters;
  /*This variable is used to clear port channel 
   * level counters when ENABLED*/ 
  UINT4                   u4OperUpCount;       /* This variable maintains number
                                                  of times a portchannel moves to
                                                  operational up state */
  UINT4                   u4OperDownCount;     /* This variable maintains number
                                                  of times a portchannel moves to
                                                  operational down state */
  UINT4                   u4StandByPortCount;  /* Count of all ports that is in
                                                  Hot Standby state */

};


/* Added to hold the Remote Node port channel info.
 * This entry will be added for each remote D-LAG node present */
struct LaDLAGRemoteAggEntry {
   tLaRBNodeEmbd           LaDLAGRemoteAggNode;
                           /* RB Node for LaDLAGRemoteAggEntry  */

   tLaSll                  LaDLAGRemotePortList;
                           /* This is a singly linked list which contains 
                            * pointers to the pRemotePortEntry maintained */

   tLaLacAggEntry          *pAggEntry;
                           /* Backward reference pointer to pAggEntry */

   tLaSystem               DLAGRemoteSystem;
                           /* D-LAG System Id and System priority of the
                            * remote D-LAG node */

   tLaDLAGRemotePortEntry  *pRemotePortEntry; 
                           /* Head node pointer to the remote port list,
                            * which stores the information of all ports
                            * configured for that remote D-LAG node */
   UINT4                   u4DLAGKeepAliveCount;
                           /* Current Keep alive count */

   UINT1                   u1DLAGRolePlayed;
                           /* Read-only variable to hold the current role
                            * played by the remote D-LAG node.
                            * This Variable can take the values
                            * LA_DLAG_SYSTEM_ROLE_MASTER/LA_DLAG_SYSTEM_ROLE_SLAVE
                            * or LA_DLAG_SYSTEM_ROLE_NONE */
   BOOL1                   u1DLAGElector;
                           /* This information shows that this remote node
                            * Performs the Elector functionality */
   BOOL1                   u1RedEnabled;
                           /* Remote Node is enabled with
                            * Redundancy */

   UINT1                   u1UpdateRequired;
                           /* This variable is set to LA_TRUE when
                              MTU/Speed is modified.
                              So that consolidation logic is triggered */

   UINT4                   u4PortSpeed;
                           /* Reference Speed of Remote Port of channel */

   UINT4                   u4PortHighSpeed;
                           /* Reference High of Speed of Remote port channel */ 

   UINT4                   u4Mtu;
                           /* MTU of remote port-channel */
   UINT4                   u4SlotId;
   UINT4                   u4InstanceId;
   UINT1                   u1AdminStatus;
   UINT1                   au1Reserved[3];
};

/* Added to hold the port list information of all remote D-LAG nodes
 * This entry will be added for each port configured for the remote
 * D-LAG node */
struct LaDLAGRemotePortEntry {
  tLaSllNode               NextNode;
                           /* Linked list Next pointer */
  UINT4                    u4PortIndex;
  UINT4                    u4SlotId;
                           /* Slot Id of the remote node */
  UINT4                    u4InstanceId;
                           /* Interface index of remote
                            * D-LAG node member port */
  UINT4                    u4SlotIndex;
                           /* Slot index of remote MC-LAG node */
  UINT1                    u1BundleState;
                           /* Remote Node member port
                            * bundle state */
  UINT1                    u1SyncState;
                           /* Indicates the status of the sync messages
                            * sent from the remote nodes participating 
                            * in Distributed Link Aggregation */
  UINT2                    u2Priority;
                           /* Port priority of Remote Port */
  UINT1                    u1UpdateRequired;

                           /* This variable is set to LA_TRUE when
                              Bundle/Priority/Sync State is modified.
                              So that consolidation logic is triggered
                            */
  UINT1                      au1Reserved[3];
};

/* This holds the information of Active Active DLAG 
   configuration details which is included gLaGlobalInfo
*/

typedef struct LaDLAGGlobalInfo {

  UINT4                 u4GlobalDLAGPeriodicSyncTime;
                              /* Global Periodic sync time */

  UINT4                 u4GlobalMCLAGPeriodicSyncTime;
                              /* Global Periodic sync time */

  tLaSystem             GlobalDLAGCommonSystem;
                         /* Holds the D-LAG common system Id and
                          * common system priority to be used for
                          * communicating with the peer node when
                          * D-LAG feature is enabled.
                          */

  UINT4                 u4GlobalDLAGDistributingIfIndex;
                         /* This holds the configured Distributing port
                          * interface index for a D-LAG node, on which
                          * periodic-sync/event-update messages will be
                          * sent/received. */

  tLaPortList            GlobalDLAGDistributingPortList;
                         /* This holds the configured Distributing port
                          * interface list for a D-LAG node, on which
                          * periodic-sync/event-update messages will be
                          * sent/received. */

  tLaPortList            GlobalDLAGDistributingOperPortList;
                         /* This holds the Global operational Distributing port
                          * list*/

  UINT4                  u4GlobalDLAGDistributingPortListCount;
                          /* This object is added for efficiently scanning
                           * the port list*/

  tLaAppTmrNode          GlobalDLAGPeriodicSyncTmr;
                        
                         /* Timer entry for Global D-LAG periodic timer which 
                          * used to control the Global D-LAG periodic sync
                          * PDU's trasnmission interval 
                          */

  tLaAppTmrNode          GlobalMCLAGPeriodicSyncTmr;
                        
                         /* Timer entry for Global D-LAG periodic timer which 
                          * used to control the Global D-LAG periodic sync
                          * PDU's trasnmission interval 
                          */
} tLaDLAGGlobalInfo;

/* State Machine Entry type definitions */
typedef INT4 (* LA_MUX_FUNC)(tLaLacPortEntry *);

/* MUX Machine Structure */

typedef struct LaMuxSmEntry {
      LA_MUX_FUNC pAction;
} tLaMuxSmEntry;

/* Application CallBack Structure */

typedef union LaCallBackEntry {

    INT4 (*pLaPktRxCallBack) (UINT2, UINT2 *);
    INT4 (*pLaPktTxCallBack) (UINT2, UINT2 *);
    INT4 (*pLaGetDistribPortCallBack) (UINT2, UINT2 *);
    INT4 (*pLaGetActivePortCallBack) (UINT2, UINT2 *);

} tLaCallBackEntry;

typedef struct LaCallBackArgs {
  
    UINT2 u2Port;
    UINT2 u2PortState;
    
}tLaCallBackArgs;



/*****************************************************************************/
/*  tLaGlobalInfo                                                            */
/*                                                                           */
/*  All the global variables used in the Link Aggregation module are         */
/*  contained in this global structure.                                      */
/*****************************************************************************/
typedef struct {
   tLaSll                LaAggList;

   tLaSll                LaDynReallocPortList; /* List of Ports that can be
                                                  dynamically reallocated across
                                                  aggregators */

   tLaHashTable          *pLaCacheTable;

   tLaLacPortEntry       *apLaLacPortEntry[LA_MAX_PORTS];
                                  /* This array contains the pointers to Port
                                   * Entries corresponding to their POrt
                                   * Indices. The Port Index is same as the
                                   * array index */

   tLaDLAGGlobalInfo     GlobalDLAGInfo; 
                                         /* This holds the information
                                            of Common D-LAG parameters used
                                            in Active D-LAG */

   tLaEnable             LaEnabled; /* Link Aggregation Enabled or Disabled */  
   
   tLaSystem             LaSystem;        /* Actor's Default System Id */ 
   
   tLaSystem             LaPartnerSystem; /* Partner's Default System Id */ 
   
   tLaLacPortState       LaPortDefaultState;    /* Actorr's default state */ 
   
   tLaLacPortState       LaPartnerDefaultState; /* Partner's default state */ 

   tLaMemPoolId          LaCachePoolId;
   
   tLaMemPoolId          LaMarkerPoolId;

   tLaMemPoolId          LaDlagRemoteAggPoolId;

   tLaMemPoolId          LaDlagRemotePortPoolId;

   tLaMemPoolId          LaDlagConsPortPoolId;

   tLaMemPoolId          LaPortPoolId;
   
   tLaMemPoolId          LaAggPoolId; 

   tLaTimerListId        LaTmrListId;

   tLaBoolean            bProtocolLocked;

   tLaSysTime            LaTablesLastChanged;
   
   tLaMuxSmEntry         MuxMachine [LA_MUX_STATE_COUNT][LA_MUX_EVENT_COUNT];
   
   INT4                  i4SysLogId;
   
   UINT4                 u4TraceOption;

   UINT4                 u4FreeAggs;

   UINT1                 u1LaAdminStatus; /* Stores the Configured LA status 
                                           * before the GO_ACTIVE event is 
                                           * received from RM. */
   UINT1                 gau1DLAGPdu[LA_MAX_DLAG_PDU_SIZE];

   BOOL1                 bIsMCLAGGlobalSysIDConfigured;
                         /* This variable is used to check whether System ID is
                          * configured explicitly by the user*/

   UINT1                 au1Reserved [2];

}tLaGlobalInfo;


typedef struct {
   tLaMacAddr    DestMacAddr;
   tLaMacAddr    SrcMacAddr;
   UINT2         u2Type;
   UINT1         u1Subtype;
   UINT1         u1TlvType;
   UINT2         u2RequesterPort;
   UINT2         u2Reserved;
   UINT4         u4RequesterTransId;
}tLaAggMarkerPduInfo;


/* A hash table is maintained for the cache entries */
typedef struct { 
   tLaHashNode      NextCacheEntry;
   tLaLacPortEntry  *pLaLacPortEntry;
   tLaAppTmrNode    LaCacheTmr;
   tLaSysTime       LastAccessTime;
   tLaBoolean       bMarkerStatus;
   tLaMacAddr       SrcMacAddr;
   UINT2            u2PortIndex;
   tLaMacAddr       DestMacAddr;
   UINT2            u2AggIndex;
   tVlanId         VlanId;
   UINT1     au1Reserved[2];      
   UINT4   u4Isid;
   UINT4            u4MplsVcLabel;
   UINT4            u4MplsTunnelLabel;
   UINT4            u4MplsVcTunnelLabel;
}tLaCacheEntry;


typedef struct {
   tLaMacAddr    LaDestMacAddr;
   tLaMacAddr    LaSourceMacAddr;
   UINT2         u2SlowProtoType;
   UINT1         u1PduType;
   UINT1         u1VersionNum;
}tLaPduHeader;


typedef struct {
   tLaPduHeader             LaLacpduHeader;
   tLaLacIfInfo             LaLacActorInfo;
   tLaLacIfInfo             LaLacPartnerInfo;
   tLaTicks                   CollectorMaxDelay;
}tLaLacpduInfo;

typedef struct LaCtrlMesg {

    UINT4 u4MesgType; /* Takes values - LA_CONTROL_FRAME */
    struct {
        void      *pFrame;
        UINT2     u2PortIndex;
        UINT2     u2Protocol;
        UINT1     u1PktType;
        UINT1     u1Pad1;
        UINT2     u2Length;
    }CtrlFrame;
    
}tLaCtrlMesg;

#ifdef MBSM_WANTED
typedef  struct {
    tMbsmProtoMsg *pMbsmProtoMsg;
}tMbsmCardUpdate;
#endif

#ifdef L2RED_WANTED
/* Peer node Id. */
typedef VOID * tLaRedPeerId;

/* To handle message/events given by redundancy manager. */
typedef struct {
    tRmMsg        *pFrame;     /* Message given by RM module. */
    tLaRedPeerId   PeerId;
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tLaRedRmFrame;
#endif
#ifdef ICCH_WANTED
typedef struct {
    tCRU_BUF_CHAIN_HEADER     *pFrame;     /* Message given by RM module. */
    VOID *          PeerId;
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tLaIcchFrame;

#endif /* ICCH_WANTED */
#ifdef NPAPI_WANTED
typedef  struct {
    UINT4           u4NpCallId;
    unAsyncNpapi    LaAsyncNpCbParams;
}tLaNpCbMsg;
#endif
typedef struct LaIntfMesg {
    UINT2 u2MesgType; 
    UINT2 u2PortIndex;
#if defined (NPAPI_WANTED) || (MBSM_WANTED) || defined(L2RED_WANTED) || defined (ICCH_WANTED)
    union {
#ifdef NPAPI_WANTED
        tLaNpCbMsg  LaNpCbMsg;
#endif
#ifdef MBSM_WANTED
        tMbsmCardUpdate MbsmCardUpdate;
#endif
#ifdef L2RED_WANTED
        tLaRedRmFrame   RmFrame;
#endif
#ifdef ICCH_WANTED
        tLaIcchFrame   IcchFrame;
#endif
    }uLaIntfMsg;
#endif
}tLaIntfMesg;

typedef struct
{
   UINT4    u4NpCallId;
   UINT2    u2AggIndex;
   UINT2    u2PortNumber;
   UINT2    u2Inst; 
   UINT1    u1SelectionPolicy;
   UINT1    u1StpState;
}tLaNpTrapLogInfo;


#endif /*_LATDFS_H */
