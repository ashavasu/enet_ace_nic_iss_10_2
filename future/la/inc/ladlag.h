/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: ladlag.h,v 1.7 2017/11/08 13:19:03 siva Exp $                        */
/* License Aricent Inc., 2001-2002                                           */
/*****************************************************************************/
/*    FILE  NAME            : ladlag.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Distributed Link Aggregation                   */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 15 Feb 2012                                    */
/*    AUTHOR                : DLAG team                                      */
/*    DESCRIPTION           : This file contains function prototypes for     */
/*                            the functions added to support Distributed     */
/*                            Link Aggregation and Multi chassi Link         */
/*                            Aggregation feature.                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    15 Feb 2012/          Initial Create.                          */
/*            DLAG Team                                                      */
/*---------------------------------------------------------------------------*/


#ifndef _LADLAG_H
#define _LADLAG_H

INT4 LaDLAGInit (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGDeInit (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGSetActorSystemIDToDLAG (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGResetActorSystemID (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGChangeActorSystemPriorityToDLAG (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGResetActorSystemPriority (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGCreateRemoteAggregator (tMacAddr MacAddr,
                                   tLaDLAGRemoteAggEntry **ppRetRemoteAggEntry);
INT4 LaDLAGCreateRemotePortEntry (UINT4 u4RemotePortIndex,
                                  tLaDLAGRemotePortEntry **ppRetRemotePortEntry);
INT4 LaDLAGDeleteRemoteAggregator (tLaLacAggEntry *pAggEntry,
                                   tLaDLAGRemoteAggEntry *pRemAggEntry);
INT4 LaDLAGDeleteRemotePort (tLaDLAGRemoteAggEntry *pRemAggEntry,
                             tLaDLAGRemotePortEntry *pRemPortEntry);
INT4
LaDLAGUpdateRemoteDLAGNodeList (tLaLacAggEntry *pAggEntry, UINT1 *pu1LinearBuf,
                                INT4 i4SllAction);
INT4
LaDLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry *pRemoteAggEntry,
                            UINT1 *pu1LinearBuf, INT4 i4SllAction);
INT4
LaDLAGDeleteAllRemoteAggEntries (tLaLacAggEntry *pAggEntry);
INT4
LaDLAGDeleteAllRemotePortEntries (tLaDLAGRemoteAggEntry *pRemAggEntry);
INT4 LaDLAGTxPeriodicSyncPdu (tLaLacAggEntry *pAggEntry);
INT4 LaDLAGTxEventUpdatePdu (tLaLacAggEntry *pAggEntry,
                             tLaDLAGTxInfo *pLaDLAGTxInfo);
INT4 LaDLAGTxDSU (tLaLacAggEntry *pAggEntry, tLaDLAGTxInfo *pLaDLAGTxInfo);
INT4
LaDLAGGetRemoteAggEntryBasedOnMac (tLaMacAddr LaDLAGRemoteNodeMacAddr,
                                   tLaLacAggEntry *pAggEntry,
                                   tLaDLAGRemoteAggEntry **ppRemoteAggEntry);
INT4
LaDLAGGetRemotePortEntry (UINT4 u4RemotePortIndex,
                          tLaDLAGRemoteAggEntry *pRemoteAggEntry,
                          tLaDLAGRemotePortEntry **ppRemotePortEntry);
INT4
LaDLAGGetNextRemoteAggEntry (tLaLacAggEntry *pAggEntry,
                             tLaDLAGRemoteAggEntry *pRemoteAggEntry,
                             tLaDLAGRemoteAggEntry **pNextRemoteAggEntry);
INT4
LaDLAGGetNextRemotePortEntry (tLaDLAGRemoteAggEntry *pRemoteAggEntry,
                              tLaDLAGRemotePortEntry *pRemotePortEntry,
                              tLaDLAGRemotePortEntry **pNextRemotePortEntry);
INT4 LaDLAGKeepAlive (tLaLacAggEntry *pAggEntry);
INT4
LaDLAGSnmpLowValidateRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex, 
                                             tMacAddr FsLaDlagRemotePortChannelSysID);
INT4
LaDLAGSnmpLowGetFirstValidRemotePortChannelIndex (INT4 *pi4FsLaPortChannelIfIndex, 
                                                  tMacAddr * pFsLaDLAGRemotePortChannelSystemID);
INT4
LaDLAGSnmpLowGetNextValidRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                                 INT4 *pi4NextFsLaPortChannelIfIndex,
                                                 tMacAddr FsLaDlagRemotePortChannelSysID,
                                                 tMacAddr * pNextFsLaDLAGRemotePortChannelSystemID);
INT4
LaDLAGSnmpLowValidateRemotePortIndex (INT4 i4FsLaPortChannelIfIndex, 
                                      tMacAddr FsLaDlagRemotePortChannelSysID,
                                      INT4 i4FsLaDLAGRemotePortIndex);
INT4
LaDLAGSnmpLowGetFirstValidRemotePortIndex (INT4 *pi4FsLaPortChannelIfIndex, 
                                           tMacAddr *pFsLaDLAGRemotePortChannelSystemID,
                                           INT4 *pi4FsLaDLAGRemotePortIndex);
INT4
LaDLAGSnmpLowGetNextValidRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                          INT4 *pi4NextFsLaPortChannelIfIndex,
                                          tMacAddr FsLaDlagRemotePortChannelSysID,
                                          tMacAddr *pNextFsLaDLAGRemotePortChannelSystemID,
                                          INT4 i4FsLaDLAGRemotePortIndex,
                                          INT4 *pi4NextFsLaDLAGRemotePortIndex);
INT4
LaDLAGAddToRemoteAggTable (tLaLacAggEntry *pAggEntry,
                           tLaDLAGRemoteAggEntry *pRemoteAggEntry);
VOID
LaDLAGAddToRemotePortTable (tLaDLAGRemoteAggEntry *pRemoteAggEntry,
                            tLaDLAGRemotePortEntry *pRemotePortEntry);
VOID
LaDLAGDeleteFromRemoteAggTable (tLaLacAggEntry *pAggEntry,
                                tLaDLAGRemoteAggEntry *pRemoteAggEntry);
VOID
LaDLAGDeleteFromRemotePortTable (tLaDLAGRemoteAggEntry  *pRemoteAggEntry,
                                 tLaDLAGRemotePortEntry *pRemotePortEntry);
INT4
LaDLAGFormDSU (tLaLacAggEntry *pAggEntry, UINT1 *pu1LinearBuf,
               tLaDLAGTxInfo *pLaDLAGTxInfo);
INT4 LaDLAGProcessDLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf);
INT4
LaDLAGInitForcefulLacpTx (tLaLacAggEntry *pAggEntry, tLaBoolean LaSynchronization);
INT4 LaDLAGMSSelectionAlgorithm (tLaLacAggEntry *pAggEntry, UINT1 u1PrevRolePlayed);
INT4
LaDLAGGetSysInSyncPortCount (tLaLacAggEntry *pAggEntry, UINT4 *u4SysPortInSyncCount);
INT4
LaDLAGGetRemSysInSyncPortCount (tLaDLAGRemoteAggEntry *pRemoteAggEntry,
                                UINT4 *u4RemInSyncPortCount);
INT4
LaDLAGApplyElectionResults (tLaLacAggEntry *pAggEntry, UINT1 *pu1LinearBuf);
VOID LaDLAGScanLaListAndInitDLAG (VOID);
VOID LaDLAGScanLaListAndDeInitDLAG (VOID);
INT4
LaDLAGTriggerMSSelectionProcess (tLaLacAggEntry        *pAggEntry,
                                 tMacAddr               PeerMacAddr,
                                 tLaBoolean             IsRemoteElector,
                                 UINT1                  u1Trigger);
INT4 LaDLAGChangeAttachedPortActorSysLAGIDToDLAG (tLaLacPortEntry *pPortEntry);
INT4 LaDLAGResetPortActorSysLAGID (tLaLacPortEntry *pPortEntry);
VOID LaDLAGElectorSelectionProcess(tLaLacAggEntry * pAggEntry);
#endif  /* _LADLAG_H */
