/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: stdlawr.h,v 1.7 2011/08/24 06:08:41 siva Exp $
 *
 * Description: This file contains prototypes of all functions
 *              in Link Aggregation module.
 *
 *****************************************************************************/


#ifndef _STDLAWR_H
#define _STDLAWR_H

VOID RegisterSTDLA(VOID);
INT4 Dot3adTablesLastChangedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3adAggTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3adAggIndexGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggMACAddressGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorSystemPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorSystemIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggAggregateOrIndividualGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorAdminKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorOperKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPartnerSystemIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPartnerSystemPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPartnerOperKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggCollectorMaxDelayGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorSystemPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorAdminKeySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggCollectorMaxDelaySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorSystemPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggActorAdminKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggCollectorMaxDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDot3adAggPortListTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3adAggPortListPortsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3adAggPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3adAggPortIndexGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorSystemPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorSystemIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorAdminKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorOperKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminSystemPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerOperSystemPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminSystemIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerOperSystemIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerOperKeyGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortSelectedAggIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortAttachedAggIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerOperPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerOperPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorOperStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerOperStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortAggregateOrIndividualGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorSystemPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorAdminKeySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminSystemPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminSystemIDSet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminKeySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminPortSet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorSystemPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorAdminKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminSystemPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminSystemIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortActorAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortPartnerAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);










INT4 GetNextIndexDot3adAggPortStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3adAggPortStatsLACPDUsRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsMarkerPDUsRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsMarkerResponsePDUsRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsUnknownRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsIllegalRxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsLACPDUsTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsMarkerPDUsTxGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortStatsMarkerResponsePDUsTxGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot3adAggPortDebugTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot3adAggPortDebugRxStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugLastRxTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugMuxStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugMuxReasonGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugActorChurnStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugPartnerChurnStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugActorChurnCountGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugPartnerChurnCountGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugActorSyncTransitionCountGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugPartnerSyncTransitionCountGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugActorChangeCountGet(tSnmpIndex *, tRetVal *);
INT4 Dot3adAggPortDebugPartnerChangeCountGet(tSnmpIndex *, tRetVal *);
#endif /* _STDLAWR_H */
