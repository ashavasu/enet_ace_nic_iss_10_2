/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: laprplow.h,v 1.29 2017/10/16 09:40:59 siva Exp $
 *
 *******************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsLaStatus ARG_LIST((INT4 *));

INT1
nmhGetFsLaOperStatus ARG_LIST((INT4 *));

INT1
nmhGetFsLaTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsLaMaxPortsPerPortChannel ARG_LIST((INT4 *));

INT1
nmhGetFsLaMaxPortChannels ARG_LIST((INT4 *));
INT1
nmhGetFsLaActorSystemID ARG_LIST((tMacAddr * ));

INT1
nmhGetFsLaNoPartnerIndep ARG_LIST((INT4 *));

INT1
nmhGetFsLaMCLAGSystemStatus ARG_LIST((INT4 *));

INT1
nmhGetFsLaMCLAGSystemID ARG_LIST((tMacAddr * ));

INT1
nmhGetFsLaMCLAGSystemPriority ARG_LIST((INT4 *));

INT1
nmhGetFsLaMCLAGPeriodicSyncTime ARG_LIST((UINT4 *));

INT1
nmhGetFsLaRecTmrDuration ARG_LIST((UINT4 *));

INT1
nmhGetFsLaRecThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFsLaTotalErrRecCount ARG_LIST((UINT4 *));

INT1                                                                                                                          nmhGetFsLaDefaultedStateThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFsLaHardwareFailureRecThreshold ARG_LIST((UINT4 *));                                                                                                                                                                                                  INT1
nmhGetFsLaSameStateRecThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFsLaRecThresholdExceedAction ARG_LIST((INT4 *));  

INT1
nmhGetFsLaMCLAGClearCounters ARG_LIST((INT4 *));

INT1
nmhGetFsLaClearStatistics ARG_LIST((INT4 *));

INT1
nmhGetFsLaMCLAGSystemControl ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLaSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsLaStatus ARG_LIST((INT4 ));

INT1
nmhSetFsLaTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsLaActorSystemID ARG_LIST((tMacAddr ));

INT1
nmhSetFsLaNoPartnerIndep ARG_LIST((INT4 ));

INT1
nmhSetFsLaMCLAGSystemStatus ARG_LIST((INT4 ));

INT1
nmhSetFsLaMCLAGSystemID ARG_LIST((tMacAddr ));

INT1
nmhSetFsLaMCLAGSystemPriority ARG_LIST((INT4 ));

INT1
nmhSetFsLaMCLAGPeriodicSyncTime ARG_LIST((UINT4 ));

INT1
nmhSetFsLaRecTmrDuration ARG_LIST((UINT4 ));

INT1
nmhSetFsLaRecThreshold ARG_LIST((UINT4 ));

INT1                                                                                                                          nmhSetFsLaDefaultedStateThreshold ARG_LIST((UINT4 ));

INT1
nmhSetFsLaHardwareFailureRecThreshold ARG_LIST((UINT4 ));                                                                                                                                                                                                   INT1
nmhSetFsLaSameStateRecThreshold ARG_LIST((UINT4 )); 

INT1
nmhSetFsLaRecThresholdExceedAction ARG_LIST((INT4 )); 

INT1
nmhSetFsLaMCLAGClearCounters ARG_LIST((INT4 ));

INT1 
nmhSetFsLaClearStatistics ARG_LIST((INT4 ));

INT1
nmhSetFsLaMCLAGSystemControl ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLaSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaActorSystemID ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsLaNoPartnerIndep ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaMCLAGSystemStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaMCLAGSystemID ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsLaMCLAGSystemPriority ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaMCLAGPeriodicSyncTime ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsLaRecTmrDuration ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsLaRecThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1                                                                                                                          nmhTestv2FsLaDefaultedStateThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsLaHardwareFailureRecThreshold ARG_LIST((UINT4 *  ,UINT4 ));                                                                                                                                                                                      INT1
nmhTestv2FsLaSameStateRecThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsLaRecThresholdExceedAction ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaMCLAGClearCounters ARG_LIST((UINT4 *  ,INT4 ));

INT1 
nmhTestv2FsLaClearStatistics ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaMCLAGSystemControl ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLaSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaActorSystemID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaNoPartnerIndep ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaMCLAGSystemStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaMCLAGSystemID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaMCLAGSystemPriority ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaMCLAGPeriodicSyncTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaRecTmrDuration ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaRecThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1                                                                                                                          nmhDepv2FsLaDefaultedStateThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaHardwareFailureRecThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));                                                                                                                                                              INT1
nmhDepv2FsLaSameStateRecThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaRecThresholdExceedAction ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaMCLAGClearCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1 
nmhDepv2FsLaClearStatistics ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaMCLAGSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLaPortChannelTable. */
INT1
nmhValidateIndexInstanceFsLaPortChannelTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLaPortChannelTable  */

INT1
nmhGetFirstIndexFsLaPortChannelTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaPortChannelTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaPortChannelGroup ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelAdminMacAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsLaPortChannelMacSelection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelPortCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelActivePortCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelSelectionPolicy ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDefaultPortIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelMaxPorts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelSelectionPolicyBitList ARG_LIST((INT4 ,INT4 *));

INT1
nmhDepv2FsLaDLAGSystemStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaDLAGSystemID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaDLAGSystemPriority ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaDLAGPeriodicSyncTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaDLAGDistributingPortIndex ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsLaDLAGDistributingPortList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsLaDLAGSystemStatus ARG_LIST((INT4 *));

INT1
nmhGetFsLaDLAGSystemID ARG_LIST((tMacAddr * ));

INT1
nmhGetFsLaDLAGSystemPriority ARG_LIST((INT4 *));

INT1
nmhGetFsLaDLAGPeriodicSyncTime ARG_LIST((UINT4 *));

INT1
nmhGetFsLaDLAGRolePlayed ARG_LIST((INT4 *));

INT1
nmhGetFsLaDLAGDistributingPortIndex ARG_LIST((INT4 *));

INT1
nmhGetFsLaDLAGDistributingPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsLaDLAGSystemStatus ARG_LIST((INT4 ));

INT1
nmhSetFsLaDLAGSystemID ARG_LIST((tMacAddr ));

INT1
nmhSetFsLaDLAGSystemPriority ARG_LIST((INT4 ));

INT1
nmhSetFsLaDLAGPeriodicSyncTime ARG_LIST((UINT4 ));

INT1
nmhSetFsLaDLAGDistributingPortIndex ARG_LIST((INT4 ));

INT1
nmhSetFsLaDLAGDistributingPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLaDLAGSystemStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaDLAGSystemID ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsLaDLAGSystemPriority ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaDLAGPeriodicSyncTime ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsLaDLAGDistributingPortIndex ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsLaDLAGDistributingPortList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsLaPortChannelDLAGDistributingPortIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDLAGSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsLaPortChannelDLAGSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDLAGPeriodicSyncTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGMSSelectionWaitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGRolePlayed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDLAGStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDLAGRedundancy ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDLAGMaxKeepAliveCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelDLAGPeriodicSyncPduTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGPeriodicSyncPduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGEventUpdatePduTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGEventUpdatePduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGElectedAsMasterCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGElectedAsSlaveCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDLAGDistributingPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLaPortChannelUpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDownCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelHotStandByPortsCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelOperChgTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelDownReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhSetFsLaPortChannelDLAGDistributingPortIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelDLAGSystemID ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsLaPortChannelDLAGSystemPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelDLAGPeriodicSyncTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsLaPortChannelDLAGMSSelectionWaitTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsLaPortChannelDLAGStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelDLAGRedundancy ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelDLAGDistributingPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLaPortChannelDLAGDistributingPortIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelDLAGSystemID ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsLaPortChannelDLAGSystemPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelDLAGPeriodicSyncTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsLaPortChannelDLAGMSSelectionWaitTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsLaPortChannelDLAGStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelDLAGRedundancy ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelDLAGDistributingPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsLaDLAGTrapObjectsTable. */
INT1
nmhValidateIndexInstanceFsLaDLAGTrapObjectsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLaDLAGTrapObjectsTable  */

INT1
nmhGetFirstIndexFsLaDLAGTrapObjectsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaDLAGTrapObjectsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaDLAGTrapType ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsLaDLAGRemotePortChannelTable. */
INT1
nmhValidateIndexInstanceFsLaDLAGRemotePortChannelTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsLaDLAGRemotePortChannelTable  */

INT1
nmhGetFirstIndexFsLaDLAGRemotePortChannelTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaDLAGRemotePortChannelTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaDLAGRemotePortChannelSystemPriority ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsLaDLAGRemotePortChannelRolePlayed ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsLaDLAGRemotePortChannelKeepAliveCount ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsLaDLAGRemotePortChannelSpeed ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsLaDLAGRemotePortChannelHighSpeed ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsLaDLAGRemotePortChannelMtu ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsLaDLAGRemotePortTable. */
INT1
nmhValidateIndexInstanceFsLaDLAGRemotePortTable ARG_LIST((INT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLaDLAGRemotePortTable  */

INT1
nmhGetFirstIndexFsLaDLAGRemotePortTable ARG_LIST((INT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaDLAGRemotePortTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaDLAGRemotePortBundleState ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsLaDLAGRemotePortSyncStatus ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsLaDLAGRemotePortPriority ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelTrapTxCount ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsLaPortChannelMCLAGStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelMCLAGSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsLaPortChannelMCLAGSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelMCLAGRolePlayed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelMCLAGMaxKeepAliveCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortChannelMCLAGPeriodicSyncPduTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelMCLAGPeriodicSyncPduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelMCLAGEventUpdatePduTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelMCLAGEventUpdatePduRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortChannelClearStatistics ARG_LIST((INT4 , INT4 *));

INT1
nmhGetFsLaPortUpInBundleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortDownInBundleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortBundStChgTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortDownInBundleReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortErrStateDetTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortErrStateRecTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortRecTrigReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortDefStateRecCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortSameStateRecCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortHwFailRecCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortDefStateRecTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortSameStateRecTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortHwFailRecTimeStamp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortRecState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhSetFsLaPortChannelMCLAGStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelMCLAGSystemID ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsLaPortChannelMCLAGSystemPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelClearStatistics ARG_LIST((INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelMCLAGStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelMCLAGSystemID ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsLaPortChannelMCLAGSystemPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1 
nmhTestv2FsLaPortChannelClearStatistics ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsLaMCLAGRemotePortChannelTable. */
INT1
nmhValidateIndexInstanceFsLaMCLAGRemotePortChannelTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsLaMCLAGRemotePortChannelTable  */

INT1
nmhGetFirstIndexFsLaMCLAGRemotePortChannelTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaMCLAGRemotePortChannelTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaMCLAGRemotePortChannelSystemPriority ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsLaMCLAGRemotePortChannelRolePlayed ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsLaMCLAGRemotePortChannelKeepAliveCount ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsLaMCLAGRemotePortChannelSpeed ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsLaMCLAGRemotePortChannelHighSpeed ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsLaMCLAGRemotePortChannelMtu ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsLaMCLAGRemotePortTable. */
INT1
nmhValidateIndexInstanceFsLaMCLAGRemotePortTable ARG_LIST((INT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLaMCLAGRemotePortTable  */

INT1
nmhGetFirstIndexFsLaMCLAGRemotePortTable ARG_LIST((INT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaMCLAGRemotePortTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaMCLAGRemotePortSlotIndex ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsLaMCLAGRemotePortBundleState ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsLaMCLAGRemotePortSyncStatus ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsLaMCLAGRemotePortPriority ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLaPortChannelAdminMacAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsLaPortChannelMacSelection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelSelectionPolicy ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelDefaultPortIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelMaxPorts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortChannelSelectionPolicyBitList ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLaPortChannelAdminMacAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsLaPortChannelMacSelection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelSelectionPolicy ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelDefaultPortIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelMaxPorts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortChannelSelectionPolicyBitList ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLaPortChannelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLaPortTable. */
INT1
nmhValidateIndexInstanceFsLaPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLaPortTable  */

INT1
nmhGetFirstIndexFsLaPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaPortMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortBundleState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortActorResetAdminState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLaPortAggregateWaitTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortPartnerResetAdminState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLaPortActorAdminPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortRestoreMtu ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortSelectAggregator ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsLaPortErrStateDetCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortErrStateRecCount ARG_LIST((INT4 ,UINT4 *));

INT1                                                                                                                          nmhGetFsLaPortDefaultedStateThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsLaPortHardwareFailureRecThreshold ARG_LIST((INT4 ,UINT4 *));                                                                                                                                                                                        INT1
nmhGetFsLaPortSameStateRecThreshold ARG_LIST((INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLaPortMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortActorResetAdminState ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsLaPortAggregateWaitTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsLaPortPartnerResetAdminState ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsLaPortActorAdminPort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortRestoreMtu ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsLaPortDefaultedStateThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsLaPortHardwareFailureRecThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsLaPortSameStateRecThreshold ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLaPortMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortActorResetAdminState ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLaPortAggregateWaitTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsLaPortPartnerResetAdminState ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLaPortActorAdminPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortRestoreMtu ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsLaPortDefaultedStateThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsLaPortHardwareFailureRecThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsLaPortSameStateRecThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLaPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLaHwFailTrapObjectsTable. */
INT1
nmhValidateIndexInstanceFsLaHwFailTrapObjectsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLaHwFailTrapObjectsTable  */

INT1
nmhGetFirstIndexFsLaHwFailTrapObjectsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLaHwFailTrapObjectsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLaHwFailTrapType ARG_LIST((INT4  , INT4 ,INT4 *));

