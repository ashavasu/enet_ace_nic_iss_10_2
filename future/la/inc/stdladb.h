/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdladb.h,v 1.7 2008/08/20 15:13:01 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDLADB_H
#define _STDLADB_H

UINT1 Dot3adAggTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3adAggPortListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3adAggPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3adAggPortStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot3adAggPortDebugTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 stdla [] ={1,2,840,10006,300,43};
tSNMP_OID_TYPE stdlaOID = {6, stdla};


UINT4 Dot3adTablesLastChanged [ ] ={1,2,840,10006,300,43,1,3};
UINT4 Dot3adAggIndex [ ] ={1,2,840,10006,300,43,1,1,1,1,1};
UINT4 Dot3adAggMACAddress [ ] ={1,2,840,10006,300,43,1,1,1,1,2};
UINT4 Dot3adAggActorSystemPriority [ ] ={1,2,840,10006,300,43,1,1,1,1,3};
UINT4 Dot3adAggActorSystemID [ ] ={1,2,840,10006,300,43,1,1,1,1,4};
UINT4 Dot3adAggAggregateOrIndividual [ ] ={1,2,840,10006,300,43,1,1,1,1,5};
UINT4 Dot3adAggActorAdminKey [ ] ={1,2,840,10006,300,43,1,1,1,1,6};
UINT4 Dot3adAggActorOperKey [ ] ={1,2,840,10006,300,43,1,1,1,1,7};
UINT4 Dot3adAggPartnerSystemID [ ] ={1,2,840,10006,300,43,1,1,1,1,8};
UINT4 Dot3adAggPartnerSystemPriority [ ] ={1,2,840,10006,300,43,1,1,1,1,9};
UINT4 Dot3adAggPartnerOperKey [ ] ={1,2,840,10006,300,43,1,1,1,1,10};
UINT4 Dot3adAggCollectorMaxDelay [ ] ={1,2,840,10006,300,43,1,1,1,1,11};
UINT4 Dot3adAggPortListPorts [ ] ={1,2,840,10006,300,43,1,1,2,1,1};
UINT4 Dot3adAggPortIndex [ ] ={1,2,840,10006,300,43,1,2,1,1,1};
UINT4 Dot3adAggPortActorSystemPriority [ ] ={1,2,840,10006,300,43,1,2,1,1,2};
UINT4 Dot3adAggPortActorSystemID [ ] ={1,2,840,10006,300,43,1,2,1,1,3};
UINT4 Dot3adAggPortActorAdminKey [ ] ={1,2,840,10006,300,43,1,2,1,1,4};
UINT4 Dot3adAggPortActorOperKey [ ] ={1,2,840,10006,300,43,1,2,1,1,5};
UINT4 Dot3adAggPortPartnerAdminSystemPriority [ ] ={1,2,840,10006,300,43,1,2,1,1,6};
UINT4 Dot3adAggPortPartnerOperSystemPriority [ ] ={1,2,840,10006,300,43,1,2,1,1,7};
UINT4 Dot3adAggPortPartnerAdminSystemID [ ] ={1,2,840,10006,300,43,1,2,1,1,8};
UINT4 Dot3adAggPortPartnerOperSystemID [ ] ={1,2,840,10006,300,43,1,2,1,1,9};
UINT4 Dot3adAggPortPartnerAdminKey [ ] ={1,2,840,10006,300,43,1,2,1,1,10};
UINT4 Dot3adAggPortPartnerOperKey [ ] ={1,2,840,10006,300,43,1,2,1,1,11};
UINT4 Dot3adAggPortSelectedAggID [ ] ={1,2,840,10006,300,43,1,2,1,1,12};
UINT4 Dot3adAggPortAttachedAggID [ ] ={1,2,840,10006,300,43,1,2,1,1,13};
UINT4 Dot3adAggPortActorPort [ ] ={1,2,840,10006,300,43,1,2,1,1,14};
UINT4 Dot3adAggPortActorPortPriority [ ] ={1,2,840,10006,300,43,1,2,1,1,15};
UINT4 Dot3adAggPortPartnerAdminPort [ ] ={1,2,840,10006,300,43,1,2,1,1,16};
UINT4 Dot3adAggPortPartnerOperPort [ ] ={1,2,840,10006,300,43,1,2,1,1,17};
UINT4 Dot3adAggPortPartnerAdminPortPriority [ ] ={1,2,840,10006,300,43,1,2,1,1,18};
UINT4 Dot3adAggPortPartnerOperPortPriority [ ] ={1,2,840,10006,300,43,1,2,1,1,19};
UINT4 Dot3adAggPortActorAdminState [ ] ={1,2,840,10006,300,43,1,2,1,1,20};
UINT4 Dot3adAggPortActorOperState [ ] ={1,2,840,10006,300,43,1,2,1,1,21};
UINT4 Dot3adAggPortPartnerAdminState [ ] ={1,2,840,10006,300,43,1,2,1,1,22};
UINT4 Dot3adAggPortPartnerOperState [ ] ={1,2,840,10006,300,43,1,2,1,1,23};
UINT4 Dot3adAggPortAggregateOrIndividual [ ] ={1,2,840,10006,300,43,1,2,1,1,24};
UINT4 Dot3adAggPortStatsLACPDUsRx [ ] ={1,2,840,10006,300,43,1,2,2,1,1};
UINT4 Dot3adAggPortStatsMarkerPDUsRx [ ] ={1,2,840,10006,300,43,1,2,2,1,2};
UINT4 Dot3adAggPortStatsMarkerResponsePDUsRx [ ] ={1,2,840,10006,300,43,1,2,2,1,3};
UINT4 Dot3adAggPortStatsUnknownRx [ ] ={1,2,840,10006,300,43,1,2,2,1,4};
UINT4 Dot3adAggPortStatsIllegalRx [ ] ={1,2,840,10006,300,43,1,2,2,1,5};
UINT4 Dot3adAggPortStatsLACPDUsTx [ ] ={1,2,840,10006,300,43,1,2,2,1,6};
UINT4 Dot3adAggPortStatsMarkerPDUsTx [ ] ={1,2,840,10006,300,43,1,2,2,1,7};
UINT4 Dot3adAggPortStatsMarkerResponsePDUsTx [ ] ={1,2,840,10006,300,43,1,2,2,1,8};
UINT4 Dot3adAggPortDebugRxState [ ] ={1,2,840,10006,300,43,1,2,3,1,1};
UINT4 Dot3adAggPortDebugLastRxTime [ ] ={1,2,840,10006,300,43,1,2,3,1,2};
UINT4 Dot3adAggPortDebugMuxState [ ] ={1,2,840,10006,300,43,1,2,3,1,3};
UINT4 Dot3adAggPortDebugMuxReason [ ] ={1,2,840,10006,300,43,1,2,3,1,4};
UINT4 Dot3adAggPortDebugActorChurnState [ ] ={1,2,840,10006,300,43,1,2,3,1,5};
UINT4 Dot3adAggPortDebugPartnerChurnState [ ] ={1,2,840,10006,300,43,1,2,3,1,6};
UINT4 Dot3adAggPortDebugActorChurnCount [ ] ={1,2,840,10006,300,43,1,2,3,1,7};
UINT4 Dot3adAggPortDebugPartnerChurnCount [ ] ={1,2,840,10006,300,43,1,2,3,1,8};
UINT4 Dot3adAggPortDebugActorSyncTransitionCount [ ] ={1,2,840,10006,300,43,1,2,3,1,9};
UINT4 Dot3adAggPortDebugPartnerSyncTransitionCount [ ] ={1,2,840,10006,300,43,1,2,3,1,10};
UINT4 Dot3adAggPortDebugActorChangeCount [ ] ={1,2,840,10006,300,43,1,2,3,1,11};
UINT4 Dot3adAggPortDebugPartnerChangeCount [ ] ={1,2,840,10006,300,43,1,2,3,1,12};


tMbDbEntry stdlaMibEntry[]= {

{{11,Dot3adAggIndex}, GetNextIndexDot3adAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggMACAddress}, GetNextIndexDot3adAggTable, Dot3adAggMACAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggActorSystemPriority}, GetNextIndexDot3adAggTable, Dot3adAggActorSystemPriorityGet, Dot3adAggActorSystemPrioritySet, Dot3adAggActorSystemPriorityTest, Dot3adAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggActorSystemID}, GetNextIndexDot3adAggTable, Dot3adAggActorSystemIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggAggregateOrIndividual}, GetNextIndexDot3adAggTable, Dot3adAggAggregateOrIndividualGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggActorAdminKey}, GetNextIndexDot3adAggTable, Dot3adAggActorAdminKeyGet, Dot3adAggActorAdminKeySet, Dot3adAggActorAdminKeyTest, Dot3adAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggActorOperKey}, GetNextIndexDot3adAggTable, Dot3adAggActorOperKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPartnerSystemID}, GetNextIndexDot3adAggTable, Dot3adAggPartnerSystemIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPartnerSystemPriority}, GetNextIndexDot3adAggTable, Dot3adAggPartnerSystemPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPartnerOperKey}, GetNextIndexDot3adAggTable, Dot3adAggPartnerOperKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggCollectorMaxDelay}, GetNextIndexDot3adAggTable, Dot3adAggCollectorMaxDelayGet, Dot3adAggCollectorMaxDelaySet, Dot3adAggCollectorMaxDelayTest, Dot3adAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortListPorts}, GetNextIndexDot3adAggPortListTable, Dot3adAggPortListPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3adAggPortListTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortIndex}, GetNextIndexDot3adAggPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorSystemPriority}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorSystemPriorityGet, Dot3adAggPortActorSystemPrioritySet, Dot3adAggPortActorSystemPriorityTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorSystemID}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorSystemIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorAdminKey}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorAdminKeyGet, Dot3adAggPortActorAdminKeySet, Dot3adAggPortActorAdminKeyTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorOperKey}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorOperKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerAdminSystemPriority}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerAdminSystemPriorityGet, Dot3adAggPortPartnerAdminSystemPrioritySet, Dot3adAggPortPartnerAdminSystemPriorityTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerOperSystemPriority}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerOperSystemPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerAdminSystemID}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerAdminSystemIDGet, Dot3adAggPortPartnerAdminSystemIDSet, Dot3adAggPortPartnerAdminSystemIDTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerOperSystemID}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerOperSystemIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerAdminKey}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerAdminKeyGet, Dot3adAggPortPartnerAdminKeySet, Dot3adAggPortPartnerAdminKeyTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerOperKey}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerOperKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortSelectedAggID}, GetNextIndexDot3adAggPortTable, Dot3adAggPortSelectedAggIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortAttachedAggID}, GetNextIndexDot3adAggPortTable, Dot3adAggPortAttachedAggIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorPort}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorPortPriority}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorPortPriorityGet, Dot3adAggPortActorPortPrioritySet, Dot3adAggPortActorPortPriorityTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerAdminPort}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerAdminPortGet, Dot3adAggPortPartnerAdminPortSet, Dot3adAggPortPartnerAdminPortTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerOperPort}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerOperPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerAdminPortPriority}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerAdminPortPriorityGet, Dot3adAggPortPartnerAdminPortPrioritySet, Dot3adAggPortPartnerAdminPortPriorityTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerOperPortPriority}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerOperPortPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorAdminState}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorAdminStateGet, Dot3adAggPortActorAdminStateSet, Dot3adAggPortActorAdminStateTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortActorOperState}, GetNextIndexDot3adAggPortTable, Dot3adAggPortActorOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerAdminState}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerAdminStateGet, Dot3adAggPortPartnerAdminStateSet, Dot3adAggPortPartnerAdminStateTest, Dot3adAggPortTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortPartnerOperState}, GetNextIndexDot3adAggPortTable, Dot3adAggPortPartnerOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortAggregateOrIndividual}, GetNextIndexDot3adAggPortTable, Dot3adAggPortAggregateOrIndividualGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsLACPDUsRx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsLACPDUsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsMarkerPDUsRx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsMarkerPDUsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsMarkerResponsePDUsRx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsMarkerResponsePDUsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsUnknownRx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsUnknownRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsIllegalRx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsIllegalRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsLACPDUsTx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsLACPDUsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsMarkerPDUsTx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsMarkerPDUsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortStatsMarkerResponsePDUsTx}, GetNextIndexDot3adAggPortStatsTable, Dot3adAggPortStatsMarkerResponsePDUsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortStatsTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugRxState}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugRxStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugLastRxTime}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugLastRxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugMuxState}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugMuxStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugMuxReason}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugMuxReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugActorChurnState}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugActorChurnStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugPartnerChurnState}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugPartnerChurnStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugActorChurnCount}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugActorChurnCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugPartnerChurnCount}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugPartnerChurnCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugActorSyncTransitionCount}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugActorSyncTransitionCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugPartnerSyncTransitionCount}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugPartnerSyncTransitionCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugActorChangeCount}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugActorChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{11,Dot3adAggPortDebugPartnerChangeCount}, GetNextIndexDot3adAggPortDebugTable, Dot3adAggPortDebugPartnerChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot3adAggPortDebugTableINDEX, 1, 0, 0, NULL},

{{8,Dot3adTablesLastChanged}, NULL, Dot3adTablesLastChangedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stdlaEntry = { 57, stdlaMibEntry };
#endif /* _STDLADB_H */

