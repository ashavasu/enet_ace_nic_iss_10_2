/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: laconst.h,v 1.54 2017/10/16 09:40:59 siva Exp $
 *
 *******************************************************************/

#ifndef  _LACONST_H
#define  _LACONST_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : laconst.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains all constants used in the   */
/*                            Link Aggregation module.                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

#define LA_MIN_DISTRIBUTION_VAL 1
#define LA_MAX_DISTRIBUTION_VAL 14
#define LA_TRAP_ENABLE 1
#define LA_TRAP_DISABLE 2
#define LA_PORT_UP_IN_BNDL     0
#define LA_PORT_DOWN           2
#define LA_PORT_UP_INDIVIDUAL  3

#define LA_DLAG_PORT_IN_SYNC       1
#define LA_DLAG_PORT_OUT_OF_SYNC   2

#define LA_AGG_WAIT_TIME       10
#define LA_MIN_AGG_INDEX                         LA_MAX_PORTS + 1
#define LA_MAX_AGG_INDEX                         LA_MAX_PORTS + LA_MAX_AGG
#define LA_MUX_REASON_LEN      40
/* Ethernet Type */
#define  LA_ETHERNET_TYPE                        CFA_ENET
#define  LA_PORT_CHANNEL_TYPE                    CFA_LAGG

#define LA_PORT_CHANNEL_MIN_PORTS                 2
#define LA_PORT_CHANNEL_MAX_PORTS                 LA_MAX_PORTS_PER_AGG

/* Constant for Memory Pool allocation */
#define LA_MEMORY_TYPE                           MEM_DEFAULT_MEMORY_TYPE

#define LA_MAX_CACHE_ENTRIES                     256 
#define LA_MAX_MARKER_ENTRIES                    LA_MAX_PORTS_IN_SYSTEM

/* LA Timer Task Constants */
#define LA_TIMER_TASK_NAME                       ((const UINT1*)"LaTT")
#define LA_TASK_PRIORITY                         35
#define LA_TASK_MODE                             OSIX_GLOBAL
#define LA_STACK_SIZE                            OSIX_DEFAULT_STACK_SIZE

/* Number of Different Timers in the LA module */
#define LA_TIMER_TYPES                           5

/* Count of Number of Memory Blocks per Memory Pool */

#define  LA_PORTENTRY_MEMBLK_COUNT               LA_MAX_PORTS_IN_SYSTEM
#define  LA_AGGENTRY_MEMBLK_COUNT                LA_MAX_AGG
#define  LA_AGGCONFIGENTRY_MEMBLK_COUNT          LA_MAX_AGG
#define  LA_AGGIFENTRY_MEMBLK_COUNT              LA_MAX_AGG
#define  LA_CACHEENTRY_MEMBLK_COUNT              LA_MAX_CACHE_ENTRIES
#define  LA_MARKERENTRY_MEMBLK_COUNT             LA_MAX_MARKER_ENTRIES
#define  LA_TIMER_MEMBLK_COUNT                   LA_MAX_PORTS*LA_TIMER_TYPES

/* Marker Rate Timer Actions */
#define  LA_MARKER_GET                           1
#define  LA_MARKER_SET                           2

/* Macros for return Values */
#define LA_MEM_SUCCESS                           MEM_SUCCESS
#define LA_MEM_FAILURE                           MEM_FAILURE
#define LA_CRU_SUCCESS                           CRU_SUCCESS
#define LA_CRU_FAILURE                           CRU_FAILURE
#define LA_TMR_SUCCESS                           TMR_SUCCESS
#define LA_TMR_FAILURE                           TMR_FAILURE
#define LA_OSIX_SUCCESS                          OSIX_SUCCESS
#define LA_OSIX_FAILURE                          OSIX_FAILURE
#define LA_CFA_SUCCESS                           CFA_SUCCESS
#define LA_CFA_FAILURE                           CFA_FAILURE
#define LA_BRG_SUCCESS                           BRIDGE_SUCCESS
#define LA_BRG_FAILURE                           BRIDGE_FAILURE
#define LA_NODE_IDLE                             RM_INIT
#define LA_NODE_ACTIVE                           RM_ACTIVE
#define LA_NODE_STANDBY                          RM_STANDBY

/* Return Values */
#define LA_ERR_MEM_FAILURE                       1
#define LA_ERR_NULL_PTR                          2
#define LA_ERR_NO_ENTRY                          3
#define LA_ERR_TASK                              4
#define LA_ERR_QUEUE                             5
#define LA_ERR_INVALID_VALUE                     6

/* Invalid Values */
#define LA_INVALID_VAL                           0
#define LA_INIT_VAL                              0
#define LA_16BIT_MAX                             65535
#define LA_32BIT_MAX                             4294967295U /* 0xffffffff */

#define LA_FAST_PERIODIC_TIME         1
#define LA_SLOW_PERIODIC_TIME         30
#define LA_SHORT_TIMEOUT_TIME         (3 * LA_FAST_PERIODIC_TIME)
#define LA_LONG_TIMEOUT_TIME          (3 * LA_SLOW_PERIODIC_TIME)
/* Default timer values in seconds */
#define LA_DEFAULT_CURRENT_WHILE_TIME (3 * LA_FAST_PERIODIC_TIME)
#define LA_DEFAULT_PERIODIC_TIME      LA_FAST_PERIODIC_TIME
#define LA_DEFAULT_WAIT_WHILE_TIME    2
#define LA_DEFAULT_HOLD_WHILE_TIME    1
#define LA_MAX_RECOVERY_TIME        3600
#define LA_DEFAULT_RECOVERY_TIME    0
#define LA_MIN_REC_THRESHOLD        0
#define LA_MAX_REC_THRESHOLD        20
#define LA_DEFAULT_REC_THRESHOLD    5

#define LA_MIN_DEF_STATE_THRESHOLD  0
#define LA_MAX_DEF_STATE_THRESHOLD  20
#define LA_DEF_STATE_DEFAULT_THRESHOLD  5
#define LA_ACT_NONE                     1
#define LA_ACT_SHUTDOWN                 2

/* Default D-LAG timer values in milliseconds */
#define LA_DLAG_DEFAULT_PERIODIC_SYNC_TIME      1000
#define LA_DLAG_DEFAULT_MS_SELECTION_WAIT_TIME  0

/* Default periodic sync time in Active-Active DLAG */ 
#define LA_AA_DLAG_DEFAULT_PERIODIC_SYNC_TIME      2

/* Defualt Selection policy */
#define LA_SELECT_POLICY_DEF LA_SELECT_SRC_DST_MAC

#define LA_CURRWHILE_SPLIT_INTERVALS   3
#define LA_INVALID_HW_AGG_IDX          0xFF

/* LACPDU constants */
#define LA_ACTOR_INFO                            0x01
#define LA_PARTNER_INFO                          0x02
#define LA_COLLECTOR_INFO                        0x03
#define LA_TERMINATOR_INFO                       0x00
#define LA_ACTOR_INFO_LEN                        20
#define LA_PARTNER_INFO_LEN                      20
#define LA_COLLECTOR_INFO_LEN                    16
#define LA_TERMINATOR_INFO_LEN                   0

#define LACPDU_SUB_TYPE                          0x01
#define LACP_VERSION                             1
#define LA_COLLECTOR_DELAY                       2

#define LA_SLOW_PROTOCOL_SIZE                    124

/* This macro holds the size of of Sync PDU
 * sent by a Switch when Global DLAG is enabled
 * size is = 12 (DA + SA)
 *            2 (LenOrType)
 *            4 (LA subtype + version + DSU Info + DSU Info Length)
 *            8 (System MAC + Priority of Sender)
 *           =====
 *           30 bytes 
 *           =====
 *  Per-Port-channel Info
 *            10 [Key(4) + Mtu(2) + Speed of port channel(1) + Reserved(2)+Port no (1)]
 *            8 [port no (4) + Port Up/Down (1) + Bundle state(1) +Prioirty(2)]- Per Port
 *        
 *           ========
 *           10 + 64 [(LA_MAX_PORTS_PER_AGG * Per Port) bytes. = 74 Bytes
 *           ========
 * Total Sync Data Size = 30 + (LA_MAX_AGG(8) * [74]) +4 bytes FCS.
 *
 */

#define LA_AA_DLAG_PDU_SIZE                        (30 + (LA_MAX_AGG * 74) + 4)

#define LA_AA_DLAG_SYNC_PDU_PER_PORT_INFO_LEN          8

#define LA_AA_MCLAG_SYNC_PDU_PER_PORT_INFO_LEN          12

#define LA_AA_DLAG_SYNC_PDU_NO_OF_PORTS_OFFSET         8

#define LA_AA_DLAG_ADD_OR_DEL_FIELD_LEN                1
#define LA_AA_DLAG_PORT_NUM_LEN                        4
#define LA_AA_MCLAG_PORT_STATE_FIELD_LEN               1
#define LA_AA_MCLAG_PI_MASK_FIELDLEN                   1

#define LA_LACPDU_SIZE                           LA_SLOW_PROTOCOL_SIZE
#define LA_MARKER_PDU_SIZE                       LA_SLOW_PROTOCOL_SIZE
#define LA_REQUIRED_BUF_SIZE                     60

/* DSU INFO : D-LAG Switch Unit */
#define LA_DLAG_DSU_INFO                         0xFF
#define LA_DLAG_DSU_INFO_LEN                     0x14

/* Incase of Active DLAG-Sync DSU info will lenghth as 11 */

#define LA_AA_DLAG_DSU_INFO_LEN              0x11
#define LA_AA_DLAG_ADD_DEL_INFO_LEN          0x07

/* High priority D-LAG event PDU type */
#define LA_DLAG_HIGH_PRIO_EVENT_PO_UP                 0x10
#define LA_DLAG_HIGH_PRIO_EVENT_PO_DOWN               0x20
#define LA_DLAG_HIGH_PRIO_EVENT_PO_UP_ACK             0x30
#define LA_DLAG_HIGH_PRIO_EVENT_INTF_UP               0x40
#define LA_DLAG_HIGH_PRIO_EVENT_INTF_DOWN             0x50
#define LA_DLAG_HIGH_PRIO_EVENT_PORT_DETATCH_FROM_AGG 0x60
#define LA_DLAG_HIGH_PRIO_EVENT_RED_SWITCHED_OFF      0x70

/* The following PDU type is used in Active-Active DLAG */
#define LA_AA_DLAG_HIGH_PRIO_EVENT_ADD_DEL_UPDATE        0x80
#define LA_AA_DLAG_HIGH_PRIO_PERIODIC_ADD_DEL_UPDATE     0x90

#define LA_AA_DLAG_INITIAL_UPDATE_ON_NEW_NODE            0x11
#define LA_AA_DLAG_ADD_DEL_UPDATE_ONLY                   0x12

/* Low priority D-LAG event PDU type */
#define LA_DLAG_LOW_PRIO_EVENT_DB_UPDATE             0x01
#define LA_DLAG_LOW_PRIO_EVENT_RES_SYNC              0x02
#define LA_DLAG_LOW_PRIO_EVENT_RES_SYNC_ACK          0x03
#define LA_DLAG_LOW_PRIO_EVENT_PORT_DETATCH_FROM_AGG 0x04

/* Operation of Active Active D-LAG event PDU type */
#define LA_AA_DLAG_DSU_ADD_PORT_LIST                          0x01
#define LA_AA_DLAG_DSU_DELETE_PORT_LIST                       0x02

#define LA_AA_DLAG_MTU_AND_SPEED_SAME   1
#define LA_AA_DLAG_MTU_OR_SPEED_GREATER   2
#define LA_AA_DLAG_MTU_OR_SPEED_LESS   3
#define LA_AA_DLAG_MTU_RCVD_FROM_PRV_BEST   4


#define LA_AA_DLAG_PORT_NOT_IN_ANY_LIST   0
#define LA_AA_DLAG_PORT_IN_ADD   1
#define LA_AA_DLAG_PORT_IN_DELETE   2
#define LA_AA_DLAG_PORT_IN_STANDBY   3
#define LA_AA_DLAG_PORT_REMOVE_CONS_ENTRY   4

#define LA_REC_ERR_NONE             0
#define LA_REC_ERR_DEFAULTED        1
#define LA_REC_ERR_HW_FAILURE       2
#define LA_REC_ERR_SAME_STATE       3
#define LA_REC_ERR_THRSHLD_EXCEED   4
#define LA_REC_ERR_DISABLED         5


#define LA_MILLI_SECONDS_IN_SEC    1000

/* Size of Reserved field between back-up master mac address
 * and Port Count in the D-LAG PDU */
#define LA_DLAG_DSU_RESERVED_FIELD1_LEN          0x05

#define LA_AA_DLAG_DSU_RESERVED_FIELD1_LEN          0x01

/* Offset to access D-LAG Master Mac address field in DSU
 * from TLV_Type field */
#define LA_DLAG_MS_SELECTION_RESULT_OFFSET       0x10

/* Offset to access from DSU Sub Type 
*  in the TLV */
#define LA_DLAG_DSU_SUBTYPE_OFFSET               0x10
/* D-LAG Sll Operations on D-LAG remote aggregator list and
 * remote ports list */
#define LA_DLAG_DEL_REM_NODE_INFO   0x01
#define LA_DLAG_CPY_REM_NODE_INFO   0x02
#define LA_DLAG_DEL_REM_PORT_INFO   0x03
#define LA_DLAG_EVENT_RES_SYNC_INFO 0x04


/* D-LAD Ms Triggering Scenarios */
#define LA_DLAG_MS_TRIGGER_ON_NEW_NODE         0x01
#define LA_DLAG_MS_TRIGGER_ON_DEL_NODE         0x02
#define LA_DLAG_MS_TRIGGER_ON_INTF_CHANGE      0x03
#define LA_DLAG_MS_TRIGGER_ON_DIST_INTF_DOWN   0x04

/* Linear Buffer Offsets */
#define LA_VLAN_OFFSET     14
#define LA_VLAN_MASK     0X0FFF
#define LA_VLAN_SIZE     2

#define LA_ISID_OFFSET     14
#define LA_ISID_MASK     0X00FFFFFF
#define LA_ISID_SIZE     4

#ifdef MPLS_WANTED
#define LA_MPLS_TUNNEL_OFFSET       18
#define LA_MPLS_VC_OFFSET           22
#define LA_MPLS_VC_TUNNEL_OFFSET    26
#define LA_MPLS_MASK                0XFFFFF000
#define LA_MPLS_SIZE                4
#endif

/* Marker, Parser, Multiplexer macros */
#define LA_MARKER_INFORMATION                    0x01
#define LA_MARKER_RESPONSE_INFORMATION           0x02
#define LA_MAX_MARKER_TRANS_ID                   250
#define LA_MARKER_VERSION_NUM                    0x01
#define LA_MARKER_INFO_LENGTH                    0x10
#define LA_MARKER_RESPONSE_INFO_LENGTH           0x10
#define LA_MARKER_TERMINATOR_TYPE                0x00
#define LA_MARKER_TERMINATOR_LENGTH              0x00
#define LA_MARKER_PAD                            0x0000
#define LA_MARKER_TLV_OFFSET                     16
#define LA_MARKER_REQUESTER_PORT_OFFSET          18
#define LA_MARKER_TRANS_ID_OFFSET                26
#define LA_MARKER_INFO_LENGTH_SIZE               1

#define LA_OFFSET                                0
#define LA_MAX_ETH_FRAME_SIZE                    1522

#define LA_MAX_DLAG_PDU_SIZE               1500

#define LA_PDU_VERSION_NUM_SIZE                  1
#define LA_MARKER_VER_OFFSET                     3
#define LA_FRAME_HDR_SIZE                        17

#define LA_TMR_EXPIRY_EVENT                      0x01
#define LA_CONTROL_EVENT                         0x02
#define LA_PKT_ENQ_EVENT                         0x04
#define LA_RED_BULK_UPD_EVENT                    0x08
#define LA_MBSM_STATUS_GO_ACTIVE                 0x10
#define LA_MBSM_STATUS_GO_STANDBY                0x20
#define LA_MBSM_STATUS_ACTIVE_CHANGE             0x40

#define LA_ALL_EVENTS                 (LA_TMR_EXPIRY_EVENT    | \
                                       LA_CONTROL_EVENT       | \
                                       LA_PKT_ENQ_EVENT       | \
                                       LA_RED_BULK_UPD_EVENT  | \
                                       LA_MBSM_STATUS_GO_ACTIVE  | \
                                       LA_MBSM_STATUS_GO_STANDBY  | \
                                       LA_MBSM_STATUS_ACTIVE_CHANGE)

#define LA_CONTROL_FRAME                         0x02
#define LA_CACHE_TABLE_SIZE                      64 
#define LA_MARKER_TABLE_SIZE                     8

#define LA_MIN_ILLEGAL_SUBTYPE                   1
#define LA_MAX_ILLEGAL_SUBTYPE                   10
#define LA_MAX_MARKER_RATE_COUNT                 5

#define LA_UNKNOWN                               2
#define LA_ENCAP_NONE                            CFA_ENCAP_NONE
#define LA_SLOW_PROTOCOL                         0

#define LA_MAC_ADDRESS_SIZE                      6

/* Agg If table constants */
#define LA_MAX_STR_LENGTH                        20   
#define LA_AGG_DESCR_STR_LENGTH                  56 
#define LA_AGG_IF_TYPE                           161
#define LA_AGG_IF_MTU_SIZE                       1500

/* For Snmp */
#define LA_CONNECTOR_FALSE                       2

#define LA_PORT_STATIC_AGG                       0
#define LA_PORT_DYNAMIC_AGG                      1

/* Port State constants */
#define LA_MIN_PORT_STATE_VAL                     0
#define LA_MAX_PORT_STATE_VAL                     128
#define LA_LACPACTIVITY_BITMASK                   0x80
#define LA_LACPTIMEOUT_BITMASK                    0x40
#define LA_AGGREGATION_BITMASK                    0x20
#define LA_SYNC_BITMASK                           0x10
#define LA_COLLECTING_BITMASK                     0x8
#define LA_DISTRIBUTING_BITMASK                   0x4
#define LA_DEFAULTED_BITMASK                      0x2
#define LA_EXPIRED_BITMASK                        0x1
#define LA_LACPSTATE_RO_BITSMASK                  0x1F
#define LA_CLI_LACPACTIVITY_BIT                0x01
#define LA_CLI_LACPTIMEOUT_BIT                 0x02

#define LA_PORT_STATE_MASK                       7
#define LA_LACP_ACTIVITY                         0
#define LA_LACP_TIMEOUT                          1
#define LA_AGGREGATION                           2

#define LA_ACTIVITY       80
#define LA_TIMEOUT        40 
#define LA_AGGREGATE      20 

#define LA_SNMP_TRUE                             1
#define LA_SNMP_FALSE                            2
#define LA_SNMP_MIN_VAL                          0
#define LA_SNMP_MAX_VAL                          65535
#define LA_SNMP_MIN_PRIORITY                     0
#define LA_SNMP_MAX_PRIORITY                     65535

#define LA_CREATE_PORT_MSG        1
#define LA_DELETE_PORT_MSG        2
#define LA_ENABLE_PORT_MSG        3
#define LA_DISABLE_PORT_MSG       4
#define LA_MAP_PORT_MSG           6
#define LA_UNMAP_PORT_MSG         7
#define LA_NP_CALLBACK            8
#define LA_ICCH_MSG               9

#define LA_PORTS_PER_BYTE         8

/* Trap related constants */
#define LA_TRAP_PORTCHANNEL_DOWN 2
#define LA_TRAP_PORTCHANNEL_UP   3
#define LA_TRAP_PORT_DOWN 4
#define LA_TRAP_PORT_UP   5
#define LA_TRAP_PORTCHANNEL_OPER_DOWN 6
#define LA_TRAP_PORTCHANNEL_OPER_UP   7
#define LA_TRAP_OID_LEN 10
#define LA_OBJECT_NAME_LEN 256

#define LA_TRAP_DLAG_MASTER_TO_SLAVE 1
#define LA_TRAP_DLAG_SLAVE_TO_MASTER 2


#define LA_NP_SYNC           ISS_NPAPI_MODE_SYNCHRONOUS
#define LA_NP_ASYNC          ISS_NPAPI_MODE_ASYNCHRONOUS

/* D-LAG Role Played by D-LAG Nodes */
#define LA_DLAG_SYSTEM_ROLE_NONE            0
#define LA_DLAG_SYSTEM_ROLE_MASTER          1
#define LA_DLAG_SYSTEM_ROLE_SLAVE           2
#define LA_DLAG_SYSTEM_ROLE_BACKUP_MASTER   3

/* MC-LAG Role Played by D-LAG Nodes */
#define LA_MCLAG_SYSTEM_ROLE_NONE            0
#define LA_MCLAG_SYSTEM_ROLE_MASTER          1
#define LA_MCLAG_SYSTEM_ROLE_SLAVE           2

#define LA_DLAG_DEFAULT_SYSTEM_PRIORITY        0x8000 /* Decimal 32768 */
#define LA_DLAG_MAX_KEEP_ALIVE_COUNT           0x3

/* D-LAG max timer values in milliseconds */
#define LA_DLAG_MAX_PERIODIC_SYNC_TIME         90000
#define LA_DLAG_MAX_MS_SELECTION_WAIT_TIME     90000

#define LA_AA_DLAG_MAX_PERIODIC_SYNC_TIME         90

#define LA_DLAG_MIN_SYS_PRIORITY               LA_SNMP_MIN_PRIORITY
#define LA_DLAG_MAX_SYS_PRIORITY               LA_SNMP_MAX_PRIORITY

#define LA_MC_LAGG_ENABLED                       1
#define LA_MC_LAGG_DEFAULT_INSTANCE_ID           0

/* Offset to Access port number from Interface Type */
#define LA_MC_LAGG_PORT_NUM_OFFSET               4

#define LA_ENHANCED_LB_ENABLE               LA_ENABLED
#define LA_ENHANCED_LB_DISABLE              LA_DISABLED

#define LA_RANDOMIZED_LB_ENABLE               LA_ENABLED
#define LA_RANDOMIZED_LB_DISABLE              LA_DISABLED

#endif  /* _LA_CONST_H */
