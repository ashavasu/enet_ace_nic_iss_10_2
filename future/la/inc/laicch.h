/* $Id: laicch.h,v 1.7 2015/10/31 12:22:59 siva Exp $                      */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2005-2006                                          */
/*****************************************************************************/
/*    FILE  NAME            : vlanicch.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LA ICCH                                        */
/*    MODULE NAME           : LA                                             */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 02 Jan 2014                                    */
/*    AUTHOR                : ICCH TEAM                                      */
/*    DESCRIPTION           : This file contains all constants and typedefs  */
/*                            for LA ICCH module.                            */
/*---------------------------------------------------------------------------*/
#ifndef _LAICCH_H_
#define _LAICCH_H_

/* tLaIcchData - Used to enqueue message to Icch */
typedef struct _LaIcchData {
    UINT4         u4Events;  /* Events from ICCH to LA */
    tIcchMsg     *pIcchMsg;  /* Message buffer sent from ICCH to LA */
    UINT2         u2DataLen; /* Buffer length */
    UINT1         au1Reserved[2];
}tVlanIcchData;

#define LA_ICCH_MAX_MSG_SIZE              1500
#define LA_ICCH_MSGTYPE_FIELD_SIZE        1
#define LA_ICCH_SYNC_MSG_TYPE_SIZE             1
#define LA_ICCH_LEN_FIELD_SIZE            2
enum
{
    LA_ICCH_BULK_UPD_REQUEST = ICCH_BULK_UPDT_REQ_MSG,
    LA_ICCH_BULK_UPD_TAIL_MESSAGE = ICCH_BULK_UPDT_TAIL_MSG,
    LA_ICCH_SYNC_DSU,
    LA_ICCH_INTF_COUNT_INFO
};
/* Macros to write in to ICCH buffer. */
#define LA_ICCH_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define LA_ICCH_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define LA_ICCH_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define LA_ICCH_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    ICCH_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define LA_ICCH_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define LA_ICCH_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define LA_ICCH_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define LA_ICCH_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    ICCH_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

/* Prototypes */
INT4 LaIcchRegisterWithICCH (VOID);
INT4 LaIcchDeRegisterWithICCH (VOID);
UINT4 LaIcchDeRegisterProtocols (tIcchRegParams * pIcchReg);
UINT4 LaIcchRegisterProtocols (tIcchRegParams * pIcchReg);

INT4 LaIcchHandleUpdateEvents (UINT1 u1Event, tIcchMsg * pData, 
        UINT2 u2DataLen);
VOID LaIcchProcessUpdateMsg (tLaIntfMesg * pMsg);
INT4 LaIcchSendBulkReqMsg (VOID);
INT4 LaIcchSendBulkUpdTailMsg (VOID);
INT4 LaIcchCheckAndSendMsg (tIcchMsg **ppIcchMsg, UINT2 u2MsgLen, 
        UINT2 *pu2OffSet);
VOID LaIcchHandleUpdates (tIcchMsg * pMsg, UINT2 u2DataLen);
VOID LaIcchSendBulkUpdates (VOID);
INT4 LaIcchSyncBulkLaEntries (tIcchMsg **ppMsg, UINT2 *pu2OffSet);
INT4 LaIcchSendDSUToPeer (UINT1 * pu1LinearBuf, UINT4);
INT4 LaIcchSendMsgToIcch (tIcchMsg * pMsg, UINT2 u2Len);
VOID LaIcchSendIntfCountToPeer (VOID);

INT4 LaSetIcclProperties (VOID);

#endif /* _LAICCH_H_ */
