/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: lamacro.h,v 1.32 2015/06/16 12:00:18 siva Exp $
 *
 *******************************************************************/

#ifndef _LAMACRO_H
#define _LAMACRO_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : lamacro.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains all macros used in          */
/*                            Link Aggregation module.                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/* Task creation related functions*/
#define LA_CTRL_TASK_NAME     ((const UINT1*) "LaTT")
#define LA_TASK_SIZE         (2*OSIX_DEFAULT_STACK_SIZE)
#define LA_CTRLQ_NAME        ((UINT1*) "LaCQ")
#define LA_CTRLQ_DEPTH       (40)
#define LA_CTRLQ_MODE        (0)
#define LA_INTFQ_NAME        ((UINT1*) "LaIQ")
#define LA_INTFQ_DEPTH       (LA_MAX_PORTS_IN_SYSTEM* 4)
#define LA_INTFQ_MODE        (0)
#define LA_MAX_DYN_CMDS       5

/* LA Semaphore related constants */
#define LA_PROTOCOL_SEMAPHORE                   ((const UINT1*)"LAPS")
#define LA_BINARY_SEM                           1
#define LA_SEM_FLAGS                            0 /* unused */

/* Count of Number of Memory Blocks per Memory Pool */

#define  MAX_LACTRLMESG_BUFFERS                  LA_CTRLQ_DEPTH
#define  MAX_LAINTFMESG_BUFFERS                  LA_INTFQ_DEPTH


 /*For updating Standby port state in h/w.*/

#define LA_STANDBY_INDICATION        2

/* MEM Pool Id Definitions */

#define LA_CTRLQ_ID                   gLaCtrlQId
#define LA_CTRLMESG_POOL_ID           gLaCtrlMesgPoolId
#define LA_INTFQ_ID                   gLaIntfQId
#define LA_INTFMESG_POOL_ID           gLaIntfMesgPoolId

#define LA_INITIALIZED() gu1LaInitialized
#define LA_SET_INITIALIZED() gu1LaInitialized = LA_TRUE

/* Macros for creating memory pools */

#define  LA_CREATE_MEM_POOL(x,y,pLaPoolId)\
         MemCreateMemPool(x,y,LA_MEMORY_TYPE,(tLaMemPoolId*)(pLaPoolId))


/* Macros for deleting Memory Pools */

#define  LA_DELETE_MEM_POOL(LaPoolId)\
         MemDeleteMemPool((tLaMemPoolId) (LaPoolId))



/* Macros for Memory Block Allocation from Memory Pool */

#define  LA_ALLOC_MEM_BLOCK(poolid)   MemAllocMemBlk(poolid)



/* Macros for Freeing Memory Blocks from Memory Pools */

#define  LA_FREE_MEM_BLOCK(poolid, pu1Msg)\
         MemReleaseMemBlock((poolid), (UINT1 *)(pu1Msg))


#define   LA_ENQUEUE_MESSAGE(qid,pmsg)\
             OsixQueSend(qid, (UINT1 *) &pmsg, LA_DEF_MSG_LEN)

#define   LA_DEQUEUE_MESSAGES(qid,ppmsg)\
             OsixQueRecv(qid, ((UINT1 *) ppmsg), LA_DEF_MSG_LEN, 0)

 /* Event Definitions */
#define LA_NO_WAIT          OSIX_NO_WAIT

#define   LA_CREATE_SEMAPHORE(au1Name, count, flags, psemid) \
             OsixCreateSem((au1Name), (count), (flags), (psemid))

#define   LA_DELETE_SEMAPHORE(semid) OsixSemDel(semid)

#define   LA_TAKE_PROTOCOL_SEMAPHORE(SemId)      \
          {                         \
           if (OsixSemTake(SemId) == OSIX_SUCCESS) \
           {\
                gLaGlobalInfo.bProtocolLocked = LA_TRUE;  \
           }\
          }

#define   LA_RELEASE_PROTOCOL_SEMAPHORE(SemId)      \
          {  \
                OsixSemGive(SemId);  \
                gLaGlobalInfo.bProtocolLocked = LA_FALSE;  \
          }


#define LA_MODULE_STATUS   gLaGlobalInfo.LaEnabled
#define LA_SYSTEM_CONTROL  gLaSystemControl
#define LA_SYSLOG_ID       gLaGlobalInfo.i4SysLogId
#define LA_DLAG_SYSTEM_STATUS gu1DLAGSystemStatus
#define LA_MCLAG_SYSTEM_STATUS gu1MCLAGSystemStatus
#define LA_AA_DLAG_MASTER_DOWN  gu1AADLAGMasterDown
#define LA_AA_DLAG_ROLE_PLAYED  gu1AADLAGRolePlayed
#define LA_MCLAG_PEER_EXISTS  gu1MclagPeerExists


#define LA_CALLBACK       gLaCallBack
#define LA_MUX_MACHINE    gLaGlobalInfo.MuxMachine
#define LA_FREE_AGGS      gLaGlobalInfo.u4FreeAggs
#define LA_AGG_SLL        &(gLaGlobalInfo.LaAggList)
#define LA_GET_PORTENTRY(u2Port) \
         gLaGlobalInfo.apLaLacPortEntry[u2Port-1]

#define LA_GET_AGG_CONFIG_PORT_SLL(pAggEntry) \
   &(pAggEntry->ConfigPortSll)
 
#define LA_AGG_GET_ACTIVE_PORT_SLL(pAggEntry) \
 &pAggEntry->LaAggActivePortSll
 
#define LA_GET_AGG_IF_ENTRY(pAggEntry) \
  &(pAggEntry->AggIfEntry)

#define LA_GET_AGG_CONFIG_ENTRY(pAggEntry) \
  &(pAggEntry->AggConfigEntry)

#define LA_DLAG_GLOBAL_INFO gLaGlobalInfo.GlobalDLAGInfo 

#define LA_PORTENTRY_MEMBLK_SIZE       sizeof(tLaLacPortEntry)
#define LA_AGGENTRY_MEMBLK_SIZE  sizeof(tLaLacAggEntry)
#define LA_AGGCONFIGENTRY_MEMBLK_SIZE  sizeof(tLaLacAggConfigEntry)
#define LA_AGGIFENTRY_MEMBLK_SIZE      sizeof(tLaLacAggIfEntry)
#define LA_CACHEENTRY_MEMBLK_SIZE      sizeof(tLaCacheEntry)
#define LA_MARKERENTRY_MEMBLK_SIZE     sizeof(tLaMarkerEntry)
#define LA_DLAGREMOTEAGGENTRY_MEMBLK_SIZE sizeof(tLaDLAGRemoteAggEntry)
#define LA_DLAGREMOTEPORTENTRY_MEMBLK_SIZE sizeof(tLaDLAGRemotePortEntry)
#define LA_TIMER_MEMBLK_SIZE           sizeof(tLaAppTmrNode)


/* MEM Pool Id Definitions */

#define LA_PORTENTRY_POOL_ID          gLaGlobalInfo.LaPortPoolId
#define LA_AGGENTRY_POOL_ID     gLaGlobalInfo.LaAggPoolId
#define LA_AGGCONFIGENTRY_POOL_ID     gLaGlobalInfo.LaAggConfigPoolId
#define LA_AGGIFENTRY_POOL_ID         gLaGlobalInfo.LaAggIfPoolId
#define LA_CACHEENTRY_POOL_ID         gLaGlobalInfo.LaCachePoolId
#define LA_MARKERENTRY_POOL_ID        gLaGlobalInfo.LaMarkerPoolId
#define LA_DLAGREMOTEAGGENTRY_POOL_ID gLaGlobalInfo.LaDlagRemoteAggPoolId
#define LA_DLAGREMOTEPORTENTRY_POOL_ID gLaGlobalInfo.LaDlagRemotePortPoolId 
#define LA_DLAGCONSPORTENTRY_POOL_ID   gLaGlobalInfo.LaDlagConsPortPoolId
#define LA_TIMER_LIST_ID              gLaGlobalInfo.LaTmrListId
#define LA_TASK_ID                    gLaTaskId

/* Macros for Memory Block Allocation from Memory Pool */

#define  LA_PORTENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
         (ppu1Msg = (tLaLacPortEntry *)MemAllocMemBlk(LA_PORTENTRY_POOL_ID))

#define  LA_AGGENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
            (ppu1Msg = (tLaLacAggEntry *) MemAllocMemBlk(LA_AGGENTRY_POOL_ID))

#define  LA_AGGCONFIGENTRY_ALLOC_MEM_BLOCK\
         MemAllocMemBlk(LA_AGGCONFIGENTRY_POOL_ID)

#define  LA_AGGIFENTRY_ALLOC_MEM_BLOCK\
         MemAllocMemBlk(LA_AGGIFENTRY_POOL_ID)

#define  LA_CACHEENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
         (ppu1Msg = (tLaCacheEntry *) MemAllocMemBlk(LA_CACHEENTRY_POOL_ID))

#define  LA_MARKERENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
    (ppu1Msg = (tLaMarkerEntry *) MemAllocMemBlk(LA_MARKERENTRY_POOL_ID))

#define  LA_DLAGREMOTEAGGENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
    (ppu1Msg = (tLaDLAGRemoteAggEntry *) MemAllocMemBlk(LA_DLAGREMOTEAGGENTRY_POOL_ID))

#define  LA_DLAGREMOTEPORTENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
    (ppu1Msg = (tLaDLAGRemotePortEntry *) MemAllocMemBlk(LA_DLAGREMOTEPORTENTRY_POOL_ID))

#define  LA_DLAGCONSPORTENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
    (ppu1Msg = (tLaDLAGConsPortEntry*) MemAllocMemBlk(LA_DLAGCONSPORTENTRY_POOL_ID))

#define  LA_TIMER_ALLOC_MEM_BLOCK\
         MemAllocMemBlk(LA_TIMER_POOL_ID)



/* Macros for Freeing Memory Blocks from Memory Pools */

#define  LA_PORTENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_PORTENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_AGGENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_AGGENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_AGGCONFIGENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_AGGCONFIGENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_AGGIFENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_AGGIFENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_CACHEENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_CACHEENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_MARKERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_MARKERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_DLAGREMOTEAGGENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_DLAGREMOTEAGGENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  LA_DLAGREMOTEPORTENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_DLAGREMOTEPORTENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define LA_DLAGCONSPORTENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_DLAGCONSPORTENTRY_POOL_ID,(UINT1 *)pu1Msg)

#define  LA_TIMER_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(LA_TIMER_POOL_ID, (UINT1 *)pu1Msg)


/* UTL SLL related macros */

#define   LA_SLL_FIRST(pList)\
          TMO_SLL_First((tLaSll *)pList)

#define   LA_SLL_NEXT(pList,pNode)\
          TMO_SLL_Next((tLaSll *)pList, (tLaSllNode*)pNode)

#define   LA_SLL_LAST(pList)\
          TMO_SLL_Last((tLaSll *)pList)

#define   LA_SLL_ADD(pList, pNode)\
          TMO_SLL_Add((tLaSll *)pList, (tLaSllNode *)pNode)

#define   LA_SLL_FIND(pList, pNode)\
          TMO_SLL_Find((tLaSll *)pList, (tLaSllNode *)pNode)

#define   LA_SLL_COUNT(pList)\
          TMO_SLL_Count((tLaSll *)pList)

#define   LA_SLL_SCAN(pList, pNode, Typecast)\
          TMO_SLL_Scan((tLaSll *)pList, pNode, Typecast)

#define   LA_SLL_INIT(pList)\
          TMO_SLL_Init((tLaSll *)pList)

#define   LA_SLL_INIT_NODE(pNode)\
          TMO_SLL_Init_Node((tLaSllNode*)pNode)

#define   LA_SLL_INSERT_IN_END(pList, pNode)\
          TMO_SLL_Insert((tLaSll*)pList, LA_SLL_LAST((tLaSll *)pList), \
                                                       (tLaSllNode*)pNode)
#define   LA_SLL_INSERT(pList, pPrev, pNode)\
          TMO_SLL_Insert((tLaSll*)pList, (tLaSllNode*)pPrev, (tLaSllNode*)pNode)

#define   LA_SLL_DELETE(pList, pNode)\
          TMO_SLL_Delete((tLaSll *)pList, (tLaSllNode *)pNode)

/*CLI*/
#define LA_AUDIT_FILE_ACTIVE "/tmp/la_output_file_active"
#define LA_AUDIT_FILE_STDBY "/tmp/la_output_file_stdby"
#define LA_CLI_EOF                  2
#define LA_CLI_NO_EOF                 1
#define LA_CLI_RDONLY               OSIX_FILE_RO
#define LA_CLI_WRONLY               OSIX_FILE_WO
#define LA_CLI_MAX_GROUPS_LINE_LEN  200

/* UTL HASH TABLE related Macros */

#define   LA_CREATE_HASH_TABLE              TMO_HASH_Create_Table

#define   LA_CREATE_CACHE_HASH_TABLE        LA_CREATE_HASH_TABLE
#define   LA_CREATE_MARKER_HASH_TABLE       LA_CREATE_HASH_TABLE   

#define   LA_HASH_GET_FIRST_BUCKET_NODE     TMO_HASH_Get_First_Bucket_Node

#define   LA_HASH_GET_NEXT_BUCKET_NODE      TMO_HASH_Get_Next_Bucket_Node

#define   LA_DELETE_HASH_TABLE              TMO_HASH_Delete_Table
#define   LA_DELETE_CACHE_HASH_TABLE        LA_DELETE_HASH_TABLE
#define   LA_DELETE_MARKER_HASH_TABLE       LA_DELETE_HASH_TABLE   

#define   LA_HASH_FREE(ptr)                 TMO_FREE((VOID *)ptr)

#define   LA_HASH_INIT_NODE(pNode)\
          TMO_HASH_INIT_NODE((tLaHashNode *)pNode)

#define   LA_HASH_ADD_NODE(pHashTab, pNode, u4Index,FuncParam)\
          TMO_HASH_Add_Node((tLaHashTable *)pHashTab,\
          (tLaHashNode *)pNode,(UINT4)u4Index, (UINT1 *)FuncParam)

#define   LA_HASH_DEL_NODE(pHashTab, pNode, u4Index)\
          TMO_HASH_Delete_Node((tLaHashTable *)pHashTab, \
                                         (tLaHashNode *)pNode, u4Index)

#define   LA_HASH_SCAN_BKT(pHashTab, u4Index, pNode, Typecast)\
          TMO_HASH_Scan_Bucket((tLaHashTable *)pHashTab, u4Index,\
                                          pNode, Typecast)

#define   LA_HASH_SCAN_TBL(pHashTab, u4Index)\
          TMO_HASH_Scan_Table((tLaHashTable *)pHashTab, u4Index)


/* FSAP SRM - Timer related definitions */

#define   LA_TMR_INIT             TrmTimerInit
#define   LA_CREATE_TMR_LIST      TmrCreateTimerList
#define   LA_DEL_TMR_LIST         TmrDeleteTimerList
#define   LA_START_TMR            TmrStartTimer
#define   LA_RESIZE_TMR           TmrResizeTimer
#define   LA_STOP_TMR             TmrStopTimer
#define   LA_GET_EXPRD_TMRS       TmrGetExpiredTimers
#define   LA_GET_NEXT_EXPRD_TMR   TmrGetNextExpiredTimer
#define   LA_GET_REMAINING_TIME   TmrGetRemainingTime

/* To convert the timer duration to system timer ticks */
#define   LA_CONVERT_MSECS_TO_SYS_TICKS(u4Time) \
    u4Time = (u4Time / 10);

/* MCLAG related definitions */
#define LA_MSB_BIT_SET                   0x8000
#define LA_MSB_SLOT_ID_MASK              0x7F00
#define LA_PORT_ID_MASK                  0x00FF

/* OSIX related definitions */

#define   LA_CREATE_TASK       OsixTskCrt
#define   LA_DELETE_TASK       OsixTskDel
#define   LA_GET_TASK_ID       OsixTskIdSelf
#define   LA_SEND_EVENT        OsixEvtSend
#define   LA_RECEIVE_EVENT     OsixEvtRecv
#define   LA_CREATE_QUEUE      OsixQueCrt
#define   LA_DELETE_QUEUE      OsixQueDel
#define   LA_HTONS             OSIX_HTONS
#define   LA_NTOHS             OSIX_NTOHS
#define   LA_HTONL             OSIX_HTONL
#define   LA_NTOHL             OSIX_NTOHL
#define   LA_GET_SYS_TIME      OsixGetSysTime
#define   LA_MESG_NORMAL       OSIX_MSG_NORMAL
#define   LA_DEF_MSG_LEN       OSIX_DEF_MSG_LEN


/* CRU definitions */
#define LA_ALLOC_CRU_BUF                     CRU_BUF_Allocate_MsgBufChain
#define LA_RELEASE_CRU_BUF                   CRU_BUF_Release_MsgBufChain
#define LA_COPY_FROM_CRU_BUF                 CRU_BUF_Copy_FromBufChain
#define LA_COPY_OVER_CRU_BUF                 CRU_BUF_Copy_OverBufChain
#define LA_IS_CRU_BUF_LINEAR                 CRU_BUF_Get_DataPtr_IfLinear
#define LA_GET_CRU_BUF_VALID_BYTE_COUNT      CRU_BUF_Get_ChainValidByteCount

#define   LA_MEMCPY              MEMCPY
#define   LA_MEMCMP              MEMCMP
#define   LA_MEMSET              MEMSET
#define   LA_STRLEN              STRLEN
#define   LA_STRCAT              STRCAT
#define   LA_SPRINTF             SPRINTF

/* define Multicast and Bcast addresses */

#define LA_IS_MAC_MULTICAST(p) (((p & 0x01) == 0x01) ? LA_TRUE : LA_FALSE)

#define LA_IS_BCAST_ADDRESS(addr) (LA_MEMCMP (addr, &(LA_BCAST_ADDRESS), 6))

/* Decode Module Related  Macros used to extract fields from buffer */

/* the do/while(0) loop prevents any syntax error which would otherwise result
 * if the macro is used in a if/else statement */

#define LA_GET_1BYTE(u1Val, pu1PktBuf)\
        do {                          \
           u1Val = *pu1PktBuf;        \
           pu1PktBuf += 1;       \
        } while(0)

#define LA_GET_2BYTE(u2Val, pu1PktBuf)       \
        do {                                 \
           LA_MEMCPY (&u2Val, pu1PktBuf, 2); \
           u2Val = (UINT2) (LA_NTOHS(u2Val));          \
           pu1PktBuf += 2;              \
        } while(0)

#define LA_GET_4BYTE(u4Val, pu1PktBuf)       \
        do {                                 \
           LA_MEMCPY (&u4Val, pu1PktBuf, 4); \
           u4Val = (UINT4) (LA_NTOHL(u4Val));          \
           pu1PktBuf += 4;                   \
        } while(0)



/* Encode Module Related Macros used to assign fields to buffer */

#define LA_PUT_1BYTE(pu1PktBuf,u1Val)       \
        do {                                \
           *pu1PktBuf = u1Val;              \
           pu1PktBuf += 1;             \
        } while(0)

#define LA_PUT_2BYTE(pu1PktBuf, u2Val)        \
        do {                                  \
            u2Val = (UINT2) (LA_HTONS(u2Val));          \
            LA_MEMCPY (pu1PktBuf, &u2Val, 2); \
            pu1PktBuf += 2;              \
        } while(0)

#define LA_PUT_4BYTE(pu1PktBuf, u4Val)        \
         do {                                 \
           u4Val = (UINT4) (LA_HTONL(u4Val));           \
           LA_MEMCPY (pu1PktBuf, &u4Val, 4);  \
           pu1PktBuf += 4;                    \
         } while(0)  

/* Macro for comparing LAGG ID */
#define LA_IS_LAGG_ID_EQUAL(pPortEntry1, pPortEntry2) \
         (((LA_SYSTEM_IDS_EQUAL (pPortEntry1->LaLacActorInfo.LaSystem, \
                                 pPortEntry2->LaLacActorInfo.LaSystem) \
            == LA_TRUE) && \
            (pPortEntry1->LaLacActorInfo.u2IfKey == \
             pPortEntry2->LaLacActorInfo.u2IfKey) && \
            (LA_SYSTEM_IDS_EQUAL (pPortEntry1->LaLacPartnerInfo.LaSystem, \
                                  pPortEntry2->LaLacPartnerInfo.LaSystem) \
             == LA_TRUE) && \
            (pPortEntry1->LaLacPartnerInfo.u2IfKey == \
             pPortEntry2->LaLacPartnerInfo.u2IfKey)) \
          ? LA_TRUE : LA_FALSE)
   
/* Macro for comparing two System IDs */
#define  LA_COMPARE_SYSTEM_IDS(System1, System2)                            \
    ((!(LA_MEMCMP (System1.SystemMacAddr, &(System2.SystemMacAddr),          \
 LA_MAC_ADDRESS_SIZE)))  &&                                          \
    ((System1).u2SystemPriority == (System2).u2SystemPriority)) 

/* Macro for returning truth value if System IDs equal */
#define LA_SYSTEM_IDS_EQUAL(System1, System2)                             \
           ((LA_COMPARE_SYSTEM_IDS(System1, System2)) ? LA_TRUE : LA_FALSE) 

#define LA_HIGHER_SYSTEM_ID(System1, System2)                                       \
        (((System1).u2SystemPriority < (System2).u2SystemPriority) ||               \
         (((System1).u2SystemPriority == (System2).u2SystemPriority) &&             \
          (0 >= (LA_MEMCMP( &((System1).SystemMacAddr), &((System2).SystemMacAddr), \
                    LA_MAC_ADDRESS_SIZE )))))
           
 
#define LA_ACTOR_HAS_HIGHER_SYSTEM_ID(ActorSystem, PartnerSystem)           \
 LA_HIGHER_SYSTEM_ID(ActorSystem, PartnerSystem) ? LA_TRUE : LA_FALSE
 
/* Macro for indentifying the port with higher Port ID */
#define LA_COMPARE_PORT_IDS(pPortIfInfo, pBestPortIfInfo)                   \
       (((pPortIfInfo)->u2IfPriority < (pBestPortIfInfo)->u2IfPriority) ||  \
        (((pPortIfInfo)->u2IfPriority == (pBestPortIfInfo)->u2IfPriority)   \
                                      &&                                    \
         ((pPortIfInfo)->u2IfIndex    <  (pBestPortIfInfo)->u2IfIndex)))

/* Macro for returning truth value if Best Port has Higher Port ID */
#define LA_CHECK_PORT_HAS_HIGHER_PORT_ID(pPortIfInfo, pBestPortIfInfo)     \
      ((LA_COMPARE_PORT_IDS(pPortIfInfo, pBestPortIfInfo)) ? LA_TRUE : LA_FALSE)

#define LA_GET_LINK_SELECTION_POLICY(u2AggInd) \
        LaGetLinkSelectPolicy(u2AggInd);

#define LA_SET_LINK_SELECTION_POLICY(u2AggInd, Policy) \
     gLaGlobalInfo.aLaLacAggInfo [(u2AggInd) - 1].pLaLacAggConfigEntry->LinkSelectPolicy = (Policy);


#define LA_USE_DIST_PORT_OR_NOT(u4DistributingPortCount) \
             ((u4DistributingPortCount > 0)? LA_TRUE: LA_FALSE)
                                                   


/* Macro for getting the Aggregator IfEntry pointer */
#define LA_GET_AGGIFENTRY(pLaLacPortEntry, pLaLacAggIfEntry)                \
        do {                                                                \
          if(pLaLacPortEntry->pAggEntry != NULL) {                          \
            pLaLacAggIfEntry =                                              \
               &(pLaLacPortEntry->pAggEntry->AggIfEntry);                   \
          } else {                                                          \
             pLaLacAggIfEntry = NULL;                                       \
          }                                                                 \
        } while(0)

#define LA_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#endif /* _LAMACRO_H */
