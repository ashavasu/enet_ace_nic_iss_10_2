/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: lastdlow.h,v 1.8 2011/08/24 06:08:41 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3adTablesLastChanged ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot3adAggTable. */
INT1
nmhValidateIndexInstanceDot3adAggTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3adAggTable  */

INT1
nmhGetFirstIndexDot3adAggTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3adAggTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3adAggMACAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3adAggActorSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggActorSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3adAggAggregateOrIndividual ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggActorAdminKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggActorOperKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPartnerSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3adAggPartnerSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPartnerOperKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggCollectorMaxDelay ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot3adAggActorSystemPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggActorAdminKey ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggCollectorMaxDelay ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot3adAggActorSystemPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggActorAdminKey ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggCollectorMaxDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot3adAggTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot3adAggPortListTable. */
INT1
nmhValidateIndexInstanceDot3adAggPortListTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3adAggPortListTable  */

INT1
nmhGetFirstIndexDot3adAggPortListTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3adAggPortListTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3adAggPortListPorts ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3adAggPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3adAggPortActorSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortActorSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3adAggPortActorAdminKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortActorOperKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerAdminSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerOperSystemPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerAdminSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3adAggPortPartnerOperSystemID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot3adAggPortPartnerAdminKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerOperKey ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortSelectedAggID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortAttachedAggID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortActorPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortActorPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerAdminPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerOperPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerAdminPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortPartnerOperPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortActorAdminState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3adAggPortActorOperState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3adAggPortPartnerAdminState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3adAggPortPartnerOperState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3adAggPortAggregateOrIndividual ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot3adAggPortActorSystemPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortActorAdminKey ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortPartnerAdminSystemPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortPartnerAdminSystemID ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetDot3adAggPortPartnerAdminKey ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortActorPortPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortPartnerAdminPort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortPartnerAdminPortPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot3adAggPortActorAdminState ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot3adAggPortPartnerAdminState ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot3adAggPortActorSystemPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortActorAdminKey ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortPartnerAdminSystemPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortPartnerAdminSystemID ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2Dot3adAggPortPartnerAdminKey ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortActorPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortPartnerAdminPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortPartnerAdminPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot3adAggPortActorAdminState ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot3adAggPortPartnerAdminState ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot3adAggPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot3adAggPortStatsTable. */
INT1
nmhValidateIndexInstanceDot3adAggPortStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3adAggPortStatsTable  */

INT1
nmhGetFirstIndexDot3adAggPortStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3adAggPortStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3adAggPortStatsLACPDUsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsMarkerPDUsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsMarkerResponsePDUsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsUnknownRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsIllegalRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsLACPDUsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsMarkerPDUsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortStatsMarkerResponsePDUsTx ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot3adAggPortDebugTable. */
INT1
nmhValidateIndexInstanceDot3adAggPortDebugTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot3adAggPortDebugTable  */

INT1
nmhGetFirstIndexDot3adAggPortDebugTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot3adAggPortDebugTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot3adAggPortDebugRxState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortDebugLastRxTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortDebugMuxState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortDebugMuxReason ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot3adAggPortDebugActorChurnState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortDebugPartnerChurnState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot3adAggPortDebugActorChurnCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortDebugPartnerChurnCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortDebugActorSyncTransitionCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortDebugPartnerSyncTransitionCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortDebugActorChangeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot3adAggPortDebugPartnerChangeCount ARG_LIST((INT4 ,UINT4 *));
