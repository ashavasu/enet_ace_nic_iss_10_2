#ifndef _LAADMIN_H
#define _LAADMIN_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         
 * $Id: laadmin.h,v 1.12 2016/03/03 10:20:50 siva Exp $                                                     */
/*****************************************************************************/
/*    FILE  NAME            : laadmin.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains all default configuration   */
/*                            parameters for the Link Aggregation module to  */
/*                            come up.                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/


enum {/*
       * Default Settings for system characteristics.
       */
       LA_DEFAULT_SYSTEM_PRIORITY = 0x8000,
       LA_DEFAULT_PARTNER_SYSTEM_PRIORITY = 0,
       
       LA_DEFAULT_PORT_PRIORITY = 0x80,
       LA_DEFAULT_PARTNER_PORT_PRIORITY = 0,

       LA_DEFAULT_LACP_ACTIVITY = LA_ACTIVE,
       LA_DEFAULT_PARTNER_LACP_ACTIVITY = LA_PASSIVE,
       
       LA_DEFAULT_LACP_TIMEOUT = LA_LONG_TIMEOUT,
       LA_DEFAULT_PARTNER_LACP_TIMEOUT = LA_LONG_TIMEOUT,

       LA_DEFAULT_AGGREGATION = LA_FALSE,
       LA_DEFAULT_PARTNER_AGGREGATION = LA_TRUE,

       LA_DEFAULT_LINK_SELECT_POLICY = LA_SA_DA
};

#endif /* _LAADMIN_H */
