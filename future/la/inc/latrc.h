#ifndef _LA_TRACE_H_
#define _LA_TRACE_H_

/*****************************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: latrc.h,v 1.3 2017/10/16 09:41:00 siva Exp $
*
*
* Description: This file contains all typedefs and enums used
*              in Link Aggregation module.
*
*****************************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : latrc.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains all trace related defines   */
/*                            used in  Link Aggregation module.              */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/
/* Trace and debug flags */
#define   LA_TRC_FLAG            gLaGlobalInfo.u4TraceOption

/* Module names */
#define   LA_MOD_NAME            ((const char *)"LA")

/* Trace definitions */
#ifdef TRACE_WANTED

#define LA_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(LA_TRC_FLAG, TraceType, LA_MOD_NAME, pBuf, Length, (const char *)Str)

#define LA_TRC(TraceType, Str)                                              \
        MOD_TRC(LA_TRC_FLAG, TraceType, LA_MOD_NAME, (const char *)Str)

#define LA_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(LA_TRC_FLAG, TraceType, LA_MOD_NAME, (const char *)Str, Arg1)

#define LA_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(LA_TRC_FLAG, TraceType, LA_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define LA_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(LA_TRC_FLAG, TraceType, LA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define LA_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)                 \
        MOD_TRC_ARG4(LA_TRC_FLAG, TraceType, LA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#define LA_TRC_ARG10(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7,\
                                                           Arg8, Arg9, Arg10) \
        UtlTrcLog(LA_TRC_FLAG, TraceType, LA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3, \
                  Arg4, Arg5, Arg6, Arg7, Arg8, Arg9, Arg10)

#define LA_SYS_TRC(TraceType, Str)                             \
        SYS_LOG_MSG (((UINT4)LaGetSyslogLevel (TraceType),(UINT4) LA_SYSLOG_ID, (const char *)Str))

#define LA_SYS_TRC_ARG1(TraceType, Str, Arg1)                  \
        SYS_LOG_MSG (((UINT4)LaGetSyslogLevel (TraceType),(UINT4) LA_SYSLOG_ID, (const char *)(Str),Arg1))

#define LA_SYS_TRC_ARG2(TraceType, Str, Arg1, Arg2)            \
        SYS_LOG_MSG (((UINT4)LaGetSyslogLevel (TraceType),(UINT4) LA_SYSLOG_ID, (const char *)(Str), Arg1, Arg2))

#define LA_SYS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)      \
        SYS_LOG_MSG (((UINT4)LaGetSyslogLevel (TraceType),(UINT4) LA_SYSLOG_ID, (const char *)(Str), Arg1, Arg2, Arg3))

#define LA_SYS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4) \
        SYS_LOG_MSG (((UINT4)LaGetSyslogLevel (TraceType),(UINT4) LA_SYSLOG_ID, (const char *)(Str), Arg1, Arg2, Arg3, Arg4))

#define LA_SYS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) \
        SYS_LOG_MSG (((UINT4)LaGetSyslogLevel (TraceType),(UINT4) LA_SYSLOG_ID, (const char *)(Str), Arg1, Arg2, Arg3, Arg4,Arg5))
#else  /* TRACE_WANTED */

#define LA_PKT_DUMP(TraceType, pBuf, Length, Str)
#define LA_TRC(TraceType, Str)
#define LA_TRC_ARG1(TraceType, Str, Arg1)
#define LA_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define LA_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)

#define LA_SYS_TRC(TraceType, Str)                             

#define LA_SYS_TRC_ARG1(TraceType, Str, Arg1)                  \
{\
    UNUSED_PARAM(Arg1);\
}

#define LA_SYS_TRC_ARG2(TraceType, Str, Arg1, Arg2)            \
{\
    UNUSED_PARAM(Arg1);\
    UNUSED_PARAM(Arg2);\
}

#define LA_SYS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)      \
{\
    UNUSED_PARAM(Arg1);\
    UNUSED_PARAM(Arg2);\
    UNUSED_PARAM(Arg3);\
}

#define LA_SYS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4) \
{\
    UNUSED_PARAM (Arg1);\
    UNUSED_PARAM (Arg2);\
    UNUSED_PARAM (Arg3);\
    UNUSED_PARAM (Arg4);\
}

#define LA_SYS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) \
{\
    UNUSED_PARAM (Arg1);\
    UNUSED_PARAM (Arg2);\
    UNUSED_PARAM (Arg3);\
    UNUSED_PARAM (Arg4);\
    UNUSED_PARAM (Arg5);\
}


#endif /* TRACE_WANTED */

#endif/* _LA_TRACE_H_ */
