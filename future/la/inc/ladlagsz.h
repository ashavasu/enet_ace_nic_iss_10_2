/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: ladlagsz.h,v 1.1 2016/01/11 12:51:28 siva Exp $
 *
 * Description  :  Sizing files for DLAG
 *
 *******************************************************************/

enum {
    MAX_LA_DLAG_CONS_PORTS_IN_SYS_SIZING_ID,
    MAX_LA_DLAG_REMOTE_AGG_ENTRIES_SIZING_ID,
    MAX_LA_DLAG_REMOTE_PORT_ENTRIES_SIZING_ID,
    LA_DLAG_MAX_SIZING_ID
};


#ifdef  _LADLAGSZ_C
tMemPoolId LADlagMemPoolIds[ LA_MAX_SIZING_ID];
INT4  LaDlagSizingMemCreateMemPools(VOID);
VOID  LaDlagSizingMemDeleteMemPools(VOID);
INT4  LaDlagSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LASZ_C  */
extern tMemPoolId LADlagMemPoolIds[ ];
extern INT4  LaDlagSizingMemCreateMemPools(VOID);
extern VOID  LaDlagSizingMemDeleteMemPools(VOID);
#endif /*  _LADLAGSZ_C  */


#ifdef  _LADLAGSZ_C
tFsModSizingParams FsLADlagSizingParams [] = {
{ "tLaDLAGConsPortEntry", "MAX_LA_DLAG_CONS_PORT_ENTRIES", sizeof(tLaDLAGConsPortEntry),MAX_LA_DLAG_CONS_PORT_ENTRIES, MAX_LA_DLAG_CONS_PORT_ENTRIES,0 },
{ "tLaDLAGRemoteAggEntry", "MAX_LA_DLAG_REMOTE_AGG_ENTRIES", sizeof(tLaDLAGRemoteAggEntry),MAX_LA_DLAG_REMOTE_AGG_ENTRIES, MAX_LA_DLAG_REMOTE_AGG_ENTRIES,0 },
{ "tLaDLAGRemotePortEntry", "MAX_LA_DLAG_REMOTE_PORT_ENTRIES", sizeof(tLaDLAGRemotePortEntry),MAX_LA_DLAG_REMOTE_PORT_ENTRIES, MAX_LA_DLAG_REMOTE_PORT_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _LASZ_C  */
extern tFsModSizingParams FsLADlagSizingParams [];
#endif /*  _LASZ_C  */


