/* $Id: lared.h,v 1.34 2015/03/03 13:20:35 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2005-2006                         */
/*****************************************************************************/
/*    FILE  NAME            : lared.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : L2RED TEAM                                     */
/*    DESCRIPTION           : This file contains all constants used in the   */
/*                            Link Aggregation redundancy module.            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    23 May 2005/          Initial Create.                          */
/*            ISS Team                                                       */
/*---------------------------------------------------------------------------*/

#ifndef _LA_RED_H_
#define _LA_RED_H
#ifdef CLI_WANTED
#include "cli.h"
#endif
/* Data structures needed for La redundancy module. */

typedef struct LaSyncedUpInfo {
    tLaSysTime       ReceivedTime;
    UINT2            u2PortActorKey;
    UINT1            u1TimerCount;
    UINT1            u1Pad;
}tLaRedSyncedUpInfo;

typedef struct _LaHwSyncInfo
{
    UINT2 u2HwAggIdx;
    UINT1 u1AggStatus;
    UINT1 u1Reserved;
}tLaHwSyncInfo;

typedef struct LaRedGlobalInfo {
    tLaRedSyncedUpInfo      LaSyncUpTable [LA_MAX_PORTS];
    /* Contains information sent by active node whenever a LACPDU is 
     * received on a port.
     */ 
    tLaHwSyncInfo            aLaRedHwAggInfo [LA_MAX_AGG];
#ifdef NPAPI_WANTED
    tLaTaskId               AuditTaskId;
#endif
    UINT2                   u2BulkUpdNextPort;
    /* Next port for which bulk update has to be generated. */
    UINT1                   u1NumPeersUp; 
    /* Indicates number of standby nodes that are up. */
    BOOL1                   bBulkReqRcvd;
    /* To check whether bulk request recieved from standby before 
     * RM_STANDBY_UP event. */
#ifdef NPAPI_WANTED
    UINT1                   u1IsAuditStopped;
    /* Indicates whether Audit is stopped or not. */
    UINT1                   u1LaPortHwStatusSyncUp; 
    /* Indicates whether HW Port Status is received from 
     * active or not */
    UINT1                   au1Reserved[2];
#endif
    /* HITLESS RESTART */
    UINT1                   u1StdyStReqRcvd; 
    /* Steady State Request Received Flag - OSIX_TRUE/OSIX_FALSE.
     * This flag is set when the Steady State packet request is received from
     * RM after finishing the bulk storage for Hitless Restart */
    UINT1                   au1Pad[3];
}tLaRedGlobalInfo;

typedef UINT4 tLaHwId;


/* Constants to be used of LA redundancy module. */
#define LA_RED_TRC                   0x00000100
#define LA_RM_FRAME                  5

/* HITLESS RESTART - macro value changes */
#define LA_BULK_REQ_MSG              RM_BULK_UPDT_REQ_MSG
#define LA_BULK_UPD_TAIL_MSG         RM_BULK_UPDT_TAIL_MSG
#define LA_LACP_SYNC_MSG             4
#define LA_CURR_SPLIT_TMR_EXP_MSG    5
#define LA_CLEAN_SYNCUP_ENTRIES_MSG  6
#define LA_NP_SYNC_MSG               7
#define LA_WAIT_WHILE_TMR_EXP_MSG    8
#define LA_PORT_OPER_STATUS_MSG      9 
#define LA_PORT_HW_STATUS_SYNCUP_MSG 10
/* D-LAG macros added */
#define LA_DLAG_SYNC_MSG                 11 /*D-LAG sub type msg*/
#define LA_DLAG_RED_ADD_AGG_INFO         12 /*Adding Remote Aggresgator subtype*/
#define LA_DLAG_RED_DEL_AGG_INFO         13 /*Deleting Remote Aggresgator subtype*/
#define LA_DLAG_RED_DEL_REM_PORT_INFO    14 /*Deleting Remote Port subtype*/
/*D-LAG subtype */
#define DLAG_SUB_TYPE_SIZE           2     /* Size of the D-LAG subtype*/
/* DLAG subtype msg size */
#define LA_DLAG_SYNC_MSG_SIZE        4     /*Size of D-LAG sub type sync message */
#define LA_DLAG_DEL_AGG_MSG_SIZE     10    /*Size of D-LAG delete aggregator msg*/
#define LA_DLAG_DEL_PORT_MSG_SIZE    14    /*Size of D-LAG delete port msg */
#define LA_DLAG_MAX_AGG_MSG_SIZE     512   /*Size of D-LAG add aggregator msg */
/* Macros for shifting the bytes in RM msg */
#define LA_ONE_BYTE                  1
#define LA_TWO_BYTE                  2
#define LA_FOUR_BYTE                 4
/* HITLESS RESTART */
#define LA_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ
#define LA_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define LA_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL
#define LA_HR_STATUS()               RmGetHRFlag()
#define LA_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define LA_HR_STDY_ST_REQ_RCVD()     gLaRedGlobalInfo.u1StdyStReqRcvd
/* the following macro checks the packet is sent as periodic PDU or not.
 * If the 32 byte position is set as 0x3d,then Active is in Distributing state.
 * and if the 52 byte position is set as 0x3d, then Partner is in 
 * distributing state. These 2 states determines the packet is sent in
 * steady state. */
#define LA_HR_CHECK_STDY_ST_PKT(au1LinBuf)\
        ((au1LinBuf[32] == 0x3d) && (au1LinBuf[52] == 0x3d))

#define LA_PORT_HW_STATUS_RCVD       1
#define LA_PORT_HW_STATUS_NOT_RCVD   2

#define LA_RED_LENGTH_SIZE           2  /* Number of bytes occupied by 
                                         * sync mesg length filed. */
#define LA_SYNC_MSG_TYPE_SIZE        4

#define LA_PORT_ID_SIZE              4
#define LA_PORT_STATE_SIZE           1
#define LA_PORT_STATUS_SIZE          1

#define LA_TMR_COUNT_SIZE                  1
#define LA_CURR_SPLIT_TMR_MSG_SIZE     (LA_PORT_ID_SIZE + LA_TMR_COUNT_SIZE)
#define LA_WAIT_WHILE_TMR_EXP_MSG_SIZE  LA_PORT_ID_SIZE
#define LA_PORT_OPER_STATUS_MSG_SIZE    (LA_PORT_ID_SIZE + LA_PORT_STATUS_SIZE)
#define LA_PORT_HW_STATUS_SYNCUP_MSG_SIZE  (LA_PORT_ID_SIZE + LA_PORT_STATE_SIZE)
#define LA_SYNC_PORT_MSG_SIZE              37

#define LA_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                         * protocols can start writing. */

/* Maximum no of sub updates in bulk updation process. */
#define LA_RED_NO_OF_PORTS_PER_SUB_UPDATE       10 

/* NP Leve Subtypes to be sent in the RM message. */
#define LA_NP_SUB_TYPE_SIZE          2
#define LA_NP_ADD_INFO               1
#define LA_NP_DEL_INFO               2
#define LA_NP_ADD_PORT_SUCCESS_INFO  3
#define LA_NP_ADD_PORT_FAILURE_INFO  4
#define LA_NP_DEL_PORT_SUCCESS_INFO  5
#define LA_NP_DEL_PORT_FAILURE_INFO  6

#define LA_NP_HWINFO_SIZE            4

/* Bulk Update messages are split in to 1500 bytes messages and sent. The
 * following macro is for this size. */
#define LA_BULK_SPIT_MSG_SIZE        1500

/* Macros needed for LA redundancy module. */

/* Check whether the existing partnerstate variable in the port
 * and the actor state variables are same. */
#define LA_RED_IS_PARTNR_ST_CHANGED(pLaLacpduInfo, pPortEntry)              \
        (                                                                   \
         ((pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaDistributing) != \
          (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaDistributing)) || \
         ((pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaCollecting) !=    \
          (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaCollecting) ) ||   \
         ((pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaLacpActivity) !=  \
          (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaLacpActivity) ) || \
         ((pLaLacpduInfo->LaLacActorInfo.LaLacPortState.LaLacpTimeout) !=   \
          (pPortEntry->LaLacPartnerInfo.LaLacPortState.LaLacpTimeout) )     \
        )? LA_TRUE : LA_FALSE

#define LA_NODE_STATUS()         gLaNodeStatus
        
#ifdef RM_WANTED
#define  LA_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? LA_TRUE: LA_FALSE)
#else
#define  LA_IS_NP_PROGRAMMING_ALLOWED() LA_TRUE
#endif

#define LA_INIT_NODE_STATUS()    (gLaNodeStatus = LA_NODE_IDLE)

#define LA_PROTOCOL_ADMIN_STATUS() gLaGlobalInfo.u1LaAdminStatus

#define LA_GET_SYNCUP_ENTRY(u2Port) \
         &(gLaRedGlobalInfo.LaSyncUpTable[(u2Port)-1])

#define LA_GET_AGGID_FROM_PORTENTRY(pPortEntry) \
             (pPortEntry)->LaLacActorAdminInfo.u2IfKey

#define LA_GET_NUM_STANDBY_NODES() gLaRedGlobalInfo.u1NumPeersUp

#define LA_INIT_NUM_STANDBY_NODES() \
                      gLaRedGlobalInfo.u1NumPeersUp =0;

#define LA_IS_STANDBY_UP() \
          ((gLaRedGlobalInfo.u1NumPeersUp > 0) ? LA_TRUE : LA_FALSE)

#define LA_BULK_REQ_RECD()  gLaRedGlobalInfo.bBulkReqRcvd

#define LA_RM_GET_NODE_STATUS()  LaRedGetNodeStateFromRm ()
#define LA_RM_GET_STATIC_CONFIG_STATUS()  LaRmGetStaticConfigStatus ()

#define LA_RM_GET_NUM_STANDBY_NODES_UP() \
      gLaRedGlobalInfo.u1NumPeersUp = LaRmGetStandbyNodeCount ()

#define LA_NUM_STANDBY_NODES() gLaRedGlobalInfo.u1NumPeersUp

#define LA_INIT_PROTOCOL_ADMIN_STATUS() \
      LA_PROTOCOL_ADMIN_STATUS () = LA_DISABLED;

#define LA_RED_GET_AGG_ENTRY(u2AggIndex) \
        &(gLaRedGlobalInfo.aLaRedHwAggInfo [u2AggIndex - LA_MAX_PORTS- 1])

/* Macros to write in to RM buffer. */
#define LA_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), (u4MesgType)); \
        *(pu4Offset) += 4;\
}while (0)

#define LA_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), (u2MesgType)); \
        *(pu4Offset) += 2;\
}while (0)

#define LA_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), (u1MesgType)); \
        *(pu4Offset) += 1;\
}while (0)

#define LA_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET((pdest), (psrc), *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define LA_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define LA_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define LA_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define LA_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#ifdef NPAPI_WANTED
#define LA_AUDIT_TASK           (UINT1 *)"LaAd"

#define LA_AUDIT_TASK_PRIORITY  220

#define LA_RED_AUDIT_TSKID()    (gLaRedGlobalInfo.AuditTaskId)

#define LA_RED_IS_AUDIT_STOP() \
        (gLaRedGlobalInfo.u1IsAuditStopped) 

#define LA_GET_NEXT_PORT_FROM_HWAGG(u2Index, u2Port, pHwInfo) \
 for (u2Index=0; (u2Port=pHwInfo->au4LPortList[u2Index]), u2Index < pHwInfo->u1NumPorts; u2Index ++)

#define LA_HW_LOOP_ON_AGGREGATORS(u2HwId, pHwInfo) \
        while ((u2HwId = LaFsLaGetNextAggregator ((u2HwId), (pHwInfo))) != LA_INVALID_HW_AGG_IDX)
#endif

/* Prototypes for LA redundancy module. */

INT4 LaRedSendMsgToRm (tRmMsg *pMsg, UINT2 u2BufSize);
INT4 LaRedMakeNodeActive (VOID);
INT4 LaRedMakeNodeStandbyFromIdle (VOID);

VOID LaRedStartTimers (VOID);
VOID
LaRedFormSyncMsgForPort (tLaLacPortEntry *pPortEntry,
                         tLaLacIfInfo *pPartnerInfo,
                         UINT4 *pu4Offset, tRmMsg **ppMsg);

INT4
LaRedCopyPartnerInfoIntoRMBuf (tLaLacIfInfo * pLaLacIfInfo,
                               UINT4 *pu4Offset, tRmMsg **ppRmBuffer);
VOID 
LaRedStoreSyncMsgforPort (UINT2  u2Port, 
                          tLaRedSyncedUpInfo *pLaPortSyncInfo);
INT4
LaRedLacStandbyConstruct (tLaLacPortEntry *pPort,
                          tLaLacpduInfo * pLaLacpduInfo);

VOID 
LaRedReStartCurrTimer (tLaLacPortEntry *pPortEntry, tLaSysTime SyncedTime,
                       UINT1 u1TimerCount);
INT4 LaRedSendBulkReq (VOID);

VOID
LaRedHandleSyncUpMessage (tRmMsg  *pMesg, UINT2 u2Length);

INT4
LaRedGetPortInfoFromRMMesg (tRmMsg  *pRmBuf, UINT4  *pu4Offset,
                            UINT4 *pu4IfIndex,
                            tLaRedSyncedUpInfo  *pLaPortSyncInfo);
VOID LaRedHandleGoStandbyEvent (VOID);
VOID LaRedHandleStandByUpEvent (VOID *pvPeerId);
VOID LaRedHandleBulkReqEvent (VOID *pvPeerId);
VOID LaRedHandleCurrWhileTmrExp (UINT4 u4IfIndex, UINT1 u1Count);
VOID LaRedHandleWaitWhileTmrExp (UINT4 u4IfIndex);
VOID LaRedHandlePortOperStatus (UINT4 u4IfIndex, UINT1 u1PortOperStatus);

VOID LaRedSendPduOnAllPort (VOID);
VOID LaRedCleanSyncUpEntries (VOID);
VOID LaRedGetNodeStateFromRm (VOID);
INT4 LaRedStoreSyncUpInfoForOneAgg (tRmMsg  *pMesg, UINT4 *pu4Offset);
INT4 LaRedProcessDLAGSyncMsg (tRmMsg  *pMesg, UINT4 *pu4Offset);
INT4 LaDLAGRedProcessAddRemAggEntry (tRmMsg  *pMesg, UINT4 *pu4Offset);
INT4 LaDLAGRedProcessDelRemAggEntry (tRmMsg  *pMesg, UINT4 *pu4Offset);
INT4 LaDLAGRedProcessDelRemPortEntry (tRmMsg  *pMesg, UINT4 *pu4Offset);
INT4 LaDLAGRedSendDelRemoteAggEntry (tLaLacAggEntry *pAggEntry,
                                     tLaDLAGRemoteAggEntry *pRemAggEntry);
INT4 LaDLAGRedSendDelRemotePortEntry (tLaDLAGRemoteAggEntry *pRemAggEntry,
                                      tLaDLAGRemotePortEntry *pRemPortEntry);
INT4 LaDLAGRedSendAddRemoteAggorPortEntry (tLaLacAggEntry *pAggEntry);
VOID LaDLAGRedAggBulkSyncUp (VOID);
VOID LaDLAGRedSyncInfoPerAgg (tRmMsg *pMsg, UINT4 *pu4Offset, UINT2 u2AggIndex);
VOID LaRedAggBulkSyncUp (VOID);
VOID LaRedWaitWhileTmrBulkSyncUp (UINT2 u2StartIfIndex);
VOID LaRedCurrWhileTmrBulkSyncUp (UINT2 u2StartIfIndex);
VOID LaRedPortOperStatusBulkSyncup (UINT2 u2StartIfIndex);
VOID LaRedLacpBulkSyncup (UINT2 u2StartIfIndex);
UINT2 LaRedGetAggBulkMsgSize (UINT2 u2AggIndex);
VOID LaRedSyncInfoForOneAgg (tRmMsg *pMsg, UINT4 *pu4Offset, UINT2 u2AggIndex);
INT4 LaRedSyncModifyAggPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex, 
                                  tLaHwId HwInfo);
INT4 LaRedSyncModifyPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex, UINT2 u2PortNum);
VOID LaRedAddSyncAggEntry (UINT2 u2AggIndex, UINT2 u2HwIndex);
VOID LaRedDelSyncAggEntry (UINT2 u2AggIndex);
VOID LaRedProcessLacpSyncMsg (tRmMsg *pMesg, UINT4 *pu4Offset, 
                              UINT2 *pu2Length);
VOID LaRedProcessCurrSplitTmrExp (tRmMsg *pMesg, UINT4 *pu4Offset, 
                                  UINT2 *pu2Length);
VOID LaRedProcessWaitWhileTmrExp (tRmMsg *pMesg, UINT4 *pu4Offset, 
                                  UINT2 *pu2Length);
VOID LaRedProcessPortOperStatus (tRmMsg * pMesg, UINT4 *pu4Offset,
                                UINT2 *pu2Length);


#ifdef NPAPI_WANTED
VOID LaRedPortHwStatusBulkSyncUp(UINT2 u2StartIfIndex);
VOID LaRedAddSyncPortEntry(UINT2 u2LaNpSubType,UINT2 u2AggIndex,UINT2 u2PortNum);
VOID LaRedDelSyncPortEntry(UINT2 u2LaNpSubType,UINT2 u2AggIndex,UINT2 u2PortNum);
VOID LaRedSelfUpdatePortHwStatus(VOID); 
VOID LaRedProcessPortHwStatusSyncUpMsg(tRmMsg *pMesg, UINT4 *pU4Offset, 
                                       UINT2 *pu2Length);
INT4 LaRedUpdateSwFromAggInHw(INT4 i4HwAggIndex, tLaHwInfo *pwInfo);
VOID LaRedHandlePortHwStatusSyncUp(UINT4 u4IfIndex, UINT1 u1RcvdPortHwStatus);
#endif 
VOID LaRedTrigHigherLayer (UINT1 u1TrigType);
VOID LaRedHandleBulkUpdateEvent (VOID);
INT4 LaRedSendBulkUpdateTailMsg (VOID);
#ifdef CLI_WANTED
VOID LaRedShowPortSyncInfo (tCliHandle CliHandle, UINT2 u2Port, 
                            tLaRedSyncedUpInfo *pSyncUpEntry);
#endif
#endif
