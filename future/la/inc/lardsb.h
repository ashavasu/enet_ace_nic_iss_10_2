/* $Id: lardsb.h,v 1.10 2011/09/12 07:13:26 siva Exp $ */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2005-2006                                          */
/*****************************************************************************/
/*    FILE  NAME            : lardsb.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : L2RED TEAM                                     */
/*    DESCRIPTION           : This file contains all constants used in the   */
/*                            Link Aggregation module if redundancy feature  */
/*                            is not required.                               */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    23 May 2005/          Initial Create.                          */
/*            ISS Team                                                       */
/*---------------------------------------------------------------------------*/

/* Data Structures */
typedef UINT4 tLaHwId;

#define LA_NP_ADD_INFO               1
#define LA_NP_DEL_INFO               2
#define LA_NP_ADD_PORT_SUCCESS_INFO  3
#define LA_NP_ADD_PORT_FAILURE_INFO  4
#define LA_NP_DEL_PORT_SUCCESS_INFO  5
#define LA_NP_DEL_PORT_FAILURE_INFO  6

#define LA_RM_GET_NODE_STATUS()  (gLaNodeStatus = LA_ACTIVE)

#define LA_RM_GET_NUM_STANDBY_NODES_UP()

#define LA_NUM_STANDBY_NODES()

#define LA_INIT_NUM_STANDBY_NODES()

#define LA_IS_STANDBY_UP()       LA_FALSE

#define LA_NODE_STATUS()         gLaNodeStatus /* This is set to active in LaHandleInit*/

#define LA_IS_NP_PROGRAMMING_ALLOWED()  LA_TRUE 

#define LA_INIT_NODE_STATUS()    (gLaNodeStatus = LA_NODE_ACTIVE)

#define LA_PROTOCOL_ADMIN_STATUS() gLaGlobalInfo.u1LaAdminStatus

#define LA_INIT_PROTOCOL_ADMIN_STATUS() \
      LA_PROTOCOL_ADMIN_STATUS () = LA_DISABLED

#define LA_RED_IS_PARTNR_ST_CHANGED(pLaLacpduInfo, pPortEntry)   LA_FALSE
/* HITLESS RESTART */
#define LA_HR_STDY_ST_REQ_RCVD()   OSIX_FALSE

INT4 LaRedSyncModifyAggPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex, 
                                  tLaHwId HwInfo);
VOID LaRedAddSyncAggEntry (UINT2 u2AggIndex, UINT2 u2HwIndex);
VOID LaRedDelSyncAggEntry (UINT2 u2AggIndex);
INT4 LaRedSyncModifyPortsInfo (UINT2 u2LaNpSubType, UINT2 u2AggIndex, UINT2 u2PortNum);

