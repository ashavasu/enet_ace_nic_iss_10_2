/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: laproto.h,v 1.97 2017/11/16 14:26:54 siva Exp $
 *
 * Description: This file contains prototypes of all functions
 *              in Link Aggregation module.
 *
 *****************************************************************************/
#ifndef _LAPROTO_H
#define _LAPROTO_H
#include "cli.h"
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : laproto.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 27 March 2002                                  */
/*    AUTHOR                : BridgeTeam                                     */
/*    DESCRIPTION           : This file contains prototypes of all functions */
/*                         in Link Aggregation module.                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    07 March 2002/          Initial Create.                        */
/*            Bridge Team                                                    */
/*---------------------------------------------------------------------------*/

/* latmr.c */
INT4 LaTaskInit (VOID);
INT4 LaHandleInit (VOID);
INT4 LaHandleShutDown (VOID);
VOID LaHandlePktEnqEvent (VOID);
INT4 LaCreatePortChannelInterface (UINT4 u4IfIndex);

INT4 LaTimerInit ( VOID );

INT4 LaTimerDeInit (VOID);

VOID LaTimerHandler ( VOID );

INT4 LaStartTimer (tLaAppTmrNode *pLaAppTmrNode);

INT4 LaStopTimer (tLaAppTmrNode *pTmr);

VOID LaHandleCtrlEvents (VOID);

INT4 LaRegisterWithPacketHandler (VOID);

INT4 LaProcessLacpFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex);

VOID LaAssignMempoolIds PROTO ((VOID));

/* laagg.c */


INT4 LaAggParseFrame (UINT1 *pu1LinearBuf, tLaLacPortEntry *pLaLacPortEntry,
                      tLaLacAggIfEntry *pLaLacAggIfEntry);

INT4 LaAggProcessMarkerPdu(UINT1 *pu1MarkerPdu, tLaLacPortEntry *pLaLacPortEntry,
                           tLaLacAggIfEntry *pLaLacAggIfEntry);

INT4 LaAggPurgeEntry (tLaLacPortEntry *pLaLacPortEntry);

INT4 LaAggSelectLink (tLaBufChainHeader *pBuf, UINT2 u2AggIndex,
                      UINT2 *pu2SelectedPortIndex);

VOID LaAggGetMask (UINT2 u2AggIndex,UINT1 *pu1Threshold, 
                     UINT2 *pu2Mask, UINT2 *pu2NumOfBits);

INT4 LaAggGetValidPort (UINT2 *pau2ActivePortArray, UINT2 u2Result,
                        UINT1 u1PortCount, UINT2 u2AggIndex,
                        tLaMacAddr *pSrcMacAddr, tLaMacAddr *pDestMacAddr,
                        UINT2 u2Vlan, UINT4 u4Isid,
                        UINT4 u4MplsVcLabel, UINT4 u4MplsTunnelLabel,
                        UINT4 u4MplsVcTunnelLabel,
                        UINT2 *pu2SelectedPortIndex);   

INT4 LaAggMultiplexFrame (tLaBufChainHeader *pBuf,
                          tLaLacPortEntry *pLaLacPortEntry,
                          tLaPktParams *pPktParams,
                          tLaBoolean bIsMacClientFrame);

INT4 LaHandleCacheTmrExpiry (tLaCacheEntry *pLaCacheEntry); 

INT4 LaAggGetPortFromCacheTable (tLaBufChainHeader *pBuf,
                                 tLaCacheEntry *pLaCacheEntry,
                                 UINT2 *pu2SelectedPortIndex);

INT4 LaAggDeleteMarkerEntry (tLaLacPortEntry *pLaLacPortEntry);

INT4 LaAggDeleteCacheEntry (tLaLacPortEntry *pLaLacPortEntry);


/* lamarker.c */

INT4 LaAggGenerateMarkerPdu (tLaLacPortEntry *pLaLacPortEntry); 

INT4 LaAggFormMarkerRequestPdu (tLaMacAddr *pLaSrcMacAddress,
                                UINT2 u2PortIndex, UINT4 u4TransactionId,
                                tLaBufChainHeader *pMarkerRequestPdu);

INT4 LaAggRespondToMarkerRequest (UINT1 *pMarkerRequestPdu,
                                  tLaLacPortEntry *pLaLacPortEntry,
                                  tLaLacAggIfEntry *pLaLacAggIfEntry);

INT4 LaAggReceiveMarkerResponse (UINT1 *pMarkerResponsePdu,
                                 tLaLacPortEntry *pLaLacPortEntry);   

INT4 LaAggMaintainMarkerRate (UINT2 u2AggIndex, UINT1 u1Action);


/* lactrlpm.c */
INT4 LaHandOverOutFrame (tLaBufChainHeader * pBuf,
                         UINT2 u2PortIndex, tLaPktParams * pPktParams);

INT4
LaIsThisLAControlPacket (tLaBufChainHeader * pBuf,tLaMacAddr *pLaDestMacAddress);

INT4
LaEnqueueControlFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex);

/* larxm.c */

tLaBoolean LaLacIfSamePort(tLaLacIfInfo    *pLaLacAInfo,
                           tLaLacIfInfo    *pLaLacBInfo);

tLaBoolean LaLacMatchIfInfo(tLaLacIfInfo  *pAIfInfo,
                            tLaLacIfInfo  *pBIfInfo); 

INT4 LaLacCopyIfInfo(tLaLacIfInfo  *pFromIfInfo,
                     tLaLacIfInfo  *pToIfInfo);

INT4 LaLacRecordActorAdminIfInfo(tLaLacPortEntry  *pLaLacPortEntry); 

INT4 LaLacRecordPartnerOperIfInfo(tLaLacpduInfo    *pLaLacpduInfo,
                                  tLaLacPortEntry  *pLaLacPortEntry);

INT4 LaLacRecordPartnerAdminIfInfo(tLaLacPortEntry  *pLaLacPortEntry); 

VOID LaLacUpdateSelectedFromReceivedLacpdu(tLaLacpduInfo   *pLaLacpduInfo,
                                           tLaLacPortEntry *pLaLacPortEntry);

VOID LaLacUpdateSelectedFromPartnerAdmin(tLaLacPortEntry 
                                         *pLaLacPortEntry);

VOID LaLacUpdateNtt(tLaLacPortEntry  *pLaLacPortEntry,
                    tLaLacpduInfo    *pLaLacpduInfo);

VOID LaLacInitializeCurrentWhileTimer(tLaLacPortEntry  *pLaLacPortEntry,
                                      tLaLacpTimeout   LaLacpTimeout);

INT4 LaLacRxmPortInitialize(tLaLacPortEntry  *pLaLacPortEntry);

VOID LaLacRxmPortDisabled(tLaLacPortEntry  *pLaLacPortEntry); 

VOID LaLacRxmLacpDisabled(tLaLacPortEntry  *pLaLacPortEntry); 

VOID LaLacRxmExpired(tLaLacPortEntry  *pLaLacPortEntry); 

VOID LaLacRxmDefaulted(tLaLacPortEntry  *pLaLacPortEntry);

VOID LaLacRxmCurrent(tLaLacPortEntry  *pLaLacPortEntry,
                     tLaLacpduInfo    *pLaLacpduInfo);
INT4 LaLacRxmUpdateMachines (tLaLacPortEntry *pLaLacPortEntry, 
      tLaSelect PrevAggSelected, tLaLacPortState PrevPartnerState);

INT4 LaLacRxMachine(tLaLacPortEntry    *pLaLacPortEntry,
                    tLaRxSmEvent       LaEvent,
                    tLaLacpduInfo      *pLaLacpduInfo);

INT4 LaLacReceivePduInfo(tLaLacPortEntry   *pLaLacPortEntry,
                         UINT1             *pu1LinearBuf); 

INT4 LaLacpSelectionLogic PROTO ((tLaLacPortEntry *));

/* lamux.c */

INT4 LaLacMuxControlMachine(tLaLacPortEntry  *pLaLacPortEntry,
                            tLaMuxSmEvent      LaEvent);
VOID LaLacMuxMachineInit(VOID);
INT4 LaMuxStateDetached (tLaLacPortEntry * pLaLacPortEntry);
INT4 LaMuxStateWaiting(tLaLacPortEntry * pLaLacPortEntry);
tLaBoolean LaMuxCheckIsReady(tLaLacPortEntry *pLaLacPortEntry);
tLaBoolean LaMuxAnyPortWaitWhileTimerRunning (tLaLacPortEntry * pLaLacPortEntry);

INT4 LaMuxWaitWhileExpWaiting(tLaLacPortEntry * pLaLacPortEntry);
INT4 LaMuxStateAttached(tLaLacPortEntry * pLaLacPortEntry);
INT4 LaMuxStateCollecting(tLaLacPortEntry * pLaLacPortEntry);
INT4 LaMuxStateDistributing(tLaLacPortEntry * pLaLacPortEntry);

INT4 LaCreateAggregator( UINT2 u2AdminKey ,UINT4 u4Index, tLaLacAggEntry **ppRetAggEntry);
INT4 LaDeleteAggregator (tLaLacAggEntry *pAggEntry);

VOID LaDeleteAllAggEntries(VOID);

INT4 LaAddPortToAggregator (tLaLacAggEntry *pAggEntry, tLaLacPortEntry *pTmpPortEntry);
INT4 LaAddDynPortToAggregator (tLaLacAggEntry *, tLaLacPortEntry *);
VOID LaRemovePortFromAggregator(tLaLacPortEntry *pTmpPortEntry);

INT4 LaHlCreatePhysicalPort(tLaLacPortEntry *pPortEntry);
INT4 LaHlDeletePhysicalPort(tLaLacPortEntry *pPortEntry);


INT4 LaSelectLacpAggGroup(tLaLacAggEntry *pAggEntry);
INT4 LaSelectManualAggGroup(tLaLacAggEntry *pAggEntry);
INT4  LaSortOnStandbyConstraints(tLaLacPortEntry **ppPortEntry, UINT2 u2PortCount);

VOID LaSelectRefPortForAdminInfo (tLaLacAggEntry * pAggEntry,
                                  tLaLacPortEntry ** ppPortEntry);

tLaBoolean LaUnSelectGroupIfRefPortLAGChanged (tLaLacPortEntry * pPortEntry);

/* laconfig.c */

INT4  LaEnableLacpOnPort(tLaLacPortEntry  *pLaLacPortEntry);
INT4  LaDisableLacpOnPort(tLaLacPortEntry  *pLaLacPortEntry);
INT4 LaEnableManualConfigOnPort(tLaLacPortEntry  *pLaLacPortEntry);
INT4  LaDisableManualConfigOnPort(tLaLacPortEntry  *pLaLacPortEntry);
INT4  LaChangeLacpMode(tLaLacPortEntry  *pLaLacPortEntry, 
                       tLaLacpMode      OldLacpMode,
                       tLaLacpMode      NewLacpMode);
VOID LaUpdateTableChangedTime(VOID);
INT4 LaUpdateActorSystemID(tLaMacAddr NewMacAddr);
INT4 LaIsValidSystemID(tLaMacAddr NewMacAddr);

INT4 LaChangeAggMacAddress(UINT4  ,tLaLacAggEntry *pLaLacAggEntry);
VOID LaChangeAggSpeed(tLaLacAggEntry *pAggEntry);
INT4 LaChangePortActorAdminPort (UINT2 u2Port, UINT2 u2AdminPort);
INT4 LaChangePartnerPortPriority (UINT2 u2PortIndex, UINT2 u2PortPriority);
INT4 LaChangePartnerPortKey (UINT2 u2PortIndex, UINT2 u2PortKey);
INT4 LaChangePartnerPortNumber (UINT2 u2PortIndex, UINT2 u2PortNumber);
INT4 LaChangePartnerSystemPriority (UINT2 u2PortIndex, UINT2 u2SystemPriority);
INT4 LaUpdatePartnerSystemID (UINT2 u2Index, tLaMacAddr NewMacAddr);

/* lasys.c */

VOID LaHandleInitFailure(VOID); 

INT4 LaHandleCreatePort (UINT2 u2PortNum);

INT4 LaHandleDeletePort (UINT2 u2PortNum);

VOID LaHandleEnablePort (tLaLacPortEntry *pPortEntry);

VOID LaHandleDisablePort (tLaLacPortEntry *pPortEntry);


    
    

VOID LaInitPort(tLaLacPortEntry  *pLaLacPortEntry);

INT4 LaEnable(VOID); 

INT4 LaDisable(VOID);

INT4 LaChangeActorAdminKey(UINT2  u2PortIndex, UINT2  u2PortKey);

INT4 LaChangeActorSystemPriority (UINT2 u2PortIndex,
                                  UINT2 u2SystemPriority);

INT4 LaLacHwControl(tLaLacPortEntry   *pLaLacPortEntry,
                    tLaHwEvent       LaEvent);

INT4 LaCalculateAggOperStatus (tLaLacAggEntry *pAggEntry, 
                               UINT1 u1AggAdminStatus);
INT4 LaCalculateAggMemberOperStatus (tLaLacAggEntry *pAggEntry, 
                               UINT1 u1AggAdminStatus);
INT4 LaUpdateLlPortChannelStatus(tLaLacAggEntry *pLaLacAggEntry);

tLaBoolean LaIsPortPartnerPresent(UINT2 u2PortIndex);

INT4 LaChangeActorPortPriority(UINT2  u2PortIndex, UINT2 u2PortPriority);

VOID  LaInitialisePortEntry(tLaLacPortEntry  *pLaLacPortEntry);

INT4 LaHandleRxCtrlFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                       UINT2 *pu2AggPortIndex, UINT1 u1FrameType);

INT4 LaHandleRxDataFrame (tLaBufChainHeader * pBuf, UINT2 u2PortIndex,
                       UINT2 *pu2AggPortIndex, UINT1 u1FrameType);

INT4 LaAggCreated(UINT2 u2AggIndex, UINT2 u2HwAggIndex);

INT4 LaLinkAddedToAgg(UINT2 u2AggIndex, UINT2 u2PortIndex);

/* latxm.c */

INT4 LaLacTxLacpdu(tLaLacPortEntry   *pLaLacPortEntry);

INT4 LaLacTxMachine(tLaLacPortEntry  *pLaLacPortEntry,
                    tLaTxSmEvent      LaEvent);

INT4 LaLacPeriodicTxMachine(tLaLacPortEntry  *pLaLacPortEntry,
                            tLaPTxSmEvent      LaEvent);

INT4 LaLacPTxSmMakeNoPeriodic(tLaLacPortEntry * pLaLacPortEntry);
INT4 LaLacPTxSmMakePeriodicTx(tLaLacPortEntry * pLaLacPortEntry);
INT4 LaLacPTxSmMakeFastPeriodic(tLaLacPortEntry * pLaLacPortEntry);
INT4 LaLacPTxSmMakeSlowPeriodic(tLaLacPortEntry * pLaLacPortEntry);


VOID LaReleaseCacheMemBlock (tLaHashNode *pLaHashEntry);

/* lautils.c */
VOID LaAggGetNextTransactionId (UINT2 u2AggIndex, UINT4 *pu4TransactionId);

VOID LaAggCacheEntryTableHashFn (tLaMacAddr *pLaMacAddress, UINT4 *pu4HashIndex);
INT4 LaFillAggMtu PROTO ((UINT2 u2AggIndex, UINT4 u4Mtu));

INT4 LaFillAggPauseAdminMode (UINT2 u2AggIndex, INT4 i4PauseAdminMode);

VOID
LaUtilGetPortState (UINT2 u2IfIndex, tLaLacPortState ** pLaLacPortState,
                UINT4 u4Value);

INT4 LaGetDefPortEntryForAgg PROTO ((tLaLacAggEntry *,tLaLacPortEntry **));
INT4 LaSetSelectionPolicy (tLaLacAggEntry *, INT4);
INT4 LaSetSelectionPolicyBitList (tLaLacAggEntry *, INT4);
#ifdef NPAPI_WANTED
INT4 LaHandleNpCallbackEvents (tLaIntfMesg * pLaMsg);
INT4 LaHandleAggCreateFailed PROTO ((UINT2 u2AggId));
INT4 LaHandleLinkAdditionFailed PROTO ((UINT2 u2AggId, UINT2 u2PortIndex));
INT4 LaHandleLinkRemoveFailed PROTO ((UINT2 u2AggId, UINT2 u2PortIndex));
VOID LaNpFailSendTrapAndSyslog PROTO ((tLaNpTrapLogInfo *));
VOID LaNpFailSyslog (tLaNpTrapLogInfo *pNpLogInfo);
VOID LaNpFailNotify (tLaNpTrapLogInfo *pNpLogInfo);
INT4 LaUpdatePortChannelStatusToHw (UINT2 u2AggId, 
                      UINT1 u1StpState, UINT1 u1UpdateFlag);
#endif

INT4 LaIsValidAggIndex PROTO ((UINT2 u2IfIndex));

 /* Aggregator Table Entry access routines */
INT4 LaGetAggEntry (UINT2 u2AggIndex, tLaLacAggEntry **ppLaLacAggEntry);
INT4 LaGetNextAggEntry (tLaLacAggEntry *pAggEntry, 
                        tLaLacAggEntry **pNextAggEntry);
INT4 LaGetAggEntryByKey(UINT2 u2Key, tLaLacAggEntry **ppRetAggEntry);
INT4 LaGetAggEntryBasedOnPort (UINT2 u2PortIndex, 
                               tLaLacAggEntry **ppAggEntry);
VOID LaAddToAggTable (tLaLacAggEntry *pAggEntry);
VOID LaDeleteFromAggTable (tLaLacAggEntry *pAggEntry);

 /* Aggregator Port Table Entry access routines */
INT4 LaAddToAggPortTable (tLaLacAggEntry *pAggEntry, 
                          tLaLacPortEntry *pPortEntry);
INT4 LaGetNextAggPortEntry (tLaLacAggEntry *pAggEntry, 
                            tLaLacPortEntry *pPortEntry, 
                            tLaLacPortEntry **ppNextPortEntry);
INT4 LaGetNextAggDistributingPortEntry (tLaLacAggEntry *pAggEntry, 
                            tLaLacPortEntry *pPortEntry, 
                            tLaLacPortEntry **ppNextPortEntry);
INT4 LaUtilGetNextAggPort (UINT2 u2AggIndex, UINT2 u2Port, UINT2 *pu2NextPort);
INT4 LaGetNextAggDistributingPort (UINT2 u2AggIndex, UINT2 u2Port, 
                                   UINT2 *pu2NextPort);
INT4 LaDeleteFromAggPortTable (tLaLacAggEntry *pAggEntry, 
                               tLaLacPortEntry *pPortEntry);

INT4 LaGetLinkSelectPolicy(UINT2 u2AggIndex);
INT4 LaIsValidPortState(INT4 i4PortState);

 /* Port Table Entry access routines */
INT4 LaGetPortEntry (UINT2 u2PortIndex, tLaLacPortEntry **ppLaLacPortEntry);
INT4 LaGetNextPortEntry (tLaLacPortEntry *pPortEntry, tLaLacPortEntry ** ppNextPortEntry);
INT4 LaAddToPortTable (tLaLacPortEntry *pPortEntry);
INT4 LaDeleteFromPortTable (tLaLacPortEntry *pPortEntry);

VOID LaGetAggConfigEntry (UINT2 u2AggIndex,
                          tLaLacAggConfigEntry **ppLaLacAggConfigEntry);

VOID LaGetAggIfEntry (UINT2 u2AggIndex,
                      tLaLacAggIfEntry **ppLaLacAggIfEntry);

VOID LaGetAggPortListEntry (UINT2 u2AggIndex, 
                            tLaPortList **ppLaLacAggPortListEntry);

INT4 LaValidatePortEntry (UINT2 u2PortIndex);

INT4 LaValidateAggEntry (UINT2 u2AggIndex);

VOID LaGetPortState (tLaLacPortState *pLaLacPortState, UINT1 *pu1PortState);

INT4 LaChangeActorPortState(UINT2 u2PortIndex, UINT1 u1PortStateVar,
                           UINT1 u1Set);

VOID LaChangePartnerPortState (UINT2 u2PortIndex, UINT1 u1PortStateVar,
                               UINT1 u1Value);

INT4 LaIncrementStatsOnCollectorDiscard (tLaBufChainHeader *pBuf,
                                         tLaLacPortEntry *pLaLacPortEntry,
                                         tLaMacAddr *pLaDestMacAddress);

INT4 LaIncrementStatsOnDistributorDiscard (tLaBufChainHeader *pBuf,
                                           tLaLacPortEntry *pLaLacPortEntry);

INT4 LaCopyPduBufInfo(tLaBufChainHeader   *pFromPduBuf, 
                      UINT1               **ppu1ToLinearBuf,UINT4 u4ByteCount);

INT4 LaCopyIntoLacpduInfo(UINT1          *pu1LinearBuf,
                          tLaLacpduInfo  *pLaLacpduInfo);

tLaBoolean LaLacCheckTimerExpiry(tLaTicks  *pTimerCounter);

INT4 LaLacFormLacpdu(tLaLacPortEntry  *pLaLacPortEntry,
                     UINT1            *pu1LinearBuf);

INT4 LaCopyParticipantInfoIntoBuffer(tLaLacIfInfo  *pLaLacIfInfo,
                                     UINT1         **ppPduBuffer,
                                     UINT2         u2InfoType);

INT4 LaLacGetActivePorts(UINT2   u2AggIndex,
                         UINT2   *pLinearBuf,
                         UINT1   *pu1PortCount);

INT4 LaLacTickTmrExpiry(tLaLacPortEntry  *pLaLacPortEntry);

INT4 LaLacPortChangeAggregation(tLaLacPortEntry  *pLaLacPortEntry);

INT4 LaLacUpdateStats(tLaLacPortEntry   *pLaLacPortEntry,
                      tLaPortStateFlag  LaPortStateFlag);

INT4  LaLacUpdatePortList(tLaLacPortEntry   *pLaLacPortEntry,
                          tLaPortStateFlag  LaPortStateFlag);

VOID  LaLinkStatusNotify (UINT4 u4IfIndex, UINT1 u1OperStatus,
                         UINT1 u1AdminStatus, UINT1 u1TrapType);

VOID
LaDLAGRoleChangeNotify (UINT2 u2AggIndex, UINT4 u4DLAGTrapType);

VOID
LaErrorRecTrigNotify (tLaLacPortEntry *pPortEntry, UINT1 u1RecoveryTmrReason, UINT1 u1HwFailType);


#ifdef SNMP_2_WANTED
tSNMP_OID_TYPE  *LaMakeObjIdFromDotNew (INT1 *pi1TextStr);
#endif

VOID LaMemFreeVarBindList (tSNMP_VAR_BIND *pVarBindLst);

VOID LaFreeVarbind (tSNMP_VAR_BIND *pVarBind);
    

INT4 LaCustCallBack (UINT4, tLaCallBackArgs *);

#ifdef TRACE_WANTED
/* Functions added for IT */
VOID LaPrintIfInfo(tLaLacPortEntry  *pLaLacPortEntry,
                   tLaLacIfInfo *pLacpduActorInfo, 
                   tLaLacIfInfo  *pPortPartnerInfo);

VOID LaPrintPortStates(tLaLacPortEntry  *pLaLacPortEntry);

VOID LaDisplayCacheTable (VOID);
#endif

tLaBoolean
LaIsPortInDefaulted (UINT2 u2PortIndex);

INT4
LaGetPortBundleState (UINT4 u4IfIndex, UINT1 *pui1PortBundleState);

UINT1
LaIsMCLAGEnabledOnAgg (tLaLacAggEntry *pAggEntry);

INT4
LaUtilClearPortChannelCounters (UINT2 u2AggIndex);

/* laport.c */

INT4 LaVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId, 
                                     UINT2 *pu2LocalPortId);
INT4
LaL2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status);

INT4 LaCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4 LaCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);

INT4 LaCfaSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);
INT4 LaCfaUfdIsLastDownlinkInGroup (UINT4 u4IfIndex);

INT4 LaCfaDeletePortChannel (UINT4 u4IfIndex);

INT4 LaCfaGetIfCounters (UINT2 u2IfIndex, tIfCountersStruct * pIfCounter);

VOID LaCfaGetSysMacAddress (tMacAddr SwitchMac);

VOID LaCfaNotifyProtoToApp (UINT1 u1ModeId, 
                            tNotifyProtoToApp NotifyProtoToApp);

INT4 LaCfaGetIfBridgedIfaceStatus (UINT4 u4IfIndex, UINT1 *pu1Status);

extern INT4 CfaIfmConfigNetworkInterface (UINT1 u1OperCode, UINT4 u4IfIndex,
                                        UINT4 u4Flag, tIpConfigInfo * pIpConfigInfo);
extern INT4 CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName);

INT4 LaPnacGetStartedStatus (VOID);

INT4 LaDsCheckDiffservPort (INT4 i4TrunkPort);

INT4 LaDsGetStartedStatus (VOID);

VOID LaIssFreeAggregatorMac (UINT4 u4AggIndex);

INT4 LaIssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac);

INT4 LaIssMirrorStatusCheck (UINT2 u2Port);

INT4 LaIssMirrIsPortConfigAllowed (UINT4 u4IfIndex);

INT4 LaIssGetAsyncMode (INT4 i4ProtoId);

INT4 LaL2IwfGetNextValidPort (UINT2 u2IfIndex, UINT2 *pu2NextPort);

VOID LaL2IwfCreatePortIndicationFromLA (UINT4 u4IfIndex, 
                                        UINT1 u1PortOperStatus);

INT4 LaL2IwfDeleteAndAddPortToPortChannel (UINT2 u2IfIndex, UINT2 u2AggId);

INT4 LaL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT2 u2IfIndex, 
                               UINT1 *pu1OperStatus);

INT4 LaL2IwfGetPortPnacAuthControl (UINT2 u2IfIndex, 
                                    UINT2 *pu2PortAuthControl);

VOID LaL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                           UINT1 *pu1TunnelStatus);

INT4 LaL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT2 u2IfIndex, UINT4 u4PktSize, 
                                     UINT2 u2Protocol, UINT1 u1Encap);

INT4 LaL2IwfLaHLPortOperIndication (UINT2 u2IfIndex, UINT1 u1OperStatus);

INT4 LaL2IwfPbSetDefPortType (UINT2 u2IfIndex);

INT4 LaL2IwfPbSetPcPortTypeToPhyPort (UINT4 u4PcIfIndex, UINT4 u4PhyIfIndex);

INT4 LaL2IwfRemovePortFromPortChannel (UINT2 u2IfIndex, UINT2 u2AggId);

INT4 LaL2IwfIsLaConfigAllowed (UINT4 u4IfIndex);

INT4 LaL2IwfIsPortChannelConfigAllowed (UINT4 u4IfIndex);

INT4 LaActiveDLAGGetBridgeIndexFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4BrgIndex);

INT4 LaActiveDLAGGetIfIndexFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4IfIndex);

INT4 LaActiveDLAGGetSlotIdFromBridgeIndex (UINT4 u4BrgIndex, UINT4 *pu4SlotId);

INT4 LaActiveDLAGCheckIsLocalPort(UINT2 u2PortNumber);

UINT1 LaActiveDLAGGetRolePlayed(VOID);

INT4 LaActiveDLAGGetSwitchMacAddress (UINT1 *pu1HwAddr);

INT4 LaActiveDLAGValidateSourceMacAddress (UINT1 *pu1HwAddr);

INT4 LaGetActiveSlotNo (INT4 *pi4SlotNumber);

INT4 
LaCfaGetLinkTrapEnabledStatus (UINT2 u2Port, UINT1 *pu1TrapStatus);

INT4 LaVlanUpdatePortIsolationForMcLag (UINT4 u4IfIndex, UINT1 u1Action);
#ifdef L2RED_WANTED
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
UINT4 LaAstHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);

#else
#ifdef VLAN_WANTED
UINT4 LaVlanRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, 
                                   UINT2 u2DataLen);
#else
#ifdef LLDP_WANTED
UINT4 LaLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif
#endif
#endif

UINT4 LaRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, 
                              UINT4 u4SrcEntId, UINT4 u4DestEntId);

UINT4 LaRmGetNodeState (VOID);

UINT1 LaRmGetStandbyNodeCount (VOID);

UINT1 LaRmGetStaticConfigStatus (VOID);

UINT1 LaRmHandleProtocolEvent (tRmProtoEvt *pEvt);

UINT4 LaRmRegisterProtocols (tRmRegParams * pRmReg);

UINT4 LaRmDeRegisterProtocols (VOID);

UINT4 LaRmReleaseMemoryForMsg (UINT1 *pu1Block);

INT4 LaRmSetBulkUpdatesStatus (UINT4 u4AppId);
#endif
INT4
LaRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
LaGetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
LaCliGetShowCmdOutputToFile (UINT1 *);

INT4
LaCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
LaCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

INT4 LaParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value);


#ifdef ICCH_WANTED

UINT4
LaIcchGetRolePlayed (VOID);

INT4
LaCfaCreateIcchIPInterface (UINT2 u2InstanceId);


VOID
LaVlanAddIsolationTablForIccl (VOID);

VOID
LaVlanDelIsolationTblForIccl (VOID);

UINT1
LaIcchHandleProtocolEvent (tIcchProtoEvt *pEvt);

UINT4
LaIcchEnqMsgToIcchFromAppl (tIcchMsg *pIcchMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId);
INT4
LaIcchSetBulkUpdatesStatus (UINT4 u4AppId);

INT4
LaMCLAGSnmpLowGetFirstValidRemotePortChannelIndex (INT4 *pi4FsLaPortChannelIfIndex,
                                                  tMacAddr * pFsLaDLAGRemotePortChannelSystemID);
INT4
LaMCLAGSnmpLowGetNextValidRemotePortChannelIndex (INT4 i4FsLaPortChannelIfIndex,
                                                 INT4 *pi4NextFsLaPortChannelIfIndex,
                                                 tMacAddr FsLaDlagRemotePortChannelSysID,
                                                 tMacAddr * pNextFsLaDLAGRemotePortChannelSystemID);

INT4
LaMCLAGSnmpLowGetFirstValidRemotePortIndex (INT4 *pi4FsLaPortChannelIfIndex,
                                           tMacAddr *pFsLaDLAGRemotePortChannelSystemID,
                                           INT4 *pi4FsLaDLAGRemotePortIndex);
INT4
LaMCLAGSnmpLowGetNextValidRemotePortIndex (INT4 i4FsLaPortChannelIfIndex,
                                          INT4 *pi4NextFsLaPortChannelIfIndex,
                                          tMacAddr FsLaDlagRemotePortChannelSysID,
                                          tMacAddr *pNextFsLaDLAGRemotePortChannelSystemID,
                                          INT4 i4FsLaDLAGRemotePortIndex,
                                          INT4 *pi4NextFsLaDLAGRemotePortIndex);

#endif
VOID
LaIcchGetIcclIfIndex (UINT4 *pu4IcclIfIndex); 

VOID
LaIcchSetIcclIfIndex (UINT4 u4IfIndex);

UINT4
LaLLdpUpdateLLdpStatusOnIccl (UINT4 u4IfIndex, UINT1 u1AggCap);

INT4 LaMCLAGSystemControl (tCliHandle CliHandle,INT4 i4Status);
/* lal2wr.c */

INT4 LaL2IwfAddActivePortToPortChannel (UINT2 u2IfIndex, UINT2 u2AggId);

INT4 LaL2IwfRemoveActivePortFromPortChannel (UINT2 u2IfIndex, 
                                             UINT2 u2AggId);

INT4 LaL2IwfGetPortChannelForPort (UINT2 u2IfIndex, UINT2 *pu2AggId);

INT4 LaL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode);
/* lachurn.c */

VOID LaLacUpdateActorChurnState(tLaLacPortEntry  *pLaLacPortEntry); 

VOID LaLacUpdatePartnerChurnState(tLaLacPortEntry  *pLaLacPortEntry); 

INT4 LaLacChurnDetection(tLaLacPortEntry  *pLaLacPortEntry,
                         tLaChurnEvent      LaEvent);

INT4 LaAggDeleteCacheTableWithAggIndex (UINT2 u2AggIndex);

/* lastdlow.c */


INT4 LaSnmpLowValidatePortIndex (INT4 i4Dot3adIndex);
INT4 LaSnmpLowGetFirstValidPortIndex (INT4 *pi4Dot3adFirstIndex);   
INT4 LaSnmpLowGetNextValidPortIndex (INT4 i4Dot3adIndex, INT4 *pi4Dot3adNextIndex);
INT4 LaSnmpLowValidateAggIndex (INT4 i4Dot3adIndex);
INT4 LaSnmpLowGetFirstValidAggIndex (INT4 *pi4Dot3adFirstIndex);   
INT4 LaSnmpLowGetNextValidAggIndex (INT4 i4Dot3adIndex, INT4 *pi4Dot3adNextIndex);


PUBLIC VOID
LaNotifyL2Vpn ( tLaLacAggEntry *pAggEntry,UINT1 u1DlagNodeStatus);

#ifdef MBSM_WANTED
INT4 LaMbsmUpdateCardInsertion ( tMbsmPortInfo *, tMbsmSlotInfo *);

#endif
INT4 LaRedRegisterWithRM (VOID);
INT4 LaRedDeRegisterWithRM (VOID);
INT4 LaRedSyncUpPartnerInfo (tLaLacPortEntry *pPortEntry, 
                             tLaLacpduInfo *pLaLacpduInfo);
INT4 LaRedSyncUpCurrWhileTmrExpiry (UINT2 u2PortIndex, UINT1  u1TimerCount);
INT4 LaRedSyncUpWaitWhileTmrExpiry (UINT2 u2PortIndex);
INT4 LaRedSyncUpPortOperStatus (UINT2 u2PortIndex, UINT1 u1PortOperStatus);
INT4 LaRedSyncProtocolDisable (VOID);
VOID LaRedHandleRmEvents (tLaIntfMesg * pLaMsg);
/* HITLESS RESTART */
VOID LaRedHRProcStdyStPktReq  (VOID);
INT1 LaRedHRSendStdyStTailMsg (VOID);
INT1 LaRedHRSendStdyStPkt     (UINT1 *pu1LinBuf, UINT4 u4PktLen, UINT2 u2Port,
                               UINT4 u4TimeOut);
VOID LaRedHandleDynSyncAudit (VOID);
VOID LaExecuteCmdAndCalculateChkSum  (VOID);

tRBTree
LaRBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp);
PUBLIC INT4
LaMacCmpForLaDLAGRemoteAggEntry(tRBElem * pE1, tRBElem * pE2);
VOID
LaDLAGRemoteAggInfoTreeDestroy (tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg);

INT4
LaActiveDLAGInitParams (VOID);
extern INT4 CfaIvrSetIpAddress (tCliHandle, UINT4, UINT4,  UINT4);

INT4 LaHandleRecoveryTmrExpiry (tLaLacPortEntry *pPortEntry);

/* lamclag.c */
INT4 LaActiveMCLAGUpdateRemoteMCLAGNodeList (tLaMacAddr RemoteSysMac,
                                             UINT2 u2RemoteSysPriority,
                                             UINT1 *pu1LinearBuf, INT4 i4SllAction);

INT4 LaActiveMCLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                        UINT1 *pu1LinearBuf, INT4 i4SllAction);

INT4 LaActiveMCLAGHwControl (tLaLacPortEntry * pPortEntry, UINT1 LaLacEvent);

INT4 LaActiveMCLAGFormDSU (tLaLacAggEntry * pAggEntry, UINT1 **ppu1LinearBuf,
                           tLaDLAGTxInfo * pLaDLAGTxInfo, UINT4 *pu4DataLength);

INT4 LaActiveMCLAGGetAddDeletePortFromDSU (tLaLacAggEntry * pAggEntry,
                                            UINT1 *pu1PktBuf, UINT1 u1PduType);

INT4 LaActiveMCLAGUpdateConsolidatedList (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                            INT4 i4SllAction);

INT4 LaActiveMCLAGFormAddDeleteDSU (tLaLacAggEntry * pAggEntry, UINT1 **ppu1LinearBuf,
                                    tLaDLAGTxInfo * pLaDLAGTxInfo,
                                    UINT4 *pu4DataLength);



INT4 LaActiveMCLAGMasterUpdateConsolidatedList (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGCalculateAggOperStatus (tLaLacAggEntry * pAggEntry,
                                    UINT1 u1AggAdminStatus);

INT4 LaActiveMCLAGUpdateTreeOnPropSame (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                  UINT1 *pu1SortingReq);

INT4 LaActiveMCLAGMasterProcessAddDeleteLinks (tLaLacAggEntry * pAggEntry,
                                         UINT4 u4PortBrgIndex,
                                         UINT1 u1PortStateFlag);

INT4 LaActiveMCLAGUpdateTreeOnPropGreater (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                     UINT1 *pu1SortingReq);

INT4 LaActiveMCLAGUpdateTreeOnPropLesser (tLaDLAGRemoteAggEntry * pRemoteAggEntry,
                                    UINT1 *pu1SortingReq);

INT4 LaActiveMCLAGMasterUpdateTreeOnPropSame (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGMasterUpdateTreeOnPropGreater (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGMasterUpdateTreeOnPropLesser (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGLinkAddedToAgg (UINT2 u2AggIndex, UINT2 u2PortIndex);

VOID LaActiveMCLAGChangeAggSpeed (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGMasterDownIndication (VOID);




INT4 LaActiveMCLAGMasterUpIndication (VOID);

INT4 LaActiveMCLAGChkForBestConsInfo (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGSortOnStandbyConstraints (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGInitParams (VOID);

VOID LaActiveMCLAGScanLaListAndInitMCLAG (VOID);

INT4 LaActiveCopyGlobalAndInitMCLAG (tLaLacAggEntry * pAggEntry);

INT4 LaActiveMCLAGEnable (VOID);

INT4 LaActiveMCLAGTxDSU (tLaLacAggEntry * pAggEntry, tLaDLAGTxInfo * pLaDLAGTxInfo);

INT4 LaActiveMCLAGTxEventDSU (tLaLacAggEntry * pAggEntry,
                        tLaDLAGTxInfo * pLaDLAGTxInfo);

INT4 LaActiveMCLAGTxPeriodicSyncOrAddDelPdu (VOID);



INT4 LaActiveMCLAGKeepAlive (VOID);

INT4 LaActiveMCLAGProcessMCLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf,
                                   UINT4 u4ByteCount);

INT4 LaActiveMCLAGProcessSyncPDU (tLaMacAddr RemoteSysMac,
                            UINT2 u2RemoteSysPriority, UINT1 *pu1LinearBuf,
                            INT4 i4SllAction);

INT4 LaActiveMCLAGProcessPeriodicUpdateDSU (UINT1 *pu1LinearBuf);

INT4 LaMCLAGInit (tLaLacAggEntry *pAggEntry);

INT4 LaActiveMCLAGDisable (VOID);
VOID
LaMCLAGCheckMasterSlaveLinkAvail (tLaLacAggEntry * pAggEntry, UINT1 *pu1PiMask);

INT4
LaMCLAGDeInit (tLaLacAggEntry * pAggEntry);
#ifdef EVPN_VXLAN_WANTED
INT4
LaUtilEvpnGetMCLAGSystemID (tMacAddr * pRetValEvpnMCLAGSystemID,
                            INT4 *pi4EvpnMCLAGSystemPriority);
#endif /* EVPN_VXLAN_WANTED */

VOID
LaIcchProcessMclagOperDown (UINT4 u4IfIndex);

VOID
LaIcchProcessMclagOperDownAtMaster (UINT4 u4IfIndex, tLaLacAggEntry *pAggEntry);

VOID
LaVlanProcessMclagOperUp (UINT4 u4IfIndex);

VOID
LaVlanIcchProcessMclagDown (UINT4 u4IfIndex,  UINT1 u1Status);

VOID
LaIcchProcessMclagOperDownAtSlave (UINT4 u4IfIndex);

VOID
LaVlanIcchCheckPortStatus (UINT4 u4IfIndex);

UINT4
LaCfaGetGlobalModTrc(VOID);

INT1
LaVlanUpdateMclagStatus (UINT2 u2IfIndex, UINT1 u1MclagStatus);

VOID LaSnoopNotifyCardRemovalEvent (VOID);

INT4 LaNoMclag(UINT4 u4InstanceId);

UINT2 LaGetSyslogLevel (UINT2 u2TraceType);

VOID
LaSyslogPrintAcivePorts (tLaLacPortEntry * pPortEntry);

VOID
LaUtilTicksToDate (UINT4 u4Ticks, UINT1 *pu1DateFormat);

VOID
LaGetSameState (UINT1 u1PortState, UINT1 *pu1RecSameState);

VOID
LaIssRetrieveEnhancedLBStatus (UINT4 au4PortChannelList[],
        UINT4 * pu4NumOfPortChannels);

#endif  /* _LAPROTO_H */   
