/*****************************************************************************/
/* Copyright (C) 2014 Aricent Inc . All Rights Reserved                      */
/* License Aricent Inc., 2001-2002                                           */
/* $Id: ladlagaa.h,v 1.7 2015/09/15 12:59:36 siva Exp $                       */
/*****************************************************************************/
/*    FILE  NAME            : ladlagaa.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : LINK AGGREAGTION                               */
/*    MODULE NAME           : Active-Active Distributed Link Aggregation     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 31 Mar 2014                                    */
/*    AUTHOR                : Active-Active DLAG team                        */
/*    DESCRIPTION           : This file contains function prototypes for     */
/*                            the functions added to support Active - Active */
/*                            Distributed Link Aggregation and Multi chassi  */
/*                            Link  Aggregation feature.                     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 Mar 2014/          Initial Create.                          */
/*            DISS Team                                                      */
/*---------------------------------------------------------------------------*/


#ifndef _LADLAGAA_H
#define _LADLAGAA_H

/* Functions used in Active-Active DLAG */

INT4
LaActiveDLAGFormDSU (tLaLacAggEntry * pAggEntry, UINT1 **pu1LinearBuf,
                     tLaDLAGTxInfo * pLaDLAGTxInfo,UINT4 *pu4DataLength );

INT4 LaActiveDLAGProcessDLAGPdu (UINT2 u2RecvdPortIndex, UINT1 *pu1LinearBuf,UINT4 u4ByteCount);

INT4
LaActiveDLAGUpdateRemotePortList (tLaDLAGRemoteAggEntry *pRemoteAggEntry,                                                                           UINT1 *pu1LinearBuf, INT4 i4SllAction);
INT4
LaActiveDLAGUpdateRemoteDLAGNodeList (tLaMacAddr RemoteSysMac, UINT2 RemoteSysPriority,UINT1 *pu1LinearBuf,
                                INT4 i4SllAction);
VOID LaActiveDLAGFillPortSpeedInPdu(tLaLacAggEntry *pAggEntry,tIssPortCtrlSpeed *pIssPortCtrlSpeed);

VOID LaActiveDLAGGetPortSpeedFromPdu (UINT1 u1Speed, tCfaIfInfo * pIfInfo );


INT4
LaActiveDLAGFormAddDeleteDSU (tLaLacAggEntry * pAggEntry, UINT1 **pu1LinearBuf,
                                    tLaDLAGTxInfo * pLaDLAGTxInfo,UINT4 *pu4DataLength);
INT4
LaActiveDLAGUpdateConsolidatedList(tLaDLAGRemoteAggEntry * pRemoteAggEntry, INT4 i4SllAction);

INT4 LaActiveDLAGMasterUpdateConsolidatedList (tLaLacAggEntry *pAggEntry);

INT4
LaActiveDLAGGetAddDeletePortFromDSU (tLaLacAggEntry * pAggEntry,
                                     UINT1 *pu1PktBuf,UINT1 u1PduType);

INT4 LaActiveDLAGChkUpdateAggEntryConsInfo(tLaLacAggEntry * pAggEntry,tLaDLAGSpeedPropInfo ComPareInfo);

PUBLIC INT4
LaCmpPortNo(tRBElem * pE1, tRBElem * pE2);

PUBLIC INT4
LaCmpPortPriority(tRBElem * pE1, tRBElem * pE2);

INT4 LaActiveDLAGCreateConsPortEntry(tLaDLAGConsPortEntry **ppConsPortEntry,UINT4 u4PortIndex,UINT2 u2Priority);

INT4
LaActiveDLAGAddToConsPortListTree (tLaLacAggEntry * pAggEntry,tLaDLAGConsPortEntry* pConsPortEntry);

VOID
LaActiveDLAGDestroyPortListTree(tRBTree Tree, tRBKeyFreeFn FreeFn, UINT4 u4Arg);

INT4
LaActiveDLAGGetPortEntry (UINT4 u4RemotPortIndex, tLaDLAGConsPortEntry ** ppConsPortEntry, tLaLacAggEntry * pAggEntry);

INT4
LaActiveDLAGRemovePortEntry (tLaLacAggEntry * pAggEntry, tLaDLAGConsPortEntry *pConsPortEntry);

INT4
LaActiveDLAGGetNextPortEntry (tLaDLAGConsPortEntry * pConsPortEntry, 
                    tLaDLAGConsPortEntry **ppNextConsPortEntry, tLaLacAggEntry * pAggEntry);

INT4
LaActiveDLAGAddPortEntry (tLaLacAggEntry * pAggEntry, tLaDLAGConsPortEntry * pConsPortEntry);

INT4
LaActiveDLAGRemoveRemotePortFromPortList (tLaLacAggEntry * pAggEntry, UINT4 u4RemotePortIndex);

INT4
LaActiveDLAGRemovePortEntryFromConsTree (tLaLacAggEntry * pAggEntry,tLaDLAGConsPortEntry * pConsPortEntry);


INT4
LaActiveDLAGGetNextPortEntryFromConsTree (tLaDLAGConsPortEntry *pConsPortEntry,
                                         tLaDLAGConsPortEntry **ppConsPortEntry,tLaLacAggEntry * pAggEntry);

INT4 LaActiveDLAGUpdateTreeOnPropSame (tLaDLAGRemoteAggEntry * pRemoteAggEntry,UINT1 *);
INT4 LaActiveDLAGUpdateTreeOnPropGreater (tLaDLAGRemoteAggEntry * pRemoteAggEntry,UINT1 *);
INT4 LaActiveDLAGUpdateTreeOnPropLesser (tLaDLAGRemoteAggEntry* pRemoteAggEntry,UINT1 *);

INT4 LaActiveDLAGMasterUpdateTreeOnPropSame(tLaLacAggEntry *pAggEntry);
INT4 LaActiveDLAGMasterUpdateTreeOnPropGreater(tLaLacAggEntry *pAggEntry);
INT4 LaActiveDLAGMasterUpdateTreeOnPropLesser(tLaLacAggEntry *pAggEntry);

INT4 LaActiveDLAGChkForBestConsInfo(tLaLacAggEntry * pAggEntry);

INT4 LaActiveDLAGSortOnStandbyConstraints(tLaLacAggEntry *pAggEntry);

INT4 LaActiveDLAGChkAddDelDSURequired(tLaLacAggEntry * pAggEntry);

INT4 LaActiveDLAGDeleteConsPortEntry( tLaDLAGConsPortEntry *pTmpConsPortEntry);

VOID LaActiveDLAGDeleteEntriesInAllTree (tLaLacAggEntry * pAggEntry);

VOID LaDLAGActivePrintAllTreePorts(tLaLacAggEntry * pAggEntry);

INT4 LaActiveDLAGMasterProcessAddDeleteLinks(tLaLacAggEntry* pAggEntry,UINT4 u4PortBrgIndex,UINT1 u1PortStateFlag);

INT4 LaActiveDLAGMasterDownIndication PROTO ((VOID));

INT4 LaActiveDLAGMasterUpIndication PROTO ((VOID));


INT4 LaActiveDLAGHwControl(tLaLacPortEntry * pPortEntry, UINT1 LaLacEvent);

INT4 LaActiveDLAGLinkAddedToAgg (UINT2 u2AggIndex, UINT2 u2PortIndex);

VOID
LaActiveDLAGChangeAggSpeed (tLaLacAggEntry * pAggEntry);

VOID LaActiveDLAGPrintBestConsInfo(tLaLacAggEntry *pAggEntry);

INT4 LaActiveDLAGCalculateAggOperStatus(tLaLacAggEntry *pAggEntry,UINT1 );

VOID LaActiveDLAGScanLaListAndInitDLAG (VOID);

INT4 LaActiveCopyGlobalAndInitDLAG ( tLaLacAggEntry *pAggEntry);

INT4 LaActiveDLAGEnable PROTO((VOID));

INT4 LaActiveDLAGDisable PROTO((VOID));

INT4 LaActiveDLAGTxPeriodicSyncOrAddDelPdu PROTO((VOID));

INT4
LaActiveDLAGTxDSU (tLaLacAggEntry *pAggEntry,tLaDLAGTxInfo * pLaDLAGTxInfo);

INT4
LaActiveDLAGTxEventDSU (tLaLacAggEntry * pAggEntry, tLaDLAGTxInfo * pLaDLAGTxInfo);

INT4 LaActiveDLAGKeepAlive PROTO ((VOID));

INT4 LaActiveDLAGProcessSyncPDU(tLaMacAddr RemoteSysMac,
                                 UINT2 u2RemoteSysPriority, UINT1 *pu1LinearBuf, INT4 i4SllAction);
INT4
LaActiveDLAGProcessPeriodicUpdateDSU (UINT1 *pu1LinearBuf);

INT4 LaActiveDLAGHandleCardDelete PROTO ((INT4));

VOID LaGetDissMasterSlotId PROTO ((UINT4 *pu4SlotId));

INT4 LaGetMacAddressFromSlotId PROTO ((UINT4 u4SlotId, tMacAddr *pDestMacAddr));

INT4 LaSetMacAddressToSlotId PROTO ((tMacAddr DestMacAddr, UINT4 u4SlotId));

INT4 LaGetSlotIdFromMacAddress (tMacAddr MacAddr,UINT4 *pu4SlotId);

VOID LaMCLAGProcessPeerUpEvent (VOID);

VOID LaMCLAGProcessPeerDownEvent (VOID);

INT4
LaMCLAGUpdateRemoteLagAdminStatus (tLaMacAddr RemoteSysMac, tLaLacAggEntry * pAggEntry, UINT1 u1AdminStatus);

INT4
LaLacGetMCLAGActivePorts (UINT2 u2AggIndex, UINT2 *pLinearBuf, UINT1 *pu1PortCount);

#endif  /* _LADLAGAA_H */
