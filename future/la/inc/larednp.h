/*$Id: larednp.h,v 1.8 2011/11/14 12:16:09 siva Exp $*/
#ifndef _LAREDNP_H_
#define _LAREDNP_H_

#define LA_HW_MAX_AGG_GRPS          8

/* Data structure to be used for NPAPI level. */
typedef struct _LaRedPortsInSwAgg
{
    UINT2     u2Port;   /* Active port  in sw aggregation. */
    UINT2     u2HwSync; /* Is Hw is in sync with sw for this entry. */
}tLaRedPortsInSwAgg;

/* Prototypes used by LA redundancy module at NP level. */
VOID LaRedDeleteAggregatorFromHw (UINT2 u2AggIndex);

INT4 FsLaRedIsTrunkPresentInHw (UINT2 u2AggIndex);
INT4 FsLaGetHwInfoIntoLaHwEntry (UINT2 u2AggIndex);
VOID FsLaRedWriteSwInfoInHw (UINT2 u2AggIndex, 
                             tLaRedPortsInSwAgg *pSwPortList, 
                             UINT2 u2SwNumPorts, UINT1 u1LinkSelectPolicy);

INT4 LaRedSyncHwAggIndex (UINT4  u4AggIndex, UINT4  u4HwIndex);
VOID LaRedPerformAudit (VOID);
VOID LaRedAuditTaskMain (INT1 *pi1Param);
INT4 LaRedGetSwAggIndex (UINT2 u2HwAggIndex, UINT2  *pu2SwAggIndex);
VOID LaRedSyncAggPortListWithHw (UINT2 u2AggIndex, tLaRedPortsInSwAgg *pSwPortList, 
                            UINT2 u2SwNumPorts, UINT4 u4LinkSelectPolicyBitList,
                            tLaHwInfo *pHwInfo);
VOID LaRedAddAggregatorToHw (tLaLacAggEntry *pAggEntry);
INT4 LaRedIsHwPortInSwPortList (UINT2 u2Port, tLaRedPortsInSwAgg *pSwPortList);
UINT1 FsRedGetSelPolicy (tLaHwInfo *pHwInfo);
INT4 LaRedUpdateNpapiDB (VOID);
#endif /* _LAREDNP_H_ */
