/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsla.h,v 1.13 2017/10/16 09:40:59 siva Exp $
*
*********************************************************************/
/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _FSLA_H
#define _FSLA_H 

#ifdef  SNMP_2_WANTED

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_table[] = {
    {"ccitt",        "0"},
    {"iso",        "1"},
    {"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
    {"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000"},
    {"org",        "1.3"},
    {"dod",        "1.3.6"},
    {"internet",        "1.3.6.1"},
    {"directory",        "1.3.6.1.1"},
    {"mgmt",        "1.3.6.1.2"},
    {"mib-2",        "1.3.6.1.2.1"},
    {"ip",        "1.3.6.1.2.1.4"},
    {"transmission",        "1.3.6.1.2.1.10"},
    {"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
    {"rmon",        "1.3.6.1.2.1.16"},
    {"statistics",        "1.3.6.1.2.1.16.1"},
    {"history",        "1.3.6.1.2.1.16.2"},
    {"hosts",        "1.3.6.1.2.1.16.4"},
    {"matrix",        "1.3.6.1.2.1.16.6"},
    {"filter",        "1.3.6.1.2.1.16.7"},
    {"tokenRing",        "1.3.6.1.2.1.16.10"},
    {"dot1dBridge",        "1.3.6.1.2.1.17"},
    {"dot1dStp",        "1.3.6.1.2.1.17.2"},
    {"dot1dTp",        "1.3.6.1.2.1.17.4"},
    {"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
    {"dns",        "1.3.6.1.2.1.32"},
    {"experimental",        "1.3.6.1.3"},
    {"private",        "1.3.6.1.4"},
    {"enterprises",        "1.3.6.1.4.1"},
    {"ARICENT-MPLS-TP-MIB",        "1.3.6.1.4.1.2076.13.4"},
    {"fsla",        "1.3.6.1.4.1.2076.63"},
    {"fsLaSystem",        "1.3.6.1.4.1.2076.63.1"},
    {"fsLaSystemControl",        "1.3.6.1.4.1.2076.63.1.1"},
    {"fsLaStatus",        "1.3.6.1.4.1.2076.63.1.2"},
    {"fsLaTraceOption",        "1.3.6.1.4.1.2076.63.1.3"},
    {"fsLaMaxPortsPerPortChannel",        "1.3.6.1.4.1.2076.63.1.4"},
    {"fsLaMaxPortChannels",        "1.3.6.1.4.1.2076.63.1.5"},
    {"fsLaOperStatus",        "1.3.6.1.4.1.2076.63.1.6"},
    {"fsLaActorSystemID",        "1.3.6.1.4.1.2076.63.1.7"},
    {"fsLaNoPartnerIndep",        "1.3.6.1.4.1.2076.63.1.8"},
    {"fsLaDLAGSystemStatus",        "1.3.6.1.4.1.2076.63.1.9"},
    {"fsLaDLAGSystemID",        "1.3.6.1.4.1.2076.63.1.10"},
    {"fsLaDLAGSystemPriority",        "1.3.6.1.4.1.2076.63.1.11"},
    {"fsLaDLAGPeriodicSyncTime",        "1.3.6.1.4.1.2076.63.1.12"},
    {"fsLaRecTmrDuration",        "1.3.6.1.4.1.2076.63.1.20"},
    {"fsLaRecThreshold",        "1.3.6.1.4.1.2076.63.1.21"},
    {"fsLaTotalErrRecCount",        "1.3.6.1.4.1.2076.63.1.22"},
    {"fsLaDLAGRolePlayed",        "1.3.6.1.4.1.2076.63.1.13"},
    {"fsLaDLAGDistributingPortIndex",        "1.3.6.1.4.1.2076.63.1.14"},
    {"fsLaDLAGDistributingPortList",        "1.3.6.1.4.1.2076.63.1.15"},
    {"fsLaMCLAGSystemStatus",        "1.3.6.1.4.1.2076.63.1.16"},
    {"fsLaMCLAGSystemID",        "1.3.6.1.4.1.2076.63.1.17"},
    {"fsLaMCLAGSystemPriority",        "1.3.6.1.4.1.2076.63.1.18"},
    {"fsLaMCLAGPeriodicSyncTime",        "1.3.6.1.4.1.2076.63.1.19"},
    {"fsLaRecTmrDuration",        "1.3.6.1.4.1.2076.63.1.20"},
    {"fsLaRecThreshold",        "1.3.6.1.4.1.2076.63.1.21"},
    {"fsLaTotalErrRecCount",        "1.3.6.1.4.1.2076.63.1.22"},
    {"fsLaDefaultedStateThreshold",        "1.3.6.1.4.1.2076.63.1.23"},
    {"fsLaHardwareFailureRecThreshold",        "1.3.6.1.4.1.2076.63.1.24"},
    {"fsLaSameStateRecThreshold",        "1.3.6.1.4.1.2076.63.1.25"},
    {"fsLaRecThresholdExceedAction",        "1.3.6.1.4.1.2076.63.1.26"},
    {"fsLaMCLAGClearCounters",        "1.3.6.1.4.1.2076.63.1.27"},
    {"fsLaClearStatistics",        "1.3.6.1.4.1.2076.63.1.28"},
    {"fsLaMCLAGSystemControl",        "1.3.6.1.4.1.2076.63.1.29"},
    {"fsLaPortChannel",        "1.3.6.1.4.1.2076.63.2"},
    {"fsLaPortChannelTable",        "1.3.6.1.4.1.2076.63.2.1"},
    {"fsLaPortChannelEntry",        "1.3.6.1.4.1.2076.63.2.1.1"},
    {"fsLaPortChannelIfIndex",        "1.3.6.1.4.1.2076.63.2.1.1.1"},
    {"fsLaPortChannelGroup",        "1.3.6.1.4.1.2076.63.2.1.1.2"},
    {"fsLaPortChannelAdminMacAddress",        "1.3.6.1.4.1.2076.63.2.1.1.3"},
    {"fsLaPortChannelMacSelection",        "1.3.6.1.4.1.2076.63.2.1.1.4"},
    {"fsLaPortChannelMode",        "1.3.6.1.4.1.2076.63.2.1.1.5"},
    {"fsLaPortChannelPortCount",        "1.3.6.1.4.1.2076.63.2.1.1.6"},
    {"fsLaPortChannelActivePortCount",        "1.3.6.1.4.1.2076.63.2.1.1.7"},
    {"fsLaPortChannelSelectionPolicy",        "1.3.6.1.4.1.2076.63.2.1.1.8"},
    {"fsLaPortChannelDefaultPortIndex",        "1.3.6.1.4.1.2076.63.2.1.1.9"},
    {"fsLaPortChannelMaxPorts",        "1.3.6.1.4.1.2076.63.2.1.1.10"},
    {"fsLaPortChannelSelectionPolicyBitList",        "1.3.6.1.4.1.2076.63.2.1.1.11"},
    {"fsLaPortChannelDLAGDistributingPortIndex",        "1.3.6.1.4.1.2076.63.2.1.1.12"},
    {"fsLaPortChannelDLAGSystemID",        "1.3.6.1.4.1.2076.63.2.1.1.13"},
    {"fsLaPortChannelDLAGSystemPriority",        "1.3.6.1.4.1.2076.63.2.1.1.14"},
    {"fsLaPortChannelDLAGPeriodicSyncTime",        "1.3.6.1.4.1.2076.63.2.1.1.15"},
    {"fsLaPortChannelDLAGMSSelectionWaitTime",        "1.3.6.1.4.1.2076.63.2.1.1.16"},
    {"fsLaPortChannelDLAGRolePlayed",        "1.3.6.1.4.1.2076.63.2.1.1.17"},
    {"fsLaPortChannelDLAGStatus",        "1.3.6.1.4.1.2076.63.2.1.1.18"},
    {"fsLaPortChannelDLAGRedundancy",        "1.3.6.1.4.1.2076.63.2.1.1.19"},
    {"fsLaPortChannelDLAGMaxKeepAliveCount",        "1.3.6.1.4.1.2076.63.2.1.1.20"},
    {"fsLaPortChannelDLAGPeriodicSyncPduTxCount",        "1.3.6.1.4.1.2076.63.2.1.1.21"},
    {"fsLaPortChannelDLAGPeriodicSyncPduRxCount",        "1.3.6.1.4.1.2076.63.2.1.1.22"},
    {"fsLaPortChannelDLAGEventUpdatePduTxCount",        "1.3.6.1.4.1.2076.63.2.1.1.23"},
    {"fsLaPortChannelDLAGEventUpdatePduRxCount",        "1.3.6.1.4.1.2076.63.2.1.1.24"},
    {"fsLaPortChannelDLAGElectedAsMasterCount",        "1.3.6.1.4.1.2076.63.2.1.1.25"},
    {"fsLaPortChannelDLAGElectedAsSlaveCount",        "1.3.6.1.4.1.2076.63.2.1.1.26"},
    {"fsLaPortChannelTrapTxCount",        "1.3.6.1.4.1.2076.63.2.1.1.27"},
    {"fsLaPortChannelDLAGDistributingPortList",        "1.3.6.1.4.1.2076.63.2.1.1.28"},
    {"fsLaPortChannelMCLAGStatus",        "1.3.6.1.4.1.2076.63.2.1.1.29"},
    {"fsLaPortChannelMCLAGSystemID",        "1.3.6.1.4.1.2076.63.2.1.1.30"},
    {"fsLaPortChannelMCLAGSystemPriority",        "1.3.6.1.4.1.2076.63.2.1.1.31"},
    {"fsLaPortChannelMCLAGRolePlayed",        "1.3.6.1.4.1.2076.63.2.1.1.32"},
    {"fsLaPortChannelMCLAGMaxKeepAliveCount",        "1.3.6.1.4.1.2076.63.2.1.1.33"},
    {"fsLaPortChannelMCLAGPeriodicSyncPduTxCount",        "1.3.6.1.4.1.2076.63.2.1.1.34"},
    {"fsLaPortChannelMCLAGPeriodicSyncPduRxCount",        "1.3.6.1.4.1.2076.63.2.1.1.35"},
    {"fsLaPortChannelMCLAGEventUpdatePduTxCount",        "1.3.6.1.4.1.2076.63.2.1.1.36"},
    {"fsLaPortChannelMCLAGEventUpdatePduRxCount",        "1.3.6.1.4.1.2076.63.2.1.1.37"},
    {"fsLaPortChannelClearStatistics",        "1.3.6.1.4.1.2076.63.2.1.1.38"},
    {"fsLaPortChannelUpCount",        "1.3.6.1.4.1.2076.63.2.1.1.39"},
    {"fsLaPortChannelDownCount",        "1.3.6.1.4.1.2076.63.2.1.1.40"},
    {"fsLaPortChannelHotStandByPortsCount",        "1.3.6.1.4.1.2076.63.2.1.1.41"},
    {"fsLaPortChannelOperChgTimeStamp",        "1.3.6.1.4.1.2076.63.2.1.1.42"},
    {"fsLaPortChannelDownReason",        "1.3.6.1.4.1.2076.63.2.1.1.43"},
    {"fsLaPort",        "1.3.6.1.4.1.2076.63.3"},
    {"fsLaPortTable",        "1.3.6.1.4.1.2076.63.3.1"},
    {"fsLaPortEntry",        "1.3.6.1.4.1.2076.63.3.1.1"},
    {"fsLaPortIndex",        "1.3.6.1.4.1.2076.63.3.1.1.1"},
    {"fsLaPortMode",        "1.3.6.1.4.1.2076.63.3.1.1.2"},
    {"fsLaPortBundleState",        "1.3.6.1.4.1.2076.63.3.1.1.3"},
    {"fsLaPortActorResetAdminState",        "1.3.6.1.4.1.2076.63.3.1.1.4"},
    {"fsLaPortAggregateWaitTime",        "1.3.6.1.4.1.2076.63.3.1.1.5"},
    {"fsLaPortPartnerResetAdminState",        "1.3.6.1.4.1.2076.63.3.1.1.6"},
    {"fsLaPortActorAdminPort",        "1.3.6.1.4.1.2076.63.3.1.1.7"},
    {"fsLaPortRestoreMtu",        "1.3.6.1.4.1.2076.63.3.1.1.8"},
    {"fsLaPortSelectAggregator",        "1.3.6.1.4.1.2076.63.3.1.1.9"},
    {"fsLaPortErrStateDetCount",        "1.3.6.1.4.1.2076.63.3.1.1.10"},
    {"fsLaPortErrStateRecCount",        "1.3.6.1.4.1.2076.63.3.1.1.11"},
    {"fsLaPortDefaultedStateThreshold",        "1.3.6.1.4.1.2076.63.3.1.1.12"},
    {"fsLaPortHardwareFailureRecThreshold",        "1.3.6.1.4.1.2076.63.3.1.1.13"},
    {"fsLaPortSameStateRecThreshold",        "1.3.6.1.4.1.2076.63.3.1.1.14"},
    {"fsLaPortUpInBundleCount",        "1.3.6.1.4.1.2076.63.3.1.1.15"},
    {"fsLaPortDownInBundleCount",        "1.3.6.1.4.1.2076.63.3.1.1.16"},
    {"fsLaPortBundStChgTimeStamp",        "1.3.6.1.4.1.2076.63.3.1.1.17"},
    {"fsLaPortDownInBundleReason",        "1.3.6.1.4.1.2076.63.3.1.1.18"},
    {"fsLaPortErrStateDetTimeStamp",        "1.3.6.1.4.1.2076.63.3.1.1.19"},
    {"fsLaPortErrStateRecTimeStamp",        "1.3.6.1.4.1.2076.63.3.1.1.20"},
    {"fsLaPortRecTrigReason",        "1.3.6.1.4.1.2076.63.3.1.1.21"},
    {"fsLaPortDefStateRecCount",        "1.3.6.1.4.1.2076.63.3.1.1.22"},
    {"fsLaPortSameStateRecCount",        "1.3.6.1.4.1.2076.63.3.1.1.23"},
    {"fsLaPortHwFailRecCount",        "1.3.6.1.4.1.2076.63.3.1.1.24"},
    {"fsLaPortDefStateRecTimeStamp",        "1.3.6.1.4.1.2076.63.3.1.1.25"},
    {"fsLaPortSameStateRecTimeStamp",        "1.3.6.1.4.1.2076.63.3.1.1.26"},
    {"fsLaPortHwFailRecTimeStamp",        "1.3.6.1.4.1.2076.63.3.1.1.27"},
    {"fsLaPortRecState",        "1.3.6.1.4.1.2076.63.3.1.1.28"},
    {"fsLaTrapObjects",        "1.3.6.1.4.1.2076.63.4"},
    {"fsLaHwFailTrapObjectsTable",        "1.3.6.1.4.1.2076.63.4.1"},
    {"fsLaHwFailTrapObjectsEntry",        "1.3.6.1.4.1.2076.63.4.1.1"},
    {"fsLaTrapPortChannelIndex",        "1.3.6.1.4.1.2076.63.4.1.1.1"},
    {"fsLaTrapPortIndex",        "1.3.6.1.4.1.2076.63.4.1.1.2"},
    {"fsLaHwFailTrapType",        "1.3.6.1.4.1.2076.63.4.1.1.3"},
    {"fsLaDLAGTrapObjectsTable",        "1.3.6.1.4.1.2076.63.4.2"},
    {"fsLaDLAGTrapObjectsEntry",        "1.3.6.1.4.1.2076.63.4.2.1"},
    {"fsLaDLAGTrapPortChannelIndex",        "1.3.6.1.4.1.2076.63.4.2.1.1"},
    {"fsLaDLAGTrapType",        "1.3.6.1.4.1.2076.63.4.2.1.2"},
    {"fsFutureLaTraps",        "1.3.6.1.4.1.2076.63.5"},
    {"fsLaTraps",        "1.3.6.1.4.1.2076.63.5.0"},
    {"fsLaDLAGRemotePortChannel",        "1.3.6.1.4.1.2076.63.6"},
    {"fsLaDLAGRemotePortChannelTable",        "1.3.6.1.4.1.2076.63.6.1"},
    {"fsLaDLAGRemotePortChannelEntry",        "1.3.6.1.4.1.2076.63.6.1.1"},
    {"fsLaDLAGRemotePortChannelSystemID",        "1.3.6.1.4.1.2076.63.6.1.1.1"},
    {"fsLaDLAGRemotePortChannelSystemPriority",        "1.3.6.1.4.1.2076.63.6.1.1.2"},
    {"fsLaDLAGRemotePortChannelRolePlayed",        "1.3.6.1.4.1.2076.63.6.1.1.3"},
    {"fsLaDLAGRemotePortChannelKeepAliveCount",        "1.3.6.1.4.1.2076.63.6.1.1.4"},
    {"fsLaDLAGRemotePort",        "1.3.6.1.4.1.2076.63.7"},
    {"fsLaDLAGRemotePortTable",        "1.3.6.1.4.1.2076.63.7.1"},
    {"fsLaDLAGRemotePortEntry",        "1.3.6.1.4.1.2076.63.7.1.1"},
    {"fsLaDLAGRemotePortIndex",        "1.3.6.1.4.1.2076.63.7.1.1.1"},
    {"fsLaDLAGRemotePortBundleState",        "1.3.6.1.4.1.2076.63.7.1.1.2"},
    {"fsLaDLAGRemotePortSyncStatus",        "1.3.6.1.4.1.2076.63.7.1.1.3"},
    {"fsLaDLAGRemotePortPriority",        "1.3.6.1.4.1.2076.63.7.1.1.4"},
    {"fsLaMCLAGRemotePortChannel",        "1.3.6.1.4.1.2076.63.8"},
    {"fsLaMCLAGRemotePortChannelTable",        "1.3.6.1.4.1.2076.63.8.1"},
    {"fsLaMCLAGRemotePortChannelEntry",        "1.3.6.1.4.1.2076.63.8.1.1"},
    {"fsLaMCLAGRemotePortChannelSystemID",        "1.3.6.1.4.1.2076.63.8.1.1.1"},
    {"fsLaMCLAGRemotePortChannelSystemPriority",        "1.3.6.1.4.1.2076.63.8.1.1.2"},
    {"fsLaMCLAGRemotePortChannelRolePlayed",        "1.3.6.1.4.1.2076.63.8.1.1.3"},
    {"fsLaMCLAGRemotePortChannelKeepAliveCount",        "1.3.6.1.4.1.2076.63.8.1.1.4"},
    {"fsLaMCLAGRemotePortChannelSpeed",        "1.3.6.1.4.1.2076.63.8.1.1.5"},
    {"fsLaMCLAGRemotePortChannelHighSpeed",        "1.3.6.1.4.1.2076.63.8.1.1.6"},
    {"fsLaMCLAGRemotePortChannelMtu",        "1.3.6.1.4.1.2076.63.8.1.1.7"},
    {"fsLaMCLAGRemotePort",        "1.3.6.1.4.1.2076.63.9"},
    {"fsLaMCLAGRemotePortTable",        "1.3.6.1.4.1.2076.63.9.1"},
    {"fsLaMCLAGRemotePortEntry",        "1.3.6.1.4.1.2076.63.9.1.1"},
    {"fsLaMCLAGRemotePortIndex",        "1.3.6.1.4.1.2076.63.9.1.1.1"},
    {"fsLaMCLAGRemotePortBundleState",        "1.3.6.1.4.1.2076.63.9.1.1.2"},
    {"fsLaMCLAGRemotePortSyncStatus",        "1.3.6.1.4.1.2076.63.9.1.1.3"},
    {"fsLaMCLAGRemotePortPriority",        "1.3.6.1.4.1.2076.63.9.1.1.4"},
    {"fsLaMCLAGRemotePortSlotIndex",        "1.3.6.1.4.1.2076.63.9.1.1.5"},
    {"issExt",        "1.3.6.1.4.1.2076.81.8"},
    {"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
    {"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
    {"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
    {"security",        "1.3.6.1.5"},
    {"snmpV2",        "1.3.6.1.6"},
    {"snmpDomains",        "1.3.6.1.6.1"},
    {"snmpProxys",        "1.3.6.1.6.2"},
    {"snmpModules",        "1.3.6.1.6.3"},
    {"snmpTraps",        "1.3.6.1.6.3.1.1.5"},
    {"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
    {"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
    {"ieee802dot1mibs",        "1.3.111.2.802.1.1"},
    {"joint-iso-ccitt",        "2"},
    {0,0}
   };
#endif /* SNMP_2_WANTED */
#endif /* _FSLA_H */
