#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07 March 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


LA_SWITCHES = -ULA_CHURN_WANTED 

ifeq (${NPAPI}, YES)
LA_SWITCHES += -DNP_API
endif

TOTAL_OPNS = ${LA_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

LA_BASE_DIR = ${BASE_DIR}/la
LA_SRC_DIR  = ${LA_BASE_DIR}/src
LA_INC_DIR  = ${LA_BASE_DIR}/inc
LA_OBJ_DIR  = ${LA_BASE_DIR}/obj
COMMON_INC_DIR  = ${BASE_DIR}/inc
SNMP_INC_DIR  = ${BASE_DIR}/inc/snmp
# FS_DELTA - Define CFA_BASE_DIR
CFA_BASE_DIR = ${BASE_DIR}/cfa2
CFA_INCD    = ${CFA_BASE_DIR}/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${LA_INC_DIR} -I${CFA_INCD} -I${COMMON_INC_DIR} -I${SNMP_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
