/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mriutil.c                      */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  Any                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This files contains the utility*/
/*                           functions defined by this module*/
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#include "mriinc.h"

/***************************************************************/
/*  Function Name   : MriUtilCompareMRouteTblIndex             */
/*  Description     : Function compare the 2 multicast route   */
/*                    indexs and return the appropriate value  */
/*                    which indicates the first index less than*/
/*                    greater than or equals to second index   */
/*  Input(s)        : pMRouteIndex1- Structure contains first  */
/*                                   multicast route Index     */
/*                    pMRouteIndex2- Structure contains second */
/*                                   multicast route Index     */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : MRI_FIRST_GT_SECOND/MRI_FIRST_LT_SECOND  */
/*                    MRI_FIRST_EQ_SECOND                      */
/***************************************************************/
INT4
MriUtilCompareMRouteTblIndex (tMRouteTblIndex * pMRouteIndex1,
                              tMRouteTblIndex * pMRouteIndex2)
{
    if (pMRouteIndex1->u4GrpAddr > pMRouteIndex2->u4GrpAddr)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMRouteIndex1->u4GrpAddr < pMRouteIndex2->u4GrpAddr)
    {
        return MRI_FIRST_LT_SECOND;
    }

    if (pMRouteIndex1->u4SrcAddr > pMRouteIndex2->u4SrcAddr)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMRouteIndex1->u4SrcAddr < pMRouteIndex2->u4SrcAddr)
    {
        return MRI_FIRST_LT_SECOND;
    }

    if (pMRouteIndex1->u4SrcMask > pMRouteIndex2->u4SrcMask)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMRouteIndex1->u4SrcMask < pMRouteIndex2->u4SrcMask)
    {
        return MRI_FIRST_LT_SECOND;
    }
    return MRI_FIRST_EQ_SECOND;
}

/***************************************************************/
/*  Function Name   : MriUtilCompareMNextHopTblIndex           */
/*  Description     : Function compare the 2 multicast NextHop */
/*                    indexs and return the appropriate value. */
/*                    which indicates the first index less than*/
/*                    greater than or equals to second index   */
/*  Input(s)        : pMNextHopIndex1- Structure contains first*/
/*                                     multicast nexthop Index */
/*                    pMNextHopIndex2- Structure contains sec  */
/*                                   multicast nexthop Index   */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : MRI_FIRST_GT_SECOND/MRI_FIRST_LT_SECOND  */
/*                    MRI_FIRST_EQ_SECOND                      */
/***************************************************************/
INT4
MriUtilCompareMNextHopTblIndex (tMNextHopTblIndex * pMNextHopIndex1,
                                tMNextHopTblIndex * pMNextHopIndex2)
{

    if (pMNextHopIndex1->u4GrpAddr > pMNextHopIndex2->u4GrpAddr)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMNextHopIndex1->u4GrpAddr < pMNextHopIndex2->u4GrpAddr)
    {
        return MRI_FIRST_LT_SECOND;
    }

    if (pMNextHopIndex1->u4SrcAddr > pMNextHopIndex2->u4SrcAddr)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMNextHopIndex1->u4SrcAddr < pMNextHopIndex2->u4SrcAddr)
    {
        return MRI_FIRST_LT_SECOND;
    }

    if (pMNextHopIndex1->u4SrcMask > pMNextHopIndex2->u4SrcMask)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMNextHopIndex1->u4SrcMask < pMNextHopIndex2->u4SrcMask)
    {
        return MRI_FIRST_LT_SECOND;
    }

    if (pMNextHopIndex1->i4OIfIndex > pMNextHopIndex2->i4OIfIndex)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMNextHopIndex1->i4OIfIndex < pMNextHopIndex2->i4OIfIndex)
    {
        return MRI_FIRST_LT_SECOND;
    }

    if (pMNextHopIndex1->u4NextHop > pMNextHopIndex2->u4NextHop)
    {
        return MRI_FIRST_GT_SECOND;
    }
    else if (pMNextHopIndex1->u4NextHop < pMNextHopIndex2->u4NextHop)
    {
        return MRI_FIRST_LT_SECOND;
    }

    return MRI_FIRST_EQ_SECOND;
}

/***************************************************************/
/*  Function Name   : MriUtilGetLowMRouteIndex                 */
/*  Description     : Function compare the 2 multicast route   */
/*                    indexs and copy the lowest route index   */
/*                    to first index depending upon the flag.  */
/*  Input(s)        : pMRouteIndex1- Structure contains first  */
/*                                   multicast route Index     */
/*                    pMRouteIndex2- Structure contains second */
/*                                   multicast route Index     */
/*                    i1Status     - Flag                      */
/*  Output(s)       : pMRouteIndex1- Structure contains lowest */
/*                                   multicast route Index     */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :                                          */
/***************************************************************/
VOID
MriUtilGetLowMRouteIndex (tMRouteTblIndex * pMRouteIndex1,
                          tMRouteTblIndex * pMRouteIndex2, INT1 i1Status)
{
    if (i1Status == MRI_TRUE)
    {
        if (MriUtilCompareMRouteTblIndex (pMRouteIndex1, pMRouteIndex2)
            == MRI_FIRST_GT_SECOND)
        {
            pMRouteIndex1->u4GrpAddr = pMRouteIndex2->u4GrpAddr;
            pMRouteIndex1->u4SrcAddr = pMRouteIndex2->u4SrcAddr;
            pMRouteIndex1->u4SrcMask = pMRouteIndex2->u4SrcMask;
        }
    }
    else
    {
        pMRouteIndex1->u4GrpAddr = pMRouteIndex2->u4GrpAddr;
        pMRouteIndex1->u4SrcAddr = pMRouteIndex2->u4SrcAddr;
        pMRouteIndex1->u4SrcMask = pMRouteIndex2->u4SrcMask;
    }
}

/***************************************************************/
/*  Function Name   : MriUtilGetLowMNextHopIndex               */
/*  Description     : Function compare the 2 multicast nexthop */
/*                    indexs and copy the lowest route index   */
/*                    to first index depending upon the flag.  */
/*  Input(s)        : pMRouteIndex1- Structure contains first  */
/*                                   multicast nexthop Index   */
/*                    pMRouteIndex2- Structure contains second */
/*                                   multicast nexthop Index   */
/*                    i1Status     - Flag                      */
/*  Output(s)       : pMRouteIndex1- Structure contains lowest */
/*                                   multicast nexthop Index   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : None                                     */
/***************************************************************/
VOID
MriUtilGetLowMNextHopIndex (tMNextHopTblIndex * pMNextHopIndex1,
                            tMNextHopTblIndex * pMNextHopIndex2, INT1 i1Status)
{
    if (i1Status == MRI_TRUE)
    {
        if (MriUtilCompareMNextHopTblIndex (pMNextHopIndex1, pMNextHopIndex2)
            == MRI_FIRST_GT_SECOND)
        {
            pMNextHopIndex1->u4GrpAddr = pMNextHopIndex2->u4GrpAddr;
            pMNextHopIndex1->u4SrcAddr = pMNextHopIndex2->u4SrcAddr;
            pMNextHopIndex1->u4SrcMask = pMNextHopIndex2->u4SrcMask;
            pMNextHopIndex1->i4OIfIndex = pMNextHopIndex2->i4OIfIndex;
            pMNextHopIndex1->u4NextHop = pMNextHopIndex2->u4NextHop;
        }
    }
    else
    {
        pMNextHopIndex1->u4GrpAddr = pMNextHopIndex2->u4GrpAddr;
        pMNextHopIndex1->u4SrcAddr = pMNextHopIndex2->u4SrcAddr;
        pMNextHopIndex1->u4SrcMask = pMNextHopIndex2->u4SrcMask;
        pMNextHopIndex1->i4OIfIndex = pMNextHopIndex2->i4OIfIndex;
        pMNextHopIndex1->u4NextHop = pMNextHopIndex2->u4NextHop;
    }
}
