/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mriinit.c                      */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  Any                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This files contains the MRI    */
/*                           Initialization functions       */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/
#ifndef _MRI_INIT_C_
#define _MRI_INIT_C_

#include "mriinc.h"

/*****************************************************************************/
/* Function Name      : MriInit                                              */
/* Description        : This function creates the MRI module semaphore and   */
/*                      registers the MIB with the SNMP.                      */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gMriGlobalInfo.MriSemId                              */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
MriInit ()
{

    MEMSET (&(gMriGlobalInfo), 0, sizeof (tMriGlobalInfo));

    /* Create mutual exclusion semaphore for MRI */
    OsixSemCrt ((UINT1 *) MRI_SEM_NAME, &(gMriGlobalInfo.MriSemId));

    /* Intially semaphore value is 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gMriGlobalInfo.MriSemId);

    /* Enable IP multicasting by default */
    gMriGlobalInfo.eMRouteStatus = MRI_MROUTE_ENABLE;

#ifdef FS_NPAPI
    if (FsNpIpv4McInit () == FNP_FAILURE)
    {
        gMriGlobalInfo.eMRouteStatus = MRI_MROUTE_DISABLE;
    }
#endif

    /* Register MRI module with SNMP */
    RegisterSTDMRI ();

}

/*****************************************************************************/
/* Function Name      : MriLock                                              */
/* Description        : This function is used to take the MRI's  mutual      */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      data structure configuration task/thread.            */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gMriGlobalInfo.MriSemId                              */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
MriLock ()
{
    OsixSemTake (gMriGlobalInfo.MriSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MriUnLock                                            */
/* Description        : This function is used to give the MRI's mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      data structure by CLI/SNMP configuration task.       */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gMriGlobalInfo.MriSemId                              */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
MriUnLock ()
{
    OsixSemGive (gMriGlobalInfo.MriSemId);
    return SNMP_SUCCESS;
}

#endif
