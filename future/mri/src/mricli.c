/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/* $Id: mricli.c,v 1.5 2014/03/15 14:36:15 siva Exp $*/
/*  FILE NAME             :  mricli.c                       */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :  IPv4 Multicast Routing MIB     */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  Any                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This file contains cli command */
/*                           parsing/supporting functions.  */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#ifndef _MRI_CLI_C_
#define _MRI_CLI_C_

#include "mriinc.h"

PRIVATE INT1        MriCliIpMulticastRouting (tCliHandle, INT4);
PRIVATE INT1        MriCliShowIpMulticastRoute (tCliHandle);
PRIVATE INT1        MriCliSetIpMIfaceRateLimit (tCliHandle, INT4, INT4);
PRIVATE INT1        MriCliSetIpMIfaceTtlTreshold (tCliHandle, INT4, INT4);
PRIVATE VOID        MriGetTimeString (UINT4, INT1 *);
PRIVATE INT4        MriCliShowNextHopInfo (tCliHandle, UINT4, UINT4, UINT4,
                                           INT4, UINT4);
INT4                MriShowRunningConfig (tCliHandle CliHandle);

/***************************************************************/
/*  Function Name   : cli_process_mri_cmd                      */
/*  Description     : This Function processes the multicast    */
/*                    module related commands,calls respective */
/*                    for further processing of the user       */
/*                    commands/inputs.                         */
/*  Input(s)        :                                          */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: --                                      */
/*  Global Variables Referred :--                              */
/*  Global variables Modified :--                              */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : --                                      */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT1
cli_process_mri_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *apu4Args[MRI_CLI_MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                i1Argno = 0;
    INT1                i1RetStatus = CLI_FAILURE;
    INT4                i4IfIndex = 0;
    UINT4               u4ErrCode = 0;

    MEMSET (apu4Args, 0, MRI_CLI_MAX_ARGS);

    va_start (ap, u4Command);

    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, INT4));
    UNUSED_PARAM (i4Inst);
    while (1)
    {
        apu4Args[i1Argno++] = va_arg (ap, UINT4 *);

        if (i1Argno == MRI_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    /* Register Lock with CLI */
    CliRegisterLock (CliHandle, MriLock, MriUnLock);
    MriLock ();

    switch (u4Command)
    {
        case CLI_MRI_MROUTING_DISABLE:

            i1RetStatus = MriCliIpMulticastRouting (CliHandle,
                                                    MRI_MROUTE_DISABLE);

            break;

        case CLI_MRI_MROUTING_ENABLE:

            i1RetStatus = MriCliIpMulticastRouting (CliHandle,
                                                    MRI_MROUTE_ENABLE);

            break;

        case CLI_MRI_SHOW_MROUTE:

            i1RetStatus = MriCliShowIpMulticastRoute (CliHandle);

            break;

        case CLI_MRI_RATELIMIT:
            /* apu4Args[0]  Ratelimit */
            i4IfIndex = CLI_GET_IFINDEX ();

            i1RetStatus = MriCliSetIpMIfaceRateLimit (CliHandle, i4IfIndex,
                                                      *((INT4 *) apu4Args[0]));
            break;

        case CLI_MRI_NO_RATELIMIT:

            i4IfIndex = CLI_GET_IFINDEX ();
            i1RetStatus = MriCliSetIpMIfaceRateLimit (CliHandle, i4IfIndex,
                                                      MRI_NO_RATELIMIT_VAL);
            break;

        case CLI_MRI_TTL:
            /* apu4Args[0]  Ttl threshold */
            i4IfIndex = CLI_GET_IFINDEX ();
            i1RetStatus = MriCliSetIpMIfaceTtlTreshold (CliHandle, i4IfIndex,
                                                        *((INT4 *)
                                                          apu4Args[0]));
            break;

        case CLI_MRI_NO_TTL:

            i4IfIndex = CLI_GET_IFINDEX ();
            i1RetStatus = MriCliSetIpMIfaceTtlTreshold (CliHandle, i4IfIndex,
                                                        MRI_DEF_IFACE_TTL_TRESHOLD);
            break;

        default:
            CliPrintf (CliHandle, "\r%% Unknown command\r\n");
            MriUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i1RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_MRI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapi1MriCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i1RetStatus);
    MriUnLock ();
    CliUnRegisterLock (CliHandle);
    return i1RetStatus;
}

/***************************************************************/
/*  Function Name   : MriCliIpMulticastRouting                 */
/*  Description     : Sets the RouteEnableStatus to enabled or */
/*                    disabled.                                */
/*  Input(s)        : CliHandle, i1RouteEnableStatus           */
/*  Output(s)       : --                                       */
/*  <OPTIONAL Fields>: --                                      */
/*  Global Variables Referred :--                              */
/*  Global variables Modified :--                              */
/*  Returns         : CLI_SUCCESS / CLI_FAILURE                */
/***************************************************************/

INT1
MriCliIpMulticastRouting (tCliHandle CliHandle, INT4 i1RouteEnableStatus)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IpMRouteEnable (&u4ErrorCode, i1RouteEnableStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIpMRouteEnable (i1RouteEnableStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   :  MriCliShowIpMulticastRoute              */
/*  Description     :  displays all the multicast route        */
/*                     information for all the (S,G) entries   */
/*  Input(s)        :  --                                      */
/*  Output(s)       :  --                                      */
/*  <OPTIONAL Fields>: --                                      */
/*  Global Variables Referred : --                             */
/*  Global variables Modified : --                             */
/*  Exceptions or Operating System Error Handling : --         */
/*  Use of Recursion : --                                      */
/*  Returns         :  CLI_SUCCESS/CLI_FAILURE                 */
/***************************************************************/

INT1
MriCliShowIpMulticastRoute (tCliHandle CliHandle)
{

    UINT4               u4Grp = 0;
    UINT4               u4Src = 0;
    UINT4               u4SrcMask = 0;
    UINT4               u4NextGrp = 0;
    UINT4               u4NextSrc = 0;
    UINT4               u4NextSrcMask = 0;
    UINT4               u4NHGrp = 0;
    UINT4               u4NHSrc = 0;
    UINT4               u4NHSrcMask = 0;
    INT4                i4NHOIfIndex = 0;
    UINT4               u4NHAddr = 0;
    UINT4               u4NextNHGrp = 0;
    UINT4               u4NextNHSrc = 0;
    UINT4               u4NextNHSrcMask = 0;
    INT4                i4NextNHOIfIndex = 0;
    UINT4               u4NextNHAddr = 0;
    UINT4               u4UpstreamNbr = 0;
    INT4                i4InIfIndex = 0;
    UINT4               u4RouteUpTime = 0;
    UINT4               u4RouteExpiryTime = 0;
    INT4                i4MRouteProtocol = 0;
    INT4                i4Flag = OSIX_TRUE;
    INT4                i4MRouteStatus = 0;
    CHR1               *pu1String = NULL;
    INT1                ai1TmStr[MAX_MRI_TIME_STR_LEN];
    UINT1               au1MRouteProtocol[MRI_MAX_NUM_MRP][MRI_MAX_ADDR_BUFFER]
        = { "", "other", "local", "netmgmt", "DVMRP", "MOSPF",
        "PIMSparseDense", "CBT", "PIM-SM", "PIM-DM",
        "IGMPOnly", "BGMP", "MSDP"
    };
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    nmhGetIpMRouteEnable (&i4MRouteStatus);

    CliPrintf (CliHandle, "\r\nIPv4 Multicast Routing Status : ");

    if (i4MRouteStatus == MRI_MROUTE_ENABLE)
    {
        CliPrintf (CliHandle, "Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled\r\n");
    }

    if (nmhGetFirstIndexIpMRouteTable (&u4NextGrp, &u4NextSrc, &u4NextSrcMask)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetFirstIndexIpMRouteNextHopTable (&u4NHGrp, &u4NHSrc, &u4NHSrcMask,
                                          &i4NHOIfIndex, &u4NHAddr);

    /*Print the format of display */
    CliPrintf (CliHandle, "\r\nMulticast Routing Information\r\n");
    CliPrintf (CliHandle, "------------------------------\r\n");
    CliPrintf (CliHandle, "(S,G), uptime/expires\r\n");

    CliPrintf (CliHandle, "Muticast routing protocol, ");
    CliPrintf (CliHandle, "Upstream neighbor \r\n");
    CliPrintf (CliHandle, "Incoming interface : interface\r\n");
    CliPrintf (CliHandle, "Outgoing interface list :\r\n");
    CliPrintf (CliHandle, "\tinterface, ");
    CliPrintf (CliHandle, "state, ");
    CliPrintf (CliHandle, "uptime/expires \r\n\r\n");

    do
    {
        u4Grp = u4NextGrp;
        u4Src = u4NextSrc;
        u4SrcMask = u4NextSrcMask;

        if (nmhGetIpMRouteUpstreamNeighbor (u4Grp, u4Src, u4SrcMask,
                                            &u4UpstreamNbr) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetIpMRouteInIfIndex (u4Grp, u4Src, u4SrcMask, &i4InIfIndex)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetIpMRouteUpTime (u4Grp, u4Src, u4SrcMask, &u4RouteUpTime)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetIpMRouteExpiryTime (u4Grp, u4Src, u4SrcMask,
                                      &u4RouteExpiryTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetIpMRouteProtocol (u4Grp, u4Src, u4SrcMask,
                                    &i4MRouteProtocol) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Src);
        CliPrintf (CliHandle, "\r\n(%s,", pu1String);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Grp);
        CliPrintf (CliHandle, " %s),", pu1String);

        MriGetTimeString (u4RouteUpTime, ai1TmStr);
        CliPrintf (CliHandle, " %s", ai1TmStr);

        if (u4RouteExpiryTime != 0)
        {
            MriGetTimeString (u4RouteExpiryTime, ai1TmStr);
            CliPrintf (CliHandle, "/ %s", ai1TmStr);
        }

        CliPrintf (CliHandle, "\n\r  %s, ",
                   au1MRouteProtocol[i4MRouteProtocol]);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4UpstreamNbr);

        CliPrintf (CliHandle, "%s\r\n", pu1String);

        MEMSET (&au1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaCliConfGetIfName ((UINT4) i4InIfIndex, (INT1 *) &au1IfName[0]);
        CliPrintf (CliHandle, "\r  Incoming interface: %s\r\n", au1IfName);

        CliPrintf (CliHandle, "\r  Outgoing interface list:\r\n");

        if (((i4Flag == OSIX_TRUE)) &&
            ((u4NHGrp == u4Grp) && (u4NHSrc == u4Src) &&
             (u4NHSrcMask == u4SrcMask)))
        {
            if (MriCliShowNextHopInfo (CliHandle, u4NHGrp, u4NHSrc,
                                       u4NHSrcMask, i4NHOIfIndex, u4NHAddr)
                != CLI_SUCCESS)
            {
                return CLI_FAILURE;
            }

            i4Flag = OSIX_FALSE;
        }

        while ((nmhGetNextIndexIpMRouteNextHopTable (u4NHGrp, &u4NextNHGrp,
                                                     u4NHSrc, &u4NextNHSrc,
                                                     u4NHSrcMask,
                                                     &u4NextNHSrcMask,
                                                     i4NHOIfIndex,
                                                     &i4NextNHOIfIndex,
                                                     u4NHAddr, &u4NextNHAddr)
                == SNMP_SUCCESS) &&
               ((u4NextNHGrp == u4Grp) &&
                (u4NextNHSrc == u4Src) && (u4NextNHSrcMask == u4SrcMask)))
        {
            if (MriCliShowNextHopInfo (CliHandle, u4NextNHGrp,
                                       u4NextNHSrc, u4NextNHSrcMask,
                                       i4NextNHOIfIndex, u4NextNHAddr)
                != CLI_SUCCESS)
            {
                return CLI_FAILURE;
            }

            u4NHGrp = u4NextNHGrp;
            u4NHSrc = u4NextNHSrc;
            u4NHSrcMask = u4NextNHSrcMask;
            i4NHOIfIndex = i4NextNHOIfIndex;
            u4NHAddr = u4NextNHAddr;
        }

    }
    while (nmhGetNextIndexIpMRouteTable (u4Grp, &u4NextGrp, u4Src, &u4NextSrc,
                                         u4SrcMask, &u4NextSrcMask)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : MriCliShowNextHopInfo                    */
/*  Description     : This Function takes the next-Hop table   */
/*                    index as input parameters and print the  */
/*                    next-Hop table info                      */
/*  Input(s)        : u4NHNextGrp - Nexthop table group address*/
/*                    u4NHNextSrc - Nexthop table group address*/
/*                    u4NHNextSrcMask - Nexthop table source   */
/*                    mask.                                    */
/*                    i4NHNextOIfIndex - Nexthop table out     */
/*                    going interface.                         */
/*                    u4NHNextAddr - NextHip table nexthop     */
/*                    interface.                               */
/*  Output(s)       : --                                       */
/*  <OPTIONAL Fields>: --                                      */
/*  Global Variables Referred : --                             */
/*  Global variables Modified : --                             */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion :--                                       */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS                  */
/***************************************************************/
INT4
MriCliShowNextHopInfo (tCliHandle CliHandle, UINT4 u4NHNextGrp,
                       UINT4 u4NHNextSrc, UINT4 u4NHNextSrcMask,
                       INT4 i4NHNextOIfIndex, UINT4 u4NHNextAddr)
{
    INT1                ai1TmStr[MAX_MRI_TIME_STR_LEN];
    UINT1               au1OifState[MRI_MAX_NUM_OIFSTATE][MRI_MAX_ADDR_BUFFER]
        = { "", "Pruned", "Forwarding" };
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4NextHopState = 0;
    UINT4               u4NextHopUpTime = 0;
    UINT4               u4NextHopExpiryTime = 0;

    /*retrieve the Oiflist and correspoding details */
    if (nmhGetIpMRouteNextHopState (u4NHNextGrp, u4NHNextSrc,
                                    u4NHNextSrcMask, i4NHNextOIfIndex,
                                    u4NHNextAddr, &i4NextHopState)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetIpMRouteNextHopUpTime (u4NHNextGrp, u4NHNextSrc,
                                     u4NHNextSrcMask, i4NHNextOIfIndex,
                                     u4NHNextAddr, &u4NextHopUpTime)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetIpMRouteNextHopExpiryTime (u4NHNextGrp, u4NHNextSrc,
                                         u4NHNextSrcMask, i4NHNextOIfIndex,
                                         u4NHNextAddr, &u4NextHopExpiryTime)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*print the Oiflist and corresponding details */

    MEMSET (&au1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);

    CfaCliConfGetIfName ((UINT4) i4NHNextOIfIndex, (INT1 *) &au1IfName[0]);
    CliPrintf (CliHandle, "\r\t %s, ", au1IfName);

    CliPrintf (CliHandle, "%s, ", au1OifState[i4NextHopState]);

    MriGetTimeString (u4NextHopUpTime, ai1TmStr);

    CliPrintf (CliHandle, "%s", ai1TmStr);

    if (u4NextHopExpiryTime != 0)
    {
        MriGetTimeString (u4NextHopExpiryTime, ai1TmStr);
        CliPrintf (CliHandle, "/%s\r\n", ai1TmStr);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : MriCliSetIpMIfaceRateLimit               */
/*  Description     : Sets the Ratelimit value for a given     */
/*                    interface                                */
/*  Input(s)        : i4IfIndex( Interface Index)              */
/*                    i4RateLimit(data rate limit)             */
/*  Output(s)       : --                                       */
/*  <OPTIONAL Fields>: --                                      */
/*  Global Variables Referred : --                             */
/*  Global variables Modified : --                             */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion :--                                       */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS                  */
/***************************************************************/

INT1
MriCliSetIpMIfaceRateLimit (tCliHandle CliHandle, INT4 i4IfIndex,
                            INT4 i4RateLimit)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IpMRouteInterfaceRateLimit (&u4ErrorCode, i4IfIndex,
                                             i4RateLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIpMRouteInterfaceRateLimit (i4IfIndex, i4RateLimit)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   :MriCliSetIpMIfaceTtlTreshold              */
/*  Description     :Sets the TTL value for a given interface  */
/*  Input(s)        :i4IfIndex(Interface Index)                */
/*                   i4TTL( TTL value)                         */
/*  Output(s)       :--                                        */
/*  <OPTIONAL Fields>: --                                      */
/*  Global Variables Referred :--                              */
/*  Global variables Modified :--                              */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion :--                                       */
/*  Returns         :   CLI_FAILURE/CLI_SUCCESS                */
/***************************************************************/
INT1
MriCliSetIpMIfaceTtlTreshold (tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4TtlTresh)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IpMRouteInterfaceTtl (&u4ErrorCode, i4IfIndex, i4TtlTresh)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIpMRouteInterfaceTtl (i4IfIndex, i4TtlTresh) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MriShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4MRouteStatus = 0;

    CliRegisterLock (CliHandle, MriLock, MriUnLock);
    MriLock ();

    nmhGetIpMRouteEnable (&i4MRouteStatus);

    if (i4MRouteStatus != MRI_MROUTE_ENABLE)
    {
        CliPrintf (CliHandle, "no ip multicast routing \r\n");
    }

    MriUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : MriGetTimeString                         */
/*  Description     : takes the time in timeticks as inputs    */
/*                    and print it in DD Hr:Min:Sec format.    */
/*  Input(s)        : u4Time (time in timeticks)               */
/*  Output(s)       : pi1Time(display time string)             */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  None                                    */
/***************************************************************/
VOID
MriGetTimeString (UINT4 u4Time, INT1 *pi1Time)
{
    UINT4               u4Days = 0;
    UINT4               u4Hrs = 0;
    UINT4               u4Mins = 0;
    UINT4               u4Secs = 0;
    UINT4               u4CentiSecs = 0;

    MEMSET (pi1Time, 0, MAX_MRI_TIME_STR_LEN);

    if (u4Time == 0)
    {
        SPRINTF ((CHR1 *) pi1Time, " ");
        return;
    }

    u4CentiSecs = u4Time % SYS_TIME_TICKS_IN_A_SEC;
    u4Time = u4Time / SYS_TIME_TICKS_IN_A_SEC;
    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;
    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;
    u4Hrs = u4Time % 24;
    u4Days = u4Time / 24;

    SPRINTF ((CHR1 *) pi1Time, "%dd %02d:%02d:%02d.%02d", u4Days, u4Hrs, u4Mins,
             u4Secs, u4CentiSecs);

    return;
}

#endif/*_MRICLI_C_*/
/***************************************************************************/
/*                            END OF FILE                                  */
/***************************************************************************/
