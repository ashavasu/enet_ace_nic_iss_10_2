/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmrilw.c,v 1.3 2009/03/17 11:41:36 prabuc-iss Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "mriinc.h"

extern UINT4        IpMRouteEnable[10];
extern UINT4        IpMRouteInterfaceIfIndex[12];
extern UINT4        IpMRouteInterfaceTtl[12];
extern UINT4        IpMRouteInterfaceRateLimit[12];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteEnable
 Input       :  The Indices

                The Object 
                retValIpMRouteEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteEnable (INT4 *pi4RetValIpMRouteEnable)
{
    *pi4RetValIpMRouteEnable = gMriGlobalInfo.eMRouteStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteEntryCount
 Input       :  The Indices

                The Object 
                retValIpMRouteEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteEntryCount (UINT4 *pu4RetValIpMRouteEntryCount)
{
    UINT4               u4Group = 0;
    UINT4               u4Source = 0;
    UINT4               u4SourceMask = 0;
    UINT4               u4NextGroup = 0;
    UINT4               u4NextSource = 0;
    UINT4               u4NextSourceMask = 0;
    INT4                i4Count = 0;

    if (nmhGetFirstIndexIpMRouteTable (&u4Group, &u4Source, &u4SourceMask)
        == SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }

    i4Count++;

    while (nmhGetNextIndexIpMRouteTable (u4Group, &u4NextGroup, u4Source,
                                         &u4NextSource, u4SourceMask,
                                         &u4NextSourceMask) == SNMP_SUCCESS)
    {
        u4Group = u4NextGroup;
        u4Source = u4NextSource;
        u4SourceMask = u4NextSourceMask;
        i4Count++;
    }
    *pu4RetValIpMRouteEntryCount = i4Count;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpMRouteEnable
 Input       :  The Indices

                The Object 
                setValIpMRouteEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteEnable (INT4 i4SetValIpMRouteEnable)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT4                i4Status = SNMP_SUCCESS;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (gMriGlobalInfo.eMRouteStatus == (UINT4) i4SetValIpMRouteEnable)
    {
        return i4Status;
    }

    if (i4SetValIpMRouteEnable == MRI_MROUTE_DISABLE)
    {
#ifdef FS_NPAPI                    /* Call NPAPI */
        if (FsNpIpv4McDeInit () == FNP_FAILURE)
        {
            i4Status = SNMP_FAILURE;
        }
#endif
        gMriGlobalInfo.eMRouteStatus = MRI_MROUTE_DISABLE;
    }
    else
    {
#ifdef FS_NPAPI                    /* Call NPAPI */
        if (FsNpIpv4McInit () == FNP_FAILURE)
        {
            i4Status = SNMP_FAILURE;
        }
#endif
        gMriGlobalInfo.eMRouteStatus = MRI_MROUTE_ENABLE;
    }

    if (i4Status == SNMP_SUCCESS)
    {
        SnmpNotifyInfo.pu4ObjectId = IpMRouteEnable;
        SnmpNotifyInfo.u4OidLen = sizeof (IpMRouteEnable) / sizeof (UINT4);
        RM_GET_SEQ_NUM (&SnmpNotifyInfo.u4SeqNum);
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NULL;
        SnmpNotifyInfo.pUnLockPointer = NULL;
        SnmpNotifyInfo.u4Indices = 0;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValIpMRouteEnable));
    }

    return i4Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IpMRouteEnable
 Input       :  The Indices

                The Object 
                testValIpMRouteEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteEnable (UINT4 *pu4ErrorCode, INT4 i4TestValIpMRouteEnable)
{
    if ((i4TestValIpMRouteEnable == MRI_MROUTE_ENABLE) ||
        (i4TestValIpMRouteEnable == MRI_MROUTE_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpMRouteEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpMRouteEnable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpMRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteTable
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpMRouteTable (UINT4 u4IpMRouteGroup,
                                       UINT4 u4IpMRouteSource,
                                       UINT4 u4IpMRouteSourceMask)
{

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhValidateIndexInstanceFsPimCmnIpMRouteTable (i4Component,
                                                           IPVX_ADDR_FMLY_IPV4,
                                                           &IpMRouteGroup,
                                                           &IpMRouteSource,
                                                           i4IpMRouteSourceMaskLen)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpIpMRTable (u4IpMRouteGroup,
                                                u4IpMRouteSource,
                                                u4IpMRouteSourceMask)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyValidateRouteTblIndex (u4IpMRouteGroup, u4IpMRouteSource,
                                        u4IpMRouteSourceMask) == IGP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteTable
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpMRouteTable (UINT4 *pu4IpMRouteGroup,
                               UINT4 *pu4IpMRouteSource,
                               UINT4 *pu4IpMRouteSourceMask)
{
    INT1                i1Status = MRI_FALSE;
    tMRouteTblIndex     MRouteIndex1;
#if defined(DVMRP_WANTED) || defined(IGMPPRXY_WANTED)
    tMRouteTblIndex     MRouteIndex2;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4NextIpMRouteSourceMaskLen = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Component = 0;
    INT4                i4NextComponent = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;

    NextIpMRouteGroup.pu1_OctetList = au1NextRouteGroup;
    NextIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;

    NextIpMRouteSource.pu1_OctetList = au1NextRouteSource;
    NextIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFirstIndexFsPimCmnIpMRouteTable (&i4Component, &i4AddrType,
                                               &IpMRouteGroup, &IpMRouteSource,
                                               &i4IpMRouteSourceMaskLen)
        == SNMP_SUCCESS)
    {
        if (i4AddrType != IPVX_ADDR_FMLY_IPV4)
        {
            while (nmhGetNextIndexFsPimCmnIpMRouteTable (i4Component,
                                                         &i4NextComponent,
                                                         i4AddrType,
                                                         &i4NextAddrType,
                                                         &IpMRouteGroup,
                                                         &NextIpMRouteGroup,
                                                         &IpMRouteSource,
                                                         &NextIpMRouteSource,
                                                         i4IpMRouteSourceMaskLen,
                                                         &i4NextIpMRouteSourceMaskLen)
                   == SNMP_SUCCESS)
            {

                if (i4NextAddrType != IPVX_ADDR_FMLY_IPV4)
                {
                    i4Component = i4NextComponent;
                    i4AddrType = i4NextAddrType;
                    MEMCPY (IpMRouteGroup.pu1_OctetList,
                            NextIpMRouteGroup.pu1_OctetList,
                            NextIpMRouteGroup.i4_Length);
                    IpMRouteGroup.i4_Length = NextIpMRouteGroup.i4_Length;

                    MEMCPY (IpMRouteSource.pu1_OctetList,
                            NextIpMRouteSource.pu1_OctetList,
                            NextIpMRouteSource.i4_Length);
                    IpMRouteSource.i4_Length = NextIpMRouteSource.i4_Length;
                    i4IpMRouteSourceMaskLen = i4NextIpMRouteSourceMaskLen;
                    continue;
                }

                PTR_FETCH4 (MRouteIndex1.u4GrpAddr,
                            NextIpMRouteGroup.pu1_OctetList);
                PTR_FETCH4 (MRouteIndex1.u4SrcAddr,
                            NextIpMRouteSource.pu1_OctetList);
                MRI_MASKLEN_TO_MASK (i4IpMRouteSourceMaskLen,
                                     MRouteIndex1.u4SrcMask);

                i1Status = MRI_TRUE;
                break;
            }
        }
        else
        {
            PTR_FETCH4 (MRouteIndex1.u4GrpAddr, IpMRouteGroup.pu1_OctetList);
            PTR_FETCH4 (MRouteIndex1.u4SrcAddr, IpMRouteSource.pu1_OctetList);
            MRI_MASKLEN_TO_MASK (i4IpMRouteSourceMaskLen,
                                 MRouteIndex1.u4SrcMask);
            i1Status = MRI_TRUE;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetFirstIndexDvmrpIpMRTable (&MRouteIndex2.u4GrpAddr,
                                        &MRouteIndex2.u4SrcAddr,
                                        &MRouteIndex2.u4SrcMask) ==
        SNMP_SUCCESS)
    {
        MriUtilGetLowMRouteIndex (&MRouteIndex1, &MRouteIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetNextRouteIndex (0, 0, 0, &MRouteIndex2.u4GrpAddr,
                                    &MRouteIndex2.u4SrcAddr,
                                    &MRouteIndex2.u4SrcMask) == IGP_SUCCESS)
    {
        MriUtilGetLowMRouteIndex (&MRouteIndex1, &MRouteIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

    if (i1Status == MRI_TRUE)
    {
        *pu4IpMRouteGroup = MRouteIndex1.u4GrpAddr;
        *pu4IpMRouteSource = MRouteIndex1.u4SrcAddr;
        *pu4IpMRouteSourceMask = MRouteIndex1.u4SrcMask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteTable
 Input       :  The Indices
                IpMRouteGroup
                nextIpMRouteGroup
                IpMRouteSource
                nextIpMRouteSource
                IpMRouteSourceMask
                nextIpMRouteSourceMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpMRouteTable (UINT4 u4IpMRouteGroup,
                              UINT4 *pu4NextIpMRouteGroup,
                              UINT4 u4IpMRouteSource,
                              UINT4 *pu4NextIpMRouteSource,
                              UINT4 u4IpMRouteSourceMask,
                              UINT4 *pu4NextIpMRouteSourceMask)
{
    tMRouteTblIndex     MRouteIndex1;
    tMRouteTblIndex     MRouteIndex2;
    INT1                i1Status = MRI_FALSE;
#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4NextSourceMaskLen = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4NextAddrType = 0;
    INT4                i4Component = 0;
    INT4                i4NextComponent = 0;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    *pu4NextIpMRouteGroup = MRI_SET_ALL_BITS;
    *pu4NextIpMRouteSource = MRI_SET_ALL_BITS;
    *pu4NextIpMRouteSourceMask = MRI_SET_ALL_BITS;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    NextIpMRouteGroup.pu1_OctetList = au1NextRouteGroup;
    NextIpMRouteSource.pu1_OctetList = au1NextRouteSource;

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4SourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT;
         i4Component = i4NextComponent)
    {

        if (nmhGetNextIndexFsPimCmnIpMRouteTable (i4Component, &i4NextComponent,
                                                  i4AddrType, &i4NextAddrType,
                                                  &IpMRouteGroup,
                                                  &NextIpMRouteGroup,
                                                  &IpMRouteSource,
                                                  &NextIpMRouteSource,
                                                  i4SourceMaskLen,
                                                  &i4NextSourceMaskLen)
            == SNMP_SUCCESS)
        {
            if (i4NextAddrType != IPVX_ADDR_FMLY_IPV4)
            {
                i4Component = i4NextComponent;
                i4AddrType = i4NextAddrType;

                MEMCPY (IpMRouteGroup.pu1_OctetList,
                        NextIpMRouteGroup.pu1_OctetList,
                        NextIpMRouteGroup.i4_Length);
                IpMRouteGroup.i4_Length = NextIpMRouteGroup.i4_Length;

                MEMCPY (IpMRouteSource.pu1_OctetList,
                        NextIpMRouteSource.pu1_OctetList,
                        NextIpMRouteSource.i4_Length);
                IpMRouteSource.i4_Length = NextIpMRouteSource.i4_Length;
                i4SourceMaskLen = i4NextSourceMaskLen;
                continue;
            }

            if ((MEMCMP (IpMRouteGroup.pu1_OctetList,
                         NextIpMRouteGroup.pu1_OctetList,
                         IpMRouteGroup.i4_Length) == 0) &&
                (MEMCMP (IpMRouteSource.pu1_OctetList,
                         NextIpMRouteSource.pu1_OctetList,
                         IpMRouteSource.i4_Length) == 0) &&
                (i4SourceMaskLen == i4NextSourceMaskLen))
            {
                i4NextComponent = i4Component + 1;
                i4AddrType = IPVX_ADDR_FMLY_IPV4;
                continue;
            }

            PTR_FETCH4 (MRouteIndex1.u4GrpAddr,
                        NextIpMRouteGroup.pu1_OctetList);
            PTR_FETCH4 (MRouteIndex1.u4SrcAddr,
                        NextIpMRouteSource.pu1_OctetList);
            MRI_MASKLEN_TO_MASK (i4NextSourceMaskLen, MRouteIndex1.u4SrcMask);

            MRouteIndex2.u4GrpAddr = *pu4NextIpMRouteGroup;
            MRouteIndex2.u4SrcAddr = *pu4NextIpMRouteSource;
            MRouteIndex2.u4SrcMask = *pu4NextIpMRouteSourceMask;

            if (MriUtilCompareMRouteTblIndex (&MRouteIndex1, &MRouteIndex2)
                == MRI_FIRST_LT_SECOND)
            {
                *pu4NextIpMRouteGroup = MRouteIndex1.u4GrpAddr;
                *pu4NextIpMRouteSource = MRouteIndex1.u4SrcAddr;
                *pu4NextIpMRouteSourceMask = MRouteIndex1.u4SrcMask;
                i1Status = MRI_TRUE;
            }
            i4NextComponent = i4Component + 1;
            i4AddrType = IPVX_ADDR_FMLY_IPV4;
            continue;
        }
        i4NextComponent = i4Component + 1;
    }
#endif

#ifdef DVMRP_WANTED

    if (i1Status == MRI_TRUE)
    {
        MRouteIndex1.u4GrpAddr = *pu4NextIpMRouteGroup;
        MRouteIndex1.u4SrcAddr = *pu4NextIpMRouteSource;
        MRouteIndex1.u4SrcMask = *pu4NextIpMRouteSourceMask;
    }

    if (nmhGetNextIndexDvmrpIpMRTable (u4IpMRouteGroup, &MRouteIndex2.u4GrpAddr,
                                       u4IpMRouteSource,
                                       &MRouteIndex2.u4SrcAddr,
                                       u4IpMRouteSourceMask,
                                       &MRouteIndex2.u4SrcMask) == SNMP_SUCCESS)
    {
        MriUtilGetLowMRouteIndex (&MRouteIndex1, &MRouteIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetNextRouteIndex (u4IpMRouteGroup, u4IpMRouteSource,
                                    u4IpMRouteSourceMask,
                                    &MRouteIndex2.u4GrpAddr,
                                    &MRouteIndex2.u4SrcAddr,
                                    &MRouteIndex2.u4SrcMask) == IGP_SUCCESS)
    {
        MriUtilGetLowMRouteIndex (&MRouteIndex1, &MRouteIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

    if (i1Status == MRI_TRUE)
    {
        *pu4NextIpMRouteGroup = MRouteIndex1.u4GrpAddr;
        *pu4NextIpMRouteSource = MRouteIndex1.u4SrcAddr;
        *pu4NextIpMRouteSourceMask = MRouteIndex1.u4SrcMask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteUpstreamNeighbor
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteUpstreamNeighbor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteUpstreamNeighbor (UINT4 u4IpMRouteGroup,
                                UINT4 u4IpMRouteSource,
                                UINT4 u4IpMRouteSourceMask,
                                UINT4 *pu4RetValIpMRouteUpstreamNeighbor)
{

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteUpNbr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteUpNbr[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    IpMRouteUpNbr.pu1_OctetList = au1RouteUpNbr;

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteUpstreamNeighbor (i4Component,
                                                    IPVX_ADDR_FMLY_IPV4,
                                                    &IpMRouteGroup,
                                                    &IpMRouteSource,
                                                    i4IpMRouteSourceMaskLen,
                                                    &IpMRouteUpNbr)
            == SNMP_SUCCESS)
        {
            PTR_FETCH4 (*pu4RetValIpMRouteUpstreamNeighbor,
                        IpMRouteUpNbr.pu1_OctetList);
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED

    if (nmhGetDvmrpIpMRUpstreamNeighbor (u4IpMRouteGroup, u4IpMRouteSource,
                                         u4IpMRouteSourceMask,
                                         pu4RetValIpMRouteUpstreamNeighbor)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    *pu4RetValIpMRouteUpstreamNeighbor = 0;
    return SNMP_SUCCESS;
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInIfIndex
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteInIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInIfIndex (UINT4 u4IpMRouteGroup,
                         UINT4 u4IpMRouteSource,
                         UINT4 u4IpMRouteSourceMask,
                         INT4 *pi4RetValIpMRouteInIfIndex)
{

#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteInIfIndex (i4Component,
                                             IPVX_ADDR_FMLY_IPV4,
                                             &IpMRouteGroup,
                                             &IpMRouteSource,
                                             i4IpMRouteSourceMaskLen,
                                             pi4RetValIpMRouteInIfIndex)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRInIfIndex (u4IpMRouteGroup, u4IpMRouteSource,
                                  u4IpMRouteSourceMask,
                                  pi4RetValIpMRouteInIfIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask, IGP_IIFACE,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteInIfIndex = (INT4) u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteUpTime
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteUpTime (UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource,
                      UINT4 u4IpMRouteSourceMask,
                      UINT4 *pu4RetValIpMRouteUpTime)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteUpTime (i4Component,
                                          IPVX_ADDR_FMLY_IPV4,
                                          &IpMRouteGroup,
                                          &IpMRouteSource,
                                          i4IpMRouteSourceMaskLen,
                                          pu4RetValIpMRouteUpTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRUpTime (u4IpMRouteGroup, u4IpMRouteSource,
                               u4IpMRouteSourceMask, pu4RetValIpMRouteUpTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask, IGP_UPTIME,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteUpTime = u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteExpiryTime
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteExpiryTime (UINT4 u4IpMRouteGroup,
                          UINT4 u4IpMRouteSource,
                          UINT4 u4IpMRouteSourceMask,
                          UINT4 *pu4RetValIpMRouteExpiryTime)
{

#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteExpiryTime (i4Component,
                                              IPVX_ADDR_FMLY_IPV4,
                                              &IpMRouteGroup,
                                              &IpMRouteSource,
                                              i4IpMRouteSourceMaskLen,
                                              pu4RetValIpMRouteExpiryTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRExpiryTime (u4IpMRouteGroup, u4IpMRouteSource,
                                   u4IpMRouteSourceMask,
                                   pu4RetValIpMRouteExpiryTime) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_EXPIRY_TIME, &u8ObjectValue)
        == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteExpiryTime = u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRoutePkts
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRoutePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRoutePkts (UINT4 u4IpMRouteGroup,
                    UINT4 u4IpMRouteSource,
                    UINT4 u4IpMRouteSourceMask, UINT4 *pu4RetValIpMRoutePkts)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRoutePkts (i4Component,
                                        IPVX_ADDR_FMLY_IPV4,
                                        &IpMRouteGroup,
                                        &IpMRouteSource,
                                        i4IpMRouteSourceMaskLen,
                                        pu4RetValIpMRoutePkts) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRPkts (u4IpMRouteGroup, u4IpMRouteSource,
                             u4IpMRouteSourceMask, pu4RetValIpMRoutePkts)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_FWD_PACKETS,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRoutePkts = u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteDifferentInIfPackets
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteDifferentInIfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIpMRouteDifferentInIfPackets
    (UINT4 u4IpMRouteGroup,
     UINT4 u4IpMRouteSource,
     UINT4 u4IpMRouteSourceMask, UINT4 *pu4RetValIpMRouteDifferentInIfPackets)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteDifferentInIfPackets (i4Component,
                                                        IPVX_ADDR_FMLY_IPV4,
                                                        &IpMRouteGroup,
                                                        &IpMRouteSource,
                                                        i4IpMRouteSourceMaskLen,
                                                        pu4RetValIpMRouteDifferentInIfPackets)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRDifferentInIfPackets (u4IpMRouteGroup, u4IpMRouteSource,
                                             u4IpMRouteSourceMask,
                                             pu4RetValIpMRouteDifferentInIfPackets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_DIFF_IIFACE,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteDifferentInIfPackets = u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteOctets
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteOctets (UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource,
                      UINT4 u4IpMRouteSourceMask,
                      UINT4 *pu4RetValIpMRouteOctets)
{

#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteOctets (i4Component,
                                          IPVX_ADDR_FMLY_IPV4,
                                          &IpMRouteGroup,
                                          &IpMRouteSource,
                                          i4IpMRouteSourceMaskLen,
                                          pu4RetValIpMRouteOctets)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMROctets (u4IpMRouteGroup, u4IpMRouteSource,
                               u4IpMRouteSourceMask, pu4RetValIpMRouteOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_FWD_OCTETS,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteOctets = u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteProtocol
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteProtocol (UINT4 u4IpMRouteGroup,
                        UINT4 u4IpMRouteSource,
                        UINT4 u4IpMRouteSourceMask,
                        INT4 *pi4RetValIpMRouteProtocol)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteProtocol (i4Component,
                                            IPVX_ADDR_FMLY_IPV4,
                                            &IpMRouteGroup,
                                            &IpMRouteSource,
                                            i4IpMRouteSourceMaskLen,
                                            pi4RetValIpMRouteProtocol)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRProtocol (u4IpMRouteGroup, u4IpMRouteSource,
                                 u4IpMRouteSourceMask,
                                 pi4RetValIpMRouteProtocol) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_MCAST_PROTOCOL,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteProtocol = (INT4) u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtProto
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteRtProto (UINT4 u4IpMRouteGroup,
                       UINT4 u4IpMRouteSource,
                       UINT4 u4IpMRouteSourceMask,
                       INT4 *pi4RetValIpMRouteRtProto)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteRtProto (i4Component,
                                           IPVX_ADDR_FMLY_IPV4,
                                           &IpMRouteGroup,
                                           &IpMRouteSource,
                                           i4IpMRouteSourceMaskLen,
                                           pi4RetValIpMRouteRtProto)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRRtProto (u4IpMRouteGroup, u4IpMRouteSource,
                                u4IpMRouteSourceMask, pi4RetValIpMRouteRtProto)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_RT_PROTOCOL,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteRtProto = (INT4) u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtAddress
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteRtAddress (UINT4 u4IpMRouteGroup,
                         UINT4 u4IpMRouteSource,
                         UINT4 u4IpMRouteSourceMask,
                         UINT4 *pu4RetValIpMRouteRtAddress)
{

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteRtAddress;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteRtAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    IpMRouteRtAddress.pu1_OctetList = au1RouteRtAddr;

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteRtAddress (i4Component,
                                             IPVX_ADDR_FMLY_IPV4,
                                             &IpMRouteGroup,
                                             &IpMRouteSource,
                                             i4IpMRouteSourceMaskLen,
                                             &IpMRouteRtAddress)
            == SNMP_SUCCESS)
        {
            PTR_FETCH4 (*pu4RetValIpMRouteRtAddress,
                        IpMRouteRtAddress.pu1_OctetList);
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRRtAddress (u4IpMRouteGroup, u4IpMRouteSource,
                                  u4IpMRouteSourceMask,
                                  pu4RetValIpMRouteRtAddress) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    *pu4RetValIpMRouteRtAddress = 0;
    return SNMP_SUCCESS;
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtMask
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteRtMask (UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource,
                      UINT4 u4IpMRouteSourceMask,
                      UINT4 *pu4RetValIpMRouteRtMask)
{

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    INT4                i4MaskLen = 0;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteRtMasklen (i4Component,
                                             IPVX_ADDR_FMLY_IPV4,
                                             &IpMRouteGroup,
                                             &IpMRouteSource,
                                             i4IpMRouteSourceMaskLen,
                                             &i4MaskLen) == SNMP_SUCCESS)
        {
            MRI_MASKLEN_TO_MASK (i4MaskLen, *pu4RetValIpMRouteRtMask);
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRRtMask (u4IpMRouteGroup, u4IpMRouteSource,
                               u4IpMRouteSourceMask, pu4RetValIpMRouteRtMask)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    *pu4RetValIpMRouteRtMask = 0;
    return SNMP_SUCCESS;
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteRtType
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteRtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteRtType (UINT4 u4IpMRouteGroup,
                      UINT4 u4IpMRouteSource,
                      UINT4 u4IpMRouteSourceMask, INT4 *pi4RetValIpMRouteRtType)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectValue;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteRtType (i4Component,
                                          IPVX_ADDR_FMLY_IPV4,
                                          &IpMRouteGroup,
                                          &IpMRouteSource,
                                          i4IpMRouteSourceMaskLen,
                                          pi4RetValIpMRouteRtType)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRRtType (u4IpMRouteGroup, u4IpMRouteSource,
                               u4IpMRouteSourceMask, pi4RetValIpMRouteRtType)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_ROUTE_TYPE,
                                            &u8ObjectValue) == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteRtType = (INT4) u8ObjectValue.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteHCOctets
 Input       :  The Indices
                IpMRouteGroup
                IpMRouteSource
                IpMRouteSourceMask

                The Object 
                retValIpMRouteHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteHCOctets (UINT4 u4IpMRouteGroup,
                        UINT4 u4IpMRouteSource,
                        UINT4 u4IpMRouteSourceMask,
                        tSNMP_COUNTER64_TYPE * pu8RetValIpMRouteHCOctets)
{

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteGroup = 0;
    UINT4               u4TempIpMRouteSource = 0;
    UINT4               u4TempIpMRouteSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteGroup = OSIX_NTOHL (u4IpMRouteGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteSource = OSIX_NTOHL (u4IpMRouteSource);
    MEMCPY (IpMRouteSource.pu1_OctetList, (UINT1 *) &u4TempIpMRouteSource,
            IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteSourceMask = OSIX_NTOHL (u4IpMRouteSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteSourceMask, i4IpMRouteSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhGetFsPimCmnIpMRouteHCOctets (i4Component,
                                            IPVX_ADDR_FMLY_IPV4,
                                            &IpMRouteGroup,
                                            &IpMRouteSource,
                                            i4IpMRouteSourceMaskLen,
                                            pu8RetValIpMRouteHCOctets)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMRHCOctets (u4IpMRouteGroup, u4IpMRouteSource,
                                 u4IpMRouteSourceMask,
                                 pu8RetValIpMRouteHCOctets) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetRouteEntryObjectByName (u4IpMRouteGroup, u4IpMRouteSource,
                                            u4IpMRouteSourceMask,
                                            IGP_FWD_HCOCTETS,
                                            pu8RetValIpMRouteHCOctets)
        == IGP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IpMRouteNextHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpMRouteNextHopTable (UINT4 u4IpMRouteNextHopGroup,
                                              UINT4 u4IpMRouteNextHopSource,
                                              UINT4 u4IpMRouteNextHopSourceMask,
                                              INT4 i4IpMRouteNextHopIfIndex,
                                              UINT4 u4IpMRouteNextHopAddress)
{
#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddress;
    INT4                i4IpMRouteNextHopSourceMaskLen = 0;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHop[IPVX_IPV6_ADDR_LEN];
    INT4                i4Component = 1;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    IpMRouteNextHopGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteNextHopGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteNextHopGroup.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopGroup, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopSource.pu1_OctetList = au1RouteSource;
    IpMRouteNextHopSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteNextHopSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddress.pu1_OctetList = au1RouteNextHop;
    IpMRouteNextHopAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddress.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask,
                         i4IpMRouteNextHopSourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {
        if (nmhValidateIndexInstanceFsPimCmnIpMRouteNextHopTable (i4Component,
                                                                  IPVX_ADDR_FMLY_IPV4,
                                                                  &IpMRouteNextHopGroup,
                                                                  &IpMRouteNextHopSource,
                                                                  i4IpMRouteNextHopSourceMaskLen,
                                                                  i4IpMRouteNextHopIfIndex,
                                                                  &IpMRouteNextHopAddress)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpIpMNextHopTable (u4IpMRouteNextHopGroup,
                                                      u4IpMRouteNextHopSource,
                                                      u4IpMRouteNextHopSourceMask,
                                                      i4IpMRouteNextHopIfIndex,
                                                      u4IpMRouteNextHopAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyValidateNextHopTblIndex (&IgpNextHopIndex) == IGP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpMRouteNextHopTable (UINT4 *pu4IpMRouteNextHopGroup,
                                      UINT4 *pu4IpMRouteNextHopSource,
                                      UINT4 *pu4IpMRouteNextHopSourceMask,
                                      INT4 *pi4IpMRouteNextHopIfIndex,
                                      UINT4 *pu4IpMRouteNextHopAddress)
{
    tMNextHopTblIndex   MNextHopIndex1;
    INT1                i1Status = MRI_FALSE;

#if defined(DVMRP_WANTED) || defined(IGMPPRXY_WANTED)
    tMNextHopTblIndex   MNextHopIndex2;
#endif

#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
    tIgpNextHopTblIndex IgpTempNextHopIndex;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHop[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteNextHop[IPVX_IPV6_ADDR_LEN];
    INT4                i4IpMRouteNextHopIfIndex = 0;
    INT4                i4NextIpMRouteNextHopIfIndex = 0;
    INT4                i4IpMRouteSourceMaskLen = 0;
    INT4                i4NextIpMRouteSourceMaskLen = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Component = 0;
    INT4                i4NextComponent = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHop;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    NextIpMRouteGroup.pu1_OctetList = au1NextRouteGroup;
    NextIpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;

    NextIpMRouteSource.pu1_OctetList = au1NextRouteSource;
    NextIpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;

    NextIpMRouteNextHopAddr.pu1_OctetList = au1NextRouteNextHop;
    NextIpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetFirstIndexFsPimCmnIpMRouteNextHopTable (&i4Component, &i4AddrType,
                                                      &IpMRouteGroup,
                                                      &IpMRouteSource,
                                                      &i4IpMRouteSourceMaskLen,
                                                      &i4IpMRouteNextHopIfIndex,
                                                      &IpMRouteNextHopAddr) ==
        SNMP_SUCCESS)
    {
        if (i4AddrType != IPVX_ADDR_FMLY_IPV4)
        {
            while (nmhGetNextIndexFsPimCmnIpMRouteNextHopTable (i4Component,
                                                                &i4NextComponent,
                                                                i4AddrType,
                                                                &i4NextAddrType,
                                                                &IpMRouteGroup,
                                                                &NextIpMRouteGroup,
                                                                &IpMRouteSource,
                                                                &NextIpMRouteSource,
                                                                i4IpMRouteSourceMaskLen,
                                                                &i4NextIpMRouteSourceMaskLen,
                                                                i4IpMRouteNextHopIfIndex,
                                                                &i4NextIpMRouteNextHopIfIndex,
                                                                &IpMRouteNextHopAddr,
                                                                &NextIpMRouteNextHopAddr)
                   == SNMP_SUCCESS)
            {

                if (i4NextAddrType != IPVX_ADDR_FMLY_IPV4)
                {
                    i4Component = i4NextComponent;
                    i4AddrType = i4NextAddrType;

                    MEMCPY (IpMRouteGroup.pu1_OctetList,
                            NextIpMRouteGroup.pu1_OctetList,
                            NextIpMRouteGroup.i4_Length);
                    IpMRouteGroup.i4_Length = NextIpMRouteGroup.i4_Length;

                    MEMCPY (IpMRouteSource.pu1_OctetList,
                            NextIpMRouteSource.pu1_OctetList,
                            NextIpMRouteSource.i4_Length);
                    IpMRouteSource.i4_Length = NextIpMRouteSource.i4_Length;

                    i4IpMRouteSourceMaskLen = i4NextIpMRouteSourceMaskLen;
                    i4IpMRouteNextHopIfIndex = i4NextIpMRouteNextHopIfIndex;

                    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
                            NextIpMRouteNextHopAddr.pu1_OctetList,
                            NextIpMRouteNextHopAddr.i4_Length);
                    IpMRouteNextHopAddr.i4_Length =
                        NextIpMRouteNextHopAddr.i4_Length;

                    continue;
                }

                PTR_FETCH4 (MNextHopIndex1.u4GrpAddr,
                            NextIpMRouteGroup.pu1_OctetList);
                PTR_FETCH4 (MNextHopIndex1.u4SrcAddr,
                            NextIpMRouteSource.pu1_OctetList);
                MRI_MASKLEN_TO_MASK (i4NextIpMRouteSourceMaskLen,
                                     MNextHopIndex1.u4SrcMask);
                MNextHopIndex1.i4OIfIndex = i4NextIpMRouteNextHopIfIndex;
                /* Next-Hop should be always group address */
                PTR_FETCH4 (MNextHopIndex1.u4NextHop,
                            NextIpMRouteGroup.pu1_OctetList);
                i1Status = MRI_TRUE;
                break;
            }
        }
        else
        {
            PTR_FETCH4 (MNextHopIndex1.u4GrpAddr, IpMRouteGroup.pu1_OctetList);
            PTR_FETCH4 (MNextHopIndex1.u4SrcAddr, IpMRouteSource.pu1_OctetList);
            MRI_MASKLEN_TO_MASK (i4IpMRouteSourceMaskLen,
                                 MNextHopIndex1.u4SrcMask);
            MNextHopIndex1.i4OIfIndex = i4IpMRouteNextHopIfIndex;
            /* Next-Hop should be always group address */
            PTR_FETCH4 (MNextHopIndex1.u4NextHop, IpMRouteGroup.pu1_OctetList);
            i1Status = MRI_TRUE;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetFirstIndexDvmrpIpMNextHopTable (&MNextHopIndex2.u4GrpAddr,
                                              &MNextHopIndex2.u4SrcAddr,
                                              &MNextHopIndex2.u4SrcMask,
                                              &MNextHopIndex2.i4OIfIndex,
                                              &MNextHopIndex2.u4NextHop)
        == SNMP_SUCCESS)
    {
        MriUtilGetLowMNextHopIndex (&MNextHopIndex1, &MNextHopIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

#ifdef IGMPPRXY_WANTED

    MEMSET (&IgpTempNextHopIndex, 0, sizeof (tIgpNextHopTblIndex));

    /* Pass the first index as all zeros. So we can get the first index */
    if (IgmpProxyGetNextNextHopIndex (&IgpTempNextHopIndex, &IgpNextHopIndex)
        == IGP_SUCCESS)
    {
        MNextHopIndex2.u4GrpAddr = IgpNextHopIndex.u4GrpAddr;
        MNextHopIndex2.u4SrcAddr = IgpNextHopIndex.u4SrcAddr;
        MNextHopIndex2.u4SrcMask = IgpNextHopIndex.u4SrcMask;
        MNextHopIndex2.i4OIfIndex = IgpNextHopIndex.i4OIfIndex;
        MNextHopIndex2.u4NextHop = IgpNextHopIndex.u4NextHopAddr;

        MriUtilGetLowMNextHopIndex (&MNextHopIndex1, &MNextHopIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

    if (i1Status == MRI_TRUE)
    {
        *pu4IpMRouteNextHopGroup = MNextHopIndex1.u4GrpAddr;
        *pu4IpMRouteNextHopSource = MNextHopIndex1.u4SrcAddr;
        *pu4IpMRouteNextHopSourceMask = MNextHopIndex1.u4SrcMask;
        *pi4IpMRouteNextHopIfIndex = MNextHopIndex1.i4OIfIndex;
        *pu4IpMRouteNextHopAddress = MNextHopIndex1.u4NextHop;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteNextHopTable
 Input       :  The Indices
                IpMRouteNextHopGroup
                nextIpMRouteNextHopGroup
                IpMRouteNextHopSource
                nextIpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                nextIpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                nextIpMRouteNextHopIfIndex
                IpMRouteNextHopAddress
                nextIpMRouteNextHopAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpMRouteNextHopTable (UINT4 u4IpMRouteNextHopGroup,
                                     UINT4 *pu4NextIpMRouteNextHopGroup,
                                     UINT4 u4IpMRouteNextHopSource,
                                     UINT4 *pu4NextIpMRouteNextHopSource,
                                     UINT4 u4IpMRouteNextHopSourceMask,
                                     UINT4 *pu4NextIpMRouteNextHopSourceMask,
                                     INT4 i4IpMRouteNextHopIfIndex,
                                     INT4 *pi4NextIpMRouteNextHopIfIndex,
                                     UINT4 u4IpMRouteNextHopAddress,
                                     UINT4 *pu4NextIpMRouteNextHopAddress)
{
    tMNextHopTblIndex   MNextHopIndex1;
    tMNextHopTblIndex   MNextHopIndex2;
    INT1                i1Status = MRI_FALSE;

#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex1;
    tIgpNextHopTblIndex IgpNextHopIndex2;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    tSNMP_OCTET_STRING_TYPE NextIpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextRouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4NextSourceMaskLen = 0;
    INT4                i4NextHopIfIndex = 0;
    INT4                i4NextNextHopIfIndex = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4NextAddrType = 0;
    INT4                i4Component = 0;
    INT4                i4NextComponent = 0;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    *pu4NextIpMRouteNextHopGroup = MRI_SET_ALL_BITS;
    *pu4NextIpMRouteNextHopSource = MRI_SET_ALL_BITS;
    *pu4NextIpMRouteNextHopSourceMask = MRI_SET_ALL_BITS;
    *pi4NextIpMRouteNextHopIfIndex = MRI_SET_ALL_BITS;
    *pu4NextIpMRouteNextHopAddress = MRI_SET_ALL_BITS;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteNextHopGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHopAddr;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    NextIpMRouteGroup.pu1_OctetList = au1NextRouteGroup;
    NextIpMRouteSource.pu1_OctetList = au1NextRouteSource;
    NextIpMRouteNextHopAddr.pu1_OctetList = au1NextRouteNextHopAddr;

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask, i4SourceMaskLen);

    i4NextHopIfIndex = i4IpMRouteNextHopIfIndex;

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT;
         i4Component = i4NextComponent)
    {

        if (nmhGetNextIndexFsPimCmnIpMRouteNextHopTable (i4Component,
                                                         &i4NextComponent,
                                                         i4AddrType,
                                                         &i4NextAddrType,
                                                         &IpMRouteGroup,
                                                         &NextIpMRouteGroup,
                                                         &IpMRouteSource,
                                                         &NextIpMRouteSource,
                                                         i4SourceMaskLen,
                                                         &i4NextSourceMaskLen,
                                                         i4NextHopIfIndex,
                                                         &i4NextNextHopIfIndex,
                                                         &IpMRouteNextHopAddr,
                                                         &NextIpMRouteNextHopAddr)
            == SNMP_SUCCESS)
        {
            if (i4NextAddrType != IPVX_ADDR_FMLY_IPV4)
            {
                i4Component = i4NextComponent;
                i4AddrType = i4NextAddrType;

                MEMCPY (IpMRouteGroup.pu1_OctetList,
                        NextIpMRouteGroup.pu1_OctetList,
                        NextIpMRouteGroup.i4_Length);
                IpMRouteGroup.i4_Length = NextIpMRouteGroup.i4_Length;

                MEMCPY (IpMRouteSource.pu1_OctetList,
                        NextIpMRouteSource.pu1_OctetList,
                        NextIpMRouteSource.i4_Length);
                IpMRouteSource.i4_Length = NextIpMRouteSource.i4_Length;
                i4SourceMaskLen = i4NextSourceMaskLen;

                MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
                        NextIpMRouteNextHopAddr.pu1_OctetList,
                        NextIpMRouteNextHopAddr.i4_Length);
                IpMRouteNextHopAddr.i4_Length =
                    NextIpMRouteNextHopAddr.i4_Length;
                i4NextHopIfIndex = i4NextNextHopIfIndex;
                continue;
            }

            PTR_FETCH4 (MNextHopIndex1.u4GrpAddr,
                        NextIpMRouteGroup.pu1_OctetList);
            PTR_FETCH4 (MNextHopIndex1.u4SrcAddr,
                        NextIpMRouteSource.pu1_OctetList);
            MRI_MASKLEN_TO_MASK (i4NextSourceMaskLen, MNextHopIndex1.u4SrcMask);
            MNextHopIndex1.i4OIfIndex = i4NextNextHopIfIndex;
            PTR_FETCH4 (MNextHopIndex1.u4NextHop,
                        NextIpMRouteGroup.pu1_OctetList);

            MNextHopIndex2.u4GrpAddr = *pu4NextIpMRouteNextHopGroup;
            MNextHopIndex2.u4SrcAddr = *pu4NextIpMRouteNextHopSource;
            MNextHopIndex2.u4SrcMask = *pu4NextIpMRouteNextHopSourceMask;
            MNextHopIndex2.i4OIfIndex = *pi4NextIpMRouteNextHopIfIndex;
            MNextHopIndex2.u4NextHop = *pu4NextIpMRouteNextHopAddress;

            if (MriUtilCompareMNextHopTblIndex
                (&MNextHopIndex1, &MNextHopIndex2) == MRI_FIRST_LT_SECOND)
            {
                *pu4NextIpMRouteNextHopGroup = MNextHopIndex1.u4GrpAddr;
                *pu4NextIpMRouteNextHopSource = MNextHopIndex1.u4SrcAddr;
                *pu4NextIpMRouteNextHopSourceMask = MNextHopIndex1.u4SrcMask;
                *pi4NextIpMRouteNextHopIfIndex = MNextHopIndex1.i4OIfIndex;
                *pu4NextIpMRouteNextHopAddress = MNextHopIndex1.u4NextHop;
                i1Status = MRI_TRUE;
            }

            i4NextComponent = i4Component + 1;
            i4AddrType = IPVX_ADDR_FMLY_IPV4;
            continue;
        }
        i4NextComponent = i4Component + 1;
    }
#endif

#ifdef DVMRP_WANTED
    if (i1Status == MRI_TRUE)
    {
        MNextHopIndex1.u4GrpAddr = *pu4NextIpMRouteNextHopGroup;
        MNextHopIndex1.u4SrcAddr = *pu4NextIpMRouteNextHopSource;
        MNextHopIndex1.u4SrcMask = *pu4NextIpMRouteNextHopSourceMask;
        MNextHopIndex1.i4OIfIndex = *pi4NextIpMRouteNextHopIfIndex;
        MNextHopIndex1.u4NextHop = *pu4NextIpMRouteNextHopAddress;
    }

    if (nmhGetNextIndexDvmrpIpMNextHopTable (u4IpMRouteNextHopGroup,
                                             &MNextHopIndex2.u4GrpAddr,
                                             u4IpMRouteNextHopSource,
                                             &MNextHopIndex2.u4SrcAddr,
                                             u4IpMRouteNextHopSourceMask,
                                             &MNextHopIndex2.u4SrcMask,
                                             i4IpMRouteNextHopIfIndex,
                                             &MNextHopIndex2.i4OIfIndex,
                                             u4IpMRouteNextHopAddress,
                                             &MNextHopIndex2.u4NextHop)
        == SNMP_SUCCESS)
    {
        MriUtilGetLowMNextHopIndex (&MNextHopIndex1, &MNextHopIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex1.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex1.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex1.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex1.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex1.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyGetNextNextHopIndex (&IgpNextHopIndex1, &IgpNextHopIndex2)
        == IGP_SUCCESS)
    {
        MNextHopIndex2.u4GrpAddr = IgpNextHopIndex2.u4GrpAddr;
        MNextHopIndex2.u4SrcAddr = IgpNextHopIndex2.u4SrcAddr;
        MNextHopIndex2.u4SrcMask = IgpNextHopIndex2.u4SrcMask;
        MNextHopIndex2.i4OIfIndex = IgpNextHopIndex2.i4OIfIndex;
        MNextHopIndex2.u4NextHop = IgpNextHopIndex2.u4NextHopAddr;

        MriUtilGetLowMNextHopIndex (&MNextHopIndex1, &MNextHopIndex2, i1Status);
        i1Status = MRI_TRUE;
    }
#endif

    if (i1Status == MRI_TRUE)
    {
        *pu4NextIpMRouteNextHopGroup = MNextHopIndex1.u4GrpAddr;
        *pu4NextIpMRouteNextHopSource = MNextHopIndex1.u4SrcAddr;
        *pu4NextIpMRouteNextHopSourceMask = MNextHopIndex1.u4SrcMask;
        *pi4NextIpMRouteNextHopIfIndex = MNextHopIndex1.i4OIfIndex;
        *pu4NextIpMRouteNextHopAddress = MNextHopIndex1.u4NextHop;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopState
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteNextHopState (UINT4 u4IpMRouteNextHopGroup,
                            UINT4 u4IpMRouteNextHopSource,
                            UINT4 u4IpMRouteNextHopSourceMask,
                            INT4 i4IpMRouteNextHopIfIndex,
                            UINT4 u4IpMRouteNextHopAddress,
                            INT4 *pi4RetValIpMRouteNextHopState)
{
#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
    UINT4               u4ObjValue = 0;
#endif
#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4Component = 0;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteNextHopGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHopAddr;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask, i4SourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {

        if (nmhGetFsPimCmnIpMRouteNextHopState (i4Component,
                                                IPVX_ADDR_FMLY_IPV4,
                                                &IpMRouteGroup,
                                                &IpMRouteSource,
                                                i4SourceMaskLen,
                                                i4IpMRouteNextHopIfIndex,
                                                &IpMRouteNextHopAddr,
                                                pi4RetValIpMRouteNextHopState)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMNextHopState (u4IpMRouteNextHopGroup,
                                    u4IpMRouteNextHopSource,
                                    u4IpMRouteNextHopSourceMask,
                                    i4IpMRouteNextHopIfIndex,
                                    u4IpMRouteNextHopAddress,
                                    pi4RetValIpMRouteNextHopState)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyGetNextHopEntryObjectByName (&IgpNextHopIndex, IGP_STATE,
                                              &u4ObjValue) == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteNextHopState = (UINT4) u4ObjValue;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopUpTime
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteNextHopUpTime (UINT4 u4IpMRouteNextHopGroup,
                             UINT4 u4IpMRouteNextHopSource,
                             UINT4 u4IpMRouteNextHopSourceMask,
                             INT4 i4IpMRouteNextHopIfIndex,
                             UINT4 u4IpMRouteNextHopAddress,
                             UINT4 *pu4RetValIpMRouteNextHopUpTime)
{
#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
    UINT4               u4ObjValue = 0;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4Component = 0;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteNextHopGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHopAddr;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask, i4SourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {

        if (nmhGetFsPimCmnIpMRouteNextHopUpTime (i4Component,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 &IpMRouteGroup,
                                                 &IpMRouteSource,
                                                 i4SourceMaskLen,
                                                 i4IpMRouteNextHopIfIndex,
                                                 &IpMRouteNextHopAddr,
                                                 pu4RetValIpMRouteNextHopUpTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMNextHopUpTime (u4IpMRouteNextHopGroup,
                                     u4IpMRouteNextHopSource,
                                     u4IpMRouteNextHopSourceMask,
                                     i4IpMRouteNextHopIfIndex,
                                     u4IpMRouteNextHopAddress,
                                     pu4RetValIpMRouteNextHopUpTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyGetNextHopEntryObjectByName (&IgpNextHopIndex, IGP_UPTIME,
                                              &u4ObjValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteNextHopUpTime = u4ObjValue;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopExpiryTime
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteNextHopExpiryTime (UINT4 u4IpMRouteNextHopGroup,
                                 UINT4 u4IpMRouteNextHopSource,
                                 UINT4 u4IpMRouteNextHopSourceMask,
                                 INT4 i4IpMRouteNextHopIfIndex,
                                 UINT4 u4IpMRouteNextHopAddress,
                                 UINT4 *pu4RetValIpMRouteNextHopExpiryTime)
{
#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
    UINT4               u4ObjValue = 0;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4Component = 0;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteNextHopGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHopAddr;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask, i4SourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {

        if (nmhGetFsPimCmnIpMRouteNextHopExpiryTime (i4Component,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     &IpMRouteGroup,
                                                     &IpMRouteSource,
                                                     i4SourceMaskLen,
                                                     i4IpMRouteNextHopIfIndex,
                                                     &IpMRouteNextHopAddr,
                                                     pu4RetValIpMRouteNextHopExpiryTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMNextHopExpiryTime (u4IpMRouteNextHopGroup,
                                         u4IpMRouteNextHopSource,
                                         u4IpMRouteNextHopSourceMask,
                                         i4IpMRouteNextHopIfIndex,
                                         u4IpMRouteNextHopAddress,
                                         pu4RetValIpMRouteNextHopExpiryTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyGetNextHopEntryObjectByName (&IgpNextHopIndex, IGP_EXPIRY_TIME,
                                              &u4ObjValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteNextHopExpiryTime = u4ObjValue;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopClosestMemberHops
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopClosestMemberHops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteNextHopClosestMemberHops (UINT4 u4IpMRouteNextHopGroup,
                                        UINT4 u4IpMRouteNextHopSource,
                                        UINT4 u4IpMRouteNextHopSourceMask,
                                        INT4 i4IpMRouteNextHopIfIndex,
                                        UINT4 u4IpMRouteNextHopAddress,
                                        INT4
                                        *pi4RetValIpMRouteNextHopClosestMemberHops)
{
    *pi4RetValIpMRouteNextHopClosestMemberHops = 0;

    UNUSED_PARAM (u4IpMRouteNextHopGroup);
    UNUSED_PARAM (u4IpMRouteNextHopSource);
    UNUSED_PARAM (u4IpMRouteNextHopSourceMask);
    UNUSED_PARAM (i4IpMRouteNextHopIfIndex);
    UNUSED_PARAM (u4IpMRouteNextHopAddress);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopProtocol
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteNextHopProtocol (UINT4 u4IpMRouteNextHopGroup,
                               UINT4 u4IpMRouteNextHopSource,
                               UINT4 u4IpMRouteNextHopSourceMask,
                               INT4 i4IpMRouteNextHopIfIndex,
                               UINT4 u4IpMRouteNextHopAddress,
                               INT4 *pi4RetValIpMRouteNextHopProtocol)
{
#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
    UINT4               u4ObjValue = 0;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4Component = 0;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteNextHopGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHopAddr;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask, i4SourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {

        if (nmhGetFsPimCmnIpMRouteNextHopProtocol (i4Component,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   &IpMRouteGroup,
                                                   &IpMRouteSource,
                                                   i4SourceMaskLen,
                                                   i4IpMRouteNextHopIfIndex,
                                                   &IpMRouteNextHopAddr,
                                                   pi4RetValIpMRouteNextHopProtocol)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMNextHopProtocol (u4IpMRouteNextHopGroup,
                                       u4IpMRouteNextHopSource,
                                       u4IpMRouteNextHopSourceMask,
                                       i4IpMRouteNextHopIfIndex,
                                       u4IpMRouteNextHopAddress,
                                       pi4RetValIpMRouteNextHopProtocol)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyGetNextHopEntryObjectByName (&IgpNextHopIndex,
                                              IGP_MCAST_PROTOCOL,
                                              &u4ObjValue) == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteNextHopProtocol = (INT4) u4ObjValue;
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteNextHopPkts
 Input       :  The Indices
                IpMRouteNextHopGroup
                IpMRouteNextHopSource
                IpMRouteNextHopSourceMask
                IpMRouteNextHopIfIndex
                IpMRouteNextHopAddress

                The Object 
                retValIpMRouteNextHopPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteNextHopPkts (UINT4 u4IpMRouteNextHopGroup,
                           UINT4 u4IpMRouteNextHopSource,
                           UINT4 u4IpMRouteNextHopSourceMask,
                           INT4 i4IpMRouteNextHopIfIndex,
                           UINT4 u4IpMRouteNextHopAddress,
                           UINT4 *pu4RetValIpMRouteNextHopPkts)
{
#ifdef IGMPPRXY_WANTED
    tIgpNextHopTblIndex IgpNextHopIndex;
    UINT4               u4ObjValue = 0;
#endif

#ifdef PIM_WANTED
    tSNMP_OCTET_STRING_TYPE IpMRouteGroup;
    tSNMP_OCTET_STRING_TYPE IpMRouteSource;
    tSNMP_OCTET_STRING_TYPE IpMRouteNextHopAddr;
    UINT1               au1RouteGroup[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteSource[IPVX_IPV6_ADDR_LEN];
    UINT1               au1RouteNextHopAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4SourceMaskLen = 0;
    INT4                i4Component = 0;
    UINT4               u4TempIpMRouteNextHopGroup = 0;
    UINT4               u4TempIpMRouteNextHopSource = 0;
    UINT4               u4TempIpMRouteNextHopAddress = 0;
    UINT4               u4TempIpMRouteNextHopSourceMask = 0;

    IpMRouteGroup.pu1_OctetList = au1RouteGroup;
    IpMRouteGroup.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopGroup = OSIX_NTOHL (u4IpMRouteNextHopGroup);
    MEMCPY (IpMRouteGroup.pu1_OctetList, (UINT1 *) &u4TempIpMRouteNextHopGroup,
            IPVX_IPV4_ADDR_LEN);

    IpMRouteSource.pu1_OctetList = au1RouteSource;
    IpMRouteSource.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopSource = OSIX_NTOHL (u4IpMRouteNextHopSource);
    MEMCPY (IpMRouteSource.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopSource, IPVX_IPV4_ADDR_LEN);

    IpMRouteNextHopAddr.pu1_OctetList = au1RouteNextHopAddr;
    IpMRouteNextHopAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4TempIpMRouteNextHopAddress = OSIX_NTOHL (u4IpMRouteNextHopAddress);
    MEMCPY (IpMRouteNextHopAddr.pu1_OctetList,
            (UINT1 *) &u4TempIpMRouteNextHopAddress, IPVX_IPV4_ADDR_LEN);

    u4TempIpMRouteNextHopSourceMask = OSIX_NTOHL (u4IpMRouteNextHopSourceMask);
    MRI_MASK_TO_MASKLEN (u4TempIpMRouteNextHopSourceMask, i4SourceMaskLen);

    for (i4Component = 1; i4Component < PIMSM_MAX_COMPONENT; i4Component++)
    {

        if (nmhGetFsPimCmnIpMRouteNextHopPkts (i4Component,
                                               IPVX_ADDR_FMLY_IPV4,
                                               &IpMRouteGroup,
                                               &IpMRouteSource,
                                               i4SourceMaskLen,
                                               i4IpMRouteNextHopIfIndex,
                                               &IpMRouteNextHopAddr,
                                               pu4RetValIpMRouteNextHopPkts)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpIpMNextHopPkts (u4IpMRouteNextHopGroup,
                                   u4IpMRouteNextHopSource,
                                   u4IpMRouteNextHopSourceMask,
                                   i4IpMRouteNextHopIfIndex,
                                   u4IpMRouteNextHopAddress,
                                   pu4RetValIpMRouteNextHopPkts)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    IgpNextHopIndex.u4GrpAddr = u4IpMRouteNextHopGroup;
    IgpNextHopIndex.u4SrcAddr = u4IpMRouteNextHopSource;
    IgpNextHopIndex.u4SrcMask = u4IpMRouteNextHopSourceMask;
    IgpNextHopIndex.i4OIfIndex = i4IpMRouteNextHopIfIndex;
    IgpNextHopIndex.u4NextHopAddr = u4IpMRouteNextHopAddress;

    if (IgmpProxyGetNextHopEntryObjectByName (&IgpNextHopIndex, IGP_FWD_PACKETS,
                                              &u4ObjValue) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteNextHopPkts = u4ObjValue;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IpMRouteInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpMRouteInterfaceTable (INT4 i4IpMRouteInterfaceIfIndex)
{
#ifdef PIM_WANTED
    if (nmhValidateIndexInstanceFsPimCmnInterfaceTable
        (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpInterfaceTable (i4IpMRouteInterfaceIfIndex)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyValidateInterfaceTblIndex (i4IpMRouteInterfaceIfIndex)
        == IGP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpMRouteInterfaceTable (INT4 *pi4IpMRouteInterfaceIfIndex)
{

    INT1                i1Status = MRI_FALSE;
    INT4                i4RetIfIndex = 0;
#ifdef PIM_WANTED
    INT4                i4Ifindex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;

    if (nmhGetFirstIndexFsPimCmnInterfaceTable (&i4Ifindex, &i4AddrType)
        == SNMP_SUCCESS)
    {
        if (i4AddrType != IPVX_ADDR_FMLY_IPV4)
        {
            while (nmhGetNextIndexFsPimCmnInterfaceTable (i4Ifindex,
                                                          &i4NextIfIndex,
                                                          i4AddrType,
                                                          &i4NextAddrType)
                   == SNMP_SUCCESS)
            {
                if (i4NextAddrType != IPVX_ADDR_FMLY_IPV4)
                {
                    i4Ifindex = i4NextIfIndex;
                    i4AddrType = i4NextAddrType;
                    continue;
                }
                *pi4IpMRouteInterfaceIfIndex = i4NextIfIndex;
                i1Status = MRI_TRUE;
                break;
            }
        }
        else
        {
            *pi4IpMRouteInterfaceIfIndex = i4Ifindex;
            i1Status = MRI_TRUE;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetFirstIndexDvmrpInterfaceTable (&i4RetIfIndex) == SNMP_SUCCESS)
    {
        if (i1Status == MRI_TRUE)
        {
            *pi4IpMRouteInterfaceIfIndex =
                (i4RetIfIndex < *pi4IpMRouteInterfaceIfIndex) ?
                i4RetIfIndex : *pi4IpMRouteInterfaceIfIndex;
        }
        else
        {
            *pi4IpMRouteInterfaceIfIndex = i4RetIfIndex;
            i1Status = MRI_TRUE;
        }
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetNextInterfaceIndex (0, &i4RetIfIndex) == IGP_SUCCESS)
    {
        if (i1Status == MRI_TRUE)
        {
            *pi4IpMRouteInterfaceIfIndex =
                (i4RetIfIndex < *pi4IpMRouteInterfaceIfIndex) ?
                i4RetIfIndex : *pi4IpMRouteInterfaceIfIndex;
        }
        else
        {
            *pi4IpMRouteInterfaceIfIndex = i4RetIfIndex;
            i1Status = MRI_TRUE;
        }
    }
#endif

    if (i1Status == MRI_TRUE)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
                nextIpMRouteInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpMRouteInterfaceTable (INT4 i4IpMRouteInterfaceIfIndex,
                                       INT4 *pi4NextIpMRouteInterfaceIfIndex)
{
    INT4                i4RetIfIndex = 0;
    INT1                i1Status = MRI_FALSE;
#ifdef PIM_WANTED
    INT4                i4Ifindex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4NextAddrType = 0;

    i4Ifindex = i4IpMRouteInterfaceIfIndex;

    while (nmhGetNextIndexFsPimCmnInterfaceTable (i4Ifindex, &i4NextIfIndex,
                                                  i4AddrType, &i4NextAddrType)
           == SNMP_SUCCESS)
    {
        if (i4NextAddrType != IPVX_ADDR_FMLY_IPV4)
        {
            i4Ifindex = i4NextIfIndex;
            i4AddrType = i4NextAddrType;
            continue;
        }
        else
        {
            *pi4NextIpMRouteInterfaceIfIndex = i4NextIfIndex;
            i1Status = MRI_TRUE;
            break;
        }
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetNextIndexDvmrpInterfaceTable (i4IpMRouteInterfaceIfIndex,
                                            &i4RetIfIndex) == SNMP_SUCCESS)
    {
        if (i1Status == MRI_TRUE)
        {
            *pi4NextIpMRouteInterfaceIfIndex =
                (i4RetIfIndex < *pi4NextIpMRouteInterfaceIfIndex) ?
                i4RetIfIndex : *pi4NextIpMRouteInterfaceIfIndex;

        }
        else
        {
            *pi4NextIpMRouteInterfaceIfIndex = i4RetIfIndex;
            i1Status = MRI_TRUE;
        }
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetNextInterfaceIndex (i4IpMRouteInterfaceIfIndex,
                                        &i4RetIfIndex) == IGP_SUCCESS)
    {
        if (i1Status == MRI_TRUE)
        {
            *pi4NextIpMRouteInterfaceIfIndex =
                (i4RetIfIndex < *pi4NextIpMRouteInterfaceIfIndex) ?
                i4RetIfIndex : *pi4NextIpMRouteInterfaceIfIndex;

        }
        else
        {
            *pi4NextIpMRouteInterfaceIfIndex = i4RetIfIndex;
            i1Status = MRI_TRUE;
        }
    }
#endif

    if (i1Status == MRI_TRUE)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceTtl
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceTtl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceTtl (INT4 i4IpMRouteInterfaceIfIndex,
                            INT4 *pi4RetValIpMRouteInterfaceTtl)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectVal;
#endif

#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceTtl (i4IpMRouteInterfaceIfIndex,
                                    IPVX_ADDR_FMLY_IPV4,
                                    pi4RetValIpMRouteInterfaceTtl)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceTtl (i4IpMRouteInterfaceIfIndex,
                                 pi4RetValIpMRouteInterfaceTtl) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_TTL, &u8ObjectVal)
        == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteInterfaceTtl = (INT4) u8ObjectVal.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceProtocol
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceProtocol (INT4 i4IpMRouteInterfaceIfIndex,
                                 INT4 *pi4RetValIpMRouteInterfaceProtocol)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectVal;
#endif

#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceProtocol (i4IpMRouteInterfaceIfIndex,
                                         IPVX_ADDR_FMLY_IPV4,
                                         pi4RetValIpMRouteInterfaceProtocol)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceProtocol (i4IpMRouteInterfaceIfIndex,
                                      pi4RetValIpMRouteInterfaceProtocol)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_PROTO, &u8ObjectVal)
        == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteInterfaceProtocol = (INT4) u8ObjectVal.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceRateLimit
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceRateLimit (INT4 i4IpMRouteInterfaceIfIndex,
                                  INT4 *pi4RetValIpMRouteInterfaceRateLimit)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectVal;
#endif

#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceRateLimit (i4IpMRouteInterfaceIfIndex,
                                          IPVX_ADDR_FMLY_IPV4,
                                          pi4RetValIpMRouteInterfaceRateLimit)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceRateLimit (i4IpMRouteInterfaceIfIndex,
                                       pi4RetValIpMRouteInterfaceRateLimit)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_RLIMIT, &u8ObjectVal)
        == IGP_SUCCESS)
    {
        *pi4RetValIpMRouteInterfaceRateLimit = (INT4) u8ObjectVal.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceInMcastOctets
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceInMcastOctets (INT4 i4IpMRouteInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValIpMRouteInterfaceInMcastOctets)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectVal;
#endif

#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceInMcastOctets (i4IpMRouteInterfaceIfIndex,
                                              IPVX_ADDR_FMLY_IPV4,
                                              pu4RetValIpMRouteInterfaceInMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceInMcastOctets (i4IpMRouteInterfaceIfIndex,
                                           pu4RetValIpMRouteInterfaceInMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_IN_MOCTETS,
                                               &u8ObjectVal) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteInterfaceInMcastOctets = u8ObjectVal.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceOutMcastOctets
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceOutMcastOctets (INT4 i4IpMRouteInterfaceIfIndex,
                                       UINT4
                                       *pu4RetValIpMRouteInterfaceOutMcastOctets)
{
#ifdef IGMPPRXY_WANTED
    tSNMP_COUNTER64_TYPE u8ObjectVal;
#endif

#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceOutMcastOctets (i4IpMRouteInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               pu4RetValIpMRouteInterfaceOutMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceOutMcastOctets (i4IpMRouteInterfaceIfIndex,
                                            pu4RetValIpMRouteInterfaceOutMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_OUT_MOCTETS,
                                               &u8ObjectVal) == IGP_SUCCESS)
    {
        *pu4RetValIpMRouteInterfaceOutMcastOctets = u8ObjectVal.lsn;
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceHCInMcastOctets
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceHCInMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceHCInMcastOctets (INT4 i4IpMRouteInterfaceIfIndex,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValIpMRouteInterfaceHCInMcastOctets)
{
#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceHCInMcastOctets (i4IpMRouteInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                pu8RetValIpMRouteInterfaceHCInMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceHCInMcastOctets (i4IpMRouteInterfaceIfIndex,
                                             pu8RetValIpMRouteInterfaceHCInMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_HC_IN_MOCTETS,
                                               pu8RetValIpMRouteInterfaceHCInMcastOctets)
        == IGP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteInterfaceHCOutMcastOctets
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                retValIpMRouteInterfaceHCOutMcastOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteInterfaceHCOutMcastOctets (INT4 i4IpMRouteInterfaceIfIndex,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValIpMRouteInterfaceHCOutMcastOctets)
{
#ifdef PIM_WANTED
    if (nmhGetFsPimCmnInterfaceHCOutMcastOctets (i4IpMRouteInterfaceIfIndex,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 pu8RetValIpMRouteInterfaceHCOutMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhGetDvmrpInterfaceHCOutMcastOctets (i4IpMRouteInterfaceIfIndex,
                                              pu8RetValIpMRouteInterfaceHCOutMcastOctets)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyGetInterfaceEntryObjetByName (i4IpMRouteInterfaceIfIndex,
                                               IGP_IFACE_HC_OUT_MOCTETS,
                                               pu8RetValIpMRouteInterfaceHCOutMcastOctets)
        == IGP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpMRouteInterfaceTtl
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                setValIpMRouteInterfaceTtl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteInterfaceTtl (INT4 i4IpMRouteInterfaceIfIndex,
                            INT4 i4SetValIpMRouteInterfaceTtl)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SnmpNotifyInfo.pu4ObjectId = IpMRouteInterfaceTtl;
    SnmpNotifyInfo.u4OidLen = sizeof (IpMRouteInterfaceTtl) / sizeof (UINT4);
    RM_GET_SEQ_NUM (&SnmpNotifyInfo.u4SeqNum);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

#ifdef PIM_WANTED
    if (nmhValidateIndexInstanceFsPimCmnInterfaceTable
        (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceTtl (i4IpMRouteInterfaceIfIndex,
                                        IPVX_ADDR_FMLY_IPV4,
                                        i4SetValIpMRouteInterfaceTtl)
            == SNMP_SUCCESS)
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4IpMRouteInterfaceIfIndex,
                              i4SetValIpMRouteInterfaceTtl));
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpInterfaceTable
        (i4IpMRouteInterfaceIfIndex) == SNMP_SUCCESS)
    {
        if (nmhSetDvmrpInterfaceTtl (i4IpMRouteInterfaceIfIndex,
                                     i4SetValIpMRouteInterfaceTtl)
            == SNMP_SUCCESS)
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4IpMRouteInterfaceIfIndex,
                              i4SetValIpMRouteInterfaceTtl));
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyValidateInterfaceTblIndex (i4IpMRouteInterfaceIfIndex)
        == IGP_SUCCESS)
    {
        if (IgmpProxySetInterfaceEntryObjectByName (i4IpMRouteInterfaceIfIndex,
                                                    IGP_IFACE_TTL,
                                                    i4SetValIpMRouteInterfaceTtl)
            == IGP_SUCCESS)
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4IpMRouteInterfaceIfIndex,
                              i4SetValIpMRouteInterfaceTtl));
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpMRouteInterfaceRateLimit
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                setValIpMRouteInterfaceRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteInterfaceRateLimit (INT4 i4IpMRouteInterfaceIfIndex,
                                  INT4 i4SetValIpMRouteInterfaceRateLimit)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SnmpNotifyInfo.pu4ObjectId = IpMRouteInterfaceRateLimit;
    SnmpNotifyInfo.u4OidLen = sizeof (IpMRouteInterfaceRateLimit) /
        sizeof (UINT4);
    RM_GET_SEQ_NUM (&SnmpNotifyInfo.u4SeqNum);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

#ifdef PIM_WANTED
    if (nmhValidateIndexInstanceFsPimCmnInterfaceTable
        (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4) == SNMP_SUCCESS)
    {
        if (nmhSetFsPimCmnInterfaceRateLimit (i4IpMRouteInterfaceIfIndex,
                                              IPVX_ADDR_FMLY_IPV4,
                                              i4SetValIpMRouteInterfaceRateLimit)
            == SNMP_SUCCESS)
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4IpMRouteInterfaceIfIndex,
                              i4SetValIpMRouteInterfaceRateLimit));
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpInterfaceTable
        (i4IpMRouteInterfaceIfIndex) == SNMP_SUCCESS)
    {
        if (nmhSetDvmrpInterfaceRateLimit (i4IpMRouteInterfaceIfIndex,
                                           i4SetValIpMRouteInterfaceRateLimit)
            == SNMP_SUCCESS)
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4IpMRouteInterfaceIfIndex,
                              i4SetValIpMRouteInterfaceRateLimit));
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyValidateInterfaceTblIndex (i4IpMRouteInterfaceIfIndex)
        == IGP_SUCCESS)
    {
        if (IgmpProxySetInterfaceEntryObjectByName (i4IpMRouteInterfaceIfIndex,
                                                    IGP_IFACE_RLIMIT,
                                                    i4SetValIpMRouteInterfaceRateLimit)
            == IGP_SUCCESS)
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4IpMRouteInterfaceIfIndex,
                              i4SetValIpMRouteInterfaceRateLimit));
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#endif
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IpMRouteInterfaceTtl
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                testValIpMRouteInterfaceTtl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteInterfaceTtl (UINT4 *pu4ErrorCode,
                               INT4 i4IpMRouteInterfaceIfIndex,
                               INT4 i4TestValIpMRouteInterfaceTtl)
{
#ifdef PIM_WANTED
    if (nmhValidateIndexInstanceFsPimCmnInterfaceTable
        (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4) == SNMP_SUCCESS)
    {
        if (nmhTestv2FsPimCmnInterfaceTtl (pu4ErrorCode,
                                           i4IpMRouteInterfaceIfIndex,
                                           IPVX_ADDR_FMLY_IPV4,
                                           i4TestValIpMRouteInterfaceTtl)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        if (*pu4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_MRI_INVALID_TTL);
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpInterfaceTable
        (i4IpMRouteInterfaceIfIndex) == SNMP_SUCCESS)
    {
        if (nmhTestv2DvmrpInterfaceTtl (pu4ErrorCode,
                                        i4IpMRouteInterfaceIfIndex,
                                        i4TestValIpMRouteInterfaceTtl)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        if (*pu4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_MRI_INVALID_TTL);
        }

        return SNMP_FAILURE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyValidateInterfaceTblIndex (i4IpMRouteInterfaceIfIndex)
        == IGP_SUCCESS)
    {
        if (IgmpProxyTestInterfaceEntryObjectValueByName (pu4ErrorCode,
                                                          i4IpMRouteInterfaceIfIndex,
                                                          IGP_IFACE_TTL,
                                                          i4TestValIpMRouteInterfaceTtl)
            == IGP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        if (*pu4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_MRI_INVALID_TTL);
        }

        return SNMP_FAILURE;
    }
#endif

    CLI_SET_ERR (CLI_MRI_NO_MRP_ON_INTERFACE);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IpMRouteInterfaceRateLimit
 Input       :  The Indices
                IpMRouteInterfaceIfIndex

                The Object 
                testValIpMRouteInterfaceRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteInterfaceRateLimit (UINT4 *pu4ErrorCode,
                                     INT4 i4IpMRouteInterfaceIfIndex,
                                     INT4 i4TestValIpMRouteInterfaceRateLimit)
{
#ifdef PIM_WANTED
    if (nmhValidateIndexInstanceFsPimCmnInterfaceTable
        (i4IpMRouteInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4) == SNMP_SUCCESS)
    {
        if (nmhTestv2FsPimCmnInterfaceRateLimit (pu4ErrorCode,
                                                 i4IpMRouteInterfaceIfIndex,
                                                 IPVX_ADDR_FMLY_IPV4,
                                                 i4TestValIpMRouteInterfaceRateLimit)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        if (*pu4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_MRI_INVALID_RATELIMIT);
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef DVMRP_WANTED
    if (nmhValidateIndexInstanceDvmrpInterfaceTable
        (i4IpMRouteInterfaceIfIndex) == SNMP_SUCCESS)
    {
        if (nmhTestv2DvmrpInterfaceRateLimit (pu4ErrorCode,
                                              i4IpMRouteInterfaceIfIndex,
                                              i4TestValIpMRouteInterfaceRateLimit)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        if (*pu4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_MRI_INVALID_RATELIMIT);
        }
        return SNMP_FAILURE;
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (IgmpProxyValidateInterfaceTblIndex (i4IpMRouteInterfaceIfIndex)
        == IGP_SUCCESS)
    {
        if (IgmpProxyTestInterfaceEntryObjectValueByName (pu4ErrorCode,
                                                          i4IpMRouteInterfaceIfIndex,
                                                          IGP_IFACE_RLIMIT,
                                                          i4TestValIpMRouteInterfaceRateLimit)
            == IGP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        if (*pu4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_MRI_INVALID_RATELIMIT);
        }
        return SNMP_FAILURE;
    }
#endif

    CLI_SET_ERR (CLI_MRI_NO_MRP_ON_INTERFACE);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpMRouteInterfaceTable
 Input       :  The Indices
                IpMRouteInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpMRouteInterfaceTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpMRouteBoundaryTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteBoundaryTable
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIpMRouteBoundaryTable (INT4 i4IpMRouteBoundaryIfIndex,
                                               UINT4 u4IpMRouteBoundaryAddress,
                                               UINT4
                                               u4IpMRouteBoundaryAddressMask)
{
    UNUSED_PARAM (i4IpMRouteBoundaryIfIndex);
    UNUSED_PARAM (u4IpMRouteBoundaryAddress);
    UNUSED_PARAM (u4IpMRouteBoundaryAddressMask);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteBoundaryTable
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpMRouteBoundaryTable (INT4 *pi4IpMRouteBoundaryIfIndex,
                                       UINT4 *pu4IpMRouteBoundaryAddress,
                                       UINT4 *pu4IpMRouteBoundaryAddressMask)
{
    UNUSED_PARAM (pi4IpMRouteBoundaryIfIndex);
    UNUSED_PARAM (pu4IpMRouteBoundaryAddress);
    UNUSED_PARAM (pu4IpMRouteBoundaryAddressMask);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteBoundaryTable
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                nextIpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                nextIpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask
                nextIpMRouteBoundaryAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpMRouteBoundaryTable (INT4 i4IpMRouteBoundaryIfIndex,
                                      INT4 *pi4NextIpMRouteBoundaryIfIndex,
                                      UINT4 u4IpMRouteBoundaryAddress,
                                      UINT4 *pu4NextIpMRouteBoundaryAddress,
                                      UINT4 u4IpMRouteBoundaryAddressMask,
                                      UINT4 *pu4NextIpMRouteBoundaryAddressMask)
{
    UNUSED_PARAM (i4IpMRouteBoundaryIfIndex);
    UNUSED_PARAM (u4IpMRouteBoundaryAddress);
    UNUSED_PARAM (u4IpMRouteBoundaryAddressMask);
    UNUSED_PARAM (pi4NextIpMRouteBoundaryIfIndex);
    UNUSED_PARAM (pu4NextIpMRouteBoundaryAddress);
    UNUSED_PARAM (pu4NextIpMRouteBoundaryAddressMask);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteBoundaryStatus
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask

                The Object 
                retValIpMRouteBoundaryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteBoundaryStatus (INT4 i4IpMRouteBoundaryIfIndex,
                              UINT4 u4IpMRouteBoundaryAddress,
                              UINT4 u4IpMRouteBoundaryAddressMask,
                              INT4 *pi4RetValIpMRouteBoundaryStatus)
{
    UNUSED_PARAM (i4IpMRouteBoundaryIfIndex);
    UNUSED_PARAM (u4IpMRouteBoundaryAddress);
    UNUSED_PARAM (u4IpMRouteBoundaryAddressMask);
    UNUSED_PARAM (pi4RetValIpMRouteBoundaryStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpMRouteBoundaryStatus
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask

                The Object 
                setValIpMRouteBoundaryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteBoundaryStatus (INT4 i4IpMRouteBoundaryIfIndex,
                              UINT4 u4IpMRouteBoundaryAddress,
                              UINT4 u4IpMRouteBoundaryAddressMask,
                              INT4 i4SetValIpMRouteBoundaryStatus)
{
    UNUSED_PARAM (i4IpMRouteBoundaryIfIndex);
    UNUSED_PARAM (u4IpMRouteBoundaryAddress);
    UNUSED_PARAM (u4IpMRouteBoundaryAddressMask);
    UNUSED_PARAM (i4SetValIpMRouteBoundaryStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IpMRouteBoundaryStatus
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask

                The Object 
                testValIpMRouteBoundaryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteBoundaryStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4IpMRouteBoundaryIfIndex,
                                 UINT4 u4IpMRouteBoundaryAddress,
                                 UINT4 u4IpMRouteBoundaryAddressMask,
                                 INT4 i4TestValIpMRouteBoundaryStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IpMRouteBoundaryIfIndex);
    UNUSED_PARAM (u4IpMRouteBoundaryAddress);
    UNUSED_PARAM (u4IpMRouteBoundaryAddressMask);
    UNUSED_PARAM (i4TestValIpMRouteBoundaryStatus);

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpMRouteBoundaryTable
 Input       :  The Indices
                IpMRouteBoundaryIfIndex
                IpMRouteBoundaryAddress
                IpMRouteBoundaryAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpMRouteBoundaryTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpMRouteScopeNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpMRouteScopeNameTable
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceIpMRouteScopeNameTable
    (UINT4 u4IpMRouteScopeNameAddress,
     UINT4 u4IpMRouteScopeNameAddressMask,
     tSNMP_OCTET_STRING_TYPE * pIpMRouteScopeNameLanguage)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpMRouteScopeNameTable
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIpMRouteScopeNameTable (UINT4 *pu4IpMRouteScopeNameAddress,
                                        UINT4 *pu4IpMRouteScopeNameAddressMask,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIpMRouteScopeNameLanguage)
{
    UNUSED_PARAM (pu4IpMRouteScopeNameAddress);
    UNUSED_PARAM (pu4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpMRouteScopeNameTable
 Input       :  The Indices
                IpMRouteScopeNameAddress
                nextIpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                nextIpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage
                nextIpMRouteScopeNameLanguage
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpMRouteScopeNameTable (UINT4 u4IpMRouteScopeNameAddress,
                                       UINT4 *pu4NextIpMRouteScopeNameAddress,
                                       UINT4 u4IpMRouteScopeNameAddressMask,
                                       UINT4
                                       *pu4NextIpMRouteScopeNameAddressMask,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIpMRouteScopeNameLanguage,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextIpMRouteScopeNameLanguage)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (pu4NextIpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pu4NextIpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (pNextIpMRouteScopeNameLanguage);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIpMRouteScopeNameString
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                retValIpMRouteScopeNameString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteScopeNameString (UINT4 u4IpMRouteScopeNameAddress,
                               UINT4 u4IpMRouteScopeNameAddressMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpMRouteScopeNameLanguage,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIpMRouteScopeNameString)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (pRetValIpMRouteScopeNameString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteScopeNameDefault
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                retValIpMRouteScopeNameDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteScopeNameDefault (UINT4 u4IpMRouteScopeNameAddress,
                                UINT4 u4IpMRouteScopeNameAddressMask,
                                tSNMP_OCTET_STRING_TYPE *
                                pIpMRouteScopeNameLanguage,
                                INT4 *pi4RetValIpMRouteScopeNameDefault)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (pi4RetValIpMRouteScopeNameDefault);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpMRouteScopeNameStatus
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                retValIpMRouteScopeNameStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpMRouteScopeNameStatus (UINT4 u4IpMRouteScopeNameAddress,
                               UINT4 u4IpMRouteScopeNameAddressMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpMRouteScopeNameLanguage,
                               INT4 *pi4RetValIpMRouteScopeNameStatus)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (pi4RetValIpMRouteScopeNameStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIpMRouteScopeNameString
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                setValIpMRouteScopeNameString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteScopeNameString (UINT4 u4IpMRouteScopeNameAddress,
                               UINT4 u4IpMRouteScopeNameAddressMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpMRouteScopeNameLanguage,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIpMRouteScopeNameString)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (pSetValIpMRouteScopeNameString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIpMRouteScopeNameDefault
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                setValIpMRouteScopeNameDefault
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteScopeNameDefault (UINT4 u4IpMRouteScopeNameAddress,
                                UINT4 u4IpMRouteScopeNameAddressMask,
                                tSNMP_OCTET_STRING_TYPE *
                                pIpMRouteScopeNameLanguage,
                                INT4 i4SetValIpMRouteScopeNameDefault)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (i4SetValIpMRouteScopeNameDefault);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIpMRouteScopeNameStatus
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                setValIpMRouteScopeNameStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIpMRouteScopeNameStatus (UINT4 u4IpMRouteScopeNameAddress,
                               UINT4 u4IpMRouteScopeNameAddressMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pIpMRouteScopeNameLanguage,
                               INT4 i4SetValIpMRouteScopeNameStatus)
{
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (i4SetValIpMRouteScopeNameStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IpMRouteScopeNameString
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                testValIpMRouteScopeNameString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteScopeNameString (UINT4 *pu4ErrorCode,
                                  UINT4 u4IpMRouteScopeNameAddress,
                                  UINT4 u4IpMRouteScopeNameAddressMask,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pIpMRouteScopeNameLanguage,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIpMRouteScopeNameString)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (pTestValIpMRouteScopeNameString);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpMRouteScopeNameDefault
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                testValIpMRouteScopeNameDefault
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteScopeNameDefault (UINT4 *pu4ErrorCode,
                                   UINT4 u4IpMRouteScopeNameAddress,
                                   UINT4 u4IpMRouteScopeNameAddressMask,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pIpMRouteScopeNameLanguage,
                                   INT4 i4TestValIpMRouteScopeNameDefault)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (i4TestValIpMRouteScopeNameDefault);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpMRouteScopeNameStatus
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage

                The Object 
                testValIpMRouteScopeNameStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IpMRouteScopeNameStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4IpMRouteScopeNameAddress,
                                  UINT4 u4IpMRouteScopeNameAddressMask,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pIpMRouteScopeNameLanguage,
                                  INT4 i4TestValIpMRouteScopeNameStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IpMRouteScopeNameAddress);
    UNUSED_PARAM (u4IpMRouteScopeNameAddressMask);
    UNUSED_PARAM (pIpMRouteScopeNameLanguage);
    UNUSED_PARAM (i4TestValIpMRouteScopeNameStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpMRouteScopeNameTable
 Input       :  The Indices
                IpMRouteScopeNameAddress
                IpMRouteScopeNameAddressMask
                IpMRouteScopeNameLanguage
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpMRouteScopeNameTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
