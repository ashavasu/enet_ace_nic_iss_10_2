#!/bin/csh
# Copyright (C) 2009 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent                                       |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : Any                                           |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#


PROJECT_NAME            = FutureMRI
PROJECT_BASE_DIR        = ${BASE_DIR}/mri
PROJECT_SOURCE_DIR      = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR     = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR      = ${PROJECT_BASE_DIR}/obj
FUTURE_INC_DIR          = $(BASE_DIR)/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES   = $(PROJECT_INCLUDE_DIR)/stdmridb.h   \
                          $(PROJECT_INCLUDE_DIR)/stdmrilw.h   \
                          $(PROJECT_INCLUDE_DIR)/stdmriwr.h   \
                          $(PROJECT_INCLUDE_DIR)/mritdfs.h    \
                          $(PROJECT_INCLUDE_DIR)/mriglob.h    \
                          $(PROJECT_INCLUDE_DIR)/mrimacro.h   \
                          $(PROJECT_INCLUDE_DIR)/mriinc.h     \
                          $(PROJECT_INCLUDE_DIR)/mriproto.h  
 


PROJECT_FINAL_INCLUDES_DIRS     =  -I$(PROJECT_INCLUDE_DIR)   \
                                     $(COMMON_INCLUDE_DIRS)   \
                                   -I$(FUTURE_INC_DIR)

PROJECT_FINAL_INCLUDE_FILES    +=  $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES    =    $(COMMON_DEPENDENCIES)           \
                             $(PROJECT_FINAL_INCLUDE_FILES)   \
                             $(PROJECT_BASE_DIR)/Makefile     \
                             $(PROJECT_BASE_DIR)/make.h
