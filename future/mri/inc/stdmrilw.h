/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmrilw.h,v 1.1.1.1 2009/02/11 06:20:19 prabuc-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */


INT1  nmhGetIpMRouteEnable ARG_LIST((INT4 *));

INT1  nmhGetIpMRouteEntryCount ARG_LIST((UINT4 *));

INT1  nmhSetIpMRouteEnable ARG_LIST((INT4 ));

INT1  nmhTestv2IpMRouteEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1  nmhDepv2IpMRouteEnable ARG_LIST((UINT4 *, tSnmpIndexList*,
                                       tSNMP_VAR_BIND*));

INT1  nmhValidateIndexInstanceIpMRouteTable ARG_LIST((UINT4  , UINT4  ,
                                                      UINT4 ));

INT1  nmhGetFirstIndexIpMRouteTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

INT1  nmhGetNextIndexIpMRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 ,
                                             UINT4 * , UINT4 , UINT4 *));

INT1  nmhGetIpMRouteUpstreamNeighbor ARG_LIST((UINT4  , UINT4  ,
                                               UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteInIfIndex ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1  nmhGetIpMRouteUpTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteExpiryTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRoutePkts ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteDifferentInIfPackets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteOctets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteProtocol ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1  nmhGetIpMRouteRtProto ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1  nmhGetIpMRouteRtAddress ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteRtMask ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1  nmhGetIpMRouteRtType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1  nmhGetIpMRouteHCOctets ARG_LIST((UINT4  , UINT4  , UINT4 ,
                                       tSNMP_COUNTER64_TYPE *));

INT1  nmhValidateIndexInstanceIpMRouteNextHopTable 
                    ARG_LIST((UINT4  , UINT4 , UINT4  , INT4  , UINT4 ));

INT1  nmhGetFirstIndexIpMRouteNextHopTable
                    ARG_LIST((UINT4 * , UINT4 * ,UINT4 * , INT4 * , UINT4 *));

INT1  nmhGetNextIndexIpMRouteNextHopTable
                    ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 ,
                    UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

INT1  nmhGetIpMRouteNextHopState 
                    ARG_LIST((UINT4  , UINT4  , UINT4  , INT4, UINT4 ,
                    INT4 *));

INT1  nmhGetIpMRouteNextHopUpTime 
                    ARG_LIST((UINT4  , UINT4  , UINT4  , INT4 , UINT4 ,
                    UINT4 *));

INT1  nmhGetIpMRouteNextHopExpiryTime 
                    ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,
                    UINT4 *));

INT1  nmhGetIpMRouteNextHopClosestMemberHops 
                    ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,
                    INT4 *));

INT1  nmhGetIpMRouteNextHopProtocol 
                    ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,
                    INT4 *));

INT1  nmhGetIpMRouteNextHopPkts 
                    ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,
                    UINT4 *));

INT1  nmhValidateIndexInstanceIpMRouteInterfaceTable ARG_LIST((INT4 ));

INT1  nmhGetFirstIndexIpMRouteInterfaceTable ARG_LIST((INT4 *));

INT1  nmhGetNextIndexIpMRouteInterfaceTable ARG_LIST((INT4 , INT4 *));

INT1  nmhGetIpMRouteInterfaceTtl ARG_LIST((INT4 ,INT4 *));

INT1  nmhGetIpMRouteInterfaceProtocol ARG_LIST((INT4 ,INT4 *));

INT1  nmhGetIpMRouteInterfaceRateLimit ARG_LIST((INT4 ,INT4 *));

INT1  nmhGetIpMRouteInterfaceInMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1  nmhGetIpMRouteInterfaceOutMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1  nmhGetIpMRouteInterfaceHCInMcastOctets 
                               ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1  nmhGetIpMRouteInterfaceHCOutMcastOctets 
                               ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1  nmhSetIpMRouteInterfaceTtl ARG_LIST((INT4  ,INT4 ));

INT1  nmhSetIpMRouteInterfaceRateLimit ARG_LIST((INT4  ,INT4 ));

INT1  nmhTestv2IpMRouteInterfaceTtl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1  nmhTestv2IpMRouteInterfaceRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1  nmhDepv2IpMRouteInterfaceTable
                   ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1  nmhValidateIndexInstanceIpMRouteBoundaryTable 
                   ARG_LIST((INT4  , UINT4  , UINT4 ));

INT1  nmhGetFirstIndexIpMRouteBoundaryTable
                   ARG_LIST((INT4 * , UINT4 * , UINT4 *));

INT1  nmhGetNextIndexIpMRouteBoundaryTable
                   ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 ,
                   UINT4 *));

INT1  nmhGetIpMRouteBoundaryStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1  nmhSetIpMRouteBoundaryStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1  nmhTestv2IpMRouteBoundaryStatus 
                   ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1  nmhDepv2IpMRouteBoundaryTable 
                   ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1  nmhValidateIndexInstanceIpMRouteScopeNameTable 
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *));

INT1  nmhGetFirstIndexIpMRouteScopeNameTable
                   ARG_LIST((UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

INT1  nmhGetNextIndexIpMRouteScopeNameTable 
                   ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * ,
                   tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

INT1  nmhGetIpMRouteScopeNameString 
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,
                   tSNMP_OCTET_STRING_TYPE * ));

INT1  nmhGetIpMRouteScopeNameDefault
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,
                   INT4 *));

INT1  nmhGetIpMRouteScopeNameStatus 
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,
                   INT4 *));

INT1  nmhSetIpMRouteScopeNameString 
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * ,
                   tSNMP_OCTET_STRING_TYPE *));

INT1  nmhSetIpMRouteScopeNameDefault
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * ,
                   INT4 ));


INT1  nmhSetIpMRouteScopeNameStatus 
                   ARG_LIST((UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * ,
                   INT4 ));

INT1  nmhTestv2IpMRouteScopeNameString
                   ARG_LIST((UINT4 *  ,UINT4  , UINT4 , 
                   tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1  nmhTestv2IpMRouteScopeNameDefault
                   ARG_LIST((UINT4 *  ,UINT4  , UINT4  , 
                   tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1  nmhTestv2IpMRouteScopeNameStatus 
                   ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,
                   tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1  nmhDepv2IpMRouteScopeNameTable 
                   ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
