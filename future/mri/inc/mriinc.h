/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mriinc.h                       */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  --                             */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  FUNCTIONS DEFINED(Applicable for Source files) :        */
/*  DESCRIPTION           :                                 */
/************************************************************/
/*                                                          */
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#ifndef __MRI_INC_H__
#define __MRI_INC_H__

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "fssnmp.h" 
#include "utilipvx.h"
#include "pim.h"
#include "igmp.h"
 
#include "mri.h"
#include "mricli.h"
#include "mritdfs.h"
#include "mriproto.h"
#include "mrimacro.h"
#include "mriglob.h"
#include "stdmrilw.h"
#include "stdmriwr.h"

#include "npapi.h"
#include "ipmcnp.h"

#endif
