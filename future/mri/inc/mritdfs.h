/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mridefn.h                      */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  --                             */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This function contains all the */
/*                           structures used byt the module */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#ifndef __MRI_DEFN_H__
#define __MRI_DEFN_H__

typedef enum
{
   MRI_MROUTE_ENABLE = 1,     /* 1 */
   MRI_MROUTE_DISABLE         /* 2 */

} eMriMRouteStatus;

typedef struct MriGlobalInfo 
{
   tOsixSemId        MriSemId;
   eMriMRouteStatus  eMRouteStatus;

}tMriGlobalInfo;

/* This structure contains the ipMRouteTable Index */
typedef struct MRouteTblIndex
{
   UINT4 u4GrpAddr;    /* Multicast Route table group address */
   UINT4 u4SrcAddr;    /* Multicast Route table source address */
   UINT4 u4SrcMask;    /* Multicast Route table source mask */

}tMRouteTblIndex;   


/* This structure contains the ipMRouteNextHopTable Index */
typedef struct MNextHopTblIndex 
{
   UINT4 u4GrpAddr;    /* Multicast NextHop table group address */
   UINT4 u4SrcAddr;    /* Multicast NextHop table source address */
   UINT4 u4SrcMask;    /* Multicast NextHop table source mask */
   INT4  i4OIfIndex;   /* Multicast NextHop table outgoing interface */
   UINT4 u4NextHop;    /* Multicast NextHop table nextHop address */

}tMNextHopTblIndex;

#endif
