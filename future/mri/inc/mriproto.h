
/*$Id: mriproto.h,v 1.2 2011/10/28 10:45:09 siva Exp $*/
/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mriproto.h                     */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  Any                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This function contains the 
                             protoypes types of the function
                             defined/used by MRI module     */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#ifndef __MRI_PROTO_H__
#define __MRI_PROTO_H__

INT4
MriUtilCompareMRouteTblIndex(tMRouteTblIndex *, tMRouteTblIndex *);

INT4
MriUtilCompareMNextHopTblIndex (tMNextHopTblIndex *, tMNextHopTblIndex *);
 
VOID
MriUtilGetLowMRouteIndex (tMRouteTblIndex *, tMRouteTblIndex *, INT1);

VOID
MriUtilGetLowMNextHopIndex (tMNextHopTblIndex *, tMNextHopTblIndex *, INT1);

#ifdef PIM_WANTED
/* Interface Table */
extern INT1
nmhValidateIndexInstanceFsPimCmnInterfaceTable ARG_LIST((INT4  , INT4 ));

extern INT1
nmhGetFirstIndexFsPimCmnInterfaceTable ARG_LIST((INT4 * , INT4 *));

extern INT1
nmhGetNextIndexFsPimCmnInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

extern INT1
nmhGetFsPimCmnInterfaceTtl ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnInterfaceProtocol ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnInterfaceRateLimit ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnInterfaceInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnInterfaceOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnInterfaceHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsPimCmnInterfaceHCOutMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhTestv2FsPimCmnInterfaceTtl ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsPimCmnInterfaceRateLimit ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsPimCmnInterfaceTtl ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsPimCmnInterfaceRateLimit ARG_LIST((INT4  , INT4  ,INT4 ));

/* MRoute Table */

extern INT1
nmhValidateIndexInstanceFsPimCmnIpMRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

extern INT1
nmhGetFirstIndexFsPimCmnIpMRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

extern INT1
nmhGetNextIndexFsPimCmnIpMRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteUpstreamNeighbor ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsPimCmnIpMRouteInIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRoutePkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteDifferentInIfPackets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteOctets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteRtProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteRtAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsPimCmnIpMRouteRtMasklen ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteRtType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteHCOctets ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

/* MNextHop Table */

extern INT1
nmhValidateIndexInstanceFsPimCmnIpMRouteNextHopTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhGetFirstIndexFsPimCmnIpMRouteNextHopTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetNextIndexFsPimCmnIpMRouteNextHopTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsPimCmnIpMRouteNextHopState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteNextHopUpTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteNextHopExpiryTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteNextHopProtocol ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsPimCmnIpMRouteNextHopPkts ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));
#endif

#ifdef DVMRP_WANTED
/* DVMRP MRoute Table */
extern INT1
nmhValidateIndexInstanceDvmrpIpMRTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

extern INT1
nmhGetFirstIndexDvmrpIpMRTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

extern INT1
nmhGetNextIndexDvmrpIpMRTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

extern INT1
nmhGetDvmrpIpMRUpstreamNeighbor ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRInIfIndex ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDvmrpIpMRUpTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRExpiryTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRPkts ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRDifferentInIfPackets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMROctets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRProtocol ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDvmrpIpMRRtProto ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDvmrpIpMRRtAddress ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRRtMask ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMRRtType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDvmrpIpMRHCOctets ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/*  Dvmrp NextHop Table. */
extern INT1
nmhValidateIndexInstanceDvmrpIpMNextHopTable ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ));

extern INT1
nmhGetFirstIndexDvmrpIpMNextHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

extern INT1
nmhGetNextIndexDvmrpIpMNextHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

extern INT1
nmhGetDvmrpIpMNextHopState ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDvmrpIpMNextHopUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMNextHopExpiryTime ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDvmrpIpMNextHopProtocol ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDvmrpIpMNextHopPkts ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Dvmrp Interface Table. */
extern INT1
nmhValidateIndexInstanceDvmrpInterfaceTable ARG_LIST((INT4 ));

extern INT1
nmhGetFirstIndexDvmrpInterfaceTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexDvmrpInterfaceTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetDvmrpInterfaceTtl ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDvmrpInterfaceProtocol ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDvmrpInterfaceRateLimit ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDvmrpInterfaceInMcastOctets ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetDvmrpInterfaceOutMcastOctets ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetDvmrpInterfaceHCInMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetDvmrpInterfaceHCOutMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhSetDvmrpInterfaceTtl ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDvmrpInterfaceRateLimit ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhTestv2DvmrpInterfaceTtl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2DvmrpInterfaceRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

#endif
#endif
