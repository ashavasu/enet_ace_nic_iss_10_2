/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmridb.h,v 1.1.1.1 2009/02/11 06:20:19 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDMRIDB_H
#define _STDMRIDB_H

UINT1 IpMRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpMRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpMRouteInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IpMRouteBoundaryTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpMRouteScopeNameTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IMP_OCTET_PRIM};

UINT4 stdmri [] ={1,3,6,1,2,1,83};
tSNMP_OID_TYPE stdmriOID = {7, stdmri};


UINT4 IpMRouteEnable [ ] ={1,3,6,1,2,1,83,1,1,1};
UINT4 IpMRouteEntryCount [ ] ={1,3,6,1,2,1,83,1,1,7};
UINT4 IpMRouteGroup [ ] ={1,3,6,1,2,1,83,1,1,2,1,1};
UINT4 IpMRouteSource [ ] ={1,3,6,1,2,1,83,1,1,2,1,2};
UINT4 IpMRouteSourceMask [ ] ={1,3,6,1,2,1,83,1,1,2,1,3};
UINT4 IpMRouteUpstreamNeighbor [ ] ={1,3,6,1,2,1,83,1,1,2,1,4};
UINT4 IpMRouteInIfIndex [ ] ={1,3,6,1,2,1,83,1,1,2,1,5};
UINT4 IpMRouteUpTime [ ] ={1,3,6,1,2,1,83,1,1,2,1,6};
UINT4 IpMRouteExpiryTime [ ] ={1,3,6,1,2,1,83,1,1,2,1,7};
UINT4 IpMRoutePkts [ ] ={1,3,6,1,2,1,83,1,1,2,1,8};
UINT4 IpMRouteDifferentInIfPackets [ ] ={1,3,6,1,2,1,83,1,1,2,1,9};
UINT4 IpMRouteOctets [ ] ={1,3,6,1,2,1,83,1,1,2,1,10};
UINT4 IpMRouteProtocol [ ] ={1,3,6,1,2,1,83,1,1,2,1,11};
UINT4 IpMRouteRtProto [ ] ={1,3,6,1,2,1,83,1,1,2,1,12};
UINT4 IpMRouteRtAddress [ ] ={1,3,6,1,2,1,83,1,1,2,1,13};
UINT4 IpMRouteRtMask [ ] ={1,3,6,1,2,1,83,1,1,2,1,14};
UINT4 IpMRouteRtType [ ] ={1,3,6,1,2,1,83,1,1,2,1,15};
UINT4 IpMRouteHCOctets [ ] ={1,3,6,1,2,1,83,1,1,2,1,16};
UINT4 IpMRouteNextHopGroup [ ] ={1,3,6,1,2,1,83,1,1,3,1,1};
UINT4 IpMRouteNextHopSource [ ] ={1,3,6,1,2,1,83,1,1,3,1,2};
UINT4 IpMRouteNextHopSourceMask [ ] ={1,3,6,1,2,1,83,1,1,3,1,3};
UINT4 IpMRouteNextHopIfIndex [ ] ={1,3,6,1,2,1,83,1,1,3,1,4};
UINT4 IpMRouteNextHopAddress [ ] ={1,3,6,1,2,1,83,1,1,3,1,5};
UINT4 IpMRouteNextHopState [ ] ={1,3,6,1,2,1,83,1,1,3,1,6};
UINT4 IpMRouteNextHopUpTime [ ] ={1,3,6,1,2,1,83,1,1,3,1,7};
UINT4 IpMRouteNextHopExpiryTime [ ] ={1,3,6,1,2,1,83,1,1,3,1,8};
UINT4 IpMRouteNextHopClosestMemberHops [ ] ={1,3,6,1,2,1,83,1,1,3,1,9};
UINT4 IpMRouteNextHopProtocol [ ] ={1,3,6,1,2,1,83,1,1,3,1,10};
UINT4 IpMRouteNextHopPkts [ ] ={1,3,6,1,2,1,83,1,1,3,1,11};
UINT4 IpMRouteInterfaceIfIndex [ ] ={1,3,6,1,2,1,83,1,1,4,1,1};
UINT4 IpMRouteInterfaceTtl [ ] ={1,3,6,1,2,1,83,1,1,4,1,2};
UINT4 IpMRouteInterfaceProtocol [ ] ={1,3,6,1,2,1,83,1,1,4,1,3};
UINT4 IpMRouteInterfaceRateLimit [ ] ={1,3,6,1,2,1,83,1,1,4,1,4};
UINT4 IpMRouteInterfaceInMcastOctets [ ] ={1,3,6,1,2,1,83,1,1,4,1,5};
UINT4 IpMRouteInterfaceOutMcastOctets [ ] ={1,3,6,1,2,1,83,1,1,4,1,6};
UINT4 IpMRouteInterfaceHCInMcastOctets [ ] ={1,3,6,1,2,1,83,1,1,4,1,7};
UINT4 IpMRouteInterfaceHCOutMcastOctets [ ] ={1,3,6,1,2,1,83,1,1,4,1,8};
UINT4 IpMRouteBoundaryIfIndex [ ] ={1,3,6,1,2,1,83,1,1,5,1,1};
UINT4 IpMRouteBoundaryAddress [ ] ={1,3,6,1,2,1,83,1,1,5,1,2};
UINT4 IpMRouteBoundaryAddressMask [ ] ={1,3,6,1,2,1,83,1,1,5,1,3};
UINT4 IpMRouteBoundaryStatus [ ] ={1,3,6,1,2,1,83,1,1,5,1,4};
UINT4 IpMRouteScopeNameAddress [ ] ={1,3,6,1,2,1,83,1,1,6,1,1};
UINT4 IpMRouteScopeNameAddressMask [ ] ={1,3,6,1,2,1,83,1,1,6,1,2};
UINT4 IpMRouteScopeNameLanguage [ ] ={1,3,6,1,2,1,83,1,1,6,1,3};
UINT4 IpMRouteScopeNameString [ ] ={1,3,6,1,2,1,83,1,1,6,1,4};
UINT4 IpMRouteScopeNameDefault [ ] ={1,3,6,1,2,1,83,1,1,6,1,5};
UINT4 IpMRouteScopeNameStatus [ ] ={1,3,6,1,2,1,83,1,1,6,1,6};


tMbDbEntry stdmriMibEntry[]= {

{{10,IpMRouteEnable}, NULL, IpMRouteEnableGet, IpMRouteEnableSet, IpMRouteEnableTest, IpMRouteEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,IpMRouteGroup}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteSource}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteSourceMask}, GetNextIndexIpMRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteUpstreamNeighbor}, GetNextIndexIpMRouteTable, IpMRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteInIfIndex}, GetNextIndexIpMRouteTable, IpMRouteInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteUpTime}, GetNextIndexIpMRouteTable, IpMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteExpiryTime}, GetNextIndexIpMRouteTable, IpMRouteExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRoutePkts}, GetNextIndexIpMRouteTable, IpMRoutePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteDifferentInIfPackets}, GetNextIndexIpMRouteTable, IpMRouteDifferentInIfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteOctets}, GetNextIndexIpMRouteTable, IpMRouteOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteProtocol}, GetNextIndexIpMRouteTable, IpMRouteProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteRtProto}, GetNextIndexIpMRouteTable, IpMRouteRtProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteRtAddress}, GetNextIndexIpMRouteTable, IpMRouteRtAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteRtMask}, GetNextIndexIpMRouteTable, IpMRouteRtMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteRtType}, GetNextIndexIpMRouteTable, IpMRouteRtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteHCOctets}, GetNextIndexIpMRouteTable, IpMRouteHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpMRouteTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteNextHopGroup}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopSource}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopSourceMask}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopIfIndex}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopAddress}, GetNextIndexIpMRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopState}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopUpTime}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopExpiryTime}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopClosestMemberHops}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopClosestMemberHopsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopProtocol}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteNextHopPkts}, GetNextIndexIpMRouteNextHopTable, IpMRouteNextHopPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteNextHopTableINDEX, 5, 0, 0, NULL},

{{12,IpMRouteInterfaceIfIndex}, GetNextIndexIpMRouteInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteInterfaceTtl}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceTtlGet, IpMRouteInterfaceTtlSet, IpMRouteInterfaceTtlTest, IpMRouteInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteInterfaceProtocol}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteInterfaceRateLimit}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceRateLimitGet, IpMRouteInterfaceRateLimitSet, IpMRouteInterfaceRateLimitTest, IpMRouteInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpMRouteInterfaceTableINDEX, 1, 0, 0, "0"},

{{12,IpMRouteInterfaceInMcastOctets}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteInterfaceOutMcastOctets}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteInterfaceHCInMcastOctets}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteInterfaceHCOutMcastOctets}, GetNextIndexIpMRouteInterfaceTable, IpMRouteInterfaceHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, IpMRouteInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,IpMRouteBoundaryIfIndex}, GetNextIndexIpMRouteBoundaryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IpMRouteBoundaryTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteBoundaryAddress}, GetNextIndexIpMRouteBoundaryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteBoundaryTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteBoundaryAddressMask}, GetNextIndexIpMRouteBoundaryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteBoundaryTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteBoundaryStatus}, GetNextIndexIpMRouteBoundaryTable, IpMRouteBoundaryStatusGet, IpMRouteBoundaryStatusSet, IpMRouteBoundaryStatusTest, IpMRouteBoundaryTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpMRouteBoundaryTableINDEX, 3, 0, 1, NULL},

{{12,IpMRouteScopeNameAddress}, GetNextIndexIpMRouteScopeNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteScopeNameTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteScopeNameAddressMask}, GetNextIndexIpMRouteScopeNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IpMRouteScopeNameTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteScopeNameLanguage}, GetNextIndexIpMRouteScopeNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IpMRouteScopeNameTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteScopeNameString}, GetNextIndexIpMRouteScopeNameTable, IpMRouteScopeNameStringGet, IpMRouteScopeNameStringSet, IpMRouteScopeNameStringTest, IpMRouteScopeNameTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IpMRouteScopeNameTableINDEX, 3, 0, 0, NULL},

{{12,IpMRouteScopeNameDefault}, GetNextIndexIpMRouteScopeNameTable, IpMRouteScopeNameDefaultGet, IpMRouteScopeNameDefaultSet, IpMRouteScopeNameDefaultTest, IpMRouteScopeNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpMRouteScopeNameTableINDEX, 3, 0, 0, "2"},

{{12,IpMRouteScopeNameStatus}, GetNextIndexIpMRouteScopeNameTable, IpMRouteScopeNameStatusGet, IpMRouteScopeNameStatusSet, IpMRouteScopeNameStatusTest, IpMRouteScopeNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpMRouteScopeNameTableINDEX, 3, 0, 1, NULL},

{{10,IpMRouteEntryCount}, NULL, IpMRouteEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stdmriEntry = { 47, stdmriMibEntry };
#endif /* _STDMRIDB_H */

