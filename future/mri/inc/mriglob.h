/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mriglob.c                      */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  Any                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This File contain the Global   */
/*                           variables of MRI module        */
/*                           Initialization functions       */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#ifndef _MRI_GLOB_H_
#define _MRI_GLOB_H_

#ifdef _MRI_INIT_C_

tMriGlobalInfo  gMriGlobalInfo;

#else

extern tMriGlobalInfo gMriGlobalInfo;

#endif

#endif
