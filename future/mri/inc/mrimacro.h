/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  mrimacro.h                     */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  MRI                            */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  ANY                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This function contains all     */
/*                           macros used by the MRI module  */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/
#ifndef __MRI_MACRO_H__
#define __MRI_MACRO_H__

#define MRI_SEM_NAME  ("MRI")

#define MRI_FALSE  0
#define MRI_TRUE   1

#define MRI_DEF_IFACE_TTL_TRESHOLD   0
 
#define MRI_NO_RATELIMIT_VAL     0

#define MRI_SET_ALL_BITS  0xFFFFFFFF

#define MRI_PIM_MAX_COMPONENT   PIMSM_MAX_COMPONENT

#define MRI_FIRST_GT_SECOND (1)
#define MRI_FIRST_EQ_SECOND (0)
#define MRI_FIRST_LT_SECOND (-1)

#define MRI_MASK_TO_MASKLEN(Mask,Masklen)  \
{ \
   Masklen = CfaGetCidrSubnetMaskIndex (Mask); \
}

#define MRI_MASKLEN_TO_MASK(Masklen,Mask) \
{\
   Mask = (0xFFFFFFFF << (32 - (Masklen)));\
}

#endif
