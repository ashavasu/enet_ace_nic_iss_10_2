
/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : dscxecli.c                                       |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : FutureSoft                                       |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : Diffserv configuration for SwitchCore CXE chipset|
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for set/get objects in           |
* |                          fsissdfs.mib                                     |
* |                                                                           |
*  ---------------------------------------------------------------------------
*/
#ifndef __DSCXECLI_C__
#define __DSCXECLI_C__

#include "dscxeinc.h"
#include "iss.h"
#include "dscxecli.h"
#include "fsissdcli.h"

#ifdef DIFFSRV_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_ds_cmd                                 */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the CFA module as     */
/*                        defined in diffsrvcmd.h                            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_ds_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[10];
    INT1                argno = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    UINT4               u4ClassMap = 0;
    UINT4               u4PolicyMap = 0;
    UINT4               u4TrafficClass = 0;
    UINT4               u4REDCurveId = 0;
    INT4                i4PortNum = 0;

    if ((u4Command != CLI_DS_SYSTEM_CONTROL) &&
        (u4Command != CLI_DS_NO_SHUT) &&
        (u4Command != CLI_DS_SHUT) && (gDsGlobalInfo.DsStatus == DS_DISABLE))
    {
        CliPrintf (CliHandle, "\r%% Quality of Service is disabled\r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* second argument is always InterfaceName/Index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array. 
     * Store 5 arguments at the max. This is because Diffserv commands do not
     * take more than 10 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == DS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, DfsLock, DfsUnLock);

    DFS_LOCK ();

    switch (u4Command)
    {
        case CLI_DS_SHUT:
            i4RetStatus = DiffservShutdown (CliHandle, DS_SHUTDOWN);
            break;

        case CLI_DS_NO_SHUT:
            i4RetStatus = DiffservShutdown (CliHandle, DS_START);
            break;

        case CLI_DS_SYSTEM_CONTROL:
            i4RetStatus = DiffservSystemControl (CliHandle, (UINT4) (args[0]));
            break;

        case CLI_DS_CLASS_MAP:
            i4RetStatus =
                DiffservCreateClassMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_NO_CLASS_MAP:
            i4RetStatus =
                DiffservDestroyClassMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_POL_MAP:
            i4RetStatus =
                DiffservCreatePolicyMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_NO_POL_MAP:
            i4RetStatus =
                DiffservDestroyPolicyMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_TC_CREATE:
            if (args[1] != NULL)
            {
                CfaCliGetIfIndex (args[1], args[2], &i4PortNum);
            }
            i4RetStatus =
                DiffservCreateTC (CliHandle, *(INT4 *) (args[0]), i4PortNum);
            break;

        case CLI_DS_TC_NO_CREATE:
            i4RetStatus = DiffservDeleteTC (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_RANDOM_DETECT:
            i4RetStatus = DiffservSetREDParam (CliHandle, *(INT4 *) (args[0]),    /* ID */
                                               *(INT4 *) (args[1]),    /* Curve Start */
                                               *(INT4 *) (args[2]),    /* Curve Stop */
                                               *(INT4 *) (args[3]),    /* QRange */
                                               *(INT4 *) (args[4]));    /* StopProbability */
            break;

        case CLI_DS_NO_RANDOM_DETECT:
            i4RetStatus =
                DiffservDeleteREDCurve (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_DSCP_QUEUE_MAP:
            i4RetStatus =
                DiffservSetDSCPQueueMap (CliHandle, *(INT4 *) (args[0]),
                                         *(INT4 *) (args[1]));
            break;

        case CLI_DS_QUEUE_PREC_SWITCHED:
            i4RetStatus =
                DiffservSetQueuePrecSwitched (CliHandle, (UINT4) (args[0]),
                                              (UINT4) (args[1]));
            break;

        case CLI_DS_QUEUE_PREC_ROUTED:
            i4RetStatus =
                DiffservSetQueuePrecRouted (CliHandle, (UINT4) (args[0]),
                                            (UINT4) (args[1]));
            break;

        case CLI_DS_CLASS_MAP_MATCH:
            /* Get the Filter Type and filter number */
            i4RetStatus = DiffservMatchFilter (CliHandle, (UINT4) (args[0]),
                                               *(UINT4 *) (args[1]));
            break;

        case CLI_DS_POLCL_MAP:
            /* Get the class map value  to associate with the policy map */
            i4RetStatus = DiffservPolicyClass (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_NO_POLCL_MAP:
            i4RetStatus =
                DiffservNoPolicyClass (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_POL_IN_ACTION:
            /* Get the in profile action type and corresponding value */

            i4RetStatus = DiffservInProfAction (CliHandle, (UINT4) (args[0]),
                                                *(UINT4 *) (args[1]));
            break;

        case CLI_DS_NO_POL_IN_ACTION:
            i4RetStatus =
                DiffservNoInProfAction (CliHandle, (UINT4) (args[0]), 0);
            break;

        case CLI_DS_TC_MAP:
            i4RetStatus = DiffservMapTC (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_TC_NO_MAP:
            i4RetStatus = DiffservUnmapTC (CliHandle);
            break;

        case CLI_DS_TC_MAX_BW:
            /* Set the max. bandwidth alone */
            i4RetStatus =
                DiffservSetTCMaxBw (CliHandle, *(INT4 *) (args[0]), DS_FALSE);
            break;

        case CLI_DS_TC_MAX_BW_DROP:
            /* Set the max. bandwidth and the drop action */
            i4RetStatus =
                DiffservSetTCMaxBw (CliHandle, *(INT4 *) (args[0]), DS_TRUE);
            break;

        case CLI_DS_TC_NO_MAX_BW_DROP:
            /* Apply the previously configured Drop thresholds alone.
             * Do not reset the max bandwidth */
            i4RetStatus = DiffservResetTCMaxBw (CliHandle, DS_TRUE);
            break;

        case CLI_DS_TC_NO_MAX_BW:
            /* Reset the max bandwidth also apart from applying the 
             * drop thresholds */
            i4RetStatus = DiffservResetTCMaxBw (CliHandle, DS_FALSE);
            break;

        case CLI_DS_TC_GUARNTEED_BW:
            i4RetStatus =
                DiffservSetTCGuarntdBw (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_TC_NO_GUARNTEED_BW:
            i4RetStatus = DiffservResetTCGuarntdBw (CliHandle);
            break;

        case CLI_DS_TC_QUEUE_THRESHOLD:
            i4RetStatus =
                DiffservSetTCDropLevels (CliHandle, (INT4) args[0],
                                         (INT4) (args[1]), (INT4) (args[2]));
            break;

        case CLI_DS_TC_NO_QUEUE_THRESHOLD:
            i4RetStatus = DiffservResetTCDropLevels (CliHandle);
            break;

        case CLI_DS_TC_RED_MAP:
            i4RetStatus = DiffservSetTCREDMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_TC_NO_RED_MAP:
            /* Setting a value of DS_MIN_REDCURVES-1 disables RED on this 
             * Traffic class */
            i4RetStatus = DiffservSetTCREDMap (CliHandle, DS_MIN_REDCURVES - 1);
            break;

        case CLI_DS_TC_FAIRQUEUE:
            i4RetStatus =
                DiffservSetTCNumFlowGroups (CliHandle, (INT4) (args[0]));
            break;

        case CLI_DS_TC_NO_FAIRQUEUE:
            /* Setting a value of 0 disables flow groups in the hardware 
             * for this traffic class */
            i4RetStatus = DiffservSetTCNumFlowGroups (CliHandle, 0);
            break;

        case CLI_DS_TC_WEIGHT:
            i4RetStatus = DiffservSetTCWeight (CliHandle, (INT4) (args[0]),
                                               (INT4) (args[1]));
            break;

        case CLI_DS_TC_NO_WEIGHT:
            i4RetStatus = DiffservResetTCWeight (CliHandle, (INT4) (args[0]));
            break;

        case CLI_DS_PORT_MAX_BW_DROP:
            i4RetStatus =
                DiffservSetPortMaxBw (CliHandle, *(INT4 *) (args[0]), DS_TRUE);
            break;

        case CLI_DS_PORT_MAX_BW:
            i4RetStatus =
                DiffservSetPortMaxBw (CliHandle, *(INT4 *) (args[0]), DS_FALSE);
            break;

        case CLI_DS_PORT_NO_MAX_BW_DROP:
            i4RetStatus = DiffservResetPortMaxBw (CliHandle, DS_TRUE);
            break;

        case CLI_DS_PORT_NO_MAX_BW:
            i4RetStatus = DiffservResetPortMaxBw (CliHandle, DS_FALSE);
            break;

        case CLI_DS_PORT_QUEUE_THRESHOLD:
            i4RetStatus =
                DiffservSetPortDropLevels (CliHandle, (INT4) args[0],
                                           (INT4) (args[1]), (INT4) (args[2]));
            break;

        case CLI_DS_PORT_NO_QUEUE_THRESHOLD:
            i4RetStatus = DiffservResetPortDropLevels (CliHandle);
            break;

        case CLI_DS_PORT_RED_MAP:
            i4RetStatus =
                DiffservSetPortREDMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_PORT_NO_RED_MAP:
            /* Setting a value of DS_MAX_REDCURVES+1 disables RED on this 
             * port */
            i4RetStatus =
                DiffservSetPortREDMap (CliHandle, DS_MAX_REDCURVES + 1);
            break;

        case CLI_SHOW_DS_POLICYMAP:
            /* Get the policy map and/or the class map for which details 
             * must be displayed */

            if (((UINT4 *) (args[0])) != NULL)
            {
                u4PolicyMap = *(UINT4 *) (args[0]);
            }
            if (((UINT4 *) (args[1])) != NULL)
            {
                u4ClassMap = *(UINT4 *) (args[1]);
            }
            i4RetStatus = DiffservShowPolicyMap (CliHandle, u4PolicyMap,
                                                 u4ClassMap);

            break;

        case CLI_SHOW_DS_CLASSMAP:
            /* Get the Class map number */

            if (((UINT4 *) (args[0])) != NULL)
            {
                u4ClassMap = *(UINT4 *) (args[0]);
            }
            i4RetStatus = DiffservShowClassMap (CliHandle, u4ClassMap);
            break;

        case CLI_SHOW_DS_DSCP_QUEUE_MAP:
            i4RetStatus = DiffservShowDscpQueueMap (CliHandle);
            break;

        case CLI_SHOW_DS_TC_FLOWGROUPS_MAP:
            i4RetStatus = DiffservShowTCFlowGroupsMap (CliHandle);
            break;

        case CLI_SHOW_DS_QUEUE_PREC:
            i4RetStatus = DiffservShowQueuePrecedence (CliHandle);
            break;

        case CLI_SHOW_DS_TRAFFICCLASS:
            if (((UINT4 *) (args[0])) != NULL)
            {
                u4TrafficClass = *(UINT4 *) (args[0]);
            }
            i4RetStatus = DiffservShowTrafficClass (CliHandle, u4TrafficClass);
            break;

        case CLI_SHOW_DS_RED:
            if (((UINT4 *) (args[0])) != NULL)
            {
                u4REDCurveId = *(UINT4 *) (args[0]);
            }
            i4RetStatus = DiffservShowREDParams (CliHandle, u4REDCurveId);
            break;

        case CLI_SHOW_DS_INTERFACE:
            if (args[0] != NULL)
            {
                CfaCliGetIfIndex (args[0], args[1], &i4PortNum);
            }
            i4RetStatus = DiffservShowQosInterface (CliHandle, i4PortNum);
            break;

        case CLI_SHOW_DS_STATISTICS:
            i4RetStatus = DiffservShowQosStatistics (CliHandle);
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", DsCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    DFS_UNLOCK ();

    return i4RetStatus;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : DiffservShutdown                                  */
/*                                                                          */
/*     DESCRIPTION      : This function sets the Diffserv system control    */
/*                        status                                            */
/*                                                                          */
/*     INPUT            :  u4Status, CliHandle-CLI Handler                  */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/*****************************************************************************/

INT4
DiffservShutdown (tCliHandle CliHandle, UINT4 u4Status)
{

    UINT4               u4ErrCode = 0;
    INT4                i4State;

    if (nmhTestv2FsDsSystemControl (&u4ErrCode, (INT4) u4Status)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDsSystemControl ((INT4) u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (u4Status == DS_START)
    {
        i4State = DS_ENABLE;
        /* Set Module status */
        if (nmhTestv2FsDsStatus (&u4ErrCode, i4State) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsDsStatus (i4State) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        /* If "no shutdown qos" is executed diffserv will be started and
         * enabled */
    }

    return (CLI_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : DiffservSystemControl                             */
/*                                                                          */
/*     DESCRIPTION      : This function sets the Diffserv module            */
/*                        status                                            */
/*                                                                          */
/*     INPUT            :  u4Status, CliHandle-CLI Handler                  */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/*****************************************************************************/

INT4
DiffservSystemControl (tCliHandle CliHandle, UINT4 u4Status)
{

    UINT4               u4ErrCode = 0;
    INT4                i4State = 0;

    if (u4Status == CLI_ENABLE)
    {
        u4Status = DS_START;
        i4State = DS_ENABLE;
    }
    /* 'set qos disable' will only disable the hardware, but it
     * will not deallocate the memory. Once allocated, it will 
     * be present through out the life time of the system  */
    else if (u4Status == CLI_DISABLE)
    {
        i4State = DS_DISABLE;
    }

    /* Set system control status */

    if (u4Status == DS_START)
    {
        if (nmhTestv2FsDsSystemControl (&u4ErrCode, (INT4) u4Status)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsDsSystemControl ((INT4) u4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    /* Set Module status */
    if (nmhTestv2FsDsStatus (&u4ErrCode, i4State) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDsStatus (i4State) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCreateClassMap                             */
/*                                                                           */
/*     DESCRIPTION      : This function creates the Diffserv classifier      */
/*                                                                           */
/*     INPUT            :  i4Classmap - Classifier to be created             */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservCreateClassMap (tCliHandle CliHandle, INT4 i4Classmap)
{
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    UINT4               u4ErrCode;
    INT4                i4Status = 0;

    /* If Class map already exists, do nothing */

    if (nmhGetFsDiffServMultiFieldClfrStatus ((INT4) i4Classmap, &i4Status) !=
        SNMP_SUCCESS)

    {
        /* Class Map does not exist and must be created */

        if (nmhTestv2FsDiffServMultiFieldClfrStatus
            (&u4ErrCode, i4Classmap, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsDiffServMultiFieldClfrStatus
            (i4Classmap, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* The execution of this command must return the Class map configuration 
     * mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_CLASSMAP_MODE, i4Classmap);
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservDestroyClassMap                            */
/*                                                                           */
/*     DESCRIPTION      : This function destroys the Diffserv MF classifier  */
/*                                                                           */
/*     INPUT            :  i4Classmap - Classifier to be deleted             */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservDestroyClassMap (tCliHandle CliHandle, INT4 i4ClassMapId)
{
    INT4                i4FsDiffServClfrId;
    INT4                i4FsDiffServClfrMFClfrId;
    INT4                i4PrevFsDiffServClfrId = 0;
    INT4                i4Status;

    /* Check if the class map exists */
    if (nmhGetFsDiffServMultiFieldClfrStatus ((INT4) i4ClassMapId, &i4Status) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%  Invalid Class Map \r\n");
        return (CLI_FAILURE);
    }

    /* Class map exists. We now need to check if there is any policy map
     * dependency for this class map */

    /* Scan all the Classifier entries */

    if (nmhGetFirstIndexFsDiffServClfrTable (&i4FsDiffServClfrId) !=
        SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsDiffServClfrMFClfrId (i4FsDiffServClfrId,
                                              &i4FsDiffServClfrMFClfrId) !=
                SNMP_FAILURE)
            {

                if (i4FsDiffServClfrMFClfrId == i4ClassMapId)
                {
                    /*Policy map <-> class map match found , hence this
                     * class map cannot be deleted */

                    CliPrintf (CliHandle, "\r%% Class-map %d is being used  by "
                               "a policy-map and cannot be modified\r\n",
                               i4ClassMapId);
                    return (CLI_FAILURE);
                }

                i4PrevFsDiffServClfrId = i4FsDiffServClfrId;
            }
        }
        while (nmhGetNextIndexFsDiffServClfrTable (i4PrevFsDiffServClfrId,
                                                   &i4FsDiffServClfrId) !=
               SNMP_FAILURE);

    }

    /* No Policy map  has been created  or no policy map is using this
     * classmap and only classmap exists , then destroy the MFClfr  */

    nmhSetFsDiffServMultiFieldClfrStatus (i4ClassMapId, DS_DESTROY);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCreatePolicyMap                            */
/*                                                                           */
/*     DESCRIPTION      : This function creates the Diffserv classifier      */
/*                                                                           */
/*     INPUT            :  i4PolIndex - Policy map to be created             */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservCreatePolicyMap (tCliHandle CliHandle, INT4 i4PolIndex)
{
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    INT4                i4Status;
    UINT4               u4ErrCode;

    /* If Policy map exists ,do nothing */

    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)

    {
        /* Policy map does not exist and it must be created */

        if (nmhTestv2FsDiffServClfrStatus
            (&u4ErrCode, i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFsDiffServClfrStatus
            (i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* The execution of this command must return the policy map 
     * configuration mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_POLICYMAP_MODE, i4PolIndex);
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservDestroyPolicyMap                           */
/*                                                                           */
/*     DESCRIPTION      : This function destroys the Diffserv classifier     */
/*                                                                           */
/*     INPUT            :  i4PolicymapId -  to be deleted                    */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservDestroyPolicyMap (tCliHandle CliHandle, INT4 i4PolicyMapId)
{
    INT4                i4RowStatus;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pDiffServClfrEntry = DsGetClfrEntry (i4PolicyMapId);

    if (pDiffServClfrEntry == NULL)

    {
        CliPrintf (CliHandle, "\r%% Invalid Policy map\r\n");
        return (CLI_FAILURE);
    }

    /* Set the Policy map status to NOT_IN_SERVICE and then destroy
     * each entry for in-profile action, out-profile action, no-match action
     * before finally destroying the Policy map
     */
    nmhGetFsDiffServClfrStatus (pDiffServClfrEntry->i4DsClfrId, &i4RowStatus);

    if (i4RowStatus == DS_ACTIVE)
    {
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, DS_NOT_IN_SERVICE) == DS_FAILURE)

            /* This operation is required since there is a dependency between
             * the Classifier entry and the Inprofile/ outprofile/
             * no-match action entry */
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (pDiffServClfrEntry->i4DsClfrInProActionId != 0)

    {
        if (nmhSetFsDiffServInProfileActionStatus
            (pDiffServClfrEntry->i4DsClfrInProActionId,
             DS_DESTROY) == DS_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (nmhSetFsDiffServClfrStatus
        (pDiffServClfrEntry->i4DsClfrId, DS_DESTROY) == DS_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservMatchFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function creates the MF clfr                  */
/*                        with the values from Ip/Mac filter                 */
/*                                                                           */
/*     INPUT            : u4FilterType - IP/MAC ACL                          */
/*                        i4FilterNo - IP/MAC Filter number                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservMatchFilter (tCliHandle CliHandle, UINT4 u4FilterType, INT4 i4FilterNo)
{
    tIssL2FilterEntry  *pDsL2FilterEntry;
    tIssL3FilterEntry  *pDsL3FilterEntry;
    INT4                i4ClMapIndex = 0;
    INT4                i4MFClfrId;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4PrevFsDiffServClfrId = 0;
    INT4                i4FsDiffServClfrId;
    INT4                i4FsDiffServClfrMFClfrId;

    /* Get the current class map identifier */
    i4MFClfrId = CLI_GET_CLASSMAP_ID ();

    nmhGetFsDiffServMultiFieldClfrStatus (i4MFClfrId, &i4RowStatus);

    if (i4RowStatus != DS_NOT_READY)
    {
        /* This classifier already exists and is going to be modified,
         * by destroying the existing MFclfr. If there is an associated 
         * policymap then modification must not be allowed
         */

        /* Scan all the Classifier entries */
        if (nmhGetFirstIndexFsDiffServClfrTable (&i4FsDiffServClfrId) !=
            SNMP_FAILURE)
        {
            do
            {
                if (nmhGetFsDiffServClfrMFClfrId (i4FsDiffServClfrId,
                                                  &i4FsDiffServClfrMFClfrId) !=
                    SNMP_FAILURE)
                {

                    if (i4FsDiffServClfrMFClfrId == i4MFClfrId)
                    {
                        /*Policy map <-> class map match found , hence this
                         * class map cannot be deleted */
                        CliPrintf (CliHandle,
                                   "\r%% Class-map %d is being used by a"
                                   " policy map\r\n", i4MFClfrId);

                        return (CLI_FAILURE);
                    }

                    i4PrevFsDiffServClfrId = i4FsDiffServClfrId;
                }
            }
            while (nmhGetNextIndexFsDiffServClfrTable (i4PrevFsDiffServClfrId,
                                                       &i4FsDiffServClfrId) !=
                   SNMP_FAILURE);

        }

        /* Policy maps have not been created or no policy map
         * is associated with this Class Map 
         *  Delete the MFClfr  */

        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, DS_DESTROY);

        /* Creating a new class map now */
        if (nmhTestv2FsDiffServMultiFieldClfrStatus
            (&u4ErrCode, i4MFClfrId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsDiffServMultiFieldClfrStatus
            (i4MFClfrId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* If Filter is a MAC filter, set the corresponding parameters */

    if (u4FilterType == DS_MAC_FILTER)
    {
        /* Check if this filter exists */

        if ((pDsL2FilterEntry = IssGetL2FilterEntry (i4FilterNo)) == NULL)

        {
            CliPrintf (CliHandle, "\r%% Invalid MAC filter\r\n");
            return (CLI_FAILURE);
        }

        /* Get the current class map identifier */
        i4ClMapIndex = CLI_GET_CLASSMAP_ID ();

        if (DsCreateMFClfrFromMacFilter
            (CliHandle, i4FilterNo, i4ClMapIndex) == DS_FAILURE)

        {
            return (CLI_FAILURE);
        }
    }

    /* If Filter is a IP filter, set the corresponsing parameters */
    else if (u4FilterType == DS_IP_FILTER)
    {

        /* Check if this filter exists */
        if ((pDsL3FilterEntry = IssGetL3FilterEntry (i4FilterNo)) == NULL)

        {
            CliPrintf (CliHandle, "\r %% Invalid IP filter\r\n");
            return (CLI_FAILURE);
        }

        /* Get the current class map identifier */
        i4ClMapIndex = CLI_GET_CLASSMAP_ID ();

        if (DsCreateMFClfrFromIpFilter
            (CliHandle, i4FilterNo, i4ClMapIndex) == DS_FAILURE)

        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetClassMapPrompt                           */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetClassMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{

    UINT4               u4Len;
    INT4                i4ClassMapIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-cmap" as the prompt 
     * for the mode CLASSMAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-cmap");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_CLASSMAP_MODE);
    if (STRNCMP (pi1ModeName, CLI_CLASSMAP_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;
    u4Len = STRLEN (pi1ModeName);

    i4ClassMapIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_CLASSMAP_ID (i4ClassMapIndex);

    STRCPY (pi1DispStr, "(config-cmap)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetPolicyMapPrompt                          */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetPolicyMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4PolicyMapIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-pmap" as the prompt 
     * for the mode POLICYMAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-pmap");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_POLICYMAP_MODE);
    if (STRNCMP (pi1ModeName, CLI_POLICYMAP_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4PolicyMapIndex = (atoi) ((char *) pi1ModeName);

    CLI_SET_POLICYMAP_ID (i4PolicyMapIndex);

    STRCPY (pi1DispStr, "(config-pmap)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetPolicyClassMapPrompt                    */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Policy Class map prompt in         */
/*                        pi1DispStr if valid                                */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetPolicyClassMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4PolClMapIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-pmap-c" as the prompt 
     * for the mode POLICY CLASS MAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-pmap-c");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_POLICYCLASS_MODE);
    if (STRNCMP (pi1ModeName, CLI_POLICYCLASS_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4PolClMapIndex = (atoi) ((char *) pi1ModeName);

    CLI_SET_POCL_MAP_ID (i4PolClMapIndex);

    STRCPY (pi1DispStr, "(config-pmap-c)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateMFClfrFromMacFilter                        */
/*                                                                           */
/*     DESCRIPTION      : This function creates a multifield classifier      */
/*                        entry based on the MAC filter associated.          */
/*                                                                           */
/*     INPUT            : i4MacFilterNo - associated MAC filter number       */
/*                        i4MFClfrId    - multifield classifier ID for       */
/*                                        this entry                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateMFClfrFromMacFilter (tCliHandle CliHandle, INT4 i4MacFilterNo,
                             INT4 i4MFClfrId)
{
    tIssL2FilterEntry  *pDsMacFilter = NULL;
    UINT4               u4ErrCode = 0;
    tMacAddr            zeroAddr;

    /* Please note : only creation of a new multifield classifier will be
     * done in this routine. It will not update an existing one.
     */

    /* Get the details of the associated MAC filter */
    pDsMacFilter = IssGetL2FilterEntry (i4MacFilterNo);

    DS_MEMSET (zeroAddr, 0, DS_ETHERNET_ADDR_SIZE);

    if (nmhTestv2FsDiffServMultiFieldClfrFilterId (&u4ErrCode, i4MFClfrId,
                                                   i4MacFilterNo) ==
        SNMP_FAILURE)

    {
        CliPrintf (CliHandle, "\r%% Invalid Filter Id\r\n");
        return DS_FAILURE;
    }

    if (nmhTestv2FsDiffServMultiFieldClfrFilterType (&u4ErrCode, i4MFClfrId,
                                                     DS_MAC_FILTER) ==
        SNMP_FAILURE)

    {
        CliPrintf (CliHandle, "\r%% Invalid Filter Type\r\n");
        return DS_FAILURE;
    }

    /* Set the Mac filter parameters */
    /*Set Filter Id */
    if (nmhSetFsDiffServMultiFieldClfrFilterId (i4MFClfrId, i4MacFilterNo)
        == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /*Set Filter Type */
    if (nmhSetFsDiffServMultiFieldClfrFilterType (i4MFClfrId, DS_MAC_FILTER)
        == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /* Once all the filter parameters have been configured, set the row status
     * to ACTIVE */

    if (nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        return (DS_FAILURE);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateMFClfrFromIpFilter                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates a multifield classifier      */
/*                        entry based on the IP filter associated.           */
/*                                                                           */
/*     INPUT            : i4IpFilterNo - associated IP filter number         */
/*                        i4MFClfrId   - multifield classifier ID for        */
/*                                       this entry                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateMFClfrFromIpFilter (tCliHandle CliHandle, INT4 i4IpFilterNo,
                            INT4 i4MFClfrId)
{
    tIssL3FilterEntry  *pDsIpFilter = NULL;
    UINT4               u4ErrCode = 0;
    /* Please note : only creation of a new multifield classifier will be
     * done in this routine. It will not update an existing one.
     */

    /* Get the details of the associated IP filter */

    pDsIpFilter = IssGetL3FilterEntry (i4IpFilterNo);
    if (pDsIpFilter == NULL)

    {
        CliPrintf (CliHandle, "\r%% Invalid IP filter\r\n");
        return DS_FAILURE;
    }

    if (nmhTestv2FsDiffServMultiFieldClfrFilterId (&u4ErrCode, i4MFClfrId,
                                                   i4IpFilterNo) ==
        SNMP_FAILURE)

    {
        CliPrintf (CliHandle, "\r%% Invalid Filter Id\r\n");
        return DS_FAILURE;
    }

    if (nmhTestv2FsDiffServMultiFieldClfrFilterType (&u4ErrCode, i4MFClfrId,
                                                     DS_IP_FILTER) ==
        SNMP_FAILURE)

    {
        CliPrintf (CliHandle, "\r%% Invalid Filter Type\r\n");
        return DS_FAILURE;
    }

    /*Set Filter Id */
    if (nmhSetFsDiffServMultiFieldClfrFilterId (i4MFClfrId, i4IpFilterNo)
        == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /*Set Filter Type */
    if (nmhSetFsDiffServMultiFieldClfrFilterType (i4MFClfrId, DS_IP_FILTER)
        == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /* Once all the filter parameters have been configured, set the row status
     * to ACTIVE */

    if (nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservPolicyClass                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Policy-class map relation   */
/*                                                                           */
/*     INPUT            :  i4ClassMap Id- Class map                          */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservPolicyClass (tCliHandle CliHandle, INT4 i4ClassMapId)
{
    INT4                i4PolIndex;
    tDsClfr             DsClfr;
    INT4                i4Status;
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    UINT4               u4ErrCode = 0;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServClfrEntry *pClfrList = NULL;
    tDsSllNode         *pSllNode = NULL;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POLICYMAP_ID ();

    /* Check if class map exists */

    if (nmhGetFsDiffServMultiFieldClfrStatus
        (i4ClassMapId, &i4Status) != SNMP_SUCCESS)

    {
        CliPrintf (CliHandle, "\r%% Invalid Class map \r\n");
        return (CLI_FAILURE);
    }

    /* Check if the class-map already allocated to another policy-map.
     * We can't allocate the same class-map to different policy-map
     */
    pDsClfrEntry = DsGetClfrEntry (i4PolIndex);

    if (pDsClfrEntry != NULL)
    {
        pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

        while (pSllNode != NULL)
        {
            pClfrList = (tDiffServClfrEntry *) pSllNode;

            if ((i4ClassMapId == pClfrList->i4DsClfrMFClfrId)
                && (pDsClfrEntry != pClfrList))
            {
                CliPrintf (CliHandle,
                           "\r%% Class map already allocated to another policy-map\r\n");
                return (CLI_FAILURE);
            }
            else if ((i4ClassMapId == pClfrList->i4DsClfrMFClfrId)
                     && (pDsClfrEntry == pClfrList))
            {
                /* If there is no change in class-map id, then do nothing */
                /* The execution of this command must return the policy class
                 * map configuration mode */

                SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_POLICYCLASS_MODE,
                         i4PolIndex);
                CliChangePath ((CHR1 *) au1IfName);
                return (CLI_SUCCESS);
            }

            pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
        }

        nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status);

        if ((i4Status == DS_NOT_READY) || (i4Status == DS_ACTIVE))
        {
            /* Policy-map exists and has been previously associated
             * to a class map */
            /* First set the Classifier status to NOT_IN_SERVICE 
             * Destroy in-profile action and Data path 
             * Now, Destroy the Classifier, and create it again. 
             * Remap  the policy map the the specified class map
             */

            if (i4Status == DS_ACTIVE)
            {
                if (nmhSetFsDiffServClfrStatus
                    (pDsClfrEntry->i4DsClfrId, DS_NOT_IN_SERVICE) == DS_FAILURE)

                    /* This operation is required since there is a dependency
                     * between the Classifier entry and the Inprofile/
                     * outprofile/ no-match action entry */
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (pDsClfrEntry->i4DsClfrInProActionId != 0)
            {
                if (nmhSetFsDiffServInProfileActionStatus
                    (pDsClfrEntry->i4DsClfrInProActionId,
                     DS_DESTROY) == DS_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (nmhSetFsDiffServClfrStatus
                (pDsClfrEntry->i4DsClfrId, DS_DESTROY) == DS_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhTestv2FsDiffServClfrStatus
                (&u4ErrCode, i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsDiffServClfrStatus
                (i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle,
                       "\r\nExisting Policy-map configurations have been deleted. Please apply the policy-map to make it active.\r\n");
        }
    }
    /* set the already existing MF clfr index */
    DsClfr.i4DsMFClfrId = i4ClassMapId;
    DsClfr.i4DsClfrId = i4PolIndex;
    DsClfr.i4DsInProActId = 0;
    DsClfr.i4DsTrafficClassId = 0;

    /* Create the classifier 
     */
    if (DsCreateClfr (CliHandle, &DsClfr, DS_TRUE) == DS_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* The execution of this command must return the Policy map configuration 
     * mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_POLICYCLASS_MODE, i4PolIndex);
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateClfr                                       */
/*                                                                           */
/*     DESCRIPTION      : This function creates a classifier entry.          */
/*                                                                           */
/*     INPUT            : DsClfr - classifier details                        */
/*                        u1NewFlag - flag for status of classifier          */
/*                                    TRUE - new, FALSE - existing           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateClfr (tCliHandle CliHandle, tDsClfr * pDsClfr, BOOLEAN u1NewFlag)
{
    INT4                i4ClfrStatus = 0;
    UINT4               u4ErrCode = 0;

    /* This routine allows both creation and updation of a classifier entry.
     */
    if (nmhGetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, &i4ClfrStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* If MFClfrId/ In-profile action Id/ Out-profile Action Id  are 
     * set to a value other than zero then it will be destroyed first and 
     * then the new value will be set in the Classifier Entry */

    if (pDsClfr->i4DsMFClfrId != 0)
    {
        if (nmhTestv2FsDiffServClfrMFClfrId
            (&u4ErrCode, pDsClfr->i4DsClfrId,
             pDsClfr->i4DsMFClfrId) == SNMP_FAILURE)
        {
            return DS_FAILURE;
        }
    }

    if (pDsClfr->i4DsInProActId != 0)
    {
        if (nmhTestv2FsDiffServClfrInProActionId
            (&u4ErrCode, pDsClfr->i4DsClfrId,
             pDsClfr->i4DsInProActId) == SNMP_FAILURE)
        {
            return DS_FAILURE;
        }
    }

    /* The existing traffic class may be reused by some other policy and
     * hence is not deleted */

    if (u1NewFlag != DS_TRUE)
    {
        /* An existing entry is getting updated. So set the classifier row
         * status to NOT_IN_SERVICE. After setting the other parameters change
         * the row status of the classifier entry to ACTIVE.
         */

        if (i4ClfrStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDsClfr->i4DsClfrId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
        }
    }
    if (pDsClfr->i4DsMFClfrId != 0)

    {
        if (nmhSetFsDiffServClfrMFClfrId
            (pDsClfr->i4DsClfrId, pDsClfr->i4DsMFClfrId) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    if (pDsClfr->i4DsInProActId != 0)

    {
        if (nmhSetFsDiffServClfrInProActionId
            (pDsClfr->i4DsClfrId, pDsClfr->i4DsInProActId) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    if (pDsClfr->i4DsTrafficClassId != 0)

    {
        if (nmhSetFsDiffServClfrTrafficClassId
            (pDsClfr->i4DsClfrId, pDsClfr->i4DsTrafficClassId) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    if (nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservInProfAction                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures the policy map in-profile */
/*                        action                                             */
/*                                                                           */
/*     INPUT            : u4InProfType- Inprofile action                     */
/*                        u4Value - DSCP /IP Precedence/COS value as the case*/
/*                        may be                                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservInProfAction (tCliHandle CliHandle, UINT4 u4InProfType, UINT4 u4Value)
{

    tDsAction           DsAction;
    tDsClfr             DsClfr;
    INT4                i4Status;
    INT4                i4PolIndex;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map already exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)

    {
        return (CLI_FAILURE);
    }

    DsAction.i4ActionId = i4PolIndex;

    /* Get the In-profile action from the user input type and value */
    switch (u4InProfType)

    {
        case DS_DSCP:
            DsAction.u4Dscp = u4Value;
            DsAction.u4Precedence = 0;
            DsAction.u4Cos = 0;
            DsAction.i4Action = DS_ACT_DSCP;
            break;
        case DS_PREC:
            DsAction.u4Precedence = u4Value;
            DsAction.u4Dscp = 0;
            DsAction.u4Cos = 0;
            DsAction.i4Action = DS_ACT_PREC;
            break;
        case DS_COS:
            DsAction.u4Cos = u4Value;
            DsAction.u4Dscp = 0;
            DsAction.u4Precedence = 0;
            DsAction.i4Action = DS_ACT_COS;
            break;
    }

    /* Create the in profile action entry  */
    if (DsCreateInProfAction (CliHandle, &DsAction) == DS_FAILURE)

    {
        return (CLI_FAILURE);
    }
    DsClfr.i4DsInProActId = i4PolIndex;
    DsClfr.i4DsClfrId = i4PolIndex;
    DsClfr.i4DsMFClfrId = 0;
    DsClfr.i4DsTrafficClassId = 0;

    /* Update the classifier with the in profile action
     * just created. When unsuccessful delete the action entry just created.
     */
    if (DsCreateClfr (CliHandle, &DsClfr, DS_FALSE) == DS_FAILURE)

    {
        if (DsClfr.i4DsInProActId != 0)

        {
            nmhSetFsDiffServInProfileActionStatus (DsClfr.i4DsInProActId,
                                                   DS_DESTROY);
        }
        return (CLI_FAILURE);

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateInProfAction                               */
/*                                                                           */
/*     DESCRIPTION      : This function creates a in profile action entry.   */
/*                                                                           */
/*     INPUT            : DsAction  - action details                         */
/*                                                                           */
/*     OUTPUT           : ppu1ErrBuf - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateInProfAction (tCliHandle CliHandle, tDsAction * pDsAction)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4Update = 0;

    /* Check if In-profile entry exists for this policy map */

    if (nmhGetFsDiffServInProfileActionStatus
        (pDsAction->i4ActionId, &i4RowStatus) == SNMP_FAILURE)

    {
        if (nmhTestv2FsDiffServInProfileActionStatus
            (&u4ErrCode, pDsAction->i4ActionId,
             DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CliPrintf (CliHandle, "\r%% Invalid in profile action\r\n");
            return DS_FAILURE;
        }

        /* Create the inprofile entry */

        if (nmhSetFsDiffServInProfileActionStatus
            (pDsAction->i4ActionId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }

    else

    {
        /* Inprofile entry previously exists. 
         * First set the Classifier status to NOT_IN_SERVICE.
         * Then set the In profile entry to NOT_IN_SERVICE.
         * Now set the action specified by the user and
         * reset the InProfile entry status to ACTIVE */
        nmhGetFsDiffServClfrStatus (pDsAction->i4ActionId, &i4RowStatus);
        if (i4RowStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDsAction->i4ActionId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)
                /* This operation is required since there is a dependency between
                 * the Classifier entry and the Inprofile action entry */

            {
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            u4Update = 1;
        }
        if (nmhSetFsDiffServInProfileActionStatus
            (pDsAction->i4ActionId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }

    }

    i4RowStatus = DS_DESTROY;

    /* Test In profile action value */
    /* If any test/set fails destroy the InProfileAction Entry */

    switch (pDsAction->i4Action)
    {
        case DS_ACT_DSCP:
            if (nmhTestv2FsDiffServInProfileActionDscp
                (&u4ErrCode, pDsAction->i4ActionId,
                 pDsAction->u4Dscp) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                return DS_FAILURE;
            }
            break;

        case DS_ACT_PREC:
            if (nmhTestv2FsDiffServInProfileActionIpTOS
                (&u4ErrCode, pDsAction->i4ActionId,
                 pDsAction->u4Precedence) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                return DS_FAILURE;
            }
            break;

        case DS_ACT_COS:
            if (nmhTestv2FsDiffServInProfileActionNewPrio
                (&u4ErrCode, pDsAction->i4ActionId,
                 pDsAction->u4Cos) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                return DS_FAILURE;
            }
            break;
    }

    /* Set In profile action value */
    switch (pDsAction->i4Action)
    {
        case DS_ACT_DSCP:
            if (nmhSetFsDiffServInProfileActionDscp
                (pDsAction->i4ActionId, pDsAction->u4Dscp) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            break;

        case DS_ACT_PREC:
            if (nmhSetFsDiffServInProfileActionIpTOS
                (pDsAction->i4ActionId,
                 pDsAction->u4Precedence) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            break;

        case DS_ACT_COS:
            if (nmhSetFsDiffServInProfileActionNewPrio
                (pDsAction->i4ActionId, pDsAction->u4Cos) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            break;
    }

    /* Set In profile action entry to active */
    if (nmhSetFsDiffServInProfileActionStatus
        (pDsAction->i4ActionId, DS_ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                               i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }

    if (u4Update)
    {
        /* This flag indicates that this entry previously existed and 
         * is now being updated */
        /* Set the rowstatus of the Classifier to Active */

        if (nmhSetFsDiffServClfrStatus (pDsAction->i4ActionId, DS_ACTIVE) ==
            SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservNoInProfAction                             */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the in-profile action        */
/*                                                                           */
/*     INPUT            : u4InProfType- Inprofile action                     */
/*                        u4Value - DSCP /IP Precedence/COS value as the case*/
/*                        may be                                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservNoInProfAction (tCliHandle CliHandle, UINT4 u4InProfType, UINT4 u4Value)
{
    INT4                i4Status;
    INT4                i4PolIndex;
    UINT4               u4ActionFlag = 0;

    /* The value given by user is not used to delete In-profile action
     * entry.Only type of action such as cos,dscp or precedence is used
     * to delete the entry.*/
    UNUSED_PARAM (u4Value);

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map already exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r% Invalid policy-map\r\n");
        return CLI_FAILURE;
    }

    /* Get the In-profile action from the user input type and value */
    switch (u4InProfType)
    {
        case DS_DSCP:
            u4ActionFlag = FS_DS_ACTN_INSERT_DSCP;
            break;
        case DS_PREC:
            u4ActionFlag = FS_DS_ACTN_INSERT_TOSP;
            break;
        case DS_COS:
            u4ActionFlag = FS_DS_ACTN_INSERT_PRIO;
            break;
    }

    /* Delete the in-profile action */
    if (DsDeleteInProfAction (CliHandle, i4PolIndex, u4ActionFlag) ==
        (INT4) CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsDeleteInProfAction                               */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the policy map in-profile    */
/*                        action                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI handle                             */
/*                        i4PolicyIndex - policy-map index                   */
/*                        u4Flag - flag refers to which in-profile action    */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DsDeleteInProfAction (tCliHandle CliHandle, INT4 i4PolicyIndex, UINT4 u4Flag)
{
    INT4                i4RowStatus;
    UINT4               u4Value = 0;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pDiffServClfrEntry = DsGetClfrEntry (i4PolicyIndex);

    if (pDiffServClfrEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Policy map\r\n");
        return CLI_FAILURE;
    }

    /* Get the row status for the policy-map */
    nmhGetFsDiffServClfrStatus (pDiffServClfrEntry->i4DsClfrId, &i4RowStatus);

    /* Check if policy-map has in-profile action associated with it */
    if (pDiffServClfrEntry->i4DsClfrInProActionId != 0)
    {
        /* Check if in-profile action existing is same as given by user
         * If same delete otherwise simply return success */

        if (nmhGetFsDiffServInProfileActionFlag
            (pDiffServClfrEntry->i4DsClfrInProActionId,
             &u4Value) == SNMP_SUCCESS)
        {
            if (u4Flag != u4Value)
            {
                return CLI_SUCCESS;
            }
            else
            {
                /* This operation is required since there is a dependency
                 * between the Classifier entry and the Inprofile action entry*/
                if (i4RowStatus == DS_ACTIVE)
                {
                    if (nmhSetFsDiffServClfrStatus
                        (pDiffServClfrEntry->i4DsClfrId,
                         DS_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }

                /* User given in-profile action and existing one are same.
                 * So delete the in-profile action entry */

                if (nmhSetFsDiffServInProfileActionStatus
                    (pDiffServClfrEntry->i4DsClfrInProActionId,
                     DS_DESTROY) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                pDiffServClfrEntry->i4DsClfrInProActionId = 0;
            }
        }
    }
    else
    {
        return CLI_SUCCESS;
    }

    /* Set the policy-map status to old status if it is ACTIVE after deleting 
     * in-profile action entry. This will add the entry in the hardware also. */

    if (i4RowStatus == DS_ACTIVE)
    {
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, i4RowStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservMapTC                                      */
/*                                                                           */
/*     DESCRIPTION      : This function maps an already created traffic class*/
/*                        to the current policy.                             */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic Class ID                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservMapTC (tCliHandle CliHandle, INT4 i4TrafficClass)
{
    UINT4               u4ErrCode;
    INT4                i4PolIndex;
    INT4                i4Status;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Since there is no dependency between the traffic class and any other
     * field of the Classifier we need NOT set the Classifier entry status
     * to NOT-IN-SERVICE */

    if (nmhTestv2FsDiffServClfrTrafficClassId (&u4ErrCode, i4PolIndex,
                                               i4TrafficClass) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDiffServClfrTrafficClassId (i4PolIndex, i4TrafficClass)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservUnmapTC                                    */
/*                                                                           */
/*     DESCRIPTION      : This function unmaps any Traffic class from the    */
/*                        current policy.                                    */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic Class ID                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservUnmapTC (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;
    INT4                i4PolIndex;
    INT4                i4Status;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Setting a value of 0 unmaps the current traffic class from the policy */

    if (nmhTestv2FsDiffServClfrTrafficClassId (&u4ErrCode, i4PolIndex, 0)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDiffServClfrTrafficClassId (i4PolIndex, 0) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetTrafficClassPrompt                       */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetTrafficClassPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{

    UINT4               u4Len;
    INT4                i4TrafficClassIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-tc" as the prompt 
     * for the mode CLASSMAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-tc");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_TRAFFICCLASS_MODE);
    if (STRNCMP (pi1ModeName, CLI_TRAFFICCLASS_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;
    u4Len = STRLEN (pi1ModeName);

    i4TrafficClassIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_TRAFFICCLASS_ID (i4TrafficClassIndex);

    STRCPY (pi1DispStr, "(config-tc)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCreateTC                                   */
/*                                                                           */
/*     DESCRIPTION      : This function creates a traffic class and maps it  */
/*                        to an output port.                                 */
/*                                                                           */
/*     INPUT            : i4TrafficClass- Traffic class ID                   */
/*                        i4OutPort - Output port to which this traffic class*/
/*                                    is attached.                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservCreateTC (tCliHandle CliHandle, INT4 i4TrafficClass, INT4 i4OutPort)
{
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    UINT4               u4ErrCode;
    INT4                i4RowStatus;
    INT4                i4TCPort = 0;

    if (nmhGetFsDsTCEntryStatus (i4TrafficClass, &i4RowStatus) == SNMP_SUCCESS)
    {
        if ((i4RowStatus == DS_ACTIVE) && (i4OutPort != 0))
        {
            nmhGetFsDsTCPortNum (i4TrafficClass, &i4TCPort);

            if (i4OutPort != i4TCPort)
            {
                CliPrintf (CliHandle,
                           "\r%% Traffic class already exists, cannot change output port\r\n");
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        /* Traffic class does not exist - create newly */
        if (nmhTestv2FsDsTCEntryStatus (&u4ErrCode, i4TrafficClass,
                                        DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCEntryStatus (i4TrafficClass, DS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4OutPort != 0)
        {
            if (i4OutPort > DS_MAX_PORTS)
            {
                CliPrintf (CliHandle, "\r%% Invalid output port\r\n");
                return CLI_FAILURE;
            }

            /* Attach the traffic class to the output port specified */
            if (nmhTestv2FsDsTCPortNum (&u4ErrCode, i4TrafficClass, i4OutPort)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsDsTCPortNum (i4TrafficClass, i4OutPort) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        /* Make the Traffic class entry active */
        if (nmhTestv2FsDsTCEntryStatus (&u4ErrCode, i4TrafficClass,
                                        DS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCEntryStatus (i4TrafficClass, DS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* The execution of this command must return the Traffic Class
     * configuration mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_TRAFFICCLASS_MODE, i4TrafficClass);
    CliChangePath ((CHR1 *) au1IfName);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservDeleteTC                                   */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a traffic class              */
/*                                                                           */
/*     INPUT            : i4TrafficClass- Traffic class ID                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservDeleteTC (tCliHandle CliHandle, INT4 i4TrafficClass)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsDsTCEntryStatus (&u4ErrCode, i4TrafficClass,
                                    DS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCEntryStatus (i4TrafficClass, DS_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetTCMaxBw                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the meter bandwidth for the     */
/*                        traffic class.                                     */
/*                                                                           */
/*     INPUT            : i4MaxBw - Packets for this TC will be dropped if   */
/*                                  their outgoing rate exceeds this value.  */
/*                        i4DropOption - A value of TRUE indicates that all  */
/*                                       three drop thresholds must be set   */
/*                                       to 0 so that packets are dropped    */
/*                                       as soon as their rate exceeds the   */
/*                                       max. bandwidth.                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetTCMaxBw (tCliHandle CliHandle, INT4 i4MaxBw, INT4 i4DropOption)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (i4DropOption == DS_TRUE)
    {
        /* Set all 3 drop levels as 0 so that packets are dropped as soon as
         * the outgoing bandwidth for this Traffic Class exceeds this limit */
        if (nmhTestv2FsDsTCDropAtMaxBandwidth (&u4ErrCode, i4TrafficClass,
                                               DS_SNMP_TRUE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCDropAtMaxBandwidth (i4TrafficClass, DS_SNMP_TRUE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsDsTCMaxBandwidth (&u4ErrCode, i4TrafficClass,
                                     i4MaxBw) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Set the max bandwidth */
    if (nmhSetFsDsTCMaxBandwidth (i4TrafficClass, i4MaxBw) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservResetTCMaxBw                               */
/*                                                                           */
/*     DESCRIPTION      : This function resets the meter bandwidth for the   */
/*                        traffic class to the default value.                */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic class ID                  */
/*                        i4DropOption - TRUE  - Indicates that the drop     */
/*                                               thresholds alone need to be */
/*                                               changed.                    */
/*                                       FALSE - Indicates that both         */
/*                                               Max. bandwidth and the      */
/*                                               drop thresholds need to     */
/*                                               be set.                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservResetTCMaxBw (tCliHandle CliHandle, INT4 i4DropOption)
{
    UINT4               u4ErrCode;
    INT4                i4Port = 0;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (nmhGetFsDsTCPortNum (i4TrafficClass, &i4Port) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4Port != 0)
    {
        /* The DropAtMaxBandwidth configuration can be changed only if the
         * Traffic class is mapped to a port */
        if (nmhTestv2FsDsTCDropAtMaxBandwidth (&u4ErrCode, i4TrafficClass,
                                               DS_SNMP_FALSE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* Setting DropAtMaxBandwidth as FALSE will apply the previously 
         * configured values for Drop levels in the hardware */
        if (nmhSetFsDsTCDropAtMaxBandwidth (i4TrafficClass, DS_SNMP_FALSE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4DropOption == DS_FALSE)
    {
        /* Reset the MaxBandwidth configuration only if the 
         * Drop option has not been specified in the command */
        if (nmhTestv2FsDsTCMaxBandwidth (&u4ErrCode, i4TrafficClass,
                                         DS_DEF_PORT_MAX_BW) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* Set the default max. bandwidth */
        if (nmhSetFsDsTCMaxBandwidth (i4TrafficClass, DS_DEF_PORT_MAX_BW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetTCGuarntdBw                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the guaranteed bandwidth for    */
/*                        the traffic class.                                 */
/*                                                                           */
/*     INPUT            : i4GuarntdBw - Packets for this TC will be guarnteed*/
/*                                      this bandwidth at the output port    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetTCGuarntdBw (tCliHandle CliHandle, INT4 i4GuarntdBw)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (nmhTestv2FsDsTCGuaranteedBandwidth (&u4ErrCode, i4TrafficClass,
                                            i4GuarntdBw) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCGuaranteedBandwidth (i4TrafficClass, i4GuarntdBw) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservResetTCGuarntdBw                           */
/*                                                                           */
/*     DESCRIPTION      : This function resets the guaranteed bandwidth for  */
/*                        the traffic class to the default value.            */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic class ID                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservResetTCGuarntdBw (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (nmhTestv2FsDsTCGuaranteedBandwidth (&u4ErrCode, i4TrafficClass, 0) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCGuaranteedBandwidth (i4TrafficClass, 0) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetTCDropLevels                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the three drop levels for the   */
/*                        traffic class.                                     */
/*                                                                           */
/*     INPUT            : i4DropLevel1, i4DropLevel2, i4DropLevel3           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetTCDropLevels (tCliHandle CliHandle, INT4 i4DropLevel1,
                         INT4 i4DropLevel2, INT4 i4DropLevel3)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (i4DropLevel1 != 0)
    {
        if (nmhTestv2FsDsTCDropLevel1 (&u4ErrCode, i4TrafficClass,
                                       i4DropLevel1) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCDropLevel1 (i4TrafficClass, i4DropLevel1) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4DropLevel2 != 0)
    {
        if (nmhTestv2FsDsTCDropLevel2 (&u4ErrCode, i4TrafficClass,
                                       i4DropLevel2) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCDropLevel2 (i4TrafficClass, i4DropLevel2) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4DropLevel3 != 0)
    {
        if (nmhTestv2FsDsTCDropLevel3 (&u4ErrCode, i4TrafficClass,
                                       i4DropLevel3) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCDropLevel3 (i4TrafficClass, i4DropLevel3) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservResetTCDropLevels                          */
/*                                                                           */
/*     DESCRIPTION      : This function resets the all three drop levels for */
/*                        the traffic class to their default values.         */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic class ID                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservResetTCDropLevels (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (nmhTestv2FsDsTCDropLevel1 (&u4ErrCode, i4TrafficClass,
                                   DS_MIN_DROP_LEVEL1) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCDropLevel1 (i4TrafficClass, DS_MIN_DROP_LEVEL1) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsTCDropLevel2 (&u4ErrCode, i4TrafficClass,
                                   DS_MIN_DROP_LEVEL2) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCDropLevel2 (i4TrafficClass, DS_MIN_DROP_LEVEL2) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsTCDropLevel3 (&u4ErrCode, i4TrafficClass,
                                   DS_MIN_DROP_LEVEL3) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCDropLevel3 (i4TrafficClass, DS_MIN_DROP_LEVEL3) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetTCREDMap                                */
/*                                                                           */
/*     DESCRIPTION      : This function associates a RED curve with the      */
/*                        the traffic class.                                 */
/*                                                                           */
/*     INPUT            : i4REDCurveId                                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetTCREDMap (tCliHandle CliHandle, INT4 i4REDCurveId)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (nmhTestv2FsDsTCREDCurveId (&u4ErrCode, i4TrafficClass,
                                   i4REDCurveId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCREDCurveId (i4TrafficClass, i4REDCurveId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetTCNumFlowGroups                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the number of flow groups used  */
/*                        by this traffic class.                             */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic class ID                  */
/*                        u4NumFlowGroups                                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetTCNumFlowGroups (tCliHandle CliHandle, INT4 i4NumFlowGroups)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (nmhTestv2FsDsTCNumFlowGroups (&u4ErrCode, i4TrafficClass,
                                      i4NumFlowGroups) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsTCNumFlowGroups (i4TrafficClass, i4NumFlowGroups) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetTCWeight                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the traffic class weight        */
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic class ID                  */
/*                        i4Value - Value for WeightFactor or WeightShift    */
/*                        i4Select - Indicates whether WeightFactor or       */
/*                                   WeightShift was configured.             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetTCWeight (tCliHandle CliHandle, INT4 i4Value, INT4 i4Select)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (i4Select == DS_WEIGHT_FACTOR)
    {
        if (nmhTestv2FsDsTCWeightFactor (&u4ErrCode, i4TrafficClass,
                                         i4Value) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCWeightFactor (i4TrafficClass, i4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4Select == DS_WEIGHT_SHIFT)
    {
        if (nmhTestv2FsDsTCWeightShift (&u4ErrCode, i4TrafficClass,
                                        i4Value) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCWeightShift (i4TrafficClass, i4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservResetTCWeight                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the default traffic class weight*/
/*                                                                           */
/*     INPUT            : i4TrafficClass - Traffic class ID                  */
/*                        i4Select - Indicates whether WeightFactor or       */
/*                                   WeightShift was configured.             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservResetTCWeight (tCliHandle CliHandle, INT4 i4Select)
{
    UINT4               u4ErrCode;
    INT4                i4TrafficClass;

    /* Get the current Traffic Class identifier */
    i4TrafficClass = CLI_GET_TRAFFICCLASS_ID ();

    if (i4Select == DS_WEIGHT_FACTOR)
    {
        if (nmhTestv2FsDsTCWeightFactor (&u4ErrCode, i4TrafficClass, 0) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCWeightFactor (i4TrafficClass, 0) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4Select == DS_WEIGHT_SHIFT)
    {
        if (nmhTestv2FsDsTCWeightShift (&u4ErrCode, i4TrafficClass, 0) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsTCWeightShift (i4TrafficClass, 0) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetREDParam                                */
/*                                                                           */
/*     DESCRIPTION      : This function initialises a RED curve's parameters.*/
/*                        It creates the RED curve if not already created.   */
/*                                                                           */
/*     INPUT            : i4REDCurveId - RED curve ID                        */
/*                        i4REDStart, i4REDStop, i4REDQRange,                */
/*                        i4REDStopProbability                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetREDParam (tCliHandle CliHandle, INT4 i4REDCurveId,
                     INT4 i4REDStart, INT4 i4REDStop,
                     INT4 i4REDQRange, INT4 i4REDStopProbability)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    if (nmhGetFsDsREDCurveStatus (i4REDCurveId, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* RED curve exists. Put it into not-in-service */
        if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode, i4REDCurveId,
                                         DS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsREDCurveStatus (i4REDCurveId, DS_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode, i4REDCurveId,
                                         DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsDsREDCurveStatus (i4REDCurveId, DS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsDsREDCurveStart (&u4ErrCode, i4REDCurveId, i4REDStart)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsREDCurveStop (&u4ErrCode, i4REDCurveId, i4REDStop)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsREDCurveQRange (&u4ErrCode, i4REDCurveId, i4REDQRange)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsREDCurveStopProbability (&u4ErrCode, i4REDCurveId,
                                              i4REDStopProbability)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDsREDCurveStart (i4REDCurveId, i4REDStart) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDsREDCurveStop (i4REDCurveId, i4REDStop) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDsREDCurveQRange (i4REDCurveId, i4REDQRange) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDsREDCurveStopProbability (i4REDCurveId, i4REDStopProbability)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Make the RED entry active */
    if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode, i4REDCurveId, DS_ACTIVE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsREDCurveStatus (i4REDCurveId, DS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservDeleteREDCurve                             */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a RED curve                  */
/*                                                                           */
/*     INPUT            : i4REDCurveId - RED curve ID                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservDeleteREDCurve (tCliHandle CliHandle, INT4 i4REDCurveId)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    if (nmhGetFsDsREDCurveStatus (i4REDCurveId, &i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid RED curve\r\n");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode, i4REDCurveId,
                                     DS_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDsREDCurveStatus (i4REDCurveId, DS_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetDSCPQueueMap                            */
/*                                                                           */
/*     DESCRIPTION      : This function maps the DSCP value to a Queue ID.   */
/*                                                                           */
/*     INPUT            : i4REDCurveId - RED curve ID                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetDSCPQueueMap (tCliHandle CliHandle, UINT4 u4DSCP, UINT4 u4Queue)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsDsQPriority (&u4ErrCode, u4DSCP, u4Queue) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDsQPriority (u4DSCP, u4Queue) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetQueuePrecSwitched                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the COS and DSCP queue          */
/*                        precedence for switched packets.                   */
/*                                                                           */
/*     INPUT            : i4REDCurveId - RED curve ID                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetQueuePrecSwitched (tCliHandle CliHandle, UINT4 u4DSCPPrec,
                              UINT4 u4COSPrec)
{
    UINT4               u4ErrCode;

    if (u4DSCPPrec != 0)
    {
        if (nmhTestv2FsDsIPTOSQPrecedenceSwitched (&u4ErrCode, u4DSCPPrec) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsDsIPTOSQPrecedenceSwitched (u4DSCPPrec) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (u4COSPrec != 0)
    {
        if (nmhTestv2FsDsDot1PQPrecedenceSwitched (&u4ErrCode, u4COSPrec) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsDsDot1PQPrecedenceSwitched (u4COSPrec) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetQueuePrecRouted                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the COS and DSCP queue          */
/*                        precedence for Routed  packets.                    */
/*                                                                           */
/*     INPUT            : i4REDCurveId - RED curve ID                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetQueuePrecRouted (tCliHandle CliHandle, UINT4 u4DSCPPrec,
                            UINT4 u4COSPrec)
{
    UINT4               u4ErrCode;

    if (u4DSCPPrec != 0)
    {
        if (nmhTestv2FsDsIPTOSQPrecedenceRouted (&u4ErrCode, u4DSCPPrec) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsDsIPTOSQPrecedenceRouted (u4DSCPPrec) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (u4COSPrec != 0)
    {
        if (nmhTestv2FsDsDot1PQPrecedenceRouted (&u4ErrCode, u4COSPrec) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsDsDot1PQPrecedenceRouted (u4COSPrec) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetPortMaxBw                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the meter bandwidth for the     */
/*                        traffic class.                                     */
/*                                                                           */
/*     INPUT            : i4MaxBw - Packets for this Port will be dropped if */
/*                                  their outgoing rate exceeds this value.  */
/*                        i4DropOption - A value of TRUE indicates that all  */
/*                                       three drop thresholds must be set   */
/*                                       to 0 so that packets are dropped    */
/*                                       as soon as their rate exceeds the   */
/*                                       max. bandwidth.                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetPortMaxBw (tCliHandle CliHandle, INT4 i4MaxBw, INT4 i4DropOption)
{
    UINT4               u4ErrCode;
    INT4                i4PortNum;

    /* Get the current Port number */
    i4PortNum = CLI_GET_IFINDEX ();

    if (i4DropOption == DS_TRUE)
    {
        if (nmhTestv2FsDsPortDropAtMaxBandwidth (&u4ErrCode, i4PortNum,
                                                 DS_SNMP_TRUE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* Set all 3 drop levels as 0 so that packets are dropped as soon as
         * the outgoing bandwidth for this Traffic Class exceeds this limit */
        if (nmhSetFsDsPortDropAtMaxBandwidth (i4PortNum, DS_SNMP_TRUE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsDsPortMaxBandwidth (&u4ErrCode, i4PortNum, i4MaxBw) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the max bandwidth */
    if (nmhSetFsDsPortMaxBandwidth (i4PortNum, i4MaxBw) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservResetPortMaxBw                             */
/*                                                                           */
/*     DESCRIPTION      : This function resets the meter bandwidth for the   */
/*                        traffic class to the default value.                */
/*                                                                           */
/*     INPUT            : i4PortNum - Port number                            */
/*                        i4DropOption - TRUE  - Indicates that the drop     */
/*                                               thresholds alone need to be */
/*                                               changed.                    */
/*                                       FALSE - Indicates that both         */
/*                                               Max. bandwidth and the      */
/*                                               drop thresholds need to     */
/*                                               be set.                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservResetPortMaxBw (tCliHandle CliHandle, INT4 i4DropOption)
{
    UINT4               u4ErrCode;
    INT4                i4PortNum;

    /* Get the current Port number */
    i4PortNum = CLI_GET_IFINDEX ();

    if (nmhTestv2FsDsPortMaxBandwidth (&u4ErrCode, i4PortNum,
                                       DS_DEF_PORT_MAX_BW) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Setting DropAtMaxBandwidth as FALSE will apply the previously 
     * configured values for Drop levels in the hardware */
    if (nmhSetFsDsPortDropAtMaxBandwidth (i4PortNum, DS_SNMP_FALSE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4DropOption == DS_FALSE)
    {
        if (nmhTestv2FsDsPortDropAtMaxBandwidth (&u4ErrCode, i4PortNum,
                                                 DS_SNMP_FALSE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /* Reset the max bandwidth */
        if (nmhSetFsDsPortMaxBandwidth (i4PortNum, DS_DEF_PORT_MAX_BW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetPortDropLevels                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the three drop levels for the   */
/*                        traffic class.                                     */
/*                                                                           */
/*     INPUT            : i4DropLevel1, i4DropLevel2, i4DropLevel3           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetPortDropLevels (tCliHandle CliHandle, INT4 i4DropLevel1,
                           INT4 i4DropLevel2, INT4 i4DropLevel3)
{
    UINT4               u4ErrCode;
    INT4                i4PortNum;

    /* Get the current Port number */
    i4PortNum = CLI_GET_IFINDEX ();

    if (i4DropLevel1 != 0)
    {
        if (nmhTestv2FsDsPortDropLevel1 (&u4ErrCode, i4PortNum,
                                         i4DropLevel1) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsPortDropLevel1 (i4PortNum, i4DropLevel1) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4DropLevel2 != 0)
    {
        if (nmhTestv2FsDsPortDropLevel2 (&u4ErrCode, i4PortNum,
                                         i4DropLevel2) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsPortDropLevel2 (i4PortNum, i4DropLevel2) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4DropLevel3 != 0)
    {
        if (nmhTestv2FsDsPortDropLevel3 (&u4ErrCode, i4PortNum,
                                         i4DropLevel3) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDsPortDropLevel3 (i4PortNum, i4DropLevel3) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservResetPortDropLevels                        */
/*                                                                           */
/*     DESCRIPTION      : This function resets the all three drop levels for */
/*                        the traffic class to their default values.         */
/*                                                                           */
/*     INPUT            : i4PortNum - Traffic class ID                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservResetPortDropLevels (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;
    INT4                i4PortNum;

    /* Get the current Port number */
    i4PortNum = CLI_GET_IFINDEX ();

    if (nmhTestv2FsDsPortDropLevel1 (&u4ErrCode, i4PortNum,
                                     DS_MIN_DROP_LEVEL1) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsPortDropLevel1 (i4PortNum, DS_MIN_DROP_LEVEL1) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsPortDropLevel2 (&u4ErrCode, i4PortNum,
                                     DS_MIN_DROP_LEVEL2) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsPortDropLevel2 (i4PortNum, DS_MIN_DROP_LEVEL2) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDsPortDropLevel3 (&u4ErrCode, i4PortNum,
                                     DS_MIN_DROP_LEVEL3) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsPortDropLevel3 (i4PortNum, DS_MIN_DROP_LEVEL3) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservSetPortREDMap                              */
/*                                                                           */
/*     DESCRIPTION      : This function associates a RED curve with the      */
/*                        the traffic class.                                 */
/*                                                                           */
/*     INPUT            : i4REDCurveId                                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservSetPortREDMap (tCliHandle CliHandle, INT4 i4REDCurveId)
{
    UINT4               u4ErrCode;
    INT4                i4PortNum;

    /* Get the current Port number */
    i4PortNum = CLI_GET_IFINDEX ();

    if (nmhTestv2FsDsPortREDCurveId (&u4ErrCode, i4PortNum,
                                     i4REDCurveId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsPortREDCurveId (i4PortNum, i4REDCurveId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservNoPolicyClass                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the Policy-class map relation*/
/*                                                                           */
/*     INPUT            :  i4ClassMap Id- Class map                          */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservNoPolicyClass (tCliHandle CliHandle, INT4 i4ClassMap)
{
    INT4                i4PolIndex;

    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POLICYMAP_ID ();

    pDiffServClfrEntry = DsGetClfrEntry (i4PolIndex);

    if (pDiffServClfrEntry->i4DsClfrMFClfrId == i4ClassMap)
    {
        /*Policy map <-> class map match found */

        /*Set the policy map status to NOT_IN_SERVICE  and delete all the 
         * configured parameters for the policy map */

        if (pDiffServClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDiffServClfrEntry->i4DsClfrId,
                 DS_NOT_IN_SERVICE) == DS_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }

        /* Delete the In-profile action */
        if (pDiffServClfrEntry->i4DsClfrInProActionId != 0)
        {
            if (nmhSetFsDiffServInProfileActionStatus
                (pDiffServClfrEntry->i4DsClfrInProActionId,
                 DS_DESTROY) == DS_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }

        /*Destroy the policy map */
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, DS_DESTROY) == DS_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        /*Create the policy map again */
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, DS_CREATE_AND_WAIT) == DS_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%% This class map is not associated with this policy map \r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowPolicyMap                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Policy-class details    */
/*                                                                           */
/*     INPUT            :  i4ClassMapId- Class map                           */
/*                          i4PolicyMapId -Policy Map                        */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowPolicyMap (tCliHandle CliHandle, INT4 i4PolicyMapId,
                       INT4 i4ClassMapId)
{
    INT4                i4ClfrStatus = 0;
    INT4                i4DsStatus = 0;
    UINT4               u4InProfFlag = 0;
    UINT4               u4InNewPrio = 0;
    UINT4               u4InIpTOs = 0;
    UINT4               u4InActDscp = 0;
    INT4                i4ClfrId = 0;
    tDsClfr             DsClfr;
    INT4                i4NextClfrId = 0;
    INT4                i4MFClfrId = 0;
    UINT1               u1ShowAll = DS_FALSE;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               u1Check = DS_TRUE;
    INT4                i4PortNum = 0;
    INT4                i4MaxBandwidth = 0;
    INT4                i4GuarntdBandwidth = 0;
    INT4                i4DropAtMaxBw = DS_SNMP_FALSE;
    INT4                i4DropLevel1 = 0;
    INT4                i4DropLevel2 = 0;
    INT4                i4DropLevel3 = 0;
    INT4                i4WeightShift = 0;
    INT4                i4WeightFactor = 0;
    INT4                i4NumFlowGroups = 0;
    INT4                i4REDCurveId = 0;
    INT4                i4TCId = 0;

    if (i4PolicyMapId != 0)
    {
        /* Details of a specific policy map need to be displayed */
        i4ClfrId = i4PolicyMapId;
        if (nmhGetFsDiffServClfrStatus (i4ClfrId, &i4ClfrStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid policy map \r\n");

            return (CLI_FAILURE);
        }

        /* Get the Clfr and MCClfrId from it */
        nmhGetFsDiffServClfrMFClfrId (i4PolicyMapId, &i4MFClfrId);

        if (i4ClassMapId != 0)
        {
            /* Details of a specific class map need to be displayed */
            if (i4ClassMapId != i4MFClfrId)
            {
                /* Invalid combination for policy map and class map */
                CliPrintf (CliHandle, "%% Invalid Class Map \r\n");

                return (CLI_FAILURE);
            }
        }

        i4NextClfrId = i4PolicyMapId;
    }
    else
    {
        /* 
         * This flag is used to indicate that all policy 
         * maps must be displayed 
         */
        u1ShowAll = DS_TRUE;
    }

    CliPrintf (CliHandle, "\r\nDiffServ Configurations: \r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    if (u1ShowAll == DS_TRUE)
    {
        if (nmhGetFirstIndexFsDiffServClfrTable (&i4NextClfrId) == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4ClfrId = i4NextClfrId;

        if (nmhGetFsDiffServClfrStatus (i4ClfrId, &i4ClfrStatus)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid  policy map specified\r\n");

            return (CLI_FAILURE);
        }

        /* Get the Clfr and MCClfrId from it */
        nmhGetFsDiffServClfrMFClfrId (i4ClfrId, &i4MFClfrId);

        if (u1Check == DS_TRUE)
        {
            nmhGetFsDsStatus (&i4DsStatus);
        }
        if (nmhGetFsDiffServClfrInProActionId (i4ClfrId, &DsClfr.i4DsInProActId)
            != SNMP_FAILURE)
        {
            if (DsClfr.i4DsInProActId != 0)
            {
                if (nmhGetFsDiffServInProfileActionFlag
                    (DsClfr.i4DsInProActId, &u4InProfFlag) == SNMP_SUCCESS)
                {
                    if (u4InProfFlag & FS_DS_ACTN_INSERT_PRIO)
                    {
                        nmhGetFsDiffServInProfileActionNewPrio
                            (DsClfr.i4DsInProActId, &u4InNewPrio);
                    }
                    else if (u4InProfFlag & FS_DS_ACTN_INSERT_TOSP)
                    {
                        nmhGetFsDiffServInProfileActionIpTOS
                            (DsClfr.i4DsInProActId, &u4InIpTOs);
                    }
                    else if (u4InProfFlag & FS_DS_ACTN_INSERT_DSCP)
                    {
                        nmhGetFsDiffServInProfileActionDscp
                            (DsClfr.i4DsInProActId, &u4InActDscp);
                    }
                }
            }
        }

        if (nmhGetFsDiffServClfrTrafficClassId
            (i4ClfrId, &DsClfr.i4DsTrafficClassId) != SNMP_FAILURE)
        {
            if (DsClfr.i4DsTrafficClassId != 0)
            {
                i4TCId = DsClfr.i4DsTrafficClassId;

                nmhGetFsDsTCPortNum (i4TCId, &i4PortNum);
                nmhGetFsDsTCMaxBandwidth (i4TCId, &i4MaxBandwidth);
                nmhGetFsDsTCGuaranteedBandwidth (i4TCId, &i4GuarntdBandwidth);
                nmhGetFsDsTCDropAtMaxBandwidth (i4TCId, &i4DropAtMaxBw);
                if (i4DropAtMaxBw == DS_SNMP_FALSE)
                {
                    nmhGetFsDsTCDropLevel1 (i4TCId, &i4DropLevel1);
                    nmhGetFsDsTCDropLevel2 (i4TCId, &i4DropLevel2);
                    nmhGetFsDsTCDropLevel3 (i4TCId, &i4DropLevel3);
                }
                nmhGetFsDsTCWeightFactor (i4TCId, &i4WeightFactor);
                nmhGetFsDsTCWeightShift (i4TCId, &i4WeightShift);
                nmhGetFsDsTCNumFlowGroups (i4TCId, &i4NumFlowGroups);
                nmhGetFsDsTCREDCurveId (i4TCId, &i4REDCurveId);
            }
        }
        if (u1Check == DS_TRUE)
        {
            if (i4DsStatus == DS_ENABLE)
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been disabled \r\n");
            }

            u1Check = DS_FALSE;
        }

        CliPrintf (CliHandle, "\r\nPolicy Map %d ", i4ClfrId);

        if (i4ClfrStatus == DS_ACTIVE)
        {
            CliPrintf (CliHandle, "is active\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "is not active\r\n");
        }

        CliPrintf (CliHandle, "\r\nClass Map: %d\r\n", i4MFClfrId);
        CliPrintf (CliHandle, "-------------\r\n");

        if (DsClfr.i4DsInProActId != 0)
        {
/*            CliPrintf (CliHandle, "\r\nIn Profile Entry \r\n");
            CliPrintf (CliHandle, "----------------\r\n");*/

            CliPrintf (CliHandle, "%-28s: ", "Action");

            if (u4InProfFlag & FS_DS_ACTN_DO_NOT_SWITCH)
            {
                CliPrintf (CliHandle, "drop\r\n");
            }
            else if (u4InProfFlag & FS_DS_ACTN_INSERT_PRIO)
            {
                CliPrintf (CliHandle, "cos %d\r\n", u4InNewPrio);
            }
            else if (u4InProfFlag & FS_DS_ACTN_INSERT_TOSP)
            {
                CliPrintf (CliHandle, "precedence %d\r\n", u4InIpTOs);
            }
            else if (u4InProfFlag & FS_DS_ACTN_INSERT_DSCP)
            {
                CliPrintf (CliHandle, "dscp %d\r\n", u4InActDscp);
            }
            else
            {
                CliPrintf (CliHandle, "none\r\n");
            }
        }

        CliPrintf (CliHandle, "%-28s: %d\r\n", "Traffic class", i4TCId);

        i4Quit = CliPrintf (CliHandle, "\r\n");

        if (u1ShowAll == DS_FALSE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDiffServClfrTable (i4ClfrId, &i4NextClfrId)
            != SNMP_FAILURE) && (i4Quit == CLI_SUCCESS));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowClassMap                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Policy-class details    */
/*                                                                           */
/*     INPUT            :  u4ClassIndex- Class map                           */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservShowClassMap (tCliHandle CliHandle, UINT4 u4ClassIndex)
{
    UINT1               u1Flag = DS_TRUE;
    INT4                i4NextIndex = u4ClassIndex;
    INT4                i4PrevIndex;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1              *pu1Temp = NULL;
    CHR1               *pu1String = NULL;
    UINT4               u4FilterId = 0;
    INT4                i4FilterType = 0;
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    INT4                i4RetVal;

    pu1String = (CHR1 *) & au1IfName[0];
    pu1Temp = &au1IfName[0];

    if (u4ClassIndex == 0)
    {
        /* Details of all class maps must be displayed . u1Flag is reset to
         * indicate the same */

        u1Flag = DS_FALSE;
        if (nmhGetFirstIndexFsDiffServMultiFieldClfrTable (&i4NextIndex)
            == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        if (nmhGetFsDiffServMultiFieldClfrStatus (u4ClassIndex, &i4RetVal) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Class map \r\n");

            return (CLI_FAILURE);
        }
    }

    do
    {
        MEMSET (au1IfName, 0, DS_MAX_NAME_LENGTH);

        if (nmhGetFsDiffServMultiFieldClfrFilterId (i4NextIndex,
                                                    &u4FilterId) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhGetFsDiffServMultiFieldClfrFilterType (i4NextIndex,
                                                      &i4FilterType) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        nmhGetFsDiffServMultiFieldClfrStatus (i4NextIndex, &i4RetVal);

        CliPrintf (CliHandle, "\r\nDiffServ Configurations: \r\n");
        CliPrintf (CliHandle, "------------------------\r\n");

        CliPrintf (CliHandle, "\r\nClass map %d\r\n", i4NextIndex);
        CliPrintf (CliHandle, "--------------\r\n");

        if (i4RetVal == DS_ACTIVE)
        {
            CliPrintf (CliHandle, "%-28s: %d\r\n", "Filter ID", u4FilterId);
            CliPrintf (CliHandle, "%-28s: ", "Filter Type");
            if (i4FilterType == DS_MAC_FILTER)
            {
                CliPrintf (CliHandle, "MAC-FILTER\r\n");
            }
            else if (i4FilterType == DS_IP_FILTER)
            {
                CliPrintf (CliHandle, "IP-FILTER\r\n");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }

        i4Quit = CliPrintf (CliHandle, "\r\n");

        i4PrevIndex = i4NextIndex;

        /* break from the loop if class map index is specified */
        if (u1Flag == DS_TRUE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDiffServMultiFieldClfrTable
            (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS)
           && (i4Quit == CLI_SUCCESS));

    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowDscpQueueMap                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the mapping between the     */
/*                        DSCP value and the output Queue ID                 */
/*                                                                           */
/*     INPUT            : CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowDscpQueueMap (tCliHandle CliHandle)
{
    UINT4               au4DSCPQueueMap[64];
    UINT4               u4DSCP, u4Row, u4Col;

    MEMSET (au4DSCPQueueMap, 0, (sizeof (UINT4) * 64));

    for (u4DSCP = 0; u4DSCP < 64; u4DSCP++)
    {
        nmhGetFsDsQPriority (u4DSCP, &au4DSCPQueueMap[u4DSCP]);
    }

    CliPrintf (CliHandle, "\r\nDiffServ DSCP-Queue map \r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    CliPrintf (CliHandle, "\r\n   d1 : d2  0  1  2  3  4  5  6  7  8  9\r\n");
    for (u4Row = 0; u4Row < 7; u4Row++)
    {
        CliPrintf (CliHandle, "    %d :    ", u4Row);
        if (u4Row < 6)
        {
            for (u4Col = 0; u4Col < 10; u4Col++)
            {
                CliPrintf (CliHandle, "%02d ",
                           au4DSCPQueueMap[u4Row * 10 + u4Col]);
            }
        }
        else
        {
            for (u4Col = 0; u4Col < 4; u4Col++)
            {
                CliPrintf (CliHandle, "%02d ",
                           au4DSCPQueueMap[u4Row * 10 + u4Col]);
            }
        }
        CliPrintf (CliHandle, "\r\n");
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowQueuePrecedence                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the queue precedence of     */
/*                        DSCP and the COS for routed and switched packets   */
/*                                                                           */
/*     INPUT            : CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowQueuePrecedence (tCliHandle CliHandle)
{
    INT4                i4Dot1pQPrecSwitched = 0;
    INT4                i4Dot1pQPrecRouted = 0;
    INT4                i4IPTOSQPrecSwitched = 0;
    INT4                i4IPTOSQPrecRouted = 0;

    nmhGetFsDsDot1PQPrecedenceSwitched (&i4Dot1pQPrecSwitched);
    nmhGetFsDsDot1PQPrecedenceRouted (&i4Dot1pQPrecRouted);
    nmhGetFsDsIPTOSQPrecedenceSwitched (&i4IPTOSQPrecSwitched);
    nmhGetFsDsIPTOSQPrecedenceRouted (&i4IPTOSQPrecRouted);

    CliPrintf (CliHandle, "\r\nQueue Precedence \r\n");
    CliPrintf (CliHandle, "---------------- \r\n");
    CliPrintf (CliHandle, " Switched:\r\n");
    CliPrintf (CliHandle, "COS Precedence :   %d\r\n", i4Dot1pQPrecSwitched);
    CliPrintf (CliHandle, "DSCP Precedence :  %d\r\n", i4IPTOSQPrecSwitched);
    CliPrintf (CliHandle, "\r\n Routed:\r\n");
    CliPrintf (CliHandle, "COS Precedence :   %d\r\n", i4Dot1pQPrecRouted);
    CliPrintf (CliHandle, "DSCP Precedence :  %d\r\n", i4IPTOSQPrecRouted);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowTCFlowGroupsMap                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the range of flow groups    */
/*                        mapped to each Traffic class.                      */
/*                                                                           */
/*     INPUT            : CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowTCFlowGroupsMap (tCliHandle CliHandle)
{
    INT4                i4FirstFlowGroup = 0;
    INT4                i4NumFlowGroups = 0;
    INT4                i4TCId = 0;
    INT4                i4NextTCId = 0;
    INT4                i4Quit = CLI_SUCCESS;

    CliPrintf (CliHandle, "\r\nDiffServ TrafficClass-FairQueue mapping: \r\n");
    CliPrintf (CliHandle, "-----------------------------------------\r\n");

    if (nmhGetFirstIndexFsDsTrafficClassConfigTable (&i4NextTCId) ==
        SNMP_FAILURE)
    {
        return (CLI_SUCCESS);
    }

    do
    {
        i4TCId = i4NextTCId;

        nmhGetFsDsTCNumFlowGroups (i4TCId, &i4NumFlowGroups);

        if (i4NumFlowGroups == 0)
        {
            continue;
        }

        nmhGetFsDsTCFirstFlowGroup (i4TCId, &i4FirstFlowGroup);

        CliPrintf (CliHandle, "Traffic-class %d: %d-%d\r\n",
                   i4TCId, i4FirstFlowGroup,
                   (i4FirstFlowGroup + i4NumFlowGroups - 1));

        i4Quit = CliPrintf (CliHandle, "\r\n");
    }
    while ((nmhGetNextIndexFsDsTrafficClassConfigTable (i4TCId, &i4NextTCId)
            != SNMP_FAILURE) && (i4Quit == CLI_SUCCESS));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowTrafficClass                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the TrafficClass details    */
/*                                                                           */
/*     INPUT            : i4TrafficClassId                                   */
/*                        CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowTrafficClass (tCliHandle CliHandle, INT4 i4TrafficClassId)
{
    INT4                i4TCStatus = 0;
    INT4                i4DsStatus = 0;
    INT4                i4PortNum = 0;
    INT4                i4MaxBandwidth = 0;
    INT4                i4GuarntdBandwidth = 0;
    INT4                i4DropAtMaxBw = DS_SNMP_FALSE;
    INT4                i4DropLevel1 = 0;
    INT4                i4DropLevel2 = 0;
    INT4                i4DropLevel3 = 0;
    INT4                i4WeightShift = 0;
    UINT4               u4NumBytesAccLow = 0;
    UINT4               u4NumBytesAccHigh = 0;
    UINT4               u4NumBytesDiscLow = 0;
    UINT4               u4NumBytesDiscHigh = 0;
    INT4                i4WeightFactor = 0;
    INT4                i4FirstFlowGroup = 0;
    INT4                i4NumFlowGroups = 0;
    INT4                i4REDCurveId = 0;
    INT4                i4TCId = 0;
    INT4                i4NextTCId = 0;
    UINT1               u1ShowAll = DS_FALSE;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               u1Check = DS_TRUE;
    tSNMP_COUNTER64_TYPE AccBytesCount;
    tSNMP_COUNTER64_TYPE DiscBytesCount;

    if (i4TrafficClassId != 0)
    {
        /* Details of a specific TrafficClass need to be displayed */
        if (nmhGetFsDsTCEntryStatus (i4TrafficClassId, &i4TCStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid traffic class\r\n");

            return (CLI_FAILURE);
        }

        i4NextTCId = i4TrafficClassId;
    }
    else
    {
        /* 
         * This flag is used to indicate that all policy 
         * maps must be displayed 
         */
        u1ShowAll = DS_TRUE;
    }

    CliPrintf (CliHandle, "\r\nDiffServ Traffic class Configurations: \r\n");
    CliPrintf (CliHandle, "---------------------------------------\r\n");

    if (u1ShowAll == DS_TRUE)
    {
        if (nmhGetFirstIndexFsDsTrafficClassConfigTable (&i4NextTCId) ==
            SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4TCId = i4NextTCId;

        i4DropLevel1 = 0;
        i4DropLevel2 = 0;
        i4DropLevel3 = 0;

        if (nmhGetFsDsTCEntryStatus (i4TCId, &i4TCStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid  traffic class specified\r\n");

            return (CLI_FAILURE);
        }

        nmhGetFsDsTCPortNum (i4TCId, &i4PortNum);
        nmhGetFsDsTCMaxBandwidth (i4TCId, &i4MaxBandwidth);
        nmhGetFsDsTCGuaranteedBandwidth (i4TCId, &i4GuarntdBandwidth);
        nmhGetFsDsTCDropAtMaxBandwidth (i4TCId, &i4DropAtMaxBw);
        if (i4DropAtMaxBw == DS_SNMP_FALSE)
        {
            nmhGetFsDsTCDropLevel1 (i4TCId, &i4DropLevel1);
            nmhGetFsDsTCDropLevel2 (i4TCId, &i4DropLevel2);
            nmhGetFsDsTCDropLevel3 (i4TCId, &i4DropLevel3);
        }
        nmhGetFsDsTCWeightFactor (i4TCId, &i4WeightFactor);
        nmhGetFsDsTCWeightShift (i4TCId, &i4WeightShift);
        nmhGetFsDsTCFirstFlowGroup (i4TCId, &i4FirstFlowGroup);
        nmhGetFsDsTCNumFlowGroups (i4TCId, &i4NumFlowGroups);
        nmhGetFsDsTCREDCurveId (i4TCId, &i4REDCurveId);
        nmhGetFsDsTCAccBytesCount (i4TCId, &AccBytesCount);
        nmhGetFsDsTCDiscBytesCount (i4TCId, &DiscBytesCount);

        u4NumBytesAccLow = AccBytesCount.lsn;
        u4NumBytesAccHigh = AccBytesCount.msn;

        u4NumBytesDiscLow = DiscBytesCount.lsn;
        u4NumBytesDiscHigh = DiscBytesCount.msn;

        if (u1Check == DS_TRUE)
        {
            nmhGetFsDsStatus (&i4DsStatus);
        }

        if (u1Check == DS_TRUE)
        {
            if (i4DsStatus == DS_ENABLE)
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been enabled \r\n\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been disabled \r\n\r\n");
            }

            u1Check = DS_FALSE;
        }

        CliPrintf (CliHandle, " Traffic class %d ", i4TCId);

        if (i4TCStatus == DS_ACTIVE)
        {
            CliPrintf (CliHandle, "is active\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "is not active\r\n");
        }

        CliPrintf (CliHandle, "%-28s: %d\r\n", "Output port", i4PortNum);
        CliPrintf (CliHandle, "%-28s: %d (in multiples of 64000 bps)\r\n",
                   "Maximum bandwidth", i4MaxBandwidth);
        CliPrintf (CliHandle, "%-28s: %d (in multiples of 64000 bps)\r\n",
                   "Guaranteed bandwidth", i4GuarntdBandwidth);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "Drop Threshold1", i4DropLevel1);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "Drop Threshold2", i4DropLevel2);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "Drop Threshold3", i4DropLevel3);
        CliPrintf (CliHandle, "%-28s: %d\r\n", "Weight factor", i4WeightFactor);
        CliPrintf (CliHandle, "%-28s: %d\r\n", "Weight shift", i4WeightShift);
        CliPrintf (CliHandle, "%-28s: %d\r\n", "First fair-queue assigned",
                   i4FirstFlowGroup);
        CliPrintf (CliHandle, "%-28s: %d\r\n", "Number of fair-queues used",
                   i4NumFlowGroups);
        CliPrintf (CliHandle, "%-28s: %d\r\n", "RED Curve ID", i4REDCurveId);

        if (u4NumBytesAccHigh == 0)
        {
            CliPrintf (CliHandle, "%-28s: %d\r\n",
                       "Number of bytes accepted", u4NumBytesAccLow);
        }
        else
        {
            CliPrintf (CliHandle, "%-28s: High: %d Low: %d\r\n",
                       "Number of bytes accepted", u4NumBytesAccHigh,
                       u4NumBytesAccLow);
        }

        if (u4NumBytesDiscHigh == 0)
        {
            CliPrintf (CliHandle, "%-28s: %d\r\n",
                       "Number of bytes discarded", u4NumBytesDiscLow);
        }
        else
        {
            CliPrintf (CliHandle, "%-28s: High: %d Low: %d\r\n",
                       "Number of bytes discarded", u4NumBytesDiscHigh,
                       u4NumBytesDiscLow);
        }

        i4Quit = CliPrintf (CliHandle, "\r\n");

        if (u1ShowAll == DS_FALSE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDsTrafficClassConfigTable (i4TCId, &i4NextTCId)
            != SNMP_FAILURE) && (i4Quit == CLI_SUCCESS));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowQosStatistics                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the global QoS statstics    */
/*                                                                           */
/*     INPUT            : CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowQosStatistics (tCliHandle CliHandle)
{
    INT4                i4DsStatus;
    INT4                i4NumOfferedPkts = 0;
    INT4                i4NumAcceptedPkts = 0;
    INT4                i4NumRejectedPkts = 0;
    INT4                i4NumOfferedBytes = 0;
    INT4                i4NumAcceptedBytes = 0;
    INT4                i4NumRejectedBytes = 0;
    INT4                i4NumWrongPortAccptdPkts = 0;
    INT4                i4NumWrongPortRjctdPkts = 0;
    INT4                i4NumAcceptFairGroupPkts = 0;
    INT4                i4NumDiscardUnfairGroupPkts = 0;
    INT4                i4NumDiscardLevel2Pkts = 0;
    INT4                i4NumDiscardLevel3Pkts = 0;
    INT4                i4NumAcceptFairTCPkts = 0;
    INT4                i4NumAcceptDontcarePkts = 0;
    INT4                i4NumDiscardPortREDPkts = 0;
    INT4                i4NumDiscardTCREDPkts = 0;

    nmhGetFsDsStatus (&i4DsStatus);

    if (i4DsStatus == DS_ENABLE)
    {
        nmhGetFsDsNumWFHBDOfferedPkts (&i4NumOfferedPkts);
        nmhGetFsDsNumWFHBDAcceptedPkts (&i4NumAcceptedPkts);
        nmhGetFsDsNumWFHBDRejectedPkts (&i4NumRejectedPkts);
        nmhGetFsDsNumWFHBDOfferedBytes (&i4NumOfferedBytes);
        nmhGetFsDsNumWFHBDAcceptedBytes (&i4NumAcceptedBytes);
        nmhGetFsDsNumWFHBDRejectedBytes (&i4NumRejectedBytes);
        nmhGetFsDsNumWrongPortAcceptedPkts (&i4NumWrongPortAccptdPkts);
        nmhGetFsDsNumWrongPortRejectedPkts (&i4NumWrongPortRjctdPkts);
        nmhGetFsDsNumAcceptFairGroupPkts (&i4NumAcceptFairGroupPkts);
        nmhGetFsDsNumDiscardUnfairGroupPkts (&i4NumDiscardUnfairGroupPkts);
        nmhGetFsDsNumDiscardDropLevel2Pkts (&i4NumDiscardLevel2Pkts);
        nmhGetFsDsNumDiscardDropLevel3Pkts (&i4NumDiscardLevel3Pkts);
        nmhGetFsDsNumAcceptFairTCPkts (&i4NumAcceptFairTCPkts);
        nmhGetFsDsNumAcceptDontCarePkts (&i4NumAcceptDontcarePkts);
        nmhGetFsDsNumDiscardPortREDPkts (&i4NumDiscardPortREDPkts);
        nmhGetFsDsNumDiscardTCREDPkts (&i4NumDiscardTCREDPkts);

        CliPrintf (CliHandle, "Quality of Service is enabled \r\n");

        CliPrintf (CliHandle, "\r\nDiffServ Statistics \r\n");
        CliPrintf (CliHandle, "---------------------\r\n");

        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets offered to WFHBD", i4NumOfferedPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets accepted by WFHBD", i4NumAcceptedPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets rejected by WFHBD", i4NumRejectedPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of bytes offered to WFHBD", i4NumOfferedBytes);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of bytes accepted by WFHBD", i4NumAcceptedBytes);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of bytes rejected by WFHBD", i4NumRejectedBytes);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Wrong port accepted packets count",
                   i4NumWrongPortAccptdPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Wrong port rejected packets count",
                   i4NumWrongPortRjctdPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Fair flow group accepted packets count",
                   i4NumAcceptFairGroupPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of Unfair flow group packets discarded",
                   i4NumDiscardUnfairGroupPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Packets discarded for exceeding threshold 2 count (port+traffic class)",
                   i4NumDiscardLevel2Pkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Packets discarded for exceeding threshold 3 count (port+traffic class)",
                   i4NumDiscardLevel3Pkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets accepted as belonging to a fair traffic class",
                   i4NumAcceptFairTCPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets accepted without any active accept/reject decision",
                   i4NumAcceptDontcarePkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets discarded by port RED",
                   i4NumDiscardTCREDPkts);
        CliPrintf (CliHandle, "%-71s: %d\r\n",
                   "Number of packets discarded by traffic class RED",
                   i4NumDiscardTCREDPkts);
    }
    else
    {
        CliPrintf (CliHandle, "Quality of Service is disabled \r\n");

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowQosInterface                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays port based QoS info         */
/*                                                                           */
/*     INPUT            : i4PortNum                                          */
/*                        CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowQosInterface (tCliHandle CliHandle, INT4 i4PortNum)
{
    INT4                i4DsStatus = 0;
    INT4                i4MaxBandwidth = 0;
    INT4                i4DropAtMaxBw = DS_SNMP_FALSE;
    INT4                i4DropLevel1 = 0;
    INT4                i4DropLevel2 = 0;
    INT4                i4DropLevel3 = 0;
    INT4                i4REDCurveId = 0;
    INT4                i4TmpPort = 0;
    INT4                i4NextPortNum = 0;
    UINT1               u1ShowAll = DS_FALSE;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               u1Check = DS_TRUE;

    if (i4PortNum != 0)
    {
        /* Details of a specific port need to be displayed */
        if (nmhValidateIndexInstanceFsDsPortConfigTable (i4PortNum) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid interface\r\n");

            return (CLI_FAILURE);
        }

        i4NextPortNum = i4PortNum;
    }
    else
    {
        /* 
         * This flag is used to indicate that all policy 
         * maps must be displayed 
         */
        u1ShowAll = DS_TRUE;
    }

    CliPrintf (CliHandle, "\r\nDiffServ Port configurations: \r\n");
    CliPrintf (CliHandle, "------------------------------\r\n");

    if (u1ShowAll == DS_TRUE)
    {
        if (nmhGetFirstIndexFsDsPortConfigTable (&i4NextPortNum) ==
            SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4TmpPort = i4NextPortNum;

        nmhGetFsDsPortMaxBandwidth (i4TmpPort, &i4MaxBandwidth);
        nmhGetFsDsPortDropAtMaxBandwidth (i4TmpPort, &i4DropAtMaxBw);
        if (i4DropAtMaxBw != DS_SNMP_TRUE)
        {
            nmhGetFsDsPortDropLevel1 (i4TmpPort, &i4DropLevel1);
            nmhGetFsDsPortDropLevel2 (i4TmpPort, &i4DropLevel2);
            nmhGetFsDsPortDropLevel3 (i4TmpPort, &i4DropLevel3);
        }
        nmhGetFsDsPortREDCurveId (i4TmpPort, &i4REDCurveId);

        if (u1Check == DS_TRUE)
        {
            nmhGetFsDsStatus (&i4DsStatus);
        }

        if (u1Check == DS_TRUE)
        {
            if (i4DsStatus == DS_ENABLE)
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been enabled \r\n\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been disabled \r\n\r\n");
            }

            u1Check = DS_FALSE;
        }

        CliPrintf (CliHandle, " Port %d\r\n", i4TmpPort);

        CliPrintf (CliHandle, "%-28s: %d (in multiples of 64000 bps)\r\n",
                   "Maximum bandwidth", i4MaxBandwidth);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "Drop Threshold1", i4DropLevel1);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "Drop Threshold2", i4DropLevel2);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "Drop Threshold3", i4DropLevel3);
        CliPrintf (CliHandle, "%-28s: %d\r\n", "RED Curve ID", i4REDCurveId);

        i4Quit = CliPrintf (CliHandle, "\r\n");

        if (u1ShowAll == DS_FALSE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDsPortConfigTable (i4TmpPort, &i4NextPortNum)
            != SNMP_FAILURE) && (i4Quit == CLI_SUCCESS));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowREDParams                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the RED curve parameters    */
/*                                                                           */
/*     INPUT            : i4REDCurveId                                       */
/*                        CliHandle-CLI Handler                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowREDParams (tCliHandle CliHandle, INT4 i4REDCurveId)
{
    INT4                i4DsStatus = 0;
    INT4                i4REDStatus;
    INT4                i4REDStart = 0;
    INT4                i4REDStop = 0;
    INT4                i4REDQRange = 0;
    INT4                i4REDStopProb = 0;
    INT4                i4TmpREDCurve = 0;
    INT4                i4NextREDCurveId = 0;
    UINT1               u1ShowAll = DS_FALSE;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               u1Check = DS_TRUE;

    if (i4REDCurveId != 0)
    {
        /* Details of a specific RED curve need to be displayed */
        if (nmhGetFsDsREDCurveStatus (i4REDCurveId, &i4REDStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid RED curve\r\n");

            return (CLI_FAILURE);
        }

        i4NextREDCurveId = i4REDCurveId;
    }
    else
    {
        /* 
         * This flag is used to indicate that all policy 
         * maps must be displayed 
         */
        u1ShowAll = DS_TRUE;
    }

    CliPrintf (CliHandle, "\r\nDiffServ RED configurations: \r\n");
    CliPrintf (CliHandle, "------------------------------\r\n");

    if (u1ShowAll == DS_TRUE)
    {
        if (nmhGetFirstIndexFsDsREDConfigTable (&i4NextREDCurveId) ==
            SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }

    do
    {
        i4TmpREDCurve = i4NextREDCurveId;

        if (nmhGetFsDsREDCurveStatus (i4TmpREDCurve, &i4REDStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid RED curve\r\n");

            return (CLI_FAILURE);
        }

        nmhGetFsDsREDCurveStart (i4TmpREDCurve, &i4REDStart);
        nmhGetFsDsREDCurveStop (i4TmpREDCurve, &i4REDStop);
        nmhGetFsDsREDCurveQRange (i4TmpREDCurve, &i4REDQRange);
        nmhGetFsDsREDCurveStopProbability (i4TmpREDCurve, &i4REDStopProb);

        if (u1Check == DS_TRUE)
        {
            nmhGetFsDsStatus (&i4DsStatus);
        }

        if (u1Check == DS_TRUE)
        {
            if (i4DsStatus == DS_ENABLE)
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been disabled \r\n");
            }

            u1Check = DS_FALSE;
        }

        CliPrintf (CliHandle, "\r\nRED Curve %d ", i4TmpREDCurve);

        if (i4REDStatus == DS_ACTIVE)
        {
            CliPrintf (CliHandle, "is active\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "is not active\r\n");
        }

        CliPrintf (CliHandle, "%-28s: %d (in multiples of 16 KB)\r\n",
                   "Curve start", i4REDStart);
        CliPrintf (CliHandle, "%-28s: %d (in multiples of 16 KB)\r\n",
                   "Curve stop", i4REDStop);
        CliPrintf (CliHandle, "%-28s: %d (in log2 units of actual bytes)\r\n",
                   "QRange", i4REDQRange);
        CliPrintf (CliHandle, "%-28s: %d%\r\n", "Probability at Start+QRange",
                   i4REDStopProb);

        i4Quit = CliPrintf (CliHandle, "\r\n");

        if (u1ShowAll == DS_FALSE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDsREDConfigTable (i4TmpREDCurve,
                                               &i4NextREDCurveId)
            != SNMP_FAILURE) && (i4Quit == CLI_SUCCESS));

    return (CLI_SUCCESS);
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowRunningConfig                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        information for diffserv                           */
/*                                                                           */
/*     INPUT            : CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4DsStatus = 0;
    INT4                i4DsSystemStatus = 0;
    INT4                i4Index = -1;

    CliRegisterLock (CliHandle, DfsLock, DfsUnLock);

    DFS_LOCK ();

    nmhGetFsDsSystemControl (&i4DsSystemStatus);
    if (i4DsSystemStatus != DS_START)
    {
        CliPrintf (CliHandle, "shutdown qos\n");
        CliPrintf (CliHandle, "!\r\n");
        DFS_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }

    nmhGetFsDsStatus (&i4DsStatus);
    if (i4DsStatus != DS_DISABLE)
    {
        CliPrintf (CliHandle, "set qos enable\r\n");
        DiffservShowRunningConfigScalars (CliHandle);
        DiffservClassMapShowRunningConfig (CliHandle);
        DiffservPolicyMapShowRunningConfig (CliHandle);
        DiffservTrafficClassShowRunningConfig (CliHandle);
        DiffservREDCurveShowRunningConfig (CliHandle);

        if (u4Module == ISS_DIFFSERV_SHOW_RUNNING_CONFIG)
        {
            DFS_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            DiffservShowRunningConfigInterfaceDetails (CliHandle, i4Index);

            CliRegisterLock (CliHandle, DfsLock, DfsUnLock);
            DFS_LOCK ();
        }

        CliPrintf (CliHandle, "!\r\n");
    }

    DFS_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowRunningConfigScalars                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current queue-precedence*/
/*                        and DSCP to queue mapping configurations           */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservShowRunningConfigScalars (tCliHandle CliHandle)
{
    UINT4               u4DSCP;
    INT4                i4QPrec, i4Queue, i4DefaultQueue;

    nmhGetFsDsIPTOSQPrecedenceSwitched (&i4QPrec);

    if (i4QPrec != 5)
    {
        CliPrintf (CliHandle, "qos queue-precedence switched dscp %d\r\r\n",
                   i4QPrec);
    }

    nmhGetFsDsDot1PQPrecedenceSwitched (&i4QPrec);

    if (i4QPrec != 6)
    {
        CliPrintf (CliHandle, "qos queue-precedence switched cos %d\r\r\n",
                   i4QPrec);
    }

    nmhGetFsDsIPTOSQPrecedenceRouted (&i4QPrec);

    if (i4QPrec != 6)
    {
        CliPrintf (CliHandle, "qos queue-precedence routed dscp %d\r\n",
                   i4QPrec);
    }

    nmhGetFsDsDot1PQPrecedenceRouted (&i4QPrec);

    if (i4QPrec != 5)
    {
        CliPrintf (CliHandle, "qos queue-precedence routed cos %d\r\n",
                   i4QPrec);
    }

    i4DefaultQueue = -1;

    for (u4DSCP = 0; u4DSCP < 64; u4DSCP++)
    {
        if (u4DSCP % 8 == 0)
        {
            ++i4DefaultQueue;
        }

        nmhGetFsDsQPriority (u4DSCP, &i4Queue);

        if (i4Queue != i4DefaultQueue)
        {
            CliPrintf (CliHandle, "qos map dscp-queue %d to %d\r\n",
                       u4DSCP, i4Queue);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservClassMapShowRunningConfig                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current class-map       */
/*                        configuration details                              */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservClassMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4NextIndex;
    INT4                i4PrevIndex;
    INT4                i4ClfrStatus;
    INT4                i4FilterType = 0;
    UINT4               u4FilterId = 0;
    UINT4               u4Flag = 0;

    if (nmhGetFirstIndexFsDiffServMultiFieldClfrTable (&i4NextIndex)
        != SNMP_FAILURE)
    {
        do
        {
            i4PrevIndex = i4NextIndex;

            if (nmhGetFsDiffServMultiFieldClfrStatus (i4NextIndex,
                                                      &i4ClfrStatus) !=
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "class-map %d\r\n", i4NextIndex);
                u4Flag = 1;

                if (i4ClfrStatus != DS_ACTIVE)
                {
                    CliPrintf (CliHandle, "!\r\n");
                    continue;
                }
                if (nmhGetFsDiffServMultiFieldClfrFilterId (i4NextIndex,
                                                            &u4FilterId) !=
                    SNMP_FAILURE)
                {
                    if (nmhGetFsDiffServMultiFieldClfrFilterType (i4NextIndex,
                                                                  &i4FilterType)
                        != SNMP_FAILURE)
                    {

                        if (i4FilterType == DS_MAC_FILTER)
                        {
                            CliPrintf (CliHandle, "  match access-group");
                            CliPrintf (CliHandle, " mac-access-list");
                            CliPrintf (CliHandle, " %d\r\n", u4FilterId);
                            u4Flag = 1;
                        }
                        else if (i4FilterType == DS_IP_FILTER)
                        {
                            CliPrintf (CliHandle, "  match access-group");
                            CliPrintf (CliHandle, " ip-access-list");
                            CliPrintf (CliHandle, " %d\r\n", u4FilterId);
                            u4Flag = 1;
                        }

                    }
                }
            }
        }
        while ((nmhGetNextIndexFsDiffServMultiFieldClfrTable
                (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS));
    }

    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservPolicyMapShowRunningConfig                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current policy-map      */
/*                        configuration details                              */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservPolicyMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4ClfrStatus = 0;
    INT4                i4ClfrId = 0;
    INT4                i4NextClfrId = 0;
    INT4                i4MFClfrId = 0;
    UINT4               u4InProfFlag = 0;
    UINT4               u4InNewPrio = 0;
    UINT4               u4InIpTOs = 0;
    UINT4               u4InActDscp = 0;
    UINT4               u4Flag = 0;
    tDsClfr             DsClfr;

    if (nmhGetFirstIndexFsDiffServClfrTable (&i4NextClfrId) != SNMP_FAILURE)
    {
        do
        {
            i4ClfrId = i4NextClfrId;

            if (nmhGetFsDiffServClfrStatus (i4ClfrId, &i4ClfrStatus)
                != SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "policy-map %d\r\n", i4ClfrId);
                u4Flag = 1;

                if (i4ClfrStatus != DS_ACTIVE)
                {
                    CliPrintf (CliHandle, "!\r\n");
                    continue;
                }

                /* Get the Clfr and MCClfrId from it */
                if (nmhGetFsDiffServClfrMFClfrId (i4ClfrId, &i4MFClfrId)
                    != SNMP_FAILURE)
                {
                    if (i4MFClfrId != 0)
                    {
                        CliPrintf (CliHandle, "  class %d\r\n", i4MFClfrId);

                        u4Flag = 2;
                        if (nmhGetFsDiffServClfrInProActionId (i4ClfrId,
                                                               &DsClfr.
                                                               i4DsInProActId)
                            != SNMP_FAILURE)
                        {
                            if (DsClfr.i4DsInProActId != 0)
                            {
                                if (nmhGetFsDiffServInProfileActionFlag
                                    (DsClfr.i4DsInProActId,
                                     &u4InProfFlag) == SNMP_SUCCESS)
                                {
                                    if (u4InProfFlag & FS_DS_ACTN_INSERT_PRIO)
                                    {
                                        nmhGetFsDiffServInProfileActionNewPrio
                                            (DsClfr.i4DsInProActId,
                                             &u4InNewPrio);
                                        CliPrintf (CliHandle, "    set");
                                        CliPrintf (CliHandle, " cos %d\r\n",
                                                   u4InNewPrio);
                                        u4Flag = 3;
                                    }
                                    else if (u4InProfFlag &
                                             FS_DS_ACTN_INSERT_TOSP)
                                    {
                                        nmhGetFsDiffServInProfileActionIpTOS
                                            (DsClfr.i4DsInProActId, &u4InIpTOs);
                                        CliPrintf (CliHandle, "    set");
                                        CliPrintf (CliHandle,
                                                   " ip precedence %d\r\n",
                                                   u4InIpTOs);
                                        u4Flag = 3;
                                    }
                                    else if (u4InProfFlag &
                                             FS_DS_ACTN_INSERT_DSCP)
                                    {
                                        nmhGetFsDiffServInProfileActionDscp
                                            (DsClfr.i4DsInProActId,
                                             &u4InActDscp);
                                        CliPrintf (CliHandle, "    set");
                                        CliPrintf (CliHandle,
                                                   " ip dscp %d\r\n",
                                                   u4InActDscp);
                                        u4Flag = 3;
                                    }
                                }
                            }
                        }
                        if (nmhGetFsDiffServClfrTrafficClassId (i4ClfrId,
                                                                &DsClfr.
                                                                i4DsTrafficClassId)
                            != SNMP_FAILURE)
                        {
                            if (DsClfr.i4DsTrafficClassId != 0)
                            {
                                CliPrintf (CliHandle,
                                           "    set qos traffic-class %d\r\n",
                                           DsClfr.i4DsTrafficClassId);
                            }
                        }

                    }
                }
            }

        }
        while ((nmhGetNextIndexFsDiffServClfrTable (i4ClfrId, &i4NextClfrId)
                != SNMP_FAILURE));

    }
    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    else if (u4Flag == 2 || u4Flag == 3)
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservTrafficClassShowRunningConfig              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current traffic-class   */
/*                        configuration details                              */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservTrafficClassShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4NextIndex;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4PrevIndex;
    INT4                i4PortNum = 0;
    INT4                i4MaxBandwidth = 0;
    INT4                i4GuarntdBandwidth = 0;
    INT4                i4DropAtMaxBw = DS_SNMP_FALSE;
    INT4                i4DropLevel1 = 0;
    INT4                i4DropLevel2 = 0;
    INT4                i4DropLevel3 = 0;
    INT4                i4WeightShift = 0;
    INT4                i4WeightFactor = 0;
    INT4                i4NumFlowGroups = 0;
    INT4                i4REDCurveId = 0;

    if (nmhGetFirstIndexFsDsTrafficClassConfigTable (&i4NextIndex) !=
        SNMP_FAILURE)
    {
        do
        {
            i4PrevIndex = i4NextIndex;

            nmhGetFsDsTCPortNum (i4NextIndex, &i4PortNum);
            nmhGetFsDsTCMaxBandwidth (i4NextIndex, &i4MaxBandwidth);
            nmhGetFsDsTCGuaranteedBandwidth (i4NextIndex, &i4GuarntdBandwidth);
            nmhGetFsDsTCDropAtMaxBandwidth (i4NextIndex, &i4DropAtMaxBw);
            nmhGetFsDsTCDropLevel1 (i4NextIndex, &i4DropLevel1);
            nmhGetFsDsTCDropLevel2 (i4NextIndex, &i4DropLevel2);
            nmhGetFsDsTCDropLevel3 (i4NextIndex, &i4DropLevel3);
            nmhGetFsDsTCWeightFactor (i4NextIndex, &i4WeightFactor);
            nmhGetFsDsTCWeightShift (i4NextIndex, &i4WeightShift);
            nmhGetFsDsTCNumFlowGroups (i4NextIndex, &i4NumFlowGroups);
            nmhGetFsDsTCREDCurveId (i4NextIndex, &i4REDCurveId);

            if (i4PortNum == 0)
            {
                CliPrintf (CliHandle, "qos traffic-class %d\r\n", i4NextIndex);
            }
            else
            {
                CfaCliConfGetIfName ((UINT4) i4PortNum, &ai1IfName[0]);

                CliPrintf (CliHandle, "qos traffic-class %d map %s\r\n",
                           i4NextIndex, ai1IfName);
            }

            if (i4MaxBandwidth != DS_DEF_PORT_MAX_BW)
            {
                CliPrintf (CliHandle, " bandwidth %d", i4MaxBandwidth);

                if (i4DropAtMaxBw == DS_SNMP_TRUE)
                {
                    CliPrintf (CliHandle, " exceed-action-drop\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            else
            {
                if (i4DropAtMaxBw == DS_SNMP_TRUE)
                {
                    CliPrintf (CliHandle,
                               " bandwidth %d exceed-action-drop\r\n",
                               i4MaxBandwidth);
                }
            }

            if (i4GuarntdBandwidth != 0)
            {
                CliPrintf (CliHandle, " min-reserved-bandwidth %d\r\n",
                           i4GuarntdBandwidth);
            }

            if (i4DropLevel1 != DS_MIN_DROP_LEVEL1 ||
                i4DropLevel2 != DS_MIN_DROP_LEVEL2 ||
                i4DropLevel3 != DS_MIN_DROP_LEVEL3)
            {
                if (i4DropLevel3 != DS_MIN_DROP_LEVEL3)
                {
                    CliPrintf (CliHandle, " queue-threshold %d %d %d\r\n",
                               i4DropLevel1, i4DropLevel2, i4DropLevel3);
                }
                else if (i4DropLevel2 != DS_MIN_DROP_LEVEL2)
                {
                    CliPrintf (CliHandle, " queue-threshold %d %d\r\n",
                               i4DropLevel1, i4DropLevel2);
                }
                else if (i4DropLevel1 != DS_MIN_DROP_LEVEL1)
                {
                    CliPrintf (CliHandle, " queue-threshold %d\r\n",
                               i4DropLevel1);
                }
            }

            if (i4WeightFactor != DS_DEF_WEIGHT_FACTOR)
            {
                CliPrintf (CliHandle, " weight factor %d\r\n", i4WeightFactor);
            }

            if (i4WeightShift != DS_DEF_WEIGHT_SHIFT)
            {
                CliPrintf (CliHandle, " weight shift %d\r\n", i4WeightShift);
            }

            if (i4NumFlowGroups != 0)
            {
                CliPrintf (CliHandle, " fair-queue %d\r\n", i4NumFlowGroups);
            }

            if (i4REDCurveId != 0)
            {
                CliPrintf (CliHandle, " set random-detect %d\r\n",
                           i4REDCurveId);
            }

            CliPrintf (CliHandle, "!\r\n");
        }
        while ((nmhGetNextIndexFsDsTrafficClassConfigTable
                (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS));

        CliPrintf (CliHandle, "!\r\n");
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservREDCurveShowRunningConfig                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current RED curve       */
/*                        configuration details                              */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservREDCurveShowRunningConfig (tCliHandle CliHandle)
{
    UINT4               u4Flag = 0;
    INT4                i4NextIndex;
    INT4                i4PrevIndex;
    INT4                i4REDStatus;
    INT4                i4REDStart = 0;
    INT4                i4REDStop = 0;
    INT4                i4REDQRange = 0;
    INT4                i4REDStopProb = 0;

    if (nmhGetFirstIndexFsDsREDConfigTable (&i4NextIndex) != SNMP_FAILURE)
    {
        do
        {
            i4PrevIndex = i4NextIndex;

            nmhGetFsDsREDCurveStatus (i4NextIndex, &i4REDStatus);

            if (i4REDStatus != DS_ACTIVE)
            {
                continue;
            }

            u4Flag = 1;

            nmhGetFsDsREDCurveStart (i4NextIndex, &i4REDStart);
            nmhGetFsDsREDCurveStop (i4NextIndex, &i4REDStop);
            nmhGetFsDsREDCurveQRange (i4NextIndex, &i4REDQRange);
            nmhGetFsDsREDCurveStopProbability (i4NextIndex, &i4REDStopProb);

            CliPrintf (CliHandle, "qos random-detect %d", i4NextIndex);
            CliPrintf (CliHandle,
                       " start %d stop %d qrange %d stop-probability %d\r\n",
                       i4REDStart, i4REDStop, i4REDQRange, i4REDStopProb);

        }
        while ((nmhGetNextIndexFsDsREDConfigTable
                (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS));
    }

    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : DiffservShowRunningConfigInterfaceDetails          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Interface objects in    */
/*                        Diffserv.                                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{

    INT4                i4NextIndex;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4PrevIndex;
    UINT4               u4Flag = 0;
    INT4                i4MaxBandwidth = 0;
    INT4                i4DropAtMaxBw = DS_SNMP_FALSE;
    INT4                i4DropLevel1 = 0;
    INT4                i4DropLevel2 = 0;
    INT4                i4DropLevel3 = 0;
    INT4                i4REDCurveId = 0;
    INT4                i4DsStatus = 0;
    INT4                i4DsSystemStatus = 0;

    CliRegisterLock (CliHandle, DfsLock, DfsUnLock);
    DFS_LOCK ();

    nmhGetFsDsSystemControl (&i4DsSystemStatus);
    if (i4DsSystemStatus != DS_START)
    {
        CliPrintf (CliHandle, "shutdown qos\n");
        CliPrintf (CliHandle, "!\r\n");
        DFS_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }

    nmhGetFsDsStatus (&i4DsStatus);

    if (i4DsStatus == DS_DISABLE)
    {
        DFS_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }
    if (i4Index == -1)
    {
        if (nmhGetFirstIndexFsDsPortConfigTable (&i4NextIndex) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhValidateIndexInstanceFsDsPortConfigTable (i4Index) ==
            SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }

        i4NextIndex = i4Index;
    }

    do
    {
        i4PrevIndex = i4NextIndex;

        u4Flag = 1;

        nmhGetFsDsPortMaxBandwidth (i4NextIndex, &i4MaxBandwidth);
        nmhGetFsDsPortDropAtMaxBandwidth (i4NextIndex, &i4DropAtMaxBw);
        nmhGetFsDsPortDropLevel1 (i4NextIndex, &i4DropLevel1);
        nmhGetFsDsPortDropLevel2 (i4NextIndex, &i4DropLevel2);
        nmhGetFsDsPortDropLevel3 (i4NextIndex, &i4DropLevel3);
        nmhGetFsDsPortREDCurveId (i4NextIndex, &i4REDCurveId);

        if (i4Index == -1)
        {
            CfaCliConfGetIfName ((UINT4) i4NextIndex, &ai1IfName[0]);

            CliPrintf (CliHandle, "int %s\r\n", ai1IfName);
        }

        if (i4MaxBandwidth != DS_DEF_PORT_MAX_BW)
        {
            CliPrintf (CliHandle, " qos bandwidth %d", i4MaxBandwidth);

            if (i4DropAtMaxBw == DS_SNMP_TRUE)
            {
                CliPrintf (CliHandle, " exceed-action-drop\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        else
        {
            if (i4DropAtMaxBw == DS_SNMP_TRUE)
            {
                CliPrintf (CliHandle,
                           " qos bandwidth %d exceed-action-drop\r\n",
                           i4MaxBandwidth);
            }
        }

        if (i4DropLevel1 != DS_MIN_DROP_LEVEL1 ||
            i4DropLevel2 != DS_MIN_DROP_LEVEL2 ||
            i4DropLevel3 != DS_MIN_DROP_LEVEL3)
        {
            CliPrintf (CliHandle, " qos queue-threshold %d %d %d\r\n",
                       i4DropLevel1, i4DropLevel2, i4DropLevel3);
        }

        if (i4REDCurveId != DS_MAX_REDCURVES + 1)
        {
            CliPrintf (CliHandle, " qos set random-detect %d\r\n",
                       i4REDCurveId);
        }

        if (i4Index != -1)
        {
            break;
        }

        CliPrintf (CliHandle, "!\r\n");
    }
    while ((nmhGetNextIndexFsDsPortConfigTable
            (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS));

    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    DFS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);

}

#endif /* */

#else

INT4
cli_process_ds_cmd (UINT4 u4Command, ...)
{
    mmi_printf ("DiffServ module not available !\r\n");
    UNUSED_PARAM (u4Command);
    return CLI_SUCCESS;
}

#endif /* DIFFSRV_WANTED */
#endif
