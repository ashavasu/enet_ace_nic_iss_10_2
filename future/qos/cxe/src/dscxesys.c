/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dscxesys.c,v 1.5 2013/12/16 15:26:29 siva Exp $
*
* Description     : This file calls function related to Diffserver module.
*
********************************************************************/
#ifndef _DSCXESYS_C
#define _DSCXESYS_C

#include "dscxeinc.h"
#include "dscxecli.h"
#include "fsissdwr.h"

tOsixSemId          gSemId;
/*****************************************************************************/
/* Function Name      : DsInit                                               */
/*                                                                           */
/* Description        : This function creates the Mutual exclusion           */
/*                      semaphore and starts the Diffserver module           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsInit (VOID)
{

    if (OsixCreateSem (DFS_MUT_EXCL_SEM_NAME, 1, 0,
                       &(DFS_MUT_EXCL_SEM_ID)) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "creation FAILED\n");
        return DS_FAILURE;
    }

    if (DsStart () == DS_FAILURE)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Start failed\n");
        return DS_FAILURE;
    }

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsStart                                              */
/*                                                                           */
/* Description        : This function allocates memory pools for all tables  */
/*                      in the DiffServ module. It initialises the           */
/*                      the GLobal structure gDsGLobalInfo.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*                      DS_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
DsStart (VOID)
{
    DS_MEMSET (&gDsGlobalInfo, 0, sizeof (tDsGlobalInfo));

    /* Assign Trace Option */
    gDsGlobalInfo.u4DsTrcFlag = 0;

    /* Allocate memory pool for MultiField classifier table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_MFCLFR_MEMBLK_SIZE,
                                  DS_MFCLFR_MEMBLK_COUNT,
                                  &(DS_MFCLFR_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC,
                "MultiField Classifier Memory Pool Creation FAILED\n");
        DsDeleteMemPools ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Classifier Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_CLFR_MEMBLK_SIZE,
                                  DS_CLFR_MEMBLK_COUNT,
                                  &(DS_CLFR_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC,
                "Classifier Entry Memory Pool Creation FAILED\n");
        DsDeleteMemPools ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for In Profile Action Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_INPROACT_MEMBLK_SIZE,
                                  DS_INPROACT_MEMBLK_COUNT,
                                  &(DS_INPROACT_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC,
                "In Profile Action Entry Memory Pool Creation FAILED\n");
        DsDeleteMemPools ();
        return DS_FAILURE;
    }

    /* Initialise CXE specific structure */
    if (DsWfhbdInfoInit () == DS_FAILURE)
    {
        DS_TRC (INIT_SHUT_TRC, "Wfhbd Entry Memory Pool Creation FAILED\n");
        DsDeleteMemPools ();
        return DS_FAILURE;
    }

    /* Initialise the SLL lists for All the Tables */
    DS_SLL_INIT (&(DS_CLFR_LIST));
    DS_SLL_INIT (&(DS_MFCLFR_LIST));
    DS_SLL_INIT (&(DS_INPROACT_LIST));
    RegisterFSISSD ();

    gDsGlobalInfo.DsSystemControl = DS_START;
    gDsGlobalInfo.DsStatus = DS_DISABLE;

    DS_TRC (CONTROL_PLANE_TRC, "SYS: DIFFSERV MODULE INITIALIZED\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsEnable                                             */
/*                                                                           */
/* Description        : This function scans through all the tables and       */
/*                      programs the hardware accordingly.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*                      DS_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
DsEnable (VOID)
{
    INT4                i4RetVal;
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    DS_TRC (INIT_SHUT_TRC, "SYS: Enabling DIFFSERV Module ... \n");

#ifdef NPAPI_WANTED
    if (DsConfigWfhbdInfoInHw () == DS_FAILURE)
    {
        DS_TRC (INIT_SHUT_TRC, "SYS: Hw Wfhbd Info Enable FAILED !!! \n");
        return DS_FAILURE;
    }
#endif

    /* Now that all configurations have been applied on the hardware
     * reset the dirty flag for all ports */
    DS_INIT_PORT_DIRTY_FLAG ();

    /* Scan through the Classifier Table and program 
     * the valid entries in the device.*/
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_NOT_IN_SERVICE)
        {
            /* Get the Elements from the list here */
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

            if (pDsMFClfrEntry == NULL)
            {
                return DS_FAILURE;
            }

            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);

#ifdef NPAPI_WANTED
            /* Call the hardware routine here */
            if (pDsClfrEntry->i4DsClfrTrafficClassId == 0)
            {
                i4RetVal =
                    DsHwSetClassifierAction (pDsClfrEntry, pDsMFClfrEntry,
                                             pDsInProActEntry,
                                             DS_NP_CLSFR_TC_DONTCARE);
            }
            else
            {
                i4RetVal =
                    DsHwSetClassifierAction (pDsClfrEntry, pDsMFClfrEntry,
                                             pDsInProActEntry,
                                             DS_NP_CLSFR_TC_MAP);
            }

            if (i4RetVal == FNP_FAILURE)
            {
                DS_TRC (INIT_SHUT_TRC,
                        "SYS: Hw Wfhbd Info Enable - Enabling Classifier entry FAILED !!! \n");
                return DS_FAILURE;
            }
#endif
            pDsClfrEntry->u1DsClfrStatus = DS_ACTIVE;
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    /* Set the status of DiffServ as Enable */
    gDsGlobalInfo.DsStatus = DS_ENABLE;

    DS_TRC (CONTROL_PLANE_TRC, "SYS: DIFFSERV MODULE ENABLED\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsDeleteMemPools.                                 */
/*                                                                           */
/* Description        : This function is called when the initialization of   */
/*                      DiffServ fails at any point during  initialization.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
DsDeleteMemPools (VOID)
{
    if (DS_MFCLFR_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_MFCLFR_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {
            DS_TRC (INIT_SHUT_TRC,
                    "MF Classifier Entry Memory Pool Release FAILED\n");
        }
        DS_MFCLFR_MEMPOOL_ID = DS_INVALID_VAL;
    }

    if (DS_CLFR_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_CLFR_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC,
                    "Classifier Entry Memory Pool Release FAILED\n");
        }
        DS_CLFR_MEMPOOL_ID = DS_INVALID_VAL;
    }

    if (DS_INPROACT_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_INPROACT_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC,
                    "In Profile Action Entry Memory Pool Release FAILED\n");
        }
        DS_INPROACT_MEMPOOL_ID = DS_INVALID_VAL;
    }
}

/*****************************************************************************/
/* Function Name      : DsShutdown                                           */
/*                                                                           */
/* Description        : This function is called to shutdown DiffServ. This   */
/*                      function internally calls DsDisable before releasing */
/*                      all the memory.                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsShutDown (VOID)
{
    /* If the DS module is shut down return FAILURE */
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {

        DS_TRC (INIT_SHUT_TRC, "System is not Started. Cannot Shutdown.\n");
        return DS_FAILURE;
    }

    /* Disable the DiffServ Module */
    DsDisable ();

    DsWfhbdInfoDeInit ();

    DsDeleteMemPools ();

    gDsGlobalInfo.DsSystemControl = DS_SHUTDOWN;

    DS_TRC (INIT_SHUT_TRC, "SYS: DIFFSERV MODULE SHUT DOWN\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsDisable                                            */
/*                                                                           */
/* Description        : This function deletes all the entries for datapath,  */
/*                      classifier and Scheduler from the hardware.          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsDisable (VOID)
{
    INT4                i4Retval = DS_SUCCESS;
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;

    /* If the DS module is shut down return FAILURE */
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {

        DS_TRC (INIT_SHUT_TRC, "System is not Started. Cannot Disable.\n");
        return DS_FAILURE;
    }

    /* If the DS module is not ENABLED return FAILURE */
    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {

        DS_TRC (INIT_SHUT_TRC, "System is not Enabled. Cannot Disable.\n");
        return DS_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsDisableWfhbdInfoInHw () == DS_FAILURE)
    {
        DS_TRC (INIT_SHUT_TRC, "SYS: Hw Disable Wfhbd Info FAILED !!! \n");
        return DS_FAILURE;
    }
#endif

    /* Scan through the clfr Table and put 
     * the ACTIVE entries to NOT_IN_SERVICE */
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
#ifdef NPAPI_WANTED
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

            /* Call the Classifier delete for the hardware */
            if (DsHwResetClassifierAction (pDsClfrEntry, pDsMFClfrEntry)
                == FNP_FAILURE)
            {
                i4Retval = DS_FAILURE;
            }

#endif
            pDsClfrEntry->u1DsClfrStatus = DS_NOT_IN_SERVICE;
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    gDsGlobalInfo.DsStatus = DS_DISABLE;

    DS_TRC (INIT_SHUT_TRC, "SYS: DIFFSERV MODULE DISABLED\n");

    return i4Retval;
}

/*****************************************************************************/
/* Function Name      : DsValidateMFClfrId                                   */
/*                                                                           */
/* Description        : This function is called for validating the MF        */
/*                      Classifier Id.         .                             */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServMFClfrId                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateMFClfrId (INT4 i4FsDiffServMFClfrId)
{
    if ((i4FsDiffServMFClfrId < DS_MIN_MFCLFR_ID) ||
        (i4FsDiffServMFClfrId > DS_MAX_MFCLFR_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateClfrId                                     */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      Classifier Id.         .                             */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServClfrId                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateClfrId (INT4 i4FsDiffServClfrId)
{
    if ((i4FsDiffServClfrId < DS_MIN_CLFR_ID) ||
        (i4FsDiffServClfrId > DS_MAX_CLFR_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateInProfileActionId                          */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      In Profile Action Id.                                */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServInProfileActionId                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateInProfileActionId (INT4 i4FsDiffServInProfileActionId)
{
    if ((i4FsDiffServInProfileActionId < DS_MIN_IN_PROACT_ID) ||
        (i4FsDiffServInProfileActionId > DS_MAX_IN_PROACT_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidMultiFieldClfrId               */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the MF Classifier Table.                          */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServMultiFieldClfrId                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidMultiFieldClfrId (INT4 *pi4FsDiffServMultiFieldClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDiffServMFClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pFirstDiffServMFClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServMFClfrEntry = pDiffServMFClfrEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServMFClfrEntry->i4DsMultiFieldClfrId >
                pDiffServMFClfrEntry->i4DsMultiFieldClfrId)
            {
                pFirstDiffServMFClfrEntry = pDiffServMFClfrEntry;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServMultiFieldClfrId =
            pFirstDiffServMFClfrEntry->i4DsMultiFieldClfrId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidMultiFieldClfrId                */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the MF Classifier table.                             */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServDataPathId                    */
/*                    : INT4 i4FsDiffServDataPathId                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidMultiFieldClfrId (INT4 i4FsDiffServMultiFieldClfrId,
                                       INT4 *pi4NextFsDiffServMultiFieldClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDiffServMFClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pNextDiffServMFClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if (i4FsDiffServMultiFieldClfrId <
            pDiffServMFClfrEntry->i4DsMultiFieldClfrId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServMFClfrEntry = pDiffServMFClfrEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServMFClfrEntry->i4DsMultiFieldClfrId >
                    pDiffServMFClfrEntry->i4DsMultiFieldClfrId)
                {
                    pNextDiffServMFClfrEntry = pDiffServMFClfrEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServMultiFieldClfrId =
            pNextDiffServMFClfrEntry->i4DsMultiFieldClfrId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidClfrId                         */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the Classifier Table.                             */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServClfrId                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidClfrId (INT4 *pi4FsDiffServClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;
    tDiffServClfrEntry *pFirstDiffServClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServClfrEntry = pDiffServClfrEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServClfrEntry->i4DsClfrId >
                pDiffServClfrEntry->i4DsClfrId)
            {
                pFirstDiffServClfrEntry = pDiffServClfrEntry;
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServClfrId = pFirstDiffServClfrEntry->i4DsClfrId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidClfrId                          */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the Classifier table.                                */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServDataPathId                    */
/*                    : INT4 i4FsDiffServDataPathId                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidClfrId (INT4 i4FsDiffServClfrId,
                             INT4 *pi4NextFsDiffServClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;
    tDiffServClfrEntry *pNextDiffServClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (i4FsDiffServClfrId < pDiffServClfrEntry->i4DsClfrId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServClfrEntry = pDiffServClfrEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServClfrEntry->i4DsClfrId >
                    pDiffServClfrEntry->i4DsClfrId)
                {
                    pNextDiffServClfrEntry = pDiffServClfrEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServClfrId = pNextDiffServClfrEntry->i4DsClfrId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidInProfileActionId              */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the In Profile Action Table.                      */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServInProfileActionId                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidInProfileActionId (INT4 *pi4FsDiffServInProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServInProfileActionEntry *pDiffServInProActEntry = NULL;
    tDiffServInProfileActionEntry *pFirstDiffServInProActEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_INPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServInProActEntry = (tDiffServInProfileActionEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServInProActEntry = pDiffServInProActEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServInProActEntry->i4DsInProfileActionId >
                pDiffServInProActEntry->i4DsInProfileActionId)
            {
                pFirstDiffServInProActEntry = pDiffServInProActEntry;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_INPROACT_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServInProfileActionId =
            pFirstDiffServInProActEntry->i4DsInProfileActionId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidInProfileActionId               */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the In Profile Action table.                         */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServDataPathId                    */
/*                    : INT4 i4FsDiffServDataPathId                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidInProfileActionId (INT4 i4FsDiffServInProfileActionId,
                                        INT4
                                        *pi4NextFsDiffServInProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServInProfileActionEntry *pDiffServInProActEntry = NULL;
    tDiffServInProfileActionEntry *pNextDiffServInProActEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_INPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServInProActEntry = (tDiffServInProfileActionEntry *) pSllNode;

        if (i4FsDiffServInProfileActionId <
            pDiffServInProActEntry->i4DsInProfileActionId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServInProActEntry = pDiffServInProActEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServInProActEntry->i4DsInProfileActionId >
                    pDiffServInProActEntry->i4DsInProfileActionId)
                {
                    pNextDiffServInProActEntry = pDiffServInProActEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_INPROACT_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServInProfileActionId =
            pNextDiffServInProActEntry->i4DsInProfileActionId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsGetMFClfrEntry                                     */
/*                                                                           */
/* Description        : This function is called for getting the Multi Field  */
/*                      Clfr Entry for a given MF Clfr Id.                   */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServMultiFieldClfrId                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MF Clfr Entry   - On success                         */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServMultiFieldClfrEntry *
DsGetMFClfrEntry (INT4 i4DiffServMultiFieldClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDiffServMFClfrEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if (pDiffServMFClfrEntry->i4DsMultiFieldClfrId ==
            i4DiffServMultiFieldClfrId)
        {
            return pDiffServMFClfrEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetClfrEntry                                       */
/*                                                                           */
/* Description        : This function is called for getting the Classifier   */
/*                      Entry for a given Clfr Id.                           */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServClfrId                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Classifier Entry - On success                        */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServClfrEntry *
DsGetClfrEntry (INT4 i4DiffServClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDiffServClfrEntry->i4DsClfrId == i4DiffServClfrId)
        {
            return pDiffServClfrEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetInProfileActionEntry                            */
/*                                                                           */
/* Description        : This function is called for getting the In Profile   */
/*                      Action Entry for a given In Profile Action Id.       */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServInProfileActionId                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : In Profile Action Entry - On success                 */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServInProfileActionEntry *
DsGetInProfileActionEntry (INT4 i4DiffServInProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServInProfileActionEntry *pDiffServInProActEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_INPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServInProActEntry = (tDiffServInProfileActionEntry *) pSllNode;

        if (pDiffServInProActEntry->i4DsInProfileActionId
            == i4DiffServInProfileActionId)
        {
            return pDiffServInProActEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_INPROACT_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsQualifyMFClfrData                                  */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Enry for MF classifier Table.                        */
/*                                                                           */
/* Input(s)           : tDiffServMultiFieldClfrEntry **ppDsMFClfrEntry       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyMFClfrData (tDiffServMultiFieldClfrEntry * pDsMFClfrEntry)
{
    tIssL2FilterEntry  *pDsMacFilter = NULL;
    tIssL3FilterEntry  *pDsIpFilter = NULL;
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pMFClfrList = NULL;

    if (pDsMFClfrEntry != NULL)
    {
        if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_MAC_FILTER)
        {
            pDsMacFilter = IssGetL2FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsMacFilter == NULL)
            {
                return DS_FAILURE;
            }
            if (pDsMacFilter->IssL2FilterAction != ISS_ALLOW)
            {
                return DS_FAILURE;
            }
            if (pDsMacFilter->u1IssL2FilterStatus != DS_ACTIVE)
            {
                return DS_FAILURE;
            }
        }
        else if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_IP_FILTER)
        {
            pDsIpFilter = IssGetL3FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsIpFilter == NULL)
            {
                return DS_FAILURE;
            }
            if (pDsIpFilter->IssL3FilterAction != ISS_ALLOW)
            {
                return DS_FAILURE;
            }
            if (pDsIpFilter->u1IssL3FilterStatus != DS_ACTIVE)
            {
                return DS_FAILURE;
            }
        }

        /* Need to verify if the same filter is already used by 
         * some other multi-field classifier */
        pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);
        while (pSllNode != NULL)
        {
            pMFClfrList = (tDiffServMultiFieldClfrEntry *) pSllNode;

            if (pDsMFClfrEntry->i4DsMultiFieldClfrId ==
                pMFClfrList->i4DsMultiFieldClfrId)
            {
                pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
                continue;
            }

            if ((pMFClfrList->u1DsMultiFieldClfrFilterType ==
                 pDsMFClfrEntry->u1DsMultiFieldClfrFilterType) &&
                (pMFClfrList->u4DsMultiFieldClfrFilterId ==
                 pDsMFClfrEntry->u4DsMultiFieldClfrFilterId))
            {
                return DS_FAILURE;
            }

            pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
        }

    }
    pDsMFClfrEntry->u1DsMultiFieldClfrStatus = DS_NOT_IN_SERVICE;
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsCheckMFClfrDependency                              */
/*                                                                           */
/* Description        : This function is called for checking the dependency  */
/*                      for MF classifier Entry.                             */
/*                                                                           */
/* Input(s)           : INT4 i4DsMFClfrId                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsCheckMFClfrDependency (INT4 i4DsMFClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    /* Go through the classifier table and check with
     * each active entry if this MFClfr is there*/
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (pDsClfrEntry->i4DsClfrMFClfrId == i4DsMFClfrId)
            {
                return DS_FAILURE;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifyClfrData                                    */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Entry for classifier Table.                          */
/*                                                                           */
/* Input(s)           : tDiffServMultiFieldClfrEntry **ppDsMFClfrEntry       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyClfrData (tDiffServClfrEntry * pDsClfrEntry)
{
    tDiffServInProfileActionEntry *pDsClfrInProActEntry = NULL;
    tDiffServTCEntry   *pDsTCEntry = NULL;

    /* Check for the existence of active entries for
     * InProfile Action, OutProfile Action  */
    if (pDsClfrEntry->i4DsClfrInProActionId != 0)
    {
        pDsClfrInProActEntry = DsGetInProfileActionEntry
            (pDsClfrEntry->i4DsClfrInProActionId);
        if (pDsClfrInProActEntry != NULL)
        {
            if (pDsClfrInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE)
            {
                return DS_FAILURE;
            }
        }
    }

    if (pDsClfrEntry->i4DsClfrTrafficClassId != 0)
    {
        pDsTCEntry = DS_TC_ENTRY (pDsClfrEntry->i4DsClfrTrafficClassId);

        if (pDsTCEntry != NULL)
        {
            if (pDsTCEntry->u4EntryStatus != DS_ACTIVE)
            {
                return DS_FAILURE;
            }
        }
    }

    /* Move the RowStatus to NOT_IN_SERVICE */
    pDsClfrEntry->u1DsClfrStatus = DS_NOT_IN_SERVICE;

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifyInProfileActionData                         */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Entry for In Profile Action Table.                   */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyInProfileActionData (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UINT4               u4InProActDefMask = 0x01;
    UINT4               u4InProActVal = 0;
    INT4                i4Count;

    if (pDsInProActEntry != NULL)
    {
        /* Check for all the mandatory objects */
        for (i4Count = DS_INPROACT_FLAGS - 1; i4Count >= 0; i4Count--)
        {
            u4InProActVal = pDsInProActEntry->u4DsInProfileActionFlag
                & (u4InProActDefMask << i4Count);
            if (u4InProActVal)
            {
                if (DsValiateInProActFunctPtrArr[i4Count].pValFunct
                    (pDsInProActEntry) != DS_SUCCESS)
                {
                    return DS_FAILURE;
                }
            }
        }

        pDsInProActEntry->u1DsInProfileActionStatus = DS_NOT_IN_SERVICE;
        return DS_SUCCESS;
    }
    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsCheckInProfileActionDependency                     */
/*                                                                           */
/* Description        : This function is called for checking the dependency  */
/*                      for In Profile Action Entry.                         */
/*                                                                           */
/* Input(s)           : INT4 i4DsInProActId                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsCheckInProfileActionDependency (INT4 i4DsInProActId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    /* Go through the classifier table and check with each 
     * active entry if this In Profile Action Entry is there */
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (pDsClfrEntry->i4DsClfrInProActionId == i4DsInProActId)
            {
                return DS_FAILURE;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return DS_SUCCESS;
}

/* Routines to validate the In Profile Action */

/*****************************************************************************/
/* Function Name      : DsValInProActInsertPrio                              */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert Proirity                                      */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertPrio (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActSetCOSQueue                             */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      SetCOSQueue.                                         */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActSetCOSQueue (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertTOS                               */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert TOS.                                          */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertTOS (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActCopyToCPU                               */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Copy To CPU.                                         */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActCopyToCPU (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActDoNotSwitch                             */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Do Not Switch.                                       */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActDoNotSwitch (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActSetOutPortUCast                         */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      SetOutPortUCast.                                     */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActSetOutPortUCast (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    return
        (pDsInProActEntry->u4DsInProfileActionPort) ? DS_SUCCESS : DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsValInProActCopyToMirror                            */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Copy To Mirror.                                      */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActCopyToMirror (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActIncrFFPPktCounter                       */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Increment FFP Pkt Counter.                           */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActIncrFFPPktCounter (tDiffServInProfileActionEntry *
                                pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertPrioFromTOS                       */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert Proirity From TOS.                            */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertPrioFromTOS (tDiffServInProfileActionEntry *
                                pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertTOSFromPrio                       */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert TOS from Proirity.                            */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertTOSFromPrio (tDiffServInProfileActionEntry *
                                pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertDSCP                              */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert DSCP.                                         */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertDSCP (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActSetOutPortNonUCast                      */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      SetOutPortNonUCast.                                  */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActSetOutPortNonUCast (tDiffServInProfileActionEntry *
                                 pDsInProActEntry)
{
    return
        (pDsInProActEntry->u4DsInProfileActionPort) ? DS_SUCCESS : DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsValInProActDoSwitch                                */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Do Switch.                                           */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActDoSwitch (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsGetStartedStatus                                   */
/*                                                                           */
/* Description        : This function gets the system status whether its     */
/*                      started or shutdown.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_TRUE     - On success                             */
/*                      DS_FALSE    - On Failure                             */
/*****************************************************************************/
INT4
DsGetStartedStatus ()
{
    if (gDsGlobalInfo.DsSystemControl == DS_START)
    {
        return DS_TRUE;
    }
    else
    {
        return DS_FALSE;
    }
}

/*****************************************************************************/
/* Function Name      : DsCheckiffservPort                                   */
/*                                                                           */
/* Description        : This function checks whether the given port is a     */
/*                      Diffsrv port                                         */
/*                                                                           */
/* Input(s)           : i4TrunkPort - Trunk Port                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS     - On success                          */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckDiffservPort (INT4 i4TrunkPort)
{
    tIssL2FilterEntry  *pDsMacFilter = NULL;
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl == DS_SHUTDOWN)
    {
        return DS_SUCCESS;
    }

    DFS_LOCK ();

    /* Scan through the Multifield Clfr Table */
    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_MAC_FILTER)
        {
            pDsMacFilter = IssGetL2FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsMacFilter != NULL)
            {
                if ((i4TrunkPort == pDsMacFilter->i4IssL2FilterInPort) ||
                    (i4TrunkPort == pDsMacFilter->i4IssL2FilterOutPort))
                {
                    DS_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                 "Diffsrv port %d should not be LACP enabled \r\n",
                                 i4TrunkPort);

                    DFS_UNLOCK ();
                    return DS_FAILURE;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    DFS_UNLOCK ();
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsCheckMFClfrTable                                   */
/*                                                                           */
/* Description        : This function checks whether the given filter is     */
/*                      being used by any class-map. This is called from the */
/*                      ISS module when the filter is about to be deleted.   */
/*                                                                           */
/*                      NOTE: DO NOT CALL THIS ROUTINE INTERNALLY FROM QOS   */
/*                            SINCE IT TAKES THE DFS LOCK.                   */
/*                                                                           */
/* Input(s)           : i4FilterNo, i4FilterType                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckMFClfrTable (INT4 i4FilterNo, INT4 i4FilterType)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFCEntry = NULL;

    DFS_LOCK ();
    /* Scan through the Multifield Clfr Table and check whether this filter 
       is being used by any class-map */
    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsMFCEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if ((pDsMFCEntry->u4DsMultiFieldClfrFilterId == (UINT4) i4FilterNo) &&
            (pDsMFCEntry->u1DsMultiFieldClfrFilterType == (UINT1) i4FilterType))
        {
            DFS_UNLOCK ();
            return DS_FAILURE;
        }
        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    DFS_UNLOCK ();
    return DS_SUCCESS;
}

/* *********** CXE specific routines ************** */

/*****************************************************************************/
/* Function Name      : DsWfhbdInfoInit                                      */
/*                                                                           */
/* Description        : This function is called when Diffserv module is      */
/*                      started. It allocates memory pools for the WFHBD     */
/*                      tables in the DiffServ module. It initialises the    */
/*                      the Global structure gDsCxeInfo.                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCxeDsInfo                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gCxeDsInfo                                           */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsWfhbdInfoInit (VOID)
{
    UINT2               u2Portnum;
    INT4                i4QPrio;
    UINT4               u4DSCP;
    tDsFreeFlowGroupInfo *pDsFreeFlowGroup = NULL;

    DS_MEMSET (&(DS_CXE_INFO ()), 0, sizeof (tDsCxeInfo));

    /* Allocate memory pool for Traffic class Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_TRAFFICCLASS_MEMBLK_SIZE,
                                  DS_TRAFFICCLASS_MEMBLK_COUNT,
                                  &(DS_TRAFFICCLASS_MEMPOOL_ID))
        != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC,
                "Traffic Class Table Memory Pool Creation FAILED\n");
        DsWfhbdInfoDeInit ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Random Early Detect/Discard Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_RED_MEMBLK_SIZE,
                                  DS_RED_MEMBLK_COUNT,
                                  &(DS_RED_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC, "RED Table Memory Pool Creation FAILED\n");
        DsWfhbdInfoDeInit ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Port Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_PORT_MEMBLK_SIZE,
                                  DS_PORT_MEMBLK_COUNT,
                                  &(DS_PORT_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC, "Port Table Memory Pool Creation FAILED\n");
        DsWfhbdInfoDeInit ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Free Flow Group Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_FREE_FLOWGROUP_MEMBLK_SIZE,
                                  DS_FREE_FLOWGROUP_MEMBLK_COUNT,
                                  &(DS_FREE_FLOWGROUP_MEMPOOL_ID))
        != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC, "Free flow goup Memory Pool Creation FAILED\n");
        DsWfhbdInfoDeInit ();
        return DS_FAILURE;
    }

    /* Initialize the free flow group list */
    DS_SLL_INIT (&(DS_FREE_FLOWGROUP_LIST));
    (DS_CXE_INFO ()).u4NumFreeFlowGroups = 2048;

    /* Create the first node in the list and mark all 2048 flow groups
     * as free */
    if (DS_FREEFLOWGROUP_ALLOC_MEM_BLOCK (&pDsFreeFlowGroup) != DS_MEM_SUCCESS)
    {
        DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                "No Free Flow group entry \n");
        DsWfhbdInfoDeInit ();
        return SNMP_FAILURE;
    }

    DS_MEMSET (pDsFreeFlowGroup, 0, sizeof (tDsFreeFlowGroupInfo));
    pDsFreeFlowGroup->u4FirstFlowGroup = 1;
    pDsFreeFlowGroup->u4NumFreeFlowGroups = 2048;

    DS_SLL_ADD (&(DS_FREE_FLOWGROUP_LIST), &(pDsFreeFlowGroup->DsNextNode));

    /* Initialize the Q Priority precedence and the Dscp-QPriority mapping */
    (DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched = 5;
    (DS_CXE_INFO ()).u4IPTOSQPrecdncRouted = 6;
    (DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched = 6;
    (DS_CXE_INFO ()).u4Dot1PQPrecdncRouted = 5;

    /* Initialize the DSCP to QPriio mapping */
    i4QPrio = -1;
    for (u4DSCP = 0; u4DSCP < 64; u4DSCP++)
    {
        if (u4DSCP % 8 == 0)
        {
            ++i4QPrio;
        }
        DS_DSCP_TO_QMAP (u4DSCP) = i4QPrio;
    }

    /* Initialize the port QoS entries */

    for (u2Portnum = 1; u2Portnum <= DS_MAX_PORTS; u2Portnum++)
    {
        DS_PORTENTRY_ALLOC_MEM_BLOCK (&(DS_PORT_ENTRY (u2Portnum)));

        (DS_PORT_ENTRY (u2Portnum))->u4DropAtMaxBw = DS_FALSE;

        (DS_PORT_ENTRY (u2Portnum))->u4MaxBw = DS_DEF_PORT_MAX_BW;

        /* The following are set to the default values in the hardware */

        /* DropLevel1 - Store 16KB beyond the MaxBw (1Gbps) rate
         * before starting to drop */
        (DS_PORT_ENTRY (u2Portnum))->u4DropLevel1 = DS_MIN_DROP_LEVEL1;
        /* DropLevel2 - 128KB beyond droplevel1 */
        (DS_PORT_ENTRY (u2Portnum))->u4DropLevel2 = DS_MIN_DROP_LEVEL2;
        /* DropLevel3 - 128KB beyond droplevel2 */
        (DS_PORT_ENTRY (u2Portnum))->u4DropLevel3 = DS_MIN_DROP_LEVEL3;

        /* DS_MAX_REDCURVES+1 indicates that RED is disabled on the port */
        (DS_PORT_ENTRY (u2Portnum))->u4REDCurveId = DS_MAX_REDCURVES + 1;
    }

    /* RED curves DS_MIN_REDCURVES-1 and DS_MAX_REDCURVES+1 are used as
     * default curves */
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsWfhbdInfoDeInit                                    */
/*                                                                           */
/* Description        : This function when DiffServ is shutdown or when Init */
/*                      fails . It releases all memory allocated in the      */
/*                      DsWfhbdInfoInit routine.                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsWfhbdInfoDeInit (VOID)
{
    if (DS_TRAFFICCLASS_MEMPOOL_ID != DS_INVALID_VAL)
    {
        if (DS_DELETE_ENTRY_MEM_POOL (DS_TRAFFICCLASS_MEMPOOL_ID) !=
            DS_MEM_SUCCESS)
        {
            DS_TRC (INIT_SHUT_TRC,
                    "Traffic Class Entry Memory Pool Release FAILED\n");
        }
        DS_TRAFFICCLASS_MEMPOOL_ID = DS_INVALID_VAL;
    }
    if (DS_RED_MEMPOOL_ID != DS_INVALID_VAL)
    {
        if (DS_DELETE_ENTRY_MEM_POOL (DS_RED_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {
            DS_TRC (INIT_SHUT_TRC, "RED Entry Memory Pool Release FAILED\n");
        }
        DS_RED_MEMPOOL_ID = DS_INVALID_VAL;
    }
    if (DS_PORT_MEMPOOL_ID != DS_INVALID_VAL)
    {
        if (DS_DELETE_ENTRY_MEM_POOL (DS_PORT_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {
            DS_TRC (INIT_SHUT_TRC, "Port Entry Memory Pool Release FAILED\n");
        }
        DS_PORT_MEMPOOL_ID = DS_INVALID_VAL;
    }
    if (DS_FREE_FLOWGROUP_MEMPOOL_ID != DS_INVALID_VAL)
    {
        if (DS_DELETE_ENTRY_MEM_POOL (DS_FREE_FLOWGROUP_MEMPOOL_ID) !=
            DS_MEM_SUCCESS)
        {
            DS_TRC (INIT_SHUT_TRC,
                    "Free flow group Memory Pool Release FAILED\n");
        }
        DS_FREE_FLOWGROUP_MEMPOOL_ID = DS_INVALID_VAL;
    }

    return DS_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : DsConfigWfhbdInfoInHw                                */
/*                                                                           */
/* Description        : This function is called whenever the Diffserv module */
/*                      is enabled. It programs all WFHBD configurations     */
/*                      available in the software onto the hardware.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCxeDsInfo                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gCxeDsInfo                                           */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsConfigWfhbdInfoInHw (VOID)
{
    UINT4               u4Index;
    INT4                i4RetVal = DS_SUCCESS;
    BOOL1               bResult;
    UINT4               u4DSCP;
    INT4                i4QPrio;
    tDiffServTCEntry   *pDsTCEntry = NULL;
    tDiffServPortEntry *pDsPortEntry = NULL;
    tDiffServREDEntry  *pDsREDEntry = NULL;

    /* Enable WFHBD on all ports */
    if (DsHwEnableWfhbdOnAllPorts () == FNP_FAILURE)
    {
        DS_TRC (INIT_SHUT_TRC,
                "SYS: Hw Wfhbd Enable on all ports FAILED!!! \n");
        return DS_FAILURE;
    }

    /* Initialize the Q Priority precedence and the Dscp-QPriority mapping */

    if ((DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched != 5)
    {
        if (DsHwSetIPTOSQPrecedence ((DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched,
                                     DS_NP_PREC_SWITCHED) == FNP_FAILURE)
        {
            return DS_FAILURE;
        }
    }

    if ((DS_CXE_INFO ()).u4IPTOSQPrecdncRouted != 6)
    {
        if (DsHwSetIPTOSQPrecedence ((DS_CXE_INFO ()).u4IPTOSQPrecdncRouted,
                                     DS_NP_PREC_ROUTED) == FNP_FAILURE)
        {
            return DS_FAILURE;
        }
    }

    if ((DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched != 6)
    {
        if (DsHwSetDot1PQPrecedence ((DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched,
                                     DS_NP_PREC_SWITCHED) == FNP_FAILURE)
        {
            return DS_FAILURE;
        }
    }

    if ((DS_CXE_INFO ()).u4Dot1PQPrecdncRouted != 5)
    {
        if (DsHwSetDot1PQPrecedence ((DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched,
                                     DS_NP_PREC_ROUTED) == FNP_FAILURE)
        {
            return DS_FAILURE;
        }
    }

    i4QPrio = -1;
    for (u4DSCP = 0; u4DSCP < 64; u4DSCP++)
    {
        if (u4DSCP % 8 == 0)
        {
            ++i4QPrio;
        }
        if (DS_DSCP_TO_QMAP (u4DSCP) != i4QPrio)
        {
            if (DsHwSetDSCPQPrioMap (u4DSCP, DS_DSCP_TO_QMAP (u4DSCP)) ==
                FNP_FAILURE)
            {
                DS_TRC (INIT_SHUT_TRC,
                        "SYS: Hw DSCP to Queue mapping set FAILED!!! \n");
                i4RetVal = DS_FAILURE;
            }
        }
    }

    /* Loop thru the RED curves and set the appropriate parameters */
    for (u4Index = DS_MIN_REDCURVES; u4Index <= DS_MAX_REDCURVES; u4Index++)
    {
        pDsREDEntry = DS_RED_ENTRY (u4Index);
        if (pDsREDEntry == NULL)
        {
            continue;
        }
        if (pDsREDEntry->u4REDCurveStatus == DS_ACTIVE)
        {
            if (DsHwSetREDParams
                (u4Index, pDsREDEntry, DS_NP_RED_CURVE_ENABLE) == FNP_FAILURE)
            {
                DS_TRC (INIT_SHUT_TRC, "SYS: Hw RED Params set FAILED!!! \n");
                i4RetVal = DS_FAILURE;
            }
        }
    }

    /* Loop thru all ports on which QoS parameters have been configured and
     * set default values for all of them */
    for (u4Index = 1; u4Index <= DS_MAX_PORTS; u4Index++)
    {
        DS_IS_PORT_DIRTY (u4Index, bResult);

        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        pDsPortEntry = DS_PORT_ENTRY (u4Index);

        if (pDsPortEntry == NULL)
        {
            continue;
        }

        if (DsHwSetPortParams
            (u4Index, pDsPortEntry, DS_NP_PORT_PARAM_SET_ALL) == FNP_FAILURE)
        {
            DS_TRC (INIT_SHUT_TRC, "SYS: Hw Port Params set FAILED!!! \n");
            i4RetVal = DS_FAILURE;
        }
    }

    /* Loop thru the active Traffic classes and set the appropriate
     * parameters */
    for (u4Index = DS_NUM_RESERVED_TRAFFIC_CLASSES + 1;
         u4Index <= DS_MAX_TC; u4Index++)
    {
        pDsTCEntry = DS_TC_ENTRY (u4Index);
        if (pDsTCEntry == NULL)
        {
            continue;
        }
        if (pDsTCEntry->u4EntryStatus == DS_ACTIVE)
        {
            if (DsHwSetTCParams
                (u4Index, pDsTCEntry, DS_NP_TC_PARAM_SET_ALL) == FNP_FAILURE)
            {
                DS_TRC (INIT_SHUT_TRC,
                        "SYS: Hw Traffic-class Params set FAILED!!! \n");
                i4RetVal = DS_FAILURE;
            }
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : DsDisableWfhbdInfoInHw                               */
/*                                                                           */
/* Description        : This function is called whenever the Diffserv module */
/*                      is disabled. It removes all WFHBD configurations made*/
/*                      and programs the default values in the Hardware.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCxeDsInfo                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gCxeDsInfo                                           */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsDisableWfhbdInfoInHw (VOID)
{
    INT4                i4RetVal = DS_SUCCESS;
    UINT4               u4Index;
    UINT4               u4DSCP;
    INT4                i4QPrio;
    BOOL1               bResult;
    tDiffServREDEntry  *pDsREDEntry = NULL;
    tDiffServTCEntry   *pDsTCEntry = NULL;
    tDiffServPortEntry *pDsPortEntry = NULL;

    /* Loop thru the non-reserved Traffic classes and set default values 
     * for all of them */
    for (u4Index = DS_NUM_RESERVED_TRAFFIC_CLASSES + 1;
         u4Index <= DS_MAX_TC; u4Index++)
    {
        pDsTCEntry = DS_TC_ENTRY (u4Index);

        if (pDsTCEntry == NULL)
        {
            continue;
        }
        if (pDsTCEntry->u4EntryStatus == DS_ACTIVE)
        {
            if (DsHwSetTCParams
                (u4Index, pDsTCEntry, DS_NP_TC_PARAM_SET_DEFAULT_ALL)
                == FNP_FAILURE)
            {
                i4RetVal = DS_FAILURE;
            }
        }
    }

    /* Loop thru all ports on which QoS parameters have been configured and
     * set default values for all of them */
    for (u4Index = 1; u4Index <= DS_MAX_PORTS; u4Index++)
    {
        DS_IS_PORT_DIRTY (u4Index, bResult);
        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        pDsPortEntry = DS_PORT_ENTRY (u4Index);

        if (pDsPortEntry == NULL)
        {
            continue;
        }
        if (DsHwSetPortParams
            (u4Index, pDsPortEntry,
             DS_NP_PORT_PARAM_SET_DEFAULT_ALL) == FNP_FAILURE)
        {
            i4RetVal = DS_FAILURE;
        }
    }

    /* Loop thru the RED curves and set all of them as disabled */
    for (u4Index = DS_MIN_REDCURVES; u4Index <= DS_MAX_REDCURVES; u4Index++)
    {
        pDsREDEntry = DS_RED_ENTRY (u4Index);

        if (pDsREDEntry == NULL)
        {
            continue;
        }
        if (pDsREDEntry->u4REDCurveStatus == DS_ACTIVE)
        {
            if (DsHwSetREDParams
                (u4Index, pDsREDEntry, DS_NP_RED_CURVE_DISABLE) == FNP_FAILURE)
            {
                i4RetVal = DS_FAILURE;
            }
        }
    }

    /* Initialize the Q Priority precedence and the Dscp-QPriority mapping */
    if ((DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched != 5)
    {
        if (DsHwSetIPTOSQPrecedence (5, DS_NP_PREC_SWITCHED) == FNP_FAILURE)
        {
            i4RetVal = DS_FAILURE;
        }
    }

    if ((DS_CXE_INFO ()).u4IPTOSQPrecdncRouted != 6)
    {
        if (DsHwSetIPTOSQPrecedence (6, DS_NP_PREC_ROUTED) == FNP_FAILURE)
        {
            i4RetVal = DS_FAILURE;
        }
    }

    if ((DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched != 6)
    {
        if (DsHwSetDot1PQPrecedence (6, DS_NP_PREC_SWITCHED) == FNP_FAILURE)
        {
            i4RetVal = DS_FAILURE;
        }
    }

    if ((DS_CXE_INFO ()).u4Dot1PQPrecdncRouted != 5)
    {
        if (DsHwSetDot1PQPrecedence (5, DS_NP_PREC_ROUTED) == FNP_FAILURE)
        {
            i4RetVal = DS_FAILURE;
        }
    }

    i4QPrio = -1;
    for (u4DSCP = 0; u4DSCP < 64; u4DSCP++)
    {
        if (u4DSCP % 8 == 0)
        {
            ++i4QPrio;
        }
        if (DS_DSCP_TO_QMAP (u4DSCP) != i4QPrio)
        {
            if (DsHwSetDSCPQPrioMap (u4DSCP, i4QPrio) == FNP_FAILURE)
            {
                i4RetVal = DS_FAILURE;
            }
        }
    }

    /* Disable WFHBD on all ports */
    if (DsHwDisableWfhbdOnAllPorts () == FNP_FAILURE)
    {
        DS_TRC (INIT_SHUT_TRC,
                "SYS: Hw Wfhbd Disable on all ports FAILED!!! \n");
        i4RetVal = DS_FAILURE;
    }

    return i4RetVal;
}
#endif /* NPAPI_WANTED */

/*****************************************************************************/
/* Function Name      : DsCheckTCForREDCurveId                               */
/*                                                                           */
/* Description        : This function is called to check if any of the       */
/*                      Traffic Class entries use the given RED Curve Id.    */
/*                                                                           */
/* Input(s)           : i4REDCurveId                                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCxeDsInfo                                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Traffic Class Id - On success                        */
/*                      0                - On Failure                        */
/*****************************************************************************/
INT4
DsCheckTCForREDCurveId (INT4 i4REDCurveId)
{
    UINT4               u4TCId;

    for (u4TCId = 1; u4TCId <= DS_MAX_TC; u4TCId++)
    {
        if ((DS_TC_ENTRY (u4TCId)) == NULL)
        {
            continue;
        }
        if ((DS_TC_ENTRY (u4TCId))->u4REDCurveId == (UINT4) i4REDCurveId)
        {
            return u4TCId;
        }
    }

    return 0;
}

#ifdef FSAP_SEM_DEBUG
/*****************************************************************************/
/* Function Name      : DfsLock                                              */
/*                                                                           */
/* Description        : This function is used to take the DiffServer mutual  */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : pu1File - File from where the call to this function  */
/*                                is made                                    */
/*                      u4Line  - Line number in pu1File from where the      */
/*                                call to this function is made              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
DfsLockDbg (UINT1 *pu1File, UINT4 u4Line)
{
    if (OsixTakeSemDbg (SELF, DFS_MUT_EXCL_SEM_NAME,
                        OSIX_WAIT, 0, pu1File, u4Line) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "Take FAILED\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
#endif /* FSAP_SEM_DEBUG */

/*****************************************************************************/
/* Function Name      : DfsLock                                              */
/*                                                                           */
/* Description        : This function is used to take the DiffServer mutual  */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
DfsLock (VOID)
{
    if (OsixSemTake (DFS_MUT_EXCL_SEM_ID) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "Take FAILED\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DfsUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the Diff Server mutual */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
DfsUnLock (VOID)
{
    if (OsixSemGive (DFS_MUT_EXCL_SEM_ID) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "Take FAILED\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

#endif /* _DSCXESYS_C */
