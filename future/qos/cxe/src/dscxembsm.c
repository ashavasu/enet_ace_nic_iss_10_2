
#include "dscxeinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmDiffservCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the Diffserv HW configuration when   */
/*                a card is inserted into the MBSM System for the            */
/*                distributed architecture                                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmDiffservCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pPortInfo);

    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    tDiffServMeterEntry *pDsMeterEntry = NULL;

    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        /* Call the routine to Initialise the diffsrv in h/w */
        if (DsMbsmHwInit (pSlotInfo) != FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }
    else
    {
        return MBSM_SUCCESS;
    }

    TMO_SLL_Scan (&DS_CLFR_LIST, pDsClfrEntry, tDiffServClfrEntry *)
    {

        if (pDsClfrEntry->u1DsClfrStatus != DS_ACTIVE)
        {
            continue;
        }
        else
        {

            /* Get the Other Elements from the list here */
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

            if (pDsMFClfrEntry == NULL)
            {
                continue;
            }
            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);
            pDsOutProActEntry = DsGetOutProfileActionEntry
                (pDsClfrEntry->i4DsClfrOutProActionId);
            if (pDsOutProActEntry != NULL)
            {
                pDsMeterEntry = DsGetMeterEntry
                    (pDsOutProActEntry->i4DsOutProfileActionMID);
            }

            /* Call the hardware routine here */
            if (DsMbsmHwClassifierAdd (pDsClfrEntry, pDsMFClfrEntry,
                                       pDsInProActEntry, pDsOutProActEntry,
                                       pDsMeterEntry, pSlotInfo) != FNP_SUCCESS)
            {
                return MBSM_FAILURE;
            }
        }

    }

    TMO_SLL_Scan (&DS_SCHEDULER_LIST, pDsSchedulerEntry,
                  tDiffServSchedulerEntry *)
    {

        if (pDsSchedulerEntry->u1DsSchedulerStatus != DS_ACTIVE)
        {
            continue;
        }

        else
        {
            /* Call the hardware routine here */
            if (DsMbsmHwSchedulerAdd (pDsSchedulerEntry, pSlotInfo) !=
                FNP_SUCCESS)
            {
                return DS_FAILURE;
            }
        }

    }
    return MBSM_SUCCESS;
}
