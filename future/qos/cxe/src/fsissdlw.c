/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissdlw.c,v 1.10 2008/12/30 04:57:56 premap-iss Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "dscxeinc.h"
# include  "dscxecli.h"

extern UINT4        fsissd[8];
extern UINT4        FsDsSystemControl[11];
PRIVATE VOID        DsNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsSystemControl
 Input       :  The Indices

                The Object 
                retValFsDsSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsSystemControl (INT4 *pi4RetValFsDsSystemControl)
{
    *pi4RetValFsDsSystemControl = gDsGlobalInfo.DsSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsStatus
 Input       :  The Indices

                The Object 
                retValFsDsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsStatus (INT4 *pi4RetValFsDsStatus)
{
    *pi4RetValFsDsStatus = gDsGlobalInfo.DsStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsIPTOSQPrecedenceSwitched
 Input       :  The Indices

                The Object 
                retValFsDsIPTOSQPrecedenceSwitched
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsIPTOSQPrecedenceSwitched (INT4 *pi4RetValFsDsIPTOSQPrecedenceSwitched)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pi4RetValFsDsIPTOSQPrecedenceSwitched = 0;

        return SNMP_SUCCESS;
    }

    *pi4RetValFsDsIPTOSQPrecedenceSwitched =
        (DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsIPTOSQPrecedenceRouted
 Input       :  The Indices

                The Object 
                retValFsDsIPTOSQPrecedenceRouted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsIPTOSQPrecedenceRouted (INT4 *pi4RetValFsDsIPTOSQPrecedenceRouted)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pi4RetValFsDsIPTOSQPrecedenceRouted = 0;

        return SNMP_SUCCESS;
    }

    *pi4RetValFsDsIPTOSQPrecedenceRouted =
        (DS_CXE_INFO ()).u4IPTOSQPrecdncRouted;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsDot1PQPrecedenceSwitched
 Input       :  The Indices

                The Object 
                retValFsDsDot1PQPrecedenceSwitched
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsDot1PQPrecedenceSwitched (INT4 *pi4RetValFsDsDot1PQPrecedenceSwitched)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pi4RetValFsDsDot1PQPrecedenceSwitched = 0;

        return SNMP_SUCCESS;
    }

    *pi4RetValFsDsDot1PQPrecedenceSwitched =
        (DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsDot1PQPrecedenceRouted
 Input       :  The Indices

                The Object 
                retValFsDsDot1PQPrecedenceRouted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsDot1PQPrecedenceRouted (INT4 *pi4RetValFsDsDot1PQPrecedenceRouted)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pi4RetValFsDsDot1PQPrecedenceRouted = 0;

        return SNMP_SUCCESS;
    }

    *pi4RetValFsDsDot1PQPrecedenceRouted =
        (DS_CXE_INFO ()).u4Dot1PQPrecdncRouted;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWFHBDOfferedPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumWFHBDOfferedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWFHBDOfferedPkts (UINT4 *pu4RetValFsDsNumWFHBDOfferedPkts)
{
    *pu4RetValFsDsNumWFHBDOfferedPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_OFFERED_PKTS,
                             pu4RetValFsDsNumWFHBDOfferedPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWFHBDAcceptedPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumWFHBDAcceptedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWFHBDAcceptedPkts (UINT4 *pu4RetValFsDsNumWFHBDAcceptedPkts)
{
    *pu4RetValFsDsNumWFHBDAcceptedPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_ACCEPTED_PKTS,
                             pu4RetValFsDsNumWFHBDAcceptedPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWFHBDRejectedPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumWFHBDRejectedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWFHBDRejectedPkts (UINT4 *pu4RetValFsDsNumWFHBDRejectedPkts)
{
    *pu4RetValFsDsNumWFHBDRejectedPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_REJECTED_PKTS,
                             pu4RetValFsDsNumWFHBDRejectedPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWFHBDOfferedBytes
 Input       :  The Indices

                The Object 
                retValFsDsNumWFHBDOfferedBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWFHBDOfferedBytes (UINT4 *pu4RetValFsDsNumWFHBDOfferedBytes)
{
    *pu4RetValFsDsNumWFHBDOfferedBytes = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_OFFERED_BYTES,
                             pu4RetValFsDsNumWFHBDOfferedBytes) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWFHBDAcceptedBytes
 Input       :  The Indices

                The Object 
                retValFsDsNumWFHBDAcceptedBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWFHBDAcceptedBytes (UINT4 *pu4RetValFsDsNumWFHBDAcceptedBytes)
{
    *pu4RetValFsDsNumWFHBDAcceptedBytes = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_ACCEPTED_BYTES,
                             pu4RetValFsDsNumWFHBDAcceptedBytes) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWFHBDRejectedBytes
 Input       :  The Indices

                The Object 
                retValFsDsNumWFHBDRejectedBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWFHBDRejectedBytes (UINT4 *pu4RetValFsDsNumWFHBDRejectedBytes)
{
    *pu4RetValFsDsNumWFHBDRejectedBytes = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_REJECTED_BYTES,
                             pu4RetValFsDsNumWFHBDRejectedBytes) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWrongPortAcceptedPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumWrongPortAcceptedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWrongPortAcceptedPkts (UINT4
                                    *pu4RetValFsDsNumWrongPortAcceptedPkts)
{
    *pu4RetValFsDsNumWrongPortAcceptedPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_ACCEPT_WRONGPORT_PKTS,
                             pu4RetValFsDsNumWrongPortAcceptedPkts) ==
        FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumWrongPortRejectedPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumWrongPortRejectedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumWrongPortRejectedPkts (UINT4
                                    *pu4RetValFsDsNumWrongPortRejectedPkts)
{
    *pu4RetValFsDsNumWrongPortRejectedPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_DISCARD_WRONGPORT_PKTS,
                             pu4RetValFsDsNumWrongPortRejectedPkts) ==
        FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumAcceptFairGroupPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumAcceptFairGroupPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumAcceptFairGroupPkts (UINT4 *pu4RetValFsDsNumAcceptFairGroupPkts)
{
    *pu4RetValFsDsNumAcceptFairGroupPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_ACCEPT_FAIR_GROUP_PKTS,
                             pu4RetValFsDsNumAcceptFairGroupPkts) ==
        FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumDiscardUnfairGroupPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumDiscardUnfairGroupPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumDiscardUnfairGroupPkts (UINT4
                                     *pu4RetValFsDsNumDiscardUnfairGroupPkts)
{
    *pu4RetValFsDsNumDiscardUnfairGroupPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_DISCARD_UNFAIR_GROUP_PKTS,
                             pu4RetValFsDsNumDiscardUnfairGroupPkts) ==
        FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumDiscardDropLevel2Pkts
 Input       :  The Indices

                The Object 
                retValFsDsNumDiscardDropLevel2Pkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumDiscardDropLevel2Pkts (UINT4
                                    *pu4RetValFsDsNumDiscardDropLevel2Pkts)
{
    *pu4RetValFsDsNumDiscardDropLevel2Pkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_DISCARD_DROP_LEVEL2_PKTS,
                             pu4RetValFsDsNumDiscardDropLevel2Pkts) ==
        FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumDiscardDropLevel3Pkts
 Input       :  The Indices

                The Object 
                retValFsDsNumDiscardDropLevel3Pkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumDiscardDropLevel3Pkts (UINT4
                                    *pu4RetValFsDsNumDiscardDropLevel3Pkts)
{
    *pu4RetValFsDsNumDiscardDropLevel3Pkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_DISCARD_DROP_LEVEL3_PKTS,
                             pu4RetValFsDsNumDiscardDropLevel3Pkts) ==
        FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumAcceptFairTCPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumAcceptFairTCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumAcceptFairTCPkts (UINT4 *pu4RetValFsDsNumAcceptFairTCPkts)
{
    *pu4RetValFsDsNumAcceptFairTCPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_ACCEPT_FAIR_TC_PKTS,
                             pu4RetValFsDsNumAcceptFairTCPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumAcceptDontCarePkts
 Input       :  The Indices

                The Object 
                retValFsDsNumAcceptDontCarePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumAcceptDontCarePkts (UINT4 *pu4RetValFsDsNumAcceptDontCarePkts)
{
    *pu4RetValFsDsNumAcceptDontCarePkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_ACCEPT_DONTCARE_PKTS,
                             pu4RetValFsDsNumAcceptDontCarePkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumDiscardPortREDPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumDiscardPortREDPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumDiscardPortREDPkts (UINT4 *pu4RetValFsDsNumDiscardPortREDPkts)
{
    *pu4RetValFsDsNumDiscardPortREDPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_DISCARD_PORT_RED_PKTS,
                             pu4RetValFsDsNumDiscardPortREDPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsNumDiscardTCREDPkts
 Input       :  The Indices

                The Object 
                retValFsDsNumDiscardTCREDPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsNumDiscardTCREDPkts (UINT4 *pu4RetValFsDsNumDiscardTCREDPkts)
{
    *pu4RetValFsDsNumDiscardTCREDPkts = 0;

    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetWFHBDCounter (DS_NP_COUNTER_DISCARD_TC_RED_PKTS,
                             pu4RetValFsDsNumDiscardTCREDPkts) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsDsSystemControl
 Input       :  The Indices

                The Object 
                setValFsDsSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsSystemControl (INT4 i4SetValFsDsSystemControl)
{
    if (i4SetValFsDsSystemControl == (INT4) gDsGlobalInfo.DsSystemControl)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "SNMP: DS is already having the same System Control State\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsDsSystemControl == DS_START)
    {
        if (DsStart () != DS_SUCCESS)
        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Start FAILED\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (DsShutDown () != DS_SUCCESS)
        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Shutdown FAILED\n");
            return SNMP_FAILURE;
        }
        /* Notify MSR with the diffserv oids */
        DsNotifyProtocolShutdownStatus ();
    }

    gDsGlobalInfo.DsSystemControl = i4SetValFsDsSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsStatus
 Input       :  The Indices

                The Object 
                setValFsDsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsStatus (INT4 i4SetValFsDsStatus)
{
    if (i4SetValFsDsStatus == (INT4) gDsGlobalInfo.DsStatus)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "SNMP: DS is already having the same System Status\n");
        return SNMP_SUCCESS;
    }
    if (i4SetValFsDsStatus == DS_ENABLE)
    {
        if (DsEnable () != DS_SUCCESS)
        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Enable FAILED\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (DsDisable () != DS_SUCCESS)
        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Disble FAILED\n");
            return SNMP_FAILURE;
        }
    }

    gDsGlobalInfo.DsStatus = i4SetValFsDsStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsIPTOSQPrecedenceSwitched
 Input       :  The Indices

                The Object 
                setValFsDsIPTOSQPrecedenceSwitched
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsIPTOSQPrecedenceSwitched (INT4 i4SetValFsDsIPTOSQPrecedenceSwitched)
{
    if ((DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched ==
        (UINT4) i4SetValFsDsIPTOSQPrecedenceSwitched)
    {
        return SNMP_SUCCESS;
    }

    (DS_CXE_INFO ()).u4IPTOSQPrecdncSwitched =
        (UINT4) i4SetValFsDsIPTOSQPrecedenceSwitched;

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetIPTOSQPrecedence (i4SetValFsDsIPTOSQPrecedenceSwitched,
                                     DS_NP_PREC_SWITCHED) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsIPTOSQPrecedenceRouted
 Input       :  The Indices

                The Object 
                setValFsDsIPTOSQPrecedenceRouted
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsIPTOSQPrecedenceRouted (INT4 i4SetValFsDsIPTOSQPrecedenceRouted)
{
    if ((DS_CXE_INFO ()).u4IPTOSQPrecdncRouted ==
        (UINT4) i4SetValFsDsIPTOSQPrecedenceRouted)
    {
        return SNMP_SUCCESS;
    }

    (DS_CXE_INFO ()).u4IPTOSQPrecdncRouted =
        (UINT4) i4SetValFsDsIPTOSQPrecedenceRouted;

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetIPTOSQPrecedence (i4SetValFsDsIPTOSQPrecedenceRouted,
                                     DS_NP_PREC_ROUTED) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsDot1PQPrecedenceSwitched
 Input       :  The Indices

                The Object 
                setValFsDsDot1PQPrecedenceSwitched
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsDot1PQPrecedenceSwitched (INT4 i4SetValFsDsDot1PQPrecedenceSwitched)
{
    if ((DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched ==
        (UINT4) i4SetValFsDsDot1PQPrecedenceSwitched)
    {
        return SNMP_SUCCESS;
    }

    (DS_CXE_INFO ()).u4Dot1PQPrecdncSwitched =
        (UINT4) i4SetValFsDsDot1PQPrecedenceSwitched;

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetDot1PQPrecedence (i4SetValFsDsDot1PQPrecedenceSwitched,
                                     DS_NP_PREC_SWITCHED) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsDot1PQPrecedenceRouted
 Input       :  The Indices

                The Object 
                setValFsDsDot1PQPrecedenceRouted
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsDot1PQPrecedenceRouted (INT4 i4SetValFsDsDot1PQPrecedenceRouted)
{
    if ((DS_CXE_INFO ()).u4Dot1PQPrecdncRouted ==
        (UINT4) i4SetValFsDsDot1PQPrecedenceRouted)
    {
        return SNMP_SUCCESS;
    }

    (DS_CXE_INFO ()).u4Dot1PQPrecdncRouted =
        (UINT4) i4SetValFsDsDot1PQPrecedenceRouted;

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetDot1PQPrecedence (i4SetValFsDsDot1PQPrecedenceRouted,
                                     DS_NP_PREC_ROUTED) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsDsSystemControl
 Input       :  The Indices

                The Object 
                testValFsDsSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsSystemControl (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsDsSystemControl)
{
    if ((i4TestValFsDsSystemControl == DS_START) ||
        (i4TestValFsDsSystemControl == DS_SHUTDOWN))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDsStatus
 Input       :  The Indices

                The Object 
                testValFsDsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsDsStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDsStatus == DS_ENABLE) ||
        (i4TestValFsDsStatus == DS_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDsIPTOSQPrecedenceSwitched
 Input       :  The Indices

                The Object 
                testValFsDsIPTOSQPrecedenceSwitched
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsIPTOSQPrecedenceSwitched (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsDsIPTOSQPrecedenceSwitched)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Precedence range: 1-6 */
    if ((i4TestValFsDsIPTOSQPrecedenceSwitched < 1) ||
        (i4TestValFsDsIPTOSQPrecedenceSwitched > 6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsIPTOSQPrecedenceRouted
 Input       :  The Indices

                The Object 
                testValFsDsIPTOSQPrecedenceRouted
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsIPTOSQPrecedenceRouted (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsDsIPTOSQPrecedenceRouted)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Precedence range: 1-6 */
    if ((i4TestValFsDsIPTOSQPrecedenceRouted > 0) &&
        (i4TestValFsDsIPTOSQPrecedenceRouted < 7))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDsDot1PQPrecedenceSwitched
 Input       :  The Indices

                The Object 
                testValFsDsDot1PQPrecedenceSwitched
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsDot1PQPrecedenceSwitched (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsDsDot1PQPrecedenceSwitched)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Precedence range: 1-6 */
    if ((i4TestValFsDsDot1PQPrecedenceSwitched > 0) &&
        (i4TestValFsDsDot1PQPrecedenceSwitched < 7))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDsDot1PQPrecedenceRouted
 Input       :  The Indices

                The Object 
                testValFsDsDot1PQPrecedenceRouted
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsDot1PQPrecedenceRouted (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsDsDot1PQPrecedenceRouted)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Precedence range: 1-6 */
    if ((i4TestValFsDsDot1PQPrecedenceRouted > 0) &&
        (i4TestValFsDsDot1PQPrecedenceRouted < 7))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : FsDiffServMultiFieldClfrTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable (INT4
                                                       i4FsDiffServMultiFieldClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (DsValidateMFClfrId (i4FsDiffServMultiFieldClfrId) != DS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServMultiFieldClfrTable (INT4
                                               *pi4FsDiffServMultiFieldClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((DsSnmpLowGetFirstValidMultiFieldClfrId
         (pi4FsDiffServMultiFieldClfrId)) == DS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
                nextFsDiffServMultiFieldClfrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServMultiFieldClfrTable (INT4 i4FsDiffServMultiFieldClfrId,
                                              INT4
                                              *pi4NextFsDiffServMultiFieldClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (i4FsDiffServMultiFieldClfrId < 0)
    {
        return SNMP_FAILURE;
    }

    if ((DsSnmpLowGetNextValidMultiFieldClfrId
         (i4FsDiffServMultiFieldClfrId,
          pi4NextFsDiffServMultiFieldClfrId)) == DS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServMultiFieldClfrFilterId
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                retValFsDiffServMultiFieldClfrFilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMultiFieldClfrFilterId (INT4 i4FsDiffServMultiFieldClfrId,
                                        UINT4
                                        *pu4RetValFsDiffServMultiFieldClfrFilterId)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing"
                "its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);

    if (pDsMultiFieldClfrEntry != NULL)
    {
        *pu4RetValFsDiffServMultiFieldClfrFilterId =
            pDsMultiFieldClfrEntry->u4DsMultiFieldClfrFilterId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServMultiFieldClfrFilterType
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                retValFsDiffServMultiFieldClfrFilterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMultiFieldClfrFilterType (INT4 i4FsDiffServMultiFieldClfrId,
                                          INT4
                                          *pi4RetValFsDiffServMultiFieldClfrFilterType)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing"
                "its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);

    if (pDsMultiFieldClfrEntry != NULL)
    {
        *pi4RetValFsDiffServMultiFieldClfrFilterType =
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrFilterType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServMultiFieldClfrStatus
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                retValFsDiffServMultiFieldClfrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMultiFieldClfrStatus (INT4 i4FsDiffServMultiFieldClfrId,
                                      INT4
                                      *pi4RetValFsDiffServMultiFieldClfrStatus)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);

    if (pDsMultiFieldClfrEntry != NULL)
    {
        *pi4RetValFsDiffServMultiFieldClfrStatus =
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServMultiFieldClfrFilterId
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                setValFsDiffServMultiFieldClfrFilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMultiFieldClfrFilterId (INT4 i4FsDiffServMultiFieldClfrId,
                                        UINT4
                                        u4SetValFsDiffServMultiFieldClfrFilterId)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);

    if ((pDsMultiFieldClfrEntry != NULL) &&
        (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus != DS_ACTIVE))
    {
        pDsMultiFieldClfrEntry->u4DsMultiFieldClfrFilterId =
            u4SetValFsDiffServMultiFieldClfrFilterId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServMultiFieldClfrFilterType
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                setValFsDiffServMultiFieldClfrFilterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMultiFieldClfrFilterType (INT4 i4FsDiffServMultiFieldClfrId,
                                          INT4
                                          i4SetValFsDiffServMultiFieldClfrFilterType)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);

    if ((pDsMultiFieldClfrEntry != NULL) &&
        (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus != DS_ACTIVE))
    {
        pDsMultiFieldClfrEntry->u1DsMultiFieldClfrFilterType =
            (UINT1) i4SetValFsDiffServMultiFieldClfrFilterType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServMultiFieldClfrStatus
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                setValFsDiffServMultiFieldClfrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMultiFieldClfrStatus (INT4 i4FsDiffServMultiFieldClfrId,
                                      INT4
                                      i4SetValFsDiffServMultiFieldClfrStatus)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);

    if (pDsMultiFieldClfrEntry != NULL)
    {
        if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus ==
            (UINT1) i4SetValFsDiffServMultiFieldClfrStatus)
        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServMultiFieldClfrStatus == DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Dath Path Entry is not there in the Database */
        if (i4SetValFsDiffServMultiFieldClfrStatus != DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsDiffServMultiFieldClfrStatus)
    {
        case DS_CREATE_AND_WAIT:

            if (DS_MFCLFRENTRY_ALLOC_MEM_BLOCK (&pDsMultiFieldClfrEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Multi Field Clfr Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsMultiFieldClfrEntry, 0,
                       sizeof (tDiffServMultiFieldClfrEntry));
            pDsMultiFieldClfrEntry->i4DsMultiFieldClfrId =
                i4FsDiffServMultiFieldClfrId;

            /* Setting the default values for the column objects */
            pDsMultiFieldClfrEntry->u4DsMultiFieldClfrFilterId = 0;
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrFilterType = 0;
            DS_SLL_ADD (&(DS_MFCLFR_LIST),
                        &(pDsMultiFieldClfrEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus = DS_NOT_READY;
            break;

        case DS_NOT_IN_SERVICE:

            if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus == DS_ACTIVE)
            {
                /* Check if any active classifier is using this MF Filter */
                if (DsCheckMFClfrDependency
                    (pDsMultiFieldClfrEntry->i4DsMultiFieldClfrId)
                    != DS_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus =
                    DS_NOT_IN_SERVICE;
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case DS_ACTIVE:

            /* call the function to qualify the MFClfr Entry data */
            if (DsQualifyMFClfrData (pDsMultiFieldClfrEntry) != DS_SUCCESS)
            {
                CLI_SET_ERR (CLI_DS_FILTER_INACTIVE_ERR);
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)
            {
                return SNMP_FAILURE;
            }

            if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus !=
                DS_NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus = DS_ACTIVE;
            break;

        case DS_DESTROY:

            if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus == DS_ACTIVE)
            {
                /* Check if any active classifier is using this MF Filter */
                if (DsCheckMFClfrDependency
                    (pDsMultiFieldClfrEntry->i4DsMultiFieldClfrId)
                    != DS_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_MFCLFR_LIST),
                           &(pDsMultiFieldClfrEntry->DsNextNode));

            /* Delete the In Profile Action Entry from the Software */
            if (DS_MFCLFRENTRY_FREE_MEM_BLOCK (pDsMultiFieldClfrEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Multi Field Clfr Entry \n");
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMultiFieldClfrFilterId
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                testValFsDiffServMultiFieldClfrFilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMultiFieldClfrFilterId (UINT4 *pu4ErrorCode,
                                           INT4 i4FsDiffServMultiFieldClfrId,
                                           UINT4
                                           u4TestValFsDiffServMultiFieldClfrFilterId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_MFC_ID_VALID (i4FsDiffServMultiFieldClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsDiffServMultiFieldClfrFilterId < DS_MIN_FILTER_ID) ||
        (u4TestValFsDiffServMultiFieldClfrFilterId > DS_MAX_FILTER_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMultiFieldClfrFilterType
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                testValFsDiffServMultiFieldClfrFilterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMultiFieldClfrFilterType (UINT4 *pu4ErrorCode,
                                             INT4 i4FsDiffServMultiFieldClfrId,
                                             INT4
                                             i4TestValFsDiffServMultiFieldClfrFilterType)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_MFC_ID_VALID (i4FsDiffServMultiFieldClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServMultiFieldClfrFilterType == DS_MAC_FILTER) ||
        (i4TestValFsDiffServMultiFieldClfrFilterType == DS_IP_FILTER))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMultiFieldClfrStatus
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                testValFsDiffServMultiFieldClfrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMultiFieldClfrStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDiffServMultiFieldClfrId,
                                         INT4
                                         i4TestValFsDiffServMultiFieldClfrStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServMultiFieldClfrStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServMultiFieldClfrStatus > DS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_MFC_ID_VALID (i4FsDiffServMultiFieldClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServMultiFieldClfrStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_MFCLFR_LIST)) == DS_MFCLFR_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_CLASS_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServClfrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServClfrTable (INT4 i4FsDiffServClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (DsValidateClfrId (i4FsDiffServClfrId) != DS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServClfrTable (INT4 *pi4FsDiffServClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((DsSnmpLowGetFirstValidClfrId (pi4FsDiffServClfrId)) == DS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
                nextFsDiffServClfrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServClfrTable (INT4 i4FsDiffServClfrId,
                                    INT4 *pi4NextFsDiffServClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (i4FsDiffServClfrId < 0)
    {
        return SNMP_FAILURE;
    }

    if ((DsSnmpLowGetNextValidClfrId
         (i4FsDiffServClfrId, pi4NextFsDiffServClfrId)) == DS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrMFClfrId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrMFClfrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrMFClfrId (INT4 i4FsDiffServClfrId,
                              INT4 *pi4RetValFsDiffServClfrMFClfrId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry != NULL)
    {
        *pi4RetValFsDiffServClfrMFClfrId = pDsClfrEntry->i4DsClfrMFClfrId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrInProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrInProActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrInProActionId (INT4 i4FsDiffServClfrId,
                                   INT4 *pi4RetValFsDiffServClfrInProActionId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry != NULL)
    {
        *pi4RetValFsDiffServClfrInProActionId =
            pDsClfrEntry->i4DsClfrInProActionId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrTrafficClassId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrTrafficClassId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrTrafficClassId (INT4 i4FsDiffServClfrId,
                                    INT4 *pi4RetValFsDiffServClfrTrafficClassId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry != NULL)
    {
        *pi4RetValFsDiffServClfrTrafficClassId =
            pDsClfrEntry->i4DsClfrTrafficClassId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrStatus
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrStatus (INT4 i4FsDiffServClfrId,
                            INT4 *pi4RetValFsDiffServClfrStatus)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry != NULL)
    {
        *pi4RetValFsDiffServClfrStatus = pDsClfrEntry->u1DsClfrStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrMFClfrId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrMFClfrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrMFClfrId (INT4 i4FsDiffServClfrId,
                              INT4 i4SetValFsDiffServClfrMFClfrId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if ((pDsClfrEntry != NULL) && (pDsClfrEntry->u1DsClfrStatus != DS_ACTIVE))
    {
        pDsClfrEntry->i4DsClfrMFClfrId = i4SetValFsDiffServClfrMFClfrId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrInProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrInProActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrInProActionId (INT4 i4FsDiffServClfrId,
                                   INT4 i4SetValFsDiffServClfrInProActionId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry != NULL)
    {
        pDsClfrEntry->i4DsClfrInProActionId =
            i4SetValFsDiffServClfrInProActionId;

        /* Don't program the HW before the Classifier entry is made Active
         * since the MultiFieldClassifierID can be changed until then */
        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            pDsMultiFieldClfrEntry = DsGetMFClfrEntry
                (pDsClfrEntry->i4DsClfrMFClfrId);

            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);

#ifdef NPAPI_WANTED
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                /* Update the classifier in the hardware here */
                if (DsHwSetClassifierAction (pDsClfrEntry,
                                             pDsMultiFieldClfrEntry,
                                             pDsInProActEntry,
                                             DS_NP_CLSFR_TC_DONTCARE)
                    != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif /*  */
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrTrafficClassId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrTrafficClassId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrTrafficClassId (INT4 i4FsDiffServClfrId,
                                    INT4 i4SetValFsDiffServClfrTrafficClassId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsClfrEntry->i4DsClfrTrafficClassId ==
        i4SetValFsDiffServClfrTrafficClassId)
    {
        return SNMP_SUCCESS;
    }

    pDsClfrEntry->i4DsClfrTrafficClassId = i4SetValFsDiffServClfrTrafficClassId;

    /* Don't program the HW before the Classifier entry is made Active
     * since the MultiFieldClassifierID can be changed until then */
    if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
    {
        pDsMultiFieldClfrEntry = DsGetMFClfrEntry
            (pDsClfrEntry->i4DsClfrMFClfrId);

#ifdef NPAPI_WANTED
        if (gDsGlobalInfo.DsStatus == DS_ENABLE)
        {
            if (i4SetValFsDiffServClfrTrafficClassId == 0)
            {
                /* Remove Traffic class ID in the HW Classifier Table */
                if (DsHwSetClassifierAction (pDsClfrEntry,
                                             pDsMultiFieldClfrEntry,
                                             NULL, DS_NP_CLSFR_TC_UNMAP)
                    == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* Set Traffic class ID in the HW Classifier Table */
                if (DsHwSetClassifierAction (pDsClfrEntry,
                                             pDsMultiFieldClfrEntry,
                                             NULL, DS_NP_CLSFR_TC_MAP)
                    == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
        }
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrStatus
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrStatus (INT4 i4FsDiffServClfrId,
                            INT4 i4SetValFsDiffServClfrStatus)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry != NULL)
    {
        if (pDsClfrEntry->u1DsClfrStatus ==
            (UINT1) i4SetValFsDiffServClfrStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValFsDiffServClfrStatus == DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Classifier Entry is not there in the Database */
        if (i4SetValFsDiffServClfrStatus != DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsDiffServClfrStatus)
    {
        case DS_CREATE_AND_WAIT:

            if (DS_CLFRENTRY_ALLOC_MEM_BLOCK (&pDsClfrEntry) != DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Clfr Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsClfrEntry, 0, sizeof (tDiffServClfrEntry));
            pDsClfrEntry->i4DsClfrId = i4FsDiffServClfrId;

            /*Setting the default values for the column objects. */
            pDsClfrEntry->i4DsClfrMFClfrId = 0;
            pDsClfrEntry->i4DsClfrInProActionId = 0;
            pDsClfrEntry->i4DsClfrTrafficClassId = 0;

            DS_SLL_ADD (&(DS_CLFR_LIST), &(pDsClfrEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsClfrEntry->u1DsClfrStatus = DS_NOT_READY;
            break;

        case DS_NOT_IN_SERVICE:

            if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                pDsMFClfrEntry = DsGetMFClfrEntry
                    (pDsClfrEntry->i4DsClfrMFClfrId);

                /* Call the Classifier delete for the hadware */
                i4RetVal =
                    DsHwResetClassifierAction (pDsClfrEntry, pDsMFClfrEntry);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

#endif /*  */
                pDsClfrEntry->u1DsClfrStatus = DS_NOT_IN_SERVICE;
            }
            else
            {
                return SNMP_FAILURE;
            }

            break;

        case DS_ACTIVE:

            /* call the function to qualify the Clfr entry data */
            if (DsQualifyClfrData (pDsClfrEntry) != DS_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)
            {
                return SNMP_FAILURE;
            }

            if (pDsClfrEntry->u1DsClfrStatus != DS_NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

            /* Get the Other Elements from the list here */
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

            if (pDsMFClfrEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);

#ifdef NPAPI_WANTED
            /* Call routine here to add the Clfr to H/W */
            if (pDsClfrEntry->i4DsClfrTrafficClassId == 0)
            {
                i4RetVal =
                    DsHwSetClassifierAction (pDsClfrEntry, pDsMFClfrEntry,
                                             pDsInProActEntry,
                                             DS_NP_CLSFR_TC_DONTCARE);
            }
            else
            {
                i4RetVal =
                    DsHwSetClassifierAction (pDsClfrEntry, pDsMFClfrEntry,
                                             pDsInProActEntry,
                                             DS_NP_CLSFR_TC_MAP);
            }

            if (i4RetVal != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

#endif /*  */
            pDsClfrEntry->u1DsClfrStatus = DS_ACTIVE;
            break;

        case DS_DESTROY:

            if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                pDsMFClfrEntry = DsGetMFClfrEntry
                    (pDsClfrEntry->i4DsClfrMFClfrId);

                /* Remove all QoS actions from the Hw Classifier entry */
                i4RetVal =
                    DsHwResetClassifierAction (pDsClfrEntry, pDsMFClfrEntry);
                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
#endif /*  */
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_CLFR_LIST), &(pDsClfrEntry->DsNextNode));

            /* Delete the In Profile Action Entry from the Software */
            if (DS_CLFRENTRY_FREE_MEM_BLOCK (pDsClfrEntry) != DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No ClfrEntry Action Entry \n");
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrMFClfrId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrMFClfrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrMFClfrId (UINT4 *pu4ErrorCode, INT4 i4FsDiffServClfrId,
                                 INT4 i4TestValFsDiffServClfrMFClfrId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL, *pClfrList;
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServClfrMFClfrId < DS_MIN_MFCLFR_ID) ||
        (i4TestValFsDiffServClfrMFClfrId > DS_MAX_MFCLFR_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4TestValFsDiffServClfrMFClfrId);

    if (pDsMultiFieldClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus != DS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_CLASS_INACTIVE_ERR);
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Cannot change the Multi Field Classifier ID of an already
     * active Classifier entry */
    if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_MFC_CHANGE_CLASS_ACTIVE_ERR);
        return SNMP_FAILURE;
    }

    /* Need to verify if the same multi-field classifier is already used by 
     * some other Classifier */
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);
    while (pSllNode != NULL)
    {
        pClfrList = (tDiffServClfrEntry *) pSllNode;
        if ((i4TestValFsDiffServClfrMFClfrId == pClfrList->i4DsClfrMFClfrId) &&
            (i4FsDiffServClfrId != pClfrList->i4DsClfrId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrInProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrInProActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrInProActionId (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDiffServClfrId,
                                      INT4 i4TestValFsDiffServClfrInProActionId)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServClfrInProActionId < DS_MIN_IN_PROACT_ID) ||
        (i4TestValFsDiffServClfrInProActionId > DS_MAX_IN_PROACT_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

    if (pDsMFClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDsInProActEntry = DsGetInProfileActionEntry
        (i4TestValFsDiffServClfrInProActionId);

    if ((pDsInProActEntry == NULL) ||
        (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrTrafficClassId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrTrafficClassId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrTrafficClassId (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDiffServClfrId,
                                       INT4
                                       i4TestValFsDiffServClfrTrafficClassId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServTCEntry   *pDsTCEntry = NULL;
    UINT4               u4OutPort = 0;
    UINT4               u4FilterId;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return DS_FAILURE;
    }

    pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

    if (pDsMFClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* A value of 0 indicates that the existing Traffic Class
     * is to be unmapped from the Classifier */
    if (i4TestValFsDiffServClfrTrafficClassId == 0)
    {
        return SNMP_SUCCESS;
    }

    /* Check if this Traffic class exists */
    if (DsValidateTCId (i4TestValFsDiffServClfrTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_TC_ERR);
        return SNMP_FAILURE;
    }

    u4FilterId = pDsMFClfrEntry->u4DsMultiFieldClfrFilterId;

    /* Release the DiffServ lock here to avoid nested locking with ISS lock */
    DFS_UNLOCK ();

    u4OutPort = IssExGetOutPortFromACL (u4FilterId);

    /* Take back the DiffServ lock */
    DFS_LOCK ();

    /* Recheck the pointers for validity after the lock has been taken back */
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);

    if (pDsClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return DS_FAILURE;
    }

    pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

    if (pDsMFClfrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pDsTCEntry = DS_TC_ENTRY (i4TestValFsDiffServClfrTrafficClassId);

    if (pDsTCEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_TC_ERR);
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4EntryStatus != DS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_TC_ERR);
        return SNMP_FAILURE;
    }

    /* Check whether the outport mentioned in the ACL matches with the 
     * port to which the Traffic Class is attached */
    if ((u4OutPort != 0) && (u4OutPort != pDsTCEntry->u4PortNum))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_OUTPORT_MISMATCH_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrStatus
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrStatus (UINT4 *pu4ErrorCode, INT4 i4FsDiffServClfrId,
                               INT4 i4TestValFsDiffServClfrStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServClfrStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServClfrStatus > DS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServClfrStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_CLFR_LIST)) == DS_CLFR_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_POLICY_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServInProfileActionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServInProfileActionTable (INT4
                                                        i4FsDiffServInProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (DsValidateInProfileActionId (i4FsDiffServInProfileActionId) !=
        DS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServInProfileActionTable (INT4
                                                *pi4FsDiffServInProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if ((DsSnmpLowGetFirstValidInProfileActionId
         (pi4FsDiffServInProfileActionId)) == DS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
                nextFsDiffServInProfileActionId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServInProfileActionTable (INT4
                                               i4FsDiffServInProfileActionId,
                                               INT4
                                               *pi4NextFsDiffServInProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    if (i4FsDiffServInProfileActionId < 0)
    {
        return SNMP_FAILURE;
    }

    if ((DsSnmpLowGetNextValidInProfileActionId
         (i4FsDiffServInProfileActionId,
          pi4NextFsDiffServInProfileActionId)) == DS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionFlag
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionFlag (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     *pu4RetValFsDiffServInProfileActionFlag)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        *pu4RetValFsDiffServInProfileActionFlag =
            pDsInProActEntry->u4DsInProfileActionFlag;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionNewPrio
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionNewPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionNewPrio (INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        *pu4RetValFsDiffServInProfileActionNewPrio)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        *pu4RetValFsDiffServInProfileActionNewPrio =
            pDsInProActEntry->u4DsInProfileActionNewPrio;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionIpTOS
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionIpTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionIpTOS (INT4 i4FsDiffServInProfileActionId,
                                      UINT4
                                      *pu4RetValFsDiffServInProfileActionIpTOS)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        *pu4RetValFsDiffServInProfileActionIpTOS =
            pDsInProActEntry->u4DsInProfileActionIpTOS;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionPort
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionPort (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     *pu4RetValFsDiffServInProfileActionPort)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        *pu4RetValFsDiffServInProfileActionPort =
            pDsInProActEntry->u4DsInProfileActionPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionDscp
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionDscp (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     *pu4RetValFsDiffServInProfileActionDscp)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        *pu4RetValFsDiffServInProfileActionDscp =
            pDsInProActEntry->u4DsInProfileActionDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionStatus
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionStatus (INT4 i4FsDiffServInProfileActionId,
                                       INT4
                                       *pi4RetValFsDiffServInProfileActionStatus)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        *pi4RetValFsDiffServInProfileActionStatus =
            pDsInProActEntry->u1DsInProfileActionStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionFlag
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionFlag (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     u4SetValFsDiffServInProfileActionFlag)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))
    {
        pDsInProActEntry->u4DsInProfileActionFlag =
            u4SetValFsDiffServInProfileActionFlag;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionNewPrio
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionNewPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionNewPrio (INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4SetValFsDiffServInProfileActionNewPrio)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))
    {
        pDsInProActEntry->u4DsInProfileActionNewPrio =
            u4SetValFsDiffServInProfileActionNewPrio;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |= FS_DS_ACTN_INSERT_PRIO;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionIpTOS
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionIpTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionIpTOS (INT4 i4FsDiffServInProfileActionId,
                                      UINT4
                                      u4SetValFsDiffServInProfileActionIpTOS)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))
    {
        pDsInProActEntry->u4DsInProfileActionIpTOS =
            u4SetValFsDiffServInProfileActionIpTOS;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |= FS_DS_ACTN_INSERT_TOSP;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionPort
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionPort (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     u4SetValFsDiffServInProfileActionPort)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))
    {
        pDsInProActEntry->u4DsInProfileActionPort =
            u4SetValFsDiffServInProfileActionPort;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |=
            FS_DS_ACTN_SET_OUT_PORT_UCAST;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionDscp
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionDscp (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     u4SetValFsDiffServInProfileActionDscp)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))
    {
        pDsInProActEntry->u4DsInProfileActionDscp =
            u4SetValFsDiffServInProfileActionDscp;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |= FS_DS_ACTN_INSERT_DSCP;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionStatus
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionStatus (INT4 i4FsDiffServInProfileActionId,
                                       INT4
                                       i4SetValFsDiffServInProfileActionStatus)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;

    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);

    if (pDsInProActEntry != NULL)
    {
        if (pDsInProActEntry->u1DsInProfileActionStatus ==
            (UINT1) i4SetValFsDiffServInProfileActionStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValFsDiffServInProfileActionStatus == DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Dath Path Entry is not there in the Database */
        if (i4SetValFsDiffServInProfileActionStatus != DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsDiffServInProfileActionStatus)
    {
        case DS_CREATE_AND_WAIT:

            if (DS_INPROACTENTRY_ALLOC_MEM_BLOCK (&pDsInProActEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free In Profile Action Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsInProActEntry, 0,
                       sizeof (tDiffServInProfileActionEntry));
            pDsInProActEntry->i4DsInProfileActionId =
                i4FsDiffServInProfileActionId;
            DS_SLL_ADD (&(DS_INPROACT_LIST), &(pDsInProActEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsInProActEntry->u1DsInProfileActionStatus = DS_NOT_READY;
            break;

        case DS_NOT_IN_SERVICE:

            if (pDsInProActEntry->u1DsInProfileActionStatus == DS_ACTIVE)
            {
                /* Check if any active classifier has this In Profile Action */
                if (DsCheckInProfileActionDependency
                    (pDsInProActEntry->i4DsInProfileActionId) != DS_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                pDsInProActEntry->u1DsInProfileActionStatus = DS_NOT_IN_SERVICE;
            }
            else
            {
                return SNMP_FAILURE;
            }

            break;

        case DS_ACTIVE:

            /* call the function to qualify the In Profile Action Entry data */
            if (DsQualifyInProfileActionData (pDsInProActEntry) != DS_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)
            {
                return SNMP_FAILURE;
            }

            if (pDsInProActEntry->u1DsInProfileActionStatus !=
                DS_NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

            pDsInProActEntry->u1DsInProfileActionStatus = DS_ACTIVE;
            break;

        case DS_DESTROY:

            if (pDsInProActEntry->u1DsInProfileActionStatus == DS_ACTIVE)
            {
                /* Check if any active classifier has this In Profile Action */
                if (DsCheckInProfileActionDependency
                    (pDsInProActEntry->i4DsInProfileActionId) != DS_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_INPROACT_LIST),
                           &(pDsInProActEntry->DsNextNode));

            /* Delete the In Profile Action Entry from the Software */
            if (DS_INPROACTENTRY_FREE_MEM_BLOCK (pDsInProActEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No In Profile Action Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionFlag
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionFlag (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4TestValFsDiffServInProfileActionFlag)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServInProfileActionFlag | DS_INPROFILEACTION_MASK) ==
        DS_INPROFILEACTION_MASK)
    {

        /* check for the contradictory flags here */
        if ((u4TestValFsDiffServInProfileActionFlag &
             FS_DS_ACTN_DO_NOT_SWITCH)
            && (u4TestValFsDiffServInProfileActionFlag & FS_DS_ACTN_DO_SWITCH))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((u4TestValFsDiffServInProfileActionFlag & FS_DS_ACTN_INSERT_PRIO)
            && (u4TestValFsDiffServInProfileActionFlag &
                FS_DS_ACTN_INSERT_PRIO_FROM_TOSP))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((u4TestValFsDiffServInProfileActionFlag & FS_DS_ACTN_INSERT_TOSP)
            && (u4TestValFsDiffServInProfileActionFlag &
                FS_DS_ACTN_INSERT_TOSP_FROM_PRIO))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionNewPrio
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionNewPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionNewPrio (UINT4 *pu4ErrorCode,
                                           INT4 i4FsDiffServInProfileActionId,
                                           UINT4
                                           u4TestValFsDiffServInProfileActionNewPrio)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServInProfileActionNewPrio > DS_MAX_PRIO_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionIpTOS
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionIpTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionIpTOS (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDiffServInProfileActionId,
                                         UINT4
                                         u4TestValFsDiffServInProfileActionIpTOS)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServInProfileActionIpTOS > DS_MAX_PRECEDENCE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionPort
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionPort (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4TestValFsDiffServInProfileActionPort)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServInProfileActionPort < DS_MAX_NO_PORTS))
    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionDscp
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionDscp (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4TestValFsDiffServInProfileActionDscp)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServInProfileActionDscp <= DS_MAX_DSCP_VALUE)
    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionStatus
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDiffServInProfileActionId,
                                          INT4
                                          i4TestValFsDiffServInProfileActionStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServInProfileActionStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServInProfileActionStatus > DS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServInProfileActionStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_INPROACT_LIST)) == DS_INPROACT_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_INPROF_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDsDSCPRemapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDsDSCPRemapTable
 Input       :  The Indices
                FsDsDSCPValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDsDSCPRemapTable (UINT4 u4FsDsDSCPValue)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    /* DSCP value index range: 0-63 */
    if (u4FsDsDSCPValue <= 63)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDsDSCPRemapTable
 Input       :  The Indices
                FsDsDSCPValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDsDSCPRemapTable (UINT4 *pu4FsDsDSCPValue)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    *pu4FsDsDSCPValue = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDsDSCPRemapTable
 Input       :  The Indices
                FsDsDSCPValue
                nextFsDsDSCPValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDsDSCPRemapTable (UINT4 u4FsDsDSCPValue,
                                   UINT4 *pu4NextFsDsDSCPValue)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (u4FsDsDSCPValue >= 63)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsDsDSCPValue = u4FsDsDSCPValue + 1;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsQPriority
 Input       :  The Indices
                FsDsDSCPValue

                The Object 
                retValFsDsQPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsQPriority (UINT4 u4FsDsDSCPValue, INT4 *pi4RetValFsDsQPriority)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (u4FsDsDSCPValue > 63)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsQPriority = DS_DSCP_TO_QMAP (u4FsDsDSCPValue);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDsQPriority
 Input       :  The Indices
                FsDsDSCPValue

                The Object 
                setValFsDsQPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsQPriority (UINT4 u4FsDsDSCPValue, INT4 i4SetValFsDsQPriority)
{
    if (DS_DSCP_TO_QMAP (u4FsDsDSCPValue) == (UINT4) i4SetValFsDsQPriority)
    {
        return SNMP_SUCCESS;
    }

    /* Set the appropriate max. DSCP value for this queue priority */
    DS_DSCP_TO_QMAP (u4FsDsDSCPValue) = (UINT4) i4SetValFsDsQPriority;

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetDSCPQPrioMap (u4FsDsDSCPValue,
                                 i4SetValFsDsQPriority) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDsQPriority
 Input       :  The Indices
                FsDsDSCPValue

                The Object 
                testValFsDsQPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsQPriority (UINT4 *pu4ErrorCode, UINT4 u4FsDsDSCPValue,
                        INT4 i4TestValFsDsQPriority)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4FsDsDSCPValue > 63)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsQPriority < 0 || i4TestValFsDsQPriority > 7)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDsTrafficClassConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDsTrafficClassConfigTable
 Input       :  The Indices
                FsDsTrafficClassId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDsTrafficClassConfigTable (INT4 i4FsDsTrafficClassId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsDsTrafficClassId <= DS_NUM_RESERVED_TRAFFIC_CLASSES) ||
        (i4FsDsTrafficClassId > DS_MAX_TC))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDsTrafficClassConfigTable
 Input       :  The Indices
                FsDsTrafficClassId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDsTrafficClassConfigTable (INT4 *pi4FsDsTrafficClassId)
{
    return (nmhGetNextIndexFsDsTrafficClassConfigTable
            (0, pi4FsDsTrafficClassId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDsTrafficClassConfigTable
 Input       :  The Indices
                FsDsTrafficClassId
                nextFsDsTrafficClassId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDsTrafficClassConfigTable (INT4 i4FsDsTrafficClassId,
                                            INT4 *pi4NextFsDsTrafficClassId)
{
    UINT4               u4TcId;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    /* Start from above the reserved Traffic Classes */
    i4FsDsTrafficClassId =
        (i4FsDsTrafficClassId < DS_NUM_RESERVED_TRAFFIC_CLASSES) ?
        (DS_NUM_RESERVED_TRAFFIC_CLASSES) : (i4FsDsTrafficClassId);

    for (u4TcId = i4FsDsTrafficClassId + 1; u4TcId <= DS_MAX_TC; u4TcId++)
    {
        if (DS_TC_ENTRY (u4TcId) != NULL)
        {
            *pi4NextFsDsTrafficClassId = u4TcId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

INT4
DsValidateTCId (INT4 i4FsDsTrafficClassId)
{
    if ((i4FsDsTrafficClassId <= DS_NUM_RESERVED_TRAFFIC_CLASSES) ||
        (i4FsDsTrafficClassId > DS_MAX_TC))
    {
        return DS_FAILURE;
    }

    if (DS_TC_ENTRY (i4FsDsTrafficClassId) == NULL)
    {
        return DS_FAILURE;
    }

    return DS_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsTCPortNum
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCPortNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCPortNum (INT4 i4FsDsTrafficClassId, INT4 *pi4RetValFsDsTCPortNum)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCPortNum = (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCMaxBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCMaxBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCMaxBandwidth (INT4 i4FsDsTrafficClassId,
                          INT4 *pi4RetValFsDsTCMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCMaxBandwidth =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4MaxBw;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCGuaranteedBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCGuaranteedBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCGuaranteedBandwidth (INT4 i4FsDsTrafficClassId,
                                 INT4 *pi4RetValFsDsTCGuaranteedBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCGuaranteedBandwidth =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4GuaranteedBw;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCDropLevel1
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCDropLevel1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCDropLevel1 (INT4 i4FsDsTrafficClassId,
                        INT4 *pi4RetValFsDsTCDropLevel1)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCDropLevel1 =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropLevel1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCDropLevel2
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCDropLevel2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCDropLevel2 (INT4 i4FsDsTrafficClassId,
                        INT4 *pi4RetValFsDsTCDropLevel2)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCDropLevel2 =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropLevel2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCDropLevel3
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCDropLevel3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCDropLevel3 (INT4 i4FsDsTrafficClassId,
                        INT4 *pi4RetValFsDsTCDropLevel3)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCDropLevel3 =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropLevel3;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCDropAtMaxBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCDropAtMaxBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCDropAtMaxBandwidth (INT4 i4FsDsTrafficClassId,
                                INT4 *pi4RetValFsDsTCDropAtMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropAtMaxBw == DS_TRUE)
    {
        *pi4RetValFsDsTCDropAtMaxBandwidth = DS_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsDsTCDropAtMaxBandwidth = DS_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCWeightFactor
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCWeightFactor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCWeightFactor (INT4 i4FsDsTrafficClassId,
                          INT4 *pi4RetValFsDsTCWeightFactor)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCWeightFactor =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4WeightFactor;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCWeightShift
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCWeightShift
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCWeightShift (INT4 i4FsDsTrafficClassId,
                         INT4 *pi4RetValFsDsTCWeightShift)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCWeightShift =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4WeightShift;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCFirstFlowGroup
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCFirstFlowGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCFirstFlowGroup (INT4 i4FsDsTrafficClassId,
                            INT4 *pi4RetValFsDsTCFirstFlowGroup)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCFirstFlowGroup = (DS_TC_ENTRY (i4FsDsTrafficClassId))->
        u4FirstFlowGroup;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCNumFlowGroups
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCNumFlowGroups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCNumFlowGroups (INT4 i4FsDsTrafficClassId,
                           INT4 *pi4RetValFsDsTCNumFlowGroups)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCNumFlowGroups =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4NumFlowGroups;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCREDCurveId
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCREDCurveId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCREDCurveId (INT4 i4FsDsTrafficClassId,
                        INT4 *pi4RetValFsDsTCREDCurveId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCREDCurveId =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4REDCurveId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCAccBytesCount
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCAccBytesCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCAccBytesCount (INT4 i4FsDsTrafficClassId,
                           tSNMP_COUNTER64_TYPE * pu8RetValFsDsTCAccBytesCount)
{
    UINT4               u4NumBytesAccLow = 0;
    UINT4               u4NumBytesAccHigh = 0;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetTCCounter (i4FsDsTrafficClassId, DS_NP_COUNTER_TC_ACCEPT_BYTES,
                          &u4NumBytesAccLow, &u4NumBytesAccHigh) == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    pu8RetValFsDsTCAccBytesCount->lsn = u4NumBytesAccLow;
    pu8RetValFsDsTCAccBytesCount->msn = u4NumBytesAccHigh;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCDiscBytesCount
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCDiscBytesCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCDiscBytesCount (INT4 i4FsDsTrafficClassId,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValFsDsTCDiscBytesCount)
{
    UINT4               u4NumBytesDiscLow = 0;
    UINT4               u4NumBytesDiscHigh = 0;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (DsHwGetTCCounter (i4FsDsTrafficClassId, DS_NP_COUNTER_TC_DISCARD_BYTES,
                          &u4NumBytesDiscLow, &u4NumBytesDiscHigh)
        == FNP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    pu8RetValFsDsTCDiscBytesCount->lsn = u4NumBytesDiscLow;
    pu8RetValFsDsTCDiscBytesCount->msn = u4NumBytesDiscHigh;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsTCEntryStatus
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                retValFsDsTCEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsTCEntryStatus (INT4 i4FsDsTrafficClassId,
                         INT4 *pi4RetValFsDsTCEntryStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsTCEntryStatus =
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4EntryStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDsTCPortNum
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCPortNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCPortNum (INT4 i4FsDsTrafficClassId, INT4 i4SetValFsDsTCPortNum)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4PortNum == (UINT4) i4SetValFsDsTCPortNum)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4PortNum = (UINT4) i4SetValFsDsTCPortNum;

    /* No longer port-independent */
    pDsTCEntry->u4PortIndependent = DS_FALSE;

    /* The hardware is programmed when the Traffic Class Row Status
     * is set as active */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCMaxBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCMaxBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCMaxBandwidth (INT4 i4FsDsTrafficClassId,
                          INT4 i4SetValFsDsTCMaxBandwidth)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4MaxBw == (UINT4) i4SetValFsDsTCMaxBandwidth)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4MaxBw = (UINT4) i4SetValFsDsTCMaxBandwidth;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry,
                             DS_NP_TC_PARAM_SET_MAXBW) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCGuaranteedBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCGuaranteedBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCGuaranteedBandwidth (INT4 i4FsDsTrafficClassId,
                                 INT4 i4SetValFsDsTCGuaranteedBandwidth)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4GuaranteedBw == (UINT4) i4SetValFsDsTCGuaranteedBandwidth)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4GuaranteedBw = (UINT4) i4SetValFsDsTCGuaranteedBandwidth;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry,
                             DS_NP_TC_PARAM_SET_GUARANTEEDBW) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCDropLevel1
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCDropLevel1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCDropLevel1 (INT4 i4FsDsTrafficClassId,
                        INT4 i4SetValFsDsTCDropLevel1)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4DropLevel1 == (UINT4) i4SetValFsDsTCDropLevel1)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4DropLevel1 = (UINT4) i4SetValFsDsTCDropLevel1;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry, DS_NP_TC_PARAM_SET_DP1) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCDropLevel2
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCDropLevel2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCDropLevel2 (INT4 i4FsDsTrafficClassId,
                        INT4 i4SetValFsDsTCDropLevel2)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4DropLevel2 == (UINT4) i4SetValFsDsTCDropLevel2)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4DropLevel2 = (UINT4) i4SetValFsDsTCDropLevel2;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry, DS_NP_TC_PARAM_SET_DP2) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCDropLevel3
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCDropLevel3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCDropLevel3 (INT4 i4FsDsTrafficClassId,
                        INT4 i4SetValFsDsTCDropLevel3)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4DropLevel3 == (UINT4) i4SetValFsDsTCDropLevel3)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4DropLevel3 = (UINT4) i4SetValFsDsTCDropLevel3;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry, DS_NP_TC_PARAM_SET_DP3) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCDropAtMaxBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCDropAtMaxBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCDropAtMaxBandwidth (INT4 i4FsDsTrafficClassId,
                                INT4 i4SetValFsDsTCDropAtMaxBandwidth)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsDsTCDropAtMaxBandwidth == DS_SNMP_TRUE)
    {
        if (pDsTCEntry->u4DropAtMaxBw == DS_TRUE)
        {
            return SNMP_SUCCESS;
        }

        pDsTCEntry->u4DropAtMaxBw = DS_TRUE;
    }
    else
    {
        if (pDsTCEntry->u4DropAtMaxBw == DS_FALSE)
        {
            return SNMP_SUCCESS;
        }

        pDsTCEntry->u4DropAtMaxBw = DS_FALSE;
    }

#ifdef NPAPI_WANTED
    /* Set Drop level1 to - 0 if DropAtMaxBw is true
     *                    - the actual value if DropAtMaxBw is false 
     * This will be taken care of inside the NP call */

    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry, DS_NP_TC_PARAM_SET_DP1) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCWeightFactor
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCWeightFactor
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCWeightFactor (INT4 i4FsDsTrafficClassId,
                          INT4 i4SetValFsDsTCWeightFactor)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4WeightFactor == (UINT4) i4SetValFsDsTCWeightFactor)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4WeightFactor = (UINT4) i4SetValFsDsTCWeightFactor;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry,
                             DS_NP_TC_PARAM_SET_WGTFACTOR) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCWeightShift
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCWeightShift
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCWeightShift (INT4 i4FsDsTrafficClassId,
                         INT4 i4SetValFsDsTCWeightShift)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4WeightShift == (UINT4) i4SetValFsDsTCWeightShift)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4WeightShift = (UINT4) i4SetValFsDsTCWeightShift;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry,
                             DS_NP_TC_PARAM_SET_WGTSHIFT) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCNumFlowGroups
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCNumFlowGroups
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCNumFlowGroups (INT4 i4FsDsTrafficClassId,
                           INT4 i4SetValFsDsTCNumFlowGroups)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;
    tDsFreeFlowGroupInfo *pFreeNode = NULL;
    UINT4               u4FirstFlowGroup = 0;
    UINT4               u4NumFlowGroups = 0;
    UINT4               u4NextBlockStart;
    UINT4               u4Merged = 0;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4NumFlowGroups == (UINT4) i4SetValFsDsTCNumFlowGroups)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValFsDsTCNumFlowGroups == 0)
    {
        /* Store the flow group info for use after the NPAPI is programmed */
        u4FirstFlowGroup = pDsTCEntry->u4FirstFlowGroup;
        u4NumFlowGroups = pDsTCEntry->u4NumFlowGroups;
        pDsTCEntry->u4FirstFlowGroup = 0;
        pDsTCEntry->u4NumFlowGroups = 0;
    }
    else
    {
        /* Find the appropriate first flow group */
        pFreeNode =
            (tDsFreeFlowGroupInfo *) (DS_SLL_FIRST ((&DS_FREE_FLOWGROUP_LIST)));

        while (pFreeNode != NULL)
        {
            if (pFreeNode->u4NumFreeFlowGroups >=
                (UINT4) i4SetValFsDsTCNumFlowGroups)
            {
                /* The required number of contiguous free flow groups found */
                (DS_CXE_INFO ()).u4NumFreeFlowGroups -=
                    i4SetValFsDsTCNumFlowGroups;
                pDsTCEntry->u4NumFlowGroups =
                    (UINT4) i4SetValFsDsTCNumFlowGroups;
                u4FirstFlowGroup = pFreeNode->u4FirstFlowGroup;
                pDsTCEntry->u4FirstFlowGroup = u4FirstFlowGroup;

                /* Now update the free flow group list */
                pFreeNode->u4NumFreeFlowGroups -= i4SetValFsDsTCNumFlowGroups;
                if (pFreeNode->u4NumFreeFlowGroups == 0)
                {
                    /* There are no free flow groups in this node.
                     * Discard this node */
                    DS_SLL_DELETE (&(DS_FREE_FLOWGROUP_LIST),
                                   (tTMO_SLL_NODE *) pFreeNode);

                    DS_FREEFLOWGROUP_FREE_MEM_BLOCK (pFreeNode);
                }
                else
                {
                    pFreeNode->u4FirstFlowGroup += i4SetValFsDsTCNumFlowGroups;
                }

                break;
            }

            pFreeNode = (tDsFreeFlowGroupInfo *)
                DS_SLL_NEXT (&(DS_FREE_FLOWGROUP_LIST),
                             ((tTMO_SLL_NODE *) (pFreeNode)));
        }

        if (u4FirstFlowGroup == 0)
        {
            /* We don't have a contiguous block of the required number of
             * flow groups. Return failure. */
            return SNMP_FAILURE;

        }
    }

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry,
                             DS_NP_TC_PARAM_SET_NUMFLWGRPS) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    if (i4SetValFsDsTCNumFlowGroups == 0)
    {
        /* Release the flowgroups allocated to this traffic class to the
         * Free Flowgroup pool */

        /* Scan thru the Free list to check if the block about to be released
         * can be merged with an existing free block */
        u4NextBlockStart = u4FirstFlowGroup + u4NumFlowGroups;

        pFreeNode =
            (tDsFreeFlowGroupInfo *) (DS_SLL_FIRST ((&DS_FREE_FLOWGROUP_LIST)));

        while (pFreeNode != NULL)
        {
            if (pFreeNode->u4FirstFlowGroup == u4NextBlockStart)
            {
                /* The block about to be released can be attached at the
                 * beginning of this free block in the list */
                pFreeNode->u4FirstFlowGroup = u4FirstFlowGroup;
                pFreeNode->u4NumFreeFlowGroups += u4NumFlowGroups;
                u4Merged = 1;
                break;
            }

            if ((pFreeNode->u4FirstFlowGroup + pFreeNode->u4NumFreeFlowGroups)
                == u4FirstFlowGroup)
            {
                /* The block about to be released can be attached at the
                 * end of this free block in the list */
                pFreeNode->u4NumFreeFlowGroups += u4NumFlowGroups;
                u4Merged = 1;
                break;
            }

            pFreeNode = (tDsFreeFlowGroupInfo *)
                DS_SLL_NEXT (&(DS_FREE_FLOWGROUP_LIST),
                             ((tTMO_SLL_NODE *) (pFreeNode)));
        }

        if (u4Merged == 0)
        {
            /* Cannot be merged with an existing free block.
             * Create a new free block */
            if (DS_FREEFLOWGROUP_ALLOC_MEM_BLOCK (&pFreeNode) != DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Flow group entry \n");
                return SNMP_FAILURE;
            }

            DS_MEMSET (pFreeNode, 0, sizeof (tDsFreeFlowGroupInfo));
            pFreeNode->u4FirstFlowGroup = u4FirstFlowGroup;
            pFreeNode->u4NumFreeFlowGroups = u4NumFlowGroups;

            DS_SLL_ADD (&(DS_FREE_FLOWGROUP_LIST), &(pFreeNode->DsNextNode));
        }

        (DS_CXE_INFO ()).u4NumFreeFlowGroups += u4NumFlowGroups;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCREDCurveId
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCREDCurveId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCREDCurveId (INT4 i4FsDsTrafficClassId,
                        INT4 i4SetValFsDsTCREDCurveId)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    if ((pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pDsTCEntry->u4REDCurveId == (UINT4) i4SetValFsDsTCREDCurveId)
    {
        return SNMP_SUCCESS;
    }

    pDsTCEntry->u4REDCurveId = (UINT4) i4SetValFsDsTCREDCurveId;

#ifdef NPAPI_WANTED
    if ((gDsGlobalInfo.DsStatus == DS_ENABLE) &&
        (pDsTCEntry->u4EntryStatus == DS_ACTIVE))
    {
        if (DsHwSetTCParams (i4FsDsTrafficClassId,
                             pDsTCEntry,
                             DS_NP_TC_PARAM_SET_REDCURVE) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsTCEntryStatus
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                setValFsDsTCEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsTCEntryStatus (INT4 i4FsDsTrafficClassId,
                         INT4 i4SetValFsDsTCEntryStatus)
{
    tDiffServTCEntry   *pDsTCEntry = NULL;

    pDsTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId);

    if (pDsTCEntry != NULL)
    {
        if (pDsTCEntry->u4EntryStatus == (UINT1) i4SetValFsDsTCEntryStatus)
        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDsTCEntryStatus == DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Traffic class is not present in the Database */
        if (i4SetValFsDsTCEntryStatus != DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsDsTCEntryStatus)
    {
        case DS_CREATE_AND_WAIT:

            if (DS_TCENTRY_ALLOC_MEM_BLOCK (&pDsTCEntry) != DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Traffic class Entry \n");
                return SNMP_FAILURE;
            }

            DS_TC_ENTRY (i4FsDsTrafficClassId) = pDsTCEntry;

            DS_MEMSET (pDsTCEntry, 0, sizeof (tDiffServTCEntry));

            /* Setting the default values for the fields */
            pDsTCEntry->u4MaxBw = DS_DEF_PORT_MAX_BW;    /* Initialise to
                                                           1 Gbps */
            pDsTCEntry->u4WeightFactor = DS_DEF_WEIGHT_FACTOR;
            pDsTCEntry->u4WeightShift = DS_DEF_WEIGHT_SHIFT;
            pDsTCEntry->u4DropLevel1 = DS_MIN_DROP_LEVEL1;
            pDsTCEntry->u4DropLevel2 = DS_MIN_DROP_LEVEL2;
            pDsTCEntry->u4DropLevel3 = DS_MIN_DROP_LEVEL3;
            pDsTCEntry->u4DropAtMaxBw = DS_FALSE;

            /* Considered port-independent until it is attached to a port */
            pDsTCEntry->u4PortIndependent = DS_TRUE;

            /* Moving the Row Status to NOT_IN_SERVICE as mandatory objects
             * are not having required values. */
            pDsTCEntry->u4EntryStatus = DS_NOT_IN_SERVICE;
            break;

        case DS_NOT_IN_SERVICE:
#ifdef NPAPI_WANTED
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                /* Set default values for all parameters */
                if (DsHwSetTCParams (i4FsDsTrafficClassId,
                                     pDsTCEntry,
                                     DS_NP_TC_PARAM_SET_DEFAULT_ALL)
                    == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pDsTCEntry->u4EntryStatus = DS_NOT_IN_SERVICE;
            break;

        case DS_ACTIVE:

#ifdef NPAPI_WANTED
            /* Attach the traffic class to the output port in the hardware */
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                /* NOTE:
                 * Calling SetTCParams with the SET_ALL flag will take care of 
                 * enabling wrong-port accept. Hence for every traffic class
                 * created this routine should be called with the SET_ALL 
                 * flag atleast once also. */
                if (DsHwSetTCParams (i4FsDsTrafficClassId,
                                     pDsTCEntry,
                                     DS_NP_TC_PARAM_SET_ALL) == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pDsTCEntry->u4EntryStatus = DS_ACTIVE;
            break;

        case DS_DESTROY:

#ifdef NPAPI_WANTED
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                /* Set default values for all parameters */
                if (DsHwSetTCParams (i4FsDsTrafficClassId,
                                     pDsTCEntry,
                                     DS_NP_TC_PARAM_SET_DEFAULT_ALL)
                    == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            /* Delete the Traffic Class Entry from the Software */
            if (DS_TCENTRY_FREE_MEM_BLOCK (pDsTCEntry) != DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "Free Mem block for TC Entry failed !!!\n");

                return SNMP_FAILURE;
            }

            DS_TC_ENTRY (i4FsDsTrafficClassId) = NULL;

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDsTCPortNum
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCPortNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCPortNum (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                        INT4 i4TestValFsDsTCPortNum)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCPortNum < 1 || i4TestValFsDsTCPortNum > DS_MAX_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4EntryStatus == DS_ACTIVE)
    {
        /* Traffic class is already active */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCMaxBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCMaxBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCMaxBandwidth (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                             INT4 i4TestValFsDsTCMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCMaxBandwidth < 1 ||
        i4TestValFsDsTCMaxBandwidth > DS_MAX_MAX_BW)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCGuaranteedBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCGuaranteedBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCGuaranteedBandwidth (UINT4 *pu4ErrorCode,
                                    INT4 i4FsDsTrafficClassId,
                                    INT4 i4TestValFsDsTCGuaranteedBandwidth)
{
    UINT4               u4TotalGrntBw = 0;
    UINT4               u4TCId, u4PortNum;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCGuaranteedBandwidth < 1 ||
        i4TestValFsDsTCGuaranteedBandwidth > DS_MAX_MAX_BW)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* The Traffic Class is not yet attached to a port */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortNum = (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum;

    for (u4TCId = 1; u4TCId <= DS_MAX_TC; u4TCId++)
    {
        if ((DS_TC_ENTRY (u4TCId) != NULL) &&
            (u4TCId != (UINT4) i4FsDsTrafficClassId))
        {
            if ((DS_TC_ENTRY (u4TCId))->u4PortNum == u4PortNum)
            {
                u4TotalGrntBw += (DS_TC_ENTRY (u4TCId))->u4GuaranteedBw;
            }
        }
    }

    /* The Total guaranteed bandwiidth of all traffic classes attached to
     * this port should not exceed the Max. bandwidth configured on the port */
    if ((u4TotalGrntBw + i4TestValFsDsTCGuaranteedBandwidth) >
        (DS_PORT_ENTRY (u4PortNum))->u4MaxBw)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_BW_EXCEED_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCDropLevel1
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCDropLevel1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCDropLevel1 (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                           INT4 i4TestValFsDsTCDropLevel1)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Drop levels cannot be configured when DropAtMaxBw is set */

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropAtMaxBw == DS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_DROP_AT_MAXBW_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCDropLevel1 < DS_MIN_DROP_LEVEL1 ||
        i4TestValFsDsTCDropLevel1 > DS_MAX_DROP_LEVEL1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* The Traffic class is not yet attached to a port */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCDropLevel2
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCDropLevel2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCDropLevel2 (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                           INT4 i4TestValFsDsTCDropLevel2)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Drop levels cannot be configured when DropAtMaxBw is set */
    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropAtMaxBw == DS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_DROP_AT_MAXBW_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCDropLevel2 < DS_MIN_DROP_LEVEL2 ||
        i4TestValFsDsTCDropLevel2 > DS_MAX_DROP_LEVEL2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* Not yet attached to a classifier */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCDropLevel3
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCDropLevel3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCDropLevel3 (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                           INT4 i4TestValFsDsTCDropLevel3)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Drop levels cannot be configured when DropAtMaxBw is set */
    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4DropAtMaxBw == DS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_DROP_AT_MAXBW_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCDropLevel3 < DS_MIN_DROP_LEVEL3 ||
        i4TestValFsDsTCDropLevel3 > DS_MAX_DROP_LEVEL3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* Not yet attached to a classifier */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCDropAtMaxBandwidth
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCDropAtMaxBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCDropAtMaxBandwidth (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDsTrafficClassId,
                                   INT4 i4TestValFsDsTCDropAtMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0 &&
        (DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDsTCDropAtMaxBandwidth != DS_SNMP_TRUE) &&
        (i4TestValFsDsTCDropAtMaxBandwidth != DS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCWeightFactor
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCWeightFactor
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCWeightFactor (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                             INT4 i4TestValFsDsTCWeightFactor)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCWeightFactor < 0 || i4TestValFsDsTCWeightFactor > 7)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* Not yet attached to a classifier */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCWeightShift
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCWeightShift
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCWeightShift (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                            INT4 i4TestValFsDsTCWeightShift)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCWeightShift < 0 || i4TestValFsDsTCWeightShift > 7)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* Not yet attached to a classifier */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCNumFlowGroups
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCNumFlowGroups
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCNumFlowGroups (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                              INT4 i4TestValFsDsTCNumFlowGroups)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Number of flow groups assigned to a TC should be one of these values */
    if ((i4TestValFsDsTCNumFlowGroups != 0) &&
        (i4TestValFsDsTCNumFlowGroups != 1) &&
        (i4TestValFsDsTCNumFlowGroups != 2) &&
        (i4TestValFsDsTCNumFlowGroups != 8) &&
        (i4TestValFsDsTCNumFlowGroups != 32) &&
        (i4TestValFsDsTCNumFlowGroups != 64) &&
        (i4TestValFsDsTCNumFlowGroups != 128) &&
        (i4TestValFsDsTCNumFlowGroups != 256) &&
        (i4TestValFsDsTCNumFlowGroups != 512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        /* Not yet attached to a classifier */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCNumFlowGroups == 0)
    {
        return SNMP_SUCCESS;
    }

    if (i4TestValFsDsTCNumFlowGroups >
        (INT4) (DS_CXE_INFO ()).u4NumFreeFlowGroups)
    {
        /* Required number of flow groups not available */
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_DS_FLOWGROUPS_UNAVAIL_ERR);
        return SNMP_FAILURE;
    }

    /* Even if this suceeds the request may fail if the required number
     * of flow groups is not available contiguously. That check is made only
     * in the SET */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCREDCurveId
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCREDCurveId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCREDCurveId (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                           INT4 i4TestValFsDsTCREDCurveId)
{
    tDiffServREDEntry  *pREDEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateTCId (i4FsDsTrafficClassId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* The value DS_MIN_REDCURVES - 1 is used to indicate that
     * RED is disabled on this traffic class */
    if (i4TestValFsDsTCREDCurveId < DS_MIN_REDCURVES - 1 ||
        i4TestValFsDsTCREDCurveId > (DS_MIN_PORT_RED_CURVE - 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortIndependent == DS_TRUE)
    {
        /* No other parameter except Max. Bandwidth can be set on a 
         * port-independent TC */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_TC_PORT_INDEP_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsTCREDCurveId == DS_MIN_REDCURVES - 1)
    {
        return SNMP_SUCCESS;
    }

    if ((DS_TC_ENTRY (i4FsDsTrafficClassId))->u4PortNum == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* The corresponding RED curve should have already been created */
    if ((pREDEntry = DS_RED_ENTRY (i4TestValFsDsTCREDCurveId)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_RED_CURVE_ERR);
        return SNMP_FAILURE;
    }

    if (pREDEntry->u4REDCurveStatus != DS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_RED_CURVE_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsTCEntryStatus
 Input       :  The Indices
                FsDsTrafficClassId

                The Object 
                testValFsDsTCEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsTCEntryStatus (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                            INT4 i4TestValFsDsTCEntryStatus)
{
    tDiffServTCEntry   *pDiffServTCEntry = NULL;
    UINT4               u4TotalGrntBw = 0;
    UINT4               u4TCId, u4PortNum;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4FsDsTrafficClassId <= DS_NUM_RESERVED_TRAFFIC_CLASSES) ||
        (i4FsDsTrafficClassId > DS_MAX_TC))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pDiffServTCEntry = DS_TC_ENTRY (i4FsDsTrafficClassId);

    if (pDiffServTCEntry != NULL)
    {
        if (i4TestValFsDsTCEntryStatus == DS_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Traffic class is not present in the Database */
        if (i4TestValFsDsTCEntryStatus != DS_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsDsTCEntryStatus < DS_ACTIVE) ||
        (i4TestValFsDsTCEntryStatus > DS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsDsTCEntryStatus)
    {
        case DS_NOT_IN_SERVICE:

            if (pDiffServTCEntry->u4EntryStatus == DS_ACTIVE)
            {
                /* Check if any active Classifier entry is using this 
                 * Traffic class */
                if (DsCheckClfrForTrafficClassId (i4FsDsTrafficClassId) != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case DS_ACTIVE:

            if (pDiffServTCEntry->u4EntryStatus != DS_NOT_IN_SERVICE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (pDiffServTCEntry->u4PortIndependent == DS_FALSE)
            {
                u4PortNum = pDiffServTCEntry->u4PortNum;
                if (u4PortNum == 0)
                {
                    /* Not yet attached to an output port */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                for (u4TCId = 1; u4TCId <= DS_MAX_TC; u4TCId++)
                {
                    if (DS_TC_ENTRY (u4TCId) != NULL)
                    {
                        if ((DS_TC_ENTRY (u4TCId))->u4PortNum == u4PortNum)
                        {
                            u4TotalGrntBw +=
                                (DS_TC_ENTRY (u4TCId))->u4GuaranteedBw;
                        }
                    }
                }

                /* The Total guaranteed bandwidth of all traffic classes 
                 * attached to the same port as this traffic class should not 
                 * exceed the Max. bandwidth configured on the port */
                if (u4TotalGrntBw > (DS_PORT_ENTRY (u4PortNum))->u4MaxBw)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_DS_TC_BW_EXCEED_ERR);
                    return SNMP_FAILURE;
                }
            }

            break;

        case DS_DESTROY:

            if (pDiffServTCEntry->u4EntryStatus == DS_ACTIVE)
            {
                /* Check if any active Classifier entry is using this 
                 * Traffic class */
                if (DsCheckClfrForTrafficClassId (i4FsDsTrafficClassId) != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_DS_TC_IN_USE_ERR);
                    return SNMP_FAILURE;
                }
            }

            break;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsCheckClfrForTrafficClassId                         */
/*                                                                           */
/* Description        : This function is called to check if any of the       */
/*                      Classifier entries use the given Traffic class ID    */
/*                                                                           */
/* Input(s)           : i4DsTrafficClassID                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Classifier Entry - On success                        */
/*                      NULL           - On Failure                          */
/*****************************************************************************/
tDiffServClfrEntry *
DsCheckClfrForTrafficClassId (INT4 i4DsClfrTrafficClassID)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDiffServClfrEntry->i4DsClfrTrafficClassId ==
            i4DsClfrTrafficClassID)
        {
            return pDiffServClfrEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return NULL;
}

/* LOW LEVEL Routines for Table : FsDsPortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDsPortConfigTable
 Input       :  The Indices
                FsDsPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDsPortConfigTable (INT4 i4FsDsPortNum)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (i4FsDsPortNum < 1 || i4FsDsPortNum > DS_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

INT4
DsValidateIfIndex (INT4 i4PortNum)
{
    if (i4PortNum < 1 || i4PortNum > DS_MAX_PORTS)
    {
        return DS_FAILURE;
    }

    return DS_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDsPortConfigTable
 Input       :  The Indices
                FsDsPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDsPortConfigTable (INT4 *pi4FsDsPortNum)
{
    return (nmhGetNextIndexFsDsPortConfigTable (0, pi4FsDsPortNum));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDsPortConfigTable
 Input       :  The Indices
                FsDsPortNum
                nextFsDsPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDsPortConfigTable (INT4 i4FsDsPortNum,
                                    INT4 *pi4NextFsDsPortNum)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (i4FsDsPortNum < DS_MAX_PORTS)
    {
        *pi4NextFsDsPortNum = i4FsDsPortNum + 1;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsPortMaxBandwidth
 Input       :  The Indices
                FsDsPortNum

                The Object 
                retValFsDsPortMaxBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsPortMaxBandwidth (INT4 i4FsDsPortNum,
                            INT4 *pi4RetValFsDsPortMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsPortMaxBandwidth = (DS_PORT_ENTRY (i4FsDsPortNum))->u4MaxBw;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsPortDropLevel1
 Input       :  The Indices
                FsDsPortNum

                The Object 
                retValFsDsPortDropLevel1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsPortDropLevel1 (INT4 i4FsDsPortNum, INT4 *pi4RetValFsDsPortDropLevel1)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsPortDropLevel1 =
        (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsPortDropLevel2
 Input       :  The Indices
                FsDsPortNum

                The Object 
                retValFsDsPortDropLevel2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsPortDropLevel2 (INT4 i4FsDsPortNum, INT4 *pi4RetValFsDsPortDropLevel2)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsPortDropLevel2 =
        (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsPortDropLevel3
 Input       :  The Indices
                FsDsPortNum

                The Object 
                retValFsDsPortDropLevel3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsPortDropLevel3 (INT4 i4FsDsPortNum, INT4 *pi4RetValFsDsPortDropLevel3)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsPortDropLevel3 =
        (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel3;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsPortDropAtMaxBandwidth
 Input       :  The Indices
                FsDsPortNum

                The Object 
                retValFsDsPortDropAtMaxBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsPortDropAtMaxBandwidth (INT4 i4FsDsPortNum,
                                  INT4 *pi4RetValFsDsPortDropAtMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw == DS_TRUE)
    {
        *pi4RetValFsDsPortDropAtMaxBandwidth = DS_SNMP_TRUE;
    }
    else
    {
        *pi4RetValFsDsPortDropAtMaxBandwidth = DS_SNMP_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsPortREDCurveId
 Input       :  The Indices
                FsDsPortNum

                The Object 
                retValFsDsPortREDCurveId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsPortREDCurveId (INT4 i4FsDsPortNum, INT4 *pi4RetValFsDsPortREDCurveId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsPortREDCurveId =
        (DS_PORT_ENTRY (i4FsDsPortNum))->u4REDCurveId;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDsPortMaxBandwidth
 Input       :  The Indices
                FsDsPortNum

                The Object 
                setValFsDsPortMaxBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsPortMaxBandwidth (INT4 i4FsDsPortNum,
                            INT4 i4SetValFsDsPortMaxBandwidth)
{
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4MaxBw ==
        (UINT4) i4SetValFsDsPortMaxBandwidth)
    {
        return SNMP_SUCCESS;
    }

    (DS_PORT_ENTRY (i4FsDsPortNum))->u4MaxBw =
        (UINT4) i4SetValFsDsPortMaxBandwidth;

    /* Set this port's MODIFIED bit */
    DS_SET_PORT_DIRTY (i4FsDsPortNum);

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetPortParams (i4FsDsPortNum,
                               DS_PORT_ENTRY (i4FsDsPortNum),
                               DS_NP_PORT_PARAM_SET_MAXBW) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsPortDropLevel1
 Input       :  The Indices
                FsDsPortNum

                The Object 
                setValFsDsPortDropLevel1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsPortDropLevel1 (INT4 i4FsDsPortNum, INT4 i4SetValFsDsPortDropLevel1)
{
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel1 ==
        (UINT4) i4SetValFsDsPortDropLevel1)
    {
        return SNMP_SUCCESS;
    }

    (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel1 =
        (UINT4) i4SetValFsDsPortDropLevel1;

    /* Set this port's MODIFIED bit */
    DS_SET_PORT_DIRTY (i4FsDsPortNum);

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetPortParams (i4FsDsPortNum,
                               DS_PORT_ENTRY (i4FsDsPortNum),
                               DS_NP_PORT_PARAM_SET_DP1) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsPortDropLevel2
 Input       :  The Indices
                FsDsPortNum

                The Object 
                setValFsDsPortDropLevel2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsPortDropLevel2 (INT4 i4FsDsPortNum, INT4 i4SetValFsDsPortDropLevel2)
{
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel2 ==
        (UINT4) i4SetValFsDsPortDropLevel2)
    {
        return SNMP_SUCCESS;
    }

    (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel2 =
        (UINT4) i4SetValFsDsPortDropLevel2;

    /* Set this port's MODIFIED bit */
    DS_SET_PORT_DIRTY (i4FsDsPortNum);

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetPortParams (i4FsDsPortNum,
                               DS_PORT_ENTRY (i4FsDsPortNum),
                               DS_NP_PORT_PARAM_SET_DP2) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsPortDropLevel3
 Input       :  The Indices
                FsDsPortNum

                The Object 
                setValFsDsPortDropLevel3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsPortDropLevel3 (INT4 i4FsDsPortNum, INT4 i4SetValFsDsPortDropLevel3)
{
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel3 ==
        (UINT4) i4SetValFsDsPortDropLevel3)
    {
        return SNMP_SUCCESS;
    }

    (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropLevel3 =
        (UINT4) i4SetValFsDsPortDropLevel3;

    /* Set this port's MODIFIED bit */
    DS_SET_PORT_DIRTY (i4FsDsPortNum);

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetPortParams (i4FsDsPortNum,
                               DS_PORT_ENTRY (i4FsDsPortNum),
                               DS_NP_PORT_PARAM_SET_DP3) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsPortDropAtMaxBandwidth
 Input       :  The Indices
                FsDsPortNum

                The Object 
                setValFsDsPortDropAtMaxBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsPortDropAtMaxBandwidth (INT4 i4FsDsPortNum,
                                  INT4 i4SetValFsDsPortDropAtMaxBandwidth)
{
    if (i4SetValFsDsPortDropAtMaxBandwidth == DS_SNMP_TRUE)
    {
        if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw == DS_TRUE)
        {
            return SNMP_SUCCESS;
        }

        (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw = DS_TRUE;
    }
    else
    {
        if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw == DS_FALSE)
        {
            return SNMP_SUCCESS;
        }

        (DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw = DS_FALSE;
    }

    /* Set this port's MODIFIED bit */
    DS_SET_PORT_DIRTY (i4FsDsPortNum);

#ifdef NPAPI_WANTED
    /* Set Drop level1  - to 0 if DropAtMaxBw is true
     *                  - to the actual value if DropAtMaxBw is false */
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetPortParams (i4FsDsPortNum,
                               DS_PORT_ENTRY (i4FsDsPortNum),
                               DS_NP_PORT_PARAM_SET_DP1) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsPortREDCurveId
 Input       :  The Indices
                FsDsPortNum

                The Object 
                setValFsDsPortREDCurveId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsPortREDCurveId (INT4 i4FsDsPortNum, INT4 i4SetValFsDsPortREDCurveId)
{
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4REDCurveId ==
        (UINT4) i4SetValFsDsPortREDCurveId)
    {
        return SNMP_SUCCESS;
    }

    (DS_PORT_ENTRY (i4FsDsPortNum))->u4REDCurveId =
        (UINT4) i4SetValFsDsPortREDCurveId;

    /* Set this port's MODIFIED bit */
    DS_SET_PORT_DIRTY (i4FsDsPortNum);

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        if (DsHwSetPortParams (i4FsDsPortNum,
                               DS_PORT_ENTRY (i4FsDsPortNum),
                               DS_NP_PORT_PARAM_SET_REDCURVE) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDsPortMaxBandwidth
 Input       :  The Indices
                FsDsPortNum

                The Object 
                testValFsDsPortMaxBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsPortMaxBandwidth (UINT4 *pu4ErrorCode, INT4 i4FsDsPortNum,
                               INT4 i4TestValFsDsPortMaxBandwidth)
{
    UINT4               u4TCId;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsPortMaxBandwidth < 1 ||
        i4TestValFsDsPortMaxBandwidth > DS_MAX_MAX_BW)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if atleast one traffic class is attached to this port */
    for (u4TCId = DS_NUM_RESERVED_TRAFFIC_CLASSES + 1;
         u4TCId <= DS_MAX_TC; u4TCId++)
    {
        if (DS_TC_ENTRY (u4TCId) == NULL)
        {
            continue;
        }

        if ((DS_TC_ENTRY (u4TCId))->u4PortNum == (UINT4) i4FsDsPortNum)
        {
            return SNMP_SUCCESS;
        }
    }
    /* No traffic class attached to this port, cannot set max bw */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_DS_PORT_MAXBW_ERR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsPortDropLevel1
 Input       :  The Indices
                FsDsPortNum

                The Object 
                testValFsDsPortDropLevel1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsPortDropLevel1 (UINT4 *pu4ErrorCode, INT4 i4FsDsPortNum,
                             INT4 i4TestValFsDsPortDropLevel1)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Drop levels cannot be configured when DropAtMaxBw is set 
     * i.e. when metering is enabled */
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw == DS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_DROP_AT_MAXBW_ERR);
        return SNMP_FAILURE;
    }

    /* 14 - 27 */
    if (i4TestValFsDsPortDropLevel1 < DS_MIN_DROP_LEVEL1 ||
        i4TestValFsDsPortDropLevel1 > DS_MAX_DROP_LEVEL1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsPortDropLevel2
 Input       :  The Indices
                FsDsPortNum

                The Object 
                testValFsDsPortDropLevel2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsPortDropLevel2 (UINT4 *pu4ErrorCode, INT4 i4FsDsPortNum,
                             INT4 i4TestValFsDsPortDropLevel2)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Drop levels cannot be configured when DropAtMaxBw is set */
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw == DS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_DROP_AT_MAXBW_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsPortDropLevel2 < DS_MIN_DROP_LEVEL2 ||
        i4TestValFsDsPortDropLevel2 > DS_MAX_DROP_LEVEL2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsPortDropLevel3
 Input       :  The Indices
                FsDsPortNum

                The Object 
                testValFsDsPortDropLevel3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsPortDropLevel3 (UINT4 *pu4ErrorCode, INT4 i4FsDsPortNum,
                             INT4 i4TestValFsDsPortDropLevel3)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Drop levels cannot be configured when DropAtMaxBw is set 
     * i.e. when metering is enabled */
    if ((DS_PORT_ENTRY (i4FsDsPortNum))->u4DropAtMaxBw == DS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_DROP_AT_MAXBW_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsPortDropLevel3 < DS_MIN_DROP_LEVEL3 ||
        i4TestValFsDsPortDropLevel3 > DS_MAX_DROP_LEVEL3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsPortDropAtMaxBandwidth
 Input       :  The Indices
                FsDsPortNum

                The Object 
                testValFsDsPortDropAtMaxBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsPortDropAtMaxBandwidth (UINT4 *pu4ErrorCode, INT4 i4FsDsPortNum,
                                     INT4 i4TestValFsDsPortDropAtMaxBandwidth)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDsPortDropAtMaxBandwidth != DS_SNMP_TRUE) &&
        (i4TestValFsDsPortDropAtMaxBandwidth != DS_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsPortREDCurveId
 Input       :  The Indices
                FsDsPortNum

                The Object 
                testValFsDsPortREDCurveId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsPortREDCurveId (UINT4 *pu4ErrorCode, INT4 i4FsDsPortNum,
                             INT4 i4TestValFsDsPortREDCurveId)
{
    tDiffServREDEntry  *pREDEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateIfIndex (i4FsDsPortNum) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* DS_MAX_REDCURVES + 1 is used to indicate that RED curve is
     * disabled on this port */
    if (i4TestValFsDsPortREDCurveId < DS_MIN_PORT_RED_CURVE ||
        i4TestValFsDsPortREDCurveId > DS_MAX_REDCURVES + 1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsPortREDCurveId == DS_MAX_REDCURVES + 1)
    {
        return SNMP_SUCCESS;
    }

    /* The corresponding RED curve should have already been created */
    if ((pREDEntry = DS_RED_ENTRY (i4TestValFsDsPortREDCurveId)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_RED_CURVE_ERR);
        return SNMP_FAILURE;
    }

    if (pREDEntry->u4REDCurveStatus != DS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DS_INVALID_RED_CURVE_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDsREDConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDsREDConfigTable
 Input       :  The Indices
                FsDsREDCurveId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDsREDConfigTable (INT4 i4FsDsREDCurveId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsDsREDCurveId < 1) || (i4FsDsREDCurveId > DS_MAX_REDCURVES))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDsREDConfigTable
 Input       :  The Indices
                FsDsREDCurveId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDsREDConfigTable (INT4 *pi4FsDsREDCurveId)
{
    return (nmhGetNextIndexFsDsREDConfigTable (DS_MIN_REDCURVES - 1,
                                               pi4FsDsREDCurveId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDsREDConfigTable
 Input       :  The Indices
                FsDsREDCurveId
                nextFsDsREDCurveId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDsREDConfigTable (INT4 i4FsDsREDCurveId,
                                   INT4 *pi4NextFsDsREDCurveId)
{
    UINT4               u4REDCurveId;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    for (u4REDCurveId = i4FsDsREDCurveId + 1; u4REDCurveId <= DS_MAX_REDCURVES;
         u4REDCurveId++)
    {
        if (DS_RED_ENTRY (u4REDCurveId) != NULL)
        {
            *pi4NextFsDsREDCurveId = u4REDCurveId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

INT4
DsValidateREDCurveId (INT4 i4FsDsREDCurveId)
{
    if ((i4FsDsREDCurveId < DS_MIN_REDCURVES) ||
        (i4FsDsREDCurveId > DS_MAX_REDCURVES))
    {
        return DS_FAILURE;
    }

    if (DS_RED_ENTRY (i4FsDsREDCurveId) == NULL)
    {
        return DS_FAILURE;
    }

    return DS_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsREDCurveStart
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                retValFsDsREDCurveStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsREDCurveStart (INT4 i4FsDsREDCurveId,
                         INT4 *pi4RetValFsDsREDCurveStart)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsREDCurveStart = (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDStart;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsREDCurveStop
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                retValFsDsREDCurveStop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsREDCurveStop (INT4 i4FsDsREDCurveId, INT4 *pi4RetValFsDsREDCurveStop)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsREDCurveStop = (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDStop;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsREDCurveQRange
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                retValFsDsREDCurveQRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsREDCurveQRange (INT4 i4FsDsREDCurveId,
                          INT4 *pi4RetValFsDsREDCurveQRange)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsREDCurveQRange =
        (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDQRange;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsREDCurveStopProbability
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                retValFsDsREDCurveStopProbability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsREDCurveStopProbability (INT4 i4FsDsREDCurveId,
                                   INT4 *pi4RetValFsDsREDCurveStopProbability)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsREDCurveStopProbability =
        (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDStopProbability;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsREDCurveStatus
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                retValFsDsREDCurveStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsREDCurveStatus (INT4 i4FsDsREDCurveId,
                          INT4 *pi4RetValFsDsREDCurveStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsDsREDCurveStatus =
        (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDCurveStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDsREDCurveStart
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                setValFsDsREDCurveStart
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsREDCurveStart (INT4 i4FsDsREDCurveId, INT4 i4SetValFsDsREDCurveStart)
{
    if (DS_RED_ENTRY (i4FsDsREDCurveId) == NULL)
    {
        return SNMP_FAILURE;
    }

    (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDStart =
        (UINT4) i4SetValFsDsREDCurveStart;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsREDCurveStop
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                setValFsDsREDCurveStop
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsREDCurveStop (INT4 i4FsDsREDCurveId, INT4 i4SetValFsDsREDCurveStop)
{
    if (DS_RED_ENTRY (i4FsDsREDCurveId) == NULL)
    {
        return SNMP_FAILURE;
    }

    (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDStop =
        (UINT4) i4SetValFsDsREDCurveStop;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsREDCurveQRange
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                setValFsDsREDCurveQRange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsREDCurveQRange (INT4 i4FsDsREDCurveId,
                          INT4 i4SetValFsDsREDCurveQRange)
{
    if (DS_RED_ENTRY (i4FsDsREDCurveId) == NULL)
    {
        return SNMP_FAILURE;
    }

    (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDQRange =
        (UINT4) i4SetValFsDsREDCurveQRange;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsREDCurveStopProbability
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                setValFsDsREDCurveStopProbability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsREDCurveStopProbability (INT4 i4FsDsREDCurveId,
                                   INT4 i4SetValFsDsREDCurveStopProbability)
{
    if (DS_RED_ENTRY (i4FsDsREDCurveId) == NULL)
    {
        return SNMP_FAILURE;
    }

    (DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDStopProbability =
        (UINT4) i4SetValFsDsREDCurveStopProbability;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsREDCurveStatus
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                setValFsDsREDCurveStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsREDCurveStatus (INT4 i4FsDsREDCurveId,
                          INT4 i4SetValFsDsREDCurveStatus)
{
    tDiffServREDEntry  *pDiffServREDEntry = NULL;
    pDiffServREDEntry = DS_RED_ENTRY (i4FsDsREDCurveId);

    if (pDiffServREDEntry != NULL)
    {
        if (pDiffServREDEntry->u4REDCurveStatus ==
            (UINT1) i4SetValFsDsREDCurveStatus)
        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDsREDCurveStatus == DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This RED Curve is not present in the Database */
        if (i4SetValFsDsREDCurveStatus != DS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsDsREDCurveStatus)
    {
        case DS_CREATE_AND_WAIT:

            if (DS_REDENTRY_ALLOC_MEM_BLOCK (&pDiffServREDEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free RED Curve Entry \n");
                return SNMP_FAILURE;
            }

            DS_RED_ENTRY (i4FsDsREDCurveId) = pDiffServREDEntry;

            DS_MEMSET (pDiffServREDEntry, 0, sizeof (tDiffServREDEntry));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDiffServREDEntry->u4REDCurveStatus = DS_NOT_IN_SERVICE;
            break;

        case DS_NOT_IN_SERVICE:

#ifdef NPAPI_WANTED
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                if (DsHwSetREDParams (i4FsDsREDCurveId,
                                      DS_RED_ENTRY (i4FsDsREDCurveId),
                                      DS_NP_RED_CURVE_DISABLE) == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            pDiffServREDEntry->u4REDCurveStatus = DS_NOT_IN_SERVICE;

            break;

        case DS_ACTIVE:

#ifdef NPAPI_WANTED
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                if (DsHwSetREDParams (i4FsDsREDCurveId,
                                      DS_RED_ENTRY (i4FsDsREDCurveId),
                                      DS_NP_RED_CURVE_ENABLE) == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pDiffServREDEntry->u4REDCurveStatus = DS_ACTIVE;

            break;

        case DS_DESTROY:

#ifdef NPAPI_WANTED
            if (gDsGlobalInfo.DsStatus == DS_ENABLE)
            {
                if (DsHwSetREDParams (i4FsDsREDCurveId,
                                      DS_RED_ENTRY (i4FsDsREDCurveId),
                                      DS_NP_RED_CURVE_DISABLE) == FNP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            /* Delete the RED Curve from the Software */
            if (DS_REDENTRY_FREE_MEM_BLOCK (pDiffServREDEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "Free Mem block for RED Curve Entry failed !!!\n");

                return SNMP_FAILURE;
            }

            DS_RED_ENTRY (i4FsDsREDCurveId) = NULL;

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDsREDCurveStart
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                testValFsDsREDCurveStart
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsREDCurveStart (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                            INT4 i4TestValFsDsREDCurveStart)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsREDCurveStart < 1 || i4TestValFsDsREDCurveStart > 16383)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* The row status should be set as NotInService before setting any
     * field in this RED Curve entry */
    if ((DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDCurveStatus !=
        DS_NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsREDCurveStop
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                testValFsDsREDCurveStop
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsREDCurveStop (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                           INT4 i4TestValFsDsREDCurveStop)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsREDCurveStop < 1 || i4TestValFsDsREDCurveStop > 16383)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* The row status should be set as NotInService before setting any
     * field in this RED Curve entry */
    if ((DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDCurveStatus !=
        DS_NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsREDCurveQRange
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                testValFsDsREDCurveQRange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsREDCurveQRange (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                             INT4 i4TestValFsDsREDCurveQRange)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsREDCurveQRange < DS_MIN_RED_QRANGE ||
        i4TestValFsDsREDCurveQRange > DS_MAX_RED_QRANGE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* The row status should be set as NotInService before setting any
     * field in this RED Curve entry */
    if ((DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDCurveStatus !=
        DS_NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsREDCurveStopProbability
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                testValFsDsREDCurveStopProbability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsREDCurveStopProbability (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDsREDCurveId,
                                      INT4 i4TestValFsDsREDCurveStopProbability)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DsValidateREDCurveId (i4FsDsREDCurveId) == DS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFsDsREDCurveStopProbability < 0 ||
        i4TestValFsDsREDCurveStopProbability > DS_MAX_RED_STOPPROB)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* The row status should be set as NotInService before setting any
     * field in this RED Curve entry */
    if ((DS_RED_ENTRY (i4FsDsREDCurveId))->u4REDCurveStatus !=
        DS_NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDsREDCurveStatus
 Input       :  The Indices
                FsDsREDCurveId

                The Object 
                testValFsDsREDCurveStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsREDCurveStatus (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                             INT4 i4TestValFsDsREDCurveStatus)
{
    tDiffServREDEntry  *pDiffServREDEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4FsDsREDCurveId < 1) || (i4FsDsREDCurveId > DS_MAX_REDCURVES))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pDiffServREDEntry = DS_RED_ENTRY (i4FsDsREDCurveId);

    if (pDiffServREDEntry != NULL)
    {
        if (i4TestValFsDsREDCurveStatus == DS_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This RED Curve is not present in the Database */
        if (i4TestValFsDsREDCurveStatus != DS_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsDsREDCurveStatus < DS_ACTIVE) ||
        (i4TestValFsDsREDCurveStatus > DS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check row status dependencies */

    switch (i4TestValFsDsREDCurveStatus)
    {
        case DS_NOT_IN_SERVICE:

            if (pDiffServREDEntry->u4REDCurveStatus != DS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case DS_ACTIVE:

            if (pDiffServREDEntry->u4REDCurveStatus != DS_NOT_IN_SERVICE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* The max. difference between REDStop and REDStart can only be
             * 256 MB since this is the max. value of QRange in the Hardware.
             * Since Stop and Start are stored as multiples of 16KB 
             * blocks the max. difference between them translates as 
             * 16000 units */
            if ((pDiffServREDEntry->u4REDStop - pDiffServREDEntry->u4REDStart)
                > 16000)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_DS_QRANGE_EXCEED_ERR);
                return SNMP_FAILURE;
            }

            break;

        case DS_DESTROY:

            if (pDiffServREDEntry->u4REDCurveStatus == DS_ACTIVE)
            {
                /* Check if any active Traffic class is using this 
                 * RED Curve  */
                if (DsCheckTCForREDCurveId (i4FsDsREDCurveId) != 0)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_DS_RED_IN_USE_ERR);
                    return SNMP_FAILURE;
                }
            }

            break;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrValidateDiffServInProfileActionEntry
 *  Description     : This function validates the DiffServInProfileActionEntry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateDiffServInProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
#define   INPROFILE_ACTION_FLAG_INDEX     2
    UINT4               u4Flag;
#ifdef SNMP_2_WANTED
    UINT4               u4Error;
#endif
    tSNMP_OID_TYPE      FlagOid;
    UINT4               au4OidList[30];
    UINT4               u4FlagIndex = 0;
    tSNMP_MULTI_DATA_TYPE Data;
    tSnmpIndex          snmpIndex;
    tSNMP_MULTI_DATA_TYPE Index;

    /* Static flag mapping for the MIB objects */
    UINT4               au4ActionFlag[] = { 0, 0, 0, FS_DS_ACTN_INSERT_PRIO,
/*4*/ FS_DS_ACTN_INSERT_TOSP,
/* 5*/ FS_DS_ACTN_SET_OUT_PORT_UCAST,
/*6*/ FS_DS_ACTN_INSERT_DSCP,
/*7*/ 0
    };

    if (pOid->u4_Length == 13)
    {
        /* row status is saved twice, the second
         * save is as per the MIB (table walk), the first
         * save is implementation specific
         * and the OID is passed has the column identifier
         * unlike for other cases. In this case OID len is 13
         */
        return (1 /* MSR_SAVE */ );
    }
    /* Get the OID for Action Flag */
    FlagOid.pu4_OidList = au4OidList;
    DS_MEMSET (&au4OidList[0], 0, sizeof (au4OidList));
    DS_MEMCPY (&au4OidList[0], pOid->pu4_OidList,
               sizeof (UINT4) * pOid->u4_Length);
    DS_MEMCPY (&au4OidList[pOid->u4_Length], pInstance->pu4_OidList,
               sizeof (UINT4) * pInstance->u4_Length);
    FlagOid.u4_Length = pOid->u4_Length + pInstance->u4_Length;
    au4OidList[FlagOid.u4_Length - 2] = INPROFILE_ACTION_FLAG_INDEX;

    /* Get the InProfile Action Flag */
    DS_MEMSET (&Index, 0, sizeof (Index));
    snmpIndex.u4No = 0;
    snmpIndex.pIndex = &Index;

    /* The Flag index here is actually the MIB column identifier 
     * In the case of row status save initially the 
     * instance pu4_OidList[0] has the instance number 
     * and in other case it has the column identifier
     * which serves as the index into the au4ActionFlag array.
     */

    u4FlagIndex = pInstance->pu4_OidList[0];

#ifdef SNMP_2_WANTED
    SNMPGet (FlagOid, &Data, &u4Error, &snmpIndex, SNMP_DEFAULT_CONTEXT);
#endif

    u4Flag = Data.u4_ULongValue;

    /* This validation is required so as that only
     * the MIB variables that are used as per the action
     * flag are saved. example if the ActionFlag indicates
     * the action to be policed_dscp, then only the policed dscp
     * value needs to be stored, the policed_precedence value
     * or MIB variable will not be stored for the given row 
     */

    if (au4ActionFlag[u4FlagIndex] && !(u4Flag & au4ActionFlag[u4FlagIndex]))
    {
        return (2 /*MSR_SKIP */ );
    }

    UNUSED_PARAM (pData);
    return (1 /*MSR_SAVE */ );
}                                /* MsrValidateDiffServInProfileActionEntry */

#ifdef WEBNM_WANTED
/****************************************************************************
 *  Function Name   : DsWebnmGetPolicyMapEntry
 *  Description     : This function fetches the DiffServPolicyMapEntries     
 *                    and fills it in a pointer based on the CLFR Flags set 
 *  Input           : i4PolicyMapId - PolicyMap entry index to create.
 *                    pClfrData     - Policy Map entry ptr to be returned to 
 *                                    WEB
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE               
 *  NOTE            : Diffserver Protocol Lock (DFS_LOCK ()) MUST be taken
 *                    before calling this function.
 *****************************************************************************/
INT4
DsWebnmGetPolicyMapEntry (INT4 i4PolicyMapId, tDiffServWebClfrData * pClfrData)
{
    UINT4               u4Value;
    INT4                i4OutCome = 0;
    INT4                i4InProfActionId;

    MEMSET (pClfrData, 0, sizeof (tDiffServWebClfrData));

    i4OutCome = nmhGetFsDiffServClfrInProActionId (i4PolicyMapId,
                                                   &i4InProfActionId);
    if ((i4OutCome != SNMP_FAILURE) && (i4InProfActionId != 0))
    {
        if (nmhGetFsDiffServInProfileActionFlag
            (i4InProfActionId, &u4Value) == SNMP_SUCCESS)
        {
            if (u4Value & FS_DS_ACTN_INSERT_TOSP)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType,
                         "Policed-Precedence");
                nmhGetFsDiffServInProfileActionIpTOS (i4InProfActionId,
                                                      &u4Value);
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, "%u",
                         u4Value);
            }
            else if (u4Value & FS_DS_ACTN_INSERT_DSCP)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType,
                         "Policed-Dscp");
                nmhGetFsDiffServInProfileActionDscp (i4InProfActionId,
                                                     &u4Value);
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, "%u",
                         u4Value);
            }
            else if (u4Value & FS_DS_ACTN_INSERT_PRIO)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType,
                         "Policed-Priority");
                nmhGetFsDiffServInProfileActionNewPrio (i4InProfActionId,
                                                        &u4Value);

                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, "%d",
                         u4Value);
            }
            else
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType, "-");
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, " ");
            }
        }
    }
    else
    {
        SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType, "-");
        SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, " ");
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function Name   : DsWebnmSetPolicyMapEntry
 *  Description     : This function sets the DiffServPolicyMapEntries     
 *                    based on the Flag value passed
 *  Input           : pClfrEntry  - PolicyMap data entry values.
 *                    pu1ErrString - Error string returned to WEB in case of 
 *                                   error.
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE               
 *  NOTE            : Diffserver Protocol Lock (DFS_LOCK ()) MUST be taken
 *                    before calling this function.
 *****************************************************************************/
INT4
DsWebnmSetPolicyMapEntry (tDiffServWebSetClfrEntry * pClfrEntry,
                          UINT1 *pu1ErrString)
{
    UINT4               u4ErrorCode;

    /* Policy-Map Creation */
    if (nmhSetFsDiffServClfrStatus
        (pClfrEntry->i4DsClfrId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, " Unable to create Policy-Map ID/Entry");
        return SNMP_FAILURE;
    }

    /* In-Profile Action */
    if (nmhSetFsDiffServInProfileActionStatus
        (pClfrEntry->i4DsClfrInProActionId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Unable to create In-Profile Action Entry");
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    else
    {
        if (pClfrEntry->u4InProfActType == 1)
        {
            if (nmhTestv2FsDiffServInProfileActionDscp
                (&u4ErrorCode, pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InDscp) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid In-Profile Dscp Value");
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServInProfileActionDscp
                (pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InDscp) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Unable to set In-Profile Dscp Value");
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
                return SNMP_FAILURE;
            }
        }
        else if (pClfrEntry->u4InProfActType == 2)
        {
            if (nmhTestv2FsDiffServInProfileActionIpTOS
                (&u4ErrorCode, pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InPrec) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid In-Profile Precedence Value");
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServInProfileActionIpTOS
                (pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InPrec) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString,
                        "Unable to set In-Profile Precedence Value");
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
                return SNMP_FAILURE;
            }
        }
        else if (pClfrEntry->u4InProfActType == 3)
        {
            if (nmhTestv2FsDiffServInProfileActionNewPrio
                (&u4ErrorCode, pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4CosValue) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid COS Value");
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServInProfileActionNewPrio
                (pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4CosValue) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Unable to set COS Value");
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
                return SNMP_FAILURE;
            }
        }
        if (nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_ACTIVE) == SNMP_FAILURE)
        {
            STRCPY (pu1ErrString, "In-Profile Action Entry Activation Failed");
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
            return SNMP_FAILURE;
        }
    }

    /* Setting The Ids to Policy-Map */
    if (nmhTestv2FsDiffServClfrMFClfrId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrMFClfrId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Class-Map Id");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhSetFsDiffServClfrMFClfrId
        (pClfrEntry->i4DsClfrId, pClfrEntry->i4DsClfrMFClfrId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Unable to set Class-Map Id");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsDiffServClfrInProActionId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrInProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid In-Profile Id");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhSetFsDiffServClfrInProActionId
        (pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrInProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Unable to set In-Profile Id");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsDiffServClfrTrafficClassId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrTrafficClassId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Traffic Class Id");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }

    if (nmhSetFsDiffServClfrTrafficClassId
        (pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrTrafficClassId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Unable to set Traffic Class Id");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                    DS_ACTIVE) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Unable to activate Policy-Map entry ");
        nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
        nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  DsNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
DsNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsissd, (sizeof (fsissd) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsDsSystemControl,
                      (sizeof (FsDsSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the diff serv shutdown, with 
     * diff serv oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MsrNotifyProtocolShutdown (au1ObjectOid, 2, MSR_INVALID_CNTXT);
#endif
    return;
}
#endif
