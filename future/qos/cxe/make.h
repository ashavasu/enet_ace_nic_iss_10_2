#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30/12/2005                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options for building the        |
# |                            FutureISS CXE Diffsrv object file.            |
# |                                                                          |
# +--------------------------------------------------------------------------+
#

PRJ_NAME		= FutureQOS
PRJ_BASE_DIR	= ${BASE_DIR}/qos/cxe
PRJ_SOURCE_DIR	= ${PRJ_BASE_DIR}/src
PRJ_INCLUDE_DIR	= ${PRJ_BASE_DIR}/inc
PRJ_OBJECT_DIR	= ${PRJ_BASE_DIR}/obj
FUTURE_INC_DIR		= $(BASE_DIR)/inc

# Specify the project include directories

PRJ_FINAL_INCLUDE_DIRS	=  -I$(PRJ_INCLUDE_DIR) \
                             $(COMMON_INCLUDE_DIRS) \
                           -I$(FUTURE_INC_DIR)


# Specify the project dependencies

PRJ_INCLUDE_FILES  = $(PRJ_INCLUDE_DIR)/dscxemacro.h \
			            $(PRJ_INCLUDE_DIR)/dscxeglob.h \
			            $(PRJ_INCLUDE_DIR)/dscxetdfs.h \
			            $(PRJ_INCLUDE_DIR)/dscxeinc.h \
			            $(PRJ_INCLUDE_DIR)/fsissdlw.h \
			            $(PRJ_INCLUDE_DIR)/fsissdwr.h \
			            $(PRJ_INCLUDE_DIR)/dscxeextn.h \
			            $(PRJ_INCLUDE_DIR)/dscxetrc.h


PRJ_FINAL_INCLUDE_FILES  +=  $(PRJ_INCLUDE_FILES)

PRJ_DEPENDENCIES	=    $(COMMON_DEPENDENCIES)\
                       $(PRJ_FINAL_INCLUDE_FILES) \
				           $(PRJ_BASE_DIR)/Makefile \
				           $(PRJ_BASE_DIR)/make.h

