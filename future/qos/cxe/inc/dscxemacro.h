#ifndef _DSCXEMACRO_H
#define _DSCXEMACRO_H


/*****************************************************************************/
/* Enumerated Constants                                                      */
/*****************************************************************************/

#define DFS_MUT_EXCL_SEM_NAME        (const UINT1 *) "DSSM"
#define DFS_MUT_EXCL_SEM_ID          gSemId
#define tDsMemPoolId                 tMemPoolId

#define DS_MAX_NO_PORTS              BRG_MAX_PHY_PORTS

#define DS_ETHERNET_ADDR_SIZE        6
#define DS_MAX_VLAN_ID               4094
#define DS_MIN_VLAN_ID               1

#define DS_MAX_CIDR                  32
#define DS_STATUS                    (gDsGlobalInfo.DsStatus)

#define DS_MAX_PORT_MASK             16

#define DS_INPROFILEACTION_MASK      ( FS_DS_ACTN_INSERT_PRIO            |\
				                       FS_DS_ACTN_SET_COS_QUEUE          |\
                                                       FS_DS_ACTN_INSERT_TOSP            |\
                                                       FS_DS_ACTN_COPY_TO_CPU            |\
                                                       FS_DS_ACTN_DO_NOT_SWITCH          |\
                                                       FS_DS_ACTN_SET_OUT_PORT_UCAST     |\
                                                       FS_DS_ACTN_COPY_TO_MIRROR         |\
                                                       FS_DS_ACTN_INCR_FFPPKT_COUNTER    |\
                                                       FS_DS_ACTN_INSERT_PRIO_FROM_TOSP  |\
                                                       FS_DS_ACTN_INSERT_TOSP_FROM_PRIO  |\
                                                       FS_DS_ACTN_INSERT_DSCP            |\
                                                       FS_DS_ACTN_SET_OUT_PORT_NON_UCAST |\
                                                       FS_DS_ACTN_DO_SWITCH              |\
                                                       FS_DS_ACTN_DROP_PRECEDENCE        |\
									   FS_DS_ACTN_SET_OUT_PORT_ALL       )

#define DS_OUTPROFILEACTION_MASK     ( FS_DS_OUT_ACTN_COPY_TO_CPU    |\
				                       FS_DS_OUT_ACTN_DO_NOT_SWITCH  |\
                                                       FS_DS_OUT_ACTN_INSERT_DSCP    |\
                                                       FS_DS_OUT_ACTN_DO_SWITCH      |\
                                                       FS_DS_OUT_ACTN_DROP_PRECEDENCE)


#define DS_MAX_SUBNET_BIT_NO         32
	
#define DS_MIN_PRECEDENCE_VALUE      (-0x80000000)
#define DS_MAX_PRECEDENCE_VALUE      0x7fffffff

#define DS_MAX_PROTOCOL_NO           255
#define DS_MAX_PRECEDENCE             7
#define DS_MIN_IP_HLEN               5
#define DS_MAX_IP_HLEN               5
                                                
#define DS_DEFVAL_PROTOCOL_NO        255
#define DS_DEFVAL_PORT_MAX           65535

#define DS_IP_VERSION_4              4
#define DS_IP_VERSION_6              6

#define DS_INPROACT_FLAGS            13
#define DS_OUTPROACT_FLAGS           5

#define DS_MIN_REFRESH_COUNT         NP_DIFFSRV_MIN_REFRESH_COUNT
#define DS_MAX_REFRESH_COUNT         NP_DIFFSRV_MAX_REFRESH_COUNT

#define DS_MIN_PORT_VALUE            0
#define DS_MAX_PORT_VALUE            65536

#define DS_MIN_FILTER_ID             1
#define DS_MAX_FILTER_ID             65535

#define DS_MAX_DSCP_VALUE            63
#define DS_MAX_PRIO_VALUE            7

#define DS_ETHER_TYPE_802DOT3        1   /* change the value accodingly */
#define DS_ETHER_TYPE_ETHII          2   /* change the value accodingly */

#define DS_MEMORY_TYPE               MEM_DEFAULT_MEMORY_TYPE
#define DS_MIN_QUEUE_COUNT           1
#define DS_MAX_QUEUE_COUNT           4

/* Macros for return Values */
#define DS_MEM_SUCCESS               MEM_SUCCESS
#define DS_MEM_FAILURE               MEM_FAILURE

/*Validation Routines constants */
#define DS_MIN_MFCLFR_ID             0 
#define DS_MAX_MFCLFR_ID             0x7FFFFFFF
#define DS_MIN_CLFR_ID               0
#define DS_MAX_CLFR_ID               0x7FFFFFFF
#define DS_MIN_IN_PROACT_ID          0
#define DS_MAX_IN_PROACT_ID          0x7FFFFFFF

/* RowStatus values */
#define DS_ACTIVE                    ACTIVE
#define DS_NOT_IN_SERVICE            NOT_IN_SERVICE 
#define DS_NOT_READY                 NOT_READY
#define DS_CREATE_AND_GO             CREATE_AND_GO
#define DS_CREATE_AND_WAIT           CREATE_AND_WAIT
#define DS_DESTROY                   DESTROY

/* Macros for the Memory Operations */
#define DS_MEMCPY                    MEMCPY
#define DS_MEMSET                    MEMSET
#define DS_MEMCMP                    MEMCMP
#define DS_STRLEN                    STRLEN
#define DS_STRCMP                    STRCMP
#define DS_STRCPY                    STRCPY

/* Memory Pool ID Macros */
#define DS_MFCLFR_MEMPOOL_ID         gDsGlobalInfo.DsMfClfrPoolId
#define DS_CLFR_MEMPOOL_ID           gDsGlobalInfo.DsClfrPoolId
#define DS_INPROACT_MEMPOOL_ID       gDsGlobalInfo.DsInProfileActionPoolId

/* Memory Block Size Macros */
#define DS_MFCLFR_MEMBLK_SIZE        sizeof(tDiffServMultiFieldClfrEntry)
#define DS_CLFR_MEMBLK_SIZE          sizeof(tDiffServClfrEntry)
#define DS_INPROACT_MEMBLK_SIZE      sizeof(tDiffServInProfileActionEntry)

/* Memory Block Count Macros */
#define DS_MFCLFR_MEMBLK_COUNT       10
#define DS_CLFR_MEMBLK_COUNT         10
#define DS_INPROACT_MEMBLK_COUNT     10




/* Various Lists */
#define DS_MFCLFR_LIST               gDsGlobalInfo.DsMfClfrListHead
#define DS_CLFR_LIST                 gDsGlobalInfo.DsClfrListHead
#define DS_INPROACT_LIST             gDsGlobalInfo.DsInProfileActionListHead

#define DS_MFCLFR_LIST_HEAD          (&DS_MFCLFR_LIST)->Tail
#define DS_CLFR_LIST_HEAD            (&DS_CLFR_LIST)->Tail
#define DS_INPROACT_LIST_HEAD        (&DS_INPROACT_LIST)->Tail

/* Return Values */
#define DS_ERR_MEM_FAILURE           1
#define DS_ERR_NULL_PTR              2
#define DS_ERR_NO_ENTRY              3
#define DS_ERR_TASK                  4
#define DS_ERR_QUEUE                 5
#define DS_ERR_INVALID_VALUE         6

/* Invalid Values */
#define DS_INVALID_VAL               0
#define DS_INIT_VAL                  0
#define DS_INVALID_DSCP             -1

/* Debug related definitions */
#define DS_SYS_TRACE                 0x01
#define DS_IF_TRACE                  0x02
#define DS_PROT_TRACE                0x03
#define DS_TRACE_ALL                 0xFF

/* Macro for creating memory pools */
#define DS_CREATE_ENTRY_MEM_POOL(x,y,pDsPoolId)\
	         MemCreateMemPool(x,y,DS_MEMORY_TYPE,(tDsMemPoolId*)pDsPoolId)

/* Macro for deleting Memory Pools */
#define DS_DELETE_ENTRY_MEM_POOL(DsPoolId)\
                 MemDeleteMemPool((tDsMemPoolId) DsPoolId)

/* Macros for Memory Block Allocation from Memory Pool */

#define DS_MFCLFRENTRY_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_MFCLFR_MEMPOOL_ID, (UINT1 **)ppu1Block)

#define DS_CLFRENTRY_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_CLFR_MEMPOOL_ID, (UINT1 **)ppu1Block)

#define DS_INPROACTENTRY_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_INPROACT_MEMPOOL_ID, (UINT1 **)ppu1Block)

/* Macros for Freeing Memory Blocks from Memory Pools */
#define DS_CONFIGENTRY_FREE_MEM_BLOCK(pu1Block)\
	         MemReleaseMemBlock(DS_CONFIGENTRY_POOL_ID, (UINT1 *)pu1Block)

#define DS_MFCLFRENTRY_FREE_MEM_BLOCK(pu1Block)\
	        MemReleaseMemBlock(DS_MFCLFR_MEMPOOL_ID, (UINT1 *)pu1Block)

#define DS_CLFRENTRY_FREE_MEM_BLOCK(pu1Block)\
	        MemReleaseMemBlock(DS_CLFR_MEMPOOL_ID, (UINT1 *)pu1Block)

#define DS_INPROACTENTRY_FREE_MEM_BLOCK(pu1Block)\
	        MemReleaseMemBlock(DS_INPROACT_MEMPOOL_ID, (UINT1 *)pu1Block)

/* Macros for List Operations */
#define DS_SLL_INIT(pList) \
              TMO_SLL_Init(pList)

#define DS_SLL_INIT_NODE(pNode) \
	      TMO_SLL_Init_Node(pNode)

#define DS_SLL_ADD(pList, pNode) \
	      TMO_SLL_Add(pList, pNode)

#define DS_SLL_DELETE(pList, pNode) \
	      TMO_SLL_Delete (pList, pNode)
    
#define DS_SLL_DELETE_IN_MIDDLE(pList, pPrev, pNode, pNext) \
         TMO_SLL_Delete_In_Middle (pList, pPrev, pNode, pNext)
    
#define DS_SLL_COUNT(pList) \
	      TMO_SLL_Count(pList)

#define DS_SLL_NEXT(pList, pNode) \
	      TMO_SLL_Next(pList,pNode)

#define DS_SLL_FIRST(pList) \
	      TMO_SLL_First(pList)

#define DS_SLL_GET(pList) \
	      TMO_SLL_Get(pList)

#define DS_SLL_LAST(pList) \
	      TMO_SLL_Last(pList)

#define DS_SLL_IS_NODE_IN_LIST(pNode) \
	      TMO_SLL_Is_Node_In_List(pNode)

#define DS_SLL_SCAN(pList,pNode,TypeCast) \
	      TMO_SLL_Scan(pList,pNode,TypeCast)

/* Adds the port list au1List2 to au1List1 - * 
 * i.e. adds members of au1List2 to au1List1 */

#define DS_ADD_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < DS_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

#define DS_AND_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < DS_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] &= au1List2[u2ByteIndex];\
                 }\
              }

#define  DS_IS_NULL_PORT_LIST(aPortList,u2ListLen,u1Result) \
{\
    UINT2 u2LocIndx = 0;\
        u1Result = DS_TRUE;\
    for (u2LocIndx = 0; u2LocIndx < (u2ListLen); u2LocIndx++) {\
        if ((aPortList)[u2LocIndx] != 0) {\
            (u1Result) = DS_FALSE;\
            break;\
        }\
    }\
}

#define DS_IS_MFC_ID_VALID(MFCId) \
        (((MFCId <= DS_MIN_MFCLFR_ID) || (MFCId > DS_MAX_MFCLFR_ID)) ? DS_FALSE : DS_TRUE)

#define DS_IS_CLFR_ID_VALID(CLFRId) \
        (((CLFRId <= DS_MIN_CLFR_ID) || (CLFRId > DS_MAX_CLFR_ID)) ? DS_FALSE : DS_TRUE)
 
#define DS_IS_INPROACT_ID_VALID(INPROACTId) \
        (((INPROACTId <= DS_MIN_IN_PROACT_ID) || (INPROACTId > DS_MAX_IN_PROACT_ID)) \
	? DS_FALSE : DS_TRUE)

#define DS_IS_OUTPROACT_ID_VALID(OUTPROACTId) \
        (((OUTPROACTId <= DS_MIN_OUT_PROACT_ID) || (OUTPROACTId > DS_MAX_OUT_PROACT_ID)) \
        ? DS_FALSE : DS_TRUE)

#define DS_IS_METER_ID_VALID(METERId) \
        (((METERId <= DS_MIN_METER_ID) || (METERId > DS_MAX_METER_ID)) ? DS_FALSE : DS_TRUE)

#define DS_IS_SCHEDULER_ID_VALID(SCHEDULERId) \
        (((SCHEDULERId <= DS_MIN_SCHEDULER_ID) || (SCHEDULERId > DS_MAX_SCHEDULER_ID)) \
	? DS_FALSE : DS_TRUE)

#define   DS_IS_ADDR_CLASS_A(u4Addr)\
	          ((u4Addr != 0 ) && (u4Addr & 0x80000000) == 0)
#define   DS_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define   DS_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000)

#ifndef NPAPI_WANTED
/* Egress flag */
#define FS_DS_EGRESS                              0x001 
                                                  
/* Classfier updation flags */                    
#define FS_DS_UPDATE_INPROFILE                    0x01 
#define FS_DS_UPDATE_OUTPROFILE                   0x02
                                                  
/* In Profile Action allowed vlaues */            
#define FS_DS_ACTN_INSERT_PRIO                    0x000001
#define FS_DS_ACTN_SET_COS_QUEUE                  0x000002
#define FS_DS_ACTN_INSERT_TOSP                    0x000004
#define FS_DS_ACTN_COPY_TO_CPU                    0x000008
#define FS_DS_ACTN_DO_NOT_SWITCH                  0x000010
#define FS_DS_ACTN_SET_OUT_PORT_UCAST             0x000020
#define FS_DS_ACTN_COPY_TO_MIRROR                 0x000040
#define FS_DS_ACTN_INCR_FFPPKT_COUNTER            0x000080
#define FS_DS_ACTN_INSERT_PRIO_FROM_TOSP          0x000100
#define FS_DS_ACTN_INSERT_TOSP_FROM_PRIO          0x000200
#define FS_DS_ACTN_INSERT_DSCP                    0x000400
#define FS_DS_ACTN_SET_OUT_PORT_NON_UCAST         0x000800
#define FS_DS_ACTN_DO_SWITCH                      0x002000
#define FS_DS_ACTN_DROP_PRECEDENCE                0x4000
#define FS_DS_ACTN_SET_OUT_PORT_ALL               0x8000

#endif /* NPAPI_WANTED */

    
/* Macros to access Switchcore Specific data structure */

#define DS_CXE_INFO()                gDsGlobalInfo.DsCxeInfo

#define DS_FREE_FLOWGROUP_LIST       (DS_CXE_INFO ()).DsFreeFlowGroupListHead
    
#define DS_TRAFFICCLASS_MEMPOOL_ID   (DS_CXE_INFO ()).DsTCPoolId
#define DS_RED_MEMPOOL_ID            (DS_CXE_INFO ()).DsREDPoolId
#define DS_PORT_MEMPOOL_ID           (DS_CXE_INFO ()).DsPortPoolId
#define DS_FREE_FLOWGROUP_MEMPOOL_ID (DS_CXE_INFO ()).DsFreeFlowGroupPoolId

#define DS_TRAFFICCLASS_MEMBLK_SIZE   sizeof(tDiffServTCEntry)
#define DS_RED_MEMBLK_SIZE            sizeof(tDiffServREDEntry)
#define DS_PORT_MEMBLK_SIZE           sizeof(tDiffServPortEntry)
#define DS_FREE_FLOWGROUP_MEMBLK_SIZE sizeof(tDsFreeFlowGroupInfo)

#define DS_TRAFFICCLASS_MEMBLK_COUNT   DS_MAX_TC/2
#define DS_RED_MEMBLK_COUNT            DS_MAX_REDCURVES/2
#define DS_PORT_MEMBLK_COUNT           DS_MAX_PORTS
#define DS_FREE_FLOWGROUP_MEMBLK_COUNT 1024 /* If every alternate flow group
                                               is used then we need 1024 free
                                               flow group entries */

#define DS_MAX_MAX_BW           250000  /* Maximum value that can be set for 
                                           the Max. bandwidth parameter for 
                                           both port and traffic class
                                           - Corresponds to 16 Gbps */

#define DS_NUM_RESERVED_TRAFFIC_CLASSES 0  /* Traffic class 0 is reserved */
    
#define DS_TCENTRY_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_TRAFFICCLASS_MEMPOOL_ID, (UINT1 **)ppu1Block)
#define DS_REDENTRY_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_RED_MEMPOOL_ID, (UINT1 **)ppu1Block)
#define DS_PORTENTRY_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_PORT_MEMPOOL_ID, (UINT1 **)ppu1Block)
#define DS_FREEFLOWGROUP_ALLOC_MEM_BLOCK(ppu1Block)\
	        MemAllocateMemBlock(DS_FREE_FLOWGROUP_MEMPOOL_ID, (UINT1 **)ppu1Block)
    
#define DS_TCENTRY_FREE_MEM_BLOCK(pu1Block)\
	         MemReleaseMemBlock(DS_TRAFFICCLASS_MEMPOOL_ID, (UINT1 *)pu1Block)
#define DS_REDENTRY_FREE_MEM_BLOCK(pu1Block)\
	         MemReleaseMemBlock(DS_RED_MEMPOOL_ID, (UINT1 *)pu1Block)
#define DS_PORTENTRY_FREE_MEM_BLOCK(pu1Block)\
	         MemReleaseMemBlock(DS_PORT_MEMPOOL_ID, (UINT1 *)pu1Block)
#define DS_FREEFLOWGROUP_FREE_MEM_BLOCK(pu1Block)\
	         MemReleaseMemBlock(DS_FREE_FLOWGROUP_MEMPOOL_ID, (UINT1 *)pu1Block)

#define DS_DSCP_TO_QMAP(u4DSCP) \
        (DS_CXE_INFO()).au1DscpToQMap[u4DSCP]
        
#define DS_TC_ENTRY(u4TcId) \
        gDsGlobalInfo.DsCxeInfo.apDsTCEntry[(u4TcId)-1]
#define DS_RED_ENTRY(u4REDCurveId) \
        gDsGlobalInfo.DsCxeInfo.apDsREDEntry[(u4REDCurveId)-1]
#define DS_PORT_ENTRY(u4PortNum) \
        gDsGlobalInfo.DsCxeInfo.apDsPortEntry[(u4PortNum)-1]

#define DS_INIT_PORT_DIRTY_FLAG() \
        MEMSET (gDsGlobalInfo.DsCxeInfo.au1PortParamsModified, 0, \
                              DS_PORT_LIST_SIZE)

#define DS_SET_PORT_DIRTY(u4PortNum) \
        OSIX_BITLIST_SET_BIT (gDsGlobalInfo.DsCxeInfo.au1PortParamsModified, \
                              u4PortNum, DS_PORT_LIST_SIZE)
        
#define DS_IS_PORT_DIRTY(u4PortNum, bResult) \
        OSIX_BITLIST_IS_BIT_SET (gDsGlobalInfo.DsCxeInfo.au1PortParamsModified,\
                                 u4PortNum,\
                                 DS_PORT_LIST_SIZE, bResult)
#endif /* _DSCXEMACRO_H */
