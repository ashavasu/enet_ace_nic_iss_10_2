#ifndef _DSTRACE_H_
#define _DSTRACE_H_

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dstrc.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DIFFSERV                                       */
/*    MODULE NAME           : All Modules in Diffserv                        */
/*    DSNGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Manish K S                                     */
/*    DESCRIPTION           : This file contains all trace related defines   */
/*                            used in  DiffServ module.                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/
/* Trace and debug flags */
#define   DS_TRC_FDSG            gDsGlobalInfo.u4DsTrcFlag

/* Module names */
#define   DS_MOD_NAME            ((const char *)"DS")

/* Trace definitions */
#ifdef TRACE_WANTED

#define DS_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(DS_TRC_FDSG, TraceType, DS_MOD_NAME, pBuf, Length, (const char *)Str)

#define DS_TRC(TraceType, Str)                                              \
        MOD_TRC(DS_TRC_FDSG, TraceType, DS_MOD_NAME, (const char *)Str)

#define DS_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(DS_TRC_FDSG, TraceType, DS_MOD_NAME, (const char *)Str, Arg1)

#define DS_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(DS_TRC_FDSG, TraceType, DS_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define DS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(DS_TRC_FDSG, TraceType, DS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define DS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)                 \
        MOD_TRC_ARG4(DS_TRC_FDSG, TraceType, DS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#define DS_TRC_ARG10(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7,\
                                                           Arg8, Arg9, Arg10) \
        UtlTrcLog(DS_TRC_FDSG, TraceType, DS_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3, \
                  Arg4, Arg5, Arg6, Arg7, Arg8, Arg9, Arg10)

#else  /* TRACE_WANTED */

#define DS_PKT_DUMP(TraceType, pBuf, Length, Str)
#define DS_TRC(TraceType, Str)
#define DS_TRC_ARG1(TraceType, Str, Arg1)
#define DS_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define DS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)

#endif /* TRACE_WANTED */

#endif/* _DSTRACE_H_ */
