#ifndef _DSCXEPROTO_H
#define _DSCXEPROTO_H


/* dscxesys.c */
INT4 DsValInProActInsertPrio (tDiffServInProfileActionEntry *);
INT4 DsValInProActSetCOSQueue (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertTOS (tDiffServInProfileActionEntry *);
INT4 DsValInProActCopyToCPU (tDiffServInProfileActionEntry *);
INT4 DsValInProActDoNotSwitch (tDiffServInProfileActionEntry *);
INT4 DsValInProActSetOutPortUCast (tDiffServInProfileActionEntry *);
INT4 DsValInProActCopyToMirror (tDiffServInProfileActionEntry *);
INT4 DsValInProActIncrFFPPktCounter (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertPrioFromTOS (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertTOSFromPrio (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertDSCP (tDiffServInProfileActionEntry *);
INT4 DsValInProActSetOutPortNonUCast (tDiffServInProfileActionEntry *);
INT4 DsValInProActDoSwitch (tDiffServInProfileActionEntry *);
#ifdef _DSCXESYS_C
tDsInProActValEntry DsValiateInProActFunctPtrArr [DS_INPROACT_FLAGS] = {

                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertPrio},
		   {(DS_INPROACTVAL_FUNCT) DsValInProActSetCOSQueue},
		   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertTOS},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActCopyToCPU},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActDoNotSwitch},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActSetOutPortUCast},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActCopyToMirror},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActIncrFFPPktCounter},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertPrioFromTOS},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertTOSFromPrio},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertDSCP},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActSetOutPortNonUCast},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActDoSwitch}
};
#else

extern tDsInProActValEntry DsValiateInProActFunctPtrArr [DS_INPROACT_FLAGS];

#endif  /* _DSCXESYS_C */   

INT4 DsEnable(VOID); 
INT4 DsDisable(VOID);

INT4 DsShutDown(VOID);

VOID DsDeleteMemPools (VOID);

INT4 DsValidateMFClfrId (INT4 );
INT4 DsValidateClfrId (INT4 );
INT4 DsValidateInProfileActionId (INT4 );
INT4 DsValidateOutProfileActionId (INT4 );
INT4 DsValidateMeterId (INT4 );
INT4 DsValidateSchedulerId (INT4 );

INT4 DsSnmpLowGetFirstValidMultiFieldClfrId (INT4 *);
INT4 DsSnmpLowGetNextValidMultiFieldClfrId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidClfrId (INT4 *);
INT4 DsSnmpLowGetNextValidClfrId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidInProfileActionId (INT4 *);
INT4 DsSnmpLowGetNextValidInProfileActionId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidOutProfileActionId (INT4 *);
INT4 DsSnmpLowGetNextValidOutProfileActionId (INT4, INT4 *);
INT4 DsValidateIfIndex (INT4 i4PortNum);

tDiffServMultiFieldClfrEntry* DsGetMFClfrEntry (INT4);
tDiffServClfrEntry* DsGetClfrEntry (INT4);
tDiffServInProfileActionEntry* DsGetInProfileActionEntry (INT4);
INT4 DsQualifyMFClfrData (tDiffServMultiFieldClfrEntry *);
INT4 DsQualifyClfrData (tDiffServClfrEntry *);
INT4 DsQualifyInProfileActionData (tDiffServInProfileActionEntry *);
INT4 DsCheckMFClfrDependency (INT4);
INT4 DsCheckInProfileActionDependency (INT4);
INT4 DsCheckOutProfileActionDependency (INT4);

INT4 DsStart (VOID);

INT4
MsrValidateDiffServInProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDiffServOutProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4 DsWebnmGetPolicyMapEntry PROTO ((INT4 i4PolicyMapId,
                                      tDiffServWebClfrData *pClfrData));
INT4 DsWebnmSetPolicyMapEntry PROTO ((tDiffServWebSetClfrEntry *pClfrEntry,
                                      UINT1 *pu1ErrString));

tDiffServClfrEntry* DsCheckClfrForTrafficClassId (INT4 i4DsOutProfileActionID);
INT4 DsWfhbdInfoInit (VOID);
VOID DsHandleWfhbdInfoInitFailure (VOID);
INT4 DsWfhbdInfoDeInit (VOID);
INT4 DsValidateTCId (INT4 i4FsCxeDsTrafficClassId);
INT4 DsValidateREDCurveId (INT4 i4FsCxeDsREDCurveId);
INT4 DsCheckTCForREDCurveId (INT4 i4REDCurveId);
#ifdef NPAPI_WANTED
INT4 DsConfigWfhbdInfoInHw (VOID);
INT4 DsDisableWfhbdInfoInHw (VOID);
#endif

#endif /* _DSCXEPROTO_H_ */
