#ifndef _DSCXETDFS_H
#define _DSCXETDFS_H

/*****************************************************************************/
/* Enumerated Constants                                                      */
/*****************************************************************************/

/* The data-structures - tVlanId and tMacAddress 
 * are to be taken from earlier decalrations */


typedef tTMO_SLL            tDsSll;

typedef enum
{
	DS_ENABLE = 1,
   DS_DISABLE
}tDsStatus;

typedef enum
{
	DS_START = 1,
   DS_SHUTDOWN
}tDsSystemControl;

typedef struct DsFreeFlowGroupInfo
{
    tDsSllNode     DsNextNode;
    UINT4          u4FirstFlowGroup;
    UINT4          u4NumFreeFlowGroups;

} tDsFreeFlowGroupInfo;

/* CXE Specific info */
typedef struct _DsCxeInfo
{
    UINT4            u4IPTOSQPrecdncSwitched;
    UINT4            u4IPTOSQPrecdncRouted;
    UINT4            u4Dot1PQPrecdncSwitched;
    UINT4            u4Dot1PQPrecdncRouted;
    UINT1            au1DscpToQMap[64];
    UINT4            u4NumFreeFlowGroups;

    tDiffServTCEntry *apDsTCEntry[DS_MAX_TC]; /* Array of pointers to
                                                   Traffic class 
                                                   entries */ 
    tDiffServREDEntry *apDsREDEntry[DS_MAX_REDCURVES]; /* Array of pointers 
                                                            to RED curve 
                                                            entries */ 
    tDiffServPortEntry *apDsPortEntry[DS_MAX_PORTS];
                                                /* Array of pointers to Port
                                                   entries */

    tDsSll            DsFreeFlowGroupListHead;
    tDsMemPoolId      DsFreeFlowGroupPoolId;
    tDsMemPoolId      DsTCPoolId;
    tDsMemPoolId      DsREDPoolId;
    tDsMemPoolId      DsPortPoolId;
    UINT1             au1PortParamsModified[DS_PORT_LIST_SIZE];
}tDsCxeInfo;


/* Data Structure to keep all the Pool Ids and List Heads */
typedef struct DsGlobalInfo
{
    tDsMemPoolId         DsMfClfrPoolId;
    tDsMemPoolId         DsClfrPoolId;
    tDsMemPoolId         DsInProfileActionPoolId;
    tDsMemPoolId         DsOutProfileActionPoolId;
    tDsSll               DsMfClfrListHead;
    tDsSll               DsClfrListHead;
    tDsSll               DsInProfileActionListHead;
    tDsSll               DsOutProfileActionListHead;
    UINT4                u4DsTrcFlag;
    tDsSystemControl     DsSystemControl;
    tDsStatus            DsStatus;
    tDsCxeInfo           DsCxeInfo;

}tDsGlobalInfo;


typedef INT4 (* DS_MFCLFRVAL_FUNCT)(tDiffServMultiFieldClfrEntry *);

typedef struct DsMFClfrValEntry {

   DS_MFCLFRVAL_FUNCT  pValFunct;

} tDsMFClfrValEntry;


typedef INT4 (* DS_INPROACTVAL_FUNCT)(tDiffServInProfileActionEntry *);

typedef struct DsInProActValEntry {

   DS_INPROACTVAL_FUNCT  pValFunct;

} tDsInProActValEntry;

#endif /* _DSCXETDFS_H */

