/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissddb.h,v 1.3 2007/02/01 15:03:02 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSDDB_H
#define _FSISSDDB_H

UINT1 FsDiffServMultiFieldClfrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServClfrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServInProfileActionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDsDSCPRemapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDsTrafficClassConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDsPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDsREDConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsissd [] ={1,3,6,1,4,1,2076,83,1,1};
tSNMP_OID_TYPE fsissdOID = {10, fsissd};


UINT4 FsDsSystemControl [ ] ={1,3,6,1,4,1,2076,83,1,1,1};
UINT4 FsDsStatus [ ] ={1,3,6,1,4,1,2076,83,1,1,2};
UINT4 FsDsIPTOSQPrecedenceSwitched [ ] ={1,3,6,1,4,1,2076,83,1,1,3};
UINT4 FsDsIPTOSQPrecedenceRouted [ ] ={1,3,6,1,4,1,2076,83,1,1,4};
UINT4 FsDsDot1PQPrecedenceSwitched [ ] ={1,3,6,1,4,1,2076,83,1,1,5};
UINT4 FsDsDot1PQPrecedenceRouted [ ] ={1,3,6,1,4,1,2076,83,1,1,6};
UINT4 FsDsNumWFHBDOfferedPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,7};
UINT4 FsDsNumWFHBDAcceptedPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,8};
UINT4 FsDsNumWFHBDRejectedPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,9};
UINT4 FsDsNumWFHBDOfferedBytes [ ] ={1,3,6,1,4,1,2076,83,1,1,10};
UINT4 FsDsNumWFHBDAcceptedBytes [ ] ={1,3,6,1,4,1,2076,83,1,1,11};
UINT4 FsDsNumWFHBDRejectedBytes [ ] ={1,3,6,1,4,1,2076,83,1,1,12};
UINT4 FsDsNumWrongPortAcceptedPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,13};
UINT4 FsDsNumWrongPortRejectedPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,14};
UINT4 FsDsNumAcceptFairGroupPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,15};
UINT4 FsDsNumDiscardUnfairGroupPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,16};
UINT4 FsDsNumDiscardDropLevel2Pkts [ ] ={1,3,6,1,4,1,2076,83,1,1,17};
UINT4 FsDsNumDiscardDropLevel3Pkts [ ] ={1,3,6,1,4,1,2076,83,1,1,18};
UINT4 FsDsNumAcceptFairTCPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,19};
UINT4 FsDsNumAcceptDontCarePkts [ ] ={1,3,6,1,4,1,2076,83,1,1,20};
UINT4 FsDsNumDiscardPortREDPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,21};
UINT4 FsDsNumDiscardTCREDPkts [ ] ={1,3,6,1,4,1,2076,83,1,1,22};
UINT4 FsDiffServMultiFieldClfrId [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,1};
UINT4 FsDiffServMultiFieldClfrFilterId [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,2};
UINT4 FsDiffServMultiFieldClfrFilterType [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,3};
UINT4 FsDiffServMultiFieldClfrStatus [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,4};
UINT4 FsDiffServClfrId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,1};
UINT4 FsDiffServClfrMFClfrId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,2};
UINT4 FsDiffServClfrInProActionId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,3};
UINT4 FsDiffServClfrTrafficClassId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,4};
UINT4 FsDiffServClfrStatus [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,5};
UINT4 FsDiffServInProfileActionId [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,1};
UINT4 FsDiffServInProfileActionFlag [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,2};
UINT4 FsDiffServInProfileActionNewPrio [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,3};
UINT4 FsDiffServInProfileActionIpTOS [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,4};
UINT4 FsDiffServInProfileActionPort [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,5};
UINT4 FsDiffServInProfileActionDscp [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,6};
UINT4 FsDiffServInProfileActionStatus [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,7};
UINT4 FsDsDSCPValue [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,1};
UINT4 FsDsQPriority [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,2};
UINT4 FsDsTrafficClassId [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,1};
UINT4 FsDsTCPortNum [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,2};
UINT4 FsDsTCMaxBandwidth [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,3};
UINT4 FsDsTCGuaranteedBandwidth [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,4};
UINT4 FsDsTCDropLevel1 [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,5};
UINT4 FsDsTCDropLevel2 [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,6};
UINT4 FsDsTCDropLevel3 [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,7};
UINT4 FsDsTCDropAtMaxBandwidth [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,8};
UINT4 FsDsTCWeightFactor [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,9};
UINT4 FsDsTCWeightShift [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,10};
UINT4 FsDsTCFirstFlowGroup [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,12};
UINT4 FsDsTCNumFlowGroups [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,13};
UINT4 FsDsTCREDCurveId [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,14};
UINT4 FsDsTCAccBytesCount [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,15};
UINT4 FsDsTCDiscBytesCount [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,16};
UINT4 FsDsTCEntryStatus [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,17};
UINT4 FsDsPortNum [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,1};
UINT4 FsDsPortMaxBandwidth [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,2};
UINT4 FsDsPortDropLevel1 [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,3};
UINT4 FsDsPortDropLevel2 [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,4};
UINT4 FsDsPortDropLevel3 [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,5};
UINT4 FsDsPortDropAtMaxBandwidth [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,6};
UINT4 FsDsPortREDCurveId [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,7};
UINT4 FsDsREDCurveId [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,1};
UINT4 FsDsREDCurveStart [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,2};
UINT4 FsDsREDCurveStop [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,3};
UINT4 FsDsREDCurveQRange [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,4};
UINT4 FsDsREDCurveStopProbability [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,5};
UINT4 FsDsREDCurveStatus [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,6};


tMbDbEntry fsissdMibEntry[]= {

{{11,FsDsSystemControl}, NULL, FsDsSystemControlGet, FsDsSystemControlSet, FsDsSystemControlTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{11,FsDsStatus}, NULL, FsDsStatusGet, FsDsStatusSet, FsDsStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{11,FsDsIPTOSQPrecedenceSwitched}, NULL, FsDsIPTOSQPrecedenceSwitchedGet, FsDsIPTOSQPrecedenceSwitchedSet, FsDsIPTOSQPrecedenceSwitchedTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0},

{{11,FsDsIPTOSQPrecedenceRouted}, NULL, FsDsIPTOSQPrecedenceRoutedGet, FsDsIPTOSQPrecedenceRoutedSet, FsDsIPTOSQPrecedenceRoutedTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0},

{{11,FsDsDot1PQPrecedenceSwitched}, NULL, FsDsDot1PQPrecedenceSwitchedGet, FsDsDot1PQPrecedenceSwitchedSet, FsDsDot1PQPrecedenceSwitchedTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0},

{{11,FsDsDot1PQPrecedenceRouted}, NULL, FsDsDot1PQPrecedenceRoutedGet, FsDsDot1PQPrecedenceRoutedSet, FsDsDot1PQPrecedenceRoutedTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0},

{{11,FsDsNumWFHBDOfferedPkts}, NULL, FsDsNumWFHBDOfferedPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWFHBDAcceptedPkts}, NULL, FsDsNumWFHBDAcceptedPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWFHBDRejectedPkts}, NULL, FsDsNumWFHBDRejectedPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWFHBDOfferedBytes}, NULL, FsDsNumWFHBDOfferedBytesGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWFHBDAcceptedBytes}, NULL, FsDsNumWFHBDAcceptedBytesGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWFHBDRejectedBytes}, NULL, FsDsNumWFHBDRejectedBytesGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWrongPortAcceptedPkts}, NULL, FsDsNumWrongPortAcceptedPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumWrongPortRejectedPkts}, NULL, FsDsNumWrongPortRejectedPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumAcceptFairGroupPkts}, NULL, FsDsNumAcceptFairGroupPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumDiscardUnfairGroupPkts}, NULL, FsDsNumDiscardUnfairGroupPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumDiscardDropLevel2Pkts}, NULL, FsDsNumDiscardDropLevel2PktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumDiscardDropLevel3Pkts}, NULL, FsDsNumDiscardDropLevel3PktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumAcceptFairTCPkts}, NULL, FsDsNumAcceptFairTCPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumAcceptDontCarePkts}, NULL, FsDsNumAcceptDontCarePktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumDiscardPortREDPkts}, NULL, FsDsNumDiscardPortREDPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{11,FsDsNumDiscardTCREDPkts}, NULL, FsDsNumDiscardTCREDPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{13,FsDiffServMultiFieldClfrId}, GetNextIndexFsDiffServMultiFieldClfrTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServMultiFieldClfrTableINDEX, 1},

{{13,FsDiffServMultiFieldClfrFilterId}, GetNextIndexFsDiffServMultiFieldClfrTable, FsDiffServMultiFieldClfrFilterIdGet, FsDiffServMultiFieldClfrFilterIdSet, FsDiffServMultiFieldClfrFilterIdTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServMultiFieldClfrTableINDEX, 1},

{{13,FsDiffServMultiFieldClfrFilterType}, GetNextIndexFsDiffServMultiFieldClfrTable, FsDiffServMultiFieldClfrFilterTypeGet, FsDiffServMultiFieldClfrFilterTypeSet, FsDiffServMultiFieldClfrFilterTypeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServMultiFieldClfrTableINDEX, 1},

{{13,FsDiffServMultiFieldClfrStatus}, GetNextIndexFsDiffServMultiFieldClfrTable, FsDiffServMultiFieldClfrStatusGet, FsDiffServMultiFieldClfrStatusSet, FsDiffServMultiFieldClfrStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServMultiFieldClfrTableINDEX, 1},

{{13,FsDiffServClfrId}, GetNextIndexFsDiffServClfrTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServClfrTableINDEX, 1},

{{13,FsDiffServClfrMFClfrId}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrMFClfrIdGet, FsDiffServClfrMFClfrIdSet, FsDiffServClfrMFClfrIdTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1},

{{13,FsDiffServClfrInProActionId}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrInProActionIdGet, FsDiffServClfrInProActionIdSet, FsDiffServClfrInProActionIdTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1},

{{13,FsDiffServClfrTrafficClassId}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrTrafficClassIdGet, FsDiffServClfrTrafficClassIdSet, FsDiffServClfrTrafficClassIdTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1},

{{13,FsDiffServClfrStatus}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrStatusGet, FsDiffServClfrStatusSet, FsDiffServClfrStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1},

{{13,FsDiffServInProfileActionId}, GetNextIndexFsDiffServInProfileActionTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDiffServInProfileActionFlag}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionFlagGet, FsDiffServInProfileActionFlagSet, FsDiffServInProfileActionFlagTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDiffServInProfileActionNewPrio}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionNewPrioGet, FsDiffServInProfileActionNewPrioSet, FsDiffServInProfileActionNewPrioTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDiffServInProfileActionIpTOS}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionIpTOSGet, FsDiffServInProfileActionIpTOSSet, FsDiffServInProfileActionIpTOSTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDiffServInProfileActionPort}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionPortGet, FsDiffServInProfileActionPortSet, FsDiffServInProfileActionPortTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDiffServInProfileActionDscp}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionDscpGet, FsDiffServInProfileActionDscpSet, FsDiffServInProfileActionDscpTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDiffServInProfileActionStatus}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionStatusGet, FsDiffServInProfileActionStatusSet, FsDiffServInProfileActionStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1},

{{13,FsDsDSCPValue}, GetNextIndexFsDsDSCPRemapTable, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDsDSCPRemapTableINDEX, 1},

{{13,FsDsQPriority}, GetNextIndexFsDsDSCPRemapTable, FsDsQPriorityGet, FsDsQPrioritySet, FsDsQPriorityTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsDSCPRemapTableINDEX, 1},

{{13,FsDsTrafficClassId}, GetNextIndexFsDsTrafficClassConfigTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCPortNum}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCPortNumGet, FsDsTCPortNumSet, FsDsTCPortNumTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCMaxBandwidth}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCMaxBandwidthGet, FsDsTCMaxBandwidthSet, FsDsTCMaxBandwidthTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCGuaranteedBandwidth}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCGuaranteedBandwidthGet, FsDsTCGuaranteedBandwidthSet, FsDsTCGuaranteedBandwidthTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCDropLevel1}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCDropLevel1Get, FsDsTCDropLevel1Set, FsDsTCDropLevel1Test, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCDropLevel2}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCDropLevel2Get, FsDsTCDropLevel2Set, FsDsTCDropLevel2Test, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCDropLevel3}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCDropLevel3Get, FsDsTCDropLevel3Set, FsDsTCDropLevel3Test, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCDropAtMaxBandwidth}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCDropAtMaxBandwidthGet, FsDsTCDropAtMaxBandwidthSet, FsDsTCDropAtMaxBandwidthTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCWeightFactor}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCWeightFactorGet, FsDsTCWeightFactorSet, FsDsTCWeightFactorTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCWeightShift}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCWeightShiftGet, FsDsTCWeightShiftSet, FsDsTCWeightShiftTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCFirstFlowGroup}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCFirstFlowGroupGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCNumFlowGroups}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCNumFlowGroupsGet, FsDsTCNumFlowGroupsSet, FsDsTCNumFlowGroupsTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCREDCurveId}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCREDCurveIdGet, FsDsTCREDCurveIdSet, FsDsTCREDCurveIdTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCAccBytesCount}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCAccBytesCountGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCDiscBytesCount}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCDiscBytesCountGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsTCEntryStatus}, GetNextIndexFsDsTrafficClassConfigTable, FsDsTCEntryStatusGet, FsDsTCEntryStatusSet, FsDsTCEntryStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDsTrafficClassConfigTableINDEX, 1},

{{13,FsDsPortNum}, GetNextIndexFsDsPortConfigTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDsPortConfigTableINDEX, 1},

{{13,FsDsPortMaxBandwidth}, GetNextIndexFsDsPortConfigTable, FsDsPortMaxBandwidthGet, FsDsPortMaxBandwidthSet, FsDsPortMaxBandwidthTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsPortConfigTableINDEX, 1},

{{13,FsDsPortDropLevel1}, GetNextIndexFsDsPortConfigTable, FsDsPortDropLevel1Get, FsDsPortDropLevel1Set, FsDsPortDropLevel1Test, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsPortConfigTableINDEX, 1},

{{13,FsDsPortDropLevel2}, GetNextIndexFsDsPortConfigTable, FsDsPortDropLevel2Get, FsDsPortDropLevel2Set, FsDsPortDropLevel2Test, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsPortConfigTableINDEX, 1},

{{13,FsDsPortDropLevel3}, GetNextIndexFsDsPortConfigTable, FsDsPortDropLevel3Get, FsDsPortDropLevel3Set, FsDsPortDropLevel3Test, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsPortConfigTableINDEX, 1},

{{13,FsDsPortDropAtMaxBandwidth}, GetNextIndexFsDsPortConfigTable, FsDsPortDropAtMaxBandwidthGet, FsDsPortDropAtMaxBandwidthSet, FsDsPortDropAtMaxBandwidthTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDsPortConfigTableINDEX, 1},

{{13,FsDsPortREDCurveId}, GetNextIndexFsDsPortConfigTable, FsDsPortREDCurveIdGet, FsDsPortREDCurveIdSet, FsDsPortREDCurveIdTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsPortConfigTableINDEX, 1},

{{13,FsDsREDCurveId}, GetNextIndexFsDsREDConfigTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDsREDConfigTableINDEX, 1},

{{13,FsDsREDCurveStart}, GetNextIndexFsDsREDConfigTable, FsDsREDCurveStartGet, FsDsREDCurveStartSet, FsDsREDCurveStartTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsREDConfigTableINDEX, 1},

{{13,FsDsREDCurveStop}, GetNextIndexFsDsREDConfigTable, FsDsREDCurveStopGet, FsDsREDCurveStopSet, FsDsREDCurveStopTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsREDConfigTableINDEX, 1},

{{13,FsDsREDCurveQRange}, GetNextIndexFsDsREDConfigTable, FsDsREDCurveQRangeGet, FsDsREDCurveQRangeSet, FsDsREDCurveQRangeTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsREDConfigTableINDEX, 1},

{{13,FsDsREDCurveStopProbability}, GetNextIndexFsDsREDConfigTable, FsDsREDCurveStopProbabilityGet, FsDsREDCurveStopProbabilitySet, FsDsREDCurveStopProbabilityTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDsREDConfigTableINDEX, 1},

{{13,FsDsREDCurveStatus}, GetNextIndexFsDsREDConfigTable, FsDsREDCurveStatusGet, FsDsREDCurveStatusSet, FsDsREDCurveStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDsREDConfigTableINDEX, 1},
};
tMibData fsissdEntry = { 69, fsissdMibEntry };
#endif /* _FSISSDDB_H */

