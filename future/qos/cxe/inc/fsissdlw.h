/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissdlw.h,v 1.3 2007/02/01 15:03:02 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsDsStatus ARG_LIST((INT4 *));

INT1
nmhGetFsDsIPTOSQPrecedenceSwitched ARG_LIST((INT4 *));

INT1
nmhGetFsDsIPTOSQPrecedenceRouted ARG_LIST((INT4 *));

INT1
nmhGetFsDsDot1PQPrecedenceSwitched ARG_LIST((INT4 *));

INT1
nmhGetFsDsDot1PQPrecedenceRouted ARG_LIST((INT4 *));

INT1
nmhGetFsDsNumWFHBDOfferedPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWFHBDAcceptedPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWFHBDRejectedPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWFHBDOfferedBytes ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWFHBDAcceptedBytes ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWFHBDRejectedBytes ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWrongPortAcceptedPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumWrongPortRejectedPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumAcceptFairGroupPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumDiscardUnfairGroupPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumDiscardDropLevel2Pkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumDiscardDropLevel3Pkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumAcceptFairTCPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumAcceptDontCarePkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumDiscardPortREDPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsDsNumDiscardTCREDPkts ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsDsStatus ARG_LIST((INT4 ));

INT1
nmhSetFsDsIPTOSQPrecedenceSwitched ARG_LIST((INT4 ));

INT1
nmhSetFsDsIPTOSQPrecedenceRouted ARG_LIST((INT4 ));

INT1
nmhSetFsDsDot1PQPrecedenceSwitched ARG_LIST((INT4 ));

INT1
nmhSetFsDsDot1PQPrecedenceRouted ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDsStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDsIPTOSQPrecedenceSwitched ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDsIPTOSQPrecedenceRouted ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDsDot1PQPrecedenceSwitched ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDsDot1PQPrecedenceRouted ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for FsDiffServMultiFieldClfrTable. */
INT1
nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServMultiFieldClfrTable  */

INT1
nmhGetFirstIndexFsDiffServMultiFieldClfrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServMultiFieldClfrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServMultiFieldClfrFilterId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServMultiFieldClfrFilterType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServMultiFieldClfrStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServMultiFieldClfrFilterId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServMultiFieldClfrFilterType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServMultiFieldClfrStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServMultiFieldClfrFilterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServMultiFieldClfrFilterType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServMultiFieldClfrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDiffServClfrTable. */
INT1
nmhValidateIndexInstanceFsDiffServClfrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServClfrTable  */

INT1
nmhGetFirstIndexFsDiffServClfrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServClfrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServClfrMFClfrId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServClfrInProActionId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServClfrTrafficClassId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServClfrStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServClfrMFClfrId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServClfrInProActionId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServClfrTrafficClassId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServClfrStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServClfrMFClfrId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServClfrInProActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServClfrTrafficClassId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServClfrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDiffServInProfileActionTable. */
INT1
nmhValidateIndexInstanceFsDiffServInProfileActionTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServInProfileActionTable  */

INT1
nmhGetFirstIndexFsDiffServInProfileActionTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServInProfileActionTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServInProfileActionFlag ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionNewPrio ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionIpTOS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionDscp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServInProfileActionFlag ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionNewPrio ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionIpTOS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionDscp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServInProfileActionFlag ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionNewPrio ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionIpTOS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDsDSCPRemapTable. */
INT1
nmhValidateIndexInstanceFsDsDSCPRemapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDsDSCPRemapTable  */

INT1
nmhGetFirstIndexFsDsDSCPRemapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDsDSCPRemapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsQPriority ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsQPriority ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsQPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsDsTrafficClassConfigTable. */
INT1
nmhValidateIndexInstanceFsDsTrafficClassConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDsTrafficClassConfigTable  */

INT1
nmhGetFirstIndexFsDsTrafficClassConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDsTrafficClassConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsTCPortNum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCMaxBandwidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCGuaranteedBandwidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCDropLevel1 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCDropLevel2 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCDropLevel3 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCDropAtMaxBandwidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCWeightFactor ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCWeightShift ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCFirstFlowGroup ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCNumFlowGroups ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCREDCurveId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsTCAccBytesCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsDsTCDiscBytesCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsDsTCEntryStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsTCPortNum ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCMaxBandwidth ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCGuaranteedBandwidth ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCDropLevel1 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCDropLevel2 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCDropLevel3 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCDropAtMaxBandwidth ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCWeightFactor ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCWeightShift ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCNumFlowGroups ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCREDCurveId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsTCEntryStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsTCPortNum ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCMaxBandwidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCGuaranteedBandwidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCDropLevel1 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCDropLevel2 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCDropLevel3 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCDropAtMaxBandwidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCWeightFactor ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCWeightShift ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCNumFlowGroups ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCREDCurveId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsTCEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDsPortConfigTable. */
INT1
nmhValidateIndexInstanceFsDsPortConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDsPortConfigTable  */

INT1
nmhGetFirstIndexFsDsPortConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDsPortConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsPortMaxBandwidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsPortDropLevel1 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsPortDropLevel2 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsPortDropLevel3 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsPortDropAtMaxBandwidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsPortREDCurveId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsPortMaxBandwidth ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsPortDropLevel1 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsPortDropLevel2 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsPortDropLevel3 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsPortDropAtMaxBandwidth ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsPortREDCurveId ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsPortMaxBandwidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsPortDropLevel1 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsPortDropLevel2 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsPortDropLevel3 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsPortDropAtMaxBandwidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsPortREDCurveId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDsREDConfigTable. */
INT1
nmhValidateIndexInstanceFsDsREDConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDsREDConfigTable  */

INT1
nmhGetFirstIndexFsDsREDConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDsREDConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsREDCurveStart ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsREDCurveStop ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsREDCurveQRange ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsREDCurveStopProbability ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDsREDCurveStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsREDCurveStart ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsREDCurveStop ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsREDCurveQRange ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsREDCurveStopProbability ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDsREDCurveStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsREDCurveStart ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsREDCurveStop ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsREDCurveQRange ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsREDCurveStopProbability ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDsREDCurveStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
