#ifndef _DSCXEGLOB_H
#define _DSCXEGLOB_H


#ifdef _DSCXESYS_C

tDsGlobalInfo gDsGlobalInfo;

UINT4         gu4PortMask[DS_MAX_PORT_MASK + 1] = {

                     0x0000, 0x8000, 0xC000, 0xE000, 0xF000,
		     0xF800, 0xFC00, 0xFE00, 0xFF00,
		     0xFF80, 0xFFC0, 0xFFE0, 0xFFF0,
		     0xFFF8, 0xFFFC, 0xFFFE, 0xFFFF
};

#else

extern tDsGlobalInfo gDsGlobalInfo;
extern UINT4  gu4PortMask[DS_MAX_PORT_MASK + 1];

#endif /* _DSCXESYS_C */

#endif /* _DSCXEGLOB_H */

