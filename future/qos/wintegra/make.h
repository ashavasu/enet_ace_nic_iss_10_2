#!/bin/csh
/*$Id: make.h,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/

# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30/04/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | DIFFSERV   | Creation of makefile                              |
# |         | 10/05/2002 |                                                   |
# +--------------------------------------------------------------------------+

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME		= FutureQOS
PROJECT_BASE_DIR	= ${BASE_DIR}/qos/others
PROJECT_SOURCE_DIR	= ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR	= ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR	= ${PROJECT_BASE_DIR}/obj
FUTURE_INC_DIR		= $(BASE_DIR)/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/dsglob.h \
			 $(PROJECT_INCLUDE_DIR)/dsmacro.h \
			 $(PROJECT_INCLUDE_DIR)/dstdfs.h \
			 $(PROJECT_INCLUDE_DIR)/fsdscon.h \
			 $(PROJECT_INCLUDE_DIR)/fsdsmdb.h \
			 $(PROJECT_INCLUDE_DIR)/fsdsogi.h \
			 $(PROJECT_INCLUDE_DIR)/dsextn.h \
			 $(PROJECT_INCLUDE_DIR)/fsdsmid.h \
			 $(PROJECT_INCLUDE_DIR)/dsextn.h \
			 $(PROJECT_INCLUDE_DIR)/dstrc.h \
			 $(PROJECT_INCLUDE_DIR)/dsproto.h \
			 $(PROJECT_INCLUDE_DIR)/dsinc.h


PROJECT_FINAL_INCLUDES_DIRS	=  -I$(PROJECT_INCLUDE_DIR) \
				 $(COMMON_INCLUDE_DIRS)\
				-I$(FUTURE_INC_DIR)

PROJECT_FINAL_INCLUDE_FILES    +=  $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES	=    $(COMMON_DEPENDENCIES)\
            $(PROJECT_FINAL_INCLUDE_FILES) \
				$(PROJECT_BASE_DIR)/Makefile \
				$(PROJECT_BASE_DIR)/make.h
