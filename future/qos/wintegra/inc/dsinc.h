
/*$Id: dsinc.h,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/
#ifndef _DSINC_H
#define _DSINC_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dsinc.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ module                                */
/*    MODULE NAME           : DiffServ module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Manish K S                                     */
/*    DESCRIPTION           : This file contains global variables used       */
/*                            in DiffServ Module.                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/

/* Other includes */
#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "bridge.h"
#include "msr.h"


#ifdef MBSM_WANTED
#include "mbsm.h"
#endif


/* Snmp related includes */
#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"

#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "diffsrv.h"
#include "la.h"
#include "l2iwf.h"


/* DiffServ Includes */
#include "dsmacro.h"
#include "dstdfs.h"
#include "dsproto.h"
#include "dsglob.h"
#include "dsextn.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "difsrvnp.h"
#endif

#include "dstrc.h"
#include "fsdslow.h"
#include "fsdsmid.h"
#include "iss.h"

#endif /* _DSINC_H */
