/*$Id: dstdfs.h,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/

#ifndef _DSTDFS_H
#define _DSTDFS_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dstdfs.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ                                       */
/*    MODULE NAME           : All modules in Diffserv        .               */
/*    DSNGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Manish K S                                     */
/*    DESCRIPTION           : This file contains all typedefs and enums used */
/*                            DiffServ module.                               */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Enumerated Constants                                                      */
/*****************************************************************************/

/* The data-structures - tVlanId and tMacAddress 
 * are to be taken from earlier decalrations */


typedef tTMO_SLL            tDsSll;

typedef enum
{
	DS_ENABLE = 1,
   DS_DISABLE
}tDsStatus;

typedef enum
{
	DS_START = 1,
   DS_SHUTDOWN
}tDsSystemControl;


/* Data Structure to keep all the Pool Ids and List Heads */
typedef struct DsGlobalInfo
{
    tDsMemPoolId         DsMfClfrPoolId;
    tDsMemPoolId         DsClfrPoolId;
    tDsMemPoolId         DsInProfileActionPoolId;
    tDsMemPoolId         DsOutProfileActionPoolId;
    tDsMemPoolId         DsMeterPoolId;
    tDsMemPoolId         DsSchedulerPoolId;
    tDsSll               DsMfClfrListHead;
    tDsSll               DsClfrListHead;
    tDsSll               DsInProfileActionListHead;
    tDsSll               DsOutProfileActionListHead;
    tDsSll               DsMeterListHead;
    tDsSll               DsSchedulerListHead;
    UINT4                u4DsTrcFlag;
    tDsSystemControl     DsSystemControl;
    tDsStatus            DsStatus;

}tDsGlobalInfo;


typedef INT4 (* DS_MFCLFRVAL_FUNCT)(tDiffServMultiFieldClfrEntry *);

typedef struct DsMFClfrValEntry {

   DS_MFCLFRVAL_FUNCT  pValFunct;

} tDsMFClfrValEntry;


typedef INT4 (* DS_INPROACTVAL_FUNCT)(tDiffServInProfileActionEntry *);

typedef struct DsInProActValEntry {

   DS_INPROACTVAL_FUNCT  pValFunct;

} tDsInProActValEntry;

typedef INT4 (* DS_OUTPROACTVAL_FUNCT)(tDiffServOutProfileActionEntry *);

typedef struct DsOutProActValEntry {

   DS_OUTPROACTVAL_FUNCT  pValFunct;

} tDsOutProActValEntry;

#endif /* _DSTDFS_H */
