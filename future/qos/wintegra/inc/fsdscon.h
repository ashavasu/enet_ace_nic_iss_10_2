/*$Id: fsdscon.h,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/
# ifndef fsdsOCON_H
# define fsdsOCON_H
/*
 *  The Constant Declarations for
 *  fsDiffServSystem
 */

# define FSDSSYSTEMCONTROL                                 (1)
# define FSDSSTATUS                                        (2)


/*
 *  The Constant Declarations for
 *  fsDiffServMultiFieldClfrTable
 */

# define FSDIFFSERVMULTIFIELDCLFRID                        (1)
# define FSDIFFSERVMULTIFIELDCLFRFILTERNO                  (2)
# define FSDIFFSERVMULTIFIELDCLFRIFILTERTYPE               (3)
# define FSDIFFSERVMULTIFIELDCLFRFILTERPORTS               (4)
# define FSDIFFSERVMULTIFIELDCLFRSTATUS                    (5)

/*
 *  The Constant Declarations for
 *  fsDiffServClfrTable
 */

# define FSDIFFSERVCLFRID                                  (1)
# define FSDIFFSERVCLFRMFCLFRID                            (2)
# define FSDIFFSERVCLFRINPROACTIONID                       (3)
# define FSDIFFSERVCLFROUTPROACTIONID                      (4)
# define FSDIFFSERVCLFRSTATUS                              (5)

/*
 *  The Constant Declarations for
 *  fsDiffServInProfileActionTable
 */

# define FSDIFFSERVINPROFILEACTIONID                       (1)
# define FSDIFFSERVINPROFILEACTIONFLAG                     (2)
# define FSDIFFSERVINPROFILEACTIONNEWPRIO                  (3)
# define FSDIFFSERVINPROFILEACTIONIPTOS                    (4)
# define FSDIFFSERVINPROFILEACTIONPORT                     (5)
# define FSDIFFSERVINPROFILEACTIONDSCP                     (6)
# define FSDIFFSERVINPROFILEACTIONSTATUS                   (7)

/*
 *  The Constant Declarations for
 *  fsDiffServOutProfileActionTable
 */

# define FSDIFFSERVOUTPROFILEACTIONID                      (1)
# define FSDIFFSERVOUTPROFILEACTIONFLAG                    (2)
# define FSDIFFSERVOUTPROFILEACTIONDSCP                    (3)
# define FSDIFFSERVOUTPROFILEACTIONMID                     (4)
# define FSDIFFSERVOUTPROFILEACTIONSTATUS                  (5)

/*
 *  The Constant Declarations for
 *  fsDiffServMeterTable
 */

# define FSDIFFSERVMETERID                                 (1)
# define FSDIFFSERVMETERTOKENSIZE                          (2)
# define FSDIFFSERVMETERREFRESHCOUNT                       (3)
# define FSDIFFSERVMETERSTATUS                             (4)

/*
 *  The Constant Declarations for
 *  fsDiffServSchedulerTable
 */

# define FSDIFFSERVSCHEDULERID                             (1)
# define FSDIFFSERVSCHEDULERDPID                           (2)
# define FSDIFFSERVSCHEDULERQUEUECOUNT                     (3)
# define FSDIFFSERVSCHEDULERWEIGHT                         (4)
# define FSDIFFSERVSCHEDULERSTATUS                         (5)

#endif /*  fsissdfsOCON_H  */
