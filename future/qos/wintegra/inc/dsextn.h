/*$Id: dsextn.h,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/
#ifndef _DSEXTN_H
#define _DSEXTN_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dsextn.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ module                                */
/*    MODULE NAME           : DiffServ module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Manish K S                                     */
/*    DESCRIPTION           : This file contains global variables used       */
/*                            in DiffServ Module.                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/

extern UINT4             u4CidrSubnetMask[DS_MAX_CIDR + 1];
#endif /* _DSEXTN_H */
