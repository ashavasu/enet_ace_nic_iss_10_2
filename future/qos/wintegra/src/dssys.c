/*$Id: dssys.c,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dssys.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ module                                */
/*    MODULE NAME           : DiffServ module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the functions used by low   */
/*                            level routines in DiffServ Module.             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/

#ifndef _DSSYS_C
#define _DSSYS_C

#include "dsinc.h"
#include "dscli.h"
#include "fsissdwr.h"

tOsixSemId          gSemId;
/*****************************************************************************/
/* Function Name      : DsInit                                               */
/*                                                                           */
/* Description        : This function creates the Mutual exclusion           */
/*                      semaphore and starts the Diffserver module           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsInit (VOID)
{

    if (OsixCreateSem (DFS_MUT_EXCL_SEM_NAME, 1, 0,
                       &(DFS_MUT_EXCL_SEM_ID)) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "creation FAILED\n");
        return DS_FAILURE;
    }

    if (DsStart () == DS_FAILURE)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Start failed\n");
        return DS_FAILURE;
    }

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsStart                                              */
/*                                                                           */
/* Description        : This function allocates memory pools for all tables  */
/*                      in the DiffServ module. It initialises the           */
/*                      the GLobal structure gDsGLobalInfo.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*                      DS_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
DsStart (VOID)
{
#ifdef NPAPI_WANTED
    UINT4               au4TempMinBw[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4TempMaxBw[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4TempBwFlag[VLAN_DEV_MAX_NUM_COSQ];
#endif
    INT4                i4IfaceIndex;
    INT4                i4CoSqId;

    DS_MEMSET (&gDsGlobalInfo, 0, sizeof (tDsGlobalInfo));
#ifdef NPAPI_WANTED
    DS_MEMSET (au4TempMinBw, 0, VLAN_DEV_MAX_NUM_COSQ);
    DS_MEMSET (au4TempMaxBw, 0, VLAN_DEV_MAX_NUM_COSQ);
    DS_MEMSET (au4TempBwFlag, 0, VLAN_DEV_MAX_NUM_COSQ);
#endif
    /* Assign Trace Option */
    gDsGlobalInfo.u4DsTrcFlag = 0;

    for (i4IfaceIndex = 0; i4IfaceIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfaceIndex++)
    {
        /* Initializing array for CoSqAlgorithm */
        gu4PortCosqScheduleAlgo[i4IfaceIndex].u4CosqScheduleAlgo =
            DS_COSQ_SCHE_ALGO_STRICT;
        gu4PortCosqScheduleAlgo[i4IfaceIndex].u4Status =
            DS_IFACE_STATUS_INVALID;

        for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
        {
            /* Initializing array for CoSq Weight */
            gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqWeights =
                DS_COSQ_MIN_WEIGHT;
            gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4Status =
                DS_IFACE_STATUS_INVALID;

        }
#ifdef NPAPI_WANTED
        if (DsIsNpEasyrider () == DS_TRUE)
        {
            /* Initializing array for Minimum, Maximum Bandwidth & Bandwidth Flag */
            DsHwGetCosqBandwidth ((UINT4) (i4IfaceIndex + 1),
                                  au4TempMinBw, au4TempMaxBw, au4TempBwFlag);

            for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
            {
                gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqMinBw =
                    au4TempMinBw[i4CoSqId];
                gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqMaxBw =
                    au4TempMaxBw[i4CoSqId];
                gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqBwFlag =
                    au4TempBwFlag[i4CoSqId];
            }
        }
#else
        for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
        {
            gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqMinBw = 0;
            gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqMaxBw = 0;
            gu4CosqWeightsBw[i4IfaceIndex][i4CoSqId].u4CosqBwFlag = 0;
        }
#endif
    }

    /* Allocate memory pool for MultiField classifier table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_MFCLFR_MEMBLK_SIZE,
                                  DS_MFCLFR_MEMBLK_COUNT,
                                  &(DS_MFCLFR_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC,
                "MultiField Classifier Memory Pool Creation FAILED\n");
        DsHandleInitFailure ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Classifier Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_CLFR_MEMBLK_SIZE,
                                  DS_CLFR_MEMBLK_COUNT,
                                  &(DS_CLFR_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC,
                "Classifier Entry Memory Pool Creation FAILED\n");
        DsHandleInitFailure ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Meter Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_METER_MEMBLK_SIZE,
                                  DS_METER_MEMBLK_COUNT,
                                  &(DS_METER_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC, "Meter Entry Memory Pool Creation FAILED\n");
        DsHandleInitFailure ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Scheduler Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_SCHEDULER_MEMBLK_SIZE,
                                  DS_SCHEDULER_MEMBLK_COUNT,
                                  &(DS_SCHEDULER_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC, "Scheduler Entry Memory Pool Creation FAILED\n");
        DsHandleInitFailure ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for In Profile Action Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_INPROACT_MEMBLK_SIZE,
                                  DS_INPROACT_MEMBLK_COUNT,
                                  &(DS_INPROACT_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC,
                "In Profile Action Entry Memory Pool Creation FAILED\n");
        DsHandleInitFailure ();
        return DS_FAILURE;
    }

    /* Allocate memory pool for Out Profile Action Table */
    if (DS_CREATE_ENTRY_MEM_POOL (DS_OUTPROACT_MEMBLK_SIZE,
                                  DS_OUTPROACT_MEMBLK_COUNT,
                                  &(DS_OUTPROACT_MEMPOOL_ID)) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC,
                "Out Profile Action Entry Memory Pool Creation FAILED\n");
        DsHandleInitFailure ();
        return DS_FAILURE;
    }

    /* Initialise the SLL lists for All the Tables */
    DS_SLL_INIT (&(DS_CLFR_LIST));
    DS_SLL_INIT (&(DS_METER_LIST));
    DS_SLL_INIT (&(DS_MFCLFR_LIST));
    DS_SLL_INIT (&(DS_INPROACT_LIST));
    DS_SLL_INIT (&(DS_OUTPROACT_LIST));
    DS_SLL_INIT (&(DS_SCHEDULER_LIST));

    RegisterFSISSD ();

    gDsGlobalInfo.DsSystemControl = DS_START;
    gDsGlobalInfo.DsStatus = DS_DISABLE;

    DS_TRC (CONTROL_PLANE_TRC, "SYS: DIFFSERV MODULE INITIALISED\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsEnable                                             */
/*                                                                           */
/* Description        : This function scans through all the tables and       */
/*                      programs the hardware accordingly.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*                      DS_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
DsEnable (VOID)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    tDiffServMeterEntry *pDsMeterEntry = NULL;

    DS_TRC (INIT_SHUT_TRC, "SYS: Enabling DIFFSERV Module ... \n");

#ifdef NPAPI_WANTED
    /* Call the routine to Initialise the diffsrv in h/w */
    if (DsHwInit () != FNP_SUCCESS)
    {
        return DS_FAILURE;
    }
#endif

    /* Scan through the Classifier Table and program 
     * the valid entries it in device.*/
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_NOT_IN_SERVICE)
        {

            /* Get the Elements from the list here */
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

            if (pDsMFClfrEntry == NULL)
            {
                return DS_FAILURE;
            }

            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);
            pDsOutProActEntry = DsGetOutProfileActionEntry
                (pDsClfrEntry->i4DsClfrOutProActionId);
            if (pDsOutProActEntry != NULL)
            {
                pDsMeterEntry = DsGetMeterEntry
                    (pDsOutProActEntry->i4DsOutProfileActionMID);
            }

#ifdef NPAPI_WANTED
            /* Call the hardware routine here */
            if (DsHwClassifierAdd (pDsClfrEntry,
                                   pDsMFClfrEntry,
                                   pDsInProActEntry,
                                   pDsOutProActEntry,
                                   pDsMeterEntry) != FNP_SUCCESS)
            {
                return DS_FAILURE;
            }
#endif
            pDsClfrEntry->u1DsClfrStatus = DS_ACTIVE;
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    /* Scan through the Scheduler Table and program 
     * the valid entries it in device.*/
    pSllNode = DS_SLL_FIRST (&DS_SCHEDULER_LIST);

    while (pSllNode != NULL)
    {
        pDsSchedulerEntry = (tDiffServSchedulerEntry *) pSllNode;

        if (pDsSchedulerEntry->u1DsSchedulerStatus == DS_NOT_IN_SERVICE)
        {
#ifdef NPAPI_WANTED
            /* Call the hardware routine here */
            if (DsHwSchedulerAdd (pDsSchedulerEntry) != FNP_SUCCESS)
            {
                return DS_FAILURE;
            }
#endif
            pDsSchedulerEntry->u1DsSchedulerStatus = DS_ACTIVE;
        }
        pSllNode = DS_SLL_NEXT (&DS_SCHEDULER_LIST, pSllNode);
    }

#ifdef NPAPI_WANTED
    if (DsProgramSchAlgo () == DS_FAILURE)
    {
        return DS_FAILURE;
    }
#endif

    /* Set the status of DiffServ as Enable */
    gDsGlobalInfo.DsStatus = DS_ENABLE;

    DS_TRC (CONTROL_PLANE_TRC, "SYS: DIFFSERV MODULE ENABLED\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsHandleInitFailure.                                 */
/*                                                                           */
/* Description        : This function is called when the initialization of   */
/*                      DiffServ fails at any point during  initialization.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
DsHandleInitFailure (VOID)
{
    if (DS_MFCLFR_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_MFCLFR_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {
            DS_TRC (INIT_SHUT_TRC,
                    "MF Classifier Entry Memory Pool Release FAILED\n");
        }
    }

    if (DS_CLFR_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_CLFR_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC,
                    "Classifier Entry Memory Pool Release FAILED\n");
        }
    }

    if (DS_METER_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_METER_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC, "Meter Entry Memory Pool Release FAILED\n");
        }
    }

    if (DS_SCHEDULER_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_SCHEDULER_MEMPOOL_ID) !=
            DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC,
                    "Scheduler Entry Memory Pool Release FAILED\n");
        }
    }

    if (DS_INPROACT_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_INPROACT_MEMPOOL_ID) != DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC,
                    "In Profile Action Entry Memory Pool Release FAILED\n");
        }
    }

    if (DS_OUTPROACT_MEMPOOL_ID != DS_INVALID_VAL)
    {

        if (DS_DELETE_ENTRY_MEM_POOL (DS_OUTPROACT_MEMPOOL_ID) !=
            DS_MEM_SUCCESS)
        {

            DS_TRC (INIT_SHUT_TRC,
                    "Out Profile Action Entry Memory Pool Release FAILED\n");
        }
    }

}

/*****************************************************************************/
/* Function Name      : DsShutdown                                           */
/*                                                                           */
/* Description        : This function is called to shutdown DiffServ. This   */
/*                      function internally calls DsDisable before releasing */
/*                      all the memory.                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsShutDown (VOID)
{
    /* If the DS module is shut down return FAILURE */
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {

        DS_TRC (INIT_SHUT_TRC, "System is not Started. Cannot Shutdown.\n");
        return DS_FAILURE;
    }

    /* Disable the DiffServ Module */
    DsDisable ();

    /* Delete the MF Classifier Entry Mem Pool */
    if (DS_DELETE_ENTRY_MEM_POOL (DS_MFCLFR_MEMPOOL_ID) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC,
                "MF Classifier Entry Memory Pool Release FAILED\n");
    }

    /* Delete the Classifier Entry Mem Pool */
    if (DS_DELETE_ENTRY_MEM_POOL (DS_CLFR_MEMPOOL_ID) != DS_MEM_SUCCESS)
    {

        DS_TRC (INIT_SHUT_TRC, "Classifier Entry Memory Pool Release FAILED\n");
    }

    /* Delete the Meter Entry Mem Pool */
    if (DS_DELETE_ENTRY_MEM_POOL (DS_METER_MEMPOOL_ID) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC, "Entry Memory Pool Release FAILED\n");
    }

    /* Delete the Scheduler Entry Mem Pool */
    if (DS_DELETE_ENTRY_MEM_POOL (DS_SCHEDULER_MEMPOOL_ID) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC, "Scheduler Entry Memory Pool Release FAILED\n");
    }

    /* Delete the In Profile Action Entry Mem Pool */
    if (DS_DELETE_ENTRY_MEM_POOL (DS_INPROACT_MEMPOOL_ID) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC,
                "In Profile Action Entry Memory Pool Release FAILED\n");
    }

    /* Delete the Out Profile Action Entry Mem Pool */
    if (DS_DELETE_ENTRY_MEM_POOL (DS_OUTPROACT_MEMPOOL_ID) != DS_MEM_SUCCESS)
    {
        DS_TRC (INIT_SHUT_TRC,
                "Out Profile Action Entry Memory Pool Release FAILED\n");
    }

    gDsGlobalInfo.DsSystemControl = DS_SHUTDOWN;

    DS_TRC (INIT_SHUT_TRC, "SYS: DIFFSERV MODULE SHUT DOWN\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsDisable                                            */
/*                                                                           */
/* Description        : This function deletes all the entries for datapath,  */
/*                      classifier and Scheduler from the hardware.          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsDisable (VOID)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;

    /* If the DS module is shut down return FAILURE */
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {

        DS_TRC (INIT_SHUT_TRC, "System is not Started. Cannot Disable.\n");
        return DS_FAILURE;
    }

    /* If the DS module is not ENABLED return FAILURE */
    if (gDsGlobalInfo.DsStatus != DS_ENABLE)
    {

        DS_TRC (INIT_SHUT_TRC, "System is not Enabled. Cannot Disable.\n");
        return DS_FAILURE;
    }

    /* Scan through the clfr Table and put 
     * the ACTIVE entries to NOT_IN_SERVICE */
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            pDsClfrEntry->u1DsClfrStatus = DS_NOT_IN_SERVICE;
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    /* Scan through the Scheduler Table and put
     * the ACTIVE entries to NOT_IN_SERVICE */
    pSllNode = DS_SLL_FIRST (&DS_SCHEDULER_LIST);

    while (pSllNode != NULL)
    {
        pDsSchedulerEntry = (tDiffServSchedulerEntry *) pSllNode;

        if (pDsSchedulerEntry->u1DsSchedulerStatus == DS_ACTIVE)
        {
            pDsSchedulerEntry->u1DsSchedulerStatus = DS_NOT_IN_SERVICE;
        }
        pSllNode = DS_SLL_NEXT (&DS_SCHEDULER_LIST, pSllNode);
    }

    gDsGlobalInfo.DsStatus = DS_DISABLE;

    DS_TRC (INIT_SHUT_TRC, "SYS: DIFFSERV MODULE DISABLED\n");

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateMFClfrId                                   */
/*                                                                           */
/* Description        : This function is called for validating the MF        */
/*                      Classifier Id.         .                             */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServMFClfrId                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateMFClfrId (INT4 i4FsDiffServMFClfrId)
{
    if ((i4FsDiffServMFClfrId < DS_MIN_MFCLFR_ID) ||
        (i4FsDiffServMFClfrId > DS_MAX_MFCLFR_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateClfrId                                     */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      Classifier Id.         .                             */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServClfrId                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateClfrId (INT4 i4FsDiffServClfrId)
{
    if ((i4FsDiffServClfrId < DS_MIN_CLFR_ID) ||
        (i4FsDiffServClfrId > DS_MAX_CLFR_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateInProfileActionId                          */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      In Profile Action Id.                                */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServInProfileActionId                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateInProfileActionId (INT4 i4FsDiffServInProfileActionId)
{
    if ((i4FsDiffServInProfileActionId < DS_MIN_IN_PROACT_ID) ||
        (i4FsDiffServInProfileActionId > DS_MAX_IN_PROACT_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateOutProfileActionId                         */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      Out Profile Action Id.                               */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServOutProfileActionId                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateOutProfileActionId (INT4 i4FsDiffServOutProfileActionId)
{
    if ((i4FsDiffServOutProfileActionId < DS_MIN_OUT_PROACT_ID) ||
        (i4FsDiffServOutProfileActionId > DS_MAX_OUT_PROACT_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateMeterId                                    */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      Meter Id.                                            */
/*                                                                           */
/* Input(s)           : INT4 i4FsDiffServMeterId                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateMeterId (INT4 i4FsDiffServMeterId)
{
    if ((i4FsDiffServMeterId < DS_MIN_METER_ID) ||
        (i4FsDiffServMeterId > DS_MAX_METER_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValidateSchedulerId                                */
/*                                                                           */
/* Description        : This function is called for validating the           */
/*                      Scheduler Id.                                        */
/*                                                                           */
/* Input(s)           : INT4 i4FsDsValidateSchedulerId                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsValidateSchedulerId (INT4 i4FsDsValidateSchedulerId)
{
    if ((i4FsDsValidateSchedulerId < DS_MIN_SCHEDULER_ID) ||
        (i4FsDsValidateSchedulerId > DS_MAX_SCHEDULER_ID))
    {
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidMultiFieldClfrId               */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the MF Classifier Table.                          */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServMultiFieldClfrId                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidMultiFieldClfrId (INT4 *pi4FsDiffServMultiFieldClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDiffServMFClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pFirstDiffServMFClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServMFClfrEntry = pDiffServMFClfrEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServMFClfrEntry->i4DsMultiFieldClfrId >
                pDiffServMFClfrEntry->i4DsMultiFieldClfrId)
            {
                pFirstDiffServMFClfrEntry = pDiffServMFClfrEntry;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServMultiFieldClfrId =
            pFirstDiffServMFClfrEntry->i4DsMultiFieldClfrId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidMultiFieldClfrId                */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the MF Classifier table.                             */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServDataPathId                    */
/*                    : INT4 i4FsDiffServDataPathId                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidMultiFieldClfrId (INT4 i4FsDiffServMultiFieldClfrId,
                                       INT4 *pi4NextFsDiffServMultiFieldClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDiffServMFClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pNextDiffServMFClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if (i4FsDiffServMultiFieldClfrId <
            pDiffServMFClfrEntry->i4DsMultiFieldClfrId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServMFClfrEntry = pDiffServMFClfrEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServMFClfrEntry->i4DsMultiFieldClfrId >
                    pDiffServMFClfrEntry->i4DsMultiFieldClfrId)
                {
                    pNextDiffServMFClfrEntry = pDiffServMFClfrEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServMultiFieldClfrId =
            pNextDiffServMFClfrEntry->i4DsMultiFieldClfrId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidClfrId                         */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the Classifier Table.                             */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServClfrId                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidClfrId (INT4 *pi4FsDiffServClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;
    tDiffServClfrEntry *pFirstDiffServClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServClfrEntry = pDiffServClfrEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServClfrEntry->i4DsClfrId >
                pDiffServClfrEntry->i4DsClfrId)
            {
                pFirstDiffServClfrEntry = pDiffServClfrEntry;
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServClfrId = pFirstDiffServClfrEntry->i4DsClfrId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidClfrId                          */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the Classifier table.                                */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServDataPathId                    */
/*                    : INT4 i4FsDiffServDataPathId                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidClfrId (INT4 i4FsDiffServClfrId,
                             INT4 *pi4NextFsDiffServClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;
    tDiffServClfrEntry *pNextDiffServClfrEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (i4FsDiffServClfrId < pDiffServClfrEntry->i4DsClfrId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServClfrEntry = pDiffServClfrEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServClfrEntry->i4DsClfrId >
                    pDiffServClfrEntry->i4DsClfrId)
                {
                    pNextDiffServClfrEntry = pDiffServClfrEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServClfrId = pNextDiffServClfrEntry->i4DsClfrId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidInProfileActionId              */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the In Profile Action Table.                      */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServInProfileActionId                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidInProfileActionId (INT4 *pi4FsDiffServInProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServInProfileActionEntry *pDiffServInProActEntry = NULL;
    tDiffServInProfileActionEntry *pFirstDiffServInProActEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_INPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServInProActEntry = (tDiffServInProfileActionEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServInProActEntry = pDiffServInProActEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServInProActEntry->i4DsInProfileActionId >
                pDiffServInProActEntry->i4DsInProfileActionId)
            {
                pFirstDiffServInProActEntry = pDiffServInProActEntry;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_INPROACT_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServInProfileActionId =
            pFirstDiffServInProActEntry->i4DsInProfileActionId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidInProfileActionId               */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the In Profile Action table.                         */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServDataPathId                    */
/*                    : INT4 i4FsDiffServDataPathId                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidInProfileActionId (INT4 i4FsDiffServInProfileActionId,
                                        INT4
                                        *pi4NextFsDiffServInProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServInProfileActionEntry *pDiffServInProActEntry = NULL;
    tDiffServInProfileActionEntry *pNextDiffServInProActEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_INPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServInProActEntry = (tDiffServInProfileActionEntry *) pSllNode;

        if (i4FsDiffServInProfileActionId <
            pDiffServInProActEntry->i4DsInProfileActionId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServInProActEntry = pDiffServInProActEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServInProActEntry->i4DsInProfileActionId >
                    pDiffServInProActEntry->i4DsInProfileActionId)
                {
                    pNextDiffServInProActEntry = pDiffServInProActEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_INPROACT_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServInProfileActionId =
            pNextDiffServInProActEntry->i4DsInProfileActionId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidOutProfileActionId             */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the In Profile Action Table.                      */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServOutProfileActionId                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidOutProfileActionId (INT4 *pi4FsDiffServOutProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServOutProfileActionEntry *pDiffServOutProActEntry = NULL;
    tDiffServOutProfileActionEntry *pFirstDiffServOutProActEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_OUTPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServOutProActEntry = (tDiffServOutProfileActionEntry *) pSllNode;

        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServOutProActEntry = pDiffServOutProActEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServOutProActEntry->i4DsOutProfileActionId >
                pDiffServOutProActEntry->i4DsOutProfileActionId)
            {
                pFirstDiffServOutProActEntry = pDiffServOutProActEntry;
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_OUTPROACT_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServOutProfileActionId =
            pFirstDiffServOutProActEntry->i4DsOutProfileActionId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidOutProfileActionId              */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the In Profile Action table.                         */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServOutProfileActionId            */
/*                    : INT4 i4FsDiffServOutProfileActionId                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidOutProfileActionId (INT4 i4FsDiffServOutProfileActionId,
                                         INT4
                                         *pi4NextFsDiffServOutProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServOutProfileActionEntry *pDiffServOutProActEntry = NULL;
    tDiffServOutProfileActionEntry *pNextDiffServOutProActEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_OUTPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServOutProActEntry = (tDiffServOutProfileActionEntry *) pSllNode;

        if (i4FsDiffServOutProfileActionId <
            pDiffServOutProActEntry->i4DsOutProfileActionId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServOutProActEntry = pDiffServOutProActEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServOutProActEntry->i4DsOutProfileActionId >
                    pDiffServOutProActEntry->i4DsOutProfileActionId)
                {
                    pNextDiffServOutProActEntry = pDiffServOutProActEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_OUTPROACT_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServOutProfileActionId =
            pNextDiffServOutProActEntry->i4DsOutProfileActionId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidMeterId                        */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the Meter Table.                                  */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServMeterId                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidMeterId (INT4 *pi4FsDiffServMeterId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMeterEntry *pDiffServMeterEntry = NULL;
    tDiffServMeterEntry *pFirstDiffServMeterEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_METER_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMeterEntry = (tDiffServMeterEntry *) pSllNode;
        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServMeterEntry = pDiffServMeterEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServMeterEntry->i4DsMeterId >
                pDiffServMeterEntry->i4DsMeterId)
            {
                pFirstDiffServMeterEntry = pDiffServMeterEntry;
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_METER_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServMeterId = pFirstDiffServMeterEntry->i4DsMeterId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidMeterId                         */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the No Match Action table.                           */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServMeterId                       */
/*                    : INT4 i4FsDiffServMeterId                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidMeterId (INT4 i4FsDiffServMeterId,
                              INT4 *pi4NextFsDiffServMeterId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMeterEntry *pDiffServMeterEntry = NULL;
    tDiffServMeterEntry *pNextDiffServMeterEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_METER_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMeterEntry = (tDiffServMeterEntry *) pSllNode;

        if (i4FsDiffServMeterId < pDiffServMeterEntry->i4DsMeterId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServMeterEntry = pDiffServMeterEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServMeterEntry->i4DsMeterId >
                    pDiffServMeterEntry->i4DsMeterId)
                {
                    pNextDiffServMeterEntry = pDiffServMeterEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_METER_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServMeterId = pNextDiffServMeterEntry->i4DsMeterId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetFirstValidSchedulerId                    */
/*                                                                           */
/* Description        : This function is called for getting the first Index  */
/*                      in the Scheduler Table.                              */
/*                                                                           */
/* Input(s)           : INT4 *pi4FsDiffServSchedulerId                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetFirstValidSchedulerId (INT4 *pi4FsDiffServSchedulerId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServSchedulerEntry *pDiffServSchedulerEntry = NULL;
    tDiffServSchedulerEntry *pFirstDiffServSchedulerEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_SCHEDULER_LIST);

    while (pSllNode != NULL)
    {
        pDiffServSchedulerEntry = (tDiffServSchedulerEntry *) pSllNode;

        if (u1FoundFlag == DS_FALSE)
        {
            pFirstDiffServSchedulerEntry = pDiffServSchedulerEntry;
            u1FoundFlag = DS_TRUE;
        }
        else
        {
            if (pFirstDiffServSchedulerEntry->i4DsSchedulerId >
                pDiffServSchedulerEntry->i4DsSchedulerId)
            {
                pFirstDiffServSchedulerEntry = pDiffServSchedulerEntry;
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_SCHEDULER_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4FsDiffServSchedulerId =
            pFirstDiffServSchedulerEntry->i4DsSchedulerId;

        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsSnmpLowGetNextValidSchedulerId                     */
/*                                                                           */
/* Description        : This function is called getting the Next Entry for   */
/*                      the Scheduler table.                                 */
/*                                                                           */
/* Input(s)           : INT4 *pi4NextFsDiffServSchedulerId                   */
/*                    : INT4 i4FsDiffServSchedulerId                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
DsSnmpLowGetNextValidSchedulerId (INT4 i4FsDiffServSchedulerId,
                                  INT4 *pi4NextFsDiffServSchedulerId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServSchedulerEntry *pDiffServSchedulerEntry = NULL;
    tDiffServSchedulerEntry *pNextDiffServSchedulerEntry = NULL;
    UINT1               u1FoundFlag = DS_FALSE;

    pSllNode = DS_SLL_FIRST (&DS_SCHEDULER_LIST);

    while (pSllNode != NULL)
    {
        pDiffServSchedulerEntry = (tDiffServSchedulerEntry *) pSllNode;

        if (i4FsDiffServSchedulerId < pDiffServSchedulerEntry->i4DsSchedulerId)
        {

            if (u1FoundFlag == DS_FALSE)
            {
                pNextDiffServSchedulerEntry = pDiffServSchedulerEntry;
                u1FoundFlag = DS_TRUE;
            }
            else
            {
                if (pNextDiffServSchedulerEntry->i4DsSchedulerId >
                    pDiffServSchedulerEntry->i4DsSchedulerId)
                {
                    pNextDiffServSchedulerEntry = pDiffServSchedulerEntry;
                }
            }
        }

        pSllNode = DS_SLL_NEXT (&DS_SCHEDULER_LIST, pSllNode);
    }

    if (u1FoundFlag == DS_TRUE)
    {
        *pi4NextFsDiffServSchedulerId =
            pNextDiffServSchedulerEntry->i4DsSchedulerId;
        return DS_SUCCESS;
    }
    else
    {
        return DS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : DsGetMFClfrEntry                                     */
/*                                                                           */
/* Description        : This function is called for getting the Multi Field  */
/*                      Clfr Entry for a given MF Clfr Id.                   */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServMultiFieldClfrId                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MF Clfr Entry   - On success                         */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServMultiFieldClfrEntry *
DsGetMFClfrEntry (INT4 i4DiffServMultiFieldClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDiffServMFClfrEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if (pDiffServMFClfrEntry->i4DsMultiFieldClfrId ==
            i4DiffServMultiFieldClfrId)
        {
            return pDiffServMFClfrEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetClfrEntry                                       */
/*                                                                           */
/* Description        : This function is called for getting the Classifier   */
/*                      Entry for a given Clfr Id.                           */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServClfrId                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Classifier Entry - On success                        */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServClfrEntry *
DsGetClfrEntry (INT4 i4DiffServClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDiffServClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDiffServClfrEntry->i4DsClfrId == i4DiffServClfrId)
        {
            return pDiffServClfrEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetInProfileActionEntry                            */
/*                                                                           */
/* Description        : This function is called for getting the In Profile   */
/*                      Action Entry for a given In Profile Action Id.       */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServInProfileActionId                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : In Profile Action Entry - On success                 */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServInProfileActionEntry *
DsGetInProfileActionEntry (INT4 i4DiffServInProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServInProfileActionEntry *pDiffServInProActEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_INPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServInProActEntry = (tDiffServInProfileActionEntry *) pSllNode;

        if (pDiffServInProActEntry->i4DsInProfileActionId
            == i4DiffServInProfileActionId)
        {
            return pDiffServInProActEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_INPROACT_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetOutProfileActionEntry                           */
/*                                                                           */
/* Description        : This function is called for getting the In Profile   */
/*                      Action Entry for a given Out Profile Action Id.      */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServOutProfileActionId                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Out Profile Action Entry - On success                */
/*                      NULLL           - On Failure                         */
/*****************************************************************************/
tDiffServOutProfileActionEntry *
DsGetOutProfileActionEntry (INT4 i4DiffServOutProfileActionId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServOutProfileActionEntry *pDiffServOutProActEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_OUTPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDiffServOutProActEntry = (tDiffServOutProfileActionEntry *) pSllNode;

        if (pDiffServOutProActEntry->i4DsOutProfileActionId
            == i4DiffServOutProfileActionId)
        {
            return pDiffServOutProActEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_OUTPROACT_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetMeterEntry                                      */
/*                                                                           */
/* Description        : This function is called for getting the Meter        */
/*                      Entry for a given Meter Id.                          */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServMeterId                               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Meter Entry - On success                             */
/*                      NULLL       - On Failure                             */
/*****************************************************************************/
tDiffServMeterEntry *
DsGetMeterEntry (INT4 i4DiffServMeterId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMeterEntry *pDiffServMeterEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_METER_LIST);

    while (pSllNode != NULL)
    {
        pDiffServMeterEntry = (tDiffServMeterEntry *) pSllNode;

        if (pDiffServMeterEntry->i4DsMeterId == i4DiffServMeterId)
        {
            return pDiffServMeterEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_METER_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsGetSchedulerEntry                                  */
/*                                                                           */
/* Description        : This function is called for getting the Scheduler    */
/*                      Entry for a given Scheduler Id.                      */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServSchedulerId                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Scheduler Entry - On success                         */
/*                      NULLL       - On Failure                             */
/*****************************************************************************/
tDiffServSchedulerEntry *
DsGetSchedulerEntry (INT4 i4DiffServSchedulerId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServSchedulerEntry *pDiffServSchedulerEntry = NULL;

    pSllNode = DS_SLL_FIRST (&DS_SCHEDULER_LIST);

    while (pSllNode != NULL)
    {
        pDiffServSchedulerEntry = (tDiffServSchedulerEntry *) pSllNode;

        if (pDiffServSchedulerEntry->i4DsSchedulerId == i4DiffServSchedulerId)
        {
            return pDiffServSchedulerEntry;
        }

        pSllNode = DS_SLL_NEXT (&DS_SCHEDULER_LIST, pSllNode);
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : DsValidateSchedulerDP                                */
/*                                                                           */
/* Description        : This function is called for validating the DataPath  */
/*                      Entry for the Scheduler.                             */
/*                                                                           */
/* Input(s)           : INT4 i4DiffServSchedulerDPId                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValidateSchedulerDP (INT4 i4DiffServSchedulerDPId)
{
    UNUSED_PARAM (i4DiffServSchedulerDPId);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifySchedulerData                               */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Entry for the Scheduler.                             */
/*                                                                           */
/* Input(s)           : tDiffServSchedulerEntry  *pDsSchedulerEntry          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifySchedulerData (tDiffServSchedulerEntry * pDsSchedulerEntry)
{
    UINT1               au1DsSchedulerWeight[DS_SCHEDULER_WEIGHT_SIZE];

    DS_MEMSET (au1DsSchedulerWeight, 0, DS_SCHEDULER_WEIGHT_SIZE);
    if ((pDsSchedulerEntry) != NULL)
    {
        /* Check for all the mandatory objects */
        if ((pDsSchedulerEntry->u4DsSchedulerQueueCount != 0) &&
            (DS_MEMCMP (pDsSchedulerEntry->au1DsSchedulerWeight,
                        au1DsSchedulerWeight, DS_SCHEDULER_WEIGHT_SIZE)))
        {
            pDsSchedulerEntry->u1DsSchedulerStatus = DS_NOT_IN_SERVICE;
            return DS_SUCCESS;
        }
    }
    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsQualifyMFClfrData                                  */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Enry for MF classifier Table.                        */
/*                                                                           */
/* Input(s)           : tDiffServMultiFieldClfrEntry **ppDsMFClfrEntry       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyMFClfrData (tDiffServMultiFieldClfrEntry * pDsMFClfrEntry)
{
    tIssL2FilterEntry  *pDsMacFilter = NULL;
    tIssL3FilterEntry  *pDsIpFilter = NULL;

    if (pDsMFClfrEntry != NULL)
    {
        if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_MAC_FILTER)
        {
            pDsMacFilter = IssGetL2FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsMacFilter == NULL)
            {
                return DS_FAILURE;
            }
            if (pDsMacFilter->IssL2FilterAction != ISS_ALLOW)
            {
                return DS_FAILURE;
            }
            /*Copy the filter portlist which is used in NPAPI */
            DS_MEMSET (pDsMFClfrEntry->DsPorts, 0, DS_PORT_LIST_SIZE);
            DS_MEMCPY (pDsMFClfrEntry->DsPorts,
                       pDsMacFilter->IssL2FilterInPortList, DS_PORT_LIST_SIZE);
        }
        else if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_IP_FILTER)
        {
            pDsIpFilter = IssGetL3FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsIpFilter == NULL)
            {
                return DS_FAILURE;
            }
            if (pDsIpFilter->IssL3FilterAction != ISS_ALLOW)
            {
                return DS_FAILURE;
            }
            if (pDsIpFilter->IssL3FilterDirection != ISS_DIRECTION_IN)
            {
                return DS_FAILURE;
            }
            /*Copy the filter portlist which is used in NPAPI */
            DS_MEMSET (pDsMFClfrEntry->DsPorts, 0, DS_PORT_LIST_SIZE);
            DS_MEMCPY (pDsMFClfrEntry->DsPorts,
                       pDsIpFilter->IssL3FilterInPortList, DS_PORT_LIST_SIZE);
        }
        pDsMFClfrEntry->u1DsMultiFieldClfrStatus = DS_NOT_IN_SERVICE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsCheckMFClfrDependency                              */
/*                                                                           */
/* Description        : This function is called for checking the dependency  */
/*                      for MF classifier Entry.                             */
/*                                                                           */
/* Input(s)           : INT4 i4DsMFClfrId                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsCheckMFClfrDependency (INT4 i4DsMFClfrId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    /* Go through the classifier table and check with
     * each active entry if this MFClfr is there*/
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (pDsClfrEntry->i4DsClfrMFClfrId == i4DsMFClfrId)
            {
                return DS_FAILURE;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifyClfrData                                    */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Entry for classifier Table.                          */
/*                                                                           */
/* Input(s)           : tDiffServMultiFieldClfrEntry **ppDsMFClfrEntry       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyClfrData (tDiffServClfrEntry * pDsClfrEntry)
{
    tDiffServInProfileActionEntry *pDsClfrInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsClfrOutProActEntry = NULL;

    /* Check for the existence of active entries for
     * InProfile Action, OutProfile Action  */
    if (pDsClfrEntry->i4DsClfrInProActionId != 0)
    {
        pDsClfrInProActEntry = DsGetInProfileActionEntry
            (pDsClfrEntry->i4DsClfrInProActionId);
        if (pDsClfrInProActEntry != NULL)
        {
            if (pDsClfrInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE)
            {
                return DS_FAILURE;
            }
        }
    }

    if (pDsClfrEntry->i4DsClfrOutProActionId != 0)
    {
        pDsClfrOutProActEntry = DsGetOutProfileActionEntry
            (pDsClfrEntry->i4DsClfrOutProActionId);

        if (pDsClfrOutProActEntry != NULL)
        {
            if (pDsClfrOutProActEntry->u1DsOutProfileActionStatus != DS_ACTIVE)
            {
                return DS_FAILURE;
            }
        }
    }

    /* Move the RowStatus to NOT_IN_SERVICE */
    pDsClfrEntry->u1DsClfrStatus = DS_NOT_IN_SERVICE;

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifyInProfileActionData                         */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Entry for In Profile Action Table.                   */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyInProfileActionData (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UINT4               u4InProActDefMask = 0x01;
    UINT4               u4InProActVal = 0;
    INT4                i4Count;

    if (pDsInProActEntry != NULL)
    {
        /* Check for all the mandatory objects */
        for (i4Count = DS_INPROACT_FLAGS - 1; i4Count >= 0; i4Count--)
        {
            u4InProActVal = pDsInProActEntry->u4DsInProfileActionFlag
                & (u4InProActDefMask << i4Count);
            if (u4InProActVal)
            {
                if (DsValiateInProActFunctPtrArr[i4Count].pValFunct
                    (pDsInProActEntry) != DS_SUCCESS)
                {
                    return DS_FAILURE;
                }
            }
        }

        pDsInProActEntry->u1DsInProfileActionStatus = DS_NOT_IN_SERVICE;
        return DS_SUCCESS;
    }
    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsCheckInProfileActionDependency                     */
/*                                                                           */
/* Description        : This function is called for checking the dependency  */
/*                      for In Profile Action Entry.                         */
/*                                                                           */
/* Input(s)           : INT4 i4DsInProActId                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsCheckInProfileActionDependency (INT4 i4DsInProActId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    /* Go through the classifier table and check with each 
     * active entry if this In Profile Action Entry is there */
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (pDsClfrEntry->i4DsClfrInProActionId == i4DsInProActId)
            {
                return DS_FAILURE;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsCheckOutProfileActionDependency                    */
/*                                                                           */
/* Description        : This function is called for checking the dependency  */
/*                      for Out Profile Action Entry.                        */
/*                                                                           */
/* Input(s)           : INT4 i4DsOutProActId                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsCheckOutProfileActionDependency (INT4 i4DsOutProActId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServClfrEntry *pDsClfrEntry = NULL;

    /* Go through the classifier table and check with each 
     * active entry if this Out Profile Action Entry is there */
    pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsClfrEntry = (tDiffServClfrEntry *) pSllNode;

        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (pDsClfrEntry->i4DsClfrOutProActionId == i4DsOutProActId)
            {
                return DS_FAILURE;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifyOutProfileActionData                        */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                      Entry for Out Profile Action Table.                  */
/*                                                                           */
/* Input(s)           : tDiffServOutProfileActionEntry **ppDsOutProActEntry  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4                DsQualifyOutProfileActionData
    (tDiffServOutProfileActionEntry * pDsOutProActEntry)
{
    UINT4               u4OutProActDefMask = 0x01;
    UINT4               u4OutProActVal = 0;
    INT4                i4Count;

    if (pDsOutProActEntry != NULL)
    {
        /* Check for all the mandatory objects */
        for (i4Count = DS_OUTPROACT_FLAGS - 1; i4Count >= 0; i4Count--)
        {
            u4OutProActVal = pDsOutProActEntry->u4DsOutProfileActionFlag
                & (u4OutProActDefMask << i4Count);
            if (u4OutProActVal)
            {
                if (DsValiateOutProActFunctPtrArr[i4Count].pValFunct == NULL)
                {
                    return DS_FAILURE;
                }

                if (DsValiateOutProActFunctPtrArr[i4Count].pValFunct
                    (pDsOutProActEntry) != DS_SUCCESS)
                {
                    return DS_FAILURE;
                }
            }
        }

        pDsOutProActEntry->u1DsOutProfileActionStatus = DS_NOT_IN_SERVICE;
        return DS_SUCCESS;
    }

    return DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsCheckMeterDependency                               */
/*                                                                           */
/* Description        : This function is called for checking the dependency  */
/*                      for Meter Entry.                                     */
/*                                                                           */
/* Input(s)           : INT4 i4DsMeterId                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gDsGlobalInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsCheckMeterDependency (INT4 i4DsMeterId)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;

    /* Go through the Out Profile Action Table and check
     * with each active entry if this Meter Entry is there */
    pSllNode = DS_SLL_FIRST (&DS_OUTPROACT_LIST);

    while (pSllNode != NULL)
    {
        pDsOutProActEntry = (tDiffServOutProfileActionEntry *) pSllNode;

        if (pDsOutProActEntry->u1DsOutProfileActionStatus == DS_ACTIVE)
        {
            if (pDsOutProActEntry->i4DsOutProfileActionMID == i4DsMeterId)
            {
                return DS_FAILURE;
            }
        }
        pSllNode = DS_SLL_NEXT (&DS_OUTPROACT_LIST, pSllNode);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsQualifyMeterData                                   */
/*                                                                           */
/* Description        : This function is called for checking the data in the */
/*                     : Entry for Meter Table.                              */
/*                                                                           */
/* Input(s)           : tDiffServMeterEntry *pDsMeterEntry                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gDsGlobalInfo                                        */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsQualifyMeterData (tDiffServMeterEntry * pDsMeterEntry)
{
    if ((pDsMeterEntry) != NULL)
    {
        /* Check for all the mandatory objects */
        if ((pDsMeterEntry->u4DsMetertokenSize != 0) &&
            (pDsMeterEntry->u4DsMeterRefreshCount != 0))
        {
            pDsMeterEntry->u1DsMeterStatus = DS_NOT_IN_SERVICE;
            return DS_SUCCESS;
        }
    }
    return DS_FAILURE;
}

/* Routines to validate the In Profile Action */

/*****************************************************************************/
/* Function Name      : DsValInProActInsertPrio                              */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert Proirity                                      */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertPrio (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActSetCOSQueue                             */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      SetCOSQueue.                                         */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActSetCOSQueue (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertTOS                               */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert TOS.                                          */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertTOS (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActCopyToCPU                               */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Copy To CPU.                                         */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActCopyToCPU (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActDoNotSwitch                             */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Do Not Switch.                                       */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActDoNotSwitch (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActSetOutPortUCast                         */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      SetOutPortUCast.                                     */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActSetOutPortUCast (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    return
        (pDsInProActEntry->u4DsInProfileActionPort) ? DS_SUCCESS : DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsValInProActCopyToMirror                            */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Copy To Mirror.                                      */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActCopyToMirror (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActIncrFFPPktCounter                       */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Increment FFP Pkt Counter.                           */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActIncrFFPPktCounter (tDiffServInProfileActionEntry *
                                pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertPrioFromTOS                       */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert Proirity From TOS.                            */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertPrioFromTOS (tDiffServInProfileActionEntry *
                                pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertTOSFromPrio                       */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert TOS from Proirity.                            */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertTOSFromPrio (tDiffServInProfileActionEntry *
                                pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActInsertDSCP                              */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Insert DSCP.                                         */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActInsertDSCP (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValInProActSetOutPortNonUCast                      */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      SetOutPortNonUCast.                                  */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActSetOutPortNonUCast (tDiffServInProfileActionEntry *
                                 pDsInProActEntry)
{
    return
        (pDsInProActEntry->u4DsInProfileActionPort) ? DS_SUCCESS : DS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DsValInProActDoSwitch                                */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      In Profile Action Entry when the flag passed is      */
/*                      Do Switch.                                           */
/*                                                                           */
/* Input(s)           : tDiffServInProfileActionEntry *pDsInProActEntry      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValInProActDoSwitch (tDiffServInProfileActionEntry * pDsInProActEntry)
{
    UNUSED_PARAM (pDsInProActEntry);
    return DS_SUCCESS;
}

/* Routines to validate the Out Profile Action */

/*****************************************************************************/
/* Function Name      : DsValOutProActCopyToCPU                              */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      Out Profile Entry, the flag passed is Copy To CPU.   */
/*                                                                           */
/* Input(s)           : tDiffServOutProfileActionEntry *pDsOutProActEntry    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValOutProActCopyToCPU (tDiffServOutProfileActionEntry * pDsOutProActEntry)
{
    UNUSED_PARAM (pDsOutProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValOutProActDoNotSwitch                            */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      Out Profile Entry, the flag passed is Do Not Switch. */
/*                                                                           */
/* Input(s)           : tDiffServOutProfileActionEntry *pDsOutProActEntry    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValOutProActDoNotSwitch (tDiffServOutProfileActionEntry * pDsOutProActEntry)
{
    UNUSED_PARAM (pDsOutProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValOutProActInsertDSCP                             */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      Out Profile Entry, the flag passed is Insert DSCP.   */
/*                                                                           */
/* Input(s)           : tDiffServOutProfileActionEntry *pDsOutProActEntry    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValOutProActInsertDSCP (tDiffServOutProfileActionEntry * pDsOutProActEntry)
{
    UNUSED_PARAM (pDsOutProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsValOutProActDoSwitch                               */
/*                                                                           */
/* Description        : This function is called for validating data in the   */
/*                      MF Classifier when the flag passed is Do Switch.     */
/*                                                                           */
/* Input(s)           : tDiffServOutProfileActionEntry *pDsOutProActEntry    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS  - On success                             */
/*                      DS_FAILURE  - On Failure                             */
/*****************************************************************************/
INT4
DsValOutProActDoSwitch (tDiffServOutProfileActionEntry * pDsOutProActEntry)
{
    UNUSED_PARAM (pDsOutProActEntry);
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsGetStartedStatus                                   */
/*                                                                           */
/* Description        : This function gets the system status whether its     */
/*                      started or shutdown.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_TRUE     - On success                             */
/*                      DS_FALSE    - On Failure                             */
/*****************************************************************************/
INT4
DsGetStartedStatus ()
{
    if (gDsGlobalInfo.DsSystemControl == DS_START)
    {
        return DS_TRUE;
    }
    else
    {
        return DS_FALSE;
    }
}

/*****************************************************************************/
/* Function Name      : DsCheckiffservPort                                   */
/*                                                                           */
/* Description        : This function checks whether the given port is a     */
/*                      Diffsrv port                                         */
/*                                                                           */
/* Input(s)           : i4TrunkPort - Trunk Port                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS     - On success                          */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckDiffservPort (INT4 i4TrunkPort)
{
    tSNMP_OCTET_STRING_TYPE DiffSrvPorts;
    UINT1               au1Ports[DS_PORT_LIST_SIZE];
    BOOL1               bResult;
    tIssL2FilterEntry  *pDsMacFilter = NULL;
    tIssL3FilterEntry  *pDsIpFilter = NULL;
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;

    if (gDsGlobalInfo.DsSystemControl == DS_SHUTDOWN)
    {
        return DS_SUCCESS;
    }

    DiffSrvPorts.i4_Length = DS_PORT_LIST_SIZE;
    DiffSrvPorts.pu1_OctetList = au1Ports;

    DFS_LOCK ();

    /* Scan through the Multifield Clfr Table */
    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsMFClfrEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;
        MEMSET (DiffSrvPorts.pu1_OctetList, 0, DS_PORT_LIST_SIZE);

        if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_MAC_FILTER)
        {
            pDsMacFilter = IssGetL2FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsMacFilter != NULL)
            {
                DS_MEMCPY (DiffSrvPorts.pu1_OctetList,
                           pDsMacFilter->IssL2FilterInPortList,
                           DiffSrvPorts.i4_Length);
            }
        }
        else if (pDsMFClfrEntry->u1DsMultiFieldClfrFilterType == DS_IP_FILTER)
        {
            pDsIpFilter = IssGetL3FilterEntry
                (pDsMFClfrEntry->u4DsMultiFieldClfrFilterId);
            if (pDsIpFilter != NULL)
            {
                DS_MEMCPY (DiffSrvPorts.pu1_OctetList,
                           pDsIpFilter->IssL3FilterInPortList,
                           DiffSrvPorts.i4_Length);
            }
        }

        OSIX_BITLIST_IS_BIT_SET (DiffSrvPorts.pu1_OctetList, i4TrunkPort,
                                 DiffSrvPorts.i4_Length, bResult);

        if (bResult == OSIX_TRUE)
        {
            DS_TRC_ARG1 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "Diffsrv port %d should not be LACP enabled \r\n",
                         i4TrunkPort);

            DFS_UNLOCK ();
            return DS_FAILURE;
        }
        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }
    DFS_UNLOCK ();
    return DS_SUCCESS;
}

/* **********   APIs to be called from other modules ********** */

/*****************************************************************************/
/* Function Name      : DsCheckMFClfrTable                                   */
/*                                                                           */
/* Description        : This function checks whether the given filter is     */
/*                      being used by any class-map. This is called from the */
/*                      ISS module when the filter is about to be deleted.   */
/*                                                                           */
/*                      NOTE: DO NOT CALL THIS ROUTINE INTERNALLY FROM QOS   */
/*                            SINCE IT TAKES THE DFS LOCK.                   */
/*                                                                           */
/* Input(s)           : i4FilterNo, i4FilterType                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckMFClfrTable (INT4 i4FilterNo, INT4 i4FilterType)
{
    tDsSllNode         *pSllNode = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFCEntry = NULL;

    DFS_LOCK ();
    /* Scan through the Multifield Clfr Table and check whether this filter 
       is being used by any class-map */
    pSllNode = DS_SLL_FIRST (&DS_MFCLFR_LIST);

    while (pSllNode != NULL)
    {
        pDsMFCEntry = (tDiffServMultiFieldClfrEntry *) pSllNode;

        if ((pDsMFCEntry->u4DsMultiFieldClfrFilterId == (UINT4) i4FilterNo) &&
            (pDsMFCEntry->u1DsMultiFieldClfrFilterType == (UINT1) i4FilterType))
        {
            DFS_UNLOCK ();
            return DS_FAILURE;
        }
        pSllNode = DS_SLL_NEXT (&DS_MFCLFR_LIST, pSllNode);
    }

    DFS_UNLOCK ();
    return DS_SUCCESS;
}

#ifdef FSAP_SEM_DEBUG
/*****************************************************************************/
/* Function Name      : DfsLock                                              */
/*                                                                           */
/* Description        : This function is used to take the DiffServer mutual  */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : pu1File - File from where the call to this function  */
/*                                is made                                    */
/*                      u4Line  - Line number in pu1File from where the      */
/*                                call to this function is made              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
DfsLockDbg (UINT1 *pu1File, UINT4 u4Line)
{
    if (OsixTakeSemDbg (SELF, DFS_MUT_EXCL_SEM_NAME,
                        OSIX_WAIT, 0, pu1File, u4Line) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "Take FAILED\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
#endif /* FSAP_SEM_DEBUG */

/*****************************************************************************/
/* Function Name      : DfsLock                                              */
/*                                                                           */
/* Description        : This function is used to take the DiffServer mutual  */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
DfsLock (VOID)
{
    if (OsixSemTake (DFS_MUT_EXCL_SEM_ID) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "Take FAILED\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DfsUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the Diff Server mutual */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
DfsUnLock (VOID)
{
    if (OsixSemGive (DFS_MUT_EXCL_SEM_ID) != OSIX_SUCCESS)
    {
        DS_TRC (ALL_FAILURE_TRC, "Diff Server Mutual Exclusion Semaphore "
                "Take FAILED\n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsIsNpEasyrider                                      */
/*                                                                           */
/* Description        : This function is used to know if chipset is Easyrider*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_TRUE or DS_FALSE                                  */
/*****************************************************************************/
INT4
DsIsNpEasyrider ()
{
    return DS_FALSE;
}

/*****************************************************************************/
/* Function Name      : DsCreatePort                                         */
/*                                                                           */
/* Description        : This function is used to make cosq status valid      */
/*                      for the given port                                   */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Interface Index                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS or DS_FAILURE                             */
/*****************************************************************************/
INT4
DsCreatePort (UINT2 u2PortIndex)
{
    UINT4               i4CoSqId = 0;

    if (u2PortIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES || u2PortIndex <= 0)
    {
        return DS_SUCCESS;
    }
    gu4PortCosqScheduleAlgo[u2PortIndex - 1].u4Status = DS_IFACE_STATUS_VALID;
    for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
    {
        /* Initializing array for CoSq Weight */
        gu4CosqWeightsBw[u2PortIndex - 1][i4CoSqId].u4Status =
            DS_IFACE_STATUS_VALID;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DsProgramSchAlgo                                     */
/*                                                                           */
/* Description        : This function scans through all the scheduling       */
/*                      algorithm  tables and programs the hardware          */
/*                      accordingly all active ports                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS or DS_FAILURE                             */
/*****************************************************************************/
INT4
DsProgramSchAlgo ()
{
    tCfaIfInfo          IfInfo;
    UINT4               u4Port = 0;
    UINT4               u4PrevPort = 0;
    UINT4               au4Weights[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4MinBw[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4MaxBw[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4BwFlag[VLAN_DEV_MAX_NUM_COSQ];
    INT4                i4CoSqId = 0;

    while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
    {
        if (u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES)
        {
            u4PrevPort = u4Port;
            continue;
        }

        if (CfaGetIfInfo (u4Port, &IfInfo) == CFA_SUCCESS)
        {
            if (IfInfo.u1IfType == CFA_ENET)
            {
                gu4PortCosqScheduleAlgo[u4Port - 1].u4Status =
                    DS_IFACE_STATUS_VALID;

                for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
                {
                    gu4CosqWeightsBw[u4Port - 1][i4CoSqId].u4Status =
                        DS_IFACE_STATUS_VALID;

                    au4Weights[i4CoSqId] =
                        gu4CosqWeightsBw[u4Port - 1][i4CoSqId].u4CosqWeights;
                    au4MinBw[i4CoSqId] =
                        gu4CosqWeightsBw[u4Port - 1][i4CoSqId].u4CosqMinBw;
                    au4MaxBw[i4CoSqId] =
                        gu4CosqWeightsBw[u4Port - 1][i4CoSqId].u4CosqMaxBw;
                    au4BwFlag[i4CoSqId] =
                        gu4CosqWeightsBw[u4Port - 1][i4CoSqId].u4CosqBwFlag;

#ifdef NPAPI_WANTED
                    /*Changing the algorithm in Hardware */
                    if (DsHwChangeCosqScheduleAlgo
                        (u4Port,
                         gu4PortCosqScheduleAlgo[u4Port - 1].u4CosqScheduleAlgo,
                         au4Weights, au4MinBw, au4MaxBw,
                         au4BwFlag) == FNP_FAILURE)
                    {
                        return DS_FAILURE;
                    }
#endif
                }
            }
        }
        u4PrevPort = u4Port;
    }

    return DS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSRegenerateVlanPriority                            */
/* Description        : This function is Called From VlanModule. It will     */
/*                      regenerate the priority value to the vlan module     */
/* Input(s)           : u4IfIndex,u2VlanId,u1InPriority.                     */
/* Output(s)          : u1RegenPri.                                          */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : u1RegenPri.                                          */
/* Called By          : Vlan Module                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT1
QoSRegenerateVlanPriority (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1InPriority)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u1InPriority);

    return DS_SUCCESS;
}
#endif /* _DSSYS_C */
