
/*$Id: dscli.c,v 1.1.1.1 2012/03/01 10:13:33 siva Exp $*/

/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : dscli.c                                          |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : FutureSoft                                       |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : Diffserv configuration                           |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for set/get objects in           |
* |                          fsissdfs.mib                                     |
* |                                                                           |
*  ---------------------------------------------------------------------------
*/
#ifndef __DSCLI_C__
#define __DSCLI_C__

#include "dsinc.h"
#include "iss.h"
#include "dscli.h"
#include "fsissdwr.h"
#include "fsissdcli.h"

#ifdef DIFFSRV_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_ds_cmd                                 */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the CFA module as     */
/*                        defined in diffsrvcmd.h                            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_ds_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[10];
    INT1                argno = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    UINT4               u4ClassMap = 0;
    UINT4               u4PolicyMap = 0;
    UINT4               u4QosVal = 0;
    UINT4               u4MinBw = 0;
    UINT4               u4PortId = 0;

    if ((u4Command != CLI_DS_SYSTEM_CONTROL) &&
        (u4Command != CLI_DS_NO_SHUT) &&
        (u4Command != CLI_DS_SHUT) && (gDsGlobalInfo.DsStatus == DS_DISABLE))
    {
        CliPrintf (CliHandle, "\r%% Quality of Service is disabled\r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* second argument is always InterfaceName/Index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array. 
     * Store 5 arguments at the max. This is because Diffserv commands do not
     * take more than 10 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == DS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, DfsLock, DfsUnLock);

    DFS_LOCK ();

    switch (u4Command)
    {
        case CLI_DS_SHUT:
            i4RetStatus = DiffservShutdown (CliHandle, DS_SHUTDOWN);
            break;

        case CLI_DS_NO_SHUT:
            i4RetStatus = DiffservShutdown (CliHandle, DS_START);
            break;

        case CLI_DS_SYSTEM_CONTROL:
            i4RetStatus =
                DiffservSystemControl (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_DS_CLASS_MAP:
            i4RetStatus =
                DiffservCreateClassMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_NO_CLASS_MAP:
            i4RetStatus =
                DiffservDestroyClassMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_POL_MAP:
            i4RetStatus =
                DiffservCreatePolicyMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_NO_POL_MAP:
            i4RetStatus =
                DiffservDestroyPolicyMap (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_CLASS_MAP_MATCH:

            /* Get the Filter Type and filter number */
            i4RetStatus =
                DiffservMatchFilter (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                     *(UINT4 *) (args[1]));
            break;

        case CLI_DS_POLCL_MAP:
            /* Get the class map value  to associate with the policy map */
            i4RetStatus = DiffservPolicyClass (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_NO_POLCL_MAP:
            i4RetStatus =
                DiffservNoPolicyClass (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_DS_POL_IN_ACTION:
            /* Get the in profile action type and corresponding value */

            i4RetStatus =
                DiffservInProfAction (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                      *(UINT4 *) (args[1]));
            break;

        case CLI_DS_NO_POL_IN_ACTION:
            i4RetStatus =
                DiffservNoInProfAction (CliHandle, CLI_PTR_TO_U4 (args[0]), 0);
            break;

        case CLI_DS_POLICE:

            /* Get the metering value , out profile action and the 
             * corresponding value */

            if ((UINT4 *) (args[2]) != NULL)
            {
                u4QosVal = *(UINT4 *) (args[2]);
            }
            i4RetStatus =
                DiffservOutProfAction (CliHandle, *(UINT4 *) (args[0]),
                                       CLI_PTR_TO_U4 (args[1]), u4QosVal);
            break;

        case CLI_DS_COSQ_ALGO:

            u4PortId = CLI_GET_IFINDEX ();
            DiffServSetCosqAlgo (CliHandle, u4PortId, *(UINT4 *) args[0]);
            break;

        case CLI_DS_COSQ_WEIGHT_BW:

            u4PortId = CLI_GET_IFINDEX ();

            if ((UINT4 *) (args[2]) != NULL)
            {
                u4MinBw = *(UINT4 *) args[2];
            }

            DiffServUpdateCosqWeightBw (CliHandle, u4PortId,
                                        *(UINT4 *) args[0], *(UINT4 *) args[1],
                                        u4MinBw);
            break;

        case CLI_SHOW_DS_POLICYMAP:
            /* Get the policy map and/or the class map for which details 
             * must be displayed */

            if (((UINT4 *) (args[0])) != NULL)
            {
                u4PolicyMap = *(UINT4 *) (args[0]);
            }
            if (((UINT4 *) (args[1])) != NULL)
            {
                u4ClassMap = *(UINT4 *) (args[1]);
            }
            i4RetStatus = DiffservShowPolicyMap (CliHandle, u4PolicyMap,
                                                 u4ClassMap);

            break;

        case CLI_SHOW_DS_CLASSMAP:
            /* Get the Class map number */

            if (((UINT4 *) (args[0])) != NULL)
            {
                u4ClassMap = *(UINT4 *) (args[0]);
            }
            i4RetStatus = DiffservShowClassMap (CliHandle, u4ClassMap);
            break;

        case CLI_SHOW_DS_COSQ_ALGO:

            u4PortId = *(UINT4 *) args[0];
            DiffservShowCoSqAlgo (CliHandle, (INT4) u4PortId);
            break;

        case CLI_SHOW_DS_COSQ_WEIGHT_BW:

            u4PortId = *(UINT4 *) args[0];
            DiffservShowCoSqWeightBw (CliHandle, (INT4) u4PortId);
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", DsCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    DFS_UNLOCK ();

    return i4RetStatus;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : DiffservShutdown                                  */
/*                                                                          */
/*     DESCRIPTION      : This function sets the Diffserv system control    */
/*                        status                                            */
/*                                                                          */
/*     INPUT            :  u4Status, CliHandle-CLI Handler                  */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/*****************************************************************************/

INT4
DiffservShutdown (tCliHandle CliHandle, UINT4 u4Status)
{

    UINT4               u4ErrCode = 0;
    INT4                i4State;

    if (nmhTestv2FsDsSystemControl (&u4ErrCode, (INT4) u4Status)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDsSystemControl ((INT4) u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (u4Status == DS_START)
    {
        i4State = DS_ENABLE;
        /* Set Module status */
        if (nmhTestv2FsDsStatus (&u4ErrCode, i4State) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsDsStatus (i4State) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        /* If "no shutdown qos" is executed diffserv will be started and
         * enabled */
    }

    return (CLI_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : DiffservSystemControl                             */
/*                                                                          */
/*     DESCRIPTION      : This function sets the Diffserv module            */
/*                        status                                            */
/*                                                                          */
/*     INPUT            :  u4Status, CliHandle-CLI Handler                  */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/*****************************************************************************/

INT4
DiffservSystemControl (tCliHandle CliHandle, UINT4 u4Status)
{

    UINT4               u4ErrCode = 0;
    INT4                i4State = 0;

    if (u4Status == CLI_ENABLE)
    {
        u4Status = DS_START;
        i4State = DS_ENABLE;
    }
    /* 'set qos disable' will only disable the hardware, but it
     * will not deallocate the memory. Once allocated, it will 
     * be present through out the life time of the system  */
    else if (u4Status == CLI_DISABLE)
    {
        i4State = DS_DISABLE;
    }

    /* Set system control status */

    if (u4Status == DS_START)
    {
        if (nmhSetFsDsSystemControl ((INT4) u4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    /* Set Module status */
    if (nmhTestv2FsDsStatus (&u4ErrCode, i4State) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDsStatus (i4State) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCreateClassMap                             */
/*                                                                           */
/*     DESCRIPTION      : This function creates the Diffserv classifier      */
/*                                                                           */
/*     INPUT            :  i4Classmap - Classifier to be created             */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservCreateClassMap (tCliHandle CliHandle, INT4 i4Classmap)
{
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    UINT4               u4ErrCode;
    INT4                i4Status = 0;

    /* If Class map already exists, do nothing */

    if (nmhGetFsDiffServMultiFieldClfrStatus ((INT4) i4Classmap, &i4Status) !=
        SNMP_SUCCESS)

    {
        /* Class Map does not exist and must be created */

        if (nmhTestv2FsDiffServMultiFieldClfrStatus
            (&u4ErrCode, i4Classmap, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsDiffServMultiFieldClfrStatus
            (i4Classmap, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* The execution of this command must return the Class map configuration 
     * mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_CLASSMAP_MODE, i4Classmap);
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservDestroyClassMap                            */
/*                                                                           */
/*     DESCRIPTION      : This function destroys the Diffserv MF classifier  */
/*                                                                           */
/*     INPUT            :  i4Classmap - Classifier to be deleted             */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservDestroyClassMap (tCliHandle CliHandle, INT4 i4ClassMapId)
{
    INT4                i4FsDiffServClfrId;
    INT4                i4FsDiffServClfrMFClfrId;
    INT4                i4PrevFsDiffServClfrId = 0;
    INT4                i4Status;

    /* Check if the class map exists */
    if (nmhGetFsDiffServMultiFieldClfrStatus ((INT4) i4ClassMapId, &i4Status) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%  Invalid Class Map \r\n");
        return (CLI_FAILURE);
    }

    /* Class map exists. We now need to check if there is any policy map
     * dependency for this class map */

    /* Scan all the Classifier entries */

    if (nmhGetFirstIndexFsDiffServClfrTable (&i4FsDiffServClfrId) !=
        SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsDiffServClfrMFClfrId (i4FsDiffServClfrId,
                                              &i4FsDiffServClfrMFClfrId) !=
                SNMP_FAILURE)
            {

                if (i4FsDiffServClfrMFClfrId == i4ClassMapId)
                {
                    /*Policy map <-> class map match found , hence this
                     * class map cannot be deleted */

                    CliPrintf (CliHandle, "\r%% Class-map %d is being used  by "
                               "a policy-map and cannot be modified\r\n",
                               i4ClassMapId);
                    return (CLI_FAILURE);
                }

                i4PrevFsDiffServClfrId = i4FsDiffServClfrId;
            }
        }
        while (nmhGetNextIndexFsDiffServClfrTable (i4PrevFsDiffServClfrId,
                                                   &i4FsDiffServClfrId) !=
               SNMP_FAILURE);

    }

    /* No Policy map  has been created  or no policy map is using this
     * classmap and only classmap exists , then destroy the MFClfr  */

    nmhSetFsDiffServMultiFieldClfrStatus (i4ClassMapId, DS_DESTROY);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCreatePolicyMap                            */
/*                                                                           */
/*     DESCRIPTION      : This function creates the Diffserv classifier      */
/*                                                                           */
/*     INPUT            :  i4PolIndex - Policy map to be created             */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservCreatePolicyMap (tCliHandle CliHandle, INT4 i4PolIndex)
{
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    INT4                i4Status;
    UINT4               u4ErrCode;

    /* If Policy map exists ,do nothing */

    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)

    {
        /* Policy map does not exist and it must be created */

        if (nmhTestv2FsDiffServClfrStatus
            (&u4ErrCode, i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFsDiffServClfrStatus
            (i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* The execution of this command must return the policy map 
     * configuration mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_POLICYMAP_MODE, i4PolIndex);
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservDestroyPolicyMap                           */
/*                                                                           */
/*     DESCRIPTION      : This function destroys the Diffserv classifier     */
/*                                                                           */
/*     INPUT            :  i4PolicymapId -  to be deleted                    */
/*                         CliHandle - CLI Handler                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservDestroyPolicyMap (tCliHandle CliHandle, INT4 i4PolicyMapId)
{
    INT4                i4RowStatus;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pDiffServClfrEntry = DsGetClfrEntry (i4PolicyMapId);

    if (pDiffServClfrEntry == NULL)

    {
        CliPrintf (CliHandle, "\r%% Invalid Policy map\r\n");
        return (CLI_FAILURE);
    }

    /* Set the Policy map status to NOT_IN_SERVICE and then destroy
     * each entry for in-profile action, out-profile action, no-match action
     * before finally destroying the Policy map
     */
    nmhGetFsDiffServClfrStatus (pDiffServClfrEntry->i4DsClfrId, &i4RowStatus);

    if (i4RowStatus == DS_ACTIVE)
    {
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, DS_NOT_IN_SERVICE) == DS_FAILURE)

            /* This operation is required since there is a dependency between
             * the Classifier entry and the Inprofile/ outprofile/
             * no-match action entry */
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (pDiffServClfrEntry->i4DsClfrInProActionId != 0)

    {
        if (nmhSetFsDiffServInProfileActionStatus
            (pDiffServClfrEntry->i4DsClfrInProActionId,
             DS_DESTROY) == DS_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (pDiffServClfrEntry->i4DsClfrOutProActionId != 0)

    {
        if (nmhSetFsDiffServOutProfileActionStatus
            (pDiffServClfrEntry->i4DsClfrOutProActionId,
             DS_DESTROY) == DS_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetFsDiffServMeterStatus
            (pDiffServClfrEntry->i4DsClfrOutProActionId,
             DS_DESTROY) == DS_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetFsDiffServClfrStatus
        (pDiffServClfrEntry->i4DsClfrId, DS_DESTROY) == DS_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservMatchFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function creates the MF clfr                  */
/*                        with the values from Ip/Mac filter                 */
/*                                                                           */
/*     INPUT            : u4FilterType - IP/MAC ACL                          */
/*                        i4FilterNo - IP/MAC Filter number                  */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservMatchFilter (tCliHandle CliHandle, UINT4 u4FilterType, INT4 i4FilterNo)
{
    tIssL2FilterEntry  *pDsL2FilterEntry;
    tIssL3FilterEntry  *pDsL3FilterEntry;
    INT4                i4ClMapIndex = 0;
    INT4                i4MFClfrId;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4PrevFsDiffServClfrId = 0;
    INT4                i4FsDiffServClfrId;
    INT4                i4FsDiffServClfrMFClfrId;

    /* Get the current class map identifier */
    i4MFClfrId = CLI_GET_CLASSMAP_ID ();

    nmhGetFsDiffServMultiFieldClfrStatus (i4MFClfrId, &i4RowStatus);

    if (i4RowStatus != DS_NOT_READY)
    {
        /* This classifier already exists and is going to be modified,
         * by destroying the existing MFclfr. If there is an associated 
         * policymap then modification must not be allowed
         */

        /* Scan all the Classifier entries */
        if (nmhGetFirstIndexFsDiffServClfrTable (&i4FsDiffServClfrId) !=
            SNMP_FAILURE)
        {
            do
            {
                if (nmhGetFsDiffServClfrMFClfrId (i4FsDiffServClfrId,
                                                  &i4FsDiffServClfrMFClfrId) !=
                    SNMP_FAILURE)
                {

                    if (i4FsDiffServClfrMFClfrId == i4MFClfrId)
                    {
                        /*Policy map <-> class map match found , hence this
                         * class map cannot be deleted */
                        CliPrintf (CliHandle,
                                   "\r%% Class-map %d is being used by a"
                                   " policy map\r\n", i4MFClfrId);

                        return (CLI_FAILURE);
                    }

                    i4PrevFsDiffServClfrId = i4FsDiffServClfrId;
                }
            }
            while (nmhGetNextIndexFsDiffServClfrTable (i4PrevFsDiffServClfrId,
                                                       &i4FsDiffServClfrId) !=
                   SNMP_FAILURE);

        }

        /* Policy maps have not been created or no policy map
         * is associated with this Class Map 
         *  Delete the MFClfr  */

        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, DS_DESTROY);

        /* Creating a new class map now */
        if (nmhTestv2FsDiffServMultiFieldClfrStatus
            (&u4ErrCode, i4MFClfrId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsDiffServMultiFieldClfrStatus
            (i4MFClfrId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* If Filter is a MAC filter, set the corresponding parameters */

    if (u4FilterType == DS_MAC_FILTER)
    {
        /* Check if this filter exists */

        if ((pDsL2FilterEntry = IssGetL2FilterEntry (i4FilterNo)) == NULL)

        {
            CliPrintf (CliHandle, "\r%% Invalid MAC filter\r\n");
            return (CLI_FAILURE);
        }

        /* Get the current class map identifier */
        i4ClMapIndex = CLI_GET_CLASSMAP_ID ();

        if (DsCreateMFClfrFromMacFilter
            (CliHandle, i4FilterNo, i4ClMapIndex) == DS_FAILURE)

        {
            return (CLI_FAILURE);
        }
    }

    /* If Filter is a IP filter, set the corresponsing parameters */
    else if (u4FilterType == DS_IP_FILTER)
    {

        /* Check if this filter exists */
        if ((pDsL3FilterEntry = IssGetL3FilterEntry (i4FilterNo)) == NULL)

        {
            CliPrintf (CliHandle, "\r %% Invalid IP filter\r\n");
            return (CLI_FAILURE);
        }
        if (pDsL3FilterEntry->IssL3FilterDirection != DS_INGRESS)
        {
            CliPrintf (CliHandle, "\r %% Filter Direction should be IN\r\n");
            return (CLI_FAILURE);
        }
        /* Get the current class map identifier */
        i4ClMapIndex = CLI_GET_CLASSMAP_ID ();

        if (DsCreateMFClfrFromIpFilter
            (CliHandle, i4FilterNo, i4ClMapIndex) == DS_FAILURE)

        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetClassMapPrompt                           */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetClassMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{

    UINT4               u4Len;
    INT4                i4ClassMapIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-cmap" as the prompt 
     * for the mode CLASSMAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-cmap");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_CLASSMAP_MODE);
    if (STRNCMP (pi1ModeName, CLI_CLASSMAP_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;
    u4Len = STRLEN (pi1ModeName);

    i4ClassMapIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_CLASSMAP_ID (i4ClassMapIndex);

    STRCPY (pi1DispStr, "(config-cmap)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetPolicyMapPrompt                          */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetPolicyMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4PolicyMapIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-pmap" as the prompt 
     * for the mode POLICYMAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-pmap");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_POLICYMAP_MODE);
    if (STRNCMP (pi1ModeName, CLI_POLICYMAP_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4PolicyMapIndex = (atoi) ((char *) pi1ModeName);

    CLI_SET_POLICYMAP_ID (i4PolicyMapIndex);

    STRCPY (pi1DispStr, "(config-pmap)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffsrvGetPolicyClassMapPrompt                    */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Policy Class map prompt in         */
/*                        pi1DispStr if valid                                */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
DiffsrvGetPolicyClassMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4PolClMapIndex;

    if (!pi1DispStr)
    {
        return DS_FALSE;
    }

    /* NULL is passed to return "config-pmap-c" as the prompt 
     * for the mode POLICY CLASS MAP */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-pmap-c");
        return DS_TRUE;
    }

    u4Len = STRLEN (CLI_POLICYCLASS_MODE);
    if (STRNCMP (pi1ModeName, CLI_POLICYCLASS_MODE, u4Len) != 0)
    {
        return DS_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4PolClMapIndex = (atoi) ((char *) pi1ModeName);

    CLI_SET_POCL_MAP_ID (i4PolClMapIndex);

    STRCPY (pi1DispStr, "(config-pmap-c)#");
    return DS_TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateMFClfrFromMacFilter                        */
/*                                                                           */
/*     DESCRIPTION      : This function creates a multifield classifier      */
/*                        entry based on the MAC filter associated.          */
/*                                                                           */
/*     INPUT            : i4MacFilterNo - associated MAC filter number       */
/*                        i4MFClfrId    - multifield classifier ID for       */
/*                                        this entry                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateMFClfrFromMacFilter (tCliHandle CliHandle, INT4 i4MacFilterNo,
                             INT4 i4MFClfrId)
{
    tIssL2FilterEntry  *pDsMacFilter = NULL;
    INT4                i4RowStatus = DS_DESTROY;
    UINT4               u4ErrCode = 0;
    tMacAddr            zeroAddr;

    /* Please note : only creation of a new multifield classifier will be
     * done in this routine. It will not update an existing one.
     */

    /* Get the details of the associated MAC filter */
    pDsMacFilter = IssGetL2FilterEntry (i4MacFilterNo);

    DS_MEMSET (zeroAddr, 0, DS_ETHERNET_ADDR_SIZE);

    if (nmhTestv2FsDiffServMultiFieldClfrFilterId (&u4ErrCode, i4MFClfrId,
                                                   i4MacFilterNo) ==
        SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CliPrintf (CliHandle, "\r%% Invalid Filter Id\r\n");
        return DS_FAILURE;
    }

    if (nmhTestv2FsDiffServMultiFieldClfrFilterType (&u4ErrCode, i4MFClfrId,
                                                     DS_MAC_FILTER) ==
        SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CliPrintf (CliHandle, "\r%% Invalid Filter Type\r\n");
        return DS_FAILURE;
    }

    /* Set the Mac filter parameters */
    /*Set Filter Id */
    if (nmhSetFsDiffServMultiFieldClfrFilterId (i4MFClfrId, i4MacFilterNo)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /*Set Filter Type */
    if (nmhSetFsDiffServMultiFieldClfrFilterType (i4MFClfrId, DS_MAC_FILTER)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /* Once all the filter parameters have been configured, set the row status
     * to ACTIVE */

    if (nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateMFClfrFromIpFilter                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates a multifield classifier      */
/*                        entry based on the IP filter associated.           */
/*                                                                           */
/*     INPUT            : i4IpFilterNo - associated IP filter number         */
/*                        i4MFClfrId   - multifield classifier ID for        */
/*                                       this entry                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateMFClfrFromIpFilter (tCliHandle CliHandle, INT4 i4IpFilterNo,
                            INT4 i4MFClfrId)
{
    tIssL3FilterEntry  *pDsIpFilter = NULL;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    /* Please note : only creation of a new multifield classifier will be
     * done in this routine. It will not update an existing one.
     */

    /* Get the details of the associated IP filter */

    pDsIpFilter = IssGetL3FilterEntry (i4IpFilterNo);
    if (pDsIpFilter == NULL)

    {
        CliPrintf (CliHandle, "\r%% Invalid IP filter\r\n");
        return DS_FAILURE;
    }

    i4RowStatus = DS_DESTROY;

    if (nmhTestv2FsDiffServMultiFieldClfrFilterId (&u4ErrCode, i4MFClfrId,
                                                   i4IpFilterNo) ==
        SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CliPrintf (CliHandle, "\r%% Invalid Filter Id\r\n");
        return DS_FAILURE;
    }

    if (nmhTestv2FsDiffServMultiFieldClfrFilterType (&u4ErrCode, i4MFClfrId,
                                                     DS_IP_FILTER) ==
        SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CliPrintf (CliHandle, "\r%% Invalid Filter Type\r\n");
        return DS_FAILURE;
    }

    /*Set Filter Id */
    if (nmhSetFsDiffServMultiFieldClfrFilterId (i4MFClfrId, i4IpFilterNo)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /*Set Filter Type */
    if (nmhSetFsDiffServMultiFieldClfrFilterType (i4MFClfrId, DS_IP_FILTER)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (DS_FAILURE);
    }

    /* Once all the filter parameters have been configured, set the row status
     * to ACTIVE */

    if (nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMultiFieldClfrStatus (i4MFClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservPolicyClass                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Policy-class map relation   */
/*                                                                           */
/*     INPUT            :  i4ClassMap Id- Class map                          */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservPolicyClass (tCliHandle CliHandle, INT4 i4ClassMapId)
{
    INT4                i4PolIndex;
    tDsClfr             DsClfr;
    INT4                i4Status;
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    UINT4               u4ErrCode = 0;
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServClfrEntry *pClfrList = NULL;
    tDsSllNode         *pSllNode = NULL;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POLICYMAP_ID ();

    /* Check if class map exists */

    if (nmhGetFsDiffServMultiFieldClfrStatus
        (i4ClassMapId, &i4Status) != SNMP_SUCCESS)

    {
        CliPrintf (CliHandle, "\r%% Invalid Class map \r\n");
        return (CLI_FAILURE);
    }

    /* Check if the class-map already allocated to another policy-map.
     * We can't allocate the same class-map to different policy-map
     */
    pDsClfrEntry = DsGetClfrEntry (i4PolIndex);
    if (pDsClfrEntry != NULL)
    {

        pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);
        while (pSllNode != NULL)
        {
            pClfrList = (tDiffServClfrEntry *) pSllNode;
            if ((i4ClassMapId == pClfrList->i4DsClfrMFClfrId)
                && (pDsClfrEntry != pClfrList))
            {
                CliPrintf (CliHandle,
                           "\r%% Class map already allocated to another policy-map\r\n");
                return (CLI_FAILURE);
            }
            else if ((i4ClassMapId == pClfrList->i4DsClfrMFClfrId)
                     && (pDsClfrEntry == pClfrList))
            {
                /* If there is no change in class-map id, then do nothing */
                /* The execution of this command must return the policy class
                 * map configuration mode */

                SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_POLICYCLASS_MODE,
                         i4PolIndex);
                CliChangePath ((CHR1 *) au1IfName);
                return (CLI_SUCCESS);
            }

            pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
        }

        nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status);

        if ((i4Status == DS_NOT_READY) || (i4Status == DS_ACTIVE))
        {
            /* Policy-map exists and has been previously associated to a class map */
            /* First set the Classifier status to NOT_IN_SERVICE 
             * Destroy in-profile/out-profile/no-match action and Data path 
             * Now, Destroy the Classifier, and create it again. 
             * Remap  the policy map the the specified class map
             */

            if (i4Status == DS_ACTIVE)
            {
                if (nmhSetFsDiffServClfrStatus
                    (pDsClfrEntry->i4DsClfrId, DS_NOT_IN_SERVICE) == DS_FAILURE)

                    /* This operation is required since there is a dependency
                     * between the Classifier entry and the Inprofile/
                     * outprofile/ no-match action entry */
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (pDsClfrEntry->i4DsClfrInProActionId != 0)
            {
                if (nmhSetFsDiffServInProfileActionStatus
                    (pDsClfrEntry->i4DsClfrInProActionId,
                     DS_DESTROY) == DS_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (pDsClfrEntry->i4DsClfrOutProActionId != 0)
            {
                if (nmhSetFsDiffServOutProfileActionStatus
                    (pDsClfrEntry->i4DsClfrOutProActionId,
                     DS_DESTROY) == DS_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (nmhSetFsDiffServMeterStatus
                    (pDsClfrEntry->i4DsClfrOutProActionId,
                     DS_DESTROY) == DS_FAILURE)

                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }

            if (nmhSetFsDiffServClfrStatus
                (pDsClfrEntry->i4DsClfrId, DS_DESTROY) == DS_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhTestv2FsDiffServClfrStatus
                (&u4ErrCode, i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsDiffServClfrStatus
                (i4PolIndex, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle,
                       "\r\nExisting Policy-map configurations have been deleted. Please apply the policy-map to make it active.\r\n");
        }
    }
    /* set the already existing MF clfr index */
    DsClfr.i4DsMFClfrId = i4ClassMapId;
    DsClfr.i4DsClfrId = i4PolIndex;
    DsClfr.i4DsInProActId = 0;
    DsClfr.i4DsOutProActId = 0;

    /* Create the classifier 
     */
    if (DsCreateClfr (CliHandle, &DsClfr, DS_TRUE) == DS_FAILURE)

    {
        return (CLI_FAILURE);
    }
    /* The execution of this command must return the Policy map configuration 
     * mode */
    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_POLICYCLASS_MODE, i4PolIndex);
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateClfr                                       */
/*                                                                           */
/*     DESCRIPTION      : This function creates a classifier entry.          */
/*                                                                           */
/*     INPUT            : DsClfr - classifier details                        */
/*                        u1NewFlag - flag for status of classifier          */
/*                                    TRUE - new, FALSE - existing           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateClfr (tCliHandle CliHandle, tDsClfr * pDsClfr, BOOLEAN u1NewFlag)
{
    INT4                i4RowStatus = 0;
    INT4                i4ClfrStatus = 0;
    UINT4               u4ErrCode = 0;

    /* This routine allows both creation and updation of a classifier entry.
     */
    if (nmhGetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, &i4ClfrStatus) ==
        SNMP_FAILURE)

    {
        return (CLI_FAILURE);

    }
    i4RowStatus = DS_DESTROY;

    /* If MFClfrId/ In-profile action Id/ Out-profile Action Id  are 
     * set to a value other than zero then it will be set in the Classifier 
     * Entry */

    if (pDsClfr->i4DsMFClfrId != 0)

    {
        if (nmhTestv2FsDiffServClfrMFClfrId
            (&u4ErrCode, pDsClfr->i4DsClfrId,
             pDsClfr->i4DsMFClfrId) == SNMP_FAILURE)

        {
            nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
            return DS_FAILURE;
        }
    }
    if (pDsClfr->i4DsInProActId != 0)

    {
        if (nmhTestv2FsDiffServClfrInProActionId
            (&u4ErrCode, pDsClfr->i4DsClfrId,
             pDsClfr->i4DsInProActId) == SNMP_FAILURE)

        {
            nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
            return DS_FAILURE;
        }
    }
    if (pDsClfr->i4DsOutProActId != 0)

    {
        if (nmhTestv2FsDiffServClfrOutProActionId
            (&u4ErrCode, pDsClfr->i4DsClfrId,
             pDsClfr->i4DsOutProActId) == SNMP_FAILURE)

        {
            nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
            return DS_FAILURE;
        }
    }

    if (u1NewFlag != DS_TRUE)
    {
        /* An existing entry is getting updated. So set the classifier row status 
         * to NOT_IN_SERVICE. After setting the other parameters change the 
         * row status of the classifier entry to ACTIVE.
         */

        if (i4ClfrStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDsClfr->i4DsClfrId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)

            {
                nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
        }
    }
    if (pDsClfr->i4DsMFClfrId != 0)

    {
        if (nmhSetFsDiffServClfrMFClfrId
            (pDsClfr->i4DsClfrId, pDsClfr->i4DsMFClfrId) == SNMP_FAILURE)

        {
            nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    if (pDsClfr->i4DsInProActId != 0)

    {
        if (nmhSetFsDiffServClfrInProActionId
            (pDsClfr->i4DsClfrId, pDsClfr->i4DsInProActId) == SNMP_FAILURE)

        {
            nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    if (pDsClfr->i4DsOutProActId != 0)

    {
        if (nmhSetFsDiffServClfrOutProActionId
            (pDsClfr->i4DsClfrId, pDsClfr->i4DsOutProActId) == SNMP_FAILURE)

        {
            nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    if (nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServClfrStatus (pDsClfr->i4DsClfrId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservInProfAction                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures the policy map in-profile */
/*                        action                                             */
/*                                                                           */
/*     INPUT            : u4InProfType- Inprofile action                     */
/*                        u4Value - DSCP /IP Precedence/COS value as the case*/
/*                        may be                                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservInProfAction (tCliHandle CliHandle, UINT4 u4InProfType, UINT4 u4Value)
{

    tDsAction           DsAction;
    tDsClfr             DsClfr;
    INT4                i4Status;
    INT4                i4PolIndex;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map already exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)

    {
        return (CLI_FAILURE);
    }

    DsAction.i4ActionId = i4PolIndex;

    /* Get the In-profile action from the user input type and value */
    switch (u4InProfType)

    {
        case DS_DSCP:
            DsAction.u4Dscp = u4Value;
            DsAction.u4Precedence = 0;
            DsAction.u4Cos = 0;
            DsAction.i4Action = DS_ACT_DSCP;
            break;
        case DS_PREC:
            DsAction.u4Precedence = u4Value;
            DsAction.u4Dscp = 0;
            DsAction.u4Cos = 0;
            DsAction.i4Action = DS_ACT_PREC;
            break;
        case DS_COS:
            DsAction.u4Cos = u4Value;
            DsAction.u4Dscp = 0;
            DsAction.u4Precedence = 0;
            DsAction.i4Action = DS_ACT_COS;
            break;
    }

    /* Create the in profile action entry  */
    if (DsCreateInProfAction (CliHandle, &DsAction) == DS_FAILURE)

    {
        return (CLI_FAILURE);
    }
    DsClfr.i4DsInProActId = i4PolIndex;
    DsClfr.i4DsClfrId = i4PolIndex;
    DsClfr.i4DsMFClfrId = 0;
    DsClfr.i4DsOutProActId = 0;

    /* Update the classifier with the in profile action
     * just created. When unsuccessful delete the action entry just created.
     */
    if (DsCreateClfr (CliHandle, &DsClfr, DS_FALSE) == DS_FAILURE)

    {
        if (DsClfr.i4DsInProActId != 0)

        {
            nmhSetFsDiffServInProfileActionStatus (DsClfr.i4DsInProActId,
                                                   DS_DESTROY);
        }
        return (CLI_FAILURE);

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateInProfAction                               */
/*                                                                           */
/*     DESCRIPTION      : This function creates a in profile action entry.   */
/*                                                                           */
/*     INPUT            : DsAction  - action details                         */
/*                                                                           */
/*     OUTPUT           : ppu1ErrBuf - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateInProfAction (tCliHandle CliHandle, tDsAction * pDsAction)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4Update = 0;

    /* Check if In-profile entry exists for this policy map */

    if (nmhGetFsDiffServInProfileActionStatus
        (pDsAction->i4ActionId, &i4RowStatus) == SNMP_FAILURE)

    {
        if (nmhTestv2FsDiffServInProfileActionStatus
            (&u4ErrCode, pDsAction->i4ActionId,
             DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CliPrintf (CliHandle, "\r%% Invalid in profile action\r\n");
            return DS_FAILURE;
        }

        /* Create the inprofile entry */

        if (nmhSetFsDiffServInProfileActionStatus
            (pDsAction->i4ActionId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }

    else

    {
        /* Inprofile entry previously exists. First set the Classifier status 
         * to NOT_IN_SERVICE. Then set the In profile entry to NOT_IN_SERVICE.
         * NOw set the action specified by the user and update the classifier.
         * Finally reset the Classifier status to DS_ACTIVE */
        nmhGetFsDiffServClfrStatus (pDsAction->i4ActionId, &i4RowStatus);
        if (i4RowStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDsAction->i4ActionId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)
                /* This operation is required since there is a dependency between
                 * the Classifier entry and the Inprofile action entry */

            {
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            u4Update = 1;
        }
        if (nmhSetFsDiffServInProfileActionStatus
            (pDsAction->i4ActionId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
        /* This flag is used to indicate whether this entry is new or 
         * previously existed and is now being updated */

    }
    i4RowStatus = DS_DESTROY;
    /* Test In profile action value */
    /* If any test/set fails destroy the InProfileAction Entry */
    switch (pDsAction->i4Action)

    {
        case DS_ACT_DSCP:
            if (nmhTestv2FsDiffServInProfileActionDscp
                (&u4ErrCode, pDsAction->i4ActionId,
                 pDsAction->u4Dscp) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                return DS_FAILURE;
            }
            break;
        case DS_ACT_PREC:
            if (nmhTestv2FsDiffServInProfileActionIpTOS
                (&u4ErrCode, pDsAction->i4ActionId,
                 pDsAction->u4Precedence) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                return DS_FAILURE;
            }
            break;
        case DS_ACT_COS:
            if (nmhTestv2FsDiffServInProfileActionNewPrio
                (&u4ErrCode, pDsAction->i4ActionId,
                 pDsAction->u4Cos) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                return DS_FAILURE;
            }
            break;
    }
    /* Set In profile action value */
    switch (pDsAction->i4Action)

    {
        case DS_ACT_DSCP:
            if (nmhSetFsDiffServInProfileActionDscp
                (pDsAction->i4ActionId, pDsAction->u4Dscp) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            break;
        case DS_ACT_PREC:
            if (nmhSetFsDiffServInProfileActionIpTOS
                (pDsAction->i4ActionId,
                 pDsAction->u4Precedence) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            break;
        case DS_ACT_COS:
            if (nmhSetFsDiffServInProfileActionNewPrio
                (pDsAction->i4ActionId, pDsAction->u4Cos) == SNMP_FAILURE)

            {
                nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                                       i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            break;
    }

    /* Set In profile action entry to active */
    if (nmhSetFsDiffServInProfileActionStatus
        (pDsAction->i4ActionId, DS_ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDiffServInProfileActionStatus (pDsAction->i4ActionId,
                                               i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    if (u4Update)
    {
        /* This flag indicates that this entry previously existed and 
         * is now being updated */
        /* Set the rowstatus of the Classifier to Active */

        if (nmhSetFsDiffServClfrStatus (pDsAction->i4ActionId, DS_ACTIVE) ==
            SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservNoInProfAction                             */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the in-profile action        */
/*                                                                           */
/*     INPUT            : u4InProfType- Inprofile action                     */
/*                        u4Value - DSCP /IP Precedence/COS value as the case*/
/*                        may be                                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     Return           : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservNoInProfAction (tCliHandle CliHandle, UINT4 u4InProfType, UINT4 u4Value)
{
    INT4                i4Status;
    INT4                i4PolIndex;
    UINT4               u4ActionFlag = 0;

    /* The value given by user is not used to delete In-profile action
     * entry.Only type of action such as cos,dscp or precedence is used
     * to delete the entry.*/
    UNUSED_PARAM (u4Value);

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map already exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r% Invalid policy-map\r\n");
        return CLI_FAILURE;
    }

    /* Get the In-profile action from the user input type and value */
    switch (u4InProfType)
    {
        case DS_DSCP:
            u4ActionFlag = FS_DS_ACTN_INSERT_DSCP;
            break;
        case DS_PREC:
            u4ActionFlag = FS_DS_ACTN_INSERT_TOSP;
            break;
        case DS_COS:
            u4ActionFlag = FS_DS_ACTN_INSERT_PRIO;
            break;
    }

    /* Delete the in-profile action */
    if (DsDeleteInProfAction (CliHandle, i4PolIndex, u4ActionFlag) ==
        (INT4) CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsDeleteInProfAction                               */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the policy map in-profile    */
/*                        action                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI handle                             */
/*                        i4PolicyIndex - policy-map index                   */
/*                        u4Flag - flag refers to which in-profile action    */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DsDeleteInProfAction (tCliHandle CliHandle, INT4 i4PolicyIndex, UINT4 u4Flag)
{
    INT4                i4RowStatus;
    UINT4               u4Value = 0;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    pDiffServClfrEntry = DsGetClfrEntry (i4PolicyIndex);

    if (pDiffServClfrEntry == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Policy map\r\n");
        return CLI_FAILURE;
    }

    /* Get the row status for the policy-map */
    nmhGetFsDiffServClfrStatus (pDiffServClfrEntry->i4DsClfrId, &i4RowStatus);

    /* Check if policy-map has in-profile action associated with it */
    if (pDiffServClfrEntry->i4DsClfrInProActionId != 0)
    {
        /* Check if in-profile action existing is same as given by user
         * If same delete otherwise simply return success */

        if (nmhGetFsDiffServInProfileActionFlag
            (pDiffServClfrEntry->i4DsClfrInProActionId,
             &u4Value) == SNMP_SUCCESS)
        {
            if (u4Flag != u4Value)
            {
                return CLI_SUCCESS;
            }
            else
            {
                /* This operation is required since there is a dependency
                 * between the Classifier entry and the Inprofile action entry*/
                if (i4RowStatus == DS_ACTIVE)
                {
                    if (nmhSetFsDiffServClfrStatus
                        (pDiffServClfrEntry->i4DsClfrId,
                         DS_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }

                /* User given in-profile action and existing one are same.
                 * So delete the in-profile action entry */

                if (nmhSetFsDiffServInProfileActionStatus
                    (pDiffServClfrEntry->i4DsClfrInProActionId,
                     DS_DESTROY) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                pDiffServClfrEntry->i4DsClfrInProActionId = 0;
            }
        }
    }
    else
    {
        return CLI_SUCCESS;
    }

    /* Set the policy-map status to old status if it is ACTIVE after deleting 
     * in-profile action entry. This will add the entry in the hardware also. */

    if (i4RowStatus == DS_ACTIVE)
    {
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, i4RowStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservOutProfAction                              */
/*                                                                           */
/*     DESCRIPTION      : This function configures the policy map out-profile*/
/*                        action                                             */
/*                                                                           */
/*     INPUT            : pu1DsInput                                         */
/*                                                                           */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservOutProfAction (tCliHandle CliHandle, UINT4 u4RefreshRate,
                       UINT4 u4Action, UINT4 u4Value)
{
    tDsAction           DsAction;
    tDsClfr             DsClfr;
    INT4                i4Status;
    INT4                i4PolIndex;
    tDsMeter            DsMeter;
    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POCL_MAP_ID ();

    /* check if policy-map already exists */
    if (nmhGetFsDiffServClfrStatus (i4PolIndex, &i4Status) == SNMP_FAILURE)

    {
        return (CLI_FAILURE);
    }

    pDiffServClfrEntry = DsGetClfrEntry (i4PolIndex);

    DsMeter.i4MeterId = i4PolIndex;
    DsMeter.u4Refresh = u4RefreshRate;

    /* Create a metering entry */

    if (DsCreateMeterEntry (CliHandle, &DsMeter) == DS_FAILURE)

    {
        nmhSetFsDiffServMeterStatus (i4PolIndex, DS_DESTROY);
        return (CLI_FAILURE);
    }

    /* Create the out profile action entry */
    DsAction.i4ActionId = i4PolIndex;
    switch (u4Action)

    {
        case DS_DROP:
            DsAction.u4Dscp = 0;
            DsAction.u4Precedence = 0;
            DsAction.u4Cos = 0;
            DsAction.i4Action = DS_ACT_DROP;
            break;
        case DS_DSCP:
            DsAction.u4Dscp = u4Value;
            DsAction.u4Precedence = 0;
            DsAction.u4Cos = 0;
            DsAction.i4Action = DS_ACT_DSCP;
            break;
    }
    if (DsCreateOutProfAction (CliHandle, &DsAction, i4PolIndex) == DS_FAILURE)

    {
        return (CLI_FAILURE);
    }
    DsClfr.i4DsOutProActId = i4PolIndex;
    DsClfr.i4DsClfrId = i4PolIndex;
    DsClfr.i4DsMFClfrId = 0;
    DsClfr.i4DsInProActId = 0;

    /* Update the classifier with the in profile action
     * just created. When unsuccessful delete the action entry just created.
     */
    if (DsCreateClfr (CliHandle, &DsClfr, DS_FALSE) == DS_FAILURE)

    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateOutProfAction                              */
/*                                                                           */
/*     DESCRIPTION      : This function creates a out profile action entry.  */
/*                                                                           */
/*     INPUT            : DsAction  - action details                         */
/*                                                                           */
/*     OUTPUT           : ppu1ErrBuf - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateOutProfAction (tCliHandle CliHandle, tDsAction * pDsAction,
                       INT4 i4MeterId)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4Update = 0;

    /* Please note : only creation of a new out profile action will be
     * done in this routine. It will not update an existing one.
     */
    if (nmhGetFsDiffServOutProfileActionStatus
        (pDsAction->i4ActionId, &i4RowStatus) == SNMP_FAILURE)

    {
        if (nmhTestv2FsDiffServOutProfileActionStatus
            (&u4ErrCode, pDsAction->i4ActionId,
             DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            return DS_FAILURE;
        }
        /* Create the outprofile entry */
        if (nmhSetFsDiffServOutProfileActionStatus
            (pDsAction->i4ActionId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }

    else

    {
        /* If Out profile action previously exists, put the classifer entry to 
         * NOT_IN_SERVICE, set all the new parameters and set the classifer 
         * entry row status to active again
         */
        /* This operation is required since there is a dependency between
         * the Classifier entry and the Outprofile action entry */
        nmhGetFsDiffServClfrStatus (pDsAction->i4ActionId, &i4RowStatus);
        if (i4RowStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDsAction->i4ActionId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);
                return DS_FAILURE;
            }
            u4Update = 1;
        }
        if (nmhSetFsDiffServOutProfileActionStatus
            (pDsAction->i4ActionId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
        /* This flag indicated whether this  entry previously existed 
         * or is newly created */
    }

    i4RowStatus = DS_DESTROY;

    /* Test the Metering value */
    /* If any test/set fails destroy the OutProfile Action Entry - identifier
     * and corresponding QOS values */

    if (nmhTestv2FsDiffServOutProfileActionMID
        (&u4ErrCode, pDsAction->i4ActionId, i4MeterId) == SNMP_FAILURE)

    {
        nmhSetFsDiffServOutProfileActionStatus (pDsAction->i4ActionId,
                                                i4RowStatus);
        return DS_FAILURE;
    }
    if (pDsAction->i4Action == DS_ACT_DSCP)

    {
        if (nmhTestv2FsDiffServOutProfileActionDscp
            (&u4ErrCode, pDsAction->i4ActionId,
             pDsAction->u4Dscp) == SNMP_FAILURE)

        {
            nmhSetFsDiffServOutProfileActionStatus (pDsAction->i4ActionId,
                                                    i4RowStatus);
            return DS_FAILURE;
        }
    }

    else

    {
        tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
        pDsOutProActEntry = DsGetOutProfileActionEntry (pDsAction->i4ActionId);
        if (pDsOutProActEntry != NULL)

        {
            pDsOutProActEntry->u4DsOutProfileActionFlag = 0;
            pDsOutProActEntry->u4DsOutProfileActionFlag |=
                FS_DS_OUT_ACTN_DO_NOT_SWITCH;
        }
    }

    if (nmhSetFsDiffServOutProfileActionMID
        (pDsAction->i4ActionId, i4MeterId) == SNMP_FAILURE)

    {
        nmhSetFsDiffServOutProfileActionStatus (pDsAction->i4ActionId,
                                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }

    if (pDsAction->i4Action == DS_ACT_DSCP)

    {
        if (nmhSetFsDiffServOutProfileActionDscp
            (pDsAction->i4ActionId, pDsAction->u4Dscp) == SNMP_FAILURE)

        {
            nmhSetFsDiffServOutProfileActionStatus (pDsAction->i4ActionId,
                                                    i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }

    if (nmhSetFsDiffServOutProfileActionStatus
        (pDsAction->i4ActionId, DS_ACTIVE) == SNMP_FAILURE)

    {
        nmhSetFsDiffServOutProfileActionStatus (pDsAction->i4ActionId,
                                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }

    if (u4Update)

    {
        /* This flag indicates that this entry previously existed and 
         * is now being updated */
        if (nmhSetFsDiffServClfrStatus (pDsAction->i4ActionId, DS_ACTIVE) ==
            SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DsCreateMeterEntry                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates a meter entry.               */
/*                                                                           */
/*     INPUT            : DsMeter - metering details                         */
/*                        CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : DS_SUCCESS - on successful creation of the entry   */
/*                        DS_FAILURE - on failure of entry creation          */
/*                                                                           */
/*****************************************************************************/
INT4
DsCreateMeterEntry (tCliHandle CliHandle, tDsMeter * pDsMeter)
{
    UINT4               u4ErrCode = 0;
    UINT1               u1FlagMeter = 0;
    INT4                i4RowStatus;
    INT4                i4Status;

    /* Check if metering entry previously exists */
    if (nmhGetFsDiffServMeterStatus (pDsMeter->i4MeterId, &i4RowStatus)
        != SNMP_FAILURE)

    {
        nmhGetFsDiffServClfrStatus (pDsMeter->i4MeterId, &i4Status);
        if (i4Status == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDsMeter->i4MeterId, DS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (DS_FAILURE);
            }
            u1FlagMeter = 1;
        }
        /* This flag indicates if this entry previously existed or
         *  if it is being created*/

        /* Destroy the previous metering entry and create it again */
        if (nmhSetFsDiffServOutProfileActionStatus
            (pDsMeter->i4MeterId, DS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }

        if (nmhSetFsDiffServMeterStatus
            (pDsMeter->i4MeterId, DS_DESTROY) == SNMP_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return DS_FAILURE;
        }

    }

    /* Create a Metering Entry */

    if (nmhTestv2FsDiffServMeterStatus
        (&u4ErrCode, pDsMeter->i4MeterId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

    {
        return DS_FAILURE;
    }
    if (nmhSetFsDiffServMeterStatus
        (pDsMeter->i4MeterId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    i4RowStatus = DS_DESTROY;

    /* The bucket size here is set to a value of 32K
     * The refresh-byte determines the band-width allowed
     * e.g. a value of 1 allocates 1Mbps of band-width. 
     */

    /* Test the Metering Id, refresh count and token size. If any of
     * the test/set fails, the metering entry must be destroyed */

    if (nmhTestv2FsDiffServMetertokenSize
        (&u4ErrCode, pDsMeter->i4MeterId,
         FS_DS_METER_32768_TOKENS) == SNMP_FAILURE)

    {
        nmhSetFsDiffServMeterStatus (pDsMeter->i4MeterId, i4RowStatus);
        return DS_FAILURE;
    }
    if (nmhTestv2FsDiffServMeterRefreshCount
        (&u4ErrCode, pDsMeter->i4MeterId, pDsMeter->u4Refresh) == SNMP_FAILURE)

    {
        nmhSetFsDiffServMeterStatus (pDsMeter->i4MeterId, i4RowStatus);
        return DS_FAILURE;
    }
    if (nmhSetFsDiffServMetertokenSize
        (pDsMeter->i4MeterId, FS_DS_METER_32768_TOKENS) == SNMP_FAILURE)

    {
        nmhSetFsDiffServMeterStatus (pDsMeter->i4MeterId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    if (nmhSetFsDiffServMeterRefreshCount
        (pDsMeter->i4MeterId, pDsMeter->u4Refresh) == SNMP_FAILURE)

    {
        nmhSetFsDiffServMeterStatus (pDsMeter->i4MeterId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    if (nmhSetFsDiffServMeterStatus (pDsMeter->i4MeterId, DS_ACTIVE)
        == SNMP_FAILURE)

    {
        nmhSetFsDiffServMeterStatus (pDsMeter->i4MeterId, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return DS_FAILURE;
    }
    if (u1FlagMeter == 1)
    {
        /* This flag indicates that this entry previously existed and 
         * is now being updated */
        if (nmhSetFsDiffServClfrStatus
            (pDsMeter->i4MeterId, DS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (DS_FAILURE);
        }
    }
    return DS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservNoPolicyClass                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the Policy-class map relation*/
/*                                                                           */
/*     INPUT            :  i4ClassMap Id- Class map                          */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservNoPolicyClass (tCliHandle CliHandle, INT4 i4ClassMap)
{
    INT4                i4PolIndex;

    tDiffServClfrEntry *pDiffServClfrEntry = NULL;

    /* Get the current policy map identifier */
    i4PolIndex = CLI_GET_POLICYMAP_ID ();

    pDiffServClfrEntry = DsGetClfrEntry (i4PolIndex);
    if (pDiffServClfrEntry == NULL)
    {
        return (CLI_FAILURE);
    }
    if (pDiffServClfrEntry->i4DsClfrMFClfrId == i4ClassMap)

    {
        /*Policy map <-> class map match found */

        /*Set the policy map status to NOT_IN_SERVICE  and delete all the 
         * configured parameters for the policy map */

        if (pDiffServClfrEntry->u1DsClfrStatus == DS_ACTIVE)
        {
            if (nmhSetFsDiffServClfrStatus
                (pDiffServClfrEntry->i4DsClfrId,
                 DS_NOT_IN_SERVICE) == DS_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        /* Delete the Out profile action */
        if (pDiffServClfrEntry->i4DsClfrOutProActionId != 0)

        {
            if (nmhSetFsDiffServOutProfileActionStatus
                (pDiffServClfrEntry->i4DsClfrOutProActionId,
                 DS_DESTROY) == DS_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            if (nmhSetFsDiffServMeterStatus
                (pDiffServClfrEntry->i4DsClfrOutProActionId,
                 DS_DESTROY) == DS_FAILURE)

            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        /*Destroy the policy map */
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, DS_DESTROY) == DS_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        /*Create the policy map again */
        if (nmhSetFsDiffServClfrStatus
            (pDiffServClfrEntry->i4DsClfrId, DS_CREATE_AND_WAIT) == DS_FAILURE)

        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%% This class map is not associated with this policy map \r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffServSetCosqAlgo                                */
/*                                                                           */
/*     DESCRIPTION      : Function to change  cosq scheduling algorithm      */
/*                                                                           */
/*     INPUT            : CliHandle      - Handle to  the CLI Context        */
/*                        u4PortNumber   - Port Number                       */
/*                        u4CosqAlgo     - New Algorithm to set              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DiffServSetCosqAlgo (tCliHandle CliHandle, UINT4 u4PortNumber, UINT4 u4CosqAlgo)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsDiffServCoSqAlgorithm
        (&u4ErrCode, (INT4) u4PortNumber, (INT4) u4CosqAlgo) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (nmhSetFsDiffServCoSqAlgorithm
        ((INT4) u4PortNumber, (INT4) u4CosqAlgo) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
#endif

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffServUpdateCosqWeightBw                         */
/*                                                                           */
/*     DESCRIPTION      : This function will update global array of cosq     */
/*                        weights and bandwidths                             */
/*                                                                           */
/*     INPUT            : CliHandle      - Handle to  the CLI Context        */
/*                        u4PortNo       - Port Number                       */
/*                        u4TrafficClass - Cosq Id                           */
/*                        u4Weights      - Weight of cosq to be configured   */
/*                        u4MinBw        - Minimum Bandwidth                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DiffServUpdateCosqWeightBw (tCliHandle CliHandle, UINT4 u4PortNo,
                            UINT4 u4TrafficClass, UINT4 u4Weights,
                            UINT4 u4MinBw)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsDiffServCoSqWeight
        (&u4ErrCode, (INT4) u4PortNo, (INT4) u4TrafficClass, (INT4) u4Weights)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (nmhSetFsDiffServCoSqWeight
        ((INT4) u4PortNo, (INT4) u4TrafficClass,
         (INT4) u4Weights) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
#endif

    if (u4MinBw != 0)
    {
        if (nmhTestv2FsDiffServCoSqBwMin
            (&u4ErrCode, (INT4) u4PortNo, (INT4) u4TrafficClass, u4MinBw)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetFsDiffServCoSqBwMin
            ((INT4) u4PortNo, (INT4) u4TrafficClass, u4MinBw);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/

/*                                                                           */
/*     FUNCTION NAME    : DiffservShowPolicyMap                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Policy-class details    */
/*                                                                           */
/*     INPUT            :  i4ClassMapId- Class map                           */
/*                          i4PolicyMapId -Policy Map                        */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
DiffservShowPolicyMap (tCliHandle CliHandle, INT4 i4PolicyMapId,
                       INT4 i4ClassMapId)
{
    UINT1              *pu1Temp = NULL;
    INT4                i4ClfrStatus = 0;
    INT4                i4DsStatus = 0;
    INT4                i4ActMID = 0;
    UINT4               u4InProfFlag = 0;
    UINT4               u4InNewPrio = 0;
    UINT4               u4InIpTOs = 0;
    UINT4               u4InActDscp = 0;
    UINT4               u4TokSize = 0;
    UINT4               u4RefCount = 0;
    UINT4               u4OutProfFlag = 0;
    UINT4               u4OutActDscp = 0;
    tSNMP_OCTET_STRING_TYPE PortList;
    CHR1               *pu1String = NULL;
    INT4                i4ClfrId = 0;
    tDsClfr             DsClfr;
    INT4                i4NextClfrId = 0;
    INT4                i4MFClfrId = 0;
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    UINT1               au1Tmp[DS_MAX_NAME_LENGTH];
    UINT1               au1PortList[DS_PORT_LIST_SIZE];
    INT1               *piIfName = NULL;
    UINT1               u1ShowAll = DS_FALSE;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               u1Check = DS_TRUE;

#ifdef NPAPI_WANTED
    INT4                i4DataPathId = 0;
    tDiffServCounters   DsCounters;
#endif /*  */

    pu1String = (CHR1 *) & au1IfName[0];
    pu1Temp = &au1IfName[0];
    piIfName = (INT1 *) &au1Tmp[0];
    PortList.pu1_OctetList = &au1PortList[0];

    if (i4PolicyMapId != 0)
    {
        /* Details of a specific policy map need to be displayed */
        i4ClfrId = i4PolicyMapId;
        if (nmhGetFsDiffServClfrStatus (i4ClfrId, &i4ClfrStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid policy map \r\n");

            return (CLI_FAILURE);
        }

        /* Get the Clfr and MCClfrId from it */
        nmhGetFsDiffServClfrMFClfrId (i4PolicyMapId, &i4MFClfrId);

        if (i4ClassMapId != 0)
        {
            /* Details of a specific class map need to be displayed */
            if (i4ClassMapId != i4MFClfrId)
            {
                /* Invalid combination for policy map and class map */
                CliPrintf (CliHandle, "%% Invalid Class Map \r\n");

                return (CLI_FAILURE);
            }
        }

        i4NextClfrId = i4PolicyMapId;
    }
    else
    {
        /* 
         * This flag is used to indicate that all policy 
         * maps must be displayed 
         */
        u1ShowAll = DS_TRUE;
    }

    CliPrintf (CliHandle, "\r\nDiffServ Configurations: \r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    if (u1ShowAll == DS_TRUE)
    {
        if (nmhGetFirstIndexFsDiffServClfrTable (&i4NextClfrId) == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }

    do
    {
        MEMSET (au1IfName, 0, DS_MAX_NAME_LENGTH);
        MEMSET (au1PortList, 0, DS_PORT_LIST_SIZE);
        MEMSET (au1Tmp, 0, DS_MAX_NAME_LENGTH);

        i4ClfrId = i4NextClfrId;

        if (nmhGetFsDiffServClfrStatus (i4ClfrId, &i4ClfrStatus)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid  policy map specified\r\n");

            return (CLI_FAILURE);
        }

        /* Get the Clfr and MCClfrId from it */
        nmhGetFsDiffServClfrMFClfrId (i4ClfrId, &i4MFClfrId);

        if (u1Check == DS_TRUE)
        {
            nmhGetFsDsStatus (&i4DsStatus);
        }
        if (nmhGetFsDiffServClfrInProActionId (i4ClfrId, &DsClfr.i4DsInProActId)
            != SNMP_FAILURE)
        {
            if (DsClfr.i4DsInProActId != 0)
            {
                if (nmhGetFsDiffServInProfileActionFlag
                    (DsClfr.i4DsInProActId, &u4InProfFlag) == SNMP_SUCCESS)
                {
                    if (u4InProfFlag & FS_DS_ACTN_INSERT_PRIO)
                    {
                        nmhGetFsDiffServInProfileActionNewPrio
                            (DsClfr.i4DsInProActId, &u4InNewPrio);
                    }
                    else if (u4InProfFlag & FS_DS_ACTN_INSERT_TOSP)
                    {
                        nmhGetFsDiffServInProfileActionIpTOS
                            (DsClfr.i4DsInProActId, &u4InIpTOs);
                    }
                    else if (u4InProfFlag & FS_DS_ACTN_INSERT_DSCP)
                    {
                        nmhGetFsDiffServInProfileActionDscp
                            (DsClfr.i4DsInProActId, &u4InActDscp);
                    }
                }
            }
        }

        if (nmhGetFsDiffServClfrOutProActionId
            (i4ClfrId, &DsClfr.i4DsOutProActId) != SNMP_FAILURE)
        {
            if (DsClfr.i4DsOutProActId != 0)
            {
                if (nmhGetFsDiffServOutProfileActionMID
                    (DsClfr.i4DsOutProActId, &i4ActMID) != SNMP_FAILURE)
                {
                    if (i4ActMID != 0)
                    {
                        nmhGetFsDiffServMetertokenSize (i4ActMID, &u4TokSize);
                        nmhGetFsDiffServMeterRefreshCount
                            (i4ActMID, &u4RefCount);
                    }
                }

                if (nmhGetFsDiffServOutProfileActionFlag
                    (DsClfr.i4DsOutProActId, &u4OutProfFlag) == SNMP_SUCCESS)
                {
                    if (u4OutProfFlag & FS_DS_OUT_ACTN_INSERT_DSCP)
                    {
                        nmhGetFsDiffServOutProfileActionDscp
                            (DsClfr.i4DsOutProActId, &u4OutActDscp);
                    }
                }
            }
        }

#ifdef NPAPI_WANTED
        if ((i4PolicyMapId != 0) && (DsClfr.i4DsOutProActId != 0))
        {
            DsHwGetCounters (i4DataPathId, i4ClfrId, &DsCounters);
        }
#endif /*  */

        if (u1Check == DS_TRUE)
        {
            if (i4DsStatus == DS_ENABLE)
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Quality of Service has been disabled \r\n");
            }

            u1Check = DS_FALSE;
        }

        CliPrintf (CliHandle, "\r\nPolicy Map %d ", i4ClfrId);

        if (i4ClfrStatus == DS_ACTIVE)
        {
            CliPrintf (CliHandle, "is active\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "is not active\r\n");
        }

        CliPrintf (CliHandle, "\r\nClass Map: %d\r\n", i4MFClfrId);
        CliPrintf (CliHandle, "-------------\r\n");

        if (DsClfr.i4DsInProActId != 0)
        {
            CliPrintf (CliHandle, "\r\nIn Profile Entry \r\n");
            CliPrintf (CliHandle, "----------------\r\n");

            CliPrintf (CliHandle, "%-28s: ", "In profile action");

            if (u4InProfFlag & FS_DS_ACTN_DO_NOT_SWITCH)
            {
                CliPrintf (CliHandle, "drop\r\n");
            }
            else if (u4InProfFlag & FS_DS_ACTN_INSERT_PRIO)
            {
                CliPrintf (CliHandle, "policed-cos %d\r\n", u4InNewPrio);
            }
            else if (u4InProfFlag & FS_DS_ACTN_INSERT_TOSP)
            {
                CliPrintf (CliHandle, "policed-precedence %d\r\n", u4InIpTOs);
            }
            else if (u4InProfFlag & FS_DS_ACTN_INSERT_DSCP)
            {
                CliPrintf (CliHandle, "policed-dscp %d\r\n", u4InActDscp);
            }
            else
            {
                CliPrintf (CliHandle, "none\r\n");
            }
        }

        if (DsClfr.i4DsOutProActId != 0)
        {
            CliPrintf (CliHandle, "\r\nOut Profile Entry \r\n");
            CliPrintf (CliHandle, "-----------------\r\n");

            if (i4ActMID != 0)
            {
                CliPrintf (CliHandle, "Metering on \r\n");

                if (u4TokSize != 0)
                {
                    CliPrintf (CliHandle, "%-28s: %d\r\n",
                               "burst bytes/token size", u4TokSize);
                }

                if (u4RefCount != 0)
                {
                    CliPrintf (CliHandle, "%-28s: %d\r\n",
                               "Refresh count", u4RefCount);
                }
            }

            CliPrintf (CliHandle, "%-28s: ", "Out profile action");

            if (u4OutProfFlag & FS_DS_OUT_ACTN_DO_NOT_SWITCH)
            {
                CliPrintf (CliHandle, "drop\r\n");
            }
            else if (u4OutProfFlag & FS_DS_OUT_ACTN_INSERT_DSCP)
            {
                CliPrintf (CliHandle, "policed-dscp %d\r\n", u4OutActDscp);
            }
            else
            {
                CliPrintf (CliHandle, "none\r\n");
            }
        }

#ifdef NPAPI_WANTED
        if ((i4PolicyMapId != 0) && (DsClfr.i4DsOutProActId != 0))
        {
            CliPrintf (CliHandle, "\r\nDiffServ Statistics \r\n");
            CliPrintf (CliHandle, "-------------------\r\n");

            CliPrintf (CliHandle, "%-28s: %d\r\n", "In Profile Packets",
                       DsCounters.u4Inpkt);
            CliPrintf (CliHandle, "%-28s: %d\r\n", "Out Profile Packets",
                       DsCounters.u4Outpkt);
        }
#endif /*  */

        i4Quit = CliPrintf (CliHandle, "\r\n");

        if (u1ShowAll == DS_FALSE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDiffServClfrTable (i4ClfrId, &i4NextClfrId)
            != SNMP_FAILURE) && (i4Quit == CLI_SUCCESS));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowClassMap                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Policy-class details    */
/*                                                                           */
/*     INPUT            :  u4ClassIndex- Class map                           */
/*                         CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservShowClassMap (tCliHandle CliHandle, UINT4 u4ClassIndex)
{
    UINT1               u1Flag = DS_TRUE;
    INT4                i4NextIndex = u4ClassIndex;
    INT4                i4PrevIndex;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1              *pu1Temp = NULL;
    CHR1               *pu1String = NULL;
    UINT4               u4FilterId = 0;
    INT4                i4FilterType = 0;
    UINT1               au1IfName[DS_MAX_NAME_LENGTH];
    INT4                i4RetVal;

    pu1String = (CHR1 *) & au1IfName[0];
    pu1Temp = &au1IfName[0];

    if (u4ClassIndex == 0)
    {
        /* Details of all class maps must be displayed . u1Flag is reset to
         * indicate the same */

        u1Flag = DS_FALSE;
        if (nmhGetFirstIndexFsDiffServMultiFieldClfrTable (&i4NextIndex)
            == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
    }
    else
    {
        if (nmhGetFsDiffServMultiFieldClfrStatus (u4ClassIndex, &i4RetVal) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Class map \r\n");

            return (CLI_FAILURE);
        }
    }

    do
    {
        MEMSET (au1IfName, 0, DS_MAX_NAME_LENGTH);

        if (nmhGetFsDiffServMultiFieldClfrFilterId (i4NextIndex,
                                                    &u4FilterId) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhGetFsDiffServMultiFieldClfrFilterType (i4NextIndex,
                                                      &i4FilterType) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        CliPrintf (CliHandle, "\r\nDiffServ Configurations: \r\n");
        CliPrintf (CliHandle, "------------------------\r\n");

        CliPrintf (CliHandle, "\r\nClass map %d\r\n", i4NextIndex);
        CliPrintf (CliHandle, "--------------\r\n");

        CliPrintf (CliHandle, "%-28s: %d\r\n", "Filter ID", u4FilterId);
        CliPrintf (CliHandle, "%-28s: ", "Filter Type");
        if (i4FilterType == DS_MAC_FILTER)
        {
            CliPrintf (CliHandle, "MAC-FILTER\r\n");
        }
        else if (i4FilterType == DS_IP_FILTER)
        {
            CliPrintf (CliHandle, "IP-FILTER\r\n");
        }

        i4Quit = CliPrintf (CliHandle, "\r\n");

        i4PrevIndex = i4NextIndex;

        /* break from the loop if class map index is specified */
        if (u1Flag == DS_TRUE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsDiffServMultiFieldClfrTable
            (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS)
           && (i4Quit == CLI_SUCCESS));

    return (i4Quit);
}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME : DiffservShowCoSqAlgo                                      */
/*                                                                           */
/* DESCRIPTION   : This function displays the Scheduling algorithm used      */
/*                    for the given interface.                               */
/*                    If the interface identifier is given as zero then      */
/*                    the scheduling algorithms for all the interfaces       */
/*                    are displayed.                                         */
/*                 interface                                                 */
/*                                                                           */
/* INPUT         : CliHandle - Handle to  the CLI Context                    */
/*                 i4IfaceIndex  - The interface identifier for which the    */
/*                              Scheduling algorithm is to be displayed      */
/*                                                                           */
/* OUTPUT        : NONE                                                      */
/*                                                                           */
/* RETURNS       : NONE                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
DiffservShowCoSqAlgo (tCliHandle CliHandle, INT4 i4IfaceIndex)
{
    INT4                i4CoSqAlg = 0;
    INT4                i4NextIntf = 0;
    UINT4               u4PageStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1IfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Algorithm[9][30] = { "",
        "StrictPriority\0",
        "RoundRobin\0",
        "WeightedRoundRobin\0",
        "WeightedFairQueing\0",
        "Strict-RoundRobin\0",
        "Strict-WeightedRoundRobin\0",
        "Strict-WeightedFairQueing\0",
        "DeficitRoundRobin\0"
    };

    MEMSET (au1IfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (i4IfaceIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        CliPrintf (CliHandle, "%% Invalid Interface.\r\n");
        return;
    }

    CliPrintf (CliHandle, "\r\n CoSq Algorithm \r\n");
    CliPrintf (CliHandle, "----------------- \r\n");
    CliPrintf (CliHandle, "Interface       Algorithm \r\n");
    CliPrintf (CliHandle, "---------       --------- \r\n");

    if (i4IfaceIndex != 0)
    {
        nmhGetFsDiffServCoSqAlgorithm (i4IfaceIndex, &i4CoSqAlg);

        CfaCliGetIfName ((UINT4) i4IfaceIndex, (INT1 *) au1IfaceName);

        CliPrintf (CliHandle, "%-16s", au1IfaceName);
        CliPrintf (CliHandle, "%s \r\n", au1Algorithm[i4CoSqAlg]);
    }
    else
    {
        nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (&i4NextIntf);
        do
        {
            i4IfaceIndex = i4NextIntf;
            nmhGetFsDiffServCoSqAlgorithm (i4IfaceIndex, &i4CoSqAlg);

            CfaCliGetIfName ((UINT4) i4IfaceIndex, (INT1 *) au1IfaceName);

            CliPrintf (CliHandle, "%-16s", au1IfaceName);
            u4PageStatus = CliPrintf (CliHandle, "%s \r\n",
                                      au1Algorithm[i4CoSqAlg]);
            if (u4PageStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }
            if (nmhGetNextIndexFsDiffServCoSqAlgorithmTable
                (i4IfaceIndex, &i4NextIntf) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME : DiffservShowCoSqWeightBw                                  */
/*                                                                           */
/* DESCRIPTION   : This function displays the CoS weights and bandwidths     */
/*                 assigned to each cosq for a given interface.              */
/*                 If the interface identifier is given as zero              */
/*                 the cos weights of all the interfaces are displayed.      */
/*                                                                           */
/* INPUT         : CliHandle - Handle to  the CLI Context                    */
/*                 i4IfaceIndex  - The interface identifier for which the    */
/*                              weights is to be displayed                   */
/*                                                                           */
/* OUTPUT        : NONE                                                      */
/*                                                                           */
/* RETURNS       : NONE                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
DiffservShowCoSqWeightBw (tCliHandle CliHandle, INT4 i4IfaceIndex)
{
    INT4                i4CoSqId = 0;
    INT4                i4CoSqFlag = 0;
    INT4                i4NextIntf = 0;
    INT4                i4NextCoSqId = 0;
    INT4                i4CoSqWt = 0;
    UINT4               u4CosqMinBw = 0;
    UINT4               u4CoSqMaxBw = 0;
    UINT4               u4PageStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1IfaceName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (i4IfaceIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface.\r\n");
        return;
    }

    CliPrintf (CliHandle, "\r\nCoSq Weights and Bandwidths \r\n");
    CliPrintf (CliHandle, "--------------------------- \r\n");
    CliPrintf (CliHandle,
               "Interface  CoSqId  CoSqWeight   MinBw   MaxBw   Flag\r\n");
    CliPrintf (CliHandle,
               "---------  ------  ----------   -----   -----   ----\r\n");

    if (i4IfaceIndex != 0)
    {
        for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
        {
            nmhGetFsDiffServCoSqWeight (i4IfaceIndex, i4CoSqId, &i4CoSqWt);
            nmhGetFsDiffServCoSqBwMin (i4IfaceIndex, i4CoSqId, &u4CosqMinBw);
            nmhGetFsDiffServCoSqBwMax (i4IfaceIndex, i4CoSqId, &u4CoSqMaxBw);
            nmhGetFsDiffServCoSqBwFlags (i4IfaceIndex, i4CoSqId, &i4CoSqFlag);

            CfaCliGetIfName ((UINT4) i4IfaceIndex, (INT1 *) au1IfaceName);

            CliPrintf (CliHandle, "%-13s", au1IfaceName);
            CliPrintf (CliHandle, "%-11ld", i4CoSqId);
            CliPrintf (CliHandle, "%-8ld", i4CoSqWt);
            CliPrintf (CliHandle, "%-8ld", u4CosqMinBw);
            CliPrintf (CliHandle, "%-9ld", u4CoSqMaxBw);
            CliPrintf (CliHandle, "%-5ld \r\n", i4CoSqFlag);
        }
    }
    else
    {
        nmhGetFirstIndexFsDiffServCoSqWeightBwTable (&i4NextIntf,
                                                     &i4NextCoSqId);
        do
        {
            i4IfaceIndex = i4NextIntf;
            i4CoSqId = i4NextCoSqId;

            nmhGetFsDiffServCoSqWeight (i4IfaceIndex, i4CoSqId, &i4CoSqWt);
            nmhGetFsDiffServCoSqBwMin (i4IfaceIndex, i4CoSqId, &u4CosqMinBw);
            nmhGetFsDiffServCoSqBwMax (i4IfaceIndex, i4CoSqId, &u4CoSqMaxBw);
            nmhGetFsDiffServCoSqBwFlags (i4IfaceIndex, i4CoSqId, &i4CoSqFlag);

            CfaCliGetIfName ((UINT4) i4IfaceIndex, (INT1 *) au1IfaceName);

            CliPrintf (CliHandle, "%-13s", au1IfaceName);
            CliPrintf (CliHandle, "%-11ld", i4CoSqId);
            CliPrintf (CliHandle, "%-8ld", i4CoSqWt);
            CliPrintf (CliHandle, "%-8ld", u4CosqMinBw);
            CliPrintf (CliHandle, "%-9ld", u4CoSqMaxBw);
            u4PageStatus = CliPrintf (CliHandle, "%-5ld \r\n", i4CoSqFlag);

            if (u4PageStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }
            if (nmhGetNextIndexFsDiffServCoSqWeightBwTable
                (i4IfaceIndex, &i4NextIntf, i4CoSqId,
                 &i4NextCoSqId) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DcCliPrintPortList                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of port in port list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

VOID
DsCliPrintPortList (tCliHandle CliHandle, INT4 i4CommaCount, UINT1 *piIfName)
{

    INT4                i4Times;
    INT4                i4Loop;

    i4Times = DS_PORT_LIST_SIZE * 2;

    if (i4CommaCount == 1)
    {
        CliPrintf (CliHandle, "%s ", piIfName);
        return;
    }
    for (i4Loop = 1; i4Loop <= i4Times; i4Loop++)
    {
        /* 4 ports per line */

        if (i4CommaCount <= (4 * i4Loop))
        {
            if (i4CommaCount == (4 * (i4Loop - 1)) + 1)
            {
                CliPrintf (CliHandle, "\r\n                              ");
            }
            CliPrintf (CliHandle, ",%s", piIfName);
            break;
        }
    }
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservShowRunningConfig                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        information for diffserv                           */
/*                                                                           */
/*     INPUT            : CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4DsStatus = 0;
    INT4                i4DsSystemStatus = 0;
    CliRegisterLock (CliHandle, DfsLock, DfsUnLock);
    DFS_LOCK ();
    nmhGetFsDsSystemControl (&i4DsSystemStatus);
    if (i4DsSystemStatus != DS_START)
    {
        CliPrintf (CliHandle, "shutdown qos\r\n");
        CliPrintf (CliHandle, "!\r\n");
        DFS_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }
    nmhGetFsDsStatus (&i4DsStatus);
    if (i4DsStatus != DS_DISABLE)
    {
        CliPrintf (CliHandle, "set qos enable\r\n");
        DiffservClassMapShowRunningConfig (CliHandle);
        DiffservPolicyMapShowRunningConfig (CliHandle);
        DiffservCoSqAlgoShowRunningConfig (CliHandle);
        DiffservCoSqWeightBwShowRunningConfig (CliHandle);
        CliPrintf (CliHandle, "!\r\n");
    }
    DFS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservClassMapShowRunningConfig                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current class-map       */
/*                        configuration details                              */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservClassMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4NextIndex;
    INT4                i4PrevIndex;
    INT4                i4ClfrStatus;
    INT4                i4FilterType = 0;
    UINT4               u4FilterId = 0;
    UINT4               u4Flag = 0;

    if (nmhGetFirstIndexFsDiffServMultiFieldClfrTable (&i4NextIndex)
        != SNMP_FAILURE)
    {

        do
        {
            if (nmhGetFsDiffServMultiFieldClfrStatus (i4NextIndex,
                                                      &i4ClfrStatus) !=
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "class-map %d\r\n", i4NextIndex);
                u4Flag = 1;
                if (i4ClfrStatus != DS_ACTIVE)
                {
                    CliPrintf (CliHandle, "!\r\n");
                    return (CLI_SUCCESS);
                }
                if (nmhGetFsDiffServMultiFieldClfrFilterId (i4NextIndex,
                                                            &u4FilterId) !=
                    SNMP_FAILURE)
                {
                    if (nmhGetFsDiffServMultiFieldClfrFilterType (i4NextIndex,
                                                                  &i4FilterType)
                        != SNMP_FAILURE)
                    {

                        if (i4FilterType == DS_MAC_FILTER)
                        {
                            CliPrintf (CliHandle, "  match access-group");
                            CliPrintf (CliHandle, " mac-access-list");
                            CliPrintf (CliHandle, " %d\r\n", u4FilterId);
                            u4Flag = 1;
                        }
                        else if (i4FilterType == DS_IP_FILTER)
                        {
                            CliPrintf (CliHandle, "  match access-group");
                            CliPrintf (CliHandle, " ip-access-list");
                            CliPrintf (CliHandle, " %d\r\n", u4FilterId);
                            u4Flag = 1;
                        }

                    }
                }
            }
            i4PrevIndex = i4NextIndex;

        }
        while ((nmhGetNextIndexFsDiffServMultiFieldClfrTable
                (i4PrevIndex, &i4NextIndex) == SNMP_SUCCESS));

    }

    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCoSqAlgoShowRunningConfig                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current cosq scheduling */
/*                        algorithm configuration details                    */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservCoSqAlgoShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4CoSqAlg = 0;
    INT4                i4NextIntf = 0;
    INT4                i4IfaceIndex = 0;
    UINT4               u4Flag = 0;
    UINT1               au1Algorithm[9][30] = { "",
        "StrictPriority\0",
        "RoundRobin\0",
        "WeightedRoundRobin\0",
        "WeightedFairQueing\0",
        "Strict-RoundRobin\0",
        "Strict-WeightedRoundRobin\0",
        "Strict-WeightedFairQueing\0",
        "DeficitRoundRobin\0"
    };

    if (nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (&i4NextIntf)
        != SNMP_FAILURE)
    {
        do
        {
            if ((nmhGetFsDiffServCoSqAlgorithm (i4NextIntf, &i4CoSqAlg)
                 != SNMP_FAILURE) && (i4CoSqAlg != DS_COSQ_SCHE_ALGO_STRICT))
            {
                u4Flag = 1;
                CliPrintf (CliHandle, "!\r\n");
                CliPrintf (CliHandle, "interface  gigabitethernet 0/%ld\r\n",
                           i4NextIntf);
                CliPrintf (CliHandle, " cosq scheduling algorithm ");
                CliPrintf (CliHandle, "%s \r\n", au1Algorithm[i4CoSqAlg]);
            }
            i4IfaceIndex = i4NextIntf;
        }
        while (nmhGetNextIndexFsDiffServCoSqAlgorithmTable
               (i4IfaceIndex, &i4NextIntf) == SNMP_SUCCESS);
    }
    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservCoSqWeightBwShowRunningConfig              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current cosq weights and*/
/*                        bandwidth configuration details                    */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservCoSqWeightBwShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4CoSqId = 0;
    INT4                i4NextIntf = 0;
    INT4                i4NextCoSqId = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4CoSqWt = 0;
    UINT4               u4CoSqMinBw = 0;
    UINT4               u4Flag = 0;
    UINT4               u4Flag1 = 0;
    UINT4               u4Flag2 = 0;

    if (nmhGetFirstIndexFsDiffServCoSqWeightBwTable (&i4NextIntf, &i4NextCoSqId)
        != SNMP_FAILURE)
    {
        do
        {
            if (i4IfaceIndex != i4NextIntf)
            {
                u4Flag = 0;
            }
            nmhGetFsDiffServCoSqWeight (i4NextIntf, i4NextCoSqId, &i4CoSqWt);
            nmhGetFsDiffServCoSqBwMin (i4NextIntf, i4NextCoSqId, &u4CoSqMinBw);

            if (i4CoSqWt != DS_COSQ_MIN_WEIGHT)
            {
                if (u4Flag != 1)
                {
                    CliPrintf (CliHandle, "!\r\n");
                    CliPrintf (CliHandle, "interface  gigabitethernet 0/%ld\r",
                               i4NextIntf);
                }
                u4Flag = 1;
                u4Flag1 = 1;
                CliPrintf (CliHandle, "\n traffic-class  %d weight %ld",
                           i4NextCoSqId, i4CoSqWt);
                u4Flag2 = 1;
            }

            if (DsIsNpEasyrider () == DS_FALSE)
            {
                if (u4CoSqMinBw != 0)
                {
                    if (u4Flag != 1)
                    {
                        CliPrintf (CliHandle, "!\r\n");
                        CliPrintf (CliHandle,
                                   "interface  gigabitethernet 0/%ld\r",
                                   i4NextIntf);
                    }
                    u4Flag = 1;
                    if (u4Flag1 != 1)
                    {
                        CliPrintf (CliHandle, "\n traffic-class  %d",
                                   i4NextCoSqId);
                    }
                    CliPrintf (CliHandle, " minbanwidth  %ld", u4CoSqMinBw);
                    u4Flag2 = 1;
                }
            }

            if (DsIsNpEasyrider () == DS_TRUE)
            {
                if (u4CoSqMinBw != DS_COSQ_DEF_BW)
                {
                    if (u4Flag != 1)
                    {
                        CliPrintf (CliHandle, "!\r\n");
                        CliPrintf (CliHandle,
                                   "interface  gigabitethernet 0/%ld\r",
                                   i4NextIntf);
                    }
                    u4Flag = 1;
                    if (u4Flag1 != 1)
                    {
                        CliPrintf (CliHandle, "\n traffic-class  %d",
                                   i4NextCoSqId);
                    }
                    CliPrintf (CliHandle, " minbanwidth  %ld", u4CoSqMinBw);
                    u4Flag2 = 1;
                }
            }

            i4IfaceIndex = i4NextIntf;
            i4CoSqId = i4NextCoSqId;
        }
        while (nmhGetNextIndexFsDiffServCoSqWeightBwTable
               (i4IfaceIndex, &i4NextIntf, i4CoSqId,
                &i4NextCoSqId) == SNMP_SUCCESS);
    }

    if (u4Flag2 == 1)
    {
        CliPrintf (CliHandle, " \r\n!\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DiffservPolicyMapShowRunningConfig                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current policy-map      */
/*                        configuration details                              */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
DiffservPolicyMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4ClfrStatus = 0;
    INT4                i4ClfrId = 0;
    INT4                i4NextClfrId = 0;
    INT4                i4MFClfrId = 0;
#if defined (BCM5690_WANTED) || defined (BCM5695_WANTED) || defined (BCM5650) ||defined (BCMX_WANTED)
    INT4                i4ActMID = 0;
    UINT4               u4RefCount = 0;
    UINT4               u4OutProfFlag = 0;
    UINT4               u4OutActDscp = 0;
#endif
    UINT4               u4InProfFlag = 0;
    UINT4               u4InNewPrio = 0;
    UINT4               u4InIpTOs = 0;
    UINT4               u4InActDscp = 0;
    UINT4               u4Flag = 0;
    tDsClfr             DsClfr;
    if (nmhGetFirstIndexFsDiffServClfrTable (&i4NextClfrId) != SNMP_FAILURE)
    {
        do
        {
            i4ClfrId = i4NextClfrId;

            if (nmhGetFsDiffServClfrStatus (i4ClfrId, &i4ClfrStatus)
                != SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "policy-map %d\r\n", i4ClfrId);
                u4Flag = 1;
                if (i4ClfrStatus != DS_ACTIVE)
                {
                    CliPrintf (CliHandle, "!\r\n");
                    return (CLI_SUCCESS);
                }

                /* Get the Clfr and MCClfrId from it */
                if (nmhGetFsDiffServClfrMFClfrId (i4ClfrId, &i4MFClfrId)
                    != SNMP_FAILURE)
                {
                    if (i4MFClfrId != 0)
                    {
                        CliPrintf (CliHandle, "  class %d\r\n", i4MFClfrId);

                        u4Flag = 2;
                        if (nmhGetFsDiffServClfrInProActionId (i4ClfrId,
                                                               &DsClfr.
                                                               i4DsInProActId)
                            != SNMP_FAILURE)
                        {
                            if (DsClfr.i4DsInProActId != 0)
                            {
                                if (nmhGetFsDiffServInProfileActionFlag
                                    (DsClfr.i4DsInProActId,
                                     &u4InProfFlag) == SNMP_SUCCESS)
                                {
                                    if (u4InProfFlag & FS_DS_ACTN_INSERT_PRIO)
                                    {
                                        nmhGetFsDiffServInProfileActionNewPrio
                                            (DsClfr.i4DsInProActId,
                                             &u4InNewPrio);
                                        CliPrintf (CliHandle, "    set");
                                        CliPrintf (CliHandle, " cos %d\r\n",
                                                   u4InNewPrio);
                                        u4Flag = 3;
                                    }
                                    else if (u4InProfFlag &
                                             FS_DS_ACTN_INSERT_TOSP)
                                    {
                                        nmhGetFsDiffServInProfileActionIpTOS
                                            (DsClfr.i4DsInProActId, &u4InIpTOs);
                                        CliPrintf (CliHandle, "    set");
                                        CliPrintf (CliHandle,
                                                   " ip precedence %d\r\n",
                                                   u4InIpTOs);
                                        u4Flag = 3;
                                    }
                                    else if (u4InProfFlag &
                                             FS_DS_ACTN_INSERT_DSCP)
                                    {
                                        nmhGetFsDiffServInProfileActionDscp
                                            (DsClfr.i4DsInProActId,
                                             &u4InActDscp);
                                        CliPrintf (CliHandle, "    set");
                                        CliPrintf (CliHandle,
                                                   " ip dscp %d\r\n",
                                                   u4InActDscp);
                                        u4Flag = 3;
                                    }
                                }
                            }
                        }

                    }
                }

#if defined (BCM5690_WANTED) || defined (BCM5695_WANTED) || defined (BCM5650) ||defined (BCMX_WANTED)

                if (nmhGetFsDiffServClfrOutProActionId
                    (i4ClfrId, &DsClfr.i4DsOutProActId) != SNMP_FAILURE)
                {
                    if (DsClfr.i4DsOutProActId != 0)
                    {
                        if (nmhGetFsDiffServOutProfileActionMID
                            (DsClfr.i4DsOutProActId, &i4ActMID) != SNMP_FAILURE)
                        {
                            if (i4ActMID != 0)
                            {
                                nmhGetFsDiffServMeterRefreshCount
                                    (i4ActMID, &u4RefCount);

                                if (u4RefCount != 0)
                                {
                                    if (nmhGetFsDiffServOutProfileActionFlag
                                        (DsClfr.i4DsOutProActId,
                                         &u4OutProfFlag) == SNMP_SUCCESS)
                                    {
                                        if (u4OutProfFlag &
                                            FS_DS_OUT_ACTN_INSERT_DSCP)
                                        {
                                            nmhGetFsDiffServOutProfileActionDscp
                                                (DsClfr.i4DsOutProActId,
                                                 &u4OutActDscp);
                                            CliPrintf (CliHandle, "    police");
                                            CliPrintf (CliHandle, " %d",
                                                       u4RefCount);
                                            CliPrintf (CliHandle,
                                                       " exceed-action policed-dscp-transmit %d\r\n",
                                                       u4OutActDscp);
                                            u4Flag = 3;
                                        }
                                        else if (u4OutProfFlag &
                                                 FS_DS_OUT_ACTN_DO_NOT_SWITCH)
                                        {
                                            CliPrintf (CliHandle, "    police");
                                            CliPrintf (CliHandle, " %d",
                                                       u4RefCount);
                                            CliPrintf (CliHandle,
                                                       " exceed-action drop\r\n");
                                            u4Flag = 3;
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
#endif /* */
            }

        }
        while ((nmhGetNextIndexFsDiffServClfrTable (i4ClfrId, &i4NextClfrId)
                != SNMP_FAILURE));

    }
    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\n");
    }
    else if (u4Flag == 2 || u4Flag == 3)
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}
#endif /* */

#else

INT4
cli_process_ds_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    mmi_printf ("DiffServ module not available !\r\n");
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Command);
    return CLI_SUCCESS;
}

#endif /* ISS_WANTED */
#endif
