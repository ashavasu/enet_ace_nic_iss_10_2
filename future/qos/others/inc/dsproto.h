
/*$Id: dsproto.h,v 1.8 2012/04/05 14:04:17 siva Exp $*/
#ifndef _DSPROTO_H
#define _DSPROTO_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dsproto.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ                                       */
/*    MODULE NAME           : All modules in Link Aggregation.               */
/*    DSNGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Manish K S                                     */
/*    DESCRIPTION           : This file contains prototypes of functions in  */
/*                            DiffServ module.                               */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/

/* dssys.c */
INT4 DsValInProActInsertPrio (tDiffServInProfileActionEntry *);
INT4 DsValInProActSetCOSQueue (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertTOS (tDiffServInProfileActionEntry *);
INT4 DsValInProActCopyToCPU (tDiffServInProfileActionEntry *);
INT4 DsValInProActDoNotSwitch (tDiffServInProfileActionEntry *);
INT4 DsValInProActSetOutPortUCast (tDiffServInProfileActionEntry *);
INT4 DsValInProActCopyToMirror (tDiffServInProfileActionEntry *);
INT4 DsValInProActIncrFFPPktCounter (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertPrioFromTOS (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertTOSFromPrio (tDiffServInProfileActionEntry *);
INT4 DsValInProActInsertDSCP (tDiffServInProfileActionEntry *);
INT4 DsValInProActSetOutPortNonUCast (tDiffServInProfileActionEntry *);
INT4 DsValInProActDoSwitch (tDiffServInProfileActionEntry *);

INT4 DsValOutProActCopyToCPU (tDiffServOutProfileActionEntry *);
INT4 DsValOutProActDoNotSwitch (tDiffServOutProfileActionEntry *);
INT4 DsValOutProActInsertDSCP (tDiffServOutProfileActionEntry *);
INT4 DsValOutProActDoSwitch (tDiffServOutProfileActionEntry *);

#ifdef _DSSYS_C
tDsInProActValEntry DsValiateInProActFunctPtrArr [DS_INPROACT_FLAGS] = {

                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertPrio},
     {(DS_INPROACTVAL_FUNCT) DsValInProActSetCOSQueue},
     {(DS_INPROACTVAL_FUNCT) DsValInProActInsertTOS},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActCopyToCPU},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActDoNotSwitch},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActSetOutPortUCast},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActCopyToMirror},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActIncrFFPPktCounter},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertPrioFromTOS},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertTOSFromPrio},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActInsertDSCP},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActSetOutPortNonUCast},
                   {(DS_INPROACTVAL_FUNCT) DsValInProActDoSwitch}
};

tDsOutProActValEntry DsValiateOutProActFunctPtrArr [DS_OUTPROACT_FLAGS] = {

                   {(DS_OUTPROACTVAL_FUNCT) DsValOutProActCopyToCPU},
                   {(DS_OUTPROACTVAL_FUNCT) DsValOutProActDoNotSwitch},
                   {(DS_OUTPROACTVAL_FUNCT) DsValOutProActInsertDSCP},
     {(DS_OUTPROACTVAL_FUNCT) NULL},
                   {(DS_OUTPROACTVAL_FUNCT) DsValOutProActDoSwitch}
};

#else

extern tDsInProActValEntry DsValiateInProActFunctPtrArr [DS_INPROACT_FLAGS];
extern tDsOutProActValEntry DsValiateOutProActFunctPtrArr [DS_OUTPROACT_FLAGS];

#endif  /* _DSSYS_C */   

INT4 DsEnable(VOID); 
INT4 DsDisable(VOID);

INT4 DsShutDown(VOID);

VOID DsHandleInitFailure(VOID);

INT4 DsValidateMFClfrId (INT4 );
INT4 DsValidateClfrId (INT4 );
INT4 DsValidateInProfileActionId (INT4 );
INT4 DsValidateOutProfileActionId (INT4 );
INT4 DsValidateMeterId (INT4 );
INT4 DsValidateSchedulerId (INT4 );

INT4 DsSnmpLowGetFirstValidMultiFieldClfrId (INT4 *);
INT4 DsSnmpLowGetNextValidMultiFieldClfrId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidClfrId (INT4 *);
INT4 DsSnmpLowGetNextValidClfrId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidInProfileActionId (INT4 *);
INT4 DsSnmpLowGetNextValidInProfileActionId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidOutProfileActionId (INT4 *);
INT4 DsSnmpLowGetNextValidOutProfileActionId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidMeterId (INT4 *);
INT4 DsSnmpLowGetNextValidMeterId (INT4, INT4 *);
INT4 DsSnmpLowGetFirstValidSchedulerId (INT4 *);
INT4 DsSnmpLowGetNextValidSchedulerId (INT4, INT4 *);

tDiffServMultiFieldClfrEntry* DsGetMFClfrEntry (INT4);
tDiffServClfrEntry* DsGetClfrEntry (INT4);
tDiffServInProfileActionEntry* DsGetInProfileActionEntry (INT4);
tDiffServOutProfileActionEntry* DsGetOutProfileActionEntry (INT4);
tDiffServMeterEntry* DsGetMeterEntry (INT4);
tDiffServSchedulerEntry* DsGetSchedulerEntry (INT4);

INT4 DsQualifySchedulerData (tDiffServSchedulerEntry *);
INT4 DsQualifyMFClfrData (tDiffServMultiFieldClfrEntry *);
INT4 DsQualifyClfrData (tDiffServClfrEntry *);
INT4 DsQualifyInProfileActionData (tDiffServInProfileActionEntry *);
INT4 DsQualifyMeterData (tDiffServMeterEntry *);
INT4 DsQualifyOutProfileActionData (tDiffServOutProfileActionEntry *);

INT4 DsValidateSchedulerDP (INT4);
INT4 DsCheckMFClfrDependency (INT4);
INT4 DsCheckInProfileActionDependency (INT4);
INT4 DsCheckOutProfileActionDependency (INT4);
INT4 DsCheckMeterDependency (INT4);

INT4 RegisterFsDswithFutureSNMP (VOID);

INT4
MsrValidateDiffServInProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDiffServOutProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4 DsWebnmGetPolicyMapEntry PROTO ((INT4 i4PolicyMapId,
                                      tDiffServWebClfrData *pClfrData));
INT4 DsWebnmSetPolicyMapEntry PROTO ((tDiffServWebSetClfrEntry *pClfrEntry,
                                      UINT1 u1RowStatus, UINT1 *pu1ErrString));
INT4 DsIsNpEasyrider PROTO ((VOID));

#endif  /* _DSPROTO_H */   
