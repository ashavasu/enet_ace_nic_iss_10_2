/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: diffsrvcmd.def,v 1.6 2014/11/05 10:59:20 siva Exp $                                                         
*                                                                    
*********************************************************************/
/********** DIFFSERV MODE COMMANDS ********************/

DEFINE GROUP : DIFFSRV_CONFIG_CMDS

#if defined (NPAPI_WANTED)

   COMMAND : shutdown qos
   ACTION  : cli_process_ds_cmd (CliHandle, CLI_DS_SHUT, NULL);
   SYNTAX  : shutdown qos
   PRVID   : 15
   HELP    : Shutsdown Quality-of-Service operation
   CXT_HELP : shutdown Shuts down the feature | 
              qos Quality of Service configuration | 
              <CR> Shutsdown Quality-of-Service operation

   COMMAND : no shutdown qos
   ACTION  : cli_process_ds_cmd (CliHandle, CLI_DS_NO_SHUT, NULL);
   SYNTAX  : no shutdown qos
   PRVID   : 15
   HELP    : Starts and enables Quality-of-Service operation
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              shutdown Shuts down the feature | 
              qos Quality of Service configuration | 
              <CR> Starts and enables Quality-of-Service operation

   COMMAND : set qos { enable | disable }
   ACTION  : {
                 UINT4 u4Type=0;
                 if($2 != NULL)
                 {
                     u4Type= CLI_ENABLE;
                 } 
                 if($3 != NULL)
                 {
                     u4Type= CLI_DISABLE;
                 }
                 cli_process_ds_cmd (CliHandle, CLI_DS_SYSTEM_CONTROL, NULL, 
                 u4Type); 
	     }
   SYNTAX  : set qos { enable | disable }
   PRVID   : 15
   HELP    : Configures diffserv system control status
   CXT_HELP : set Configures the parameters | 
              qos Quality of Service configuration | 
              enable Enables differentiated services | 
              disable Disables differentiated services | 
              <CR> Configures diffserv system control status

   COMMAND : class-map <short(1-65535)>  
   ACTION  : cli_process_ds_cmd (CliHandle, CLI_DS_CLASS_MAP, NULL,  $1 ); 
   SYNTAX  : class-map <class-map-number(1-65535)>
   PRVID   : 15
   HELP    : Configures a QoS class-map
   CXT_HELP : class-map Configures class map related information | 
              (1-65535) Class map number | 
              <CR> Configures a QoS class-map

   COMMAND : no class-map <short(1-65535)>  
   ACTION  : cli_process_ds_cmd (CliHandle, CLI_DS_NO_CLASS_MAP, NULL, $2); 
   SYNTAX  : no class-map <class-map-number(1-65535)>
   PRVID   : 15
   HELP    : Deleted a QoS class-map
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              class-map Class map related configuration | 
              (1-65535) Class map number | 
              <CR> Deleted a QoS class-map

   COMMAND : policy-map <short(1-65535)>  
   ACTION  : cli_process_ds_cmd (CliHandle, CLI_DS_POL_MAP, NULL, $1 ); 
   SYNTAX  : policy-map <policy-map-number(1-65535)>
   PRVID   : 15
   HELP    : Configures a QoS policy-map
   CXT_HELP : policy-map Configures policy map related information | 
              (1-65535) Policy map number | 
              <CR> Configures a QoS policy-map

   COMMAND : no policy-map <short(1-65535)>  
   ACTION  : cli_process_ds_cmd (CliHandle, CLI_DS_NO_POL_MAP, NULL, $2); 
   SYNTAX  : no policy-map <policy-map-number(1-65535)>
   PRVID   : 15
   HELP    : Deleted a QoS policy-map
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              policy-map Policy map related configuration | 
              (1-65535) Policy map number | 
              <CR> Deleted a QoS policy-map
#endif

END GROUP

DEFINE GROUP : DIFFSRV_CLASS_CMDS

#if defined (NPAPI_WANTED)

   COMMAND : match access-group { mac-access-list | ip-access-list } <short(1-65535)> 
   ACTION  : {
                 UINT1 u4FilterType = 0;
                 UINT1 *pu1Val = NULL;     

                 if ( $1 != NULL)
                 {
                     if ( $2 != NULL)
                     {
                         u4FilterType = DS_MAC_FILTER;
                         pu1Val = (UINT1 *)($4);
                     }
                     else if ($3 != NULL)
                     {
                         u4FilterType = DS_IP_FILTER;
                         pu1Val = (UINT1 *)($4);
                     }
                 }
                 cli_process_ds_cmd(CliHandle, CLI_DS_CLASS_MAP_MATCH, NULL, 
                                    u4FilterType, pu1Val);
             }

   SYNTAX  : match access-group { mac-access-list | ip-access-list } <acl-index-num (1-65535) > 
   PRVID   : 15
   HELP    : Configures the classification criteria for this class-map
   CXT_HELP : match Performs matching operation | 
              access-group Access group related configuration | 
              mac-access-list Access list is created based on MAC addresses for non-IP traffic | 
              ip-access-list Access list is created based on IP addresses | 
              (1-65535) ACL index number | 
              <CR> Configures the classification criteria for this class-map

  COMMAND  : exit
  ACTION   : {
               CLI_SET_CLASSMAP_ID(-1);
               CliChangePath("..");
            }
  SYNTAX   : exit
  HELP     : Return to Global configuraiton mode
  CXT_HELP : exit Exit to the global configuration (config)# mode | 
             <CR> Return to Global configuraiton mode
            
#endif            

END GROUP


DEFINE GROUP : DIFFSRV_POLICY_CMDS

#if defined (NPAPI_WANTED)

   COMMAND : class <short(1-65535)>  
   ACTION  : cli_process_ds_cmd(CliHandle,  CLI_DS_POLCL_MAP, NULL, $1); 
   SYNTAX  : class <class-map-number(1-65535)>
   PRVID   : 15
   HELP    : Configures policy criteria for the policy map
   CXT_HELP : class Configures the class related details | 
              (1-65535) Class map number | 
              <CR> Configures policy criteria for the policy map

   COMMAND : no class <short(1-65535)>  
   ACTION  : cli_process_ds_cmd(CliHandle,  CLI_DS_NO_POLCL_MAP, NULL, $2); 
   SYNTAX  : no class <class-map-number(1-65535)>
   PRVID   : 15
   HELP    : Deletes a policy criteria for the class map 
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              class Class related configuration | 
              (1-65535) Class map number | 
              <CR> Deletes a policy criteria for the class map

  COMMAND  : exit
  ACTION   : {
               CLI_SET_POLICYMAP_ID (-1);
		         CliChangePath("..");
	          }
  SYNTAX   : exit
  HELP     : Return to Global configuraiton mode
  CXT_HELP : exit Exit to the global configuration (config)# mode | 
             <CR> Return to Global configuraiton mode

#endif
  
END GROUP
 
DEFINE GROUP : DIFFSRV_POLCL_CMDS

#if defined (NPAPI_WANTED)

   COMMAND : set {cos <short(0-7)>| ip { dscp <short(0-63)> | precedence <short(0-7)>}}  
   ACTION  : {
                 UINT1	*pu1Value = NULL;
                 UINT4   u4Type  = 0;

                 if ($1 != NULL)
                 {
                     u4Type = DS_COS;
                     pu1Value  = (UINT1 *)($2);
                 }
                 else if ($3 != NULL)
                 {
                     if ($4 != NULL)
                     {
                     u4Type = DS_DSCP;
                     pu1Value  = (UINT1 *)($5);
                     }
                     else if ($6 != NULL)
                     {
                     u4Type = DS_PREC;
                     pu1Value  = (UINT1 *)($7);
                     }
                 }
                 cli_process_ds_cmd(CliHandle,  CLI_DS_POL_IN_ACTION, NULL,
                 u4Type, pu1Value);
             }
 
   SYNTAX  : set {cos <new-cos(0-7)> | ip dscp <new-dscp(0-63)> | ip precedence <new-precedence(0-7)>}
   PRVID   : 15
   HELP    : Configures the QoS values
	          cos 		     : IEEE 802.1Q/ISL class of service/user priority
             IP DSCP 	  : Set IP DSCP (DiffServ CodePoint)
		       IP precedence: Set IP Precedence value
   CXT_HELP : set Configures the parameters
   cos Class of Service related configuration
   (0-7) New CoS value assigned to the classified traffic
   ip IP related configuration
   dscp Differentiated services code point configuration
   (0-63) New DSCP value assigned to the classified traffic
   precedence Precedence related configuration
   (0-7) New precedence value assigned to the classified traffic
   <CR> Configures the QoS values.

   COMMAND : no set {cos <short(0-7)> | ip { dscp <short(0-63)> | precedence <short(0-7)>}}
   ACTION  : {
                 UINT1   u4Type  = 0;

                 if ($2 != NULL)
                 {
                     u4Type = DS_COS;
                 }
                 if ($5 != NULL)
                 {
                     u4Type = DS_DSCP;
                 }
                 if ($7 != NULL)
                 {
                     u4Type = DS_PREC;
                 }
                 cli_process_ds_cmd(CliHandle,  CLI_DS_NO_POL_IN_ACTION,
                 NULL, u4Type, 0);
             }
   SYNTAX  : no set {cos <new-cos(0-7)> | ip { dscp <new-dscp(0-63)> | precedence <new-precedence(0-7)>}}
   PRVID   : 15
   HELP    : Deletes the QoS values
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              set Configures the parameters | 
              cos Class of Service related configuration | 
              (0-7) New CoS value assigned to the classified traffic | 
              ip IP related configuration | 
              dscp Differentiated services code point configuration | 
              (0-63) New DSCP value assigned to the classified traffic | 
              precedence Precedence related configuration | 
              (0-7) New precedence value assigned to the classified traffic | 
              <CR> Deletes the QoS values

#if defined ( BCM5690_WANTED )

   COMMAND : police <short(1-1023)> exceed-action {drop | policed-dscp-transmit <short(0-63)>}
   
   ACTION  : {
               UINT4 u4Type=0;
               UINT1 *pu1Qosvalue =NULL;   	
               UINT1 *pu1TrafRate = NULL;

                  pu1TrafRate = (UINT1 *)($1);
                  if ($3 != NULL)
                  {
                      u4Type = DS_DROP;
                  }
                  else if ($4 != NULL)
                  {
                      u4Type = DS_DSCP;
                      pu1Qosvalue= (UINT1 *)($5);
                  }
                  cli_process_ds_cmd (CliHandle, CLI_DS_POLICE, NULL, 
                                   pu1TrafRate, u4Type, pu1Qosvalue);
	     }		    
   SYNTAX  : police  <rate-Mbps(1-1023)> exceed-action {drop | policed-dscp-transmit <new-dscp(0-63)>} 
   PRVID   : 15
   HELP    : Configure the rate, the out-of-profile action
   CXT_HELP : police Configures policer related information | 
              (1-1023) Average traffic rate in Mega bits per second | 
              exceed-action Switch action configuration | 
              drop Packet is dropped, when the rate is exceeded | 
              policed-dscp-transmit DSCP of the packet is changed to that specified in the policed-DSCP map, when the rate is exceeded | 
              (0-63) New DSCP value | 
              <CR> Configure the rate, the out-of-profile action

#endif

#if defined (BCM5695_WANTED) || defined (BCM5650X_WANTED) || defined (BCMX_WANTED) || defined (SWC)

   COMMAND : police  <integer(64-1048572)> exceed-action {drop | policed-dscp-transmit <short(0-63)>} 
   ACTION  : {
               UINT4 u4Type=0;
               UINT1 *pu1Qosvalue =NULL;   	
               UINT1 *pu1TrafRate = NULL;

                  pu1TrafRate = (UINT1 *)($1);
                  if ($3 != NULL)
                  {
                      u4Type = DS_DROP;
                  }
                  else if ($4 != NULL)
                  {
                      u4Type = DS_DSCP;
                      pu1Qosvalue= (UINT1 *)($5);
                  }
                  cli_process_ds_cmd (CliHandle, CLI_DS_POLICE, NULL, 
                                   pu1TrafRate, u4Type, pu1Qosvalue);
             }		    
   SYNTAX  : police  <rate-Kbps(64-1048572)> exceed-action {drop | policed-dscp-transmit <new-dscp(0-63)>}
   PRVID   : 15
   HELP    : Configure the rate in Kbps for the out-of-profile action 
   CXT_HELP : police  Configures policer related information | 
              (64-1048572) Average traffic rate in Kilo bits per second | 
              exceed-action Switch action configuration | 
              drop Packet is dropped, when the rate is exceeded | 
              policed-dscp-transmit DSCP of the packet is changed to that specified in the policed-DSCP map, when the rate is exceeded | 
              (0-63) New DSCP value | 
              <CR> Configure the rate in Kbps for the out-of-profile action

#endif

  COMMAND  : exit
  ACTION   : {
               INT4 i4PolIndex;

               i4PolIndex = CLI_GET_POCL_MAP_ID ();
               CLI_SET_POLICYMAP_ID (i4PolIndex);
		         CliChangePath("..");
             }
  SYNTAX   : exit
  HELP     : Return to Policy Map configuraiton mode
  CXT_HELP : exit Exits to the  policy-map configuration (config-pmap)# mode | 
             <CR> Return to Policy Map configuraiton mode
  
#endif
  
END GROUP 

DEFINE GROUP : DIFFSRV_IFACE_CMDS

#if defined (NPAPI_WANTED)

   COMMAND : cosq scheduling algorithm { strict | rr | wrr | wfq | strict-rr | strict-wrr | strict-wfq | deficit }
   ACTION  :{
                UINT4   u4CosqAlgo = 0;

                if ($3 != NULL)
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_STRICT;
                }
                else if ($4 != NULL)
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_RR;
                }
                else if($5 != NULL )
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_WRR;
                }
                else if($6 != NULL )
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_WFQ;
                }
                else if($7 != NULL )
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_STRICT_RR;
                }
                else if($8 != NULL )
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_STRICT_WRR;
                }
                else if($9 != NULL )
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_STRICT_WFQ;
                }
                else if($10 != NULL )
                {
                    u4CosqAlgo = DS_COSQ_SCHE_ALGO_DEFICIT;
                }
                cli_process_ds_cmd (CliHandle, CLI_DS_COSQ_ALGO,
                                       NULL, &u4CosqAlgo);
            }

   SYNTAX  : cosq scheduling algorithm { strict | rr | wrr | wfq | strict-rr | strict-wrr | strict-wfq | deficit }
   PRVID   : 15
   HELP    : Sets cosq scheduling algorithm.
   CXT_HELP : cosq Configures class of service queue related information | 
              scheduling Scheduling algorithm related configuration | 
              algorithm Algorithm related configuration | 
              strictStrict scheduling algorithm | 
              rr Round robin algorithm | 
              wrr Weighted round robin algorithm | 
              wfq Weighted fair queing algorithm | 
              strict-rr Strict round robin algorithm | 
              strict-wrr Strict weighted round robin algorithm | 
              strict-wfq Strict weighted fair queing algorithm | 
              deficit Deficit algorithm | 
              <CR> Sets cosq scheduling algorithm

   COMMAND : traffic-class <integer(0-7)> weight <integer(0-15)> [ minbandwidth <integer(1-262143)>]
   ACTION  :{
               if ($4 != NULL)
               {
                   cli_process_ds_cmd (CliHandle, CLI_DS_COSQ_WEIGHT_BW, NULL, $1, $3, $5);
               }
               else
               {
                   cli_process_ds_cmd (CliHandle, CLI_DS_COSQ_WEIGHT_BW, NULL, $1, $3, NULL);
               }
            }

   SYNTAX  : traffic-class <integer(0-7)> weight <integer(0-15)> [ minbandwidth <integer(1-262143)>]
   PRVID   : 15
   HELP    : Sets weight and bandwidth for traffic classes.
   CXT_HELP : traffic-class Configures traffic class related information | 
              (0-7)CoSq number | 
              weight Weight related configuration | 
              (0-15) CoSq weight | 
              minbandwidth Minimum bandwidth configuration | 
              (1-262143) Minimum bandwidth | 
              <CR> Sets weight and bandwidth for traffic classes
                                                                     
#endif

END GROUP

DEFINE GROUP : DIFFSRV_EXEC_CMDS

#if defined (NPAPI_WANTED)

COMMAND : show policy-map [<short(1-65535)> [class <short(1-65535)>] ] 
ACTION  : cli_process_ds_cmd ( CliHandle, CLI_SHOW_DS_POLICYMAP, NULL, $2, $4);
SYNTAX  : show policy-map [<policy-map-num(1-65535)> [class <class-map-num(1-65535)>]  
PRVID   : 15
HELP    : Displays policy-map information
CXT_HELP : show Displays the configuration / statistics / general information | 
           policy-map Policy map related configuration | 
           (1-65535) Policy map number | 
           class Class related configuration | 
           (1-65535) Class map number | 
           <CR> Displays policy-map information

COMMAND : show class-map [<short(1-65535)>]  
ACTION  : cli_process_ds_cmd ( CliHandle, CLI_SHOW_DS_CLASSMAP, NULL, $2 );
SYNTAX  : show class-map [<class-map-num(1-65535)>]  
PRVID   : 15
HELP    : Displays class-map information
CXT_HELP : show Displays the configuration / statistics / general information | 
           class-map Class map related configuration | 
           (1-65535) Class map number | 
           <CR> Displays class-map information

COMMAND : show cosq algorithm [interface <ifXtype> <ifnum>]
ACTION  : {
              UINT4 u4PortId = 0;
              if ($3 != NULL)
              {
                  if (CfaCliGetIfIndex ($4, $5, &u4PortId) == CLI_FAILURE)
                  {
                      CliPrintf (CliHandle, "%% Invalid Interface \r\n");
                      return (CLI_FAILURE);
                  }
              }

              cli_process_ds_cmd (CliHandle, CLI_SHOW_DS_COSQ_ALGO, NULL, &u4PortId);
          }
SYNTAX  : show cosq algorithm [ interface <interface-type> <interface-id> ]
PRVID   : 15
HELP    : Displays the CoSq algorithm used for the interface
CXT_HELP : show Displays the configuration / statistics / general information | 
           cosq Class of service queue related configuration | 
           algorithm Algorithm related configuration | 
           interface Interface related configuration | 
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type | 
           <ifnum> Interface number (Example: 0/1) | 
           <CR> Displays the CoSq algorithm used for the interface

COMMAND : show cosq weights-bw [interface <ifXtype> <ifnum> ]
ACTION  : {
              UINT4 u4PortId = 0;
              if ($3 != NULL)
              {
                  if (CfaCliGetIfIndex ($4, $5, &u4PortId) == CLI_FAILURE)
                  {
                      CliPrintf (CliHandle, "%% Invalid Interface \r\n");
                      return (CLI_FAILURE);
                  }
              }

              cli_process_ds_cmd (CliHandle, CLI_SHOW_DS_COSQ_WEIGHT_BW, NULL, &u4PortId);
          }
SYNTAX  : show cosq weights-bw [ interface <interface-type> <interface-id> ]
PRVID   : 15
HELP    : Displays the CoSq weights and the bandwidth for the interface
CXT_HELP : show Displays the configuration / statistics / general information | 
           cosq Class of service queue related configuration | 
           weights-bw Weights and bandwidth related configuration | 
           interface Interface related configuration | 
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type | 
           <ifnum> Interface number (Example: 0/1) | 
           <CR> Displays the CoSq weights and the bandwidth for the interface

#endif

END GROUP
