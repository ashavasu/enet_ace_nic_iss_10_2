#ifndef __QOS_CLI_C__
#define __QOS_CLI_C__

/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                                  */
/* $Id: qoscli.c,v 1.5 2013/02/15 13:24:54 siva Exp $*/
/* Licensee Aricent Inc., 2001-2002                         */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qoscli.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-CLI                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This module provide the CLI Mangement Interface */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#include "qosinc.h"
#include "qosmrvlscli.h"

/*****************************************************************************/
/* Function Name      : cli_process_qos_cmd                                  */
/* Description        : This function is Classify the Given Command and      */
/*                      configure the QoS module through CLI using setof     */
/*                      nmh routines.                                        */
/* Input(s)           : CliHandle - Handle to CLI session                    */
/*                    : u4Command - Command Type to Classify.                */
/*                    : ...       - Variable number of Arg for the above     */
/*                    :             Command type if possible.                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gapi1QoSCliErrString.                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Thread.                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
PUBLIC INT4
cli_process_qos_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *apu1Args[QOS_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    tMacAddr            MacAddr;
    INT4                i4Inst = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    UNUSED_PARAM (i4IfIndex);

    MEMSET (apu1Args, 0, QOS_CLI_MAX_ARGS);
    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
    }

    /* Walk through the rest of the arguments and store in apu1Args array. */
    while (1)
    {
        apu1Args[i1ArgNo++] = va_arg (ap, UINT1 *);

        if (i1ArgNo == QOS_CLI_MAX_ARGS)
        {
            break;
        }

    }
    va_end (ap);

    /* Register Lock with CLI */
    CliRegisterLock (CliHandle, QoSLock, QoSUnLock);
    QoSLock ();

    switch (u4Command)
    {
        case CLI_QOS_SET_SYS_CNTRL_DISABLE:

            i4RetStatus = QoSCliSetSysControl (CliHandle,
                                               QOS_SYS_CNTL_SHUTDOWN);
            break;

        case CLI_QOS_SET_SYS_CNTRL_ENABLE:

            i4RetStatus = QoSCliSetSysControl (CliHandle, QOS_SYS_CNTL_START);

            break;

        case CLI_QOS_SET_SYS_STATUS:
            /* apu1Args[0] = enable */
            /* apu1Args[0] = disable */
            if (apu1Args[0] != NULL)
            {
                i4RetStatus = QoSCliSetStatus (CliHandle,
                                               QOS_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = QoSCliSetStatus (CliHandle,
                                               QOS_SYS_STATUS_DISABLE);
            }

            break;

        case CLI_QOS_ADD_PRI_MAP:
            /* apu1Args[0] = priority-map-id */
            i4RetStatus = QoSCliAddPriMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu1Args[0]));

            break;

        case CLI_QOS_SET_PRI_MAP_PARAMS:
            /* apu1Args[0] = IfNum     */
            /* apu1Args[1] = InPriType */
            /* apu1Args[2] = InPri     */
            /* apu1Args[3] = ReGenPri  */
            i4RetStatus = QoSCliSetPriMapParams (CliHandle,
                                                 (CLI_PTR_TO_U4 (apu1Args[0])),
                                                 (UINT1) (CLI_PTR_TO_U4
                                                          (apu1Args[1])),
                                                 (UINT1) (CLI_PTR_TO_U4
                                                          (apu1Args[2])),
                                                 (UINT1) (CLI_PTR_TO_U4
                                                          (apu1Args[3])));
            break;

        case CLI_QOS_SET_PRIORITY_MAP_PARAMS:
            /* apu1Args[0] = InPriType */
            /* apu1Args[1] = Mac     */
            /* apu1Args[2] = ReGenPri  */
            /* apu1Args[3] = VlanId  */
            if (apu1Args[1] != NULL)
            {
                StrToMac (apu1Args[1], MacAddr);
            }

            i4RetStatus = QoSCliSetPriorityMapParams (CliHandle,
                                                      (CLI_PTR_TO_I4
                                                       (apu1Args[0])), MacAddr,
                                                      (UINT1) (CLI_PTR_TO_U4
                                                               (apu1Args[2])),
                                                      (UINT4) (CLI_PTR_TO_U4
                                                               (apu1Args[3])));
            break;

        case CLI_QOS_SHOW_PRI_MAP:
            /* apu1Args[0] = priority-map-id */
            i4RetStatus = QoSCliShowPriMapEntry (CliHandle,
                                                 CLI_PTR_TO_U4 (apu1Args[0]));

            break;

        case CLI_QOS_DEL_PRI_MAP:
            /* apu1Args[0] = priority-map-id */
            i4RetStatus = QoSCliDelPriMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu1Args[0]));

            break;

        case CLI_QOS_SHOW_GLB_INFO:
            i4RetStatus = QoSCliShowGlobalInfo (CliHandle);

            break;

        case CLI_QOS_ADD_QMAP:
            /* apu1Args[0] = i4MapType   */
            /* apu1Args[1] = u4MapVal    */
            /* apu1Args[2] = u4QIdx      */
            i4RetStatus = QoSCliAddQMapEntry (CliHandle,
                                              CLI_PTR_TO_I4 (apu1Args[0]),
                                              CLI_PTR_TO_U4 (apu1Args[1]),
                                              CLI_PTR_TO_U4 (apu1Args[2]));

            break;

        case CLI_QOS_DEFAULT_QMAP:
            /* apu1Args[0] = i4MapType   */
            /* apu1Args[1] = u4MapVal    */
            i4RetStatus = QoSCliDefaultQMapEntry (CliHandle,
                                                  CLI_PTR_TO_I4 (apu1Args[0]),
                                                  CLI_PTR_TO_U4 (apu1Args[1]));

            break;

        case CLI_QOS_SHOW_QMAP:
            /* apu1Args[0] = u4IfIndex */
            i4RetStatus = QoSCliShowQMapEntry (CliHandle,
                                               CLI_PTR_TO_I4 (apu1Args[0]));
            break;

        case CLI_QOS_SCHED_ALGO:
            /* apu1Args[0] = u4CosqAlgo */
            i4RetStatus = QoSCliSetSchedulingPloicy (CliHandle,
                                                     CLI_PTR_TO_U4 (apu1Args
                                                                    [0]));
            break;

        case CLI_QOS_SHOW_SCHED_ALGO:

            i4RetStatus = QoSCliShowSchedulingPolicy (CliHandle);
            break;

        case CLI_QOS_MATCH_PRI:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                QosCliPortPriortyConfig (CliHandle, u4IfIndex,
                                         CLI_PTR_TO_I4 (apu1Args[0]));
            break;

        case CLI_QOS_NO_MATCH_PRI:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                QosCliPortNoPriortyConfig (CliHandle, u4IfIndex,
                                           CLI_PTR_TO_I4 (apu1Args[0]));
            break;

        case CLI_QOS_OVERRIDE_PRIORITY:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                QosCliPortOverrideConfig (CliHandle, u4IfIndex,
                                          CLI_PTR_TO_I4 (apu1Args[0]));
            break;

        case CLI_QOS_NO_OVERRIDE_PRIORITY:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                QosCliPortNoOverrideConfig (CliHandle, u4IfIndex,
                                            CLI_PTR_TO_I4 (apu1Args[0]));
            break;

        case CLI_QOS_TAGIFBOTH_MATCH_PRI:

            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                QosCliPortVlanIPPriorityConfig (CliHandle, u4IfIndex,
                                                CLI_PTR_TO_I4 (apu1Args[0]));
            break;

        case CLI_QOS_SHOW_PORT_CONFIG:

            i4RetStatus =
                QosCliShowPortConfig (CliHandle, CLI_PTR_TO_U4 (apu1Args[0]));
            break;

        case CLI_QOS_SHOW_PORT_OVERRIDE_CONFIG:

            i4RetStatus =
                QosCliShowPortOverrideConfig (CliHandle,
                                              CLI_PTR_TO_U4 (apu1Args[0]));
            break;

        default:

            CliPrintf (CliHandle, "\r%% Unknown QoS command. \r\n");
            QoSUnLock ();
            CliUnRegisterLock (CliHandle);

            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if (u4ErrCode > 0)
        {
            CliPrintf (CliHandle, "\r%% %s", gapi1QoSCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);
    QoSUnLock ();
    CliUnRegisterLock (CliHandle);

    return (i4RetStatus);

}                                /* End of  cli_process_qos_cmd () */

/*****************************************************************************/
/* Function Name      : QoSCliSetSysControl                                  */
/* Description        : This function is used to set the System Contol of    */
/*                      the QoS Module as Start or Shutdown.                 */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4SysControl - Start  / Shutdown                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetSysControl (tCliHandle CliHandle, INT4 i4SysControl)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    i4RetStatus = nmhTestv2FsQoSSystemControl (&u4ErrorCode, i4SysControl);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSSystemControl (i4SysControl);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetStatus                                      */
/* Description        : This function is used to set the System status of    */
/*                      the QoS Module as Enable or Disable.                 */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4SysStatus  - Enable / Disable                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetStatus (tCliHandle CliHandle, INT4 i4SysStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    i4RetStatus = nmhTestv2FsQoSStatus (&u4ErrorCode, i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSStatus (i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliShowGlobalInfo                                 */
/* Description        : This function is used to get the System Global       */
/*                      informations of QoS module.                          */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliShowGlobalInfo (tCliHandle CliHandle)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4SysControl = 0;
    INT4                i4SysStatus = 0;
    UINT4               u4TrcFlag = 0;

    i4RetStatus = nmhGetFsQoSSystemControl (&i4SysControl);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSStatus (&i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSTrcFlag (&u4TrcFlag);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "QoS Global Information \r\n");
    CliPrintf (CliHandle, "---------------------- \r\n");

    if (i4SysControl == QOS_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Control", "Start");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Control", "Shutdown");
    }

    if (i4SysStatus == QOS_SYS_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Control", "Enable");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Control", "Disable");
    }

    CliPrintf (CliHandle, "%-28s : %d\r\n", "Trace Flag", u4TrcFlag);
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSPriMapPrompt                                      */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Priority map Table                                   */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSPriMapPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_PRI_MAP_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_PRI_MAP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_PRI_MAP_ID (u4Idx);
    STRCPY (pi1PromptStr, "(config-pri-map)#");

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliGetPriMapIndexFromName                         */
/* Description        : This function is used to get the Table Index from    */
/*                      the Given Table Name. If Name is not found it give   */
/*                      the next free Index                                  */
/* Input(s)           : pInOctStrName - Table Name.                          */
/*                    : pu4Idx        - Ptr to the Index of the Table for    */
/*                    :                 the given Name                       */
/*                    : pu4Status     - Status says that the  Entry needs to */
/*                    :                 be create or modify                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : PriMapTble Functions                                 */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliGetPriMapIndexFromName (tSNMP_OCTET_STRING_TYPE * pInOctStrName,
                              UINT4 *pu4Idx, UINT4 *pu4Status)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    UINT4               u4CurrIdx = 0;
    UINT4               u4PreIdx = 0;
    UINT4               u4IdxFound = CLI_FAILURE;
    INT4                i4SysControl = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* System Contol map be QOS_SYS_CNTL_SHUTDOWN */
    i4RetStatus = nmhGetFsQoSSystemControl (&i4SysControl);
    if ((i4RetStatus == SNMP_FAILURE)
        || (i4SysControl == QOS_SYS_CNTL_SHUTDOWN))
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (CLI_FAILURE);
    }

    /* No Entry Found Create first Entry  idx = 1 */
    i4RetStatus = nmhGetFirstIndexFsQoSPriorityMapTable (&u4CurrIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        *pu4Idx = 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
        return (CLI_SUCCESS);
    }

    /* Allocate Memory For Name */
    pOctStrName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
    if (pOctStrName == NULL)
    {
        return (CLI_FAILURE);
    }
    MEMSET (pOctStrName->pu1_OctetList, 0, pOctStrName->i4_Length);

    do
    {
        /* Get the Name of the Current Entry */
        i4RetStatus = nmhGetFsQoSPriorityMapName (u4CurrIdx, pOctStrName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrName);
            return (CLI_FAILURE);
        }

        if (STRCMP (pOctStrName->pu1_OctetList, pInOctStrName->pu1_OctetList)
            == 0)
        {
            *pu4Idx = u4CurrIdx;
            *pu4Status = QOS_CLI_MODIFY_ENTRY;

            free_octetstring (pOctStrName);
            return (CLI_SUCCESS);
        }

        if (((u4CurrIdx - u4PreIdx) > 1) && (u4IdxFound == CLI_FAILURE))
        {
            *pu4Idx = u4PreIdx + 1;
            *pu4Status = QOS_CLI_NEW_ENTRY;
            u4IdxFound = CLI_SUCCESS;
        }

        u4PreIdx = u4CurrIdx;

        i4RetStatus = nmhGetNextIndexFsQoSPriorityMapTable (u4CurrIdx,
                                                            &u4CurrIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    if ((u4CurrIdx == u4PreIdx) && (u4IdxFound == CLI_FAILURE))
    {
        *pu4Idx = u4CurrIdx + 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
    }

    free_octetstring (pOctStrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddPriMapEntry                                 */
/* Description        : This function is used to create Priority Map table   */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Create.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1PriMap[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode,
                                                       u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the Priority Map Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1PriMap, "%s%d", QOS_CLI_PRI_MAP_MODE, u4Idx);

    if (CliChangePath ((CHR1 *) au1PriMap) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Priority Map mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliDelPriMapEntry                                 */
/* Description        : This function is used to delete Priority Map table   */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Delete.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode, u4Idx,
                                                   DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetPriMapParams                                */
/* Description        : This function is used to Set Priority Map table Entry*/
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4IfNum      - Interface Name.                       */
/*                    : u4VlanId     - Vlan Id                               */
/*                    : u1InType  - Incomming Priority Type               */
/*                    : u1InPri      - Incomming Priority Value              */
/*                    : u1TrafCls    - Regene Priority Value                 */
/*                    : i1InnerReGenPri - Inner Regene Priority Value        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSPriMapTblEntry (UINT4 u4Idx, UINT4 u4CurIfNum,
                        UINT4 u4CurInPriType, UINT4 u4CurInPri,
                        UINT4 u4CurRegenPri, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (u4CurIfNum != CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, u4CurIfNum);
    }

    if (u4CurInPriType != CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapInPriType (u4Idx, u4CurInPriType);
    }

    if (u4CurInPri != CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapInPriority (u4Idx, u4CurInPri);
    }

    if (u4CurRegenPri != CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapRegenPriority (u4Idx,
                                                           u4CurRegenPri);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, i4RowStatus);
}

INT4
QoSCliSetPriMapParams (tCliHandle CliHandle, UINT4 u4IfNum, UINT1 u1InType,
                       UINT1 u1InPri, UINT1 u1TrafCls)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UINT4               u4CurIfNum = CLI_RS_INVALID_MASK;
    UINT4               u4CurInPriType = CLI_RS_INVALID_MASK;
    UINT4               u4CurInPri = CLI_RS_INVALID_MASK;
    UINT4               u4CurRegenPri = CLI_RS_INVALID_MASK;

    /* Get the Priority Map Id */
    u4Idx = CLI_GET_PRI_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode,
                                                       u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /*
     * 1. Test New value 
     * 2. Get Current value
     * 3. Set New value
     * 4. if Set Failed Restore Old Value else goto #1
     */

    if (u4IfNum != 0)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapIfIndex (&u4ErrorCode, u4Idx,
                                                        u4IfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurInPriType,
                                    u4CurInPri, u4CurRegenPri, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4CurIfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, u4IfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapInPriType (&u4ErrorCode, u4Idx,
                                                      u1InType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriType (u4Idx,
                                                   (INT4 *) &u4CurInPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapInPriType (u4Idx, u1InType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapInPriority (&u4ErrorCode, u4Idx,
                                                       u1InPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus =
        nmhGetFsQoSPriorityMapInPriority (u4Idx, (INT4 *) &u4CurInPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapInPriority (u4Idx, u1InPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapRegenPriority (&u4ErrorCode, u4Idx,
                                                          u1TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4CurRegenPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapRegenPriority (u4Idx, u1TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Set Entry as ACTIVE */
    i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

INT4
QoSCliSetPriorityMapParams (tCliHandle CliHandle, INT4 i4InType,
                            tMacAddr MacAddr, UINT1 u1RegenPriorty,
                            UINT4 u4VlanId)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UINT4               u4CurInPriType = CLI_RS_INVALID_MASK;
    UINT4               u4CurInVlanId = CLI_RS_INVALID_MASK;
    UINT4               u4CurRegenPri = CLI_RS_INVALID_MASK;
    tMacAddr            CurMacAddress;

    /* Get the Priority Map Id */
    u4Idx = CLI_GET_PRI_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode,
                                                       u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /*
     * 1. Test New value 
     * 2. Get Current value
     * 3. Set New value
     * 4. if Set Failed Restore Old Value else goto #1
     */
    i4RetStatus = nmhTestv2FsQoSPriorityMapVlanId (&u4ErrorCode, u4Idx,
                                                   u4VlanId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4CurInVlanId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapVlanId (u4Idx, u4VlanId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4InType == QOS_CLI_IN_PRI_TYPE_SRC_MAC ||
        i4InType == QOS_CLI_IN_PRI_TYPE_DEST_MAC)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapMacAddress (&u4ErrorCode, u4Idx,
                                                           MacAddr);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapMacAddress (u4Idx, &CurMacAddress);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapMacAddress (u4Idx, MacAddr);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    i4RetStatus = nmhTestv2FsQoSPriorityMapInPriType (&u4ErrorCode, u4Idx,
                                                      i4InType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriType (u4Idx,
                                                   (INT4 *) &u4CurInPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapInPriType (u4Idx, i4InType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapRegenPriority (&u4ErrorCode, u4Idx,
                                                          u1RegenPriorty);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4CurRegenPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapRegenPriority (u4Idx, u1RegenPriorty);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Set Entry as ACTIVE */
    i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliNoSetPriMapParams                              */
/* Description        : This function is used to Set default value for       */
/*                      an Entry in the Priority Map Table                   */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Interface  - Interface                             */
/*                    : u4Vlan       - Vlan Id                               */
/*                    : u4InnerReGenPri - Inner Regene Priority Value        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/

VOID
QoSCliRSNoSetPriMapParams (UINT4 u4Idx, UINT4 u4CurIfNum,
                           UINT4 u4CurInRegenPri, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UNUSED_PARAM (u4CurInRegenPri);

    if (u4CurIfNum != CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, u4CurIfNum);
    }
    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, i4RowStatus);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayPriMapEntry                          */
/* Description        : This function is used to Display the Priority Map    */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowPriMapEntry                                */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4IfNum = 0;
    UINT4               u4TrafCls = 0;
    INT4                i4InPri = 0;
    INT4                i4InPriType = 0;
    UINT4               u4VlanId = 0;
    tMacAddr            MacAddress;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[QOS_CLI_MAX_MAC_STRING_SIZE];
    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4IfNum);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (u4IfNum, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriType (u4Idx, &i4InPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriority (u4Idx, &i4InPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4VlanId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapMacAddress (u4Idx, &MacAddress);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    PrintMacAddress (MacAddress, au1String);
    i4RetStatus = nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "%-28s : %d\r\n", "PriorityMapId", u4Idx);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "IfIndex", au1IfName);

    switch (i4InPriType)
    {
        case QOS_CLI_IN_PRI_TYPE_VALN_PRI:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InType", "VlanPriority");
            CliPrintf (CliHandle, "%-28s : %d\r\n", "InPriority", i4InPri);

            break;

        case QOS_CLI_IN_PRI_TYPE_VLAN_ID:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InType", "Vlan-ID");
            CliPrintf (CliHandle, "%-28s : %d\r\n", "VlanId", u4VlanId);
            break;

        case QOS_CLI_IN_PRI_TYPE_SRC_MAC:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InType", "Src-Mac");
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Src-Mac", au1String);
            break;

        case QOS_CLI_IN_PRI_TYPE_DEST_MAC:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InType", "Dest-Mac");
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Dest-Mac", au1String);
            break;

        default:
            CliPrintf (CliHandle, "%-28s : %s --- %d \r\n", "InType",
                       "Invalid", i4InPriType);
            break;
    }

    CliPrintf (CliHandle, "%-28s : %d\r\n", "RegenPriority", u4TrafCls);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowPriMapEntry                                */
/* Description        : This function is used to Display the Priority Map    */
/*                      Table Entries Parameters for the name pu1Name, if    */
/*                      it is NULL then All Entries will be displayed        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Display           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayPriMapEntry                          */
/*****************************************************************************/
INT4
QoSCliShowPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Priority Map Entries\r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSPriorityMapTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Check the Status */
            i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayPriMapEntry (CliHandle, u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            i4RetStatus = nmhGetNextIndexFsQoSPriorityMapTable (u4Idx, &u4Idx);

        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayPriMapEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddQMapEntry                                   */
/* Description        : This function is used to create QueueMap table Entry */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : i4MapType    - MapTye   CLASS or PRI_TYPE            */
/*                    : u4MapVal     - Value for the Type                    */
/*                    : u4QId        - Q Id for the Map                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSQMapEntry (INT4 i4MapType,
                   UINT4 u4PriVal, UINT4 u4CurQIdx, INT4 i4RowStatus)
{

    INT4                i4RetStatus = SNMP_FAILURE;
    if (u4CurQIdx != CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQMapQId (i4MapType, u4PriVal, u4CurQIdx);
    }
    UNUSED_PARAM (i4RowStatus);

}

INT4
QoSCliAddQMapEntry (tCliHandle CliHandle, INT4 i4MapType,
                    UINT4 u4MapVal, UINT4 u4QIdx)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4PriVal = 0;

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */

    u4PriVal = u4MapVal;

    i4RetStatus = nmhTestv2FsQoSQMapQId (&u4ErrorCode,
                                         i4MapType, u4PriVal, u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQMapQId (i4MapType, u4PriVal, u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

INT4
QoSCliDefaultQMapEntry (tCliHandle CliHandle, INT4 i4MapType, UINT4 u4MapVal)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4PriVal = 0, u4QIdx = 0;

    UINT1               au1QId[DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE] =
        { 1, 0, 0, 1, 2, 2, 3, 3 };

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */

    u4PriVal = u4MapVal;

    if (i4MapType == QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI)
        u4QIdx = au1QId[u4MapVal];
    else
    {
        if (u4MapVal <= QOS_ASSIGN_TRAFFIC_CLASS_0)
        {
            u4QIdx = 0;
        }
        else if (u4MapVal <= QOS_ASSIGN_TRAFFIC_CLASS_1)
        {
            u4QIdx = 1;
        }
        else if (u4MapVal <= QOS_ASSIGN_TRAFFIC_CLASS_2)
        {
            u4QIdx = 2;
        }
        else if (u4MapVal <= QOS_ASSIGN_TRAFFIC_CLASS_3)
        {
            u4QIdx = 3;
        }

    }

    i4RetStatus = nmhTestv2FsQoSQMapQId (&u4ErrorCode,
                                         i4MapType, u4PriVal, u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQMapQId (i4MapType, u4PriVal, u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowQMapEntry                                  */
/* Description        : This function is used to Display Q Map table Entry   */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliShowQMapEntry (tCliHandle CliHandle, INT4 i4IfIdx)
{
    UINT4               u4PriVal = 0;
    INT4                i4PriType = 0;
    UINT4               u4QId = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Queue Map Entries\r\n");
    CliPrintf (CliHandle, "---------------------\r\n");

    CliPrintf (CliHandle, "%-15s %-15s %-15s \r\n",
               "PriorityType", "Priority Value", "Mapped Queue");
    CliPrintf (CliHandle, "%-15s %-15s %-15s \r\n",
               "---------------", "---------------", "---------------");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSQMapTable (&i4PriType, &u4PriVal);

    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {

        if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
        {
            /* Display Entry */
            i4RetStatus = nmhGetFsQoSQMapQId (i4PriType, u4PriVal, &u4QId);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            switch (i4PriType)
            {

                case QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI:
                    CliPrintf (CliHandle, "%-15s %-15d %-15d"
                               "\r\n", "VlanPri", u4PriVal, u4QId);
                    break;

                case QOS_CLI_QMAP_PRI_TYPE_IP_DSCP:
                    CliPrintf (CliHandle, "%-15s %-15d %-15d"
                               "\r\n", "IpDscp", u4PriVal, u4QId);
                    break;

                default:
                    CliPrintf (CliHandle, "%-15s %-15d %-15d"
                               "\r\n", "Invalid", u4PriVal, u4QId);
                    break;
            }
        }

        i4RetStatus = nmhGetNextIndexFsQoSQMapTable (i4PriType, &i4PriType,
                                                     u4PriVal, &u4PriVal);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : QoSCliSetSchedulingAlog                  */
/*  Description     : This function is used to configure the   */
/*                    packet scheduling algo. for device       */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    i4SchedAlgo - Scheduling Algo.           */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
QoSCliSetSchedulingPloicy (tCliHandle CliHandle, INT4 i4SchedAlgo)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsQosSchedulingAlgorithm (&u4ErrorCode, i4SchedAlgo)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsQosSchedulingAlgorithm (i4SchedAlgo) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : QoSCliShowSchedulingAlog                 */
/*  Description     : This function is used to displasy  the   */
/*                    packet scheduling algo. for device       */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    i4SchedAlgo - Scheduling Algo.           */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
QoSCliShowSchedulingPolicy (tCliHandle CliHandle)
{
    INT4                i4SchAlgo;

    nmhGetFsQosSchedulingAlgorithm (&i4SchAlgo);

    if (i4SchAlgo == QOS_STRICT_PRIORITY)
    {
        CliPrintf (CliHandle, "\r\nScheduling Policy: Strict Priority\r\n");
    }
    else if (i4SchAlgo == QOS_WEIGHTED_ROUND_ROBIN)
    {
        CliPrintf (CliHandle,
                   "\r\nScheduling Policy: Weighted Round Robin\r\n");
    }
    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : QosCliPortPriortyConfig                     */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliPortPriortyConfig (tCliHandle CliHandle, UINT4 u4PortId, INT4 i4Type)
{
    if (i4Type == QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI)
    {
        if (nmhSetFsQoSVlanPriPriority (u4PortId, QOS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetFsQoSIPDscpPriority (u4PortId, QOS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : QosCliPortNoPriortyConfig                   */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliPortNoPriortyConfig (tCliHandle CliHandle, UINT4 u4PortId, INT4 i4Type)
{
    if (i4Type == QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI)
    {
        if (nmhSetFsQoSVlanPriPriority (u4PortId, QOS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetFsQoSIPDscpPriority (u4PortId, QOS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : QosCliPortOverrideConfig                    */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliPortOverrideConfig (tCliHandle CliHandle, UINT4 u4PortId, INT4 i4Type)
{

    if (i4Type == QOS_CLI_QMAP_PRI_TYPE_VLAN_ID)
    {
        if (nmhSetFsQoSVIDOverride (u4PortId, QOS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (i4Type == QOS_CLI_QMAP_PRI_TYPE_SRC_MAC)
    {
        if (nmhSetFsQoSSAOverride (u4PortId, QOS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetFsQoSDAOverride (u4PortId, QOS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : QosCliPortNoOverrideConfig                  */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliPortNoOverrideConfig (tCliHandle CliHandle, UINT4 u4PortId, INT4 i4Type)
{
    if (i4Type == QOS_CLI_QMAP_PRI_TYPE_VLAN_ID)
    {
        if (nmhSetFsQoSVIDOverride (u4PortId, QOS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (i4Type == QOS_CLI_QMAP_PRI_TYPE_SRC_MAC)
    {
        if (nmhSetFsQoSSAOverride (u4PortId, QOS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetFsQoSDAOverride (u4PortId, QOS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : QosCliPortVlanIPPriorityConfig              */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliPortVlanIPPriorityConfig (tCliHandle CliHandle, UINT4 u4PortId,
                                INT4 i4Type)
{
    if (i4Type == QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI)
    {
        if (nmhSetFsQoSVlanIPPriority (u4PortId, QOS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetFsQoSVlanIPPriority (u4PortId, QOS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : CliQosShowPortConfig                        */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliShowPortConfig (tCliHandle CliHandle, UINT4 u4PortId)
{
    UINT4               u4PrevIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IEEEPriority, i4IPPriority, i4IEEEIPPriority;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1NameStr;

    if (u4PortId > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface.\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPriority Config Table \r\n");

    CliPrintf (CliHandle, "--------------------- \r\n");

    CliPrintf (CliHandle, "Port   VlanPri   IpDscp    Vlan/IP \r\n");

    CliPrintf (CliHandle, "----   -------   ------    ------- \r\n");

    if (u4PortId == 0)
    {
        /* For all ports */
        if (nmhGetFirstIndexFsQosPortConfigTable (&u4PortId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        /* 
         * Show for a specific Port. 
         *
         * Setting the Current indices to the values to the previous 
         * element such that the Get Next operation will fetch the next 
         * element in the lexicographic order.
         */
        if (nmhValidateIndexInstanceFsQosPortConfigTable (u4PortId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u1isShowAll = FALSE;
    }
    do
    {
        CfaCliGetIfName (u4PortId, pi1IfName);

        nmhGetFsQoSVlanPriPriority (u4PortId, &i4IEEEPriority);
        nmhGetFsQoSIPDscpPriority (u4PortId, &i4IPPriority);
        nmhGetFsQoSVlanIPPriority (u4PortId, &i4IEEEIPPriority);

        CliPrintf (CliHandle, "%-4s  ", au1NameStr);
        if (i4IEEEPriority == QOS_ENABLE)
        {
            CliPrintf (CliHandle, "%-8s  ", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s  ", "Disabled");
        }
        if (i4IPPriority == QOS_ENABLE)
        {
            CliPrintf (CliHandle, "%-8s  ", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s  ", "Disabled");
        }
        if (i4IEEEIPPriority == QOS_ENABLE)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%s\r\n", "VlanPri");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "%s\r\n", "IpDscp");
        }

        if (u1isShowAll == FALSE)
        {
            /* Done with show for particular interface
             ** no need to continue */
            break;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, exit */
            break;
        }
        u4PrevIndex = u4PortId;
    }
    while (nmhGetNextIndexFsQosPortConfigTable (u4PrevIndex, &u4PortId)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/******************************************************************/
/*  Function Name   : QosCliShowPortOverrideConfig                */
/*  Description     : This function is used to display            */
/*                    Current Port Configuration                  */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4PortId    - Port on which Systemcontrol   */
/*                                  is applied.                   */
/*                                                                */
/*                    i4ShowPort  - Port for which config show is */
/*                                  requied                       */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
QosCliShowPortOverrideConfig (tCliHandle CliHandle, UINT4 u4PortId)
{
    UINT4               u4PrevIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4VlanOverride, i4SrcOverride, i4MacOverride;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) au1NameStr;

    if (u4PortId > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface.\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPriority Override Config Table \r\n");

    CliPrintf (CliHandle, "---------------------------------- \r\n");

    CliPrintf (CliHandle, "Port   Vlan-ID   SRC-MAC   DEST-MAC \r\n");

    CliPrintf (CliHandle, "----   -------   -------   -------- \r\n");

    if (u4PortId == 0)
    {
        /* For all ports */
        if (nmhGetFirstIndexFsQosPortConfigTable (&u4PortId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        /* 
         * Show for a specific Port. 
         *
         * Setting the Current indices to the values to the previous 
         * element such that the Get Next operation will fetch the next 
         * element in the lexicographic order.
         */
        if (nmhValidateIndexInstanceFsQosPortConfigTable (u4PortId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u1isShowAll = FALSE;
    }
    do
    {
        CfaCliGetIfName (u4PortId, pi1IfName);

        nmhGetFsQoSVIDOverride (u4PortId, &i4VlanOverride);
        nmhGetFsQoSSAOverride (u4PortId, &i4SrcOverride);
        nmhGetFsQoSDAOverride (u4PortId, &i4MacOverride);

        CliPrintf (CliHandle, "%-4s  ", au1NameStr);
        if (i4VlanOverride == QOS_ENABLE)
        {
            CliPrintf (CliHandle, "%-8s  ", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s  ", "Disabled");
        }
        if (i4SrcOverride == QOS_ENABLE)
        {
            CliPrintf (CliHandle, "%-8s  ", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-8s  ", "Disabled");
        }
        if (i4MacOverride == QOS_ENABLE)
        {
            u4PagingStatus = CliPrintf (CliHandle, "%s\r\n", "Enabled");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "%s\r\n", "Disabled");
        }

        if (u1isShowAll == FALSE)
        {
            /* Done with show for particular interface
             ** no need to continue */
            break;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt, no more print required, exit */
            break;
        }
        u4PrevIndex = u4PortId;
    }
    while (nmhGetNextIndexFsQosPortConfigTable (u4PrevIndex, &u4PortId)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliShowRunningConfig                              
 *                                                                           
 *     DESCRIPTION      : This function displays current QoS  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4SysControl = 0;
    INT4                i4SysStatus = 0;
    INT4                i4QosSchedAlgo = QOS_WEIGHTED_ROUND_ROBIN;
    CliRegisterLock (CliHandle, QoSLock, QoSUnLock);
    QoSLock ();
    nmhGetFsQoSSystemControl (&i4SysControl);
    if (i4SysControl != QOS_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "shutdown qos\r\n");
        CliPrintf (CliHandle, "!\r\n");
        QoSUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }
    nmhGetFsQoSStatus (&i4SysStatus);
    if (i4SysStatus != QOS_SYS_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "qos disable\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }

    nmhGetFsQosSchedulingAlgorithm (&i4QosSchedAlgo);
    if (i4QosSchedAlgo != QOS_WEIGHTED_ROUND_ROBIN)
    {
        CliPrintf (CliHandle, "qos scheduling policy strict\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }

    QoSCliShowRunningConfigTables (CliHandle);
    QoSUnLock ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);

}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliShowRunningConfigTables                          *  
 *                                                                           
 *     DESCRIPTION      : This function displays current QoS  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/

INT4
QoSCliShowRunningConfigTables (tCliHandle CliHandle)
{

    QoSPriorityMapShowRunningConfig (CliHandle);
    QoSQueueMapShowRunningConfig (CliHandle);
    QosShowRunningConfigInterfaceDetails (CliHandle);

    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSPriorityMapShowRunningConfig                      
 *                                                                           
 *     DESCRIPTION      : This function displays current PriorityMap  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSPriorityMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4IfNum = 0;
    UINT4               u4Idx = 0;
    UINT4               u4VlanId = 0;
    INT4                i4InPri = 0;
    INT4                i4InPriType = 0;
    UINT4               u4TrafCls = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tMacAddr            MacAddr;
    UINT1               au1String[QOS_CLI_MAX_MAC_STRING_SIZE];
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    if ((nmhGetFirstIndexFsQoSPriorityMapTable (&u4Idx)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "!\r\n");
        return (CLI_SUCCESS);
    }

    do
    {
        /* Skip the Default Entries */
        pPriMapNode = QoSUtlGetPriorityMapNode (u4Idx);
        if (pPriMapNode != NULL)
        {
            if (QoSIsDefPriorityMapTblEntry (pPriMapNode) == QOS_SUCCESS)
            {
                continue;
            }
        }

        if (nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus) != SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "priority-map %d\r\n", u4Idx);
            if (i4RowStatus != ACTIVE)
            {
                CliPrintf (CliHandle, "!\r\n");
                continue;
            }

            CliPrintf (CliHandle, " map ");

            if (nmhGetFsQoSPriorityMapInPriType (u4Idx, &i4InPriType) !=
                SNMP_FAILURE)
            {
                if (i4InPriType == QOS_CLI_IN_PRI_TYPE_VALN_PRI)
                {
                    if (nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4IfNum) !=
                        SNMP_FAILURE)
                    {
                        if (u4IfNum != 0)
                        {
                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            if (CfaCliConfGetIfName (u4IfNum,
                                                     (INT1 *) au1IfName)
                                == CLI_FAILURE)
                            {
                                return (CLI_FAILURE);
                            }
                            CliPrintf (CliHandle, "interface %s ", au1IfName);
                        }
                    }

                    CliPrintf (CliHandle, "in-priority-type vlanPri ");

                    if (nmhGetFsQoSPriorityMapInPriority (u4Idx, &i4InPri) !=
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "in-priority %ld ", i4InPri);
                    }
                }
                else
                {
                    /* Get the Vlan Id */
                    if (nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4VlanId) !=
                        SNMP_FAILURE)
                    {
                        if (u4VlanId != 0)
                        {
                            CliPrintf (CliHandle, "Vlan %ld ", u4VlanId);
                        }
                    }

                    if (i4InPriType == QOS_CLI_IN_PRI_TYPE_VLAN_ID)
                    {
                        CliPrintf (CliHandle, "in-priority-type vlan-id ");
                    }
                    else
                    {
                        if (i4InPriType == QOS_CLI_IN_PRI_TYPE_SRC_MAC)
                        {
                            CliPrintf (CliHandle, "in-priority-type src-mac ");
                        }
                        else if (i4InPriType == QOS_CLI_IN_PRI_TYPE_DEST_MAC)
                        {
                            CliPrintf (CliHandle, "in-priority-type dest-mac ");
                        }

                        MEMSET (&(au1String[0]), 0,
                                QOS_CLI_MAX_MAC_STRING_SIZE);
                        nmhGetFsQoSPriorityMapMacAddress (u4Idx, &MacAddr);
                        PrintMacAddress (MacAddr, au1String);
                        CliPrintf (CliHandle, "unicast %s ", au1String);
                    }
                }

                if (nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4TrafCls)
                    != SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "regen-priority %ld ", u4TrafCls);
                }
            }
            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "!\r\n");

        }
    }
    while ((nmhGetNextIndexFsQoSPriorityMapTable (u4Idx, &u4Idx)
            == SNMP_SUCCESS));

    CliPrintf (CliHandle, "!\r\n");

    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSQueueMapShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current QueueMap  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSQueueMapShowRunningConfig (tCliHandle CliHandle)
{

    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4PriType = 0;
    UINT4               u4PriVal = 0;
    UINT4               u4QId = 0;

    if (nmhGetFirstIndexFsQoSQMapTable (&i4PriType, &u4PriVal) != SNMP_FAILURE)
    {
        do
        {
            pQMapNode = QoSUtlGetQMapNode (i4PriType, u4PriVal);

            if (pQMapNode != NULL)
            {
                if (QoSUtlIsDefQMapTblEntry (pQMapNode) == QOS_SUCCESS)
                {
                    continue;
                }

            }

            CliPrintf (CliHandle, "queue-map ");

            if (i4PriType != 0)
            {
                CliPrintf (CliHandle, "regn-priority ");
                switch (i4PriType)
                {
                    case QOS_CLI_QMAP_PRI_TYPE_NONE:
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI:
                        CliPrintf (CliHandle, "vlanPri %ld ", u4PriVal);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_IP_DSCP:
                        CliPrintf (CliHandle, "ipDscp %ld ", u4PriVal);
                        break;
                    default:
                        break;
                }
            }
            if (nmhGetFsQoSQMapQId (i4PriType,
                                    u4PriVal, &u4QId) != SNMP_FAILURE)
            {

                CliPrintf (CliHandle, "queue-id %ld ", u4QId);
            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "!\r\n");

        }

        while (nmhGetNextIndexFsQoSQMapTable (i4PriType, &i4PriType,
                                              u4PriVal, &u4PriVal)
               == SNMP_SUCCESS);
    }
    CliPrintf (CliHandle, "!\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : QosShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays table  objects in ACL  for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
QosShowRunningConfigInterfaceDetails (tCliHandle CliHandle)
{

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4RetStatus;
    INT1                i1OutCome;
    UINT4               u4PrevIndex, u4NextIndex;
    INT1               *piIfName, i1Flag = 0;

    UINT4               u4Quit = CLI_SUCCESS;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    /*MacAccessLists */

    i1OutCome = nmhGetFirstIndexFsQosPortConfigTable (&u4NextIndex);

    while (i1OutCome != SNMP_FAILURE)
    {
        CfaCliConfGetIfName ((UINT4) u4NextIndex, piIfName);
        i1Flag = 0;

        nmhGetFsQoSVIDOverride (u4NextIndex, &i4RetStatus);
        if (i4RetStatus != QOS_ENABLE)
        {
            i1Flag = 1;
            CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            CliPrintf (CliHandle, " no qos vlan-id override\r\n");
        }

        nmhGetFsQoSSAOverride (u4NextIndex, &i4RetStatus);
        if (i4RetStatus != QOS_ENABLE)
        {
            if (i1Flag == 0)
            {
                i1Flag = 1;
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }
            CliPrintf (CliHandle, " no qos src-mac override\r\n");
        }

        nmhGetFsQoSDAOverride (u4NextIndex, &i4RetStatus);
        if (i4RetStatus != QOS_ENABLE)
        {
            if (i1Flag == 0)
            {
                i1Flag = 1;
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }
            CliPrintf (CliHandle, " no qos dest-mac override\r\n");
        }
        nmhGetFsQoSVlanPriPriority (u4NextIndex, &i4RetStatus);
        if (i4RetStatus != QOS_ENABLE)
        {
            if (i1Flag == 0)
            {
                i1Flag = 1;
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }
            CliPrintf (CliHandle, " no qos match vlanPri\r\n");
        }
        nmhGetFsQoSIPDscpPriority (u4NextIndex, &i4RetStatus);
        if (i4RetStatus != QOS_ENABLE)
        {
            if (i1Flag == 0)
            {
                i1Flag = 1;
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }
            CliPrintf (CliHandle, " no qos match ipDscp\r\n");
        }

        nmhGetFsQoSVlanIPPriority (u4NextIndex, &i4RetStatus);
        if (i4RetStatus != QOS_ENABLE)
        {
            if (i1Flag == 0)
            {
                i1Flag = 1;
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }

            u4Quit = CliPrintf (CliHandle, "  qos tagifboth match IpDscp\r\n");
        }
        if (i1Flag == 1)
        {
            u4Quit = CliPrintf (CliHandle, "\n!\r\n");
        }

        if (u4Quit == CLI_FAILURE)
        {
            return;
        }

        u4PrevIndex = u4NextIndex;

        i1OutCome = nmhGetNextIndexFsQosPortConfigTable
            (u4PrevIndex, &u4NextIndex);
    }
    return;
}
#endif /* __QOS_CLI_C__ */
