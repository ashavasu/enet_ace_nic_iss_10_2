/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/* $Id: qossys.c,v 1.7 2013/02/15 13:24:54 siva Exp $                      */
/*                                                                          */
/*  FILE NAME             : qossys.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-SYS                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the main functions; this     */
/*                          file contains routines for the global memory    */
/*                          initialisation and module initialisation        */
/*                          1. Memory Allocation  / Deletion                */
/*                          2. Semaphore Creation                           */
/*                          3. Table Initializations.                       */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_SYS_C__
#define __QOS_SYS_C__

#include "qosinc.h"

/*****************************************************************************/
/* Function Name      : QoSInit                                              */
/* Description        : This function is used to Start the QoS Sub-Systme    */
/*                      during the System startup                            */
/*                      1. It Create Sema4 for QoS.                          */
/*                      2. It Call QoSStart                                  */
/*                      3. It Register QOS MIB with SNMP Agent               */
/* Input(s)           : i1Params  - UnUsed Param                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/* Called By          : System Main Thread.                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
DsInit ()
{
    INT4                i4RetStatus = FAILURE;

    MEMSET (&(gQoSGlobalInfo), 0, (sizeof (tQoSGlobalInfo)));

    /* Create Mutual exclusion semaphore for QoS */
    i4RetStatus =
        OsixSemCrt ((UINT1 *) QOS_SEM_NAME, &(gQoSGlobalInfo.QoSSemId));

    if (i4RetStatus != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Sema4 Creation Failed!.\r\n",
                      __FUNCTION__);

        return FAILURE;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gQoSGlobalInfo.QoSSemId);

    i4RetStatus = QoSStart ();
    if (i4RetStatus != QOS_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSStart () Failed!.\r\n",
                      __FUNCTION__);

        return FAILURE;
    }
    /* Register QOS MIB OID with SNMP Agent */
    RegisterFSQOS ();

    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSLock                                              */
/* Description        : This function is used to take the QoS  mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo.QoSSemId                              */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP,QoSMain                                         */
/*****************************************************************************/
INT4
QoSLock ()
{
    if (OsixSemTake (gQoSGlobalInfo.QoSSemId) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Failed to Take QoS Mutual Exclusion Sema4.\r\n");
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUnLock                                            */
/* Description        : This function is used to give the QoS mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo.QoSSemId                              */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP,QoSMain                                         */
/*****************************************************************************/
INT4
QoSUnLock ()
{
    if (OsixSemGive (gQoSGlobalInfo.QoSSemId) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Failed to Give QoS Mutual Exclusion Sema4.\r\n");
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSInitGlobal                                        */
/* Description        : This function is used to Set Default Values for      */
/*                      Global variables                                     */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/* Called By          : QoSStart,QoSShutdown                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSInitGlobal (VOID)
{
    tOsixSemId          QoSSemId;

    /* Backup the Sem4 Id Clear all golbal values finally assign the Sem4Id */
    MEMSET (&(QoSSemId), 0, (sizeof (tOsixSemId)));
    MEMCPY (&(QoSSemId), &(gQoSGlobalInfo.QoSSemId), (sizeof (tOsixSemId)));

    MEMSET (&(gQoSGlobalInfo), 0, (sizeof (tQoSGlobalInfo)));
    MEMCPY (&(gQoSGlobalInfo.QoSSemId), &(QoSSemId), (sizeof (tOsixSemId)));

    gQoSGlobalInfo.eSysControl = QOS_SYS_CNTL_START;
    gQoSGlobalInfo.eSysStatus = QOS_SYS_STATUS_ENABLE;
    gQoSGlobalInfo.u4TrcFlag = QOS_TRC_MIN_LEVEL;
    gQoSGlobalInfo.u4QosSchedulingPolicy = QOS_WEIGHTED_ROUND_ROBIN;
}

/*****************************************************************************/
/* Function Name      : QoSStart                                             */
/* Description        : This function is used to Start the QoS Module.It     */
/*                      will do the following Actions                        */
/*                      1. Allocate Memory from the Resource Management      */
/*                         Module.                                           */
/*                      2. All Tables fro QoS Module will be Created         */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSStart ()
{
    UINT4               u4RetStatus = QOS_FAILURE;
    INT4                i4RetStatus = QOS_FAILURE;

    QoSInitGlobal ();
    u4RetStatus = QoSCreateMemPools ();

    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSCreateMemPools () Failed!.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    u4RetStatus = QoSCreateTables ();

    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSCreateTables () Failed!.\r\n",
                      __FUNCTION__);

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_SHUTDOWN)
    {
        /* Default entries will be added only if the MSR database is not
           tored. Or else default entries will be restored from the
           MSR database. */
        /* Add Default Table Entries */
        i4RetStatus = QoSAddDefTblEntries ();
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "%s : QoSAddDefTblEntries () Failed!.\r\n",
                          __FUNCTION__);

            i4RetStatus = QoSShutdown ();
            if (i4RetStatus != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : QoSShutdown () Failed!.\r\n",
                              __FUNCTION__);
            }
            return (QOS_FAILURE);
        }
        gQoSGlobalInfo.eSysStatus = QOS_SYS_STATUS_ENABLE;

        u4RetStatus = (UINT4) QoSEnable ();

        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSEnable () Failed!.\r\n",
                          __FUNCTION__);

            QoSDeleteMemPools ();

            return (QOS_FAILURE);
        }
        u4RetStatus = QoSEnable ();
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSShutdown                                          */
/* Description        : This function is used to Shutdown the QoS Module.It  */
/*                      will do the following Actions                        */
/*                      1. Deleting the Hardware Configurations.             */
/*                      2. Relasing the allocated Memory to the Resource     */
/*                         Management Module.                                */
/*                      3. System Control and System Status will set it as   */
/*                          SHUTDOWN and DISABEL Respectively.               */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QoSShutdown ()
{
    INT4                i4RetStatus = QOS_FAILURE;

    /*Remove Hardware entries */
    i4RetStatus = QoSDisable ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* This Function used to Remove Node from the Table or Tree and Relase 
     * the memory to the respective MemoryPool*/
    i4RetStatus = QoSDeleteTables ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Delete all MemoryPool */
    QoSDeleteMemPools ();

    /*Init Global structure */
    QoSInitGlobal ();

    /* Change the  System Control as SHUTDOWN */
    gQoSGlobalInfo.eSysControl = QOS_SYS_CNTL_SHUTDOWN;
    gQoSGlobalInfo.eSysStatus = QOS_SYS_STATUS_DISABLE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigQMap                                        */
/* Description        : This function is used to Add or Delete the QMap      */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigQMap (UINT4 u4CfgType)
{
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RegenPriType = 0;
    INT4                i4NextRGPType = 0;
    UINT4               u4RegenPri = 0;
    UINT4               u4NextRegenPri = 0;

    i4RetStatus = QoSGetNextQMapTblEntryIndex (i4RegenPriType, &i4NextRGPType,
                                               u4RegenPri, &u4NextRegenPri);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pQMapNode = QoSUtlGetQMapNode (i4NextRGPType, u4NextRegenPri);
        if (pQMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the QMap Table need to Add */
        if (u4CfgType == QOS_CFG_TYPE_CREATE)
        {
            i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_ADD);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrQueueMapToScheduler() : Add "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        else
        {
            i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_DEL);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrQueueMapToScheduler() : Del "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

        i4RegenPriType = i4NextRGPType;
        u4RegenPri = u4NextRegenPri;

        i4RetStatus = QoSGetNextQMapTblEntryIndex (i4RegenPriType,
                                                   &i4NextRGPType, u4RegenPri,
                                                   &u4NextRegenPri);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigPortConfigTbl                               */
/* Description        : This function is used to Add Port Configuration      */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSEnable                                            */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigPortConfigTbl (VOID)
{
    UINT4               u4IfIndex = 0;

    for (u4IfIndex = 1; u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         u4IfIndex++)
    {
        if ((QosHwWrSetVlanPriStatus (u4IfIndex,
                                      gQoSPortConfigTable[u4IfIndex].
                                      u1VlanPriStatus)) == QOS_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        if ((QosHwWrSetIPDscpStatus (u4IfIndex,
                                     gQoSPortConfigTable[u4IfIndex].
                                     u1IPDscpStatus)) == QOS_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        if ((QosHwWrSetVlanIPStatus (u4IfIndex,
                                     gQoSPortConfigTable[u4IfIndex].
                                     u1VlanIPStatus)) == QOS_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        if ((QosHwWrSetVIDOverrideStatus (u4IfIndex,
                                          gQoSPortConfigTable[u4IfIndex].
                                          u1VIDTagStatus)) == QOS_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        if ((QosHwWrSetSAOverrideStatus (u4IfIndex,
                                         gQoSPortConfigTable[u4IfIndex].
                                         u1SAOverrideStatus)) == QOS_FAILURE)
        {
            return (SNMP_FAILURE);
        }

        if ((QosHwWrSetDAOverrideStatus (u4IfIndex,
                                         gQoSPortConfigTable[u4IfIndex].
                                         u1DAOverrideStatus)) == QOS_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigPriConfigTbl                                */
/* Description        : This function is used to Add PriorityMap Table       */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSEnable                                            */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigPriConfigTbl (VOID)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT4               u4PriorityMapID = 0;
    UINT4               u4NextPriorityMapID = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSGetNextInPriMapTblEntryIndex (u4PriorityMapID,
                                                   &u4NextPriorityMapID);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pPriMapNode = QoSUtlGetPriorityMapNode (u4NextPriorityMapID);
        if (pPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the QMap Table need to Add */
        if (pPriMapNode->u1Status == ACTIVE)
        {
            i4RetStatus = QoSHwWrAddPriMapEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrAddPriMapEntry () : Add "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

        u4PriorityMapID = u4NextPriorityMapID;

        i4RetStatus = QoSGetNextInPriMapTblEntryIndex (u4PriorityMapID,
                                                       &u4NextPriorityMapID);
    }
    while (i4RetStatus == QOS_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSEnable                                            */
/* Description        : This function is used to Enable the QoS Module.It    */
/*                      will do the following Actions                        */
/*                       It will configure the hardware with Current Data    */
/*                         in the Software.                                  */
/*                      1. Init the Hardware                                 */
/*                      2. It Add the Class(Filters) into the hardware.      */
/*                      3. It Add the Meters into the hardware.              */
/*                      4. It Add the Policies into the hardware.            */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QoSEnable ()
{
    INT4                i4RetStatus = QOS_FAILURE;

    /* Init the HARDWARE */
    i4RetStatus = QoSHwWrInit ();
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : ERROR: QoSHwWrInit () Failed!. "
                      "\r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* Set the SchedPolicy */
    i4RetStatus =
        QoSHwWrSetSchedulingAlgo (gQoSGlobalInfo.u4QosSchedulingPolicy);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Add Port Config */
    i4RetStatus = QoSConfigPortConfigTbl ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Add QueueMap   */
    i4RetStatus = QoSConfigQMap (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Add Priority Config */
    i4RetStatus = QoSConfigPriConfigTbl ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDisable                                           */
/* Description        : This function is used to Disable the QoS Module.It   */
/*                      will do the following Actions                        */
/*                      1. It Delete the Policies from the hardware.         */
/*                      2. It Delete the Meters from the hardware.           */
/*                      3. It Delete the Class(Filters) from the hardware.   */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDisable ()
{
    INT4                i4RetStatus = QOS_FAILURE;

    /* Init the HARDWARE */
    i4RetStatus = QoSHwWrDeInit ();
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : ERROR: QoSHwWrDeInit () Failed!. "
                      "\r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteMemPools                                    */
/* Description        : This function is used to delete Memory Pool for QoS  */
/*                      Tables Using Memory Management Module                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : None.                                                */
/* Called By          : QoSStart                                             */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSDeleteMemPools (VOID)
{
    /* Delete All Memory Pools */
    if (gQoSGlobalInfo.QoSInPriMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSInPriMapTblMemPoolId);
        gQoSGlobalInfo.QoSInPriMapTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSQMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSQMapTblMemPoolId);
        gQoSGlobalInfo.QoSQMapTblMemPoolId = 0;
    }
}

/*****************************************************************************/
/* Function Name      : QoSCreateMemPools                                    */
/* Description        : This function is used to Create Memory Pool for QoS  */
/*                      Tables Using Memory Management Module                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSStart                                             */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCreateMemPools (VOID)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    /* Create Memory Pools for QoS Tables */
    /* 1. Incoming Priority Map Table  */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSInPriorityMapNode),
                                    (QOS_PRI_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSInPriMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 2. Q Map Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSQMapNode),
                                    (QOS_Q_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSQMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteTables                                      */
/* Description        : This function is used to delete the QoS Table and    */
/*                       it Entries if it Exists                             */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteTables (VOID)
{

    /* Delete the Table (Sll,Hash,RBTree) */
    if (gQoSGlobalInfo.pRbInPriMapUniTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbInPriMapUniTbl, NULL, 0);
        gQoSGlobalInfo.pRbInPriMapUniTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbInPriMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbInPriMapTbl,
                       QoSInPriTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbInPriMapTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbQMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbQMapTbl, QoSQMapTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbQMapTbl = NULL;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateTables                                      */
/* Description        : This function is used to Create RbTrees and Init the */
/*                      SLL_Node for QoS Tables.                             */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSStart                                             */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCreateTables (VOID)
{
    /* 1. Incoming Priority Map Table  */
    gQoSGlobalInfo.pRbInPriMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSInPriorityMapNode, RbNode),
                              QoSInPriMapCmpFun);

    if (gQoSGlobalInfo.pRbInPriMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s :PriMapTbl-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 1a. Incoming Priority Map Unique Table  */
    gQoSGlobalInfo.pRbInPriMapUniTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSInPriorityMapNode,
                                             RbNodeUnique),
                              QoSInPriMapUniCmpFun);

    if (gQoSGlobalInfo.pRbInPriMapUniTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s :PriMapUniqueTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 2. Q Map Table */
    gQoSGlobalInfo.pRbQMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSQMapNode, RbNode),
                              QoSQMapCmpFun);
    if (gQoSGlobalInfo.pRbQMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QMapTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefPriorityMapTblEntries                       */
/* Description        : This function is used to Create default tables for   */
/*                      PriorityMap Table Entry.                             */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSAddDefPriorityMapTblEntries ()
{

    tQoSInPriorityMapNode *pPMNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT4               u4PriMapId = 0;

    /*Add PriorityMap Table Entries */

    for (u1IPVal = 0; u1IPVal <= QOS_PM_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u4PriMapId = u1IPVal + 1;

        pPMNode = QoSCreatePriMapTblEntry (u4PriMapId);
        if (pPMNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Update the Entry  and dependent Tbls */
        pPMNode->u4Id = u4PriMapId;
        pPMNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_SYS;
        pPMNode->u4IfIndex = 0;
        pPMNode->u2VlanId = 0;
        pPMNode->u1InType = QOS_PM_TBL_DEF_ENTRY_TYPE;
        pPMNode->u1InPriority = u1IPVal;
        pPMNode->u1RegenPriority = u1IPVal;
        pPMNode->u1Status = ACTIVE;

        /* Add into the UniPriMapTble */
        i4RetStatus = QoSAddPriMapUniTblEntry (pPMNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }

        /* Configure the HW  if Needed */

    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefQMapTblEntries                              */
/* Description        : This function is used to Create default tables for   */
/*                      QueueMap Table Entry.                                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefQMapTblEntries ()
{

    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT1               u1RegenPri = 0;
    UINT1               au1QId[DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE] =
        { 1, 0, 0, 1, 2, 2, 3, 3 };

    for (u1IPVal = 0; u1IPVal <= QOS_PM_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u1RegenPri = u1IPVal;
        pQMapNode = QoSCreateQMapTblEntry (QOS_QMAP_PRI_TYPE_VLAN_PRI, u1IPVal);
        if (pQMapNode == NULL)
        {
            return (QOS_FAILURE);
        }
        /* Update the Entry  and dependent Tbls */
        pQMapNode->u1RegenPriType = QOS_QMAP_PRI_TYPE_VLAN_PRI;
        pQMapNode->u1RegenPri = u1RegenPri;
        pQMapNode->u4QId = au1QId[u1IPVal];

        /* Configure the HW  if Needed */
        /* 1. Add the entry into the HARDWARE. */
        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_ADD);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrMapClassToQueue() : Add "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);

            }
        }
    }                            /*End of -for */
    for (u1IPVal = 0; u1IPVal <= QOS_IN_PRIORITY_IP_DSCP_MAX; u1IPVal++)
    {
        u1RegenPri = u1IPVal;
        pQMapNode = QoSCreateQMapTblEntry (QOS_QMAP_PRI_TYPE_IP_DSCP, u1IPVal);
        if (pQMapNode == NULL)
        {
            return (QOS_FAILURE);
        }
        /* Update the Entry  and dependent Tbls */
        pQMapNode->u1RegenPriType = QOS_QMAP_PRI_TYPE_IP_DSCP;
        pQMapNode->u1RegenPri = u1IPVal;

        if (u1IPVal <= QOS_ASSIGN_TRAFFIC_CLASS_0)
        {
            pQMapNode->u4QId = 0;
        }
        else if (u1IPVal <= QOS_ASSIGN_TRAFFIC_CLASS_1)
        {
            pQMapNode->u4QId = 1;
        }
        else if (u1IPVal <= QOS_ASSIGN_TRAFFIC_CLASS_2)
        {
            pQMapNode->u4QId = 2;
        }
        else if (u1IPVal <= QOS_ASSIGN_TRAFFIC_CLASS_3)
        {
            pQMapNode->u4QId = 3;
        }

        /* Configure the HW  if Needed */
        /* 1. Add the entry into the HARDWARE. */
        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_ADD);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrMapClassToQueue() : Add "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);

            }
        }

    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefPortConfigTblEntries                        */
/* Description        : This function is used to Create default tables for   */
/*                      Port Config table .                                  */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefPortConfigTblEntries ()
{

    INT4                i4IfIndex;

    /* By default IEEETag and IPTosDscp type of classification is
     * enabled in hardware */
    for (i4IfIndex = 1; i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        gQoSPortConfigTable[i4IfIndex].u1VlanPriStatus = QOS_ENABLE;
        gQoSPortConfigTable[i4IfIndex].u1IPDscpStatus = QOS_ENABLE;
        gQoSPortConfigTable[i4IfIndex].u1VlanIPStatus = QOS_ENABLE;
        gQoSPortConfigTable[i4IfIndex].u1SAOverrideStatus = QOS_ENABLE;
        gQoSPortConfigTable[i4IfIndex].u1DAOverrideStatus = QOS_ENABLE;
        gQoSPortConfigTable[i4IfIndex].u1VIDTagStatus = QOS_ENABLE;
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefTblEntries                                  */
/* Description        : This function is used to Create default tables for   */
/*                      QOS Tables.                                          */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSInit                                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefTblEntries ()
{
    INT4                i4RetStatus = QOS_FAILURE;
    /*Add PriorityMap Table Entries */

    i4RetStatus = QoSAddDefPriorityMapTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /*Add QueueMapTable  Table Entries */
    i4RetStatus = QoSAddDefQMapTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /*Add Port Config Table Entries */
    i4RetStatus = QoSAddDefPortConfigTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSRegenerateVlanPriority                            */
/* Description        : This function is Called From VlanModule.It will the  */
/*                      u1RegenPri Value to Vlan Module                      */
/* Input(s)           : u4IfIndex,u2VlanId,u1InPriority.                     */
/* Output(s)          : u1RegenPri.                                          */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : u1RegenPri.                                          */
/* Called By          : Vlan Module                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT1
QoSRegenerateVlanPriority (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1InPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1               u1RegenPri = 0;
    UINT1               u1InType = 0;
    UINT2               u2TempVlanId = 0;
    UINT4               u4TempIfIndex = 0;

    u2TempVlanId = u2VlanId;
    u4TempIfIndex = u4IfIndex;

    /*For the given the ifindex,vlanid,inpriority-Find the corresponding
       RegenPri using the function QoSUtlGetPriorityMapUniNode */

    /*Append InPriType to VlanPri */

    u1InType = QOS_IN_PRI_TYPE_VLAN_PRI;

    pPriMapNode =
        QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                     u2VlanId, u1InPriority, u1InType);
    if (pPriMapNode != NULL)
    {
        u1RegenPri = pPriMapNode->u1RegenPriority;
        return u1RegenPri;
    }

    /*Since there is No Prioritymapnode with given u4IfIndex,u2VlanId,
     * u1InPriority.Give the Default Value*/
    else
    {

        /*To give more specific Value For the given u4IfIndex,u2VlanId,
         * u1InPriority.Make the u4IfIndex=0 and check whether the 
         * corresponding  PriorityMap Node is Available*/

        u4IfIndex = 0;
        pPriMapNode =
            QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                         u2VlanId, u1InPriority, u1InType);

        if (pPriMapNode != NULL)
        {
            u1RegenPri = pPriMapNode->u1RegenPriority;
            return u1RegenPri;
        }

        /*To give Next specific Value For the given u4IfIndex,u2VlanId,
         * u1InPriority.Make the  u2VlanId=0 and check whether the 
         * corresponding  PriorityMap Node is Available*/

        else
        {
            u2VlanId = 0;
            u4IfIndex = u4TempIfIndex;
            pPriMapNode =
                QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                             u2VlanId, u1InPriority, u1InType);

            if (pPriMapNode != NULL)
            {
                u1RegenPri = pPriMapNode->u1RegenPriority;
                return u1RegenPri;
            }
        }
    }
/* If No Match is present, give the Default Value with u4IfIndex=0
   u2VlanId=0*/

    u4IfIndex = 0;
    u2VlanId = 0;
    pPriMapNode =
        QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                     u2VlanId, u1InPriority, u1InType);

    return (pPriMapNode->u1RegenPriority);

}

/*****************************************************************************/
/* Function Name      : DsGetStartedStatus                                   */
/*                                                                           */
/* Description        : This function gets the system status whether its     */
/*                      started or shutdown.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_TRUE     - On success                             */
/*                      DS_FALSE    - On Failure                             */
/*****************************************************************************/
INT4
DsGetStartedStatus ()
{

    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/*****************************************************************************/
/* Function Name      : DsCheckiffservPort                                   */
/*                                                                           */
/* Description        : This function checks whether the given port is a     */
/*                      Diffsrv port                                         */
/*                                                                           */
/* Input(s)           : i4TrunkPort - Trunk Port                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS     - On success                          */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckDiffservPort (INT4 i4TrunkPort)
{
    UNUSED_PARAM (i4TrunkPort);
    return DS_SUCCESS;
}

/* **********   APIs to be called from other modules ********** */

/*****************************************************************************/
/* Function Name      : DsCheckMFClfrTable                                   */
/*                                                                           */
/* Description        : This function checks whether the given filter is     */
/*                      being used by any class-map. This is called from the */
/*                      ISS module when the filter is about to be deleted.   */
/*                                                                           */
/*                      NOTE: DO NOT CALL THIS ROUTINE INTERNALLY FROM QOS   */
/*                            SINCE IT TAKES THE DFS LOCK.                   */
/*                                                                           */
/* Input(s)           : i4FilterNo, i4FilterType                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS - On success                              */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckMFClfrTable (INT4 i4FilterNo, INT4 i4FilterType)
{
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (i4FilterType);

    return DS_SUCCESS;
}

#endif /* __QOS_SYS_C__ */
