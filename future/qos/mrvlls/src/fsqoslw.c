/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsqoslw.c,v 1.5 2013/12/16 15:26:35 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsqoslw.h"
# include  "qosinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQoSSystemControl
 Input       :  The Indices

                The Object 
                retValFsQoSSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSystemControl (INT4 *pi4RetValFsQoSSystemControl)
{
    *pi4RetValFsQoSSystemControl = gQoSGlobalInfo.eSysControl;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: System Control = %s SUCCESS\r\n", __FUNCTION__,
                  (*pi4RetValFsQoSSystemControl) == 1 ? "Start" : "Shutdown");

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSStatus
 Input       :  The Indices

                The Object 
                retValFsQoSStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSStatus (INT4 *pi4RetValFsQoSStatus)
{
    *pi4RetValFsQoSStatus = gQoSGlobalInfo.eSysStatus;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %s SUCCESS \r\n",
                  __FUNCTION__,
                  (*pi4RetValFsQoSStatus) == 1 ? "Enabled " : "Disabled");

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSTrcFlag
 Input       :  The Indices

                The Object 
                retValFsQoSTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSTrcFlag (UINT4 *pu4RetValFsQoSTrcFlag)
{
    *pu4RetValFsQoSTrcFlag = gQoSGlobalInfo.u4TrcFlag;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: Trace Level = %d SUCCESS \r\n",
                  __FUNCTION__, *pu4RetValFsQoSTrcFlag);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQosSchedulingAlgorithm
 Input       :  The Indices

                The Object 
                retValFsQosSchedulingAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQosSchedulingAlgorithm (INT4 *pi4RetValFsQosSchedulingAlgorithm)
{
    *pi4RetValFsQosSchedulingAlgorithm =
        (INT4) gQoSGlobalInfo.u4QosSchedulingPolicy;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: Scheduling algorithm = %d SUCCESS \r\n",
                  __FUNCTION__, *pi4RetValFsQosSchedulingAlgorithm);

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSSystemControl
 Input       :  The Indices

                The Object 
                setValFsQoSSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSystemControl (INT4 i4SetValFsQoSSystemControl)
{
    INT4                i4RetStats = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl == (UINT4) i4SetValFsQoSSystemControl)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Already in the same status System "
                      "Control = %d SUCCESS.\r\n", __FUNCTION__,
                      i4SetValFsQoSSystemControl);

        return (SNMP_SUCCESS);
    }

    if (i4SetValFsQoSSystemControl == QOS_SYS_CNTL_START)
    {
        i4RetStats = QoSStart ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSStart () - FAILED \r\n",
                          __FUNCTION__);

            return (SNMP_FAILURE);
        }
    }
    else
    {
        i4RetStats = QoSShutdown ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSShutdown () - FAILED \r\n",
                          __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    gQoSGlobalInfo.eSysControl = i4SetValFsQoSSystemControl;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Control = %d SUCCESS \r\n",
                  __FUNCTION__, i4SetValFsQoSSystemControl);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSStatus
 Input       :  The Indices

                The Object 
                setValFsQoSStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSStatus (INT4 i4SetValFsQoSStatus)
{
    INT4                i4RetStats = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysStatus == (UINT4) i4SetValFsQoSStatus)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Already in the same status "
                      "System Status = %d SUCCESS \r\n",
                      __FUNCTION__, i4SetValFsQoSStatus);

        return (SNMP_SUCCESS);

    }

    if (i4SetValFsQoSStatus == QOS_SYS_STATUS_ENABLE)
    {
        i4RetStats = QoSEnable ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSEnable () - FAILED. \r\n",
                          __FUNCTION__);

            return (SNMP_FAILURE);
        }

    }
    else
    {
        i4RetStats = QoSDisable ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDisable () - FAILED.\r\n",
                          __FUNCTION__);

            return (SNMP_FAILURE);
        }
    }

    gQoSGlobalInfo.eSysStatus = i4SetValFsQoSStatus;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %d SUCCESS.\r\n",
                  __FUNCTION__, i4SetValFsQoSStatus);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSTrcFlag
 Input       :  The Indices

                The Object 
                setValFsQoSTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSTrcFlag (UINT4 u4SetValFsQoSTrcFlag)
{
    gQoSGlobalInfo.u4TrcFlag = u4SetValFsQoSTrcFlag;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: Trace Level = %d SUCCESS. \r\n",
                  __FUNCTION__, u4SetValFsQoSTrcFlag);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQosSchedulingAlgorithm
 Input       :  The Indices

                The Object 
                setValFsQosSchedulingAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQosSchedulingAlgorithm (INT4 i4SetValFsQosSchedulingAlgorithm)
{

    if ((i4SetValFsQosSchedulingAlgorithm != QOS_STRICT_PRIORITY) &&
        (i4SetValFsQosSchedulingAlgorithm != QOS_WEIGHTED_ROUND_ROBIN))
    {
        return SNMP_FAILURE;
    }
    gQoSGlobalInfo.u4QosSchedulingPolicy =
        (UINT4) i4SetValFsQosSchedulingAlgorithm;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: Scheduling Algorithm  = %d SUCCESS. \r\n",
                  __FUNCTION__, i4SetValFsQosSchedulingAlgorithm);
    QoSHwWrSetSchedulingAlgo (i4SetValFsQosSchedulingAlgorithm);

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQoSSystemControl
 Input       :  The Indices

                The Object 
                testValFsQoSSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsQoSSystemControl)
{
    if ((i4TestValFsQoSSystemControl != QOS_SYS_CNTL_SHUTDOWN) &&
        (i4TestValFsQoSSystemControl != QOS_SYS_CNTL_START))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : System Control= %d FAILED.\r\n",
                      __FUNCTION__, i4TestValFsQoSSystemControl);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_SYS_CONTROL);

        return (SNMP_FAILURE);
    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Control= %d SUCCESS.\r\n",
                  __FUNCTION__, i4TestValFsQoSSystemControl);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSStatus
 Input       :  The Indices

                The Object 
                testValFsQoSStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsQoSStatus)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (SNMP_FAILURE);
    }

    if ((i4TestValFsQoSStatus != QOS_SYS_STATUS_DISABLE) &&
        (i4TestValFsQoSStatus != QOS_SYS_STATUS_ENABLE))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status %d FAILED.\r\n",
                      __FUNCTION__, i4TestValFsQoSStatus);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_SYS_STATUS);

        return (SNMP_FAILURE);

    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %d SUCCESS.\r\n",
                  __FUNCTION__, i4TestValFsQoSStatus);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSTrcFlag
 Input       :  The Indices

                The Object 
                testValFsQoSTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSTrcFlag (UINT4 *pu4ErrorCode, UINT4 u4TestValFsQoSTrcFlag)
{
    if (u4TestValFsQoSTrcFlag > QOS_TRC_MAX_LEVEL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Trace Level = %d FAILED.\r\n",
                      __FUNCTION__, u4TestValFsQoSTrcFlag);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_TRC_FLAG);

        return (SNMP_FAILURE);
    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : Trace Level = %d SUCCESS.\r\n",
                  __FUNCTION__, u4TestValFsQoSTrcFlag);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQosSchedulingAlgorithm
 Input       :  The Indices

                The Object 
                testValFsQosSchedulingAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQosSchedulingAlgorithm (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsQosSchedulingAlgorithm)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsQosSchedulingAlgorithm != QOS_STRICT_PRIORITY) &&
        (i4TestValFsQosSchedulingAlgorithm != QOS_WEIGHTED_ROUND_ROBIN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsQoSStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsQoSTrcFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSTrcFlag (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsQosSchedulingAlgorithm
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQosSchedulingAlgorithm (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSPriorityMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSPriorityMapTable (UINT4 u4FsQoSPriorityMapID)
{
    INT4                i4RetStatus = 0;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSPriorityMapTable (UINT4 *pu4FsQoSPriorityMapID)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextInPriMapTblEntryIndex (0, pu4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextInPriMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
                nextFsQoSPriorityMapID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSPriorityMapTable (UINT4 u4FsQoSPriorityMapID,
                                      UINT4 *pu4NextFsQoSPriorityMapID)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    i4RetStatus =
        QoSGetNextInPriMapTblEntryIndex (u4FsQoSPriorityMapID,
                                         pu4NextFsQoSPriorityMapID);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextInPriMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapName
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapName (UINT4 u4FsQoSPriorityMapID,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsQoSPriorityMapName)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pRetValFsQoSPriorityMapName == NULL) ||
        (pRetValFsQoSPriorityMapName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pPriMapNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSPriorityMapName, pu1Str);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapIfIndex
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapIfIndex (UINT4 u4FsQoSPriorityMapID,
                               UINT4 *pu4RetValFsQoSPriorityMapIfIndex)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPriorityMapIfIndex = pPriMapNode->u4IfIndex;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapVlanId
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapVlanId (UINT4 u4FsQoSPriorityMapID,
                              UINT4 *pu4RetValFsQoSPriorityMapVlanId)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pu4RetValFsQoSPriorityMapVlanId = (UINT4) (pPriMapNode->u2VlanId);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapInPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapInPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapInPriority (UINT4 u4FsQoSPriorityMapID,
                                  INT4 *pi4RetValFsQoSPriorityMapInPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapInPriority = (INT4) (pPriMapNode->u1InPriority);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapInPriType
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapInPriType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapInPriType (UINT4 u4FsQoSPriorityMapID,
                                 INT4 *pi4RetValFsQoSPriorityMapInPriType)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapInPriType = (INT4) (pPriMapNode->u1InType);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapRegenPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapRegenPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapRegenPriority (UINT4 u4FsQoSPriorityMapID,
                                     UINT4
                                     *pu4RetValFsQoSPriorityMapRegenPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPriorityMapRegenPriority =
        (UINT4) (pPriMapNode->u1RegenPriority);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapMacAddress
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapMacAddress (UINT4 u4FsQoSPriorityMapID,
                                  tMacAddr * pRetValFsQoSPriorityMapMacAddress)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsQoSPriorityMapMacAddress, pPriMapNode->MacAddr,
            MAC_ADDR_LEN);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapConfigStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapConfigStatus (UINT4 u4FsQoSPriorityMapID,
                                    INT4 *pi4RetValFsQoSPriorityMapConfigStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapConfigStatus = (INT4) (pPriMapNode->u1ConfStatus);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapStatus (UINT4 u4FsQoSPriorityMapID,
                              INT4 *pi4RetValFsQoSPriorityMapStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapStatus = (INT4) (pPriMapNode->u1Status);

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapName
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapName (UINT4 u4FsQoSPriorityMapID,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsQoSPriorityMapName)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1              *pu1Str = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pPriMapNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSPriorityMapName);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapIfIndex
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapIfIndex (UINT4 u4FsQoSPriorityMapID,
                               UINT4 u4SetValFsQoSPriorityMapIfIndex)
{

    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u4IfIndex = u4SetValFsQoSPriorityMapIfIndex;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapVlanId
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapVlanId (UINT4 u4FsQoSPriorityMapID,
                              UINT4 u4SetValFsQoSPriorityMapVlanId)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u2VlanId = (UINT2) u4SetValFsQoSPriorityMapVlanId;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapInPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapInPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapInPriority (UINT4 u4FsQoSPriorityMapID,
                                  INT4 i4SetValFsQoSPriorityMapInPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1InPriority = (UINT1) i4SetValFsQoSPriorityMapInPriority;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapInPriType
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapInPriType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapInPriType (UINT4 u4FsQoSPriorityMapID,
                                 INT4 i4SetValFsQoSPriorityMapInPriType)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1InType = (UINT1) i4SetValFsQoSPriorityMapInPriType;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapRegenPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapRegenPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapRegenPriority (UINT4 u4FsQoSPriorityMapID,
                                     UINT4
                                     u4SetValFsQoSPriorityMapRegenPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1RegenPriority =
        (UINT1) u4SetValFsQoSPriorityMapRegenPriority;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapMacAddress
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapMacAddress (UINT4 u4FsQoSPriorityMapID,
                                  tMacAddr SetValFsQoSPriorityMapMacAddress)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    MEMCPY (pPriMapNode->MacAddr, SetValFsQoSPriorityMapMacAddress,
            MAC_ADDR_LEN);
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapStatus (UINT4 u4FsQoSPriorityMapID,
                              INT4 i4SetValFsQoSPriorityMapStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode != NULL)
    {
        i4RowStatus = (UINT4) pPriMapNode->u1Status;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSPriorityMapStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4TestValFsQoSPriorityMapStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSPriorityMapStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSPriorityMapStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pPriMapNode = QoSCreatePriMapTblEntry (u4FsQoSPriorityMapID);

            if (pPriMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreatePriMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            pPriMapNode->u1Status = NOT_IN_SERVICE;

            return (SNMP_SUCCESS);

            break;

            /* Modify an Entry */
        case ACTIVE:

            /* Add the Unique Entry in the PriorityMap Unique Tbl */
            if (pPriMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSAddPriMapUniTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            i4RetStatus = QoSAddPriMapUniTblEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSAddPriMapUniTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            /* Add the Unique Entry in the PriorityMap Unique Tbl */
            i4RetStatus = QoSHwWrAddPriMapEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrAddPriMapEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }

            pPriMapNode->u1Status = ACTIVE;

            break;

        case NOT_IN_SERVICE:

            /* 1a. update the hardware for priority information */
            if (pPriMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSAddPriMapUniTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            i4RetStatus = QoSHwWrDelPriorityEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrDelPriorityEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            /* 1a. Remove Entry from the Priority Map Unique Table */
            i4RetStatus = QoSDeletePriMapUniTblEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeletePriMapUniTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }

            pPriMapNode->u1Status = NOT_IN_SERVICE;

            break;

        case DESTROY:

            /* 1. update the hardware for priority information */
            i4RetStatus = QoSHwWrDelPriorityEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrDelPriorityEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }

            i4RetStatus = QoSDeletePriMapTblEntry (pPriMapNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeletePriMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            break;

            /* Not Supported */
        case CREATE_AND_GO:
            return (SNMP_FAILURE);
            break;

        default:
            return (SNMP_FAILURE);
            break;
    }

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapName
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapName (UINT4 *pu4ErrorCode, UINT4 u4FsQoSPriorityMapID,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsQoSPriorityMapName)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSPriorityMapName,
                                           pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pPriMapNode->au1Name)) == (UINT4)
        pTestValFsQoSPriorityMapName->i4_Length)
    {
        if ((STRNCMP (pPriMapNode->au1Name,
                      pTestValFsQoSPriorityMapName->pu1_OctetList,
                      pTestValFsQoSPriorityMapName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlIsUniqueName (pTestValFsQoSPriorityMapName,
                                      QOS_TBL_TYPE_PRI_MAP);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapIfIndex
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapIfIndex (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsQoSPriorityMapID,
                                  UINT4 u4TestValFsQoSPriorityMapIfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSPriorityMapIfIndex == 0)
    {
        /* Reset the IfIndex */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateIfIndex (u4TestValFsQoSPriorityMapIfIndex,
                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapVlanId
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapVlanId (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsQoSPriorityMapID,
                                 UINT4 u4TestValFsQoSPriorityMapVlanId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSPriorityMapVlanId == 0)
    {
        /* Reset the VlanId */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateVlanId (u4TestValFsQoSPriorityMapVlanId,
                                        pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    /* Check  whether vlan is active or not */
    i4RetStatus = QoSUtlValidatePriMapTblVlanId ((UINT2)
                                                 u4TestValFsQoSPriorityMapVlanId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapInPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapInPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapInPriority (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSPriorityMapID,
                                     INT4 i4TestValFsQoSPriorityMapInPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4InPriority = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (SNMP_FAILURE);
    }

    i4InPriority = i4TestValFsQoSPriorityMapInPriority;

    switch (pPriMapNode->u1InType)
    {
        case QOS_IN_PRI_TYPE_VLAN_PRI:
        case QOS_IN_PRI_TYPE_VLAN_ID:
        case QOS_IN_PRI_TYPE_DEST_MAC:
        case QOS_IN_PRI_TYPE_SRC_MAC:

            if ((i4InPriority < QOS_IN_PRIORITY_VLAN_PRI_MIN) ||
                (i4InPriority > QOS_IN_PRIORITY_VLAN_PRI_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_PRI_MIN,
                              QOS_IN_PRIORITY_VLAN_PRI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_PRI);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if ((i4InPriority < QOS_IN_PRIORITY_IP_DSCP_MIN) ||
                (i4InPriority > QOS_IN_PRIORITY_IP_DSCP_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_DSCP_MIN,
                              QOS_IN_PRIORITY_IP_DSCP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_DSCP);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            return (SNMP_FAILURE);
            break;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapInPriType
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapInPriType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapInPriType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsQoSPriorityMapID,
                                    INT4 i4TestValFsQoSPriorityMapInPriType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    /* Check  Object's Value */
    if ((i4TestValFsQoSPriorityMapInPriType < QOS_IN_PRI_TYPE_MIN_VAL) ||
        (i4TestValFsQoSPriorityMapInPriType > QOS_IN_PRI_TYPE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriType value is out of range. The range"
                      " should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_IN_PRI_TYPE_MIN_VAL, QOS_IN_PRI_TYPE_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapRegenPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapRegenPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapRegenPriority (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsQoSPriorityMapID,
                                        UINT4
                                        u4TestValFsQoSPriorityMapRegenPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RegnPriority = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (SNMP_FAILURE);
    }

    i4RegnPriority = u4TestValFsQoSPriorityMapRegenPriority;

    switch (pPriMapNode->u1InType)
    {
            /* Intentional fall through */
        case QOS_IN_PRI_TYPE_VLAN_PRI:
        case QOS_IN_PRI_TYPE_VLAN_ID:
        case QOS_IN_PRI_TYPE_DEST_MAC:
        case QOS_IN_PRI_TYPE_SRC_MAC:

            if ((i4RegnPriority < QOS_IN_PRIORITY_VLAN_PRI_MIN) ||
                (i4RegnPriority > QOS_IN_PRIORITY_VLAN_PRI_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_PRI_MIN,
                              QOS_IN_PRIORITY_VLAN_PRI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_PRI);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if ((i4RegnPriority < QOS_IN_PRIORITY_IP_DSCP_MIN) ||
                (i4RegnPriority > QOS_IN_PRIORITY_IP_DSCP_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_DSCP_MAX,
                              QOS_IN_PRIORITY_IP_DSCP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_DSCP);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            return (SNMP_FAILURE);
            break;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapMacAddress
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapMacAddress (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSPriorityMapID,
                                     tMacAddr TestValFsQoSPriorityMapMacAddress)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (SNMP_FAILURE);
    }
    /* Mac address should have been already configured */
    i4RetStatus = QoSUtlValidatePriMapTblMacAddress (u4FsQoSPriorityMapID,
                                                     TestValFsQoSPriorityMapMacAddress,
                                                     pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblMacAddress () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsQoSPriorityMapID,
                                 INT4 i4TestValFsQoSPriorityMapStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSInPriorityMapNode *pPriMapUniqueNode = NULL;
    UINT4               u4Count = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSPriorityMapStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbInPriMapTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_PRI_MAP_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Priority Map Table"
                                  " Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_PRI_MAP_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_MAX_ENTRY);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
                break;
        }
    }
    else
    {

        /* Entry Found In The Table */
        switch (i4TestValFsQoSPriorityMapStatus)
        {
            case ACTIVE:

                if ((pPriMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* Check the Priority Map Table uniqueness of 
                 * (If,Vlan,InPriType,InPriVal) 
                 * if it is Unique then Return SUCCESS otherwise FAILURE */
                pPriMapUniqueNode =
                    QoSUtlGetPriorityMapUniNode (pPriMapNode->u4IfIndex,
                                                 pPriMapNode->u2VlanId,
                                                 pPriMapNode->u1InPriority,
                                                 pPriMapNode->u1InType);
                if ((pPriMapUniqueNode != NULL) &&
                    (pPriMapUniqueNode->u4Id != pPriMapNode->u4Id))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_DUP_ENTRY);
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pPriMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* 1. Check(Test) Dependency for this Entry. */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pPriMapNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Class Map Table Entry.\r\n",
                                  __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:

                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefPriorityMapTblEntry (pPriMapNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsPriorityMapDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* 1. Check(Test) Dependency for this Entry. */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pPriMapNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Class Map Table Entry.\r\n",
                                  __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSPriorityMapTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSQMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSQMapTable
 Input       :  The Indices
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSQMapTable (INT4 i4FsQoSQMapRegenPriType,
                                        UINT4 u4FsQoSQMapRegenPriority)
{
    tQoSQMapNode       *pQMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pQMapNode = QoSUtlGetQMapNode (i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSQMapTable
 Input       :  The Indices
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSQMapTable (INT4 *pi4FsQoSQMapRegenPriType,
                                UINT4 *pu4FsQoSQMapRegenPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQMapTblEntryIndex (0, pi4FsQoSQMapRegenPriType,
                                               0, pu4FsQoSQMapRegenPriority);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSQMapTable
 Input       :  The Indices
                FsQoSQMapRegenPriType
                nextFsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
                nextFsQoSQMapRegenPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSQMapTable (INT4 i4FsQoSQMapRegenPriType,
                               INT4 *pi4NextFsQoSQMapRegenPriType,
                               UINT4 u4FsQoSQMapRegenPriority,
                               UINT4 *pu4NextFsQoSQMapRegenPriority)
{

    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQMapTblEntryIndex (i4FsQoSQMapRegenPriType,
                                               pi4NextFsQoSQMapRegenPriType,
                                               u4FsQoSQMapRegenPriority,
                                               pu4NextFsQoSQMapRegenPriority);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQoSQMapQId
 Input       :  The Indices
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                retValFsQoSQMapQId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQMapQId (INT4 i4FsQoSQMapRegenPriType,
                    UINT4 u4FsQoSQMapRegenPriority,
                    UINT4 *pu4RetValFsQoSQMapQId)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = QoSUtlGetQMapNode (i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQMapQId = pQMapNode->u4QId;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSQMapQId
 Input       :  The Indices
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                setValFsQoSQMapQId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQMapQId (INT4 i4FsQoSQMapRegenPriType,
                    UINT4 u4FsQoSQMapRegenPriority, UINT4 u4SetValFsQoSQMapQId)
{
    tQoSQMapNode       *pQMapNode = NULL;
    UINT1               u1Flag = 0;

    pQMapNode = QoSUtlGetQMapNode (i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQMapNode->u4QId = u4SetValFsQoSQMapQId;

    QoSValidateQMapTblEntry (pQMapNode);

    if (QoSHwWrMapClassToQueue (i4FsQoSQMapRegenPriType,
                                u4FsQoSQMapRegenPriority, u4SetValFsQoSQMapQId,
                                u1Flag) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToQueue () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQoSQMapQId
 Input       :  The Indices
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                testValFsQoSQMapQId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQMapQId (UINT4 *pu4ErrorCode, INT4 i4FsQoSQMapRegenPriType,
                       UINT4 u4FsQoSQMapRegenPriority,
                       UINT4 u4TestValFsQoSQMapQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check Index */
    i4RetStatus = QoSUtlValidateQMapTblIdxInst (i4FsQoSQMapRegenPriType,
                                                u4FsQoSQMapRegenPriority,
                                                pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQMapTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Independent Table no need to check the Q Id */
    if (u4TestValFsQoSQMapQId > QOS_QMAP_MAX_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSQMapTable
 Input       :  The Indices
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSQMapTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQosPortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQosPortConfigTable
 Input       :  The Indices
                FsQoSIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQosPortConfigTable (UINT4 u4FsQoSIfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4ErrorCode = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQosPortConfigTable
 Input       :  The Indices
                FsQoSIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQosPortConfigTable (UINT4 *pu4FsQoSIfIndex)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Give the First Interfacae */
    *pu4FsQoSIfIndex = 1;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQosPortConfigTable
 Input       :  The Indices
                FsQoSIfIndex
                nextFsQoSIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQosPortConfigTable (UINT4 u4FsQoSIfIndex,
                                     UINT4 *pu4NextFsQoSIfIndex)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (u4FsQoSIfIndex >= SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        return (SNMP_FAILURE);
    }
    *pu4NextFsQoSIfIndex = u4FsQoSIfIndex + 1;
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQoSVlanPriPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                retValFsQoSVlanPriPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVlanPriPriority (UINT4 u4FsQoSIfIndex,
                            INT4 *pi4RetValFsQoSVlanPriPriority)
{
    UINT4               u4ErrorCode;
    INT4                i4RetStatus;

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSVlanPriPriority =
        gQoSPortConfigTable[u4FsQoSIfIndex].u1VlanPriStatus;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSIPDscpPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                retValFsQoSIPDscpPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSIPDscpPriority (UINT4 u4FsQoSIfIndex,
                           INT4 *pi4RetValFsQoSIPDscpPriority)
{
    INT4                i4RetStatus;
    UINT4               u4ErrorCode;

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSIPDscpPriority =
        gQoSPortConfigTable[u4FsQoSIfIndex].u1IPDscpStatus;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSVlanIPPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                retValFsQoSVlanIPPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVlanIPPriority (UINT4 u4FsQoSIfIndex,
                           INT4 *pi4RetValFsQoSVlanIPPriority)
{
    UINT4               u4ErrorCode;
    INT4                i4RetStatus;

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSVlanIPPriority =
        gQoSPortConfigTable[u4FsQoSIfIndex].u1VlanIPStatus;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSVIDOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                retValFsQoSVIDOverride
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVIDOverride (UINT4 u4FsQoSIfIndex, INT4 *pi4RetValFsQoSVIDOverride)
{
    UINT4               u4ErrorCode;
    INT4                i4RetStatus;

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSVIDOverride =
        gQoSPortConfigTable[u4FsQoSIfIndex].u1VIDTagStatus;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSSAOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                retValFsQoSSAOverride
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSAOverride (UINT4 u4FsQoSIfIndex, INT4 *pi4RetValFsQoSSAOverride)
{
    UINT4               u4ErrorCode;
    INT4                i4RetStatus;

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSSAOverride =
        gQoSPortConfigTable[u4FsQoSIfIndex].u1SAOverrideStatus;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSDAOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                retValFsQoSDAOverride
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSDAOverride (UINT4 u4FsQoSIfIndex, INT4 *pi4RetValFsQoSDAOverride)
{
    UINT4               u4ErrorCode;
    INT4                i4RetStatus;

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSDAOverride =
        gQoSPortConfigTable[u4FsQoSIfIndex].u1DAOverrideStatus;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSVlanPriPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                setValFsQoSVlanPriPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVlanPriPriority (UINT4 u4FsQoSIfIndex,
                            INT4 i4SetValFsQoSVlanPriPriority)
{
    gQoSPortConfigTable[u4FsQoSIfIndex].u1VlanPriStatus =
        (UINT1) i4SetValFsQoSVlanPriPriority;

    /* Do the configuration in hardware */
    if (QosHwWrSetVlanPriStatus (u4FsQoSIfIndex, i4SetValFsQoSVlanPriPriority)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsQoSIPDscpPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                setValFsQoSIPDscpPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSIPDscpPriority (UINT4 u4FsQoSIfIndex,
                           INT4 i4SetValFsQoSIPDscpPriority)
{
    gQoSPortConfigTable[u4FsQoSIfIndex].u1IPDscpStatus =
        (UINT1) i4SetValFsQoSIPDscpPriority;

    /* Do the configuration in hardware */
    if (QosHwWrSetIPDscpStatus (u4FsQoSIfIndex, i4SetValFsQoSIPDscpPriority)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsQoSVlanIPPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                setValFsQoSVlanIPPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVlanIPPriority (UINT4 u4FsQoSIfIndex,
                           INT4 i4SetValFsQoSVlanIPPriority)
{
    gQoSPortConfigTable[u4FsQoSIfIndex].u1VlanIPStatus =
        (UINT1) i4SetValFsQoSVlanIPPriority;

    /* Do the configuration in hardware */
    if (QosHwWrSetVlanIPStatus (u4FsQoSIfIndex, i4SetValFsQoSVlanIPPriority)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsQoSVIDOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                setValFsQoSVIDOverride
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVIDOverride (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSVIDOverride)
{
    gQoSPortConfigTable[u4FsQoSIfIndex].u1VIDTagStatus =
        (UINT1) i4SetValFsQoSVIDOverride;

    /* Do the configuration in hardware */
    if (QosHwWrSetVIDOverrideStatus (u4FsQoSIfIndex, i4SetValFsQoSVIDOverride)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsQoSSAOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                setValFsQoSSAOverride
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSAOverride (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSSAOverride)
{
    gQoSPortConfigTable[u4FsQoSIfIndex].u1SAOverrideStatus =
        (UINT1) i4SetValFsQoSSAOverride;

    /* Do the configuration in hardware */
    if (QosHwWrSetSAOverrideStatus (u4FsQoSIfIndex, i4SetValFsQoSSAOverride)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsQoSDAOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                setValFsQoSDAOverride
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSDAOverride (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSDAOverride)
{
    gQoSPortConfigTable[u4FsQoSIfIndex].u1DAOverrideStatus =
        (UINT1) i4SetValFsQoSDAOverride;

    /* Do the configuration in hardware */
    if (QosHwWrSetDAOverrideStatus (u4FsQoSIfIndex, i4SetValFsQoSDAOverride)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQoSVlanPriPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                testValFsQoSVlanPriPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVlanPriPriority (UINT4 *pu4ErrorCode, UINT4 u4FsQoSIfIndex,
                               INT4 i4TestValFsQoSVlanPriPriority)
{
    INT4                i4RetStatus;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsQoSVlanPriPriority != QOS_ENABLE &&
        i4TestValFsQoSVlanPriPriority != QOS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSIPDscpPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                testValFsQoSIPDscpPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSIPDscpPriority (UINT4 *pu4ErrorCode, UINT4 u4FsQoSIfIndex,
                              INT4 i4TestValFsQoSIPDscpPriority)
{
    INT4                i4RetStatus;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsQoSIPDscpPriority != QOS_ENABLE &&
        i4TestValFsQoSIPDscpPriority != QOS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSVlanIPPriority
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                testValFsQoSVlanIPPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVlanIPPriority (UINT4 *pu4ErrorCode, UINT4 u4FsQoSIfIndex,
                              INT4 i4TestValFsQoSVlanIPPriority)
{
    INT4                i4RetStatus;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsQoSVlanIPPriority != QOS_ENABLE &&
        i4TestValFsQoSVlanIPPriority != QOS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSVIDOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                testValFsQoSVIDOverride
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVIDOverride (UINT4 *pu4ErrorCode, UINT4 u4FsQoSIfIndex,
                           INT4 i4TestValFsQoSVIDOverride)
{
    INT4                i4RetStatus;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsQoSVIDOverride != QOS_ENABLE &&
        i4TestValFsQoSVIDOverride != QOS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSSAOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                testValFsQoSSAOverride
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSAOverride (UINT4 *pu4ErrorCode, UINT4 u4FsQoSIfIndex,
                          INT4 i4TestValFsQoSSAOverride)
{
    INT4                i4RetStatus;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsQoSSAOverride != QOS_ENABLE &&
        i4TestValFsQoSSAOverride != QOS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSDAOverride
 Input       :  The Indices
                FsQoSIfIndex

                The Object 
                testValFsQoSDAOverride
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSDAOverride (UINT4 *pu4ErrorCode, UINT4 u4FsQoSIfIndex,
                          INT4 i4TestValFsQoSDAOverride)
{
    INT4                i4RetStatus;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex ((INT4) u4FsQoSIfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsQoSDAOverride != QOS_ENABLE &&
        i4TestValFsQoSDAOverride != QOS_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQosPortConfigTable
 Input       :  The Indices
                FsQoSIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQosPortConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
