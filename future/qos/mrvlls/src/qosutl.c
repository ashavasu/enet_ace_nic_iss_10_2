/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/* $Id: qosutl.c,v 1.5 2013/02/15 13:24:54 siva Exp $                      */
/*                                                                          */
/*  FILE NAME             : qosutl.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-UTL                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the utility functions        */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef _QOS_UTL_C_
#define _QOS_UTL_C_

#include "qosinc.h"
#include "fsvlan.h"
#include "l2iwf.h"

/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSUtlValidateTableName                              */
/* Description        : This function is used validate the Given Table Name  */
/*                      String. Set the ErrorCode accordingly.               */
/*                      1.It check the Length of the String, it should be    */
/*                        1 to 31 Characters. ERR = WRONG_LENGTH             */
/*                      2.It check the Chars of  the String, it can be       */
/*                        ALPAH, DIGIT , "_" ,"-". ERR = WRONG_VALUE         */
/* Input(s)           : pSNMPOctSrt - Pointer to SNMP OCT STR.               */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateTableName (tSNMP_OCTET_STRING_TYPE * pSNMPOctSrt,
                         UINT4 *pu4ErrorCode)
{
    UINT1              *pu1String = NULL;
    INT4                i4StrLen = 0;

    if ((pSNMPOctSrt == NULL) || (pSNMPOctSrt->pu1_OctetList == NULL))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: Name String is NULL.\r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return (QOS_FAILURE);
    }

    pu1String = pSNMPOctSrt->pu1_OctetList;
    i4StrLen = pSNMPOctSrt->i4_Length;

    /* Check Length */
    if ((i4StrLen < 1) || (i4StrLen >= QOS_MAX_TABLE_NAME))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Table name length %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4StrLen, (QOS_MAX_TABLE_NAME - 1));

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return (QOS_FAILURE);
    }

    /* Check Char. */
    while (i4StrLen > 0)
    {
        if ((isalpha (*pu1String) || (ISDIGIT (*pu1String)) ||
             ((*pu1String) == '-') || ((*pu1String) == '_')) == 0)

        {
            QOS_TRC_ARG2 (MGMT_TRC, "%s : Table name  Invalid %c char."
                          "Valid Char = (A-Z, a-z, 0-9, _, - ).\r\n",
                          __FUNCTION__, *pu1String);

            CLI_SET_ERR (QOS_CLI_ERR_TBL_NAME_INVALID);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            return (QOS_FAILURE);
        }

        pu1String++;
        i4StrLen--;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateIfIndex                                */
/* Description        : This function is used validate the Given Interface   */
/*                      with the interface module.                           */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4IfIndex   - Interface Number.                      */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateIfIndex (UINT4 u4IfIndex, UINT4 *pu4ErrorCode)
{
    /* Check the Interface index */
    if ((u4IfIndex == 0) || (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : Invalid IfIndex %d.\r\n",
                      __FUNCTION__, u4IfIndex);
        CLI_SET_ERR (QOS_CLI_ERR_INVALID_IF_INDEX);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateVlanId                                 */
/* Description        : This function is used validate the Given Vlan Id     */
/*                      with the interface module.                           */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4VlanId    - Vland Id.                              */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateVlanId (UINT4 u4VlanId, UINT4 *pu4ErrorCode)
{
    /* Check the VlaId is Valid */
    if ((u4VlanId < VLAN_DEV_MIN_VLAN_ID) || (u4VlanId > VLAN_MAX_VLAN_ID))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : Invalid Vlan Id %d.\r\n",
                      __FUNCTION__, u4VlanId);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_INDEX);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSInPriMapCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the PriMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSInPriMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSInPriorityMapNode *pInPriMapNode1 = NULL;
    tQoSInPriorityMapNode *pInPriMapNode2 = NULL;

    pInPriMapNode1 = (tQoSInPriorityMapNode *) e1;
    pInPriMapNode2 = (tQoSInPriorityMapNode *) e2;

    /* Compare the InPriMap Id */

    if (pInPriMapNode1->u4Id < pInPriMapNode2->u4Id)
    {
        return (-1);
    }
    else if (pInPriMapNode1->u4Id > pInPriMapNode2->u4Id)
    {
        return (1);
    }

    return (0);
}

INT4
QoSInPriMapUniCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSInPriorityMapNode *pInPriMapNode1 = NULL;
    tQoSInPriorityMapNode *pInPriMapNode2 = NULL;

    pInPriMapNode1 = (tQoSInPriorityMapNode *) e1;
    pInPriMapNode2 = (tQoSInPriorityMapNode *) e2;

    /* Compare the Incoming Interface */
    if (pInPriMapNode1->u4IfIndex < pInPriMapNode2->u4IfIndex)
    {
        return (-1);
    }
    else if (pInPriMapNode1->u4IfIndex > pInPriMapNode2->u4IfIndex)
    {
        return (1);
    }
    else
    {
        /* Compare the Incoming VLAN ID   */
        if (pInPriMapNode1->u2VlanId < pInPriMapNode2->u2VlanId)
        {
            return (-1);
        }
        else if (pInPriMapNode1->u2VlanId > pInPriMapNode2->u2VlanId)
        {
            return (1);
        }
        else
        {
            /* Compare the Incoming priority type */
            if (pInPriMapNode1->u1InType < pInPriMapNode2->u1InType)
            {
                return (-1);
            }
            else if (pInPriMapNode1->u1InType > pInPriMapNode2->u1InType)
            {
                return (1);
            }
            else
            {
                /* Compare the Incoming priority Value */
                if (pInPriMapNode1->u1InPriority < pInPriMapNode2->u1InPriority)
                {
                    return (-1);
                }
                else if (pInPriMapNode1->u1InPriority >
                         pInPriMapNode2->u1InPriority)
                {
                    return (1);
                }
                else
                {
                    if (MEMCMP (pInPriMapNode1->MacAddr,
                                pInPriMapNode2->MacAddr, sizeof (tMacAddr)) < 0)
                    {
                        return (-1);
                    }
                    else if (MEMCMP (pInPriMapNode1->MacAddr,
                                     pInPriMapNode2->MacAddr,
                                     sizeof (tMacAddr)) > 0)
                    {
                        return (1);
                    }
                    else
                    {
                        return (0);
                    }
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : QoSInPriTblRBTFreeNodeFn                             */
/* Description        : This function is used to Delete the Priority Map     */
/*                      Table Entries and Release the Memory to the          */
/*                      respective Mem Pool .                                */
/* Input(s)           : tRBElem     - Pointer to Priority Map Table Entry    */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSInPriTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : InPriTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSInPriorityMapNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSInPriMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : InPriTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetPriorityMapNode                             */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSInPriorityMapNode'             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSInPriorityMapNode *
QoSUtlGetPriorityMapNode (UINT4 u4PriMapId)
{
    tQoSInPriorityMapNode TempPriMapNode;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    MEMSET (&TempPriMapNode, 0, sizeof (tQoSInPriorityMapNode));
    TempPriMapNode.u4Id = u4PriMapId;

    pPriMapNode = (tQoSInPriorityMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbInPriMapTbl, (tRBElem *) & TempPriMapNode);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Priority Map Id %d is not Found in the "
                      "Priority Map Table. \r\n", __FUNCTION__, u4PriMapId);
    }

    return (pPriMapNode);
}

tQoSInPriorityMapNode *
QoSUtlGetPriorityMapUniNode (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1InPri,
                             UINT1 u1InType)
{
    tQoSInPriorityMapNode TempPriMapNode;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    MEMSET (&TempPriMapNode, 0, sizeof (tQoSInPriorityMapNode));

    TempPriMapNode.u4IfIndex = u4IfIndex;
    TempPriMapNode.u2VlanId = u2VlanId;
    TempPriMapNode.u1InPriority = u1InPri;
    TempPriMapNode.u1InType = u1InType;

    pPriMapNode = (tQoSInPriorityMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbInPriMapUniTbl,
                   (tRBElem *) & TempPriMapNode);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG5 (MGMT_TRC, "%s :UniquePriMapNode (Inedx %d.%d.%d.%d) "
                      " is not Found in the Priority Map Table. \r\n",
                      __FUNCTION__, u4IfIndex, u2VlanId, u1InPri, u1InType);
    }

    return (pPriMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreatePriMapTblEntry                              */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Priority Map Table    */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSInPriorityMapNode *
QoSCreatePriMapTblEntry (UINT4 u4PriMapId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSInPriorityMapNode *pNewInPriMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewInPriMapNode = (tQoSInPriorityMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSInPriMapTblMemPoolId);
    if (pNewInPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for Priority Map Id %d.\r\n", __FUNCTION__, u4PriMapId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewInPriMapNode, 0, sizeof (tQoSInPriorityMapNode));
    SPRINTF ((CHR1 *) pNewInPriMapNode->au1Name, "PRI-%d", u4PriMapId);

    pNewInPriMapNode->u4Id = u4PriMapId;
    pNewInPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_SYS;
    pNewInPriMapNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Priority Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbInPriMapTbl,
                             (tRBElem *) pNewInPriMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSInPriMapTblMemPoolId,
                                (UINT1 *) pNewInPriMapNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for PriMapId %d. \r\n", __FUNCTION__, u4PriMapId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for PriMapId %d. \r\n", __FUNCTION__, u4PriMapId);
        return (NULL);
    }

    return (pNewInPriMapNode);
}

INT4
QoSAddPriMapUniTblEntry (tQoSInPriorityMapNode * pPriMapNode)
{
    tQoSInPriorityMapNode *pTmpPriMapNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Get this Entry from the Priority Map Unique Table
     * if it fails add it.*/
    pTmpPriMapNode = RBTreeGet (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                (tRBElem *) pPriMapNode);
    if (pTmpPriMapNode == NULL)
    {
        /* 2. Add this New Entry into the Priority Map Unique Table */
        u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                 (tRBElem *) pPriMapNode);
        if (u4RetStatus != RB_SUCCESS)
        {
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeletePriMapTblEntry                              */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. Remove the Entry from the Priority Map Table      */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pInPriMapNode   - Pointer to InPriMapNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeletePriMapUniTblEntry (tQoSInPriorityMapNode * pPriMapNode)
{
    tQoSInPriorityMapNode *pTmpPriMapNode = NULL;

    pTmpPriMapNode = RBTreeGet (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                (tRBElem *) pPriMapNode);
    if (pTmpPriMapNode != NULL)
    {
        pTmpPriMapNode = RBTreeRem (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                    (tRBElem *) pPriMapNode);
        if (pTmpPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed for "
                          "UniqueNode. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

INT4
QoSDeletePriMapTblEntry (tQoSInPriorityMapNode * pInPriMapNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSInPriorityMapNode *pDelInPriMapNode = NULL;

    if (pInPriMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the Priority Map Table */
    pDelInPriMapNode = RBTreeRem (gQoSGlobalInfo.pRbInPriMapTbl,
                                  (tRBElem *) pInPriMapNode);
    if (pDelInPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 1a. Remove Entry from the Priority Map Unique Table */
    u4RetStatus = QoSDeletePriMapUniTblEntry (pInPriMapNode);
    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeletePriMapUniTblEntry () "
                      "Failed for UniqueNode. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelInPriMapNode, 0, sizeof (tQoSInPriorityMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSInPriMapTblMemPoolId,
                                      (UINT1 *) pDelInPriMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextInPriMapTblEntryIndex                      */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextInPriMapTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSInPriorityMapNode *pInPriMapNode = NULL;
    tQoSInPriorityMapNode InPriMapNode;

    if (u4CurrentIndex == 0)
    {
        pInPriMapNode = (tQoSInPriorityMapNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbInPriMapTbl);
    }
    else
    {
        InPriMapNode.u4Id = u4CurrentIndex;
        pInPriMapNode = (tQoSInPriorityMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbInPriMapTbl,
                           &InPriMapNode, QoSInPriMapCmpFun);
    }

    if (pInPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for PriMapId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pInPriMapNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePriMapTblIdxInst                       */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Priority Map Table, if it is ACTIVE  */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSPriorityMapID - PriMapID                      */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePriMapTblIdxInst (UINT4 u4FsQoSPriorityMapID, UINT4 *pu4ErrorCode)
{
    INT4                i4RetStatus = 0;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RANGE);

        return (QOS_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (QOS_FAILURE);
    }

    if (pPriMapNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePriMapTblMacAddress                    */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Priority Map Table, if it is ACTIVE  */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSPriorityMapID - PriMapID                      */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePriMapTblMacAddress (UINT4 u4FsQoSPriorityMapID, tMacAddr MacAddr,
                                   UINT4 *pu4ErrorCode)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (QOS_FAILURE);
    }

    if (VlanGetFdbEntryDetails (pPriMapNode->u2VlanId,
                                MacAddr, &u2Port, &u1Status) != SUCCESS)
    {
        QOS_TRC (MGMT_TRC, "Mac Entry does not exist.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_MAC_ADDRESS_NOT_PRESENT);

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePriMapTblVlanId                        */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Priority Map Table, if it is ACTIVE  */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSPriorityMapID - PriMapID                      */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePriMapTblVlanId (UINT2 u2VlanId, UINT4 *pu4ErrorCode)
{

    if (L2IwfIsVlanActive (u2VlanId) == OSIX_FALSE)
    {
        QOS_TRC (MGMT_TRC, "Mac Entry does not exist.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_VLAN_NOT_PRESENT);

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     Q MAP TABLE FUNCTIONS                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSQMapCmpFun                                        */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the tQoSQMapNode and return the values */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSQMapNode       *pQMapNode1 = NULL;
    tQoSQMapNode       *pQMapNode2 = NULL;

    pQMapNode1 = (tQoSQMapNode *) e1;
    pQMapNode2 = (tQoSQMapNode *) e2;

    /* Compare the QMap RegenPriType */
    if (pQMapNode1->u1RegenPriType < pQMapNode2->u1RegenPriType)
    {
        return (-1);
    }
    else if (pQMapNode1->u1RegenPriType > pQMapNode2->u1RegenPriType)
    {
        return (1);
    }
    else
    {
        /* Compare the QMap RegenPri Value */
        if (pQMapNode1->u1RegenPri < pQMapNode2->u1RegenPri)
        {
            return (-1);
        }
        else if (pQMapNode1->u1RegenPri > pQMapNode2->u1RegenPri)
        {
            return (1);
        }
        else
        {
            return (0);
        }
    }
}

/*****************************************************************************/
/* Function Name      : QoSQMapTblRBTFreeNodeFn                              */
/* Description        : This function is used to Delete the Q Map  Table     */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Q Map  Table Entry          */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQMapTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QMap Tbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSQMapNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QMapTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetQMapNode                                    */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4QMapId    - Index to tQoSQMapNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSQMapNode'                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQMapNode       *
QoSUtlGetQMapNode (INT4 i4RegenPriType, UINT4 u4RegenPri)
{
    tQoSQMapNode        TempQMapNode;
    tQoSQMapNode       *pQMapNode = NULL;

    MEMSET (&TempQMapNode, 0, sizeof (tQoSQMapNode));

    TempQMapNode.u1RegenPri = (UINT1) u4RegenPri;
    TempQMapNode.u1RegenPriType = (UINT1) i4RegenPriType;

    pQMapNode = (tQoSQMapNode *) RBTreeGet (gQoSGlobalInfo.pRbQMapTbl,
                                            (tRBElem *) & TempQMapNode);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : QMap Index %d.%d is not Found "
                      "in the QMap Table.\r\n", __FUNCTION__,
                      i4RegenPriType, u4RegenPri);
    }

    return (pQMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateQMapTblEntry                                */
/* Description        : This function is used to Create an Entry in   the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the QMap Table            */
/* Input(s)           : u4QMapId      - Index to QMapNode                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  QMapNode                          */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQMapNode       *
QoSCreateQMapTblEntry (INT4 i4RegenPriType, UINT4 u4RegenPri)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSQMapNode       *pNewQMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewQMapNode = (tQoSQMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSQMapTblMemPoolId);
    if (pNewQMapNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for QMapIndex %d.%d \r\n", __FUNCTION__,
                      i4RegenPriType, u4RegenPri);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewQMapNode, 0, sizeof (tQoSQMapNode));

    pNewQMapNode->u1RegenPriType = (UINT1) i4RegenPriType;
    pNewQMapNode->u1RegenPri = (UINT1) u4RegenPri;
    pNewQMapNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the QMap Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbQMapTbl,
                             (tRBElem *) pNewQMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSQMapTblMemPoolId,
                                (UINT1 *) pNewQMapNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                          i4RegenPriType, u4RegenPri);

            return (NULL);
        }

        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                      i4RegenPriType, u4RegenPri);
        return (NULL);
    }

    return (pNewQMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextQMapTblEntryIndex                          */
/* Description        : This function is used to Get the Next Entry from the */
/*                      QMap Table ,it will do the following Action          */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextQMapTblEntryIndex (INT4 i4RegenPriType, INT4 *pi4NextRegenPriType,
                             UINT4 u4RegenPri, UINT4 *pu4NextRegenPri)
{
    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQMapNode        QMapNode;

    QMapNode.u1RegenPriType = (UINT1) i4RegenPriType;
    QMapNode.u1RegenPri = (UINT1) u4RegenPri;

    pQMapNode = (tQoSQMapNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQMapTbl,
                                                &QMapNode, QoSQMapCmpFun);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for "
                      "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                      i4RegenPriType, u4RegenPri);
        return (QOS_FAILURE);
    }

    *pi4NextRegenPriType = pQMapNode->u1RegenPriType;
    *pu4NextRegenPri = pQMapNode->u1RegenPri;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteQMapTblEntry                                */
/* Description        : This function is used to Delete an Entry from the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Remove the Entry from the QMap Table              */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pQMapNode   - Pointer to QMapNode                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    tQoSQMapNode       *pDelQMapNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pQMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the QMap Table */
    pDelQMapNode = RBTreeRem (gQoSGlobalInfo.pRbQMapTbl, (tRBElem *) pQMapNode);
    if (pDelQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelQMapNode, 0, sizeof (tQoSQMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQMapTblMemPoolId,
                                      (UINT1 *) pDelQMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQMapTblIdxInst                         */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the QMap Table, if it is ACTIVE          */
/*                      return Failure Success otherwise.                    */
/* Input(s)           :                                                      */
/*                    : u4RegenPriType   -  CLASS Value                      */
/*                    : u4RegenPri        - RegnPriority Value               */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQMapTblIdxInst (INT4 i4RegenPriType, UINT4 u4RegenPri,
                              UINT4 *pu4ErrorCode)
{
    tQoSQMapNode       *pQMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    switch (i4RegenPriType)
    {

        case QOS_QMAP_PRI_TYPE_VLAN_PRI:

            if (u4RegenPri > QOS_QMAP_PRI_VLAN_PRI_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if (u4RegenPri > QOS_QMAP_PRI_IP_DSCP_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            return (SNMP_FAILURE);
    }

    pQMapNode = QoSUtlGetQMapNode (i4RegenPriType, u4RegenPri);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_QMAP_NO_ENTRY);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateQMapTblEntry                              */
/* Description        : This function is used validate the Mandatory Params  */
/*                      for a Give Q Map Node.                               */
/* Input(s)           : pQMapNode   - Ptr tp tQoSQMapNode                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    if (pQMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /*Check Mandatory Fields */
    if (pQMapNode->u4QId == 0)
    {
        pQMapNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    /* All Mandatory Fields are set, change the State to NOT_IN_SERVICE */
    pQMapNode->u1Status = NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefQMapTblEntry                                 */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  Queue Map Id.If not         */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSQMapNode- pQMapNode                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    if (pQMapNode != NULL)
    {

        if (pQMapNode->u1RegenPriType == QOS_QMAP_PRI_TYPE_VLAN_PRI)
        {
            if (pQMapNode->u1RegenPri <= QOS_PM_TBL_DEF_ENTRY_MAX)
            {
                return (QOS_SUCCESS);
            }
        }
        if (pQMapNode->u1RegenPriType == QOS_QMAP_PRI_TYPE_IP_DSCP)
        {
            if (pQMapNode->u1RegenPri <= QOS_IN_PRIORITY_IP_DSCP_MAX)
            {
                return (QOS_SUCCESS);
            }
        }
    }
    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : RbWalkAction                                         */
/* Description        : This function is used to check Gvie Name in the      */
/*                    : respective table is unique or not                    */
/* Input(s)           : e           -  tRBElem      ptr                      */
/*                    : visit       - Type of the eRBVisit                   */
/*                    : level       - RBTree Search level                    */
/*                    : arg         - Input Arg                              */
/*                    : out         - Output Arg - Return value              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
RbWalkAction (tRBElem * e, eRBVisit visit, UINT4 level, void *arg, void *out)
{
    tQoSRBWalkInput    *pRbWalkInput = NULL;
    UINT1              *pu1TblEntryName = NULL;

    *(UINT4 *) out = QOS_SUCCESS;
    pRbWalkInput = (tQoSRBWalkInput *) arg;

    UNUSED_PARAM (level);

    if ((visit == leaf) || (visit == postorder))
    {
        switch (pRbWalkInput->u4TblType)
        {
            case QOS_TBL_TYPE_PRI_MAP:
                pu1TblEntryName = ((tQoSInPriorityMapNode *) e)->au1Name;
                break;

            default:
                return (QOS_FAILURE);
                break;
        }

        /* Check the Name  is Unique or Not */
        if ((STRLEN (pu1TblEntryName)) == (UINT4)
            pRbWalkInput->pTblEntryName->i4_Length)
        {
            if ((STRNCMP (pu1TblEntryName,
                          pRbWalkInput->pTblEntryName->pu1_OctetList,
                          pRbWalkInput->pTblEntryName->i4_Length)) == 0)
            {
                *(UINT4 *) out = QOS_FAILURE;
                return (RB_WALK_BREAK);
            }
        }

    }

    return (RB_WALK_CONT);
}

/*****************************************************************************/
/* Function Name      : QoSUtlIsUniqueName                                   */
/* Description        : This function is used to check Unique name in the    */
/*                    : Give Table                                           */
/* Input(s)           : pFsQoSTblEntryName  - Tbl Entry Name                 */
/*                    : u4TblType           - Tbl Type                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlIsUniqueName (tSNMP_OCTET_STRING_TYPE * pFsQoSTblEntryName,
                    UINT4 u4TblType)
{
    tQoSRBWalkInput     RbWalkInput;
    tRBTree             pRbTree = NULL;
    UINT4               u4Out = QOS_FAILURE;

    RbWalkInput.pTblEntryName = pFsQoSTblEntryName;

    switch (u4TblType)
    {
        case QOS_TBL_TYPE_PRI_MAP:
            RbWalkInput.u4TblType = QOS_TBL_TYPE_PRI_MAP;
            pRbTree = gQoSGlobalInfo.pRbInPriMapTbl;
            break;

        default:
            return (QOS_FAILURE);
            break;
    }

    RBTreeWalk (pRbTree, RbWalkAction, (void *) &RbWalkInput, (void *) &u4Out);
    if (u4Out == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefPriorityMapTblEntry                          */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  Priority Map Id.If not      */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSInPriorityMapNode- pPriMapNode                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSIsDefPriorityMapTblEntry (tQoSInPriorityMapNode * pPriMapNode)
{
    if (pPriMapNode != NULL)
    {
        if ((pPriMapNode->u4Id > 0) &&
            (pPriMapNode->u4Id <= QOS_PM_TBL_DEF_ENTRY_MAX + 1))
        {
            return (QOS_SUCCESS);
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*     FUNCTION NAME    : QoSUtlIsDefQMapTblEntry                            */
/*                                                                           */
/*     DESCRIPTION      : This function used to check the given QMap entry   */
/*                        is default or not.                                 */
/*                                                                           */
/*     INPUT            : pQMapNode - pointer to QoSQMapEntry                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS/QOS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
QoSUtlIsDefQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    UINT4               u4QIdx = 0;

    UINT1               au1QId[DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE] =
        { 1, 0, 0, 1, 2, 2, 3, 3 };

    if (pQMapNode->u1RegenPriType == QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI)
    {
        if (pQMapNode->u4QId == au1QId[(pQMapNode->u1RegenPri)])
        {
            return (QOS_SUCCESS);
        }
        else
        {
            return (QOS_FAILURE);
        }
    }
    else
    {
        if (pQMapNode->u1RegenPri <= QOS_ASSIGN_TRAFFIC_CLASS_0)
        {
            u4QIdx = 0;
        }
        else if (pQMapNode->u1RegenPri <= QOS_ASSIGN_TRAFFIC_CLASS_1)
        {
            u4QIdx = 1;
        }
        else if (pQMapNode->u1RegenPri <= QOS_ASSIGN_TRAFFIC_CLASS_2)
        {
            u4QIdx = 2;
        }
        else if (pQMapNode->u1RegenPri <= QOS_ASSIGN_TRAFFIC_CLASS_3)
        {
            u4QIdx = 3;
        }

        if (pQMapNode->u4QId == u4QIdx)
        {
            return (QOS_SUCCESS);
        }
        else
        {
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}
#endif /* _QOS_UTL_C_ */
