/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/* $Id: qosnpwr.c,v 1.4 2010/11/18 13:36:20 siva Exp $                     */
/*                                                                          */
/*  FILE NAME             : qosnpwr.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-NP-WR                                       */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the NP Wraper Functions      */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_NP_WR_C__
#define __QOS_NP_WR_C__

#include "qosinc.h"
#include "difsrvnp.h"

/*****************************************************************************/
/* Function Name      : QoSHwWrInit                                          */
/* Description        : This function is used to init the QoS in the         */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrInit (VOID)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QoSHwInit ();

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwInit () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrDeInit                                        */
/* Description        : This function is used to De-init the QoS in the      */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrDeInit (VOID)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QoSHwDeInit ();

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwDeInit () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMapClassToQueue                               */
/* Description        : This function is used to map Class or Priority to Q  */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           :                                                      */
/*                    :                                                      */
/*                    : i4RegenPriType- VLAN/TOS/DSCp/MPLS/NONE              */
/*                    : u4Priority    - Priority Value of above Type         */
/*                    : u4QId         - Q Id to the CLASS or  RegenPri       */
/*                    : u1Flag        - Map(1) or UnMap(2)                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMapClassToQueue (INT4 i4RegenPriType, UINT4 u4Priority, UINT4 u4QId,
                        UINT1 u1Flag)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;

    UNUSED_PARAM (u1Flag);

    switch (i4RegenPriType)
    {
        case QOS_QMAP_PRI_TYPE_VLAN_PRI:

            i4RetStatus = QosHwMapVlanPriTrafficClass (u4Priority, u4QId);

            break;

        case QOS_QMAP_PRI_TYPE_IP_DSCP:

            i4RetStatus = QosHwMapDSCPTrafficClass (u4Priority, u4QId);
            break;

        default:

            break;
    }

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToQueue () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (i4RegenPriType);
    UNUSED_PARAM (u4Priority);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSetSchedulingAlgo                             */
/* Description        : This function is used to set the scheduling policy   */
/*                      for the hardware                                     */
/* Input(s)           : UINT4 - Scheduling Algorithm                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSetSchedulingAlgo (UINT4 u4SchedulingAlgo)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwSetSchedulingAlgo (u4SchedulingAlgo);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSetSchedulingAlgo () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (u4SchedulingAlgo);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrAddPriMapEntry                                */
/* Description        : This function is used to program the priority        */
/*                      information in the hardware                          */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrAddPriMapEntry (tQoSInPriorityMapNode * pPriMapNode)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    UINT4               u4IfIndex = 0;

    switch (pPriMapNode->u1InType)
    {
        case QOS_IN_PRI_TYPE_VLAN_PRI:

            if (pPriMapNode->u4IfIndex == 0)
            {
                for (u4IfIndex = 1;
                     u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES; u4IfIndex++)
                {

                    i4RetStatus = QosHwSetRegenUserPriority (u4IfIndex,
                                                             pPriMapNode->
                                                             u1InPriority,
                                                             pPriMapNode->
                                                             u1RegenPriority);
                }
            }
            else
            {

                i4RetStatus = QosHwSetRegenUserPriority
                    (pPriMapNode->u4IfIndex, pPriMapNode->u1InPriority,
                     pPriMapNode->u1RegenPriority);
            }

            break;

        case QOS_IN_PRI_TYPE_VLAN_ID:

            i4RetStatus = QosHwMapVlanRegenPriority (pPriMapNode->u2VlanId,
                                                     pPriMapNode->
                                                     u1RegenPriority);
            break;

        case QOS_IN_PRI_TYPE_SRC_MAC:
        case QOS_IN_PRI_TYPE_DEST_MAC:    /* Intentional Fall through */

            i4RetStatus = QosHwMapMacRegenPriority (pPriMapNode->u2VlanId,
                                                    pPriMapNode->MacAddr,
                                                    pPriMapNode->
                                                    u1RegenPriority);
            break;

        default:

            break;
    }

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwWrAddPriMapEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

#else
    UNUSED_PARAM (pPriMapNode);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrDelPriorityEntry                              */
/* Description        : This function is used to program the priority        */
/*                      information in the hardware                          */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrDelPriorityEntry (tQoSInPriorityMapNode * pPriMapNode)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    UINT4               u4IfIndex = 0;

    switch (pPriMapNode->u1InType)
    {
        case QOS_IN_PRI_TYPE_VLAN_PRI:

            if (pPriMapNode->u4IfIndex == 0)
            {
                for (u4IfIndex = 1;
                     u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES; u4IfIndex++)
                {

                    i4RetStatus = QosHwSetRegenUserPriority (u4IfIndex,
                                                             pPriMapNode->
                                                             u1InPriority,
                                                             pPriMapNode->
                                                             u1InPriority);
                }
            }
            else
            {

                i4RetStatus = QosHwSetRegenUserPriority
                    (pPriMapNode->u4IfIndex, pPriMapNode->u1InPriority,
                     pPriMapNode->u1InPriority);
            }

            break;

        case QOS_IN_PRI_TYPE_VLAN_ID:

            i4RetStatus = QosHwMapVlanRegenPriority (pPriMapNode->u2VlanId,
                                                     QOS_RESET_PRI_MAP_PRI);
            break;

        case QOS_IN_PRI_TYPE_SRC_MAC:
        case QOS_IN_PRI_TYPE_DEST_MAC:    /* Intentional Fall through */

            i4RetStatus = QosHwMapMacRegenPriority (pPriMapNode->u2VlanId,
                                                    pPriMapNode->MacAddr,
                                                    QOS_RESET_PRI_MAP_PRI);
            break;

        default:

            break;
    }

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwWrDelPriorityEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

#else
    UNUSED_PARAM (pPriMapNode);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetVlanPriStatus                              */
/* Description        : This function is used to configure the Vlan Priority */
/*                      status                                               */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetVlanPriStatus (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSVlanPriStatus)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus =
        QosHwSetVlanPriPriority (u4FsQoSIfIndex, i4SetValFsQoSVlanPriStatus);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwWrSetVlanPriStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (u4FsQoSIfIndex);
    UNUSED_PARAM (i4SetValFsQoSVlanPriStatus);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetIPDscpStatus                               */
/* Description        : This function is used to configure the  IPDSCP Status*/
/*                                                                           */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetIPDscpStatus (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSIPDscpStatus)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus =
        QosHwSetIPDSCPPriority (u4FsQoSIfIndex, i4SetValFsQoSIPDscpStatus);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwWrSetIPDscpStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (u4FsQoSIfIndex);
    UNUSED_PARAM (i4SetValFsQoSIPDscpStatus);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetVlanIPStatus                               */
/* Description        : This function is used to configure the VLANIP Status */
/*                                                                           */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetVlanIPStatus (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSVlanIPStatus)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus =
        QosHwSetVlanIPPriority (u4FsQoSIfIndex, i4SetValFsQoSVlanIPStatus);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwWrSetVlanIPStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (u4FsQoSIfIndex);
    UNUSED_PARAM (i4SetValFsQoSVlanIPStatus);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetSAOverrideStatus                           */
/* Description        : This function is used to configure the SA status     */
/*                                                                           */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetSAOverrideStatus (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSSAOverride)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus = QosHwSetSAOverride (u4FsQoSIfIndex, i4SetValFsQoSSAOverride);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwWrSetSAOverrideStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (u4FsQoSIfIndex);
    UNUSED_PARAM (i4SetValFsQoSSAOverride);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetDAOverrideStatus                           */
/* Description        : This function is used to configure the VID status    */
/*                                                                           */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetDAOverrideStatus (UINT4 u4FsQoSIfIndex, INT4 i4SetValFsQoSDAOverride)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus = QosHwSetDAOverride (u4FsQoSIfIndex, i4SetValFsQoSDAOverride);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwWrSetDAOverrideStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (u4FsQoSIfIndex);
    UNUSED_PARAM (i4SetValFsQoSDAOverride);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetVIDOverrideStatus                          */
/* Description        : This function is used to configure the VID status    */
/*                                                                           */
/* Input(s)           : pPriMapNode                                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetVIDOverrideStatus (UINT4 u4FsQoSIfIndex,
                             INT4 i4SetValFsQoSVIDOverride)
{

#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus =
        QosHwSetVIDOverride (u4FsQoSIfIndex, i4SetValFsQoSVIDOverride);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwWrSetVIDOverrideStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (u4FsQoSIfIndex);
    UNUSED_PARAM (i4SetValFsQoSVIDOverride);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}
#endif /* __QOS_NP_WR_C__ */
