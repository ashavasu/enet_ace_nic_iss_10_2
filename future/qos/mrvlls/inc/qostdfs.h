/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qostdfs.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-TDFS                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the structures and enum  for  */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_TDFS_H__
#define __QOS_TDFS_H__

/* Incoming Priority Map Table  */
typedef struct _tQoSInPriorityMapNode
{
    tRBNodeEmbd RbNode;                  /* Link to Traverse the Table       */
    tRBNodeEmbd RbNodeUnique;            /* Link to Traverse the Table       */
    tMacAddr    MacAddr;
    UINT1 au1Name[QOS_MAX_TABLE_NAME];   /* Name of the Table                */
    UINT4 u4Id;                          /* Index of the Table               */
    UINT4 u4RefCount;                    /* No. of usage of this Entry       */
    UINT4 u4IfIndex;                     /* Incoming Interface (port)        */
    UINT2 u2VlanId;                      /* Incoming VLAN ID                 */
    UINT1 u1InPriority;                  /* Incoming priority Value          */
    UINT1 u1InType;                      /* Incoming type of packet
                                            vlan/ip-dscp/src-mac/dest-mac    */
    UINT1 u1ConfStatus;                  /* Entry configuration status       */
    UINT1 u1RegenPriority;               /* ReGenerator Priority for a InPri.*/
    UINT1 u1Status;                      /* Status of this Entry             */
    UINT1 u1Resvd[1];                   /* 4Byte Alignment                  */

} tQoSInPriorityMapNode;

/* Queue Map Table */
typedef struct _tQoSQMapNode
{
    tRBNodeEmbd RbNode;                 /* Link to Traverse the Table       */
    UINT4 u4QId;                        /* Q Id Mapped with this Entry      */
    UINT1 u1RegenPriType;               /* Regenerator Priority Type        */
    UINT1 u1RegenPri;                   /* Regenerator Priority             */
    UINT1 u1Status;                     /* Status of this Entry             */
    UINT1 u1Resvd[1];                   /* 4Byte Alignment                  */
} tQoSQMapNode;

/* Qos Port configuration table */
typedef struct _tQoSPortConfigTable
{

    UINT1    u1VlanPriStatus;/* variable to enable or disable vlan priority
                                      classificaion on the particular port.*/
    UINT1    u1IPDscpStatus;/* variable to enable/disable IPDscp
                                      classificaion on the particular port.*/
    UINT1    u1VlanIPStatus; /* variable to enable/disable IEEEIP Tag */
    UINT1    u1VIDTagStatus; /* variable to enable/disable VID Override */
    UINT1    u1SAOverrideStatus; /* variable to enable/disable SA Override */
    UINT1    u1DAOverrideStatus; /* variable to enable/disable DA Override */
    UINT1    u1Status;    /* variable to store the status of the port */
}tQoSPortConfigTable;

typedef enum _tSysCntl {

   QOS_SYS_CNTL_SHUTDOWN = 0,  /* 0 */
   QOS_SYS_CNTL_START          /* 1 */

} etSysCntl;

typedef enum _tSysStatus {

    QOS_SYS_STATUS_DISABLE = 0,  /* 0 */
    QOS_SYS_STATUS_ENABLE        /* 1 */

} etSysStatus;

/* Global Structure  */
typedef struct _tQosGlobalInfo
{
    /* Memory Pool Ids for QoS Tables */
    tOsixTaskId QoSTaskId;
    tMemPoolId  QoSInPriMapTblMemPoolId;
    tMemPoolId  QoSQMapTblMemPoolId;
    /* Tables Headers */
    tRBTree     pRbInPriMapTbl;
    tRBTree     pRbInPriMapUniTbl;
    tRBTree     pRbQMapTbl;
    /* System Globals  */
    tOsixSemId  QoSSemId;
    etSysCntl   eSysControl;
    etSysStatus eSysStatus;
    UINT4       u4TrcFlag;
    UINT4       u4QosSchedulingPolicy;
}tQoSGlobalInfo;

typedef struct _tQoSRBWalkInput
{
    tSNMP_OCTET_STRING_TYPE *pTblEntryName;
    UINT4                   u4TblType;

} tQoSRBWalkInput;

#endif /* __QOS_TDFS_H__ */


