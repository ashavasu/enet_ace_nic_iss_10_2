/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qostrc.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-TRACE                                       */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This file contains procedures and definitions   */
/*                          used in debugging and Tracing in Aricent QoS    */
/*                          Module.                                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_TRC_H__
#define __QOS_TRC_H__

/* Trace and debug flags */
#define   QOS_TRC_FLAG            (gQoSGlobalInfo.u4TrcFlag)

/* Module names */
#define   QOS_MOD_NAME            ((const char *)"QoS")

/* Trace definitions */
#ifdef TRACE_WANTED

#define QOS_TRC(TraceType, fmt)                             \
        MOD_TRC(QOS_TRC_FLAG, TraceType, QOS_MOD_NAME,      \
                (const char *)fmt)

#define QOS_TRC_ARG1(TraceType, fmt, arg1)       	        \
        MOD_TRC_ARG1(QOS_TRC_FLAG, TraceType, QOS_MOD_NAME, \
                     (const char *)fmt, arg1)

#define QOS_TRC_ARG2(TraceType, fmt, arg1, arg2)       	    \
        MOD_TRC_ARG2(QOS_TRC_FLAG, TraceType, QOS_MOD_NAME, \
                     (const char *)fmt, arg1, arg2)

#define QOS_TRC_ARG3(TraceType, fmt, arg1, arg2, arg3)	    \
        MOD_TRC_ARG3(QOS_TRC_FLAG, TraceType, QOS_MOD_NAME, \
                     (const char *)fmt, arg1, arg2, arg3)

#define QOS_TRC_ARG4(TraceType, fmt, arg1, arg2, arg3, arg4)   \
        MOD_TRC_ARG4(QOS_TRC_FLAG, TraceType, QOS_MOD_NAME,    \
                     (const char *)fmt, arg1, arg2, arg3, arg4)

#define QOS_TRC_ARG5(TraceType, fmt, arg1, arg2, arg3, arg4,arg5)   \
    MOD_TRC_ARG5(QOS_TRC_FLAG, TraceType, QOS_MOD_NAME,    \
                 (const char *)fmt, arg1, arg2, arg3, arg4, arg5)

#else  /* TRACE_WANTED */

#define QOS_TRC(TraceType, Str)
#define QOS_TRC_ARG1(TraceType, fmt, arg1)
#define QOS_TRC_ARG2(TraceType, fmt, arg1, arg2)       	    
#define QOS_TRC_ARG3(TraceType, fmt, arg1, arg2, arg3)
#define QOS_TRC_ARG4(TraceType, fmt, arg1, arg2, arg3, arg4)
#define QOS_TRC_ARG5(TraceType, fmt, arg1, arg2, arg3, arg4, arg5)

#endif /* TRACE_WANTED */

#endif /* __QOS_TRC_H__ */

