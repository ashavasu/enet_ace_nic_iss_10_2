/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: qosproto.h,v 1.5 2012/04/05 14:04:15 siva Exp $
 *
 * Description : This function contains all the prototype declarations in QOS.
 *
 *****************************************************************************/
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosproto.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-PROTO                                       */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the functions prototype for   */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_PROTO_H__
#define __QOS_PROTO_H__ 


/*****************************************************************************/
/* FileName : qossys.c  functions prototype                                  */
/*****************************************************************************/
/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/
INT4 QoSStart PROTO ((VOID));
INT4 QoSShutdown PROTO ((VOID));
INT4 QoSEnable PROTO ((VOID));
INT4 QoSDisable PROTO ((VOID));
VOID QoSInitGlobal PROTO ((VOID));
VOID QoSDeleteMemPools PROTO ((VOID));
INT4 QoSCreateMemPools PROTO ((VOID));

INT4
QoSAddPriMapUniTblEntry PROTO ((tQoSInPriorityMapNode *pPriMapNode));


INT4
QoSDeletePriMapUniTblEntry PROTO ((tQoSInPriorityMapNode *pPriMapNode));

INT4
QoSUtlIsUniqueName PROTO ((tSNMP_OCTET_STRING_TYPE
                           *pTestValFsQoSPriorityMapName, UINT4 u4TblType));

tQoSInPriorityMapNode *
QoSUtlGetPriorityMapUniNode PROTO ((UINT4 u4IfIndex, UINT2 u2VlanId,
                                    UINT1 u1InPri, UINT1 u1InPriType));

INT4
QoSGetNextQMapTblEntryIndex PROTO ((INT4  i4RegenPriType, INT4 *pi4NextRGPType,
                                    UINT4 u4RegenPri, UINT4 *pu4NextRegenPri));

INT4
QoSValidateQMapTblEntry PROTO ((tQoSQMapNode *pQMapNode));

INT4
QoSUtlValidateQMapTblIdxInst PROTO ((INT4 i4RegenPriType, UINT4 u4RegenPri,
                                     UINT4 *pu4ErrorCode));

INT4 QoSIsDefPriorityMapTblEntry PROTO ((tQoSInPriorityMapNode *pPriMapNode));

tQoSQMapNode *
QoSUtlGetQMapNode PROTO ((INT4 i4RegenPriType, UINT4 u4RegenPri));

INT4
QoSAddDefPriorityMapTblEntries PROTO ((VOID));
    
INT4
QoSAddDefPortConfigTblEntries PROTO ((VOID));

tQoSQMapNode *
QoSCreateQMapTblEntry PROTO ((INT4 i4RegenPriType, UINT4 u4RegenPri));

INT4
QoSInPriMapUniCmpFun PROTO ((tRBElem * e1, tRBElem * e2));

tQoSInPriorityMapNode *
QoSUtlGetPriorityMapUniNode PROTO ((UINT4 u4IfIndex, UINT2 u2VlanId,
                                    UINT1 u1InPri, UINT1 u1InPriType));

INT4
QoSUtlValidatePriMapTblVlanId PROTO ((UINT2 u2VlanId, UINT4 *pu4ErrorCode));

INT4
QoSUtlValidatePriMapTblMacAddress PROTO((UINT4,tMacAddr MacAddr,
                                   UINT4 *pu4ErrorCode));

INT4 QoSIsDefQMapTblEntry PROTO ((tQoSQMapNode *pQMapNode));
INT4 QoSAddDefQMapTblEntries PROTO ((VOID));

INT4
RbWalkAction PROTO ((tRBElem * e, eRBVisit visit, UINT4 level,
                     VOID *arg, VOID *out));

INT4
QoSDeleteQMapTblEntry PROTO ((tQoSQMapNode *pQMapNode));

INT4
QoSConfigQMap PROTO ((UINT4 u4CfgType));
    
INT4 
QoSConfigPriConfigTbl PROTO ((VOID));

INT4 
QoSConfigPortConfigTbl PROTO ((VOID));



/*****************************************************************************/
/* FileName : qosutl.c  functions prototype                                  */
/*****************************************************************************/
/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/
INT4 QoSUtlValidateTableName PROTO ((tSNMP_OCTET_STRING_TYPE *pSNMPOctSrt, 
                                     UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateVlanId PROTO ((UINT4 u4VlanId, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateClass PROTO ((UINT4 u4ClassId, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidatePriMapTblIdxInst PROTO ((UINT4 u4FsQoSPriorityMapID,
                                            UINT4 *pu4ErrorCode));

INT4
QoSUtlIsDefQMapTblEntry PROTO ((tQoSQMapNode  *pQMapNode));

/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/
INT4 QoSInPriMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
tQoSInPriorityMapNode * QoSUtlGetPriorityMapNode PROTO ((UINT4 u4PriMapId));
tQoSInPriorityMapNode * QoSCreatePriMapTblEntry PROTO ((UINT4 u4PriMapId)); 
INT4 QoSValidatePriMapTblEntry PROTO ((tQoSInPriorityMapNode *pInPriMapNode));
INT4 QoSDeletePriMapTblEntry PROTO ((tQoSInPriorityMapNode *pInPriMapNode));
INT4 QoSGetNextInPriMapTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
                                             UINT4 *pu4NextIndex));
INT4
QoSUtlValidatePriorityMapId PROTO ((UINT4 u4PriMapId, UINT4 *pu4ErrorCode));
INT4
QoSInPriTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4
QoSInPriMapUniCmpFun PROTO ((tRBElem * e1, tRBElem * e2));


/*****************************************************************************/
/*                     Q MAP TABLE FUNCTIONS                                 */
/*****************************************************************************/
INT4 QoSQMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSQMapTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));


/*****************************************************************************/
/* FileName : qosnpwr.c  functions prototype                                 */
/*****************************************************************************/
INT4
QoSHwWrInit PROTO ((VOID));

INT4
QoSHwWrDeInit PROTO ((VOID));

INT4
QoSHwWrMapClassToQueue PROTO ((INT4 i4RegenPriType, UINT4 u4Priority, UINT4 u4QId, UINT1 u1Flag));

INT4
QoSHwWrSetSchedulingAlgo PROTO ((UINT4 u4SchedulingAlgo));

INT4
QoSHwWrAddPriMapEntry PROTO ((tQoSInPriorityMapNode * pPriMapNode));

INT4
QoSHwWrDelPriorityEntry PROTO ((tQoSInPriorityMapNode * pPriMapNode));

INT4
QosHwWrSetVlanPriStatus PROTO((UINT4 u4FsQoSIfIndex , INT4 i4SetValFsQoSVlanPriStatus));

INT4
QosHwWrSetIPDscpStatus PROTO((UINT4 u4FsQoSIfIndex , INT4 i4SetValFsQoSIPDscpStatus));

INT4
QosHwWrSetVlanIPStatus PROTO ((UINT4 u4FsQoSIfIndex , INT4 i4SetValFsQoSVlanIPStatus));

INT4
QosHwWrSetSAOverrideStatus PROTO ((UINT4 u4FsQoSIfIndex , INT4 i4SetValFsQoSSAOverride));

INT4
QosHwWrSetDAOverrideStatus PROTO ((UINT4 u4FsQoSIfIndex , INT4 i4SetValFsQoSDAOverride));

INT4
QosHwWrSetVIDOverrideStatus PROTO ((UINT4 u4FsQoSIfIndex , INT4 i4SetValFsQoSVIDOverride));
    
#ifdef NPAPI_WANTED

#endif /* NPAPI_WANTED */

#endif /* __QOS_PROTO_H__ */

/*****************************************************************************/
/* FileName : qosutl.c  functions prototype                                  */
/*****************************************************************************/
/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/
/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/
/*****************************************************************************/
/*                     CLASS MAP TABLE FUNCTIONS                             */
/*****************************************************************************/
/*****************************************************************************/
/*               CLASS 2 PRIORITY MAP TABLE FUNCTIONS                        */
/*****************************************************************************/
/*****************************************************************************/
/*                        METER TABLE FUNCTIONS                              */
/*****************************************************************************/
/*****************************************************************************/
/*                     POLICY MAP TABLE FUNCTIONS                            */
/*****************************************************************************/
/*****************************************************************************/
/*                     CLASS INFO TABLE FUNCTIONS                            */
/*****************************************************************************/
