/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosglob.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-GLOB                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the Global Variables for      */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_GLOB_H__
#define __QOS_GLOB_H__ 

#ifdef __QOS_SYS_C__
tQoSPortConfigTable gQoSPortConfigTable[SYS_DEF_MAX_PHYSICAL_INTERFACES + 1];
tQoSGlobalInfo gQoSGlobalInfo;
#else
PUBLIC tQoSPortConfigTable gQoSPortConfigTable[SYS_DEF_MAX_PHYSICAL_INTERFACES + 1];
PUBLIC tQoSGlobalInfo gQoSGlobalInfo;
#endif /* __QOS_SYS_C__ */

#endif /* __QOS_GLOB_H__ */





