/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: qosdefn.h,v 1.5 2013/04/15 10:57:04 siva Exp $
 *
 * Description : This function contains all the constants and macros for the
 *               Aricent QOS Module.
 *****************************************************************************/

/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosdefn.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-DEFS                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the constants and macros for  */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_DEFS_H__
#define __QOS_DEFS_H__ 

#ifndef __FUNCTION__
#define __FUNCTION__ "FunName"
#endif

#define QOS_PM_TBL_DEF_ENTRY_MAX               7

#define QOS_MEM_POOL_SIZE_FACTOR  (1)


/*  1. Priority Map Table  */
#define QOS_PRI_MAP_TBL_INVLD_INREGPRI        8


#define QOS_QMAP_ADD                           1
#define QOS_QMAP_DEL                           2


#define DEFAULT_MAX_ARRAYSIZE_PRI              8
#define DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE   8

#define QOS_PM_TBL_DEF_ENTRY_TYPE              QOS_IN_PRI_TYPE_VLAN_PRI

/* Common Macros and Constants */
#define QOS_FILTER_INFO_MAX_ENTRIES    QOS_CLS_MAP_TBL_MAX_ENTRIES
#define QOS_MAX_TABLE_NAME             32
#define QOS_SEM_NAME                   ("QSMS")
#define QOS_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)
#define QOS_MODULE_SHUTDOW_ERROR_MSG \
                           " QoS Should Be Started Before Accessing It !.\r\n"
#define QOS_FOREVER()                  while(1) 
#define QOS_CFG_TYPE_CREATE            1
#define QOS_CFG_TYPE_DELETE            2
#define QOS_SEND_EVENT                 OsixEvtSend
#define QOS_RECV_EVENT                 OsixEvtRecv
#define QOS_TASK_ID                    gQoSGlobalInfo.QoSTaskId
#define QOS_GET_TASK_ID                OsixTskIdSelf
#define QOS_EVENT_WAIT_FLAGS           OSIX_WAIT                           
/* Trace Level Macros */
#define QOS_TRC_MIN_LEVEL              0
#define QOS_TRC_MAX_LEVEL              0x000000FF

/* Range Checks for Table Entries */
#define QOS_PRI_MAP_TBL_MAX_INDEX_RANGE          65535     
#define QOS_Q_MAP_TBL_MAX_INDEX_RANGE            65535
                           
#define QOS_CHECK_TABLE_REF_COUNT(pTableEntry) \
        (pTableEntry->u4RefCount != 0) ? QOS_FAILURE : QOS_SUCCESS

/* Should be Null Teriminated pu1Str */
#define QOS_UTL_COPY_TABLE_NAME(pSNMPOctSrt,pu1Str) \
      pSNMPOctSrt->i4_Length = STRLEN (pu1Str); \
      STRNCPY (pSNMPOctSrt->pu1_OctetList, pu1Str,(pSNMPOctSrt->i4_Length))

#define QOS_UTL_SET_TABLE_NAME(pu1Str,pSNMPOctSrt) \
    MEMSET (pu1Str, 0, QOS_MAX_TABLE_NAME); \
    STRNCPY (pu1Str,pSNMPOctSrt->pu1_OctetList,(pSNMPOctSrt->i4_Length))

#define QOS_QMAP_MIN_VALUE 0
#define QOS_QMAP_MAX_VALUE 3

#define QOS_STRICT_PRIORITY 0
#define QOS_WEIGHTED_ROUND_ROBIN 1

#define QOS_ASSIGN_TRAFFIC_CLASS_0    15
#define QOS_ASSIGN_TRAFFIC_CLASS_1    31
#define QOS_ASSIGN_TRAFFIC_CLASS_2    47
#define QOS_ASSIGN_TRAFFIC_CLASS_3    63

                           
                           
/*  1. Priority Map Table  */
#define QOS_IN_PRI_TYPE_MIN_VAL               0
#define QOS_IN_PRI_TYPE_MAX_VAL               8


#define QOS_IN_PRIORITY_MIN_VAL               0
#define QOS_IN_PRIORITY_MAX_VAL              63
#define QOS_REGEN_PRIORITY_MIN_VAL            0
#define QOS_REGEN_PRIORITY_MAX_VAL           63
/* Vlan Pri (3) + Drop eligible BIT (1) */
#define QOS_IN_PRIORITY_VLAN_PRI_MIN          0
#define QOS_IN_PRIORITY_VLAN_PRI_MAX          7

    /* IP DSCP BIT(6) */                           
#define QOS_IN_PRIORITY_IP_DSCP_MIN            0
#define QOS_IN_PRIORITY_IP_DSCP_MAX           63
                                                
#define QOS_PRI_MAP_TBL_CONF_STATUS_SYS        1
#define QOS_PRI_MAP_TBL_CONF_STATUS_MGNT       2

/*    1. Priority Map Table Index  - Priority Map Id */
#define QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE(PMId) \
          ((PMId > 0) && (PMId <= QOS_PRI_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

    
/*    5. Policy Table  */
#define QOS_POLICY_ACTION_FLAG_LENGTH         1

#define QOS_POLICY_PHB_TYPE_MIN_VAL           0
#define QOS_POLICY_PHB_TYPE_MAX_VAL           4

#define QOS_POLICY_PHB_DEFAULT_MIN_VAL        0
#define QOS_POLICY_PHB_DEFAULT_MAX_VAL        63

/* Min Max Vlaues */
#define QOS_DSCP_MIN_VAL                      0
#define QOS_DSCP_MAX_VAL                     63

#define QOS_PRI_MIN_VAL                       0
#define QOS_PRI_MAX_VAL                       7


/* 11. QMap Table */
    
#define QOS_QMAP_PRI_VLAN_PRI_MIN        0
#define QOS_QMAP_PRI_VLAN_PRI_MAX        15
#define QOS_QMAP_PRI_IP_DSCP_MIN         0
#define QOS_QMAP_PRI_IP_DSCP_MAX         63 

#define QOS_CHECK_Q_MAP_TBL_INDEX_RANGE(QMapId) \
          ((QMapId> 0) && (QMapId <= QOS_Q_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

#endif /* __QOS_DEFS_H__ */

#define QOS_TBL_TYPE_PRI_MAP       1
                
#define QOS_QUEUE_SIZE             50
#define QOS_MSG_QUEUE_ID           gQosPktQId
#define QOS_DEF_MSG_LEN            OSIX_DEF_MSG_LEN
#define QOS_SEND_TO_QUEUE          OsixQueSend
#define QOS_SEND_EVENT             OsixEvtSend


#define QOS_CREATE_MEM_POOL        MemCreateMemPool 
#define QOS_QMSG_ALLOC_MEMBLK      MemAllocMemBlk(gQoSGlobalInfo.QosQPktMemPoolId)
#define QOS_RELEASE_QMSG_MEM_BLOCK(pPkt) \
         MemReleaseMemBlock(gQoSGlobalInfo.QosQPktMemPoolId, (UINT1 *) pPkt)

#define QOS_ENABLE                 1
#define QOS_DISABLE                0
    
#define QOS_DISABLE_PORT_MSG       1
#define QOS_ENABLE_PORT_MSG        2
#define QOS_DELETE_PORT_MSG        3 
#define QOS_CREATE_PORT_MSG        4

#define QOS_MSG_EVENT       0x01

#define QOS_PRI_MAP_TBL_MAX_ENTRIES  100
#define QOS_Q_MAP_TBL_MAX_ENTRIES    100

#define QOS_SUCCESS   1
#define QOS_FAILURE   0

 
