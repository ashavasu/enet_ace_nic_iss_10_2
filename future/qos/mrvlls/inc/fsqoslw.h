/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsqoslw.h,v 1.2 2009/03/09 07:00:32 nswamy-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsQoSStatus ARG_LIST((INT4 *));

INT1
nmhGetFsQoSTrcFlag ARG_LIST((UINT4 *));

INT1
nmhGetFsQosSchedulingAlgorithm ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsQoSStatus ARG_LIST((INT4 ));

INT1
nmhSetFsQoSTrcFlag ARG_LIST((UINT4 ));

INT1
nmhSetFsQosSchedulingAlgorithm ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsQoSStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsQoSTrcFlag ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsQosSchedulingAlgorithm ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsQoSStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsQoSTrcFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsQosSchedulingAlgorithm ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSPriorityMapTable. */
INT1
nmhValidateIndexInstanceFsQoSPriorityMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSPriorityMapTable  */

INT1
nmhGetFirstIndexFsQoSPriorityMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSPriorityMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSPriorityMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSPriorityMapIfIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapVlanId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapInPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapInPriType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapRegenPriority ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapMacAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetFsQoSPriorityMapConfigStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSPriorityMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSPriorityMapIfIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapVlanId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapInPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPriorityMapInPriType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPriorityMapRegenPriority ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapMacAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetFsQoSPriorityMapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSPriorityMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSPriorityMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapVlanId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapInPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPriorityMapInPriType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPriorityMapRegenPriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapMacAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2FsQoSPriorityMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSPriorityMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSQMapTable. */
INT1
nmhValidateIndexInstanceFsQoSQMapTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSQMapTable  */

INT1
nmhGetFirstIndexFsQoSQMapTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSQMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSQMapQId ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSQMapQId ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSQMapQId ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSQMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQosPortConfigTable. */
INT1
nmhValidateIndexInstanceFsQosPortConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQosPortConfigTable  */

INT1
nmhGetFirstIndexFsQosPortConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQosPortConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSVlanPriPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSIPDscpPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSVlanIPPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSVIDOverride ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSSAOverride ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSDAOverride ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSVlanPriPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSIPDscpPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSVlanIPPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSVIDOverride ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSSAOverride ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSDAOverride ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSVlanPriPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSIPDscpPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSVlanIPPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSVIDOverride ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSSAOverride ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSDAOverride ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQosPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
