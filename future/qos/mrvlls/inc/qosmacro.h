#ifndef _QOSMACRO_H
#define _QOSMACRO_H


#define QOS_MIN_DSCP_VALUE       0
#define QOS_MAX_DSCP_VALUE       63

#define QOS_MIN_TRAFFIC_CLASS    0
#define QOS_MAX_TRAFFIC_CLASS    3

#define QOS_ASSIGN_TRAFFIC_CLASS_0    15
#define QOS_ASSIGN_TRAFFIC_CLASS_1    31 
#define QOS_ASSIGN_TRAFFIC_CLASS_2    47
#define QOS_ASSIGN_TRAFFIC_CLASS_3    63 

#define QOS_MIN_PRIORITY_VALUE   0
#define QOS_MAX_PRIORITY_VALUE   7
#define QOS_IFSTATUS_INVALID 0
#define QOS_IFSTATUS_VALID    1

#define QOS_ENABLE               1
#define QOS_DISABLE              2


#define QOS_MUT_EXCL_SEM_NAME        (const UINT1 *) "QoSSEM"
#define QOS_MUT_EXCL_SEM_ID          gSemId

#endif
