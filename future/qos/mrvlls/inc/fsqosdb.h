/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsqosdb.h,v 1.2 2009/03/09 07:00:32 nswamy-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSQOSDB_H
#define _FSQOSDB_H

UINT1 FsQoSPriorityMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSQMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQosPortConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsqos [] ={1,3,6,1,4,1,2076,83};
tSNMP_OID_TYPE fsqosOID = {8, fsqos};


UINT4 FsQoSSystemControl [ ] ={1,3,6,1,4,1,2076,83,1,1,1};
UINT4 FsQoSStatus [ ] ={1,3,6,1,4,1,2076,83,1,1,2};
UINT4 FsQoSTrcFlag [ ] ={1,3,6,1,4,1,2076,83,1,1,3};
UINT4 FsQosSchedulingAlgorithm [ ] ={1,3,6,1,4,1,2076,83,1,1,4};
UINT4 FsQoSPriorityMapID [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,1};
UINT4 FsQoSPriorityMapName [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,2};
UINT4 FsQoSPriorityMapIfIndex [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,3};
UINT4 FsQoSPriorityMapVlanId [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,4};
UINT4 FsQoSPriorityMapInPriority [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,5};
UINT4 FsQoSPriorityMapInPriType [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,6};
UINT4 FsQoSPriorityMapRegenPriority [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,7};
UINT4 FsQoSPriorityMapMacAddress [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,8};
UINT4 FsQoSPriorityMapConfigStatus [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,9};
UINT4 FsQoSPriorityMapStatus [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,10};
UINT4 FsQoSQMapRegenPriType [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,1};
UINT4 FsQoSQMapRegenPriority [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,2};
UINT4 FsQoSQMapQId [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,3};
UINT4 FsQoSIfIndex [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,1};
UINT4 FsQoSVlanPriPriority [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,2};
UINT4 FsQoSIPDscpPriority [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,3};
UINT4 FsQoSVlanIPPriority [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,4};
UINT4 FsQoSVIDOverride [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,5};
UINT4 FsQoSSAOverride [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,6};
UINT4 FsQoSDAOverride [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,7};


tMbDbEntry fsqosMibEntry[]= {

{{11,FsQoSSystemControl}, NULL, FsQoSSystemControlGet, FsQoSSystemControlSet, FsQoSSystemControlTest, FsQoSSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsQoSStatus}, NULL, FsQoSStatusGet, FsQoSStatusSet, FsQoSStatusTest, FsQoSStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsQoSTrcFlag}, NULL, FsQoSTrcFlagGet, FsQoSTrcFlagSet, FsQoSTrcFlagTest, FsQoSTrcFlagDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsQosSchedulingAlgorithm}, NULL, FsQosSchedulingAlgorithmGet, FsQosSchedulingAlgorithmSet, FsQosSchedulingAlgorithmTest, FsQosSchedulingAlgorithmDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsQoSPriorityMapID}, GetNextIndexFsQoSPriorityMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSPriorityMapTableINDEX, 1, 0, 0, NULL},

{{13,FsQoSPriorityMapName}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapNameGet, FsQoSPriorityMapNameSet, FsQoSPriorityMapNameTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, NULL},

{{13,FsQoSPriorityMapIfIndex}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapIfIndexGet, FsQoSPriorityMapIfIndexSet, FsQoSPriorityMapIfIndexTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{13,FsQoSPriorityMapVlanId}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapVlanIdGet, FsQoSPriorityMapVlanIdSet, FsQoSPriorityMapVlanIdTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{13,FsQoSPriorityMapInPriority}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapInPriorityGet, FsQoSPriorityMapInPrioritySet, FsQoSPriorityMapInPriorityTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{13,FsQoSPriorityMapInPriType}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapInPriTypeGet, FsQoSPriorityMapInPriTypeSet, FsQoSPriorityMapInPriTypeTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{13,FsQoSPriorityMapRegenPriority}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapRegenPriorityGet, FsQoSPriorityMapRegenPrioritySet, FsQoSPriorityMapRegenPriorityTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{13,FsQoSPriorityMapMacAddress}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapMacAddressGet, FsQoSPriorityMapMacAddressSet, FsQoSPriorityMapMacAddressTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, NULL},

{{13,FsQoSPriorityMapConfigStatus}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapConfigStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsQoSPriorityMapTableINDEX, 1, 0, 0, "1"},

{{13,FsQoSPriorityMapStatus}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapStatusGet, FsQoSPriorityMapStatusSet, FsQoSPriorityMapStatusTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 1, NULL},

{{13,FsQoSQMapRegenPriType}, GetNextIndexFsQoSQMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsQoSQMapTableINDEX, 2, 0, 0, NULL},

{{13,FsQoSQMapRegenPriority}, GetNextIndexFsQoSQMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSQMapTableINDEX, 2, 0, 0, NULL},

{{13,FsQoSQMapQId}, GetNextIndexFsQoSQMapTable, FsQoSQMapQIdGet, FsQoSQMapQIdSet, FsQoSQMapQIdTest, FsQoSQMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQMapTableINDEX, 2, 0, 0, NULL},

{{13,FsQoSIfIndex}, GetNextIndexFsQosPortConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQosPortConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsQoSVlanPriPriority}, GetNextIndexFsQosPortConfigTable, FsQoSVlanPriPriorityGet, FsQoSVlanPriPrioritySet, FsQoSVlanPriPriorityTest, FsQosPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosPortConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsQoSIPDscpPriority}, GetNextIndexFsQosPortConfigTable, FsQoSIPDscpPriorityGet, FsQoSIPDscpPrioritySet, FsQoSIPDscpPriorityTest, FsQosPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosPortConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsQoSVlanIPPriority}, GetNextIndexFsQosPortConfigTable, FsQoSVlanIPPriorityGet, FsQoSVlanIPPrioritySet, FsQoSVlanIPPriorityTest, FsQosPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosPortConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsQoSVIDOverride}, GetNextIndexFsQosPortConfigTable, FsQoSVIDOverrideGet, FsQoSVIDOverrideSet, FsQoSVIDOverrideTest, FsQosPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosPortConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsQoSSAOverride}, GetNextIndexFsQosPortConfigTable, FsQoSSAOverrideGet, FsQoSSAOverrideSet, FsQoSSAOverrideTest, FsQosPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosPortConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsQoSDAOverride}, GetNextIndexFsQosPortConfigTable, FsQoSDAOverrideGet, FsQoSDAOverrideSet, FsQoSDAOverrideTest, FsQosPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosPortConfigTableINDEX, 1, 0, 0, "1"},
};
tMibData fsqosEntry = { 24, fsqosMibEntry };
#endif /* _FSQOSDB_H */

