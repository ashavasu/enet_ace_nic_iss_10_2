/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/* $Id: qosinc.h,v 1.4 2012/03/30 13:18:15 siva Exp $                      */
/*                                                                          */
/*  FILE NAME             : qosinc.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-INC                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File is the superset of all header file    */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_INC_H__
#define __QOS_INC_H__

/* Globale Header */
#include "lr.h"
#include "fssnmp.h" 
#include "snmputil.h" 
#include "trace.h"
#include "iss.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#endif
#include "qos.h"
#include "diffsrv.h"
/* Module Header */
#include "fsqoslw.h"
#include "fsqoswr.h"
#include "qosdefn.h"
#include "qostdfs.h"
#include "qosproto.h"
#include "qosglob.h"
#include "qostrc.h"
#include "qosmrvlscli.h"


#endif /* __QOS_INC_H__ */
