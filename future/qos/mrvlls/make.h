#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30/01/2008                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME		= FutureQOS
PROJECT_BASE_DIR	= ${BASE_DIR}/qos/mrvlls
PROJECT_SOURCE_DIR	= ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR	= ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR	= ${PROJECT_BASE_DIR}/obj
FUTURE_INC_DIR		= $(BASE_DIR)/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = \
			 $(PROJECT_INCLUDE_DIR)/fsqosdb.h   \
			 $(PROJECT_INCLUDE_DIR)/fsqoslw.h   \
			 $(PROJECT_INCLUDE_DIR)/fsqoswr.h   \
			 $(PROJECT_INCLUDE_DIR)/qosdefn.h    \
			 $(PROJECT_INCLUDE_DIR)/qosglob.h    \
			 $(PROJECT_INCLUDE_DIR)/qosinc.h     \
			 $(PROJECT_INCLUDE_DIR)/qosproto.h   \
			 $(PROJECT_INCLUDE_DIR)/qostdfs.h    \
			 $(PROJECT_INCLUDE_DIR)/qostrc.h


PROJECT_FINAL_INCLUDES_DIRS	=  -I$(PROJECT_INCLUDE_DIR) \
				 $(COMMON_INCLUDE_DIRS)\
				-I$(FUTURE_INC_DIR)

PROJECT_FINAL_INCLUDE_FILES    +=  $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES	=    $(COMMON_DEPENDENCIES)\
            $(PROJECT_FINAL_INCLUDE_FILES) \
				$(PROJECT_BASE_DIR)/Makefile \
				$(PROJECT_BASE_DIR)/make.h

