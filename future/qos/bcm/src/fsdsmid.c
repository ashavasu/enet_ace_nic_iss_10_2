# include  "include.h"
# include  "fsdsmid.h"
# include  "fsdslow.h"
# include  "fsdscon.h"
# include  "fsdsogi.h"
# include  "extern.h"
# include  "midconst.h"

/****************************************************************************
 Function   : fsDiffServSystemGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServSystemGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fsDiffServSystem_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_fsDiffServSystem_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fsDiffServSystem_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_fsDiffServSystem_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fsDiffServSystem_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSDSSYSTEMCONTROL:
        {
            i1_ret_val = nmhGetFsDsSystemControl (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDSSTATUS:
        {
            i1_ret_val = nmhGetFsDsStatus (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServSystemSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServSystemSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDSSYSTEMCONTROL:
        {
            i1_ret_val = nmhSetFsDsSystemControl (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDSSTATUS:
        {
            i1_ret_val = nmhSetFsDsStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServSystemTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServSystemTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case FSDSSYSTEMCONTROL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDsSystemControl (&u4ErrorCode,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDSSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDsStatus (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServDataPathEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServDataPathEntryGet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServDataPathTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServDataPathId = FALSE;
    INT4                i4_next_fsDiffServDataPathId = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsDiffServPorts = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServDataPathTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServDataPathTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServDataPathId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServDataPathTable
                     (i4_fsDiffServDataPathId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServDataPathId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServDataPathTable
                     (&i4_fsDiffServDataPathId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServDataPathId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServDataPathId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServDataPathTable
                     (i4_fsDiffServDataPathId,
                      &i4_next_fsDiffServDataPathId)) == SNMP_SUCCESS)
                {
                    i4_fsDiffServDataPathId = i4_next_fsDiffServDataPathId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServDataPathId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVDATAPATHID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServDataPathId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServDataPathId;
            }
            break;
        }
        case FSDIFFSERVPORTS:
        {
            poctet_retval_fsDiffServPorts =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_fsDiffServPorts == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsDiffServPorts (i4_fsDiffServDataPathId,
                                       poctet_retval_fsDiffServPorts);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsDiffServPorts;
            }
            else
            {
                free_octetstring (poctet_retval_fsDiffServPorts);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVDATAPATHIFDIRECTION:
        {
            i1_ret_val =
                nmhGetFsDiffServDataPathIfDirection (i4_fsDiffServDataPathId,
                                                     &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVDATAPATHSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServDataPathStatus (i4_fsDiffServDataPathId,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServDataPathEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServDataPathEntrySet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServDataPathId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServDataPathId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVPORTS:
        {
            i1_ret_val =
                nmhSetFsDiffServPorts (i4_fsDiffServDataPathId,
                                       p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVDATAPATHIFDIRECTION:
        {
            i1_ret_val =
                nmhSetFsDiffServDataPathIfDirection (i4_fsDiffServDataPathId,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVDATAPATHSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServDataPathStatus (i4_fsDiffServDataPathId,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVDATAPATHID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServDataPathEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServDataPathEntryTest (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServDataPathId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServDataPathId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServDataPathTable(i4_fsDiffServDataPathId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVPORTS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServPorts (&u4ErrorCode, i4_fsDiffServDataPathId,
                                          p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVDATAPATHIFDIRECTION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServDataPathIfDirection (&u4ErrorCode,
                                                        i4_fsDiffServDataPathId,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVDATAPATHSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServDataPathStatus (&u4ErrorCode,
                                                   i4_fsDiffServDataPathId,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVDATAPATHID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServMultiFieldClfrEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServMultiFieldClfrEntryGet (tSNMP_OID_TYPE * p_in_db,
                                  tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                  UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServMultiFieldClfrTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServMultiFieldClfrId = FALSE;
    INT4                i4_next_fsDiffServMultiFieldClfrId = FALSE;

    tMacAddr            mac_addr_ret_val_fsDiffServMultiFieldClfrSrcMacAddr;
    tMacAddr            mac_addr_ret_val_fsDiffServMultiFieldClfrDstMacAddr;
    UINT4               u4_addr_ret_val_fsDiffServMultiFieldClfrSrcIpAddr;
    UINT4               u4_addr_ret_val_fsDiffServMultiFieldClfrDstIpAddr;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServMultiFieldClfrTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServMultiFieldClfrTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServMultiFieldClfrId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable
                     (i4_fsDiffServMultiFieldClfrId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServMultiFieldClfrId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServMultiFieldClfrTable
                     (&i4_fsDiffServMultiFieldClfrId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServMultiFieldClfrId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServMultiFieldClfrId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServMultiFieldClfrTable
                     (i4_fsDiffServMultiFieldClfrId,
                      &i4_next_fsDiffServMultiFieldClfrId)) == SNMP_SUCCESS)
                {
                    i4_fsDiffServMultiFieldClfrId =
                        i4_next_fsDiffServMultiFieldClfrId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServMultiFieldClfrId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVMULTIFIELDCLFRID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServMultiFieldClfrId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServMultiFieldClfrId;
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRFLAG:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrFlag
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRFILTERFORMAT:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrFilterFormat
                (i4_fsDiffServMultiFieldClfrId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCMACADDR:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcMacAddr
                (i4_fsDiffServMultiFieldClfrId,
                 &mac_addr_ret_val_fsDiffServMultiFieldClfrSrcMacAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString
                    (mac_addr_ret_val_fsDiffServMultiFieldClfrSrcMacAddr,
                     MAC_ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTMACADDR:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstMacAddr
                (i4_fsDiffServMultiFieldClfrId,
                 &mac_addr_ret_val_fsDiffServMultiFieldClfrDstMacAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString
                    (mac_addr_ret_val_fsDiffServMultiFieldClfrDstMacAddr,
                     MAC_ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRVLANID:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrVlanId
                (i4_fsDiffServMultiFieldClfrId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRETHERTYPE:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrEtherType
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRIPVERSION:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrIpVersion
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRIPHLEN:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrIpHLen
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSCP:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDscp
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRPROTOCOL:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrProtocol
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCIPADDR:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcIpAddr
                (i4_fsDiffServMultiFieldClfrId,
                 &u4_addr_ret_val_fsDiffServMultiFieldClfrSrcIpAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL
                    (u4_addr_ret_val_fsDiffServMultiFieldClfrSrcIpAddr);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCIPPRELEN:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcIpPreLen
                (i4_fsDiffServMultiFieldClfrId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTIPADDR:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstIpAddr
                (i4_fsDiffServMultiFieldClfrId,
                 &u4_addr_ret_val_fsDiffServMultiFieldClfrDstIpAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL
                    (u4_addr_ret_val_fsDiffServMultiFieldClfrDstIpAddr);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTIPPRELEN:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstIpPreLen
                (i4_fsDiffServMultiFieldClfrId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMIN:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcPortMin
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMAX:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcPortMax
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMIN:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstPortMin
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMAX:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstPortMax
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORT:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcPort
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMASK:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrSrcPortMask
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORT:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstPort
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMASK:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrDstPortMask
                (i4_fsDiffServMultiFieldClfrId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRPRECEDENCE:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrPrecedence
                (i4_fsDiffServMultiFieldClfrId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServMultiFieldClfrStatus
                (i4_fsDiffServMultiFieldClfrId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServMultiFieldClfrEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServMultiFieldClfrEntrySet (tSNMP_OID_TYPE * p_in_db,
                                  tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                  tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_fsDiffServMultiFieldClfrId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    tMacAddr            mac_addr_val_fsDiffServMultiFieldClfrSrcMacAddr;
    tMacAddr            mac_addr_val_fsDiffServMultiFieldClfrDstMacAddr;
    UINT4               u4_addr_val_fsDiffServMultiFieldClfrSrcIpAddr;
    UINT4               u4_addr_val_fsDiffServMultiFieldClfrDstIpAddr;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServMultiFieldClfrId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVMULTIFIELDCLFRFLAG:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrFlag
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRFILTERFORMAT:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrFilterFormat
                (i4_fsDiffServMultiFieldClfrId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCMACADDR:
        {
            for (i4_offset = FALSE; i4_offset < MAC_ADDR_LEN; i4_offset++)
                mac_addr_val_fsDiffServMultiFieldClfrSrcMacAddr[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcMacAddr
                (i4_fsDiffServMultiFieldClfrId,
                 mac_addr_val_fsDiffServMultiFieldClfrSrcMacAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTMACADDR:
        {
            for (i4_offset = FALSE; i4_offset < MAC_ADDR_LEN; i4_offset++)
                mac_addr_val_fsDiffServMultiFieldClfrDstMacAddr[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstMacAddr
                (i4_fsDiffServMultiFieldClfrId,
                 mac_addr_val_fsDiffServMultiFieldClfrDstMacAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRVLANID:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrVlanId
                (i4_fsDiffServMultiFieldClfrId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRETHERTYPE:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrEtherType
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRIPVERSION:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrIpVersion
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRIPHLEN:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrIpHLen
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSCP:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDscp
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRPROTOCOL:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrProtocol
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCIPADDR:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_fsDiffServMultiFieldClfrSrcIpAddr =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcIpAddr
                (i4_fsDiffServMultiFieldClfrId,
                 u4_addr_val_fsDiffServMultiFieldClfrSrcIpAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCIPPRELEN:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcIpPreLen
                (i4_fsDiffServMultiFieldClfrId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTIPADDR:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_fsDiffServMultiFieldClfrDstIpAddr =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstIpAddr
                (i4_fsDiffServMultiFieldClfrId,
                 u4_addr_val_fsDiffServMultiFieldClfrDstIpAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTIPPRELEN:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstIpPreLen
                (i4_fsDiffServMultiFieldClfrId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMIN:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcPortMin
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMAX:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcPortMax
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMIN:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstPortMin
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMAX:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstPortMax
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORT:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcPort
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMASK:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrSrcPortMask
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORT:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstPort
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMASK:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrDstPortMask
                (i4_fsDiffServMultiFieldClfrId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRPRECEDENCE:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrPrecedence
                (i4_fsDiffServMultiFieldClfrId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMULTIFIELDCLFRSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServMultiFieldClfrStatus
                (i4_fsDiffServMultiFieldClfrId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVMULTIFIELDCLFRID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServMultiFieldClfrEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServMultiFieldClfrEntryTest (tSNMP_OID_TYPE * p_in_db,
                                   tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                   tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_fsDiffServMultiFieldClfrId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    tMacAddr            mac_addr_val_fsDiffServMultiFieldClfrSrcMacAddr;
    tMacAddr            mac_addr_val_fsDiffServMultiFieldClfrDstMacAddr;
    UINT4               u4_addr_val_fsDiffServMultiFieldClfrSrcIpAddr;
    UINT4               u4_addr_val_fsDiffServMultiFieldClfrDstIpAddr;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServMultiFieldClfrId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable(i4_fsDiffServMultiFieldClfrId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVMULTIFIELDCLFRFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrFlag (&u4ErrorCode,
                                                       i4_fsDiffServMultiFieldClfrId,
                                                       p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRFILTERFORMAT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrFilterFormat (&u4ErrorCode,
                                                               i4_fsDiffServMultiFieldClfrId,
                                                               p_value->
                                                               i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCMACADDR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < MAC_ADDR_LEN; i4_offset++)
                mac_addr_val_fsDiffServMultiFieldClfrSrcMacAddr[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcMacAddr (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             mac_addr_val_fsDiffServMultiFieldClfrSrcMacAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTMACADDR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < MAC_ADDR_LEN; i4_offset++)
                mac_addr_val_fsDiffServMultiFieldClfrDstMacAddr[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstMacAddr (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             mac_addr_val_fsDiffServMultiFieldClfrDstMacAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRVLANID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrVlanId (&u4ErrorCode,
                                                         i4_fsDiffServMultiFieldClfrId,
                                                         p_value->
                                                         i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRETHERTYPE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrEtherType (&u4ErrorCode,
                                                            i4_fsDiffServMultiFieldClfrId,
                                                            p_value->
                                                            u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRIPVERSION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrIpVersion (&u4ErrorCode,
                                                            i4_fsDiffServMultiFieldClfrId,
                                                            p_value->
                                                            u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRIPHLEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrIpHLen (&u4ErrorCode,
                                                         i4_fsDiffServMultiFieldClfrId,
                                                         p_value->
                                                         u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSCP:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDscp (&u4ErrorCode,
                                                       i4_fsDiffServMultiFieldClfrId,
                                                       p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRPROTOCOL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrProtocol (&u4ErrorCode,
                                                           i4_fsDiffServMultiFieldClfrId,
                                                           p_value->
                                                           u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCIPADDR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_fsDiffServMultiFieldClfrSrcIpAddr =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcIpAddr (&u4ErrorCode,
                                                            i4_fsDiffServMultiFieldClfrId,
                                                            u4_addr_val_fsDiffServMultiFieldClfrSrcIpAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCIPPRELEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcIpPreLen (&u4ErrorCode,
                                                              i4_fsDiffServMultiFieldClfrId,
                                                              p_value->
                                                              i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTIPADDR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_fsDiffServMultiFieldClfrDstIpAddr =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstIpAddr (&u4ErrorCode,
                                                            i4_fsDiffServMultiFieldClfrId,
                                                            u4_addr_val_fsDiffServMultiFieldClfrDstIpAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTIPPRELEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstIpPreLen (&u4ErrorCode,
                                                              i4_fsDiffServMultiFieldClfrId,
                                                              p_value->
                                                              i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMIN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcPortMin (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             p_value->
                                                             u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMAX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcPortMax (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             p_value->
                                                             u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMIN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstPortMin (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             p_value->
                                                             u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMAX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstPortMax (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             p_value->
                                                             u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCPORT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcPort (&u4ErrorCode,
                                                          i4_fsDiffServMultiFieldClfrId,
                                                          p_value->
                                                          u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSRCPORTMASK:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrSrcPortMask (&u4ErrorCode,
                                                              i4_fsDiffServMultiFieldClfrId,
                                                              p_value->
                                                              u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTPORT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstPort (&u4ErrorCode,
                                                          i4_fsDiffServMultiFieldClfrId,
                                                          p_value->
                                                          u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRDSTPORTMASK:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrDstPortMask (&u4ErrorCode,
                                                              i4_fsDiffServMultiFieldClfrId,
                                                              p_value->
                                                              u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRPRECEDENCE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrPrecedence (&u4ErrorCode,
                                                             i4_fsDiffServMultiFieldClfrId,
                                                             p_value->
                                                             i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMULTIFIELDCLFRSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMultiFieldClfrStatus (&u4ErrorCode,
                                                         i4_fsDiffServMultiFieldClfrId,
                                                         p_value->
                                                         i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVMULTIFIELDCLFRID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServClfrEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServClfrEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServClfrTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServClfrId = FALSE;
    INT4                i4_next_fsDiffServClfrId = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServClfrTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServClfrTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServClfrId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServClfrTable
                     (i4_fsDiffServClfrId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServClfrId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServClfrTable (&i4_fsDiffServClfrId))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServClfrId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServClfrId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServClfrTable (i4_fsDiffServClfrId,
                                                         &i4_next_fsDiffServClfrId))
                    == SNMP_SUCCESS)
                {
                    i4_fsDiffServClfrId = i4_next_fsDiffServClfrId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServClfrId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVCLFRID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServClfrId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServClfrId;
            }
            break;
        }
        case FSDIFFSERVCLFRMFCLFRID:
        {
            i1_ret_val =
                nmhGetFsDiffServClfrMFClfrId (i4_fsDiffServClfrId,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVCLFRINPROACTIONID:
        {
            i1_ret_val =
                nmhGetFsDiffServClfrInProActionId (i4_fsDiffServClfrId,
                                                   &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVCLFROUTPROACTIONID:
        {
            i1_ret_val =
                nmhGetFsDiffServClfrOutProActionId (i4_fsDiffServClfrId,
                                                    &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVCLFRNOMATCHACTIONID:
        {
            i1_ret_val =
                nmhGetFsDiffServClfrNoMatchActionId (i4_fsDiffServClfrId,
                                                     &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVCLFRDATAPATHID:
        {
            i1_ret_val =
                nmhGetFsDiffServClfrDataPathId (i4_fsDiffServClfrId,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVCLFRSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServClfrStatus (i4_fsDiffServClfrId,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServClfrEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServClfrEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServClfrId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServClfrId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVCLFRMFCLFRID:
        {
            i1_ret_val =
                nmhSetFsDiffServClfrMFClfrId (i4_fsDiffServClfrId,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVCLFRINPROACTIONID:
        {
            i1_ret_val =
                nmhSetFsDiffServClfrInProActionId (i4_fsDiffServClfrId,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVCLFROUTPROACTIONID:
        {
            i1_ret_val =
                nmhSetFsDiffServClfrOutProActionId (i4_fsDiffServClfrId,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVCLFRNOMATCHACTIONID:
        {
            i1_ret_val =
                nmhSetFsDiffServClfrNoMatchActionId (i4_fsDiffServClfrId,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVCLFRDATAPATHID:
        {
            i1_ret_val =
                nmhSetFsDiffServClfrDataPathId (i4_fsDiffServClfrId,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVCLFRSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServClfrStatus (i4_fsDiffServClfrId,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVCLFRID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServClfrEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServClfrEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServClfrId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServClfrId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServClfrTable(i4_fsDiffServClfrId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVCLFRMFCLFRID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServClfrMFClfrId (&u4ErrorCode,
                                                 i4_fsDiffServClfrId,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVCLFRINPROACTIONID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServClfrInProActionId (&u4ErrorCode,
                                                      i4_fsDiffServClfrId,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVCLFROUTPROACTIONID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServClfrOutProActionId (&u4ErrorCode,
                                                       i4_fsDiffServClfrId,
                                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVCLFRNOMATCHACTIONID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServClfrNoMatchActionId (&u4ErrorCode,
                                                        i4_fsDiffServClfrId,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVCLFRDATAPATHID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServClfrDataPathId (&u4ErrorCode,
                                                   i4_fsDiffServClfrId,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVCLFRSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServClfrStatus (&u4ErrorCode,
                                               i4_fsDiffServClfrId,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVCLFRID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServInProfileActionEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServInProfileActionEntryGet (tSNMP_OID_TYPE * p_in_db,
                                   tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                   UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServInProfileActionTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServInProfileActionId = FALSE;
    INT4                i4_next_fsDiffServInProfileActionId = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServInProfileActionTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServInProfileActionTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServInProfileActionId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServInProfileActionTable
                     (i4_fsDiffServInProfileActionId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServInProfileActionId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServInProfileActionTable
                     (&i4_fsDiffServInProfileActionId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServInProfileActionId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServInProfileActionId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServInProfileActionTable
                     (i4_fsDiffServInProfileActionId,
                      &i4_next_fsDiffServInProfileActionId)) == SNMP_SUCCESS)
                {
                    i4_fsDiffServInProfileActionId =
                        i4_next_fsDiffServInProfileActionId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServInProfileActionId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVINPROFILEACTIONID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServInProfileActionId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServInProfileActionId;
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONFLAG:
        {
            i1_ret_val =
                nmhGetFsDiffServInProfileActionFlag
                (i4_fsDiffServInProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONNEWPRIO:
        {
            i1_ret_val =
                nmhGetFsDiffServInProfileActionNewPrio
                (i4_fsDiffServInProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONIPTOS:
        {
            i1_ret_val =
                nmhGetFsDiffServInProfileActionIpTOS
                (i4_fsDiffServInProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONPORT:
        {
            i1_ret_val =
                nmhGetFsDiffServInProfileActionPort
                (i4_fsDiffServInProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONDSCP:
        {
            i1_ret_val =
                nmhGetFsDiffServInProfileActionDscp
                (i4_fsDiffServInProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServInProfileActionStatus
                (i4_fsDiffServInProfileActionId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServInProfileActionEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServInProfileActionEntrySet (tSNMP_OID_TYPE * p_in_db,
                                   tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                   tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServInProfileActionId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServInProfileActionId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVINPROFILEACTIONFLAG:
        {
            i1_ret_val =
                nmhSetFsDiffServInProfileActionFlag
                (i4_fsDiffServInProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONNEWPRIO:
        {
            i1_ret_val =
                nmhSetFsDiffServInProfileActionNewPrio
                (i4_fsDiffServInProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONIPTOS:
        {
            i1_ret_val =
                nmhSetFsDiffServInProfileActionIpTOS
                (i4_fsDiffServInProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONPORT:
        {
            i1_ret_val =
                nmhSetFsDiffServInProfileActionPort
                (i4_fsDiffServInProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONDSCP:
        {
            i1_ret_val =
                nmhSetFsDiffServInProfileActionDscp
                (i4_fsDiffServInProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVINPROFILEACTIONSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServInProfileActionStatus
                (i4_fsDiffServInProfileActionId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVINPROFILEACTIONID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServInProfileActionEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServInProfileActionEntryTest (tSNMP_OID_TYPE * p_in_db,
                                    tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                    tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServInProfileActionId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServInProfileActionId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServInProfileActionTable(i4_fsDiffServInProfileActionId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVINPROFILEACTIONFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServInProfileActionFlag (&u4ErrorCode,
                                                        i4_fsDiffServInProfileActionId,
                                                        p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVINPROFILEACTIONNEWPRIO:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServInProfileActionNewPrio (&u4ErrorCode,
                                                           i4_fsDiffServInProfileActionId,
                                                           p_value->
                                                           u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVINPROFILEACTIONIPTOS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServInProfileActionIpTOS (&u4ErrorCode,
                                                         i4_fsDiffServInProfileActionId,
                                                         p_value->
                                                         u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVINPROFILEACTIONPORT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServInProfileActionPort (&u4ErrorCode,
                                                        i4_fsDiffServInProfileActionId,
                                                        p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVINPROFILEACTIONDSCP:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServInProfileActionDscp (&u4ErrorCode,
                                                        i4_fsDiffServInProfileActionId,
                                                        p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVINPROFILEACTIONSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServInProfileActionStatus (&u4ErrorCode,
                                                          i4_fsDiffServInProfileActionId,
                                                          p_value->
                                                          i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVINPROFILEACTIONID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServOutProfileActionEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServOutProfileActionEntryGet (tSNMP_OID_TYPE * p_in_db,
                                    tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                    UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServOutProfileActionTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServOutProfileActionId = FALSE;
    INT4                i4_next_fsDiffServOutProfileActionId = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServOutProfileActionTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServOutProfileActionTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServOutProfileActionId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServOutProfileActionTable
                     (i4_fsDiffServOutProfileActionId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServOutProfileActionId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServOutProfileActionTable
                     (&i4_fsDiffServOutProfileActionId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServOutProfileActionId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServOutProfileActionId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServOutProfileActionTable
                     (i4_fsDiffServOutProfileActionId,
                      &i4_next_fsDiffServOutProfileActionId)) == SNMP_SUCCESS)
                {
                    i4_fsDiffServOutProfileActionId =
                        i4_next_fsDiffServOutProfileActionId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServOutProfileActionId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVOUTPROFILEACTIONID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServOutProfileActionId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServOutProfileActionId;
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONFLAG:
        {
            i1_ret_val =
                nmhGetFsDiffServOutProfileActionFlag
                (i4_fsDiffServOutProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONDSCP:
        {
            i1_ret_val =
                nmhGetFsDiffServOutProfileActionDscp
                (i4_fsDiffServOutProfileActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONMID:
        {
            i1_ret_val =
                nmhGetFsDiffServOutProfileActionMID
                (i4_fsDiffServOutProfileActionId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServOutProfileActionStatus
                (i4_fsDiffServOutProfileActionId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServOutProfileActionEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServOutProfileActionEntrySet (tSNMP_OID_TYPE * p_in_db,
                                    tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                    tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServOutProfileActionId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServOutProfileActionId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVOUTPROFILEACTIONFLAG:
        {
            i1_ret_val =
                nmhSetFsDiffServOutProfileActionFlag
                (i4_fsDiffServOutProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONDSCP:
        {
            i1_ret_val =
                nmhSetFsDiffServOutProfileActionDscp
                (i4_fsDiffServOutProfileActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONMID:
        {
            i1_ret_val =
                nmhSetFsDiffServOutProfileActionMID
                (i4_fsDiffServOutProfileActionId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVOUTPROFILEACTIONSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServOutProfileActionStatus
                (i4_fsDiffServOutProfileActionId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVOUTPROFILEACTIONID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServOutProfileActionEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServOutProfileActionEntryTest (tSNMP_OID_TYPE * p_in_db,
                                     tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                     tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServOutProfileActionId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServOutProfileActionId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServOutProfileActionTable(i4_fsDiffServOutProfileActionId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVOUTPROFILEACTIONFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServOutProfileActionFlag (&u4ErrorCode,
                                                         i4_fsDiffServOutProfileActionId,
                                                         p_value->
                                                         u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVOUTPROFILEACTIONDSCP:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServOutProfileActionDscp (&u4ErrorCode,
                                                         i4_fsDiffServOutProfileActionId,
                                                         p_value->
                                                         u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVOUTPROFILEACTIONMID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServOutProfileActionMID (&u4ErrorCode,
                                                        i4_fsDiffServOutProfileActionId,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVOUTPROFILEACTIONSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServOutProfileActionStatus (&u4ErrorCode,
                                                           i4_fsDiffServOutProfileActionId,
                                                           p_value->
                                                           i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVOUTPROFILEACTIONID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServNoMatchActionEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServNoMatchActionEntryGet (tSNMP_OID_TYPE * p_in_db,
                                 tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                 UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServNoMatchActionTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServNoMatchActionId = FALSE;
    INT4                i4_next_fsDiffServNoMatchActionId = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServNoMatchActionTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServNoMatchActionTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServNoMatchActionId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServNoMatchActionTable
                     (i4_fsDiffServNoMatchActionId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServNoMatchActionId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServNoMatchActionTable
                     (&i4_fsDiffServNoMatchActionId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServNoMatchActionId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServNoMatchActionId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServNoMatchActionTable
                     (i4_fsDiffServNoMatchActionId,
                      &i4_next_fsDiffServNoMatchActionId)) == SNMP_SUCCESS)
                {
                    i4_fsDiffServNoMatchActionId =
                        i4_next_fsDiffServNoMatchActionId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServNoMatchActionId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVNOMATCHACTIONID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServNoMatchActionId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServNoMatchActionId;
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONFLAG:
        {
            i1_ret_val =
                nmhGetFsDiffServNoMatchActionFlag (i4_fsDiffServNoMatchActionId,
                                                   &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONNEWPRIO:
        {
            i1_ret_val =
                nmhGetFsDiffServNoMatchActionNewPrio
                (i4_fsDiffServNoMatchActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONIPTOS:
        {
            i1_ret_val =
                nmhGetFsDiffServNoMatchActionIpTOS
                (i4_fsDiffServNoMatchActionId, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONPORT:
        {
            i1_ret_val =
                nmhGetFsDiffServNoMatchActionPort (i4_fsDiffServNoMatchActionId,
                                                   &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONDSCP:
        {
            i1_ret_val =
                nmhGetFsDiffServNoMatchActionDscp (i4_fsDiffServNoMatchActionId,
                                                   &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServNoMatchActionStatus
                (i4_fsDiffServNoMatchActionId, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServNoMatchActionEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServNoMatchActionEntrySet (tSNMP_OID_TYPE * p_in_db,
                                 tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                 tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServNoMatchActionId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServNoMatchActionId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVNOMATCHACTIONFLAG:
        {
            i1_ret_val =
                nmhSetFsDiffServNoMatchActionFlag (i4_fsDiffServNoMatchActionId,
                                                   p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONNEWPRIO:
        {
            i1_ret_val =
                nmhSetFsDiffServNoMatchActionNewPrio
                (i4_fsDiffServNoMatchActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONIPTOS:
        {
            i1_ret_val =
                nmhSetFsDiffServNoMatchActionIpTOS
                (i4_fsDiffServNoMatchActionId, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONPORT:
        {
            i1_ret_val =
                nmhSetFsDiffServNoMatchActionPort (i4_fsDiffServNoMatchActionId,
                                                   p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONDSCP:
        {
            i1_ret_val =
                nmhSetFsDiffServNoMatchActionDscp (i4_fsDiffServNoMatchActionId,
                                                   p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVNOMATCHACTIONSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServNoMatchActionStatus
                (i4_fsDiffServNoMatchActionId, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVNOMATCHACTIONID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServNoMatchActionEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServNoMatchActionEntryTest (tSNMP_OID_TYPE * p_in_db,
                                  tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                  tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServNoMatchActionId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServNoMatchActionId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServNoMatchActionTable(i4_fsDiffServNoMatchActionId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVNOMATCHACTIONFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServNoMatchActionFlag (&u4ErrorCode,
                                                      i4_fsDiffServNoMatchActionId,
                                                      p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVNOMATCHACTIONNEWPRIO:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServNoMatchActionNewPrio (&u4ErrorCode,
                                                         i4_fsDiffServNoMatchActionId,
                                                         p_value->
                                                         u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVNOMATCHACTIONIPTOS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServNoMatchActionIpTOS (&u4ErrorCode,
                                                       i4_fsDiffServNoMatchActionId,
                                                       p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVNOMATCHACTIONPORT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServNoMatchActionPort (&u4ErrorCode,
                                                      i4_fsDiffServNoMatchActionId,
                                                      p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVNOMATCHACTIONDSCP:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServNoMatchActionDscp (&u4ErrorCode,
                                                      i4_fsDiffServNoMatchActionId,
                                                      p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVNOMATCHACTIONSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServNoMatchActionStatus (&u4ErrorCode,
                                                        i4_fsDiffServNoMatchActionId,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVNOMATCHACTIONID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServMeterEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServMeterEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServMeterTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServMeterId = FALSE;
    INT4                i4_next_fsDiffServMeterId = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServMeterTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServMeterTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServMeterId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServMeterTable
                     (i4_fsDiffServMeterId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServMeterId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServMeterTable
                     (&i4_fsDiffServMeterId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServMeterId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServMeterId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServMeterTable (i4_fsDiffServMeterId,
                                                          &i4_next_fsDiffServMeterId))
                    == SNMP_SUCCESS)
                {
                    i4_fsDiffServMeterId = i4_next_fsDiffServMeterId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServMeterId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVMETERID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServMeterId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServMeterId;
            }
            break;
        }
        case FSDIFFSERVMETERTOKENSIZE:
        {
            i1_ret_val =
                nmhGetFsDiffServMetertokenSize (i4_fsDiffServMeterId,
                                                &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMETERREFRESHCOUNT:
        {
            i1_ret_val =
                nmhGetFsDiffServMeterRefreshCount (i4_fsDiffServMeterId,
                                                   &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVMETERSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServMeterStatus (i4_fsDiffServMeterId,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServMeterEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServMeterEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServMeterId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServMeterId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVMETERTOKENSIZE:
        {
            i1_ret_val =
                nmhSetFsDiffServMetertokenSize (i4_fsDiffServMeterId,
                                                p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMETERREFRESHCOUNT:
        {
            i1_ret_val =
                nmhSetFsDiffServMeterRefreshCount (i4_fsDiffServMeterId,
                                                   p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVMETERSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServMeterStatus (i4_fsDiffServMeterId,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVMETERID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServMeterEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServMeterEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServMeterId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServMeterId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServMeterTable(i4_fsDiffServMeterId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVMETERTOKENSIZE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMetertokenSize (&u4ErrorCode,
                                                   i4_fsDiffServMeterId,
                                                   p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMETERREFRESHCOUNT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMeterRefreshCount (&u4ErrorCode,
                                                      i4_fsDiffServMeterId,
                                                      p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVMETERSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServMeterStatus (&u4ErrorCode,
                                                i4_fsDiffServMeterId,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVMETERID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsDiffServSchedulerEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsDiffServSchedulerEntryGet (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsDiffServSchedulerTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsDiffServSchedulerId = FALSE;
    INT4                i4_next_fsDiffServSchedulerId = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsDiffServSchedulerWeight = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsDiffServSchedulerTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsDiffServSchedulerTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsDiffServSchedulerId =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsDiffServSchedulerTable
                     (i4_fsDiffServSchedulerId)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsDiffServSchedulerId;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsDiffServSchedulerTable
                     (&i4_fsDiffServSchedulerId)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsDiffServSchedulerId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsDiffServSchedulerId =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsDiffServSchedulerTable
                     (i4_fsDiffServSchedulerId,
                      &i4_next_fsDiffServSchedulerId)) == SNMP_SUCCESS)
                {
                    i4_fsDiffServSchedulerId = i4_next_fsDiffServSchedulerId;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsDiffServSchedulerId;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSDIFFSERVSCHEDULERID:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsDiffServSchedulerId;
            }
            else
            {
                i4_return_val = i4_next_fsDiffServSchedulerId;
            }
            break;
        }
        case FSDIFFSERVSCHEDULERDPID:
        {
            i1_ret_val =
                nmhGetFsDiffServSchedulerDPId (i4_fsDiffServSchedulerId,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVSCHEDULERQUEUECOUNT:
        {
            i1_ret_val =
                nmhGetFsDiffServSchedulerQueueCount (i4_fsDiffServSchedulerId,
                                                     &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVSCHEDULERWEIGHT:
        {
            poctet_retval_fsDiffServSchedulerWeight =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (5);
            if (poctet_retval_fsDiffServSchedulerWeight == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsDiffServSchedulerWeight (i4_fsDiffServSchedulerId,
                                                 poctet_retval_fsDiffServSchedulerWeight);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsDiffServSchedulerWeight;
            }
            else
            {
                free_octetstring (poctet_retval_fsDiffServSchedulerWeight);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSDIFFSERVSCHEDULERSTATUS:
        {
            i1_ret_val =
                nmhGetFsDiffServSchedulerStatus (i4_fsDiffServSchedulerId,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsDiffServSchedulerEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsDiffServSchedulerEntrySet (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsDiffServSchedulerId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsDiffServSchedulerId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSDIFFSERVSCHEDULERDPID:
        {
            i1_ret_val =
                nmhSetFsDiffServSchedulerDPId (i4_fsDiffServSchedulerId,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVSCHEDULERQUEUECOUNT:
        {
            i1_ret_val =
                nmhSetFsDiffServSchedulerQueueCount (i4_fsDiffServSchedulerId,
                                                     p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVSCHEDULERWEIGHT:
        {
            i1_ret_val =
                nmhSetFsDiffServSchedulerWeight (i4_fsDiffServSchedulerId,
                                                 p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSDIFFSERVSCHEDULERSTATUS:
        {
            i1_ret_val =
                nmhSetFsDiffServSchedulerStatus (i4_fsDiffServSchedulerId,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSDIFFSERVSCHEDULERID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsDiffServSchedulerEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsDiffServSchedulerEntryTest (tSNMP_OID_TYPE * p_in_db,
                              tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                              tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsDiffServSchedulerId = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsDiffServSchedulerId =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsDiffServSchedulerTable(i4_fsDiffServSchedulerId)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSDIFFSERVSCHEDULERDPID:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServSchedulerDPId (&u4ErrorCode,
                                                  i4_fsDiffServSchedulerId,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVSCHEDULERQUEUECOUNT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServSchedulerQueueCount (&u4ErrorCode,
                                                        i4_fsDiffServSchedulerId,
                                                        p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVSCHEDULERWEIGHT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServSchedulerWeight (&u4ErrorCode,
                                                    i4_fsDiffServSchedulerId,
                                                    p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSDIFFSERVSCHEDULERSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsDiffServSchedulerStatus (&u4ErrorCode,
                                                    i4_fsDiffServSchedulerId,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSDIFFSERVSCHEDULERID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
