/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 *  $Id: dsmbsm.c,v 1.3 2014/12/09 12:46:06 siva Exp $
 *
 *  Description: This file contains HW routine for Diffserv HW configuration
 *
 *  ********************************************************************/


#include "dsinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmDiffservCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the Diffserv HW configuration when   */
/*                a card is inserted into the MBSM System for the            */
/*                distributed architecture                                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmDiffservCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    tDiffServMeterEntry *pDsMeterEntry = NULL;

    UNUSED_PARAM (pPortInfo);

    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        /* Call the routine to Initialise the diffsrv in h/w */
        if (DsMbsmHwInit (pSlotInfo) != FNP_SUCCESS)
        {
            DS_TRC (ALL_FAILURE_TRC, "IssMbsmDiffservCardInsert : DsMbsmHwInit returns failure\n");
            return MBSM_FAILURE;
        }
    }
    else
    {
        return MBSM_SUCCESS;
    }

    TMO_SLL_Scan (&DS_CLFR_LIST, pDsClfrEntry, tDiffServClfrEntry *)
    {

        if (pDsClfrEntry->u1DsClfrStatus != DS_ACTIVE)
        {
            continue;
        }
        else
        {

            /* Get the Other Elements from the list here */
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);

            if (pDsMFClfrEntry == NULL)
            {
                continue;
            }
            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);
            pDsOutProActEntry = DsGetOutProfileActionEntry
                (pDsClfrEntry->i4DsClfrOutProActionId);
            if (pDsOutProActEntry != NULL)
            {
                pDsMeterEntry = DsGetMeterEntry
                    (pDsOutProActEntry->i4DsOutProfileActionMID);
            }

            /* Call the hardware routine here */
            if (DsMbsmHwClassifierAdd (pDsClfrEntry, pDsMFClfrEntry,
                                       pDsInProActEntry, pDsOutProActEntry,
                                       pDsMeterEntry, pSlotInfo) != FNP_SUCCESS)
            {
                DS_TRC (ALL_FAILURE_TRC, "IssMbsmDiffservCardInsert : DsMbsmHwClassifierAdd returns failure\n");
                return MBSM_FAILURE;
            }
        }

    }

    TMO_SLL_Scan (&DS_SCHEDULER_LIST, pDsSchedulerEntry,
                  tDiffServSchedulerEntry *)
    {

        if (pDsSchedulerEntry->u1DsSchedulerStatus != DS_ACTIVE)
        {
            continue;
        }

        else
        {
            /* Call the hardware routine here */
            if (DsMbsmHwSchedulerAdd (pDsSchedulerEntry, pSlotInfo) !=
                FNP_SUCCESS)
            {
                DS_TRC (ALL_FAILURE_TRC, "IssMbsmDiffservCardInsert : DsMbsmHwSchedulerAdd returns failure\n");
                return DS_FAILURE;
            }
        }

    }

    DS_TRC (CONTROL_PLANE_TRC, "IssMbsmDiffservCardInsert updated the Diffserv HW configuration and returns success\n");

    return MBSM_SUCCESS;
}
