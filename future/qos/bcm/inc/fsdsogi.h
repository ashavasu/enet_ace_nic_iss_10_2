
# ifndef fsdsOGP_H
# define fsdsOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSDIFFSERVSYSTEM                             (0)
# define SNMP_OGP_INDEX_FSDIFFSERVMULTIFIELDCLFRTABLE                (1)
# define SNMP_OGP_INDEX_FSDIFFSERVCLFRTABLE                          (2)
# define SNMP_OGP_INDEX_FSDIFFSERVINPROFILEACTIONTABLE               (3)
# define SNMP_OGP_INDEX_FSDIFFSERVOUTPROFILEACTIONTABLE              (4)
# define SNMP_OGP_INDEX_FSDIFFSERVMETERTABLE                         (5)
# define SNMP_OGP_INDEX_FSDIFFSERVSCHEDULERTABLE                     (6)

#endif /*  fsissdfsOGP_H  */
