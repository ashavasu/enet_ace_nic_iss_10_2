/*****************************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : fsdslow.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ module                                */
/*    MODULE NAME           : DiffServ module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 18 Dec 2002                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains low level DiffServ routines */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/

# include  "dsinc.h"
# include  "dscli.h"
extern UINT4        gu4PortMask[DS_MAX_PORT_MASK + 1];

extern UINT4        fsissd[8];
extern UINT4        FsDsSystemControl[11];
PRIVATE VOID        DsNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsSystemControl
 Input       :  The Indices

                The Object 
                retValFsDsSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsSystemControl (INT4 *pi4RetValFsDsSystemControl)
{
    *pi4RetValFsDsSystemControl = gDsGlobalInfo.DsSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDsStatus
 Input       :  The Indices

                The Object 
                retValFsDsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsStatus (INT4 *pi4RetValFsDsStatus)
{
    *pi4RetValFsDsStatus = gDsGlobalInfo.DsStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsDsSystemControl
 Input       :  The Indices

                The Object 
                setValFsDsSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsSystemControl (INT4 i4SetValFsDsSystemControl)
{
    if (i4SetValFsDsSystemControl == (INT4) gDsGlobalInfo.DsSystemControl)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "SNMP: DS is already having the same System Control State\n");
        return SNMP_SUCCESS;
    }
    if (i4SetValFsDsSystemControl == DS_START)

    {
        if (DsStart () != DS_SUCCESS)

        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Start FAILED\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (DsShutDown () != DS_SUCCESS)

        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Shutdown FAILED\n");
            return SNMP_FAILURE;
        }
        /* Notify MSR with the diffserv oids */
        DsNotifyProtocolShutdownStatus ();
    }
    gDsGlobalInfo.DsSystemControl = i4SetValFsDsSystemControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsStatus
 Input       :  The Indices

                The Object 
                setValFsDsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsStatus (INT4 i4SetValFsDsStatus)
{
    if (i4SetValFsDsStatus == (INT4) gDsGlobalInfo.DsStatus)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "SNMP: DS is already having the same System Status\n");
        return SNMP_SUCCESS;
    }
    if (i4SetValFsDsStatus == DS_ENABLE)

    {
        if (DsEnable () != DS_SUCCESS)

        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Enable FAILED\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (DsDisable () != DS_SUCCESS)

        {
            DS_TRC (MGMT_TRC | ALL_FAILURE_TRC, "SNMP: DS Disble FAILED\n");
            return SNMP_FAILURE;
        }
    }
    gDsGlobalInfo.DsStatus = i4SetValFsDsStatus;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsDsSystemControl
 Input       :  The Indices

                The Object 
                testValFsDsSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsSystemControl (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsDsSystemControl)
{
    if ((i4TestValFsDsSystemControl == DS_START) ||
        (i4TestValFsDsSystemControl == DS_SHUTDOWN))

    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDsStatus
 Input       :  The Indices

                The Object 
                testValFsDsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsDsStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDsStatus == DS_ENABLE) ||
        (i4TestValFsDsStatus == DS_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : FsDiffServMultiFieldClfrTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable (INT4
                                                       i4FsDiffServMultiFieldClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (DsValidateMFClfrId (i4FsDiffServMultiFieldClfrId) != DS_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServMultiFieldClfrTable (INT4
                                               *pi4FsDiffServMultiFieldClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetFirstValidMultiFieldClfrId
         (pi4FsDiffServMultiFieldClfrId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
                nextFsDiffServMultiFieldClfrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServMultiFieldClfrTable (INT4 i4FsDiffServMultiFieldClfrId,
                                              INT4
                                              *pi4NextFsDiffServMultiFieldClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (i4FsDiffServMultiFieldClfrId < 0)

    {
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetNextValidMultiFieldClfrId
         (i4FsDiffServMultiFieldClfrId,
          pi4NextFsDiffServMultiFieldClfrId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServMultiFieldClfrFilterId
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                retValFsDiffServMultiFieldClfrFilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMultiFieldClfrFilterId (INT4 i4FsDiffServMultiFieldClfrId,
                                        UINT4
                                        *pu4RetValFsDiffServMultiFieldClfrFilterId)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing"
                "its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);
    if (pDsMultiFieldClfrEntry != NULL)

    {
        *pu4RetValFsDiffServMultiFieldClfrFilterId =
            pDsMultiFieldClfrEntry->u4DsMultiFieldClfrFilterId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServMultiFieldClfrFilterType
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                retValFsDiffServMultiFieldClfrFilterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMultiFieldClfrFilterType (INT4 i4FsDiffServMultiFieldClfrId,
                                          INT4
                                          *pi4RetValFsDiffServMultiFieldClfrFilterType)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing"
                "its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);
    if (pDsMultiFieldClfrEntry != NULL)

    {
        *pi4RetValFsDiffServMultiFieldClfrFilterType =
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrFilterType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServMultiFieldClfrStatus
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                retValFsDiffServMultiFieldClfrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMultiFieldClfrStatus (INT4 i4FsDiffServMultiFieldClfrId,
                                      INT4
                                      *pi4RetValFsDiffServMultiFieldClfrStatus)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);
    if (pDsMultiFieldClfrEntry != NULL)

    {
        *pi4RetValFsDiffServMultiFieldClfrStatus =
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServMultiFieldClfrFilterId
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                setValFsDiffServMultiFieldClfrFilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMultiFieldClfrFilterId (INT4 i4FsDiffServMultiFieldClfrId,
                                        UINT4
                                        u4SetValFsDiffServMultiFieldClfrFilterId)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);
    if ((pDsMultiFieldClfrEntry != NULL) &&
        (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus != DS_ACTIVE))

    {
        pDsMultiFieldClfrEntry->u4DsMultiFieldClfrFilterId =
            u4SetValFsDiffServMultiFieldClfrFilterId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServMultiFieldClfrFilterType
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                setValFsDiffServMultiFieldClfrFilterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMultiFieldClfrFilterType (INT4 i4FsDiffServMultiFieldClfrId,
                                          INT4
                                          i4SetValFsDiffServMultiFieldClfrFilterType)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);
    if ((pDsMultiFieldClfrEntry != NULL) &&
        (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus != DS_ACTIVE))

    {
        pDsMultiFieldClfrEntry->u1DsMultiFieldClfrFilterType =
            (UINT1) i4SetValFsDiffServMultiFieldClfrFilterType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServMultiFieldClfrStatus
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                setValFsDiffServMultiFieldClfrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMultiFieldClfrStatus (INT4 i4FsDiffServMultiFieldClfrId,
                                      INT4
                                      i4SetValFsDiffServMultiFieldClfrStatus)
{
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    pDsMultiFieldClfrEntry = DsGetMFClfrEntry (i4FsDiffServMultiFieldClfrId);
    if (pDsMultiFieldClfrEntry != NULL)

    {
        if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus ==
            (UINT1) i4SetValFsDiffServMultiFieldClfrStatus)

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServMultiFieldClfrStatus == DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    else
    {

        /* This Dath Path Entry is not there in the Database */
        if (i4SetValFsDiffServMultiFieldClfrStatus != DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValFsDiffServMultiFieldClfrStatus)

    {
        case DS_CREATE_AND_WAIT:
            if (DS_MFCLFRENTRY_ALLOC_MEM_BLOCK (pDsMultiFieldClfrEntry) == NULL)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Multi Field Clfr Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsMultiFieldClfrEntry, 0,
                       sizeof (tDiffServMultiFieldClfrEntry));
            pDsMultiFieldClfrEntry->i4DsMultiFieldClfrId =
                i4FsDiffServMultiFieldClfrId;

            /* Setting the default values for the column objects */
            pDsMultiFieldClfrEntry->u4DsMultiFieldClfrFilterId = 0;
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrFilterType = 0;
            DS_SLL_ADD (&(DS_MFCLFR_LIST),
                        &(pDsMultiFieldClfrEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus = DS_NOT_READY;
            break;
        case DS_NOT_IN_SERVICE:
            if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus == DS_ACTIVE)
            {
                /* Check if any active classifier is using this MF Filter */
                if (DsCheckMFClfrDependency
                    (pDsMultiFieldClfrEntry->i4DsMultiFieldClfrId)
                    != DS_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus =
                    DS_NOT_IN_SERVICE;
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;
        case DS_ACTIVE:

            /* call the function to qualify the MFClfr Entry data */
            if (DsQualifyMFClfrData (pDsMultiFieldClfrEntry) != DS_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)
            {
                return SNMP_FAILURE;
            }
            if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus !=
                DS_NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }
            pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus = DS_ACTIVE;
            break;
        case DS_DESTROY:
            if (pDsMultiFieldClfrEntry->u1DsMultiFieldClfrStatus == DS_ACTIVE)
            {
                /* Check if any active classifier is using this MF Filter */
                if (DsCheckMFClfrDependency
                    (pDsMultiFieldClfrEntry->i4DsMultiFieldClfrId)
                    != DS_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_MFCLFR_LIST),
                           &(pDsMultiFieldClfrEntry->DsNextNode));

            /* Delete the In Profile Action Entry from the Software */
            if (DS_MFCLFRENTRY_FREE_MEM_BLOCK (pDsMultiFieldClfrEntry) !=
                DS_MEM_SUCCESS)
            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Multi Field Clfr Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMultiFieldClfrFilterId
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                testValFsDiffServMultiFieldClfrFilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMultiFieldClfrFilterId (UINT4 *pu4ErrorCode,
                                           INT4 i4FsDiffServMultiFieldClfrId,
                                           UINT4
                                           u4TestValFsDiffServMultiFieldClfrFilterId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_MFC_ID_VALID (i4FsDiffServMultiFieldClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServMultiFieldClfrFilterId < DS_MIN_FILTER_ID) ||
        (u4TestValFsDiffServMultiFieldClfrFilterId > DS_MAX_FILTER_ID))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMultiFieldClfrFilterType
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                testValFsDiffServMultiFieldClfrFilterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMultiFieldClfrFilterType (UINT4 *pu4ErrorCode,
                                             INT4 i4FsDiffServMultiFieldClfrId,
                                             INT4
                                             i4TestValFsDiffServMultiFieldClfrFilterType)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_MFC_ID_VALID (i4FsDiffServMultiFieldClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServMultiFieldClfrFilterType == DS_MAC_FILTER) ||
        (i4TestValFsDiffServMultiFieldClfrFilterType == DS_IP_FILTER))

    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMultiFieldClfrStatus
 Input       :  The Indices
                FsDiffServMultiFieldClfrId

                The Object 
                testValFsDiffServMultiFieldClfrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMultiFieldClfrStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDiffServMultiFieldClfrId,
                                         INT4
                                         i4TestValFsDiffServMultiFieldClfrStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServMultiFieldClfrStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServMultiFieldClfrStatus > DS_DESTROY))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_MFC_ID_VALID (i4FsDiffServMultiFieldClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServMultiFieldClfrStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_MFCLFR_LIST)) == DS_MFCLFR_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_CLASS_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServClfrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServClfrTable (INT4 i4FsDiffServClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (DsValidateClfrId (i4FsDiffServClfrId) != DS_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServClfrTable (INT4 *pi4FsDiffServClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetFirstValidClfrId (pi4FsDiffServClfrId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
                nextFsDiffServClfrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServClfrTable (INT4 i4FsDiffServClfrId,
                                    INT4 *pi4NextFsDiffServClfrId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (i4FsDiffServClfrId < 0)

    {
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetNextValidClfrId
         (i4FsDiffServClfrId, pi4NextFsDiffServClfrId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrMFClfrId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrMFClfrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrMFClfrId (INT4 i4FsDiffServClfrId,
                              INT4 *pi4RetValFsDiffServClfrMFClfrId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        *pi4RetValFsDiffServClfrMFClfrId = pDsClfrEntry->i4DsClfrMFClfrId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrInProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrInProActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrInProActionId (INT4 i4FsDiffServClfrId,
                                   INT4 *pi4RetValFsDiffServClfrInProActionId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        *pi4RetValFsDiffServClfrInProActionId =
            pDsClfrEntry->i4DsClfrInProActionId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrOutProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrOutProActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrOutProActionId (INT4 i4FsDiffServClfrId,
                                    INT4 *pi4RetValFsDiffServClfrOutProActionId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        *pi4RetValFsDiffServClfrOutProActionId =
            pDsClfrEntry->i4DsClfrOutProActionId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServClfrStatus
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                retValFsDiffServClfrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServClfrStatus (INT4 i4FsDiffServClfrId,
                            INT4 *pi4RetValFsDiffServClfrStatus)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        *pi4RetValFsDiffServClfrStatus = pDsClfrEntry->u1DsClfrStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrMFClfrId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrMFClfrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrMFClfrId (INT4 i4FsDiffServClfrId,
                              INT4 i4SetValFsDiffServClfrMFClfrId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if ((pDsClfrEntry != NULL) && (pDsClfrEntry->u1DsClfrStatus != DS_ACTIVE))

    {
        pDsClfrEntry->i4DsClfrMFClfrId = i4SetValFsDiffServClfrMFClfrId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrInProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrInProActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrInProActionId (INT4 i4FsDiffServClfrId,
                                   INT4 i4SetValFsDiffServClfrInProActionId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        pDsClfrEntry->i4DsClfrInProActionId =
            i4SetValFsDiffServClfrInProActionId;
        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)

        {

            /* Get other elements to call the hardware Update */
            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);
            pDsOutProActEntry = DsGetOutProfileActionEntry
                (pDsClfrEntry->i4DsClfrOutProActionId);
            pDsMultiFieldClfrEntry = DsGetMFClfrEntry
                (pDsClfrEntry->i4DsClfrMFClfrId);

            /* Get the meter entry if OutProfile Action is not NULL */
            if (pDsOutProActEntry != NULL)

            {
                pDsMeterEntry = DsGetMeterEntry
                    (pDsOutProActEntry->i4DsOutProfileActionMID);
            }

            else

            {
                pDsMeterEntry = NULL;
            }

#ifdef NPAPI_WANTED
            /* Update the classifier in the hardware here */
            if (DsHwClassifierUpdate (pDsClfrEntry, pDsMultiFieldClfrEntry,
                                      pDsInProActEntry, pDsOutProActEntry,
                                      pDsMeterEntry) != FNP_SUCCESS)

            {
                return SNMP_FAILURE;
            }

#endif /*  */
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrOutProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrOutProActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrOutProActionId (INT4 i4FsDiffServClfrId,
                                    INT4 i4SetValFsDiffServClfrOutProActionId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMultiFieldClfrEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        pDsClfrEntry->i4DsClfrOutProActionId =
            i4SetValFsDiffServClfrOutProActionId;
        if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)

        {
            /* Get other elements to call the hardware Update */
            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);
            pDsOutProActEntry = DsGetOutProfileActionEntry
                (pDsClfrEntry->i4DsClfrOutProActionId);
            pDsMultiFieldClfrEntry = DsGetMFClfrEntry
                (pDsClfrEntry->i4DsClfrMFClfrId);

            /* Get the meter entry if OutProfile Action is not NULL */
            if (pDsOutProActEntry != NULL)

            {
                pDsMeterEntry = DsGetMeterEntry
                    (pDsOutProActEntry->i4DsOutProfileActionMID);
            }

            else

            {
                pDsMeterEntry = NULL;
            }

#ifdef NPAPI_WANTED
            /* Update the classifier in the hardware here */
            if (DsHwClassifierUpdate (pDsClfrEntry, pDsMultiFieldClfrEntry,
                                      pDsInProActEntry, pDsOutProActEntry,
                                      pDsMeterEntry) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

#endif /*  */
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServClfrStatus
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                setValFsDiffServClfrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServClfrStatus (INT4 i4FsDiffServClfrId,
                            INT4 i4SetValFsDiffServClfrStatus)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL;
    tDiffServMultiFieldClfrEntry *pDsMFClfrEntry = NULL;
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    tDiffServMeterEntry *pDsMeterEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal;

#endif /*  */
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        if (pDsClfrEntry->u1DsClfrStatus ==
            (UINT1) i4SetValFsDiffServClfrStatus)

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServClfrStatus == DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }

    else

    {

        /* This Classifier Entry is not there in the Database */
        if (i4SetValFsDiffServClfrStatus != DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValFsDiffServClfrStatus)

    {
        case DS_CREATE_AND_WAIT:
            if (DS_CLFRENTRY_ALLOC_MEM_BLOCK (pDsClfrEntry) == NULL)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Clfr Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsClfrEntry, 0, sizeof (tDiffServClfrEntry));
            pDsClfrEntry->i4DsClfrId = i4FsDiffServClfrId;

            /*Setting the default values for the column objects. */
            pDsClfrEntry->i4DsClfrMFClfrId = 0;
            pDsClfrEntry->i4DsClfrInProActionId = 0;
            pDsClfrEntry->i4DsClfrOutProActionId = 0;
            DS_SLL_ADD (&(DS_CLFR_LIST), &(pDsClfrEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsClfrEntry->u1DsClfrStatus = DS_NOT_READY;
            break;
        case DS_NOT_IN_SERVICE:
            if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)

            {

#ifdef NPAPI_WANTED
                pDsOutProActEntry = DsGetOutProfileActionEntry
                    (pDsClfrEntry->i4DsClfrOutProActionId);
                pDsMFClfrEntry = DsGetMFClfrEntry
                    (pDsClfrEntry->i4DsClfrMFClfrId);

                /* Get the meter entry if OutProfile Action is not NULL */
                if (pDsOutProActEntry != NULL)

                {
                    pDsMeterEntry = DsGetMeterEntry
                        (pDsOutProActEntry->i4DsOutProfileActionMID);
                }
                else
                {
                    pDsMeterEntry = NULL;
                }
                /* Call the Classifier delete for the hadware */
                i4RetVal = DsHwClassifierDelete (pDsClfrEntry, pDsMFClfrEntry,
                                                 pDsMeterEntry);
                if (i4RetVal != FNP_SUCCESS)

                {
                    return SNMP_FAILURE;
                }

#endif /*  */
                pDsClfrEntry->u1DsClfrStatus = DS_NOT_IN_SERVICE;
            }

            else

            {
                return SNMP_FAILURE;
            }
            break;
        case DS_ACTIVE:

            /* call the function to qualify the Clfr entry data */
            if (DsQualifyClfrData (pDsClfrEntry) != DS_SUCCESS)

            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)

            {
                return SNMP_FAILURE;
            }
            if (pDsClfrEntry->u1DsClfrStatus != DS_NOT_IN_SERVICE)

            {
                return SNMP_FAILURE;
            }

            /* Get the Other Elements from the list here */
            pDsMFClfrEntry = DsGetMFClfrEntry (pDsClfrEntry->i4DsClfrMFClfrId);
            if (pDsMFClfrEntry == NULL)

            {
                return SNMP_FAILURE;
            }
            pDsInProActEntry = DsGetInProfileActionEntry
                (pDsClfrEntry->i4DsClfrInProActionId);
            pDsOutProActEntry = DsGetOutProfileActionEntry
                (pDsClfrEntry->i4DsClfrOutProActionId);

            /* Get the meter entry if OutProfile Action is not NULL */
            if (pDsOutProActEntry != NULL)

            {
                pDsMeterEntry = DsGetMeterEntry
                    (pDsOutProActEntry->i4DsOutProfileActionMID);
            }

            else

            {
                pDsMeterEntry = NULL;
            }

#ifdef NPAPI_WANTED
            /* Call routine here to add the Clfr to H/W */
            i4RetVal =
                DsHwClassifierAdd (pDsClfrEntry, pDsMFClfrEntry,
                                   pDsInProActEntry, pDsOutProActEntry,
                                   pDsMeterEntry);
            if (i4RetVal != FNP_SUCCESS)

            {
                return SNMP_FAILURE;
            }

#endif /*  */
            pDsClfrEntry->u1DsClfrStatus = DS_ACTIVE;
            break;
        case DS_DESTROY:
            if (pDsClfrEntry->u1DsClfrStatus == DS_ACTIVE)

            {

#ifdef NPAPI_WANTED
                pDsOutProActEntry = DsGetOutProfileActionEntry
                    (pDsClfrEntry->i4DsClfrOutProActionId);
                pDsMFClfrEntry = DsGetMFClfrEntry
                    (pDsClfrEntry->i4DsClfrMFClfrId);

                /* Get the meter entry if OutProfile Action is not NULL */
                if (pDsOutProActEntry != NULL)

                {
                    pDsMeterEntry = DsGetMeterEntry
                        (pDsOutProActEntry->i4DsOutProfileActionMID);
                }
                else
                {
                    pDsMeterEntry = NULL;
                }
                /* Call the Classifier delete for the hadware */
                i4RetVal = DsHwClassifierDelete (pDsClfrEntry, pDsMFClfrEntry,
                                                 pDsMeterEntry);
                if (i4RetVal != FNP_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
#endif /*  */
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_CLFR_LIST), &(pDsClfrEntry->DsNextNode));

            /* Delete the In Profile Action Entry from the Software */
            if (DS_CLFRENTRY_FREE_MEM_BLOCK (pDsClfrEntry) != DS_MEM_SUCCESS)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No ClfrEntry Action Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrMFClfrId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrMFClfrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrMFClfrId (UINT4 *pu4ErrorCode, INT4 i4FsDiffServClfrId,
                                 INT4 i4TestValFsDiffServClfrMFClfrId)
{
    tDiffServClfrEntry *pDsClfrEntry = NULL, *pClfrList;
    tDsSllNode         *pSllNode = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServClfrMFClfrId < DS_MIN_MFCLFR_ID) ||
        (i4TestValFsDiffServClfrMFClfrId > DS_MAX_MFCLFR_ID))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Need to verify if the same classifier is used twice */
    pDsClfrEntry = DsGetClfrEntry (i4FsDiffServClfrId);
    if (pDsClfrEntry != NULL)

    {
        pSllNode = DS_SLL_FIRST (&DS_CLFR_LIST);
        while (pSllNode != NULL)

        {
            pClfrList = (tDiffServClfrEntry *) pSllNode;
            if (i4TestValFsDiffServClfrMFClfrId == pClfrList->i4DsClfrMFClfrId)

            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            pSllNode = DS_SLL_NEXT (&DS_CLFR_LIST, pSllNode);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrInProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrInProActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrInProActionId (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDiffServClfrId,
                                      INT4 i4TestValFsDiffServClfrInProActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServClfrInProActionId < DS_MIN_IN_PROACT_ID) ||
        (i4TestValFsDiffServClfrInProActionId > DS_MAX_IN_PROACT_ID))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrOutProActionId
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrOutProActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrOutProActionId (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDiffServClfrId,
                                       INT4
                                       i4TestValFsDiffServClfrOutProActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServClfrOutProActionId < DS_MIN_OUT_PROACT_ID) ||
        (i4TestValFsDiffServClfrOutProActionId > DS_MAX_OUT_PROACT_ID))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServClfrStatus
 Input       :  The Indices
                FsDiffServClfrId

                The Object
                testValFsDiffServClfrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServClfrStatus (UINT4 *pu4ErrorCode, INT4 i4FsDiffServClfrId,
                               INT4 i4TestValFsDiffServClfrStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_CLFR_ID_VALID (i4FsDiffServClfrId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServClfrStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServClfrStatus > DS_DESTROY))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServClfrStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_CLFR_LIST)) == DS_CLFR_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_POLICY_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServInProfileActionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServInProfileActionTable (INT4
                                                        i4FsDiffServInProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (DsValidateInProfileActionId (i4FsDiffServInProfileActionId) !=
        DS_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServInProfileActionTable (INT4
                                                *pi4FsDiffServInProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetFirstValidInProfileActionId
         (pi4FsDiffServInProfileActionId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
                nextFsDiffServInProfileActionId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServInProfileActionTable (INT4
                                               i4FsDiffServInProfileActionId,
                                               INT4
                                               *pi4NextFsDiffServInProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (i4FsDiffServInProfileActionId < 0)

    {
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetNextValidInProfileActionId
         (i4FsDiffServInProfileActionId,
          pi4NextFsDiffServInProfileActionId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionFlag
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionFlag (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     *pu4RetValFsDiffServInProfileActionFlag)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        *pu4RetValFsDiffServInProfileActionFlag =
            pDsInProActEntry->u4DsInProfileActionFlag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionNewPrio
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionNewPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionNewPrio (INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        *pu4RetValFsDiffServInProfileActionNewPrio)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        *pu4RetValFsDiffServInProfileActionNewPrio =
            pDsInProActEntry->u4DsInProfileActionNewPrio;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionIpTOS
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionIpTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionIpTOS (INT4 i4FsDiffServInProfileActionId,
                                      UINT4
                                      *pu4RetValFsDiffServInProfileActionIpTOS)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        *pu4RetValFsDiffServInProfileActionIpTOS =
            pDsInProActEntry->u4DsInProfileActionIpTOS;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionPort
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionPort (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     *pu4RetValFsDiffServInProfileActionPort)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        *pu4RetValFsDiffServInProfileActionPort =
            pDsInProActEntry->u4DsInProfileActionPort;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionDscp
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionDscp (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     *pu4RetValFsDiffServInProfileActionDscp)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        *pu4RetValFsDiffServInProfileActionDscp =
            pDsInProActEntry->u4DsInProfileActionDscp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServInProfileActionStatus
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                retValFsDiffServInProfileActionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServInProfileActionStatus (INT4 i4FsDiffServInProfileActionId,
                                       INT4
                                       *pi4RetValFsDiffServInProfileActionStatus)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        *pi4RetValFsDiffServInProfileActionStatus =
            pDsInProActEntry->u1DsInProfileActionStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionFlag
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionFlag (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     u4SetValFsDiffServInProfileActionFlag)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))

    {
        pDsInProActEntry->u4DsInProfileActionFlag =
            u4SetValFsDiffServInProfileActionFlag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionNewPrio
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionNewPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionNewPrio (INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4SetValFsDiffServInProfileActionNewPrio)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))

    {
        pDsInProActEntry->u4DsInProfileActionNewPrio =
            u4SetValFsDiffServInProfileActionNewPrio;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |= FS_DS_ACTN_INSERT_PRIO;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionIpTOS
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionIpTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionIpTOS (INT4 i4FsDiffServInProfileActionId,
                                      UINT4
                                      u4SetValFsDiffServInProfileActionIpTOS)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))

    {
        pDsInProActEntry->u4DsInProfileActionIpTOS =
            u4SetValFsDiffServInProfileActionIpTOS;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |= FS_DS_ACTN_INSERT_TOSP;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionPort
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionPort (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     u4SetValFsDiffServInProfileActionPort)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))

    {
        pDsInProActEntry->u4DsInProfileActionPort =
            u4SetValFsDiffServInProfileActionPort;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |=
            FS_DS_ACTN_SET_OUT_PORT_UCAST;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionDscp
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionDscp (INT4 i4FsDiffServInProfileActionId,
                                     UINT4
                                     u4SetValFsDiffServInProfileActionDscp)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if ((pDsInProActEntry != NULL)
        && (pDsInProActEntry->u1DsInProfileActionStatus != DS_ACTIVE))

    {
        pDsInProActEntry->u4DsInProfileActionDscp =
            u4SetValFsDiffServInProfileActionDscp;

        /* set the flag for the action */
        pDsInProActEntry->u4DsInProfileActionFlag = 0;
        pDsInProActEntry->u4DsInProfileActionFlag |= FS_DS_ACTN_INSERT_DSCP;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServInProfileActionStatus
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                setValFsDiffServInProfileActionStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServInProfileActionStatus (INT4 i4FsDiffServInProfileActionId,
                                       INT4
                                       i4SetValFsDiffServInProfileActionStatus)
{
    tDiffServInProfileActionEntry *pDsInProActEntry = NULL;
    pDsInProActEntry =
        DsGetInProfileActionEntry (i4FsDiffServInProfileActionId);
    if (pDsInProActEntry != NULL)

    {
        if (pDsInProActEntry->u1DsInProfileActionStatus ==
            (UINT1) i4SetValFsDiffServInProfileActionStatus)

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServInProfileActionStatus == DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }

    else

    {

        /* This Dath Path Entry is not there in the Database */
        if (i4SetValFsDiffServInProfileActionStatus != DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValFsDiffServInProfileActionStatus)

    {
        case DS_CREATE_AND_WAIT:
            if (DS_INPROACTENTRY_ALLOC_MEM_BLOCK (pDsInProActEntry) == NULL)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free In Profile Action Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsInProActEntry, 0,
                       sizeof (tDiffServInProfileActionEntry));
            pDsInProActEntry->i4DsInProfileActionId =
                i4FsDiffServInProfileActionId;
            DS_SLL_ADD (&(DS_INPROACT_LIST), &(pDsInProActEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsInProActEntry->u1DsInProfileActionStatus = DS_NOT_READY;
            break;
        case DS_NOT_IN_SERVICE:
            if (pDsInProActEntry->u1DsInProfileActionStatus == DS_ACTIVE)

            {

                /* Check if any active classifier has this In Profile Action */
                if (DsCheckInProfileActionDependency
                    (pDsInProActEntry->i4DsInProfileActionId) != DS_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
                pDsInProActEntry->u1DsInProfileActionStatus = DS_NOT_IN_SERVICE;
            }

            else

            {
                return SNMP_FAILURE;
            }
            break;
        case DS_ACTIVE:

            /* call the function to qualify the In Profile Action Entry data */
            if (DsQualifyInProfileActionData (pDsInProActEntry) != DS_SUCCESS)

            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)

            {
                return SNMP_FAILURE;
            }
            if (pDsInProActEntry->u1DsInProfileActionStatus !=
                DS_NOT_IN_SERVICE)

            {
                return SNMP_FAILURE;
            }
            pDsInProActEntry->u1DsInProfileActionStatus = DS_ACTIVE;
            break;
        case DS_DESTROY:
            if (pDsInProActEntry->u1DsInProfileActionStatus == DS_ACTIVE)

            {

                /* Check if any active classifier has this In Profile Action */
                if (DsCheckInProfileActionDependency
                    (pDsInProActEntry->i4DsInProfileActionId) != DS_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_INPROACT_LIST),
                           &(pDsInProActEntry->DsNextNode));

            /* Delete the In Profile Action Entry from the Software */
            if (DS_INPROACTENTRY_FREE_MEM_BLOCK (pDsInProActEntry) !=
                DS_MEM_SUCCESS)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No In Profile Action Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionFlag
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionFlag (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4TestValFsDiffServInProfileActionFlag)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServInProfileActionFlag | DS_INPROFILEACTION_MASK) ==
        DS_INPROFILEACTION_MASK)

    {

        /* check for the contradictory flags here */
        if ((u4TestValFsDiffServInProfileActionFlag &
             FS_DS_ACTN_DO_NOT_SWITCH)
            && (u4TestValFsDiffServInProfileActionFlag & FS_DS_ACTN_DO_SWITCH))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((u4TestValFsDiffServInProfileActionFlag & FS_DS_ACTN_INSERT_PRIO)
            && (u4TestValFsDiffServInProfileActionFlag &
                FS_DS_ACTN_INSERT_PRIO_FROM_TOSP))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((u4TestValFsDiffServInProfileActionFlag & FS_DS_ACTN_INSERT_TOSP)
            && (u4TestValFsDiffServInProfileActionFlag &
                FS_DS_ACTN_INSERT_TOSP_FROM_PRIO))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionNewPrio
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionNewPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionNewPrio (UINT4 *pu4ErrorCode,
                                           INT4 i4FsDiffServInProfileActionId,
                                           UINT4
                                           u4TestValFsDiffServInProfileActionNewPrio)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServInProfileActionNewPrio > DS_MAX_PRIO_VALUE)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionIpTOS
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionIpTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionIpTOS (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDiffServInProfileActionId,
                                         UINT4
                                         u4TestValFsDiffServInProfileActionIpTOS)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServInProfileActionIpTOS > DS_MAX_PRECEDENCE)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionPort
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionPort (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4TestValFsDiffServInProfileActionPort)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServInProfileActionPort < DS_MAX_NO_PORTS))

    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionDscp
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionDscp (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServInProfileActionId,
                                        UINT4
                                        u4TestValFsDiffServInProfileActionDscp)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServInProfileActionDscp <= DS_MAX_DSCP_VALUE)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServInProfileActionStatus
 Input       :  The Indices
                FsDiffServInProfileActionId

                The Object
                testValFsDiffServInProfileActionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServInProfileActionStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsDiffServInProfileActionId,
                                          INT4
                                          i4TestValFsDiffServInProfileActionStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServInProfileActionStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServInProfileActionStatus > DS_DESTROY))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_INPROACT_ID_VALID (i4FsDiffServInProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServInProfileActionStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_INPROACT_LIST)) == DS_INPROACT_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_INPROF_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServOutProfileActionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServOutProfileActionTable
 Input       :  The Indices
                FsDiffServOutProfileActionId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServOutProfileActionTable (INT4
                                                         i4FsDiffServOutProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (DsValidateOutProfileActionId (i4FsDiffServOutProfileActionId) !=
        DS_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServOutProfileActionTable
 Input       :  The Indices
                FsDiffServOutProfileActionId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServOutProfileActionTable (INT4
                                                 *pi4FsDiffServOutProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetFirstValidOutProfileActionId
         (pi4FsDiffServOutProfileActionId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServOutProfileActionTable
 Input       :  The Indices
                FsDiffServOutProfileActionId
                nextFsDiffServOutProfileActionId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServOutProfileActionTable (INT4
                                                i4FsDiffServOutProfileActionId,
                                                INT4
                                                *pi4NextFsDiffServOutProfileActionId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (i4FsDiffServOutProfileActionId < 0)

    {
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetNextValidOutProfileActionId
         (i4FsDiffServOutProfileActionId,
          pi4NextFsDiffServOutProfileActionId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServOutProfileActionFlag
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                retValFsDiffServOutProfileActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServOutProfileActionFlag (INT4 i4FsDiffServOutProfileActionId,
                                      UINT4
                                      *pu4RetValFsDiffServOutProfileActionFlag)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if (pDsOutProActEntry != NULL)

    {
        *pu4RetValFsDiffServOutProfileActionFlag =
            pDsOutProActEntry->u4DsOutProfileActionFlag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServOutProfileActionDscp
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                retValFsDiffServOutProfileActionDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServOutProfileActionDscp (INT4 i4FsDiffServOutProfileActionId,
                                      UINT4
                                      *pu4RetValFsDiffServOutProfileActionDscp)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if (pDsOutProActEntry != NULL)

    {
        *pu4RetValFsDiffServOutProfileActionDscp =
            pDsOutProActEntry->u4DsOutProfileActionDscp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServOutProfileActionMID
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                retValFsDiffServOutProfileActionMID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServOutProfileActionMID (INT4 i4FsDiffServOutProfileActionId,
                                     INT4
                                     *pi4RetValFsDiffServOutProfileActionMID)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if (pDsOutProActEntry != NULL)

    {
        *pi4RetValFsDiffServOutProfileActionMID =
            pDsOutProActEntry->i4DsOutProfileActionMID;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServOutProfileActionStatus
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                retValFsDiffServOutProfileActionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServOutProfileActionStatus (INT4 i4FsDiffServOutProfileActionId,
                                        INT4
                                        *pi4RetValFsDiffServOutProfileActionStatus)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if (pDsOutProActEntry != NULL)

    {
        *pi4RetValFsDiffServOutProfileActionStatus =
            pDsOutProActEntry->u1DsOutProfileActionStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServOutProfileActionFlag
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                setValFsDiffServOutProfileActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServOutProfileActionFlag (INT4 i4FsDiffServOutProfileActionId,
                                      UINT4
                                      u4SetValFsDiffServOutProfileActionFlag)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if ((pDsOutProActEntry != NULL)
        && (pDsOutProActEntry->u1DsOutProfileActionStatus != DS_ACTIVE))

    {
        pDsOutProActEntry->u4DsOutProfileActionFlag =
            u4SetValFsDiffServOutProfileActionFlag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServOutProfileActionDscp
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                setValFsDiffServOutProfileActionDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServOutProfileActionDscp (INT4 i4FsDiffServOutProfileActionId,
                                      UINT4
                                      u4SetValFsDiffServOutProfileActionDscp)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if ((pDsOutProActEntry != NULL)
        && (pDsOutProActEntry->u1DsOutProfileActionStatus != DS_ACTIVE))

    {
        pDsOutProActEntry->u4DsOutProfileActionDscp =
            u4SetValFsDiffServOutProfileActionDscp;

        /* set the flag */
        pDsOutProActEntry->u4DsOutProfileActionFlag = 0;
        pDsOutProActEntry->u4DsOutProfileActionFlag |=
            FS_DS_OUT_ACTN_INSERT_DSCP;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServOutProfileActionMID
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                setValFsDiffServOutProfileActionMID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServOutProfileActionMID (INT4 i4FsDiffServOutProfileActionId,
                                     INT4 i4SetValFsDiffServOutProfileActionMID)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if ((pDsOutProActEntry != NULL)
        && (pDsOutProActEntry->u1DsOutProfileActionStatus != DS_ACTIVE))

    {
        pDsOutProActEntry->i4DsOutProfileActionMID =
            i4SetValFsDiffServOutProfileActionMID;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServOutProfileActionStatus
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                setValFsDiffServOutProfileActionStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServOutProfileActionStatus (INT4 i4FsDiffServOutProfileActionId,
                                        INT4
                                        i4SetValFsDiffServOutProfileActionStatus)
{
    tDiffServOutProfileActionEntry *pDsOutProActEntry = NULL;
    pDsOutProActEntry =
        DsGetOutProfileActionEntry (i4FsDiffServOutProfileActionId);
    if (pDsOutProActEntry != NULL)

    {
        if (pDsOutProActEntry->u1DsOutProfileActionStatus ==
            (UINT1) i4SetValFsDiffServOutProfileActionStatus)

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServOutProfileActionStatus == DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }

    else

    {

        /* This Dath Path Entry is not there in the Database */
        if (i4SetValFsDiffServOutProfileActionStatus != DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValFsDiffServOutProfileActionStatus)

    {
        case DS_CREATE_AND_WAIT:
            if (DS_OUTPROACTENTRY_ALLOC_MEM_BLOCK (pDsOutProActEntry) == NULL)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Out Profile Action Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsOutProActEntry, 0,
                       sizeof (tDiffServOutProfileActionEntry));
            pDsOutProActEntry->i4DsOutProfileActionId =
                i4FsDiffServOutProfileActionId;
            pDsOutProActEntry->u4DsOutProfileActionFlag |=
                FS_DS_OUT_ACTN_DO_NOT_SWITCH;
            DS_SLL_ADD (&(DS_OUTPROACT_LIST), &(pDsOutProActEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsOutProActEntry->u1DsOutProfileActionStatus = DS_NOT_READY;
            break;
        case DS_NOT_IN_SERVICE:
            if (pDsOutProActEntry->u1DsOutProfileActionStatus == DS_ACTIVE)

            {

                /* Check if any active classifier has this Out Profile Action */
                if (DsCheckOutProfileActionDependency
                    (pDsOutProActEntry->i4DsOutProfileActionId) != DS_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
                pDsOutProActEntry->u1DsOutProfileActionStatus =
                    DS_NOT_IN_SERVICE;
            }

            else

            {
                return SNMP_FAILURE;
            }
            break;
        case DS_ACTIVE:

            /* call the function to qualify the Out Profile Action Entry data */
            if (DsQualifyOutProfileActionData (pDsOutProActEntry) != DS_SUCCESS)

            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)

            {
                return SNMP_FAILURE;
            }
            if (pDsOutProActEntry->u1DsOutProfileActionStatus !=
                DS_NOT_IN_SERVICE)

            {
                return SNMP_FAILURE;
            }
            pDsOutProActEntry->u1DsOutProfileActionStatus = DS_ACTIVE;
            break;
        case DS_DESTROY:
            if (pDsOutProActEntry->u1DsOutProfileActionStatus == DS_ACTIVE)

            {

                /* Check if any active classifier has this Out Profile Action */
                if (DsCheckOutProfileActionDependency
                    (pDsOutProActEntry->i4DsOutProfileActionId) != DS_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_OUTPROACT_LIST),
                           &(pDsOutProActEntry->DsNextNode));

            /* Delete the Out Profile Action Entry from the Software */
            if (DS_OUTPROACTENTRY_FREE_MEM_BLOCK (pDsOutProActEntry) !=
                DS_MEM_SUCCESS)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Out Profile Action Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServOutProfileActionFlag
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                testValFsDiffServOutProfileActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServOutProfileActionFlag (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDiffServOutProfileActionId,
                                         UINT4
                                         u4TestValFsDiffServOutProfileActionFlag)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_OUTPROACT_ID_VALID (i4FsDiffServOutProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServOutProfileActionFlag | DS_OUTPROFILEACTION_MASK)
        == DS_OUTPROFILEACTION_MASK)

    {

        /* check for the contradictory flags here */
        if ((u4TestValFsDiffServOutProfileActionFlag &
             FS_DS_OUT_ACTN_DO_NOT_SWITCH)
            && (u4TestValFsDiffServOutProfileActionFlag &
                FS_DS_OUT_ACTN_DO_SWITCH))

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServOutProfileActionDscp
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                testValFsDiffServOutProfileActionDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServOutProfileActionDscp (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDiffServOutProfileActionId,
                                         UINT4
                                         u4TestValFsDiffServOutProfileActionDscp)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_OUTPROACT_ID_VALID (i4FsDiffServOutProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDiffServOutProfileActionDscp <= DS_MAX_DSCP_VALUE)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServOutProfileActionMID
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                testValFsDiffServOutProfileActionMID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServOutProfileActionMID (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServOutProfileActionId,
                                        INT4
                                        i4TestValFsDiffServOutProfileActionMID)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_OUTPROACT_ID_VALID (i4FsDiffServOutProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServOutProfileActionMID < DS_MIN_METER_ID) ||
        (i4TestValFsDiffServOutProfileActionMID > DS_MAX_METER_ID))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServOutProfileActionStatus
 Input       :  The Indices
                FsDiffServOutProfileActionId

                The Object
                testValFsDiffServOutProfileActionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServOutProfileActionStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsDiffServOutProfileActionId,
                                           INT4
                                           i4TestValFsDiffServOutProfileActionStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_OUTPROACT_ID_VALID (i4FsDiffServOutProfileActionId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServOutProfileActionStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServOutProfileActionStatus > DS_DESTROY))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServOutProfileActionStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_OUTPROACT_LIST)) == DS_OUTPROACT_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_OUTPROF_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServMeterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServMeterTable
 Input       :  The Indices
                FsDiffServMeterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServMeterTable (INT4 i4FsDiffServMeterId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (DsValidateMeterId (i4FsDiffServMeterId) != DS_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServMeterTable
 Input       :  The Indices
                FsDiffServMeterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServMeterTable (INT4 *pi4FsDiffServMeterId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetFirstValidMeterId (pi4FsDiffServMeterId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServMeterTable
 Input       :  The Indices
                FsDiffServMeterId
                nextFsDiffServMeterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServMeterTable (INT4 i4FsDiffServMeterId,
                                     INT4 *pi4NextFsDiffServMeterId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (i4FsDiffServMeterId < 0)

    {
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetNextValidMeterId
         (i4FsDiffServMeterId, pi4NextFsDiffServMeterId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServMetertokenSize
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                retValFsDiffServMetertokenSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMetertokenSize (INT4 i4FsDiffServMeterId,
                                UINT4 *pu4RetValFsDiffServMetertokenSize)
{
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsMeterEntry = DsGetMeterEntry (i4FsDiffServMeterId);
    if (pDsMeterEntry != NULL)

    {
        *pu4RetValFsDiffServMetertokenSize = pDsMeterEntry->u4DsMetertokenSize;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServMeterRefreshCount
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                retValFsDiffServMeterRefreshCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMeterRefreshCount (INT4 i4FsDiffServMeterId,
                                   UINT4 *pu4RetValFsDiffServMeterRefreshCount)
{
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsMeterEntry = DsGetMeterEntry (i4FsDiffServMeterId);
    if (pDsMeterEntry != NULL)

    {
        *pu4RetValFsDiffServMeterRefreshCount =
            pDsMeterEntry->u4DsMeterRefreshCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServMeterStatus
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                retValFsDiffServMeterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServMeterStatus (INT4 i4FsDiffServMeterId,
                             INT4 *pi4RetValFsDiffServMeterStatus)
{
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsMeterEntry = DsGetMeterEntry (i4FsDiffServMeterId);
    if (pDsMeterEntry != NULL)

    {
        *pi4RetValFsDiffServMeterStatus = pDsMeterEntry->u1DsMeterStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServMetertokenSize
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                setValFsDiffServMetertokenSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMetertokenSize (INT4 i4FsDiffServMeterId,
                                UINT4 u4SetValFsDiffServMetertokenSize)
{
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    pDsMeterEntry = DsGetMeterEntry (i4FsDiffServMeterId);
    if ((pDsMeterEntry != NULL) &&
        (pDsMeterEntry->u1DsMeterStatus != DS_ACTIVE))

    {
        pDsMeterEntry->u4DsMetertokenSize = u4SetValFsDiffServMetertokenSize;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServMeterRefreshCount
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                setValFsDiffServMeterRefreshCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMeterRefreshCount (INT4 i4FsDiffServMeterId,
                                   UINT4 u4SetValFsDiffServMeterRefreshCount)
{
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    pDsMeterEntry = DsGetMeterEntry (i4FsDiffServMeterId);
    if ((pDsMeterEntry != NULL) &&
        (pDsMeterEntry->u1DsMeterStatus != DS_ACTIVE))

    {
        pDsMeterEntry->u4DsMeterRefreshCount =
            u4SetValFsDiffServMeterRefreshCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServMeterStatus
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                setValFsDiffServMeterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServMeterStatus (INT4 i4FsDiffServMeterId,
                             INT4 i4SetValFsDiffServMeterStatus)
{
    tDiffServMeterEntry *pDsMeterEntry = NULL;
    pDsMeterEntry = DsGetMeterEntry (i4FsDiffServMeterId);
    if (pDsMeterEntry != NULL)

    {
        if (pDsMeterEntry->u1DsMeterStatus ==
            (UINT1) i4SetValFsDiffServMeterStatus)

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServMeterStatus == DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }

    else

    {

        /* This Meter Entry is not there in the Database */
        if (i4SetValFsDiffServMeterStatus != DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValFsDiffServMeterStatus)

    {
        case DS_CREATE_AND_WAIT:
            if (DS_METERENTRY_ALLOC_MEM_BLOCK (pDsMeterEntry) == NULL)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Meter Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsMeterEntry, 0, sizeof (tDiffServMeterEntry));
            pDsMeterEntry->i4DsMeterId = i4FsDiffServMeterId;
            DS_SLL_ADD (&(DS_METER_LIST), &(pDsMeterEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsMeterEntry->u1DsMeterStatus = DS_NOT_READY;
            break;
        case DS_NOT_IN_SERVICE:
            if (pDsMeterEntry->u1DsMeterStatus == DS_ACTIVE)

            {

                /* Check if any active Out Profile Action has this Meter */
                if (DsCheckMeterDependency
                    (pDsMeterEntry->i4DsMeterId) != DS_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
                pDsMeterEntry->u1DsMeterStatus = DS_NOT_IN_SERVICE;
            }

            else

            {
                return SNMP_FAILURE;
            }
            break;
        case DS_ACTIVE:

            /* call the function to qualify the Meter Entry data */
            if (DsQualifyMeterData (pDsMeterEntry) != DS_SUCCESS)

            {
                return SNMP_FAILURE;
            }

            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)

            {
                return SNMP_FAILURE;
            }
            if (pDsMeterEntry->u1DsMeterStatus != DS_NOT_IN_SERVICE)

            {
                return SNMP_FAILURE;
            }
            pDsMeterEntry->u1DsMeterStatus = DS_ACTIVE;
            break;
        case DS_DESTROY:
            if (pDsMeterEntry->u1DsMeterStatus == DS_ACTIVE)

            {

                /* Check if any active Out Profile Action has this Meter */
                if (DsCheckMeterDependency
                    (pDsMeterEntry->i4DsMeterId) != DS_SUCCESS)

                {
                    return SNMP_FAILURE;
                }
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_METER_LIST), &(pDsMeterEntry->DsNextNode));

            /* Delete the Meter Entry from the Software */
            if (DS_METERENTRY_FREE_MEM_BLOCK (pDsMeterEntry) != DS_MEM_SUCCESS)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Meter Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMetertokenSize
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                testValFsDiffServMetertokenSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMetertokenSize (UINT4 *pu4ErrorCode,
                                   INT4 i4FsDiffServMeterId,
                                   UINT4 u4TestValFsDiffServMetertokenSize)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_METER_ID_VALID (i4FsDiffServMeterId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServMetertokenSize == FS_DS_METER_512_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_1024_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_2048_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_4096_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_8192_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_16384_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_32768_TOKENS) ||
        (u4TestValFsDiffServMetertokenSize == FS_DS_METER_65536_TOKENS))

    {
        return SNMP_SUCCESS;
    }

    else

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMeterRefreshCount
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                testValFsDiffServMeterRefreshCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMeterRefreshCount (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDiffServMeterId,
                                      UINT4
                                      u4TestValFsDiffServMeterRefreshCount)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServMeterRefreshCount < DS_MIN_REFRESH_COUNT) ||
        (u4TestValFsDiffServMeterRefreshCount > DS_MAX_REFRESH_COUNT))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_METER_ID_VALID (i4FsDiffServMeterId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServMeterStatus
 Input       :  The Indices
                FsDiffServMeterId

                The Object
                testValFsDiffServMeterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServMeterStatus (UINT4 *pu4ErrorCode, INT4 i4FsDiffServMeterId,
                                INT4 i4TestValFsDiffServMeterStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServMeterStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServMeterStatus > DS_DESTROY))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_METER_ID_VALID (i4FsDiffServMeterId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServMeterStatus == DS_CREATE_AND_WAIT) &&
        (DS_SLL_COUNT (&(DS_METER_LIST)) == DS_METER_MEMBLK_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DS_METER_MAX_LIMIT_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServSchedulerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServSchedulerTable
 Input       :  The Indices
                FsDiffServSchedulerId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDiffServSchedulerTable (INT4 i4FsDiffServSchedulerId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be enabled before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (DsValidateSchedulerId (i4FsDiffServSchedulerId) != DS_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    else

    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServSchedulerTable
 Input       :  The Indices
                FsDiffServSchedulerId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDiffServSchedulerTable (INT4 *pi4FsDiffServSchedulerId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetFirstValidSchedulerId (pi4FsDiffServSchedulerId)) ==
        DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServSchedulerTable
 Input       :  The Indices
                FsDiffServSchedulerId
                nextFsDiffServSchedulerId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServSchedulerTable (INT4 i4FsDiffServSchedulerId,
                                         INT4 *pi4NextFsDiffServSchedulerId)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    if (i4FsDiffServSchedulerId < 0)

    {
        return SNMP_FAILURE;
    }
    if ((DsSnmpLowGetNextValidSchedulerId
         (i4FsDiffServSchedulerId, pi4NextFsDiffServSchedulerId)) == DS_SUCCESS)

    {
        return SNMP_SUCCESS;
    }

    else

    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServSchedulerDPId
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                retValFsDiffServSchedulerDPId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServSchedulerDPId (INT4 i4FsDiffServSchedulerId,
                               INT4 *pi4RetValFsDiffServSchedulerDPId)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if (pDsSchedulerEntry != NULL)

    {
        *pi4RetValFsDiffServSchedulerDPId =
            pDsSchedulerEntry->i4DsSchedulerDatapathId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServSchedulerQueueCount
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                retValFsDiffServSchedulerQueueCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServSchedulerQueueCount (INT4 i4FsDiffServSchedulerId,
                                     UINT4
                                     *pu4RetValFsDiffServSchedulerQueueCount)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if (pDsSchedulerEntry != NULL)

    {
        *pu4RetValFsDiffServSchedulerQueueCount =
            pDsSchedulerEntry->u4DsSchedulerQueueCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServSchedulerWeight
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                retValFsDiffServSchedulerWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServSchedulerWeight (INT4 i4FsDiffServSchedulerId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsDiffServSchedulerWeight)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if (pDsSchedulerEntry != NULL)

    {
        pRetValFsDiffServSchedulerWeight->i4_Length =
            DS_MAX_SCHEDULER_WEIGHT_SIZE;
        DS_MEMCPY (pRetValFsDiffServSchedulerWeight->pu1_OctetList,
                   pDsSchedulerEntry->au1DsSchedulerWeight,
                   pRetValFsDiffServSchedulerWeight->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServSchedulerStatus
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                retValFsDiffServSchedulerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServSchedulerStatus (INT4 i4FsDiffServSchedulerId,
                                 INT4 *pi4RetValFsDiffServSchedulerStatus)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        DS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " SNMPSTD: DiffServ should be started before accessing its MIB objects !!\n");
        return SNMP_FAILURE;
    }
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if (pDsSchedulerEntry != NULL)

    {
        *pi4RetValFsDiffServSchedulerStatus =
            pDsSchedulerEntry->u1DsSchedulerStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServSchedulerDPId
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                setValFsDiffServSchedulerDPId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServSchedulerDPId (INT4 i4FsDiffServSchedulerId,
                               INT4 i4SetValFsDiffServSchedulerDPId)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if ((pDsSchedulerEntry != NULL) &&
        (pDsSchedulerEntry->u1DsSchedulerStatus != DS_ACTIVE))

    {
        pDsSchedulerEntry->i4DsSchedulerDatapathId =
            i4SetValFsDiffServSchedulerDPId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServSchedulerQueueCount
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                setValFsDiffServSchedulerQueueCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServSchedulerQueueCount (INT4 i4FsDiffServSchedulerId,
                                     UINT4
                                     u4SetValFsDiffServSchedulerQueueCount)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if ((pDsSchedulerEntry != NULL) &&
        (pDsSchedulerEntry->u1DsSchedulerStatus != DS_ACTIVE))

    {
        pDsSchedulerEntry->u4DsSchedulerQueueCount =
            u4SetValFsDiffServSchedulerQueueCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServSchedulerWeight
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                setValFsDiffServSchedulerWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServSchedulerWeight (INT4 i4FsDiffServSchedulerId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsDiffServSchedulerWeight)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if ((pDsSchedulerEntry != NULL) &&
        (pDsSchedulerEntry->u1DsSchedulerStatus != DS_ACTIVE))

    {
        DS_MEMSET (pDsSchedulerEntry->au1DsSchedulerWeight, 0,
                   DS_MAX_SCHEDULER_WEIGHT_SIZE);
        DS_MEMCPY (pDsSchedulerEntry->au1DsSchedulerWeight,
                   pSetValFsDiffServSchedulerWeight->pu1_OctetList,
                   pSetValFsDiffServSchedulerWeight->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDiffServSchedulerStatus
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                setValFsDiffServSchedulerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServSchedulerStatus (INT4 i4FsDiffServSchedulerId,
                                 INT4 i4SetValFsDiffServSchedulerStatus)
{
    tDiffServSchedulerEntry *pDsSchedulerEntry = NULL;

#ifdef NPAPI_WANTED
    INT4                i4RetVal;

#endif /*  */
    pDsSchedulerEntry = DsGetSchedulerEntry (i4FsDiffServSchedulerId);
    if (pDsSchedulerEntry != NULL)

    {
        if (pDsSchedulerEntry->u1DsSchedulerStatus ==
            (UINT1) i4SetValFsDiffServSchedulerStatus)

        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsDiffServSchedulerStatus == DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }

    else

    {

        /* This Scheduler Entry is not there in the Database */
        if (i4SetValFsDiffServSchedulerStatus != DS_CREATE_AND_WAIT)

        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValFsDiffServSchedulerStatus)

    {
        case DS_CREATE_AND_WAIT:
            if (DS_SCHEDULERENTRY_ALLOC_MEM_BLOCK (pDsSchedulerEntry) == NULL)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Scheduler Entry \n");
                return SNMP_FAILURE;
            }
            DS_MEMSET (pDsSchedulerEntry, 0, sizeof (tDiffServSchedulerEntry));
            pDsSchedulerEntry->i4DsSchedulerId = i4FsDiffServSchedulerId;
            pDsSchedulerEntry->u4DsSchedulerQueueCount = DS_MIN_QUEUE_COUNT;
            DS_SLL_ADD (&(DS_SCHEDULER_LIST), &(pDsSchedulerEntry->DsNextNode));

            /* Moving the Row Status to NOT_READY as mandatory objects are not
             * having required values. */
            pDsSchedulerEntry->u1DsSchedulerStatus = DS_NOT_READY;
            break;
        case DS_NOT_IN_SERVICE:
            if (pDsSchedulerEntry->u1DsSchedulerStatus == DS_ACTIVE)

            {
                pDsSchedulerEntry->u1DsSchedulerStatus = DS_NOT_IN_SERVICE;
            }

            else

            {
                return SNMP_FAILURE;
            }
            break;
        case DS_ACTIVE:

            /* call the function to qualify the scheduler entry data */
            if (DsQualifySchedulerData (pDsSchedulerEntry) != DS_SUCCESS)

            {
                return SNMP_FAILURE;
            }

            /* Validate datapath */
            /* Check for the DiffServ Status */
            if (DS_STATUS == DS_DISABLE)

            {
                return SNMP_FAILURE;
            }
            if (pDsSchedulerEntry->u1DsSchedulerStatus != DS_NOT_IN_SERVICE)

            {
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            /* Add the validated schduler to the Data Path in the hardware */
            i4RetVal = DsHwSchedulerAdd (pDsSchedulerEntry);
            if (i4RetVal != FNP_SUCCESS)

            {
                return SNMP_FAILURE;
            }

#endif /*  */
            pDsSchedulerEntry->u1DsSchedulerStatus = DS_ACTIVE;
            break;
        case DS_DESTROY:
            if (pDsSchedulerEntry->u1DsSchedulerStatus == DS_ACTIVE)

            {

                /* Since the driver does not support the deletion of scheduler
                 * from the hardware, so the scheduler would be deleted from the
                 * hardware only when the data path delete would be called.*/
                return SNMP_FAILURE;
            }

            /* Deleting the node from the list */
            DS_SLL_DELETE (&(DS_SCHEDULER_LIST),
                           &(pDsSchedulerEntry->DsNextNode));

            /* Delete the Entry from the Software */
            if (DS_SCHEDULERENTRY_FREE_MEM_BLOCK (pDsSchedulerEntry) !=
                DS_MEM_SUCCESS)

            {
                DS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                        "No Free Scheduler Entry \n");
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServSchedulerDPId
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                testValFsDiffServSchedulerDPId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServSchedulerDPId (UINT4 *pu4ErrorCode,
                                  INT4 i4FsDiffServSchedulerId,
                                  INT4 i4TestValFsDiffServSchedulerDPId)
{
    UNUSED_PARAM (i4TestValFsDiffServSchedulerDPId);
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_SCHEDULER_ID_VALID (i4FsDiffServSchedulerId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServSchedulerQueueCount
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                testValFsDiffServSchedulerQueueCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServSchedulerQueueCount (UINT4 *pu4ErrorCode,
                                        INT4 i4FsDiffServSchedulerId,
                                        UINT4
                                        u4TestValFsDiffServSchedulerQueueCount)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_SCHEDULER_ID_VALID (i4FsDiffServSchedulerId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDiffServSchedulerQueueCount < DS_MIN_QUEUE_COUNT) ||
        (u4TestValFsDiffServSchedulerQueueCount > DS_MAX_QUEUE_COUNT))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServSchedulerWeight
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                testValFsDiffServSchedulerWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServSchedulerWeight (UINT4 *pu4ErrorCode,
                                    INT4 i4FsDiffServSchedulerId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsDiffServSchedulerWeight)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_SCHEDULER_ID_VALID (i4FsDiffServSchedulerId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((pTestValFsDiffServSchedulerWeight->i4_Length <
         DS_MIN_SCHEDULER_WEIGHT_SIZE)
        || (pTestValFsDiffServSchedulerWeight->i4_Length >
            DS_MAX_SCHEDULER_WEIGHT_SIZE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServSchedulerStatus
 Input       :  The Indices
                FsDiffServSchedulerId

                The Object
                testValFsDiffServSchedulerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServSchedulerStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsDiffServSchedulerId,
                                    INT4 i4TestValFsDiffServSchedulerStatus)
{
    if (gDsGlobalInfo.DsSystemControl != DS_START)

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (DS_IS_SCHEDULER_ID_VALID (i4FsDiffServSchedulerId) == DS_FALSE)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServSchedulerStatus < DS_ACTIVE) ||
        (i4TestValFsDiffServSchedulerStatus > DS_DESTROY))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrValidateDiffServInProfileActionEntry
 *  Description     : This function validates the DiffServInProfileActionEntry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateDiffServInProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
#define   INPROFILE_ACTION_FLAG_INDEX     2
    UINT4               u4Flag;
#ifdef SNMP_2_WANTED
    UINT4               u4Error;
#endif
    tSNMP_OID_TYPE      FlagOid;
    UINT4               au4OidList[30];
    UINT4               u4FlagIndex = 0;
    tSNMP_MULTI_DATA_TYPE Data;
    tSnmpIndex          snmpIndex;
    tSNMP_MULTI_DATA_TYPE Index;

    /* Static flag mapping for the MIB objects */
    UINT4               au4ActionFlag[] = { 0, 0, 0, FS_DS_ACTN_INSERT_PRIO,
/*4*/ FS_DS_ACTN_INSERT_TOSP,
/* 5*/ FS_DS_ACTN_SET_OUT_PORT_UCAST,
/*6*/ FS_DS_ACTN_INSERT_DSCP,
/*7*/ 0
    };

    if (pOid->u4_Length == 13)
    {
        /* row status is saved twice, the second
         * save is as per the MIB (table walk), the first
         * save is implementation specific
         * and the OID is passed has the column identifier
         * unlike for other cases. In this case OID len is 13
         */
        return (1 /* MSR_SAVE */ );
    }
    /* Get the OID for Action Flag */
    FlagOid.pu4_OidList = au4OidList;
    DS_MEMSET (&au4OidList[0], 0, sizeof (au4OidList));
    DS_MEMCPY (&au4OidList[0], pOid->pu4_OidList,
               sizeof (UINT4) * pOid->u4_Length);
    DS_MEMCPY (&au4OidList[pOid->u4_Length], pInstance->pu4_OidList,
               sizeof (UINT4) * pInstance->u4_Length);
    FlagOid.u4_Length = pOid->u4_Length + pInstance->u4_Length;
    au4OidList[FlagOid.u4_Length - 2] = INPROFILE_ACTION_FLAG_INDEX;

    /* Get the InProfile Action Flag */
    DS_MEMSET (&Index, 0, sizeof (Index));
    snmpIndex.u4No = 0;
    snmpIndex.pIndex = &Index;

    /* The Flag index here is actually the MIB column identifier 
     * In the case of row status save initially the 
     * instance pu4_OidList[0] has the instance number 
     * and in other case it has the column identifier
     * which serves as the index into the au4ActionFlag array.
     */

    u4FlagIndex = pInstance->pu4_OidList[0];

#ifdef SNMP_2_WANTED
    SNMPGet (FlagOid, &Data, &u4Error, &snmpIndex, SNMP_DEFAULT_CONTEXT);
#endif

    u4Flag = Data.u4_ULongValue;

    /* This validation is required so as that only
     * the MIB variables that are used as per the action
     * flag are saved. example if the ActionFlag indicates
     * the action to be policed_dscp, then only the policed dscp
     * value needs to be stored, the policed_precedence value
     * or MIB variable will not be stored for the given row 
     */

    if (au4ActionFlag[u4FlagIndex] && !(u4Flag & au4ActionFlag[u4FlagIndex]))
    {
        return (2 /*MSR_SKIP */ );
    }

    UNUSED_PARAM (pData);
    return (1 /*MSR_SAVE */ );
}                                /* MsrValidateDiffServInProfileActionEntry */

/************************************************************************
 *  Function Name   : MsrValidateDiffServOutProfileActionEntry
 *  Description     : This function validates the DiffServOutProfileActionEntry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateDiffServOutProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
#define   OUTPROFILE_ACTION_FLAG_INDEX     2
    UINT4               u4Flag;
#ifdef SNMP_2_WANTED
    UINT4               u4Error;
#endif
    tSNMP_OID_TYPE      FlagOid;
    UINT4               au4OidList[30];
    UINT4               u4FlagIndex = 0;
    tSNMP_MULTI_DATA_TYPE Data;
    tSnmpIndex          snmpIndex;
    tSNMP_MULTI_DATA_TYPE Index;

    /* Static flag mapping for the MIB objects */
    UINT4               au4ActionFlag[] = { 0, 0, 0, FS_DS_OUT_ACTN_INSERT_DSCP,
/*4*/ 0,
/* 5*/ 0
    };

    if (pOid->u4_Length == 13 /* length when OID has row status */ )
    {
        /* row status is saved twice, the second
         * save is as per the MIB (table walk), the first
         * save is implementation specific
         * and the OID passed has the column identifier
         * unlike for other cases. In this case OID len is 13
         */
        return (1 /* MSR_SAVE */ );
    }
    /* Get the OID for Action Flag */
    FlagOid.pu4_OidList = au4OidList;
    DS_MEMSET (&au4OidList[0], 0, sizeof (au4OidList));
    DS_MEMCPY (&au4OidList[0], pOid->pu4_OidList,
               sizeof (UINT4) * pOid->u4_Length);
    DS_MEMCPY (&au4OidList[pOid->u4_Length], pInstance->pu4_OidList,
               sizeof (UINT4) * pInstance->u4_Length);
    FlagOid.u4_Length = pOid->u4_Length + pInstance->u4_Length;
    au4OidList[FlagOid.u4_Length - 2] = OUTPROFILE_ACTION_FLAG_INDEX;

    /* Get the OutProfile Action Flag */
    DS_MEMSET (&Index, 0, sizeof (Index));
    snmpIndex.u4No = 0;
    snmpIndex.pIndex = &Index;

    /* The Flag index here is actually the MIB column identifier 
     * In the case of row status save initially the 
     * instance pu4_OidList[0] has the instance number 
     * and in other case it has the column identifier
     * which serves as the index into the au4ActionFlag array.
     */

    u4FlagIndex = pInstance->pu4_OidList[0];

#ifdef SNMP_2_WANTED
    SNMPGet (FlagOid, &Data, &u4Error, &snmpIndex, SNMP_DEFAULT_CONTEXT);
#endif

    u4Flag = Data.u4_ULongValue;

    /* This validation is required so as that only
     * the MIB variables that are used as per the action
     * flag are saved. example if the ActionFlag indicates
     * the action to be policed_dscp, then only the policed dscp
     * value needs to be stored, the policed_precedence value
     * or MIB variable will not be stored for the given row 
     */

    if (au4ActionFlag[u4FlagIndex] && !(u4Flag & au4ActionFlag[u4FlagIndex]))
    {
        return (2 /*MSR_SKIP */ );
    }

    UNUSED_PARAM (pData);
    return (1 /*MSR_SAVE */ );
}                                /* MsrValidateDiffServOutProfileActionEntry */

/****************************************************************************
 *  Function Name   : DsWebnmGetPolicyMapEntry
 *  Description     : This function fetches the DiffServPolicyMapEntries     
 *                    and fills it in a pointer based on the CLFR Flags set 
 *  Input           : i4PolicyMapId - PolicyMap entry index to create.
 *                    pClfrData     - Policy Map entry ptr to be returned to 
 *                                    WEB
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE               
 *  NOTE            : Diffserver Protocol Lock (DFS_LOCK ()) MUST be taken
 *                    before calling this function.
 *****************************************************************************/
INT4
DsWebnmGetPolicyMapEntry (INT4 i4PolicyMapId, tDiffServWebClfrData * pClfrData)
{
    UINT4               u4Value;
    INT4                i4OutCome = 0;
    INT4                i4InProfActionId, i4OutProfActionId;

    MEMSET (pClfrData, 0, sizeof (tDiffServWebClfrData));

    i4OutCome = nmhGetFsDiffServClfrInProActionId (i4PolicyMapId,
                                                   &i4InProfActionId);
    if ((i4OutCome != SNMP_FAILURE) && (i4InProfActionId != 0))
    {
        if (nmhGetFsDiffServInProfileActionFlag
            (i4InProfActionId, &u4Value) == SNMP_SUCCESS)
        {
            if (u4Value & FS_DS_ACTN_INSERT_TOSP)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType,
                         "Policed-Precedence");
                nmhGetFsDiffServInProfileActionIpTOS (i4InProfActionId,
                                                      &u4Value);
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, "%u",
                         u4Value);
            }
            else if (u4Value & FS_DS_ACTN_INSERT_DSCP)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType,
                         "Policed-Dscp");
                nmhGetFsDiffServInProfileActionDscp (i4InProfActionId,
                                                     &u4Value);
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, "%u",
                         u4Value);
            }
            else if (u4Value & FS_DS_ACTN_INSERT_PRIO)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType,
                         "Policed-Priority");
                nmhGetFsDiffServInProfileActionNewPrio (i4InProfActionId,
                                                        &u4Value);

                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, "%d",
                         u4Value);
            }
            else
            {
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType, "-");
                SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, " ");
            }
        }
    }
    else
    {
        SPRINTF ((char *) pClfrData->au1DsClfrInProfActionType, "-");
        SPRINTF ((char *) pClfrData->au1DsClfrInProfActionValue, " ");
    }

    i4OutCome = nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId,
                                                    &i4OutProfActionId);
    if ((i4OutCome != SNMP_FAILURE) && (i4OutProfActionId != 0))
    {
        if (nmhGetFsDiffServOutProfileActionFlag
            (i4OutProfActionId, &u4Value) == SNMP_SUCCESS)
        {
            if (u4Value & FS_DS_OUT_ACTN_DO_NOT_SWITCH)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionType,
                         "Drop");
                SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionValue, " ");
            }
            else if (u4Value & FS_DS_OUT_ACTN_INSERT_DSCP)
            {
                SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionType,
                         "Policed-Dscp");
                nmhGetFsDiffServOutProfileActionDscp (i4OutProfActionId,
                                                      &u4Value);
                SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionValue, "%d",
                         u4Value);
            }
            else
            {
                SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionType, "-");
                SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionValue, " ");
            }
        }
    }
    else
    {
        SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionType, "-");
        SPRINTF ((char *) pClfrData->au1DsClfrOutProfActionValue, " ");
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function Name   : DsWebnmSetPolicyMapEntry
 *  Description     : This function sets the DiffServPolicyMapEntries     
 *                    based on the Flag value passed
 *  Input           : pClfrEntry  - PolicyMap data entry values.
 *                    pu1ErrString - Error string returned to WEB in case of 
 *                                   error.
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE               
 *  NOTE            : Diffserver Protocol Lock (DFS_LOCK ()) MUST be taken
 *                    before calling this function.
 *****************************************************************************/
INT4
DsWebnmSetPolicyMapEntry (tDiffServWebSetClfrEntry * pClfrEntry,
                          UINT1 u1RowStatus, UINT1 *pu1ErrString)
{
    UINT4               u4ErrorCode;
    UINT4               u4OutProfActFlag;
    UINT1               u1Apply = 0;

    if (u1RowStatus == ISS_NOT_IN_SERVICE)
    {
        u1Apply = 1;
    }
    /* Set Routine */
    /* Policy-Map Creation */
    if (nmhSetFsDiffServClfrStatus
        (pClfrEntry->i4DsClfrId, u1RowStatus) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Policy-Map ID");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDiffServClfrMFClfrId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrMFClfrId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Class Map ID.");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDiffServClfrInProActionId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrInProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid In-Profile Action ID.");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDiffServClfrOutProActionId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrOutProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Out-Profile Action ID.");
        return SNMP_FAILURE;
    }

    /* In-Profile Action */
    if (nmhSetFsDiffServInProfileActionStatus
        (pClfrEntry->i4DsClfrInProActionId, u1RowStatus) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid In-Profile Action Id");
        if (u1Apply != 1)
        {
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    else
    {
        if (pClfrEntry->u4InProfActType == 1)
        {
            if (nmhTestv2FsDiffServInProfileActionDscp
                (&u4ErrorCode, pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InDscp) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid In-Profile Dscp Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServInProfileActionDscp
                (pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InDscp) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid In-Profile Dscp Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
        }
        else if (pClfrEntry->u4InProfActType == 2)
        {
            if (nmhTestv2FsDiffServInProfileActionIpTOS
                (&u4ErrorCode, pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InPrec) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid In-Profile Precedence Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServInProfileActionIpTOS
                (pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4InPrec) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid In-Profile Precedence Value");
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
        }
        else if (pClfrEntry->u4InProfActType == 3)
        {
            if (nmhTestv2FsDiffServInProfileActionNewPrio
                (&u4ErrorCode, pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4CosValue) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid COS Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServInProfileActionNewPrio
                (pClfrEntry->i4DsClfrInProActionId,
                 pClfrEntry->u4CosValue) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid COS Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
        }
        if (nmhSetFsDiffServInProfileActionStatus
            (pClfrEntry->i4DsClfrInProActionId, DS_ACTIVE) == SNMP_FAILURE)
        {
            STRCPY (pu1ErrString, "In-Profile Action Failure in Activation");
            if (u1Apply != 1)
            {
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
            }
            return SNMP_FAILURE;
        }
    }

    /* Out-Profile Action */
    if (nmhSetFsDiffServOutProfileActionStatus
        (pClfrEntry->i4DsClfrOutProActionId, u1RowStatus) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Out-Profile Action Id");
        if (u1Apply != 1)
        {
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    else
    {
        if (pClfrEntry->u4OutProfActType == 1)
        {
            /* Initialise the out-profile action flag to zero */
            u4OutProfActFlag = 0;
            if (nmhGetFsDiffServOutProfileActionFlag
                (pClfrEntry->i4DsClfrOutProActionId,
                 &(u4OutProfActFlag)) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid Out-Profile Action");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus
                        (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            else
            {
                u4OutProfActFlag |= FS_DS_OUT_ACTN_DO_NOT_SWITCH;
            }

            if (nmhSetFsDiffServOutProfileActionFlag
                (pClfrEntry->i4DsClfrOutProActionId,
                 u4OutProfActFlag) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid Out-Profile Action");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus
                        (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
        }
        else if (pClfrEntry->u4OutProfActType == 2)
        {
            if (nmhTestv2FsDiffServOutProfileActionDscp
                (&u4ErrorCode, pClfrEntry->i4DsClfrOutProActionId,
                 pClfrEntry->u4OutDscp) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid Out-Profile Dscp Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus
                        (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServOutProfileActionDscp
                (pClfrEntry->i4DsClfrOutProActionId,
                 pClfrEntry->u4OutDscp) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid Out-Profile Dscp Value");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus
                        (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
        }

        /* Meter Information */
        if (pClfrEntry->u4TrafficRate > 0)
        {
            if (nmhGetFsDiffServMeterStatus
                (pClfrEntry->i4MeterId, (INT4 *) &u4ErrorCode) != SNMP_FAILURE)
            {
                if (nmhSetFsDiffServMeterStatus
                    (pClfrEntry->i4MeterId, DS_DESTROY) == SNMP_FAILURE)
                {
                    STRCPY (pu1ErrString,
                            "Meter entry with the given ID already exists");
                    if (u1Apply != 1)
                    {
                        nmhSetFsDiffServInProfileActionStatus
                            (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                        nmhSetFsDiffServOutProfileActionStatus
                            (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                        nmhSetFsDiffServClfrStatus
                            (pClfrEntry->i4DsClfrId, DS_DESTROY);
                    }
                    return SNMP_FAILURE;
                }
            }

            if (nmhTestv2FsDiffServMeterStatus
                (&u4ErrorCode, pClfrEntry->i4MeterId,
                 DS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid Meter ID");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus
                        (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServMeterStatus
                (pClfrEntry->i4MeterId, DS_CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Cannot create Meter entry");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServInProfileActionStatus
                        (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus
                        (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }

            if (nmhTestv2FsDiffServMetertokenSize
                (&u4ErrorCode, pClfrEntry->i4MeterId, FS_DS_METER_32768_TOKENS)
                == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid token size");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                    nmhSetFsDiffServInProfileActionStatus (pClfrEntry->
                                                           i4DsClfrInProActionId,
                                                           DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus (pClfrEntry->
                                                            i4DsClfrOutProActionId,
                                                            DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }

            if (nmhTestv2FsDiffServMeterRefreshCount
                (&u4ErrorCode, pClfrEntry->i4MeterId,
                 pClfrEntry->u4TrafficRate) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid refresh count");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                    nmhSetFsDiffServInProfileActionStatus (pClfrEntry->
                                                           i4DsClfrInProActionId,
                                                           DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus (pClfrEntry->
                                                            i4DsClfrOutProActionId,
                                                            DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServMetertokenSize
                (pClfrEntry->i4MeterId,
                 FS_DS_METER_32768_TOKENS) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid token size");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                    nmhSetFsDiffServInProfileActionStatus (pClfrEntry->
                                                           i4DsClfrInProActionId,
                                                           DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus (pClfrEntry->
                                                            i4DsClfrOutProActionId,
                                                            DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServMeterRefreshCount
                (pClfrEntry->i4MeterId,
                 pClfrEntry->u4TrafficRate) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid refresh count");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                    nmhSetFsDiffServInProfileActionStatus (pClfrEntry->
                                                           i4DsClfrInProActionId,
                                                           DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus (pClfrEntry->
                                                            i4DsClfrOutProActionId,
                                                            DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            if (nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_ACTIVE)
                == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Cannot set Meter entry status");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                    nmhSetFsDiffServInProfileActionStatus (pClfrEntry->
                                                           i4DsClfrInProActionId,
                                                           DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus (pClfrEntry->
                                                            i4DsClfrOutProActionId,
                                                            DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
            /* Setting the Meter-Id to Out-Profile Action Id */
            if (nmhSetFsDiffServOutProfileActionMID
                (pClfrEntry->i4DsClfrOutProActionId,
                 pClfrEntry->i4MeterId) == SNMP_FAILURE)
            {
                STRCPY (pu1ErrString, "Invalid Out-Profile Meter Id");
                if (u1Apply != 1)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                    nmhSetFsDiffServInProfileActionStatus (pClfrEntry->
                                                           i4DsClfrInProActionId,
                                                           DS_DESTROY);
                    nmhSetFsDiffServOutProfileActionStatus (pClfrEntry->
                                                            i4DsClfrOutProActionId,
                                                            DS_DESTROY);
                    nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                                DS_DESTROY);
                }
                return SNMP_FAILURE;
            }
        }

        if (nmhSetFsDiffServOutProfileActionStatus
            (pClfrEntry->i4DsClfrOutProActionId, DS_ACTIVE) == SNMP_FAILURE)
        {
            STRCPY (pu1ErrString, "Invalid Out-Profile Action Id");
            if (u1Apply != 1)
            {
                if (pClfrEntry->u4TrafficRate > 0)
                {
                    nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId,
                                                 DS_DESTROY);
                }
                nmhSetFsDiffServInProfileActionStatus
                    (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
                nmhSetFsDiffServOutProfileActionStatus
                    (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
                nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
            }
            return SNMP_FAILURE;
        }
    }

    /* Setting The Ids to Policy-Map */
    if (nmhTestv2FsDiffServClfrMFClfrId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrMFClfrId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Class-Map Id");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsDiffServClfrInProActionId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrInProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid In-Profile Id");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsDiffServClfrOutProActionId
        (&u4ErrorCode, pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrOutProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Out-Profile Id");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }

    if (nmhSetFsDiffServClfrMFClfrId
        (pClfrEntry->i4DsClfrId, pClfrEntry->i4DsClfrMFClfrId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Class-Map Id");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    if (nmhSetFsDiffServClfrInProActionId
        (pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrInProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid In-Profile Id");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    if (nmhSetFsDiffServClfrOutProActionId
        (pClfrEntry->i4DsClfrId,
         pClfrEntry->i4DsClfrOutProActionId) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Invalid Out-Profile Id");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    if (nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId,
                                    DS_ACTIVE) == SNMP_FAILURE)
    {
        STRCPY (pu1ErrString, "Cannot set Policy-Map entry status");
        if (u1Apply != 1)
        {
            if (pClfrEntry->u4TrafficRate > 0)
            {
                nmhSetFsDiffServMeterStatus (pClfrEntry->i4MeterId, DS_DESTROY);
            }
            nmhSetFsDiffServInProfileActionStatus
                (pClfrEntry->i4DsClfrInProActionId, DS_DESTROY);
            nmhSetFsDiffServOutProfileActionStatus
                (pClfrEntry->i4DsClfrOutProActionId, DS_DESTROY);
            nmhSetFsDiffServClfrStatus (pClfrEntry->i4DsClfrId, DS_DESTROY);
        }
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDiffServCoSqAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServCoSqAlgorithmTable
 Input       :  The Indices
                FsDiffServPortId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDiffServCoSqAlgorithmTable (INT4 i4FsDiffServPortId)
{
    if ((i4FsDiffServPortId > 0) &&
        (i4FsDiffServPortId <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServCoSqAlgorithmTable
 Input       :  The Indices
                FsDiffServPortId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (INT4 *pi4FsDiffServPortId)
{
    INT4                i4IfIndex = 0;

    for (i4IfIndex = 1; i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        if (gu4PortCosqScheduleAlgo[i4IfIndex - 1].u4Status ==
            DS_IFACE_STATUS_VALID)
        {
            *pi4FsDiffServPortId = i4IfIndex;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServCoSqAlgorithmTable
 Input       :  The Indices
                FsDiffServPortId
                nextFsDiffServPortId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServCoSqAlgorithmTable (INT4 i4FsDiffServPortId,
                                             INT4 *pi4NextFsDiffServPortId)
{
    INT4                i4IfIndex = 0;

    if ((i4FsDiffServPortId <= 0) ||
        (i4FsDiffServPortId >= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return SNMP_FAILURE;
    }

    for (i4IfIndex = (i4FsDiffServPortId + 1); i4IfIndex
         <= SYS_DEF_MAX_PHYSICAL_INTERFACES; i4IfIndex++)
    {
        if (gu4PortCosqScheduleAlgo[i4IfIndex - 1].u4Status ==
            DS_IFACE_STATUS_VALID)
        {
            *pi4NextFsDiffServPortId = i4IfIndex;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServCoSqAlgorithm
 Input       :  The Indices
                FsDiffServPortId

                The Object
                retValFsDiffServCoSqAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServCoSqAlgorithm (INT4 i4FsDiffServPortId,
                               INT4 *pi4RetValFsDiffServCoSqAlgorithm)
{
    *pi4RetValFsDiffServCoSqAlgorithm =
        gu4PortCosqScheduleAlgo[i4FsDiffServPortId - 1].u4CosqScheduleAlgo;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServCoSqAlgorithm
 Input       :  The Indices
                FsDiffServPortId

                The Object
                setValFsDiffServCoSqAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServCoSqAlgorithm (INT4 i4FsDiffServPortId,
                               INT4 i4SetValFsDiffServCoSqAlgorithm)
{
    UINT4               au4Weights[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4MinBw[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4MaxBw[VLAN_DEV_MAX_NUM_COSQ];
    UINT4               au4BwFlag[VLAN_DEV_MAX_NUM_COSQ];
    INT4                i4CoSqId = 0;

    MEMSET (au4Weights, 0, VLAN_DEV_MAX_NUM_COSQ);
    MEMSET (au4MinBw, 0, VLAN_DEV_MAX_NUM_COSQ);
    MEMSET (au4MaxBw, 0, VLAN_DEV_MAX_NUM_COSQ);
    MEMSET (au4BwFlag, 0, VLAN_DEV_MAX_NUM_COSQ);

    for (i4CoSqId = 0; i4CoSqId < VLAN_DEV_MAX_NUM_COSQ; i4CoSqId++)
    {
        au4Weights[i4CoSqId] =
            gu4CosqWeightsBw[i4FsDiffServPortId - 1][i4CoSqId].u4CosqWeights;
        au4MinBw[i4CoSqId] =
            gu4CosqWeightsBw[i4FsDiffServPortId - 1][i4CoSqId].u4CosqMinBw;
        au4MaxBw[i4CoSqId] =
            gu4CosqWeightsBw[i4FsDiffServPortId - 1][i4CoSqId].u4CosqMaxBw;
        au4BwFlag[i4CoSqId] =
            gu4CosqWeightsBw[i4FsDiffServPortId - 1][i4CoSqId].u4CosqBwFlag;
    }

#ifdef NPAPI_WANTED
    if (gDsGlobalInfo.DsStatus == DS_ENABLE)
    {
        /*Changing the algorithm in Hardware */
        if (DsHwChangeCosqScheduleAlgo
            (i4FsDiffServPortId, i4SetValFsDiffServCoSqAlgorithm,
             au4Weights, au4MinBw, au4MaxBw, au4BwFlag) == FNP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    gu4PortCosqScheduleAlgo[i4FsDiffServPortId - 1].u4CosqScheduleAlgo
        = i4SetValFsDiffServCoSqAlgorithm;

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServCoSqAlgorithm
 Input       :  The Indices
                FsDiffServPortId

                The Object
                testValFsDiffServCoSqAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServCoSqAlgorithm (UINT4 *pu4ErrorCode, INT4 i4FsDiffServPortId,
                                  INT4 i4TestValFsDiffServCoSqAlgorithm)
{
    INT4                i4Index = 0;
    if ((i4FsDiffServPortId <= 0) ||
        (i4FsDiffServPortId > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_STRICT) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_RR) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_WRR) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_WFQ) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_STRICT_RR) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_STRICT_WRR) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_STRICT_WFQ) &&
        (i4TestValFsDiffServCoSqAlgorithm != DS_COSQ_SCHE_ALGO_DEFICIT))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    for (i4Index = 0; i4Index < VLAN_DEV_MAX_NUM_COSQ; i4Index++)
    {
        if (i4TestValFsDiffServCoSqAlgorithm == DS_COSQ_SCHE_ALGO_WRR)
        {
            if (gu4CosqWeightsBw[i4FsDiffServPortId - 1][i4Index].u4CosqWeights
                == DS_COSQ_STRICT_WEIGHT)
            {
                PRINTF
                    ("Failed : All Weights should be non zero for WRR Algorithm\n");
                return SNMP_FAILURE;
            }
        }

        if (i4TestValFsDiffServCoSqAlgorithm == DS_COSQ_SCHE_ALGO_STRICT_RR)
        {
            if (gu4CosqWeightsBw[i4FsDiffServPortId - 1][i4Index].
                u4CosqWeights > DS_COSQ_MIN_WEIGHT)
            {
                PRINTF
                    ("Failed : All Weights should be either zero or one for strict-rr\n");
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsDiffServCoSqWeightBwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDiffServCoSqWeightBwTable
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDiffServCoSqWeightBwTable (INT4
                                                     i4FsDiffServBaseCoSqPortId,
                                                     INT4
                                                     i4FsDiffServPortCoSqId)
{
    if ((i4FsDiffServBaseCoSqPortId <= 0)
        || (i4FsDiffServBaseCoSqPortId > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return SNMP_FAILURE;
    }
    if ((i4FsDiffServPortCoSqId < 0)
        || (i4FsDiffServPortCoSqId > VLAN_DEV_MAX_NUM_COSQ - 1))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDiffServCoSqWeightBwTable
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDiffServCoSqWeightBwTable (INT4 *pi4FsDiffServBaseCoSqPortId,
                                             INT4 *pi4FsDiffServPortCoSqId)
{
    INT4                i4IfIndex = 0;

    for (i4IfIndex = 1; i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        if (gu4CosqWeightsBw[i4IfIndex - 1][0].u4Status ==
            DS_IFACE_STATUS_VALID)
        {
            *pi4FsDiffServBaseCoSqPortId = i4IfIndex;
            *pi4FsDiffServPortCoSqId = 0;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDiffServCoSqWeightBwTable
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                nextFsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId
                nextFsDiffServPortCoSqId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDiffServCoSqWeightBwTable (INT4 i4FsDiffServBaseCoSqPortId,
                                            INT4
                                            *pi4NextFsDiffServBaseCoSqPortId,
                                            INT4 i4FsDiffServPortCoSqId,
                                            INT4 *pi4NextFsDiffServPortCoSqId)
{
    INT4                i4IfIndex = 0;

    if ((i4FsDiffServBaseCoSqPortId <= 0)
        || (i4FsDiffServBaseCoSqPortId > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return SNMP_FAILURE;
    }

    else if ((i4FsDiffServPortCoSqId < 0)
             || (i4FsDiffServPortCoSqId > VLAN_DEV_MAX_NUM_COSQ - 1))
    {
        return SNMP_FAILURE;
    }
    else if (i4FsDiffServPortCoSqId == VLAN_DEV_MAX_NUM_COSQ - 1)
    {
        if (i4FsDiffServBaseCoSqPortId == SYS_DEF_MAX_PHYSICAL_INTERFACES)
        {
            return SNMP_FAILURE;
        }

        for (i4IfIndex = (i4FsDiffServBaseCoSqPortId + 1); i4IfIndex
             <= SYS_DEF_MAX_PHYSICAL_INTERFACES; i4IfIndex++)
        {
            if (gu4CosqWeightsBw[i4IfIndex - 1][i4FsDiffServPortCoSqId].u4Status
                == DS_IFACE_STATUS_VALID)
            {
                *pi4NextFsDiffServBaseCoSqPortId = i4IfIndex;
                *pi4NextFsDiffServPortCoSqId = 0;
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        *pi4NextFsDiffServBaseCoSqPortId = i4FsDiffServBaseCoSqPortId;
        *pi4NextFsDiffServPortCoSqId = i4FsDiffServPortCoSqId + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDiffServCoSqWeight
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                retValFsDiffServCoSqWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServCoSqWeight (INT4 i4FsDiffServBaseCoSqPortId,
                            INT4 i4FsDiffServPortCoSqId,
                            INT4 *pi4RetValFsDiffServCoSqWeight)
{
    *pi4RetValFsDiffServCoSqWeight =
        gu4CosqWeightsBw[i4FsDiffServBaseCoSqPortId -
                         1][i4FsDiffServPortCoSqId].u4CosqWeights;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServCoSqBwMin
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                retValFsDiffServCoSqBwMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServCoSqBwMin (INT4 i4FsDiffServBaseCoSqPortId,
                           INT4 i4FsDiffServPortCoSqId,
                           UINT4 *pu4RetValFsDiffServCoSqBwMin)
{
    *pu4RetValFsDiffServCoSqBwMin =
        gu4CosqWeightsBw[i4FsDiffServBaseCoSqPortId -
                         1][i4FsDiffServPortCoSqId].u4CosqMinBw;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServCoSqBwMax
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                retValFsDiffServCoSqBwMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServCoSqBwMax (INT4 i4FsDiffServBaseCoSqPortId,
                           INT4 i4FsDiffServPortCoSqId,
                           UINT4 *pu4RetValFsDiffServCoSqBwMax)
{
    *pu4RetValFsDiffServCoSqBwMax =
        gu4CosqWeightsBw[i4FsDiffServBaseCoSqPortId -
                         1][i4FsDiffServPortCoSqId].u4CosqMaxBw;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDiffServCoSqBwFlags
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                retValFsDiffServCoSqBwFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDiffServCoSqBwFlags (INT4 i4FsDiffServBaseCoSqPortId,
                             INT4 i4FsDiffServPortCoSqId,
                             INT4 *pi4RetValFsDiffServCoSqBwFlags)
{
    *pi4RetValFsDiffServCoSqBwFlags =
        gu4CosqWeightsBw[i4FsDiffServBaseCoSqPortId -
                         1][i4FsDiffServPortCoSqId].u4CosqBwFlag;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDiffServCoSqWeight
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                setValFsDiffServCoSqWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServCoSqWeight (INT4 i4FsDiffServBaseCoSqPortId,
                            INT4 i4FsDiffServPortCoSqId,
                            INT4 i4SetValFsDiffServCoSqWeight)
{

    gu4CosqWeightsBw[i4FsDiffServBaseCoSqPortId -
                     1][i4FsDiffServPortCoSqId].u4CosqWeights =
        i4SetValFsDiffServCoSqWeight;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsDiffServCoSqBwMin
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                setValFsDiffServCoSqBwMin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDiffServCoSqBwMin (INT4 i4FsDiffServBaseCoSqPortId,
                           INT4 i4FsDiffServPortCoSqId,
                           UINT4 u4SetValFsDiffServCoSqBwMin)
{

    gu4CosqWeightsBw[i4FsDiffServBaseCoSqPortId -
                     1][i4FsDiffServPortCoSqId].u4CosqMinBw =
        u4SetValFsDiffServCoSqBwMin;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDiffServCoSqWeight
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                testValFsDiffServCoSqWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServCoSqWeight (UINT4 *pu4ErrorCode,
                               INT4 i4FsDiffServBaseCoSqPortId,
                               INT4 i4FsDiffServPortCoSqId,
                               INT4 i4TestValFsDiffServCoSqWeight)
{
    if ((i4FsDiffServBaseCoSqPortId <= 0)
        || (i4FsDiffServBaseCoSqPortId > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4FsDiffServPortCoSqId < 0)
        || (i4FsDiffServPortCoSqId > VLAN_DEV_MAX_NUM_COSQ - 1))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDiffServCoSqWeight < DS_COSQ_STRICT_WEIGHT)
        || (i4TestValFsDiffServCoSqWeight > DS_COSQ_MAX_WEIGHT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDiffServCoSqBwMin
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId

                The Object
                testValFsDiffServCoSqBwMin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDiffServCoSqBwMin (UINT4 *pu4ErrorCode,
                              INT4 i4FsDiffServBaseCoSqPortId,
                              INT4 i4FsDiffServPortCoSqId,
                              UINT4 u4TestValFsDiffServCoSqBwMin)
{
    if ((i4FsDiffServBaseCoSqPortId <= 0)
        || (i4FsDiffServBaseCoSqPortId > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4FsDiffServPortCoSqId < 0)
        || (i4FsDiffServPortCoSqId > VLAN_DEV_MAX_NUM_COSQ - 1))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsDiffServCoSqBwMin < DS_COSQ_MIN_BW)
        || (u4TestValFsDiffServCoSqBwMin > DS_COSQ_MAX_BW))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDsSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDsSystemControl (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDsStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDsStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServMultiFieldClfrTable
 Input       :  The Indices
                FsDiffServMultiFieldClfrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServMultiFieldClfrTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServOutProfileActionTable
 Input       :  The Indices
                FsDiffServOutProfileActionId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServOutProfileActionTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServInProfileActionTable
 Input       :  The Indices
                FsDiffServInProfileActionId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServInProfileActionTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServMeterTable
 Input       :  The Indices
                FsDiffServMeterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServMeterTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServSchedulerTable
 Input       :  The Indices
                FsDiffServSchedulerId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServSchedulerTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServClfrTable
 Input       :  The Indices
                FsDiffServClfrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServClfrTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsDiffServCoSqAlgorithmTable
Input       :  The Indices
FsDiffServPortId
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDiffServCoSqAlgorithmTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDiffServCoSqWeightBwTable
 Input       :  The Indices
                FsDiffServBaseCoSqPortId
                FsDiffServPortCoSqId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDiffServCoSqWeightBwTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  DsNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
DsNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsissd, (sizeof (fsissd) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsDsSystemControl,
                      (sizeof (FsDsSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the diff serv shutdown, with 
     * diff serv oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, MSR_INVALID_CNTXT);
#endif
    return;
}
