#ifndef _FSISSDWR_H
#define _FSISSDWR_H

VOID RegisterFSISSD(VOID);

VOID UnRegisterFSISSD(VOID);
INT4 FsDsSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsDsStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDsSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsDsStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDsSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDsStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDsSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDsStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexFsDiffServMultiFieldClfrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServMultiFieldClfrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrFilterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrFilterTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrFilterIdSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrFilterTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrFilterIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrFilterTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServMultiFieldClfrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsDiffServClfrTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServClfrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrMFClfrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrInProActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrOutProActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrMFClfrIdSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrInProActionIdSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrOutProActionIdSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrMFClfrIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrInProActionIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrOutProActionIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServClfrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsDiffServInProfileActionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServInProfileActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionNewPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionIpTOSGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionNewPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionIpTOSSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionPortSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionNewPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionIpTOSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServInProfileActionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsDiffServOutProfileActionTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServOutProfileActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionMIDGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionMIDSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionMIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServOutProfileActionTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsDiffServMeterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServMeterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMetertokenSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterRefreshCountGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMetertokenSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterRefreshCountSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServMetertokenSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterRefreshCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServMeterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsDiffServSchedulerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServSchedulerIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerDPIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerQueueCountGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerWeightGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerDPIdSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerQueueCountSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerWeightSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerDPIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerQueueCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsDiffServCoSqAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServCoSqAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqAlgorithmSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqAlgorithmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqAlgorithmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDiffServCoSqWeightBwTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDiffServCoSqWeightGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqBwMinGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqBwMaxGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqBwFlagsGet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqWeightSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqBwMinSet(tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServCoSqBwMinTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDiffServSchedulerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 FsDiffServCoSqWeightBwTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



#endif /* _FSISSDWR_H */
