/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissddb.h,v 1.2 2010/02/11 12:17:46 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSDDB_H
#define _FSISSDDB_H

UINT1 FsDiffServMultiFieldClfrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServClfrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServInProfileActionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServOutProfileActionTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServMeterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServSchedulerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServCoSqAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDiffServCoSqWeightBwTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsissd [] ={1,3,6,1,4,1,2076,83};
tSNMP_OID_TYPE fsissdOID = {8, fsissd};


UINT4 FsDsSystemControl [ ] ={1,3,6,1,4,1,2076,83,1,1,1};
UINT4 FsDsStatus [ ] ={1,3,6,1,4,1,2076,83,1,1,2};
UINT4 FsDiffServMultiFieldClfrId [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,1};
UINT4 FsDiffServMultiFieldClfrFilterId [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,2};
UINT4 FsDiffServMultiFieldClfrFilterType [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,3};
UINT4 FsDiffServMultiFieldClfrStatus [ ] ={1,3,6,1,4,1,2076,83,1,2,1,1,4};
UINT4 FsDiffServClfrId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,1};
UINT4 FsDiffServClfrMFClfrId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,2};
UINT4 FsDiffServClfrInProActionId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,3};
UINT4 FsDiffServClfrOutProActionId [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,4};
UINT4 FsDiffServClfrStatus [ ] ={1,3,6,1,4,1,2076,83,1,3,1,1,5};
UINT4 FsDiffServInProfileActionId [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,1};
UINT4 FsDiffServInProfileActionFlag [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,2};
UINT4 FsDiffServInProfileActionNewPrio [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,3};
UINT4 FsDiffServInProfileActionIpTOS [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,4};
UINT4 FsDiffServInProfileActionPort [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,5};
UINT4 FsDiffServInProfileActionDscp [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,6};
UINT4 FsDiffServInProfileActionStatus [ ] ={1,3,6,1,4,1,2076,83,1,4,1,1,7};
UINT4 FsDiffServOutProfileActionId [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,1};
UINT4 FsDiffServOutProfileActionFlag [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,2};
UINT4 FsDiffServOutProfileActionDscp [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,3};
UINT4 FsDiffServOutProfileActionMID [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,4};
UINT4 FsDiffServOutProfileActionStatus [ ] ={1,3,6,1,4,1,2076,83,1,5,1,1,5};
UINT4 FsDiffServMeterId [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,1};
UINT4 FsDiffServMetertokenSize [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,2};
UINT4 FsDiffServMeterRefreshCount [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,3};
UINT4 FsDiffServMeterStatus [ ] ={1,3,6,1,4,1,2076,83,1,6,1,1,4};
UINT4 FsDiffServSchedulerId [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,1};
UINT4 FsDiffServSchedulerDPId [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,2};
UINT4 FsDiffServSchedulerQueueCount [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,3};
UINT4 FsDiffServSchedulerWeight [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,4};
UINT4 FsDiffServSchedulerStatus [ ] ={1,3,6,1,4,1,2076,83,1,7,1,1,5};
UINT4 FsDiffServPortId [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,1};
UINT4 FsDiffServCoSqAlgorithm [ ] ={1,3,6,1,4,1,2076,83,1,8,1,1,2};
UINT4 FsDiffServBaseCoSqPortId [ ] ={1,3,6,1,4,1,2076,83,1,9,1,1,1};
UINT4 FsDiffServPortCoSqId [ ] ={1,3,6,1,4,1,2076,83,1,9,1,1,2};
UINT4 FsDiffServCoSqWeight [ ] ={1,3,6,1,4,1,2076,83,1,9,1,1,3};
UINT4 FsDiffServCoSqBwMin [ ] ={1,3,6,1,4,1,2076,83,1,9,1,1,4};
UINT4 FsDiffServCoSqBwMax [ ] ={1,3,6,1,4,1,2076,83,1,9,1,1,5};
UINT4 FsDiffServCoSqBwFlags [ ] ={1,3,6,1,4,1,2076,83,1,9,1,1,6};


tMbDbEntry fsissdMibEntry[]= {

{{11,FsDsSystemControl}, NULL, FsDsSystemControlGet, FsDsSystemControlSet, FsDsSystemControlTest, FsDsSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsDsStatus}, NULL, FsDsStatusGet, FsDsStatusSet, FsDsStatusTest, FsDsStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsDiffServMultiFieldClfrId}, GetNextIndexFsDiffServMultiFieldClfrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServMultiFieldClfrFilterId}, GetNextIndexFsDiffServMultiFieldClfrTable, FsDiffServMultiFieldClfrFilterIdGet, FsDiffServMultiFieldClfrFilterIdSet, FsDiffServMultiFieldClfrFilterIdTest, FsDiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServMultiFieldClfrFilterType}, GetNextIndexFsDiffServMultiFieldClfrTable, FsDiffServMultiFieldClfrFilterTypeGet, FsDiffServMultiFieldClfrFilterTypeSet, FsDiffServMultiFieldClfrFilterTypeTest, FsDiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServMultiFieldClfrStatus}, GetNextIndexFsDiffServMultiFieldClfrTable, FsDiffServMultiFieldClfrStatusGet, FsDiffServMultiFieldClfrStatusSet, FsDiffServMultiFieldClfrStatusTest, FsDiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServMultiFieldClfrTableINDEX, 1, 0, 1, NULL},

{{13,FsDiffServClfrId}, GetNextIndexFsDiffServClfrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServClfrTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServClfrMFClfrId}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrMFClfrIdGet, FsDiffServClfrMFClfrIdSet, FsDiffServClfrMFClfrIdTest, FsDiffServClfrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1, 0, 0, "0"},

{{13,FsDiffServClfrInProActionId}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrInProActionIdGet, FsDiffServClfrInProActionIdSet, FsDiffServClfrInProActionIdTest, FsDiffServClfrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1, 0, 0, "0"},

{{13,FsDiffServClfrOutProActionId}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrOutProActionIdGet, FsDiffServClfrOutProActionIdSet, FsDiffServClfrOutProActionIdTest, FsDiffServClfrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1, 0, 0, "0"},

{{13,FsDiffServClfrStatus}, GetNextIndexFsDiffServClfrTable, FsDiffServClfrStatusGet, FsDiffServClfrStatusSet, FsDiffServClfrStatusTest, FsDiffServClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServClfrTableINDEX, 1, 0, 1, NULL},

{{13,FsDiffServInProfileActionId}, GetNextIndexFsDiffServInProfileActionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServInProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServInProfileActionFlag}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionFlagGet, FsDiffServInProfileActionFlagSet, FsDiffServInProfileActionFlagTest, FsDiffServInProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServInProfileActionNewPrio}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionNewPrioGet, FsDiffServInProfileActionNewPrioSet, FsDiffServInProfileActionNewPrioTest, FsDiffServInProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServInProfileActionIpTOS}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionIpTOSGet, FsDiffServInProfileActionIpTOSSet, FsDiffServInProfileActionIpTOSTest, FsDiffServInProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServInProfileActionPort}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionPortGet, FsDiffServInProfileActionPortSet, FsDiffServInProfileActionPortTest, FsDiffServInProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServInProfileActionDscp}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionDscpGet, FsDiffServInProfileActionDscpSet, FsDiffServInProfileActionDscpTest, FsDiffServInProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServInProfileActionStatus}, GetNextIndexFsDiffServInProfileActionTable, FsDiffServInProfileActionStatusGet, FsDiffServInProfileActionStatusSet, FsDiffServInProfileActionStatusTest, FsDiffServInProfileActionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServInProfileActionTableINDEX, 1, 0, 1, NULL},

{{13,FsDiffServOutProfileActionId}, GetNextIndexFsDiffServOutProfileActionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServOutProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServOutProfileActionFlag}, GetNextIndexFsDiffServOutProfileActionTable, FsDiffServOutProfileActionFlagGet, FsDiffServOutProfileActionFlagSet, FsDiffServOutProfileActionFlagTest, FsDiffServOutProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServOutProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServOutProfileActionDscp}, GetNextIndexFsDiffServOutProfileActionTable, FsDiffServOutProfileActionDscpGet, FsDiffServOutProfileActionDscpSet, FsDiffServOutProfileActionDscpTest, FsDiffServOutProfileActionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServOutProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServOutProfileActionMID}, GetNextIndexFsDiffServOutProfileActionTable, FsDiffServOutProfileActionMIDGet, FsDiffServOutProfileActionMIDSet, FsDiffServOutProfileActionMIDTest, FsDiffServOutProfileActionTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServOutProfileActionTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServOutProfileActionStatus}, GetNextIndexFsDiffServOutProfileActionTable, FsDiffServOutProfileActionStatusGet, FsDiffServOutProfileActionStatusSet, FsDiffServOutProfileActionStatusTest, FsDiffServOutProfileActionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServOutProfileActionTableINDEX, 1, 0, 1, NULL},

{{13,FsDiffServMeterId}, GetNextIndexFsDiffServMeterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServMetertokenSize}, GetNextIndexFsDiffServMeterTable, FsDiffServMetertokenSizeGet, FsDiffServMetertokenSizeSet, FsDiffServMetertokenSizeTest, FsDiffServMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServMeterRefreshCount}, GetNextIndexFsDiffServMeterTable, FsDiffServMeterRefreshCountGet, FsDiffServMeterRefreshCountSet, FsDiffServMeterRefreshCountTest, FsDiffServMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServMeterStatus}, GetNextIndexFsDiffServMeterTable, FsDiffServMeterStatusGet, FsDiffServMeterStatusSet, FsDiffServMeterStatusTest, FsDiffServMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServMeterTableINDEX, 1, 0, 1, NULL},

{{13,FsDiffServSchedulerId}, GetNextIndexFsDiffServSchedulerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServSchedulerDPId}, GetNextIndexFsDiffServSchedulerTable, FsDiffServSchedulerDPIdGet, FsDiffServSchedulerDPIdSet, FsDiffServSchedulerDPIdTest, FsDiffServSchedulerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServSchedulerQueueCount}, GetNextIndexFsDiffServSchedulerTable, FsDiffServSchedulerQueueCountGet, FsDiffServSchedulerQueueCountSet, FsDiffServSchedulerQueueCountTest, FsDiffServSchedulerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServSchedulerTableINDEX, 1, 0, 0, "1"},

{{13,FsDiffServSchedulerWeight}, GetNextIndexFsDiffServSchedulerTable, FsDiffServSchedulerWeightGet, FsDiffServSchedulerWeightSet, FsDiffServSchedulerWeightTest, FsDiffServSchedulerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServSchedulerStatus}, GetNextIndexFsDiffServSchedulerTable, FsDiffServSchedulerStatusGet, FsDiffServSchedulerStatusSet, FsDiffServSchedulerStatusTest, FsDiffServSchedulerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServSchedulerTableINDEX, 1, 0, 1, NULL},

{{13,FsDiffServPortId}, GetNextIndexFsDiffServCoSqAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServCoSqAlgorithmTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServCoSqAlgorithm}, GetNextIndexFsDiffServCoSqAlgorithmTable, FsDiffServCoSqAlgorithmGet, FsDiffServCoSqAlgorithmSet, FsDiffServCoSqAlgorithmTest, FsDiffServCoSqAlgorithmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDiffServCoSqAlgorithmTableINDEX, 1, 0, 0, NULL},

{{13,FsDiffServBaseCoSqPortId}, GetNextIndexFsDiffServCoSqWeightBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServCoSqWeightBwTableINDEX, 2, 0, 0, NULL},

{{13,FsDiffServPortCoSqId}, GetNextIndexFsDiffServCoSqWeightBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDiffServCoSqWeightBwTableINDEX, 2, 0, 0, NULL},

{{13,FsDiffServCoSqWeight}, GetNextIndexFsDiffServCoSqWeightBwTable, FsDiffServCoSqWeightGet, FsDiffServCoSqWeightSet, FsDiffServCoSqWeightTest, FsDiffServCoSqWeightBwTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDiffServCoSqWeightBwTableINDEX, 2, 0, 0, NULL},

{{13,FsDiffServCoSqBwMin}, GetNextIndexFsDiffServCoSqWeightBwTable, FsDiffServCoSqBwMinGet, FsDiffServCoSqBwMinSet, FsDiffServCoSqBwMinTest, FsDiffServCoSqWeightBwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDiffServCoSqWeightBwTableINDEX, 2, 0, 0, NULL},

{{13,FsDiffServCoSqBwMax}, GetNextIndexFsDiffServCoSqWeightBwTable, FsDiffServCoSqBwMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDiffServCoSqWeightBwTableINDEX, 2, 0, 0, NULL},

{{13,FsDiffServCoSqBwFlags}, GetNextIndexFsDiffServCoSqWeightBwTable, FsDiffServCoSqBwFlagsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDiffServCoSqWeightBwTableINDEX, 2, 0, 0, NULL},
};
tMibData fsissdEntry = { 40, fsissdMibEntry };
#endif /* _FSISSDDB_H */

