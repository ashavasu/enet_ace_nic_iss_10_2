/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsDsStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsDsStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDsStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDsSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDsStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDiffServMultiFieldClfrTable. */
INT1
nmhValidateIndexInstanceFsDiffServMultiFieldClfrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServMultiFieldClfrTable  */

INT1
nmhGetFirstIndexFsDiffServMultiFieldClfrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServMultiFieldClfrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServMultiFieldClfrFilterId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServMultiFieldClfrFilterType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServMultiFieldClfrStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServMultiFieldClfrFilterId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServMultiFieldClfrFilterType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServMultiFieldClfrStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServMultiFieldClfrFilterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServMultiFieldClfrFilterType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServMultiFieldClfrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDiffServMultiFieldClfrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDiffServClfrTable. */
INT1
nmhValidateIndexInstanceFsDiffServClfrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServClfrTable  */

INT1
nmhGetFirstIndexFsDiffServClfrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServClfrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServClfrMFClfrId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServClfrInProActionId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServClfrOutProActionId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServClfrStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServClfrMFClfrId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServClfrInProActionId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServClfrOutProActionId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServClfrStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServClfrMFClfrId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServClfrInProActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServClfrOutProActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServClfrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDiffServClfrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDiffServInProfileActionTable. */
INT1
nmhValidateIndexInstanceFsDiffServInProfileActionTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServInProfileActionTable  */

INT1
nmhGetFirstIndexFsDiffServInProfileActionTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServInProfileActionTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServInProfileActionFlag ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionNewPrio ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionIpTOS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionDscp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServInProfileActionStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServInProfileActionFlag ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionNewPrio ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionIpTOS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionDscp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServInProfileActionStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServInProfileActionFlag ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionNewPrio ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionIpTOS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServInProfileActionStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDiffServInProfileActionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDiffServOutProfileActionTable. */
INT1
nmhValidateIndexInstanceFsDiffServOutProfileActionTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServOutProfileActionTable  */

INT1
nmhGetFirstIndexFsDiffServOutProfileActionTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServOutProfileActionTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServOutProfileActionFlag ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServOutProfileActionDscp ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServOutProfileActionMID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServOutProfileActionStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServOutProfileActionFlag ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServOutProfileActionDscp ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServOutProfileActionMID ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServOutProfileActionStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServOutProfileActionFlag ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServOutProfileActionDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServOutProfileActionMID ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServOutProfileActionStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDiffServOutProfileActionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDiffServMeterTable. */
INT1
nmhValidateIndexInstanceFsDiffServMeterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServMeterTable  */

INT1
nmhGetFirstIndexFsDiffServMeterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServMeterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServMetertokenSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServMeterRefreshCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServMeterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServMetertokenSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServMeterRefreshCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServMeterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServMetertokenSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServMeterRefreshCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServMeterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDiffServMeterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDiffServSchedulerTable. */
INT1
nmhValidateIndexInstanceFsDiffServSchedulerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServSchedulerTable  */

INT1
nmhGetFirstIndexFsDiffServSchedulerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServSchedulerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServSchedulerDPId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDiffServSchedulerQueueCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDiffServSchedulerWeight ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDiffServSchedulerStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServSchedulerDPId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDiffServSchedulerQueueCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDiffServSchedulerWeight ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDiffServSchedulerStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServSchedulerDPId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServSchedulerQueueCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDiffServSchedulerWeight ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDiffServSchedulerStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDiffServCoSqAlgorithmTable. */
INT1
nmhValidateIndexInstanceFsDiffServCoSqAlgorithmTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServCoSqAlgorithmTable  */

INT1
nmhGetFirstIndexFsDiffServCoSqAlgorithmTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServCoSqAlgorithmTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServCoSqAlgorithm ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsDiffServCoSqAlgorithm ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsDiffServCoSqAlgorithm ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServCoSqAlgorithm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDiffServCoSqWeightBwTable. */
INT1
nmhValidateIndexInstanceFsDiffServCoSqWeightBwTable ARG_LIST((INT4  , INT4 ));

/* Proto Validate Index Instance for FsDiffServCoSqWeightBwTable. */
INT1
nmhDepv2FsDiffServSchedulerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhTestv2FsDiffServCoSqBwMin ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDiffServCoSqWeightBwTable  */

INT1
nmhGetFirstIndexFsDiffServCoSqWeightBwTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDiffServCoSqWeightBwTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDiffServCoSqWeight ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsDiffServCoSqBwMin ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDiffServCoSqBwMax ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDiffServCoSqBwFlags ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDiffServCoSqWeight ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsDiffServCoSqBwMin ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDiffServCoSqWeight ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsDiffServCoSqBwMin ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhDepv2FsDiffServCoSqAlgorithmTable ARG_LIST((UINT4 *, tSnmpIndexList*, 
                                               tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDiffServCoSqWeightBwTable ARG_LIST((UINT4 *, tSnmpIndexList*, 
                                              tSNMP_VAR_BIND*));
