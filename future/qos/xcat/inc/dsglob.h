/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : dsglob.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : DiffServ module                                */
/*    MODULE NAME           : DiffServ module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 2002                                           */
/*    AUTHOR                : Manish K S                                     */
/*    DESCRIPTION           : This file contains global variables used       */
/*                            in DiffServ Module.                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    2002 /Manish K S           Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _DSGLOB_H
#define _DSGLOB_H


#ifdef _DSSYS_C

tDsGlobalInfo gDsGlobalInfo;
UINT4         gu4PortMask[DS_MAX_PORT_MASK + 1] = {

                     0x0000, 0x8000, 0xC000, 0xE000, 0xF000,
		     0xF800, 0xFC00, 0xFE00, 0xFF00,
		     0xFF80, 0xFFC0, 0xFFE0, 0xFFF0,
		     0xFFF8, 0xFFFC, 0xFFFE, 0xFFFF
};

tCosqScheduleAlgo gu4PortCosqScheduleAlgo[SYS_DEF_MAX_PHYSICAL_INTERFACES];
tCosqWeightBw     gu4CosqWeightsBw[SYS_DEF_MAX_PHYSICAL_INTERFACES][VLAN_DEV_MAX_NUM_COSQ];


#else

extern tDsGlobalInfo gDsGlobalInfo;
extern UINT4  gu4PortMask[DS_MAX_PORT_MASK + 1];
#endif /* _DSSYS_C */

#endif /* _DSGLOB_H */

