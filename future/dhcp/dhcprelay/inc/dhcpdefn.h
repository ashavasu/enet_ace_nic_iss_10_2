/*****************************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: dhcpdefn.h,v 1.9 2017/12/14 10:23:41 siva Exp $
 ******************************************************************************/

#ifndef DHCPDEFN_H
#define DHCPDEFN_H
/****************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved                        * 
* Copyright (C) 2006 Aricent Inc . All Rights Reserved               *
*                                                               *
*   FILE NAME                 :  dhcpdefn.h                     *
*   PRINCIPAL AUTHOR          :  J Harsha Vardhan               *
*   SUBSYSTEM                 :                                 *
*   MODULE NAME               :                                 *
*   LANGUAGE                  :  C                              *
*   TARGET ENVIRONMENT        :  Unix                           *
*   DATE OF FIRST RELEASE     :                                 *
*   DESCRIPTION               :  Contains the definitions of    *
*                                constants and structures       *
****************************************************************/

#define DHCP_UDP_OVERHEAD       (14 + /* Ethernet header */        \
                                20 + /* IP header */               \
                                8)   /* UDP header */
#define DHCP_SNAME_LEN          64
#define DHCP_FILE_LEN           128
#define DHCP_FIXED_NON_UDP      236
#define DHCP_FIXED_LEN          (DHCP_FIXED_NON_UDP + DHCP_UDP_OVERHEAD)
                                          /* Everything but options. */
#define DHCP_MTU_MAX            1500
#define DHCP_OPTION_LEN         (DHCP_MTU_MAX - DHCP_FIXED_LEN)

#define BOOTP_MIN_LEN           300
#define DHCP_MIN_LEN            548

/*--------------- POSSIBLE VALUES FOR FLAGS FIELD -----------------*/
#define    BOOTP_BROADCAST 32768L

/*-------------- POSSIBLE VALUES FOR HARDWARE TYPE (HTYPE) FIELD---*/
#define HTYPE_ETHER        1               /* Ethernet 10Mbps              */
#define HTYPE_IEEE802      6               /* IEEE 802.2 Token Ring...    */

/*--------- MAGIC COOKIE VALIDATING DHCP OPTIONS FIELD (AND BOOTP VENDOR
   EXTENSIONS FIELD).-------------- */
#define DHCP_OPTIONS_COOKIE    0x63825363
#define DHCP_COOKIE_SIZE       4
#ifdef RELAY_AGNT_INFO
#define DHCP_SUBOPTION_SUBNETMASK_SIZE 4
#endif /* RELAY_AGNT_INFO */

/*--------------- DHCP OPTION CODES ---------------------------------*/
  
#define DHCP_OPTION_PAD                    0
#define DHCP_OPTION_SUBNET_MASK            1
#define DHCP_OPTION_TIME_OFFSET            2
#define DHCP_OPTION_ROUTERS                3
#define DHCP_OPTION_TIME_SERVERS           4
#define DHCP_OPTION_NAME_SERVERS           5
#define DHCP_OPTION_DOMAIN_NAME_SERVERS    6
#define DHCP_OPTION_LOG_SERVERS            7
#define DHCP_OPTION_COOKIE_SERVERS         8
#define DHCP_OPTION_LPR_SERVERS            9
#define DHCP_OPTION_IMPRESS_SERVERS        10
#define DHCP_OPTION_RESOURCE_LOCATION_SERVERS    11
#define DHCP_OPTION_HOST_NAME                    12
#define DHCP_OPTION_BOOT_SIZE                    13
#define DHCP_OPTION_MERIT_DUMP                   14
#define DHCP_OPTION_DOMAIN_NAME                  15
#define DHCP_OPTION_SWAP_SERVER                  16
#define DHCP_OPTION_ROOT_PATH                    17
#define DHCP_OPTION_EXTENSIONS_PATH              18
#define DHCP_OPTION_IP_FORWARDING                19
#define DHCP_OPTION_NON_LOCAL_SOURCE_ROUTING     20
#define DHCP_OPTION_POLICY_FILTER                21
#define DHCP_OPTION_MAX_DGRAM_REASSEMBLY         22
#define DHCP_OPTION_DEFAULT_IP_TTL               23
#define DHCP_OPTION_PATH_MTU_AGING_TIMEOUT       24
#define DHCP_OPTION_PATH_MTU_PLATEAU_TABLE       25
#define DHCP_OPTION_INTERFACE_MTU                26
#define DHCP_OPTION_ALL_SUBNETS_LOCAL            27
#define DHCP_OPTION_BROADCAST_ADDRESS            28
#define DHCP_OPTION_PERFORM_MASK_DISCOVERY       29
#define DHCP_OPTION_MASK_SUPPLIER                30
#define DHCP_OPTION_ROUTER_DISCOVERY             31
#define DHCP_OPTION_ROUTER_SOLICITATION_ADDRESS  32
#define DHCP_OPTION_STATIC_ROUTES                33
#define DHCP_OPTION_TRAILER_ENCAPSULATION        34
#define DHCP_OPTION_ARP_CACHE_TIMEOUT            35
#define DHCP_OPTION_IEEE802_3_ENCAPSULATION      36
#define DHCP_OPTION_DEFAULT_TCP_TTL              37
#define DHCP_OPTION_TCP_KEEPALIVE_INTERVAL       38
#define DHCP_OPTION_TCP_KEEPALIVE_GARBAGE        39
#define DHCP_OPTION_NIS_DOMAIN                   40
#define DHCP_OPTION_NIS_SERVERS                  41
#define DHCP_OPTION_NTP_SERVERS                  42
#define DHCP_OPTION_VENDOR_ENCAPSULATED_OPTIONS  43
#define DHCP_OPTION_NETBIOS_NAME_SERVERS         44
#define DHCP_OPTION_NETBIOS_DD_SERVER            45
#define DHCP_OPTION_NETBIOS_NODE_TYPE            46
#define DHCP_OPTION_NETBIOS_SCOPE                47
#define DHCP_OPTION_X_FONT_SERVERS               48
#define DHCP_OPTION_X_DISPLAY_MANAGER            49
#define DHCP_OPTION_REQUESTED_ADDRESS            50
#define DHCP_OPTION_LEASE_TIME                   51
#define DHCP_OPTION_OPTION_OVERLOAD              52
#define DHCP_OPTION_MESSAGE_TYPE                 53
#define DHCP_OPTION_SERVER_IDENTIFIER            54
#define DHCP_OPTION_PARAMETER_REQUEST_LIST       55
#define DHCP_OPTION_MESSAGE                      56
#define DHCP_OPTION_MAX_MESSAGE_SIZE             57
#define DHCP_OPTION_RENEWAL_TIME                 58
#define DHCP_OPTION_REBINDING_TIME               59
#define DHCP_OPTION_CLASS_IDENTIFIER             60
#define DHCP_OPTION_CLIENT_IDENTIFIER            61

/*___________________ NEWLY ADDED _______________________________*/
#define DHCP_OPTION_NW_INFO_SERVICE_DOMAIN       64
#define DHCP_OPTION_NW_INFO_SERVICE_SERVERS      65
#define DHCP_OPTION_TFTP_SERVER_NAME             66
#define DHCP_OPTION_BOOT_FILE_NAME               67
#define DHCP_OPTION_MOBILE_IP_HOME_AGENT         68
#define DHCP_OPTION_SMTP_SERVER                  69
#define DHCP_OPTION_POP3_SERVER                  70
#define DHCP_OPTION_NNTP_SERVER                  71
#define DHCP_OPTION_WWW_SERVER                   72
#define DHCP_OPTION_DEFAULT_FINGER_SERVER        73
#define DHCP_OPTION_DEFAULT_INTERNET_RELAY_CHAT_SERVER       74
#define DHCP_OPTION_STREETTALK_SERVER                        75
#define DHCP_OPTION_STREETTALK_DIRECTORY_ASSISTANCE_SERVER   76

#define DHCP_OPTION_USER_CLASS_ID                            77
#define DHCP_OPTION_END                                      255
#define DHCP_RELAY_OPTION_MIN_LEN                            64

/*Added for RFC 5010 - Relay Agent Flags Suboption */

#define DHCP_SUBOPTION_FLAGS_CODE 0xA
#define DHCP_SUBOPTION_FLAGS_LENGTH 1
#define DHCP_SUBOPTION_FLAGS_UNICAST_BIT 0x80
#define DHCP_SUBOPTION_FLAGS_BROADCAST_BIT 0
#define DHCP_OPTION_CLIENT_BROADCAST_BIT 128

#define DHCP_OPTION_FIRST_LOW       0
#define DHCP_OPTION_FIRST_HIGH      61
#define DHCP_OPTION_SECOUND_LOW     64
#define DHCP_OPTION_SECOUND_HIGH    77
#define DHCP_OPTION_LAST_VALID      255

#ifdef RELAY_AGNT_INFO
#define DHCP_OPTION_RAI 82
#define DHCP_SUBOPTION_SERVER_OVERRIDE 5
#define DHCP_SUBOPTION_LINK_SELECT 11
#define DHCP_SUBOPTION_VPN 151
#define DHCP_SUBOPTION_SUBNETMASK 3
#define DHCP_SUBOPTION_REMOTEID 2
#define DHCP_SUBOPTION_CKTID 1

#define DHCP_MAX_REMOTEID_SIZE 100
#define DHCP_MAX_CKTID_SIZE 100
#define DHCP_MAC_ADDRESS_LENGTH 6


#define DHCP_CIRCUIT_ACTION_FLAG_LENGTH          4

#define DHCP_SET_CIRCUIT_ACTIONS_MASK(pActFlag,pSNMPOctSrt) \
    MEMCPY (pActFlag,pSNMPOctSrt->pu1_OctetList,pSNMPOctSrt->i4_Length); \

#define DHCP_GET_CIRCUIT_ACTIONS_MASK(pSNMPOctSrt,u4Mask) \
        pSNMPOctSrt->i4_Length = DHCP_CIRCUIT_ACTION_FLAG_LENGTH; \
            MEMCPY (pSNMPOctSrt->pu1_OctetList,&u4Mask, pSNMPOctSrt->i4_Length);



#endif /* RELAY_AGNT_INFO */

/* DHCP message types. */
#define DHCPDISCOVER    1
#define DHCPOFFER       2
#define DHCPREQUEST     3
#define DHCPDECLINE     4
#define DHCPACK         5
#define DHCPNAK         6
#define DHCPRELEASE     7
#define DHCPINFORM      8


#define DHCP_SR_MAX_RCV_PKTS           20
#define DHCP_SR_MAX_PORTS           20



#define MAX_HW_ADDRESS_BYTES   12
#define DHCP_HARD_ADD_LEN      16


#define DHCP_TRUE                  1
#define DHCP_FALSE                 0

#define BOOTPS                  67
#define BOOTPC                  68

#define DHCPBROADCAST             1
#define DHCPUNICAST               0

/* Log values for failures */
#define DHCP_ALLOC_FAIL 1
#define DHCP_EXTERN_FAIL 2

/* Log values for others */
#define DHCP_MAJOR_EVENT 3
#define DHCP_LOG 4

#define DHCP_INC_INBET(X,Y,Z) ( (X>=Y) && (X<=Z) )


#define DHCP_BROADCAST_ADDRESS 0xffffffff

#endif /* DHCPDEFN_H */


#define BROADCASTBIT 0x8000
#define FIND_INTERFACE 0xffff    

#define DHCP_SERVER_PORT        67
#define DHCP_CLIENT_PORT        68

#define DHCPRLY_CMSG_LEN  (sizeof (struct cmsghdr) + sizeof (struct in_pktinfo))
