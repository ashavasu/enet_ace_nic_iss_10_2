/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmidhwr.h,v 1.2 2017/12/14 10:23:42 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/

#ifndef _FSMIDHWR_H
#define _FSMIDHWR_H

VOID RegisterFSMIDHRly(VOID);

VOID UnRegisterFSMIDHRly(VOID);
INT4 FsMIDhcpConfigGblTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigGblTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigGblTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigGblTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMIDhcpContextTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDhcpRelayingGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayServersOnlyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelaySecsThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayHopsThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAICircuitIDSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIRemoteIDSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAISubnetMaskSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAICircuitIDSubOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIRemoteIDSubOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAISubnetMaskSubOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIOptionWronglySetGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAISpaceConstraintGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigDhcpCircuitOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayCounterResetGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayContextRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIVPNIDSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayingSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayServersOnlySet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelaySecsThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayHopsThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAICircuitIDSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIRemoteIDSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAISubnetMaskSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigDhcpCircuitOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayCounterResetSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayContextRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIVPNIDSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayServersOnlyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelaySecsThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayHopsThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAICircuitIDSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIRemoteIDSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAISubnetMaskSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpConfigDhcpCircuitOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayCounterResetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayContextRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayRAIVPNIDSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpContextTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDhcpRelaySrvAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDhcpRelaySrvAddressRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelaySrvAddressRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelaySrvAddressRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelaySrvAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDhcpRelayIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDhcpRelayIfCircuitIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfRemoteIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfCircuitIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfRemoteIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfCircuitIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfRemoteIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDhcpRelayIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMIDHWR_H */
