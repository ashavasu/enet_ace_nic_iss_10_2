#ifndef DHCPTDFS_H
#define DHCPTDFS_H
/****************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved                        * 
* Licenses Aricent Inc.,1997-98               *
*                                                               *
* $Id: dhcptdfs.h,v 1.6 2011/05/26 06:28:26 siva Exp $
*                                                               *
*   FILE NAME                 :  dhcptdfs.h                     *
*   PRINCIPAL AUTHOR          :  Aricent Inc.                *
*   SUBSYSTEM                 :                                 *
*   MODULE NAME               :                                 *
*   LANGUAGE                  :  C                              *
*   TARGET ENVIRONMENT        :  Unix                           *
*   DATE OF FIRST RELEASE     :                                 *
*   DESCRIPTION               :  Contains the definitions of    *
*                                DHCP Packet                    *
****************************************************************/





#ifndef PACK_REQUIRED
#pragma pack(1)
#endif
typedef struct DhcpPacket {
    UINT1  u1Op;           /* Message opcode/type */
    UINT1  u1Htype;        /* Hardware addr type (see net/if_types.h) */
    UINT1  u1Hlen;         /* Hardware addr length */
    UINT1  u1Hops;         /* Number of relay agent hops from client */
    UINT4  u4Xid;          /* Transaction ID */
    UINT2  u2Secs;         /* Seconds since client started looking */
    UINT2  u2Flags;        /* Flag bits */
    UINT4  u4Ciaddr;       /* Client IP address (if already in use) */
    UINT4  u4Yiaddr;       /* Client IP address */
    UINT4  u4Siaddr;       /* IP address of next server to talk to */
    UINT4  u4Giaddr;       /* DHCP relay agent IP address */
    UINT1  u1Chaddr[DHCP_HARD_ADD_LEN];   /* Client hardware address */
    UINT1  au1Sname[DHCP_SNAME_LEN];        /* Server name */
    UINT1  au1File[DHCP_FILE_LEN];          /* Boot filename */
    UINT1  au1Options[DHCP_OPTION_LEN];     /* Optional parameters
                                          (actual length dependent on MTU). */
    UINT2 u2Padding;       /* For structure alignment */    
}tDhcpPacket;
#ifndef PACK_REQUIRED
#pragma pack()
#endif 

typedef struct DhcpRPktInfo {
   UINT4 u4Index; /* for back referencing the Pkts array */
   UINT4 u4IfNum; /* Port no of recvd Pkt */
   UINT4 u4LeaseDuration; 
   UINT4 SerId;
   UINT4 ReqIp;
   UINT4 Giaddr;
   UINT2 u2Len; /* Length of the pkt */
   UINT1 u1PktType; /* Computed pkt type */
   UINT1 u1Flags; /* The LSB of this is 1 for pkts recvd by Broadcast */
   tDhcpPacket Pkt;
} tDhcpPktInfo;

#endif /*DHCPTDFS_H */
