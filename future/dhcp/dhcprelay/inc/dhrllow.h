/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved 
*
* $Id: dhrllow.h,v 1.7 2017/12/14 10:23:42 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpRelaying ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayServersOnly ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelaySecsThreshold ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayHopsThreshold ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayRAIOptionControl ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayRAICircuitIDSubOptionControl ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((INT4 *));

INT1
nmhGetDhcpRelayRAIOptionInserted ARG_LIST((UINT4 *));

INT1
nmhGetDhcpRelayRAICircuitIDSubOptionInserted ARG_LIST((UINT4 *));

INT1
nmhGetDhcpRelayRAIRemoteIDSubOptionInserted ARG_LIST((UINT4 *));

INT1
nmhGetDhcpRelayRAISubnetMaskSubOptionInserted ARG_LIST((UINT4 *));

INT1
nmhGetDhcpRelayRAIOptionWronglySet ARG_LIST((UINT4 *));

INT1
nmhGetDhcpRelayRAISpaceConstraint ARG_LIST((UINT4 *));

INT1
nmhGetDhcpConfigTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetDhcpConfigDhcpCircuitOption ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
INT1
nmhGetDhcpRelayCounterReset ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetDhcpRelayRAIVPNIDSubOptionControl ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpRelaying ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayServersOnly ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelaySecsThreshold ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayHopsThreshold ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayRAIOptionControl ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayRAICircuitIDSubOptionControl ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((INT4 ));

INT1
nmhSetDhcpConfigTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetDhcpConfigDhcpCircuitOption ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
INT1
nmhSetDhcpRelayCounterReset ARG_LIST((INT4 ));

INT1
nmhSetDhcpRelayRAIVPNIDSubOptionControl ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpRelaying ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayServersOnly ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelaySecsThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayHopsThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayRAIOptionControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayRAICircuitIDSubOptionControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpConfigTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpConfigDhcpCircuitOption ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpRelayCounterReset ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpRelayRAIVPNIDSubOptionControl ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpRelaying ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayServersOnly ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelaySecsThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayHopsThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayRAIOptionControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayRAICircuitIDSubOptionControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpConfigTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpConfigDhcpCircuitOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2DhcpRelayCounterReset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpRelayRAIVPNIDSubOptionControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpRelaySrvAddressTable. */
INT1
nmhValidateIndexInstanceDhcpRelaySrvAddressTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpRelaySrvAddressTable  */

INT1
nmhGetFirstIndexDhcpRelaySrvAddressTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpRelaySrvAddressTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpRelaySrvAddressRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpRelaySrvAddressRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpRelaySrvAddressRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpRelaySrvAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpRelayIfTable. */
INT1
nmhValidateIndexInstanceDhcpRelayIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpRelayIfTable  */

INT1
nmhGetFirstIndexDhcpRelayIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpRelayIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpRelayIfCircuitId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpRelayIfRemoteId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpRelayIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpRelayIfCircuitId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDhcpRelayIfRemoteId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpRelayIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpRelayIfCircuitId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2DhcpRelayIfRemoteId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpRelayIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpRelayIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
