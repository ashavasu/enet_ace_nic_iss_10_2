
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpincs.h,v 1.10 2015/06/17 04:49:33 siva Exp $ 
 *
 * Description: Common include files for dhcp relay module.
 *******************************************************************/
#ifndef _DHCPINCS_H

#include "lr.h"
#include "ip.h"
#include "vcm.h"
#include "fssocket.h"
#include "lnxip.h"
#include "dhcpdefn.h"
#include "dhcpport.h"
#include "dhcptdfs.h"
#include "dhcp.h"
#include "dhrlutil.h"
#include "dhrlincs.h"
#include "fsvlan.h"
#include "dhrllow.h"
#include "fsmidhlw.h"
#ifdef L3_SWITCHING_WANTED
#include "npapi.h"
#include "ipnp.h"
#endif

#ifdef NPAPI_WANTED
#include "dhrlnpwr.h"
#endif /* NPAPI_WANTED */
#endif
