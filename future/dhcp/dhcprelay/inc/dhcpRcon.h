
# ifndef fsdhc_OCON_H
# define fsdhc_OCON_H
/*
 *  The Constant Declarations for
 *  dhcpRelay
 */
# define DHCPRELAYING               (1)
# define DHCPRELAYSERVERSONLY               (2)
# define DHCPRELAYSERVERIPADDR               (3)
# define DHCPRELAYSECSTHRESHOLD               (4)
# define DHCPRELAYHOPSTHRESHOLD               (5)
# define DHCPRELAYRAIOPTIONCONTROL               (6)
# define DHCPRELAYRAICIRCUITIDSUBOPTIONCONTROL               (7)
# define DHCPRELAYRAIREMOTEIDSUBOPTIONCONTROL               (8)
# define DHCPRELAYRAISUBNETMASKSUBOPTIONCONTROL               (9)
# define DHCPRELAYRAIOPTIONINSERTED               (10)
# define DHCPRELAYRAICIRCUITIDSUBOPTIONINSERTED               (11)
# define DHCPRELAYRAIREMOTEIDSUBOPTIONINSERTED               (12)
# define DHCPRELAYRAISUBNETMASKSUBOPTIONINSERTED               (13)
# define DHCPRELAYRAIOPTIONWRONGLYSET               (14)
# define DHCPRELAYRAISPACECONSTRAINT               (15)
# define DHCPCONFIGTRACELEVEL               (16)

#endif /*  fsdhcpRelay_OCON_H  */
