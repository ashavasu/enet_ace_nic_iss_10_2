/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: dhrlprot.h,v 1.16 2017/12/14 10:23:42 siva Exp $b                     */
/*****************************************************************************/

#ifndef DHRLPROT_H
#define DHRLPROT_H
#include "dhrlport.h"



extern INT4 DhcpRelayStartTimer (tTimerListId TimerList, 
                          tTmrAppTimer * pTimer, UINT4 u4Sec);


extern INT4 DhcpRelaySendToSli (INT1 *pMessage, UINT2 u2Size, 
                                UINT4 u4DstIp, UINT2 u2DstPort, INT4 i4SockDesc);

extern INT4 DhcpRelaySendBroadcast (INT1 *pMessage, UINT2 u2Size, 
                                 UINT4 u4IfIndex, UINT4 u4Port, INT4 i4SockDesc);

extern UINT1 DhcpRelaySendUnicast (INT1 *, UINT2 , UINT4 ,  UINT4 , UINT2,
                                            INT4 i4SockDesc);
extern INT4 DhcpSendToClient(INT4 i4IfIndex, tDhcpPktInfo *pPkt, UINT1 u1BroadCast,
                                            INT4  i4SockDesc);

extern INT4 DhcpRelayProcPkt (tDhcpPktInfo *pDhcpPacket, INT4 i4SockDesc);

extern INT4 MakeClientEntry (tDhcpPktInfo *pPkt, UINT4);

/* changed prototype to remove -pedantic warnings */
extern INT4 DhcpGetInterfaceAddr ( UINT2 u2Port, UINT4 *pu4Addr);
extern INT4 DhcpGetDestIfIndex ( UINT4 u4ContextId, UINT4 u4DestAddr,  INT4 *pi4IfIndex);  
extern INT4 DhcpGetIfMtu ( INT4 i4IfIndex, UINT4 *pu4MTU ); 
extern INT4 DhcpCheckIfForwardingEnabled(VOID);
extern INT4 DhcpGetCircuitID ( tDhcpPktInfo *pPktInfo, UINT4 u4ContextId,
                                                            UINT4 *pu4CktID );
extern VOID DhcpGetRemoteID ( tDhcpPktInfo *pDhcpPacket, UINT4 u4ContextId,
                                                        UINT1 au1RemoteID[] );
extern VOID DhcpGetSubnetMask ( tDhcpPktInfo *pDhcpPacket, UINT4 *pu4SubnetMask );
    
extern INT4 DhcpCheckIfRAIOptionEnabled(UINT4);
extern INT4 DhcpCheckIfCircuitIDSubOptionEnabled(UINT4);
extern INT4 DhcpCheckIfRemoteIDSubOptionEnabled(UINT4);
extern INT4 DhcpCheckIfSubnetMaskSubOptionEnabled(UINT4);
extern INT4 DhcpCheckIfVPNIDSubOptionEnabled(UINT4);
extern VOID DhcpIncrementRAIOptionInsertedCounter(UINT4);
extern VOID DhcpIncrementCircuitIDSubOptionInsertedCounter(UINT4);
extern VOID DhcpIncrementRemoteIDSubOptionInsertedCounter(UINT4);
extern VOID DhcpIncrementSubnetMaskSubOptionInsertedCounter(UINT4);
extern VOID DhcpIncrementSpaceConstraintCounter(UINT4);
extern VOID DhcpIncrementRAIOptionWronglyInsertedCounter(UINT4);
extern VOID DhcpIncrementRaiOptionCounters(UINT4);
extern INT4 DhcpCheckIfPktIsDHCP (tDhcpPktInfo *pPkt);
extern INT4 DhcpCheckIfRAIOptionSet ( tDhcpPktInfo *pPkt );
extern INT4 DhcpCheckIfThisOptionIsSet ( tDhcpPktInfo *pPkt, UINT4 u4Option );
extern INT4 DhcpGetCircuitIDFromPacket ( tDhcpPktInfo *pPkt, UINT4 *pu4CktID );
extern VOID DhcpStripOffRAIOptions ( tDhcpPktInfo *pDhcpPacket ) ;
extern VOID DhcpAddRequiredRAIOptions ( tDhcpPktInfo *pDhcpPacket, 
                               UINT4   u4ContextId, UINT2 *pu2AddedBytes);

extern VOID DhcpRelayRemoveServerRec(UINT4 u4ContextId,tServer *pServer);
extern VOID DhcpRelayAddToFreeServer(UINT4 u4ContextId,tServer *pServer);
extern VOID DhcpRelayAddServer(UINT4 u4ContextId,tServer *);
extern INT4 DhcpRelayInitFreeServersPool(UINT4);
extern tServer   *DhcpRelayGetServer(UINT4 u4ContextId,UINT4 u4IpAddress);
extern tServer   *DhcpRelayGetFreeServerRec(UINT4);
 
VOID DhcpRelayShutdown (UINT4);
VOID DhrlDeInit (VOID);
INT1 DhcpRelayEnable (UINT4);
VOID DhcpRlPktInSocket (INT4);
INT4 DhcpRelayOpenSocket (UINT4);
INT4 DhcpRlyCloseSocket (INT4);
INT1 DhrlPacketRcvd (INT4);
INT1 DhrlRegisterWithIp (UINT4);
INT1 DhrlDeRegisterWithIp (UINT4);
INT4 DhcpRlyRegisterWithVcm (VOID);
INT4 DhcpRlyCreatePools (VOID);
INT4 DhcpRlyInitContextGlobals (UINT4);
INT4 DhcpRlyCreateContext (UINT4);
VOID DhcpRlyDeleteContext (UINT4);
VOID DhcpRlyDeInitContextGlobals (UINT4);
INT4 DhrlRBCompareIfIndex (tRBElem * , tRBElem * );
INT4 DhrlRBCompareCktId (tRBElem * , tRBElem * );
INT4 DhrlRBCompareRemoteId (tRBElem * , tRBElem * );
VOID DhrlIfStateChgHdlr (tNetIpv4IfInfo * , UINT1 );
VOID DhcpRlyDequePkt (VOID);
INT1 DhrlDequeIpPkt (VOID);
VOID DhcpRlyDequeVcmPkt (VOID);
VOID DhrlProcessIfaceDeletion (UINT4, UINT4);

extern INT4 DhcpGetFirstIfIndex( UINT4 u4ContextId,INT4 *pi4IfIndex); 
extern INT4 DhcpGetNextIfIndex ( INT4 i4IfIndex, INT4 *pi4IfIndex,UINT4 u4ContextId );

extern tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring      PROTO((INT4));

extern VOID 
free_octetstring          PROTO((tSNMP_OCTET_STRING_TYPE *));

#endif /*DHRLPROT_H*/
