 /* $Id: dhrclipt.h,v 1.8 2015/06/17 04:49:33 siva Exp $*/
#ifndef _DHRCLIPT_H
#define _DHRCLIPT_H

/* Prototype declarations for dhcp relay commands */
INT4 DhcpRelaySetServerOnly PROTO ((tCliHandle, INT4));
INT4 DhcpRelaySetService PROTO ((tCliHandle, INT4));
INT4 DhcpRelaySetServer PROTO ((tCliHandle, UINT4));
INT4 DhcpRelaySetNoServer PROTO ((tCliHandle, UINT4));
INT4 DhcpRelaySetInfoOption PROTO ((tCliHandle, INT4));
INT4 DhcpRelaySetCktId PROTO ((tCliHandle, UINT4, UINT4));
INT4 DhcpRelaySetRemId PROTO ((tCliHandle, UINT4, UINT1*, UINT1));
INT4 DhcpRelayShowInfo PROTO ((tCliHandle)); 
INT4 DhcpRelayShowIfInfo PROTO ((tCliHandle)); 
INT4 DhcpRelayShowSpecificIfInfo PROTO ((tCliHandle, INT4)); 
INT4 DhcpRelaySetDebug PROTO ((tCliHandle, INT4, UINT1));
INT4 DhcpRelayShowServer PROTO ((tCliHandle));
INT4 DhcpRelayShowRunningConfig(tCliHandle CliHandle, UINT4 u4Module);
INT4 DhrlShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4, INT4);
VOID IssDhcpRelayShowDebugging (tCliHandle, INT4);
INT4 DhcpRelaySetCircuitOption PROTO ((tCliHandle,UINT4 ));
INT4 DhcpRelaySetClearCounters PROTO ((tCliHandle));
#endif /* _DHRCLIPT_H */
