/* $Id: dhrlglob.h,v 1.15 2015/06/17 04:49:33 siva Exp $*/
#ifndef DHRLGLOB_H
#define DHRLGLOB_H

#ifdef  INCLUDE_DHCP_RELAY_GLOB_VARS 

/* Global Structure to hold the record of active relay contex nodes */
tGlobalRlyNode  gGlobalRlyNode;
/* Global Structure to hold the parameters common to entire DHCP RELAY TASK */
tRlyNode        gRlyNode;

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
VOID RegisterDHRL(VOID);
VOID RegisterFSMIDHRly (VOID);
#endif
#else 
extern tGlobalRlyNode  gGlobalRlyNode;
extern tRlyNode        gRlyNode; 
#endif 
#endif /* DHRLGLOB_H */
