/*$Id: dhrlwr.h,v 1.11 2017/12/14 10:23:42 siva Exp $*/
#ifndef _FSDHCPWR_H
#define _FSDHCPWR_H

VOID RegisterDHRL(VOID);
VOID UnRegisterDHRL(VOID);
INT4 DhcpRelayingGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayServersOnlyGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelaySecsThresholdGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayHopsThresholdGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAICircuitIDSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIRemoteIDSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAISubnetMaskSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAICircuitIDSubOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIRemoteIDSubOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAISubnetMaskSubOptionInsertedGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIOptionWronglySetGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAISpaceConstraintGet(tSnmpIndex *, tRetVal *);
INT4 DhcpConfigTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 DhcpConfigDhcpCircuitOptionGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayCounterResetGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIVPNIDSubOptionControlGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayingSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayServersOnlySet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelaySecsThresholdSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayHopsThresholdSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAICircuitIDSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIRemoteIDSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAISubnetMaskSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 DhcpConfigTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 DhcpConfigDhcpCircuitOptionSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayCounterResetSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIVPNIDSubOptionControlSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayServersOnlyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelaySecsThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayHopsThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAICircuitIDSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIRemoteIDSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAISubnetMaskSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpConfigTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpConfigDhcpCircuitOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayCounterResetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayRAIVPNIDSubOptionControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayingDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayServersOnlyDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelaySecsThresholdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayHopsThresholdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayRAIOptionControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayRAICircuitIDSubOptionControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayRAIRemoteIDSubOptionControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayRAISubnetMaskSubOptionControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpConfigTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpConfigDhcpCircuitOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayCounterResetDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpRelayRAIVPNIDSubOptionControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDhcpRelaySrvAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpRelaySrvAddressRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelaySrvAddressRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelaySrvAddressRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelaySrvAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexDhcpRelayIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpRelayIfCircuitIdGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfRemoteIdGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfCircuitIdSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfRemoteIdSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfCircuitIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfRemoteIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpRelayIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



#endif /* _FSDHCPWR_H */
