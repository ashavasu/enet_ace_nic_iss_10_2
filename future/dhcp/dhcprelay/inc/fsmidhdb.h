/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmidhdb.h,v 1.2 2017/12/14 10:23:42 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIDHDB_H
#define _FSMIDHDB_H

UINT1 FsMIDhcpContextTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDhcpRelaySrvAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIDhcpRelayIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsmidhrly [] ={1,3,6,1,4,1,29601,2,92};
tSNMP_OID_TYPE fsmidhrlyOID = {9, fsmidhrly};


UINT4 FsMIDhcpConfigGblTraceLevel [ ] ={1,3,6,1,4,1,29601,2,92,1,1};
UINT4 FsMIDhcpContextId [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,1};
UINT4 FsMIDhcpRelaying [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,2};
UINT4 FsMIDhcpRelayServersOnly [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,3};
UINT4 FsMIDhcpRelaySecsThreshold [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,4};
UINT4 FsMIDhcpRelayHopsThreshold [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,5};
UINT4 FsMIDhcpRelayRAIOptionControl [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,6};
UINT4 FsMIDhcpRelayRAICircuitIDSubOptionControl [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,7};
UINT4 FsMIDhcpRelayRAIRemoteIDSubOptionControl [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,8};
UINT4 FsMIDhcpRelayRAISubnetMaskSubOptionControl [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,9};
UINT4 FsMIDhcpRelayRAIOptionInserted [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,10};
UINT4 FsMIDhcpRelayRAICircuitIDSubOptionInserted [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,11};
UINT4 FsMIDhcpRelayRAIRemoteIDSubOptionInserted [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,12};
UINT4 FsMIDhcpRelayRAISubnetMaskSubOptionInserted [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,13};
UINT4 FsMIDhcpRelayRAIOptionWronglySet [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,14};
UINT4 FsMIDhcpRelayRAISpaceConstraint [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,15};
UINT4 FsMIDhcpConfigTraceLevel [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,16};
UINT4 FsMIDhcpConfigDhcpCircuitOption [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,17};
UINT4 FsMIDhcpRelayCounterReset [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,18};
UINT4 FsMIDhcpRelayContextRowStatus [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,19};
UINT4 FsMIDhcpRelayRAIVPNIDSubOptionControl [ ] ={1,3,6,1,4,1,29601,2,92,2,1,1,20};
UINT4 FsMIDhcpRelaySrvIpAddress [ ] ={1,3,6,1,4,1,29601,2,92,2,2,1,1};
UINT4 FsMIDhcpRelaySrvAddressRowStatus [ ] ={1,3,6,1,4,1,29601,2,92,2,2,1,2};
UINT4 FsMIDhcpRelayIfCircuitId [ ] ={1,3,6,1,4,1,29601,2,92,2,3,1,1};
UINT4 FsMIDhcpRelayIfRemoteId [ ] ={1,3,6,1,4,1,29601,2,92,2,3,1,2};
UINT4 FsMIDhcpRelayIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,92,2,3,1,3};




tMbDbEntry fsmidhrlyMibEntry[]= {

{{11,FsMIDhcpConfigGblTraceLevel}, NULL, FsMIDhcpConfigGblTraceLevelGet, FsMIDhcpConfigGblTraceLevelSet, FsMIDhcpConfigGblTraceLevelTest, FsMIDhcpConfigGblTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsMIDhcpContextId}, GetNextIndexFsMIDhcpContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpRelaying}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayingGet, FsMIDhcpRelayingSet, FsMIDhcpRelayingTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelayServersOnly}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayServersOnlyGet, FsMIDhcpRelayServersOnlySet, FsMIDhcpRelayServersOnlyTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelaySecsThreshold}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelaySecsThresholdGet, FsMIDhcpRelaySecsThresholdSet, FsMIDhcpRelaySecsThresholdTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "0"},

{{13,FsMIDhcpRelayHopsThreshold}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayHopsThresholdGet, FsMIDhcpRelayHopsThresholdSet, FsMIDhcpRelayHopsThresholdTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "4"},

{{13,FsMIDhcpRelayRAIOptionControl}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAIOptionControlGet, FsMIDhcpRelayRAIOptionControlSet, FsMIDhcpRelayRAIOptionControlTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelayRAICircuitIDSubOptionControl}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAICircuitIDSubOptionControlGet, FsMIDhcpRelayRAICircuitIDSubOptionControlSet, FsMIDhcpRelayRAICircuitIDSubOptionControlTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelayRAIRemoteIDSubOptionControl}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAIRemoteIDSubOptionControlGet, FsMIDhcpRelayRAIRemoteIDSubOptionControlSet, FsMIDhcpRelayRAIRemoteIDSubOptionControlTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelayRAISubnetMaskSubOptionControl}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAISubnetMaskSubOptionControlGet, FsMIDhcpRelayRAISubnetMaskSubOptionControlSet, FsMIDhcpRelayRAISubnetMaskSubOptionControlTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelayRAIOptionInserted}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAIOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpRelayRAICircuitIDSubOptionInserted}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAICircuitIDSubOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpRelayRAIRemoteIDSubOptionInserted}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAIRemoteIDSubOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpRelayRAISubnetMaskSubOptionInserted}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAISubnetMaskSubOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpRelayRAIOptionWronglySet}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAIOptionWronglySetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpRelayRAISpaceConstraint}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAISpaceConstraintGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDhcpContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMIDhcpConfigTraceLevel}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpConfigTraceLevelGet, FsMIDhcpConfigTraceLevelSet, FsMIDhcpConfigTraceLevelTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIDhcpConfigDhcpCircuitOption}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpConfigDhcpCircuitOptionGet, FsMIDhcpConfigDhcpCircuitOptionSet, FsMIDhcpConfigDhcpCircuitOptionTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMIDhcpRelayCounterReset}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayCounterResetGet, FsMIDhcpRelayCounterResetSet, FsMIDhcpRelayCounterResetTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelayContextRowStatus}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayContextRowStatusGet, FsMIDhcpRelayContextRowStatusSet, FsMIDhcpRelayContextRowStatusTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 1, NULL},

{{13,FsMIDhcpRelayRAIVPNIDSubOptionControl}, GetNextIndexFsMIDhcpContextTable, FsMIDhcpRelayRAIVPNIDSubOptionControlGet, FsMIDhcpRelayRAIVPNIDSubOptionControlSet, FsMIDhcpRelayRAIVPNIDSubOptionControlTest, FsMIDhcpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpContextTableINDEX, 1, 0, 0, "2"},

{{13,FsMIDhcpRelaySrvIpAddress}, GetNextIndexFsMIDhcpRelaySrvAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIDhcpRelaySrvAddressTableINDEX, 2, 0, 0, NULL},

{{13,FsMIDhcpRelaySrvAddressRowStatus}, GetNextIndexFsMIDhcpRelaySrvAddressTable, FsMIDhcpRelaySrvAddressRowStatusGet, FsMIDhcpRelaySrvAddressRowStatusSet, FsMIDhcpRelaySrvAddressRowStatusTest, FsMIDhcpRelaySrvAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpRelaySrvAddressTableINDEX, 2, 0, 1, NULL},

{{13,FsMIDhcpRelayIfCircuitId}, GetNextIndexFsMIDhcpRelayIfTable, FsMIDhcpRelayIfCircuitIdGet, FsMIDhcpRelayIfCircuitIdSet, FsMIDhcpRelayIfCircuitIdTest, FsMIDhcpRelayIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIDhcpRelayIfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIDhcpRelayIfRemoteId}, GetNextIndexFsMIDhcpRelayIfTable, FsMIDhcpRelayIfRemoteIdGet, FsMIDhcpRelayIfRemoteIdSet, FsMIDhcpRelayIfRemoteIdTest, FsMIDhcpRelayIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIDhcpRelayIfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIDhcpRelayIfRowStatus}, GetNextIndexFsMIDhcpRelayIfTable, FsMIDhcpRelayIfRowStatusGet, FsMIDhcpRelayIfRowStatusSet, FsMIDhcpRelayIfRowStatusTest, FsMIDhcpRelayIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDhcpRelayIfTableINDEX, 2, 0, 1, NULL},
};
tMibData fsmidhrlyEntry = { 26, fsmidhrlyMibEntry };

#endif /* _FSMIDHDB_H */

