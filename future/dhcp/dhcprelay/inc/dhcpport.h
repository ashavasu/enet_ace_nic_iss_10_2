/*$Id: dhcpport.h,v 1.10 2015/06/17 04:49:33 siva Exp $*/
#ifndef DHCPCOMM_INC_DHCPPORT_H
#define DHCPCOMM_INC_DHCPPORT_H


#define DHCP_BUF_CHAIN_HEADER       tCRU_BUF_CHAIN_HEADER
#define DHCP_UDP_TO_APP_MSG_PARMS   t_UDP_TO_APP_MSG_PARMS

#define BOOTP_REQUEST              1
#define BOOTP_REPLY                2

#define DhcpBufCopyFromBufChain     CRU_BUF_Copy_FromBufChain
#define DhcpBufCopyOverBufChain     CRU_BUF_Copy_OverBufChain
#define DhcpBufAllocateMsgBufChain  CRU_BUF_Allocate_MsgBufChain
#define DhcpBufDuplicateBufChain    CRU_BUF_Duplicate_BufChain
#define DhcpBufReleaseMsgBufChain   CRU_BUF_Release_MsgBufChain

#define DhcpBufInitializeManager    CRU_BUF_Initialize_Manager
#define DhcpAllocActiveTimerList    CRU_Allocate_Active_TimerList
#define DhcpAllocFreeTimerBlockPool CRU_Allocate_Free_TimerBlockPool
#define DhcpBufChainHeader          tCRU_BUF_CHAIN_HEADER
#define DhcpStartLibTimer           CRU_Start_Timer
#define DhcpStopLibTimer            CRU_Stop_Timer



#define DhcpGetValidByteCount       CRU_BUF_Get_ChainValidByteCount
#define DhcpProcessTimerExpiry CRU_Process_Timer_Expiry

#define DHCP_CREATE_TMR_LIST(au1TaskName, u4Event, pTmrListId)         \
   TmrCreateTimerList((au1TaskName), (u4Event), NULL, (pTmrListId))

#define DHCP_OK                     TMO_OK
#define DHCP_NOT_OK                 TMO_NOT_OK
#define DHCP_OR                     TMO_OR


#define DHCPRLY_DUMMY(x)   (x=x)


#define DHCP_ALLOC_IF_CONFIG_POOL(pu1Blk)              \
   (pu1Blk = (tDhrlIfConfigInfo *)MemAllocMemBlk(DHCP_RLY_IF_POOL_ID))
#define DHCP_RELEASE_IF_CONFIG_POOL(pu1Blk) \
   MemReleaseMemBlock(DHCP_RLY_IF_POOL_ID, (UINT1 *)pu1Blk)

#define DHCP_ALLOC_SERVER_POOL(pu1Blk) \
    (pu1Blk = (tServer *) MemAllocMemBlk(DHCP_RLY_SERVER_POOL_ID))

typedef tMemPoolId tDhcpMemPoolId;
#define DHCP_MEM_SUCCESS          MEM_SUCCESS
#define DHCP_MEM_FAILURE          MEM_FAILURE

#define DHCPR_PROT_MUTEX_SEMA4          ((const UINT1 *)"DRPS")
#define DHCPR_SEM_CREATE_INIT_CNT       1
#define DHCPR_SEM_FLAGS                  OSIX_DEFAULT_SEM_MODE
/* end of Mutex related definitions*/

#define  DHCP_HTONS(x) OSIX_HTONS(x)       
#define  DHCP_NTOHS(x) OSIX_NTOHS(x)       
#define  DHCP_HTONL(x) OSIX_HTONL(x)       
#define  DHCP_NTOHL(x) OSIX_NTOHL(x)       

#endif /*DHCPCOMM_INC_DHCPPORT_H*/
