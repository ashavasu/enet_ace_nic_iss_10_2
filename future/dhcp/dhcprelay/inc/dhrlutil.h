/*****************************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: dhrlutil.h,v 1.3 2015/06/17 04:49:35 siva Exp $
 ******************************************************************************/
#ifndef DHRLUTIL_H
#define DHRLUTIL_H

PUBLIC INT4 DhrlUtilGetIfCiruitId(UINT4,INT4, UINT4 *);
PUBLIC INT4 DhcpRlySetContextId (UINT4);
PUBLIC VOID DhcpRlyUtilDelRBTree (UINT4);
PUBLIC INT4 DhcpRlyUtilRBFreeInterface (tRBElem *, UINT4);
VOID   DhcpRlyResetContextId (VOID);
INT4   DhcpRlyGetFirstActiveContextID (UINT4*);
INT4   DhcpRlyGetNextActiveContextID (UINT4, UINT4*) ;
INT4   DhcpRlyGetNextContextId (UINT4, UINT4*);
    
#endif /* DHRLUTIL_H */
