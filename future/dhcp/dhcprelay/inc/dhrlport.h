/* $Id: dhrlport.h,v 1.9 2015/06/17 04:49:35 siva Exp $*/
#ifndef DHRLPORT_H
#define DHRLPORT_H



#define DHCP_PKT_EVENT 0x00000002
#define DHCP_PKT_Q DHRL_UDP_INPUT_Q_NAME
#define DHRL_UDP_INPUT_Q_NAME "DRLQ"
#define DHRL_TASK_NAME (UINT1 *)"DHRL"
#define DHCP_Q_DEPTH 10
#define DHCP_Q_MODE OSIX_LOCAL
#define DHCP_Q_ID gDhrlQId
#define DHCP_PARMS_GET_UDP_TO_APP_MSG_PARMS(pBuf)\
         (t_UDP_TO_APP_MSG_PARMS *) &(pBuf->ModuleData)
#define DhcpGetPktSizeFromParms(pParms) \
                          pParms->u2Len
    

#define DhcpGetUdpToAppParms DHCP_PARMS_GET_UDP_TO_APP_MSG_PARMS
#define DhcpGetIfNumFromParms IP_PARMS_GET_IFACE_FROM_UDP_TO_APP_MSG_PARMS
#define DhcpReleaseParms(pParms) IP_PARMS_RELEASE_UDP_TO_APP_MSG_PARMS(pParms)
#define DhcpCheckIfMyAddr(u4ContextId,u4Addr) NetIpv4IfIsOurAddressInCxt (u4ContextId,u4Addr)
#define DHCP_SPRINTF SPRINTF 
#define DHCP_STRLEN STRLEN 
#endif /*DHRLPORT.H*/
