/*$Id: dhrlsz.h,v 1.5 2015/06/17 04:49:35 siva Exp $*/
enum {
    MAX_DHRL_FREE_SERVER_POOL_SIZING_ID,
    MAX_DHRL_INTERFACE_SIZING_ID,
    MAX_DHRL_PKT_INFO_SIZE_SIZING_ID,
    MAX_DHRL_CLNT_INFO_POOL_SIZING_ID,
    MAX_DHRL_RLY_CXT_POOL_SIZING_ID,
    DHRL_MAX_SIZING_ID
};


#ifdef  _DHRLSZ_C
tMemPoolId DHRLMemPoolIds[ DHRL_MAX_SIZING_ID];
INT4  DhrlSizingMemCreateMemPools(VOID);
VOID  DhrlSizingMemDeleteMemPools(VOID);
INT4  DhrlSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _DHRLSZ_C  */
extern tMemPoolId DHRLMemPoolIds[ ];
extern INT4  DhrlSizingMemCreateMemPools(VOID);
extern VOID  DhrlSizingMemDeleteMemPools(VOID);
extern INT4  DhrlSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _DHRLSZ_C  */


#ifdef  _DHRLSZ_C
tFsModSizingParams FsDHRLSizingParams [] = {
{ "tDhrlMaxServer", "MAX_DHRL_FREE_SERVER_POOL", sizeof(tDhrlMaxServer),MAX_DHRL_FREE_SERVER_POOL, MAX_DHRL_FREE_SERVER_POOL,0 },
{ "tDhrlIfConfigInfo", "MAX_DHRL_INTERFACE", sizeof(tDhrlIfConfigInfo),MAX_DHRL_INTERFACE, MAX_DHRL_INTERFACE,0 },
{ "tDhcpPktInfo", "MAX_DHRL_PKT_INFO_SIZE", sizeof(tDhcpPktInfo),MAX_DHRL_PKT_INFO_SIZE, MAX_DHRL_PKT_INFO_SIZE,0 },
{ "tclientinfo", "MAX_DHRL_CLNT_INFO_POOL", sizeof(tclientinfo),MAX_DHRL_CLNT_INFO_POOL, MAX_DHRL_CLNT_INFO_POOL,0 },
{ "tRlyCxtNode", "MAX_DHRL_RLY_CXT", sizeof(tRlyCxtNode),MAX_DHRL_RLY_CXT,MAX_DHRL_RLY_CXT,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _DHRLSZ_C  */
extern tFsModSizingParams FsDHRLSizingParams [];
#endif /*  _DHRLSZ_C  */


