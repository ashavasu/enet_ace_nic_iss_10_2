 /* $Id: dhrldefn.h,v 1.16 2015/06/17 04:49:33 siva Exp $*/
#ifndef DHRLDEFN_H
#define DHRLDEFN_H

#define MAX_SERVERS           5
#define DHCPRL_BROADCAST_FLAG 0x01
#define FINAL_PORT -1
#define DEFAULT_SEC_THRESHOLD 0
#define DEFAULT_HOPS_THRESHOLD 4
#define DHCP_RLY_INVALID_IP_ADDR 0
#define MAX_CLIENT_INFO 20
#define DHCP_RLY_DEFAULT_TRACE_LEVEL 0x01

#define DHRL_MIN_L3_IF_INDEX             1 
#ifdef LA_WANTED
#define DHRL_MAX_L3_IF_INDEX             (SYS_DEF_MAX_PHYSICAL_INTERFACES\
                                         +LA_MAX_AGG +\
                                         IP_DEV_MAX_L3VLAN_INTF)
#else
#define DHRL_MAX_L3_IF_INDEX             (SYS_DEF_MAX_PHYSICAL_INTERFACES\
                                         + IP_DEV_MAX_L3VLAN_INTF)
#endif

#define DHCP_LIMITED_BROADCAST_IPADDR 0xffffffff

#define DHRL_TASK_ID  gi4DhcpTaskId

#define DHRL_PKT_RECV_EVENT    0x00000001
#define DHRL_IF_DELETE_EVENT   0x00000010
#define DHRL_VCM_EVENT         0x00000100


#define DHCPRL_SELECT_TIMEOUT_VAL        1
#define DHCP_RELAY_TASK_PRIORITY         80

#define  DHRL_IS_ADDR_CLASS_A(u4Addr)  ((u4Addr & 0x80000000) == 0)          
#define  DHRL_IS_ADDR_CLASS_B(u4Addr)  ((u4Addr & 0xc0000000) == 0x80000000) 
#define  DHRL_IS_ADDR_CLASS_C(u4Addr)  ((u4Addr & 0xe0000000) == 0xc0000000)

#define DHCP_DEF_RELAYING         DHCP_DISABLE
#define DHCP_DEF_RAI_OPTIONCTRL   DHCP_DISABLE

#define   DHRL_RB_GREATER 1
#define   DHRL_RB_EQUAL   0
#define   DHRL_RB_LESS   -1

#define   IP_DHRL_QUEUE            (UINT1 *) "IpDhrl"
#define   DHRL_QUEUE_DEPTH         10
#define   DHRL_ID                  67
#define   DHRL_IFACE_DEL_PKT       1

#define DHCP_RESET                 1
#define DHCP_NO_RESET              2

/*------------------------------------------------------------------------*/
#define DHCP_RLY_INV_SOCKET_ID      (-1)
#define DHCP_RLY_DFLT_CXT_ID        (0)
#define DHCP_RLY_INVALID_CXT_ID     (0xFFFFFFFF)
/* Queue to process the received Dhcp Relay Packet */
#define DHRL_PKT_QUEUE              (UINT1 *) "PkDr"

#define DHCP_RLY_TRC_FLAG           (gRlyNode.u4DhcpTrace)
#define DHCP_RLY_SEM_ID             (gRlyNode.DhcpRSemId)
#define DHCP_RLY_TSK_ID             (gRlyNode.DhcpTaskId)
#define DHCP_RLY_QUE_ID             (gRlyNode.DhrlQId)
#define DHCP_RLY_IP_QUE_ID          (gRlyNode.IpDhrlQId)
#define DHCP_RLY_SERVER_POOL_ID     (gRlyNode.ServerPoolId)
#define DHCP_RLY_IF_POOL_ID         (gRlyNode.IfConfigPoolId)
#define DHCP_RLY_PKT_INFO_POOL_ID   (gRlyNode.PktInfoId)
#define DHCP_RLY_CLNT_INFO_POOL_ID  (gRlyNode.ClntInfoPoolId)
#define DHCP_RLY_CXT_POOL_ID        (gRlyNode.RlyCxtPoolId)
#define DHCP_RLY_TASK_INIT_STATUS   (gRlyNode.u1DhcpRlyTaskInit)
#define DHCP_RLY_SERVER_POOL_CNT    (gRlyNode.u4ServerPoolCnt)
#define DHCP_RLY_IF_POOL_CNT        (gRlyNode.u4IfConfigPoolCnt)
#define DHCP_RLY_PKT_INFO_POOL_CNT  (gRlyNode.u4PktInfoPoolCnt)
#define DHCP_RLY_CLNT_INFO_POOL_CNT (gRlyNode.u4ClntInfoPoolCnt)
#define DHCP_RLY_CXT_POOL_CNT       (gRlyNode.u4RlyCxtPoolCnt)

#define DHCP_RLY_CXT_NODE           gGlobalRlyNode.apRelayCxtNode

/*------------------------------------------------------------------------*/

#endif /* DHRLDEFN_H */
