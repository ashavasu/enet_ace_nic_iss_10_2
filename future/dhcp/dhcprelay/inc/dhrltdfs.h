/*$Id: dhrltdfs.h,v 1.11 2017/12/14 10:23:42 siva Exp $*/
#ifndef DHRLTDFS_H
#define DHRLTDFS_H


typedef struct Server {
   UINT4 IpAddr;
   UINT1 u1Status;
   UINT1 au1Padding[3]; /* For structure alignment */
   struct Server *pNext;
} tServer;


/* Added for including the information base for
     association of portno's and client packets. */
/* 28/8/98 - harsha  */
typedef struct cliinfo {
  UINT4 u4Xid;
  UINT1  u1Chaddr[DHCP_HARD_ADD_LEN];   /* Client Hardware address */
  UINT2 u2Port;
  UINT1 u1Used;
  UINT1 u1Padding;   /* For structure alignment */
} tclientinfo;

/* Structure to process IP message */
typedef struct DhcprIpMsg 
{
    UINT4    u4IfIndex;
    UINT4    u4CxtId;
    UINT4    u4Cmd;
}tDhcpRIpMsg;

typedef struct DhrlIfConfigInfo {
    tRBNodeEmbd  IfToIdRBNode;           /*Embedded RBTree node for RBTree that
                                           hols interface to id mapping */
    tRBNodeEmbd  CktIdToIfRBNode;        /*Embedded RBTree node for RBTree that
                                           hols ckt id to interface mapping*/
    tRBNodeEmbd  RemIdToIfRBNode;        /*Embedded RBTree node for RBTree that
                                           hols remote id to interface 
                                           mapping */
    UINT4 u4Port;                         /* L3 interface index */
    UINT4 u4CxtId;
    UINT4 u4CircuitId;                    /* Configured circuit id */
    UINT1 au1RemoteId[DHRL_MAX_RID_LEN];  /* Configured remote id */
    UINT1 u1RowStatus;                    /* Row status value */
    UINT1 u1AlignByte [3];
} tDhrlIfConfigInfo;

typedef struct DhrlMaxServer {
     tServer    aDhrlMaxServer[MAX_SERVERS];
}tDhrlMaxServer;

/* Structure to process the DHCP Relay packet per context basis */
typedef struct DhcprPktMsg
{
    INT4    i4SockDesc;
}tDhcpRPktMsg;

/* This structure holds the parameters common for all VRF instances 
 * of DHCP Relay task */
typedef struct RlyNode {
        /* DHCP RLY Task ID */
        tOsixTaskId     DhcpTaskId;
        /* DHCP RLY QUEUE ID */
        tOsixQId        DhrlQId;
        tOsixQId        IpDhrlQId;
        /* Seamaphore ID */
        tOsixSemId      DhcpRSemId;
        /* Mempool IDs */
        tDhcpMemPoolId  ServerPoolId;
        tDhcpMemPoolId  IfConfigPoolId;
        tDhcpMemPoolId  PktInfoId;
        tDhcpMemPoolId  ClntInfoPoolId;
        tDhcpMemPoolId  RlyCxtPoolId;
        /* Flag to control the DHCP traces globally */
        UINT4   u4DhcpTrace;
#ifdef IP_WANTED 
        /* For FSIP creating opening one socket is enough */
        INT4            i4DhcpRelaySockDesc;      /* Socket Discriptor */
#endif
}tRlyNode;

/* Structures added for DHCPRELAY MI support */
typedef struct RlyCxtNode {
        tRBTree         pIfToIdRBRoot;
        tRBTree         pCktIdToIfRBRoot;
        tRBTree         pRemoteIdToIfRBRoot;
        /*Following variables are added to support Multiple servers in RA*/
        tServer         *pFreeServersPool; /* start address of the mempool block */
        tServer         *pServersIp; /* List of Servers configured for this RA */
        tServer         *pFreeServersIp;/*List of Free records which can be used
                               for server configurations*/
        tclientinfo     *apClntInfo [MAX_CLIENT_INFO];
        UINT4           u4CxtId; /* Key of this DS */
        UINT4           u4CfRelaying;/* The relay agent is start or shutdown */
        UINT4           u4CfSecsThresh;
        UINT4           u4CfHopsThresh;
        /* Whether to forward to Configured servers only */
        UINT4           u4CfdhcpRelayFwdServersOnly;
#ifdef RELAY_AGNT_INFO
        UINT4           u4CfRAIOption;
        UINT4           u4CfRAICircuitIDSubOption;
        UINT4           u4CfRAIRemoteIDSubOption;
        UINT4           u4CfRAISubnetMaskSubOption;
        UINT4           u4StatRAIOptionInserted;
        UINT4           u4StatRAICircuitIDSubOptionInserted;
        UINT4           u4StatRAIRemoteIDSubOptionInserted;
        UINT4           u4StatRAISubnetMaskSubOptionInserted;
        UINT4           u4StatRAIOptionWronglyInserted;
        UINT4           u4StatRAISpaceConstraint;
        UINT4           u4CfRAICktIDDefaultOptiontype;
        UINT4           u4CfRAIVPNIDSubOption;
#endif
        UINT4           u4DhcpCountResetCounters;
        UINT4           u4DhcpCxtTrace;
#ifdef LNXIP4_WANTED
        /* For Linux IP need to create a socket for each context */
        INT4            i4DhcpRelaySockDesc;      /* Socket Discriptor */
#endif
     UINT1  u1Status;
        UINT1       au1Padding[3];
}tRlyCxtNode;

typedef struct GlobalRlyNode {
        tRlyCxtNode *apRelayCxtNode [SYS_DEF_MAX_NUM_CONTEXTS];
        UINT4       u4CurrCxtId; /* Used for Configuration */
        UINT4       u4ActiveCxtCnt; /* Useful for Tabular Object Walk */
}tGlobalRlyNode;

typedef struct DhcpRlyVcmParams {
        UINT4   u4ContextId;
        UINT1   u1Event;
        UINT1   au1Padding[3];
}tDhcpRlyVcmParams;

#endif /*DHRLTDFS_H*/

