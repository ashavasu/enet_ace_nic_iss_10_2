/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dhrldb.h,v 1.12 2017/12/14 10:23:41 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDHCPDB_H
#define _FSDHCPDB_H

UINT1 DhcpRelaySrvAddressTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DhcpRelayIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsdhcp [] ={1,3,6,1,4,1,2076,24};
tSNMP_OID_TYPE fsdhcpOID = {8, fsdhcp};


UINT4 DhcpRelaying [ ] ={1,3,6,1,4,1,2076,24,1,1};
UINT4 DhcpRelayServersOnly [ ] ={1,3,6,1,4,1,2076,24,1,2};
UINT4 DhcpRelaySecsThreshold [ ] ={1,3,6,1,4,1,2076,24,1,3};
UINT4 DhcpRelayHopsThreshold [ ] ={1,3,6,1,4,1,2076,24,1,4};
UINT4 DhcpRelayRAIOptionControl [ ] ={1,3,6,1,4,1,2076,24,1,5};
UINT4 DhcpRelayRAICircuitIDSubOptionControl [ ] ={1,3,6,1,4,1,2076,24,1,6};
UINT4 DhcpRelayRAIRemoteIDSubOptionControl [ ] ={1,3,6,1,4,1,2076,24,1,7};
UINT4 DhcpRelayRAISubnetMaskSubOptionControl [ ] ={1,3,6,1,4,1,2076,24,1,8};
UINT4 DhcpRelayRAIOptionInserted [ ] ={1,3,6,1,4,1,2076,24,1,9};
UINT4 DhcpRelayRAICircuitIDSubOptionInserted [ ] ={1,3,6,1,4,1,2076,24,1,10};
UINT4 DhcpRelayRAIRemoteIDSubOptionInserted [ ] ={1,3,6,1,4,1,2076,24,1,11};
UINT4 DhcpRelayRAISubnetMaskSubOptionInserted [ ] ={1,3,6,1,4,1,2076,24,1,12};
UINT4 DhcpRelayRAIOptionWronglySet [ ] ={1,3,6,1,4,1,2076,24,1,13};
UINT4 DhcpRelayRAISpaceConstraint [ ] ={1,3,6,1,4,1,2076,24,1,14};
UINT4 DhcpConfigTraceLevel [ ] ={1,3,6,1,4,1,2076,24,1,15};
UINT4 DhcpConfigDhcpCircuitOption [ ] ={1,3,6,1,4,1,2076,24,1,16};
UINT4 DhcpRelayCounterReset [ ] ={1,3,6,1,4,1,2076,24,1,17};
UINT4 DhcpRelayRAIVPNIDSubOptionControl [ ] ={1,3,6,1,4,1,2076,24,1,18};
UINT4 DhcpRelaySrvIpAddress [ ] ={1,3,6,1,4,1,2076,24,2,1,1,1};
UINT4 DhcpRelaySrvAddressRowStatus [ ] ={1,3,6,1,4,1,2076,24,2,1,1,2};
UINT4 DhcpRelayIfCircuitId [ ] ={1,3,6,1,4,1,2076,24,2,2,1,1};
UINT4 DhcpRelayIfRemoteId [ ] ={1,3,6,1,4,1,2076,24,2,2,1,2};
UINT4 DhcpRelayIfRowStatus [ ] ={1,3,6,1,4,1,2076,24,2,2,1,3};




tMbDbEntry fsdhcpMibEntry[]= {

{{10,DhcpRelaying}, NULL, DhcpRelayingGet, DhcpRelayingSet, DhcpRelayingTest, DhcpRelayingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelayServersOnly}, NULL, DhcpRelayServersOnlyGet, DhcpRelayServersOnlySet, DhcpRelayServersOnlyTest, DhcpRelayServersOnlyDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelaySecsThreshold}, NULL, DhcpRelaySecsThresholdGet, DhcpRelaySecsThresholdSet, DhcpRelaySecsThresholdTest, DhcpRelaySecsThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,DhcpRelayHopsThreshold}, NULL, DhcpRelayHopsThresholdGet, DhcpRelayHopsThresholdSet, DhcpRelayHopsThresholdTest, DhcpRelayHopsThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{10,DhcpRelayRAIOptionControl}, NULL, DhcpRelayRAIOptionControlGet, DhcpRelayRAIOptionControlSet, DhcpRelayRAIOptionControlTest, DhcpRelayRAIOptionControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelayRAICircuitIDSubOptionControl}, NULL, DhcpRelayRAICircuitIDSubOptionControlGet, DhcpRelayRAICircuitIDSubOptionControlSet, DhcpRelayRAICircuitIDSubOptionControlTest, DhcpRelayRAICircuitIDSubOptionControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelayRAIRemoteIDSubOptionControl}, NULL, DhcpRelayRAIRemoteIDSubOptionControlGet, DhcpRelayRAIRemoteIDSubOptionControlSet, DhcpRelayRAIRemoteIDSubOptionControlTest, DhcpRelayRAIRemoteIDSubOptionControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelayRAISubnetMaskSubOptionControl}, NULL, DhcpRelayRAISubnetMaskSubOptionControlGet, DhcpRelayRAISubnetMaskSubOptionControlSet, DhcpRelayRAISubnetMaskSubOptionControlTest, DhcpRelayRAISubnetMaskSubOptionControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelayRAIOptionInserted}, NULL, DhcpRelayRAIOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpRelayRAICircuitIDSubOptionInserted}, NULL, DhcpRelayRAICircuitIDSubOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpRelayRAIRemoteIDSubOptionInserted}, NULL, DhcpRelayRAIRemoteIDSubOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpRelayRAISubnetMaskSubOptionInserted}, NULL, DhcpRelayRAISubnetMaskSubOptionInsertedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpRelayRAIOptionWronglySet}, NULL, DhcpRelayRAIOptionWronglySetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpRelayRAISpaceConstraint}, NULL, DhcpRelayRAISpaceConstraintGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpConfigTraceLevel}, NULL, DhcpConfigTraceLevelGet, DhcpConfigTraceLevelSet, DhcpConfigTraceLevelTest, DhcpConfigTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,DhcpConfigDhcpCircuitOption}, NULL, DhcpConfigDhcpCircuitOptionGet, DhcpConfigDhcpCircuitOptionSet, DhcpConfigDhcpCircuitOptionTest, DhcpConfigDhcpCircuitOptionDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,DhcpRelayCounterReset}, NULL, DhcpRelayCounterResetGet, DhcpRelayCounterResetSet, DhcpRelayCounterResetTest, DhcpRelayCounterResetDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpRelayRAIVPNIDSubOptionControl}, NULL, DhcpRelayRAIVPNIDSubOptionControlGet, DhcpRelayRAIVPNIDSubOptionControlSet, DhcpRelayRAIVPNIDSubOptionControlTest, DhcpRelayRAIVPNIDSubOptionControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,DhcpRelaySrvIpAddress}, GetNextIndexDhcpRelaySrvAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DhcpRelaySrvAddressTableINDEX, 1, 0, 0, NULL},

{{12,DhcpRelaySrvAddressRowStatus}, GetNextIndexDhcpRelaySrvAddressTable, DhcpRelaySrvAddressRowStatusGet, DhcpRelaySrvAddressRowStatusSet, DhcpRelaySrvAddressRowStatusTest, DhcpRelaySrvAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpRelaySrvAddressTableINDEX, 1, 0, 1, NULL},

{{12,DhcpRelayIfCircuitId}, GetNextIndexDhcpRelayIfTable, DhcpRelayIfCircuitIdGet, DhcpRelayIfCircuitIdSet, DhcpRelayIfCircuitIdTest, DhcpRelayIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DhcpRelayIfTableINDEX, 1, 0, 0, NULL},

{{12,DhcpRelayIfRemoteId}, GetNextIndexDhcpRelayIfTable, DhcpRelayIfRemoteIdGet, DhcpRelayIfRemoteIdSet, DhcpRelayIfRemoteIdTest, DhcpRelayIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpRelayIfTableINDEX, 1, 0, 0, NULL},

{{12,DhcpRelayIfRowStatus}, GetNextIndexDhcpRelayIfTable, DhcpRelayIfRowStatusGet, DhcpRelayIfRowStatusSet, DhcpRelayIfRowStatusTest, DhcpRelayIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpRelayIfTableINDEX, 1, 0, 1, NULL},
};
tMibData fsdhcpEntry = { 23, fsdhcpMibEntry };

#endif /* _FSDHCPDB_H */

