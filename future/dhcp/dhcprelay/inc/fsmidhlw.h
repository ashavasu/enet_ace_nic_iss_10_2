/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmidhlw.h,v 1.2 2017/12/14 10:23:42 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDhcpConfigGblTraceLevel ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDhcpConfigGblTraceLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDhcpConfigGblTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDhcpConfigGblTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDhcpContextTable. */
INT1
nmhValidateIndexInstanceFsMIDhcpContextTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDhcpContextTable  */

INT1
nmhGetFirstIndexFsMIDhcpContextTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDhcpContextTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDhcpRelaying ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayServersOnly ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelaySecsThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayHopsThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayRAIOptionControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayRAICircuitIDSubOptionControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayRAIOptionInserted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpRelayRAICircuitIDSubOptionInserted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionInserted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionInserted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpRelayRAIOptionWronglySet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpRelayRAISpaceConstraint ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpConfigTraceLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpConfigDhcpCircuitOption ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIDhcpRelayCounterReset ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDhcpRelayContextRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIDhcpRelayRAIVPNIDSubOptionControl ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDhcpRelaying ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayServersOnly ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelaySecsThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayHopsThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayRAIOptionControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayRAICircuitIDSubOptionControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpConfigTraceLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpConfigDhcpCircuitOption ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIDhcpRelayCounterReset ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayContextRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDhcpRelayRAIVPNIDSubOptionControl ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDhcpRelaying ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayServersOnly ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelaySecsThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayHopsThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayRAIOptionControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayRAICircuitIDSubOptionControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayRAIRemoteIDSubOptionControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayRAISubnetMaskSubOptionControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpConfigTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpConfigDhcpCircuitOption ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIDhcpRelayCounterReset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayContextRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDhcpRelayRAIVPNIDSubOptionControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDhcpContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDhcpRelaySrvAddressTable. */
INT1
nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDhcpRelaySrvAddressTable  */

INT1
nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDhcpRelaySrvAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDhcpRelaySrvAddressRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDhcpRelaySrvAddressRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDhcpRelaySrvAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDhcpRelaySrvAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDhcpRelayIfTable. */
INT1
nmhValidateIndexInstanceFsMIDhcpRelayIfTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDhcpRelayIfTable  */

INT1
nmhGetFirstIndexFsMIDhcpRelayIfTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDhcpRelayIfTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDhcpRelayIfCircuitId ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIDhcpRelayIfRemoteId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIDhcpRelayIfRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDhcpRelayIfCircuitId ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMIDhcpRelayIfRemoteId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIDhcpRelayIfRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDhcpRelayIfCircuitId ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMIDhcpRelayIfRemoteId ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIDhcpRelayIfRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDhcpRelayIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
