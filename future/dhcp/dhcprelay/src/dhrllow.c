/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved 
*
* $Id: dhrllow.c,v 1.39 2017/12/14 10:23:42 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "dhcpincs.h"
# include  "lr.h"
# include  "fssnmp.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpRelaying
 Input       :  The Indices

                The Object 
                retValDhcpRelaying
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelaying (INT4 *pi4RetValDhcpRelaying)
{

    return (nmhGetFsMIDhcpRelaying (DHCP_RLY_DFLT_CXT_ID,
                                    pi4RetValDhcpRelaying));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayServersOnly
 Input       :  The Indices

                The Object 
                retValDhcpRelayServersOnly
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayServersOnly (INT4 *pi4RetValDhcpRelayServersOnly)
{
    return (nmhGetFsMIDhcpRelayServersOnly (DHCP_RLY_DFLT_CXT_ID,
                                            pi4RetValDhcpRelayServersOnly));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelaySecsThreshold
 Input       :  The Indices

                The Object 
                retValDhcpRelaySecsThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelaySecsThreshold (INT4 *pi4RetValDhcpRelaySecsThreshold)
{
    return (nmhGetFsMIDhcpRelaySecsThreshold (DHCP_RLY_DFLT_CXT_ID,
                                              pi4RetValDhcpRelaySecsThreshold));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayHopsThreshold
 Input       :  The Indices

                The Object 
                retValDhcpRelayHopsThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayHopsThreshold (INT4 *pi4RetValDhcpRelayHopsThreshold)
{
    return (nmhGetFsMIDhcpRelayHopsThreshold (DHCP_RLY_DFLT_CXT_ID,
                                              pi4RetValDhcpRelayHopsThreshold));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAIOptionControl
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAIOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAIOptionControl (INT4 *pi4RetValDhcpRelayRAIOptionControl)
{
    return (nmhGetFsMIDhcpRelayRAIOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                                 pi4RetValDhcpRelayRAIOptionControl));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAICircuitIDSubOptionControl
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAICircuitIDSubOptionControl (INT4
                                             *pi4RetValDhcpRelayRAICircuitIDSubOptionControl)
{
    return (nmhGetFsMIDhcpRelayRAICircuitIDSubOptionControl
            (DHCP_RLY_DFLT_CXT_ID,
             pi4RetValDhcpRelayRAICircuitIDSubOptionControl));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAIRemoteIDSubOptionControl
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAIRemoteIDSubOptionControl (INT4
                                            *pi4RetValDhcpRelayRAIRemoteIDSubOptionControl)
{
    return (nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionControl
            (DHCP_RLY_DFLT_CXT_ID,
             pi4RetValDhcpRelayRAIRemoteIDSubOptionControl));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAISubnetMaskSubOptionControl
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAISubnetMaskSubOptionControl (INT4
                                              *pi4RetValDhcpRelayRAISubnetMaskSubOptionControl)
{
    return (nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionControl
            (DHCP_RLY_DFLT_CXT_ID,
             pi4RetValDhcpRelayRAISubnetMaskSubOptionControl));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAIVPNIDSubOptionControl
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAIVPNIDSubOptionControl (INT4
                                         *pi4RetValDhcpRelayRAIVPNIDSubOptionControl)
{
    return (nmhGetFsMIDhcpRelayRAIVPNIDSubOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                                         pi4RetValDhcpRelayRAIVPNIDSubOptionControl));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAIOptionInserted
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAIOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAIOptionInserted (UINT4 *pu4RetValDhcpRelayRAIOptionInserted)
{
    return (nmhGetFsMIDhcpRelayRAIOptionInserted (DHCP_RLY_DFLT_CXT_ID,
                                                  pu4RetValDhcpRelayRAIOptionInserted));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAICircuitIDSubOptionInserted
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAICircuitIDSubOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAICircuitIDSubOptionInserted (UINT4
                                              *pu4RetValDhcpRelayRAICircuitIDSubOptionInserted)
{
    return (nmhGetFsMIDhcpRelayRAICircuitIDSubOptionInserted
            (DHCP_RLY_DFLT_CXT_ID,
             pu4RetValDhcpRelayRAICircuitIDSubOptionInserted));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAIRemoteIDSubOptionInserted
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAIRemoteIDSubOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAIRemoteIDSubOptionInserted (UINT4
                                             *pu4RetValDhcpRelayRAIRemoteIDSubOptionInserted)
{
    return (nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionInserted
            (DHCP_RLY_DFLT_CXT_ID,
             pu4RetValDhcpRelayRAIRemoteIDSubOptionInserted));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAISubnetMaskSubOptionInserted
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAISubnetMaskSubOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAISubnetMaskSubOptionInserted (UINT4
                                               *pu4RetValDhcpRelayRAISubnetMaskSubOptionInserted)
{
    return (nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionInserted
            (DHCP_RLY_DFLT_CXT_ID,
             pu4RetValDhcpRelayRAISubnetMaskSubOptionInserted));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAIOptionWronglySet
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAIOptionWronglySet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAIOptionWronglySet (UINT4
                                    *pu4RetValDhcpRelayRAIOptionWronglySet)
{
    return (nmhGetFsMIDhcpRelayRAIOptionWronglySet (DHCP_RLY_DFLT_CXT_ID,
                                                    pu4RetValDhcpRelayRAIOptionWronglySet));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayRAISpaceConstraint
 Input       :  The Indices

                The Object 
                retValDhcpRelayRAISpaceConstraint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayRAISpaceConstraint (UINT4 *pu4RetValDhcpRelayRAISpaceConstraint)
{
    return (nmhGetFsMIDhcpRelayRAISpaceConstraint (DHCP_RLY_DFLT_CXT_ID,
                                                   pu4RetValDhcpRelayRAISpaceConstraint));
}

/****************************************************************************
 Function    :  nmhGetDhcpConfigTraceLevel
 Input       :  The Indices

                The Object 
                retValDhcpConfigTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpConfigTraceLevel (INT4 *pi4RetValDhcpConfigTraceLevel)
{
    return (nmhGetFsMIDhcpConfigTraceLevel (DHCP_RLY_DFLT_CXT_ID,
                                            pi4RetValDhcpConfigTraceLevel));
}

/****************************************************************************
 Function    :  nmhGetDhcpConfigDhcpCircuitOption
 Input       :  The Indices

                The Object 
                retValDhcpConfigDhcpCircuitOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpConfigDhcpCircuitOption (tSNMP_OCTET_STRING_TYPE *
                                   pRetValDhcpConfigDhcpCircuitOption)
{
    return (nmhGetFsMIDhcpConfigDhcpCircuitOption (DHCP_RLY_DFLT_CXT_ID,
                                                   pRetValDhcpConfigDhcpCircuitOption));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayCounterReset
 Input       :  The Indices

                The Object 
                retValDhcpRelayCounterReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayCounterReset (INT4 *pi4RetValDhcpRelayCounterReset)
{
    return (nmhGetFsMIDhcpRelayCounterReset (DHCP_RLY_DFLT_CXT_ID,
                                             pi4RetValDhcpRelayCounterReset));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpRelaying
 Input       :  The Indices

                The Object 
                setValDhcpRelaying
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelaying (INT4 i4SetValDhcpRelaying)
{
    if (i4SetValDhcpRelaying == DHCP_ENABLE)
    {
        if (nmhSetFsMIDhcpRelayContextRowStatus (DHCP_RLY_DFLT_CXT_ID,
                                                 CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsMIDhcpRelaying (DHCP_RLY_DFLT_CXT_ID,
                                        i4SetValDhcpRelaying) == SNMP_SUCCESS)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus
                    (DHCP_RLY_DFLT_CXT_ID, ACTIVE) == SNMP_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
                else
                {
                    if (nmhSetFsMIDhcpRelayContextRowStatus
                        (DHCP_RLY_DFLT_CXT_ID, DESTROY) == SNMP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            else
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus
                    (DHCP_RLY_DFLT_CXT_ID, DESTROY) == SNMP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4SetValDhcpRelaying == DHCP_DISABLE)
    {
        if (nmhSetFsMIDhcpRelaying (DHCP_RLY_DFLT_CXT_ID,
                                    i4SetValDhcpRelaying) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        else
        {
            if (nmhSetFsMIDhcpRelayContextRowStatus
                (DHCP_RLY_DFLT_CXT_ID, DESTROY) == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDhcpRelayServersOnly
 Input       :  The Indices

                The Object 
                setValDhcpRelayServersOnly
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayServersOnly (INT4 i4SetValDhcpRelayServersOnly)
{
    if (nmhSetFsMIDhcpRelayServersOnly (DHCP_RLY_DFLT_CXT_ID,
                                        i4SetValDhcpRelayServersOnly) ==
        SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelaySecsThreshold
 Input       :  The Indices

                The Object 
                setValDhcpRelaySecsThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelaySecsThreshold (INT4 i4SetValDhcpRelaySecsThreshold)
{

    if (nmhSetFsMIDhcpRelaySecsThreshold (DHCP_RLY_DFLT_CXT_ID,
                                          i4SetValDhcpRelaySecsThreshold) ==
        SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayHopsThreshold
 Input       :  The Indices

                The Object 
                setValDhcpRelayHopsThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayHopsThreshold (INT4 i4SetValDhcpRelayHopsThreshold)
{
    if (nmhSetFsMIDhcpRelayHopsThreshold (DHCP_RLY_DFLT_CXT_ID,
                                          i4SetValDhcpRelayHopsThreshold) ==
        SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayRAIOptionControl
 Input       :  The Indices

                The Object 
                setValDhcpRelayRAIOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayRAIOptionControl (INT4 i4SetValDhcpRelayRAIOptionControl)
{

    if (nmhSetFsMIDhcpRelayRAIOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                             i4SetValDhcpRelayRAIOptionControl)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayRAICircuitIDSubOptionControl
 Input       :  The Indices

                The Object 
                setValDhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayRAICircuitIDSubOptionControl (INT4
                                             i4SetValDhcpRelayRAICircuitIDSubOptionControl)
{
    if (nmhSetFsMIDhcpRelayRAICircuitIDSubOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                                         i4SetValDhcpRelayRAICircuitIDSubOptionControl)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayRAIRemoteIDSubOptionControl
 Input       :  The Indices

                The Object 
                setValDhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayRAIRemoteIDSubOptionControl (INT4
                                            i4SetValDhcpRelayRAIRemoteIDSubOptionControl)
{
    if (nmhSetFsMIDhcpRelayRAIRemoteIDSubOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                                        i4SetValDhcpRelayRAIRemoteIDSubOptionControl)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayRAISubnetMaskSubOptionControl
 Input       :  The Indices

                The Object 
                setValDhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayRAISubnetMaskSubOptionControl (INT4
                                              i4SetValDhcpRelayRAISubnetMaskSubOptionControl)
{
    if (nmhSetFsMIDhcpRelayRAISubnetMaskSubOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                                          i4SetValDhcpRelayRAISubnetMaskSubOptionControl)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayRAIVPNIDSubOptionControl
 Input       :  The Indices

                The Object 
                setValDhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayRAIVPNIDSubOptionControl (INT4
                                         i4SetValDhcpRelayRAIVPNIDSubOptionControl)
{
    if (nmhSetFsMIDhcpRelayRAIVPNIDSubOptionControl (DHCP_RLY_DFLT_CXT_ID,
                                                     i4SetValDhcpRelayRAIVPNIDSubOptionControl)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhExSetDhcpConfigTraceLevel
 Input       :  The Indices

                The Object 
                setValDhcpConfigTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpConfigTraceLevel (INT4 i4SetValDhcpConfigTraceLevel)
{
    if (nmhSetFsMIDhcpConfigTraceLevel (DHCP_RLY_DFLT_CXT_ID,
                                        i4SetValDhcpConfigTraceLevel) ==
        SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpConfigDhcpCircuitOption
 Input       :  The Indices

                The Object 
                setValDhcpConfigDhcpCircuitOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpConfigDhcpCircuitOption (tSNMP_OCTET_STRING_TYPE *
                                   pSetValDhcpConfigDhcpCircuitOption)
{
    if (nmhSetFsMIDhcpConfigDhcpCircuitOption (DHCP_RLY_DFLT_CXT_ID,
                                               pSetValDhcpConfigDhcpCircuitOption)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayCounterReset
 Input       :  The Indices

                The Object 
                setValDhcpRelayCounterReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayCounterReset (INT4 i4SetValDhcpRelayCounterReset)
{
    if (nmhSetFsMIDhcpRelayCounterReset (DHCP_RLY_DFLT_CXT_ID,
                                         i4SetValDhcpRelayCounterReset) ==
        SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpRelaying
 Input       :  The Indices

                The Object 
                testValDhcpRelaying
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelaying (UINT4 *pu4ErrorCode, INT4 i4TestValDhcpRelaying)
{
    if (i4TestValDhcpRelaying == DHCP_ENABLE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (pu4ErrorCode,
                                                    DHCP_RLY_DFLT_CXT_ID,
                                                    CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValDhcpRelaying == DHCP_DISABLE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (pu4ErrorCode,
                                                    DHCP_RLY_DFLT_CXT_ID,
                                                    DESTROY) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayServersOnly
 Input       :  The Indices

                The Object 
                testValDhcpRelayServersOnly
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayServersOnly (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDhcpRelayServersOnly)
{
    if (nmhTestv2FsMIDhcpRelayServersOnly (pu4ErrorCode,
                                           DHCP_RLY_DFLT_CXT_ID,
                                           i4TestValDhcpRelayServersOnly) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelaySecsThreshold
 Input       :  The Indices

                The Object 
                testValDhcpRelaySecsThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelaySecsThreshold (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValDhcpRelaySecsThreshold)
{
    if (nmhTestv2FsMIDhcpRelaySecsThreshold (pu4ErrorCode,
                                             DHCP_RLY_DFLT_CXT_ID,
                                             i4TestValDhcpRelaySecsThreshold) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayHopsThreshold
 Input       :  The Indices

                The Object 
                testValDhcpRelayHopsThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayHopsThreshold (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValDhcpRelayHopsThreshold)
{
    if (nmhTestv2FsMIDhcpRelayHopsThreshold (pu4ErrorCode,
                                             DHCP_RLY_DFLT_CXT_ID,
                                             i4TestValDhcpRelayHopsThreshold) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayRAIOptionControl
 Input       :  The Indices

                The Object 
                testValDhcpRelayRAIOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayRAIOptionControl (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValDhcpRelayRAIOptionControl)
{
    if (nmhTestv2FsMIDhcpRelayRAIOptionControl (pu4ErrorCode,
                                                DHCP_RLY_DFLT_CXT_ID,
                                                i4TestValDhcpRelayRAIOptionControl)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayRAICircuitIDSubOptionControl
 Input       :  The Indices

                The Object 
                testValDhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayRAICircuitIDSubOptionControl (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4TestValDhcpRelayRAICircuitIDSubOptionControl)
{
    if (nmhTestv2FsMIDhcpRelayRAICircuitIDSubOptionControl (pu4ErrorCode,
                                                            DHCP_RLY_DFLT_CXT_ID,
                                                            i4TestValDhcpRelayRAICircuitIDSubOptionControl)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 Function    :  nmhTestv2DhcpRelayRAIRemoteIDSubOptionControl
 Input       :  The Indices

                The Object 
                testValDhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayRAIRemoteIDSubOptionControl (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4TestValDhcpRelayRAIRemoteIDSubOptionControl)
{
    if (nmhTestv2FsMIDhcpRelayRAIRemoteIDSubOptionControl (pu4ErrorCode,
                                                           DHCP_RLY_DFLT_CXT_ID,
                                                           i4TestValDhcpRelayRAIRemoteIDSubOptionControl)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayRAISubnetMaskSubOptionControl
 Input       :  The Indices

                The Object 
                testValDhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayRAISubnetMaskSubOptionControl (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4TestValDhcpRelayRAISubnetMaskSubOptionControl)
{
    if (nmhTestv2FsMIDhcpRelayRAISubnetMaskSubOptionControl (pu4ErrorCode,
                                                             DHCP_RLY_DFLT_CXT_ID,
                                                             i4TestValDhcpRelayRAISubnetMaskSubOptionControl)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayRAIVPNIDSubOptionControl
 Input       :  The Indices

                The Object 
                testValDhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayRAIVPNIDSubOptionControl (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValDhcpRelayRAIVPNIDSubOptionControl)
{
    if (nmhTestv2FsMIDhcpRelayRAIVPNIDSubOptionControl (pu4ErrorCode,
                                                        DHCP_RLY_DFLT_CXT_ID,
                                                        i4TestValDhcpRelayRAIVPNIDSubOptionControl)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpConfigTraceLevel
 Input       :  The Indices

                The Object 
                testValDhcpConfigTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpConfigTraceLevel (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDhcpConfigTraceLevel)
{
    if (nmhTestv2FsMIDhcpConfigTraceLevel (pu4ErrorCode,
                                           DHCP_RLY_DFLT_CXT_ID,
                                           i4TestValDhcpConfigTraceLevel) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpConfigDhcpCircuitOption
 Input       :  The Indices

                The Object 
                testValDhcpConfigDhcpCircuitOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpConfigDhcpCircuitOption (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValDhcpConfigDhcpCircuitOption)
{
    if (nmhTestv2FsMIDhcpConfigDhcpCircuitOption (pu4ErrorCode,
                                                  DHCP_RLY_DFLT_CXT_ID,
                                                  pTestValDhcpConfigDhcpCircuitOption)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayCounterReset
 Input       :  The Indices

                The Object 
                testValDhcpRelayCounterReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayCounterReset (UINT4 *pu4ErrorCode,
                                INT4 i4TestValDhcpRelayCounterReset)
{

    if (nmhTestv2FsMIDhcpRelayCounterReset (pu4ErrorCode,
                                            DHCP_RLY_DFLT_CXT_ID,
                                            i4TestValDhcpRelayCounterReset) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpRelaying
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelaying (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayServersOnly
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayServersOnly (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelaySecsThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelaySecsThreshold (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayHopsThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayHopsThreshold (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayRAIOptionControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayRAIOptionControl (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayRAICircuitIDSubOptionControl (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayRAIRemoteIDSubOptionControl (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayRAISubnetMaskSubOptionControl (UINT4 *pu4ErrorCode,
                                                tSnmpIndexList * pSnmpIndexList,
                                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayRAIVPNIDSubOptionControl (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpConfigTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpConfigTraceLevel (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpConfigDhcpCircuitOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpConfigDhcpCircuitOption (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayCounterReset
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayCounterReset (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpRelaySrvAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpRelaySrvAddressTable
 Input       :  The Indices
                DhcpRelaySrvIpAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpRelaySrvAddressTable (UINT4 u4DhcpRelaySrvIpAddress)
{
    if (nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable
        (DHCP_RLY_DFLT_CXT_ID, u4DhcpRelaySrvIpAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpRelaySrvAddressTable
 Input       :  The Indices
                DhcpRelaySrvIpAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpRelaySrvAddressTable (UINT4 *pu4DhcpRelaySrvIpAddress)
{
    INT4                i4CxtId = DHCP_DISABLE_STATUS;
    return (nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable (&i4CxtId,
                                                          pu4DhcpRelaySrvIpAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpRelaySrvAddressTable
 Input       :  The Indices
                DhcpRelaySrvIpAddress
                nextDhcpRelaySrvIpAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpRelaySrvAddressTable (UINT4 u4DhcpRelaySrvIpAddress,
                                         UINT4 *pu4NextDhcpRelaySrvIpAddress)
{
    INT4                i4NextCnxtid = DHCP_DISABLE_STATUS;

    return (nmhGetNextIndexFsMIDhcpRelaySrvAddressTable
            (DHCP_RLY_DFLT_CXT_ID, &i4NextCnxtid, u4DhcpRelaySrvIpAddress,
             pu4NextDhcpRelaySrvIpAddress));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpRelaySrvAddressRowStatus
 Input       :  The Indices
                DhcpRelaySrvIpAddress

                The Object 
                retValDhcpRelaySrvAddressRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelaySrvAddressRowStatus (UINT4 u4DhcpRelaySrvIpAddress,
                                    INT4 *pi4RetValDhcpRelaySrvAddressRowStatus)
{
    return (nmhGetFsMIDhcpRelaySrvAddressRowStatus (DHCP_RLY_DFLT_CXT_ID,
                                                    u4DhcpRelaySrvIpAddress,
                                                    pi4RetValDhcpRelaySrvAddressRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpRelaySrvAddressRowStatus
 Input       :  The Indices
                DhcpRelaySrvIpAddress

                The Object 
                setValDhcpRelaySrvAddressRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelaySrvAddressRowStatus (UINT4 u4DhcpRelaySrvIpAddress,
                                    INT4 i4SetValDhcpRelaySrvAddressRowStatus)
{

    if (nmhSetFsMIDhcpRelaySrvAddressRowStatus (DHCP_RLY_DFLT_CXT_ID,
                                                u4DhcpRelaySrvIpAddress,
                                                i4SetValDhcpRelaySrvAddressRowStatus)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpRelaySrvAddressRowStatus
 Input       :  The Indices
                DhcpRelaySrvIpAddress

                The Object 
                testValDhcpRelaySrvAddressRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelaySrvAddressRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4DhcpRelaySrvIpAddress,
                                       INT4
                                       i4TestValDhcpRelaySrvAddressRowStatus)
{
    if (nmhTestv2FsMIDhcpRelaySrvAddressRowStatus (pu4ErrorCode,
                                                   DHCP_RLY_DFLT_CXT_ID,
                                                   u4DhcpRelaySrvIpAddress,
                                                   i4TestValDhcpRelaySrvAddressRowStatus)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpRelaySrvAddressTable
 Input       :  The Indices
                DhcpRelaySrvIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelaySrvAddressTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpRelayIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpRelayIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpRelayIfTable (INT4 i4IfIndex)
{

    if (nmhValidateIndexInstanceFsMIDhcpRelayIfTable (DHCP_RLY_DFLT_CXT_ID,
                                                      i4IfIndex) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpRelayIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpRelayIfTable (INT4 *pi4IfIndex)
{
    INT4                i4CxtId = DHCP_DISABLE_STATUS;
    return (nmhGetFirstIndexFsMIDhcpRelayIfTable (&i4CxtId, pi4IfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpRelayIfTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpRelayIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    INT4                i4NextCntxid = DHCP_DISABLE_STATUS;

    return (nmhGetNextIndexFsMIDhcpRelayIfTable
            (DHCP_RLY_DFLT_CXT_ID, &i4NextCntxid, i4IfIndex, pi4NextIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpRelayIfCircuitId
 Input       :  The Indices
                IfIndex

                The Object 
                retValDhcpRelayIfCircuitId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayIfCircuitId (INT4 i4IfIndex,
                            UINT4 *pu4RetValDhcpRelayIfCircuitId)
{
    return (nmhGetFsMIDhcpRelayIfCircuitId (DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                            pu4RetValDhcpRelayIfCircuitId));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayIfRemoteId
 Input       :  The Indices
                IfIndex

                The Object 
                retValDhcpRelayIfRemoteId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayIfRemoteId (INT4 i4IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValDhcpRelayIfRemoteId)
{
    return (nmhGetFsMIDhcpRelayIfRemoteId (DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                           pRetValDhcpRelayIfRemoteId));
}

/****************************************************************************
 Function    :  nmhGetDhcpRelayIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValDhcpRelayIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpRelayIfRowStatus (INT4 i4IfIndex, INT4 *pi4RetValDhcpRelayIfRowStatus)
{
    return (nmhGetFsMIDhcpRelayIfRowStatus (DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                            pi4RetValDhcpRelayIfRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpRelayIfCircuitId
 Input       :  The Indices
                IfIndex

                The Object 
                setValDhcpRelayIfCircuitId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayIfCircuitId (INT4 i4IfIndex, UINT4 u4SetValDhcpRelayIfCircuitId)
{

    if (nmhSetFsMIDhcpRelayIfCircuitId (DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                        u4SetValDhcpRelayIfCircuitId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDhcpRelayIfRemoteId
 Input       :  The Indices
                IfIndex

                The Object 
                setValDhcpRelayIfRemoteId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayIfRemoteId (INT4 i4IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValDhcpRelayIfRemoteId)
{
    if (nmhSetFsMIDhcpRelayIfRemoteId (DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                       pSetValDhcpRelayIfRemoteId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpRelayIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValDhcpRelayIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpRelayIfRowStatus (INT4 i4IfIndex, INT4 i4SetValDhcpRelayIfRowStatus)
{

    if (nmhSetFsMIDhcpRelayIfRowStatus (DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                        i4SetValDhcpRelayIfRowStatus) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayIfCircuitId
 Input       :  The Indices
                IfIndex

                The Object 
                testValDhcpRelayIfCircuitId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayIfCircuitId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               UINT4 u4TestValDhcpRelayIfCircuitId)
{

    if (nmhTestv2FsMIDhcpRelayIfCircuitId (pu4ErrorCode,
                                           DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                           u4TestValDhcpRelayIfCircuitId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayIfRemoteId
 Input       :  The Indices
                IfIndex

                The Object 
                testValDhcpRelayIfRemoteId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayIfRemoteId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValDhcpRelayIfRemoteId)
{

    if (nmhTestv2FsMIDhcpRelayIfRemoteId (pu4ErrorCode,
                                          DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                          pTestValDhcpRelayIfRemoteId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpRelayIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValDhcpRelayIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpRelayIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4TestValDhcpRelayIfRowStatus)
{
    if (nmhTestv2FsMIDhcpRelayIfRowStatus (pu4ErrorCode,
                                           DHCP_RLY_DFLT_CXT_ID, i4IfIndex,
                                           i4TestValDhcpRelayIfRowStatus) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpRelayIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpRelayIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
