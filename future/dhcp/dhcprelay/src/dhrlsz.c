/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhrlsz.c,v 1.4 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _DHRLSZ_C
#include "dhcpincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DhrlSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DHRL_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsDHRLSizingParams[i4SizingId].u4StructSize,
                              FsDHRLSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(DHRLMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DhrlSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DhrlSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDHRLSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DHRLMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DhrlSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DHRL_MAX_SIZING_ID; i4SizingId++)
    {
        if (DHRLMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DHRLMemPoolIds[i4SizingId]);
            DHRLMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
