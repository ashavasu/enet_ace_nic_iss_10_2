/*$Id: dhrlport.c,v 1.31 2015/09/15 11:53:34 siva Exp $*/
#include "dhcpincs.h"

extern INT1 nmhGetIssSwitchBaseMacAddress ARG_LIST ((tMacAddr *));
#ifdef BSDCOMP_SLI_WANTED
UINT1 DhcpRelaySendRawPacket(INT1 *pMessage, UINT2 u2Size, UINT4 u4IfIndex,
                                  INT1 *pi1HwAddr,UINT1 u1Len, UINT4 u4Ipaddr);

static INT1 gai1PacketBuffer[MAX_JUMBO_PDU_SIZE];
#endif



/************************************************************************/
/*  Function Name   : DhcpRelayStartTimer                                    */
/*  Description     : This function start a timer for 'u4Sec' duration. */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*                  : u4Sec - Timer expiry time in seconds.             */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpRelayStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec)
{
    UINT4               u4Ticks;

    /* This function start a timer for u4Sec Duration .. */

    u4Ticks = u4Sec * SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (TmrStartTimer (TimerList, pTimer, u4Ticks) == TMR_FAILURE)
    {

        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Starting Problem with Timer !!!\n");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpRelaySendToSli                                */
/*  Description     : This function calls sendto() for sending UDP      */
/*                  : messages to 'u2DstPort' at 'u4DstIp' address.     */
/*  Input(s)        : pMessage - Message to be sent.                    */
/*                    u2Size - size of the message.                     */
/*                    u4DstIp - destination Ip-Address.                 */
/*                    u2DstPort - UDP destination port number.          */
/*                    i4SockDesc - Socket Descriptor                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpRelaySendToSli (INT1 *pMessage, UINT2 u2Size,
                    UINT4 u4DstIp, UINT2 u2DstPort, INT4 i4SockDesc)
{
    struct sockaddr_in  RemoteAddr;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4DstIp);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2DstPort);

    if (sendto (i4SockDesc, pMessage, u2Size, 0,
                (struct sockaddr *) &RemoteAddr, sizeof (RemoteAddr)) < 0)
    {
        perror ("sendto failed!!!\r\n");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpRelaySendBroadcast                            */
/*  Description     : This function is called for broadcasting DHCP     */
/*                    Messages over the interface - u4IfIndex. Calls    */
/*                    DhcpRelaySendToSli(), for transmitting UDP message*/
/*  Input(s)        : pMessage - DHCP message to be sent.               */
/*                    u2Size - size of the message.                     */
/*                    u4IfIndex - Ip interface.                         */
/*                    i4SockDesc - Socket Descriptor                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpRelaySendBroadcast (INT1 *pMessage, UINT2 u2Size,
                        UINT4 u4IfIndex, UINT4 u4Port, INT4 i4SockDesc)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct sockaddr_in  RemoteAddr;
    UINT4               u4Dest = 0xffffffff;
    INT4                i4RetVal = DHCP_SUCCESS;
    UINT1               au1Cmsg[DHCPRLY_CMSG_LEN];    /* For storing Auxillary Data - 
                                                       IP Packet INFO. */
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    PktInfo.msg_name = (void *) &RemoteAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    MEMSET(&Iov,0,sizeof(struct iovec));
    Iov.iov_base = (UINT1 *) pMessage;
    Iov.iov_len = u2Size;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = DHCP_CMSG_LEVEL;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = (UINT2) u4IfIndex;
    pIpPktInfo->ipi_addr.s_addr = u4Dest;
    pIpPktInfo->ipi_spec_dst.s_addr = 0;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u4Port);

    if (sendmsg (i4SockDesc, &PktInfo, 0) < 0)
    {
        perror ("sendmsg \r\n");
        i4RetVal = DHCP_FAILURE;
        return i4RetVal;
    }
#ifdef SLI_WANTED
    i4RetVal = DhcpRelaySendToSli (pMessage, u2Size, u4Dest, (UINT2) u4Port,
                                        i4SockDesc);
#endif
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpRelaySendUnicast                              */
/*  Description     : This function is called for sending DHCP unicast  */
/*                    Messages over the interface - u4IfIndex. Calls    */
/*                    DhcpSendToSli(), for transmitting UDP message     */
/*  Input(s)        : pMessage - DHCP message to be sent                */
/*                    u2Size - size of the message                      */
/*                    u4Dest - destination IP address                   */
/*                    u4IfIndex - Ip interface                          */
/*                    u2Port - UDP destination port number              */
/*                    i4SockDesc - Socket Descriptor through which data */
/*                    to be sent                                        */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/
UINT1
DhcpRelaySendUnicast (INT1 *pMessage, UINT2 u2Size, UINT4 u4Dest,
                      UINT4 u4IfIndex, UINT2 u2Port,INT4 i4SockDesc)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct sockaddr_in  RemoteAddr;
    UINT1               u1RetVal = DHCP_SUCCESS;
    UINT1               au1Cmsg[24];    /* For storing Auxillary Data - 
                                           IP Packet INFO. */
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&RemoteAddr, 0, sizeof (struct sockaddr_in));
    PktInfo.msg_name = (void *) &RemoteAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    MEMSET(&Iov,0,sizeof(struct iovec));
    Iov.iov_base = (UINT1 *) pMessage;
    Iov.iov_len = u2Size;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif

    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = DHCP_CMSG_LEVEL;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    pIpPktInfo->ipi_ifindex = (INT4) u4IfIndex;

    pIpPktInfo->ipi_addr.s_addr = OSIX_HTONL(u4Dest);
    pIpPktInfo->ipi_spec_dst.s_addr = 0;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL(u4Dest);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2Port);

    if (sendmsg (i4SockDesc, &PktInfo, 0) < 0)
    {
        perror ("sendmsg \r\n");
        return DHCP_FAILURE;
    }
#ifdef SLI_WANTED
    u1RetVal = (UINT1) DhcpRelaySendToSli (pMessage, u2Size, u4Dest, u2Port,
                                        i4SockDesc);
#endif
    return u1RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpSendToClient                                 */
/*  Description     : This Function Sends the BOOT REPLY from server to */
/*                    the client. If the broadcast bit is set, then it   */
/*                    calls the DhcpRelaySendBroadcast() function other- */
/*                    wise, it adds the route for client subnet and uni */
/*                    casts the Packet by calling DhcpRelaySendToSli() */
/*  Input(s)        : Dhcp Packet - DHCP packet structure               */
/*                    i4IfIndex - Ip interface.                         */
/*                    Broadcast Option                                 */
/*                    i4SockDesc - Socket Descriptor                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpSendToClient (INT4 i4IfIndex,
                  tDhcpPktInfo * pDhcpPkt, 
                  UINT1 u1PktBroadCastOpt,
                  INT4  i4SockDesc
                  )
{
    INT4                i4Retval = DHCP_FAILURE;
#ifdef BSDCOMP_SLI_WANTED
    UINT4               u4Ipaddr = 0;
    INT1                i1Hw_addr[CFA_ENET_ADDR_LEN] ;
    UINT1               u1RetVal = DHCP_FAILURE;
#endif

    if (u1PktBroadCastOpt != 0)    /* Send as IP and link layer broadcast */
    {
        /* Typecasted 1st arg to remove -pedantic warnings */
        i4Retval = DhcpRelaySendBroadcast ((INT1 *) &pDhcpPkt->Pkt,
                                           pDhcpPkt->u2Len,
                                           i4IfIndex, DHCP_CLIENT_PORT,
                                           i4SockDesc);
        if (i4Retval == DHCP_FAILURE)
        {

            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "Server Message to SLI Failed !!!!\n");
            return i4Retval;
        }
    }
    else
    {
#ifdef BSDCOMP_SLI_WANTED
	    u4Ipaddr = (pDhcpPkt->Pkt.u4Yiaddr); 

	    MEMSET (i1Hw_addr , 0 , pDhcpPkt->Pkt.u1Hlen);
	    MEMCPY (i1Hw_addr, (INT1 *)pDhcpPkt->Pkt.u1Chaddr, pDhcpPkt->Pkt.u1Hlen);

	    /* Sending packets via RAW socket, since there is no arp entry to reach client */
	    u1RetVal = DhcpRelaySendRawPacket((INT1 *) &pDhcpPkt->Pkt, pDhcpPkt->u2Len, (UINT4) i4IfIndex, 
			    (INT1 *)i1Hw_addr,pDhcpPkt->Pkt.u1Hlen, u4Ipaddr );

	    if (u1RetVal == DHCP_FAILURE)
            {
                    MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                    "Server Message to SLI Failed !!!!\n");
                    return DHCP_FAILURE;
            }
#else
            /* DSL_ADD */
            i4Retval = (INT4) DhcpRelaySendUnicast ((INT1 *) &pDhcpPkt->Pkt,
                                                    pDhcpPkt->u2Len,
                                                    DHCP_HTONL (pDhcpPkt->Pkt.
                                                                u4Yiaddr),
                                                    (UINT4) i4IfIndex,
                                                    DHCP_CLIENT_PORT,
                                                    i4SockDesc);
        if (i4Retval == DHCP_FAILURE)
        {
            perror("sendmsg failed to client");
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "Server Message to SLI Failed !!!!\n");
            return i4Retval;
        }

#endif
    }
    return DHCP_SUCCESS;
}

#ifdef BSDCOMP_SLI_WANTED
/************************************************************************/
/*  Function Name   : DhcpRelaySendRawPacket                            */
/*  Description     : Fills the IP Header and UDP Header along with     */
/*                    DHCP Message payload and sends the Message over   */
/*                    Ethernet.                                         */
/*  Input(s)        : pMessage   - DHCP Message PayLoad                 */
/*                    u2Size     - Size of the DHCP Messae              */
/*                    u4IfIndex  - IP Interface Index                   */
/*                    pi1HwAddr  - H/W address to send the packet       */
/*                    u1Len      - Length og H/W address                */
/*                    u4Ipaddr   - IP Address of Destination            */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/
UINT1 DhcpRelaySendRawPacket(INT1 *pMessage, UINT2 u2Size, UINT4 u4IfIndex,
                             INT1 *pi1HwAddr,UINT1 u1Len, UINT4 u4Ipaddr)
{

    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4DhcpRUnicastSockId = 0;
    UINT2               u2Val = 0;
    UINT2               u2Bufflen = 0;
    struct sockaddr_ll  DestAddr;
    t_IP_HEADER         IpHdr;


    MEMSET (&DestAddr, 0, sizeof (struct sockaddr_ll));
    MEMSET (&NetIpIfInfo, 0 , sizeof(tNetIpv4IfInfo));
    MEMSET (&IpHdr, 0, IP_HDR_LEN);

    i4DhcpRUnicastSockId = socket (PF_PACKET,SOCK_DGRAM, IPPROTO_RAW);

    if (i4DhcpRUnicastSockId < 0)
    {
        perror ("Opening DHCPR PF_PACKET Socket Failed !!!\n");
        return DHCP_FAILURE;
    }

    u2Bufflen = DHCP_UDP_HDR_LEN + IP_HDR_LEN + u2Size;

    MEMSET ( gai1PacketBuffer, 0 ,MAX_JUMBO_PDU_SIZE);

    if (u2Bufflen > MAX_JUMBO_PDU_SIZE )
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Exceeding max packet size !!!\n");
        close (i4DhcpRUnicastSockId);
        return DHCP_FAILURE;
    }

    if (( NetIpv4GetIfInfo ( (INT4) u4IfIndex, &NetIpIfInfo)) == NETIPV4_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "NetIpv4GetIfInfo Failed !!!\n");
        close (i4DhcpRUnicastSockId);
        return DHCP_FAILURE;
    }

    /* Fill the Ip Header */
    IpHdr.u1Ver_hdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    IpHdr.u1Tos = 0;
    IpHdr.u2Totlen = OSIX_HTONS (u2Bufflen);
    IpHdr.u2Id = 0;
    IpHdr.u2Fl_offs = 0;
    IpHdr.u1Ttl = IP_DEF_TTL;
    IpHdr.u1Proto = IPPROTO_UDP;
    IpHdr.u2Cksum = 0;
    IpHdr.u4Src = DHCP_HTONL (NetIpIfInfo.u4Addr);
    IpHdr.u4Dest = u4Ipaddr;
    IpHdr.u2Cksum = UtlIpCSumLinBuf ((const INT1 *) &IpHdr, IP_HDR_LEN);
    IpHdr.u2Cksum = OSIX_HTONS (IpHdr.u2Cksum);
    MEMCPY (gai1PacketBuffer, &IpHdr, IP_HDR_LEN);

    /* Fill the UDP Header */
    u2Val = IP_HTONS (DHCP_PORT_SERVER);
    MEMCPY (gai1PacketBuffer + IP_HDR_LEN, &u2Val, sizeof (UINT2));
    u2Val = IP_HTONS (DHCP_PORT_CLIENT);
    MEMCPY (gai1PacketBuffer + IP_HDR_LEN + 2, &u2Val, sizeof (UINT2));
    u2Val = IP_HTONS (DHCP_UDP_HDR_LEN + u2Size);
    MEMCPY (gai1PacketBuffer + IP_HDR_LEN + 4, &u2Val, sizeof (UINT2));
    u2Val = 0;

    /* Copy the DHCP Message */
    MEMCPY (gai1PacketBuffer + IP_HDR_LEN + DHCP_UDP_HDR_LEN, pMessage, u2Size);

    /* Fill Destination Mac Obtained from DHcp packet */
    MEMCPY (DestAddr.sll_addr, pi1HwAddr, u1Len);
    DestAddr.sll_family = PF_PACKET;
    DestAddr.sll_ifindex = (UINT2) u4IfIndex;
    DestAddr.sll_protocol = IP_HTONS (ETH_P_IP);
    DestAddr.sll_halen = CFA_ENET_ADDR_LEN;

    if (sendto (i4DhcpRUnicastSockId, gai1PacketBuffer, u2Bufflen, 0,
                (struct sockaddr *) &DestAddr, sizeof (struct sockaddr_ll)) < 0)
    {
        perror (" DHCPRelaySendRawPacket : Sendto Failed due to !!!\n");
        MEMSET ( gai1PacketBuffer, 0 ,MAX_JUMBO_PDU_SIZE);
        close (i4DhcpRUnicastSockId);
        return DHCP_FAILURE;
    }
    MEMSET ( gai1PacketBuffer, 0 ,MAX_JUMBO_PDU_SIZE);
    close (i4DhcpRUnicastSockId);
    return DHCP_SUCCESS;


}
#endif



/************************************************************************/
/*  Function Name   : DhcpGetInterfaceAddr                              */
/*  Description     : Reads the Ip Address with Port                   */
/*  Input(s)        : u2Port - Port Number                   */
/*  Output(s)       : pu4Addr - pointer to Ip Address                  */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpGetInterfaceAddr (UINT2 u2Port, UINT4 *pu4Addr)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    *pu4Addr = NetIpIfInfo.u4Addr;

    return DHCP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : DhcpGetDestIfIndex                               */
/*  Description     : Reads the Destination ip Address with            */
/*                    an Interface                                     */
/*  Input(s)        : Dest Ip Addr - Destination Ip Addreess           */
/*  Output(s)       :  pi4IfIndex - Ptr to Interface Index             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpGetDestIfIndex (UINT4 u4ContextId, UINT4 u4DestAddr, INT4 *pi4IfIndex)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4DestAddr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        *pi4IfIndex = (INT4) NetIpRtInfo.u4RtIfIndx;
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpGetIfMtu                                    */
/*  Description     : Reads the MTU with an Interface  index           */
/*  Input(s)        : i4IfIndex - Interface Index                     */
/*  Output(s)       : pu4MTU - Mtu of the INterface                    */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpGetIfMtu (INT4 i4IfIndex, UINT4 *pu4MTU)
{

    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo ((UINT4) i4IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    *pu4MTU = NetIpIfInfo.u4Mtu;

    return DHCP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : DhcpGetFirstIfIndex                */
/*  Description     : Validation of First Index                */
/*  Input(s)        : u4ContextId - Context Id                          */
/*                    pi4IfIndex - Interface Index                      */
/*  Output(s)       : None                           */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpGetFirstIfIndex (UINT4 u4ContextId,INT4 *pi4IfIndex)
{
#ifdef LNXIP4_WANTED
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,&NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    else
    {
        *pi4IfIndex = (INT4) NetIpIfInfo.u4IfIndex;
        return DHCP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pi4IfIndex);
    return DHCP_SUCCESS;
#endif
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfForwardingEnabled                      */
/*  Description     : Checking of Ip Forwarding Status                 */
/*  Input(s)        : NOne                                */
/*  Output(s)       : None                           */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpCheckIfForwardingEnabled ()
{
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpGetNextIfIndex                                */
/*  Description     : Reading of Next interface INdex                  */
/*  Input(s)        : i4IfIndex - Interface Index                      */
/*  Output(s)       : pi4IfIndex - Pointer to Interface Index          */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpGetNextIfIndex (INT4 i4IfIndex, INT4 *pi4IfIndex, UINT4 u4ContextId)
{
#ifdef LNXIP4_WANTED
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetNextIfInfoInCxt (u4ContextId, (UINT4) i4IfIndex, 
                                   &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    else
    {
        *pi4IfIndex = (INT4) NetIpIfInfo.u4IfIndex;
        return DHCP_SUCCESS;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (u4ContextId);
    return DHCP_SUCCESS;
#endif
}

/************************************************************************/
/*  Function Name   : DhcpGetCircuitID                                  */
/*  Description     : Reading of Circuit Id from the Packet             */
/*  Input(s)        : pPktInfo - tDhcpPacket                            */
/*                    u4ContextId - Current Dhcp Relay context          */
/*  Output(s)       : pu4CktID - Circuit Id ptr                         */
/*  Returns         : None                                              */
/************************************************************************/

INT4
DhcpGetCircuitID (tDhcpPktInfo * pPktInfo, UINT4 u4ContextId,UINT4 *pu4CktID)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfCofigInfo = NULL;

    MEMSET (&DhrlIfConfigInfo,0,sizeof (tDhrlIfConfigInfo));

    if(DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return DHCP_FAILURE;
    }
    /* Check if circuit id is configured over the interface.If so, retuurn 
     * the configured value */
    DhrlIfConfigInfo.u4Port     = pPktInfo->u4IfNum;
    DhrlIfConfigInfo.u4CxtId    = u4ContextId;
    if ((pDhrlIfCofigInfo = RBTreeGet (DHCP_RLY_CXT_NODE[u4ContextId]->
            pIfToIdRBRoot,&DhrlIfConfigInfo))
        != NULL)
    {
        if ((pDhrlIfCofigInfo->u4CircuitId != 0)
            && (pDhrlIfCofigInfo->u1RowStatus == ACTIVE))
        {
            *pu4CktID = pDhrlIfCofigInfo->u4CircuitId;
            return DHCP_SUCCESS;
        }
    }

    return DHCP_FAILURE;

}

/************************************************************************/
/*  Function Name   : DhcpGetRemoteID                                   */
/*  Description     : Reading of Remote Id from the Packet              */
/*  Input(s)        : pPktInfo - tDhcpPacket                            */
/*                    u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : au1RemoteID- Remote Id ptr                        */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpGetRemoteID (tDhcpPktInfo * pDhcpPacket, UINT4 u4ContextId,
                                                UINT1 au1RemoteID[])
{
    UINT4               u4IssCurrentIpAddr = 0;
    INT4                i4IfIndex = 0;
    CHR1               *pu1Byte = NULL;
    UINT1               au1SysName[SNMP_MAX_OCTETSTRING_SIZE];
    INT4                i4RetVal = FAILURE;
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfCofigInfo = NULL;
    tMacAddr            BaseMac;
    tIpConfigInfo       IpIfInfo;
    MEMSET (BaseMac, 0, sizeof (tMacAddr));
    MEMSET (&au1SysName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (&au1RemoteID[0], 0, DHRL_MAX_RID_LEN);
    if(DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

    /* Check if circuit id is configured over the interface.If so, retuurn 
     * the configured value */
    DhrlIfConfigInfo.u4Port     = pDhcpPacket->u4IfNum;
    DhrlIfConfigInfo.u4CxtId    = u4ContextId;

    if ((pDhrlIfCofigInfo = RBTreeGet (DHCP_RLY_CXT_NODE[u4ContextId]->
            pIfToIdRBRoot, &DhrlIfConfigInfo))
        != NULL)
    {
        if ((STRLEN (pDhrlIfCofigInfo->au1RemoteId))
            && (pDhrlIfCofigInfo->u1RowStatus == ACTIVE))
        {
            /* If custom string is set to "<MGMT_IP_ADDR>" then current
             * IP of Default Vlan is inserted instead
             */
            if (STRCMP (pDhrlIfCofigInfo->au1RemoteId, "<MGMT_IP_ADDR>") == 0)
            {
                i4IfIndex = CfaGetDefaultVlanInterfaceIndex ();
                if (CfaIpIfGetIfInfo ((UINT4) i4IfIndex, &IpIfInfo) ==
                    CFA_SUCCESS)
                {
                    u4IssCurrentIpAddr = IpIfInfo.u4Addr;
                    /*Allocation for pu1Byte happens within CLI_CONVERT_IPADDR_TO_STR */
                    CLI_CONVERT_IPADDR_TO_STR (pu1Byte, u4IssCurrentIpAddr);
                    STRNCPY ((CHR1 *) au1RemoteID, pu1Byte, STRLEN (pu1Byte));
                    au1RemoteID[STRLEN (pu1Byte)] = '\0';
                }
                else
                {
                    STRNCPY (au1RemoteID, pDhrlIfCofigInfo->au1RemoteId,
                                        STRLEN (pDhrlIfCofigInfo->au1RemoteId));
                    au1RemoteID [STRLEN (pDhrlIfCofigInfo->au1RemoteId)] = '\0';
                }
                return;
            }
            /* If custom string is set to "<SYS_NAME>" then current
             * sysname is inserted instead
             */
            if (STRCMP (pDhrlIfCofigInfo->au1RemoteId, "<SYS_NAME>") == 0)
            {
#ifdef SNMP_3_WANTED
                i4RetVal = SnmpGetSysName (&au1SysName[0]);
#endif
                if ((i4RetVal == SUCCESS) && (au1SysName[0] != '\0'))
                {
                    STRNCPY (au1RemoteID, &au1SysName, DHRL_MAX_RID_LEN - 1);
                    au1RemoteID[DHRL_MAX_RID_LEN - 1] = '\0';
                }
                else
                {
                    STRNCPY (au1RemoteID, pDhrlIfCofigInfo->au1RemoteId,
                                    STRLEN (pDhrlIfCofigInfo->au1RemoteId));
                    au1RemoteID[STRLEN (pDhrlIfCofigInfo->au1RemoteId)] = '\0';
                }
                return;
            }
            STRNCPY (au1RemoteID, pDhrlIfCofigInfo->au1RemoteId,
                            STRLEN (pDhrlIfCofigInfo->au1RemoteId));
            au1RemoteID[STRLEN (pDhrlIfCofigInfo->au1RemoteId)] = '\0';
            return;
        }
    }

    pDhcpPacket = NULL;
    nmhGetIssSwitchBaseMacAddress (&BaseMac);
    CliMacToDotStr ((UINT1 *) &BaseMac, au1RemoteID);
}

/************************************************************************/
/*  Function Name   : DhcpGetSubnetMask                    */
/*  Description     : Reading of Subnet Mask                */
/*  Input(s)        : pPktInfo - tDhcpPacket                    */
/*  Output(s)       : pu4CktID - Ptr to Subnetmask                      */
/*  Returns         : None                        */
/************************************************************************/

VOID
DhcpGetSubnetMask (tDhcpPktInfo * pDhcpPacket, UINT4 *pu4SubnetMask)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (pDhcpPacket->u4IfNum,
                          &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        *pu4SubnetMask = 0;
        return;
    }

    *pu4SubnetMask = NetIpIfInfo.u4NetMask;
}
