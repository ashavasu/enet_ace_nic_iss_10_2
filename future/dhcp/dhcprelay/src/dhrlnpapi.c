
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: dhrlnpapi.c,v 1.3 2016/03/26 09:50:33 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of DHCP-RELAY
 * 		 module.
 ******************************************************************************/

#ifndef __DHRL_NPAPI_C__
#define __DHRL_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : DhrlFsNpDhcpRlyInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpRlyInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpRlyInit
                            u4ContextId - Context in which relay is being
                            enabled
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DhrlFsNpDhcpRlyInit (UINT4  u4ContextId)
{
    tFsHwNp             FsHwNp;
    UNUSED_PARAM (u4ContextId);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_RLY_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : DhrlFsNpDhcpRlyDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpRlyDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpRlyDeInit
                            u4ContexId - Context in which relay is being disabled
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DhrlFsNpDhcpRlyDeInit (UINT4    u4ContextId)
{
    tFsHwNp             FsHwNp;
    UNUSED_PARAM (u4ContextId);
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_RLY_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#endif /* __DHRL_NPAPI_C__ */
