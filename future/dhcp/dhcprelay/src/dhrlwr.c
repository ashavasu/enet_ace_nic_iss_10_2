/*$Id: dhrlwr.c,v 1.10 2017/12/14 10:23:42 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "dhrllow.h"
# include  "dhrlwr.h"
# include  "dhrldb.h"
#include   "dhcp.h"
VOID
RegisterDHRL ()
{
    SNMPRegisterMibWithLock (&fsdhcpOID, &fsdhcpEntry, DhcpRProtocolLock,
                             DhcpRProtocolUnlock, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsdhcpOID, (const UINT1 *) "fsdhcpRelay");
}

VOID
UnRegisterDHRL ()
{
    SNMPUnRegisterMib (&fsdhcpOID, &fsdhcpEntry);
    SNMPDelSysorEntry (&fsdhcpOID, (const UINT1 *) "fsdhcpRelay");
}

INT4
DhcpRelayingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelaying (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayServersOnlyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayServersOnly (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelaySecsThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelaySecsThreshold (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayHopsThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayHopsThreshold (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayRAIOptionControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAIOptionControl (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayRAICircuitIDSubOptionControlGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAICircuitIDSubOptionControl
            (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayRAIRemoteIDSubOptionControlGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAIRemoteIDSubOptionControl
            (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayRAISubnetMaskSubOptionControlGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAISubnetMaskSubOptionControl
            (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayRAIOptionInsertedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAIOptionInserted (&(pMultiData->u4_ULongValue)));
}

INT4
DhcpRelayRAICircuitIDSubOptionInsertedGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAICircuitIDSubOptionInserted
            (&(pMultiData->u4_ULongValue)));
}

INT4
DhcpRelayRAIRemoteIDSubOptionInsertedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAIRemoteIDSubOptionInserted
            (&(pMultiData->u4_ULongValue)));
}

INT4
DhcpRelayRAISubnetMaskSubOptionInsertedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAISubnetMaskSubOptionInserted
            (&(pMultiData->u4_ULongValue)));
}

INT4
DhcpRelayRAIOptionWronglySetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAIOptionWronglySet (&(pMultiData->u4_ULongValue)));
}

INT4
DhcpRelayRAISpaceConstraintGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAISpaceConstraint (&(pMultiData->u4_ULongValue)));
}

INT4
DhcpConfigTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpConfigTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpConfigDhcpCircuitOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpConfigDhcpCircuitOption (pMultiData->pOctetStrValue));
}

INT4
DhcpRelayCounterResetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayCounterReset (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayRAIVPNIDSubOptionControlGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDhcpRelayRAIVPNIDSubOptionControl
            (&(pMultiData->i4_SLongValue)));
}

INT4
DhcpRelayingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelaying (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayServersOnlySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayServersOnly (pMultiData->i4_SLongValue));
}

INT4
DhcpRelaySecsThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelaySecsThreshold (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayHopsThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayHopsThreshold (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAIOptionControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayRAIOptionControl (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAICircuitIDSubOptionControlSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayRAICircuitIDSubOptionControl
            (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayCounterResetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayCounterReset (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAIRemoteIDSubOptionControlSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayRAIRemoteIDSubOptionControl
            (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAISubnetMaskSubOptionControlSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayRAISubnetMaskSubOptionControl
            (pMultiData->i4_SLongValue));
}

INT4
DhcpConfigTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpConfigTraceLevel (pMultiData->i4_SLongValue));
}

INT4
DhcpConfigDhcpCircuitOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpConfigDhcpCircuitOption (pMultiData->pOctetStrValue));
}

INT4
DhcpRelayRAIVPNIDSubOptionControlSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDhcpRelayRAIVPNIDSubOptionControl
            (pMultiData->i4_SLongValue));
}

INT4
DhcpRelayingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelaying (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayServersOnlyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayServersOnly
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelaySecsThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelaySecsThreshold
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayHopsThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayHopsThreshold
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAIOptionControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayRAIOptionControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayCounterResetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayCounterReset
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAICircuitIDSubOptionControlTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayRAICircuitIDSubOptionControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAIRemoteIDSubOptionControlTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayRAIRemoteIDSubOptionControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayRAISubnetMaskSubOptionControlTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayRAISubnetMaskSubOptionControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpConfigTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpConfigTraceLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpConfigDhcpCircuitOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpConfigDhcpCircuitOption
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
DhcpRelayRAIVPNIDSubOptionControlTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2DhcpRelayRAIVPNIDSubOptionControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
DhcpRelayingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelaying (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayServersOnlyDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayServersOnly
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelaySecsThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelaySecsThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayHopsThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayHopsThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayRAIOptionControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayRAIOptionControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayRAICircuitIDSubOptionControlDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayRAICircuitIDSubOptionControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayRAIRemoteIDSubOptionControlDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayRAIRemoteIDSubOptionControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayRAISubnetMaskSubOptionControlDep (UINT4 *pu4Error,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayRAISubnetMaskSubOptionControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpConfigTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpConfigTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpConfigDhcpCircuitOptionDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpConfigDhcpCircuitOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelayRAIVPNIDSubOptionControlDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayRAIVPNIDSubOptionControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDhcpRelaySrvAddressTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDhcpRelaySrvAddressTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDhcpRelaySrvAddressTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
DhcpRelayCounterResetDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayCounterReset
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
DhcpRelaySrvAddressRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDhcpRelaySrvAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDhcpRelaySrvAddressRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
DhcpRelaySrvAddressRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDhcpRelaySrvAddressRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
DhcpRelaySrvAddressRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2DhcpRelaySrvAddressRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
DhcpRelaySrvAddressTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelaySrvAddressTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexDhcpRelayIfTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDhcpRelayIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDhcpRelayIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
DhcpRelayIfCircuitIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDhcpRelayIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDhcpRelayIfCircuitId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
DhcpRelayIfRemoteIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDhcpRelayIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDhcpRelayIfRemoteId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
DhcpRelayIfRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDhcpRelayIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDhcpRelayIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
DhcpRelayIfCircuitIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDhcpRelayIfCircuitId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
DhcpRelayIfRemoteIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDhcpRelayIfRemoteId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
DhcpRelayIfRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDhcpRelayIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
DhcpRelayIfCircuitIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2DhcpRelayIfCircuitId (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
DhcpRelayIfRemoteIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2DhcpRelayIfRemoteId (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
DhcpRelayIfRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2DhcpRelayIfRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
DhcpRelayIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2DhcpRelayIfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
