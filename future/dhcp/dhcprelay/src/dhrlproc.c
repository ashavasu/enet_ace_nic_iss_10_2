/*****************************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: dhrlproc.c,v 1.71 2017/12/14 10:23:42 siva Exp $
 ******************************************************************************/

#include "dhcpincs.h"
#include "dhcp.h"

/************************************************************************/
/*  Function Name   : DhcpIncrementSpaceConstraintCounter               */
/*  Description     : Increment of Space Constraint Counter             */
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpIncrementSpaceConstraintCounter (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        (DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAISpaceConstraint)++;
    }
    return;
}

#ifdef RELAY_AGNT_INFO

/************************************************************************/
/*  Function Name   : DhcpCheckIfRAIOptionEnabled                       */
/*  Description     : The function Globally Enables the Dhcp Relaying   */
/*  Input(s)        : u4ContextId - Relay context id                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpCheckIfRAIOptionEnabled (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAIOption == DHCP_ENABLE)
        {
            return DHCP_TRUE;
        }
    }
    return DHCP_FALSE;
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfCircuitIDSubOptionEnabled             */
/*  Description     : The function Enables the Dhcp Relay             */
/*                    Circuit Id SubOption checking                    */
/*  Input(s)        : u4ContextId - Current DHCP Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                             */
/************************************************************************/
INT4
DhcpCheckIfCircuitIDSubOptionEnabled (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAICircuitIDSubOption
            == DHCP_ENABLE)
        {
            return DHCP_TRUE;
        }
    }
    return DHCP_FALSE;
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfRemoteIDSubOptionEnabled               */
/*  Description     : The function Enables the Dhcp Relay               */
/*                    RemoteID SubOption checking                       */
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpCheckIfRemoteIDSubOptionEnabled (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAIRemoteIDSubOption
            == DHCP_ENABLE)
            return DHCP_TRUE;
    }
    return DHCP_FALSE;
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfSubnetMaskSubOptionEnabled             */
/*  Description     : The function  Enables the Dhcp Relay              */
/*                     SubnetMask Sub Option checking                   */
/*  Input(s)        : u4ContextId - Current Dhcp Relaying Instance      */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpCheckIfSubnetMaskSubOptionEnabled (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAISubnetMaskSubOption
            == DHCP_ENABLE)
        {
            return DHCP_TRUE;
        }
    }
    return DHCP_FALSE;
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfVPNIDSubOptionEnabled                    */
/*  Description     : The function  Enables the Dhcp Relay              */
/*                     VPN/VRF Sub Option checking                      */
/*  Input(s)        : u4ContextId - Current Dhcp Relaying Instance      */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpCheckIfVPNIDSubOptionEnabled (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAIVPNIDSubOption
            == DHCP_ENABLE)
        {
            return DHCP_TRUE;
        }
    }
    return DHCP_FALSE;
}

/************************************************************************/
/*  Function Name   : DhcpIncrementRAIOptionInsertedCounter             */
/*  Description     : Increment of RAI insertion Counter                */
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpIncrementRAIOptionInsertedCounter (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        (DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIOptionInserted)++;
    }
}

/************************************************************************/
/*  Function Name   : DhcpIncrementCircuitIDSubOptionInsertedCounter    */
/*  Description     : Increment of Ckt ID Sub Option                    */
/*                    insertion Counter                                 */
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpIncrementCircuitIDSubOptionInsertedCounter (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        (DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAICircuitIDSubOptionInserted)++;
    }
}

/************************************************************************/
/*  Function Name   : DhcpIncrementRemoteIDSubOptionInsertedCounter     */
/*  Description     : Increment of Remote ID Sub Option                 */
/*                    insertion Counter                                 */
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpIncrementRemoteIDSubOptionInsertedCounter (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        (DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIRemoteIDSubOptionInserted)++;
    }
}

/************************************************************************/
/*  Function Name   : DhcpIncrementSubnetMaskSubOptionInsertedCounter   */
/*  Description     : Increment of Subnet Mask Sub Option               */
/*                    insertion Counter                                 */
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpIncrementSubnetMaskSubOptionInsertedCounter (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        (DHCP_RLY_CXT_NODE[u4ContextId]->
         u4StatRAISubnetMaskSubOptionInserted)++;
    }
}

/************************************************************************/
/*  Function Name   : DhcpIncrementRAIOptionWronglyInsertedCounter      */
/*  Description     : Increment of Wrong insertion of RAI option       */
/*                    Counter                                          */
/*  Input(s)        : u4ContextId - Current Relay Context               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpIncrementRAIOptionWronglyInsertedCounter (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        (DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIOptionWronglyInserted)++;
    }
}

#endif

/************************************************************************/
/*  Function Name   : DhcpRelayProcPkt                                  */
/*  Description     : The function validates the header and processes   */
/*                    message based on the message type                 */
/*  Input(s)        : pDhcpPacket - DHCP packet received                */
/*                    i4SockDesc - Socket in which packet received      */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_FAILURE/DHCP_SUCCESS                        */
/************************************************************************/

INT4
DhcpRelayProcPkt (tDhcpPktInfo * pDhcpPacket, INT4 i4SockDesc)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfCofigInfo = NULL;
#ifdef IP_WANTED
    tNetIpv4IfInfo      NetIpIfInfo;
#endif
    UINT1               u1PktBroadCastOpt = 0;
    UINT2               u2CliInfoNum;
    UINT2               u2AddedBytes;
    INT4                i4IfIndex = 0xffff;
    UINT4               u4Addr = 0;
    UINT4               u4MTU;
    UINT4               u4ServerIp = DHCP_FAILURE;
    UINT4               u4ContextId = 0;
    UINT2               u2NewLengthOfPkt;
    INT4                i4Retval = DHCP_FAILURE;
    tServer            *pServerIp = NULL;
    UINT2               u2OptionLen = 0;
    UINT2               u2PadMinOptLen = 0;
    UINT2               u2PadCount = 0;
    UINT1               u1ServerPresent = FALSE;
#ifdef RELAY_AGNT_INFO
    u2AddedBytes = 0;
#endif
#ifdef IP_WANTED
    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
#endif

    if ((DHCP_NTOHS (pDhcpPacket->Pkt.u2Flags) & BROADCASTBIT) == BROADCASTBIT)
    {
        u1PktBroadCastOpt = 1;
    }
    if (NetIpv4GetCxtId (pDhcpPacket->u4IfNum, &u4ContextId) == NETIPV4_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayProcPkt: Failed to fetch the " "context Id\r\n");
        return DHCP_FAILURE;
    }
    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                      "DhcpRelayProcPkt: DHCP Relay is not present in the "
                      "context %d\r\n", u4ContextId);
        return DHCP_FAILURE;
    }
    if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying == DHCP_DISABLE)
    {
        MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                      "DhcpRelayProcPkt: DHCP Relay is disabled in the "
                      "context %d\r\n", u4ContextId);
        return DHCP_FAILURE;
    }

    if (pDhcpPacket->Pkt.u1Op == BOOTREQUEST)
    {
        if (pDhcpPacket->Pkt.au1Options[4] != DHCP_OPT_MSG_TYPE)
        {
            if (pDhcpPacket->u2Len < BOOTP_MIN_LEN)
            {
                MOD_TRC_ARG2 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                              "BOOTP Packet length is less than minimal length : "
                              "Minimum Pkt len= %d, Arrived Pkt len= %d\n",
                              BOOTP_MIN_LEN, pDhcpPacket->u2Len);
                return DHCP_FAILURE;
            }
        }

        if (DHCP_NTOHS (pDhcpPacket->Pkt.u2Secs) < (UINT2)
            DHCP_RLY_CXT_NODE[u4ContextId]->u4CfSecsThresh)
        {
            i4Retval = DHCP_FAILURE;
            MOD_TRC_ARG2 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                          "Seconds are less than Threshold : Cfg Thresh= %d, Arrived Secs= %d \n",
                          DHCP_RLY_CXT_NODE[u4ContextId]->u4CfSecsThresh,
                          DHCP_NTOHS (pDhcpPacket->Pkt.u2Secs));
            return i4Retval;
        }

        if (pDhcpPacket->Pkt.u1Hops >
            DHCP_RLY_CXT_NODE[u4ContextId]->u4CfHopsThresh)
        {
            i4Retval = DHCP_FAILURE;
            MOD_TRC_ARG2 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                          "Hops are greater than Threshold : Cfg Thresh = %d, Arrived Hop = %d \n ",
                          DHCP_RLY_CXT_NODE[u4ContextId]->u4CfHopsThresh,
                          pDhcpPacket->Pkt.u1Hops);
            return i4Retval;
        }

        pDhcpPacket->Pkt.u1Hops += 1;

        u2OptionLen =
            pDhcpPacket->u2Len - (pDhcpPacket->Pkt.au1Options -
                                  &pDhcpPacket->Pkt.u1Op);

        /* Workaround. If the incoming packet does not have END option, set it because
         * the following code assumes the presence of END option. This workaround is valid
         * as a relay can legitimately add END option. Make sure that so adding does not
         * exceed the maximum option length supported.
         */
        if ((pDhcpPacket->Pkt.au1Options[u2OptionLen - 1] != DHCP_OPTION_END) &&
            (u2OptionLen + 1 < DHCP_OPTION_LEN))
        {
            pDhcpPacket->Pkt.au1Options[u2OptionLen++] = DHCP_OPTION_END;
            pDhcpPacket->u2Len++;
        }

/*      When ISS DHCP relay receives a bootp/dhcp request, scans the option length in 
 *      the incoming packet. 
 *      If the option length is < 64 bytes, DHCP relay shall append padding after .END. 
 *      option to reach this minimum 64 bytes of option length. */
        if (u2OptionLen < DHCP_RELAY_OPTION_MIN_LEN)
        {
            for (u2PadMinOptLen = u2OptionLen;
                 u2PadMinOptLen < DHCP_RELAY_OPTION_MIN_LEN; u2PadMinOptLen++)
            {
                pDhcpPacket->Pkt.au1Options[u2PadMinOptLen] = DHCP_OPTION_PAD;
                u2PadCount++;
            }
            pDhcpPacket->u2Len += u2PadCount;
        }
        if (pDhcpPacket->Pkt.u4Giaddr == 0)
        {

#ifdef RELAY_AGNT_INFO
            if (DhcpCheckIfRAIOptionSet (pDhcpPacket) == DHCP_TRUE)
            {

                DhcpIncrementRAIOptionWronglyInsertedCounter (u4ContextId);

                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "RAI option checking failure\n");
                return i4Retval;
            }
#endif

            if (DhcpGetInterfaceAddr ((UINT2) pDhcpPacket->u4IfNum, &u4Addr) !=
                DHCP_SUCCESS)
            {
                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Invalid Ifnum: Giaddr could not be filled\n");
                return i4Retval;
            }
            pDhcpPacket->Pkt.u4Giaddr = DHCP_HTONL (u4Addr);

#ifdef RELAY_AGNT_INFO
            if (DhcpCheckIfRAIOptionEnabled (u4ContextId) == DHCP_TRUE)
            {
                if (DhcpCheckIfPktIsDHCP (pDhcpPacket) == DHCP_TRUE)
                {
                    DhcpAddRequiredRAIOptions (pDhcpPacket, u4ContextId,
                                               &u2AddedBytes);
                }
            }
#endif
        }

#ifdef RELAY_AGNT_INFO
        else
        {
            if (DhcpCheckIfMyAddr
                (u4ContextId,
                 (DHCP_NTOHL (pDhcpPacket->Pkt.u4Giaddr))) == NETIPV4_SUCCESS)
            {
                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Wrong GiAddr Filled\n");
                return i4Retval;

            }

        }
#endif
        MakeClientEntry (pDhcpPacket, u4ContextId);

        if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfdhcpRelayFwdServersOnly
            == DHCP_ENABLE)
        {

            /*This section is modified to accomodate the multiple servers 
               implementation for the relay */

            if (DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp == NULL)
            {
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Servers Only is enabled but no Servers configured\n");
                return DHCP_FAILURE;
            }
            for (pServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp;
                 pServerIp != NULL; pServerIp = pServerIp->pNext)
            {
                if (DhcpGetDestIfIndex
                    (u4ContextId, pServerIp->IpAddr,
                     &i4IfIndex) != DHCP_SUCCESS)
                {
                    i4Retval = DHCP_FAILURE;
                    MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                  "GetRoute failed for the Server : %d \n",
                                  pServerIp->IpAddr);
                    continue;
                }

                if (u2AddedBytes != 0)
                {

                    /* With the RAI Options, if the size of the packet
                       is less that or equal to the interface MTU -
                       allow the RAI Options to remain in the packet. 
                       Else, no RAI Options should be included in the packet */

                    if (DhcpGetIfMtu (i4IfIndex, &u4MTU) != DHCP_SUCCESS)
                    {
                        i4Retval = DHCP_FAILURE;
                        MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC,
                                      "DHCP",
                                      "Invalid ifnum: MTU couldnt be obtained.. "
                                      "packet forwarding to the Server :%d failed\n",
                                      pServerIp->IpAddr);
                        continue;
                    }

                    if (u4MTU >= (UINT4) (pDhcpPacket->u2Len + u2AddedBytes))
                    {
                        pDhcpPacket->u2Len += u2AddedBytes;
                    }
                    else
                    {
                        pDhcpPacket->u2Len -= u2PadCount;
                        pDhcpPacket->Pkt.au1Options[u2OptionLen - 1] =
                            DHCP_OPTION_END;
                        DhcpIncrementSpaceConstraintCounter (u4ContextId);
                    }
                }

                i4Retval =
                    (INT4) DhcpRelaySendUnicast ((INT1 *) &pDhcpPacket->Pkt,
                                                 pDhcpPacket->u2Len,
                                                 pServerIp->IpAddr,
                                                 (UINT4) i4IfIndex,
                                                 DHCP_SERVER_PORT, i4SockDesc);

                if (i4Retval == DHCP_FAILURE)
                {
                    MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                  "Sending Message to SLI Failed for the Server : %d!!!!\n",
                                  pServerIp->IpAddr);
                    continue;
                }
                if (DhcpCheckIfRAIOptionEnabled (u4ContextId) == DHCP_TRUE)
                {
                    /* RAI option is already inserted in the packet. We need to 
                       increment the counters for this server */
                    DhcpIncrementRAIOptionInsertedCounter (u4ContextId);
                    DhcpIncrementRaiOptionCounters (u4ContextId);
                }
            }
        }
        else
        {
#ifdef IP_WANTED                /* For FSIP */
            if (NetIpv4GetFirstIfInfoInCxt (u4ContextId, &NetIpIfInfo)
                == NETIPV4_FAILURE)
            {
                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Getting Interface Index failed !!!!\n");
                return i4Retval;
            }
            do
            {
                i4IfIndex = (INT4) NetIpIfInfo.u4IfIndex;
                if (u4ContextId == NetIpIfInfo.u4ContextId)
                {
                    if (DhcpCheckIfForwardingEnabled () == DHCP_SUCCESS &&
                        i4IfIndex != (INT4) pDhcpPacket->u4IfNum)
                    {

                        u2NewLengthOfPkt = pDhcpPacket->u2Len;

                        if (DhcpGetIfMtu (i4IfIndex, &u4MTU) != DHCP_SUCCESS)
                        {
                            i4Retval = DHCP_FAILURE;
                            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                     "Invalid ifnum: MTU couldnt be obtained\n");
                            return i4Retval;
                        }

                        if (u4MTU >=
                            (UINT4) (pDhcpPacket->u2Len + u2AddedBytes))
                        {
                            u2NewLengthOfPkt =
                                (UINT2) (pDhcpPacket->u2Len + u2AddedBytes);
                        }
                        else
                        {
                            pDhcpPacket->u2Len -= u2PadCount;
                            pDhcpPacket->Pkt.au1Options[u2OptionLen - 1] =
                                DHCP_OPTION_END;
                            DhcpIncrementSpaceConstraintCounter (u4ContextId);
                        }

                        /* fix to removal of -pedantic warnings */
                        i4Retval =
                            DhcpRelaySendBroadcast ((INT1 *) &pDhcpPacket->Pkt,
                                                    u2NewLengthOfPkt, i4IfIndex,
                                                    DHCP_SERVER_PORT,
                                                    i4SockDesc);
                        if (i4Retval == DHCP_FAILURE)
                        {
                            i4Retval = DHCP_FAILURE;
                            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                     "Sending Message to SLI Failed !!!!\n");
                            return i4Retval;
                        }
                        if (DhcpCheckIfRAIOptionEnabled (u4ContextId) ==
                            DHCP_TRUE)
                        {
                            DhcpIncrementRAIOptionInsertedCounter (u4ContextId);
                            DhcpIncrementRaiOptionCounters (u4ContextId);
                        }

                    }
                }
                MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
            }
            while (NetIpv4GetNextIfInfoInCxt (u4ContextId, (UINT4) i4IfIndex,
                                              &NetIpIfInfo) == NETIPV4_SUCCESS);
#endif /* FSIP End */

#ifdef LNXIP4_WANTED
            /* For LINUXIP */
            if (DhcpGetFirstIfIndex (u4ContextId, &i4IfIndex) != DHCP_SUCCESS)
            {
                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Getting Interface Index failed !!!!\n");
                return i4Retval;
            }
            do
            {
                if (DhcpCheckIfForwardingEnabled () == DHCP_SUCCESS &&
                    i4IfIndex != (INT4) pDhcpPacket->u4IfNum)
                {

                    u2NewLengthOfPkt = pDhcpPacket->u2Len;

                    if (DhcpGetIfMtu (i4IfIndex, &u4MTU) != DHCP_SUCCESS)
                    {
                        i4Retval = DHCP_FAILURE;
                        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                 "Invalid ifnum: MTU couldnt be obtained\n");
                        return i4Retval;
                    }

                    if (u4MTU >= (UINT4) (pDhcpPacket->u2Len + u2AddedBytes))
                    {
                        u2NewLengthOfPkt =
                            (UINT2) (pDhcpPacket->u2Len + u2AddedBytes);
                    }
                    else
                    {
                        pDhcpPacket->u2Len -= u2PadCount;
                        pDhcpPacket->Pkt.au1Options[u2OptionLen - 1] =
                            DHCP_OPTION_END;
                        DhcpIncrementSpaceConstraintCounter (u4ContextId);
                    }

                    OsixTskDelay (2);
                    /* fix to removal of -pedantic warnings */
                    i4Retval =
                        DhcpRelaySendBroadcast ((INT1 *) &pDhcpPacket->Pkt,
                                                u2NewLengthOfPkt, i4IfIndex,
                                                DHCP_SERVER_PORT, i4SockDesc);
                    if (i4Retval == DHCP_FAILURE)
                    {
                        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                 "Sending Message to SLI Failed !!!!\n");
                    }
                    if (DhcpCheckIfRAIOptionEnabled (u4ContextId) == DHCP_TRUE)
                    {
                        DhcpIncrementRAIOptionInsertedCounter (u4ContextId);
                        DhcpIncrementRaiOptionCounters (u4ContextId);
                    }
                }
            }
            while (DhcpGetNextIfIndex (i4IfIndex, &i4IfIndex, u4ContextId) ==
                   DHCP_SUCCESS);
#endif /* LINUXIP END */
        }
    }                            /* BOOT REQUEST */

    else if (pDhcpPacket->Pkt.u1Op == BOOTREPLY)
    {
        UINT4               u4CktID;
        INT4                i4CktIDSetFlag;

        i4CktIDSetFlag = FALSE;

        /* Copy the source IP address */
        u4ServerIp = DHCP_NTOHL (pDhcpPacket->Pkt.u4Siaddr);

        /* Check whether Server only option is enabled and packet is 
         * having the source ip address */
        if ((DHCP_RLY_CXT_NODE[u4ContextId]->u4CfdhcpRelayFwdServersOnly ==
             DHCP_ENABLE) && (u4ServerIp != ZERO))
        {

            /* Check whether the source ip of Reply mesage is available in the
             * server list */

            if (DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp != NULL)
            {
                for (pServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp;
                     pServerIp != NULL; pServerIp = pServerIp->pNext)
                {

                    if (pServerIp->IpAddr == u4ServerIp)
                    {
                        u1ServerPresent = TRUE;
                        break;
                    }
                }

                if (u1ServerPresent == FALSE)
                {
                    /* Packet from Untrusted Server Drop it */
                    return DHCP_FAILURE;
                }
            }
        }

        if (pDhcpPacket->Pkt.au1Options[4] != DHCP_OPT_MSG_TYPE)
        {
            if (pDhcpPacket->u2Len < BOOTP_MIN_LEN)
            {
                MOD_TRC_ARG2 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                              "BOOTP Packet length is less than minimal length : "
                              "Minimum Pkt len= %d, Arrived Pkt len= %d\n",
                              BOOTP_MIN_LEN, pDhcpPacket->u2Len);

                return DHCP_FAILURE;
            }
        }

#ifdef RELAY_AGNT_INFO
        if (DhcpCheckIfMyAddr
            (u4ContextId,
             (DHCP_NTOHL (pDhcpPacket->Pkt.u4Giaddr))) == NETIPV4_SUCCESS)
        {
            if (DhcpCheckIfPktIsDHCP (pDhcpPacket) == DHCP_TRUE)
            {
                i4CktIDSetFlag =
                    DhcpGetCircuitIDFromPacket (pDhcpPacket, &u4CktID);
                DhcpStripOffRAIOptions (pDhcpPacket);
            }
        }
        else
        {
            i4Retval = DHCP_FAILURE;
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     " GiAddr Address is Wrong !!!!\n");
            return i4Retval;

        }
#endif

        if (i4CktIDSetFlag != DHCP_TRUE)
        {
            for (u2CliInfoNum = 0; u2CliInfoNum < MAX_CLIENT_INFO;
                 u2CliInfoNum++)
            {
                if ((DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->
                     u4Xid == pDhcpPacket->Pkt.u4Xid)
                    &&
                    (MEMCMP
                     ((DHCP_RLY_CXT_NODE[u4ContextId]->
                       apClntInfo[u2CliInfoNum]->u1Chaddr),
                      pDhcpPacket->Pkt.u1Chaddr, DHCP_HARD_ADD_LEN) == ZERO))
                {
                    DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->
                        u1Used = DHCP_FALSE;
                    i4CktIDSetFlag = DHCP_TRUE;
                    u4CktID =
                        DHCP_RLY_CXT_NODE[u4ContextId]->
                        apClntInfo[u2CliInfoNum]->u2Port;
                    break;
                }
            }
        }

        if (i4CktIDSetFlag == DHCP_TRUE)
        {
            MEMSET (&DhrlIfConfigInfo, 0, sizeof (tDhrlIfConfigInfo));
            DhrlIfConfigInfo.u4CxtId = u4ContextId;
            DhrlIfConfigInfo.u4CircuitId = u4CktID;
            if ((pDhrlIfCofigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[u4ContextId]->pCktIdToIfRBRoot,
                            &DhrlIfConfigInfo)) != NULL)
            {
                u4CktID = pDhrlIfCofigInfo->u4Port;
            }

            if (DhcpSendToClient (u4CktID,
                                  pDhcpPacket,
                                  u1PktBroadCastOpt,
                                  i4SockDesc) == DHCP_FAILURE)
            {
                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Adding ARP Entry failed !!!!\n");
                return i4Retval;
            }
        }
        else
        {
            if (NetIpv4GetIfIndexFromAddrInCxt (u4ContextId,
                                                DHCP_NTOHL (pDhcpPacket->Pkt.
                                                            u4Giaddr),
                                                (UINT4 *) &i4IfIndex) ==
                NETIPV4_FAILURE)
            {
                i4Retval = DHCP_FAILURE;
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC,
                         "DHCP", "NetIpv4GetIfIndexFromAddr failed !!!!\n");
                return i4Retval;
            }
            if (DhcpCheckIfForwardingEnabled () == DHCP_SUCCESS &&
                i4IfIndex != (INT4) pDhcpPacket->u4IfNum)
            {
                if (DhcpSendToClient (i4IfIndex, pDhcpPacket,
                                      u1PktBroadCastOpt,
                                      i4SockDesc) == DHCP_FAILURE)
                {
                    i4Retval = DHCP_FAILURE;
                    MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC,
                             "DHCP", "Adding ARP Entry failed !!!!\n");
                    return i4Retval;
                }
            }
        }
    }                            /* Pkt is BOOT REPLY */
    else
    {
        i4Retval = DHCP_FAILURE;
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Unknown Type Of pkt is Recvd !!! \n");
        return i4Retval;
    }
    UNUSED_PARAM (u2NewLengthOfPkt);
    return DHCP_SUCCESS;
}

/*******************************************************************************
 * Name             : DhrlProcessIfaceDeletion
 * Description      : Processes the vlan Interface deletion Event.
 * Global Variables :
 * Inputs           : u4PortNo - IP Port Number
 *                    u4ContextId - Current Dhcp Relay Instance
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
DhrlProcessIfaceDeletion (UINT4 u4PortNo, UINT4 u4ContextId)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhrlProcessIfaceDeletion: Failed to delete "
                 "Interface Config Info\r\n");
        return;
    }
    DhrlIfConfigInfo.u4Port = u4PortNo;
    DhrlIfConfigInfo.u4CxtId = u4ContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[u4ContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) == NULL)
    {
        /* Such an entry does not exist */
        return;
    }

    RBTreeRem (DHCP_RLY_CXT_NODE[u4ContextId]->pIfToIdRBRoot,
               pDhrlIfConfigInfo);
    RBTreeRem (DHCP_RLY_CXT_NODE[u4ContextId]->pCktIdToIfRBRoot,
               pDhrlIfConfigInfo);
    RBTreeRem (DHCP_RLY_CXT_NODE[u4ContextId]->pRemoteIdToIfRBRoot,
               pDhrlIfConfigInfo);
    DHCP_RELEASE_IF_CONFIG_POOL (pDhrlIfConfigInfo);
}

/************************************************************************/
/*  Function Name   : MakeClientEntry                                   */
/*  Description     : Database of Client Requests                       */
/*  Input(s)        : pPktInfo - Packet Info.                           */
/*                    u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_FAILURE/DHCP_SUCCESS                         */
/************************************************************************/

INT4
MakeClientEntry (tDhcpPktInfo * pPktInfo, UINT4 u4ContextId)
{

    UINT2               u2CliInfoNum;
    UINT2               u2FreeIndex = 0;
    UINT2               u2FreeEntryFlag = DHCP_FALSE;
    static int          u2CurrIndex = 0;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return DHCP_FAILURE;
    }

    for (u2CliInfoNum = 0; u2CliInfoNum < MAX_CLIENT_INFO; u2CliInfoNum++)
    {
        if ((DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum] != NULL)
            && (DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->
                u4Xid == pPktInfo->Pkt.u4Xid)
            && (DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->
                u1Used == DHCP_TRUE))
        {
            /*Entry already exists. Update the port no and HW address */
            DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->u2Port =
                (UINT2) pPktInfo->u4IfNum;
            MEMCPY (DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->
                    u1Chaddr, pPktInfo->Pkt.u1Chaddr, DHCP_HARD_ADD_LEN);
            return DHCP_SUCCESS;
        }
        if ((DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->u1Used ==
             DHCP_FALSE) && (u2FreeEntryFlag == DHCP_FALSE))
        {
            /*Found the first free index. note it */
            u2FreeIndex = u2CliInfoNum;
            u2FreeEntryFlag = DHCP_TRUE;
        }
    }

    if (u2FreeEntryFlag == DHCP_TRUE)
    {
        DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2FreeIndex]->u1Used =
            DHCP_TRUE;
        DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2FreeIndex]->u2Port =
            (UINT2) pPktInfo->u4IfNum;
        DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2FreeIndex]->u4Xid =
            pPktInfo->Pkt.u4Xid;
        MEMCPY ((DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2FreeIndex]->
                 u1Chaddr), pPktInfo->Pkt.u1Chaddr, DHCP_HARD_ADD_LEN);
        return DHCP_SUCCESS;
    }
    if (u2CliInfoNum == MAX_CLIENT_INFO)
    {
        u2CliInfoNum = (UINT2) u2CurrIndex;

        u2CurrIndex++;
        if (u2CurrIndex == MAX_CLIENT_INFO)
        {
            u2CurrIndex = 0;
        }

        if (u2CliInfoNum < MAX_CLIENT_INFO)
        {
            DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->u1Used =
                DHCP_TRUE;
            DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->u2Port =
                (UINT2) pPktInfo->u4IfNum;
            DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->u4Xid =
                pPktInfo->Pkt.u4Xid;
            MEMCPY ((DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum]->
                     u1Chaddr), pPktInfo->Pkt.u1Chaddr, DHCP_HARD_ADD_LEN);
        }
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfPktIsDHCP                */
/*  Description     : Dhcp Packet validation                           */
/*  Input(s)        : tDhcpPktInfo                                       */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_FAILURE/DHCP_SUCCESS                         */
/************************************************************************/

#ifdef RELAY_AGNT_INFO
INT4
DhcpCheckIfPktIsDHCP (tDhcpPktInfo * pPkt)
{
    UINT4               u4OptionsCookie;

    u4OptionsCookie = DHCP_NTOHL (DHCP_OPTIONS_COOKIE);

    if (MEMCMP (&pPkt->Pkt.au1Options[0], &u4OptionsCookie,
                DHCP_COOKIE_SIZE) == 0)
    {
        return DHCP_TRUE;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfRAIOptionSet                   */
/*  Description     : Dhcp Packet validation  for RAIOption            */
/*  Input(s)        : tDhcpPktInfo                                       */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpCheckIfRAIOptionSet (tDhcpPktInfo * pPkt)
{

    return (DhcpCheckIfThisOptionIsSet (pPkt, DHCP_OPTION_RAI));
}

/************************************************************************/
/*  Function Name   : DhcpCheckIfThisOptionIsSet                        */
/*  Description     : Dhcp Packet validation  for RAI Options           */
/*  Input(s)        : tDhcpPktInfo  , option                                     */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpCheckIfThisOptionIsSet (tDhcpPktInfo * pPkt, UINT4 u4Option)
{

    UINT2               u2LenTrav = 4;    /* The first 4 bytes are for the COOKIES */

    while ((u2LenTrav < pPkt->u2Len) && (u2LenTrav < DHCP_OPTION_LEN))
    {

        if (pPkt->Pkt.au1Options[u2LenTrav] == u4Option)
            return DHCP_TRUE;

        if (pPkt->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_PAD ||
            pPkt->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_END)
        {
            u2LenTrav++;
            continue;
        }

        if ((u2LenTrav + 1) < DHCP_OPTION_LEN)
            u2LenTrav += pPkt->Pkt.au1Options[u2LenTrav + 1] + 2;
        else
            return DHCP_FALSE;
    }

    return DHCP_FALSE;

}

/************************************************************************/
/*  Function Name   : DhcpAddRequiredRAIOptions                         */
/*  Description     : Adding of RAI options from Client to server Pkt   */
/*  Input(s)        : tDhcpPktInfo,u4ContextId                          */
/*  Output(s)       : Number of Bytes of RAI options Added              */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

VOID
DhcpAddRequiredRAIOptions (tDhcpPktInfo * pDhcpPacket, UINT4 u4ContextId,
                           UINT2 *pu2AddedBytes)
{

    UINT2               u2LenTrav = 4;    /* The first 4 bytes are for the COOKIES */
    UINT2               u2OrigPktEnd, u2LenPosn;
    UINT2               u2OptionLen;
    UINT2               u2PadBytes = 0;
    INT4                i4RetVal = 0;
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    UINT4               u4IPAddr = 0;
    UINT4               u4SubnetMask = 0;
    UINT1               u1Len = 0;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

    u2OptionLen =
        pDhcpPacket->u2Len - (pDhcpPacket->Pkt.au1Options -
                              &pDhcpPacket->Pkt.u1Op);

    /* u2LenTrav check has been added for Klocworks warnings removal */
    while ((u2LenTrav < u2OptionLen) && (u2LenTrav < DHCP_OPTION_LEN))
    {

        if (pDhcpPacket->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_PAD)
        {
            u2LenTrav++;
            continue;
        }

        if (pDhcpPacket->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_END)
            break;

        if ((u2LenTrav + 1) < DHCP_OPTION_LEN)
            u2LenTrav += pDhcpPacket->Pkt.au1Options[u2LenTrav + 1] + 2;
        else
            return;
    }

    if (u2OptionLen > u2LenTrav)
    {
        /*option len - end option byte = extra pad bytes */
        u2PadBytes = u2OptionLen - (u2LenTrav + 1);
    }

    /* u2LenTrav ++;  Point to the next position after the END option 
     * The END OPTION is overwritten and moved to end of the relay agent options
     * 
     * */

    if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        return;
    pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_OPTION_RAI;

    /* Mark the place where the option length needs to be filled */
    u2LenPosn = u2LenTrav;

    if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        return;
    pDhcpPacket->Pkt.au1Options[u2LenTrav++] = 0;
    u2OrigPktEnd = u2LenTrav;

    if (DhcpCheckIfCircuitIDSubOptionEnabled (u4ContextId) == DHCP_TRUE)
    {
        UINT4               u4CktID = 0;
        /* changed defination to remove -pedantic swich */
        CHR1                ac1CktID[DHCP_MAX_CKTID_SIZE];
        UINT4               u4CfaIfIndex = 0;
        UINT2               u2VlanId = 0;
        UINT2               u2Port = 0;
        UINT1               u1IfType = 0;
        UINT1               u1BridgedIface = 0;
        UINT1               au1MacAddr[DHCP_MAC_ADDRESS_LENGTH];
        UINT1               u1Status = 0;
        CHR1               *pTemp = NULL;
        tCfaIfInfo          CfaIfInfo;

        pTemp = ac1CktID;

        MEMSET (ac1CktID, 0, DHCP_MAX_CKTID_SIZE);
        MEMSET (au1MacAddr, 0, DHCP_MAC_ADDRESS_LENGTH);

        if (DhcpGetCircuitID (pDhcpPacket, u4ContextId, &u4CktID) ==
            DHCP_SUCCESS)
        {
            if (u2LenTrav > (DHCP_OPTION_LEN - 1))
                return;
            pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_CKTID;
            DHCP_SPRINTF (ac1CktID, "%u", u4CktID);
            u1Len = (UINT1) DHCP_STRLEN (ac1CktID);
            /* Copy the CircuitId Option Length and Value into Packet */
            if (u2LenTrav > (DHCP_OPTION_LEN - 1))
                return;
            pDhcpPacket->Pkt.au1Options[u2LenTrav++] = u1Len;
            if (u2LenTrav >= DHCP_OPTION_LEN)
                return;
            MEMCPY (&pDhcpPacket->Pkt.au1Options[u2LenTrav], ac1CktID, u1Len);
            u2LenTrav += (UINT2) u1Len;
        }
        else
        {
            if (u2LenTrav > (DHCP_OPTION_LEN - 1))
                return;
            pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_CKTID;

            if (NetIpv4GetCfaIfIndexFromPort (pDhcpPacket->u4IfNum,
                                              &u4CfaIfIndex) == NETIPV4_SUCCESS)
            {
                if (CfaGetIfInfo ((UINT2) u4CfaIfIndex, &CfaIfInfo)
                    == CFA_SUCCESS)
                {
                    u2VlanId = CfaIfInfo.u2VlanId;
                    u1IfType = CfaIfInfo.u1IfType;
                    u1BridgedIface = CfaIfInfo.u1BridgedIface;
                }
            }
            else
            {
                u4CfaIfIndex = 0;
                u2VlanId = 0;
            }

            if ((u1IfType == CFA_ENET) && (u1BridgedIface == CFA_DISABLED))
            {

                u2Port = (UINT2) u4CfaIfIndex;
            }
            else
            {

                MEMCPY (au1MacAddr, pDhcpPacket->Pkt.u1Chaddr,
                        DHCP_MAC_ADDRESS_LENGTH);

                i4RetVal =
                    VlanGetFdbEntryDetails ((UINT4) u2VlanId, au1MacAddr,
                                            &u2Port, &u1Status);
                UNUSED_PARAM (i4RetVal);
            }

            /* Add the interface index in the option 82 */
            if ((DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAICktIDDefaultOptiontype &
                 DHCP_CIRCUITID_TYPE_INTERFACE_INDEX)
                == DHCP_CIRCUITID_TYPE_INTERFACE_INDEX)
            {
                u1Len = 0;
                DHCP_SPRINTF (pTemp, "%u ", u4CfaIfIndex);
                u1Len = (UINT1) DHCP_STRLEN (pTemp);
                pTemp += u1Len;
            }

            /* Add the VlanID in the option 82 */
            if ((DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAICktIDDefaultOptiontype &
                 DHCP_CIRCUITID_TYPE_VLANID) == DHCP_CIRCUITID_TYPE_VLANID)
            {
                u1Len = 0;
                DHCP_SPRINTF (pTemp, "%d ", u2VlanId);
                u1Len = (UINT1) DHCP_STRLEN (pTemp);
                pTemp += u1Len;
            }

            /* Add the physical port or lag port in the option 82 */
            if ((DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRAICktIDDefaultOptiontype &
                 DHCP_CIRCUITID_TYPE_PHYPORT_LAGPORT)
                == DHCP_CIRCUITID_TYPE_PHYPORT_LAGPORT)
            {

                u1Len = 0;
                DHCP_SPRINTF (pTemp, "%d ", u2Port);
                u1Len = (UINT1) DHCP_STRLEN (pTemp);
                pTemp += u1Len;

            }
            if (*(pTemp - 1) == ' ')
            {
                *(pTemp - 1) = '\0';
            }

            u1Len = (UINT1) DHCP_STRLEN (ac1CktID);
            /* finally Copy the CircuitId Option Length and Value into Packet */

            if (u2LenTrav > (DHCP_OPTION_LEN - 1))
                return;
            pDhcpPacket->Pkt.au1Options[u2LenTrav++] = u1Len;

            if (u2LenTrav >= DHCP_OPTION_LEN)
                return;
            MEMCPY (&pDhcpPacket->Pkt.au1Options[u2LenTrav], ac1CktID, u1Len);
            u2LenTrav += (UINT2) u1Len;

        }
    }

    if (DhcpCheckIfRemoteIDSubOptionEnabled (u4ContextId) == DHCP_TRUE)
    {
        UINT1               au1RemoteID[DHRL_MAX_RID_LEN];

        DhcpGetRemoteID (pDhcpPacket, u4ContextId, au1RemoteID);
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
            return;
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_REMOTEID;
        u1Len = (UINT1) DHCP_STRLEN (au1RemoteID);
        /* Copy the RemoteId Option Length and Value into Packet */
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
            return;
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = u1Len;
        if (u2LenTrav >= DHCP_OPTION_LEN)
            return;
        MEMCPY (&pDhcpPacket->Pkt.au1Options[u2LenTrav], au1RemoteID, u1Len);
        u2LenTrav += (UINT2) u1Len;

    }

    /*   Added as per RFC 5010 - Relay Agent Flags Suboption */

    /*   The following flag is defined to indicate whether the DHCP relay received the packet via a unicast
       or broadcast packet.  This information may be used by the DHCP server to better serve 
       clients based on whether their request was originally broadcast or unicast. */

    if (pDhcpPacket->Pkt.u2Flags == DHCP_OPTION_CLIENT_BROADCAST_BIT)
    {
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_FLAGS_CODE;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_FLAGS_LENGTH;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] =
            DHCP_SUBOPTION_FLAGS_BROADCAST_BIT;
    }
    else
    {
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_FLAGS_CODE;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_FLAGS_LENGTH;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] =
            DHCP_SUBOPTION_FLAGS_UNICAST_BIT;

    }
    if (DhcpCheckIfVPNIDSubOptionEnabled (u4ContextId) == DHCP_TRUE)
    {
        /* Filling Sub options for VRF */
        MEMSET (au1CxtName, 0, VCMALIAS_MAX_ARRAY_LEN);
        u4SubnetMask = 0;
        u1Len = 0;

        DhcpGetInterfaceAddr ((UINT2) pDhcpPacket->u4IfNum, &u4IPAddr);

        DhcpGetSubnetMask (pDhcpPacket, &u4SubnetMask);
        u4SubnetMask = (u4IPAddr & u4SubnetMask);

        /* Filling VRF name for Sub option VPN ID */
        VcmGetAliasName (u4ContextId, au1CxtName);
        u1Len = (UINT1) DHCP_STRLEN (au1CxtName);

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_VPN;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = u1Len;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        MEMCPY (&pDhcpPacket->Pkt.au1Options[u2LenTrav], au1CxtName, u1Len);
        u2LenTrav = (UINT2) (u2LenTrav + u1Len);

        /* Filling Subnet mask for Link Select sub option */
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_SUBOPTION_LINK_SELECT;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] =
            DHCP_SUBOPTION_SUBNETMASK_SIZE;

        u4SubnetMask = DHCP_HTONL (u4SubnetMask);
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        MEMCPY (&pDhcpPacket->Pkt.au1Options[u2LenTrav], &u4SubnetMask,
                DHCP_SUBOPTION_SUBNETMASK_SIZE);
        u2LenTrav = (UINT2) (u2LenTrav + DHCP_SUBOPTION_SUBNETMASK_SIZE);

        /* Filling interface IP address for Server Override sub option */
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] =
            DHCP_SUBOPTION_SERVER_OVERRIDE;

        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        pDhcpPacket->Pkt.au1Options[u2LenTrav++] =
            DHCP_SUBOPTION_SUBNETMASK_SIZE;

        u4IPAddr = DHCP_HTONL (u4IPAddr);
        if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        {
            return;
        }
        MEMCPY (&pDhcpPacket->Pkt.au1Options[u2LenTrav], &u4IPAddr,
                DHCP_SUBOPTION_SUBNETMASK_SIZE);
        u2LenTrav = (UINT2) (u2LenTrav + DHCP_SUBOPTION_SUBNETMASK_SIZE);
    }

    *pu2AddedBytes = (UINT2) (u2LenTrav - u2OrigPktEnd);
    /* Fill the Length of RAI Options */
    pDhcpPacket->Pkt.au1Options[u2LenPosn] = (UINT1) *pu2AddedBytes;

    /* Incremented by 2 to include the RAI option code and end option code */
    *pu2AddedBytes += 2;
    if (u2LenTrav > (DHCP_OPTION_LEN - 1))
        return;
    pDhcpPacket->Pkt.au1Options[u2LenTrav++] = DHCP_OPTION_END;

    /* If my RAI has been over-written over padded bytes
     * do not increment the total packet length */
    if (*pu2AddedBytes < u2PadBytes)
    {
        *pu2AddedBytes = 0;
    }
    /* Else addedbytes are the bytes that are added
     * above the padded bytes */
    else
    {
        *pu2AddedBytes -= u2PadBytes;
    }

}

/*This function is added as part of implementation for multiple servers*/

/************************************************************************/
/*  Function Name   : DhcpIncrementRaiOptionCounters                    */
/*  Description     : Incrementing the counters for rai options inserted*/
/*  Input(s)        : u4ContextId - Current Dhcp Relay Instance         */
/*  Output(s)       : Rai option counters are incremented               */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpIncrementRaiOptionCounters (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        if (DhcpCheckIfCircuitIDSubOptionEnabled (u4ContextId) == DHCP_TRUE)
        {
            DhcpIncrementCircuitIDSubOptionInsertedCounter (u4ContextId);
        }
        if (DhcpCheckIfRemoteIDSubOptionEnabled (u4ContextId) == DHCP_TRUE)
        {
            DhcpIncrementRemoteIDSubOptionInsertedCounter (u4ContextId);
        }
        if (DhcpCheckIfSubnetMaskSubOptionEnabled (u4ContextId) == DHCP_TRUE)
        {
            DhcpIncrementSubnetMaskSubOptionInsertedCounter (u4ContextId);
        }
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpStripOffRAIOptions                            */
/*  Description     : Removing of RAI options from Server to Clinet Pkt  */
/*  Input(s)        : tDhcpPktInfo                                       */
/*  Output(s)       : Number of Bytes of RAI options Added              */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

VOID
DhcpStripOffRAIOptions (tDhcpPktInfo * pDhcpPacket)
{

    UINT2               u2LenTrav = 4;    /* The first 4 bytes are for the COOKIES */
    UINT2               u2RAIOptionLen = 0;
    UINT2               u2Index = 0;
    UINT2               u2OptionLen = 0;
    UINT2               u2PadIndex = 0;
    UINT2               u2PadCount = 0;

    u2OptionLen =
        pDhcpPacket->u2Len - (pDhcpPacket->Pkt.au1Options -
                              &pDhcpPacket->Pkt.u1Op);

    while ((u2LenTrav < pDhcpPacket->u2Len) && (u2LenTrav < DHCP_OPTION_LEN))
    {
        if (pDhcpPacket->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_PAD ||
            pDhcpPacket->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_END)
        {
            u2LenTrav++;
            continue;
        }

        if (pDhcpPacket->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_RAI)
        {
            if ((u2LenTrav + 1) < DHCP_OPTION_LEN)
                u2RAIOptionLen =
                    (UINT2) (pDhcpPacket->Pkt.au1Options[u2LenTrav + 1] + 2);
            /* 1 byte for RAI Option
               1 byte for the length field */
            /* Overwrite the remaining options on the RAI options */
            for (u2Index = u2LenTrav;
                 (((u2Index + u2RAIOptionLen) <= (u2OptionLen))
                  && (u2Index < DHCP_OPTION_LEN)); u2Index++)
            {
                if ((u2Index + u2RAIOptionLen) < DHCP_OPTION_LEN)
                    pDhcpPacket->Pkt.au1Options[u2Index] =
                        pDhcpPacket->Pkt.au1Options[u2Index + u2RAIOptionLen];
            }
            break;
        }
        if ((u2LenTrav + 1) < DHCP_OPTION_LEN)
            u2LenTrav += pDhcpPacket->Pkt.au1Options[u2LenTrav + 1] + 2;
        else
            return;

    }

    /* If minimum option length falls below 64 after stripping the RAI,
     * then PAD uptil 64 bytes are reached in my option length */
    if ((u2RAIOptionLen != 0)
        && ((u2OptionLen - u2RAIOptionLen) < DHCP_RELAY_OPTION_MIN_LEN))
    {
        for (u2PadIndex = u2Index; u2PadIndex <= DHCP_RELAY_OPTION_MIN_LEN;
             u2PadIndex++)
        {
            pDhcpPacket->Pkt.au1Options[u2PadIndex] = DHCP_OPTION_PAD;
            u2PadCount++;
        }
        pDhcpPacket->u2Len += u2PadCount;
    }
    pDhcpPacket->u2Len -= u2RAIOptionLen;
}

/************************************************************************/
/*  Function Name   : DhcpGetCircuitIDFromPacket                        */
/*  Description     : Reading of of CKT ID option                       */
/*  Input(s)        : tDhcpPktInfo                                       */
/*  Output(s)       : Circuit Id                    */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/

INT4
DhcpGetCircuitIDFromPacket (tDhcpPktInfo * pPkt, UINT4 *pu4CktID)
{

    UINT2               u2LenTrav = 4;    /* The first 4 bytes are for the COOKIES */
    UINT1               u1Idx;
    UINT1               u1CktIdLen = 0;
    UINT4               u4CfaIfIndex = 0;

    *pu4CktID = 0;

    while ((u2LenTrav < pPkt->u2Len) && (u2LenTrav < DHCP_OPTION_LEN))
    {

        if (pPkt->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_RAI)
            break;

        if (pPkt->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_PAD ||
            pPkt->Pkt.au1Options[u2LenTrav] == DHCP_OPTION_END)
        {
            u2LenTrav++;
            continue;
        }

        if ((u2LenTrav + 1) < DHCP_OPTION_LEN)
            u2LenTrav += pPkt->Pkt.au1Options[u2LenTrav + 1] + 2;
        else
            return DHCP_FALSE;

    }

    if (u2LenTrav >= pPkt->u2Len)
        return DHCP_FALSE;

    u2LenTrav += 2;

    while ((u2LenTrav < pPkt->u2Len) && (u2LenTrav < DHCP_OPTION_LEN))
    {
        if (pPkt->Pkt.au1Options[u2LenTrav] == DHCP_SUBOPTION_CKTID)
        {
            u2LenTrav++;
            if (u2LenTrav >= DHCP_OPTION_LEN)
                return DHCP_FALSE;
            u1CktIdLen = pPkt->Pkt.au1Options[u2LenTrav];

            for (u1Idx = 0;
                 ((u2LenTrav < DHCP_OPTION_LEN) && (u1Idx < u1CktIdLen));
                 u1Idx++)
            {
                u2LenTrav++;
                u4CfaIfIndex *= 10;
                if (u2LenTrav >= DHCP_OPTION_LEN)
                    return DHCP_FALSE;
                u4CfaIfIndex += pPkt->Pkt.au1Options[u2LenTrav] - '0';
            }

            if (NetIpv4GetPortFromIfIndex (u4CfaIfIndex, pu4CktID) ==
                NETIPV4_FAILURE)
            {
                return DHCP_FALSE;
            }
            return DHCP_TRUE;
        }

        if ((u2LenTrav + 1) < DHCP_OPTION_LEN)
            u2LenTrav += pPkt->Pkt.au1Options[u2LenTrav + 1] + 2;
        else
            return DHCP_FALSE;
    }
    return DHCP_FALSE;
}

#endif /* RELAY_AGNT_INFO */

/************************************************************************/
/*  Function Name   : DhcpRelayShutdown                                 */
/*  Description     : Disabling a relay on a context closes the socket  */
/*                    opened in that context and deregisters with IP &  */
/*                    NP layers. It clears the statistics related varia-*/
/*                    bles and sets the relay status as disabled.       */
/*                    Disabling relay will not release the memories     */
/*                    allocated for dynamic data strutures. This will   */
/*                    help to retain the relay configurations that has  */
/*                    been done before disabling the relay.             */
/*  Input(s)        : u4ContextId - DHCP Relay Instance ID              */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpRelayShutdown (UINT4 u4ContextId)
{
#ifdef IP_WANTED
    UINT1               u1IsRlyEnabled = DHCP_FALSE;
#endif
    INT4                i4SockDesc = DHCP_RLY_INV_SOCKET_ID;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

#ifdef IP_WANTED
    i4SockDesc = gRlyNode.i4DhcpRelaySockDesc;
#endif

#ifdef LNXIP4_WANTED
    i4SockDesc = DHCP_RLY_CXT_NODE[u4ContextId]->i4DhcpRelaySockDesc;
#endif

#ifdef LNXIP4_WANTED
    if (DhcpRlyCloseSocket (i4SockDesc) == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "socket close (relay) failed !!!\n");
    }

    DHCP_RLY_CXT_NODE[u4ContextId]->i4DhcpRelaySockDesc =
        DHCP_RLY_INV_SOCKET_ID;
#endif

    DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying = DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4ContextId]->u1Status = (UINT1) NOT_READY;
#ifdef RELAY_AGNT_INFO
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAICircuitIDSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIRemoteIDSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAISubnetMaskSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIOptionWronglyInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAISpaceConstraint = 0;
#endif

#ifdef IP_WANTED
    /* For FSIP, need to close the Socket, only when the relay
     * is disabled in all the contexts */
    u1IsRlyEnabled = DhcpIsRlyEnabledInAnyCxt ();
    if (u1IsRlyEnabled == DHCP_FALSE)
    {
        /* Relay is disabled in all the contexts; So close the socket */
        if (DhcpRlyCloseSocket (i4SockDesc) == DHCP_FAILURE)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "socket close (relay) failed !!!\n");
        }
        /* Invalidate the Socket descriptor */
        gRlyNode.i4DhcpRelaySockDesc = DHCP_RLY_INV_SOCKET_ID;
    }
#endif

#ifdef L3_SWITCHING_WANTED
    /* disable receiving DHCP relay packets */
    DhrlFsNpDhcpRlyDeInit (u4ContextId);
#endif
    UNUSED_PARAM (i4SockDesc);
    return;
}

/************************************************************************/
/*  Function Name   : DhcpRelayInitFreeServersPool                      */
/*  Description     : This function Create a pool of records to store   */
/*                    the Dhcp Server informations                      */
/*  Input(s)        : u4ContextId.                                          */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpRelayInitFreeServersPool (UINT4 u4ContextId)
{
    tServer            *pServerIp = NULL;
    UINT4               u4Count;

    if (DHCP_ALLOC_SERVER_POOL (pServerIp) == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayInitFreeServersPool : Failed to allocate "
                 "Memory for tDhrlMaxServer\r\n");
        return DHCP_FAILURE;
    }

    /* Store the start address of the mempool block */
    DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersPool = pServerIp;

    /* Create a linked list for Free Servers */
    DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp = pServerIp;
    for (u4Count = 1; u4Count < MAX_SERVERS; u4Count++)
    {
        pServerIp->pNext = pServerIp + 1;
        ++pServerIp;
    }

    pServerIp->pNext = NULL;

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpRelayGetFreeServerRec                         */
/*  Description     : This function returns a free record to store the  */
/*                    Dhcp Server Informations in the given context     */
/*  Input(s)        : u4ContextId - DHCP RELAY instance id              */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to a new Server Record if available,      */
/*                    NULL otherwise                                    */
/************************************************************************/
tServer            *
DhcpRelayGetFreeServerRec (UINT4 u4ContextId)
{
    tServer            *pServerIp = NULL;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return (NULL);
    }
    if (DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp == NULL)
    {
        return (NULL);
    }

    pServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp;
    DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp
        = DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp->pNext;
    pServerIp->pNext = NULL;

    MEMSET (pServerIp, 0, sizeof (tServer));

    return (pServerIp);
}

/************************************************************************/
/*  Function Name   : DhcpRelayAddtoFreeServer                          */
/*  Description     : This function adds a server record to the         */
/*                    existing Free servers record in the given context */
/*  Input(s)        : u4ContextId - DHCP RELAY instance id              */
/*                    pServerIp - Entry to be added                     */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpRelayAddToFreeServer (UINT4 u4ContextId, tServer * pFreeServer)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }
    if (pFreeServer != NULL)
    {
        pFreeServer->pNext = DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp;
        DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp = pFreeServer;
    }

    return;
}

/************************************************************************/
/*  Function Name   : DhcpRelayAddServer                                */
/*  Description     : This function adds a server record to the         */
/*                    existing entries of a particular context          */
/*  Input(s)        : u4ContextId - Context Id                          */
/*                    pServerIp - Entry to be added                     */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpRelayAddServer (UINT4 u4ContextId, tServer * pServerIp)
{
    tServer            *pCurrServerIp = NULL;
    tServer            *pPrevServerIp = NULL;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }
    if (pServerIp != NULL)
    {
        for (pCurrServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp;
             pCurrServerIp != NULL; pCurrServerIp = pCurrServerIp->pNext)
        {

            if (pCurrServerIp->IpAddr > pServerIp->IpAddr)
            {
                break;
            }

            pPrevServerIp = pCurrServerIp;
        }

        if (pPrevServerIp == NULL)
        {
            pServerIp->pNext = pCurrServerIp;
            DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp = pServerIp;
        }
        else
        {
            pServerIp->pNext = pCurrServerIp;
            pPrevServerIp->pNext = pServerIp;
        }
    }

    return;
}

/************************************************************************/
/*  Function Name   : DhcpRelayRemoveServerRec                          */
/*  Description     : This function removes a server record allocated   */
/*                    and adds it to the free list for the given context*/
/*  Input(s)        : u4ContextId - Context Id                          */
/*                    pServerIp - Entry to be freed                     */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpRelayRemoveServerRec (UINT4 u4ContextId, tServer * pServerIp)
{
    tServer            *pCurrServerIp = NULL;
    tServer            *pPrevServerIp = NULL;

    if (pServerIp == NULL || DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

    if (pServerIp == DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp)
    {
        DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp =
            (DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp == NULL) ?
            NULL : DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp->pNext;
        return;
    }

    pPrevServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp;

    for (pCurrServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp;
         pCurrServerIp != NULL; pCurrServerIp = pCurrServerIp->pNext)
    {
        if (pCurrServerIp == pServerIp)
        {
            pPrevServerIp->pNext = pCurrServerIp->pNext;
            return;
        }

        pPrevServerIp = pCurrServerIp;
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpRelayGetServer                                */
/*  Description     : This function finds a server record for a given   */
/*                    ip address & Context Id                           */
/*  Input(s)        : u4ContextId - Context Id                          */
/*                    u4IpAddress - Ip address of the server            */
/*  Output(s)       : None.                                             */
/*  Returns         : if found, Server record corresponding to the ip   */
/*                    address, NULL otherwise                           */
/************************************************************************/
tServer            *
DhcpRelayGetServer (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    tServer            *pServerIp = NULL;
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        for (pServerIp = DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp;
             pServerIp != NULL; pServerIp = pServerIp->pNext)
        {
            if (pServerIp->IpAddr == u4IpAddress)
            {
                break;
            }
        }
    }
    return pServerIp;
}

/************************************************************************/
/*  Function Name   : DhcpIsRelayEnabled                                */
/*  Description     : Gets the DHCP Relay Status in the given context   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_ENABLE/DHCP_DISABLE                          */
/*  NOTE            : This API was developed before providing VRF       */
/*                    Support to DHCP Relay. To keep the backward       */
/*                    compatibility, this API is kept after providing   */
/*                    VRF support also; this will always return the     */
/*                    Relay enabled status on the default context; In   */
/*                    future, the new VRF specific API                  */
/*                    DhcpIsRelayEnabledInCxt should be used.           */
/************************************************************************/
BOOLEAN
DhcpIsRelayEnabled ()
{
    UINT4               u4ContextId = DHCP_RLY_DFLT_CXT_ID;
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        return (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying
                == DHCP_ENABLE) ? 1 : 0;
    }
    return 0;
}

/************************************************************************/
/*  Function Name   : DhcpIsRelayEnabledInCxt                           */
/*  Description     : Gets the DHCP Relay Status in the given context   */
/*  Input(s)        : u4ContextId - Context Id                          */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_ENABLE/DHCP_DISABLE                          */
/************************************************************************/
BOOLEAN
DhcpIsRelayEnabledInCxt (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] != NULL)
    {
        return (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying
                == DHCP_ENABLE) ? 1 : 0;
    }
    return 0;
}

/************************************************************************/
/*  Function Name   : DhcpIsRlyEnabledInAnyCxt                          */
/*  Description     : Verifies whether dhcp relay is enabled in any     */
/*                    context                                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_TRUE/DHCP_FALSE                              */
/************************************************************************/
BOOLEAN
DhcpIsRlyEnabledInAnyCxt ()
{
    UINT4               u4ActiveCxt = 0;
    UINT4               u4Iteration = 0;

    for (u4Iteration = 0; u4Iteration < MAX_DHRL_RLY_CXT; u4Iteration++)
    {
        if (DHCP_RLY_CXT_NODE[u4Iteration] != NULL)
        {
            u4ActiveCxt++;
            if (DHCP_RLY_CXT_NODE[u4Iteration]->u4CfRelaying == DHCP_ENABLE)
            {
                return DHCP_TRUE;
            }
        }
        if (u4ActiveCxt == gGlobalRlyNode.u4ActiveCxtCnt)
        {
            /* It is sufficient to loop gGlobalRlyNode.u4ActiveCxtCnt
             * times as there is only that many number of active contexts.
             * But the active contexts need not to be stored adjacently
             * in the array gGlobalRlyNode.apRelayCxtNode */
            break;
        }
    }
    return DHCP_FALSE;
}

/* -------------------------------------------------------------------+
 *  Function           : DhcpRProtocolLock
 * 
 *  Input(s)           : None.
 * 
 *  Output(s)          : None.
 * 
 *  Returns            : None.
 * 
 *  Action             : Takes a Protocol Sempaphore
 * +-------------------------------------------------------------------*/

INT4
DhcpRProtocolLock (VOID)
{
    if (OsixSemTake (DHCP_RLY_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------------+
 *  Function           : DhcpRProtocolUnlock
 * 
 *  Input(s)           : None.
 * 
 *  Output(s)          : None.
 * 
 *  Returns            : None.
 * 
 *  Action             : Releases a Protocol Sempaphore
 * +-------------------------------------------------------------------*/

INT4
DhcpRProtocolUnlock (VOID)
{

    if (OsixSemGive (DHCP_RLY_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : DhrlRBCompareCktId                                         */
/*                                                                           */
/* Description  : Compare the CktId entries in RBTree                        */
/*                                                                           */
/* Input        : e1        Pointer to Ckt Id Node node1                     */
/*                e2        Pointer to Ckt Id Node node2                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
DhrlRBCompareCktId (tRBElem * e1, tRBElem * e2)
{
    tDhrlIfConfigInfo  *pIfConfigEntry1 = e1;
    tDhrlIfConfigInfo  *pIfConfigEntry2 = e2;
    if (pIfConfigEntry1->u4CxtId > pIfConfigEntry2->u4CxtId)
    {
        return DHRL_RB_GREATER;
    }
    else if (pIfConfigEntry1->u4CxtId < pIfConfigEntry2->u4CxtId)
    {
        return DHRL_RB_LESS;
    }
    else
    {
        if (pIfConfigEntry1->u4CircuitId > pIfConfigEntry2->u4CircuitId)
        {
            return DHRL_RB_GREATER;
        }
        else if (pIfConfigEntry1->u4CircuitId < pIfConfigEntry2->u4CircuitId)
        {
            return DHRL_RB_LESS;
        }
    }
    return DHRL_RB_EQUAL;
}

/*****************************************************************************/
/* Function     : DhrlRBCompareIfIndex                                       */
/*                                                                           */
/* Description  : Compare the Interface indices in RBTree                    */
/*                                                                           */
/* Input        : e1        Pointer to If Index Node node1                   */
/*                e2        Pointer to If Index Node node2                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
DhrlRBCompareIfIndex (tRBElem * e1, tRBElem * e2)
{
    tDhrlIfConfigInfo  *pIfConfigEntry1 = e1;
    tDhrlIfConfigInfo  *pIfConfigEntry2 = e2;
    if (pIfConfigEntry1->u4CxtId > pIfConfigEntry2->u4CxtId)
    {
        return DHRL_RB_GREATER;
    }
    else if (pIfConfigEntry1->u4CxtId < pIfConfigEntry2->u4CxtId)
    {
        return DHRL_RB_LESS;
    }
    else
    {
        if (pIfConfigEntry1->u4Port > pIfConfigEntry2->u4Port)
        {
            return DHRL_RB_GREATER;
        }
        else if (pIfConfigEntry1->u4Port < pIfConfigEntry2->u4Port)
        {
            return DHRL_RB_LESS;
        }
    }
    return DHRL_RB_EQUAL;
}

/*****************************************************************************/
/* Function     : DhrlRBCompareRemoteId                                      */
/*                                                                           */
/* Description  : Compare the RemoteId entries in RBTree                     */
/*                                                                           */
/* Input        : e1        Pointer to Remote Id Node node1                  */
/*                e2        Pointer to Remote Id Node node2                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
DhrlRBCompareRemoteId (tRBElem * e1, tRBElem * e2)
{
    tDhrlIfConfigInfo  *pIfConfigEntry1 = e1;
    tDhrlIfConfigInfo  *pIfConfigEntry2 = e2;
    if (pIfConfigEntry1->u4CxtId > pIfConfigEntry2->u4CxtId)
    {
        return DHRL_RB_GREATER;
    }
    else if (pIfConfigEntry1->u4CxtId < pIfConfigEntry2->u4CxtId)
    {
        return DHRL_RB_LESS;
    }
    else
    {
        if (STRCMP (pIfConfigEntry1->au1RemoteId, pIfConfigEntry2->au1RemoteId)
            == 0)
        {
            return DHRL_RB_EQUAL;
        }
        if (STRCMP (pIfConfigEntry1->au1RemoteId, pIfConfigEntry2->au1RemoteId)
            > 0)
        {
            return DHRL_RB_GREATER;
        }
    }
    return DHRL_RB_LESS;
}
