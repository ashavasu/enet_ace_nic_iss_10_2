/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhrlapi.c,v 1.4 2015/06/17 04:49:02 siva Exp $
 *
 * Description:This file contains API routines that is provided by
 *             DHCP Relay  module.
 *****************************************************************************/

#ifndef _DHRLAPI_C_
#define _DHRLAPI_C_

#include "dhcpincs.h"
#include "dhcp.h"

/*****************************************************************************/
/* Function     : DhrlApiGetIfCiruitId                                       */
/*                                                                           */
/* Description  : This function gets the dhcp relay IfCiruitId based on given*/
/*                interface index                                            */
/*                                                                           */
/* Input        : i4IfIndex - Interface index                                */
/*                pu1DhcpRelayIfCircuitId - pointer to circuit id            */
/*                                                                           */
/* Output       : pu1DhcpRelayIfCircuitId - pointer to circuit id            */
/*                                                                           */
/* Returns      : DHCP_SUCCESS/DHCP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
DhrlApiGetIfCiruitId (INT4 i4IfIndex, UINT4 *pu4DhcpRelayIfCircuitId)
{
    INT4                i4RetVal = DHCP_SUCCESS;
    UINT4               u4Port = 0;
    UINT4               u4CxtId = 0;
    
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
            == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    if(NetIpv4GetCxtId (u4Port,&u4CxtId) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    /*Take the lock */
    DHCPR_PROTO_LOCK ();
    /* For this utility we need to pass i4IfIndex only; not u4Port.
     * Inside that utility the u4Port will be fetched from i4IfIndex 
     * */
    if (DhrlUtilGetIfCiruitId (u4CxtId, i4IfIndex, 
            pu4DhcpRelayIfCircuitId)!= DHCP_SUCCESS)
    {
        i4RetVal = DHCP_FAILURE;
    }
    /*Release the lock */
    DHCPR_PROTO_UNLOCK ();

    return i4RetVal;
}

#endif /* _DHRLAPI_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  <dhrlapi.c>                       */
/*--------------------------------------------------------------------------*/
