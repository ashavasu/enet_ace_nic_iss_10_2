/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmidhlw.c,v 1.3 2017/12/14 10:23:42 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "dhcpincs.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIDhcpConfigGblTraceLevel
 Input       :  The Indices

                The Object 
                retValFsMIDhcpConfigGblTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpConfigGblTraceLevel (INT4 *pi4RetValFsMIDhcpConfigGblTraceLevel)
{
    *pi4RetValFsMIDhcpConfigGblTraceLevel = DHCP_RLY_TRC_FLAG;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDhcpConfigGblTraceLevel
 Input       :  The Indices

                The Object 
                setValFsMIDhcpConfigGblTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpConfigGblTraceLevel (INT4 i4SetValFsMIDhcpConfigGblTraceLevel)
{
    DHCP_RLY_TRC_FLAG = i4SetValFsMIDhcpConfigGblTraceLevel;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpConfigGblTraceLevel
 Input       :  The Indices

                The Object 
                testValFsMIDhcpConfigGblTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpConfigGblTraceLevel (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsMIDhcpConfigGblTraceLevel)
{
    if ((i4TestValFsMIDhcpConfigGblTraceLevel < 0) ||
        (i4TestValFsMIDhcpConfigGblTraceLevel > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDhcpConfigGblTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDhcpConfigGblTraceLevel (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDhcpContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDhcpContextTable
 Input       :  The Indices
                FsMIDhcpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDhcpContextTable (INT4 i4FsMIDhcpContextId)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDhcpContextTable
 Input       :  The Indices
                FsMIDhcpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDhcpContextTable (INT4 *pi4FsMIDhcpContextId)
{

    if (DhcpRlyGetFirstActiveContextID ((UINT4 *) pi4FsMIDhcpContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDhcpContextTable
 Input       :  The Indices
                FsMIDhcpContextId
                nextFsMIDhcpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDhcpContextTable (INT4 i4FsMIDhcpContextId,
                                     INT4 *pi4NextFsMIDhcpContextId)
{

    if (DhcpRlyGetNextActiveContextID ((UINT4) i4FsMIDhcpContextId,
                                       (UINT4 *) pi4NextFsMIDhcpContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelaying
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelaying
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelaying (INT4 i4FsMIDhcpContextId,
                        INT4 *pi4RetValFsMIDhcpRelaying)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelaying = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRelaying;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayServersOnly
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayServersOnly
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayServersOnly (INT4 i4FsMIDhcpContextId,
                                INT4 *pi4RetValFsMIDhcpRelayServersOnly)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayServersOnly = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfdhcpRelayFwdServersOnly;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelaySecsThreshold
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelaySecsThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelaySecsThreshold (INT4 i4FsMIDhcpContextId,
                                  INT4 *pi4RetValFsMIDhcpRelaySecsThreshold)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelaySecsThreshold = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfSecsThresh;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayHopsThreshold
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayHopsThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayHopsThreshold (INT4 i4FsMIDhcpContextId,
                                  INT4 *pi4RetValFsMIDhcpRelayHopsThreshold)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayHopsThreshold = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfHopsThresh;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAIOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAIOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAIOptionControl (INT4 i4FsMIDhcpContextId,
                                     INT4
                                     *pi4RetValFsMIDhcpRelayRAIOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayRAIOptionControl = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAIOption;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAICircuitIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAICircuitIDSubOptionControl (INT4 i4FsMIDhcpContextId,
                                                 INT4
                                                 *pi4RetValFsMIDhcpRelayRAICircuitIDSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayRAICircuitIDSubOptionControl = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAICircuitIDSubOption;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionControl (INT4 i4FsMIDhcpContextId,
                                                INT4
                                                *pi4RetValFsMIDhcpRelayRAIRemoteIDSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayRAIRemoteIDSubOptionControl = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAIRemoteIDSubOption;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionControl (INT4 i4FsMIDhcpContextId,
                                                  INT4
                                                  *pi4RetValFsMIDhcpRelayRAISubnetMaskSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayRAISubnetMaskSubOptionControl = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAISubnetMaskSubOption;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAIVPNIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAIVPNIDSubOptionControl (INT4 i4FsMIDhcpContextId,
                                             INT4
                                             *pi4RetValFsMIDhcpRelayRAIVPNIDSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpRelayRAIVPNIDSubOptionControl = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAIVPNIDSubOption;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAIOptionInserted
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAIOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAIOptionInserted (INT4 i4FsMIDhcpContextId,
                                      UINT4
                                      *pu4RetValFsMIDhcpRelayRAIOptionInserted)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pu4RetValFsMIDhcpRelayRAIOptionInserted =
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4StatRAIOptionInserted;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAICircuitIDSubOptionInserted
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAICircuitIDSubOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAICircuitIDSubOptionInserted (INT4 i4FsMIDhcpContextId,
                                                  UINT4
                                                  *pu4RetValFsMIDhcpRelayRAICircuitIDSubOptionInserted)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pu4RetValFsMIDhcpRelayRAICircuitIDSubOptionInserted =
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAICircuitIDSubOptionInserted;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionInserted
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAIRemoteIDSubOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionInserted (INT4 i4FsMIDhcpContextId,
                                                 UINT4
                                                 *pu4RetValFsMIDhcpRelayRAIRemoteIDSubOptionInserted)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pu4RetValFsMIDhcpRelayRAIRemoteIDSubOptionInserted =
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAIRemoteIDSubOptionInserted;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionInserted
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAISubnetMaskSubOptionInserted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionInserted (INT4 i4FsMIDhcpContextId,
                                                   UINT4
                                                   *pu4RetValFsMIDhcpRelayRAISubnetMaskSubOptionInserted)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pu4RetValFsMIDhcpRelayRAISubnetMaskSubOptionInserted =
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAISubnetMaskSubOptionInserted;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAIOptionWronglySet
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAIOptionWronglySet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAIOptionWronglySet (INT4 i4FsMIDhcpContextId,
                                        UINT4
                                        *pu4RetValFsMIDhcpRelayRAIOptionWronglySet)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pu4RetValFsMIDhcpRelayRAIOptionWronglySet =
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAIOptionWronglyInserted;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayRAISpaceConstraint
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayRAISpaceConstraint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayRAISpaceConstraint (INT4 i4FsMIDhcpContextId,
                                       UINT4
                                       *pu4RetValFsMIDhcpRelayRAISpaceConstraint)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pu4RetValFsMIDhcpRelayRAISpaceConstraint =
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4StatRAISpaceConstraint;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpConfigTraceLevel
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpConfigTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpConfigTraceLevel (INT4 i4FsMIDhcpContextId,
                                INT4 *pi4RetValFsMIDhcpConfigTraceLevel)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        *pi4RetValFsMIDhcpConfigTraceLevel = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4DhcpCxtTrace;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpConfigDhcpCircuitOption
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpConfigDhcpCircuitOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpConfigDhcpCircuitOption (INT4 i4FsMIDhcpContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIDhcpConfigDhcpCircuitOption)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        DHCP_GET_CIRCUIT_ACTIONS_MASK (pRetValFsMIDhcpConfigDhcpCircuitOption,
                                       DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                                       u4CfRAICktIDDefaultOptiontype);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayCounterReset
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayCounterReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayCounterReset (INT4 i4FsMIDhcpContextId,
                                 INT4 *pi4RetValFsMIDhcpRelayCounterReset)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        *pi4RetValFsMIDhcpRelayCounterReset = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4DhcpCountResetCounters;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayContextRowStatus
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                retValFsMIDhcpRelayContextRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayContextRowStatus (INT4 i4FsMIDhcpContextId,
                                     INT4
                                     *pi4RetValFsMIDhcpRelayContextRowStatus)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        *pi4RetValFsMIDhcpRelayContextRowStatus = (INT4)
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u1Status;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelaying
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelaying
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelaying (INT4 i4FsMIDhcpContextId, INT4 i4SetValFsMIDhcpRelaying)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRelaying == (UINT4)
        i4SetValFsMIDhcpRelaying)
        return SNMP_SUCCESS;

    /* Start the Relay task if its DHCP_ENABLE */
    if (i4SetValFsMIDhcpRelaying == DHCP_ENABLE)
    {
        /* Check whether DHCP Server is running. If so, return failure */
#ifdef DHCP_SRV_WANTED
        if (DhcpIsSrvEnabled ())
        {
            return SNMP_FAILURE;
        }
#endif

        if (DhcpRelayEnable ((UINT4) i4FsMIDhcpContextId) == DHCP_FAILURE)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "DhcpRelayEnable failed\r\n");
            return SNMP_FAILURE;
        }

    }

    /* Shutdown the Relay if its DHCP_DISABLE */
    if (i4SetValFsMIDhcpRelaying == DHCP_DISABLE)
    {
        DhcpRelayShutdown ((UINT4) i4FsMIDhcpContextId);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayServersOnly
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayServersOnly
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayServersOnly (INT4 i4FsMIDhcpContextId,
                                INT4 i4SetValFsMIDhcpRelayServersOnly)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfdhcpRelayFwdServersOnly =
        (UINT4) i4SetValFsMIDhcpRelayServersOnly;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelaySecsThreshold
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelaySecsThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelaySecsThreshold (INT4 i4FsMIDhcpContextId,
                                  INT4 i4SetValFsMIDhcpRelaySecsThreshold)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfSecsThresh =
        (UINT4) i4SetValFsMIDhcpRelaySecsThreshold;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayHopsThreshold
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayHopsThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayHopsThreshold (INT4 i4FsMIDhcpContextId,
                                  INT4 i4SetValFsMIDhcpRelayHopsThreshold)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfHopsThresh =
        (UINT4) i4SetValFsMIDhcpRelayHopsThreshold;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayRAIOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayRAIOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayRAIOptionControl (INT4 i4FsMIDhcpContextId,
                                     INT4 i4SetValFsMIDhcpRelayRAIOptionControl)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAIOption =
        (UINT4) i4SetValFsMIDhcpRelayRAIOptionControl;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayRAICircuitIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayRAICircuitIDSubOptionControl (INT4 i4FsMIDhcpContextId,
                                                 INT4
                                                 i4SetValFsMIDhcpRelayRAICircuitIDSubOptionControl)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAICircuitIDSubOption =
        (UINT4) i4SetValFsMIDhcpRelayRAICircuitIDSubOptionControl;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayRAIRemoteIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayRAIRemoteIDSubOptionControl (INT4 i4FsMIDhcpContextId,
                                                INT4
                                                i4SetValFsMIDhcpRelayRAIRemoteIDSubOptionControl)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAIRemoteIDSubOption =
        (UINT4) i4SetValFsMIDhcpRelayRAIRemoteIDSubOptionControl;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayRAISubnetMaskSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayRAISubnetMaskSubOptionControl (INT4 i4FsMIDhcpContextId,
                                                  INT4
                                                  i4SetValFsMIDhcpRelayRAISubnetMaskSubOptionControl)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAISubnetMaskSubOption =
        (UINT4) i4SetValFsMIDhcpRelayRAISubnetMaskSubOptionControl;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayRAIVPNIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayRAIVPNIDSubOptionControl (INT4 i4FsMIDhcpContextId,
                                             INT4
                                             i4SetValFsMIDhcpRelayRAIVPNIDSubOptionControl)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAIVPNIDSubOption =
        (UINT4) i4SetValFsMIDhcpRelayRAIVPNIDSubOptionControl;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpConfigTraceLevel
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpConfigTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpConfigTraceLevel (INT4 i4FsMIDhcpContextId,
                                INT4 i4SetValFsMIDhcpConfigTraceLevel)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4DhcpCxtTrace =
        (UINT4) i4SetValFsMIDhcpConfigTraceLevel;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpConfigDhcpCircuitOption
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpConfigDhcpCircuitOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpConfigDhcpCircuitOption (INT4 i4FsMIDhcpContextId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsMIDhcpConfigDhcpCircuitOption)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    DHCP_SET_CIRCUIT_ACTIONS_MASK
        (&
         (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
          u4CfRAICktIDDefaultOptiontype),
         pSetValFsMIDhcpConfigDhcpCircuitOption);
    /* The circuit type value is always stored in the first byte of the
     *      * string. Possible values are router-index (1), vlanid (2) and lagport (3)
     *           */
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayCounterReset
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayCounterReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayCounterReset (INT4 i4FsMIDhcpContextId,
                                 INT4 i4SetValFsMIDhcpRelayCounterReset)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValFsMIDhcpRelayCounterReset == DHCP_RESET)
    {
        DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4StatRAIOptionInserted =
            DHCP_DISABLE_STATUS;
        DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAICircuitIDSubOptionInserted = DHCP_DISABLE_STATUS;
        DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAIRemoteIDSubOptionInserted = DHCP_DISABLE_STATUS;
        DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
            u4StatRAISubnetMaskSubOptionInserted = DHCP_DISABLE_STATUS;
        DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4StatRAIOptionWronglyInserted =
            DHCP_DISABLE_STATUS;
        DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4StatRAISpaceConstraint =
            DHCP_DISABLE_STATUS;

    }
    DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4DhcpCountResetCounters =
        (UINT4) i4SetValFsMIDhcpRelayCounterReset;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayContextRowStatus
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                setValFsMIDhcpRelayContextRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayContextRowStatus (INT4 i4FsMIDhcpContextId,
                                     INT4 i4SetValFsMIDhcpRelayContextRowStatus)
{
    switch (i4SetValFsMIDhcpRelayContextRowStatus)
    {
        case ACTIVE:
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u1Status = ACTIVE;
            break;
        case NOT_IN_SERVICE:
            DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u1Status = NOT_IN_SERVICE;
            break;
        case CREATE_AND_WAIT:
            if (i4FsMIDhcpContextId != DHCP_RLY_DFLT_CXT_ID)
            {
                /* For DEFAULT context alone, an entry would be created in the table 
                   fsMIDhcpContextTable, when the DHCP Relay task is initialized;
                   So no need to create a new row entry for enabling relay on default 
                   context. It is required to maintain the compatibility between 
                   dhcp relay SI & MI MIBs */
                if (DhcpRlyCreateContext ((UINT4) i4FsMIDhcpContextId) ==
                    DHCP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u1Status = NOT_READY;
            }
            break;
        case DESTROY:
            if (i4FsMIDhcpContextId != DHCP_RLY_DFLT_CXT_ID)
            {
                /* For DEFAULT context alone, we should not delete the entry from
                   the table fsMIDhcpContextTable. It will be deleted only when
                   the relay task is de-initialised. It is required to maintain
                   the backward compatibility to DHCP Relay SI MIB */
                DhcpRlyDeleteContext ((UINT4) i4FsMIDhcpContextId);
            }
            break;
        case NOT_READY:
        case CREATE_AND_GO:
            break;
        default:
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelaying
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelaying
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelaying (UINT4 *pu4ErrorCode, INT4 i4FsMIDhcpContextId,
                           INT4 i4TestValFsMIDhcpRelaying)
{
    if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId] != NULL)
    {

        if (i4TestValFsMIDhcpRelaying == DHCP_ENABLE ||
            i4TestValFsMIDhcpRelaying == DHCP_DISABLE)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayServersOnly
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayServersOnly
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayServersOnly (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDhcpContextId,
                                   INT4 i4TestValFsMIDhcpRelayServersOnly)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if ((i4TestValFsMIDhcpRelayServersOnly != DHCP_ENABLE) &&
            (i4TestValFsMIDhcpRelayServersOnly != DHCP_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((i4TestValFsMIDhcpRelayServersOnly == DHCP_ENABLE) &&
            (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pServersIp == NULL))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelaySecsThreshold
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelaySecsThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelaySecsThreshold (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIDhcpContextId,
                                     INT4 i4TestValFsMIDhcpRelaySecsThreshold)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if (i4TestValFsMIDhcpRelaySecsThreshold >= 0 &&
            i4TestValFsMIDhcpRelaySecsThreshold <= 0xffff)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayHopsThreshold
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayHopsThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayHopsThreshold (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIDhcpContextId,
                                     INT4 i4TestValFsMIDhcpRelayHopsThreshold)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if (i4TestValFsMIDhcpRelayHopsThreshold >= DHCP_MIN_HOPS &&
            i4TestValFsMIDhcpRelayHopsThreshold <= DHCP_MAX_HOPS)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayRAIOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayRAIOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayRAIOptionControl (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIDhcpContextId,
                                        INT4
                                        i4TestValFsMIDhcpRelayRAIOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if (i4TestValFsMIDhcpRelayRAIOptionControl == DHCP_ENABLE ||
            i4TestValFsMIDhcpRelayRAIOptionControl == DHCP_DISABLE)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayRAICircuitIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayRAICircuitIDSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayRAICircuitIDSubOptionControl (UINT4 *pu4ErrorCode,
                                                    INT4 i4FsMIDhcpContextId,
                                                    INT4
                                                    i4TestValFsMIDhcpRelayRAICircuitIDSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if (i4TestValFsMIDhcpRelayRAICircuitIDSubOptionControl == DHCP_ENABLE ||
            i4TestValFsMIDhcpRelayRAICircuitIDSubOptionControl == DHCP_DISABLE)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayRAIRemoteIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayRAIRemoteIDSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayRAIRemoteIDSubOptionControl (UINT4 *pu4ErrorCode,
                                                   INT4 i4FsMIDhcpContextId,
                                                   INT4
                                                   i4TestValFsMIDhcpRelayRAIRemoteIDSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if (i4TestValFsMIDhcpRelayRAIRemoteIDSubOptionControl == DHCP_ENABLE ||
            i4TestValFsMIDhcpRelayRAIRemoteIDSubOptionControl == DHCP_DISABLE)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayRAISubnetMaskSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayRAISubnetMaskSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayRAISubnetMaskSubOptionControl (UINT4 *pu4ErrorCode,
                                                     INT4 i4FsMIDhcpContextId,
                                                     INT4
                                                     i4TestValFsMIDhcpRelayRAISubnetMaskSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        if (i4TestValFsMIDhcpRelayRAISubnetMaskSubOptionControl == DHCP_ENABLE
            || i4TestValFsMIDhcpRelayRAISubnetMaskSubOptionControl ==
            DHCP_DISABLE)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayRAIVPNIDSubOptionControl
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayRAIVPNIDSubOptionControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayRAIVPNIDSubOptionControl (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIDhcpContextId,
                                                INT4
                                                i4TestValFsMIDhcpRelayRAIVPNIDSubOptionControl)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        if (i4TestValFsMIDhcpRelayRAIVPNIDSubOptionControl == DHCP_ENABLE ||
            i4TestValFsMIDhcpRelayRAIVPNIDSubOptionControl == DHCP_DISABLE)
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpConfigTraceLevel
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpConfigTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpConfigTraceLevel (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDhcpContextId,
                                   INT4 i4TestValFsMIDhcpConfigTraceLevel)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if ((i4TestValFsMIDhcpConfigTraceLevel == DHCPR_DEBUG_ALL) ||
            (i4TestValFsMIDhcpConfigTraceLevel == DHCPR_DEBUG_ERRORS) ||
            (i4TestValFsMIDhcpConfigTraceLevel == DHCPR_DEBUG_NONE) ||
            (i4TestValFsMIDhcpConfigTraceLevel == DHCPR_DEBUG_DEF_VALUE))
        {
            return (SNMP_SUCCESS);
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpConfigDhcpCircuitOption
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpConfigDhcpCircuitOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpConfigDhcpCircuitOption (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIDhcpContextId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsMIDhcpConfigDhcpCircuitOption)
{
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {
        if (pTestValFsMIDhcpConfigDhcpCircuitOption->i4_Length >
            DHCP_CIRCUIT_ACTION_FLAG_LENGTH ||
            pTestValFsMIDhcpConfigDhcpCircuitOption->i4_Length == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u4CfRAICircuitIDSubOption
            == DHCP_DISABLE)
        {
            CLI_SET_ERR (CLI_DHCPR_NO_ACCESS_CIRCUIT_ID);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayCounterReset
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayCounterReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayCounterReset (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIDhcpContextId,
                                    INT4 i4TestValFsMIDhcpRelayCounterReset)
{
    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_SUCCESS)
    {

        if ((i4TestValFsMIDhcpRelayCounterReset == DHCP_RESET) ||
            (i4TestValFsMIDhcpRelayCounterReset == DHCP_NO_RESET))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayContextRowStatus
 Input       :  The Indices
                FsMIDhcpContextId

                The Object 
                testValFsMIDhcpRelayContextRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayContextRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIDhcpContextId,
                                        INT4
                                        i4TestValFsMIDhcpRelayContextRowStatus)
{
    UINT1               u1Status = 0;

    switch (i4TestValFsMIDhcpRelayContextRowStatus)
    {
        case ACTIVE:
            if (DHCP_RLY_CXT_NODE[(UINT4) i4FsMIDhcpContextId] == NULL)
            {
                return SNMP_FAILURE;
            }
            else
            {
                u1Status = DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u1Status;
            }
            /* Active can be made active from NOT_READY or NOT_IN_SERVICE 
             *                  * state */
            if ((u1Status == NOT_IN_SERVICE) ||
                (u1Status == NOT_READY) || (u1Status == ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE can be made from ACTIVE state */
            if (DHCP_RLY_CXT_NODE[(UINT4) i4FsMIDhcpContextId] == NULL)
            {
                return SNMP_FAILURE;
            }
            else
            {
                u1Status = DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->u1Status;
            }
            if ((u1Status == ACTIVE) ||
                (u1Status == NOT_IN_SERVICE) || (u1Status == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            break;

        case CREATE_AND_WAIT:
            if ((i4FsMIDhcpContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
                || (i4FsMIDhcpContextId < 0))
            {
                return SNMP_FAILURE;
            }

            if (i4FsMIDhcpContextId == DHCP_RLY_DFLT_CXT_ID)
            {
                return SNMP_SUCCESS;
            }
            /* Check if the entry already exist */
            if (DHCP_RLY_CXT_NODE[(UINT4) i4FsMIDhcpContextId] != NULL)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
            break;

        case DESTROY:
            if ((i4FsMIDhcpContextId >= SYS_DEF_MAX_NUM_CONTEXTS)
                || (i4FsMIDhcpContextId < 0))
            {
                return SNMP_FAILURE;
            }
            if (i4FsMIDhcpContextId == DHCP_RLY_DFLT_CXT_ID)
            {
                return SNMP_SUCCESS;
            }
            if (DHCP_RLY_CXT_NODE[(UINT4) i4FsMIDhcpContextId] == NULL)
            {
                return SNMP_FAILURE;
            }
            return (SNMP_SUCCESS);

        case CREATE_AND_GO:
        case NOT_READY:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDhcpContextTable
 Input       :  The Indices
                FsMIDhcpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDhcpContextTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDhcpRelaySrvAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable
 Input       :  The Indices
                FsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable (INT4 i4FsMIDhcpContextId,
                                                      UINT4
                                                      u4FsMIDhcpRelaySrvIpAddress)
{
    tServer            *pServerIp = NULL;

    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    for (pServerIp = DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pServersIp;
         pServerIp != NULL; pServerIp = pServerIp->pNext)
    {
        if (pServerIp->IpAddr == u4FsMIDhcpRelaySrvIpAddress)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable
 Input       :  The Indices
                FsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable (INT4 *pi4FsMIDhcpContextId,
                                              UINT4
                                              *pu4FsMIDhcpRelaySrvIpAddress)
{
    if (DhcpRlyGetFirstActiveContextID ((UINT4 *) pi4FsMIDhcpContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (!(DHCP_RLY_CXT_NODE[*pi4FsMIDhcpContextId]->pServersIp))
    {
        return SNMP_FAILURE;
    }

    *pu4FsMIDhcpRelaySrvIpAddress =
        DHCP_RLY_CXT_NODE[*pi4FsMIDhcpContextId]->pServersIp->IpAddr;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDhcpRelaySrvAddressTable
 Input       :  The Indices
                FsMIDhcpContextId
                nextFsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress
                nextFsMIDhcpRelaySrvIpAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDhcpRelaySrvAddressTable (INT4 i4FsMIDhcpContextId,
                                             INT4 *pi4NextFsMIDhcpContextId,
                                             UINT4 u4FsMIDhcpRelaySrvIpAddress,
                                             UINT4
                                             *pu4NextFsMIDhcpRelaySrvIpAddress)
{

    tServer            *pServerIp = NULL;
    INT4                i4NextCntxid = i4FsMIDhcpContextId;

    if (nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable
        (i4FsMIDhcpContextId, u4FsMIDhcpRelaySrvIpAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    for (pServerIp = DHCP_RLY_CXT_NODE[i4NextCntxid]->pServersIp;
         pServerIp != NULL; pServerIp = pServerIp->pNext)
    {
        if (pServerIp->IpAddr > u4FsMIDhcpRelaySrvIpAddress)
        {
            *pu4NextFsMIDhcpRelaySrvIpAddress = pServerIp->IpAddr;
            *pi4NextFsMIDhcpContextId = i4NextCntxid;
            return SNMP_SUCCESS;
        }
    }
    while (DhcpRlyGetNextActiveContextID ((UINT4) i4FsMIDhcpContextId,
                                          (UINT4 *) &i4NextCntxid) ==
           SNMP_SUCCESS)
    {
        if (!(DHCP_RLY_CXT_NODE[i4NextCntxid]->pServersIp))
        {
            i4FsMIDhcpContextId = i4NextCntxid;
            continue;
        }
        else
        {
            *pu4NextFsMIDhcpRelaySrvIpAddress =
                DHCP_RLY_CXT_NODE[i4NextCntxid]->pServersIp->IpAddr;
            *pi4NextFsMIDhcpContextId = i4NextCntxid;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelaySrvAddressRowStatus
 Input       :  The Indices
                FsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress

                The Object 
                retValFsMIDhcpRelaySrvAddressRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelaySrvAddressRowStatus (INT4 i4FsMIDhcpContextId,
                                        UINT4 u4FsMIDhcpRelaySrvIpAddress,
                                        INT4
                                        *pi4RetValFsMIDhcpRelaySrvAddressRowStatus)
{

    tServer            *pServerIp = NULL;

    if (nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable
        (i4FsMIDhcpContextId, u4FsMIDhcpRelaySrvIpAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pServerIp = DhcpRelayGetServer (i4FsMIDhcpContextId,
                                    u4FsMIDhcpRelaySrvIpAddress);

    if (pServerIp == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIDhcpRelaySrvAddressRowStatus = pServerIp->u1Status;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelaySrvAddressRowStatus
 Input       :  The Indices
                FsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress

                The Object 
                setValFsMIDhcpRelaySrvAddressRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelaySrvAddressRowStatus (INT4 i4FsMIDhcpContextId,
                                        UINT4 u4FsMIDhcpRelaySrvIpAddress,
                                        INT4
                                        i4SetValFsMIDhcpRelaySrvAddressRowStatus)
{

    tServer            *pServerIp = NULL;

    switch (i4SetValFsMIDhcpRelaySrvAddressRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            pServerIp = DhcpRelayGetFreeServerRec (i4FsMIDhcpContextId);

            if (pServerIp == NULL)
            {
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "No more Servers can be configured\n");
                return SNMP_FAILURE;
            }

            pServerIp->IpAddr = u4FsMIDhcpRelaySrvIpAddress;

            if (i4SetValFsMIDhcpRelaySrvAddressRowStatus == CREATE_AND_WAIT)
            {

                pServerIp->u1Status = NOT_READY;
            }

            else
            {
                pServerIp->u1Status = ACTIVE;
            }

            /* add this entry to the existing records */

            DhcpRelayAddServer (i4FsMIDhcpContextId, pServerIp);

            break;

        case ACTIVE:

            pServerIp = DhcpRelayGetServer (i4FsMIDhcpContextId,
                                            u4FsMIDhcpRelaySrvIpAddress);

            if (pServerIp == NULL)
            {
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Server entry not yet Created\n");
                return SNMP_FAILURE;
            }
            pServerIp->u1Status = ACTIVE;

            break;
        case NOT_IN_SERVICE:

            pServerIp = DhcpRelayGetServer (i4FsMIDhcpContextId,
                                            u4FsMIDhcpRelaySrvIpAddress);

            if (pServerIp == NULL)
            {
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Server entry not yet Created\n");
                return SNMP_FAILURE;
            }

            pServerIp->u1Status = NOT_IN_SERVICE;

            break;
        case DESTROY:
            pServerIp = DhcpRelayGetServer (i4FsMIDhcpContextId,
                                            u4FsMIDhcpRelaySrvIpAddress);

            if (pServerIp == NULL)
            {
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "Server entry not yet Created\n");
                return SNMP_SUCCESS;
            }

            /* remove the entry from existing servers list and add it to free
               servers list */
            DhcpRelayRemoveServerRec (i4FsMIDhcpContextId, pServerIp);
            DhcpRelayAddToFreeServer (i4FsMIDhcpContextId, pServerIp);

            break;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelaySrvAddressRowStatus
 Input       :  The Indices
                FsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress

                The Object 
                testValFsMIDhcpRelaySrvAddressRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelaySrvAddressRowStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIDhcpContextId,
                                           UINT4 u4FsMIDhcpRelaySrvIpAddress,
                                           INT4
                                           i4TestValFsMIDhcpRelaySrvAddressRowStatus)
{
    INT1                i1Flag;

    if (NetIpv4IfIsOurAddressInCxt ((UINT4) i4FsMIDhcpContextId,
                                    u4FsMIDhcpRelaySrvIpAddress) ==
        NETIPV4_SUCCESS)
    {
        CLI_SET_ERR (CLI_DHCPR_INVALID_SERVER);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1Flag =
        nmhValidateIndexInstanceFsMIDhcpRelaySrvAddressTable
        (i4FsMIDhcpContextId, u4FsMIDhcpRelaySrvIpAddress);

    if (i1Flag == SNMP_SUCCESS)
    {
        if ((i4TestValFsMIDhcpRelaySrvAddressRowStatus == ACTIVE) ||
            (i4TestValFsMIDhcpRelaySrvAddressRowStatus == NOT_IN_SERVICE) ||
            (i4TestValFsMIDhcpRelaySrvAddressRowStatus == DESTROY))
        {
            if (DHCP_IS_VALID_IP (u4FsMIDhcpRelaySrvIpAddress) == 1)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        if ((i4TestValFsMIDhcpRelaySrvAddressRowStatus == CREATE_AND_WAIT) ||
            (i4TestValFsMIDhcpRelaySrvAddressRowStatus == CREATE_AND_GO))
        {

            /*check if DHCP Server Max Limit is reached */
            if (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pFreeServersIp == NULL)
            {
                CLI_SET_ERR (CLI_DHCPR_RCHD_MAX_SERVERS);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (DHCP_IS_VALID_IP (u4FsMIDhcpRelaySrvIpAddress) == 1)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_DHCPR_INVALID_SERVER);
                return SNMP_FAILURE;
            }

        }
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDhcpRelaySrvAddressTable
 Input       :  The Indices
                FsMIDhcpContextId
                FsMIDhcpRelaySrvIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDhcpRelaySrvAddressTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDhcpRelayIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDhcpRelayIfTable
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDhcpRelayIfTable (INT4 i4FsMIDhcpContextId,
                                              INT4 i4IfIndex)
{
    UINT4               u4Port;

    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDhcpRelayIfTable
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDhcpRelayIfTable (INT4 *pi4FsMIDhcpContextId,
                                      INT4 *pi4IfIndex)
{
    tDhrlIfConfigInfo  *pNextEntry = NULL;
    if (DhcpRlyGetFirstActiveContextID ((UINT4 *) pi4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pNextEntry = RBTreeGetFirst
         (DHCP_RLY_CXT_NODE[*pi4FsMIDhcpContextId]->pIfToIdRBRoot)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if (NetIpv4GetCfaIfIndexFromPort (pNextEntry->u4Port,
                                           (UINT4 *) (pi4IfIndex))
             == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDhcpRelayIfTable
 Input       :  The Indices
                FsMIDhcpContextId
                nextFsMIDhcpContextId
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDhcpRelayIfTable (INT4 i4FsMIDhcpContextId,
                                     INT4 *pi4NextFsMIDhcpContextId,
                                     INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pNextEntry = NULL;
    UINT4               u4Port;
    INT4                i4NxtIfIndex;
    INT4                i4ContextId;

    MEMSET (&DhrlIfConfigInfo, 0, sizeof (tDhrlIfConfigInfo));

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = i4FsMIDhcpContextId;
    if ((pNextEntry =
         RBTreeGetNext (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                        &DhrlIfConfigInfo, NULL)) != NULL)
    {
        if (NetIpv4GetCfaIfIndexFromPort (pNextEntry->u4Port,
                                          (UINT4 *) (&i4NxtIfIndex))
            == NETIPV4_FAILURE)
        {
            return SNMP_FAILURE;
        }

        *pi4NextIfIndex = i4NxtIfIndex;
        *pi4NextFsMIDhcpContextId = pNextEntry->u4CxtId;
        return SNMP_SUCCESS;
    }
    else
    {
        i4ContextId = i4FsMIDhcpContextId;
        while (DhcpRlyGetNextActiveContextID
               (i4ContextId,
                (UINT4 *) pi4NextFsMIDhcpContextId) != DHCP_FAILURE)
        {
            i4ContextId = *pi4NextFsMIDhcpContextId;
            if ((pNextEntry = RBTreeGetFirst
                 (DHCP_RLY_CXT_NODE[*pi4NextFsMIDhcpContextId]->
                  pIfToIdRBRoot)) == NULL)
            {
                continue;
            }
            else if (NetIpv4GetCfaIfIndexFromPort (pNextEntry->u4Port,
                                                   (UINT4 *) (pi4NextIfIndex))
                     == NETIPV4_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayIfCircuitId
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                retValFsMIDhcpRelayIfCircuitId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayIfCircuitId (INT4 i4FsMIDhcpContextId,
                                INT4 i4IfIndex,
                                UINT4 *pu4RetValFsMIDhcpRelayIfCircuitId)
{
    if (nmhValidateIndexInstanceFsMIDhcpRelayIfTable (i4FsMIDhcpContextId,
                                                      i4IfIndex) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (DhrlUtilGetIfCiruitId (i4FsMIDhcpContextId,
                               i4IfIndex, pu4RetValFsMIDhcpRelayIfCircuitId) !=
        DHCP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayIfRemoteId
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                retValFsMIDhcpRelayIfRemoteId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayIfRemoteId (INT4 i4FsMIDhcpContextId,
                               INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pRetValFsMIDhcpRelayIfRemoteId)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Get port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If remote id is not configured over this interface, return with
     *      * zero length string */
    if (!STRLEN (pDhrlIfConfigInfo->au1RemoteId))
    {
        pRetValFsMIDhcpRelayIfRemoteId->i4_Length = 0;
        return SNMP_SUCCESS;
    }

    STRNCPY (pRetValFsMIDhcpRelayIfRemoteId->pu1_OctetList,
             pDhrlIfConfigInfo->au1RemoteId,
             STRLEN (pDhrlIfConfigInfo->au1RemoteId));
    pRetValFsMIDhcpRelayIfRemoteId->
        pu1_OctetList[STRLEN (pDhrlIfConfigInfo->au1RemoteId)] = '\0';
    pRetValFsMIDhcpRelayIfRemoteId->i4_Length =
        STRLEN (pRetValFsMIDhcpRelayIfRemoteId->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDhcpRelayIfRowStatus
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                retValFsMIDhcpRelayIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDhcpRelayIfRowStatus (INT4 i4FsMIDhcpContextId,
                                INT4 i4IfIndex,
                                INT4 *pi4RetValFsMIDhcpRelayIfRowStatus)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    MEMSET (&DhrlIfConfigInfo, 0, sizeof (tDhrlIfConfigInfo));

    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Get port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIDhcpRelayIfRowStatus = pDhrlIfConfigInfo->u1RowStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayIfCircuitId
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                setValFsMIDhcpRelayIfCircuitId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayIfCircuitId (INT4 i4FsMIDhcpContextId,
                                INT4 i4IfIndex,
                                UINT4 u4SetValFsMIDhcpRelayIfCircuitId)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port = 0;

    MEMSET (&DhrlIfConfigInfo, 0, sizeof (tDhrlIfConfigInfo));

    /* Get the port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* If an entry does not exist, return failure */
    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Entry exist, Update the entry */
    if (pDhrlIfConfigInfo->u4CircuitId == u4SetValFsMIDhcpRelayIfCircuitId)
    {
        /* The same ckt id is set again.So nothing needs to be updated. */
        return SNMP_SUCCESS;
    }
    /* Entry is getting updated with a different circuit id.
     * So the key for the RBTree which holds ckt id to interface mapping is
     * changed. So remove the old entry and add the new entry */
    if (RBTreeRem
        (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pCktIdToIfRBRoot,
         pDhrlIfConfigInfo) == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "RBTreeRem Fails\n");
    }

    pDhrlIfConfigInfo->u4CircuitId = u4SetValFsMIDhcpRelayIfCircuitId;
    if (u4SetValFsMIDhcpRelayIfCircuitId == 0)
    {
#ifdef LLDP_WANTED
        LldpApiNotifyAgentCircuitId ((UINT4) i4IfIndex,
                                     u4SetValFsMIDhcpRelayIfCircuitId);
#endif
        /* Resetting the configuration. */
        return SNMP_SUCCESS;
    }

    if (RBTreeAdd (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pCktIdToIfRBRoot,
                   pDhrlIfConfigInfo) == RB_SUCCESS)
    {
        /* notify the agent circuit id change to LLDP */
#ifdef LLDP_WANTED
        LldpApiNotifyAgentCircuitId ((UINT4) i4IfIndex,
                                     u4SetValFsMIDhcpRelayIfCircuitId);
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayIfRemoteId
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                setValFsMIDhcpRelayIfRemoteId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayIfRemoteId (INT4 i4FsMIDhcpContextId,
                               INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIDhcpRelayIfRemoteId)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    /* Get the port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* If an entry  does not exist, return failure */
    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) == NULL)
    {
        return SNMP_FAILURE;
    }
    if (STRLEN (pDhrlIfConfigInfo->au1RemoteId) ==
        ((UINT4) pSetValFsMIDhcpRelayIfRemoteId->i4_Length))
    {
        if (!MEMCMP (pDhrlIfConfigInfo->au1RemoteId,
                     pSetValFsMIDhcpRelayIfRemoteId->pu1_OctetList,
                     pSetValFsMIDhcpRelayIfRemoteId->i4_Length))
        {
            /* The same remote id is set again.So nothing needs to be updated. */
            return SNMP_SUCCESS;
        }
    }
    /* Entry is getting updated with a different remote id.
     * So the key for the RBTree which holds rem id to interface mapping is
     * changed. So remove the old entry and add the new entry */
    if (RBTreeRem (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pRemoteIdToIfRBRoot,
                   pDhrlIfConfigInfo) == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "RBTreeRem Fails\n");
    }
    MEMSET (pDhrlIfConfigInfo->au1RemoteId, 0, DHRL_MAX_RID_LEN);
    MEMCPY (pDhrlIfConfigInfo->au1RemoteId,
            pSetValFsMIDhcpRelayIfRemoteId->pu1_OctetList,
            pSetValFsMIDhcpRelayIfRemoteId->i4_Length);
    pDhrlIfConfigInfo->au1RemoteId[pSetValFsMIDhcpRelayIfRemoteId->i4_Length] =
        '\0';
    if (pSetValFsMIDhcpRelayIfRemoteId->i4_Length == 0)
    {
        /* Resetting the value */
        return SNMP_SUCCESS;
    }

    if (RBTreeAdd (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pRemoteIdToIfRBRoot,
                   pDhrlIfConfigInfo) == RB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIDhcpRelayIfRowStatus
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                setValFsMIDhcpRelayIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDhcpRelayIfRowStatus (INT4 i4FsMIDhcpContextId,
                                INT4 i4IfIndex,
                                INT4 i4SetValFsMIDhcpRelayIfRowStatus)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    /* Get the port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMIDhcpRelayIfRowStatus)
    {
        case ACTIVE:
            DhrlIfConfigInfo.u4Port = u4Port;
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) == NULL)
                /* Check if entry exist */
            {
                return SNMP_FAILURE;
            }
            pDhrlIfConfigInfo->u1RowStatus = ACTIVE;
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:
            DhrlIfConfigInfo.u4Port = u4Port;
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) == NULL)
            {
                return SNMP_FAILURE;
            }
            pDhrlIfConfigInfo->u1RowStatus = NOT_IN_SERVICE;
            return SNMP_SUCCESS;

        case CREATE_AND_WAIT:
            DhrlIfConfigInfo.u4Port = u4Port;
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) != NULL)
            {
                /* Entry exist */
                UNUSED_PARAM (pDhrlIfConfigInfo);
                return SNMP_FAILURE;
            }

            /* Allocate for the structure */
            if (DHCP_ALLOC_IF_CONFIG_POOL (pDhrlIfConfigInfo) == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pDhrlIfConfigInfo, 0, sizeof (tDhrlIfConfigInfo));
            pDhrlIfConfigInfo->u4Port = u4Port;
            pDhrlIfConfigInfo->u4CxtId = (UINT4) i4FsMIDhcpContextId;
            pDhrlIfConfigInfo->u1RowStatus = NOT_READY;
            STRNCPY (pDhrlIfConfigInfo->au1RemoteId, "XYZ", STRLEN ("XYZ"));
            pDhrlIfConfigInfo->au1RemoteId[STRLEN ("XYZ")] = '\0';

            /* Add the structure to rbtree which holds interface to id
             * mapping. Do not add it to rbtree which holds ckt id to intterface
             * mapping nor remote id to interface mapping now. That will be
             * done once a valid ckt id or remote id is configured. */
            if (RBTreeAdd
                (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                 pDhrlIfConfigInfo) == RB_SUCCESS)
            {
                return SNMP_SUCCESS;
            }

            /* Failed to add to RBTree. So release the memory */
            DHCP_RELEASE_IF_CONFIG_POOL (pDhrlIfConfigInfo);
            return SNMP_FAILURE;

        case DESTROY:
            DhrlIfConfigInfo.u4Port = u4Port;
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) == NULL)
            {
                /* Such an entry does not exist */
                return SNMP_SUCCESS;
            }
            RBTreeRem (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                       pDhrlIfConfigInfo);
            RBTreeRem (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pCktIdToIfRBRoot,
                       pDhrlIfConfigInfo);
            RBTreeRem (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                       pRemoteIdToIfRBRoot, pDhrlIfConfigInfo);
            DHCP_RELEASE_IF_CONFIG_POOL (pDhrlIfConfigInfo);
            return SNMP_SUCCESS;
        default:
            return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayIfCircuitId
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                testValFsMIDhcpRelayIfCircuitId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayIfCircuitId (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDhcpContextId,
                                   INT4 i4IfIndex,
                                   UINT4 u4TestValFsMIDhcpRelayIfCircuitId)
{

    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsMIDhcpRelayIfTable (i4FsMIDhcpContextId,
                                                      i4IfIndex) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Test if the id lies in the interface index range or if the value is
     * zero*/
    if (!((u4TestValFsMIDhcpRelayIfCircuitId >= DHRL_MIN_L3_IF_INDEX) &&
          (u4TestValFsMIDhcpRelayIfCircuitId <= DHRL_MAX_L3_IF_INDEX)))
    {
        CLI_SET_ERR (CLI_DHCPR_INVALID_CKTID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Test if this id have already been mapped to any other interface */
    DhrlIfConfigInfo.u4CircuitId = u4TestValFsMIDhcpRelayIfCircuitId;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pCktIdToIfRBRoot,
                    &DhrlIfConfigInfo)) != NULL)
    {
        if (pDhrlIfConfigInfo->u4Port != u4Port)
        {
            /* The circuit id is already mapped to a different interface */
            CLI_SET_ERR (CLI_DHCPR_DUPLICATE_CKTID);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) != NULL)
    {
        /* Entry exist. To set value for an existing entry , row status
         * should be in NOT_READY or NOT_IN_SERVICE state */
        if ((pDhrlIfConfigInfo->u1RowStatus == NOT_READY) ||
            (pDhrlIfConfigInfo->u1RowStatus == NOT_IN_SERVICE))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayIfRemoteId
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                testValFsMIDhcpRelayIfRemoteId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayIfRemoteId (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIDhcpContextId,
                                  INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIDhcpRelayIfRemoteId)
{
    INT1                i1Count = 0;
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;
    UINT4               i4Cntr = 0;
    UINT1               au1SpecialChar[] = { '_', '<', '>' };

    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Get the port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Test if the id is same as the internal default id */
    if (!STRCMP (pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList, "XYZ"))
    {
        CLI_SET_ERR (CLI_DHCPR_INVALID_REMID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsMIDhcpRelayIfRemoteId->i4_Length >= DHRL_MAX_RID_LEN)
    {
        CLI_SET_ERR (CLI_DHCPR_WRONG_LENGTH);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;

    }
    /* Test if the id is any invalid string */

    for (i4Cntr = 0;
         i4Cntr < STRLEN (pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList);
         i4Cntr++)
    {
        if ((!ISALPHA
             (*(pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList + i4Cntr)))
            &&
            (!ISDIGIT
             (*(pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList + i4Cntr)))
            && (*(pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList + i4Cntr) !=
                '.'))
        {
            for (i1Count = 0; i1Count < MAX_DHCP_RELAY_SPECIAL_CAHRACTERS;
                 i1Count++)
            {
                if (*(pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList + i4Cntr)
                    == au1SpecialChar[i1Count])
                {
                    break;
                }
            }
            if (i1Count >= MAX_DHCP_RELAY_SPECIAL_CAHRACTERS)
            {

                CLI_SET_ERR (CLI_DHCPR_INVALID_REMID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    /* Test if this id have already been mapped to any other interface */
    STRNCPY (DhrlIfConfigInfo.au1RemoteId,
             pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList,
             STRLEN (pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList));
    DhrlIfConfigInfo.
        au1RemoteId[STRLEN (pTestValFsMIDhcpRelayIfRemoteId->pu1_OctetList)] =
        '\0';
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pRemoteIdToIfRBRoot,
                    &DhrlIfConfigInfo)) != NULL)
    {
        if (pDhrlIfConfigInfo->u4Port != u4Port)
        {
            /* The remote id is already mapped to a different interface */
            CLI_SET_ERR (CLI_DHCPR_DUPLICATE_REMID);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    DhrlIfConfigInfo.u4Port = u4Port;
    DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
    if ((pDhrlIfConfigInfo =
         RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->pIfToIdRBRoot,
                    &DhrlIfConfigInfo)) != NULL)
    {
        /* Entry exist. To set value for an existing entry , row status 
         * should be in NOT_READY or NOT_IN_SERVICE state */
        if ((pDhrlIfConfigInfo->u1RowStatus == NOT_READY) ||
            (pDhrlIfConfigInfo->u1RowStatus == NOT_IN_SERVICE))
        {
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDhcpRelayIfRowStatus
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex

                The Object 
                testValFsMIDhcpRelayIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDhcpRelayIfRowStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDhcpContextId,
                                   INT4 i4IfIndex,
                                   INT4 i4TestValFsMIDhcpRelayIfRowStatus)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    if (nmhValidateIndexInstanceFsMIDhcpContextTable (i4FsMIDhcpContextId)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Get the port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMIDhcpRelayIfRowStatus)
    {
        case ACTIVE:
            DhrlIfConfigInfo.u4Port = u4Port;
            /* Check if entry exist */
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) != NULL)
            {
                /* To make the row status active, either circuit id or remote id 
                 *                  * should be cofigured with a valid value.*/
                if ((pDhrlIfConfigInfo->u4CircuitId == 0) &&
                    (STRLEN (pDhrlIfConfigInfo->au1RemoteId) == 0))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                /* Active can be made active from NOT_READY or NOT_IN_SERVICE 
                 *                  * state */
                if ((pDhrlIfConfigInfo->u1RowStatus == NOT_IN_SERVICE) ||
                    (pDhrlIfConfigInfo->u1RowStatus == NOT_READY) ||
                    (pDhrlIfConfigInfo->u1RowStatus == ACTIVE))
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        case NOT_IN_SERVICE:
            DhrlIfConfigInfo.u4Port = u4Port;
            /* Check if entry exist */
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) != NULL)
            {
                /* NOT_IN_SERVICE can be made from ACTIVE state */
                if ((pDhrlIfConfigInfo->u1RowStatus == ACTIVE) ||
                    (pDhrlIfConfigInfo->u1RowStatus == NOT_IN_SERVICE))
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        case CREATE_AND_WAIT:
            DhrlIfConfigInfo.u4Port = u4Port;
            /* Check if the entry already exist */
            DhrlIfConfigInfo.u4CxtId = (UINT4) i4FsMIDhcpContextId;
            if ((pDhrlIfConfigInfo =
                 RBTreeGet (DHCP_RLY_CXT_NODE[i4FsMIDhcpContextId]->
                            pIfToIdRBRoot, &DhrlIfConfigInfo)) == NULL)
            {
                UNUSED_PARAM (pDhrlIfConfigInfo);
                return SNMP_SUCCESS;
            }
            /* An entry exist */
            break;

        case DESTROY:
            UNUSED_PARAM (pDhrlIfConfigInfo);
            return (SNMP_SUCCESS);

        case CREATE_AND_GO:
        case NOT_READY:
            UNUSED_PARAM (pDhrlIfConfigInfo);
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            UNUSED_PARAM (pDhrlIfConfigInfo);
            return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDhcpRelayIfTable
 Input       :  The Indices
                FsMIDhcpContextId
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDhcpRelayIfTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
