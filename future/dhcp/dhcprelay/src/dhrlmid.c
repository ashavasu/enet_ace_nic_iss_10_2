# include  "include.h"
# include  "dhcpRmid.h"
# include  "dhcpRlow.h"
# include  "dhcpRcon.h"
# include  "dhcpRogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsdhcprelaycli.h"

/****************************************************************************
 Function   : dhcpRelayGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpRelayGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
              UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_dhcpRelay_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

    UINT4               u4_addr_ret_val_dhcpRelayServerIpAddr;
   /*** $$TRACE_LOG (ENTRY,"dhcpRelayGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

    /* Intialisation of structure u8_counter_val with ZERO */
    MEMSET (&u8_counter_val, 0, sizeof (tSNMP_COUNTER64_TYPE));

    /*** DECLARATION_END ***/

    LEN_dhcpRelay_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_dhcpRelay_INDEX++;
    if (u1_search_type == EXACT)
    {
        if ((LEN_dhcpRelay_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_dhcpRelay_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case DHCPRELAYING:
        {
            i1_ret_val = nmhGetDhcpRelaying (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYSERVERSONLY:
        {
            i1_ret_val = nmhGetDhcpRelayServersOnly (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYSERVERIPADDR:
        {
            i1_ret_val =
                nmhGetDhcpRelayServerIpAddr
                (&u4_addr_ret_val_dhcpRelayServerIpAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_ret_val_dhcpRelayServerIpAddr);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYSECSTHRESHOLD:
        {
            i1_ret_val = nmhGetDhcpRelaySecsThreshold (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYHOPSTHRESHOLD:
        {
            i1_ret_val = nmhGetDhcpRelayHopsThreshold (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAIOPTIONCONTROL:
        {
            i1_ret_val = nmhGetDhcpRelayRAIOptionControl (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAICIRCUITIDSUBOPTIONCONTROL:
        {
            i1_ret_val =
                nmhGetDhcpRelayRAICircuitIDSubOptionControl (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAIREMOTEIDSUBOPTIONCONTROL:
        {
            i1_ret_val =
                nmhGetDhcpRelayRAIRemoteIDSubOptionControl (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAISUBNETMASKSUBOPTIONCONTROL:
        {
            i1_ret_val =
                nmhGetDhcpRelayRAISubnetMaskSubOptionControl (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAIOPTIONINSERTED:
        {
            i1_ret_val = nmhGetDhcpRelayRAIOptionInserted (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAICIRCUITIDSUBOPTIONINSERTED:
        {
            i1_ret_val =
                nmhGetDhcpRelayRAICircuitIDSubOptionInserted (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAIREMOTEIDSUBOPTIONINSERTED:
        {
            i1_ret_val =
                nmhGetDhcpRelayRAIRemoteIDSubOptionInserted (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAISUBNETMASKSUBOPTIONINSERTED:
        {
            i1_ret_val =
                nmhGetDhcpRelayRAISubnetMaskSubOptionInserted (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAIOPTIONWRONGLYSET:
        {
            i1_ret_val = nmhGetDhcpRelayRAIOptionWronglySet (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPRELAYRAISPACECONSTRAINT:
        {
            i1_ret_val = nmhGetDhcpRelayRAISpaceConstraint (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCONFIGTRACELEVEL:
        {
            i1_ret_val = nmhGetDhcpConfigTraceLevel (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpRelaySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpRelaySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
              UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpRelayServerIpAddr;
   /*** $$TRACE_LOG (ENTRY,"dhcpRelaySet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPRELAYING:
        {
            i1_ret_val = nmhSetDhcpRelaying (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYSERVERSONLY:
        {
            i1_ret_val = nmhSetDhcpRelayServersOnly (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYSERVERIPADDR:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpRelayServerIpAddr =
                CRU_BMC_DWFROMPDU (u1_octet_string);
            i1_ret_val =
                nmhSetDhcpRelayServerIpAddr (u4_addr_val_dhcpRelayServerIpAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYSECSTHRESHOLD:
        {
            i1_ret_val = nmhSetDhcpRelaySecsThreshold (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYHOPSTHRESHOLD:
        {
            i1_ret_val = nmhSetDhcpRelayHopsThreshold (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYRAIOPTIONCONTROL:
        {
            i1_ret_val =
                nmhSetDhcpRelayRAIOptionControl (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYRAICIRCUITIDSUBOPTIONCONTROL:
        {
            i1_ret_val =
                nmhSetDhcpRelayRAICircuitIDSubOptionControl (p_value->
                                                             i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYRAIREMOTEIDSUBOPTIONCONTROL:
        {
            i1_ret_val =
                nmhSetDhcpRelayRAIRemoteIDSubOptionControl (p_value->
                                                            i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPRELAYRAISUBNETMASKSUBOPTIONCONTROL:
        {
            i1_ret_val =
                nmhSetDhcpRelayRAISubnetMaskSubOptionControl (p_value->
                                                              i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPCONFIGTRACELEVEL:
        {
            i1_ret_val = nmhSetDhcpConfigTraceLevel (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPRELAYRAIOPTIONINSERTED:
            /*  Read Only Variables. */
        case DHCPRELAYRAICIRCUITIDSUBOPTIONINSERTED:
            /*  Read Only Variables. */
        case DHCPRELAYRAIREMOTEIDSUBOPTIONINSERTED:
            /*  Read Only Variables. */
        case DHCPRELAYRAISUBNETMASKSUBOPTIONINSERTED:
            /*  Read Only Variables. */
        case DHCPRELAYRAIOPTIONWRONGLYSET:
            /*  Read Only Variables. */
        case DHCPRELAYRAISPACECONSTRAINT:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpRelayTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpRelayTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
               UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpRelayServerIpAddr;
   /*** $$TRACE_LOG (ENTRY,"dhcpRelayTest Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case DHCPRELAYING:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelaying (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYSERVERSONLY:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelayServersOnly (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYSERVERIPADDR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpRelayServerIpAddr =
                CRU_BMC_DWFROMPDU (u1_octet_string);

            i1_ret_val =
                nmhTestv2DhcpRelayServerIpAddr (&u4ErrorCode,
                                                u4_addr_val_dhcpRelayServerIpAddr);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYSECSTHRESHOLD:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelaySecsThreshold (&u4ErrorCode,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYHOPSTHRESHOLD:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelayHopsThreshold (&u4ErrorCode,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYRAIOPTIONCONTROL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelayRAIOptionControl (&u4ErrorCode,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYRAICIRCUITIDSUBOPTIONCONTROL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelayRAICircuitIDSubOptionControl (&u4ErrorCode,
                                                                p_value->
                                                                i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYRAIREMOTEIDSUBOPTIONCONTROL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelayRAIRemoteIDSubOptionControl (&u4ErrorCode,
                                                               p_value->
                                                               i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPRELAYRAISUBNETMASKSUBOPTIONCONTROL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpRelayRAISubnetMaskSubOptionControl (&u4ErrorCode,
                                                                 p_value->
                                                                 i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPCONFIGTRACELEVEL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpConfigTraceLevel (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPRELAYRAIOPTIONINSERTED:
        case DHCPRELAYRAICIRCUITIDSUBOPTIONINSERTED:
        case DHCPRELAYRAIREMOTEIDSUBOPTIONINSERTED:
        case DHCPRELAYRAISUBNETMASKSUBOPTIONINSERTED:
        case DHCPRELAYRAIOPTIONWRONGLYSET:
        case DHCPRELAYRAISPACECONSTRAINT:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
