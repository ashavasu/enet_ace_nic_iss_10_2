/*$Id: dhrlmain.c,v 1.23 2017/12/14 10:23:42 siva Exp $*/
#define INCLUDE_DHCP_RELAY_GLOB_VARS
#include "dhcpincs.h"
#include "dhcp.h"

static INT1         gai1RecvBuff[DHCP_MAX_MTU];

/************************************************************************/
/*  Function Name   : DhcpRelayInitialize                               */
/*  Description     : The function Sets the fwd Enabling to a Specified */
/*                    Dhcp Server  , Creates the Task                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_FAILURE/DHCP_SUCCESS                         */
/************************************************************************/

INT4
DhcpRelayInitialize (void)
{
    UINT4               u4ContextId = DHCP_RLY_DFLT_CXT_ID;

    /* Set the current trace level for Dhcp Relay */
    DHCP_RLY_TRC_FLAG = DHCP_RLY_DEFAULT_TRACE_LEVEL;

#ifdef IP_WANTED
    /* For FSIP initializing the socket descriptor */
    gRlyNode.i4DhcpRelaySockDesc = (INT4) DHCP_RLY_INV_SOCKET_ID;
#endif
    /* Initializing the number of Active Contexts Counter */
    gGlobalRlyNode.u4ActiveCxtCnt = 0;

    if (DhcpRlyCreatePools () == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayInitialize : Failed to create Memory Pools!!\n");
        return (DHCP_FAILURE);
    }

    /* initializes Semaphore */
    if (OsixCreateSem (DHCPR_PROT_MUTEX_SEMA4, DHCPR_SEM_CREATE_INIT_CNT,
                       DHCPR_SEM_FLAGS, &DHCP_RLY_SEM_ID) != OSIX_SUCCESS)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayInitialize : Failed to Initialize Seamaphore!!\n");
        return (DHCP_FAILURE);
    }

    /* Create Queue to receive the notifications from IP */
    if (OsixQueCrt (IP_DHRL_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    DHRL_QUEUE_DEPTH, &DHCP_RLY_IP_QUE_ID) != OSIX_SUCCESS)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Failed to create IP DHRL queue!!\n");
        return (DHCP_FAILURE);
    }

    /* Create Queue to process the received packet */
    if (OsixQueCrt (DHRL_PKT_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    DHRL_QUEUE_DEPTH, &DHCP_RLY_QUE_ID) != OSIX_SUCCESS)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Failed to create PKT DHRL queue!!\n");
        return (DHCP_FAILURE);
    }

    DhcpRlyRegisterWithVcm ();

    /* Create the context table entry for the default VRF & allocate memory
     * for dynamic data strutrues also. */
    if (DhcpRlyCreateContext (DHCP_RLY_DFLT_CXT_ID) == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayInitialize:Context Node Allocation "
                 "Failed for Default Cxt!!\r\n");
        return (DHCP_FAILURE);
    }

    /* Create the context table entry for all the active non-default VRF & 
     * allocate memory for dynamic data strutrues also. */
    while (VcmGetNextActiveL3Context (u4ContextId, &u4ContextId) == VCM_SUCCESS)
    {
        if (DhcpRlyCreateContext (u4ContextId) == DHCP_FAILURE)
        {
            MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                          "DhcpRelayInitialize:Context Node Allocation "
                          "Failed for Context Id %u!!\r\n", u4ContextId);
        }
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhrlDeInit                                        */
/*  Description     : This function reinitialises the gobal             */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhrlDeInit (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Context = DHCP_RLY_DFLT_CXT_ID;

    do
    {
        if (NULL != DHCP_RLY_CXT_NODE[u4Context])
        {
            DhcpRlyDeleteContext (u4Context);
        }
    }
    while (VcmGetNextActiveL3Context (u4Context, &u4Context) != VCM_FAILURE);

    if (DHCP_RLY_SEM_ID != 0)
    {
        OsixSemDel (DHCP_RLY_SEM_ID);
    }
    DHCP_RLY_SEM_ID = 0;

    DhrlSizingMemDeleteMemPools ();
    DHCP_RLY_SERVER_POOL_ID = 0;
    DHCP_RLY_IF_POOL_ID = 0;
    DHCP_RLY_PKT_INFO_POOL_ID = 0;
    DHCP_RLY_CLNT_INFO_POOL_ID = 0;
    DHCP_RLY_CXT_POOL_ID = 0;

    if (DHCP_RLY_IP_QUE_ID != 0)
    {
        while (OsixQueRecv (DHCP_RLY_IP_QUE_ID, (UINT1 *) &pBuf,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            if (pBuf != NULL)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            }
        }
    }

    pBuf = NULL;
    if (DHCP_RLY_QUE_ID != 0)
    {
        while (OsixQueRecv (DHCP_RLY_QUE_ID, (UINT1 *) &pBuf,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            if (pBuf != NULL)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            }
        }
    }

    VcmDeRegisterHLProtocol (DHCP_RLY_PROTOCOL_ID);
    return;
}

/**************************************************************************/
/*   Function Name   : DhrlRegisterWithIp                                 */
/*   Description     : This function register DHCP Relay  with IP Module  */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT1
DhrlRegisterWithIp (UINT4 u4ContextId)
{
    tNetIpRegInfo       NetRegInfo;

    /* Register with IP */
    MEMSET ((UINT1 *) &NetRegInfo, 0, sizeof (tNetIpRegInfo));
    NetRegInfo.pIfStChng = (VOID *) DhrlIfStateChgHdlr;
    NetRegInfo.u2InfoMask = NETIPV4_IFCHG_REQ;
    NetRegInfo.u1ProtoId = DHRL_ID;
    NetRegInfo.pRtChng = NULL;
    NetRegInfo.pProtoPktRecv = NULL;
    NetRegInfo.u4ContextId = u4ContextId;
    if (NetIpv4RegisterHigherLayerProtocol (&NetRegInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : DhrlDeRegisterWithIp                               */
/*   Description     : This function de-registers DHCP Relay  with IP     */
/*                     Module                                             */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT1
DhrlDeRegisterWithIp (UINT4 u4ContextId)
{
    if (NetIpv4DeRegisterHigherLayerProtocolInCxt (u4ContextId, DHRL_ID)
        == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/*******************************************************************************
 * Function Name   : DhrlIfStateChgHdlr
 * Description     : Function to receive the interface deletion indication from ip
 * Global Varibles :
 * Inputs          : u1PortNo
 *                   u1Bitmap
                    IFACE_DELETED       0x00000020
 * Output          : None.
 * Returns         : None
 ******************************************************************************/
VOID
DhrlIfStateChgHdlr (tNetIpv4IfInfo * pIpIfInfo, UINT1 u1Bitmap)
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf;
    tDhcpRIpMsg         IpMsg;

    MEMSET (&IpMsg, 0, sizeof (tDhcpRIpMsg));
    if (u1Bitmap & IFACE_DELETED)
    {
        pChainBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tDhcpRIpMsg), 0);

        if (pChainBuf == NULL)
        {
            return;
        }
        MEMSET (pChainBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pChainBuf, "DhStChgHdlr");

        IpMsg.u4IfIndex = pIpIfInfo->u4IfIndex;
        IpMsg.u4CxtId = pIpIfInfo->u4ContextId;
        IpMsg.u4Cmd = DHRL_IFACE_DEL_PKT;

        CRU_BUF_Copy_OverBufChain (pChainBuf, (UINT1 *) &IpMsg,
                                   0, sizeof (tDhcpRIpMsg));

        if (OsixQueSend (DHCP_RLY_IP_QUE_ID, (UINT1 *) &pChainBuf,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "Failed to send Interface delete event !!\n");
            CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);
            return;
        }

        OsixEvtSend (DHCP_RLY_TSK_ID, DHRL_IF_DELETE_EVENT);
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpRelayEnable                                   */
/*  Description     : The function enable dhcp relay and create the     */
/*                    socket for receiving dhcp packets                 */
/*                    Dhcp Relay Server,                                */
/*  Input(s)        : u4ContextId                                       */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_FAILURE/DHCP_SUCCESS                         */
/************************************************************************/

INT1
DhcpRelayEnable (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayEnable: Invalid Context - Relay Cannot be enabled\r\n");
        return DHCP_FAILURE;
    }
#ifdef IP_WANTED
    /* For FSIP need to open the socket only once */
    if (gRlyNode.i4DhcpRelaySockDesc == DHCP_RLY_INV_SOCKET_ID)
    {
        /* Socket for FSIP not opened yet */
        if (DhcpRelayOpenSocket (u4ContextId) == DHCP_FAILURE)
        {
            return DHCP_FAILURE;
        }
    }
#endif
#ifdef LNXIP4_WANTED
    /* For Linux Ip need to open the socket for each context */
    if (DhcpRelayOpenSocket (u4ContextId) == DHCP_FAILURE)
    {
        return DHCP_FAILURE;
    }
#endif

    /* enable h/w for receiving DHCP relay packets */
#ifdef L3_SWITCHING_WANTED
    if (DhrlFsNpDhcpRlyInit (u4ContextId) != FNP_SUCCESS)
    {
#ifdef LNXIP4_WANTED
        if (DhcpRlyCloseSocket (DHCP_RLY_CXT_NODE[u4ContextId]->
                                i4DhcpRelaySockDesc) == DHCP_FAILURE)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "DhcpRelayEnable: Failed to close the socket" " \r\n");
        }
#endif

#ifdef IP_WANTED
        /* Need to close the socket for FSIP, only when the relay is
           disabled in all the contexts */
        if (DhcpIsRlyEnabledInAnyCxt () == DHCP_FALSE)
        {
            /* Relay is disabled in all the contexts; so close the
               socket */
            if (DhcpRlyCloseSocket (gRlyNode.i4DhcpRelaySockDesc)
                == DHCP_FAILURE)
            {
                MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                         "DhcpRelayEnable: Failed to close the socket" " \r\n");
            }
        }
#endif
        return DHCP_FAILURE;
    }
#endif

    /* Set the Relay System Control status as DHCP_ENABLE */
    DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying = (UINT4) DHCP_ENABLE;

    DHCP_RLY_CXT_NODE[u4ContextId]->u1Status = (UINT1) ACTIVE;
    return DHCP_SUCCESS;
}

/******************************************************************************
 * Function Name   : DhcpRlPktInSocket
 * Description     : Call back function from SelAddFd(), when dhcprl packet is
 *                   received
 * Global Varibles : DHCP_TASK_ID
 * Inputs          : None
 * Output          : Sends an event to DHCPRL task
 * Returns         : None.
 ******************************************************************************/
VOID
DhcpRlPktInSocket (INT4 i4SockFd)
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf = NULL;
    tDhcpRPktMsg        PktMsg;

    MEMSET (&PktMsg, 0, sizeof (tDhcpRPktMsg));

    if (i4SockFd >= 0)
    {
        pChainBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tDhcpRPktMsg), 0);

        if (pChainBuf == NULL)
        {
            return;
        }
        MEMSET (pChainBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pChainBuf, "DhcpRlPSk");
        PktMsg.i4SockDesc = i4SockFd;

        CRU_BUF_Copy_OverBufChain (pChainBuf, (UINT1 *) &PktMsg,
                                   0, sizeof (tDhcpRPktMsg));

        if (OsixQueSend (DHCP_RLY_QUE_ID, (UINT1 *) &pChainBuf,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "DhcpRlPktInSocket: Failed to send Packet Reception "
                     "Information !!\n");
            CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);
            return;
        }

        OsixEvtSend (DHCP_RLY_TSK_ID, DHRL_PKT_RECV_EVENT);
    }

    return;

}

/******************************************************************************
 * Function Name   : DhcpRelayOpenSocket
 * Description     : Opens a socket for sending and receiving DHCP Relay packets
 * Inputs          : u4ContextId
 * Output          : None
 * Returns         : DHCP_SUCCESS/
 *                   DHCP_FAILURE
 ******************************************************************************/
INT4
DhcpRelayOpenSocket (UINT4 u4ContextId)
{
    INT4                i4RetVal;
    INT4                i4SockDesc = -1;
    INT4                i4OpnVal = DHCP_ENABLE_STATUS;
    UINT1               u1OpnVal = DHCP_ENABLE_STATUS;
    struct sockaddr_in  DhcpRelayAddr;
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo    LnxVrfEventInfo;

    MEMSET (&LnxVrfEventInfo, 0, sizeof (tLnxVrfEventInfo));
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET;
    LnxVrfEventInfo.i4SockType = SOCK_DGRAM;
    LnxVrfEventInfo.i4SockProto = IPPROTO_UDP;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock ();
    LnxVrfEventHandling (&LnxVrfEventInfo, &i4SockDesc);
    LnxVrfSockUnLock ();
#else
    i4SockDesc = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
#endif

    MEMSET (&DhcpRelayAddr, 0, sizeof (struct sockaddr_in));
    if (i4SockDesc < 0)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "opening of DHCP RELAY socket failed !!!\n");
        return DHCP_FAILURE;
    }

    /* Bind a address and port to the socket */
    DhcpRelayAddr.sin_family = AF_INET;
    DhcpRelayAddr.sin_addr.s_addr = 0;    /*INADDR_ANY */
    DhcpRelayAddr.sin_port = OSIX_HTONS (DHCP_SERVER_PORT);

    i4RetVal = bind (i4SockDesc,
                     (struct sockaddr *) &DhcpRelayAddr,
                     sizeof (DhcpRelayAddr));
    if (i4RetVal < 0)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Binding of DHCP RELAY socket failed !!!\n");
        close (i4SockDesc);
        return DHCP_FAILURE;
    }

    if (setsockopt (i4SockDesc, SOL_SOCKET,
                    SO_BROADCAST, &i4OpnVal, sizeof (i4OpnVal)) < 0)
    {
        close (i4SockDesc);
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "setsockopt failed for SO_BROADCAST!!!\n");
        return DHCP_FAILURE;
    }
    if (setsockopt (i4SockDesc, IPPROTO_IP,
                    IP_PKTINFO, &u1OpnVal, sizeof (u1OpnVal)) < 0)
    {
        close (i4SockDesc);
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "setsockopt failed for IP_PKTINFO!!!\n");
        return DHCP_FAILURE;
    }

    /*Add the socket discriptor to the SelAddFd utility. */
    if (SelAddFd (i4SockDesc, DhcpRlPktInSocket) != OSIX_SUCCESS)
    {
        close (i4SockDesc);
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "SelAddFd for DHCP RELAY socket failed !!!\n");
        return DHCP_FAILURE;
    }
#ifdef IP_WANTED
    /* For FSIP */
    gRlyNode.i4DhcpRelaySockDesc = i4SockDesc;
#endif

#ifdef LNXIP4_WANTED
    /* For Linux IP */
    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        SelRemoveFd (i4SockDesc);
        close (i4SockDesc);
        return DHCP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[u4ContextId]->i4DhcpRelaySockDesc = i4SockDesc;
#endif
    UNUSED_PARAM (u4ContextId);
    return DHCP_SUCCESS;
}

/******************************************************************************
 * Function Name   : DhcpRlyCloseSocket
 * Description     : Closes a socket 
 * Inputs          : i4SockDesc - Socket Descriptor that needs to be closed 
 * Output          : None
 * Returns         : DHCP_SUCCESS/
 *                   DHCP_FAILURE
 ******************************************************************************/
INT4
DhcpRlyCloseSocket (INT4 i4SockDesc)
{
    if (i4SockDesc < 0)
    {
        return DHCP_FAILURE;
    }

    /* De-register the call back function of the Socket descriptor */
    SelRemoveFd (i4SockDesc);

    if (i4SockDesc != -1)
    {
        if (close (i4SockDesc) != OSIX_SUCCESS)
        {
            return DHCP_FAILURE;
        }
    }

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpRelayMain                                      */
/*  Description     : Actual Excution Starts from this futntion,         */
/*                    We are creating a SLI socket Timer for selecting,  */
/*                    upon receiving packet Event                        */
/*                    Posted to the Task which is already waiting.       */
/*                    Then following does the  Packet processing         */
/*  Input(s)        : None                               */
/*  Output(s)       : None.                                              */
/*  Returns         : NONE                                              */
/************************************************************************/
VOID
DhcpRelayMain (INT1 *pi1Param)
{

    UINT4               u4Events = 0;
    UNUSED_PARAM (pi1Param);

    MEMSET (&gGlobalRlyNode, 0, sizeof (tGlobalRlyNode));
    MEMSET (&gRlyNode, 0, sizeof (tRlyNode));

    if (DhcpRlySetContextId (DHCP_RLY_INVALID_CXT_ID) == DHCP_FAILURE)
    {
        return;
    }

    /* Initialize the DHCP Relay Task */
    if (DhcpRelayInitialize () == DHCP_SUCCESS)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRelayMain: Dhcp Relay Task Initialization Succeeded\r\n");
        if (OsixTskIdSelf (&DHCP_RLY_TSK_ID) == OSIX_FAILURE)
        {
            DhrlDeInit ();
            DHCP_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }
        DHCP_INIT_COMPLETE (OSIX_SUCCESS);
#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
        RegisterDHRL ();
        RegisterFSMIDHRly ();
#endif
    }
    else
    {
        DhrlDeInit ();
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    while (1)
    {
        OsixEvtRecv (DHCP_RLY_TSK_ID,
                     (DHRL_PKT_RECV_EVENT | DHRL_IF_DELETE_EVENT |
                      DHRL_VCM_EVENT), (OSIX_WAIT | OSIX_EV_ANY), &u4Events);

        if (u4Events & DHRL_PKT_RECV_EVENT)
        {
            DhcpRlyDequePkt ();
        }

        if (u4Events & DHRL_IF_DELETE_EVENT)
        {
            DhrlDequeIpPkt ();
        }
        if (u4Events & DHRL_VCM_EVENT)
        {
            DhcpRlyDequeVcmPkt ();
        }
    }
}

/*******************************************************************************
 * Function Name   : DhcpRlyDequePkt.
 * Description     : Function to Dequeue a packet from DHCP Packet reception Que
 * Inputs          : None.
 * Output          : None.
 * Returns         : DHCP_SUCCESS
 *                   DHCP_FAILURE
 ******************************************************************************/
VOID
DhcpRlyDequePkt (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tDhcpRPktMsg        PktMsg;
    INT4                i4SockDesc;

    while (OsixQueRecv (DHCP_RLY_QUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PktMsg,
                                   0, sizeof (tDhcpRPktMsg));
        i4SockDesc = PktMsg.i4SockDesc;
        DhrlPacketRcvd (i4SockDesc);
        if (SelAddFd (i4SockDesc, DhcpRlPktInSocket) != OSIX_SUCCESS)
        {
            MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                          "DhcpRlyDequePkt: SelAddFd for DHCP RELAY socket %d "
                          "failed !!!\n", i4SockDesc);
        }

        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
    }
    return;
}

/*******************************************************************************
 * Function Name   : DhrlDequeIpPkt.
 * Description     : Function to Dequeue a packet
 * Global Varibles :
 * Inputs          : None.
 * Output          : None.
 * Returns         : DHCP_SUCCESS
 *                   DHCP_FAILURE
 ******************************************************************************/
INT1
DhrlDequeIpPkt (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tDhcpRIpMsg         IpMsg;

    MEMSET (&IpMsg, 0, sizeof (tDhcpRIpMsg));
    while (OsixQueRecv (DHCP_RLY_IP_QUE_ID,
                        (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpMsg,
                                   0, sizeof (tDhcpRIpMsg));
        switch (IpMsg.u4Cmd)
        {
            case DHRL_IFACE_DEL_PKT:
                /*Take the lock */
                DHCPR_PROTO_LOCK ();
                DhrlProcessIfaceDeletion (IpMsg.u4IfIndex, IpMsg.u4CxtId);
                /*Release the lock */
                DHCPR_PROTO_UNLOCK ();
                break;

            case VCM_CONTEXT_CREATE:
                /*Take the lock */
                DHCPR_PROTO_LOCK ();
                if (DhcpRlyCreateContext (IpMsg.u4CxtId) == DHCP_FAILURE)
                {
                    MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                  "DhrlDequeIpPkt:Failed to create context entry"
                                  " for the  context id  %d\r\n ",
                                  IpMsg.u4CxtId);
                }
                /*Release the lock */
                DHCPR_PROTO_UNLOCK ();
                break;

            case VCM_CONTEXT_DELETE:
                /*Take the lock */
                DHCPR_PROTO_LOCK ();
                DhcpRlyDeleteContext (IpMsg.u4CxtId);
                /*Release the lock */
                DHCPR_PROTO_UNLOCK ();
                break;

            default:
                break;
        }
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
    }
    return DHCP_SUCCESS;
}

/*******************************************************************************
* Function Name   : DhcpRlyDequeVcmPkt.
* Description     : Function to Dequeue a VCM related message 
* Inputs          : None.
* Output          : None.
* Returns         : DHCP_SUCCESS/
*                   DHCP_FAILURE
******************************************************************************/
VOID
DhcpRlyDequeVcmPkt (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tDhcpRIpMsg         IpMsg;

    MEMSET (&IpMsg, 0, sizeof (tDhcpRIpMsg));
    while (OsixQueRecv (DHCP_RLY_IP_QUE_ID,
                        (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpMsg,
                                   0, sizeof (tDhcpRIpMsg));
        switch (IpMsg.u4Cmd)
        {
            case DHRL_IFACE_DEL_PKT:
                /*Take the lock */
                DHCPR_PROTO_LOCK ();
                DhrlProcessIfaceDeletion (IpMsg.u4IfIndex, IpMsg.u4CxtId);
                /*Release the lock */
                DHCPR_PROTO_UNLOCK ();
                break;

            case VCM_CONTEXT_CREATE:
                /*Take the lock */
                DHCPR_PROTO_LOCK ();
                if (DhcpRlyCreateContext (IpMsg.u4CxtId) == DHCP_FAILURE)
                {
                    MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                                  "DhcpRlyDequeVcmPkt:Failed to create context entry"
                                  " for the  context id  %d\r\n ",
                                  IpMsg.u4CxtId);
                }
                /*Release the lock */
                DHCPR_PROTO_UNLOCK ();
                break;

            case VCM_CONTEXT_DELETE:
                /*Take the lock */
                DHCPR_PROTO_LOCK ();
                DhcpRlyDeleteContext (IpMsg.u4CxtId);
                /*Release the lock */
                DHCPR_PROTO_UNLOCK ();
                break;

            default:
                break;
        }
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
    }
    return;
}

/******************************************************************************
 * Function Name   : DhrlPacketRcvd.
 * Description     : Function to receive a packet
 * Global Varibles :
 * Inputs          : None 
 * Output          : i4SockDesc - Socket descriptor in which data is received
 * Returns         : DHCP_FAILURE/
 *                   DHCP_SUCCESS
 ******************************************************************************/
INT1
DhrlPacketRcvd (INT4 i4SockDesc)
{
    INT4                i4DataLen;
    struct sockaddr_in  PeerAddr;
    struct msghdr       IpPktInfo;
#ifdef SLI_WANTED
    INT4                i4AddrLen = sizeof (PeerAddr);
    struct cmsghdr      CmsgInfo;
#endif
    struct in_pktinfo  *pIpPktInfo;

    tDhcpPktInfo       *pPktInfo = NULL;

#ifdef BSDCOMP_SLI_WANTED
    UINT1               au1Cmsg[DHCPRLY_CMSG_LEN];    /* For storing Auxillary 
                                                       Data - IP Packet INFO. */
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    pPktInfo = (tDhcpPktInfo *) MemAllocMemBlk (DHCP_RLY_PKT_INFO_POOL_ID);
    if (pPktInfo == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "Memory allocation failed !!!\n");
        return DHCP_FAILURE;
    }

    MEMSET (pPktInfo, 0, sizeof (tDhcpPktInfo));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (gai1RecvBuff, 0, DHCP_MAX_MTU);

#ifdef SLI_WANTED
    IpPktInfo.msg_control = (VOID *) &CmsgInfo;
    /* Call recvfrom to receive the packet */
    if ((i4DataLen =
         recvfrom (i4SockDesc, gai1RecvBuff,
                   (INT4) DHCP_MAX_MTU, 0,
                   (struct sockaddr *) &PeerAddr, &i4AddrLen)) < 0)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "recvfrom failed !!!\n");
        MemReleaseMemBlock (DHCP_RLY_PKT_INFO_POOL_ID, (UINT1 *) pPktInfo);
        return DHCP_FAILURE;
    }
    else
    {
        /* Get the interface index from which the packet is
         * received
         */
        if (recvmsg (i4SockDesc, &IpPktInfo, 0) < 0)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "recvmsg failed !!!\n");
            MemReleaseMemBlock (DHCP_RLY_PKT_INFO_POOL_ID, (UINT1 *) pPktInfo);
            return DHCP_FAILURE;
        }
#else
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    IpPktInfo.msg_name = (void *) &PeerAddr;

    IpPktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = gai1RecvBuff;
    Iov.iov_len = sizeof (gai1RecvBuff);
    IpPktInfo.msg_iov = &Iov;
    IpPktInfo.msg_iovlen = 1;
    IpPktInfo.msg_control = (void *) au1Cmsg;
    IpPktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&IpPktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    if ((i4DataLen = recvmsg (i4SockDesc, &IpPktInfo, 0)) < 0)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "recvmsg failed !!!\n");
        MemReleaseMemBlock (DHCP_RLY_PKT_INFO_POOL_ID, (UINT1 *) pPktInfo);
        return DHCP_FAILURE;
    }
    else
    {
        /* Broadcast packet sent by us over the socket is received by
         * us when BROADCAST option is set, So ignore such packets */
        if (NetIpv4IfIsOurAddress (PeerAddr.sin_addr.s_addr) == NETIPV4_SUCCESS)
        {
            MemReleaseMemBlock (DHCP_RLY_PKT_INFO_POOL_ID, (UINT1 *) pPktInfo);
            return DHCP_SUCCESS;
        }

#endif /* SLI_WANTED */
        /*Take the lock */
        DHCPR_PROTO_LOCK ();

        pIpPktInfo =
            (struct in_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&IpPktInfo));

        pPktInfo->u4IfNum = pIpPktInfo->ipi_ifindex;

        pPktInfo->u2Len = (UINT2) i4DataLen;

        MEMSET (&(pPktInfo->Pkt), 0, sizeof (tDhcpPacket));

        MEMCPY (&(pPktInfo->Pkt), gai1RecvBuff, i4DataLen);

        DhcpRelayProcPkt (pPktInfo, i4SockDesc);

        /*Release the lock */
        DHCPR_PROTO_UNLOCK ();
        MemReleaseMemBlock (DHCP_RLY_PKT_INFO_POOL_ID, (UINT1 *) pPktInfo);
        return DHCP_SUCCESS;
    }
}

/*************************************************************************/
/*  Function Name   : DhcpRlyRegisterWithVcm                                 */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Registers with VCM for handling context creation   */
/*                    and deletion                                       */
/*************************************************************************/
INT4
DhcpRlyRegisterWithVcm (VOID)
{
    tVcmRegInfo         VcmRegInfo;

    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = DhcpRlyNotifyContextChange;
    VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    VcmRegInfo.u1ProtoId = DHCP_RLY_PROTOCOL_ID;
    if (VCM_SUCCESS == VcmRegisterHLProtocol (&VcmRegInfo))
    {
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/*************************************************************************/
/*  Function Name   : DhcpRlyNotifyContextChange                             */
/*  Input(s)        : u4IfIndex - interface index                        */
/*                    u4ContextId - context id                           */
/*                    u1Event - Context creation/deletion event          */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Sends context creation/ deletion messages to the   */
/*                    tcp task;                                          */
/*************************************************************************/
VOID
DhcpRlyNotifyContextChange (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf;
    tDhcpRIpMsg         pQMsg;

    MEMSET (&pQMsg, 0, sizeof (tDhcpRIpMsg));

    if ((u1Event == VCM_CONTEXT_DELETE) || (u1Event == VCM_CONTEXT_CREATE))
    {
        pChainBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tDhcpRIpMsg), 0);

        if (pChainBuf == NULL)
        {
            return;
        }
        MEMSET (pChainBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
        CRU_BUF_UPDATE_MODULE_INFO (pChainBuf, "DhcpRlCxtChn");

        pQMsg.u4IfIndex = u4IfIndex;
        pQMsg.u4CxtId = u4ContextId;
        pQMsg.u4Cmd = (UINT4) u1Event;

        CRU_BUF_Copy_OverBufChain (pChainBuf, (UINT1 *) &pQMsg,
                                   0, sizeof (tDhcpRIpMsg));

        if (OsixQueSend (DHCP_RLY_IP_QUE_ID, (UINT1 *) &pChainBuf,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "Failed to post the notification from VCM to DHCP "
                     "Relay\r\n");
            CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);
            return;
        }
        OsixEvtSend (DHCP_RLY_TSK_ID, DHRL_VCM_EVENT);
    }
    return;
}

/******************************************************************************/
/* Function Name : DhcpRlyCreatePools                                         */
/* Description   : This function creates mem-pools at startup, for use by     */
/*                 DHCP Relay dynamically.                                    */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : DHCP_SUCCESS if the operation was successful,              */
/*                 DHCP_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
DhcpRlyCreatePools ()
{
    if (DhrlSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "In DhcpRlyCreatePools: DhrlSizingMemCreateMemPools returns"
                 "failure!!\n");
        return (DHCP_FAILURE);
    }
    DHCP_RLY_SERVER_POOL_ID =
        DHRLMemPoolIds[MAX_DHRL_FREE_SERVER_POOL_SIZING_ID];
    DHCP_RLY_IF_POOL_ID = DHRLMemPoolIds[MAX_DHRL_INTERFACE_SIZING_ID];
    DHCP_RLY_PKT_INFO_POOL_ID =
        DHRLMemPoolIds[MAX_DHRL_PKT_INFO_SIZE_SIZING_ID];
    DHCP_RLY_CLNT_INFO_POOL_ID =
        DHRLMemPoolIds[MAX_DHRL_CLNT_INFO_POOL_SIZING_ID];
    DHCP_RLY_CXT_POOL_ID = DHRLMemPoolIds[MAX_DHRL_RLY_CXT_POOL_SIZING_ID];

    return (DHCP_SUCCESS);
}

/******************************************************************************/
/* Function Name : DhcpRlyInitContextGlobals                                  */
/* Description   : This function Initialises the context specific relay agent */
/*                 variables and creates the RBTree for each context          */
/* Input(s)      : u4CxtId - Context Id                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : DHCP_SUCCESS if the operation was successful,              */
/*                 DHCP_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
DhcpRlyInitContextGlobals (UINT4 u4CxtId)
{
    INT2                i2CliInfoNum;

    if (DHCP_RLY_CXT_NODE[u4CxtId] == NULL)
    {
        return DHCP_FAILURE;
    }

    DHCP_RLY_CXT_NODE[u4CxtId]->u4CxtId = u4CxtId;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRelaying = (UINT4) DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfSecsThresh = (UINT4) DEFAULT_SEC_THRESHOLD;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfHopsThresh = (UINT4) DEFAULT_HOPS_THRESHOLD;

    /* If u4CfdhcpRelayFwdServersOnly enabled then DHCP packets from clients
     * will be sent to the specified server. Otherwise the packets will be
     * broadcast.
     */
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfdhcpRelayFwdServersOnly =
        (UINT4) DHCP_DISABLE;

    /* This variable is added to support the multiple servers */
    DHCP_RLY_CXT_NODE[u4CxtId]->pServersIp = NULL;

#ifdef RELAY_AGNT_INFO
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRAIOption = (UINT4) DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRAICircuitIDSubOption
        = (UINT4) DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRAIRemoteIDSubOption = (UINT4) DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRAISubnetMaskSubOption
        = (UINT4) DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRAIVPNIDSubOption = (UINT4) DHCP_DISABLE;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4StatRAIOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4StatRAICircuitIDSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4StatRAIRemoteIDSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4StatRAISubnetMaskSubOptionInserted = 0;

    DHCP_RLY_CXT_NODE[u4CxtId]->u4StatRAIOptionWronglyInserted = 0;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4StatRAISpaceConstraint = 0;
    DHCP_RLY_CXT_NODE[u4CxtId]->u4CfRAICktIDDefaultOptiontype
        = DHCP_CIRCUITID_TYPE_INTERFACE_INDEX;

    /* Create RBTree for interface to circuit id mapping and vice-versa */
    if ((DHCP_RLY_CXT_NODE[u4CxtId]->pIfToIdRBRoot = RBTreeCreateEmbedded
         ((FSAP_OFFSETOF (tDhrlIfConfigInfo, IfToIdRBNode)),
          DhrlRBCompareIfIndex)) == NULL)
    {
        return DHCP_FAILURE;
    }

    if ((DHCP_RLY_CXT_NODE[u4CxtId]->pCktIdToIfRBRoot = RBTreeCreateEmbedded
         ((FSAP_OFFSETOF (tDhrlIfConfigInfo, CktIdToIfRBNode)),
          DhrlRBCompareCktId)) == NULL)
    {
        return DHCP_FAILURE;
    }

    if ((DHCP_RLY_CXT_NODE[u4CxtId]->pRemoteIdToIfRBRoot = RBTreeCreateEmbedded
         ((FSAP_OFFSETOF (tDhrlIfConfigInfo, RemIdToIfRBNode)),
          DhrlRBCompareRemoteId)) == NULL)
    {
        return DHCP_FAILURE;
    }

#endif /* Relay Agent Info */
    for (i2CliInfoNum = 0; i2CliInfoNum < MAX_CLIENT_INFO; i2CliInfoNum++)
    {
        DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum] =
            (tclientinfo *) MemAllocMemBlk (DHCP_RLY_CLNT_INFO_POOL_ID);
        if (DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum] != NULL)
        {
            MEMSET (DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum],
                    0, sizeof (tclientinfo));
            DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum]->u1Used =
                (UINT1) DHCP_FALSE;
        }
        else
        {
            /* We need to allocate MAX_CLIENT_INFO no. of memblocks for
               each relay agent; If only partial mem blocks are avilable
               then, release the partially allocated blocks and do not
               start the relay on the given context */
            MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                     "DhcpRlyInitContextGlobals: Client Info. Memory "
                     "Block Allocation failed\r\n");
            i2CliInfoNum--;
            for (; i2CliInfoNum >= 0; i2CliInfoNum--)
            {
                MemReleaseMemBlock (DHCP_RLY_CLNT_INFO_POOL_ID,
                                    (UINT1 *) DHCP_RLY_CXT_NODE[u4CxtId]->
                                    apClntInfo[i2CliInfoNum]);
                DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum] = NULL;
            }
#ifdef RELAY_AGNT_INFO
            DhcpRlyUtilDelRBTree (u4CxtId);
#endif
            return DHCP_FAILURE;
        }
    }

    /* Initializes the memory for Dhcp server addresses */
    if (DhcpRelayInitFreeServersPool (u4CxtId) == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DHCP Servers only pool initialization failed\r\n");
        for (i2CliInfoNum = 0; i2CliInfoNum < MAX_CLIENT_INFO; i2CliInfoNum++)
        {
            if (DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum] != NULL)
            {
                MemReleaseMemBlock (DHCP_RLY_CLNT_INFO_POOL_ID,
                                    (UINT1 *) DHCP_RLY_CXT_NODE[u4CxtId]->
                                    apClntInfo[i2CliInfoNum]);
                DHCP_RLY_CXT_NODE[u4CxtId]->apClntInfo[i2CliInfoNum] = NULL;
            }
        }
#ifdef RELAY_AGNT_INFO
        DhcpRlyUtilDelRBTree (u4CxtId);
#endif
        return DHCP_FAILURE;
    }
    DHCP_RLY_CXT_NODE[u4CxtId]->u1Status = NOT_READY;
    return DHCP_SUCCESS;
}

/*************************************************************************/
/*  Function Name   : DhcpRlyCreateContext                               */
/*  Input(s)        : u4ContextId - Context Id                           */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Instantiates the DHCP RELAY for the given context  */
/*************************************************************************/
INT4
DhcpRlyCreateContext (UINT4 u4CxtId)
{
    if (u4CxtId >=
        FsDHRLSizingParams[MAX_DHRL_RLY_CXT_POOL_SIZING_ID].u4PreAllocatedUnits)
    {
        MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                      "DhcpRlyCreateContext: Invalid Context Id %u obtained\r\n",
                      u4CxtId);
        return (DHCP_FAILURE);
    }

    if (DHCP_RLY_CXT_NODE[u4CxtId] != NULL)
    {
        MOD_TRC_ARG1 (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                      "DhcpRlyCreateContext: Context %u already created\r\n",
                      u4CxtId);
        return (DHCP_SUCCESS);
    }

    DHCP_RLY_CXT_NODE[u4CxtId]
        = (tRlyCxtNode *) MemAllocMemBlk (DHCP_RLY_CXT_POOL_ID);
    if (DHCP_RLY_CXT_NODE[u4CxtId] == NULL)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRlyCreateContext: Context Node allocation failed\r\n");
        return (DHCP_FAILURE);
    }
    MEMSET (DHCP_RLY_CXT_NODE[u4CxtId], 0, sizeof (tRlyCxtNode));

#ifdef RELAY_AGNT_INFO
    if (DhrlRegisterWithIp (u4CxtId) == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRlyCreateContext: DhrlRegisterWithIp Failure\r\n");
        MemReleaseMemBlock (DHCP_RLY_CXT_POOL_ID,
                            (UINT1 *) DHCP_RLY_CXT_NODE[u4CxtId]);
        DHCP_RLY_CXT_NODE[u4CxtId] = NULL;
        return DHCP_FAILURE;
    }
#endif

    if (DhcpRlyInitContextGlobals (u4CxtId) == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRlyCreateContext: Dynamic Memory Allocations failed\r\n");
        MemReleaseMemBlock (DHCP_RLY_CXT_POOL_ID,
                            (UINT1 *) DHCP_RLY_CXT_NODE[u4CxtId]);
        DHCP_RLY_CXT_NODE[u4CxtId] = NULL;
        return DHCP_FAILURE;
    }

    /* Incrementing the Active Context Count; Should be decremented only
     * when the context gets deleted; This counter will not be incremented
     * if there is any failure in context creation */
    (gGlobalRlyNode.u4ActiveCxtCnt)++;

    return (DHCP_SUCCESS);
}

/*************************************************************************/
/*  Function Name   : DhcpRlyDeleteContext                               */
/*  Input(s)        : u4ContextId - Context Id                           */
/*  Output(s)       : None.                                              */
/*  Returns         : None                                               */
/*  Description     : Deletes the DHCP RELAY instance for the given cxt  */
/*************************************************************************/
VOID
DhcpRlyDeleteContext (UINT4 u4ContextId)
{
    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

    if (DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying == DHCP_ENABLE)
    {
        DhcpRelayShutdown (u4ContextId);
    }

    /* Free the memory allocated for dynamic DS in this relay agent */
    DhcpRlyDeInitContextGlobals (u4ContextId);
    MemReleaseMemBlock (DHCP_RLY_CXT_POOL_ID,
                        (UINT1 *) DHCP_RLY_CXT_NODE[u4ContextId]);
    DHCP_RLY_CXT_NODE[u4ContextId] = NULL;

#ifdef RELAY_AGNT_INFO
    if (DhrlDeRegisterWithIp (u4ContextId) == DHCP_FAILURE)
    {
        MOD_TRC (DHCP_RLY_TRC_FLAG, ALL_FAILURE_TRC, "DHCP",
                 "DhcpRlyDeleteContext: DhrlDeRegisterWithIp Failure\r\n");
    }
#endif

    /* Decrementing the number of active contexts */
    (gGlobalRlyNode.u4ActiveCxtCnt)--;
    return;
}

/******************************************************************************/
/* Function Name : DhcpRlyDeInitContextGlobals                                */
/* Description   : This function frees the memory allocated for the relay     */
/*                 agent in the given context                                 */
/* Input(s)      : u4CxtId - Context Id                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
DhcpRlyDeInitContextGlobals (UINT4 u4ContextId)
{
    UINT2               u2CliInfoNum = 0;

    if (DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

    DHCP_RLY_CXT_NODE[u4ContextId]->u4CfRelaying = DHCP_DISABLE;
#ifdef RELAY_AGNT_INFO
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAICircuitIDSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIRemoteIDSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAISubnetMaskSubOptionInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAIOptionWronglyInserted = 0;
    DHCP_RLY_CXT_NODE[u4ContextId]->u4StatRAISpaceConstraint = 0;
#endif

    DhcpRlyUtilDelRBTree (u4ContextId);

    /* release the free servers pool */
    MemReleaseMemBlock (DHCP_RLY_SERVER_POOL_ID,
                        (UINT1 *) DHCP_RLY_CXT_NODE[u4ContextId]->
                        pFreeServersPool);
    DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersPool = NULL;
    DHCP_RLY_CXT_NODE[u4ContextId]->pFreeServersIp = NULL;
    DHCP_RLY_CXT_NODE[u4ContextId]->pServersIp = NULL;

    for (u2CliInfoNum = 0; u2CliInfoNum < MAX_CLIENT_INFO; u2CliInfoNum++)
    {
        if (DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum] != NULL)
        {
            MemReleaseMemBlock (DHCP_RLY_CLNT_INFO_POOL_ID,
                                (UINT1 *) DHCP_RLY_CXT_NODE[u4ContextId]->
                                apClntInfo[u2CliInfoNum]);
            DHCP_RLY_CXT_NODE[u4ContextId]->apClntInfo[u2CliInfoNum] = NULL;
        }
    }
    return;
}
