/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhrlutil.c,v 1.4 2015/06/17 04:49:02 siva Exp $
 *
 * Description:This file contains utility routines that is used by
 *             DHCP Relay  module.
 *****************************************************************************/

#ifndef _DHRLUTIL_C_
#define _DHRLUTIL_C_

#include "dhcpincs.h"
#include "dhcp.h"

/*****************************************************************************/
/* Function     : DhrlUtilGetIfCiruitId                                      */
/*                                                                           */
/* Description  : This function gets the dhcp relay IfCiruitId based on given*/
/*                context id & interface index                               */
/*                                                                           */
/* Input        : u4ContextId - Context ID                                   */
/*                i4IfIndex - Interface index                                */
/*                pu4DhcpRelayIfCircuitId - pointer to circuit id            */
/*                                                                           */
/* Output       : pu4DhcpRelayIfCircuitId - pointer to circuit id            */
/*                                                                           */
/* Returns      : DHCP_SUCCESS/DHCP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DhrlUtilGetIfCiruitId (UINT4 u4ContextId, INT4 i4IfIndex, 
                                        UINT4 *pu4DhcpRelayIfCircuitId)
{
    tDhrlIfConfigInfo   DhrlIfConfigInfo;
    tDhrlIfConfigInfo  *pDhrlIfConfigInfo = NULL;
    UINT4               u4Port;

    MEMSET (&DhrlIfConfigInfo,0,sizeof(tDhrlIfConfigInfo));
    /* Get port number from CfaIfIndex */
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    DhrlIfConfigInfo.u4CxtId = u4ContextId;
    DhrlIfConfigInfo.u4Port = u4Port;
    if ((pDhrlIfConfigInfo = RBTreeGet 
            (DHCP_RLY_CXT_NODE[u4ContextId]->pIfToIdRBRoot,
                                   &DhrlIfConfigInfo)) == NULL)
    {
        return DHCP_FAILURE;
    }

    *pu4DhcpRelayIfCircuitId = pDhrlIfConfigInfo->u4CircuitId;
    return DHCP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : DhcpRlySetContextId                                       */
/* Description   : Sets the current context Id to u4ContextId passed         */
/* Input(s)      : u4ContextId - context identifier                          */
/* Output(s)     : None                                                      */
/* Return(s)     : DHCP_SUCCESS /DHCP_FAILURE                                */
/*****************************************************************************/

PUBLIC INT4
DhcpRlySetContextId (UINT4 u4ContextId)
{
    gGlobalRlyNode.u4CurrCxtId = u4ContextId;

    return DHCP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : DhcpRlyReSetContextId                                     */
/* Description   : ReSets the current context Id                             */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/
VOID
DhcpRlyResetContextId ()
{
    gGlobalRlyNode.u4CurrCxtId = DHCP_RLY_INVALID_CXT_ID;

    return;
}
/*****************************************************************************/
/* Function Name : DhcpRlyUtilDelRBTree                                      */
/* Description   : This Function frees the node in each RBTree to the        */
/*                 respective memory pool and then deletes the RBTree        */
/*                 in the given context                                      */
/* Input(s)      : u4ContextId - context identifier                          */
/* Output(s)     : None                                                      */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/
PUBLIC VOID
DhcpRlyUtilDelRBTree (UINT4 u4ContextId)
{
    if(DHCP_RLY_CXT_NODE[u4ContextId] == NULL)
    {
        return;
    }

    if (DHCP_RLY_CXT_NODE[u4ContextId]-> pIfToIdRBRoot != NULL)
    {
        RBTreeDestroy(DHCP_RLY_CXT_NODE[u4ContextId]->pIfToIdRBRoot,
                                    DhcpRlyUtilRBFreeInterface,0);
    }

    if (DHCP_RLY_CXT_NODE[u4ContextId]->pCktIdToIfRBRoot != NULL)
    {
        RBTreeDelete (DHCP_RLY_CXT_NODE[u4ContextId]->pCktIdToIfRBRoot);
    }

    if (DHCP_RLY_CXT_NODE[u4ContextId]->pRemoteIdToIfRBRoot != NULL)
    {
        RBTreeDelete (DHCP_RLY_CXT_NODE[u4ContextId]->pRemoteIdToIfRBRoot);
    }

    return;
}

/*****************************************************************************/
/* Function Name : DhcpRlyUtilRBFreeInterface                                */
/* Description   : This Function releases the memory associated with         */
/*                 Interace to ID map RB tree                                */ 
/* Input(s)      : u4ContextId - context identifier                          */
/* Output(s)     : None                                                      */
/* Return(s)     : DHCP_SUCCESS/DHCP_FAILURE                                 */
/*****************************************************************************/
PUBLIC INT4
DhcpRlyUtilRBFreeInterface (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if(pRBElem != NULL)
    {
        MemReleaseMemBlock (DHCP_RLY_IF_POOL_ID, (UINT1*) pRBElem);
    }
    return DHCP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : DhcpRlyGetFirstActiveContextID                            */
/* Description   : This Function gets the first context in which dhcp relay  */
/*                 is enabled(First Active Context); If Relay is not enabled */
/*                 in any one context, then return the INVALID context       */
/* Input(s)      : None                                                      */
/* Output(s)     : pu4ContextId - First active context identifier            */
/* Return(s)     : DHCP_SUCCESS/DHCP_FAILURE                                 */
/*****************************************************************************/
INT4
DhcpRlyGetFirstActiveContextID (UINT4 *pu4RetValContextID)
{
    UINT4   u4Iteration = 0;
    UINT4   u4ActiveCxt = 0;
    UINT1   u1IsRlyEnabled = DHCP_FALSE;
    
    for(u4Iteration = 0; u4Iteration < MAX_DHRL_RLY_CXT; u4Iteration++)
    {
        if (DHCP_RLY_CXT_NODE[u4Iteration] != NULL)
        {
            u4ActiveCxt++;
            if(DHCP_RLY_CXT_NODE[u4Iteration]->u4CfRelaying ==
                                                        DHCP_ENABLE)
            {   
                *pu4RetValContextID = u4Iteration;
                u1IsRlyEnabled = DHCP_TRUE;
                break;
            }
        }
        if (u4ActiveCxt == gGlobalRlyNode.u4ActiveCxtCnt)
        {
            break;
        }
    }
    if (u1IsRlyEnabled == DHCP_FALSE)
    {
        *pu4RetValContextID = DHCP_RLY_INVALID_CXT_ID;
        return  DHCP_FAILURE;
    }
    else
    {
        return DHCP_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name : DhcpRlyGetNextActiveContextID                            */
/* Description   : This Function gets the next context in which dhcp relay  */
/*                 is enabled(Next Active Context);                         */
/* Input(s)      : None                                                      */
/* Output(s)     : pu4ContextId - First active context identifier            */
/* Return(s)     : DHCP_SUCCESS/DHCP_FAILURE                                 */
/*****************************************************************************/
INT4
DhcpRlyGetNextActiveContextID (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    UINT4   u4Iteration = u4ContextId + 1;
    
    for(; u4Iteration < MAX_DHRL_RLY_CXT; u4Iteration++)
    {
        if (DHCP_RLY_CXT_NODE[u4Iteration] != NULL)
        {
            if(DHCP_RLY_CXT_NODE[u4Iteration]->u4CfRelaying ==
                                                        DHCP_ENABLE)
            {   
                *pu4NextContextId = u4Iteration;
                return DHCP_SUCCESS;
            }
        }
    }
    return  DHCP_FAILURE;
}

/*****************************************************************************/
/* Function Name : DhcpRlyGetNextContextId                                   */
/* Description   : This Function gets the next context of the system         */
/* Input(s)      : u4ContextId - current context id                          */
/* Output(s)     : pu4ContextId - First active context identifier            */
/* Return(s)     : DHCP_SUCCESS/DHCP_FAILURE                                 */
/*****************************************************************************/
INT4
DhcpRlyGetNextContextId (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    UINT4   u4Iteration = u4ContextId + 1;
    
    for(; u4Iteration < MAX_DHRL_RLY_CXT; u4Iteration++)
    {
        if (DHCP_RLY_CXT_NODE[u4Iteration] != NULL)
        {
            *pu4NextContextId = u4Iteration;
            return DHCP_SUCCESS;
        }
    }
    return  DHCP_FAILURE;
}

#endif /* _DHRLUTIL_C_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  <dhrlutil.c>                      */
/*--------------------------------------------------------------------------*/
