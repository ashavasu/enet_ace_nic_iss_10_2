/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: dhrlcli.c,v 1.53 2017/12/14 10:23:42 siva Exp $
 * *
 * * Description: Action routines for set/get objects in
 *                fsdhcpRelay.mib  
 * *********************************************************************/
/* SOURCE FILE HEADER :

 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : dhrlcli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Balaji TS                                        |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : Dhcp Relay                                       |
 * |                                                                           |
 * |  MODULE NAME           : Dhcp Relay  configuration                        |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           |
 * |                          fsdhcpRelay.mib                                  |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __DHRLCLI_C__
#define __DHRLCLI_C__

#include "dhcpincs.h"
#include "dhrlwr.h"
#include "cli.h"
#include "dhcp.h"
#include "dhrlcli.h"
#include "dhrclipt.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fsdhcprelaycli.h"
#include "fsmidhrlycli.h"
#include "fsmidhwr.h"

/*********************************************************************/
/*  Function Name : cli_process_dhcpr_cmd                            */
/*  Description   : This function is called by commands in Command   */
/*                  definition files.This function retrieves all     */
/*                  Input parameters from the command and passes them*/
/*                  to the Protocol Action Routines.                 */
/*                                                                   */
/*  Input(s)      : va_alist - Variable no. of arguments             */
/*  Output(s)     : NONE                                             */
/*                                                                   */
/*  Return Values : None.                                            */
/*********************************************************************/

INT4
cli_process_dhcpr_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[DHCPR_CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CircuitId = DHCPR_CLI_NO_CKT_ID;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode;
    UINT1              *pu1RemoteId = NULL;
    UINT1               u1Operation = 0;
    UINT1              *pu1DhcpCxtName = NULL;
    UINT4               u4VcId = DHCP_RLY_INVALID_CXT_ID;
    INT4                i4VcmRetStatus = CLI_FAILURE;
    UINT4               u4PrevId = DHCP_RLY_INVALID_CXT_ID;
    UINT4               u4Port = 0;
    UINT4               u4RelayCount = 0;

    /* Variable Argument extraction */
    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array.
     * Store DHCPR_CLI_MAX_ARGS arguements at the max.
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == DHCPR_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, DhcpRProtocolLock, DhcpRProtocolUnlock);
    DHCPR_PROTO_LOCK ();

    /* 1) For global configuration commands, show & clear commands the 4th 
     * argument will always be the VRF name. 
     * 2) For the global configuration commands, if the VRF name is not 
     * specified, then that configuration will be done in DEFAULT context. 
     * 3) For show/clear commands, if the VRF name is not specified, then 
     * it will be applied to all the contexts. */
    /* For INTERFACE LEVEL commands, no VRF token is available. Hence its 
     * 4th arugument will not be the VRF name. 
     * So, below logic should be used only for global configuration commands, 
     * show & clear commands */

    if ((u4Command > CLI_DHCPR_NO_REMOTE_ID) &&
        (u4Command < CLI_DHCPR_MAX_COMMANDS))
    {
        /* args[0] -> it contains vrf name */
        pu1DhcpCxtName = (UINT1 *) args[0];

        if (pu1DhcpCxtName != NULL)
        {
            if (VcmIsVrfExist (pu1DhcpCxtName, &u4VcId) == VCM_TRUE)
            {
                i4VcmRetStatus = CLI_SUCCESS;
            }
        }
        else
        {
            /* If the VRF Id is not specified, then for the
             * configuration commands, obtain it from cli context.
             * If this is invalid, set it as the default VRF.
             * For show commands, when VRF name is not given
             * retain it as invalid VC */

            if (u4Command < CLI_DHCPR_CLEAR_STATS)
            {

                u4VcId = CLI_GET_CXT_ID ();
                if (u4VcId == DHCP_RLY_INVALID_CXT_ID
                    || u4VcId == (UINT4) FAILURE)
                {
                    u4VcId = DHCP_RLY_DFLT_CXT_ID;
                }
            }
            i4VcmRetStatus = CLI_SUCCESS;
        }
    }
    /* Globally setting the context for all config commands which uses vrf-name */
    /* For interface level configuration commands, fetch the context id from 
     * interface index */

    switch (u4Command)
    {
        case CLI_DHCPR_CIRCUIT_ID:
        case CLI_DHCPR_NO_CIRCUIT_ID:
        case CLI_DHCPR_REMOTE_ID:
        case CLI_DHCPR_NO_REMOTE_ID:
            u4IfIndex = CLI_GET_IFINDEX ();
            if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port)
                == NETIPV4_FAILURE)
            {
                DHCPR_PROTO_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return DHCP_FAILURE;
            }
            if (NetIpv4GetCxtId (u4Port, &u4VcId) == NETIPV4_FAILURE)
            {
                DHCPR_PROTO_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return DHCP_FAILURE;
            }
            break;
        default:
            break;
    }

    if ((i4VcmRetStatus == CLI_FAILURE) && (u4VcId == DHCP_RLY_INVALID_CXT_ID))
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        DHCPR_PROTO_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if (u4VcId != DHCP_RLY_INVALID_CXT_ID)
    {
        if (DhcpRlySetContextId (u4VcId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
            DHCPR_PROTO_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }

    switch (u4Command)
    {

        case CLI_DHCPR_DEBUG:
            /* args[1] - Value */
            i4RetStatus =
                DhcpRelaySetDebug (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                   CLI_ENABLE);
            break;

        case CLI_DHCPR_NO_DEBUG:
            /* args[1] - Value */
            i4RetStatus =
                DhcpRelaySetDebug (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                   CLI_DISABLE);
            break;

        case CLI_DHCPR_SERVICE:
            if ((DhcpRelaySetService (CliHandle, (INT4) DHCP_ENABLE)) ==
                CLI_SUCCESS)
            {
                i4RetStatus = CLI_SUCCESS;
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case CLI_DHCPR_NO_SERVICE:
            if ((DhcpRelaySetService (CliHandle, (INT4) DHCP_DISABLE)) ==
                CLI_SUCCESS)
            {
                i4RetStatus = CLI_SUCCESS;
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case CLI_DHCPR_SERVER:
            /* args[1] - IP address */
            i4RetStatus = DhcpRelaySetServer (CliHandle, *args[1]);
            break;

        case CLI_DHCPR_NO_SERVER:
            /* args[1] - IP address */
            i4RetStatus = DhcpRelaySetNoServer (CliHandle, *args[1]);
            break;

        case CLI_DHCPR_INFO_OPTION:
            i4RetStatus =
                DhcpRelaySetInfoOption (CliHandle, (INT4) DHCP_ENABLE);
            break;

        case CLI_DHCPR_NO_INFO_OPTION:
            i4RetStatus =
                DhcpRelaySetInfoOption (CliHandle, (INT4) DHCP_DISABLE);
            break;

        case CLI_DHCPR_CIRCUIT_ID:
            u4IfIndex = CLI_GET_IFINDEX ();
            u4CircuitId = *((UINT4 *) (args[0]));
            i4RetStatus = DhcpRelaySetCktId (CliHandle, u4IfIndex, u4CircuitId);
            break;

        case CLI_DHCPR_NO_CIRCUIT_ID:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = DhcpRelaySetCktId (CliHandle, u4IfIndex, u4CircuitId);
            break;

        case CLI_DHCPR_REMOTE_ID:
            u4IfIndex = CLI_GET_IFINDEX ();
            u1Operation = DHCPR_CLI_SET_REM_ID;
            i4RetStatus = DhcpRelaySetRemId (CliHandle, u4IfIndex,
                                             (UINT1 *) args[0], u1Operation);
            break;

        case CLI_DHCPR_NO_REMOTE_ID:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = DhcpRelaySetRemId (CliHandle, u4IfIndex,
                                             pu1RemoteId, u1Operation);
            break;

        case CLI_DHCPR_SHOW_INFO:
            if (DHCP_RLY_INVALID_CXT_ID != u4VcId)    /*Display for given Vrf Name */
            {
                i4RetStatus = DhcpRelayShowInfo (CliHandle);
            }
            else                /* If no Vrf Name is specified,display for all Vrf-Name */
            {
                u4VcId = DHCP_RLY_DFLT_CXT_ID;
                do
                {
                    if (DhcpRlySetContextId ((INT4) u4VcId) == DHCP_FAILURE)
                    {
                        break;
                    }

                    i4RetStatus = DhcpRelayShowInfo (CliHandle);
                    u4RelayCount++;
                    DhcpRlyResetContextId ();
                    u4PrevId = u4VcId;
                }
                while (DhcpRlyGetNextContextId (u4PrevId, &u4VcId) ==
                       DHCP_SUCCESS);
                CliPrintf (CliHandle,
                           "Total Relay Agents in the system : %d \r\n",
                           u4RelayCount);
            }
            break;

        case CLI_DHCPR_SHOW_SERVER:
            if (DHCP_RLY_INVALID_CXT_ID != u4VcId)    /*Display for given Vrf Name */
            {
                i4RetStatus = DhcpRelayShowServer (CliHandle);
            }
            else                /* If no Vrf Name is specified,display for all Vrf-Name */
            {
                u4VcId = DHCP_RLY_DFLT_CXT_ID;
                do
                {
                    if (SNMP_FAILURE == DhcpRlySetContextId ((INT4) u4VcId))
                    {
                        break;
                    }

                    DhcpRelayShowServer (CliHandle);
                    DhcpRlyResetContextId ();
                    u4PrevId = u4VcId;
                }
                while (DhcpRlyGetNextContextId (u4PrevId, &u4VcId) ==
                       DHCP_SUCCESS);
            }
            break;

        case CLI_DHCPR_SHOW_IF_INFO:
            i4RetStatus = DhcpRelayShowSpecificIfInfo (CliHandle, i4IfaceIndex);
            break;

        case CLI_DHCPR_CIRCUIT_OPTION:
            i4RetStatus = DhcpRelaySetCircuitOption (CliHandle,
                                                     (CLI_PTR_TO_U4 (args[1])));
            break;

        default:                /* CLI_DHCPR_CLEAR_STATS: */
            if (DHCP_RLY_INVALID_CXT_ID != u4VcId)    /*Display for given Vrf Name */
            {
                i4RetStatus = DhcpRelaySetClearCounters (CliHandle);
            }
            else                /* If no Vrf Name is specified,display for all Vrf-Name */
            {
                u4VcId = DHCP_RLY_DFLT_CXT_ID;
                do
                {
                    if (SNMP_FAILURE == DhcpRlySetContextId ((INT4) u4VcId))
                    {
                        break;
                    }

                    i4RetStatus = DhcpRelaySetClearCounters (CliHandle);
                    DhcpRlyResetContextId ();
                    u4PrevId = u4VcId;
                }
                while (DhcpRlyGetNextContextId (u4PrevId, &u4VcId) ==
                       DHCP_SUCCESS);
            }
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DHCPR_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", DhcprCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);
    DHCPR_PROTO_UNLOCK ();

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetServerOnly                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets servers-only option which when  */
/*                        set, enables the relay agent to forward packets to */
/*                        a specific DHCP server                             */
/*                                                                           */
/*     INPUT            : 1. i4Status  - DHCP_ENABLE / DHCP_DISABLE          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetServerOnly (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrCode;
    INT4                i4CxtId;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId,
                                             &i4RowStatus) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_DHCPR_NO_RELAY);
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode,
                                                    i4CxtId,
                                                    NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId,
                                                 NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMIDhcpRelayServersOnly (&u4ErrCode, i4CxtId,
                                           i4Status) == SNMP_SUCCESS)
    {
        if (nmhSetFsMIDhcpRelayServersOnly (i4CxtId, i4Status) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Set Failed\r\n");
            i4RetVal = CLI_FAILURE;
        }

    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode,
                                                    i4CxtId,
                                                    i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId,
                                                 i4RowStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetService                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the server ip address           */
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. u4IpAddr  - IP address                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetService (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrCode;
    INT4                i4CxtId;
    INT4                i4RowStatus = 0;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (i4Status == DHCP_ENABLE)
    {
        if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            /* While enabling relay, if row status get failed, then no 
               relay is enabled on that context */
            if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                        CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
            if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
            if (nmhTestv2FsMIDhcpRelaying (&u4ErrCode, i4CxtId, i4Status)
                == SNMP_FAILURE)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, DESTROY)
                    == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                }
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
            if (nmhSetFsMIDhcpRelaying (i4CxtId, i4Status) == SNMP_FAILURE)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, DESTROY)
                    == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                }
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
            if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                        ACTIVE) == SNMP_FAILURE)
            {
                if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, DESTROY)
                    == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                }
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
            if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, ACTIVE)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
        }
        if (i4RowStatus == ACTIVE)
        {
            CliPrintf (CliHandle,
                       "Relay Already Started in the given context\r\n");
        }
        else
        {
            if (nmhTestv2FsMIDhcpRelaying (&u4ErrCode, i4CxtId, i4Status)
                == SNMP_SUCCESS)
            {
                if (nmhSetFsMIDhcpRelaying (i4CxtId, i4Status) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                    return CLI_FAILURE;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_DHCPR_START_FAILED);
                return CLI_FAILURE;
            }
        }
    }
    else if (i4Status == DHCP_DISABLE)
    {
        /* DHCP Disable Case */
        if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            /* Relay Agent for the given context is not present */
            CLI_SET_ERR (CLI_DHCPR_NO_RELAY);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsMIDhcpRelaying (&u4ErrCode, i4CxtId, i4Status)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_DHCPR_STOP_FAILED);
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDhcpRelaying (i4CxtId, i4Status) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_DHCPR_STOP_FAILED);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetDebug                                  */
/*                                                                           */
/*     DESCRIPTION      : This function set/reset  the trace level           */
/*                        for dhcp relay                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Cli Context ID                         */
/*                        i4DebugLevel - Trace level                         */
/*                        u1TraceFlag  - CLI_ENABLE/CLI_DISABLE              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetDebug (tCliHandle CliHandle, INT4 i4DebugLevel, UINT1 u1TraceFlag)
{
    UINT4               u4ErrCode;
    INT4                i4DhcpRelayTrace;
    INT4                i4CxtId;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_DHCPR_NO_RELAY);
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                    NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (u1TraceFlag == CLI_DISABLE)
    {
        nmhGetFsMIDhcpConfigTraceLevel (i4CxtId, &i4DhcpRelayTrace);

        i4DebugLevel = (i4DhcpRelayTrace & (~i4DebugLevel));
    }
    if (nmhTestv2FsMIDhcpConfigGblTraceLevel (&u4ErrCode, i4DebugLevel) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsMIDhcpConfigTraceLevel (i4CxtId, i4DebugLevel) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Context Trace Set failed\r\n");
            i4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                    i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetServer                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets the server ip address           */
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. u4IpAddr  - IP address                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetServer (tCliHandle CliHandle, UINT4 u4IpAddr)
{

    /*code enhancement for Dhcp relay multiple servers support is added in this 
       function  */

    INT1                i1Flag = DHCP_DISABLE;
    INT4                i4RowStatus;
    UINT4               u4ErrorCode;
    INT4                i4CxtId;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhGetFsMIDhcpRelaySrvAddressRowStatus
        (i4CxtId, u4IpAddr, &i4RowStatus) == SNMP_SUCCESS)
    {
        /*Entry already exists */
        return (CLI_SUCCESS);
    }
    if (DHCP_RLY_CXT_NODE[i4CxtId] != NULL)
    {
        if (DHCP_RLY_CXT_NODE[i4CxtId]->pServersIp == NULL)
        {
            i1Flag = DHCP_ENABLE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_DHCPR_MAX_RLY_CXT);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsMIDhcpRelaySrvAddressRowStatus
        (&u4ErrorCode, i4CxtId, u4IpAddr, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsMIDhcpRelaySrvAddressRowStatus
        (i4CxtId, u4IpAddr, CREATE_AND_GO) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i1Flag == DHCP_ENABLE)
        /*First server configured, Enable the Serversonly Flag */
    {
        DhcpRelaySetServerOnly (CliHandle, DHCP_ENABLE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelayNoSetServer                               */
/*                                                                           */
/*     DESCRIPTION      : This function deletes server ip address            */
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. u4IpAddr  - IP address                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetNoServer (tCliHandle CliHandle, UINT4 u4IpAddr)
{

/*This function is modified to enable the relay to store more than one 
  server ip address  */

    UINT4               u4ErrCode;
    INT4                i4CxtId;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhTestv2FsMIDhcpRelaySrvAddressRowStatus
        (&u4ErrCode, i4CxtId, u4IpAddr, DESTROY) != SNMP_SUCCESS)
    {
        if (u4ErrCode == SNMP_ERR_NO_CREATION)
        {
            CLI_SET_ERR (CLI_DHCPR_NO_SERVER_ERR);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return (CLI_FAILURE);
    }

    if (nmhSetFsMIDhcpRelaySrvAddressRowStatus (i4CxtId,
                                                u4IpAddr,
                                                DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (!(DHCP_RLY_CXT_NODE[i4CxtId]->pServersIp))
        /*No servers configured, Disable the Dhcpserversonly flag */
    {
        DhcpRelaySetServerOnly (CliHandle, DHCP_DISABLE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetInfoOption                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the RAI option processing status*/
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. i4Status  - DHCP_ENABLE/DHCP_DISABLE            */
/*                                                                           */
/*     OUTPUT           : CLI_FAILURE / CLI_SUCCESS                          */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetInfoOption (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrCode;
    INT4                i4CxtId;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_DHCPR_NO_RELAY);
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                    NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMIDhcpRelayRAIOptionControl (&u4ErrCode,
                                                i4CxtId,
                                                i4Status) == SNMP_SUCCESS)
    {
        if (nmhSetFsMIDhcpRelayRAIOptionControl (i4CxtId,
                                                 i4Status) == SNMP_SUCCESS)
        {
            if (nmhSetFsMIDhcpRelayRAICircuitIDSubOptionControl (i4CxtId,
                                                                 i4Status) ==
                SNMP_SUCCESS)
            {
                if (nmhSetFsMIDhcpRelayRAIRemoteIDSubOptionControl (i4CxtId,
                                                                    i4Status) ==
                    SNMP_SUCCESS)
                {
                    if (nmhSetFsMIDhcpRelayRAISubnetMaskSubOptionControl
                        (i4CxtId, i4Status) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "RAI Subnet Mask suboption set failed\r\n");
                        i4RetVal = CLI_FAILURE;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "RAI Remote ID suboption set failed\r\n");
                    i4RetVal = CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "RAI Circuit ID suboption set failed\r\n");
                i4RetVal = CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle, "RAI option control set failed\r\n");
            i4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                    i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if ((i4RetVal == CLI_SUCCESS) && (i4Status == DHCP_ENABLE))
    {

        CliPrintf (CliHandle,
                   "\rWARNING: When RAI is enabled, the circuit-id needs to be configured explicitly by user\r\n");
    }
    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetCktId                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the circuit id                  */
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. u4IfIndex - Interface index                     */
/*                        2. u4CircuitId - The circuit id                    */
/*                                                                           */
/*     OUTPUT           : CLI_FAILURE / CLI_SUCCESS                          */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetCktId (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4CircuitId)
{
    tSNMP_OCTET_STRING_TYPE sOctetStrRemoteId;
    INT4                i4ExstRowStatus = DESTROY;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ExstCircuitId = 0;
    UINT4               u4ErrorCode;
    UINT1               au1RemoteId[DHRL_MAX_RID_LEN];
    INT4                i4CxtId;

    MEMSET (&au1RemoteId[0], 0, DHRL_MAX_RID_LEN);
    MEMSET (&sOctetStrRemoteId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    sOctetStrRemoteId.pu1_OctetList = &au1RemoteId[0];
    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (u4CircuitId != DHCPR_CLI_NO_CKT_ID)
    {
        if (nmhGetFsMIDhcpRelayIfRowStatus
            (i4CxtId, (INT4) u4IfIndex, &i4ExstRowStatus) == SNMP_SUCCESS)
        {
            /* Entry exist */
            i4RetVal =
                nmhGetFsMIDhcpRelayIfCircuitId (i4CxtId, (INT4) u4IfIndex,
                                                &u4ExstCircuitId);
            if (i4ExstRowStatus == ACTIVE)
            {
                /* Set it to NOT_IN_SERVICE state */
                if (nmhTestv2FsMIDhcpRelayIfRowStatus
                    (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex,
                     NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsMIDhcpRelayIfRowStatus
                    (i4CxtId, (INT4) u4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
        else
        {
            /* Add an entry to table */
            if (nmhTestv2FsMIDhcpRelayIfRowStatus
                (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex,
                 CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsMIDhcpRelayIfRowStatus
                (i4CxtId, (INT4) u4IfIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FsMIDhcpRelayIfCircuitId
            (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex,
             u4CircuitId) == SNMP_FAILURE)
        {
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                i4ExstRowStatus);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayIfCircuitId
            (i4CxtId, (INT4) u4IfIndex, u4CircuitId) == SNMP_FAILURE)
        {
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                i4ExstRowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMIDhcpRelayIfRowStatus
            (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex, ACTIVE) == SNMP_SUCCESS)
        {
            if (nmhSetFsMIDhcpRelayIfRowStatus
                (i4CxtId, (INT4) u4IfIndex, ACTIVE) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            CLI_FATAL_ERROR (CliHandle);
        }

        i4RetVal =
            nmhSetFsMIDhcpRelayIfCircuitId (i4CxtId, (INT4) u4IfIndex,
                                            u4ExstCircuitId);
        i4RetVal =
            nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                            i4ExstRowStatus);
        return CLI_FAILURE;
    }
    else
    {
        /* Check if any remote id configuration exist. */
        i4RetVal = nmhGetFsMIDhcpRelayIfRemoteId (i4CxtId, (INT4) u4IfIndex,
                                                  &sOctetStrRemoteId);
        if ((i4RetVal != SNMP_FAILURE) && (sOctetStrRemoteId.i4_Length != 0) &&
            (STRCMP (sOctetStrRemoteId.pu1_OctetList, "XYZ")))
        {
            /* There exist remote id configuration over this interface. So clear 
             * the circuit id configuration alone */
            i4RetVal =
                nmhSetFsMIDhcpRelayIfCircuitId (i4CxtId, (INT4) u4IfIndex,
                                                u4ExstCircuitId);

        }
        else
        {
            /* There is no remote id configured over this interface.So
             * delete the entry */
            i4RetVal =
                nmhTestv2FsMIDhcpRelayIfRowStatus (&u4ErrorCode, i4CxtId,
                                                   (INT4) u4IfIndex, DESTROY);
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                DESTROY);
        }
    }
    UNUSED_PARAM (i4RetVal);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetRemId                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the remote  id                  */
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. u4IfIndex - Interface index                     */
/*                        2. i4RemoteId - The circuit id                     */
/*                                                                           */
/*     OUTPUT           : CLI_FAILURE / CLI_SUCCESS                          */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetRemId (tCliHandle CliHandle, UINT4 u4IfIndex,
                   UINT1 *pu1RemoteId, UINT1 u1Operation)
{
    tSNMP_OCTET_STRING_TYPE sOctetStrRemoteId;
    tSNMP_OCTET_STRING_TYPE sOctetStrPrevRemoteId;
    INT4                i4ExstRowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode;
    UINT4               u4CircuitId;
    UINT1               au1RemoteId[DHRL_MAX_RID_LEN];
    UINT1               au1PrevRemoteId[DHRL_MAX_RID_LEN];
    INT4                i4CxtId;

    MEMSET (&au1RemoteId[0], 0, DHRL_MAX_RID_LEN);
    MEMSET (&au1PrevRemoteId[0], 0, DHRL_MAX_RID_LEN);
    MEMSET (&sOctetStrRemoteId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&sOctetStrPrevRemoteId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    sOctetStrRemoteId.pu1_OctetList = &au1RemoteId[0];
    sOctetStrPrevRemoteId.pu1_OctetList = &au1PrevRemoteId[0];
    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (u1Operation == DHCPR_CLI_SET_REM_ID)
    {
        if (STRLEN (pu1RemoteId) >= DHRL_MAX_RID_LEN)
        {
            CLI_SET_ERR (CLI_DHCPR_WRONG_LENGTH);
            return CLI_FAILURE;
        }

        sOctetStrRemoteId.i4_Length = (INT4) STRLEN (pu1RemoteId);
        STRNCPY (sOctetStrRemoteId.pu1_OctetList, pu1RemoteId,
                 STRLEN (pu1RemoteId));
        sOctetStrRemoteId.pu1_OctetList[STRLEN (pu1RemoteId)] = '\0';

        if (nmhGetFsMIDhcpRelayIfRowStatus
            (i4CxtId, (INT4) u4IfIndex, &i4ExstRowStatus) == SNMP_SUCCESS)
        {
            /* Entry exist */
            if (i4ExstRowStatus == ACTIVE)
            {
                /* Get the existing remote id value */
                i4RetVal =
                    nmhGetFsMIDhcpRelayIfRemoteId (i4CxtId, (INT4) u4IfIndex,
                                                   &sOctetStrPrevRemoteId);
                /* Set it to NOT_IN_SERVICE state */
                if (nmhTestv2FsMIDhcpRelayIfRowStatus (&u4ErrorCode, i4CxtId,
                                                       (INT4) u4IfIndex,
                                                       NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                    NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
        else
        {
            i4ExstRowStatus = DESTROY;

            /* Add an entry to table */
            if (nmhTestv2FsMIDhcpRelayIfRowStatus
                (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex,
                 CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsMIDhcpRelayIfRowStatus
                (i4CxtId, (INT4) u4IfIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FsMIDhcpRelayIfRemoteId
            (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex,
             &sOctetStrRemoteId) == SNMP_FAILURE)
        {
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                i4ExstRowStatus);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIDhcpRelayIfRemoteId
            (i4CxtId, (INT4) u4IfIndex, &sOctetStrRemoteId) == SNMP_FAILURE)
        {
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                i4ExstRowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMIDhcpRelayIfRowStatus
            (&u4ErrorCode, i4CxtId, (INT4) u4IfIndex, ACTIVE) == SNMP_SUCCESS)
        {
            if (nmhSetFsMIDhcpRelayIfRowStatus
                (i4CxtId, (INT4) u4IfIndex, ACTIVE) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            CLI_FATAL_ERROR (CliHandle);
        }

        /* In case of already existing entry, update with that value.Else
         * update with defaulrt value*/
        i4RetVal =
            nmhSetFsMIDhcpRelayIfRemoteId (i4CxtId, (INT4) u4IfIndex,
                                           &sOctetStrPrevRemoteId);
        i4RetVal =
            nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                            i4ExstRowStatus);
        return CLI_FAILURE;
    }
    else
    {
        /* Check if any ckt id configuration exist. */
        i4RetVal =
            nmhGetFsMIDhcpRelayIfCircuitId (i4CxtId, (INT4) u4IfIndex,
                                            &u4CircuitId);
        if ((i4RetVal != SNMP_FAILURE) && (u4CircuitId != 0))
        {
            /* There exist ckt id configuration over this interface. So clear 
             * the remote id configuration alone */
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRemoteId (i4CxtId, (INT4) u4IfIndex,
                                               &sOctetStrRemoteId);
        }
        else
        {
            /* There is no circuit id configured over this interface.So
             * delete the entry */
            i4RetVal =
                nmhTestv2FsMIDhcpRelayIfRowStatus (&u4ErrorCode, i4CxtId,
                                                   (INT4) u4IfIndex, DESTROY);
            i4RetVal =
                nmhSetFsMIDhcpRelayIfRowStatus (i4CxtId, (INT4) u4IfIndex,
                                                DESTROY);
        }
    }
    UNUSED_PARAM (i4RetVal);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpRelaySetCircuitOption                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the default option the          */
/*                        to be present in the circuit Id                    */
/*                                                                           */
/*     INPUT            : 1. CliHandle - CLI Context ID                      */
/*                        2. u4InCircuitActFlag - Circuit Id flag            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpRelaySetCircuitOption (tCliHandle CliHandle, UINT4 u4InCircuitActFlag)
{

    INT4                i4RetStatus = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    INT4                i4CxtId;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    /* Allocate Memory For CircuitFlag */
    pOctStrActFlag = allocmem_octetstring (DHCP_CIRCUIT_ACTION_FLAG_LENGTH);

    if (pOctStrActFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);

    MEMCPY (pOctStrActFlag->pu1_OctetList, &u4InCircuitActFlag,
            DHCP_CIRCUIT_ACTION_FLAG_LENGTH);
    i4RetStatus =
        nmhTestv2FsMIDhcpConfigDhcpCircuitOption (&u4ErrorCode, i4CxtId,
                                                  pOctStrActFlag);

    if (i4RetStatus == SNMP_FAILURE)
    {
        free_octetstring (pOctStrActFlag);
        return (CLI_FAILURE);
    }

    i4RetStatus =
        nmhSetFsMIDhcpConfigDhcpCircuitOption (i4CxtId, pOctStrActFlag);

    if (i4RetStatus == SNMP_FAILURE)
    {
        free_octetstring (pOctStrActFlag);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    free_octetstring (pOctStrActFlag);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : DhcpRelayShowSpecificIfInfo                            */
/*                                                                           */
/* Description      : This function is invoked to display DHCP Relay if info */
/*                    for a particular interface                             */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4IfIndex - the interface index                     */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
DhcpRelayShowSpecificIfInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE sOctetStrRemoteId;
    INT4                i4RowStatus;
    INT4                i4RetVal = SNMP_FAILURE;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT4               u4CircuitId;
    UINT1               au1RemoteId[DHRL_MAX_RID_LEN];
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    UINT4               u4Port = 0;
    UINT4               u4CxtId = 0;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = &ai1IfName[0];

    MEMSET (au1RemoteId, 0, DHRL_MAX_RID_LEN);
    MEMSET (&sOctetStrRemoteId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    sOctetStrRemoteId.pu1_OctetList = &au1RemoteId[0];

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (NetIpv4GetCxtId (u4Port, &u4CxtId) == NETIPV4_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsMIDhcpRelayIfRowStatus ((INT4) u4CxtId, i4IfIndex,
                                        &i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (i4RowStatus == ACTIVE)
    {
        VcmGetAliasName (u4CxtId, au1CxtName);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Context Name : %s\r\n", au1CxtName);
        CliPrintf (CliHandle, "------------\r\n");

        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
        CliPrintf (CliHandle, "\r\nInterface    %s\r\n", pi1IfName);
        i4RetVal =
            nmhGetFsMIDhcpRelayIfCircuitId ((INT4) u4CxtId, i4IfIndex,
                                            &u4CircuitId);
        if ((i4RetVal != SNMP_FAILURE) && (u4CircuitId != 0))
        {
            CliPrintf (CliHandle, "Circuit ID : %d\r\n", u4CircuitId);
        }

        i4RetVal =
            nmhGetFsMIDhcpRelayIfRemoteId ((INT4) u4CxtId, i4IfIndex,
                                           &sOctetStrRemoteId);
        if ((i4RetVal != SNMP_FAILURE) && (sOctetStrRemoteId.i4_Length != 0)
            && (STRCMP (sOctetStrRemoteId.pu1_OctetList, "XYZ")))
        {
            CliPrintf (CliHandle, "Remote  ID : %s\r\n",
                       sOctetStrRemoteId.pu1_OctetList);
        }
        else
        {
            CliPrintf (CliHandle, "Remote  ID : None\r\n");
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : DhcpRelayShowIfInfo                                    */
/*                                                                           */
/* Description      : This function is invoked to display DHCP Relay if info */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
DhcpRelayShowIfInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE sOctetStrRemoteId;
    INT4                i4IfIndex;
    INT4                i4NextIfIndex;
    INT4                i4RowStatus;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4CircuitId;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;
    UINT1               au1RemoteId[DHRL_MAX_RID_LEN];
    INT4                i4CxtId;
    INT4                i4FirstCntxId;
    INT4                i4NextCntxId;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1RemoteId, 0, DHRL_MAX_RID_LEN);
    pi1IfName = &ai1IfName[0];
    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhGetFirstIndexFsMIDhcpRelayIfTable (&i4NextCntxId,
                                              &i4NextIfIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        MEMSET (&sOctetStrRemoteId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        sOctetStrRemoteId.pu1_OctetList = &au1RemoteId[0];
        i4IfIndex = i4NextIfIndex;
        i4FirstCntxId = i4NextCntxId;
        if (i4CxtId == i4FirstCntxId)
        {
            nmhGetFsMIDhcpRelayIfRowStatus (i4CxtId, i4IfIndex, &i4RowStatus);
            if (i4RowStatus == ACTIVE)
            {
                CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
                CliPrintf (CliHandle, "\r\n Interface  %s \r\n", pi1IfName);
                i4RetVal =
                    nmhGetFsMIDhcpRelayIfCircuitId (i4CxtId, i4IfIndex,
                                                    &u4CircuitId);
                if ((i4RetVal != SNMP_FAILURE) && (u4CircuitId != 0))
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "Circuit ID : %d\r\n",
                                   u4CircuitId);
                }
                else
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "Circuit ID : None\r\n");

                }

                i4RetVal =
                    nmhGetFsMIDhcpRelayIfRemoteId (i4CxtId, i4IfIndex,
                                                   &sOctetStrRemoteId);
                if ((i4RetVal != SNMP_FAILURE)
                    && (sOctetStrRemoteId.i4_Length != 0)
                    && (STRCMP (sOctetStrRemoteId.pu1_OctetList, "XYZ")))
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "Remote  ID : %s\r\n",
                                   sOctetStrRemoteId.pu1_OctetList);
                }
                else
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "Remote  ID : None\r\n");
                }

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User have pressed 'q' */
                    break;
                }
            }
        }

    }
    while (nmhGetNextIndexFsMIDhcpRelayIfTable (i4FirstCntxId, &i4NextCntxId,
                                                i4IfIndex,
                                                &i4NextIfIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : DhcpRelayShowInfo                                      */
/*                                                                           */
/* Description      : This function is invoked to display DHCP Relay info    */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpRelayShowInfo (tCliHandle CliHandle)
{
    INT4                i4Relaying = 0;
    INT4                i4RelaySrvOnly = 0;
    UINT4               u4RAIOptionCtrl = 0;
    UINT4               u4Trace = 0;
    UINT4               u4RAIOptions = 0;
    UINT4               u4CircuitId = 0;
    UINT4               u4RemoteId = 0;
    UINT4               u4SubnetOpts = 0;
    UINT4               u4WrongOpts = 0;
    UINT4               u4SpaceCnstr = 0;
    UINT1               au1TempString[32];
    CHR1               *pu1TempString;
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT4               u4CircuitFlag = 0;
    UINT4               u4ActMask = 0x1;
    INT4                i4CxtId;
    UINT4               u4Count = 1;
    UINT4               u4FirstServerIpAddr;
    UINT4               u4NextServerIpAddr;
    UINT1               au1String[32];
    CHR1               *pu1String;
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    INT4                i4FirstCntxId;
    INT4                i4NextCntxId;
    UINT4               u4RelayCountCxt = 0;

    MEMSET (au1String, 0, 32);
    MEMSET (au1TempString, 0, 32);
    pu1String = (CHR1 *) & au1String[0];
    pu1TempString = (CHR1 *) & au1TempString[0];
    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    VcmGetAliasName (i4CxtId, au1CxtName);
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "Context Name : %s\r\n", au1CxtName);
    CliPrintf (CliHandle, "------------\r\n");

    /* dhcp relay status */
    nmhGetFsMIDhcpRelaying (i4CxtId, &i4Relaying);
    /* dhcp servers only status */
    nmhGetFsMIDhcpRelayServersOnly (i4CxtId, &i4RelaySrvOnly);
    /* dhcp servers RAI option control */
    nmhGetFsMIDhcpRelayRAIOptionControl (i4CxtId, (INT4 *) &u4RAIOptionCtrl);
    /* debug level */
    nmhGetFsMIDhcpConfigTraceLevel (i4CxtId, (INT4 *) &u4Trace);
    /* no of packtes inserted RAI option */
    nmhGetFsMIDhcpRelayRAIOptionInserted (i4CxtId, &u4RAIOptions);
    /* no of packtes inserted circuit ID */
    nmhGetFsMIDhcpRelayRAICircuitIDSubOptionInserted (i4CxtId, &u4CircuitId);
    /* no of packtes inserted remote ID */
    nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionInserted (i4CxtId, &u4RemoteId);
    /* no of packtes inserted subnetmask */
    nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionInserted (i4CxtId, &u4SubnetOpts);
    /* no of packtes dropped */
    nmhGetFsMIDhcpRelayRAIOptionWronglySet (i4CxtId, &u4WrongOpts);
    /* no of packtes which not inserted RAI option */
    nmhGetFsMIDhcpRelayRAISpaceConstraint (i4CxtId, &u4SpaceCnstr);
    /* Get the default type present in circuit Id field */
    pOctStrFlag = allocmem_octetstring (DHCP_CIRCUIT_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }
    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);
    nmhGetFsMIDhcpConfigDhcpCircuitOption (i4CxtId, pOctStrFlag);
    MEMCPY (&u4CircuitFlag, pOctStrFlag->pu1_OctetList, pOctStrFlag->i4_Length);
    /* Get the Action Flag */
    /* The circuit type value is always stored in the first byte of the 
     * string. Possible values are router-index (1), vlanid (2) and lagport (3) 
     */
    free_octetstring (pOctStrFlag);

    /* TO DO: For more than 8 bits,u4ActMask has to be updated */
    while (u4ActMask < 0xFF)
    {
        switch ((u4CircuitFlag & u4ActMask))
        {
            case DHCP_CIRCUITID_TYPE_INTERFACE_INDEX:
                SPRINTF ((CHR1 *) pu1TempString, "%s, ", "router-index");
                pu1TempString = pu1TempString + (STRLEN (pu1TempString));
                break;
            case DHCP_CIRCUITID_TYPE_VLANID:
                SPRINTF ((CHR1 *) pu1TempString, "%s, ", "VlanId");
                pu1TempString = pu1TempString + (STRLEN (pu1TempString));
                break;
            case DHCP_CIRCUITID_TYPE_PHYPORT_LAGPORT:
                SPRINTF ((CHR1 *) pu1TempString, "%s, ", "recv-port");
                pu1TempString = pu1TempString + (STRLEN (pu1TempString));
                break;
            default:
                break;

        }
        u4ActMask = (UINT4) (u4ActMask << 1);
    }
    if (*(pu1TempString - 2) == ',')
    {
        *(pu1TempString - 2) = '\0';
    }

    CliPrintf (CliHandle, "\r\n");
    switch (i4Relaying)
    {
        case 1:
            CliPrintf (CliHandle,
                       "\rDhcp Relay                  : Enabled\r\n");
            u4RelayCountCxt++;
            break;
        case 2:
            CliPrintf (CliHandle,
                       "\rDhcp Relay                  : Disabled\r\n");
            break;
        default:
            CliPrintf (CliHandle,
                       "\rDhcp Relay Status           : Unknown\r\n");
            break;
    }

    switch (i4RelaySrvOnly)
    {
        case 1:
            CliPrintf (CliHandle, "Dhcp Relay Servers only     : Enabled\r\n");
            break;
        case 2:
            CliPrintf (CliHandle, "Dhcp Relay Servers only     : Disabled\r\n");
            break;
        default:
            CliPrintf (CliHandle, "Dhcp Relay Servers only     : Unknown\r\n");
            break;
    }

    /* server ip addresses */

    if (nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable (&i4FirstCntxId,
                                                      &u4FirstServerIpAddr) ==
        SNMP_SUCCESS)
    {
        i4NextCntxId = i4FirstCntxId;
        u4NextServerIpAddr = u4FirstServerIpAddr;

        do
        {
            i4FirstCntxId = i4NextCntxId;
            u4FirstServerIpAddr = u4NextServerIpAddr;
            if (i4FirstCntxId == i4CxtId)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4FirstServerIpAddr);
                CliPrintf (CliHandle, "\r\nDHCP server %d:  %-17s",
                           u4Count, pu1String);
                u4Count++;
            }

        }
        while (nmhGetNextIndexFsMIDhcpRelaySrvAddressTable
               (i4FirstCntxId, &i4NextCntxId, u4FirstServerIpAddr,
                &u4NextServerIpAddr) == SNMP_SUCCESS);

        CliPrintf (CliHandle, "\r\n\r\n");
    }
    else
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, 0);
        CliPrintf (CliHandle, "\r\nDHCP server                 : %-17s\r\n\r\n",
                   pu1String);
    }

    switch (u4RAIOptionCtrl)
    {
        case 1:
            CliPrintf (CliHandle, "Dhcp Relay RAI option       : Enabled\r\n");
            break;
        case 2:
            CliPrintf (CliHandle, "Dhcp Relay RAI option       : Disabled\r\n");
            break;
        default:
            CliPrintf (CliHandle, "Dhcp Relay RAI option Status: Unknown\r\n");
            break;
    }

    CliPrintf (CliHandle, "Default Circuit Id information : %s \r\n",
               au1TempString);

    CliPrintf (CliHandle, "Debug Level                 : 0x%x \r\n", u4Trace);

    CliPrintf (CliHandle,
               "\r\nNo of Packets inserted RAI option               : %d \r\n",
               u4RAIOptions);

    CliPrintf (CliHandle,
               "No of Packets inserted circuit ID suboption     : %d \r\n",
               u4CircuitId);

    CliPrintf (CliHandle,
               "No of Packets inserted remote ID suboption      : %d \r\n",
               u4RemoteId);

    CliPrintf (CliHandle,
               "No of Packets inserted subnet mask suboption    : %d \r\n",
               u4SubnetOpts);

    CliPrintf (CliHandle,
               "No of Packets dropped                           : %d \r\n",
               u4WrongOpts);

    CliPrintf (CliHandle,
               "No of Packets which did not inserted RAI option : %d \r\n\r\n",
               u4SpaceCnstr);
    CliPrintf (CliHandle, "Total Relay Agents in %s : %d \r\n", au1CxtName,
               u4RelayCountCxt);

    DhcpRelayShowIfInfo (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : DhcpRelayShowServer                                    */
/*                                                                           */
/* Description      : This function is invoked to display DHCP server        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpRelayShowServer (tCliHandle CliHandle)
{

    /*This Function is modified to show the multiple servers configured in Relay */
    UINT4               u4Count = 1;
    UINT4               u4FirstServerIpAddr;
    UINT4               u4NextServerIpAddr;
    UINT1               au1String[32];
    CHR1               *pu1String;
    UINT4               u4Context;
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];
    INT4                i4FirstCntxId;
    INT4                i4NextCntxId;
    pu1String = (CHR1 *) & au1String[0];
    u4Context = gGlobalRlyNode.u4CurrCxtId;

    VcmGetAliasName (u4Context, au1CxtName);
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "Context Name : %s\r\n", au1CxtName);
    CliPrintf (CliHandle, "------------\r\n");

    if (nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable (&i4FirstCntxId,
                                                      &u4FirstServerIpAddr) ==
        SNMP_SUCCESS)
    {
        i4NextCntxId = i4FirstCntxId;
        u4NextServerIpAddr = u4FirstServerIpAddr;

        do
        {
            i4FirstCntxId = i4NextCntxId;
            u4FirstServerIpAddr = u4NextServerIpAddr;
            if (i4FirstCntxId == (INT4) u4Context)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4FirstServerIpAddr);
                CliPrintf (CliHandle, "\r\nDHCP server %d:  %-17s",
                           u4Count, pu1String);
                u4Count++;
            }

        }
        while (nmhGetNextIndexFsMIDhcpRelaySrvAddressTable
               (i4FirstCntxId, &i4NextCntxId, u4FirstServerIpAddr,
                &u4NextServerIpAddr) == SNMP_SUCCESS);

        CliPrintf (CliHandle, "\r\n\r\n");
    }
    else
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, 0);
        CliPrintf (CliHandle, "\r\nDHCP server                 : %-17s\r\n\r\n",
                   pu1String);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : DhcpRelayShowRunningConfig                             */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP Relay                                             */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
DhcpRelayShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4RetVal = 0;
    INT4                i4Relaying = 0;
    INT4                i4FirstIndex = 0;
    INT4                i4RAIOptionCtrl = 0;
    INT4                i4RAICircuitCtrl = 0;
    INT4                i4RAIRemoteCtrl = 0;
    INT4                i4RAISubMaskCtrl = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetValDhcpRelayCounterReset = 0;
    CHR1               *pc1String;
    UINT4               u4FirstServerIpAddr = 0;
    UINT4               u4NextServerIpAddr = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1VrfName[VCM_ALIAS_MAX_LEN];
    INT1               *pi1IfName = NULL;
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT4               u4CircuitFlag = 0;
    UINT4               u4ActMask = 0x1;
    UINT4               u4PrevId = DHCP_RLY_DFLT_CXT_ID;
    UINT4               u4VcId = DHCP_RLY_DFLT_CXT_ID;
    INT4                i4FirstCntxid = 0;
    INT4                i4NextCntxid = 0;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = &ai1IfName[0];

    CliRegisterLock (CliHandle, DhcpRProtocolLock, DhcpRProtocolUnlock);
    DHCPR_PROTO_LOCK ();

    if (DhcpRlyGetFirstActiveContextID (&u4VcId) == DHCP_FAILURE)
    {
        /* DHCP Relay has not been enabled in any context */
        DHCPR_PROTO_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    do
    {
        u4PrevId = u4VcId;
        MEMSET (au1VrfName, 0, VCM_ALIAS_MAX_LEN);
        if (VcmGetAliasName (u4PrevId, au1VrfName) == VCM_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nend\r\n");
        IssDhcpRelayShowDebugging (CliHandle, (INT4) u4PrevId);
        CliPrintf (CliHandle, "\r\n!\r\n");

        nmhGetFsMIDhcpRelaying ((INT4) u4PrevId, &i4Relaying);
        if (i4Relaying != DHCP_DEF_RELAYING)
        {
            if (u4PrevId != DHCP_RLY_DFLT_CXT_ID)
            {
                CliPrintf (CliHandle, "service dhcp-relay vrf %s \r\n",
                           au1VrfName);
            }
            else
            {
                CliPrintf (CliHandle, "service dhcp-relay\r\n");
            }
        }

        i4FirstIndex =
            nmhGetFirstIndexFsMIDhcpRelaySrvAddressTable (&i4FirstCntxid,
                                                          &u4FirstServerIpAddr);

        if (i4FirstIndex == SNMP_SUCCESS)
        {
            i4NextCntxid = i4FirstCntxid;
            u4NextServerIpAddr = u4FirstServerIpAddr;
            do
            {
                u4FirstServerIpAddr = u4NextServerIpAddr;
                i4FirstCntxid = i4NextCntxid;
                if (i4FirstCntxid == (INT4) u4PrevId)
                {
                    nmhGetFsMIDhcpRelaySrvAddressRowStatus (i4FirstCntxid,
                                                            u4FirstServerIpAddr,
                                                            &i4RetVal);
                    if (i4RetVal == ACTIVE)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pc1String,
                                                   u4FirstServerIpAddr);

                        if (u4PrevId != DHCP_RLY_DFLT_CXT_ID)
                        {
                            CliPrintf (CliHandle,
                                       "ip dhcp server vrf %s %s \r\n",
                                       au1VrfName, pc1String);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "ip dhcp server %s\r\n",
                                       pc1String);
                        }
                    }
                }

            }
            while (nmhGetNextIndexFsMIDhcpRelaySrvAddressTable
                   (i4FirstCntxid, &i4NextCntxid, u4FirstServerIpAddr,
                    &u4NextServerIpAddr) == SNMP_SUCCESS);

        }

        nmhGetFsMIDhcpRelayRAIOptionControl ((INT4) u4PrevId, &i4RAIOptionCtrl);
        nmhGetFsMIDhcpRelayRAICircuitIDSubOptionControl ((INT4) u4PrevId,
                                                         &i4RAICircuitCtrl);
        nmhGetFsMIDhcpRelayRAIRemoteIDSubOptionControl ((INT4) u4PrevId,
                                                        &i4RAIRemoteCtrl);
        nmhGetFsMIDhcpRelayRAISubnetMaskSubOptionControl ((INT4) u4PrevId,
                                                          &i4RAISubMaskCtrl);

        if ((i4RAIOptionCtrl != DHCP_DEF_RAI_OPTIONCTRL) &&
            (i4RAICircuitCtrl != DHCP_DEF_RAI_OPTIONCTRL) &&
            (i4RAIRemoteCtrl != DHCP_DEF_RAI_OPTIONCTRL) &&
            (i4RAISubMaskCtrl != DHCP_DEF_RAI_OPTIONCTRL))
        {
            if (u4PrevId != DHCP_RLY_DFLT_CXT_ID)
            {
                CliPrintf (CliHandle,
                           "ip dhcp relay information option vrf %s \r\n",
                           au1VrfName);
            }
            else
            {
                CliPrintf (CliHandle, "ip dhcp relay information option\r\n");
            }

            pOctStrFlag =
                allocmem_octetstring (DHCP_CIRCUIT_ACTION_FLAG_LENGTH);
            if (pOctStrFlag == NULL)
            {
                return (CLI_FAILURE);
            }
            MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);
            nmhGetFsMIDhcpConfigDhcpCircuitOption ((INT4) u4PrevId,
                                                   pOctStrFlag);
            /* Get the Action Flag */
            MEMCPY (&u4CircuitFlag, pOctStrFlag->pu1_OctetList,
                    pOctStrFlag->i4_Length);
            free_octetstring (pOctStrFlag);
            if (DHCP_RLY_CXT_NODE[u4PrevId]->u4CfRAICktIDDefaultOptiontype !=
                DHCP_CIRCUITID_TYPE_INTERFACE_INDEX)
            {
                if (u4PrevId != DHCP_RLY_DFLT_CXT_ID)
                {
                    CliPrintf (CliHandle,
                               "ip dhcp relay circuit-id option vrf %s ",
                               au1VrfName);
                }
                else
                {
                    CliPrintf (CliHandle, "ip dhcp relay circuit-id option ");
                }

                /* TO DO: For more than 8 bits,u4ActMask has to be updated */
                while (u4ActMask < 0xFFFF)
                {
                    switch ((u4CircuitFlag & u4ActMask))
                    {
                        case DHCP_CIRCUITID_TYPE_INTERFACE_INDEX:
                            CliPrintf (CliHandle, "router-index ");
                            break;
                        case DHCP_CIRCUITID_TYPE_VLANID:
                            CliPrintf (CliHandle, "vlanid ");
                            break;
                        case DHCP_CIRCUITID_TYPE_PHYPORT_LAGPORT:
                            CliPrintf (CliHandle, "recv-port ");
                            break;
                        default:
                            break;

                    }
                    u4ActMask = (UINT4) (u4ActMask << 1);
                }

                CliPrintf (CliHandle, "\r\n");
                u4ActMask = 0x1;
            }
        }

        nmhGetFsMIDhcpRelayCounterReset ((INT4) u4PrevId,
                                         &i4RetValDhcpRelayCounterReset);

        if (i4RetValDhcpRelayCounterReset == DHCP_RESET)
        {
            CliPrintf (CliHandle, "end\r\n");
            CliPrintf (CliHandle, "clear ip dhcp relay statistics\r\n");
            CliPrintf (CliHandle, "!\r\n");
        }

        if (u4Module == ISS_DHCP_SHOW_RUNNING_CONFIG)
        {
            i4FirstCntxid = 0;
            i4NextCntxid = 0;

            if (nmhGetFirstIndexFsMIDhcpRelayIfTable (&i4NextCntxid,
                                                      &i4NextIfIndex) ==
                SNMP_FAILURE)
            {
                continue;
            }

            do
            {
                i4IfIndex = i4NextIfIndex;
                i4FirstCntxid = i4NextCntxid;
                if (i4FirstCntxid == (INT4) u4PrevId)
                {
                    nmhGetFsMIDhcpRelayIfRowStatus (i4FirstCntxid,
                                                    i4IfIndex, &i4RowStatus);
                    if (i4RowStatus == ACTIVE)
                    {
                        CfaCliGetIfName ((UINT4) i4IfIndex, pi1IfName);
                        u4PagingStatus = CliPrintf (CliHandle, "\r");
                        if (u4PagingStatus == CLI_FAILURE)
                        {
                            /* User have pressed 'q'. */
                            break;
                        }
                        DHCPR_PROTO_UNLOCK ();
                        CliUnRegisterLock (CliHandle);

                        DhrlShowRunningConfigInterfaceDetails (CliHandle,
                                                               i4FirstCntxid,
                                                               i4IfIndex);

                        CliRegisterLock (CliHandle, DhcpRProtocolLock,
                                         DhcpRProtocolUnlock);
                        DHCPR_PROTO_LOCK ();
                    }
                }
            }
            while (nmhGetNextIndexFsMIDhcpRelayIfTable (i4FirstCntxid,
                                                        &i4NextCntxid,
                                                        i4IfIndex,
                                                        &i4NextIfIndex) ==
                   SNMP_SUCCESS);
        }
    }
    while (DhcpRlyGetNextActiveContextID (u4PrevId, &u4VcId) == SNMP_SUCCESS);

    DHCPR_PROTO_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : DhrlShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface configuration     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
DhrlShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4CxtId,
                                       INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE sOctetStrRemoteId;
    INT4                i4RowStatus;
    UINT4               u4CircuitId;
    UINT1               au1RemoteId[DHRL_MAX_RID_LEN];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = &ai1IfName[0];

    MEMSET (&au1RemoteId[0], 0, DHRL_MAX_RID_LEN);
    MEMSET (&sOctetStrRemoteId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    sOctetStrRemoteId.pu1_OctetList = &au1RemoteId[0];

    CliRegisterLock (CliHandle, DhcpRProtocolLock, DhcpRProtocolUnlock);
    DHCPR_PROTO_LOCK ();

    if (nmhGetFsMIDhcpRelayIfRowStatus (i4CxtId,
                                        i4IfIndex,
                                        &i4RowStatus) == SNMP_SUCCESS)
    {
        if (i4RowStatus == ACTIVE)
        {
            nmhGetFsMIDhcpRelayIfCircuitId (i4CxtId, i4IfIndex, &u4CircuitId);
            nmhGetFsMIDhcpRelayIfRemoteId (i4CxtId, i4IfIndex,
                                           &sOctetStrRemoteId);
            CfaCliConfGetIfName ((UINT4) i4IfIndex, pi1IfName);

            if ((u4CircuitId != 0) ||
                ((sOctetStrRemoteId.i4_Length != 0) &&
                 (STRCMP (sOctetStrRemoteId.pu1_OctetList, "XYZ"))))
            {
                CliPrintf (CliHandle, "interface %s\r\n", pi1IfName);
            }

            if (u4CircuitId != 0)
            {
                CliPrintf (CliHandle, " ip dhcp relay circuit-id %d\r\n",
                           u4CircuitId);
            }

            /* Test if the id is same as the internal default id */
            if ((sOctetStrRemoteId.i4_Length != 0) &&
                (STRCMP (sOctetStrRemoteId.pu1_OctetList, "XYZ")))
            {
                CliPrintf (CliHandle, " ip dhcp relay remote-id \"%s\"\r\n",
                           sOctetStrRemoteId.pu1_OctetList);
            }
            if ((u4CircuitId != 0) ||
                ((sOctetStrRemoteId.i4_Length != 0) &&
                 (STRCMP (sOctetStrRemoteId.pu1_OctetList, "XYZ"))))
            {
                CliPrintf (CliHandle, "!\r\n");
            }
        }
    }
    DHCPR_PROTO_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssDhcpRelayShowDebugging                          */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DHCP relay  debug level   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssDhcpRelayShowDebugging (tCliHandle CliHandle, INT4 i4CxtId)
{

    INT4                i4DbgLevel = 0;
    UINT1               au1CxtName[VCMALIAS_MAX_ARRAY_LEN];

    MEMSET (au1CxtName, 0, VCMALIAS_MAX_ARRAY_LEN);

    nmhGetFsMIDhcpConfigTraceLevel (i4CxtId, &i4DbgLevel);

    if (VcmGetAliasName ((UINT4) i4CxtId, au1CxtName) == VCM_FAILURE)
    {
        return;
    }

    if (i4DbgLevel == DHCP_RLY_DEFAULT_TRACE_LEVEL)
    {
        return;
    }

    if (i4DbgLevel == DHCPR_DEBUG_ALL)
    {
        if (i4CxtId == DHCP_RLY_DFLT_CXT_ID)
        {
            CliPrintf (CliHandle, " debug ip dhcp relay all\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " debug ip dhcp relay vrf %s all\r\n",
                       au1CxtName);
        }
    }
    else if ((i4DbgLevel & DHCPR_DEBUG_ERRORS) != 0)
    {
        if (i4CxtId == DHCP_RLY_DFLT_CXT_ID)
        {
            CliPrintf (CliHandle, " debug ip dhcp relay errors\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " debug ip dhcp relay vrf %s errors\r\n",
                       au1CxtName);
        }
    }

    return;

}

/******************************************************************************/
/* Function Name     : DhcpRelaySetClearCounters                              */
/*                                                                            */
/* Description       : This function clears the dhcp relay counters.          */
/*                                                                            */
/* Input Parameters  : CliHandle                                              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpRelaySetClearCounters (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;
    INT4                i4CxtId;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    i4CxtId = (INT4) gGlobalRlyNode.u4CurrCxtId;

    if (nmhGetFsMIDhcpRelayContextRowStatus (i4CxtId, &i4RowStatus) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_DHCPR_NO_RELAY);
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                    NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMIDhcpRelayCounterReset (&u4ErrCode, i4CxtId,
                                            DHCP_RESET) == SNMP_SUCCESS)
    {
        if (nmhSetFsMIDhcpRelayCounterReset (i4CxtId, DHCP_RESET) ==
            SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Relay Counter reSet failed\r\n");
            i4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsMIDhcpRelayContextRowStatus (&u4ErrCode, i4CxtId,
                                                    i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus test failed\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMIDhcpRelayContextRowStatus (i4CxtId, i4RowStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    return i4RetVal;

}

#endif
