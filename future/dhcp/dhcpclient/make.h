include ../../LR/make.h
include ../../LR/make.rule
DHCPCLIENT = ${BASE_DIR}/dhcp/dhcpclient

DSRC=$(DHCPCLIENT)/src/
DOBJ=$(DHCPCLIENT)/obj/
DINC=$(DHCPCLIENT)/inc/
DLRINC=-I$(BASE_DIR)/inc/ \
	-I$(BASE_DIR)/cfa2/inc/

DHCP_INCLUDES=$(COMMON_INCLUDE_DIRS) $(DLRINC) -I$(DINC)

DHCP_FLAGS =-UALL_TRACE_WANTED
        
DHCP_FLAGS += $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
 
DHCP_FLAGS += $(CC_FLAGS)

FINAL_DHCP_CLIENT_OBJ=$(DOBJ)FutureDhcpC.o
