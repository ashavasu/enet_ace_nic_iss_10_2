/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                           *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                                        *
 * $Id: dhcport.h,v 1.30 2017/01/25 13:18:59 siva Exp $
 * FILE NAME      : dhcpcport.h                                          *
 * LANGUAGE       : C Language                                           *
 * TARGET         : LINUX                                                *
 * AUTHOR         : A.S.Musthafa                                         *
 * DESCRIPTION    : Portable vaiables and macros used for DHCP CLIENT    *
 *                  module                                               *
 ************************************************************************/
#ifndef  __DHCPC_PORT_H
#define  __DHCPC_PORT_H

/* Task and event related constants */
#define  DHCP_SELECT_CLINET_TASK_NAME       (UINT1 *)"DCS"
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
#define  DHCP_CLIENT_ARP_TASK_NAME          (UINT1 *)"DARP"
#endif
#define  DHCP_CLIENT_TASK_PRIORITY          25
#define  DHCP_CLIENT_TASK_MODE              OSIX_DEFAULT_TASK_MODE

/* Event IDs */
#define  DHCPC_TIMER_EXPIRY             0x00000002
#define  DHCPC_MSGQ_IF_EVENT            0x00000004
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
#define  DHCPC_MSGQ_ARP                 0x00000008
#endif
#define  DHCPC_EVENT_MASK               0xffffffff

/* Macros used for sending the arp messages */
#define CRU_BUF_VALID_ARP_DATA_SIZE        15
#define CRU_BUF_ARP_SIZE                   30
#define DHCP_CLIENTID_LEN                  32

/* Macros used for adding a static route for sending the decline message */
#define DHCP_CIDR_ROUTE_ACTIVE             1
#define DHCP_CIDR_ROUTE_METRIC             2
#define DHCP_DEF_ROUTE_METRIC              1
#define DHCP_CIDR_ROUTE_TYPE               1
#define DHCP_CIDR_ROUTE_ADD_WAIT_TIMER_VAL 6
#define DHCP_CIDR_ROUTE_DELETE             6
#define DHCP_CIDR_ROUTE_PROTO              3
#define DHCP_CIDR_ROUTE_TOS                0
#define DHCPC_DEF_ROUTE_ADDRESS            0
#define DHCPC_DEF_ROUTE_MASK               0

#define DHCP_ALLOCATE_BUF                  CRU_BUF_Allocate_MsgBufChain
#define DHCP_COPY_TO_BUF                   CRU_BUF_Copy_OverBufChain
#define DHCP_RELEASE_BUF                   CRU_BUF_Release_MsgBufChain 
#define DHCP_GET_MODULE_DATA_PTR(pBuf)     &(pBuf->ModuleData)


/* The maximum timer value */
#define DHCPC_MAX_TIME_OUT             604799
 /* 7 days = 604800 seconds  - Max time supported by FSAP Timer*/


/* The maximum number of interfaces supported */
#define DHCPC_MAX_INTERFACE             IPIF_MAX_LOGICAL_IFACES

#define DHCPCIF_HASH_INDEX(Index)  Index % DHCPC_MAX_INTERFACE

/* Need to convert to secs from ticks recieved */ 
#define DHCPC_GET_TIME_IN_SECS(u4CurrentTime)\
    OsixGetSysTime (&u4CurrentTime);\
    u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

#define VALUE_IS_INVALID  0
#define VALUE_IS_IP_ADDR  1
#define VALUE_IS_MAC_ADDR 2
#define VALUE_IS_NUMBER_ARRAY 3

/* DHCP Client default options */ 
#define DHCPC_DEFAULT_NET_MASK_OPT  (CONST UINT1 *)"255.0.0.0"
#define DHCPC_DEFAULT_ROUTER_OPT    (CONST UINT1 *)"10.1.2.3"
#define DHCPC_DEFAULT_DNS_OPT       (CONST UINT1 *)"20.1.2.3"
#define DHCPC_DEFAULT_DNS_NAME      (CONST UINT1 *)"com"

#define DHCPC_SEED                         OSIX_SEED
#define DHCPC_SRAND                        OSIX_SRAND

#define DHCPC_SINGLE_OFFER_TIMER_ID        1
#define DHCPC_MULTIPLE_OFFER_TIMER_ID    2
#define DHCPC_REQUEST_TIMER_ID            3
#define DHCPC_RENEWAL_TIMER_ID            4
#define DHCPC_REBIND_TIMER_ID            5
#define DHCPC_ARP_CHECK_TIMER_ID        6
#define DHCPC_INFORM_MSG_TIMER_ID        7
#define DHCPC_DECLINE_MSG_TIMER_ID        8
#define DHCPC_RELEASE_MSG_TIMER_ID        9
#define DHCPC_NULL_STATE_TIMER_ID        10
#define BOOTP_REQUEST_TIMER_ID           11

/* 
 * 
 * DHCP_TRC# macros - mapped to UtlTrc(); 
 * 
 * */
#define  ENABLE_ALL_TRC   0x0000ffff       
#define  DISABLE_ALL_TRC  0                
#define  DEBUG_TRC        0x00000008
#define  DEFAULT_TRC      FAIL_TRC

#define  DHCPC_NAME        "DHCP CLIENT" /* name printed with every message
                                            DHCP CLIENT: Your message */
#define  DHCPC_MASK (UINT4) gu4DhcpCDebugMask 

#define DHCPC_TRC(mask,fmt)\
MOD_TRC(DHCPC_MASK,mask,DHCPC_NAME,fmt)
#define DHCPC_TRC1(mask,fmt,arg1)\
MOD_TRC_ARG1(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1)
#define DHCPC_TRC2(mask,fmt,arg1,arg2)\
MOD_TRC_ARG2(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1,arg2)
#define DHCPC_TRC3(mask,fmt,arg1,arg2,arg3)\
MOD_TRC_ARG3(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1,arg2,arg3)
#define DHCPC_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
     MOD_TRC_ARG4(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1,arg2,arg3,arg4)
#define DHCPC_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
MOD_TRC_ARG5(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1,arg2,arg3,arg4,arg5)
#define DHCPC_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
MOD_TRC_ARG6(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#ifdef DNS_RELAY_WANTED
extern INT1 nmhSetDnsRelayDefaultNameServerIpAddr (UINT4);
extern INT1 nmhSetDnsRelayDefaultNameServerDomainName (tSNMP_OCTET_STRING_TYPE * );
#endif

VOID DhcpCGetTransId (UINT1 *,UINT4 *);
INT4 DhcpCValidateIfIpAndMask ( UINT4 u4IpAddr, UINT4 u4NetMask);
INT4 DhcpCValidateClientMac ( UINT4 u4Port, UINT1 *pHwAddr);
#define dhcpc_fd_set fd_set

#define DHCPC_FAST_ACC_ENABLE 1
#define DHCPC_FAST_ACC_DISABLE 2
#define DHCPC_FAST_ACC_ARP_CHECK_MIN_TIMEOUT 1
#define DHCPC_FAST_ACC_ARP_CHECK_MAX_TIMEOUT 20
#define DHCPC_FAST_ACC_ARP_CHECK_TIMEOUT DHCPC_FAST_ACC_ARP_CHECK_MIN_TIMEOUT

#define DHCPC_FAST_ACC_NULL_ST_TIMEOUT 1
#define DHCPC_FAST_ACC_DISCOVERY_TIME_OUT 5

#define DHCPC_FAST_ACC_DISCOVERY_MIN_TIMEOUT 1
#define DHCPC_FAST_ACC_NULL_ST_MAX_TIMEOUT 300
#define DHCPC_FAST_ACC_DISCOVERY_MAX_TIMEOUT 300

#endif /* __DHCPC_PORT_H */
