/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdhcldb.h,v 1.9 2014/03/11 13:14:40 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDHCLDB_H
#define _FSDHCLDB_H

UINT1 DhcpClientConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DhcpClientOptTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 DhcpClientCounterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsdhcl [] ={1,3,6,1,4,1,2076,87};
tSNMP_OID_TYPE fsdhclOID = {8, fsdhcl};


UINT4 DhcpClientConfigIfIndex [ ] ={1,3,6,1,4,1,2076,87,1,1,1,1};
UINT4 DhcpClientRenew [ ] ={1,3,6,1,4,1,2076,87,1,1,1,2};
UINT4 DhcpClientRebind [ ] ={1,3,6,1,4,1,2076,87,1,1,1,3};
UINT4 DhcpClientInform [ ] ={1,3,6,1,4,1,2076,87,1,1,1,4};
UINT4 DhcpClientRelease [ ] ={1,3,6,1,4,1,2076,87,1,1,1,5};
UINT4 DhcpClientIdentifier [ ] ={1,3,6,1,4,1,2076,87,1,1,1,6};
UINT4 DhcpClientDebugTrace [ ] ={1,3,6,1,4,1,2076,87,1,2};
UINT4 DhcpClientOptIfIndex [ ] ={1,3,6,1,4,1,2076,87,1,3,1,1};
UINT4 DhcpClientOptType [ ] ={1,3,6,1,4,1,2076,87,1,3,1,2};
UINT4 DhcpClientOptLen [ ] ={1,3,6,1,4,1,2076,87,1,3,1,3};
UINT4 DhcpClientOptVal [ ] ={1,3,6,1,4,1,2076,87,1,3,1,4};
UINT4 DhcpClientOptRowStatus [ ] ={1,3,6,1,4,1,2076,87,1,3,1,5};
UINT4 DhcpClientIfIndex [ ] ={1,3,6,1,4,1,2076,87,2,1,1,1};
UINT4 DhcpClientCountDiscovers [ ] ={1,3,6,1,4,1,2076,87,2,1,1,2};
UINT4 DhcpClientCountRequests [ ] ={1,3,6,1,4,1,2076,87,2,1,1,3};
UINT4 DhcpClientCountReleases [ ] ={1,3,6,1,4,1,2076,87,2,1,1,4};
UINT4 DhcpClientCountDeclines [ ] ={1,3,6,1,4,1,2076,87,2,1,1,5};
UINT4 DhcpClientCountInforms [ ] ={1,3,6,1,4,1,2076,87,2,1,1,6};
UINT4 DhcpClientCountOffers [ ] ={1,3,6,1,4,1,2076,87,2,1,1,7};
UINT4 DhcpCountAcksInReqState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,8};
UINT4 DhcpCountNacksInReqState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,9};
UINT4 DhcpCountAcksInRenewState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,10};
UINT4 DhcpCountNacksInRenewState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,11};
UINT4 DhcpCountAcksInRebindState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,12};
UINT4 DhcpCountNacksInRebindState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,13};
UINT4 DhcpCountAcksInRebootState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,14};
UINT4 DhcpCountNacksInRebootState [ ] ={1,3,6,1,4,1,2076,87,2,1,1,15};
UINT4 DhcpCountErrorInHeader [ ] ={1,3,6,1,4,1,2076,87,2,1,1,16};
UINT4 DhcpCountErrorInXid [ ] ={1,3,6,1,4,1,2076,87,2,1,1,17};
UINT4 DhcpCountErrorInOptions [ ] ={1,3,6,1,4,1,2076,87,2,1,1,18};
UINT4 DhcpClientIpAddress [ ] ={1,3,6,1,4,1,2076,87,2,1,1,19};
UINT4 DhcpClientLeaseTime [ ] ={1,3,6,1,4,1,2076,87,2,1,1,20};
UINT4 DhcpClientCounterReset [ ] ={1,3,6,1,4,1,2076,87,2,1,1,21};
UINT4 DhcpClientRemainLeaseTime [ ] ={1,3,6,1,4,1,2076,87,2,1,1,22};
UINT4 DhcpClientFastAccess [ ] ={1,3,6,1,4,1,2076,87,1,4};
UINT4 DhcpClientFastAccessDiscoverTimeOut [ ] ={1,3,6,1,4,1,2076,87,1,5};
UINT4 DhcpClientFastAccessNullStateTimeOut [ ] ={1,3,6,1,4,1,2076,87,1,6};
UINT4 DhcpClientFastAccessArpCheckTimeOut [ ] ={1,3,6,1,4,1,2076,87,1,7};




tMbDbEntry fsdhclMibEntry[]= {

{{12,DhcpClientConfigIfIndex}, GetNextIndexDhcpClientConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpClientConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientRenew}, GetNextIndexDhcpClientConfigTable, DhcpClientRenewGet, DhcpClientRenewSet, DhcpClientRenewTest, DhcpClientConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpClientConfigTableINDEX, 1, 0, 0, "2"},

{{12,DhcpClientRebind}, GetNextIndexDhcpClientConfigTable, DhcpClientRebindGet, DhcpClientRebindSet, DhcpClientRebindTest, DhcpClientConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpClientConfigTableINDEX, 1, 0, 0, "2"},

{{12,DhcpClientInform}, GetNextIndexDhcpClientConfigTable, DhcpClientInformGet, DhcpClientInformSet, DhcpClientInformTest, DhcpClientConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpClientConfigTableINDEX, 1, 0, 0, "2"},

{{12,DhcpClientRelease}, GetNextIndexDhcpClientConfigTable, DhcpClientReleaseGet, DhcpClientReleaseSet, DhcpClientReleaseTest, DhcpClientConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpClientConfigTableINDEX, 1, 0, 0, "2"},

{{12,DhcpClientIdentifier}, GetNextIndexDhcpClientConfigTable, DhcpClientIdentifierGet, DhcpClientIdentifierSet, DhcpClientIdentifierTest, DhcpClientConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpClientConfigTableINDEX, 1, 0, 0, NULL},

{{10,DhcpClientDebugTrace}, NULL, DhcpClientDebugTraceGet, DhcpClientDebugTraceSet, DhcpClientDebugTraceTest, DhcpClientDebugTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0 , "0"},

{{12,DhcpClientOptIfIndex}, GetNextIndexDhcpClientOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpClientOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpClientOptType}, GetNextIndexDhcpClientOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpClientOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpClientOptLen}, GetNextIndexDhcpClientOptTable, DhcpClientOptLenGet, DhcpClientOptLenSet, DhcpClientOptLenTest, DhcpClientOptTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DhcpClientOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpClientOptVal}, GetNextIndexDhcpClientOptTable, DhcpClientOptValGet, DhcpClientOptValSet, DhcpClientOptValTest, DhcpClientOptTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpClientOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpClientOptRowStatus}, GetNextIndexDhcpClientOptTable, DhcpClientOptRowStatusGet, DhcpClientOptRowStatusSet, DhcpClientOptRowStatusTest, DhcpClientOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpClientOptTableINDEX, 2, 0, 1, NULL},

{{10,DhcpClientFastAccess}, NULL, DhcpClientFastAccessGet, DhcpClientFastAccessSet, DhcpClientFastAccessTest, DhcpClientFastAccessDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DhcpClientFastAccessDiscoverTimeOut}, NULL, DhcpClientFastAccessDiscoverTimeOutGet, DhcpClientFastAccessDiscoverTimeOutSet, DhcpClientFastAccessDiscoverTimeOutTest, DhcpClientFastAccessDiscoverTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DhcpClientFastAccessNullStateTimeOut}, NULL, DhcpClientFastAccessNullStateTimeOutGet, DhcpClientFastAccessNullStateTimeOutSet, DhcpClientFastAccessNullStateTimeOutTest, DhcpClientFastAccessNullStateTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DhcpClientFastAccessArpCheckTimeOut}, NULL, DhcpClientFastAccessArpCheckTimeOutGet, DhcpClientFastAccessArpCheckTimeOutSet, DhcpClientFastAccessArpCheckTimeOutTest, DhcpClientFastAccessArpCheckTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0,NULL},

{{12,DhcpClientIfIndex}, GetNextIndexDhcpClientCounterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCountDiscovers}, GetNextIndexDhcpClientCounterTable, DhcpClientCountDiscoversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCountRequests}, GetNextIndexDhcpClientCounterTable, DhcpClientCountRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCountReleases}, GetNextIndexDhcpClientCounterTable, DhcpClientCountReleasesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCountDeclines}, GetNextIndexDhcpClientCounterTable, DhcpClientCountDeclinesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCountInforms}, GetNextIndexDhcpClientCounterTable, DhcpClientCountInformsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCountOffers}, GetNextIndexDhcpClientCounterTable, DhcpClientCountOffersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountAcksInReqState}, GetNextIndexDhcpClientCounterTable, DhcpCountAcksInReqStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountNacksInReqState}, GetNextIndexDhcpClientCounterTable, DhcpCountNacksInReqStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountAcksInRenewState}, GetNextIndexDhcpClientCounterTable, DhcpCountAcksInRenewStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountNacksInRenewState}, GetNextIndexDhcpClientCounterTable, DhcpCountNacksInRenewStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountAcksInRebindState}, GetNextIndexDhcpClientCounterTable, DhcpCountAcksInRebindStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountNacksInRebindState}, GetNextIndexDhcpClientCounterTable, DhcpCountNacksInRebindStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountAcksInRebootState}, GetNextIndexDhcpClientCounterTable, DhcpCountAcksInRebootStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountNacksInRebootState}, GetNextIndexDhcpClientCounterTable, DhcpCountNacksInRebootStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountErrorInHeader}, GetNextIndexDhcpClientCounterTable, DhcpCountErrorInHeaderGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountErrorInXid}, GetNextIndexDhcpClientCounterTable, DhcpCountErrorInXidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpCountErrorInOptions}, GetNextIndexDhcpClientCounterTable, DhcpCountErrorInOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientIpAddress}, GetNextIndexDhcpClientCounterTable, DhcpClientIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientLeaseTime}, GetNextIndexDhcpClientCounterTable, DhcpClientLeaseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientCounterReset}, GetNextIndexDhcpClientCounterTable, DhcpClientCounterResetGet, DhcpClientCounterResetSet, DhcpClientCounterResetTest, DhcpClientCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

{{12,DhcpClientRemainLeaseTime}, GetNextIndexDhcpClientCounterTable, DhcpClientRemainLeaseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DhcpClientCounterTableINDEX, 1, 0, 0, NULL},

};
tMibData fsdhclEntry = { 38, fsdhclMibEntry };

#endif /* _FSDHCLDB_H */

