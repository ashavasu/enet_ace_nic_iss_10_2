 /* $Id: fsdhclow.h,v 1.9 2014/03/11 13:14:55 siva Exp $ */

/* Proto Validate Index Instance for DhcpClientConfigTable. */
INT1
nmhValidateIndexInstanceDhcpClientConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpClientConfigTable  */

INT1
nmhGetFirstIndexDhcpClientConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpClientConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpClientRenew ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpClientRebind ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpClientInform ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpClientRelease ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpClientIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpClientRenew ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpClientRebind ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpClientInform ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpClientRelease ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpClientIdentifier ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpClientRenew ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpClientRebind ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpClientInform ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpClientRelease ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpClientIdentifier ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpClientConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpClientDebugTrace ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpClientDebugTrace ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpClientDebugTrace ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpClientDebugTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpClientOptTable. */
INT1
nmhValidateIndexInstanceDhcpClientOptTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpClientOptTable  */

INT1
nmhGetFirstIndexDhcpClientOptTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpClientOptTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpClientOptLen ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDhcpClientOptVal ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpClientOptRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpClientOptLen ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDhcpClientOptVal ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpClientOptRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpClientOptLen ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2DhcpClientOptVal ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpClientOptRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpClientOptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpClientCounterTable. */
INT1
nmhValidateIndexInstanceDhcpClientCounterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpClientCounterTable  */

INT1
nmhGetFirstIndexDhcpClientCounterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpClientCounterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpClientCountDiscovers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientCountRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientCountReleases ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientCountDeclines ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientCountInforms ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientCountOffers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountAcksInReqState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountNacksInReqState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountAcksInRenewState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountNacksInRenewState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountAcksInRebindState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountNacksInRebindState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountAcksInRebootState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountNacksInRebootState ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountErrorInHeader ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountErrorInXid ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpCountErrorInOptions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientIpAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpClientLeaseTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpClientCounterReset ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpClientRemainLeaseTime ARG_LIST((INT4 ,INT4 *));



/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpClientCounterReset ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpClientCounterReset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpClientCounterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpClientOptTable. */
/*INT1
nmhValidateIndexInstanceDhcpClientOptTable ARG_LIST((INT4  , INT4 ));
*/
/* Proto Type for Low Level GET FIRST fn for DhcpClientOptTable  */
/*
INT1
nmhGetFirstIndexDhcpClientOptTable ARG_LIST((INT4 * , INT4 *));
*/
/* Proto type for GET_NEXT Routine.  */
/*
INT1
nmhGetNextIndexDhcpClientOptTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
*/
/* Proto type for Low Level GET Routine All Objects.  */
/*
INT1
nmhGetDhcpClientOptLen ARG_LIST((INT4  , INT4 ,INT4 *));
*/
/*INT1
nmhGetDhcpClientOptVal ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
*/
/*
INT1
nmhGetDhcpClientOptRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));
*/
/* Low Level SET Routine for All Objects.  */

/*INT1
nmhSetDhcpClientOptRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));
*/


/* Low Level TEST Routines for.  */
/*
INT1
nmhTestv2DhcpClientOptRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
*/
/* Low Level DEP Routines for.  */
/*
INT1
nmhDepv2DhcpClientOptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
*/
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpClientFastAccess ARG_LIST((INT4 *));

INT1
nmhGetDhcpClientFastAccessDiscoverTimeOut ARG_LIST((INT4 *));

INT1
nmhGetDhcpClientFastAccessNullStateTimeOut ARG_LIST((INT4 *));

INT1
nmhGetDhcpClientFastAccessArpCheckTimeOut ARG_LIST((INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpClientFastAccess ARG_LIST((INT4 ));

INT1
nmhSetDhcpClientFastAccessDiscoverTimeOut ARG_LIST((INT4 ));

INT1
nmhSetDhcpClientFastAccessNullStateTimeOut ARG_LIST((INT4 ));

INT1
nmhSetDhcpClientFastAccessArpCheckTimeOut ARG_LIST((INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpClientFastAccess ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpClientFastAccessDiscoverTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpClientFastAccessNullStateTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpClientFastAccessArpCheckTimeOut ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpClientFastAccess ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpClientFastAccessDiscoverTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpClientFastAccessNullStateTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpClientFastAccessArpCheckTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
