 /* $Id: dhcsz.h,v 1.6 2013/09/07 10:41:14 siva Exp $ */
enum {
    MAX_DHC_OUT_PKT_INFO_SIZING_ID,
    MAX_DHC_BEST_OFFER_SIZING_ID,
    MAX_DHC_CLIENT_DEF_OPTIONS_SIZING_ID,
    MAX_DHC_CLIENTID_SIZING_ID,
    MAX_DHC_INTERFACE_SIZING_ID,
    MAX_DHC_MSG_OPTIONS_SIZING_ID,
    MAX_DHC_OPT_ENTRIES_SIZING_ID,
    MAX_DHCP_MAX_OUTMSG_SIZE_SIZING_ID,
    MAX_DHC_QUEUE_MSG_SIZING_ID,
    MAX_DHC_DISC_MSG_SIZING_ID,
    DHC_MAX_SIZING_ID
};


#ifdef  _DHCSZ_C
tMemPoolId DHCMemPoolIds[ DHC_MAX_SIZING_ID];
INT4  DhcSizingMemCreateMemPools(VOID);
VOID  DhcSizingMemDeleteMemPools(VOID);
INT4  DhcSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _DHCSZ_C  */
extern tMemPoolId DHCMemPoolIds[ ];
extern INT4  DhcSizingMemCreateMemPools(VOID);
extern VOID  DhcSizingMemDeleteMemPools(VOID);
extern INT4  DhcSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _DHCSZ_C  */


#ifdef  _DHCSZ_C
tFsModSizingParams FsDHCSizingParams [] = {
{ "tDhcpPktInfo", "MAX_DHC_OUT_PKT_INFO", sizeof(tDhcpPktInfo),MAX_DHC_OUT_PKT_INFO, MAX_DHC_OUT_PKT_INFO,0 },
{ "tBestOffer", "MAX_DHC_BEST_OFFER", sizeof(tBestOffer),MAX_DHC_BEST_OFFER, MAX_DHC_BEST_OFFER,0 },
{ "tDhcpClientDefOptions", "MAX_DHC_CLIENT_DEF_OPTIONS", sizeof(tDhcpClientDefOptions),MAX_DHC_CLIENT_DEF_OPTIONS, MAX_DHC_CLIENT_DEF_OPTIONS,0 },
{ "tClientIdentifier", "MAX_DHC_CLIENTID", sizeof(tClientIdentifier),MAX_DHC_CLIENTID, MAX_DHC_CLIENTID,0 },
{ "tDhcpCRec", "MAX_DHC_INTERFACE", sizeof(tDhcpCRec),MAX_DHC_INTERFACE, MAX_DHC_INTERFACE,0 },
{ "tDhcpMsgOptions", "MAX_DHC_MSG_OPTIONS", sizeof(tDhcpMsgOptions),MAX_DHC_MSG_OPTIONS, MAX_DHC_MSG_OPTIONS,0 },
{ "tDhcpCOptions", "MAX_DHC_OPT_ENTRIES", sizeof(tDhcpCOptions),MAX_DHC_OPT_ENTRIES, MAX_DHC_OPT_ENTRIES,0 },
{ "tDhcpCMessage", "MAX_DHCP_MAX_OUTMSG_SIZE", sizeof(tDhcpCMessage),MAX_DHCP_MAX_OUTMSG_SIZE, MAX_DHCP_MAX_OUTMSG_SIZE,0 },
{ "tDhcpQMsg", "MAX_DHC_QUEUE_MSG", sizeof(tDhcpQMsg),MAX_DHC_QUEUE_MSG, MAX_DHC_QUEUE_MSG,0 },
{ "tDhcpCDiscMsg", "MAX_DHC_OPTION_REG", sizeof(tDhcpDiscOption),MAX_DHC_OPTION_REG,MAX_DHC_OPTION_REG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _DHCSZ_C  */
extern tFsModSizingParams FsDHCSizingParams [];
#endif /*  _DHCSZ_C  */


