/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dhcdef.h,v 1.27 2015/09/15 11:53:33 siva Exp $
 *
 * DESCRIPTION    : Macro definitions used for DHCP CLIENT module       *
 ************************************************************************/

#ifndef  __DHCPC_DEF_H
#define  __DHCPC_DEF_H

#define  DHCP_MAX_CHWADDR_LEN          16 /* Client hardware address size */
#define  DHCP_MAX_CIDR                   32 

#define  BOOTP_SUCCESS                 1
#define  BOOTP_FAILURE                 0
#define   FIRST_TIMER_FLAG             1
#define   NEXT_TIMER_FLAG              2
#define   MAX_BOOT_RETRIES             10
#define   BOOTP_XID                    0x20

#define  DHCP_SET                      1 
#define  DHCP_NOT_SET                  2 

#define  DHCPC_SET_OPTION              1
#define  DHCPC_RESET_OPTION            2

#define  DHCP_FAILURE_IN_OPTIONS       2 
#define  DHCP_FAILURE_IN_XID           3 

#define  DHCP_MAX_CLIENTID_LEN         255
/* MACROS of the states used in the DHCP Client Module */
#define  S_UNUSED                      0
#define  S_INIT                        1
#define  S_SELECTING                   2
#define  S_REQUESTING                  3
#define  S_BOUND                       4
#define  S_RENEWING                    5
#define  S_REBINDING                   6
#define  S_INITREBOOT                  7
#define  S_REBOOTING                   8
#define  S_NULL                        9

#define  DHCP_MAX_RETRY                 ISS_CUST_DHCP_MAX_RETRY

#define  DHCPC_RESET                    0
#define  ACK_MESSAGE_RECIEVED           1
#define  DHCPC_INFORM_MESSAGE_SENT      1

/* 25/11/2008: changed from 4 to 5 for ntp server option */
/* 24/12/2013: changed from 5 to 3 because dns and ntp are moved to rb tree */
/* 14/08/2015: changed from 3 to 2 because ROUTER option is moved to rb tree */
#define  DHCP_DEF_PARAMETERS_LEN       2

#define  DHCP_CLIENT_TASK_NAME     (UINT1 *)"DHC"

/* Flags used for DHCP client */
#define  DHCP_UNICAST_MASK             0x0000

/************************* Macros used by DHCP Client *************************/

#define  DHCP_SERVER_NAME_LEN                 64
#define  DHCP_FILE_NAME_LEN                   128
#define  DHCPC_AUTO_CONFIG_IP_ADDRESS         0x00000000

/* Auto Configuration IP Address is 192.168.1.1 */
#define HWTYPE_ETHERNET                       1
#define ENET_ADDR_LEN                         6

#define DHCPC_GET_HW_TYPE(u4IfIndex)        HWTYPE_ETHERNET
#define DHCPC_GET_HW_TYPE_LEN(u4IfIndex)    ENET_ADDR_LEN

    /* Timer values for various timers present in the DHCP_CLIENT */

#define DHCPC_NULL_STATE_WAIT_TIME_OUT     180
    /* 180 Secs i.e., 3 Minutues once a DHCP_DISCOVER Message will be transmitted to the Server in the S_NULL State */

    /* Time outs for defined are as per RFC 2131 */

#define DHCPC_DISCOVER_RETX_TIME_OUT       15
    /* 15 Secs i.e., Retransmission time out for DHCP_DISCOVER Message */

#define DHCPC_MULTIPLE_OFFER_TIME_OUT      3 
    /* 3 Secs i.e., Retransmission time out for recieving Multiple Offer */

#define DHCPC_REQUEST_RETX_TIME_OUT        15
    /* 15 Secs i.e., Retransmission time out for DHCP_REQUEST Message */

#define DHCPC_INFORM_MSG_TIME_OUT          15
    /* 15 Secs i.e., Retransmission time out for DHCP_INFORM Message */

#define DHCPC_ARP_CHECK_TIME_OUT           3      
    /* 15 Secs i.e., Retransmission time out for ARP CHECK Message */
#define DHCPC_RELEASE_TIME_OUT           2      
    /* 1 Secs i.e., Release time out for RELEASE MESSAGE Message */
#define DHCPC_PROT_MUTEX_SEMA4    (const UINT1 *) "DCPS"
#define DHCP_CLIENT_Q_NAME        (UINT1 *) "DHCQ"
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
#define DHCP_CLIENT_Q_ARP_NAME        (UINT1 *) "DARP"
#define DHCPC_ARP_RESP_PROC_FLAG        gu1DhcpCArpRespProcFlag
#endif

#define DHCP_SEM_CREATE_INIT_CNT  1

#ifdef LNXIP4_WANTED
#define DHCPC_UDP_PROTO                    17
#define DHCPC_DEF_TTL                      64 
#endif
#endif /* __DHCPC_DEF_H */
