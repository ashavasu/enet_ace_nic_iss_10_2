/************************************************************************
 * $Id: dhctdf.h,v 1.23 2015/03/18 13:21:35 siva Exp $
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                          *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       *
 * FILE NAME      : dhcpctdf.h                                          *
 * LANGUAGE       : C Language                                          *
 * TARGET         : LINUX                                               *
 * AUTHOR         : A.S.Musthafa                                        *
 * DESCRIPTION    : Type definitions used in DHCP CLIENT module         *
 ************************************************************************/
#ifndef  __DHCPC_TDF_H

#define  __DHCPC_TDF_H

/********** Typedef of received/outgoing dhcp packet *********/
typedef struct DhcpCPktInfo {

    tDhcpMsgHdr DhcpMsg;        /* dhcp message header and options           */
    UINT4       u4IfIndex;       /* Interface number for the dhcp packet      */
    UINT4       u4DstIpAddr;     /* destination Ip-Address of the packet      */
    UINT2       u2Len;           /* length of the options in the dhcp message */
    UINT2       u2BufLen;        /* length of the dhcp message           */
    UINT2       u2MaxMessageSize; /* Max DHCP Message size           */

    UINT1       u1OverLoad;
    UINT1       u1Type;    /* DHCP Client Message type             */

} tDhcpPktInfo;

    /*** Typedef of Offerred Lease Info structure */
typedef struct BestOffer {
    tDhcpPktInfo    PktInfo;
    UINT2           u2Reserved; /* For Pack Problem */
    UINT1          u1NoOfReqOptSupported; /* Num. of Req Options supported                                                   in the offer */
    UINT1          u1TotalNoOfOptionPresent; /* Total Num. of Options recieved                                                  in the offer */
}tBestOffer;

    /******* Typedef of DhcpClient structure used in tDhcpClient *******/

/* dhcp option structure */
typedef struct Dhcpcoption {
  tRBNodeEmbd   RbNode;
  UINT1         au1OptionVal[DHCP_MAX_OPT_LEN]; /* Value of option which are requested */
  UINT1         u1Type;      /* option type which are to be requested */
  UINT4         u4IfIndex;         /* Interface Index */
  UINT1         u1Len;       /* length of option value */
  UINT1         u1Status;          /* Enabled - option type will be included in discover */
  UINT1         au1Reserved[2];  /* reserved byte*/
}
tDhcpCOptions; 

 typedef struct ClientConfInfo {
    tTMO_SLL DefaultOptList;    /* Default option read from the 
                       configuration file         */
    UINT4       u4StartOfLeaseTime;     /* Start of the lease time    */
    UINT1     *pDefaultOptions;     /* Default options pointer     */
    UINT2     u2LenOfOptions;     /* Length of the options     */
    UINT2    u2Reserved;         /* For Pack Problem  : ASM     */
}tClientConf;

typedef struct ClientIdentifier {
        tRBNodeEmbd   RbNode;
        UINT1         au1String[DHCP_MAX_CLIENTID_LEN]; /* Client Id */
        UINT1         u1HwType;                    /* Hardware Type */
        UINT4         u4IfIndex;                   /* Interface Index */
        UINT1         u1Len;                       /* Length of Client ID */
        UINT1         au1Padding[3];               /* Pading for structure alignment */
} tClientIdentifier;


    /********** Typedef of DhcpClient global structure **********/
 /* This is the interface lease information which has the active lease
  * info and the offered lease information */
typedef struct DhcpC {
    UINT4        u4Xid;             /* Last transmitted Xid */
    UINT4        u4Secs;        /* Network address acquisition     
                       initialisation time */
    UINT4        u4IpAddressOfferred;/* IP Address Leased by server */
    UINT4        u4LeaseTimeOfferred;/* Lease time offerred by server */
    UINT4        u4ServerOfferred;/* Ip Address of server which offered*/

    tClientConf  DhcpClientConfInfo;/* Client config. Info */
    tBestOffer   *pBestOffer; 
    UINT1        u1State;    /* Interface state */
    UINT1        u1Align[3];
} tDhcpCIfTable; 


    /********** Typedef of DhcpClient statistics **********/
typedef struct DhcpCTxRxCnt {
    UINT4    u4DhcpCDiscoverSent;    
    UINT4    u4DhcpCRequestSent;    
    UINT4    u4DhcpCReleaseSent;    
    UINT4    u4DhcpCDeclineSent;    
    UINT4    u4DhcpCInformSent;    
    UINT4    u4DhcpCNoOfOffersRcvd;    
    UINT4    u4DhcpCNoOfAckRcvdInREQ;    
    UINT4    u4DhcpCNoOfNAckRcvdInREQ;    
    UINT4    u4DhcpCNoOfAckRcvdInRENEW;    
    UINT4    u4DhcpCNoOfNAckRcvdInRENEW;    
    UINT4    u4DhcpCNoOfAckRcvdInREBIND;    
    UINT4    u4DhcpCNoOfNAckRcvdInREBIND;    
    UINT4    u4DhcpCNoOfAckRcvdInREBOOT;    
    UINT4    u4DhcpCNoOfNAckRcvdInREBOOT;    
    UINT4    u4DhcpCCounterReset;    
} tDhcpCStats;

typedef struct DhcpCErrCnt {
    UINT4    u4DhcpCErrInHeader;    
    UINT4    u4DhcpCErrInXid;    
    UINT4    u4DhcpCErrInOptions;    
} tDhcpCErrCnt;
    /*****Typedef of structure used for adding/extracting options 
        to/from dhcp message ***************/    
typedef struct Dhcptag {
    UINT1   Type;
    UINT1   Len;
    UINT2   u2Pad;
    UINT1   *Val;
} tDhcpCTag;

typedef tTmrAppTimer   tDhcpAppTimer;

typedef struct DhcpCTimer{
    tDhcpAppTimer DhcpAppTimer;
    VOID     *pEntry;   
    UINT4    u4IfIndex;        /* Interface Index  */
    UINT4    u4TimerId;        /* Timer Identifier */
    UINT4    u4LeasedIpAddr;   /* Ip Address which is leased */
    UINT4    u4LeasedTime;     /* The leased time by the server */
    UINT4    u4DstIpAddr;      /* The destination address of the packet */
    UINT4    u4CidrAddr;       /* Route address used in DECLINE msg tmr expry */
    UINT4    u4CidrMask;       /* Route Mask used in DECLINE msg tmr expry */
} tDhcpCTimer;

typedef struct ClientIdOpt {
        UINT1  au1ChAddr[DHCP_MAX_CID_LEN]; /* Client Id */
        UINT1  u1HwType;                    /* Hardware Type */
        UINT1  u1Len;                       /* Length of Client ID */
        UINT2  u2Padding;                   /* Pading for structure alignment */
} tClientId;

typedef struct ClientRec {
    tTMO_HASH_NODE  NextNode;
    UINT4           u4IpIfIndex;                /*Ip Interface Index */
    tDhcpCIfTable   DhcIfInfo;
    tClientId       ClientId;
    tDhcpCStats     TxRxStats;
    tDhcpCErrCnt    DhcpCErrCnt;
    tDhcpCTimer     ReplyWaitTimer;
    tDhcpCTimer     LeaseTimer;
    tDhcpCTimer     ArpCheckTimer;
    tDhcpCTimer     ReleaseWaitTimer; 
    UINT4           u4ProcessedAck;
    UINT1           u1ReTxCount;
    UINT1           u1DhcpCInformMsgSent;
    UINT2           u2RenewTimerCount;
}tDhcpCRec;

typedef struct DhcpcOptionList {
    tTMO_SLL_NODE Next;
    UINT4          u4DhcpOptionId;     /* Stores the DHCP option */
 /* Holds the function pointer registered from Application */
    VOID    (*pDhcpCOptionCallBack)(UINT4, UINT4, VOID *); 
}tDhcpCRegOptionStruct;

typedef struct DhcpDiscOption
{
    tTMO_SLL_NODE Next;
    INT4 aiOptionBitMask [DHCP_OPTION_BLOCKS];
    INT4 (*pDhcDiscoveryCbFn) (UINT1,UINT1 *,UINT1);
    INT4 iTaskId;
}tDhcpDiscOption;

typedef struct RegDhcpDiscOption
{
    INT4 ai4OptionBitMask [DHCP_OPTION_BLOCKS];
    UINT1 au1Data [DHCP_OPTION_BLOCKS * DHCP_OPTION_BLOCK_SIZE];
    INT1 i1OptionCnt;
    INT1 i1pad[3];
}tRegDhcpDiscOption;


/* Data structures used for Messaging/Interface with RM  */
#ifdef L2RED_WANTED
typedef struct _DhcpcRmCtrlMsg {
 tRmMsg           *pData;     /* RM message pointer */
 UINT2             u2DataLen; /* Length of RM message */
 UINT1             u1Event;   /* RM event */
 UINT1             au1Pad[1];
}tDhcpcRmCtrlMsg;
#endif /* L2RED_WANTED */


typedef struct _DhcpQMsg {
 UINT4 u4MsgType;
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
 UINT4 u4IpAddr;
#endif
 union {
  UINT4 u4IfIndex;
  tCRU_BUF_CHAIN_HEADER *pRcvBuf;
  tDhcpCDiscCbInfo AppDhcpCDiscCbInfo;
#ifdef L2RED_WANTED
  tDhcpcRmCtrlMsg RmCtrlMsg; /* Control message from RM */
#endif
 }unDhcpMsgIfParam;
} tDhcpQMsg;

#define  dhcpIfIndex   unDhcpMsgIfParam.u4IfIndex
#define  dhcpRcvBuf    unDhcpMsgIfParam.pRcvBuf

typedef struct ClientFastAccess {
 UINT4           u4FastAccNullStTimeOut; /* Time to wait after 4 unsuccessfull
                                             * DHCP Discovers */
    UINT4           u4FastAccDisTimeOut;    /* Time to wait between DHCP 
                                             * Discovers */
    UINT4           u4FastAccArpCheckTimeOut; /* retransmission timeout between ARP mesaages*/
    UINT1           u1FastAccess;           /* Fast DHCP Mode */
    UINT1           u1Padding[3];
}tDhcpCFastAcc;

typedef struct DhcpMsgOptions
{
    UINT1    au1DhcpMaxMsgOptLen[DHCP_MAX_OUT_OPTION_LEN];
}tDhcpMsgOptions;

typedef struct DhcpClientDefOptions
{
    UINT1    au1DhcpMaxMsgOptLen[DHCP_MAX_OPT_LEN];
}tDhcpClientDefOptions;

typedef struct _DhcpClientMemPoolId{
    tMemPoolId      DhcpCInfMemPoolId;
    tMemPoolId      DhcpCOptMemPoolId;
    tMemPoolId      DhcpCIdMemPoolId;
    tMemPoolId      DhcpCBestOffPoolId;
    tMemPoolId      DhcpCQMsgPoolId;
    tMemPoolId      DhcpCDhcpMsgOptPoolId;
    tMemPoolId      DhcpCClientDefOptPoolId;
    tMemPoolId      DhcpCOutPktPoolId;
    tMemPoolId      DhcpCMessageId;
    tMemPoolId      DhcpCDiscPoolId;
} tDhcpClientMemPoolId;

typedef struct DhcpCMessage
{
    INT1    asDhcpCMessage[DHCP_MAX_OUTMSG_SIZE];
} tDhcpCMessage;

#endif /* __DHCPC_TDF_H */
