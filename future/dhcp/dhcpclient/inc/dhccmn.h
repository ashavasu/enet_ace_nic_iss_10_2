/************************************************************************
 * $Id: dhccmn.h,v 1.11 2015/03/18 13:21:34 siva Exp $
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                          *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       *
 * FILE NAME      : dhccmn.h                                            *
 * LANGUAGE       : C Language                                          *
 * TARGET         : LINUX                                               *
 * AUTHOR         : A.S.Musthafa                                        *
 * DESCRIPTION    : Common include files for DHCP CLIENT module.        *
 ************************************************************************/

#ifndef  __DHCPC_COM_H
#define  __DHCPC_COM_H

#include "lr.h"
#include "cfa.h"
#include "arp.h"
#include "ip.h"
#include "fssocket.h"
#include "tcp.h"
#include "dhcp.h"
#include "rmgr.h"
#include "snmctdfs.h"
#include "snmcdefn.h"
#include "sntp.h"
#include "iss.h"

/* DHCP Client include files */
#include    "dhcport.h"
#include    "dhcdef.h"
#include    "dhctdf.h"
#include    "dhcred.h"
#include    "dhcglob.h"
#include    "dhcsz.h"
#include    "fsdhclow.h"

#endif /* __DHCPC_COM_H */
