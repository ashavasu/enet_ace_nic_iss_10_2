/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dhcclipt.h,v 1.15 2014/03/11 13:15:19 siva Exp $
 * DESCRIPTION    : Defined function prototypes for DHCPC - CLI support
 * ***********************************************************************/

#ifndef _DHCCLIPT_H
#define _DHCCLIPT_H

INT4 DhcpClientSetFastAcc PROTO ((tCliHandle, INT4));
INT4 DhcpClientSetIdleTimer PROTO ((tCliHandle, INT4));
INT4 DhcpClientSetDiscTimer PROTO ((tCliHandle, INT4));
INT4 DhcpClientSetArpCheckTimer PROTO ((tCliHandle, INT4));
INT4 DhcpClientShowStats PROTO((tCliHandle));
INT4 DhcpClientSetRenew PROTO((tCliHandle, INT4));
INT4 DhcpClientSetRelease PROTO((tCliHandle, INT4));
INT4 DhcpClientSetTrace PROTO((tCliHandle, INT4, UINT1));
VOID IssDhcpClientShowDebugging (tCliHandle);
INT4 DhcpClientSetOption PROTO ((tCliHandle CliHandle, UINT4 u4Status,
                                UINT4 u4Index, UINT4  u4OptType));
INT4 DhcpClientShowAllOption PROTO ((tCliHandle CliHandle));
INT4 DhcpClientSetConfigId PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex,
                      UINT4  u4IdIndex, UINT1 * pu1OptVal));
INT4 DhcpClientShowIdentifiers PROTO ((tCliHandle CliHandle));
INT4 DhcpClientShowOption PROTO ((tCliHandle CliHandle,
                      UINT4 u4UserIfIndex, UINT4 u4UserOptType));
INT4 DhcpClientShowFastAccess PROTO ((tCliHandle CliHandle));
INT4 DhcpClientShowRunningConfig(tCliHandle CliHandle, UINT4 u4Module);
INT4 DhcpClientClearStatsAllInterface PROTO ((tCliHandle CliHandle));
INT4 DhcpClientClearStatsInterface PROTO ((tCliHandle CliHandle, INT4 i4Index, UINT4 u4Value));
INT4 DhcpClientAddVendorSpecificInfo PROTO ((tCliHandle CliHandle, UINT1 *pu1Value, UINT4 u4Index));
INT4 DhcpClientRemoveVendorSpecificInfo PROTO ((tCliHandle CliHandle, UINT4 u4Value));
#endif/*_DHCCLIPT_H */
