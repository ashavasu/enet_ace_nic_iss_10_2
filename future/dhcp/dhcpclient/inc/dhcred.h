/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcred.h,v 1.2 2011/12/05 14:39:44 siva Exp $
 *
 * Description: This file contains macros definitions for constants
 *              used for DHCP client Redundancy
 *********************************************************************/
#ifndef _DHCRED_H_
#define _DHCRED_H_

typedef struct _DhcpCRedGlobalInfo {
 UINT1               u1NodeStatus;
 /* RM Node Status "Idle/Active/StandBy" */
    UINT1               u1NumPeersUp;
    /* Indicates number of standby nodes that are up. */
    BOOL1               bBulkReqRcvd;
 /* Bulk Request Received Flag - OSIX_TRUE/OSIX_FALSE. This
  * flag is set when the dynamic bulk request message reaches
  * the active node before the STANDBY_UP event. Bulk updates
  * are sent only after STANDBY_UP event is reached.
 */
 UINT1               au1Pad[1];

} tDhcpCRedGlobalInfo;

/* Represents the message types encoded in the update messages */
typedef enum {
 DHCPC_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
 DHCPC_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
 DHCPC_RED_BULK_CLIENT_INFO,
 DHCPC_RED_CLIENT_INFO
}eDhcpCRedRmMsgType;

#define DHCPC_RED_MAX_MSG_SIZE        1500
#define DHCPC_RED_TYPE_FIELD_SIZE     1
#define DHCPC_RED_LEN_FIELD_SIZE      2

#define DHCPC_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define DHCPC_RED_BULK_REQ_MSG_SIZE            3

/* DHCPC_RED_DYN_CLIENT_REC_SIZE is the
 * maximum size of the Client Record  and is derived 
 * as the sum of size of tDhcpCRec and size of all the 
 * elements the tDhcpCRec points to. */

#define DHCPC_RED_DYN_CLIENT_REC_SIZE         \
        (sizeof(tDhcpCRec)   + \
        DHCP_MAX_OPT_LEN     + \
        sizeof(tBestOffer)   + \
        sizeof(tDhcpPktInfo) + \
        DHCP_MAX_OUT_OPTION_LEN)

#define DHCPC_IDLE_NODE                             RM_INIT
#define DHCPC_ACTIVE_NODE                           RM_ACTIVE
#define DHCPC_STANDBY_NODE                          RM_STANDBY

/* Macros for DHCPC redundancy feature. */

#define DHCPC_RM_NODE_STATUS()  gDhcpCRedGlobalInfo.u1NodeStatus

#define DHCPC_RM_GET_NUM_STANDBY_NODES_UP() \
      gDhcpCRedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define DHCPC_NUM_STANDBY_NODES() gDhcpCRedGlobalInfo.u1NumPeersUp

#define DHCPC_IS_STANDBY_UP() \
          ((gDhcpCRedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* If RM wanted */
#ifdef L2RED_WANTED
#define DHCPC_RM_GET_NODE_STATUS()  RmGetNodeState ()
#else
#define DHCPC_RM_GET_NODE_STATUS()  RM_ACTIVE
#endif

/* Macros to write in to RM buffer. */
#define DHCPC_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
          *(pu4Offset) += 1;\
}while (0)

#define DHCPC_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
          *(pu4Offset) += 2;\
}while (0)

#define DHCPC_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
          *(pu4Offset) += 4;\
}while (0)

#define DHCPC_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
     RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
          *(pu4Offset) +=u4Size; \
}while (0)

#define DHCPC_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
          *(pu4Offset) += 1;\
}while (0)

#define DHCPC_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
          *(pu4Offset) += 2;\
}while (0)

#define DHCPC_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
          *(pu4Offset) += 4;\
}while (0)

#define DHCPC_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
     RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
          *(pu4Offset) += u4Size; \
}while (0)

/* PROTOTYPES */

PUBLIC INT4 DhCRedInitGlobalInfo  PROTO ((VOID));
PUBLIC VOID DhCRedDeInitGlobalInfo PROTO ((VOID));
PUBLIC INT4 DhCRedRmRegisterProtocols PROTO ((tRmRegParams *pRmRegParams ));
PUBLIC INT4 DhCRedRmDeRegisterProtocols  PROTO ((VOID));
PUBLIC INT4 DhCRedRmCallBack PROTO ((UINT1 u1Event , tRmMsg * pData , 
          UINT2 u2DataLen));
PUBLIC VOID DhCRedHandleRmEvents PROTO ((tDhcpQMsg * pMsg ));
PUBLIC VOID DhCRedHandleGoActive  PROTO ((VOID));
PUBLIC VOID DhCRedHandleGoStandby  PROTO ((VOID));
PUBLIC VOID DhCRedHandleStandbyToActive  PROTO ((VOID));
PUBLIC VOID DhCRedHandleActiveToStandby  PROTO ((VOID));
PUBLIC VOID DhCRedHandleIdleToStandby  PROTO ((VOID));
PUBLIC VOID DhCRedHandleIdleToActive  PROTO ((VOID));
PUBLIC VOID DhCRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg , UINT2 u2DataLen));
PUBLIC VOID DhCRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg , UINT2 u2DataLen));
PUBLIC VOID DhCRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg , UINT2 *pu2OffSet ));
PUBLIC VOID DhCRedSendDynamicBulkMsg  PROTO ((VOID));
PUBLIC VOID DhCRedSendClientInfoBulk  PROTO ((VOID));
PUBLIC tRmMsg * DhCRedGetMsgBuffer PROTO ((UINT2 u2BufSize));
PUBLIC VOID DhCRedSendBulkUpdTailMsg  PROTO ((VOID));
PUBLIC VOID DhCRedSendBulkReqMsg  PROTO ((VOID));
PUBLIC INT4 DhCRedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length));
PUBLIC VOID DhCRedSendDynamicClientInfo PROTO ((tDhcpCRec * pDhcpCNode));
PUBLIC VOID DhCRedProcessDynamicClientInfo PROTO ((tRmMsg * pMsg , UINT2 *pu2OffSet));

#endif /* _DHCRED_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  dhcred.h                       */
/*-----------------------------------------------------------------------*/
