/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: dhcpccmd.def,v 1.27 2016/06/30 10:10:36 siva Exp $                                                         
*                                                                    
*********************************************************************/

/***** COMMANDS FOR DHCP CLIENT **************/

DEFINE GROUP: DHCPC_PEXCFG_GRP

COMMAND  : ip dhcp client discovery timer <integer (1-300)>
ACTION   : {
			cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_DISC_TIMER, NULL, $5);
		   }
SYNTAX   : ip dhcp client discovery timer <integer (1-300)>
PRVID    : 15
HELP     : Configure DHCP Client Discovery timer. Time to wait between 
           discovery messages which is sent by DHCP client.
CXT_HELP : ip IP related protocol configuration |
		   dhcp DHCP related configuration |
		   client Client related configuration |
		   discovery Discovery timer configuration |
		   timer Timer related configuration |
		   integer(1-300) Timer value in seconds |
		   <CR> Configures DHCP Discovery timer. Time to wait between 
           discovery messages which is sent by DHCP client.

COMMAND : no ip dhcp client discovery timer
ACTION  : {
			cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_DISC_TIMER, NULL);
		  }
SYNTAX  : no ip dhcp client discovery timer
PRVID   : 15
HELP    : Configure DHCP Client discovery timer with the default values.
          If dhcp fast acces mode is enabled, the default time will be 5.
          Otherwise, the default time will be 15.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
		   ip IP related protocol configuration |
		   dhcp DHCP related configuration |
		   client Client related configuration |
		   discovery Discovery timer configuration |
		   timer Timer related configuration |
		   <CR> Configure DHCP Client discovery timer with the default values
           If dhcp fast acces mode is enabled, the default time will be 5.
           Otherwise, the default time will be 15.

COMMAND : ip dhcp client idle timer <integer (1-300)>
ACTION  : {
			cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_IDLE_TIMER, NULL, $5);
		  }
SYNTAX  : ip dhcp client idle timer <integer (1-300)>
PRVID   : 15
HELP    : Configure DHCP Client idle timer. Time to wait after four 
          unsuccessfull DHCP client discovery messages
CXT_HELP : ip IP related protocol configuration | 
			dhcp DHCP related configuration |
			client Client related configuration |
			idle Null state timer configuration |
			timer Timer related configuration |
			integer(1-300) Timer value in seconds |
			<CR> Configure DHCP Client idle timer. Time to wait after four 
            unsuccessfull DHCP client discovery messages

COMMAND : no ip dhcp client idle timer
ACTION  : {
			cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_IDLE_TIMER, NULL);
		  }
SYNTAX  : no ip dhcp client idle timer
PRVID   : 15
HELP    : Configure DHCP Client idle timer with the default values
          If dhcp fast acces mode is enabled, the default time will be 1.
          Otherwise, the default time will be 180.
CXT_HELP :  no Disables the configuration / deletes the entry / resets to default value |
			ip IP related protocol configuration |
			dhcp DHCP related configuration |
			idle Null state timer configuration |
			timer Timer related configuration |
			<CR> Configure DHCP Client idle timer with the default values
            If dhcp fast acces mode is enabled, the default time will be 1.
            Otherwise, the default time will be 180.
COMMAND : ip dhcp client arp-check timer <integer (1-20)>
ACTION  : {
               cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_ARP_CHECK_TIMER, NULL, $5);
	  }
SYNTAX  : ip dhcp client arp-check timer <integer (1-20)>
PRVID   : 15
HELP    : Configures DHCP client retransmission timeout between arp messages. 
CXT_HELP : ip IP related protocol configuration |
                        dhcp DHCP related configuration |
                        client Client related configuration |
                        arp-check  timer configuration |
                        timer Timer related configuration |
                        integer(1-20) Timer value in seconds |
                        <CR> Configures DHCP client retransmission timeout between arp messages.
COMMAND : no ip dhcp client arp-check timer
ACTION  : {
              cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_ARP_CHECK_TIMER, NULL);
          }
SYNTAX  : no ip dhcp client arp-check timer
PRVID   : 15
HELP    : Configure DHCP Client arp timer with the default values
          If dhcp fast acces mode is enabled, the default time will be 3.
          Otherwise, the default time will be 20.
CXT_HELP :  no Disables the configuration / deletes the entry / resets to default value |
                        ip IP related protocol configuration |
                        dhcp DHCP related configuration |
                        arp  timer configuration |
                        timer Timer related configuration |
                        <CR> Configure DHCP Client arp  timer with the default values
            If dhcp fast acces mode is enabled, the default time will be 3.
            Otherwise, the default time will be 20.

COMMAND : ip dhcp client fast-access
ACTION  : {
			cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_FAST_ACC, NULL, NULL);
		  }
SYNTAX  : ip dhcp client fast-access
PRVID   : 15
HELP    : Enables DHCP Client fast access mode. 
CXT_HELP : ip IP related protocol configuration |
			dhcp DHCP related configuration |
			client Client related configuration |
			fast-access Fast access configuration |
			<CR> Enables DHCP Client fast access mode. 

COMMAND : no ip dhcp client fast-access
ACTION  : {
			cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_FAST_ACC, NULL);
		  }
SYNTAX  : no ip dhcp client fast-access
PRVID   : 15
HELP    : Disables DHCP Client fast access mode.
CXT_HELP : 	no Disables the configuration / deletes the entry / resets to default value |
			ip IP related protocol configuration |
			dhcp DHCP related configuration |
			client Client related configuration |
			fast-access Fast access configuration |
			<CR> Disables DHCP Client fast access mode.

COMMAND : debug ip dhcp client { all | event | packets | errors | bind }
ACTION  : {
              UINT4 u4Value=0;

              if($4 != NULL)
              {
                  u4Value = DHCPC_DEBUG_ALL;
              } 
              else if($5 != NULL)
              {
                  u4Value = DHCPC_DEBUG_EVENT;
              } 
              else if($6 != NULL)
              {
                  u4Value =  DHCPC_DEBUG_PACKETS;
              }
              else if($7 != NULL)
              {
                  u4Value = DHCPC_DEBUG_ERRORS;
              }
              else if($8 != NULL)
              {
                  u4Value = DHCPC_DEBUG_BIND;
              }
              cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_DEBUG, NULL,
                                                                   u4Value);
          }  
SYNTAX  : debug ip dhcp client { all | event | packets | errors | bind }
PRVID   : 15
HELP    : Support for debugging of DHCP client 
CXT_HELP : debug Configures trace for the protocol | 
           ip IP related protocol configuration | 
           dhcp DHCP related configuration | 
           client Client related configuration | 
           all All trace messages | 
           event Trace management messages | 
           packets Packet related messages | 
           errors Trace error code debug messages | 
           bind Trace bind messages | 
           <CR> Support for debugging of DHCP client

COMMAND : no debug ip dhcp client { all | event  | packets | errors | bind }
ACTION  : {
              UINT4 u4Value=0;

              if($5 != NULL)
              {
                  u4Value = DHCPC_DEBUG_ALL;
              } 
              else if($6 != NULL)
              {
                  u4Value = DHCPC_DEBUG_EVENT;
              }
              else if($7 != NULL)
              {
                  u4Value =  DHCPC_DEBUG_PACKETS;
              }
              else if($8 != NULL)
              {
                  u4Value = DHCPC_DEBUG_ERRORS;
              }
              else if($9 != NULL)
              {
                  u4Value = DHCPC_DEBUG_BIND;
              }
              cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_DEBUG, NULL, u4Value);
          }  
SYNTAX  : no debug ip dhcp client { all | event | packets | errors | bind }
PRVID   : 15
HELP    : No support for debugging of DHCP client 
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           debug Configures trace for the protocol | 
           ip IP related protocol configuration | 
           dhcp DHCP related configuration | 
           client Client related configuration | 
           all All trace messages | 
           event Trace management messages | 
           packets Packet related messages |
           errors Trace error code debug messages | 
           bind Trace bind messages | 
           <CR> No support for debugging of DHCP client


COMMAND : clear ip dhcp client statistics [ interface {vlan <integer (1-4094)> | <ifXtype> <ifnum>} ]
ACTION  : {
                   UINT4 u4IfIndex = 0;

                  if ($5 != NULL)
                  {
                      if ((CfaCliGetIfIndex ($6, $7, &u4IfIndex) == CLI_FAILURE) &&
                          (CfaCliGetIfIndex ($8, $9, &u4IfIndex) == CLI_FAILURE))
                      {
                          CliPrintf(CliHandle,"\r%%Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                      }
                      cli_process_dhcpc_cmd (CliHandle,CLI_DHCP_CLNT_CLEAR_INTERFACE_STATS,u4IfIndex);             
                  }
                  else
                  {
                      cli_process_dhcpc_cmd (CliHandle,CLI_DHCP_CLNT_CLEAR_ALL_INTERFACE_STATS,NULL);              
                  }

          }  
SYNTAX  : clear ip dhcp client statistics [interface {vlan <VlanId(1-4094)> |<interface-type> <interface-id>}]
PRVID   : 15
HELP    : This command is used to clear the DHCP client statistics.
CXT_HELP : clear Performs clear operation |
           ip IP related configuration |
           dhcp DHCP related configuration | 
           client Client related configuration | 
           statistics Statistics related information | 
           interface Interface related configuration | 
           vlan VLAN related configuration | 
           (1-4094) VLAN ID | 
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type | 
           <ifnum> Interface number (Example: 0/1) | 
           <CR> This command is used to clear the DHCP client statistics. 

#ifdef WGS_WANTED
COMMAND : release dhcp { cpu0 | vlanMgmt | <iftype> <ifnum> }
ACTION  : {
             UINT4 u4Ret = CLI_SUCCESS;
             UINT4 u4IfIndex = 0;
             if ($2 != NULL)
             {
                 u4Ret = CfaCliGetOobIfIndex(&u4IfIndex);
             }
             if ($3 != NULL)
             {
		 u4IfIndex = CFA_DEFAULT_ROUTER_VLAN_IFINDEX;
	     }
	     else if ($4 != NULL)
             {
                 u4Ret = CfaCliGetIfIndex($4, $5, &u4IfIndex);
             }
             if (u4Ret == CLI_FAILURE)
             {
                 CliPrintf(CliHandle, "\r%% Invalid interface specified\r\n");
             }
             else
             { 
                 cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_RELEASE,
                                                  u4IfIndex);
             } 
          } 
SYNTAX  : release dhcp { cpu0 | vlanMgmt | <interface-type> <interface-id> }
PRVID   : 15
HELP    : Release DHCP lease 
CXT_HELP : release Performs release operation | 
           dhcp DHCP related configuration | 
           cpu0 Management interface related configuration |
           vlanMgmt Management VLAN interface | 
           DYNiftype|
           DYNifnum|
           <CR> Release DHCP lease

COMMAND : renew dhcp { cpu0 | vlanMgmt | <iftype> <ifnum> }
ACTION  : {
             UINT4 u4Ret = CLI_SUCCESS;
             UINT4 u4IfIndex = 0;
             if ($2 != NULL)
             {
                 u4Ret = CfaCliGetOobIfIndex(&u4IfIndex);
             }
             if ($3 != NULL)
             {
                 u4IfIndex = CFA_DEFAULT_ROUTER_VLAN_IFINDEX;
             }
             else if ($4 != NULL)
             {
                 u4Ret = CfaCliGetIfIndex($4, $5, &u4IfIndex);
             }
             if (u4Ret == CLI_FAILURE)
             {
                 CliPrintf(CliHandle, "\r%% Invalid interface specified\r\n");
             }
             else
             { 
                 cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_RENEW,
                                                  u4IfIndex);
             }
          }
SYNTAX  : renew dhcp { cpu0 | vlanMgmt | <interface-type> <interface-id> }
PRVID   : 15
HELP    : Renew DHCP lease 
CXT_HELP : renew Performs renew operation | 
           dhcp DHCP related configuration | 
           cpu0 Management interface related configuration |
           vlanMgmt Management VLAN interface | 
           DYNiftype|
           DYNifnum|
           <CR> Renew DHCP lease
#else

COMMAND : release dhcp { cpu0 | vlan <short (1-4094)> | <iftype> <ifnum> }
ACTION  : {
             UINT4 u4Ret = CLI_SUCCESS;
             UINT4 u4IfIndex = 0;
             if ($2 != NULL)
             {
                 u4Ret = CfaCliGetOobIfIndex(&u4IfIndex);
             }
             if ($3 != NULL)
             {
                 u4Ret = CfaCliGetIfIndex($3, $4, &u4IfIndex);
             }
             else if ($5 != NULL)
             {
                 u4Ret = CfaCliGetIfIndex($5, $6, &u4IfIndex);
             }
             if (u4Ret == CLI_FAILURE)
             {
                 CliPrintf(CliHandle, "\r%% Invalid interface specified\r\n");
             }
             else
             { 
                 cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_RELEASE,
                                                  u4IfIndex);
             } 
          } 
SYNTAX  : release dhcp { cpu0 | vlan <vlan-id (1-4094)> | <interface-type> <interface-id> }
PRVID   : 15
HELP    : Release DHCP lease 
CXT_HELP : release Performs release operation | 
           dhcp DHCP related configuration | 
           cpu0 Management interface related configuration |
           vlan VLAN related configuration | 
           (1-4094) VLAN ID |
           DYNiftype|
           DYNifnum|
           <CR> Release DHCP lease

COMMAND : renew dhcp { cpu0 | vlan <short (1-4094)> | <iftype> <ifnum> }
ACTION  : {
             UINT4 u4Ret = CLI_SUCCESS;
             UINT4 u4IfIndex = 0;
             if ($2 != NULL)
             {
                 u4Ret = CfaCliGetOobIfIndex(&u4IfIndex);
             }
             if ($3 != NULL)
             {
                 u4Ret = CfaCliGetIfIndex($3, $4, &u4IfIndex);
             }
             else if ($5 != NULL)
             {
                 u4Ret = CfaCliGetIfIndex($5, $6, &u4IfIndex);
             }
             if (u4Ret == CLI_FAILURE)
             {
                 CliPrintf(CliHandle, "\r%% Invalid interface specified\r\n");
             }
             else
             { 
                 cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_RENEW,
                                                  u4IfIndex);
             }
          }
SYNTAX  : renew dhcp { cpu0 | vlan <vlan-id (1-4094)> | <interface-type> <interface-id> }
PRVID   : 15
HELP    : Renew DHCP lease 
CXT_HELP : renew Performs renew operation | 
           dhcp DHCP related configuration | 
           cpu0 Management interface related configuration |
           vlan VLAN related configuration | 
           (1-4094) VLAN ID |
           DYNiftype|
           DYNifnum|
           <CR> Renew DHCP lease
#endif

COMMAND : show ip dhcp client fast-access
ACTION  : {
             cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_SHOW_FASTACCESS, NULL );
          }
SYNTAX  : show ip dhcp client fast-access
PRVID   : 0
HELP    : Displays DHCP fast access
CXT_HELP : show Displays the configuration/statistics/general information |
          ip IP related protocol configuration |
          dhcp DHCP related configuration |
          client Client related configuration |
          fast-access Fast access related configuration |
          <CR> Displays the configured DHCP fast access 

COMMAND : show ip dhcp client stats  
ACTION  : {
             cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_SHOW_STATS, NULL );
          }
SYNTAX  : show ip dhcp client stats
PRVID   : 0
HELP    : Displays DHCP client statistics
CXT_HELP : show Displays the configuration/statistics/general information | 
           ip IP related protocol configuration | 
           dhcp DHCP related configuration | 
           client Client related configuration | 
           stats Statistics related information | 
           <CR> Displays DHCP client statistics

COMMAND  : show ip dhcp client option
ACTION   : {
	      cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_SHOW_ALL_OPTION,
		                  			NULL);

	   }
SYNTAX   : show ip dhcp client option
HELP     : Displays DHCP client options set by Server
CXT_HELP : show Displays the configured DHCP client options |
           ip IP related protocol configuration |    
           dhcp DHCP related configuration |
           client Client related configuration |
           option configured request options list |
           <CR> Displays the configured DHCP client options  

COMMAND : show ip dhcp client client-id  
ACTION  : {
             cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_SHOW_CLNTID, NULL);
          }
SYNTAX  : show ip dhcp client client-id
HELP    : Displays DHCP client client identifier
CXT_HELP: show Displays the configured DHCP client identifier |
          ip IP related protocol configuration |
          dhcp DHCP related configuration |
          client Client related configuration |
          client-id client identifier |
          <CR> Displays the configured DHCP client identifiers

END GROUP

DEFINE GROUP: DHCPC_INTCFG_GRP
COMMAND : ip dhcp client client-id {<iftype> <ifnum> | vlan <integer (1-4094)> | port-channel <integer (1-65535)> | tunnel <integer (0-128)> | loopback <integer (0-100)> | ascii <string> | hex <string> } 
ACTION  : {
	      UINT1 *pu1Data = NULL;
	      UINT4  u4IdIfIndex = 0;

	      if($4 != NULL)
	      {
	          if (CfaCliGetIfIndex ($4, $5, &u4IdIfIndex) == CLI_FAILURE)
		  {
                        if (CfaCreatePhysicalInterface($4,$5,
                                              &u4IdIfIndex) != CLI_SUCCESS)
                       {
                          CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                       }

		  }
	      }
	      else if($6 != NULL)
	      {
		      if (CfaCliGetIfIndex ($6, $7, &u4IdIfIndex) == CLI_FAILURE)
                      {
                          CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                       }
	      }
	      else if($8 != NULL)
	      {
		      if (CfaCliGetPoIndex ($8, $9, &u4IdIfIndex) == CLI_FAILURE)
                      {
                          CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                       }
	      }
	      else if ($10 != NULL)
	      {
		      /* tunnel */
		      if (CfaCliGetIfIndex ($10, $11, &u4IdIfIndex)== CLI_FAILURE)
                      {
                          CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                       }
	      }
	      else if ($12 != NULL)
	      {
		      if (CfaCliGetIfIndex ($12, $13, &u4IdIfIndex) == CLI_FAILURE)
                      {
                          CliPrintf(CliHandle,"\r%% Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                      }
	      }
	      else if ($14 != NULL)
	      {
		      pu1Data = $15;
	      }    
	      else if ($16 != NULL) 
	      {  
		      pu1Data = $17;
	      }  
	      cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_ID,
			               NULL, u4IdIfIndex, pu1Data);
	  }

SYNTAX   :  ip dhcp client client-id {<interface-type> <interface-id> | vlan <vlan-id (1-4094)> | port-channel <port-channel-id (1-65535)> | tunnel <tunnel-id (0-128)> | loopback <interface-id (0-100)> | ascii <string> | hex <string> } 
PRVID    : 15
HELP     : Sets unique identifier to dhcp client
CXT_HELP : ip IP related protocol configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           client-id client identifier |
           DYNiftype|
           DYNifnum|
           vlan VLAN interface |
           (1-4094) VLAN ID |
           port-channel Port channel interface |
           (1-65535) Port channel ID |
           tunnel Tunnel interface |
           (0-128) Tunnel ID |
           loopback Loopback interface |
           (0-100) Loopback ID |
           ascii Client-ID as ascii string |
           string Client-ID string |
           hex Client-ID in hex |   
           string Client-ID string |
           <CR> configure client identifier

COMMAND : ip dhcp client vendor-specific <string>
ACTION  : {
            
              cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_VENDOR_SPECIFIC,
                                    NULL, $4, NULL);
          }
SYNTAX  : ip dhcp client vendor-specific <vendor-info>
HELP    : This command is used to configure Vendor Specific Information.
CXT_HELP : ip IP related protocol configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           vendor-specific Vendor specific configuration |
           <string> String input |
           <CR> set option to request the server

COMMAND : no ip dhcp client vendor-specific
ACTION  : {
            
              cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_VENDOR_SPECIFIC,
                                    NULL);
          }
SYNTAX  : no ip dhcp client vendor-specific 
HELP    : This command is used to remove Vendor specific information.
CXT_HELP : no Disable configuration |
           ip IP related protocol configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           vendor-specific Vendor specific configuration |
           <CR> set option to request the server

COMMAND : no ip dhcp client client-id
ACTION  : {
	      cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_NO_ID,
                   				  NULL);
          }
SYNTAX  : no ip dhcp client client-id
PRVID   : 15
HELP    : Resets the dhcp client identifier
CXT_HELP : no Resets the dhcp client identifier |
           ip IP related protocol configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           client-id client identifier |
           <CR> resets the client id string

COMMAND : ip dhcp client request { tftp-server-name | boot-file-name | sip-server-info | option240} 
ACTION  : {
            
              if ($4 != NULL)
              {
                  cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
			  NULL, CLI_ENABLE, DHCPC_TFTP_SERVER_NAME);
              }
              else if ($5 != NULL)
              {
	          cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
			  NULL, CLI_ENABLE, DHCPC_BOOT_FILE_NAME);
              }
              else if ($6 != NULL)
              {
                  cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
                          NULL, CLI_ENABLE, DHCPC_SIP_SERVER);
              }
              else if ($7 != NULL)
              {
                  cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
                          NULL, CLI_ENABLE, DHCP_OPT_240);
              }
                    
          }
SYNTAX  : ip dhcp client request { tftp-server-name | boot-file-name | sip-server-info | option240}
HELP    : The dhcp option type can be set to request the server.
CXT_HELP : ip IP related protocol configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           request Set options to request |
           tftp-server-name TFTP server name (66) |
           boot-file-name BOOT file name (67) |
           sip-server-info sipserver details(120) |
           option240 option 240 information |
           <CR> set option to request the server


COMMAND : no ip dhcp client request { tftp-server-name | boot-file-name | sip-server-info | option240 }
ACTION  : {
              if ($5 != NULL)
              {
	          cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
			  NULL, CLI_DISABLE, DHCPC_TFTP_SERVER_NAME);
              }
              else if ($6 != NULL)
              {
                  cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
			  NULL, CLI_DISABLE, DHCPC_BOOT_FILE_NAME);
              }
              else if ($7 != NULL)
              {
                   cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
                          NULL, CLI_DISABLE, DHCPC_SIP_SERVER);        
          }
              else if ($8 != NULL)
              {
                   cli_process_dhcpc_cmd(CliHandle, CLI_DHCPCLNT_OPTION,
                          NULL, CLI_DISABLE, DHCP_OPT_240);        
              }
        

          }
SYNTAX  : no ip dhcp client request { tftp-server-name | boot-file-name | sip-server-info | option240 }
HELP    : The dhcp option type can be reset to request the server.
CXT_HELP : no Set the option not to request |
           ip IP related protocol configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           request Set options not to request |
           tftp-server-name TFTP server name (66) |
           boot-file-name BOOT file name (67) |
           sip-server-info sipserver details(120) |
           option240 option 240 information(240) |
           <CR> set option not to request the server

END GROUP
DEFINE GROUP : DHCPC_CFG_GRP
COMMAND : clear ip dhcp client statistics [ interface {vlan <integer (1-4094)> | <ifXtype> <ifnum>} ]
ACTION  : {
                   UINT4 u4IfIndex = 0;

                  if ($5 != NULL)
                  {
                      if ((CfaCliGetIfIndex ($6, $7, &u4IfIndex) == CLI_FAILURE) &&
                          (CfaCliGetIfIndex ($8, $9, &u4IfIndex) == CLI_FAILURE))
                      {
                          CliPrintf(CliHandle,"\r%%Invalid Interface Index \r\n");
                          return CLI_FAILURE;
                      }
                      cli_process_dhcpc_cmd (CliHandle,CLI_DHCP_CLNT_CLEAR_INTERFACE_STATS,u4IfIndex);
                  }
                  else
                  {
                      cli_process_dhcpc_cmd (CliHandle,CLI_DHCP_CLNT_CLEAR_ALL_INTERFACE_STATS,NULL);
                  }

          }
SYNTAX  : clear ip dhcp client statistics [interface {vlan <VlanId(1-4094)> |<interface-type> <interface-id>}]
PRVID   : 15
HELP    : This command is used to clear the DHCP client statistics.
CXT_HELP : clear Performs clear operation |
           ip IP related configuration |
           dhcp DHCP related configuration |
           client Client related configuration |
           statistics Statistics related information |
           interface Interface related configuration |
           vlan VLAN related configuration |
           (1-4094) VLAN ID |
           DYNiftype|
           DYNifnum|
           <CR> This command is used to clear the DHCP client statistics.
END GROUP
