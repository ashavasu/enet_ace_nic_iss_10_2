/************************************************************************
 * $Id: dhcglob.h,v 1.34 2015/12/29 11:59:31 siva Exp $
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                          *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       *
 * FILE NAME      : dhcglob.h                                           *
 * LANGUAGE       : C Language                                          *
 * TARGET         : LINUX                                               *
 * AUTHOR         : A.S.Musthafa                                        *
 * DESCRIPTION    : global vaiables used for DHCP CLIENT module         *
 ************************************************************************/

#ifndef  __DHCPC_GLOB_H

#define  __DHCPC_GLOB_H

#ifdef _DHCPC_GLOB_VAR

INT4              gu4DhcpCDebugMask;

const UINT1       gau1DhcpCMagicCookie[] = { 99,130,83,99};

const UINT1       *gu1DHCPCDegMsg[] ={( CONST UINT1 *)"BOOTPREQUEST",
                                      ( CONST UINT1 *)"DHCPDISCOVER",
                                      ( CONST UINT1 *)"DHCPOFFER", 
                                      ( CONST UINT1 *)"DHCPREQUEST",
                                      ( CONST UINT1 *) "DHCPDECLINE",
                                      ( CONST UINT1 *)"DHCPACK",
                                      ( CONST UINT1 *) "DHCPNAK",
                                      ( CONST UINT1 *)"DHCPRELEASE",
                                      ( CONST UINT1 *)"DHCPINFORM",
                                      ( CONST UINT1 *) "BOOTP" };
/* Timer List used for a dummy timer. Dummy timer is posted every few seconds
 * The DHCP CLIENT task hence gets scheduled and we use select to check for any 
 * incoming frames
 */

tOsixQId         gDhcpCMsgQId;
tOsixSemId       gDhcpCSemId;
tTimerListId     DhcpCDummyTimerListId;

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
tOsixQId         gDhcpCArpMsgQId;
#endif
/* Timer List used for DHCP Client */
tTimerListId     DhcpCTimerListId;  /* Dhcp Client Timer Id */

tTmrAppTimer     gDhcpCDummyTimer;

INT4            gi4DhcpCSockDesc = -1; /* Global Socket Descriptor for DHCP client */
UINT4           gu4DhcpCTaskId;
UINT4           gu4DhcpCSelectTskId;
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
INT4            gi4DhcpCArpSockDesc = -1; /* Global Socket Descriptor for ARP Response processing */
UINT4           gu4DhcpCArpTaskId;
UINT1           gu1DhcpCArpRespProcFlag; /* Flag to Process Arp response on Raw socket */
#endif

tDhcpCTag       DhcpCTag;
tRBTree         gDhcpCReqOptionList;
tRBTree         gDhcpCidList;
extern tDhcpClientMemPoolId gDhcpClientMemPoolId;
tDhcpCFastAcc   gDhcpCFastAcc;
tDhcpCRedGlobalInfo gDhcpCRedGlobalInfo;
tTMO_SLL            gDhcpDiscOptionList;
/* List to hold the list of registered DHCP options 
*and respective implementations by the application 
*/
tTMO_SLL            gDhcpOptionRegList; 

#else /* _DHCPC_GLOB_VAR */

extern INT4              gu4DhcpCDebugMask;

extern tOsixQId         gDhcpCMsgQId;
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
extern tOsixQId         gDhcpCArpMsgQId;
#endif
extern tOsixSemId       gDhcpCSemId;
extern const UINT1       gau1DhcpCMagicCookie[];
extern UINT4 u4CidrSubnetMask[]; 

/* Timer List used for a dummy timer. Dummy timer is posted every few seconds
 * The DHCP CLIENT task hence gets scheduled and we use select to check for any 
 * incoming frames
 */
extern tTimerListId      DhcpCDummyTimerListId; 

extern tTmrAppTimer      gDhcpCDummyTimer;

extern tTimerListId      DhcpCTimerListId;      /* REPLY WAIT Timer List Id */

extern     INT4          gi4DhcpCSockDesc;
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
extern     INT4          gi4DhcpCArpSockDesc; 
extern UINT4           gu4DhcpCArpTaskId;
extern UINT1           gu1DhcpCArpRespProcFlag;
#endif

extern     UINT4         gu4DhcpCTaskId;

extern UINT4           gu4DhcpCSelectTskId;
extern     tDhcpCTag     DhcpCTag;
extern tRBTree         gDhcpCReqOptionList; /* Requested Option List */
extern tRBTree         gDhcpCidList;
extern tDhcpClientMemPoolId gDhcpClientMemPoolId;
extern tDhcpCFastAcc   gDhcpCFastAcc;
extern tDhcpCRedGlobalInfo gDhcpCRedGlobalInfo;
extern tTMO_SLL            gDhcpDiscOptionList;
extern tTMO_SLL            gDhcpOptionRegList;

#endif /* _DHCPC_GLOB_VAR */

/* Function prototypes used in DHCP client module */
VOID        DhcpCReleaseTimeoutExpiry PROTO ((tDhcpCRec   *pDhcpCNode ));
VOID        DhcpClientShutDown PROTO ((VOID));
UINT1       DhcpCValidateIpAddress PROTO ((UINT4));

VOID        DhcpCProcessPkt PROTO ((UINT1 *,UINT4 ,UINT4, UINT2 ));
UINT1       DhcpCValidateBootReply PROTO ((tDhcpPktInfo *, tDhcpCRec *));
VOID        DhcpCStartProcessing PROTO ((UINT4 u4IfIndex));
UINT4       DhcpCValidatePkt PROTO ((UINT1 *, UINT2));
UINT4       DhcpCGetPktInfo  PROTO ((tDhcpPktInfo * , UINT1 *, UINT2 ));
VOID        DhcpCCopyHeader  PROTO ((tDhcpMsgHdr *, UINT1 *));

UINT1       DhcpCInitState PROTO ((tDhcpCRec *pDhcpCNode));
UINT1       DhcpCInitRebootState PROTO ((tDhcpCRec *pDhcpCNode));
VOID        ProcessDhcpCOffer PROTO ((tDhcpPktInfo *,tDhcpCRec *pDhcpCNode));
VOID        DhcpCGetReqOptSupported PROTO ((tDhcpPktInfo *, 
                                     tDhcpCRec *pDhcpCNode, 
                                     tBestOffer *));
UINT1       DhcpCSendDecline PROTO ((tDhcpCRec *pDhcpCNode, UINT4, UINT4));
UINT1       DhcpCSendDiscover PROTO ((tDhcpCRec   *pDhcpCNode));
VOID        DhcpCFillRequiredOptions PROTO (( tDhcpPktInfo *, UINT1 *, UINT2 ));
VOID        DhcpCAddOption PROTO ((tDhcpPktInfo *,tDhcpCTag *));
INT4        DhcpCGetOption PROTO ((UINT1 ,tDhcpPktInfo *));
INT1        DhcpCSendInform PROTO ((tDhcpCRec   *pDhcpCNode));
INT1        DhcpCSendReleaseMsg PROTO ((tDhcpCRec *pDhcpCNode));
INT4        DhcpCProcessRelease PROTO ((tDhcpCRec *pDhcpCNode,  UINT1 u1Flag));
UINT1       CalRenewRebindTime PROTO ((tDhcpCRec *pDhcpCNode, UINT4 , UINT4 ));
VOID        ProcessDhcpCNAck PROTO ((tDhcpPktInfo *,tDhcpCRec *pDhcpCNode));
UINT1       DhcpCSendRequest PROTO ((tDhcpCRec   *pDhcpCNode));
VOID        DhcpCArpTimerExpiry PROTO ((tDhcpCRec   *pDhcpCNode,
                                        UINT4 ,UINT4, UINT4));
VOID        ProcessDhcpCAck PROTO ((tDhcpPktInfo *, tDhcpCRec *pDhcpCNode));
UINT1       DhcpCArpCheck  PROTO ((tDhcpPktInfo *, tDhcpCRec *pDhcpCNode));
UINT1       DhcpCOfferWithoutSelect PROTO ((tDhcpPktInfo *, 
                                            tBestOffer  **pBestOffer));
VOID        DhcpCRemoveOptionType PROTO ((UINT1 u1TagType, 
                                          tDhcpPktInfo *pPktInfo));
VOID        DhcpCopyDefaultOpt PROTO ((tDhcpCRec   *pDhcpCNode));

VOID        DhcpCFillSendMessage PROTO ((tDhcpPktInfo *,INT1 *, 
                                        UINT1 ,UINT2   *));
UINT1       DhcpCSendBroadcast PROTO ((INT1 *,UINT2 ,UINT4 ));
UINT4       DhcpCSendMessage PROTO ((tDhcpPktInfo *, UINT1));
INT1        DhcpCSendToSli PROTO ((INT1 *,UINT2 ,UINT4  , UINT2 ));
UINT1       DhcpCSendUnicast (INT1 *, UINT2 , UINT4 ,  UINT4 , UINT2);
UINT1       DhcpCRouteAdd PROTO ((UINT4, UINT4, UINT4, INT4, UINT4));
UINT1       DhcpCRouteDel PROTO ((UINT4, UINT4, UINT4, INT4, UINT4));
UINT1       DhcpCfaInterfaceIpAddrConfig PROTO ((tDhcpCRec   *pDhcpCNode ,
                                                 UINT4 u4IpAddress));
VOID        DhcpCfaGddGetHwAddr PROTO ((UINT4 , UINT1 *));
tDhcpPktInfo *   DhcpCCreateOutPkt PROTO ((VOID));

INT4        DhcpCSetReqOptionType PROTO ((UINT4 u4IfIndex, UINT4 u4OptType,
                                   UINT4 u4OptStatus));
INT4        DhcpcConfReqOptionType PROTO ((UINT4 u4IfIndex, UINT4 u4OptType,
                                   UINT4 u4OptStatus));
tDhcpCOptions * DhcpCGetReqOptionEntry PROTO((UINT4 u4IfIndex, UINT4 u4OptType));

INT4        DhcpCGetFirstIndexReqOptionEntry PROTO ((UINT4 *pu4IfIndex,
                                              UINT4 *pu4OptType));

INT4        DhcpCGetNextIndexReqOptionEntry PROTO ((UINT4 u4IfIndex,
                                             UINT4 *pu4NextIfIndex,
                                             UINT4 u4OptType,
                                             UINT4 *pu4NextOptType));

INT4        DhcpCGetReqOptionValue PROTO ((UINT4 u4IfIndex, UINT4 u4OptType,
                             UINT1 *pu1OptVal, UINT1 *pu1ErrState));

INT4
DhcpCSetClientIdentifier PROTO ((UINT4 u4IfIndex,
                          UINT1  *  pu1ClientIdentifier,
                          INT4  i4Len));

INT4
DhcpcConfigClientId PROTO ((UINT4 u4IfIndex,
                          UINT1  *  pu1ClientIdentifier,
                          INT4  i4Len));
INT4
DhcpCReqOptEntryCmp PROTO ((tRBElem * , tRBElem *));

INT4
DhcpCReqOptEntryFree PROTO ((tRBElem *));

INT4
DhcpCidEntryFree PROTO ((tRBElem *));

INT4
DhcpCidEntryCmp PROTO ((tRBElem *, tRBElem *));

INT4  
DhcpCGetClientIdentifer PROTO ((UINT4 u4IfIndex, 
                         UINT1 * pu1ClientIdStr,
                         INT4  * pi4Len));

VOID DhcpCProcessOptions PROTO ((tDhcpQMsg *));

/* Callback related functions */
UINT1
DhcpCParseSipInfo PROTO (( UINT1 *pu1Len, UINT1 *pau1OptVal, UINT4 *pu4Type ));
VOID 
DhcpCParseRegisteredOptions PROTO ((UINT4 u4DhcpOption, tDhcpPktInfo *pPktInfo));

/* Timer related functions */
VOID         DhcpCProcessTmrExpiry PROTO ((VOID));
VOID         SingleOfferTimerExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         MultipleOfferTimerExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
UINT1        DhcpCGetRemainingTime PROTO ((tTimerListId , 
                                           tTmrAppTimer * ,UINT4 *));

INT4         DhcpCInitTimer PROTO ((tDhcpCRec *pInterface, 
                                    UINT4 u4TmrId, UINT4 u4Sec));
INT4         DhcpCStartTimer PROTO ((tTimerListId, tTmrAppTimer * ,UINT4));

INT4         DhcpCStopTimer PROTO ((tTimerListId , tTmrAppTimer * ));
INT4         DhcpCReStartTimer PROTO ((tTimerListId , tTmrAppTimer * , UINT4 ));
VOID         ProcessRequestTimerExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         ProcessRenewalTimerExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         ProcessRebindTimerExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         ProcessNULLStateExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCInformTmrExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCDeclineTmrExpiry PROTO ((tDhcpCRec   *pDhcpCNode,
                                           UINT4 , UINT4 ));
VOID         DhcpCReleaseTmrExpiry PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCShowBinding PROTO ((UINT4));
UINT1        DhcpValidateValue PROTO ((UINT1 *)) ;
UINT1        DhcpCValidateConfOptions PROTO ((UINT1 , UINT4 , INT1 *));
UINT4        DhcpCGetDefaultNetMask PROTO ((UINT4 u4IpAddress));
VOID         DhcpCProcessQMsg PROTO ((VOID));
VOID         DhcpCStopProcessing PROTO ((UINT4 u4Port, UINT1 u1Flag));
INT4         DhcpCTaskInit PROTO ((void));
VOID         DhcpCPktHandler PROTO ((tCRU_BUF_CHAIN_HEADER *pRcvBuf));
VOID         DhcpCHandleIfUp PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCHandleIfDown PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCResetTxRxCounts PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCResetIfTblEntries PROTO ((tDhcpCRec  *pDhcpCNode));
VOID         DhcpCStopTimers PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCRenewIp PROTO ((tDhcpCRec   *pDhcpCNode));
VOID         DhcpCProcessOperNotification PROTO ((UINT4 u4Port));
INT4  DhcpCNotify              PROTO((UINT4 u4Port, UINT4 u4MsgType));
INT4  DhcpCSocketOpen          PROTO((VOID));
INT4  DhcpCSocketClose         PROTO((VOID));

tDhcpCRec * DhcpCGetIntfRec PROTO ((UINT4 u4IpIfIndex));
INT4        DhcpCAddIntfRec PROTO ((UINT4  u4IpIfIndex));
INT4        DhcpCDeleteIntfRec PROTO ((UINT4  u4IpIfIndex));
INT1        DhcpCGetNextMgmtIfIndex PROTO ((UINT4 , UINT4 * ));
INT4        DhcpCMemInit PROTO ((VOID));
VOID        DhcpCFlushOffer PROTO ((tDhcpCRec   *pDhcpCNode));
VOID        DhcpCMemDeInit PROTO ((VOID));
VOID        DhcpCHashNodeDeleteFn PROTO ((tTMO_HASH_NODE * pHashNode));

INT4 DhcpClientUtilConvertBitMasktoOption(INT4 *pOptions, UINT1 *u1Option, INT1 *iCnt);
INT4 DhcpClientUtilCheckOption(INT4 i1Option, tDhcpDiscOption *pDiscOptionNode);
INT4 DhcpCCheckOptionIsPresent (tDhcpPktInfo * pPkt);
#ifndef SNTP_WANTED
VOID DhcpcProcessSntpServerParams (UINT4,UINT4);
#endif

/*BOOTP related functions*/
INT4 BootpSendRequestPacket (UINT4 u4IfIndex);
VOID BootpReqTmrExpiryHdlr (tDhcpCRec *pDhcpCNode);
INT4 BootpProcessReply (tDhcpPktInfo *pPktInfo, tDhcpCRec *pDhcpCNode);
UINT2 BootpCalculateTimeOut (UINT1 u1Flag);

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
VOID DhcpCArpRespTimeoutExpiry PROTO ((tDhcpCRec   *pDhcpCNode ));
INT4 DhcpCArpRespSocketOpen PROTO ((VOID));
INT4 DhcpCArpRespSocketClose PROTO ((VOID));
VOID DhcpCArpRespProcessQMsg PROTO ((VOID));
VOID DhcpCArpRespRcvPkt (UINT4 u4Port, UINT4 u4ClientIpAddr);
UINT1 DhcpCArpRespSendEvent (tDhcpPktInfo * pPktInfo);
INT4 DhcpCArpUpdateEntry (UINT4 u4IfIndex, struct arphdr *pArpHdr);
#endif
#endif /* __DHCPC_GLOB_H */
