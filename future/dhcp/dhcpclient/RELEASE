Copyright (C) 2006 Aricent Inc . All Rights Reserved
--------------------------------------------------
PRODUCT NAME            : AricentDHCPCLIENT
-------------------------------------------

     __________________________________________________________________
    |    RELEASE NUMBER  : DHCPC_2-1-0-2                               |
    |    RELEASE DATE    : February 08 2008                            |
    '__________________________________________________________________'

    ______ CHANGELOG: _________________________________________________
     **  Modified for recvfrom() system call is not called with proper
     **  argument. 

      + src/dhcintf.c
                                                            [RFC: 21978]
     __________________________________________________________________
     **  DHCP Client has been modified to send DHCP Release message to DHCP
     **  Server 

      + src/dhcinput.c
      + inc/dhcglob.h
      + inc/dhctdf.h
      + src/dhctimer.c
      + src/dhcproc.c
      + inc/dhcdef.h
                                                            [RFC: 22675]
     __________________________________________________________________
     **  Layer3 related CLI commands available for vlan is made available
     **  for router ports. 

      + inc/dhcpccmd.def
                                                            [RFC: 23711]
     __________________________________________________________________
     **  Modified printf and scanf format specifier for 64 bit migration. 

      + src/dhcpccli.c
      + src/dhcintf.c
                                                            [RFC: 23837]
     __________________________________________________________________
     **  SemId, QId, TimerListId and TaskCreate changes for protocol
     **  modules - 64Bit migration. 

      + src/dhcmain.c
      + inc/dhcglob.h
      + src/dhcintf.c
                                                            [RFC: 23855]
     __________________________________________________________________
     **  Removed warnings for work group package modules in 64 Bit
     **  processor. 

      + src/dhcpccli.c
      + src/dhctimer.c
      + src/dhcproc.c
      + src/dhcintf.c
                                                            [RFC: 24086]
     __________________________________________________________________
     **  DHCP client state changed after releasing the ip address. 

      + src/dhctimer.c
                                                            [RFC: 25297]
     __________________________________________________________________
    (______________________________END_________________________________)

     __________________________________________________________________
    |    RELEASE NUMBER  : DHCPC_2-0-3                                 |
    |    RELEASE DATE    : October 29 2007                             |
    '__________________________________________________________________'


     Known Problems in this release
    ==============================

    o DHCP RELEASE Message is not sent out. This is because CFA resets the
      Interface Ip address before the DHCP client sends the RELEASE message.



    ______ CHANGELOG: _________________________________________________

      + INF modules updated

     __________________________________________________________________
    (______________________________END_________________________________)


     __________________________________________________________________
    |    RELEASE NUMBER  : DHCPC_2-0-2                                 |
    |    RELEASE DATE    : July 20 2007                                |
    '__________________________________________________________________'


     Known Problems in this release
    ==============================

    o DHCP RELEASE Message is not sent out. This is because CFA resets the
      Interface Ip address before the DHCP client sends the RELEASE message.



    ______ CHANGELOG: _________________________________________________
     **  Updated for FSAP Id changes for DHCP. 

      + inc/dhcglob.h
      + src/dhcintf.c
      + inc/dhcdef.h
                                                            [RFC: 19960]
     __________________________________________________________________
     **  FSAP id changes done for NETIP, TCP ,SLI, PIM and VRRP modules 

      + src/dhcintf.c
      + inc/dhcport.h
      + inc/dhcdef.h
                                                            [RFC: 20617]
     __________________________________________________________________
     **  Merging circuit id and remote id configuration support from
     **  BR_4-0-0-1 to TOS 

      + src/dhcintf.c
                                                            [RFC: 21359]
     __________________________________________________________________
     **  Diab and Klocwork fixes for L3 modules 

      + src/dhcintf.c
                                                            [RFC: 21374]
     __________________________________________________________________
    (______________________________END_________________________________)



     __________________________________________________________________
    |    RELEASE NUMBER  : DHCPC_2-0-1-4                               |
    |    RELEASE DATE    : February 23 2007                            |
    '__________________________________________________________________'

    Known Problems in this release
    ==============================

    o DHCP RELEASE Message is not sent out. This is because CFA resets the 
      Interface Ip address before the DHCP client sends the RELEASE message.
      
    ______ CHANGELOG: _________________________________________________
     **  Dhcp configures incorrect gateways offered by server. 

      + src/dhcintf.c
                                                            [RFC: 15059]
     __________________________________________________________________
     **  Merging the bug fixes done in BR-4-1-0-0 during HNS Release
     **  testing to TOS. 

      + src/dhcintf.c
                                                            [RFC: 16674]
     __________________________________________________________________
     **  Fix for the mismatch in object type of dhcpClientDebugTrace. 

      + inc/fsdhcldb.h
      + inc/fsdhclwr.h
      + src/fsdhclwr.c
                                                            [RFC: 17827]
     __________________________________________________________________
     **  Fix for issues related to 105th L3vlan intf creation & also IpIntf
     **  creation based on admin status. 

      + inc/dhcport.h
                                                            [RFC: 18112]
     __________________________________________________________________
     **  Fix for DHCP Relay to fwd OFFER msg in case of LNXIP. 

      + src/dhcintf.c
                                                            [RFC: 18233]
     __________________________________________________________________
     **  Address CLI Sanity issues in L3 Modules 

      + inc/dhcdef.h
      + src/fsdhclow.c
                                                            [RFC: 18615]
     __________________________________________________________________
     **  KlocWork fixes for VRRP, DHCP and ARP 

      + src/dhcmain.c
      + src/dhcinput.c
      + src/dhcintf.c
      + inc/dhcdef.h
      + src/dhcproc.c
      + inc/dhcglob.h
      + src/bootp.c
      + src/fsdhclow.c
      + inc/dhctdf.h
      + inc/dhcport.h
      + src/dhctimer.c
                                                            [RFC: 19104]
     __________________________________________________________________
     **  Address bootp testcase failures in isstestsuite 

      + src/dhctimer.c
                                                            [RFC: 19168]
     __________________________________________________________________
    (______________________________END_________________________________)
      __________________________________________________________________
    |    RELEASE NUMBER  :  DHCPC_2-0-1-3                              |
    |    RELEASE DATE    :  April 7th 2006                             |
    '__________________________________________________________________'

    Release Highlights
    ==================
    
    o BOOTP has been made interoperable with DHCP client.
    
    Known Problems in this release
    ==============================

    o DHCP client may learn a default route from the DHCP server such that the 
      next-hop network is not available in the client
   
    .........ADDED:......................................................

     + future/dhcp/dhcpclient/src/bootp.c

    .........OBSOLETED:..................................................

     + future/dhcp/dhcpclient/inc/fsdhcmdb.h
     + future/dhcp/dhcpclient/inc/fsdhcmid.h
     + future/dhcp/dhcpclient/src/fsdhcsnmp.c
     + future/dhcp/dhcpclient/inc/fsdhccon.h
     + future/dhcp/dhcpclient/src/fsdhcmid.c
     + future/dhcp/dhcpclient/inc/fsdhcogi.h


     ______ CHANGELOG: _________________________________________________
     **  DHCP client is modified to use NetIP to interact with IP.

                                                            [RFC: 14056]
     __________________________________________________________________
     **  API provided by ARP Module is used in DHCP Client to send ARP
     **  response after configuring IP.
     **  Sending Release messages should be avoided when an interface is
     **  deleted. Since Querying NETIP for interface existence returns
     **  an inconsistent value in FSIP, interface deletion is handled in
     **  DHCP Client without Querying NETIP.

                                                            [RFC: 14067]
     __________________________________________________________________
     **  Modified to make BOOTP interoperable with DHCP.

                                                            [RFC: 14191]
                                                            [RFC: 14230]
     __________________________________________________________________
     **  Modified to avoid MSR operation for dhcpClientConfigTable,
         Since the objects in the dhcpClientConfigtable are used as
         toggling object for sending messages, MSR operation for
         dhcpClientConfigTable is invalid.

                                                            [RFC: 14220]
     __________________________________________________________________
     **  Modified to support INIT-REBOOT state in the DHCP client.

                                                            [RFC: 14394]
     __________________________________________________________________
     **  Modified for the Compilation issues when SLI and LINUX_SLI_WANTED
     **  switches are not defined.

                                                            [RFC: 14482]
     __________________________________________________________________
     **  Modified to accept the address in bootp reply packet only 
         after checking chaddr field.

                                                            [RFC: 14537]
     __________________________________________________________________
    (______________________________END_________________________________)

