/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dhcintf.c,v 1.97 2017/09/08 12:23:53 siva Exp $
 * 
 * DESCRIPTION    : Contains interfacing function for DHCP CLIENT        
 ********************************************************************/

/************************************************************************
 * Include files                                                        *
 ************************************************************************/
#include "dhccmn.h"
#include "fsdhclcli.h"
#ifdef SNMP_2_WANTED
#include "fsdhclwr.h"
#endif
#include "rtm.h"

INT1                gi1CliRecvBuff[DHCP_MAX_MTU];
INT1                gi1CliSelRecvBuff[DHCP_MAX_MTU];
UINT4               gu4DhcpCDefRoute;

tTMO_HASH_TABLE    *gDhcpCIntfTbl = NULL;
tDhcpClientMemPoolId gDhcpClientMemPoolId;

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
extern INT4 ArpGetHwAddr PROTO ((UINT2 u2IfIndex, UINT1 *pHwAddr));
#endif

/************************************************************************/
/*  Function Name   : DhcpClientInit                                    */
/*  Description     : This function initializes the data structure used */
/*                    by the DHCP Client module and creates DHCP Client */
/*                    task                                              */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Global variables                                                    */
/*  Modified        : All the global variables used are initialized     */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT4
DhcpClientInit (VOID)
{
    DHCPC_TRC (DEBUG_TRC, "Entered DhcpClientInit \n");

    gi4DhcpCSockDesc = -1;
    gu4DhcpCTaskId = 0;
    gu4DhcpCDefRoute = 0;

    /* Create the task for DHCP client */
    if (OsixTskCrt (DHCP_CLIENT_TASK_NAME,
                    DHCP_CLIENT_TASK_PRIORITY,
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) DhcpCTaskMain,
                    0, &gu4DhcpCTaskId) != OSIX_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC,
                   "FAILURE in DhcpClientInit Client Task Creation\n");
        return DHCP_FAILURE;
    }

    /* Creating a task to select  on blocking mode */
    if (OsixTskCrt (DHCP_SELECT_CLINET_TASK_NAME,
                    DHCP_CLIENT_TASK_PRIORITY,
                    OSIX_DEFAULT_STACK_SIZE,
                    (OsixTskEntry) DhcpClntSelectTaskMain,
                    0, &gu4DhcpCSelectTskId) != OSIX_SUCCESS)
    {
        return DHCP_FAILURE;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpClientInit \n");

    return DHCP_SUCCESS;
}                                /* End of DhcpClientInit function */

/************************************************************************/
/*  Function Name   : DhcpCTaskMain                                     */
/*  Description     : Entry function for DHCP CLIENT task               */
/*  Input(s)        : pDummy -> Dummy pointer                           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCTaskMain (INT1 *pDummy)
{
    UINT4               u4Event = 0;

    UNUSED_PARAM (pDummy);

    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpCTaskMain fn\n");

    if (OsixTskIdSelf (&gu4DhcpCTaskId) == OSIX_FAILURE)
    {
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (DhcpCTaskInit () == DHCP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "!!! DHCPC Task Init Failure !!!\n");
        /* Indicate the status of initialization to the main routine */
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (DhCRedInitGlobalInfo () == OSIX_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "!!! DHCPC Redundancy Init Failure !!!\n");
        /* Clean up allocated resources */
        DhcpClientShutDown ();
        /* Indicate the status of initialization to the
         * main routine */
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    DHCP_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the protocol MIB with SNMP */
    RegisterFSDHCL ();
#endif

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
    /* Task created for receiving ARP response packets */
    if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
    {
        if (OsixTskCrt (DHCP_CLIENT_ARP_TASK_NAME,
                        DHCP_CLIENT_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        (OsixTskEntry) DhcpClntArpRespTaskMain,
                        0, &gu4DhcpCArpTaskId) != OSIX_SUCCESS)
        {
            return;
        }
    }
#endif

    while (1)
    {
        OsixEvtRecv (gu4DhcpCTaskId,
                     (DHCPC_TIMER_EXPIRY | DHCPC_MSGQ_IF_EVENT),
                     (OSIX_WAIT | OSIX_EV_ANY), &u4Event);

        DHCPC_TRC1 (DEBUG_TRC, "Rcvd Event %d\n", u4Event);

        if ((u4Event & DHCPC_MSGQ_IF_EVENT) == DHCPC_MSGQ_IF_EVENT)
        {
            DhcpCProcessQMsg ();
        }

        if ((u4Event & DHCPC_TIMER_EXPIRY) == DHCPC_TIMER_EXPIRY)
        {
            DhcpCProcessTmrExpiry ();
        }
    }
}                                /* End of DhcpCTaskMain function */

/************************************************************************/
/*  Function Name   : DhcpClntSelectTaskMain                                      */
/*  Description     : This function starts the offer reuse and lease    */
/*                  : timer and creates a socket for UDP port 67.The    */
/*                    event loop via select call is used for receiving  */
/*                    messages from DHCP client/Relay agent.            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpClntSelectTaskMain (INT1 *i1pParam)
{

    struct sockaddr_in  PeerAddr;
    dhcpc_fd_set        readFds;
    INT4                i4RetVal, i4DataLen, i4AddrLen;
    struct msghdr       PktInfo;
    struct cmsghdr      CmsgInfo;
    struct in_pktinfo  *pIpPktInfo;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tDhcpQMsg          *pDhcpQMsg;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (OsixTskIdSelf (&gu4DhcpCSelectTskId) == OSIX_FAILURE)
    {
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#ifdef BSDCOMP_SLI_WANTED
    UINT1               au1Cmsg[24];    /* For storing Auxillary Data - IP Packet INFO. */
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpCIntSelectTaskMain fn\n");

    UNUSED_PARAM (i1pParam);
    gi4DhcpCSockDesc = -1;

    if (DhcpCSocketOpen () != DHCP_SUCCESS)
    {
        /* Opening DHCP Client Socket failed */
        DHCPC_TRC (FAIL_TRC, "DHCP client socket creation FAILURE\n");
        /* Indicate the status of initialization to the main routine */
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Indicate the status of initialization to the main routine */
    DHCP_INIT_COMPLETE (OSIX_SUCCESS);

    /* Initialize the DHCP Discovery option List for registering the DHCP
     * options from various callback functions */
    TMO_SLL_Init (&gDhcpDiscOptionList);

    /* Initialize the GlobalList for the Options that 
     * registers from Application */

    TMO_SLL_Init (&gDhcpOptionRegList);

    while (1)
    {

        if (gi4DhcpCSockDesc < 0)
        {
            OsixDelayTask (1);    /* sleep for 1 sec */
            continue;
        }

        /* each time the descriptors must be  initialized
         * for updating the current status */
        MEMSET (&readFds, 0, sizeof (dhcpc_fd_set));
        FD_ZERO (&readFds);

        /* Set the list of  socket descriptors which needs 
         * to be verified for  reading */
        FD_SET (gi4DhcpCSockDesc, &readFds);

        pDhcpCNode = NULL;

        if ((i4RetVal = select (gi4DhcpCSockDesc + 1, &readFds,
                                NULL, NULL, NULL)) >= 0)
        {

            if (FD_ISSET (gi4DhcpCSockDesc, &readFds))
            {
                PktInfo.msg_control = (VOID *) &CmsgInfo;
                PktInfo.msg_controllen = 2 * sizeof (CmsgInfo);

                MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
                i4AddrLen = sizeof (PeerAddr);

#ifdef SLI_WANTED
                /* Call recvfrom to receive the packet */
                if ((i4DataLen =
                     recvfrom (gi4DhcpCSockDesc, gi1CliRecvBuff,
                               (INT4) DHCP_MAX_MTU, 0,
                               (struct sockaddr *) &PeerAddr, &i4AddrLen)) > 0)
                {
                    /* Get the interface index from which the packet 
                     * is received */
                    recvmsg (gi4DhcpCSockDesc, &PktInfo, 0);
#else
                MEMSET (gi1CliRecvBuff, 0, DHCP_MAX_MTU);
                MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

                PktInfo.msg_name = (void *) &PeerAddr;
                /* Broadcast packet sent by us over the socket is received by 
                 * us when BROADCAST option is set, So ignore such packets */
                if (NetIpv4IfIsOurAddress (PeerAddr.sin_addr.s_addr) ==
                    NETIPV4_SUCCESS)
                {
                    continue;
                }
                PktInfo.msg_namelen = sizeof (struct sockaddr_in);
                Iov.iov_base = gi1CliRecvBuff;
                Iov.iov_len = sizeof (gi1CliRecvBuff);
                PktInfo.msg_iov = &Iov;
                PktInfo.msg_iovlen = 1;
                PktInfo.msg_control = (void *) au1Cmsg;
                PktInfo.msg_controllen = sizeof (au1Cmsg);

                pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
                pCmsgInfo->cmsg_level = SOL_IP;
                pCmsgInfo->cmsg_type = IP_PKTINFO;
                pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
                UNUSED_PARAM (i4AddrLen);
                if ((i4DataLen = recvmsg (gi4DhcpCSockDesc, &PktInfo, 0)) < 0)
                {
                    perror ("DHCPC - recvmsg Failed \r\n");
                }
                else
                {
#endif
                    pIpPktInfo = (struct in_pktinfo *) (VOID *)
                        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

                    /* Validate Interface for the Packet Received */
                    pDhcpCNode = DhcpCGetIntfRec (pIpPktInfo->ipi_ifindex);

                    if (pDhcpCNode == NULL)
                    {
                        DHCPC_TRC (FAIL_TRC,
                                   "Packet Received on Invalid DHCP Interface \r\n");
                        continue;
                    }

                    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (DHCP_MAX_MTU,
                                                              0)) == NULL)
                    {
                        continue;
                    }
                    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0,
                            CRU_BUF_NAME_LEN);
                    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "DhcpClSelTM");
                    CRU_BUF_Copy_OverBufChain (pBuf,
                                               (UINT1 *) gi1CliRecvBuff, 0,
                                               i4DataLen);
                    /* Copy the interface index and source IP address
                     * in to the reserved members of tMODULE_DATA 
                     * structure  */

                    CRU_BUF_Set_U4Reserved1 (pBuf, pIpPktInfo->ipi_ifindex);

                    CRU_BUF_Set_U4Reserved2 (pBuf, pIpPktInfo->ipi_addr.s_addr);

                    if ((pDhcpQMsg =
                         (tDhcpQMsg *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                                       DhcpCQMsgPoolId)) ==
                        NULL)
                    {
                        DHCPC_TRC (FAIL_TRC, "Q message allocation failure\n");
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        continue;
                    }
                    pDhcpQMsg->u4MsgType = DHCPC_PKT_ARRIVED;
                    pDhcpQMsg->dhcpRcvBuf = pBuf;
                    if (OsixQueSend (gDhcpCMsgQId,
                                     (UINT1 *) &pDhcpQMsg,
                                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        MemReleaseMemBlock (gDhcpClientMemPoolId.
                                            DhcpCQMsgPoolId,
                                            (UINT1 *) pDhcpQMsg);
                        continue;
                    }

                    OsixEvtSend (gu4DhcpCTaskId, DHCPC_MSGQ_IF_EVENT);
                }
            }
        }
    }
}

INT4
DhcpCSocketOpen (VOID)
{
    struct sockaddr_in  DhcpCAddr;
    INT4                i4RetVal;
    INT4                i4OptVal = DHCP_ENABLE_STATUS;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpCSocketOpen fn\n");

    /* Opening of DHCP Client Socket */
    gi4DhcpCSockDesc = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gi4DhcpCSockDesc < 0)
    {
        /* Opening DHCP Client Socket failed */
        DHCPC_TRC (FAIL_TRC, "DHCP client socket creation FAILURE\n");
        return (DHCP_FAILURE);
    }

    /* Bind a address and port to the socket */
    DhcpCAddr.sin_family = AF_INET;
    DhcpCAddr.sin_addr.s_addr = 0;    /*INADDR_ANY */
    DhcpCAddr.sin_port = OSIX_HTONS (DHCP_PORT_CLIENT);

    i4RetVal = bind (gi4DhcpCSockDesc, (struct sockaddr *) &DhcpCAddr,
                     sizeof (DhcpCAddr));

    if (i4RetVal < 0)
    {
        /* Binding of DHCP Client socket failed */
        DHCPC_TRC (FAIL_TRC, "Socket Bind is not successful\n");
        DhcpCSocketClose ();
        return (DHCP_FAILURE);
    }

    /* set the Socket BroadCast Option */
    if (setsockopt (gi4DhcpCSockDesc, SOL_SOCKET, SO_BROADCAST,
                    &i4OptVal, sizeof (i4OptVal)) < 0)
    {
        DHCPC_TRC (FAIL_TRC, "Setsockopt(BROADCAST) is not successful\n");
        DhcpCSocketClose ();
        return (DHCP_FAILURE);
    }

    /* set the IP level option */
    if (setsockopt (gi4DhcpCSockDesc, IPPROTO_IP,
                    IP_PKTINFO, &i4OptVal, sizeof (i4OptVal)) < 0)
    {
        DHCPC_TRC (FAIL_TRC, "Setsockopt(IPPROTO_IP) is not successful\n");
        DhcpCSocketClose ();
        return (DHCP_FAILURE);
    }
    return (DHCP_SUCCESS);
}

INT4
DhcpCSocketClose (VOID)
{
    INT4                i4RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpCSocketClose fn\n");

    if (gi4DhcpCSockDesc != -1)
    {
        i4RetVal = close (gi4DhcpCSockDesc);

        if (i4RetVal < 0)
        {
            /* DHCP Client socket close failed */
            DHCPC_TRC (FAIL_TRC,
                       "DHCP Client Socket Close is not successful\n");
            return (DHCP_FAILURE);
        }
    }

    gi4DhcpCSockDesc = -1;

    return (DHCP_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpCStartTimer                                   */
/*  Description     : This function start a timer for 'u4Sec' duration. */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*                  : u4Sec - Timer expiry time in seconds.             */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec)
{
    UINT4               u4Ticks;

    if (DHCPC_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhcpCStartTimer: Timer not initialised for STANDBY node \r\n");
        return DHCP_FAILURE;
    }

    /* This function start a timer for u4Sec Duration .. */

    u4Ticks = u4Sec * SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (TmrStartTimer (TimerList, pTimer, u4Ticks) == TMR_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC, "Starting problem with Timer");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCGetRemainingTime                             */
/*  Description     : This function gets remaining timer duration.     */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*  Output(s)       : pTimeLeft- Time Left out for the timer        */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCGetRemainingTime (tTimerListId TimerList, tTmrAppTimer * pTimer,
                       UINT4 *pTimeLeft)
{
    UINT4               u4TicksLeft = 0;
    UINT4               u4RetVal;

    u4RetVal = TmrGetRemainingTime (TimerList, pTimer, &u4TicksLeft);

    if (u4RetVal != TMR_SUCCESS)
    {
        return DHCP_FAILURE;
    }
    /* Convert ticks to secs */
    *pTimeLeft = u4TicksLeft / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (u4TicksLeft < SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
    {
        /* Return number of ticks, if its less
         * than a sec. */
        *pTimeLeft = u4TicksLeft;
    }

    return DHCP_SUCCESS;
}                                /* End Of DhcpCGetRemainingTime function */

/************************************************************************/
/*  Function Name   : DhcpCReStartTimer                                 */
/*  Description     : This function restart a timer for u4Sec duration. */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*                  : u4Sec - Timer expiry time in seconds.             */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCReStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec)
{
    UINT4               u4Ticks;

    TmrStopTimer (TimerList, pTimer);

    if (DHCPC_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhcpCReStartTimer: Timer not initialised for STANDBY node \r\n");
        return DHCP_FAILURE;
    }

    u4Ticks = u4Sec * SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (TmrStartTimer (TimerList, pTimer, u4Ticks) == TMR_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC, "Starting problem with Timer");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCStopTimer                                    */
/*  Description     : This function stops a timer                       */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCStopTimer (tTimerListId TimerList, tTmrAppTimer * pTimer)
{

    if (pTimer != NULL)
    {
        if (TmrStopTimer (TimerList, pTimer) != TMR_SUCCESS)
        {
            return DHCP_FAILURE;
        }
    }
    return DHCP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : DhcpCSendBroadcast                                */
/*  Description     : This function is called for broadcasting DHCP     */
/*                    Messages over the interface - u4IfIndex. Calls    */
/*                    DhcpSendToSli(), for transmitting UDP message.    */
/*  Input(s)        : pMessage - DHCP message to be sent.               */
/*                    u2Size - size of the message.                     */
/*                    u4IfIndex - Ip interface.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/
UINT1
DhcpCSendBroadcast (INT1 *pMessage, UINT2 u2Size, UINT4 u4IfIndex)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
    struct cmsghdr     *pCmsgInfo;
    struct sockaddr_in  RemoteAddr;
    UINT1               u1RetVal = DHCP_SUCCESS;
    UINT4               u4Dest = 0xffffffff;
    UINT2               u2Port = DHCP_PORT_SERVER;
    UINT1               au1Cmsg[24];    /* For storing Auxillary Data -
                                           IP Packet INFO. */
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (void *) &RemoteAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = (UINT1 *) pMessage;
    Iov.iov_len = u2Size;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif

    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = (INT4) u4IfIndex;
    pIpPktInfo->ipi_addr.s_addr = u4Dest;
    pIpPktInfo->ipi_spec_dst.s_addr = 0;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2Port);

    if (DHCPC_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhcpCSendBroadcast: Socket Write not allowed for STANDBY node \r\n");
        return DHCP_FAILURE;
    }
    if (sendmsg (gi4DhcpCSockDesc, &PktInfo, 0) < 0)
    {
        perror ("sendmsg \r\n");
    }
#ifdef SLI_WANTED
    u1RetVal = DhcpCSendToSli (pMessage, u2Size, u4Dest, u2Port);
#endif
    return u1RetVal;
}                                /* End of DhcpCSendBroadcast function */

/************************************************************************/
/*  Function Name   : DhcpCSendUnicast                                  */
/*  Description     : This function is called for sending DHCP unicast  */
/*                    Messages over the interface - u4IfIndex. Calls    */
/*                    DhcpSendToSli(), for transmitting UDP message     */
/*  Input(s)        : pMessage - DHCP message to be sent                */
/*                    u2Size - size of the message                      */
/*                    u4Dest - destination IP address                   */
/*                    u4IfIndex - Ip interface                          */
/*                    u2Port - UDP destination port number              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/
UINT1
DhcpCSendUnicast (INT1 *pMessage, UINT2 u2Size, UINT4 u4Dest,
                  UINT4 u4IfIndex, UINT2 u2Port)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo;
    struct sockaddr_in  RemoteAddr;
    UINT1               u1RetVal = DHCP_SUCCESS;
    UINT1               au1Cmsg[24];    /* For storing Auxillary Data - 
                                           IP Packet INFO. */
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&RemoteAddr, 0, sizeof (struct sockaddr_in));
    PktInfo.msg_name = (void *) &RemoteAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = (UINT1 *) pMessage;
    Iov.iov_len = u2Size;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif

    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    pIpPktInfo->ipi_ifindex = (INT4) u4IfIndex;

    pIpPktInfo->ipi_addr.s_addr = u4Dest;
    pIpPktInfo->ipi_spec_dst.s_addr = 0;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2Port);

    if (DHCPC_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhcpCSendUnicast: Socket Write not allowed for STANDBY node \r\n");
        return DHCP_FAILURE;
    }
    if (sendmsg (gi4DhcpCSockDesc, &PktInfo, 0) < 0)
    {
        perror ("sendmsg \r\n");
    }
#ifdef SLI_WANTED
    u1RetVal = (UINT1) DhcpCSendToSli (pMessage, u2Size, u4Dest, u2Port);
#endif
    return u1RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpCSendMessage                                  */
/*  Description     : This function sends DHCP Message - either unicast */
/*                    depending upon the u1MessageType and 'giaddr'.    */
/*                    This function calls DhcpSendToSli() function for  */
/*                    Transmitting UDP messages.                        */
/*                                                                      */
/*  Input(s)        : pOutPkt - Outgoing DHCP packet                    */
/*                    u4ciaddr - destination address                    */
/*                    u1MessageType - DHCP OFFER/ACK/NAK                */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/
UINT4
DhcpCSendMessage (tDhcpPktInfo * pOutPkt, UINT1 u1MessageType)
{
    UINT4               u4giaddr;
    UINT2               u2Size;
    UINT1               u1BcastFlag;
    UINT4               u4Retval = DHCP_FAILURE;
    UINT4               u4Dest;
    tDhcpCMessage      *pMessage = NULL;

    pMessage =
        (tDhcpCMessage *) MemAllocMemBlk (gDhcpClientMemPoolId.DhcpCMessageId);
    if (pMessage == NULL)
    {
        DHCPC_TRC (EVENT_TRC, "MemAlloc failed:: DhcpCSendMessage\r\n");
        return DHCP_FAILURE;
    }
    MEMSET (pMessage, 0, sizeof (tDhcpCMessage));

    DHCPC_TRC (EVENT_TRC, "Entered DhcpCSendMessage fn\n");

    DhcpCFillSendMessage (pOutPkt, pMessage->asDhcpCMessage, u1MessageType,
                          &u2Size);

    u4giaddr = pOutPkt->DhcpMsg.giaddr;

    if (pOutPkt->DhcpMsg.flags & DHCP_BROADCAST_MASK)
        u1BcastFlag = DHCP_SET;
    else
        u1BcastFlag = DHCP_NOT_SET;
    if (u4giaddr != 0)
    {
        /* Send the packet to the gateway */
        u4Dest = u4giaddr;
    }
    else
    {
        u4Dest =
            (u1BcastFlag ==
             DHCP_SET) ? IP_GEN_BCAST_ADDR : pOutPkt->u4DstIpAddr;
    }
    if (u4Dest == IP_GEN_BCAST_ADDR)
    {
        /*  Client broadcasts messages to 0xffffffff */
        u4Retval =
            DhcpCSendBroadcast (pMessage->asDhcpCMessage, u2Size,
                                pOutPkt->u4IfIndex);
    }
    else
    {
        /* Unicast the Packet to  Server/Relay */
        u4Retval =
            DhcpCSendUnicast (pMessage->asDhcpCMessage, u2Size,
                              u4Dest, pOutPkt->u4IfIndex, DHCP_PORT_SERVER);
    }
    /*Release the Allocated Memory */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCMessageId,
                        (UINT1 *) pMessage);
    return u4Retval;
}                                /* End of DhcpCSendMessage function */

/************************************************************************/
/*  Function Name   : DhcpCSendToSli                                    */
/*  Description     : This function calls sendto() for sending UDP  */
/*                  : messages to 'u2DstPort' at 'u4DstIp' address.     */
/*  Input(s)        : pMessage - Message to be sent.                    */
/*                    u2Size - size of the message.                     */
/*                    u4DstIp - destination Ip-Address.                 */
/*                    u2DstPort - UDP destination port number.          */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT1
DhcpCSendToSli (INT1 *pMessage, UINT2 u2Size, UINT4 u4DstIp, UINT2 u2DstPort)
{
    struct sockaddr_in  RemoteAddr;
    INT4                i4Retval = DHCP_FAILURE;

    DHCPC_TRC (DEBUG_TRC, "Sending message to SLI\n");

    if (DHCPC_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhcpCSendToSli: Socket Write not allowed for STANDBY node \r\n");
        return DHCP_FAILURE;
    }

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4DstIp);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2DstPort);

    i4Retval = sendto (gi4DhcpCSockDesc, pMessage, u2Size, 0,
                       (struct sockaddr *) &RemoteAddr, sizeof (RemoteAddr));

    return (((i4Retval < 0) ? DHCP_FAILURE : DHCP_SUCCESS));
}

/************************************************************************/
/*  Function Name   : DhcpCFillSendMessage                              */
/*  Description     : This function copies pOutPkt info to pMessage     */
/*  Input(s)        : pOutPkt - Outgoing Dhcp Message (local structure) */
/*                    pMessage - Outgoing Linear buffer.                */
/*                    u1MessageType - DHCP message type                 */
/*                    pu2Size - Pointer to packet size                  */
/*  Output(s)       : pMessage , pu2Size                                */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpCFillSendMessage (tDhcpPktInfo * pOutPkt, INT1 *pMessage,
                      UINT1 u1MessageType, UINT2 *pu2Size)
{
    UINT2               Offset = 0;
    tDhcpMsgHdr        *pHdr;

    pHdr = &(pOutPkt->DhcpMsg);

    pMessage[Offset++] = BOOTREQUEST;
    pMessage[Offset++] = pHdr->htype;
    pMessage[Offset++] = pHdr->hlen;
    pMessage[Offset++] = pHdr->hops;

    *(UINT4 *) (VOID *) (pMessage + Offset) = pHdr->xid;
    Offset += 4;

    *(UINT2 *) (VOID *) (pMessage + Offset) = (UINT2) OSIX_HTONS (pHdr->secs);
    Offset += 2;

    *(UINT2 *) (VOID *) (pMessage + Offset) = (UINT2) OSIX_HTONS (pHdr->flags);
    Offset += 2;

    *(UINT4 *) (VOID *) (pMessage + Offset) = OSIX_HTONL (pHdr->ciaddr);
    Offset += 4;

    *(UINT4 *) (VOID *) (pMessage + Offset) = OSIX_HTONL (pHdr->yiaddr);
    Offset += 4;

    *(UINT4 *) (VOID *) (pMessage + Offset) = OSIX_HTONL (pHdr->siaddr);
    Offset += 4;

    *(UINT4 *) (VOID *) (pMessage + Offset) = OSIX_HTONL (pHdr->giaddr);
    Offset += 4;

    MEMCPY (pMessage + Offset, pHdr->chaddr, 16);
    Offset += 16;
    MEMCPY (pMessage + Offset, pHdr->sname, 64);
    Offset += 64;
    MEMCPY (pMessage + Offset, pHdr->file, 128);
    Offset += 128;

    /* Add DHCP Magic cookie */
    MEMCPY (pMessage + Offset, gau1DhcpCMagicCookie, DHCP_LEN_MAGIC);
    Offset += DHCP_LEN_MAGIC;

    if (u1MessageType != BOOTP_PKT)
    {
        /* Add DHCP message type option tag */
        pMessage[Offset] = DHCP_OPT_MSG_TYPE;
        pMessage[Offset + 1] = 1;
        pMessage[Offset + 2] = u1MessageType;
        Offset += 3;
    }

    /* copy the options field */
    MEMCPY (pMessage + Offset, pHdr->pOptions, pOutPkt->u2Len);
    Offset += pOutPkt->u2Len;

    /* Add the option-overload option if the filename sname field is 
     * overloaded. */
    if (pOutPkt->u1OverLoad != 0)
    {
        pMessage[Offset] = DHCP_OPT_OVERLOAD;
        pMessage[Offset + 1] = 1;
        pMessage[Offset + 2] = pOutPkt->u1OverLoad;
        Offset += 3;
    }

    /* Add the end option */
    pMessage[Offset++] = (INT1) DHCP_OPT_END;

    /* Add the padding */
    if (u1MessageType == BOOTP_PKT)
    {
        if (Offset < BOOTP_MIN_LEN)
        {
            while (Offset < BOOTP_MIN_LEN)
            {
                pMessage[Offset++] = (INT1) DHCP_OPTION_PAD;
            }
        }
    }

    *pu2Size = Offset;

}

/************************************************************************/
/*  Function Name   : DhcpCCreateOutPkt                                 */
/*  Description     : This function allocate memory for the outgoing    */
/*                    dhcp packet.                                      */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the outgoing dhcp packet or NULL       */
/************************************************************************/

tDhcpPktInfo       *
DhcpCCreateOutPkt ()
{
    tDhcpPktInfo       *pOutPkt = NULL;

    if ((pOutPkt =
         (tDhcpPktInfo *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                          DhcpCOutPktPoolId)) == NULL)
    {
        return NULL;
    }
    if ((pOutPkt->DhcpMsg.pOptions =
         (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                   DhcpCDhcpMsgOptPoolId)) == NULL)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                            (UINT1 *) pOutPkt);
        return NULL;
    }

    /* Initialize the OutPkt */
    pOutPkt->u2BufLen = DHCP_DEF_MAX_MSGLEN;
    pOutPkt->u2MaxMessageSize = DHCP_DEF_MAX_MSGLEN;
    pOutPkt->u1OverLoad = 0;

    pOutPkt->u2Len = 0;
    return pOutPkt;
}

/************************************************************************/
/*  Function Name   : DhcpCValidateConfOptions                           */
/*  Description     : This function validate the options depending      */
/*                  : upon the length and type as specified in RFC 2132.*/
/*  Input(s)        : u1Type - type of the option.                      */
/*                    u1Len - Length of the option.                     */
/*                    pValue- option value                              */
/*  Returns         : result of comparison                              */
/************************************************************************/

UINT1
DhcpCValidateConfOptions (UINT1 u1Type, UINT4 u4Len, INT1 *pValue)
{

    if (u4Len > DHCP_MAX_OPT_LEN)
    {
        return DHCP_FAILURE;
    }

    /* *INDENT-OFF* */
    
    switch (u1Type)
    {
            /* Options With Specific Values */
        case 19: case 20: case 27: case 29: case 30:
        case 31: case 34: case 36: case 39:

            if ((((INT1)ATOI(pValue)) == 0) || (((INT1)ATOI(pValue)) == 1))
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* Options With Minimum Length 1 */
        case 12: case 14: case 15: case 17: case 18:
        case 40: case 43: case 47: case 64: case 66:
        case 67:
            if (u4Len < 1)
                return DHCP_FAILURE;
            else
                return DHCP_SUCCESS;

            /* Options With Minimum Length 4 */
        case 16:

            if (u4Len < 4)
                return DHCP_FAILURE;
            else
                return DHCP_SUCCESS;

            /* Options With Length 2 and (2*n) */

        case 25:

            if ((u4Len != 0) && (u4Len % 2 == 0))
            {
                return DHCP_SUCCESS;
            }

            return DHCP_FAILURE;

            /* Options With Minimum Length 0 and (4*n) */

        case 68:

            if (u4Len % 4 == 0)
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* With Minimum Length 4 and (4*n) */

        case 4: case 5: case 7: case 8:
        case 9: case 10: case 11: case 41: case 42: case 44:
        case 45: case 48: case 49: case 65: case 69: case 70:
        case 71: case 72: case 73: case 74: case 75: case 76:

            if ((u4Len != 0) && (u4Len % 4 == 0))
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* With Minimum Length 8 and (8*n) */

        case 21: case 33:

            if ((u4Len != 0) && (u4Len % 8 == 0))
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* Should Not Allow Configuration */
        case 0: case 255: case 50: case 52: case 53: case 54: 
        case 56: case 57: case 58: case 59: case 60: case 1:
        case 3: case 6:

            return DHCP_FAILURE;

        default:
            return DHCP_SUCCESS;

    }
    /* *INDENT-ON* */
}

/************************************************************************/
/*  Function Name   : DhcpCShowBinding                                   */
/*  Description     : Prints binding informations.Uses PRINTF()         */
/*  Input(s)        : u4IfIndex --> Interface Index                     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCShowBinding (UINT4 u4IfIndex)
{
    UINT1               i = 0, j = 0;
    UINT4               add;
    UINT1               clientid[DHCP_MAX_CID_LEN];    /* Client ID */
    UINT1               chaddr[DHCP_MAX_CHWADDR_LEN];    /* client hardware address */
    UINT4               u4RemainingPeriod;
    UINT1               u1HwType;
    UINT4               u4CurrentTime;
    tDhcpCRec          *pDhcpCNode = NULL;

    /* handle the following lines very carefully. you need lot of 
     * time for correct formating of the prints
     * */
    PRINTF ("Current DHCP Client Bindings for %x Intf are:\n", u4IfIndex);
    PRINTF (" ______________________________________________________ \n");
    PRINTF ("|      client-id         |   IpAddress  | expiretime   |\n");
    PRINTF ("|      ~~~~~~~~~         |   ~~~~~~~~~  | ~~~~~~~~~~   |\n");

    DhcpCfaGddGetHwAddr (u4IfIndex, chaddr);

    for (i = 0, j = 0; i < 6; i++, j += 3)
        SPRINTF ((char *) (clientid + j), "%02x:", chaddr[i]);

    j--;
    SPRINTF ((char *) (clientid + j), "   ");

    u1HwType = HWTYPE_ETHERNET;

    PRINTF ("|%d> %s", u1HwType, clientid);

    pDhcpCNode = DhcpCGetIntfRec (u4IfIndex);

    if (pDhcpCNode == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "DHCP Client is not enabled on Interface\n");
        return;
    }
    add = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;

    PRINTF (" | %u.%u.%u.%u \t", add >> 24, (add >> 16) & 0x000000ff,
            (add >> 8) & 0x000000ff, add & 0x000000ff);

    /* Get the current time in secs */
    OsixGetSysTime (&u4CurrentTime);
    u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    /* 
       Remaining time period of lease = Time of Expiration of Lease - Current time     Time Of Expiration of Lease    = u4Secs + u4LeaseTime 
     */

    /* Here Remaining time period of lease should be greater than 0 
       for calculating the renewal and rebind time period */

    u4RemainingPeriod = (pDhcpCNode->DhcIfInfo).u4Secs +
        (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred - u4CurrentTime;

    PRINTF ("|   %04u       |\n", u4RemainingPeriod);

    PRINTF ("|________________________|______________|______________|\n");
}

/************************************************************************/
/*  Function Name   : DhcpCfaInterfaceIpAddrConfig                     */
/*  Description     : Wrapper function for CfaDefaultInterfaceConfig    */
/*  Input(s)        : pDhcpCNode --> DHCP Client Interface Record       */
/*                  : u4IpAddr  --> IP Address                          */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCfaInterfaceIpAddrConfig (tDhcpCRec * pDhcpCNode, UINT4 u4IpAddr)
{
    UINT4               u4CfaIfIndex;
    tIpInterfaceParams  IpParams;
    UINT4               u4NetAddr;
    UINT4               u4NetMask;
    UINT4               u4NextHop;
    INT4                i4Metric;
    tDhcpPktInfo       *pPktInfo = NULL;
    tBestOffer         *pBestOffer = NULL;
#ifdef DNS_RELAY_WANTED
    UINT4               u4TempIpAddr = 0;
#endif

    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    MEMSET (&IpParams, 0, sizeof (tIpInterfaceParams));

    IpParams.u4IpAddress = u4IpAddr;

    pBestOffer = (pDhcpCNode->DhcIfInfo).pBestOffer;
    if (u4IpAddr != 0)
    {
        if (DhcpCGetOption (DHCP_OPT_SUBNET_MASK,
                            &(pBestOffer->PktInfo)) == DHCP_FOUND)
        {
            MEMCPY (&IpParams.u4SubnetMask, DhcpCTag.Val, DhcpCTag.Len);
            IpParams.u4SubnetMask = OSIX_NTOHL (IpParams.u4SubnetMask);
        }
        else
        {
            /* Get the default Subnet mask */
            IpParams.u4SubnetMask =
                DhcpCGetDefaultNetMask (IpParams.u4IpAddress);
        }
    }
    else
    {
        IpParams.u4SubnetMask = 0;
    }

    if (CfaInterfaceConfigIpParams (u4CfaIfIndex, IpParams) == CFA_SUCCESS)
    {

        if (pBestOffer != NULL)
        {
            pPktInfo = &(pBestOffer->PktInfo);

            /* Adding the route entry for the Server Which has offerred */
            if (pPktInfo->DhcpMsg.giaddr != 0)
            {
                if (DhcpCGetOption (DHCP_OPT_SERVER_ID, pPktInfo) ==
                    DHCP_NOT_FOUND)
                {
                    DHCPC_TRC (DEBUG_TRC, "Server ip not found\n");
                }

                else
                {
                    MEMCPY (&u4NetAddr, DhcpCTag.Val, sizeof (UINT4));
                    u4NetAddr = OSIX_NTOHL (u4NetAddr);
                    u4NetMask = DhcpCGetDefaultNetMask (u4IpAddr);
                    u4NextHop = pPktInfo->DhcpMsg.giaddr;
                    i4Metric = DHCP_CIDR_ROUTE_METRIC;

                    if ((DhcpCRouteAdd (u4NetAddr, u4NetMask,
                                        u4NextHop, i4Metric,
                                        pPktInfo->u4IfIndex)) != DHCP_SUCCESS)
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "Route addition to server FAILED\n");
                    }
                }
            }

            /*Add the default route if server offered the same */

            if (DhcpCGetOption (DHCP_OPT_ROUTER_OPTION, pPktInfo) ==
                DHCP_NOT_FOUND)
            {
                if (gu4DhcpCDefRoute != 0)
                {
                    /* Default route already set. So delete the default route */
                    u4NetAddr = DHCPC_DEF_ROUTE_ADDRESS;
                    u4NetMask = DHCPC_DEF_ROUTE_MASK;
                    i4Metric = DHCP_DEF_ROUTE_METRIC;
                    if ((DhcpCRouteDel (u4NetAddr, u4NetMask,
                                        gu4DhcpCDefRoute, i4Metric,
                                        pPktInfo->u4IfIndex)) != DHCP_SUCCESS)
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "Default Route deletion failed\n");
                    }
                    else
                    {
                        gu4DhcpCDefRoute = 0;
                    }

                }

                DHCPC_TRC (DEBUG_TRC, "Default route not found");
            }

            else
            {
                u4NetAddr = DHCPC_DEF_ROUTE_ADDRESS;
                u4NetMask = DHCPC_DEF_ROUTE_MASK;
                MEMCPY (&u4NextHop, DhcpCTag.Val, sizeof (UINT4));
                u4NextHop = OSIX_NTOHL (u4NextHop);
                i4Metric = DHCP_DEF_ROUTE_METRIC;

                if (((gu4DhcpCDefRoute != 0) && (u4NextHop != gu4DhcpCDefRoute))
                    || (gu4DhcpCDefRoute == 0))
                {
                    /* The default route already configured is changed */
                    if (gu4DhcpCDefRoute != 0)
                    {
                        /* Delete the previous default route given by the server */
                        if ((DhcpCRouteDel (u4NetAddr, u4NetMask,
                                            gu4DhcpCDefRoute, i4Metric,
                                            pPktInfo->u4IfIndex)) !=
                            DHCP_SUCCESS)
                        {
                            DHCPC_TRC (DEBUG_TRC,
                                       "Default Route deletion failed\n");
                        }
                    }

                    /*Add the default route when the next hop is in offered IP network
                     * and the IP address is just aquired*/
                    if (((u4NextHop & IpParams.u4SubnetMask) ==
                         (IpParams.u4IpAddress & IpParams.u4SubnetMask)) &&
                        (IpParams.u4IpAddress != 0))
                    {
                        if ((DhcpCRouteAdd (u4NetAddr, u4NetMask,
                                            u4NextHop, i4Metric,
                                            pPktInfo->u4IfIndex)) !=
                            DHCP_SUCCESS)
                        {
                            DHCPC_TRC (DEBUG_TRC,
                                       "Default Route addition failed\n");
                        }
                        else
                        {
                            gu4DhcpCDefRoute = u4NextHop;
                        }
                    }
                    else
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "Default Router not in offered IP's network!!\r\n");
                    }
                }
                if ((u4IpAddr == 0) && (gu4DhcpCDefRoute != 0))
                {
                    u4NetAddr = DHCPC_DEF_ROUTE_ADDRESS;
                    u4NetMask = DHCPC_DEF_ROUTE_MASK;
                    i4Metric = DHCP_DEF_ROUTE_METRIC;
                    if ((DhcpCRouteDel (u4NetAddr, u4NetMask,
                                        gu4DhcpCDefRoute, i4Metric,
                                        pPktInfo->u4IfIndex)) != DHCP_SUCCESS)
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "Default Route deletion failed\n");
                    }
                    else
                    {
                        gu4DhcpCDefRoute = 0;
                    }

                }

            }
            /* Enable the following peace of code when default route 
             * needs to be enabled, not handled when different default
             * route given by Server for each renew 
             */
#ifdef DNS_RELAY_WANTED
            /* Addition of Default Name Server and Domain Name Option */
            if (DhcpCGetOption (DHCP_OPT_DNS_NS,
                                &(pBestOffer.PktInfo)) == DHCP_FOUND)
            {
                /* CALL DNS RELAY API */
                MEMCPY (&u4TempIpAddr, DhcpCTag.Val, DhcpCTag.Len);
                u4TempIpAddr = OSIX_NTOHL (u4TempIpAddr);

                if (u4IpAddr == DHCPC_AUTO_CONFIG_IP_ADDRESS)
                {
                    /* Reset the Name Server IP address */
                    nmhSetDnsRelayDefaultNameServerIpAddr
                        (DHCPC_AUTO_CONFIG_IP_ADDRESS);
                }
                else
                {
                    /* Set the Name Server IP address */
                    nmhSetDnsRelayDefaultNameServerIpAddr (u4TempIpAddr);
                }
            }

            if (DhcpCGetOption (DHCP_OPT_DNS_NAME,
                                &(pBestOffer.PktInfo)) == DHCP_FOUND)
            {
                /* CALL DNS RELAY API */
                DnsName.i4_Length = DhcpCTag.Len;
                DnsName.pu1_OctetList = MEM_MALLOC (DhcpCTag.Len + 1, UINT1);
                MEMCPY (DnsName.pu1_OctetList, DhcpCTag.Val, DhcpCTag.Len);
                nmhSetDnsRelayDefaultNameServerDomainName (&DnsName);
                MEM_FREE (DnsName.pu1_OctetList);
            }
#endif
        }
        return DHCP_SUCCESS;
    }
    else
    {
        return DHCP_FAILURE;
    }
}

/************************************************************************/
/*  Function Name   : DhcpCArpTimerExpiry                               */
/*  Description     : Calls Arp_resolve to check a reply has come for   */
/*                    the arp request sent in the DhcpCArpCheck fn.     */
/*  Input(s)        : pDhcpCNode - DHCPC Interface Record               */
/*                  : u4LeasedIpAddr ->Leased IP Address                */
/*                  : u4DstIpAddr ->Packet Destination IP Address       */
/*                  : u4LeaseTime ->Lease Time                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCArpTimerExpiry (tDhcpCRec * pDhcpCNode, UINT4 u4LeasedIpAddr,
                     UINT4 u4DstIpAddr, UINT4 u4LeaseTime)
{
    UINT1               u1ChAddr[DHCP_MAX_CHWADDR_LEN];
    UINT1               u1EncapType;
    UINT1               u1State;
    INT1                i1RetVal;
    UINT4               u4StartTime;    /* IP Acquisition start time */
    UINT4               u4CidrAddr;
    UINT4               u4CidrMask;
    tBestOffer         *pBestOffer = NULL;
    t_ARP_PKT           ArpPkt;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4Retval = 0;
#ifdef LNXIP4_WANTED
    t_IFACE_PARAMS      IfaceParams;
#endif

    DHCPC_TRC (DEBUG_TRC, "Entering DhcpCArpTimerExpiry fn\n");

    pBestOffer = (pDhcpCNode->DhcIfInfo).pBestOffer;

    /* Reset the Processed ACK message recieved */
    pDhcpCNode->u4ProcessedAck = DHCPC_RESET;

    /* Call ArpResolve to check whether the reply has come for the 
       arp request sent */
    u1EncapType = ARP_ENET_V2_ENCAP;
    i4Retval =
        NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex, &u4CfaIfIndex);

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
    /* Set the flag to close the Raw socket for ARP response */
    if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
    {
        DHCPC_ARP_RESP_PROC_FLAG = OSIX_FALSE;
    }
#endif

    UNUSED_PARAM (i4Retval);
    i1RetVal = ArpResolveWithIndex (u4CfaIfIndex, u4LeasedIpAddr,
                                    (INT1 *) u1ChAddr, &u1EncapType);
    u4CidrAddr = u4DstIpAddr;
    u4CidrMask = DhcpCGetDefaultNetMask (u4CidrAddr);

    if (i1RetVal == ARP_SUCCESS)
    {
        DHCPC_TRC (DEBUG_TRC, "ARP_RESOLVE success, Sending Decline Msg\n");

        /*  Obtain the Net mask if it was provided by the DHCP Server */
        if (DhcpCGetOption (DHCP_OPT_SUBNET_MASK,
                            &(pBestOffer->PktInfo)) == DHCP_FOUND)
        {
            MEMCPY (&u4CidrMask, DhcpCTag.Val, DhcpCTag.Len);
            u4CidrMask = OSIX_NTOHL (u4CidrMask);
        }
        DhcpCSendDecline (pDhcpCNode, u4LeasedIpAddr, u4DstIpAddr);

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
        /* After Sending Decline delete the ARP entry */
        if (ISS_HW_SUPPORTED ==
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
            ArpModifyWithIndex (u4CfaIfIndex, u4LeasedIpAddr, ARP_INVALID);
        }
#endif
        /* Start a Timer, The expiry on which the added route
         * will be deleted . This is added to give some time for Decline
         * message to out from the Interface. Within this timer expiry No
         * message should be sent out for that particular interface */

        (pDhcpCNode->ReplyWaitTimer).u4IfIndex = pDhcpCNode->u4IpIfIndex;
        (pDhcpCNode->ReplyWaitTimer).u4TimerId = DHCPC_DECLINE_MSG_TIMER_ID;
        (pDhcpCNode->ReplyWaitTimer).u4CidrAddr = u4CidrAddr;
        (pDhcpCNode->ReplyWaitTimer).u4CidrMask = u4CidrMask;
        (pDhcpCNode->ReplyWaitTimer).pEntry = (VOID *) pDhcpCNode;

        DhcpCStartTimer (DhcpCTimerListId,
                         &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer),
                         DHCP_CIDR_ROUTE_ADD_WAIT_TIMER_VAL);
    }
    else if (i1RetVal == ARP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "ARP_RESOLVE not successful\n");

        /* Get the IP Acquisition start Time */
        u4StartTime = (pDhcpCNode->DhcIfInfo).u4Secs;

        /* Get the lease time */

        u1State = CalRenewRebindTime (pDhcpCNode, u4StartTime, u4LeaseTime);

        (pDhcpCNode->DhcIfInfo).u1State = u1State;

        if (u1State == S_BOUND)
        {
#ifdef LNXIP4_WANTED
            MEMSET (&IfaceParams, 0, sizeof (t_IFACE_PARAMS));
            /* Down indication is sent to RTM before configuring IP with appropriate interface index and port */
            IfaceParams.u2Port = (UINT2) pDhcpCNode->u4IpIfIndex;
            IfaceParams.u2IfIndex = (UINT2) u4CfaIfIndex;
            LnxIpNotifyIfInfo (RTM_OPER_DOWN_MSG, &IfaceParams);
#endif
            /* Write the availabe lease info into config file */
            /* Configure the Interface with the IP Address Leased */
            if ((DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                               u4LeasedIpAddr)) == DHCP_SUCCESS)
            {
                DHCPC_TRC1 (DEBUG_TRC,
                            "Configured the IP ADDRESS Leased in %x intf\n",
                            pDhcpCNode->u4IpIfIndex);
            }
            else
            {
                DHCPC_TRC1 (FAIL_TRC,
                            "FAILED in Configuring the IP ADDRESS in %x intf\n ",
                            pDhcpCNode->u4IpIfIndex);
            }
#ifdef LNXIP4_WANTED
            MEMSET (&IfaceParams, 0, sizeof (t_IFACE_PARAMS));
            /* Up indication is sent to RTM after configuring IP with appropriate interface index and port */
            IfaceParams.u2Port = (UINT2) pDhcpCNode->u4IpIfIndex;
            IfaceParams.u2IfIndex = (UINT2) u4CfaIfIndex;
            LnxIpNotifyIfInfo (RTM_OPER_UP_MSG, &IfaceParams);
#endif
            /* Store the lease information in the interface table */
            (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = u4LeasedIpAddr;
            (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = u4LeaseTime;
            (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u4StartOfLeaseTime =
                u4StartTime;
            (pDhcpCNode->DhcIfInfo).u4ServerOfferred = u4DstIpAddr;

            /* print the current bindings; only if the trace level is enabled */
            if (gu4DhcpCDebugMask & BIND_TRC)
            {
                DhcpCShowBinding (pDhcpCNode->u4IpIfIndex);
            }

            /* Intimate DHCP standby peers about the new binding */
            DhCRedSendDynamicClientInfo (pDhcpCNode);
        }

        /* Update the outdated cache in the nearby hosts */
        MEMSET (&ArpPkt, 0, sizeof (t_ARP_PKT));

        DhcpCfaGddGetHwAddr (pDhcpCNode->u4IpIfIndex,
                             (UINT1 *) ArpPkt.i1Shwaddr);

        MEMSET (ArpPkt.i1Thwaddr, 0xff, CFA_ENET_ADDR_LEN);
        ArpPkt.i2Opcode = ARP_RESPONSE;
        ArpPkt.u4Sproto_addr = u4LeasedIpAddr;
        ArpPkt.u4Tproto_addr = IP_GEN_BCAST_ADDR;

        if (ArpSendReqOrResp ((UINT2) pDhcpCNode->u4IpIfIndex,
                              CFA_ENCAP_ENETV2, &ArpPkt) == ARP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Send ARP Response Failed\r\n");
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCArpTimerExpiry fn\n");
    return;
}                                /* End of DhcpCArpTimerExpiry function */

/************************************************************************/
/*  Function Name   : DhcpCArpCheck                                     */
/*  Description     : Sends a ARP event which verifies the IP Address   */
/*                    assigned to any other host or not  and a timer    */
/*                    is started on expiry of which a the presence of   */
/*                    IP address to any other host is checked in the    */
/*                    arp cache                                         */
/*  Input(s)        : pPktInfo - Received Packet                        */
/*                    pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCArpCheck (tDhcpPktInfo * pPktInfo, tDhcpCRec * pDhcpCNode)
{
    tArpQMsg            ArpQMsg;
    UINT4               u4Temp;
    UINT1               u1RetVal;
    UINT4               u4TimeLeft = 0;
    INT4                i4RetVal = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCArpCheck fn\n");

    /* Simulate the packet as it is from IP module, 
       so that the function handling for IP module is 
       reused for DHCP Client module also */
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
    ArpQMsg.u2Port = (UINT2) (pPktInfo->u4IfIndex);
    ArpQMsg.u4IpAddr = pPktInfo->DhcpMsg.yiaddr;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC,
                   " Exiting DhcpCArpCheck fn FAILURE in ArpEnqueuePkt\n");
        return DHCP_FAILURE;
    }

    /* If Any Arp Timer Running, Stop It - for safety purpose */
    u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer),
                                      &u4TimeLeft);

    if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
        /* Stop the Arp timer */
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer));

    /* A timer is started on expiry of which arp cache will be checked 
       whether a arp reply is recieved or not */
    /* Store the Interface index and the destination pointer in the 
       timer parameter */
    MEMSET (&pDhcpCNode->ArpCheckTimer, 0, sizeof (tDhcpCTimer));
    (pDhcpCNode->ArpCheckTimer).u4IfIndex = pPktInfo->u4IfIndex;
    (pDhcpCNode->ArpCheckTimer).u4TimerId = DHCPC_ARP_CHECK_TIMER_ID;
    (pDhcpCNode->ArpCheckTimer).u4LeasedIpAddr = pPktInfo->DhcpMsg.yiaddr;

    i4RetVal = DhcpCGetOption (DHCP_OPT_SERVER_ID, pPktInfo);

    MEMCPY (&u4Temp, DhcpCTag.Val, DhcpCTag.Len);
    u4Temp = OSIX_NTOHL (u4Temp);
    (pDhcpCNode->ArpCheckTimer).u4DstIpAddr = u4Temp;

    i4RetVal = DhcpCGetOption (DHCP_OPT_LEASE_TIME, pPktInfo);
    UNUSED_PARAM (i4RetVal);

    /* No need to validate the lease time and its presence since, 
       already it is done in the DhcpCValidateBootReply function */
    MEMCPY (&u4Temp, DhcpCTag.Val, DhcpCTag.Len);
    u4Temp = OSIX_NTOHL (u4Temp);

    /* Store the lease time in the timer structure. */
    (pDhcpCNode->ArpCheckTimer).u4LeasedTime = u4Temp;

    (pDhcpCNode->ArpCheckTimer).pEntry = (VOID *) pDhcpCNode;

    DhcpCStartTimer (DhcpCTimerListId,
                     &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer),
                     gDhcpCFastAcc.u4FastAccArpCheckTimeOut);

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCArpCheck fn\n");

    return DHCP_SUCCESS;
}                                /* End of DhcpCArpCheck function */

/************************************************************************/
/*  Function Name   : DhcpCfaGddGetHwAddr                               */
/*  Description     : Wrapper function for CfaGddGetHwAddr              */
/*  Input(s)        : u4IfIndex --> Interface Index                     */
/*                  : pHwAddr  --> pointer to Hw Address Array          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCfaGddGetHwAddr (UINT4 u4Port, UINT1 *pHwAddr)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4CfaIfIndex;

    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return;
    }

    if (CfaGetIfInfo ((UINT2) u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return;
    }
    MEMCPY (pHwAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
}

INT4
DhcpCNotify (UINT4 u4IfIndex, UINT4 u4MsgType)
{
    tDhcpQMsg          *pDhcpQMsg;

    if ((pDhcpQMsg =
         (tDhcpQMsg *) MemAllocMemBlk (gDhcpClientMemPoolId.DhcpCQMsgPoolId)) ==
        NULL)
    {
        DHCPC_TRC (FAIL_TRC, "DhcpQ memory alloc failure\n");
        return DHCP_FAILURE;
    }

    pDhcpQMsg->u4MsgType = u4MsgType;
    pDhcpQMsg->dhcpIfIndex = u4IfIndex;

    if (OsixQueSend (gDhcpCMsgQId,
                     (UINT1 *) &pDhcpQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC, "Send to Q failed\n");
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCQMsgPoolId,
                            (UINT1 *) pDhcpQMsg);
        return DHCP_FAILURE;
    }

    OsixEvtSend (gu4DhcpCTaskId, DHCPC_MSGQ_IF_EVENT);
    return DHCP_SUCCESS;
}

INT4
DhcpCTaskInit (void)
{
    UINT4               u4XidSeed = 0;

    DHCPC_TRC (DEBUG_TRC, "DHCPC Task Init Entered\n");

    if (OsixCreateSem (DHCPC_PROT_MUTEX_SEMA4, DHCP_SEM_CREATE_INIT_CNT, 0,
                       &gDhcpCSemId) != OSIX_SUCCESS)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "Semaphore Creation failure for %s \n",
                    DHCPC_PROT_MUTEX_SEMA4);
        return DHCP_FAILURE;
    }

    DHCPC_SEED (u4XidSeed);

    DHCPC_SRAND (u4XidSeed);

    if (DhcpCMemInit () == DHCP_FAILURE)
    {
        OsixSemDel (gDhcpCSemId);
        return DHCP_FAILURE;
    }

    /* Initialize the global variables used */
#ifdef ALL_TRACE_WANTED
    gu4DhcpCDebugMask = ENABLE_ALL_TRC;
#else
    gu4DhcpCDebugMask = DISABLE_ALL_TRC;
#endif

    if (OsixQueCrt (DHCP_CLIENT_Q_NAME, OSIX_MAX_Q_MSG_LEN, 30 /* Q depth */ ,
                    &gDhcpCMsgQId) != OSIX_SUCCESS)
    {
        DHCPC_TRC (DEBUG_TRC, "DHCPC Q creation failed\n");
        return DHCP_FAILURE;
    }

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
    if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
    {
        if (OsixQueCrt
            (DHCP_CLIENT_Q_ARP_NAME, OSIX_MAX_Q_MSG_LEN, 30 /* Q depth */ ,
             &gDhcpCArpMsgQId) != OSIX_SUCCESS)
        {
            DHCPC_TRC (DEBUG_TRC, "DHCPC Q ARP creation failed\n");
            return DHCP_FAILURE;
        }
    }
#endif

    /**** Create the list of timers used by the DHCP Client *****/
    /* Create the Reply wait timer List */
    if (TmrCreateTimerList (DHCP_CLIENT_TASK_NAME,
                            DHCPC_TIMER_EXPIRY,
                            NULL, &DhcpCTimerListId) != TMR_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC,
                   "FAILURE in TmrCreateTimerList fn in DhcpCTaskInit\n");
        return DHCP_FAILURE;
    }

    DHCPC_TRC (DEBUG_TRC, "Exitting DHCPC Task Init\n");

    return DHCP_SUCCESS;
}

VOID
DhcpCProcessQMsg (VOID)
{
    tDhcpQMsg          *pDhcpQMsg = NULL;

    while (OsixQueRecv (gDhcpCMsgQId, (UINT1 *) (&pDhcpQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        DHCPC_TRC1 (DEBUG_TRC, "Rcvd Msg %x\n", pDhcpQMsg);

        /*Take the lock */
        DHCPC_PROTO_LOCK ();
        switch (pDhcpQMsg->u4MsgType)
        {
            case DHCPC_IF_OPER_CHG:
                DhcpCProcessOperNotification (pDhcpQMsg->dhcpIfIndex);
                break;

            case DHCPC_ACQUIRE_IP:
                DhcpCStartProcessing (pDhcpQMsg->dhcpIfIndex);
                break;
                /* Release process should not be done in the case of BOOTP packets. */
            case DHCPC_RELEASE_IP:
                DhcpCStopProcessing (pDhcpQMsg->dhcpIfIndex, OSIX_FALSE);
                break;

            case DHCPC_PKT_ARRIVED:
                DhcpCPktHandler (pDhcpQMsg->dhcpRcvBuf);
                break;

            case DHCPC_RM_MSG:
                /* DHCP Client RM msg and events handler */
                DhCRedHandleRmEvents (pDhcpQMsg);
                break;

            case DHCPC_DISC_OPTION:
                DhcpCProcessOptions (pDhcpQMsg);
                break;
            default:
                break;
        }

        /*Release the lock */
        DHCPC_PROTO_UNLOCK ();
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCQMsgPoolId,
                            (UINT1 *) pDhcpQMsg);
    }
}

VOID
DhcpCPktHandler (tCRU_BUF_CHAIN_HEADER * pRcvBuf)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4DestIp = 0;
    INT4                i4DataLen;

    i4DataLen = CRU_BUF_Get_ChainValidByteCount (pRcvBuf);

    if (CRU_BUF_Copy_FromBufChain (pRcvBuf,
                                   (UINT1 *) gi1CliSelRecvBuff,
                                   0, i4DataLen) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pRcvBuf, FALSE);
        DHCPC_TRC (FAIL_TRC, "CRU Buf copy failed\n");
        return;
    }
    else
    {
        /* Update the interface index and
         * Source IP address from the tMODULE_DATA_PTR 
         * structure
         */
        u4IfIndex = CRU_BUF_Get_U4Reserved1 (pRcvBuf);
        u4DestIp = CRU_BUF_Get_U4Reserved2 (pRcvBuf);
        CRU_BUF_Release_MsgBufChain (pRcvBuf, FALSE);
    }
    DhcpCProcessPkt ((UINT1 *) gi1CliSelRecvBuff, u4IfIndex,
                     u4DestIp, (UINT2) i4DataLen);

    MEMSET (gi1CliSelRecvBuff, 0, DHCP_MAX_MTU);
}

/************************************************************************/
/*  Function Name   : DhcpCHandleIfStateChg                             */
/*  Description     : Function to handle the interface status change:   */
/*                    CFA_IF_UP, CFA_IF_DOWN, CFA_IF_DEL                */
/*                    This function is called in CFA task context       */
/*  Input(s)        : u4Port --> Port no.                               */
/*                  : u4IfState --> Status of the interface             */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCHandleIfStateChg (UINT4 u4Port, UINT4 u4BitMap)
{

    if ((u4BitMap & (OPER_STATE | IFACE_DELETED)) == 0)
    {
        return;
    }
    if ((u4BitMap & IFACE_DELETED) == IFACE_DELETED)
    {
        DhcpCNotify (u4Port, DHCPC_RELEASE_IP);
        return;
    }

    if ((u4BitMap & OPER_STATE) == OPER_STATE)
    {
        DhcpCNotify (u4Port, DHCPC_IF_OPER_CHG);
    }
}

/************************************************************************/
/*  Function Name   : DhcpCProcessOperNotification                      */
/*  Description     : Function to handle the interface status change:   */
/*                    CFA_IF_UP, CFA_IF_DOWN                            */
/*                    This function is called in CFA task context       */
/*  Input(s)        : u4Port --> Port no.                               */
/*                  : u4IfState --> Status of the interface             */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCProcessOperNotification (UINT4 u4Port)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT4               u4CfaIfIndex;
    UINT1               u1IpAllocMethod;
    UINT1               u1AllocProto;

    /* Get the allocation Method.Process the State Change Indication */
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) != NETIPV4_SUCCESS)
    {
        return;
    }

    if (CfaIfGetIpAllocMethod (u4CfaIfIndex, &u1IpAllocMethod) == CFA_FAILURE)
    {
        return;
    }

    if (CfaIfGetIpAllocProto (u4CfaIfIndex, &u1AllocProto) == CFA_FAILURE)
    {
        return;
    }
    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
    {
        return;
    }

    pDhcpCNode = DhcpCGetIntfRec (u4Port);

    switch (NetIpIfInfo.u4Oper)
    {
        case IPIF_OPER_ENABLE:
            if (u1IpAllocMethod == CFA_IP_ALLOC_POOL)
            {
                if (u1AllocProto == CFA_PROTO_DHCP)
                {
                    if (pDhcpCNode != NULL)
                    {
                        /*If DHCP CLient is already enabled on the Interface,
                           and the Address Allocatio Method  remains dynamic
                           Handle OperUp event for the Interface */
                        DhcpCHandleIfUp (pDhcpCNode);
                    }
                    else
                    {
                        /*if the Address Allocatio Method  is dynamic
                         * Enable DHCPClient on the Interface if allocation
                         protocol is DHCP. */
                        if (DhcpCAddIntfRec (u4Port) == DHCP_FAILURE)
                        {
                            return;
                        }
                        DhcpCStartProcessing (u4Port);
                    }
                }
                else if (u1AllocProto == CFA_PROTO_BOOTP)
                {
                    if (pDhcpCNode == NULL)
                    {
                        if (DhcpCAddIntfRec (u4Port) == DHCP_FAILURE)
                        {
                            return;
                        }
                        BootpSendRequestPacket (u4Port);
                    }
                    else
                    {
                        if ((pDhcpCNode->DhcIfInfo).u1State != S_BOUND)
                        {
                            BootpSendRequestPacket (u4Port);
                        }
                    }
                }
                else
                {
                    if (pDhcpCNode != NULL)
                    {
                        /* if the Dynamic Protocol is not DHCP, 
                         * Give Indication to release the Acquired IP if
                         DHCP is enabled on the Interface. */
                        DhcpCStopProcessing (u4Port, OSIX_TRUE);
                    }
                }
            }
            else
            {
                if (pDhcpCNode != NULL)
                {
                    /* if the  Address Allocatio Method changed to Manual,
                       Give Indication to release the Acquired IP if
                       DHCP is enabled on the Interface. */
                    DhcpCStopProcessing (u4Port, OSIX_TRUE);
                }
            }
            break;

        case IPIF_OPER_DISABLE:

            if (pDhcpCNode != NULL)
            {
                if ((u1AllocProto == CFA_PROTO_BOOTP) &&
                    ((pDhcpCNode->DhcIfInfo).u1State != S_BOUND))
                {
                    /* If protocol used is BOOTP timers alone should be started                        state should not be changed. */
                    DhcpCStopTimers (pDhcpCNode);
                    return;
                }
                else if (u1AllocProto == CFA_PROTO_DHCP)
                {
                    /* If DHCP CLient is already enabled on the Interface,
                       and the Address Allocatio Method  remains dynamic
                       Handle OperDown event for the Interface */
                    DhcpCHandleIfDown (pDhcpCNode);
                    return;
                }
            }
            break;

        default:
            break;
    }
}

/* -------------------------------------------------------------------+
 *  Function           : DhcpCProtocolLock
 *
 *  Input(s)           : None.
 *
 *  Output(s)          : None.
 *
 *  Returns            : None.
 *
 *  Action             : Takes a task Sempaphore
 * +-------------------------------------------------------------------*/

INT4
DhcpCProtocolLock (VOID)
{
    if (OsixSemTake (gDhcpCSemId) != OSIX_SUCCESS)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "SemTake failure for %s \n", DHCPC_PROT_MUTEX_SEMA4);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------------+
 *  Function           : DhcpCProtocolUnlock
 *
 *  Input(s)           : None.
 *
 *  Output(s)          : None.
 *
 *  Returns            : None.
 *
 *  Action             : Releases a task Sempaphore
 * +-------------------------------------------------------------------*/
INT4
DhcpCProtocolUnlock (VOID)
{
    if (OsixSemGive (gDhcpCSemId) != OSIX_SUCCESS)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "GiveSem failure for %s \n", DHCPC_PROT_MUTEX_SEMA4);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCMemInit                                      */
/*  Description     : Initializes memory for DHCP Interfaces            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpCMemInit (VOID)
{
    /* Create the HashTable and Mempool for Interface Records */
    gDhcpCIntfTbl = TMO_HASH_Create_Table (DHCPC_MAX_INTERFACE, NULL, TRUE);

    if (gDhcpCIntfTbl == NULL)
    {
        DHCPC_TRC (FAIL_TRC,
                   " Hash Table Creation failed for DHCP INterface Table \r\n");
        return DHCP_FAILURE;

    }
    /* Create RB tree for dhcp client requesting options list */
    gDhcpCReqOptionList = RBTreeCreateEmbedded (0, DhcpCReqOptEntryCmp);
    if (gDhcpCReqOptionList == NULL)
    {
        TMO_HASH_Delete_Table (gDhcpCIntfTbl, NULL);
        DHCPC_TRC (FAIL_TRC,
                   "RB Tree Creation failed for DHCP ReqOptionEntry \r\n");
        return DHCP_FAILURE;
    }

    /* create RB tree for dhcp client identifiers list */
    gDhcpCidList = RBTreeCreateEmbedded (0, DhcpCidEntryCmp);
    if (gDhcpCidList == NULL)
    {
        TMO_HASH_Delete_Table (gDhcpCIntfTbl, NULL);

        /* Delete dhcp client requesting options list RB tree */
        RBTreeDestroy (gDhcpCReqOptionList,
                       (tRBKeyFreeFn) DhcpCReqOptEntryFree, 0);
        gDhcpCReqOptionList = NULL;
        DHCPC_TRC (FAIL_TRC,
                   "RB Tree Creation failed for DHCP ReqOptionEntry \r\n");
        return DHCP_FAILURE;
    }

    if (DhcSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        TMO_HASH_Delete_Table (gDhcpCIntfTbl, NULL);

        /* Delete dhcp client requesting options list RB tree */
        RBTreeDestroy (gDhcpCReqOptionList,
                       (tRBKeyFreeFn) DhcpCReqOptEntryFree, 0);
        gDhcpCReqOptionList = NULL;

        /* Delete dhcp client identifiers list RB tree */
        RBTreeDestroy (gDhcpCidList, (tRBKeyFreeFn) DhcpCidEntryFree, 0);
        gDhcpCidList = NULL;

        DHCPC_TRC (FAIL_TRC,
                   " Mem Pool Creation failed for DHCP Interface Nodes \r\n");
        return DHCP_FAILURE;
    }
    else
    {
        gDhcpClientMemPoolId.DhcpCInfMemPoolId =
            DHCMemPoolIds[MAX_DHC_INTERFACE_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCOptMemPoolId =
            DHCMemPoolIds[MAX_DHC_OPT_ENTRIES_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCIdMemPoolId =
            DHCMemPoolIds[MAX_DHC_CLIENTID_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCBestOffPoolId =
            DHCMemPoolIds[MAX_DHC_BEST_OFFER_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCQMsgPoolId =
            DHCMemPoolIds[MAX_DHC_QUEUE_MSG_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId =
            DHCMemPoolIds[MAX_DHC_MSG_OPTIONS_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCClientDefOptPoolId =
            DHCMemPoolIds[MAX_DHC_CLIENT_DEF_OPTIONS_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCOutPktPoolId =
            DHCMemPoolIds[MAX_DHC_OUT_PKT_INFO_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCMessageId =
            DHCMemPoolIds[MAX_DHCP_MAX_OUTMSG_SIZE_SIZING_ID];
        gDhcpClientMemPoolId.DhcpCDiscPoolId =
            DHCMemPoolIds[MAX_DHC_DISC_MSG_SIZING_ID];

    }
    /*DHCP FAST ACCESS PARAMS INIT */
    gDhcpCFastAcc.u4FastAccNullStTimeOut = DHCPC_NULL_STATE_WAIT_TIME_OUT;
    gDhcpCFastAcc.u4FastAccDisTimeOut = DHCPC_DISCOVER_RETX_TIME_OUT;
    gDhcpCFastAcc.u1FastAccess = DHCPC_FAST_ACC_DISABLE;
    gDhcpCFastAcc.u4FastAccArpCheckTimeOut = DHCPC_ARP_CHECK_TIME_OUT;
    return (DHCP_SUCCESS);
}

/*************************************************************************/
/*  Function Name   : DhcpCGetIntfRec                                    */
/*  Description     : Provides DHCP Interface Record for the IP Interface*/
/*  Input(s)        : u4IpIfIndex  - IP Interface Index                  */
/*  Output(s)       : None.                                              */
/*  Returns         : DHCP Inteface Record                               */
/*************************************************************************/
tDhcpCRec          *
DhcpCGetIntfRec (UINT4 u4IpIfIndex)
{
    UINT4               u4HashIndex;
    tDhcpCRec          *pDhcpCNode = NULL;

    u4HashIndex = DHCPCIF_HASH_INDEX (u4IpIfIndex);

    TMO_HASH_Scan_Bucket (gDhcpCIntfTbl, u4HashIndex, pDhcpCNode, tDhcpCRec *)
    {
        if (pDhcpCNode->u4IpIfIndex == u4IpIfIndex)
        {
            break;
        }
    }

    return pDhcpCNode;
}

/************************************************************************/
/*  Function Name   : DhcpCAddIntfRec                                   */
/*  Description     : Creates a DHCP Interface Record and add that to   */
/*                    the  DHCP Client database(HASH TABLE)             */
/*                    timers                                            */
/*  Input(s)        : u4IpIfIndex- Ip Inteface Index                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
DhcpCAddIntfRec (UINT4 u4IpIfIndex)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT4               u4HashIndex;

    if ((pDhcpCNode =
         (tDhcpCRec *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                       DhcpCInfMemPoolId)) == NULL)
    {
        DHCPC_TRC (FAIL_TRC,
                   "Failed to get DHCP Interface Node from DHCP Interface MemPool  \r\n");
        return DHCP_FAILURE;
    }

    MEMSET (pDhcpCNode, 0, sizeof (tDhcpCRec));

    pDhcpCNode->u4IpIfIndex = u4IpIfIndex;

    TMO_SLL_Init (&((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.DefaultOptList));

    (pDhcpCNode->TxRxStats).u4DhcpCCounterReset = DHCP_NOT_SET;

    (pDhcpCNode->DhcIfInfo).u1State = S_UNUSED;

    (pDhcpCNode->ReplyWaitTimer).pEntry = (VOID *) pDhcpCNode;
    (pDhcpCNode->LeaseTimer).pEntry = (VOID *) pDhcpCNode;
    (pDhcpCNode->ArpCheckTimer).pEntry = (VOID *) pDhcpCNode;
    (pDhcpCNode->ReleaseWaitTimer).pEntry = (VOID *) pDhcpCNode;

    DhcpCopyDefaultOpt (pDhcpCNode);

    u4HashIndex = DHCPCIF_HASH_INDEX (u4IpIfIndex);

    TMO_HASH_Add_Node (gDhcpCIntfTbl, &(pDhcpCNode->NextNode),
                       u4HashIndex, NULL);

    DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCDeleteIntfRec                                */
/*  Description     : Deletes DHCP Interface Record                     */
/*  Input(s)        : u4IpIfIndex - IP Interface Index                  */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
DhcpCDeleteIntfRec (UINT4 u4IpIfIndex)
{
    UINT4               u4HashIndex;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT4               u4CfaIfIndex = 0;
    tRBElem            *pRBElem = NULL;
    tDhcpCOptions      *pDefaultOptNode = NULL;
    tRBElem            *pRBNextElem = NULL;

    if ((pDhcpCNode = DhcpCGetIntfRec (u4IpIfIndex)) == NULL)
    {
        return DHCP_FAILURE;
    }

    u4HashIndex = DHCPCIF_HASH_INDEX (u4IpIfIndex);

    DhcpCFlushOffer (pDhcpCNode);
    if ((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions != NULL)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCClientDefOptPoolId,
                            (UINT1 *) (pDhcpCNode->DhcIfInfo).
                            DhcpClientConfInfo.pDefaultOptions);
    }

    TMO_HASH_Delete_Node (gDhcpCIntfTbl, &(pDhcpCNode->NextNode), u4HashIndex);

    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCInfMemPoolId,
                        (UINT1 *) pDhcpCNode);
    if (NetIpv4GetCfaIfIndexFromPort (u4IpIfIndex, &u4CfaIfIndex) !=
        NETIPV4_SUCCESS)
    {
        return DHCP_FAILURE;
    }

    pRBElem = RBTreeGetFirst (gDhcpCReqOptionList);

    while (pRBElem != NULL)
    {
        pDefaultOptNode = (tDhcpCOptions *) pRBElem;

        pRBNextElem = RBTreeGetNext (gDhcpCReqOptionList, pRBElem, NULL);
        if ((u4CfaIfIndex == pDefaultOptNode->u4IfIndex))
        {
            RBTreeRemove (gDhcpCReqOptionList, (tRBElem *) pDefaultOptNode);
        }
        pRBElem = pRBNextElem;
    }

    return DHCP_SUCCESS;
}

/****************************************************************************/
/*    Function Name      : DhcpCMemDeInit                                   */
/*    Description        : This function releases the DHCP Client Interfaces*/
/*    Input(s)           : None.                                            */
/*    Output(s)          : None                                             */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
DhcpCMemDeInit (VOID)
{
    tTMO_SLL_NODE      *pNode;
    tDhcpDiscOption    *pDhcpDiscOptionNode;
    TMO_HASH_Delete_Table (gDhcpCIntfTbl, DhcpCHashNodeDeleteFn);

    TMO_SLL_Scan (&gDhcpDiscOptionList, pNode, tTMO_SLL_NODE *)
    {
        pDhcpDiscOptionNode = (tDhcpDiscOption *) pNode;
        TMO_SLL_Delete (&gDhcpDiscOptionList, pNode);
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDiscPoolId,
                            (UINT1 *) pDhcpDiscOptionNode);
    }

    DhcSizingMemDeleteMemPools ();

    gDhcpCIntfTbl = NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : DhcpCHashNodeDeleteFn                            */
/*    Description         : This function releases the memory for the given  */
/*                          Hashnode.                                        */
/*    Input(s)            : pNode       - Hash Node.                         */
/*    Output(s)           : None                                             */
/*    Returns            : None.                                             */
/*****************************************************************************/
VOID
DhcpCHashNodeDeleteFn (tTMO_HASH_NODE * pHashNode)
{
    tDhcpCRec          *pDhcpCNode;

    pDhcpCNode = (tDhcpCRec *) pHashNode;

    DhcpCFlushOffer (pDhcpCNode);

    if ((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions != NULL)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCClientDefOptPoolId,
                            (UINT1 *) (pDhcpCNode->DhcIfInfo).
                            DhcpClientConfInfo.pDefaultOptions);
    }

    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCInfMemPoolId,
                        (UINT1 *) pDhcpCNode);
}

/************************************************************************/
/*  Function Name   : DhcpCGetNextMgmtIfIndex                           */
/*  Description     : This function gets the Lexographically higher     */
/*                    Management(CFA) IfIndex                           */
/*  Input(s)        : u4IfIndex - CFA Interface Index                   */
/*  Output(s)       : pNextIfIndex - Lexographically higher CFA IfIndex */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                         */
/************************************************************************/
INT1
DhcpCGetNextMgmtIfIndex (UINT4 u4IfIndex, UINT4 *pNextIfIndex)
{
    UINT4               u4HashIndex = 0;
    UINT4               u4NextMgmtIfIndex = u4IfIndex;
    UINT4               u4CurrentMgmtIfIndex = 0;
    INT4                i4Found = FALSE;
    tDhcpCRec          *pDhcpCNode = NULL;

    TMO_HASH_Scan_Table (gDhcpCIntfTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gDhcpCIntfTbl, u4HashIndex, pDhcpCNode,
                              tDhcpCRec *)
        {
            if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                              &u4CurrentMgmtIfIndex) ==
                NETIPV4_FAILURE)
            {
                continue;
            }
            if (i4Found == FALSE)
            {
                if (u4CurrentMgmtIfIndex > u4IfIndex)
                {
                    u4NextMgmtIfIndex = u4CurrentMgmtIfIndex;
                    i4Found = TRUE;
                }
            }
            else
            {
                if ((u4CurrentMgmtIfIndex > u4IfIndex) &&
                    (u4CurrentMgmtIfIndex < u4NextMgmtIfIndex))
                {
                    u4NextMgmtIfIndex = u4CurrentMgmtIfIndex;
                }
            }
        }
    }

    if (i4Found == TRUE)
    {
        *pNextIfIndex = u4NextMgmtIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpCheckOfferedIpOnInterface                     */
/*  Description     : This function is an API for getting the offered   */
/*                    IP for a particular interface                     */
/*  Input(s)        : u4PortNo   - IP port number  of the interface     */
/*                    u4IpAddress - IP address  to be checked           */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
INT4
DhcpCheckOfferedIpOnInterface (UINT4 u4PortNo, UINT4 u4IpAddress)
{
    tDhcpCRec          *pDhcpCRec = NULL;
    UINT4               u4Yiaddr = 0;

    pDhcpCRec = DhcpCGetIntfRec (u4PortNo);

    if ((pDhcpCRec != NULL) && (pDhcpCRec->DhcIfInfo.pBestOffer != NULL))
    {
        u4Yiaddr = pDhcpCRec->DhcIfInfo.pBestOffer->PktInfo.DhcpMsg.yiaddr;

        if (u4Yiaddr == u4IpAddress)
        {
            return DHCP_SUCCESS;
        }
    }
    return DHCP_FAILURE;
}

INT1
DhcpUtilHandleAlloMethodChange (INT4 i4IfIndex, INT4 i4FromAlloMethod,
                                INT4 i4ToAllocMethod, INT4 i4Proto)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT4               u4Port = 0;

    if ((i4FromAlloMethod != CFA_IP_ALLOC_POOL) || (i4Proto != CFA_PROTO_DHCP))
    {
        DHCPC_TRC (DEBUG_TRC,
                   "IP Address Allocation Method is not Dynamic or Protocol used is not DHCP\r\n");
        return (OSIX_FAILURE);
    }

    if (i4ToAllocMethod != CFA_IP_ALLOC_MAN)
    {
        DHCPC_TRC (DEBUG_TRC,
                   "New IP Address Allocation Method is not Manual\r\n");
        return (OSIX_FAILURE);
    }

    /*Take the lock */
    DHCPC_PROTO_LOCK ();

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        /*Release the lock */
        DHCPC_PROTO_UNLOCK ();
        return (OSIX_FAILURE);
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        /*Release the lock */
        DHCPC_PROTO_UNLOCK ();
        return (OSIX_FAILURE);
    }

    if (DhcpCProcessRelease (pDhcpCNode, OSIX_TRUE) != DHCP_SUCCESS)
    {
        /*Release the lock */
        DHCPC_PROTO_UNLOCK ();
        return (OSIX_FAILURE);
    }

    /*Release the lock */
    DHCPC_PROTO_UNLOCK ();
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpClientUtilCallBackRegister                    */
/*  Description     : This function is an API for Registering the       */
/*                    DHCP Options with the DHCP Client                 */
/*  Input(s)        : pCbInfo   - Callback Information                  */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

UINT4
DhcpClientUtilCallBackRegister (tDhcpCDiscCbInfo * pCbInfo)
{
    tDhcpQMsg          *pDhcpQMsg = NULL;

    if (NULL == pCbInfo)
    {
        return OSIX_FAILURE;
    }

    if ((pDhcpQMsg =
         (tDhcpQMsg *) MemAllocMemBlk (gDhcpClientMemPoolId.DhcpCQMsgPoolId)) ==
        NULL)
    {
        return OSIX_FAILURE;
    }

    pDhcpQMsg->u4MsgType = DHCPC_DISC_OPTION;

    MEMCPY (&(pDhcpQMsg->unDhcpMsgIfParam.AppDhcpCDiscCbInfo),
            pCbInfo, sizeof (tDhcpCDiscCbInfo));

    if (OsixQueSend (gDhcpCMsgQId,
                     (UINT1 *) &pDhcpQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCQMsgPoolId,
                            (UINT1 *) pDhcpQMsg);
        return OSIX_FAILURE;
    }

    OsixEvtSend (gu4DhcpCTaskId, DHCPC_MSGQ_IF_EVENT);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpClientUtilFillDhcpOptionBitMask               */
/*  Description     : This function is an API to set the Bitmask        */
/*                    based on the DHCP Option                          */
/*  Input(s)        : pOptions  - INT4 Array of size 4 to hold 255      */
/*                                options                               */
/*                    i1Option  - DHCP Option                           */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/

UINT4
DhcpClientUtilFillDhcpOptionBitMask (INT4 *pOptions, INT4 i4Option)
{
    UINT1               u1Index = 0;
    UINT1               u1BitPos = 0;

    if ((DHCP_OPTION_BLOCKS * DHCP_OPTION_BLOCK_SIZE) <= i4Option)
    {
        return OSIX_FAILURE;
    }

    /* Divide the option by 32 to find the index */
    u1Index = (UINT1) i4Option / DHCP_OPTION_BLOCK_SIZE;
    /* Mod the option by 32 to find the appropriate Bit position */
    u1BitPos = (UINT1) i4Option % DHCP_OPTION_BLOCK_SIZE;
    /* Update the bit mask in the appropriate place */
    pOptions[u1Index] = pOptions[u1Index] | (1 << u1BitPos);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpClientUtilConvertBitMasktoOption              */
/*  Description     : This function is used to set convert the Options  */
/*                    from the BITMASK                                  */
/*  Input(s)        : pOptions  - INT4 Array of size 4 to hold 255      */
/*                                options                               */
/*                    i1Option  - Array of 255 characters to hold DHCP */
/*                                Options                               */
/*                    iCnt      - Number of Options                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/

INT4
DhcpClientUtilConvertBitMasktoOption (INT4 *pOptions, UINT1 *u1Option,
                                      INT1 *iCnt)
{
    INT1                i1IndexCount = 0;
    INT1                i1BitPosCount = 0;

    /* Loop through the Index Count */
    for (i1IndexCount = 0; i1IndexCount < DHCP_OPTION_BLOCKS; i1IndexCount++)
    {
        /* Loop through all the bit positions */
        for (i1BitPosCount = 0; i1BitPosCount < DHCP_OPTION_BLOCK_SIZE;
             i1BitPosCount++)
        {
            /* Check whether the options are present */
            if (pOptions[i1IndexCount] & (1 << i1BitPosCount))
            {
                *u1Option = (UINT1)
                    ((i1IndexCount * DHCP_OPTION_BLOCK_SIZE) + i1BitPosCount);
                u1Option++;
                (*iCnt)++;
            }
        }
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpClientUtilCheckOption                         */
/*  Description     : This function is used to check whether the option */
/*                    is present in the Offer                           */
/*  Input(s)        : i1Option  - option to be checked                  */
/*                                                                      */
/*                    pDiscOptionNode  - Discovey Option Node Pointer   */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_FOUND/DHCP_NOT_FOUND                         */
/************************************************************************/

INT4
DhcpClientUtilCheckOption (INT4 i4Option, tDhcpDiscOption * pDiscOptionNode)
{
    UINT1               u1Index;
    UINT1               u1BitPos;
    INT4                ai4tmpOptions[DHCP_OPTION_BLOCKS] = { 0 };

    if ((DHCP_OPTION_BLOCKS * DHCP_OPTION_BLOCK_SIZE) <= i4Option)
    {
        return DHCP_NOT_FOUND;
    }

    /* Divide the option by 32 to find the index */
    u1Index = (UINT1) i4Option / DHCP_OPTION_BLOCK_SIZE;

    /* Mod the option by 32 to find the appropriate Bit position */
    u1BitPos = (UINT1) i4Option % DHCP_OPTION_BLOCK_SIZE;

    /* Update the bit mask in the appropriate place */
    ai4tmpOptions[u1Index] = (INT4) (ai4tmpOptions[u1Index] | (1 << u1BitPos));

    /* Check whether the options are present, if yes return Found else not found
     * */
    if (ai4tmpOptions[u1Index] & pDiscOptionNode->aiOptionBitMask[u1Index])
    {
        return DHCP_FOUND;
    }
    else
    {
        return DHCP_NOT_FOUND;
    }
}

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
/************************************************************************/
/*  Function Name   : DhcpClntArpRespTaskMain                           */
/*  Description     : This function starts a new task for processing the*/
/*                    ARP reply packet to check whether the IP offered  */
/*                    by server is already allocated                    */
/*  Input(s)        : i1pParam - Input Parameter                        */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpClntArpRespTaskMain (INT1 *i1pParam)
{

    UINT4               u4Event;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpClntArpRespTaskMain fn\n");

    if (OsixTskIdSelf (&gu4DhcpCArpTaskId) == OSIX_FAILURE)
    {
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    UNUSED_PARAM (i1pParam);
    gi4DhcpCArpSockDesc = -1;

    while (1)
    {
        OsixEvtRecv (gu4DhcpCArpTaskId,
                     (DHCPC_MSGQ_ARP), (OSIX_WAIT | OSIX_EV_ANY), &u4Event);

        DHCPC_TRC1 (DEBUG_TRC, "Rcvd Event %d\n", u4Event);

        if ((u4Event & DHCPC_MSGQ_ARP) == DHCPC_MSGQ_ARP)
        {
            DhcpCArpRespProcessQMsg ();
        }
    }
    DHCPC_TRC (DEBUG_TRC, "Exiting in DhcpClntArpRespTaskMain fn\n");
    return;
}

/************************************************************************/
/*  Function Name   : DhcpCArpRespSocketOpen                            */
/*  Description     : This function opens a raw socket to receive       */
/*                    ARP reply packets                                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpCArpRespSocketOpen (VOID)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpCArpSocketOpen fn\n");

    gi4DhcpCArpSockDesc = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL));

    if (gi4DhcpCArpSockDesc < 0)
    {
        return DHCP_FAILURE;
    }

    /* set options for non-blocking mode */
    if (fcntl (gi4DhcpCArpSockDesc, F_SETFL, O_NONBLOCK) < 0)
    {
        close (gi4DhcpCArpSockDesc);
        return DHCP_FAILURE;
    }

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCArpRespSocketClose                           */
/*  Description     : This function closes the raw socket after         */
/*                    receiving the required ARP reply packet           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpCArpRespSocketClose (VOID)
{
    INT4                i4RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhcpCArpSocketClose fn\n");

    if (gi4DhcpCArpSockDesc != -1)
    {
        i4RetVal = close (gi4DhcpCArpSockDesc);

        if (i4RetVal < 0)
        {
            /* DHCP Client socket close failed */
            DHCPC_TRC (FAIL_TRC,
                       "DHCP Client Arp Socket Close is not successful\n");
            return (DHCP_FAILURE);
        }
    }

    gi4DhcpCArpSockDesc = -1;

    return (DHCP_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpCArpRespProcessQMsg                           */
/*  Description     : This function processes the Q message             */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCArpRespProcessQMsg (VOID)
{
    tDhcpQMsg          *pDhcpQMsg = NULL;

    while (OsixQueRecv (gDhcpCArpMsgQId, (UINT1 *) (&pDhcpQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        DHCPC_TRC1 (DEBUG_TRC, "Rcvd Msg %x\n", pDhcpQMsg);

        switch (pDhcpQMsg->u4MsgType)
        {
            case DHCPC_ARP_REPLY:
                DhcpCArpRespRcvPkt (pDhcpQMsg->dhcpIfIndex,
                                    pDhcpQMsg->u4IpAddr);
                break;
            default:
                break;
        }

        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCQMsgPoolId,
                            (UINT1 *) pDhcpQMsg);
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpCArpRespRcvPkt                                */
/*  Description     : Function to listen on a raw socket to receive the */
/*                    ARP reply packet and update the ARP table based on*/
/*                    checking the destined Mac and IP address.         */
/*  Input(s)        : u4Port --> Port no.                               */
/*                  : u4ClientIpAddr --> IP address of the client       */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCArpRespRcvPkt (UINT4 u4Port, UINT4 u4ClientIpAddr)
{
    struct arphdr      *pArpHdr;
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;

    UINT4               u4SenderIp = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT1              *pArpPtr = NULL;
    INT1               *pEtherFrame = NULL;
    UINT1               au1ArpDstMac[CFA_ENET_ADDR_LEN];
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    INT4                i4ReadBytes;
    INT1                ai1Buff[DHCP_MAX_MTU];

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (ai1Buff, 0, DHCP_MAX_MTU);

    pEtherFrame = ai1Buff;
    pArpHdr =
        (struct arphdr *) (VOID *) (pEtherFrame + CFA_ENET_V2_HEADER_SIZE);

    /* Open Raw socket to listen ARP Response packets */
    if (DhcpCArpRespSocketOpen () != DHCP_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC, "DHCP client socket creation FAILURE\n");
        return;
    }
    DHCPC_ARP_RESP_PROC_FLAG = OSIX_TRUE;

    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return;
    }

    /* Get the Mac address corresponding to the Interface */
    ArpGetHwAddr ((UINT2) u4CfaIfIndex, au1HwAddr);

    while (DHCPC_ARP_RESP_PROC_FLAG)
    {
        MEMSET (pEtherFrame, 0, DHCP_MAX_MTU);
        i4ReadBytes =
            recvfrom (gi4DhcpCArpSockDesc, pEtherFrame, DHCP_MAX_MTU, 0, NULL,
                      0);
        if (i4ReadBytes > 1)
        {
            /* Process only ARP Response packets */
            if (ntohs (pArpHdr->ar_op) != ARP_RESPONSE)
            {
                continue;
            }

            /* Get the pointer beyond arp header.
               i.e. Get the pointer to sender hw address */
            pArpPtr = (UINT1 *) (pArpHdr + 1);
            /* To get sender ip address, Advance the pointer beyond sender
             * hw address*/
            pArpPtr += pArpHdr->ar_hln;
            MEMCPY (&u4SenderIp, pArpPtr, pArpHdr->ar_pln);
            u4SenderIp = OSIX_NTOHL (u4SenderIp);

            pArpPtr += pArpHdr->ar_pln;

            MEMSET (au1ArpDstMac, 0, pArpHdr->ar_hln);
            MEMCPY (au1ArpDstMac, pArpPtr, pArpHdr->ar_hln);

            /* Check if the ARP response is sent to the interface on which ARP req
             * is sent.
             * Check if the sender Ip matches the IP for which ARP request is sent
             */
            if ((u4ClientIpAddr == u4SenderIp) &&
                (MEMCMP (au1HwAddr, au1ArpDstMac, CFA_ENET_ADDR_LEN) == 0))
            {
                DhcpCArpUpdateEntry (u4Port, pArpHdr);
                break;
            }
        }
    }
    DHCPC_TRC (DEBUG_TRC, "DHCPC ARP RESP socket close\n");
    /* Close the Socket */
    DhcpCArpRespSocketClose ();
    return;
}

/************************************************************************/
/*  Function Name   : DhcpCArpRespSendEvent                             */
/*  Description     : Function to send a DHCP event to receive ARP reply*/
/*                    for the ARP request sent to check the presenc     */
/*                    of IP address assigned to any other host          */
/*  Input(s)        : pPktInfo - Received Packet                        */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
UINT1
DhcpCArpRespSendEvent (tDhcpPktInfo * pPktInfo)
{
    tDhcpQMsg          *pDhcpQMsg;

    if ((pDhcpQMsg =
         (tDhcpQMsg *) MemAllocMemBlk (gDhcpClientMemPoolId.DhcpCQMsgPoolId)) ==
        NULL)
    {
        DHCPC_TRC (FAIL_TRC, "DhcpQ memory alloc failure\n");
        return DHCP_FAILURE;
    }

    pDhcpQMsg->u4MsgType = DHCPC_ARP_REPLY;
    pDhcpQMsg->dhcpIfIndex = pPktInfo->u4IfIndex;
    pDhcpQMsg->u4IpAddr = pPktInfo->DhcpMsg.yiaddr;

    if (OsixQueSend (gDhcpCArpMsgQId,
                     (UINT1 *) &pDhcpQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC, "Send to Q failed\n");
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCQMsgPoolId,
                            (UINT1 *) pDhcpQMsg);
        return DHCP_FAILURE;
    }

    OsixEvtSend (gu4DhcpCArpTaskId, DHCPC_MSGQ_ARP);
    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCArpRespSendEvent fn\n");

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCArpUpdateEntry                               */
/*  Description     : Function to handle the ARP entry addition         */
/*  Input(s)        : u4IfIndex - Port number                           */
/*                    pArpHdr   - Arp packet received                   */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpCArpUpdateEntry (UINT4 u4IfIndex, struct arphdr *pArpHdr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    tARP_DATA           ArpData;
    UINT1              *pArpPtr = NULL;
    UINT1               au1HwMac[CFA_ENET_ADDR_LEN];
    UINT4               u4SenderIp = 0;

    MEMSET (au1HwMac, 0, CFA_ENET_ADDR_LEN);

    MEMSET (&(NetIpIfInfo), IP_ZERO, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return (DHCP_FAILURE);
    }

    pArpPtr = (UINT1 *) (pArpHdr + 1);
    MEMCPY (au1HwMac, pArpPtr, pArpHdr->ar_hln);

    /* To get sender ip address, Advance the pointer beyond sender
     * hw address*/
    pArpPtr += pArpHdr->ar_hln;
    MEMCPY (&u4SenderIp, pArpPtr, pArpHdr->ar_pln);
    u4SenderIp = OSIX_NTOHL (u4SenderIp);

    ArpData.u4IpAddr = u4SenderIp;
    ArpData.u2Port = (UINT2) u4IfIndex;
    ArpData.i2Hardware = ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);
    MEMCPY (ArpData.i1Hw_addr, (INT1 *) au1HwMac, CFA_ENET_ADDR_LEN);
    ArpData.u1EncapType = NetIpIfInfo.u1EncapType;
    ArpData.i1Hwalen = CFA_ENET_ADDR_LEN;
    ArpData.i1State = ARP_DYNAMIC;
    ArpData.u1RowStatus = ACTIVE;

    /* Add the ARP entry */
    arp_add (ArpData);

    return DHCP_SUCCESS;
}
#endif

#ifndef SNTP_WANTED
/*****************************************************************************
 *
 *    Function Name             : DhcpcProcessSntpServerParams
 *
 *    Description               : This stub function can be ported to interact 
 *                                with third party SNTP application.
 *
 *    Input(s)                  : u4PriSntpServerAddr - Primary SNTP server
 *                                address
 *                                u4SecSntpServerAddr - Secondary SNTP server
 *                                address
 *
 *    Output(s)                 : None. 
 *
 *    Returns                   : None.
 *****************************************************************************/
VOID
DhcpcProcessSntpServerParams (UINT4 u4PriSntpServerAddr,
                              UINT4 u4SecSntpServerAddr)
{
    UNUSED_PARAM (u4PriSntpServerAddr);
    UNUSED_PARAM (u4SecSntpServerAddr);
    return;
}
#endif
