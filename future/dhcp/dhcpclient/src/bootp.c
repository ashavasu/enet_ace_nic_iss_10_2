/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bootp.c,v 1.10 2014/07/12 12:26:11 siva Exp $
 *
 * Description:This file contains routines to frame Boot    
 *             request packets, transmit the packet and     
 *             parse the reply packet                       
 *
 *******************************************************************/

#include "dhccmn.h"
#include "msr.h"

/*-------------------------------------------------------------------+
 * Function           : BootpSendRequestPacket
 *
 * Input(s)           : u4IfIndex.
 *
 * Output(s)          : None.
 *
 * Returns            : BOOTP_SUCCESS if the Boot Request packet could be formed
 *                      successfully else
 *                      BOOTP_FAILURE.
 *
 * Global Variables
 *             Used   : None..
 *
 * Action :
 * This routine is called to form the BootRequest packet.
+-------------------------------------------------------------------*/
INT4
BootpSendRequestPacket (UINT4 u4IfIndex)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr = NULL;
    UINT1               u1RetVal;
    UINT4               u4Duration = 0;
    UINT2               u2OptionLen;
    UINT1              *pReqOptions;

/* For testing purposes, for sending valid BootReply packets */
    pDhcpCNode = DhcpCGetIntfRec (u4IfIndex);
    if (pDhcpCNode == NULL)
    {
        return BOOTP_FAILURE;
    }

    DHCPC_TRC (DEBUG_TRC, "Entered BootpSendRequestPacket fn\n");

    if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
    {
        DHCPC_TRC (FAIL_TRC,
                   "Packet Allocation FAILURE, BootpSendRequestPacket\n");
        return BOOTP_FAILURE;
    }

    pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;
    /* fill the fixed header */
    pOutHdr = &(pOutPkt->DhcpMsg);

    pOutHdr->Op = BOOTREQUEST;
    pOutHdr->htype = HWTYPE_ETHERNET;    /* since a default interface alone is                                              considered */
    pOutHdr->hlen = ENET_ADDR_LEN;
    pOutHdr->hops = 0;
    /* Fill the Transation ID in the packet and record the transaction ID in        the global Interface Table */

    pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid = BOOTP_XID;

    if ((pDhcpCNode->u1ReTxCount) == 0)
    {
        u4Duration = BootpCalculateTimeOut (FIRST_TIMER_FLAG);
    }
    else
    {
        u4Duration = BootpCalculateTimeOut (NEXT_TIMER_FLAG);
    }
    /* Start a single offer timer within which a DHCP_OFFER should
       have received */
    (pDhcpCNode->DhcIfInfo).u4Secs += u4Duration;

    pOutHdr->secs = (UINT2) u4Duration;
    pOutHdr->flags = DHCP_BROADCAST_MASK;    /* Broad cast the packet */

    pOutHdr->ciaddr = 0;
    pOutHdr->siaddr = 0;
    pOutHdr->yiaddr = 0;
    pOutHdr->giaddr = 0;

    DhcpCfaGddGetHwAddr (u4IfIndex, pOutHdr->chaddr);

    MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
    MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);
    /* Requesting of DHCP Default options */
    DhcpCopyDefaultOpt (pDhcpCNode);

    pReqOptions = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions;

    u2OptionLen = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions;

    DhcpCFillRequiredOptions (pOutPkt, pReqOptions, u2OptionLen);

    u1RetVal = (UINT1) DhcpCSendMessage (pOutPkt, BOOTP_PKT);

    if (u1RetVal != DHCP_SUCCESS)
    {
        /* free OutPkt Message Structure */
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                            (UINT1 *) pOutPkt->DhcpMsg.pOptions);
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                            (UINT1 *) pOutPkt);
        return BOOTP_FAILURE;
    }
    if (DhcpCInitTimer (pDhcpCNode, BOOTP_REQUEST_TIMER_ID,
                        u4Duration) == DHCP_FAILURE)
    {
        /* free OutPkt Message Structure */
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                            (UINT1 *) pOutPkt->DhcpMsg.pOptions);
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                            (UINT1 *) pOutPkt);
        return BOOTP_FAILURE;
    }

    (pDhcpCNode->u1ReTxCount)++;    /* Increment the ReTx Count */
    /* free OutPkt Message Structure */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                        (UINT1 *) pOutPkt);

    DHCPC_TRC (DEBUG_TRC, "Exitting BootpSendRequestPacket fn\n");
    return BOOTP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : BootpReqTmrExpiryHdlr. 
 *
 * Input(s)           : pDhcpCNode.
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Global Variables
 *             Used   : gDpTmrListId.
 *
 * Action :
 *
 * Called for handling timeout event
+-------------------------------------------------------------------*/
VOID
BootpReqTmrExpiryHdlr (tDhcpCRec * pDhcpCNode)
{

    DHCPC_TRC (DEBUG_TRC, "Entering BootpReqTmrExpiryHdlr fn\n");

    if (pDhcpCNode->u1ReTxCount < MAX_BOOT_RETRIES)
    {
        if (BootpSendRequestPacket (pDhcpCNode->u4IpIfIndex) == BOOTP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "BootpSendRequestPacket Failed\n");
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exitting BootpReqTmrExpiryHdlr fn\n");
}

/*-------------------------------------------------------------------+
 * Function           : BootpProcessReply.
 *
 * Input(s)           : pPktInfo, pDhcpCNode.
 *
 * Output(s)          : None
 *
 * Returns            : BOOTP_SUCCESS if the Boot information could be obtained 
 *                      and the Reply packet parsed successfully, else
 *                      BOOTP_FAILURE.
 *
 * Global Variables
 *             Used   : None.
 *
 * Action :
 * Parse the reply packet here and store the server address, bootfile name
 * in appropriate global structure, and call TFTP procedure to send the
 * Read Request packet
+-------------------------------------------------------------------*/
INT4
BootpProcessReply (tDhcpPktInfo * pPktInfo, tDhcpCRec * pDhcpCNode)
{
    tBestOffer         *pTempBestOffer = NULL;
    UINT4               u4TimeLeft = 0;
    UINT1               u1RetVal;
    UINT1               au1HwAddr[ENET_ADDR_LEN];
    UINT4               u4Temp = 0;

    DHCPC_TRC (DEBUG_TRC, "Entering BootpProcessReply fn\n");

    MEMSET (au1HwAddr, 0, ENET_ADDR_LEN);
    if ((pDhcpCNode == NULL) || (pPktInfo == NULL))
    {
        return BOOTP_FAILURE;
    }

    DhcpCfaGddGetHwAddr (pDhcpCNode->u4IpIfIndex, au1HwAddr);

    if (MEMCMP ((pPktInfo->DhcpMsg).chaddr, au1HwAddr, ENET_ADDR_LEN) != 0)
    {
        DHCPC_TRC (DEBUG_TRC, "Hwaddr mismatch Failure\n");
        return BOOTP_FAILURE;
    }
    /* Discard multiple valid boot replies */
    u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ReplyWaitTimer.
                                        DhcpAppTimer), &u4TimeLeft);

    if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));

    }

    if ((pDhcpCNode->DhcIfInfo).pBestOffer == NULL)
    {
        if ((pTempBestOffer =
             (tBestOffer *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                            DhcpCBestOffPoolId)) == NULL)
        {
            DHCPC_TRC (DEBUG_TRC, "Memallocation Failure\n");
            return BOOTP_FAILURE;
        }

        DhcpCGetReqOptSupported (pPktInfo, pDhcpCNode, pTempBestOffer);
        DHCPC_TRC (DEBUG_TRC, "First Offer received\n");

        /* When there is no offer till now received, the received
           offer is the best offer */
        (pDhcpCNode->DhcIfInfo).pBestOffer = pTempBestOffer;

        MEMCPY (&((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo),
                pPktInfo, sizeof (tDhcpPktInfo));
    }
    if ((DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                       pPktInfo->DhcpMsg.yiaddr)) ==
        DHCP_SUCCESS)
    {
        (pDhcpCNode->DhcIfInfo).u1State = S_BOUND;
        DHCPC_TRC (FAIL_TRC, "Configured the IP ADDRESS \n");
    }
    else
    {
        DHCPC_TRC (FAIL_TRC, "FAILED in Configuring the IP ADDRESS \n");
        MEM_FREE (pTempBestOffer);
        return (BOOTP_FAILURE);
    }

    /*
     * If node address is already known, and server does not speciy Yipaddr,
     * retain the already known address.
     */
    (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = pPktInfo->DhcpMsg.yiaddr;
    if (DhcpCGetOption (DHCP_OPT_SERVER_ID, pPktInfo) == DHCP_NOT_FOUND)
    {
        DHCPC_TRC (FAIL_TRC, "DhcpCGetOption FAILED \n");
        MEM_FREE (pTempBestOffer);
        return (BOOTP_FAILURE);
    }

    MEMCPY (&u4Temp, DhcpCTag.Val, DhcpCTag.Len);
    u4Temp = OSIX_NTOHL (u4Temp);
    (pDhcpCNode->DhcIfInfo).u4ServerOfferred = u4Temp;

    /* print the current bindings; only if the trace level is enabled */
    if (gu4DhcpCDebugMask & BIND_TRC)
    {
        DhcpCShowBinding (pPktInfo->u4IfIndex);
    }

    DHCPC_TRC (DEBUG_TRC, "Exitting BootpProcessReply fn\n");
    return (BOOTP_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : BootpCalculateTimeOut
 *
 * Input(s)           : None
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Global Variables
 *             Used   : None
 *
 * Side effects       : None
 * 
 * Action :
 *
 * Called before adding the retransmission timer for BOOTP. This routine
 * updates the corresponding global variable.
+-------------------------------------------------------------------*/
UINT2
BootpCalculateTimeOut (UINT1 u1Flag)
{
    static UINT4        u4Mask;
    static UINT2        u2WaitTime;
    static UINT2        u2BootreqTimeout;

    if (u1Flag == FIRST_TIMER_FLAG)
    {
        u4Mask = 1;
        u2WaitTime = 1;
        u2BootreqTimeout = 1;
    }

    /* 
     * If the timer value has exceeded 60 seconds, do not increase the timeout
     * value , but just randomize each time
     */
    if (u2BootreqTimeout < 60)
    {
        u2WaitTime *= 2;
    }

    /* 
     * Randomize the time; start with a mask and 'and' it with a mask ,
     * increase the length of the mask by one bit each time
     */
    u2BootreqTimeout = (UINT2) (u2WaitTime + (rand () & u4Mask));
    u4Mask = u4Mask << 1;
    return (u2BootreqTimeout);
}
