/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcred.c,v 1.5 2013/09/07 10:57:10 siva Exp $
 *
 * Description: This file contains DHCP Redundancy support routines
 and utility routines.
 *********************************************************************/
#ifndef _DHCRED_C_
#define _DHCRED_C_

#include "dhccmn.h"

/*****************************************************************************
 * FUNCTION NAME      : DhCRedInitGlobalInfo
 *
 * DESCRIPTION        : This function initializes global variable used to
 *                      store DHCP Client Redundancy information
 *
 * INPUT              : None
 *
 * OUTPUT             : None
 *
 * RETURNS            : None
 *
 *****************************************************************************/
PUBLIC INT4
DhCRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedInitGlobalInfo fn\n");

    MEMSET (&(RmRegParams), 0, sizeof (tRmRegParams));
    MEMSET (&(gDhcpCRedGlobalInfo), 0, sizeof (tDhcpCRedGlobalInfo));

    RmRegParams.u4EntId = RM_DHCPC_APP_ID;
    RmRegParams.pFnRcvPkt = DhCRedRmCallBack;
    if (DhCRedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedInitGlobalInfo: DhCRedRmRegisterProtocols FAILED\r\n");
        return OSIX_FAILURE;
    }

    DHCPC_RM_NODE_STATUS () = DHCPC_IDLE_NODE;
    DHCPC_NUM_STANDBY_NODES () = 0;
    gDhcpCRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedInitGlobalInfo fn\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : DhCRedRmRegisterProtocols
 *
 * DESCRIPTION        : Registers DHCP client with RM by providing an 
 *                      application ID for DHCP client and a call back function
 *                      to be called whenever RM needs to send an event to DHCP
 *
 * INPUT              : None
 *
 * OUTPUT             : None
 *
 * RETURNS            : OSIX_SUCCESS - if registration is successful
 *                      OSIX_FAILURE - otherwise
 *
 *****************************************************************************/
PUBLIC INT4
DhCRedRmRegisterProtocols (tRmRegParams * pRmRegParams)
{
    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedRmRegisterProtocols fn\n");

    if (RmRegisterProtocols (pRmRegParams) == RM_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedRmRegisterProtocols: Registration with RM FAILED\r\n");
        return OSIX_FAILURE;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedRmRegisterProtocols fn\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : DhCRedDeInitGlobalInfo
 *
 * DESCRIPTION        : This function deregisters itself from RM.
 *
 * INPUT              : None
 *
 * OUTPUT             : None
 *
 * RETURNS            : None
 *
 *****************************************************************************/
PUBLIC VOID
DhCRedDeInitGlobalInfo (VOID)
{
    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedDeInitGlobalInfo fn\n");

    if (DhCRedRmDeRegisterProtocols () == OSIX_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedDeInitGlobalInfo: DhCRedRmDeRegisterProtocols FAILED\r\n");
        return;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedDeInitGlobalInfo fn\n");

    return;
}

/*****************************************************************************
 * FUNCTION NAME      : DhCRedRmDeRegisterProtocols
 *
 * DESCRIPTION        : Deregisters DHCP with RM
 *
 * INPUT              : None
 *
 * OUTPUT             : None
 *
 * RETURNS            : OSIX_SUCCESS - if registration is successful
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
DhCRedRmDeRegisterProtocols (VOID)
{
    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedRmDeRegisterProtocols fn\n");

    if (RmDeRegisterProtocols (RM_DHCPC_APP_ID) == RM_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedRmDeRegisterProtocols: Registration with RM FAILED\r\n");
        return OSIX_FAILURE;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedRmDeRegisterProtocols fn\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : DhCRedRmCallBack
 * DESCRIPTION        : This API constructs a message containing the
 *                      given RM event and RM message and posts it to the
 *                      DHCP Client queue and sends an event
 * INPUT              : u1Event   - Event type given by RM module
 *                      pData     - RM Message to enqueue
 *                      u2DataLen - Message size
 * OUTPUT             : None
 * RETURNS            : OSIX_SUCCESS - if msg is enqueued and event sent
 *                      OSIX_FAILURE - otherwise
 *****************************************************************************/
PUBLIC INT4
DhCRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tDhcpQMsg          *pMsg = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedRmCallBack fn\n");

    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedRmCallBack: This "
                   "Event is not associated with RM !!!\r\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send these events to DHCP Client task. */
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedRmCallBack: Queue Message "
                   "associated with Event is not sent by RM !!!\r\n");
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tDhcpQMsg *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                       DhcpCQMsgPoolId)) == NULL)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedRmCallBack: Q message allocation failure\n\r");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tDhcpQMsg));

    pMsg->u4MsgType = DHCPC_RM_MSG;
    pMsg->unDhcpMsgIfParam.RmCtrlMsg.pData = pData;
    pMsg->unDhcpMsgIfParam.RmCtrlMsg.u1Event = u1Event;
    pMsg->unDhcpMsgIfParam.RmCtrlMsg.u2DataLen = u2DataLen;

    if (OsixQueSend (gDhcpCMsgQId,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCQMsgPoolId,
                            (UINT1 *) pMsg);
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        DHCPC_TRC (FAIL_TRC, "DhCRedRmCallBack: Q send failure\n\r");
        return OSIX_FAILURE;
    }

    OsixEvtSend (gu4DhcpCTaskId, DHCPC_MSGQ_IF_EVENT);

    DHCPC_TRC (DEBUG_TRC, "Exiting in DhCRedRmCallBack fn\n");

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from
 *                    RM module.
 *
 * INPUT            : pMsg - pointer to DHCP Queue message.
 *
 * OUTPUT           : None.
 *
 * RETURNS          : None.
 *
 **************************************************************************/
PUBLIC VOID
DhCRedHandleRmEvents (tDhcpQMsg * pMsg)
{
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    tRmNodeInfo        *pData = NULL;
    UINT4               u4SeqNum = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleRmEvents fn\n");

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->unDhcpMsgIfParam.RmCtrlMsg.u1Event)
    {
        case GO_ACTIVE:

            DHCPC_TRC (EVENT_TRC,
                       "DhCRedHandleRmEvents: GO_ACTIVE event reached\r\n");

            DhCRedHandleGoActive ();
            break;

        case GO_STANDBY:

            DHCPC_TRC (EVENT_TRC,
                       "DhCRedHandleRmEvents: GO_STANDBY event reached\r\n");

            DhCRedHandleGoStandby ();
            break;

        case RM_STANDBY_UP:

            DHCPC_TRC (EVENT_TRC,
                       "DhCRedHandleRmEvents: RM_STANDBY_UP event reached"
                       " Updating Standby Nodes Count.\r\n");

            pData = (tRmNodeInfo *) pMsg->unDhcpMsgIfParam.RmCtrlMsg.pData;

            if (pMsg->unDhcpMsgIfParam.RmCtrlMsg.u2DataLen !=
                RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                RmReleaseMemoryForMsg ((UINT1 *) pData);
                DHCPC_TRC (FAIL_TRC,
                           "DhCRedHandleRmEvents: Data length is incorrect\r\n");
                break;
            }

            gDhcpCRedGlobalInfo.u1NumPeersUp = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);

            /* Before the arrival of STANDBY_UP event, Bulk Request has
             * arrived from Peer DHCPC in the Standby node. Hence send the
             * Bulk updates Now.
             */

            if (gDhcpCRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
            {
                gDhcpCRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
                DhCRedSendDynamicBulkMsg ();
            }
            break;

        case RM_STANDBY_DOWN:

            DHCPC_TRC (EVENT_TRC,
                       "DhCRedHandleRmEvents: RM_STANDBY_DOWN event reached"
                       " Updating Standby Nodes Count.\r\n");

            pData = (tRmNodeInfo *) pMsg->unDhcpMsgIfParam.RmCtrlMsg.pData;

            if (pMsg->unDhcpMsgIfParam.RmCtrlMsg.u2DataLen !=
                RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                RmReleaseMemoryForMsg ((UINT1 *) pData);
                DHCPC_TRC (FAIL_TRC,
                           "DhCRedHandleRmEvents: Data length is incorrect\r\n");
                break;
            }

            gDhcpCRedGlobalInfo.u1NumPeersUp = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:

            DHCPC_TRC (EVENT_TRC,
                       "DhCRedHandleRmEvents: RM_CONFIG_RESTORE_COMPLETE event reached\r\n");

            if (DHCPC_RM_NODE_STATUS () == RM_INIT)
            {
                if (DHCPC_RM_GET_NODE_STATUS () == RM_STANDBY)
                {
                    DhCRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                    {
                        DHCPC_TRC (FAIL_TRC,
                                   "DhCRedHandleRmEvents: Acknowledgement "
                                   "to RM for GO_STANDBY event failed!!!!"
                                   "\r\n");
                    }
                }
            }

            break;

        case L2_INITIATE_BULK_UPDATES:

            DHCPC_TRC (FAIL_TRC,
                       "DhCRedHandleRmEvents: L2_INITIATE_BULK_UPDATES event"
                       " reached, Sending Bulk Request!!\r\n");

            DhCRedSendBulkReqMsg ();
            break;

        case RM_MESSAGE:

            DHCPC_TRC (EVENT_TRC,
                       "DhCRedHandleRmEvents: RM_MESSAGE event reached\r\n");

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->unDhcpMsgIfParam.RmCtrlMsg.pData,
                               &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->unDhcpMsgIfParam.RmCtrlMsg.pData,
                                 pMsg->unDhcpMsgIfParam.RmCtrlMsg.u2DataLen);

            ProtoAck.u4AppId = RM_DHCPC_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (DHCPC_RM_NODE_STATUS () == RM_ACTIVE)
            {
                DhCRedProcessPeerMsgAtActive (pMsg->unDhcpMsgIfParam.RmCtrlMsg.
                                              pData,
                                              pMsg->unDhcpMsgIfParam.RmCtrlMsg.
                                              u2DataLen);
            }
            else if (DHCPC_RM_NODE_STATUS () == RM_STANDBY)
            {
                DhCRedProcessPeerMsgAtStandby (pMsg->unDhcpMsgIfParam.RmCtrlMsg.
                                               pData,
                                               pMsg->unDhcpMsgIfParam.RmCtrlMsg.
                                               u2DataLen);
            }
            else
            {
                DHCPC_TRC (FAIL_TRC,
                           "DhCRedHandleRmEvents: Sync-up message"
                           "received at Idle Node!!!!\r\n");
            }

            RM_FREE (pMsg->unDhcpMsgIfParam.RmCtrlMsg.pData);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        default:
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedHandleRmEvents: Invalid"
                       " RM event received\r\n");
            break;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleRmEvents fn\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleGoActive
 *
 * DESCRIPTION      : This routine handles the GO_ACTIVE event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
DhCRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleGoActive fn\n\r");

    if (DHCPC_RM_NODE_STATUS () == RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoActive: GO_ACTIVE event reached"
                   " when node is already active!!!!\r\n");
        return;
    }

    if (DHCPC_RM_NODE_STATUS () == RM_INIT)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoActive: Idle to Active" " transition...\r\n");

        DhCRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (DHCPC_RM_NODE_STATUS () == RM_STANDBY)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoActive: Standby to Active"
                   " transition...\r\n");
        DhCRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    if (gDhcpCRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
    {
        /* Before the arrival of GO_ACTIVE event, Bulk Request has
         * arrived from Peer DHCP Client in the Standby node. Hence
         * sent the Bulk updates Now.
         */
        gDhcpCRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
        DhCRedSendDynamicBulkMsg ();
    }

    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoActive: Acknowledgement to RM for"
                   "GO_ACTIVE event failed!!!!\r\n");
        return;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleGoActive fn\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleGoStandby
 *
 * DESCRIPTION      : This routine handles the GO_STANDBY event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
DhCRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleGoStandby fn\n");

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (DHCPC_RM_NODE_STATUS () == RM_STANDBY)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoStandby: GO_STANDBY event reached"
                   " when node is already standby!!!!\r\n");
        return;
    }

    if (DHCPC_RM_NODE_STATUS () == RM_INIT)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoStandby: GO_STANDBY event received"
                   " when state is Idle.\r\n");

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */
    }
    else if (DHCPC_RM_NODE_STATUS () == RM_ACTIVE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedHandleGoStandby: Active to Standby"
                   " transition...\r\n");
        DhCRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedHandleGoStandby: Acknowledgement to RM for"
                       "GO_STANDBY event failed!!!!\r\n");
            return;
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleGoStandby fn\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleStandbyToActive 
 *
 * DESCRIPTION      : On Standby to Active the following actions are performed
 *                    1. Update the Node Status and standby node count
 *                    2. Walk through the entire hash table and 
 *                       start the appropriate timers.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedHandleStandbyToActive (VOID)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    t_ARP_PKT           ArpPkt;
    UINT4               u4Index = 0;
    UINT4               u4StartTime = 0;
    UINT4               u4LeaseTime = 0;
    UINT4               u4LeasedIpAddr = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4RetValIfIpAddr = 0;
    UINT1               u1State = 0;
    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleStandbyToActive fn\n");

    /* Update the Node Status and standby node count */
    DHCPC_RM_NODE_STATUS () = RM_ACTIVE;
    DHCPC_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Walk through the entire hash table and start the timers */

    for (u4Index = 0; u4Index < DHCPC_MAX_INTERFACE; u4Index++)
    {
        pDhcpCNode = DhcpCGetIntfRec (u4Index);
        if (pDhcpCNode == NULL)
        {
            continue;
        }
        else
        {
            u1State = (pDhcpCNode->DhcIfInfo).u1State;
            switch (u1State)
            {
                case S_INIT:
                    /*Intentional Fall Through */
                case S_SELECTING:
                    /*Intentional Fall Through */
                case S_REQUESTING:
                    /*Intentional Fall Through */
                case S_INITREBOOT:
                    /*Intentional Fall Through */
                case S_REBOOTING:
                    /* State Machine is reset */
                    DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                                  DHCPC_AUTO_CONFIG_IP_ADDRESS);
                    (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = DHCPC_RESET;
                    (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = DHCPC_RESET;
                    DhcpCInitState (pDhcpCNode);
                    continue;
                case S_BOUND:
                    /*Intentional Fall Through */
                case S_RENEWING:
                    /*Intentional Fall Through */
                case S_REBINDING:
                    /* inform the IP to cfa */
                    break;

                case S_UNUSED:
                    /*Intentional Fall Through */
                case S_NULL:
                    /*Intentional Fall Through */
                default:
                    /* fetch next record */
                    continue;
            }
            u4StartTime = (pDhcpCNode->DhcIfInfo).u4Secs;
            u4LeaseTime = (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred;
            u4LeasedIpAddr = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;
            u1State = CalRenewRebindTime (pDhcpCNode, u4StartTime, u4LeaseTime);

            if (u1State == S_INIT)
            {
                /* State Machine is reset */
                DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                              DHCPC_AUTO_CONFIG_IP_ADDRESS);
                (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = DHCPC_RESET;
                (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = DHCPC_RESET;
                DhcpCInitState (pDhcpCNode);
                continue;
            }

            /* Write the availabe lease info into config file */
            /* Configure the Interface with the IP Address Leased */
            if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                              &u4CfaIfIndex) == NETIPV4_FAILURE)
            {
                continue;
            }

            if (CfaGetIfIpAddr (u4CfaIfIndex, &u4RetValIfIpAddr) ==
                OSIX_FAILURE)
            {
                continue;
            }

            if (u4RetValIfIpAddr == u4LeasedIpAddr)
            {
                continue;
            }

            if ((DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                               u4LeasedIpAddr)) == DHCP_SUCCESS)
            {
                DHCPC_TRC1 (DEBUG_TRC,
                            "Configured the IP ADDRESS Leased in %x intf\n",
                            pDhcpCNode->u4IpIfIndex);
            }
            else
            {
                DHCPC_TRC1 (FAIL_TRC,
                            "FAILED in Configuring the IP ADDRESS in %x intf\n ",
                            pDhcpCNode->u4IpIfIndex);
                continue;
            }

            (pDhcpCNode->DhcIfInfo).u1State = u1State;

            MEMSET (&ArpPkt, 0, sizeof (t_ARP_PKT));

            DhcpCfaGddGetHwAddr (pDhcpCNode->u4IpIfIndex,
                                 (UINT1 *) ArpPkt.i1Shwaddr);

            MEMSET (ArpPkt.i1Thwaddr, 0xff, CFA_ENET_ADDR_LEN);
            ArpPkt.i2Opcode = ARP_RESPONSE;
            ArpPkt.u4Sproto_addr = u4LeasedIpAddr;
            ArpPkt.u4Tproto_addr = IP_GEN_BCAST_ADDR;

            if (ArpSendReqOrResp ((UINT2) pDhcpCNode->u4IpIfIndex,
                                  CFA_ENCAP_ENETV2, &ArpPkt) == ARP_FAILURE)
            {
                DHCPC_TRC (FAIL_TRC, "Send ARP Response Failed\r\n");
            }
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleStandbyToActive fn\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleActiveToStandby 
 *
 * DESCRIPTION      : On Active to Standby transition, the following actions 
 *                    are performed,
 *                    1. Update the Node Status and Standby node count
 *                    2. Walk through the entire hash table and 
 *                       stop the appropriate timers.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedHandleActiveToStandby (VOID)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT4               u4Index = 0;
    UINT4               u4TimeLeft = 0;
    UINT1               u1RetVal = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleActiveToStandby fn\n");

    /* Update the Node Status and standby node count */
    DHCPC_RM_NODE_STATUS () = RM_STANDBY;
    DHCPC_NUM_STANDBY_NODES () = 0;

    /* Walk through the entire hash table and stop the timers */

    for (u4Index = 0; u4Index < DHCPC_MAX_INTERFACE; u4Index++)
    {
        pDhcpCNode = DhcpCGetIntfRec (u4Index);
        if (pDhcpCNode == NULL)
        {
            continue;
        }
        else
        {
            /* Stop the LeaseTimer if it is running */

            u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                              &(pDhcpCNode->LeaseTimer.
                                                DhcpAppTimer), &u4TimeLeft);

            if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            {
                DhcpCStopTimer (DhcpCTimerListId,
                                &(pDhcpCNode->LeaseTimer.DhcpAppTimer));
            }

            /* Stop the ReplyWaitTimer if it is running */
            u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                              &(pDhcpCNode->ReplyWaitTimer.
                                                DhcpAppTimer), &u4TimeLeft);

            if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            {
                DhcpCStopTimer (DhcpCTimerListId,
                                &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
            }

            /* Stop the ArpCheckTimer if it is running */
            u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                              &(pDhcpCNode->ArpCheckTimer.
                                                DhcpAppTimer), &u4TimeLeft);

            if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            {
                DhcpCStopTimer (DhcpCTimerListId,
                                &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer));
            }

            /* Stop the ReleaseWaitTimer if it is running */
            u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                              &(pDhcpCNode->ReleaseWaitTimer.
                                                DhcpAppTimer), &u4TimeLeft);

            if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            {
                DhcpCStopTimer (DhcpCTimerListId,
                                &(pDhcpCNode->ReleaseWaitTimer.DhcpAppTimer));
            }
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleActiveToStandby fn\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleIdleToStandby 
 *
 * DESCRIPTION      : This routine updates the node status.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedHandleIdleToStandby (VOID)
{
    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleIdleToStandby fn\n");

    /* Update the Node Status and standby node count */

    DHCPC_RM_NODE_STATUS () = RM_STANDBY;
    DHCPC_NUM_STANDBY_NODES () = 0;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleIdleToStandby fn\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleIdleToActive 
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Idle to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedHandleIdleToActive (VOID)
{
    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedHandleIdleToActive fn\n");

    DHCPC_RM_NODE_STATUS () = RM_ACTIVE;
    DHCPC_RM_GET_NUM_STANDBY_NODES_UP ();

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedHandleIdleToActive fn\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedProcessPeerMsgAtActive
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Standby) at Active. The messages that are handled in
 *                    Active node are
 *                    1. RM_BULK_UPDT_REQ_MSG
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedProcessPeerMsgAtActive fn\n");

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;

    DHCPC_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    DHCPC_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at 
         * active node is Bulk Request message which has only Type and 
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhcpClnRedHandleRmEvents: Invalid"
                       "DhCRedProcessPeerMsgAtActive: Indication of error"
                       " to RM to process Bulk request failed!!!!\r\n");
        }
        return;
    }

    if (u2Length != DHCPC_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhcpClnRedHandleRmEvents: Invalid"
                       "DhCRedProcessPeerMsgAtActive: Indication of error"
                       " to RM to process Bulk request" " failed!!!!\r\n");
        }

        return;
    }

    if (u1MsgType == DHCPC_RED_BULK_REQ_MESSAGE)
    {
        if (gDhcpCRedGlobalInfo.u1NumPeersUp == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gDhcpCRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        DhCRedSendDynamicBulkMsg ();
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedProcessPeerMsgAtActive fn\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedProcessPeerMsgAtStandby
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Active) at standby. The messages that are handled in
 *                    Standby node are
 *                    1. RM_BULK_UPDT_TAIL_MSG
 *                    2. Dynamic sync-up messages
 *                    3. Dynamic bulk messages
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedProcessPeerMsgAtStandby fn\n");

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;

    u2MinLen = DHCPC_RED_TYPE_FIELD_SIZE + DHCPC_RED_LEN_FIELD_SIZE;

    while ((u2OffSet + u2MinLen) <= u2DataLen)
    {
        DHCPC_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        DHCPC_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedProcessPeerMsgAtStandby: "
                       "Length field in the RM packet "
                       "is less than minimum number of bytes\r\n");
            u2OffSet += u2RemMsgLen;
            continue;
        }

        if ((u2OffSet + u2RemMsgLen) > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing
             * with the next packet */
            DHCPC_TRC2 (FAIL_TRC,
                        "DhCRedProcessPeerMsgAtStandby: "
                        "Length field %d in the RM packet of len %d "
                        "is wrong\r\n", u2OffSet + u2RemMsgLen, u2DataLen);
            u2OffSet = u2DataLen;
            continue;
        }

        switch (u1MsgType)
        {
            case DHCPC_RED_BULK_UPD_TAIL_MESSAGE:
                /* Validate the length */
                if (u2Length != DHCPC_RED_BULK_UPD_TAIL_MSG_SIZE)
                {
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    DHCPC_TRC (FAIL_TRC,
                               "DhCRedProcessPeerMsgAtStandby: "
                               " Length validation failed for DHCPC_RED_BULK_UPD_TAIL_MSG_SIZE\r\n");

                    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                    {
                        DHCPC_TRC (FAIL_TRC,
                                   "DhCRedProcessPeerMsgAtStandby: Indication of error"
                                   " to RM to process Bulk update tail message"
                                   " failed!!!!\r\n");
                    }
                    u2OffSet += u2RemMsgLen;    /* Skip the attribute */
                    break;
                }
                DhCRedProcessBulkTailMsg (pMsg, &u2OffSet);
                break;
            case DHCPC_RED_BULK_CLIENT_INFO:
                /* Intentional Fall through */
            case DHCPC_RED_CLIENT_INFO:
                /* Validate the length */
                if (u2Length > DHCPC_RED_DYN_CLIENT_REC_SIZE)
                {
                    DHCPC_TRC (FAIL_TRC,
                               "DhCRedProcessPeerMsgAtStandby: "
                               " Length validation failed for Client Record\r\n");
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;

                    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                    {
                        DHCPC_TRC (FAIL_TRC,
                                   "DhCRedProcessPeerMsgAtStandby: Indication of error"
                                   " to RM to process Bulk update tail message"
                                   " failed!!!!\r\n");
                    }
                    u2OffSet += u2RemMsgLen;    /* Skip the attribute */
                    break;
                }
                DhCRedProcessDynamicClientInfo (pMsg, &u2OffSet);
                break;
            default:
                u2OffSet += u2RemMsgLen;    /* Skip the attribute */
                break;

        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedProcessPeerMsgAtStandby fn\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedProcessBulkTailMsg 
 *
 * DESCRIPTION      : This routine process the bulk update tail message and 
 *                    send bulk updates.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : pu2OffSet - Offset Value
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    MEMSET (&ProtoEvt, 0, sizeof (ProtoEvt));

    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;

    DHCPC_TRC (FAIL_TRC,
               "DhCRedProcessBulkTailMsg: Bulk Update Tail Message"
               " received at Standby node...\r\n");

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedProcessBulkTailMsg: Indication of error"
                   " to RM to process Bulk update completion"
                   " failed!!!!\r\n");
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendDynamicBulkMsg 
 *
 * DESCRIPTION      : This function sends the Bulk update messages to the
 *                    peer standby DHCP. After sending bulk messages, sends the
 *                    bulk update tail message and sets the bulk update 
 *                    complete status at active RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedSendDynamicBulkMsg (VOID)
{
    if ((gDhcpCRedGlobalInfo.u1NumPeersUp == 0))
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhcpCRedRmCallBack:" " No Standby node available\r\n");

        RmSetBulkUpdatesStatus (RM_DHCPC_APP_ID);
        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        DhCRedSendBulkUpdTailMsg ();
        return;
    }

    /* Actual Bulk msg to be sent */

    DhCRedSendClientInfoBulk ();

    RmSetBulkUpdatesStatus (RM_DHCPC_APP_ID);
    /* Send the tail msg to indicate the completion of Bulk
     * update process.
     */
    DhCRedSendBulkUpdTailMsg ();

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendClientInfoBulk
 *
 * DESCRIPTION      : This function sends the Bulk update messages to the
 *                    peer standby DHCP. The message includes the entire
 *                    hash table of the tDhcpCRec database
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedSendClientInfoBulk (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    tDhcpCRec          *pDhcpCNode = NULL;
    tDhcpCIfTable      *pDhcIfInfo = NULL;
    tClientConf        *pDhcpClientConfInfo = NULL;
    tBestOffer         *pBestOffer = NULL;
    tDhcpPktInfo       *pPktInfo = NULL;
    tDhcpMsgHdr        *pDhcpMsg = NULL;
    tClientId          *pClientId = NULL;
    UINT1              *pDefaultOptions = NULL;
    UINT4               u4Index = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4ExpiredTime = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedSendClientInfoBulk fn\n");

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgLen = DHCPC_RED_TYPE_FIELD_SIZE +
        DHCPC_RED_LEN_FIELD_SIZE + DHCPC_RED_DYN_CLIENT_REC_SIZE;

    if (pMsg == NULL)
    {
        pMsg = DhCRedGetMsgBuffer (DHCPC_RED_MAX_MSG_SIZE);

        if (pMsg == NULL)
        {
            return;
        }
    }

    /* Walk through the entire hash table and start the timers */

    for (u4Index = 0; u4Index <= DHCPC_MAX_INTERFACE; u4Index++)
    {
        pDhcpCNode = DhcpCGetIntfRec (u4Index);

        if (pDhcpCNode != NULL)
        {
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPC_RED_BULK_CLIENT_INFO);

            /* Initially Set the Length of RM msg to Maximum */

            u2LenOffSet = u2OffSet;
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                                 DHCPC_RED_DYN_CLIENT_REC_SIZE);

            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpCNode->u4IpIfIndex);

            /* DHCP CLIENT INTERFACE INFO BEGIN */

            pDhcIfInfo = &(pDhcpCNode->DhcIfInfo);

            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u4Xid);

            /* Time sync between standby and active */

            DHCPC_GET_TIME_IN_SECS (u4CurrentTime);
            u4ExpiredTime = u4CurrentTime - pDhcIfInfo->u4Secs;
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4ExpiredTime);

            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                 pDhcIfInfo->u4IpAddressOfferred);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                 pDhcIfInfo->u4LeaseTimeOfferred);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u4ServerOfferred);

            /* CLIENT CONF INFO BEGIN */

            pDhcpClientConfInfo = &((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo);

            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                 pDhcpClientConfInfo->u4StartOfLeaseTime);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                                 pDhcpClientConfInfo->u2LenOfOptions);
            pDefaultOptions = pDhcpClientConfInfo->pDefaultOptions;
            if (pDefaultOptions == NULL)
            {
                u2OffSet = 0;
                continue;
            }
            DHCPC_RM_PUT_N_BYTE (pMsg, pDefaultOptions, &u2OffSet,
                                 pDhcpClientConfInfo->u2LenOfOptions);

            /* CLIENT CONF INFO END */

            /* BEST OFFER INFO BEGIN */

            pBestOffer = pDhcIfInfo->pBestOffer;

            if (pBestOffer == NULL)
            {
                u2OffSet = 0;
                continue;
            }
            pPktInfo = &(pBestOffer->PktInfo);
            pDhcpMsg = &(pPktInfo->DhcpMsg);

            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->Op);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->htype);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->hlen);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->hops);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->xid);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pDhcpMsg->secs);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pDhcpMsg->flags);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->ciaddr);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->yiaddr);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->siaddr);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->giaddr);
            DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->chaddr, &u2OffSet, 16);
            DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->sname, &u2OffSet, 64);
            DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->file, &u2OffSet, 128);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pPktInfo->u4IfIndex);
            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pPktInfo->u4DstIpAddr);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pPktInfo->u2Len);
            if (pDhcpMsg->pOptions == NULL)
            {
                u2OffSet = 0;
                continue;
            }
            DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->pOptions, &u2OffSet,
                                 pPktInfo->u2Len);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pPktInfo->u2BufLen);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pPktInfo->u2MaxMessageSize);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pPktInfo->u1OverLoad);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pPktInfo->u1Type);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                                 pBestOffer->u1NoOfReqOptSupported);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                                 pBestOffer->u1TotalNoOfOptionPresent);

            /* BEST OFFER INFO END */

            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u1State);

            /* DHCP CLIENT INTERFACE INFO END */

            /* CLIENT ID INFO BEGIN */

            pClientId = &(pDhcpCNode->ClientId);

            DHCPC_RM_PUT_N_BYTE (pMsg, pClientId->au1ChAddr, &u2OffSet,
                                 DHCP_MAX_CID_LEN);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pClientId->u1HwType);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pClientId->u1Len);

            /* CLIENT ID INFO END */

            DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpCNode->u4ProcessedAck);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpCNode->u1ReTxCount);
            DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                                 pDhcpCNode->u1DhcpCInformMsgSent);
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                                 pDhcpCNode->u2RenewTimerCount);

            /* UPDATE THE ACTUAL LENGTH */
            DHCPC_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
        }

        /* SEND THE BULK MESSAGE TO RM */
        if ((((DHCPC_RED_MAX_MSG_SIZE - u2OffSet) < u2MsgLen) || (u4Index == DHCPC_MAX_INTERFACE)) && (u2OffSet != 0))    /* Validate length of msg */
        {
            /* Send the Message
             * 1. When there is no room available for 
             *    additional DHCP client records
             * 2. When there is no more Client records to be
             *    clubbed. */

            /* This routine sends the message to RM and in case of failure
             * releases the RM buffer memory
             */

            if (DhCRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                {
                    DHCPC_TRC (FAIL_TRC,
                               "DhCRedSendClientInfoBulk:"
                               "Ack to RM - Send to RM failed!!!!\r\n");
                }
            }

            /* Now Allocate Memory for more protection groups */
            pMsg = DhCRedGetMsgBuffer (DHCPC_RED_MAX_MSG_SIZE);

            if (pMsg == NULL)
            {
                return;
            }
            u2OffSet = 0;
        }
    }
    if (u2OffSet == 0)
    {
        /* for freeing the additional buffer which 
         *  was never filled but allocated
         */
        RM_FREE (pMsg);
    }
    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedSendClientInfoBulk fn\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedGetMsgBuffer 
 *
 * DESCRIPTION      : This routine returns CRU buffer memory from RM. In case 
 *                    of failure, it sends indication to RM about it.
 *
 * INPUT            : u2BufSize - Buffer Size.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : pMsg - Allocated Tx Buffer
 * 
 **************************************************************************/
PUBLIC tRmMsg      *
DhCRedGetMsgBuffer (UINT2 u2BufSize)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;

        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedGetMsgBuffer: Ack to RM - "
                       "Sync-up Tx" " buffer Memory allocation failed!!!!\r\n");
        }
    }
    return pMsg;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendBulkUpdTailMsg 
 *
 * DESCRIPTION      : This function will send the bulk update tail msg to the
 *                    standby node, which indicates the completion of Bulk
 *                    update process.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None 
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * DHCPC_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    pMsg = DhCRedGetMsgBuffer (DHCPC_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (pMsg == NULL)
    {
        return;
    }

    u2OffSet = 0;

    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPC_RED_BULK_UPD_TAIL_MESSAGE);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, DHCPC_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure 
     * releases the RM buffer memory
     */
    if (DhCRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhcpCRedSendPgDynamicBulkMsg: Ack to RM -"
                       " Send to RM failed!!!!\r\n");
        }

        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendBulkReqMsg 
 *
 * DESCRIPTION      : This routine sends the bulk request message from standby
 *                    to active node to initiate bulk update process.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/

PUBLIC VOID
DhCRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_DHCPC_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (DHCPC_RM_NODE_STATUS () != RM_STANDBY)
    {
        /* Node not in Standby State, so dont send 
         * Bulk request message
         */
        return;
    }

    /*
     *    DHCP Client Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |      1    |   3        |
     *    |-------------------------
     *
     *
     */
    pMsg = DhCRedGetMsgBuffer (DHCPC_RED_BULK_REQ_MSG_SIZE);

    if (pMsg == NULL)
    {
        return;
    }

    u2OffSet = 0;

    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPC_RED_BULK_REQ_MESSAGE);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, DHCPC_RED_BULK_REQ_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure 
     * releases the RM buffer memory
     */
    if (DhCRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhcpCRedSendPgDynamicBulkMsg: Ack to RM -"
                       " Send to RM failed!!!!\r\n");
        }

        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendMsgToRm 
 *
 * DESCRIPTION      : This routine enqueues the Message to RM. If the Sending
 *                    fails, frees the memory.
 *
 * INPUT            : pMsg - RM Message Data Buffer
 *                    u2Length - Length of the message 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
DhCRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_DHCPC_APP_ID, RM_DHCPC_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendDynamicClientInfo
 *
 * DESCRIPTION      : Function sends the dynamic sync up message(IP address,
 *                    Interface Index, start of Lease time and Lease timer) to
 *                    Standby node from the Active node, when the DHCP server
 *                    offers an IP address to the DHCP client. 
 *                    The following action are performed in this function.
 *                    1. No need to send dynamic sync up if the node is in
 *                       Standby or the peer node count is zero.
 *                    2. Form the dynamic sync up message for IP address,
 *                       Lease time. Start of Lease time and Interface Index and
 *                       send to standby node.
 *                    3. Send the sync message with message type as
 *                       DHCPC_RED_CLIENT_INFO
 *
 *
 * INPUT            : pDhcpCNode- DHCP Interface Record to be synced up
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedSendDynamicClientInfo (tDhcpCRec * pDhcpCNode)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    tDhcpCIfTable      *pDhcIfInfo = NULL;
    tClientConf        *pDhcpClientConfInfo = NULL;
    tBestOffer         *pBestOffer = NULL;
    tDhcpPktInfo       *pPktInfo = NULL;
    tDhcpMsgHdr        *pDhcpMsg = NULL;
    tClientId          *pClientId = NULL;
    UINT1              *pDefaultOptions = NULL;
    UINT4               u4CurrentTime = 0;
    UINT4               u4ExpiredTime = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    if (pDhcpCNode == NULL)
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedSendDynamicClientInfo: No Client record available\r\n");
        return;
    }

    if ((gDhcpCRedGlobalInfo.u1NumPeersUp == 0))
    {
        DHCPC_TRC (FAIL_TRC,
                   "DhCRedSendDynamicClientInfo: No Standby node available\r\n");
        return;
    }

    if (pMsg == NULL)
    {
        pMsg = DhCRedGetMsgBuffer (DHCPC_RED_DYN_CLIENT_REC_SIZE);

        if (pMsg == NULL)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedSendDynamicClientInfo: Tx Memory Allocation Failed \r\n");
            return;
        }
    }

    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPC_RED_CLIENT_INFO);

    /* Initially Set the Length of RM msg to Maximum */

    u2LenOffSet = u2OffSet;
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, DHCPC_RED_DYN_CLIENT_REC_SIZE);

    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpCNode->u4IpIfIndex);

    /* DHCP CLIENT INTERFACE INFO BEGIN */

    pDhcIfInfo = &(pDhcpCNode->DhcIfInfo);

    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u4Xid);

    /* Time sync between standby and active */

    DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

    u4ExpiredTime = u4CurrentTime - pDhcIfInfo->u4Secs;

    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4ExpiredTime);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u4IpAddressOfferred);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u4LeaseTimeOfferred);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u4ServerOfferred);

    /* CLIENT CONF INFO BEGIN */

    pDhcpClientConfInfo = &((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo);

    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                         pDhcpClientConfInfo->u4StartOfLeaseTime);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pDhcpClientConfInfo->u2LenOfOptions);
    pDefaultOptions = pDhcpClientConfInfo->pDefaultOptions;
    if (pDefaultOptions == NULL)
    {
        RM_FREE (pMsg);
        return;
    }
    DHCPC_RM_PUT_N_BYTE (pMsg, pDefaultOptions, &u2OffSet,
                         pDhcpClientConfInfo->u2LenOfOptions);

    /* CLIENT CONF INFO END */

    /* BEST OFFER INFO BEGIN */

    pBestOffer = pDhcIfInfo->pBestOffer;

    if (pBestOffer == NULL)
    {
        RM_FREE (pMsg);
        return;
    }
    pPktInfo = &(pBestOffer->PktInfo);
    pDhcpMsg = &(pPktInfo->DhcpMsg);

    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->Op);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->htype);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->hlen);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpMsg->hops);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->xid);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pDhcpMsg->secs);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pDhcpMsg->flags);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->ciaddr);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->yiaddr);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->siaddr);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpMsg->giaddr);
    DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->chaddr, &u2OffSet, 16);
    DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->sname, &u2OffSet, 64);
    DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->file, &u2OffSet, 128);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pPktInfo->u4IfIndex);
    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pPktInfo->u4DstIpAddr);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pPktInfo->u2Len);
    if (pDhcpMsg->pOptions == NULL)
    {
        RM_FREE (pMsg);
        return;
    }
    DHCPC_RM_PUT_N_BYTE (pMsg, pDhcpMsg->pOptions, &u2OffSet, pPktInfo->u2Len);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pPktInfo->u2BufLen);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pPktInfo->u2MaxMessageSize);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pPktInfo->u1OverLoad);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pPktInfo->u1Type);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pBestOffer->u1NoOfReqOptSupported);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pBestOffer->u1TotalNoOfOptionPresent);

    /* BEST OFFER INFO END */

    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcIfInfo->u1State);

    /* DHCP CLIENT INTERFACE INFO END */

    /* CLIENT ID INFO BEGIN */

    pClientId = &(pDhcpCNode->ClientId);

    DHCPC_RM_PUT_N_BYTE (pMsg, pClientId->au1ChAddr, &u2OffSet,
                         DHCP_MAX_CID_LEN);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pClientId->u1HwType);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pClientId->u1Len);

    /* CLIENT ID INFO END */

    DHCPC_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpCNode->u4ProcessedAck);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpCNode->u1ReTxCount);
    DHCPC_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpCNode->u1DhcpCInformMsgSent);
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2OffSet, pDhcpCNode->u2RenewTimerCount);

    /* UPDATE THE ACTUAL LENGTH */
    DHCPC_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);

    if (DhCRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedSendClientInfoBulk:"
                       "Ack to RM - Send to RM failed!!!!\r\n");
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedProcessDynamicClientInfo
 *
 * DESCRIPTION      : This function decodes the sync-up message from Active 
 *                       node and updates the Dhcp client information at the 
 *                       Standby node.
 *
 *                    The following action are performed in this function.
 *                    1. Validate the RM message Length and message.
 *                    2. Update the Client information in Standby node by 
 *                       parsing the RmMessage.
 *
 * INPUT             : pMsg - RM Sync-up message
 *                       u2RemMsgLen - Length of value of Message
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedProcessDynamicClientInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    tDhcpCIfTable      *pDhcIfInfo = NULL;
    tClientConf        *pDhcpClientConfInfo = NULL;
    tBestOffer         *pBestOffer = NULL;
    tDhcpPktInfo       *pPktInfo = NULL;
    tDhcpMsgHdr        *pDhcpMsg = NULL;
    tClientId          *pClientId = NULL;
    UINT1              *pDefaultOptions = NULL;
    UINT4               u4CurrentTime = 0;
    UINT4               u4ExpiredTime = 0;
    UINT4               u4Index = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered in DhCRedProcessDynamicClientInfo fn\n");

    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, u4Index);

    pDhcpCNode = DhcpCGetIntfRec (u4Index);
    if (pDhcpCNode == NULL)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "DhCRedProcessDynamicClientInfo: "
                    "Interface Record absent for intf %d \r\n", u4Index);
        return;
    }

    /* DHCP CLIENT INTERFACE INFO BEGIN */

    pDhcIfInfo = &(pDhcpCNode->DhcIfInfo);

    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcIfInfo->u4Xid);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ExpiredTime);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcIfInfo->u4IpAddressOfferred);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcIfInfo->u4LeaseTimeOfferred);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcIfInfo->u4ServerOfferred);

    /* Time sync between standby and active */

    /* Consider max TCP timeout as delta */
    u4ExpiredTime += 9;

    DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

    if (u4CurrentTime > u4ExpiredTime)
    {
        /* u4Secs refers to start of time for the lease */
        /* Adjust it with delay received from Active node */
        pDhcIfInfo->u4Secs = u4CurrentTime - u4ExpiredTime;
    }
    else
    {
        /* Worst case scenario handling */
        /* When the delay is more than current time, reduce the lease time
         * offered so the net result remains the same */

        pDhcIfInfo->u4LeaseTimeOfferred =
            pDhcIfInfo->u4LeaseTimeOfferred - u4ExpiredTime;

        /* PITFALL with this approach, when DHCP INFORM is asked for, the
         * above lease time adjustment will be lost as it will be synced with 
         * the DHCP server. The possibility is that events current time is less than
         * delay and DHCP_INFORM sent is rare */
    }

    /* CLIENT CONF INFO BEGIN */

    pDhcpClientConfInfo = &((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo);

    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                         pDhcpClientConfInfo->u4StartOfLeaseTime);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pDhcpClientConfInfo->u2LenOfOptions);
    pDefaultOptions = pDhcpClientConfInfo->pDefaultOptions;
    if (pDefaultOptions == NULL)
    {
        pDefaultOptions =
            (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                      DhcpCClientDefOptPoolId);
        if (pDefaultOptions == NULL)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedProcessDynamicClientInfo: "
                       "Memory allocation for DefaultOptions failed \r\n");
            return;
        }
        pDhcpClientConfInfo->pDefaultOptions = pDefaultOptions;
    }
    DHCPC_RM_GET_N_BYTE (pMsg, pDefaultOptions, pu2OffSet,
                         pDhcpClientConfInfo->u2LenOfOptions);

    /* CLIENT CONF INFO END */

    /* BEST OFFER INFO BEGIN */

    pBestOffer = pDhcIfInfo->pBestOffer;

    if (pBestOffer == NULL)
    {
        pBestOffer = MemAllocMemBlk (gDhcpClientMemPoolId.DhcpCBestOffPoolId);
        if (pBestOffer == NULL)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedProcessDynamicClientInfo: "
                       "Memory allocation for BestOffer failed \r\n");
            return;
        }
        pDhcIfInfo->pBestOffer = pBestOffer;
    }
    pPktInfo = &(pBestOffer->PktInfo);
    pDhcpMsg = &(pPktInfo->DhcpMsg);

    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpMsg->Op);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpMsg->htype);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpMsg->hlen);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpMsg->hops);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpMsg->xid);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pDhcpMsg->secs);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pDhcpMsg->flags);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpMsg->ciaddr);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpMsg->yiaddr);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpMsg->siaddr);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpMsg->giaddr);
    DHCPC_RM_GET_N_BYTE (pMsg, pDhcpMsg->chaddr, pu2OffSet, 16);
    DHCPC_RM_GET_N_BYTE (pMsg, pDhcpMsg->sname, pu2OffSet, 64);
    DHCPC_RM_GET_N_BYTE (pMsg, pDhcpMsg->file, pu2OffSet, 128);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pPktInfo->u4IfIndex);
    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pPktInfo->u4DstIpAddr);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pPktInfo->u2Len);
    if (pDhcpMsg->pOptions == NULL)
    {
        pDhcpMsg->pOptions = (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                                       DhcpCDhcpMsgOptPoolId);
        if (pDhcpMsg->pOptions == NULL)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DhCRedProcessDynamicClientInfo: "
                       "Memory allocation for DefaultOptions failed \r\n");
            return;
        }
    }
    DHCPC_RM_GET_N_BYTE (pMsg, pDhcpMsg->pOptions, pu2OffSet, pPktInfo->u2Len);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pPktInfo->u2BufLen);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pPktInfo->u2MaxMessageSize);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pPktInfo->u1OverLoad);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pPktInfo->u1Type);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pBestOffer->u1NoOfReqOptSupported);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pBestOffer->u1TotalNoOfOptionPresent);

    /* BEST OFFER INFO END */

    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcIfInfo->u1State);

    /* DHCP CLIENT INTERFACE INFO END */

    /* CLIENT ID INFO BEGIN */

    pClientId = &(pDhcpCNode->ClientId);

    DHCPC_RM_GET_N_BYTE (pMsg, pClientId->au1ChAddr, pu2OffSet,
                         DHCP_MAX_CID_LEN);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pClientId->u1HwType);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pClientId->u1Len);

    /* CLIENT ID INFO END */

    DHCPC_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpCNode->u4ProcessedAck);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpCNode->u1ReTxCount);
    DHCPC_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpCNode->u1DhcpCInformMsgSent);
    DHCPC_RM_GET_2_BYTE (pMsg, pu2OffSet, pDhcpCNode->u2RenewTimerCount);

    /* Update the CFA about the IP */

    if (pDhcIfInfo->u1State == S_BOUND)
    {
        /* Write the availabe lease info into config
         * file */
        /* Configure the Interface with the
         * IP Address Leased */
        if ((DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                           pDhcIfInfo->u4IpAddressOfferred)) ==
            DHCP_SUCCESS)
        {
            DHCPC_TRC1 (DEBUG_TRC,
                        "Configured the IP ADDRESS Leased in %x intf\n",
                        pDhcpCNode->u4IpIfIndex);
        }
        else
        {
            DHCPC_TRC1 (FAIL_TRC,
                        "FAILED in Configuring the IP ADDRESS in %x intf\n ",
                        pDhcpCNode->u4IpIfIndex);
        }
    }
    else if (pDhcIfInfo->u1State == S_INIT)
    {
        if ((DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                           DHCPC_AUTO_CONFIG_IP_ADDRESS)) ==
            DHCP_SUCCESS)
        {
            DHCPC_TRC1 (DEBUG_TRC,
                        "Configured DHCPC_AUTO_CONFIG_IP_ADDRESS in %x intf\n",
                        pDhcpCNode->u4IpIfIndex);
        }
        else
        {
            DHCPC_TRC1 (FAIL_TRC,
                        "FAILED in Configuring DHCPC_AUTO_CONFIG_IP_ADDRESS in %x intf\n ",
                        pDhcpCNode->u4IpIfIndex);
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhCRedProcessDynamicClientInfo fn\n");
    return;
}

#endif /* _DHCRED_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file dhcred.c                        */
/*-----------------------------------------------------------------------*/
