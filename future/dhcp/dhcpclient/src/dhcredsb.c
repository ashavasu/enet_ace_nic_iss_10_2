/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcredsb.c,v 1.2 2011/12/05 14:39:46 siva Exp $
 *
 * Description: This file contains DHCP Redundancy support routines
 *              as stubs when the L2RED is diabled
 *********************************************************************/
#ifndef _DHCREDSB_C_
#define _DHCREDSB_C_

#include "dhccmn.h"

/*****************************************************************************
 * FUNCTION NAME      : DhCRedInitGlobalInfo
 *
 * DESCRIPTION        : This function initializes global variable used to
 *                      store DHCP Client Redundancy information
 *
 * NOTE               : API is stubbed out as L2RED is disabled.
 *
 * INPUT              : None
 *
 * OUTPUT             : None
 *
 * RETURNS            : None
 *
 *****************************************************************************/
PUBLIC INT4
DhCRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : DhCRedDeInitGlobalInfo
 *
 * DESCRIPTION        : This function deregisters itself from RM.
 *
 * NOTE               : API is stubbed out as L2RED is disabled.
 *
 * INPUT              : None
 *
 * OUTPUT             : None
 *
 * RETURNS            : None
 *
 *****************************************************************************/
PUBLIC VOID
DhCRedDeInitGlobalInfo (VOID)
{
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from
 *                    RM module.
 *
 * NOTE             : API is stubbed out as L2RED is disabled.
 *
 * INPUT            : pMsg - pointer to DHCP Queue message.
 *
 * OUTPUT           : None.
 *
 * RETURNS          : None.
 *
 **************************************************************************/
PUBLIC VOID
DhCRedHandleRmEvents (tDhcpQMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DhCRedSendDynamicClientInfo
 *
 * DESCRIPTION      : Function sends the dynamic sync up message(IP address,
 *                    Interface Index, start of Lease time and Lease timer) to
 *                    Standby node from the Active node, when the DHCP server
 *                    offers an IP address to the DHCP client. 
 *                    The following action are performed in this function.
 *                    1. No need to send dynamic sync up if the node is in
 *                       Standby or the peer node count is zero.
 *                    2. Form the dynamic sync up message for IP address,
 *                       Lease time. Start of Lease time and Interface Index and
 *                       send to standby node.
 *                    3. Send the sync message with message type as
 *                       DHCPC_RED_CLIENT_INFO
 *
 * NOTE             : API is stubbed out as L2RED is disabled.
 *
 * INPUT            : pDhcpCNode- DHCP Interface Record to be synced up
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
DhCRedSendDynamicClientInfo (tDhcpCRec * pDhcpCNode)
{
    UNUSED_PARAM (pDhcpCNode);
    return;
}
#endif /* _DHCREDSB_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file dhcredsb.c                      */
/*-----------------------------------------------------------------------*/
