/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                          * $Id: dhcinput.c,v 1.30 2017/01/25 13:19:00 siva Exp $
 * FILE NAME      : dhcinput.c                                          *
 * LANGUAGE       : C Language                                          *
 * TARGET         : LINUX                                               *
 * AUTHOR         : A.S.Musthafa                                        *
 * DESCRIPTION    : Contains INPUT module related functions present in  *
 *                  DHCP CLIENT                                         *
 ************************************************************************/

/************************************************************************
 * Include files                             *    
 ************************************************************************/
#include "dhccmn.h"

/************************************************************************/
/*  Function Name   : DhcpCProcessPkt                                   */
/*  Description     : The function validates the header and processes   */
/*                    message based on the message type                 */
/*  Input(s)        : pRcvdBuf - DHCP packet received                   */
/*                    u1IfIndex - Interface Index                       */
/*                    u2Len - Length of the received packet             */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCProcessPkt (UINT1 *pRcvdBuf, UINT4 u4IfIndex, UINT4 u4DestIp, UINT2 u2Len)
{
    tDhcpPktInfo        PktInfo;
    tDhcpPktInfo       *pPktInfo = &PktInfo;
    UINT1               u1RetVal;
    tDhcpCRec          *pDhcpCNode = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCProcessPkt fn\n");

    pDhcpCNode = DhcpCGetIntfRec (u4IfIndex);

    if (pDhcpCNode == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "DHCP Client is not enabled on Interface\n");
        return;
    }

    pPktInfo->u4IfIndex = u4IfIndex;
    pPktInfo->u4DstIpAddr = u4DestIp;

    /* Perform a minimal header validation  */
    if (DhcpCValidatePkt (pRcvdBuf, u2Len) == DHCP_FAILURE)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "Minimal header validation FAILURE,DhcpCValidatePkt fn for %x intf\n",
                    pPktInfo->u4IfIndex);

        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;

        return;
    }

    /* Get the packet Information and the options present in the packet */
    DhcpCGetPktInfo (pPktInfo, pRcvdBuf, u2Len);

    /* Checking of BOOTP reply length */
    if (pPktInfo->u1Type == BOOTP_PKT)
    {
        if (u2Len < BOOTP_MIN_LEN)
        {
            DHCPC_TRC2 (FAIL_TRC,
                        "BOOTP Packet length is less than minimal length : "
                        "Minimum Pkt len= %d, Arrived Pkt len= %d\n",
                        BOOTP_MIN_LEN, u2Len);

            return;
        }
    }

    /* Validate the packet based on message type */
    u1RetVal = DhcpCValidateBootReply (pPktInfo, pDhcpCNode);
    if (u1RetVal == DHCP_FAILURE_IN_XID)
    {
       DHCPC_TRC1 (DEBUG_TRC, "BOOT REPLY Validation for XID FAILED"
                   " for %x Intf\n",u4IfIndex);
       (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInXid++;
        return;
    }

    if ((u1RetVal == DHCP_FAILURE_IN_OPTIONS) ||
		(u1RetVal == DHCP_FAILURE))
    {
        DHCPC_TRC1 (FAIL_TRC, "BOOT REPLY Validation FAILED for %x Intf\n",
                    u4IfIndex);

        if (u1RetVal == DHCP_FAILURE_IN_OPTIONS)
	{
            /* Increment the error count based on the packet rcvd */
            (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInOptions++;
	}
        return;
    }

    switch (pPktInfo->u1Type)
    {
        case DHCP_OFFER:
            ProcessDhcpCOffer (pPktInfo, pDhcpCNode);
            break;
        case DHCP_ACK:
            ProcessDhcpCAck (pPktInfo, pDhcpCNode);
            break;
        case DHCP_NACK:
            ProcessDhcpCNAck (pPktInfo, pDhcpCNode);
            break;
        case BOOTP_PKT:
            BootpProcessReply (pPktInfo, pDhcpCNode);
            break;
        default:
            DHCPC_TRC2 (DEBUG_TRC, "Unknown packet rcvd in %d state %d\n",
                        u4IfIndex, (pDhcpCNode->DhcIfInfo).u1State);

            break;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCProcessPkt fn\n");

    return;
}                                /* End of DhcpCProcessPkt function */

/************************************************************************/
/*  Function Name   : DhcpCValidateBootReply                            */
/*  Description     : Transcation Id and the options present in the rcvd*/
/*                    packet is checked                                 */
/*  Input(s)        : pPktInfo - Received Packet                        */
/*                    pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCValidateBootReply (tDhcpPktInfo * pPktInfo, tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal = DHCP_SUCCESS;
    UINT4               u4Temp = 0;
    UINT1               u1State;
    UINT1               u1AllocProto;
    UINT4               u4CfaIfIndex;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCValidateBootReply fn\n");

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    /* Verification is according to RFC 2131 section 4.3 Table 3. */
    /* Check the last transmitted transaction id is similar to the 
       one received in the packet */
    DHCPC_TRC1 (DEBUG_TRC, "gDhcpCIfTable's Xid is : %d\n",
                (pDhcpCNode->DhcIfInfo).u4Xid);

    DHCPC_TRC1 (DEBUG_TRC, "Packet Info's Xid is : %d\n",
                pPktInfo->DhcpMsg.xid);

    if ((pPktInfo->DhcpMsg.xid & 0xffffffff) !=
        ((pDhcpCNode->DhcIfInfo).u4Xid & 0xffffffff))
    {

        DHCPC_TRC2 (DEBUG_TRC,
                    "Transaction dissimilar for %x interface in %d State\n",
                    pPktInfo->u4IfIndex, u1State);

        return DHCP_FAILURE_IN_XID;
    }
    if (NetIpv4GetCfaIfIndexFromPort (pPktInfo->u4IfIndex, &u4CfaIfIndex)
        != NETIPV4_SUCCESS)
    {
         DHCPC_TRC2 (FAIL_TRC,
                        "IfIndex not present for %x intf in %d State\n",
                        pPktInfo->u4IfIndex, u1State);
        return DHCP_FAILURE;
    }

    if (CfaIfGetIpAllocProto (u4CfaIfIndex, &u1AllocProto) == CFA_FAILURE)
    {
         DHCPC_TRC2 (FAIL_TRC,
                        "IpAllocProto not present for %x intf in %d State\n",
                        pPktInfo->u4IfIndex, u1State);
        return DHCP_FAILURE;
    }
    if (u1AllocProto == CFA_PROTO_DHCP)
    {
        /* In the DHCP_OFFER or DHCP_ACK or DHCP_NACK message, the 
           Server Identifier option must be present */

        if (DhcpCGetOption (DHCP_OPT_SERVER_ID, pPktInfo) == DHCP_NOT_FOUND)
        {
            DHCPC_TRC2 (FAIL_TRC,
                        "Server Address option not present for %x intf in %d State\n",
                        pPktInfo->u4IfIndex, u1State);

            return DHCP_FAILURE_IN_OPTIONS;
        }

        /* In the DHCP_OFFER or DHCP_ACK or DHCP_NACK message the 
           requested IP Address option must not be present */

        if ((DhcpCGetOption (DHCP_OPT_REQUESTED_IP, pPktInfo) == DHCP_FOUND) ||
            (DhcpCGetOption (DHCP_OPT_PARAMETER_LIST, pPktInfo) == DHCP_FOUND)
            || (DhcpCGetOption (DHCP_OPT_MAX_MESSAGE_SIZE, pPktInfo) ==
                DHCP_FOUND))
        {
            DHCPC_TRC2 (FAIL_TRC,
                        "Option not allowed present for %x intf in %d State\n",
                        pPktInfo->u4IfIndex, u1State);

            return DHCP_FAILURE_IN_OPTIONS;
        }
    }
    switch (pPktInfo->u1Type)
    {
 /*****************************************************************************/
            /*  <L2L3Switch_P01>:<31/05/2002>                                             */
            /*  if((DhcpCGetOption (DHCP_OPT_LEASE_TIME,pPktInfo) == DHCP_NOT_FOUND) ||  */
            /*         (DhcpCGetOption (DHCP_OPT_CLIENT_ID,pPktInfo) == DHCP_FOUND))     */
            /*  This Check has been replaced as below since DHCP_OPT_CLIENT_ID is        */
            /*  optional.                                                                */
            /*  In Some third-party servers(Simple DNS) the check fails and where as     */
            /*  with FutureDHCP Server implementation it works fine.                     */
 /*****************************************************************************/
        case DHCP_OFFER:
            if (DhcpCGetOption (DHCP_OPT_LEASE_TIME, pPktInfo) ==
                DHCP_NOT_FOUND)
            {
                DHCPC_TRC2 (FAIL_TRC,
                            "Exiting DhcpCValidateBootReply fn. Invalid Option rcvd in DHCP_OFFER for %x interface in %d State\n",
                            pPktInfo->u4IfIndex, u1State);

                u1RetVal = DHCP_FAILURE_IN_OPTIONS;
            }

            break;
        case DHCP_NACK:
            if ((DhcpCGetOption (DHCP_OPT_LEASE_TIME, pPktInfo) == DHCP_FOUND)
                || (DhcpCGetOption (DHCP_OPT_TFTP_SNAME, pPktInfo) ==
                    DHCP_FOUND)
                || (DhcpCGetOption (DHCP_OPT_BOOT_FILE_NAME, pPktInfo) ==
                    DHCP_FOUND))
            {
                DHCPC_TRC2 (FAIL_TRC,
                            "Option not allowed present for %x intf in %d State\n",
                            pPktInfo->u4IfIndex, u1State);

                u1RetVal = DHCP_FAILURE_IN_OPTIONS;
            }
            break;

        case DHCP_ACK:
            if (pDhcpCNode->u1DhcpCInformMsgSent == DHCPC_INFORM_MESSAGE_SENT)
            {
                break;
            }

            if ((DhcpCGetOption (DHCP_OPT_LEASE_TIME, pPktInfo) ==
                 DHCP_NOT_FOUND))
            {

                DHCPC_TRC2 (FAIL_TRC,
                            "Lease time not present in DHCP_ACK in %x intf in %d state \n",
                            pPktInfo->u4IfIndex, u1State);

                u1RetVal = DHCP_FAILURE_IN_OPTIONS;
                break;
            }
            /* Now the DhcpCTag Option will have the  Lease time filled by 
               the DhcpCGetOption called above */

            MEMCPY (&u4Temp, DhcpCTag.Val, DhcpCTag.Len);
            u4Temp = OSIX_NTOHL (u4Temp);

            if (u4Temp == 0)
            {
                DHCPC_TRC2 (FAIL_TRC,
                            "Lease time invalid in DHCP_ACK msg rcvd for %x intf in %d state\n",
                            pPktInfo->u4IfIndex, u1State);

                u1RetVal = DHCP_FAILURE;
                break;
            }
 /*****************************************************************************/
            /*  <L2L3Switch_P02>:<31/05/2002>                                             */
            /*  if((DhcpCGetOption (DHCP_OPT_CLIENT_ID, pPktInfo) == DHCP_FOUND))        */
            /*           {                                                               */
            /*               DHCPC_TRC2 (FAIL_TRC,                                        */
            /*               "Exit DhcpCValidateBootReply fn Client Id present in        */
            /*                ACK msg rcv in %x intf %d state \n",                       */
            /*                pPktInfo->u4IfIndex,                                       */
            /*                DHCPC_STATE_OF_INTERFACE (pPktInfo->u4IfIndex));           */
            /*                u1RetVal = DHCP_FAILURE;                                   */
            /*           }                                                               */
            /*  This Check has been removed from here since DHCP_OPT_CLIENT_ID is        */
            /*  optional.                                                                */
            /*  In Some third-party servers(Simple DNS) the check fails and where as     */
            /*  with FutureDHCP Server implementation it works fine.                     */
 /*****************************************************************************/

            break;
        case BOOTP_PKT:
            break;
        default:
            DHCPC_TRC2 (FAIL_TRC,
                        "message should not be received in %x intf in %d state \n",
                        pPktInfo->u4IfIndex, u1State);
            u1RetVal = DHCP_FAILURE;
            break;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCValidateBootReply fn\n");

    return (u1RetVal);
}

/************************************************************************/
/*  Function Name   : DhcpCStartProcessing                              */
/*  Description     : DHCPC_START_PROCESSING event received, so start   */
/*             state processing module depending on the state           */
/*            of the interface                                          */
/*  Input(s)        : u4IfIndex                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCStartProcessing (UINT4 u4IfIndex)
{
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1State;

    pDhcpCNode = DhcpCGetIntfRec (u4IfIndex);

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCStartProcessing fn\n");

    if (pDhcpCNode == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "DHCP Client is not enabled on Interface\n");
        return;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    if ((u1State == S_UNUSED) || (u1State == S_INIT))

    {
        DhcpCInitState (pDhcpCNode);
    }
    else if (u1State == S_INITREBOOT)
    {
        /* Restart the state machine */
        if (DhcpCInitRebootState (pDhcpCNode) == DHCP_FAILURE)
        {
            DhcpCInitState (pDhcpCNode);
        }
    }
    else
    {
        DHCPC_TRC1 (DEBUG_TRC, "Unused state for intf %x\n", u4IfIndex);
    }
    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCStartProcessing fn\n");

}                                /* End of DhcpCStartProcessing function */

/************************************************************************/
/*  Function Name   : DhcpCValidatePkt                                   */
/*  Description     : Perform a minimal header validation               */
/*  Input(s)        : pDhcpPacket - received dhcp message               */
/*                    u2Len - received packet length                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

UINT4
DhcpCValidatePkt (UINT1 *pDhcpPacket, UINT2 u2Len)
{
    UINT1              *pOptions = pDhcpPacket + DHCP_LEN_FIXED_HDR;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCValidatePkt fn\n");

    if (u2Len < DHCP_LEN_FIXED_HDR)
    {
        DHCPC_TRC (FAIL_TRC, "Received msg is less than MINIMAL HEADER !\n");
        return DHCP_FAILURE;
    }

    if (MEMCMP (pOptions - 4, gau1DhcpCMagicCookie, DHCP_LEN_MAGIC))
    {
        DHCPC_TRC (FAIL_TRC, "magic cookie mismatch !\n");
        return DHCP_FAILURE;
    }

    if (pDhcpPacket[0] != BOOTREPLY)
    {
        DHCPC_TRC (FAIL_TRC, "Message OpCode is not BOOTREPLY (1) !\n");
        return DHCP_FAILURE;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCValidatePkt fn\n");

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCGetPktInfo                                    */
/*  Description     : This function copies the header and determines    */
/*                  : the packet is a DHCP packet                       */
/*  Input(s)        : pPktInfo - Local structure to be filled with      */
/*                               header information                     */
/*                    pRcvBuf -  Received DHCP packet (Linear buffer)   */
/*                    u2Len   -  Length of received packet              */
/*  Output(s)       : pPktInfo                                          */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
DhcpCGetPktInfo (tDhcpPktInfo * pPktInfo, UINT1 *pRcvdBuf, UINT2 u2Len)
{
    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCGetPktInfo fn\n");

    DhcpCCopyHeader (&pPktInfo->DhcpMsg, pRcvdBuf);

    pPktInfo->u2Len = (UINT2) (u2Len - DHCP_LEN_FIXED_HDR);

    pPktInfo->u2MaxMessageSize = DHCP_DEF_MESSAGE_SIZE;
    pPktInfo->u1OverLoad = 0;

    if (DhcpCGetOption (DHCP_OPT_MAX_MESSAGE_SIZE, pPktInfo) == DHCP_FOUND)
    {
        UINT2               u2Temp;
        MEMCPY (&u2Temp, DhcpCTag.Val, DhcpCTag.Len);
        u2Temp = (UINT2) OSIX_NTOHS (u2Temp);

        if (u2Temp > DHCP_DEF_MESSAGE_SIZE)
            pPktInfo->u2MaxMessageSize = u2Temp;
    }

    /* Decide whether it is a DHCP or BOOTP packet.
     * If the message Type option is not present it's a BOOTP packet.
     */

    if (DhcpCGetOption (DHCP_OPT_MSG_TYPE, pPktInfo) == DHCP_NOT_FOUND)
    {
        pPktInfo->u1Type = BOOTP_PKT;
    }
    else
    {
        pPktInfo->u1Type = DhcpCTag.Val[0];    /* dhcp offer,ACK or NACK */
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCGetPktInfo fn \n");

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCCopyHeader                                    */
/*  Description     : This function copies the DHCP header from the     */
/*                  : received packet                                   */
/*  Input(s)        : pHdr - Pointer to DHCP header                     */
/*                    pRcvdBuf - Received DHCP packet                   */
/*  Output(s)       : pHdr is filled with fields in DHCP header.        */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCCopyHeader (tDhcpMsgHdr * pHdr, UINT1 *pRcvdBuf)
{
    UINT2               i = 0;
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCCopyHeader fn\n");

    /*  Message op code / message type */
    pHdr->Op = pRcvdBuf[i];
    i++;

    /* Hardware address type */
    pHdr->htype = pRcvdBuf[i];
    i++;

    /*Hardware address length */
    pHdr->hlen = pRcvdBuf[i];
    i++;

    pHdr->hops = pRcvdBuf[i];
    i++;

    /*Transaction ID */
    u4Val = *(UINT4 *) (VOID *) (pRcvdBuf + i);
    pHdr->xid = u4Val;
    i += 4;

    /*seconds elapsed since client began address acquisition */
    u2Val = *(UINT2 *) (VOID *) (pRcvdBuf + i);
    pHdr->secs = (UINT2) OSIX_NTOHS (u2Val);
    i += 2;

    u2Val = *(UINT2 *) (VOID *) (pRcvdBuf + i);
    pHdr->flags = (UINT2) OSIX_NTOHS (u2Val);
    i += 2;

    /*Client IP address */
    u4Val = *(UINT4 *) (VOID *) (pRcvdBuf + i);
    pHdr->ciaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*'your' (client) IP address. */
    u4Val = *(UINT4 *) (VOID *) (pRcvdBuf + i);
    pHdr->yiaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*IP address of next server */
    u4Val = *(UINT4 *) (VOID *) (pRcvdBuf + i);
    pHdr->siaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*Relay agent IP address */
    u4Val = *(UINT4 *) (VOID *) (pRcvdBuf + i);
    pHdr->giaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*Client hardware address */
    MEMCPY (pHdr->chaddr, (pRcvdBuf + i), 16);
    i += 16;

    /*Optional server host name */
    MEMCPY (pHdr->sname, (pRcvdBuf + i), 64);
    i += 64;

    /*Boot file name */
    MEMCPY (pHdr->file, (pRcvdBuf + i), 128);
    i += 128;

    /*Optional parameters field. 
       DHCP_LEN_MAGIC is the length of the magic cookies  */

    pHdr->pOptions = pRcvdBuf + i + DHCP_LEN_MAGIC;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCCopyHeader fn\n");

}                                /* End of DhcpCCopyHeader function */

/************************************************************************/
/*  Function Name   : DhcpCStopProcessing                               */
/*  Description     : Msg type RELEASE_IP is received, stop all         */
/*                    running timers and reset the DhcpC IF table       */
/*  Input(s)        : u4Port - Port No.                                 */
/*                    u1Flag - Flag Indicates sending of RELEASE Message*/
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCStopProcessing (UINT4 u4Port, UINT1 u1Flag)
{
    tDhcpCRec          *pDhcpCNode = NULL;

    pDhcpCNode = DhcpCGetIntfRec (u4Port);

    if (pDhcpCNode == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "DHCP Client is not enabled on Interface\n");
        return;
    }
    /* Reset the retry count */
    pDhcpCNode->u1ReTxCount = 0;

    DhcpCStopTimers (pDhcpCNode);

    if (DhcpCProcessRelease (pDhcpCNode, u1Flag) == DHCP_FAILURE)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "FAILURE in sending DHCP_RELEASE for Interface %x\n",
                    u4Port);
    }

    DhcpCResetIfTblEntries (pDhcpCNode);
    DhcpCResetTxRxCounts (pDhcpCNode);
    DhcpCDeleteIntfRec (u4Port);
}

/************************************************************************/
/*  Function Name   : DhcpCStopTimers                                   */
/*  Description     : Stops all running timers                          */
/*  Input(s)        : pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCStopTimers (tDhcpCRec * pDhcpCNode)
{
    INT1                i1RetVal;
    UINT4               u4TimeLeft = 0;

    /* Stop the release wait timer if it is running */
    i1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ReleaseWaitTimer.
                                        DhcpAppTimer), &u4TimeLeft);
    if ((i1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReleaseWaitTimer.DhcpAppTimer));
        DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);
        (pDhcpCNode->DhcIfInfo).u1State = S_UNUSED;
    }

    /* Stop the reply wait timer if it is running */
    i1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ReplyWaitTimer.
                                        DhcpAppTimer), &u4TimeLeft);
    if ((i1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
    }

    /* Check whether the rebind timer is running or not */
    i1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->LeaseTimer.DhcpAppTimer),
                                      &u4TimeLeft);

    if ((i1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        /* Stop the Renewal timer */
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->LeaseTimer.DhcpAppTimer));
    }

    /* Check whether the Arp timer is running or not */
    i1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer),
                                      &u4TimeLeft);

    if ((i1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        /* Stop the Arp timer */
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer));
    }
}

/************************************************************************/
/*  Function Name   : DhcpCResetIfTblEntries                            */
/*  Description     : Resets the entries of DHCP Interface              */
/*  Input(s)        : pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCResetIfTblEntries (tDhcpCRec * pDhcpCNode)
{
    /* Reset the entries of Dhcp If table */
    DhcpCFlushOffer (pDhcpCNode);

    (pDhcpCNode->DhcIfInfo).u4Xid = DHCPC_RESET;
    (pDhcpCNode->DhcIfInfo).u4Secs = DHCPC_RESET;
    (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = DHCPC_RESET;
    (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = DHCPC_RESET;
    (pDhcpCNode->DhcIfInfo).u4ServerOfferred = DHCPC_RESET;
    (pDhcpCNode->DhcIfInfo).u1State = S_UNUSED;

    TMO_SLL_Init (&((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.DefaultOptList));

    (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u4StartOfLeaseTime = DHCPC_RESET;
    (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions = DHCPC_RESET;

    if ((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions != NULL)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCClientDefOptPoolId,
                            (UINT1 *) (pDhcpCNode->DhcIfInfo).
                            DhcpClientConfInfo.pDefaultOptions);
        (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions = NULL;
    }
}

/************************************************************************/
/*  Function Name   : DhcpCFlushOffer                                   */
/*  Description     : Flushes DHCP Offer Message on the Interface       */
/*  Input(s)        : pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCFlushOffer (tDhcpCRec * pDhcpCNode)
{
    tBestOffer         *pBestOffer;

    pBestOffer = (pDhcpCNode->DhcIfInfo).pBestOffer;

    if (pBestOffer == NULL)
    {
        /* No Offers on the Interface */
        return;
    }
    /* Freeing the current offer 
       MEM_FREE (pBestOffer->PktInfo.DhcpMsg.pOptions); */

    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pBestOffer->PktInfo.DhcpMsg.pOptions);

    pBestOffer->PktInfo.DhcpMsg.pOptions = NULL;

    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCBestOffPoolId,
                        (UINT1 *) (pBestOffer));

    (pDhcpCNode->DhcIfInfo).pBestOffer = NULL;
}

/************************************************************************/
/*  Function Name   : DhcpCResetTxRxCounts                              */
/*  Description     : Resets the TxRx counts in Interface Record        */
/*  Input(s)        : pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCResetTxRxCounts (tDhcpCRec * pDhcpCNode)
{
    /* Reset TxRx counts */
    (pDhcpCNode->TxRxStats).u4DhcpCDiscoverSent = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCRequestSent = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCReleaseSent = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCDeclineSent = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCInformSent = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfOffersRcvd = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREQ = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREQ = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInRENEW = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInRENEW = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREBIND = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREBIND = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREBOOT = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREBOOT = DHCPC_RESET;
    (pDhcpCNode->TxRxStats).u4DhcpCCounterReset = DHCPC_RESET;
}

/************************************************************************/
/*  Function Name   : DhcpCHandleIfDown                                 */
/*  Description     : Handles Interface down by stopping all runing     */
/*                    timers                                            */
/*  Input(s)        : pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCHandleIfDown (tDhcpCRec * pDhcpCNode)
{
    DhcpCStopTimers (pDhcpCNode);
    DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);

    if (((pDhcpCNode->DhcIfInfo).u1State == S_BOUND) ||
        ((pDhcpCNode->DhcIfInfo).u1State == S_REBOOTING) ||
        ((pDhcpCNode->DhcIfInfo).u1State == S_RENEWING) ||
        ((pDhcpCNode->DhcIfInfo).u1State == S_REBINDING))
    {
        (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = DHCPC_RESET;
        (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = DHCPC_RESET;
        (pDhcpCNode->DhcIfInfo).u1State = S_INITREBOOT;
    }
}

/************************************************************************/
/*  Function Name   : DhcpCHandleIfUp                                   */
/*  Description     : Handles Interface status change from DOWN to UP   */
/*                    by doing necessary action in the respective states*/
/*  Input(s)        : pDhcpCNode - DHCP Client Record                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCHandleIfUp (tDhcpCRec * pDhcpCNode)
{
    switch ((pDhcpCNode->DhcIfInfo).u1State)
    {
        case S_NULL:
        case S_UNUSED:
        case S_INIT:
        case S_REQUESTING:
        case S_SELECTING:
        {
            /* Change the state to INIT and start from
             * the beginning */
            DhcpCResetIfTblEntries (pDhcpCNode);
            (pDhcpCNode->DhcIfInfo).u1State = S_INIT;
            DhcpCStartProcessing (pDhcpCNode->u4IpIfIndex);
            break;
        }
        case S_INITREBOOT:
        {
            DhcpCStartProcessing (pDhcpCNode->u4IpIfIndex);
            break;
        }

        default:
            break;
    }
}
