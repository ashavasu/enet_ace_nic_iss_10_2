/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                 *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                 *
 * $Id: dhctimer.c,v 1.31 2016/03/26 09:50:29 siva Exp $
 * FILE NAME      : dhctimer.c                                          *
 * LANGUAGE       : C Language                                          *
 * TARGET         : LINUX                                               *
 * AUTHOR         : A.S.Musthafa                                        *
 * DESCRIPTION    : Contains timer related functions of DHCP CLIENT     *
 ************************************************************************/

#include "dhccmn.h"
#include "rtm.h"
/************************************************************************/
/*  Function Name   : DhcpCInitTimer                                    */
/*  Description     : The function allocates memory for the timer and   */
/*                    and starts the timer with requested timer ID      */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*                    u4TmrId - TimerId                                 */
/*                    u4Sec   - Time Value                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
DhcpCInitTimer (tDhcpCRec * pInterface, UINT4 u4TmrId, UINT4 u4Sec)
{
    INT4                i4RetVal = DHCP_FAILURE;

    if (pInterface == NULL)
    {
        return i4RetVal;
    }

    switch (u4TmrId)
    {
        case DHCPC_NULL_STATE_TIMER_ID:
        case BOOTP_REQUEST_TIMER_ID:
        case DHCPC_MULTIPLE_OFFER_TIMER_ID:
        case DHCPC_SINGLE_OFFER_TIMER_ID:
        case DHCPC_REQUEST_TIMER_ID:
        case DHCPC_INFORM_MSG_TIMER_ID:
            (pInterface->ReplyWaitTimer).u4IfIndex = pInterface->u4IpIfIndex;
            (pInterface->ReplyWaitTimer).u4TimerId = u4TmrId;
            ((pInterface->ReplyWaitTimer).pEntry) = (VOID *) pInterface;
            if (DhcpCStartTimer (DhcpCTimerListId,
                                 &(pInterface->ReplyWaitTimer.DhcpAppTimer),
                                 u4Sec) == DHCP_FAILURE)
            {
                ((pInterface->ReplyWaitTimer).pEntry) = (VOID *) 0;
            }
            else
            {
                i4RetVal = DHCP_SUCCESS;
            }
            break;

        case DHCPC_REBIND_TIMER_ID:
        case DHCPC_RENEWAL_TIMER_ID:
            (pInterface->LeaseTimer).u4IfIndex = pInterface->u4IpIfIndex;
            (pInterface->LeaseTimer).u4TimerId = u4TmrId;
            ((pInterface->LeaseTimer).pEntry) = (VOID *) pInterface;
            if (DhcpCStartTimer (DhcpCTimerListId,
                                 &(pInterface->LeaseTimer.DhcpAppTimer),
                                 u4Sec) == DHCP_FAILURE)
            {
                ((pInterface->LeaseTimer).pEntry) = (VOID *) 0;
            }
            else
            {
                i4RetVal = DHCP_SUCCESS;
            }
            break;

        default:
            break;
    }

    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : SingleOfferTimerExpiry                            */
/*  Description     : The function handles the single Offer Timer Expiry*/
/*                    in which retransmission of DHCP_DISCOVER message  */
/*                    is done. If retransmission exceeds 4 times start  */
/*                    a timer with 3 min. and go to S_NULL state        */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
SingleOfferTimerExpiry (tDhcpCRec * pDhcpCNode)
{
    UINT4               u4CfaIfIndex = 0;
    UINT1               i1RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered SingleOfferTimerExpiry fn\n");

    if (pDhcpCNode->u1ReTxCount >= (UINT1) DHCP_MAX_RETRY)
    {
        if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                          &u4CfaIfIndex) != NETIPV4_SUCCESS)
        {
            DHCPC_TRC2 (FAIL_TRC,
                        "Couldn't get Cfa Port %d using IpIfIndex %d in "
                        "SingleOfferTimerExpiry\r\n",
                        u4CfaIfIndex, pDhcpCNode->u4IpIfIndex);
            return;
        }
        if (u4CfaIfIndex == CFA_DEFAULT_ROUTER_VLAN_IFINDEX)

        {
            if ((CfaHandleDhcpFallback (u4CfaIfIndex)) == CFA_FAILURE)
            {
                DHCPC_TRC (FAIL_TRC,
                           "Handling DHCP-FALLBACK FAILED in "
                           "SingleOfferTimerExpiry\n");
                return;
            }
            DhcpCProcessOperNotification (pDhcpCNode->u4IpIfIndex);
        }
        else
        {
            pDhcpCNode->u1ReTxCount = DHCPC_RESET;

            /* Transfer the state to S_NULL. In this state DHCP_DISCOVER 
               message is transmitted for every 3 Min. until a DHCP_OFFER is
               recieved from any one of the servers */
            if (DhcpCInitTimer (pDhcpCNode, DHCPC_NULL_STATE_TIMER_ID,
                                gDhcpCFastAcc.u4FastAccNullStTimeOut) ==
                DHCP_FAILURE)
            {
                DHCPC_TRC (FAIL_TRC,
                           "Starting DHCPC_NULL_STATE_TIMER_ID failed \r\n");
                DhcpCInitState (pDhcpCNode);
            }
            (pDhcpCNode->DhcIfInfo).u1State = S_NULL;
        }
    }
    else
    {
        /* Retransmit the DHCP_DISCOVER message again to the Server */

        if ((i1RetVal = DhcpCSendDiscover (pDhcpCNode)) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "DISCOVER Send FAILURE in SingleOfferTimerExpiry\n");
            return;
        }

        (pDhcpCNode->u1ReTxCount)++;    /* Increment the ReTx Count */

    }

    DHCPC_TRC (DEBUG_TRC, "Exiting SingleOfferTimerExpiry fn\n");

}

/************************************************************************/
/*  Function Name   : MultipleOfferTimerExpiry                          */
/*  Description     : The function handles the Multiple Offer Timer.    */
/*                    Within this timer which the state of the          */
/*                    interface remains in the S_SELECTING state and    */
/*                    keeps collecting the offer                        */
/*                    from the server which sends the DHCP_OFFER        */
/*                    message. In this expiry function the state is     */
/*                    transitioned to S_REQUESTING state after          */
/*                    transmitting DHCP_REQUEST message                 */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
MultipleOfferTimerExpiry (tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered MultipleOfferTimerExpiry fn\n");

    if ((pDhcpCNode->DhcIfInfo).u1State != S_SELECTING)
    {
        DHCPC_TRC2 (DEBUG_TRC,
                    "MultipleOffer Timer Expired in wrong state %d for %x interface\n",
                    pDhcpCNode->u4IpIfIndex, (pDhcpCNode->DhcIfInfo).u1State);
        return;
    }
    /* Send a DHCP_REQUEST */
    u1RetVal = DhcpCSendRequest (pDhcpCNode);

    if (u1RetVal == DHCP_FAILURE)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "DHCP_REQUEST send FAILURE in MultpleOfferTimerExpiry fn for %x intf\n",
                    pDhcpCNode->u4IpIfIndex);
        DhcpCInitState (pDhcpCNode);
        return;
    }

    /* Transit the state to S_REQUESTING */
    (pDhcpCNode->DhcIfInfo).u1State = S_REQUESTING;

    DHCPC_TRC (DEBUG_TRC, "Exiting MultipleOfferTimerExpiry fn\n");
}                                /* End Of MultipleOfferTimerExpiry function */

/************************************************************************/
/*  Function Name   : ProcessRequestTimerExpiry                         */
/*  Description     : This function handles DHCP_REQUEST timer expiry   */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ProcessRequestTimerExpiry (tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal;
    UINT4               u4TimeLeft = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered ProcessRequestTimerExpiry fn\n");

    if (pDhcpCNode->u1ReTxCount >= (UINT1) DHCP_MAX_RETRY)
    {
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;

        /* Here when the state is in S_RENEWING state, no action is taken since
         * rebind timer expiry will take care */

        if ((pDhcpCNode->DhcIfInfo).u1State != S_RENEWING)
        {
            /* Check whether the any lease timer is running or not */
            u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                              &(pDhcpCNode->LeaseTimer.
                                                DhcpAppTimer), &u4TimeLeft);

            if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
                DhcpCStopTimer (DhcpCTimerListId,
                                &(pDhcpCNode->LeaseTimer.DhcpAppTimer));
            DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                          DHCPC_AUTO_CONFIG_IP_ADDRESS);
            (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = 0;
            (pDhcpCNode->DhcIfInfo).u1State = S_INIT;

            DhcpCStartProcessing (pDhcpCNode->u4IpIfIndex);

            DHCPC_TRC (DEBUG_TRC, "Sent DHCP_CLIENT_START_PROCESSING Event\n");
        }
    }
    else
    {
        u1RetVal = DhcpCSendRequest (pDhcpCNode);

        if (u1RetVal == DHCP_FAILURE)
        {
            DHCPC_TRC1 (FAIL_TRC,
                        "Sending DHCP_REQUEST FAILURE in ProcessRequestTimerExpiry fn for %x intf\n",
                        pDhcpCNode->u4IpIfIndex);
            return;
        }

        (pDhcpCNode->u1ReTxCount)++;    /* Increment the ReTx Count */

        /* Remain in the same until any response message comes from the 
         * server */
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting ProcessRequestTimerExpiry fn\n");

    return;
}                                /* End Of ProcessRequestTimerExpiry function */

/************************************************************************/
/*  Function Name   : ProcessRenewalTimerExpiry                */
/*  Description     : This function handles the renewal timer expiry,   */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ProcessRenewalTimerExpiry (tDhcpCRec * pDhcpCNode)
{
    DHCPC_TRC (DEBUG_TRC, "Entered ProcessRenewalTimerExpiry fn\n");

    if (pDhcpCNode->u2RenewTimerCount > 0)
    {
        pDhcpCNode->u2RenewTimerCount =
            (UINT2) (pDhcpCNode->u2RenewTimerCount - 1);
        /* Start a timer with available renew time */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_RENEWAL_TIMER_ID,
                            DHCPC_MAX_TIME_OUT) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting RENEWAL_TIMER failed \r\n");
        }
        return;
    }
    DhcpCRenewIp (pDhcpCNode);

    DHCPC_TRC (DEBUG_TRC, "Exiting ProcessRenewalTimerExpiry fn\n");

    return;
}                                /* End Of ProcessRenewalTimerExpiry function */

/************************************************************************/
/*  Function Name   : ProcessRebindTimerExpiry                          */
/*  Description     : This function handles the rebind timer expiry.    */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ProcessRebindTimerExpiry (tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal = DHCP_FAILURE;
    UINT4               u4TimeLeft = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered ProcessRebindTimerExpiry fn\n");

    if (pDhcpCNode->u2RenewTimerCount > 0)
    {
        pDhcpCNode->u2RenewTimerCount =
            (UINT2) (pDhcpCNode->u2RenewTimerCount - 1);
        /* Start a timer with available renew time */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_REBIND_TIMER_ID,
                            DHCPC_MAX_TIME_OUT) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting REBIND_TIMER failed \r\n");
        }
        return;
    }

    u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ReplyWaitTimer.
                                        DhcpAppTimer), &u4TimeLeft);

    /* If reply wait timer is running stop it */
    if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
        /* Reset the retry count */
        pDhcpCNode->u1ReTxCount = 0;
    }

    (pDhcpCNode->DhcIfInfo).u1State = S_REBINDING;

    /* State is assigned before sending the DHCP_REQUEST, since sending
     * DHCP_REQUEST is handled in S_REBINDING state in DhcpCSendRequest fn */

    DhcpCSendRequest (pDhcpCNode);

    DHCPC_TRC (DEBUG_TRC, "Exiting ProcessRebindTimerExpiry fn\n");

}                                /* End Of ProcessRebindTimerExpiry function */

/************************************************************************/
/*  Function Name   : DhcpCProcessTmrExpiry                             */
/*  Description     : Process Timer Expiry, Depending on the timer id,  */
/*                    calls appropriate functions                       */
/*  Input           : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCProcessTmrExpiry (VOID)
{
    tDhcpCTimer        *pDhcpCTimer;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1State;

    DHCPC_TRC (EVENT_TRC, "Entering DhcpCProcessTmrExpiry fn\n");

    while ((pDhcpCTimer =
            (tDhcpCTimer *) TmrGetNextExpiredTimer (DhcpCTimerListId)) != NULL)
    {
        /* Get the Dhcp client module defined structure pointer
           which is passed while starting the timer and has the timer id 
           and the interface index  */

        pDhcpCNode = (tDhcpCRec *) pDhcpCTimer->pEntry;

        if (pDhcpCNode == NULL)
        {
            DHCPC_TRC (DEBUG_TRC, "DHCP Client is not enabled on Interface\n");
            return;
        }
        u1State = (pDhcpCNode->DhcIfInfo).u1State;

        pDhcpCTimer->pEntry = 0;

        switch (pDhcpCTimer->u4TimerId)
        {
            case DHCPC_SINGLE_OFFER_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Single offer timer expired for %x intf\n",
                            pDhcpCTimer->u4IfIndex, u1State);

                SingleOfferTimerExpiry (pDhcpCNode);
                break;

            case DHCPC_MULTIPLE_OFFER_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Multiple offer timer expired for %x intf\n",
                            pDhcpCTimer->u4IfIndex, u1State);

                MultipleOfferTimerExpiry (pDhcpCNode);
                break;

            case DHCPC_REQUEST_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Request timer expired for %x intf in %d state\n",
                            pDhcpCTimer->u4IfIndex, u1State);

                ProcessRequestTimerExpiry (pDhcpCNode);
                break;

            case DHCPC_RENEWAL_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Renewal timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                ProcessRenewalTimerExpiry (pDhcpCNode);
                break;

            case DHCPC_REBIND_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Rebind timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                ProcessRebindTimerExpiry (pDhcpCNode);
                break;

            case DHCPC_ARP_CHECK_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "ARP timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                DhcpCArpTimerExpiry (pDhcpCNode,
                                     pDhcpCTimer->u4LeasedIpAddr,
                                     pDhcpCTimer->u4DstIpAddr,
                                     pDhcpCTimer->u4LeasedTime);
                break;

            case DHCPC_NULL_STATE_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "S_NULL state timer expired for %x intf in %d state\n",
                            pDhcpCTimer->u4IfIndex, u1State);

                ProcessNULLStateExpiry (pDhcpCNode);
                break;

            case DHCPC_INFORM_MSG_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Inform Msg timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                DhcpCInformTmrExpiry (pDhcpCNode);
                break;

            case DHCPC_DECLINE_MSG_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "Decline Msg timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                DhcpCDeclineTmrExpiry (pDhcpCNode,
                                       pDhcpCTimer->u4CidrAddr,
                                       pDhcpCTimer->u4CidrMask);
                break;

            case BOOTP_REQUEST_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "bootp req timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                BootpReqTmrExpiryHdlr (pDhcpCNode);
                break;

            case DHCPC_RELEASE_MSG_TIMER_ID:
                DHCPC_TRC2 (DEBUG_TRC,
                            "release timeout timer expired for %x intf in %d state \n",
                            pDhcpCTimer->u4IfIndex, u1State);

                DhcpCReleaseTimeoutExpiry (pDhcpCNode);
                break;

            default:
                DHCPC_TRC (DEBUG_TRC, "Invalid Timer ID expired\n");
                break;
        }                        /* End of switch statement */

        /* Free the allocated memory which stores the tDhcpCTimer 
           structure in the u4Data which is recieved when a timer 
           expires. This argument is filled whenever a timer is 
           started */
    }                            /* End of while statement */

    DHCPC_TRC (EVENT_TRC, "Exiting DhcpCProcessTmrExpiry fn\n");

    return;
}                                /* End of DhcpCProcessTmrExpiry function */

/************************************************************************/
/*  Function Name   : ProcessNULLStateExpiry                            */
/*  Description     : Calls SendDiscover message, start single offer    */
/*                    timer and go to S_INIT state                      */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ProcessNULLStateExpiry (tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered ProcessNULLStateExpiry fn \n");

    /* Broadcast a  DHCP_DISCOVER message */
    u1RetVal = DhcpCSendDiscover (pDhcpCNode);

    if (u1RetVal != DHCP_SUCCESS)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "DISCOVER Msg send FAILURE for %x intf in S_NULL state\n",
                    pDhcpCNode->u4IpIfIndex);
    }

    DHCPC_TRC (DEBUG_TRC, " Exiting ProcessNULLStateExpiry fn\n");

    return;
}                                /* End of ProcessNULLStateExpiry function */

/************************************************************************/
/*  Function Name   : DhcpCInformTmrExpiry                              */
/*  Description     : The function handles the Inform msg Timer Expiry  */
/*                    in which retransmission of Inform message         */
/*                    is done. If retransmission exceeds 4 times.       */
/*                    Go to S_INIT state                                */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCInformTmrExpiry (tDhcpCRec * pDhcpCNode)
{
    UINT1               i1RetVal;
    UINT1               u1RetVal;
    UINT4               u4TimeLeft = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCInformTmrExpiry fn\n");

    if (pDhcpCNode->u1ReTxCount >= (UINT1) DHCP_MAX_RETRY)
    {
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;

        DHCPC_TRC (FAIL_TRC, "INFORM msg RETX Exceeded. State to S_INIT\n");

        /* Check whether the rebind timer is running or not */
        u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->LeaseTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            /* Stop the Rebind timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        DHCPC_TRC (FAIL_TRC, "No Ack Rcvd from Server for DHCP_INFORM\n");
    }
    else
    {
        if ((i1RetVal = DhcpCSendInform (pDhcpCNode)) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC,
                       "INFORM Send FAILURE in DhcpCInformTmrExpiry \n");
            return;
        }
        (pDhcpCNode->u1ReTxCount)++;    /* Increment the ReTx Count */
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCInformTmrExpiry fn\n");

} /******* End Of DhcpCInformTmrExpiry function *******/

/************************************************************************/
/*  Function Name   : DhcpCDeclineTmrExpiry                             */
/*  Description     : This function handles DHCP_DECLINE timer expiry   */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*                  : u4CidrAddr - CIDR Address                         */
/*                  : u4CidrMask - CIDR Mask                            */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCDeclineTmrExpiry (tDhcpCRec * pDhcpCNode, UINT4 u4CidrAddr,
                       UINT4 u4CidrMask)
{
    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCDeclineTmrExpiry fn\n");

    (pDhcpCNode->DhcIfInfo).u1State = S_INIT;

    DhcpCStartProcessing (pDhcpCNode->u4IpIfIndex);

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCInformTmrExpiry fn\n");

    UNUSED_PARAM (u4CidrAddr);
    UNUSED_PARAM (u4CidrMask);

    return;
}

/************************************************************************/
/*  Function Name   : DhcpCReleaseTmrExpiry                             */
/*  Description     : This function handles DHCP_RELEASE timer expiry   */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCReleaseTmrExpiry (tDhcpCRec * pDhcpCNode)
{
    DHCPC_TRC (DEBUG_TRC, "Release Timer Expired\n");

    (pDhcpCNode->DhcIfInfo).u1State = S_INIT;

    DhcpCStartProcessing (pDhcpCNode->u4IpIfIndex);

    return;
}

/************************************************************************/
/*  Function Name   : DhcpCReleaseTimeoutExpiry                         */
/*  Description     : This function handles DHCP_RELEASE_TIMOUT         */
/*                                                       timer expiry   */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCReleaseTimeoutExpiry (tDhcpCRec * pDhcpCNode)
{
#ifdef LNXIP4_WANTED 
    t_IFACE_PARAMS IfaceParams;
#endif
    UINT4 u4IfIndex = 0;
    DHCPC_TRC (DEBUG_TRC, "Release Timeout Expired\n");

    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex, &u4IfIndex)
        != NETIPV4_SUCCESS)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in NetIpv4GetCfaIfIndexFromPort fn\n");
        return;
    }

#ifdef LNXIP4_WANTED
    MEMSET (&IfaceParams, 0 , sizeof (t_IFACE_PARAMS));
    /* Down indication is sent to RTM before configuring IP with appropriate interface index and port */
    IfaceParams.u2Port = (UINT2) pDhcpCNode->u4IpIfIndex;
    IfaceParams.u2IfIndex = (UINT2) u4IfIndex;
    /* Release status flag is set so that discover message is not sent after release for the oper state is changed */
    IfaceParams.u4ReleaseStatusFlag = DHCPC_RELEASE_STATUS_SET;
    LnxIpNotifyIfInfo (RTM_OPER_DOWN_MSG, &IfaceParams);
#endif
    /* Make interface IP Address to DHCPC_AUTO_CONFIG_IP_ADDRESS */
    DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);


#ifdef LNXIP4_WANTED
    MEMSET (&IfaceParams, 0 , sizeof (t_IFACE_PARAMS));
    /* Up indication is sent to RTM after configuring IP with appropriate interface index and port */
    IfaceParams.u2Port = (UINT2) pDhcpCNode->u4IpIfIndex;
    IfaceParams.u2IfIndex = (UINT2) u4IfIndex;
    /* Release status flag is set so that discover message is not sent after release for the oper state is changed */
    IfaceParams.u4ReleaseStatusFlag = DHCPC_RELEASE_STATUS_SET;
    LnxIpNotifyIfInfo (RTM_OPER_UP_MSG, &IfaceParams);
#endif
    
    /* Interface is updated with IP address as NULL on 
     * release timer expiry */
    CfaDhcpReleaseUpdate (u4IfIndex);
    return;
}

/************************************************************************/
/*  Function Name   : DhcpCRenewIp                                      */
/*  Description     : This function handles the renewal of IP address   */
/*  Input           : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCRenewIp (tDhcpCRec * pDhcpCNode)
{
    UINT4               u4LeaseTime;
    UINT4               u4Secs;
    UINT4               u4AvailRebindTime;
    UINT4               u4CurrentRebindTime;
    UINT4               u4CurrentTime;
    UINT4               u4RemainingTime = 0;

    /* Get the secs at which the IP Acquisition has started */
    u4Secs = (pDhcpCNode->DhcIfInfo).u4Secs;

    /* Get the active lease time offerred by the server */
    u4LeaseTime = (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred;

    /* As per RFC 2131 which says that the rebind time is 0.875 of Lease */
    u4CurrentRebindTime = (UINT4) (u4Secs + (u4LeaseTime * 0.875));

    /* Get the current time in secs */
    DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

    /* Currently available Rebind Time */
    u4AvailRebindTime = u4CurrentRebindTime - u4CurrentTime;

    if (u4AvailRebindTime > 0)
    {
        pDhcpCNode->u2RenewTimerCount =
            (UINT2) (u4AvailRebindTime / DHCPC_MAX_TIME_OUT);
        if (pDhcpCNode->u2RenewTimerCount != 0)
        {

            /*Fsap Timer supports only for 7 days, if renew time is more than 
             * 7 days, u2RenewTimerCount is used. u2RenewTimerCount is 
             * (Configured Val/ Max Fsap supported value). On timer expiry u2RenewTimerCount will
             * be decremented and the corresponding action for the timer expiry will be done
             * only when the u2RenewTimerCount becomes zero, until then the timer will 
             * be restarted.*/

            u4RemainingTime = u4AvailRebindTime % DHCPC_MAX_TIME_OUT;
            if (u4RemainingTime == 0)
            {
                u4AvailRebindTime = DHCPC_MAX_TIME_OUT;
                pDhcpCNode->u2RenewTimerCount =
                    (UINT2) (pDhcpCNode->u2RenewTimerCount - 1);
            }
            else
            {
                u4AvailRebindTime = (UINT4) u4RemainingTime;
            }
        }

        /* Start a timer with available rebind time */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_REBIND_TIMER_ID,
                            u4AvailRebindTime) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting DHCPC_REBIND_TIMER_ID failed \r\n");
            DhcpCInitState (pDhcpCNode);
            return;
        }
        /* Send a DHCP_REQUEST to the leasing server to extend the 
           lease time available */
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;

        DhcpCSendRequest (pDhcpCNode);

        (pDhcpCNode->DhcIfInfo).u1State = S_RENEWING;
    }
    else
    {
        (pDhcpCNode->DhcIfInfo).u1State = S_INIT;
        DhcpCStartProcessing (pDhcpCNode->u4IpIfIndex);
    }
}
