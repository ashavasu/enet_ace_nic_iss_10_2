/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dhcpccli.c,v 1.49 2016/06/29 11:11:56 siva Exp $
 * 
 * Description: This file will have the CLI configuration routines 
 * for DHCP client
 *******************************************************************/
#ifndef __DHCPCCLI_C__
#define __DHCPCCLI_C__

#ifdef DHCPC_WANTED
#include "lr.h"
#include "dhcdef.h"
#include "fssnmp.h"
#include "cli.h"
#include "dhcp.h"
#include "cfa.h"
#include "ip.h"
#include "fsdhclwr.h"
#include "dhcpccli.h"
#include "dhcclipt.h"
#include "fsdhclcli.h"
#include "dhccmn.h"

/**************************************************************************/
/*  Function Name   : cli_process_dhcpc_cmd                               */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_dhcpc_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[DHCPC_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT1              *pu1Inst;
    UINT4               u4IfIndex;
    INT4                i4Timer = 0;
    INT4                i4RetValDhcpClientFastAccess = 0;
    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store DHCPC_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == DHCPC_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, DhcpCProtocolLock, DhcpCProtocolUnlock);
    DHCPC_PROTO_LOCK ();

    switch (u4Command)
    {
        case CLI_DHCPCLNT_FAST_ACC:

            i4RetStatus = DhcpClientSetFastAcc (CliHandle, CLI_ENABLE);

            break;

        case CLI_DHCPCLNT_NO_FAST_ACC:

            i4RetStatus = DhcpClientSetFastAcc (CliHandle, CLI_DISABLE);

            break;

        case CLI_DHCPCLNT_IDLE_TIMER:
            if (args[0] != NULL)
            {
                MEMCPY (&i4Timer, args[0], sizeof (UINT4));
            }

            i4RetStatus = DhcpClientSetIdleTimer (CliHandle, i4Timer);

            break;

        case CLI_DHCPCLNT_NO_IDLE_TIMER:

            nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
            if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
            {
                i4RetStatus =
                    DhcpClientSetIdleTimer (CliHandle,
                                            DHCPC_FAST_ACC_NULL_ST_TIMEOUT);
            }
            else
            {
                i4RetStatus =
                    DhcpClientSetIdleTimer (CliHandle,
                                            DHCPC_NULL_STATE_WAIT_TIME_OUT);
            }

            break;

        case CLI_DHCPCLNT_DISC_TIMER:
            if (args[0] != NULL)
            {
                MEMCPY (&i4Timer, args[0], sizeof (UINT4));
            }

            i4RetStatus = DhcpClientSetDiscTimer (CliHandle, i4Timer);

            break;

        case CLI_DHCPCLNT_NO_DISC_TIMER:

            nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
            if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
            {
                i4RetStatus =
                    DhcpClientSetDiscTimer (CliHandle,
                                            DHCPC_FAST_ACC_DISCOVERY_TIME_OUT);
            }
            else
            {
                i4RetStatus =
                    DhcpClientSetDiscTimer (CliHandle,
                                            DHCPC_DISCOVER_RETX_TIME_OUT);
            }

            break;
        case CLI_DHCPCLNT_ARP_CHECK_TIMER:

            if (args[0] != NULL)
            {
                MEMCPY (&i4Timer, args[0], sizeof (UINT4));
            }

            i4RetStatus = DhcpClientSetArpCheckTimer (CliHandle, i4Timer);

            break;
        case CLI_DHCPCLNT_NO_ARP_CHECK_TIMER:

            nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
            if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
            {
                i4RetStatus =
                    DhcpClientSetArpCheckTimer (CliHandle,
                                                DHCPC_FAST_ACC_ARP_CHECK_MAX_TIMEOUT);
            }
            else
            {
                i4RetStatus =
                    DhcpClientSetArpCheckTimer (CliHandle,
                                                DHCPC_ARP_CHECK_TIME_OUT);
            }

            break;

        case CLI_DHCPCLNT_DEBUG:
            /* args[0] -  Debug Trace level to set */
            i4RetStatus =
                DhcpClientSetTrace (CliHandle,
                                    CLI_PTR_TO_I4 (args[0]), CLI_ENABLE);
            break;

        case CLI_DHCPCLNT_NO_DEBUG:
            /* args[0] -  Debug Trace level to set */
            i4RetStatus =
                DhcpClientSetTrace (CliHandle,
                                    CLI_PTR_TO_I4 (args[0]), CLI_DISABLE);
            break;

        case CLI_DHCPCLNT_RELEASE:
            /* No addition command arguments, so ignore the args[] array 
             * Uses pu1Inst which is the interface index
             */
            if (CfaValidateIfIndex (CLI_PTR_TO_U4 (pu1Inst)) == CFA_SUCCESS)
            {
                i4RetStatus = DhcpClientSetRelease (CliHandle,
                                                    CLI_PTR_TO_I4 (pu1Inst));
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n %% Invalid Interface Index specified\r\n");
            }
            break;

        case CLI_DHCPCLNT_RENEW:
            /* No addition command arguments, so ignore the args[] array 
             * Uses pu1Inst which is the interface index
             */
            if (CfaValidateIfIndex (CLI_PTR_TO_U4 (pu1Inst)) == CFA_SUCCESS)
            {
                i4RetStatus = DhcpClientSetRenew (CliHandle,
                                                  CLI_PTR_TO_I4 (pu1Inst));
            }
            else
            {
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n %% Invalid Interface Index specified\r\n");
            }
            break;
        case CLI_DHCPCLNT_SHOW_FASTACCESS:

            i4RetStatus = DhcpClientShowFastAccess (CliHandle);
            break;

        case CLI_DHCPCLNT_SHOW_STATS:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpClientShowStats (CliHandle);
            break;

        case CLI_DHCPCLNT_SHOW_ALL_OPTION:
            i4RetStatus = DhcpClientShowAllOption (CliHandle);
            break;

        case CLI_DHCPCLNT_ID:
            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            if ((u4IfIndex == (UINT4)CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
                   u4IfIndex =CFA_OOB_MGMT_IFINDEX;
            }

            i4RetStatus = DhcpClientSetConfigId (CliHandle,
                                                 u4IfIndex,
                                                 CLI_PTR_TO_U4 (args[0]),
                                                 (UINT1 *) (args[1]));
            break;

        case CLI_DHCPCLNT_NO_ID:
            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            if ((u4IfIndex == (UINT4)CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
                   u4IfIndex =CFA_OOB_MGMT_IFINDEX;
            }

            i4RetStatus = DhcpClientSetConfigId (CliHandle, u4IfIndex, 0, NULL);
            break;

        case CLI_DHCPCLNT_OPTION:
            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            if ((u4IfIndex == (UINT4)CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
                   u4IfIndex = CFA_OOB_MGMT_IFINDEX;
            }

            i4RetStatus = DhcpClientSetOption (CliHandle,
                                               CLI_PTR_TO_U4 (args[0]),
                                               u4IfIndex,
                                               CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_DHCPCLNT_SHOW_CLNTID:
            i4RetStatus = DhcpClientShowIdentifiers (CliHandle);
            break;

        case CLI_DHCP_CLNT_CLEAR_ALL_INTERFACE_STATS:
            i4RetStatus = DhcpClientClearStatsAllInterface (CliHandle);
            break;

        case CLI_DHCP_CLNT_CLEAR_INTERFACE_STATS:
            i4RetStatus =
                DhcpClientClearStatsInterface (CliHandle,
                                               CLI_PTR_TO_I4 (pu1Inst),
                                               DHCP_SET);
            break;

        case CLI_DHCPCLNT_VENDOR_SPECIFIC:
            /* args[0] - string input 
             */
            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            if ((u4IfIndex == (UINT4)CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
                   u4IfIndex =CFA_OOB_MGMT_IFINDEX;
            }

            i4RetStatus =
                DhcpClientAddVendorSpecificInfo (CliHandle, args[0], u4IfIndex);
            if (i4RetStatus == CLI_SUCCESS)
            {
                /* Add OPTION 43 to the parameter list */
                i4RetStatus = (INT4) DhcpClientSetOption (CliHandle,
                                                          CLI_ENABLE,
                                                          u4IfIndex,
                                                          DHCP_OPT_SVENDOR_SPECIFIC);
            }
            else
            {
                CliPrintf (CliHandle, "Unable to add vendor specific data \n");
            }
            break;

        case CLI_DHCPCLNT_NO_VENDOR_SPECIFIC:
            /* no input required from args 
             */
            u4IfIndex = (UINT4) CLI_GET_VLANID ();
            if ((u4IfIndex == (UINT4)CFA_INVALID_IFINDEX) && (CfaIsMgmtPortEnabled() == TRUE))
            {
                   u4IfIndex =CFA_OOB_MGMT_IFINDEX;
            }
            
            DhcpClientRemoveVendorSpecificInfo (CliHandle, u4IfIndex);
            if (i4RetStatus == CLI_SUCCESS)
            {
                /* Delete option 43 from parameter list of vlan */
                i4RetStatus = (INT4) DhcpClientSetOption (CliHandle,
                                                          CLI_DISABLE,
                                                          u4IfIndex,
                                                          DHCP_OPT_SVENDOR_SPECIFIC);
            }
            break;

        default:
            CliPrintf (CliHandle, "\r\n%% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DHCPC_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", DHCPCCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);
    DHCPC_PROTO_UNLOCK ();

    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name     : DhcpClientSetFastAcc                                  */
/*                                                                           */
/* Description       : This function sets the Dhcp client fast access        */
/*                     mode which will update the discovery timer and null   */
/*                       state timer                                              */
/*                                                                           */
/* Input Parameters  : CliHandle  - CliContext ID                            */
/*                     i4FastAccFlag  - Fast Access Mode enable or disable   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpClientSetFastAcc (tCliHandle CliHandle, INT4 i4FastAccFlag)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpClientFastAccess (&u4ErrCode, i4FastAccFlag) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientFastAccess (i4FastAccFlag) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : DhcpClientSetIdleTimer                                */
/*                                                                           */
/* Description       : This function sets or resets the null state timer     */
/*                                                                           */
/* Input Parameters  : CliHandle  - CliContext ID                            */
/*                     i4IdleTimer  - Timer Value                            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
DhcpClientSetIdleTimer (tCliHandle CliHandle, INT4 i4IdleTimer)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpClientFastAccessNullStateTimeOut
        (&u4ErrCode, i4IdleTimer) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientFastAccessNullStateTimeOut
        (i4IdleTimer) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : DhcpClientSetDiscTimer                                */
/*                                                                           */
/* Description       : This function sets or resets the discovery timer      */
/*                                                                           */
/* Input Parameters  : CliHandle  - CliContext ID                            */
/*                     i4DiscTimer - Discovery timer value                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpClientSetDiscTimer (tCliHandle CliHandle, INT4 i4DiscTimer)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpClientFastAccessDiscoverTimeOut
        (&u4ErrCode, i4DiscTimer) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientFastAccessDiscoverTimeOut (i4DiscTimer) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : DhcpClientSetArpCheckTimer                            */
/*                                                                           */
/* Description       : This function sets or resets the Arp cheche timer     */
/*                                                                           */
/* Input Parameters  : CliHandle  - CliContext ID                            */
/*                     i4ArpCheckTimer - Arp TimeOut value                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpClientSetArpCheckTimer (tCliHandle CliHandle, INT4 i4ArpCheckTimer)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpClientFastAccessArpCheckTimeOut
        (&u4ErrCode, i4ArpCheckTimer) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientFastAccessArpCheckTimeOut (i4ArpCheckTimer) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : DhcpClientSetRenew                                    */
/*                                                                           */
/* Description       : This function sets the Dhcp renew status and          */
/*                     sends the renew message on that interface.            */
/*                                                                           */
/* Input Parameters  : CliHandle  - CliContext ID                            */
/*                     i4IfIndex  - Interface Index on which renew message   */
/*                                  should be sent.                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpClientSetRenew (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrCode;

    /* DHCP knows only port numbers for which interfaces are registered
     * with IP
     */
    if (nmhTestv2DhcpClientRenew (&u4ErrCode, i4IfIndex, DHCP_SET) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientRenew (i4IfIndex, DHCP_SET) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name     : DhcpClientSetRelease                                  */
/*                                                                           */
/* Description       : This function sets the Dhcp release status and        */
/*                     sends the release message on that interface.          */
/*                                                                           */
/* Input Parameters  : CliHandle  - CliContext ID                            */
/*                     i4IfIndex  - Interface Index on which release message */
/*                                  should be sent.                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
DhcpClientSetRelease (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrCode;

    /* DHCP knows only port numbers for which interfaces are registered
     * with IP
     */

    if (nmhTestv2DhcpClientRelease (&u4ErrCode, i4IfIndex, DHCP_SET) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientRelease (i4IfIndex, DHCP_SET) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : DhcpClientSetTrace                                  */
/*                                                                        */
/*  Description     : This function configures DHCPC Trace level          */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4TraceLevel - Trace level to set                   */
/*                    u1TraceFlag  - flag to check for set/re-set         */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
DhcpClientSetTrace (tCliHandle CliHandle, INT4 i4TraceLevel, UINT1 u1TraceFlag)
{
    UINT4               u4ErrCode;
    INT4                i4DhcpClientTrace;

    nmhGetDhcpClientDebugTrace (&i4DhcpClientTrace);
    if (u1TraceFlag == CLI_DISABLE)
    {
        /* for reset of all trace command i4TraceLevel will have all bits sets
         * and doing a complement will reset all bit positions.
         */
        i4TraceLevel = i4DhcpClientTrace & (~i4TraceLevel);
    }
    else
    {
        i4TraceLevel = i4DhcpClientTrace | i4TraceLevel;
    }

    if (nmhTestv2DhcpClientDebugTrace (&u4ErrCode, i4TraceLevel) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetDhcpClientDebugTrace (i4TraceLevel) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : ShowDhcpClientFastAccess                            */
/*                                                                        */
/*  Description     : This function displays DHCP Client Fast Access      */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
DhcpClientShowFastAccess (tCliHandle CliHandle)
{
    INT4                i4RetValDhcpClientFastAccess;
    INT4                i4RetValDhcpClientFastAccessDiscoverTimeOut;
    INT4                i4RetValDhcpClientFastAccessNullStateTimeOut;
    INT4                i4RetValDhcpClientFastAccessArpCheckTimeOut;
    nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
    nmhGetDhcpClientFastAccessDiscoverTimeOut
        (&i4RetValDhcpClientFastAccessDiscoverTimeOut);
    nmhGetDhcpClientFastAccessNullStateTimeOut
        (&i4RetValDhcpClientFastAccessNullStateTimeOut);
    nmhGetDhcpClientFastAccessArpCheckTimeOut
        (&i4RetValDhcpClientFastAccessArpCheckTimeOut);
    CliPrintf (CliHandle, "DHCP Client Timer Settings\r\n");
    CliPrintf (CliHandle, "---- ------ ----- -------\r\n");
    if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Fast Access Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Fast Access Mode");
    }
    CliPrintf (CliHandle, "\n%-38s : %d\r\n",
               "Dhcp Client Fast Access DiscoverTimeOut",
               i4RetValDhcpClientFastAccessDiscoverTimeOut);
    CliPrintf (CliHandle, "\n%-38s : %d\r\n",
               "Dhcp Client Fast Access NullStateTimeOut",
               i4RetValDhcpClientFastAccessNullStateTimeOut);
    CliPrintf (CliHandle, "\n%-38s : %d\r\n",
               "Dhcp Client Fast Access Arp Check TimeOut",
               i4RetValDhcpClientFastAccessArpCheckTimeOut);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : ShowDhcpClientCounters                              */
/*                                                                        */
/*  Description     : This function displays DHCP Client statistics.      */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
DhcpClientShowStats (tCliHandle CliHandle)
{
    INT4                i4CurrentIndex = 0;
    INT4                i4NextIndex = 0;
    INT4                i4OutCome;
    INT4                i4PageStatus;
    UINT4               u4IpAddress = 0;
    UINT4               u4LeaseTime = 0;
    UINT4               u4RemainLease = 0;
    UINT4               u4Discovers = 0;
    UINT4               u4Requests = 0;
    UINT4               u4Declines = 0;
    UINT4               u4Releases = 0;
    UINT4               u4Informs = 0;
    UINT4               u4Offers = 0;
    UINT4               u4AcksInReq = 0;
    UINT4               u4NAcksInReq = 0;
    UINT4               u4AcksInRenew = 0;
    UINT4               u4NAcksInRenew = 0;
    UINT4               u4AcksInRebind = 0;
    UINT4               u4NAcksInRebind = 0;
    UINT4               u4AcksInReboot = 0;
    UINT4               u4NAcksInReboot = 0;
    CHR1               *pu1IpAddress = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4CountErrorInHeader = 0;
    UINT4               u4CountErrorInXid = 0;
    UINT4               u4ErrorInOptions = 0;

    i4OutCome = (INT4) (nmhGetFirstIndexDhcpClientConfigTable (&i4NextIndex));

    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nDhcp Client Statistics\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n\r\n");
    do
    {
        if (CfaCliGetIfName (i4NextIndex, (INT1 *) au1IfName) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "Interface  %20s %-17s\r\n", ":", au1IfName);

        }
        nmhGetDhcpClientIpAddress (i4NextIndex, &u4IpAddress);
        nmhGetDhcpClientLeaseTime (i4NextIndex, (INT4 *) &u4LeaseTime);
        nmhGetDhcpClientRemainLeaseTime (i4NextIndex, (INT4 *) &u4RemainLease);
        nmhGetDhcpClientCountDiscovers (i4NextIndex, &u4Discovers);
        nmhGetDhcpClientCountRequests (i4NextIndex, &u4Requests);
        nmhGetDhcpClientCountDeclines (i4NextIndex, &u4Declines);
        nmhGetDhcpClientCountReleases (i4NextIndex, &u4Releases);
        nmhGetDhcpClientCountInforms (i4NextIndex, &u4Informs);
        nmhGetDhcpClientCountOffers (i4NextIndex, &u4Offers);
        nmhGetDhcpCountAcksInReqState (i4NextIndex, &u4AcksInReq);
        nmhGetDhcpCountNacksInReqState (i4NextIndex, &u4NAcksInReq);
        nmhGetDhcpCountAcksInRenewState (i4NextIndex, &u4AcksInRenew);
        nmhGetDhcpCountNacksInRenewState (i4NextIndex, &u4NAcksInRenew);
        nmhGetDhcpCountAcksInRebindState (i4NextIndex, &u4AcksInRebind);
        nmhGetDhcpCountNacksInRebindState (i4NextIndex, &u4NAcksInRebind);
        nmhGetDhcpCountAcksInRebootState (i4NextIndex, &u4AcksInReboot);
        nmhGetDhcpCountNacksInRebootState (i4NextIndex, &u4NAcksInReboot);
        nmhGetDhcpCountErrorInHeader (i4NextIndex, &u4CountErrorInHeader);
        nmhGetDhcpCountErrorInXid (i4NextIndex, &u4CountErrorInXid);
        nmhGetDhcpCountErrorInOptions (i4NextIndex, &u4ErrorInOptions);

        i4CurrentIndex = i4NextIndex;

        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddress, u4IpAddress);
        CliPrintf (CliHandle, "Client IP Address %13s %-17s\r\n", ":",
                   pu1IpAddress);

        CliPrintf (CliHandle, "Client Lease Time %13s %d\r\n", ":",
                   u4LeaseTime);

        CliPrintf (CliHandle, "Client Remain Lease Time %6s %d\r\n", ":",
                   u4RemainLease);

        CliPrintf (CliHandle, "Message Statistics\r\n");
        CliPrintf (CliHandle, "------------------\r\n");

        CliPrintf (CliHandle, "DHCP DISCOVER %17s %d\r\n", ":", u4Discovers);

        CliPrintf (CliHandle, "DHCP REQUEST %18s %d\r\n", ":", u4Requests);

        CliPrintf (CliHandle, "DHCP DECLINE %18s %d\r\n", ":", u4Declines);

        CliPrintf (CliHandle, "DHCP RELEASE %18s %d\r\n", ":", u4Releases);

        CliPrintf (CliHandle, "DHCP INFORM %19s %d\r\n", ":", u4Informs);

        CliPrintf (CliHandle, "DHCP OFFER %20s %d\r\n\r\n", ":", u4Offers);

        CliPrintf (CliHandle, "DHCP ACKS IN REQ %14s %d\r\n", ":", u4AcksInReq);

        CliPrintf (CliHandle, "DHCP NACKS IN REQ %13s %d\r\n", ":",
                   u4NAcksInReq);

        CliPrintf (CliHandle, "DHCP ACKS IN RENEW %12s %d\r\n", ":",
                   u4AcksInRenew);

        CliPrintf (CliHandle, "DHCP NACKS IN RENEW %11s %d\r\n", ":",
                   u4NAcksInRenew);

        CliPrintf (CliHandle, "DHCP ACKS IN REBIND %11s %d\r\n", ":",
                   u4AcksInRebind);

        CliPrintf (CliHandle, "DHCP NACKS IN REBIND %10s %d\r\n", ":",
                   u4NAcksInRebind);

        CliPrintf (CliHandle, "DHCP ACKS IN REBOOT %11s %d\r\n", ":",
                   u4AcksInRebind);

        CliPrintf (CliHandle, "DHCP NACKS IN REBOOT %10s %d\r\n", ":",
                   u4NAcksInRebind);

        CliPrintf (CliHandle, "DHCP COUNT ERROR IN HEADER %4s %d\r\n", ":",
                   u4CountErrorInHeader);

        CliPrintf (CliHandle, "DHCP COUNT ERROR IN XID %7s %d\r\n", ":",
                   u4CountErrorInXid);

        i4PageStatus =
            CliPrintf (CliHandle, "DHCP COUNT ERROR IN OPTIONS %3s %d\r\n", ":",
                       u4ErrorInOptions);

        if (i4PageStatus == CLI_FAILURE)
        {
            /* Quit displaying of entires */
            break;
        }

    }
    while (nmhGetNextIndexDhcpClientConfigTable
           (i4CurrentIndex, &i4NextIndex) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssDhcpClientShowDebugging                         */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DHCP client  debug level  */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssDhcpClientShowDebugging (tCliHandle CliHandle)
{

    INT4                i4DbgLevel = 0;

    nmhGetDhcpClientDebugTrace (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "end \r\n");

    if ((i4DbgLevel & DHCPC_DEBUG_EVENT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DHCP client events debugging is on");
    }

    if ((i4DbgLevel & DHCPC_DEBUG_PACKETS) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DHCP client packets debugging is on");
    }
    if ((i4DbgLevel & DHCPC_DEBUG_ERRORS) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DHCP client errors debugging is on");
    }
    if ((i4DbgLevel & DHCPC_DEBUG_BIND) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DHCP client bind debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientSetOption                                */
/*                                                                           */
/*     DESCRIPTION      : This function configure the requesting option type */
/*                        The configured option type will be appended in     */
/*                        discover packet.                                   */
/*                                                                           */
/*     INPUT            : CliHandle - CLI context                            */
/*                        u4Status - CLI_ENABLE/CLI_DISABLE                  */
/*                        u4Index - Interface Index                          */
/*                        u4OptType - Option Type number                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpClientSetOption (tCliHandle CliHandle, UINT4 u4Status,
                     UINT4 u4Index, UINT4 u4OptType)
{
    UINT4               u4ErrorCode;

    if (u4Status == CLI_ENABLE)
    {
        if (nmhTestv2DhcpClientOptRowStatus (&u4ErrorCode, u4Index, u4OptType,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetDhcpClientOptRowStatus (u4Index, u4OptType, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2DhcpClientOptRowStatus (&u4ErrorCode, u4Index, u4OptType,
                                             ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetDhcpClientOptRowStatus (u4Index, u4OptType, ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2DhcpClientOptRowStatus (&u4ErrorCode, u4Index, u4OptType,
                                             DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetDhcpClientOptRowStatus (u4Index, u4OptType, DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientShowAllOption                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the configured dhcp client  */
/*                        option type information in all interfaces          */
/*                                                                           */
/*     INPUT            : CliHandle - CLI context                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DhcpClientShowAllOption (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpcOptVal;
    UINT4               u4IfIndex = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4OptType = 0;
    UINT4               u4NextOptType = 0;
    INT4                i4Len = 0;
    INT4                i4OutCome = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1OptVal[DHCP_MAX_OPT_LEN];

    MEMSET (&DhcpcOptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OptVal, 0, DHCP_MAX_OPT_LEN);
    DhcpcOptVal.pu1_OctetList = &au1OptVal[0];

    i4OutCome = nmhGetFirstIndexDhcpClientOptTable ((INT4 *) (&u4NextIfIndex),
                                                    (INT4 *) (&u4NextOptType));
    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nDhcp Client Options\r\n");

    CliPrintf (CliHandle, "\r\nInterface   Type   Len   Value \r\n");
    CliPrintf (CliHandle, "---------   ----   ---   ------\r\n\r\n");

    while (i4OutCome != SNMP_FAILURE)
    {
        u4IfIndex = u4NextIfIndex;
        u4OptType = u4NextOptType;

        MEMSET (DhcpcOptVal.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);
        DhcpcOptVal.i4_Length = 0;
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        i4Len = 0;
        if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "%-10s ", au1IfName);

        }
        CliPrintf (CliHandle, "%-6d ", u4OptType);
        nmhGetDhcpClientOptLen (u4IfIndex, u4OptType, &i4Len);
        if (i4Len != 0)
            CliPrintf (CliHandle, " %-3d ", i4Len);
        nmhGetDhcpClientOptVal (u4IfIndex, u4OptType, &DhcpcOptVal);
        CliPrintf (CliHandle, " %-255s \r\n", DhcpcOptVal.pu1_OctetList);

        i4OutCome = nmhGetNextIndexDhcpClientOptTable ((INT4) u4IfIndex,
                                                       (INT4 *) &u4NextIfIndex,
                                                       (INT4) u4OptType,
                                                       (INT4 *) &u4NextOptType);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientSetConfigId                              */
/*                                                                           */
/*     DESCRIPTION      : This function configure the requesting option type */
/*                        The configured option type will be appended in     */
/*                        discover packet.                                   */
/*                                                                           */
/*     INPUT            : CliHandle - CLI context                            */
/*                        u4IfIndex - Interface Index                        */
/*                        u4IdIndex - Interface Index which mac address      */
/*                                    will be used as identifier             */
/*                        pu1OptVal - string of identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DhcpClientSetConfigId (tCliHandle CliHandle, UINT4 u4IfIndex,
                       UINT4 u4IdIndex, UINT1 *pu1OptVal)
{
    tSNMP_OCTET_STRING_TYPE ClientIdentifier;
    tCfaIfInfo          IfInfo;
    UINT4               u4ErrorCode;
    UINT1               au1ClientId[DHCP_MAX_CLIENTID_LEN];

    MEMSET (&ClientIdentifier, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1ClientId, 0, DHCP_MAX_CLIENTID_LEN);
    ClientIdentifier.pu1_OctetList = &au1ClientId[0];
    MEMSET (ClientIdentifier.pu1_OctetList, 0, DHCP_MAX_CLIENTID_LEN);
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    if (u4IdIndex != 0)
    {
        if (CfaGetIfInfo ((UINT2) u4IdIndex, &IfInfo) == CFA_SUCCESS)
        {
            /* When the client identifier is mac address, set the first byte
             * ie, hardware type as one */
            ClientIdentifier.pu1_OctetList[0] = 1;
            MEMCPY (&ClientIdentifier.pu1_OctetList[1], &IfInfo.au1MacAddr[0],
                    CFA_ENET_ADDR_LEN);
            ClientIdentifier.i4_Length = (CFA_ENET_ADDR_LEN + 1);
        }
    }

    if (pu1OptVal != NULL)
    {
        ClientIdentifier.i4_Length =
            MEM_MAX_BYTES (STRLEN (pu1OptVal), DHCP_MAX_CLIENTID_LEN);
        if (ClientIdentifier.i4_Length != 0)
        {
            MEMCPY (ClientIdentifier.pu1_OctetList, pu1OptVal,
                    ClientIdentifier.i4_Length);
        }
    }

    if (nmhTestv2DhcpClientIdentifier (&u4ErrorCode,
                                       (INT4) u4IfIndex,
                                       &ClientIdentifier) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientIdentifier ((INT4) u4IfIndex,
                                    &ClientIdentifier) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientShowIdentifiers                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the configured dhcp client  */
/*                        identifier                                         */
/*                                                                           */
/*     INPUT            : CliHandle - CLI context                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DhcpClientShowIdentifiers (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpClntIdentifier;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1CidName[DHCP_MAX_CLIENTID_LEN];
    INT1               *piIfName;
    INT4                i4OutCome = SNMP_FAILURE;
    INT4                i4NextIndex;
    INT4                i4CurrentIndex;
    UINT1              *pu1Address = NULL;
    UINT1               au1Address[DHCP_MAX_CLIENTID_LEN];
    INT4                i4RetVal = 0;

    pu1Address = au1Address;
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) (&au1IfName[0]);

    MEMSET (&au1CidName, 0, DHCP_MAX_CLIENTID_LEN);
    DhcpClntIdentifier.pu1_OctetList = au1CidName;
    DhcpClntIdentifier.i4_Length = 0;
    
    MEMSET(DhcpClntIdentifier.pu1_OctetList,0,sizeof (au1CidName));

    i4OutCome = (INT4) (nmhGetFirstIndexDhcpClientConfigTable (&i4NextIndex));
    if (i4OutCome == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    while (i4OutCome != SNMP_FAILURE)
    {
        nmhGetDhcpClientIdentifier ((UINT4) i4NextIndex, &DhcpClntIdentifier);
        /*if ( DhcpClientIdentifier.i4_Length != 0) */
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            i4RetVal = CfaCliGetIfName ((UINT4) i4NextIndex, piIfName);
            UNUSED_PARAM (i4RetVal);
            CliPrintf (CliHandle, "\r\n%-12s", piIfName);

            if ((DhcpClntIdentifier.pu1_OctetList[0] == 1)
                && (DhcpClntIdentifier.i4_Length == 7))
            {
                MEMSET (au1Address, 0, DHCP_MAX_CLIENTID_LEN);
                CLI_CONVERT_MAC_TO_DOT_STR
                    (&(DhcpClntIdentifier.pu1_OctetList[1]),
                     (UINT1 *) pu1Address);
                CliPrintf (CliHandle, "Client Identifier is %s\r\n",
                           pu1Address);
            }
            else
            {
                CliPrintf (CliHandle, "Client Identifier is %s\r\n",
                           DhcpClntIdentifier.pu1_OctetList);
            }
        }
        i4CurrentIndex = i4NextIndex;
        i4OutCome = nmhGetNextIndexDhcpClientConfigTable
            (i4CurrentIndex, &i4NextIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientClearStatsAllInterface                  */
/*                                                                           */
/*     DESCRIPTION      : This function clear used to cleant the stats for   */
/*                        all the Interface of DHCP Client.                  */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
DhcpClientClearStatsAllInterface (tCliHandle CliHandle)
{
    INT1                i1OutCome = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;

    i1OutCome = nmhGetFirstIndexDhcpClientCounterTable (&i4NextPort);

    while ((i1OutCome != SNMP_FAILURE) && (i4NextPort <= MAX_DHC_INTERFACE))
    {
        if (nmhValidateIndexInstanceDhcpClientCounterTable (i4NextPort) !=
            SNMP_SUCCESS)
        {
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexDhcpClientCounterTable (i4CurrentPort,
                                                       &i4NextPort);
            continue;
        }
        DhcpClientClearStatsInterface (CliHandle, i4NextPort, DHCP_SET);

        i4CurrentPort = i4NextPort;
        i1OutCome =
            nmhGetNextIndexDhcpClientCounterTable (i4CurrentPort, &i4NextPort);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientAddVendorSpecificInfo                    */
/*                                                                           */
/*     DESCRIPTION      : This function adds vendor specific information     */
/*                        for the DHCP client on the specified interface.    */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        pu1Value--  Value                                  */
/*                          u4Index -- interface index                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     GLOBALS MODIFIED : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
DhcpClientAddVendorSpecificInfo (tCliHandle CliHandle, UINT1 *pu1Value,
                                 UINT4 u4Index)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE OptVal;

    MEMSET (&OptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    OptVal.i4_Length = (INT4) STRLEN (pu1Value);
    OptVal.pu1_OctetList = (UINT1 *) pu1Value;

    if (nmhTestv2DhcpClientOptRowStatus
        (&u4ErrorCode, (INT4) u4Index, DHCP_OPT_CVENDOR_SPECIFIC,
         CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientOptRowStatus
        (u4Index, DHCP_OPT_CVENDOR_SPECIFIC, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpClientOptLen
        (&u4ErrorCode, u4Index, DHCP_OPT_CVENDOR_SPECIFIC,
         OptVal.i4_Length) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpClientOptVal
        (&u4ErrorCode, (INT4) u4Index, DHCP_OPT_CVENDOR_SPECIFIC,
         &OptVal) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientOptLen
        (u4Index, DHCP_OPT_CVENDOR_SPECIFIC, OptVal.i4_Length) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientOptVal (u4Index, DHCP_OPT_CVENDOR_SPECIFIC, &OptVal)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpClientOptRowStatus
        (&u4ErrorCode, (INT4) u4Index, DHCP_OPT_CVENDOR_SPECIFIC,
         ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientOptRowStatus
        (u4Index, DHCP_OPT_CVENDOR_SPECIFIC, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientRemoveVendorSpecificInfo                 */
/*                                                                           */
/*     DESCRIPTION      : This function removes vendor specific information  */
/*                        for the DHCP client on the specified interface.    */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                          u4Index -- Interface index                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     GLOBALS MODIFIED : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
DhcpClientRemoveVendorSpecificInfo (tCliHandle CliHandle, UINT4 u4Index)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2DhcpClientOptRowStatus
        (&u4ErrorCode, (INT4) u4Index, DHCP_OPT_CVENDOR_SPECIFIC,
         DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientOptRowStatus
        (u4Index, DHCP_OPT_CVENDOR_SPECIFIC, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientClearStatsInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function clear the stats for the Interface    */
/*                        of DHCP Client.                                    */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                        i4Index - Interface Index                          */
/*                        u4Value-  Value                                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
DhcpClientClearStatsInterface (tCliHandle CliHandle, INT4 i4Index,
                               UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;

    if (nmhValidateIndexInstanceDhcpClientCounterTable (i4Index) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n %% Interface does not exist in client mode.\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpClientCounterReset
        (&u4ErrCode, i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpClientCounterReset (i4Index, (INT4) u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpClientShowRunningConfig                        */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        DHCP client                                        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DhcpClientShowRunningConfig (tCliHandle CliHandle, UINT4 u4ModuleId)
{
    INT4                i4CheckFlag = FALSE;
    INT4                i4FastAccess = 0;
    INT4                i4FADiscoverTimeOut = 0;
    INT4                i4FANullStateTimeOut = 0;
    INT4                i4FAArpCheckTimeOut = 0;
    INT4                i4OutConfigTable = SNMP_FAILURE;
    INT4                i4OutOptTable = SNMP_FAILURE;
    INT4                i4NextIndex;
    INT4                i4CurrentIndex;
    INT4                i4OptType = 0;
    INT4                i4NextOptType = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4Len = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1CidName[DHCP_MAX_CLIENTID_LEN];
    UINT1               au1Address[DHCP_MAX_CLIENTID_LEN];
    UINT1              *pu1Address = NULL;
    UINT1               au1OptVal[DHCP_MAX_OPT_LEN];

    tSNMP_OCTET_STRING_TYPE DhcpClntIdentifier;
    tSNMP_OCTET_STRING_TYPE DhcpcOptVal;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&au1CidName, 0, DHCP_MAX_CLIENTID_LEN);

    MEMSET (&DhcpcOptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OptVal, 0, DHCP_MAX_OPT_LEN);
    DhcpcOptVal.pu1_OctetList = &au1OptVal[0];
    
    pu1Address = au1Address;
    DhcpClntIdentifier.pu1_OctetList = au1CidName;
    DhcpClntIdentifier.i4_Length = 0;
    
    MEMSET (DhcpClntIdentifier.pu1_OctetList, 0, sizeof (au1CidName));

    CliRegisterLock (CliHandle, DhcpCProtocolLock, DhcpCProtocolUnlock);
    DHCPC_PROTO_LOCK ();
    u4ModuleId = 0;
    UNUSED_PARAM (u4ModuleId);

    nmhGetDhcpClientFastAccess (&i4FastAccess);
    nmhGetDhcpClientFastAccessDiscoverTimeOut (&i4FADiscoverTimeOut);
    nmhGetDhcpClientFastAccessNullStateTimeOut (&i4FANullStateTimeOut);
    nmhGetDhcpClientFastAccessArpCheckTimeOut (&i4FAArpCheckTimeOut);

    if (i4FastAccess != DHCPC_FAST_ACC_DISABLE)
    {
        CliPrintf (CliHandle, "end \r\n");
        CliPrintf (CliHandle, "ip dhcp client fast-access \r\n");
        i4CheckFlag = TRUE;
    }

    if (i4FADiscoverTimeOut != DHCPC_DISCOVER_RETX_TIME_OUT)
    {
        CliPrintf (CliHandle, "ip dhcp client discovery timer %d\r\n",
                   i4FADiscoverTimeOut);
    }

    if (i4FANullStateTimeOut != DHCPC_NULL_STATE_WAIT_TIME_OUT)
    {
        CliPrintf (CliHandle, "ip dhcp client idle timer %d\r\n",
                   i4FANullStateTimeOut);
    }

    if (i4FAArpCheckTimeOut != DHCPC_ARP_CHECK_TIME_OUT)
    {
        CliPrintf (CliHandle, "ip dhcp client arp-check timer %d\r\n",
                   i4FAArpCheckTimeOut);
    }
    if (i4CheckFlag != FALSE) 
    { 
        CliPrintf (CliHandle, "!\r\n");
        i4CheckFlag = FALSE;
    } 
    i4OutConfigTable =
        (INT4) (nmhGetFirstIndexDhcpClientConfigTable (&i4NextIndex));

    while (i4OutConfigTable != SNMP_FAILURE)
    {
        nmhGetDhcpClientIdentifier ((UINT4) i4NextIndex, &DhcpClntIdentifier);
        if (DhcpClntIdentifier.i4_Length != 0)
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

            if ((CfaCliConfGetIfName ((UINT4) i4NextIndex, (INT1 *) au1IfName)) ==
                CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\ninterface %s \r\n", au1IfName);
                i4CheckFlag = TRUE;
            }

            /* if client identifier is mac address */

            if ((DhcpClntIdentifier.pu1_OctetList[0] == 1)
                && (DhcpClntIdentifier.i4_Length == 7))
            {
                MEMSET (au1Address, 0, DHCP_MAX_CLIENTID_LEN);
                CLI_CONVERT_MAC_TO_DOT_STR
                    (&(DhcpClntIdentifier.pu1_OctetList[1]),
                     (UINT1 *) pu1Address);
                CliPrintf (CliHandle, "Client Identifier is %s\r\n",
                           pu1Address);
            }
            else
            {
                CliPrintf (CliHandle, "Client Identifier is %s\r\n",
                           DhcpClntIdentifier.pu1_OctetList);
            }
        }
        i4CurrentIndex = i4NextIndex;
        i4OutConfigTable = nmhGetNextIndexDhcpClientConfigTable
            (i4CurrentIndex, &i4NextIndex);
    }

    i4OutOptTable =
        (INT4) nmhGetFirstIndexDhcpClientOptTable ((&i4NextIfIndex),
                                                   (&i4NextOptType));
    while (i4OutOptTable != SNMP_FAILURE)
    {
        i4IfIndex = i4NextIfIndex;
        i4OptType = i4NextOptType;

        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

        MEMSET (DhcpcOptVal.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);
        DhcpcOptVal.i4_Length = 0;

        if ((CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1IfName) == CLI_SUCCESS) &&
            (i4PrevIfIndex != i4IfIndex) && ((i4OptType == DHCP_OPT_TFTP_SNAME) || (i4OptType == DHCP_OPT_BOOT_FILE_NAME) ||
                                            (i4OptType == DHCP_OPT_SIP_SERVER) || (i4OptType == DHCP_OPT_240) || (i4OptType == DHCP_OPT_CVENDOR_SPECIFIC)))
        {
            CliPrintf (CliHandle, "interface %s \r\n", au1IfName);
            i4PrevIfIndex = i4IfIndex;
            i4CheckFlag = TRUE;
        }
        if (i4OptType == DHCP_OPT_TFTP_SNAME)
        {
            CliPrintf (CliHandle,
                       "ip dhcp client request tftp-server-name \r\n");
        }
        else if (i4OptType == DHCP_OPT_BOOT_FILE_NAME)
        {
            CliPrintf (CliHandle, "ip dhcp client request boot-file-name \r\n");
        }
        else if (i4OptType == DHCP_OPT_SIP_SERVER)
        {
            CliPrintf (CliHandle,
                       "ip dhcp client request sip-server-info \r\n");
        }
        else if (i4OptType == DHCP_OPT_240)
        {
            CliPrintf (CliHandle, "ip dhcp client request option240 \r\n");
        }
        else if (i4OptType == DHCP_OPT_CVENDOR_SPECIFIC)
        {
            nmhGetDhcpClientOptLen (i4IfIndex, i4OptType, &i4Len);
            if (i4Len != 0)
            {
                nmhGetDhcpClientOptVal (i4IfIndex, i4OptType, &DhcpcOptVal);
                CliPrintf (CliHandle, "ip dhcp client vendor-specific %s\r\n",DhcpcOptVal.pu1_OctetList);
            }
        }
        if (i4CheckFlag == TRUE) 
        { 
            CliPrintf (CliHandle, "!\r\n"); 
            i4CheckFlag = FALSE;
        } 
        i4OutOptTable = nmhGetNextIndexDhcpClientOptTable (i4IfIndex,
                                                           &i4NextIfIndex,
                                                           i4OptType,
                                                           &i4NextOptType);
    }

    DHCPC_PROTO_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

#endif /* DHCPC_WANTED */

#endif /* __DHCPCCLI_C__ */
