/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dhcproc.c,v 1.77 2017/12/19 10:00:02 siva Exp $
 *
 * DESCRIPTION : Contains processing module functions of DHCP CLIENT 
 ********************************************************************/

/************************************************************************
 * Include files                                                        *
 ************************************************************************/

#include "dhccmn.h"
#include "rtm.h"
#include "vcm.h"
#include "fsntp.h"
#ifdef WTP_WANTED
#include "capwap.h"
#endif

tRegDhcpDiscOption  gRegDhcpDiscOption;

/* test func - function added for unit testing purpose - replace with actual callback */
VOID
test_func (UINT4 u4Type, UINT4 u4Len, VOID *Val)
{
    UNUSED_PARAM (u4Type);
    UNUSED_PARAM (u4Len);
    UNUSED_PARAM (Val);
}

/************************************************************************/
/*  Function Name   : DhcpCRegisterOptions                              */
/*  Description     : This function is used to register a DHCP option   */
/*                      with its respective implementation              */
/*  Input(s)        : u4Option - Option ID                              */
/*                     (*pDhcpCOptionCallBack) - CallBack pointer       */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCRegisterOptions (UINT4 u4Option,
                      void (*pDhcpCOptionCallBack) (UINT4, UINT4, VOID *))
{
    tTMO_SLL_NODE      *pNode = NULL;
    tDhcpCRegOptionStruct *pDhcpcRegOption = NULL;

    /* First check if valid option and valid address */
    if (u4Option < 1 || pDhcpCOptionCallBack == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "The Option is Invalid \n");
        return DHCP_FAILURE;
    }

    /* check if the option is already registered */
    TMO_SLL_Scan (&gDhcpOptionRegList, pNode, tTMO_SLL_NODE *)
    {
        pDhcpcRegOption = (tDhcpCRegOptionStruct *) pNode;

        /* If option exists, then reset with new CallBack */
        if (pDhcpcRegOption->u4DhcpOptionId == u4Option)
        {
            DHCPC_TRC (DEBUG_TRC,
                       "DhcpCoptionsRegister fn : Reseting the Registration function \n");
            pDhcpcRegOption->pDhcpCOptionCallBack = pDhcpCOptionCallBack;
            return DHCP_SUCCESS;
        }
    }

    /* if option is not registered, create a new entry in the List */
    if ((pDhcpcRegOption =
         (tDhcpCRegOptionStruct *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                                   DhcpCDiscPoolId)) == NULL)
    {
        return DHCP_FAILURE;
    }

    pDhcpcRegOption->u4DhcpOptionId = u4Option;
    pDhcpcRegOption->pDhcpCOptionCallBack = pDhcpCOptionCallBack;

    TMO_SLL_Add (&gDhcpOptionRegList, (tTMO_SLL_NODE *) pDhcpcRegOption);

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCUnRegisterOptions                            */
/*  Description     : This function deregisters the callback for        */
/*                    given option                                      */
/*  Input(s)        : u4DhcpOption - Option Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCUnRegisterOptions (UINT4 u4DhcpOption)
{
    tTMO_SLL_NODE      *pNode;
    tDhcpCRegOptionStruct *pDhcpcRegOption = NULL;

    TMO_SLL_Scan (&gDhcpOptionRegList, pNode, tTMO_SLL_NODE *)
    {
        pDhcpcRegOption = (tDhcpCRegOptionStruct *) pNode;
        if (pDhcpcRegOption->u4DhcpOptionId == u4DhcpOption)
        {
            TMO_SLL_Delete (&gDhcpOptionRegList, pNode);
            return;
        }
    }
    DHCPC_TRC (DEBUG_TRC,
               "Exiting DhcpCoptionsUnRegister fn  for unregistered Option input \n");
}

/************************************************************************/
/*  Function Name   : DhcpCParseSipInfo                                 */
/*  Description     : This function parses sip info based on encoding   */
/*                      bit                                             */
/*  Input(s)        : pu1Len - pointer to length of sip info            */
/*                      pau1OptVal - pointer to value of sip info       */
/*                      pu4Type - pointer to type of sip info           */
/*  Output(s)       : NONE                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
UINT1
DhcpCParseSipInfo (UINT1 *pu1Len, UINT1 *pau1OptVal, UINT4 *pu4Type)
{
    UINT1               u1OutBufIndex = 0;
    UINT1               u1CurIndex = 0;
    UINT1               u1LenCurstr = 0;
    UINT1               u1Len = 0;
    UINT1               au1OutData[DHCP_MAX_OPT_LEN];
    UINT4               u4IpAddr = 0;
    tUtlInAddr          IpAddr;
    CHR1               *pu1TempIp = NULL;
    MEMSET (au1OutData, 0, DHCP_MAX_OPT_LEN);

    /* SIP server could be domain name or IP address and its based on encoding,
     * ALSO need to ignore encoding bit */
    if (DhcpCTag.Val[0] == 1)
    {
        *pu4Type = DHCPC_OPTION_IP;    /* IP address requires no further preprocessing */
        u1Len = (UINT1) (DhcpCTag.Len - 1);
        for (u1CurIndex = 1; u1CurIndex <= u1Len;
             u1CurIndex = (UINT1) (u1CurIndex + sizeof (UINT4)))
        {
            if (u1OutBufIndex >= DHCP_MAX_ALLOWED_LEN)
            {                    /* Exceeded max option length. Exit from loop and return */
                break;
            }
            if (u1OutBufIndex != 0)
            {
                au1OutData[u1OutBufIndex] = ',';
                u1OutBufIndex++;
            }
            MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
            MEMSET (&u4IpAddr, 0, sizeof (UINT4));
            MEMCPY (&u4IpAddr, &DhcpCTag.Val[u1CurIndex], sizeof (UINT4));
            IpAddr.u4Addr = (UINT4) u4IpAddr;
            pu1TempIp = (CHR1 *) UtlInetNtoa (IpAddr);

            if (STRLEN (pu1TempIp) > DHCP_MAX_STR_IP_LEN)
            {                    /* invalid ip length - skip to next ip */
                continue;
            }
            MEMCPY (&au1OutData[u1OutBufIndex], pu1TempIp, STRLEN (pu1TempIp));
            u1OutBufIndex = (UINT1) (u1OutBufIndex + STRLEN (pu1TempIp));
        }
        *pu1Len = (UINT1) u1OutBufIndex;
        STRCPY (pau1OptVal, au1OutData);

    }
    else if (DhcpCTag.Val[0] == 0)
    {
        *pu4Type = DHCPC_OPTION_STRING;    /* Domain Names (string) require some processing  */
        /* PRINT DOMAIN NAME */
        /* modded code for multi input */
        *pu1Len = DhcpCTag.Len;
        u1OutBufIndex = 0;
        u1CurIndex = 1;
        u1LenCurstr = 0;
        MEMSET (pau1OptVal, 0, DHCP_MAX_OPT_LEN + 1);
        while (u1CurIndex < (*pu1Len))
        {
            u1LenCurstr = (UINT4) (DhcpCTag.Val[u1CurIndex]);
            u1CurIndex++;
            MEMCPY (&pau1OptVal[u1OutBufIndex], &DhcpCTag.Val[u1CurIndex],
                    u1LenCurstr);
            u1OutBufIndex = (UINT1) (u1OutBufIndex + u1LenCurstr);
            u1CurIndex = (UINT1) (u1CurIndex + u1LenCurstr);
            if (DhcpCTag.Val[u1CurIndex] == 0)
            {
                pau1OptVal[u1OutBufIndex] = 0;
                u1OutBufIndex++;
                u1CurIndex++;
            }
            else
            {
                pau1OptVal[u1OutBufIndex] = '.';
                u1OutBufIndex++;
            }
        }
        *pu1Len = (UINT1) (u1OutBufIndex - 1);
    }
    else
    {
        /* Invalid Input: Return Failure */
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCCheckRegisteredOptions                       */
/*  Description     : This function calls the callback function for     */
/*                      options                                         */
/*  Input(s)        : pPkt - pointer to recieved packet                 */
/*                    u4CfaIndex - interface index                      */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCParseRegisteredOptions (UINT4 u4CfaIfIndex, tDhcpPktInfo * pPkt)
{
    tTMO_SLL_NODE      *pNode;
    tDhcpCRegOptionStruct *pDhcpcRegOption = NULL;
    UINT4               u4Type = 0;
    UINT1               au1OptVal[DHCP_MAX_OPT_LEN + 1];
    UINT1               u1Len = 0;
    UINT1               u1RetVal = 0;
    /* Variables for RBTree */
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tDhcpCOptions      *pDefaultOptNode;

    /* Variables used for display */
    tDhcpCOptions      *pDhcpCDisplayOption = NULL;
    UINT1               u1CurIndex = 0;
    UINT1               au1OutData[DHCP_MAX_OPT_LEN];
    UINT1               u1OutBufIndex;
    UINT4               u4IpAddr = 0;
    tUtlInAddr          IpAddr;
    CHR1               *pu1TempIp = NULL;

    MEMSET (au1OutData, 0, DHCP_MAX_OPT_LEN);
    MEMSET (au1OptVal, 0, DHCP_MAX_OPT_LEN + 1);

    pRBElem = RBTreeGetFirst (gDhcpCReqOptionList);

    while (pRBElem != NULL)
    {
        pDefaultOptNode = (tDhcpCOptions *) pRBElem;
        if ((u4CfaIfIndex == pDefaultOptNode->u4IfIndex)
            && (DhcpCGetOption (pDefaultOptNode->u1Type, pPkt) == DHCP_FOUND))
        {
            switch (pDefaultOptNode->u1Type)
            {
                case DHCP_OPT_SIP_SERVER:
                    u1RetVal =
                        (UINT1) DhcpCParseSipInfo (&u1Len, au1OptVal, &u4Type);
                    if (u1RetVal == DHCP_FAILURE)
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "FAILURE IN PARSE SIP INFO FUNCTION\n");
                        return;    /* Void function failure */
                    }
                    break;

                case DHCP_OPT_NTP_SERVERS:
                case DHCP_OPT_DNS_NS:
                case DHCP_OPT_ROUTER_OPTION:
                    /* DNS AND NTP both expect UPTO 2 IP addresses as input, will also work for 1 IP */
                    u1Len = (UINT1) DhcpCTag.Len;
                    MEMSET (au1OptVal, 0, DHCP_MAX_OPT_LEN + 1);
                    MEMCPY (au1OptVal, DhcpCTag.Val, u1Len);
                    u4Type = DHCPC_OPTION_IP;
                    break;

                case DHCP_OPT_SVENDOR_SPECIFIC:
                case DHCP_OPT_240:
                    /* 240 and 43 options both expect single string as input */
                    u1Len = (UINT1) DhcpCTag.Len;
                    MEMSET (au1OptVal, 0, DHCP_MAX_OPT_LEN + 1);
                    MEMCPY (au1OptVal, DhcpCTag.Val, u1Len);
                    u4Type = DHCPC_OPTION_STRING;
                    break;

                default:
                    DHCPC_TRC (DEBUG_TRC, "UNEXPECTED OPTION\n");
                    return;        /* Void function failure */
            }                    /* END OF SWITCH */

            /* Scan the register list and call callback if available */
            TMO_SLL_Scan (&gDhcpOptionRegList, pNode, tTMO_SLL_NODE *)
            {
                pDhcpcRegOption = (tDhcpCRegOptionStruct *) pNode;
                if (pDhcpcRegOption->u4DhcpOptionId == pDefaultOptNode->u1Type)
                {
                    pDhcpcRegOption->pDhcpCOptionCallBack (u4Type, u1Len,
                                                           au1OptVal);
                    break;        /* break from loop once correct option is found */
                }
            }                    /* END OF SCAN */

            /* Add to Display structure */
            if (u4Type == DHCPC_OPTION_STRING)
            {
                for (u1CurIndex = 0; u1CurIndex < (u1Len - 1); u1CurIndex++)
                {
                    /* Loop thru the input and change delimiter to ',' for display purpose */
                    if (au1OptVal[u1CurIndex] == '\0')
                    {
                        au1OptVal[u1CurIndex] = ',';
                    }
                }
            }
            else
            {
                /* Assumes u4Type is IP if it is not string */
                u1OutBufIndex = 0;
                for (u1CurIndex = 0; u1CurIndex < u1Len;
                     u1CurIndex = (UINT1) (u1CurIndex + sizeof (UINT4)))
                {
                    if (u1OutBufIndex >= DHCP_MAX_ALLOWED_LEN)
                    {            /* Exceeded max option length. Exit from loop and return */
                        break;
                    }
                    if (u1OutBufIndex != 0)
                    {
                        au1OutData[u1OutBufIndex] = ',';
                        u1OutBufIndex++;
                    }
                    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                    MEMSET (&u4IpAddr, 0, sizeof (UINT4));
                    MEMCPY (&u4IpAddr, &au1OptVal[u1CurIndex], sizeof (UINT4));
                    IpAddr.u4Addr = (UINT4) u4IpAddr;
                    pu1TempIp = (CHR1 *) UtlInetNtoa (IpAddr);
                    if (STRLEN (pu1TempIp) > DHCP_MAX_STR_IP_LEN)
                    {            /* invalid ip length - skip to next ip */
                        continue;
                    }
                    MEMCPY (&au1OutData[u1OutBufIndex], pu1TempIp,
                            STRLEN (pu1TempIp));
                    u1OutBufIndex =
                        (UINT1) (u1OutBufIndex + STRLEN (pu1TempIp));
                }
                u1Len = (UINT1) u1OutBufIndex;
                SPRINTF ((CHR1 *) au1OptVal, "%s\r\n", au1OutData);
            }
            pDhcpCDisplayOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          pDefaultOptNode->
                                                          u1Type);
            if (pDhcpCDisplayOption != NULL)
            {
                if (pDhcpCDisplayOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCDisplayOption->au1OptionVal, 0,
                            DHCP_MAX_OPT_LEN);
                    pDhcpCDisplayOption->u1Len = (UINT1) u1Len;
                    MEMCPY (pDhcpCDisplayOption->au1OptionVal, au1OptVal,
                            u1Len);
                }
            }
        }                        /* END OF IF */
        pRBNextElem = RBTreeGetNext (gDhcpCReqOptionList, pRBElem, NULL);
        pRBElem = pRBNextElem;
    }                            /* END OF WHILE */

}

/************************************************************************/
/*  Function Name   : DhcpCInitState                                    */
/*  Description     : This function processes S_INIT state              */
/*  Input(s)        : pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCInitState (tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCInitState fn\n");

    /* if any offer is available flush it and then start processing
     * useful when the function revisited without rebooting */
    DhcpCFlushOffer (pDhcpCNode);

    /* Reset the counts, useful when the function revisited without 
       rebooting */
    pDhcpCNode->u1ReTxCount = DHCPC_RESET;
    pDhcpCNode->u1DhcpCInformMsgSent = DHCPC_RESET;
    pDhcpCNode->u4ProcessedAck = DHCPC_RESET;

    /* Broadcast a  DHCP_DISCOVER message */
    u1RetVal = (UINT1) DhcpCSendDiscover (pDhcpCNode);

    if (u1RetVal != DHCP_SUCCESS)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "FAILURE in Tx DHCP_DISCOVER Msg for Intf %x in S_INIT state\n",
                    pDhcpCNode->u4IpIfIndex);
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCInitState fn\n");

    return u1RetVal;
}                                /* End of DhcpCInitState function */

/************************************************************************/
/*  Function Name   : DhcpCInitRebootState                              */
/*  Description     : This function processes S_INITREBOOT state        */
/*  Input(s)        : pDhcpCNode - DHCP Client Interface Record         */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCInitRebootState (tDhcpCRec * pDhcpCNode)
{
    UINT1               u1RetVal = DHCP_FAILURE;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCInitRebootState fn\n");

    /* Broadcast a DHCP_REQUEST message */
    u1RetVal = (UINT1) DhcpCSendRequest (pDhcpCNode);

    if (u1RetVal != DHCP_SUCCESS)
    {
        DHCPC_TRC1 (FAIL_TRC,
                    "Tx DHCP_REQUEST FAILURE in S_INITREBOOT state for %x intf\n",
                    pDhcpCNode->u4IpIfIndex);

        return u1RetVal;
    }

    (pDhcpCNode->DhcIfInfo).u1State = S_REBOOTING;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCInitRebootState fn\n");

    return u1RetVal;
}                                /* End of DhcpCInitRebootState function */

/************************************************************************/
/*  Function Name   : ProcessDhcpCOffer                                 */
/*  Description     : This function processes DHCP_OFFER recived        */
/*  Input(s)        : pPktInfo -> Pointer to the packet information     */
/*                    received                                          */
/*                    pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ProcessDhcpCOffer (tDhcpPktInfo * pPktInfo, tDhcpCRec * pDhcpCNode)
{
    tBestOffer         *pTempBestOffer = NULL;
    UINT4               u4TimeLeft = 0;
    UINT4               u4SubnetMask;
    UINT4               u4CfaIfIndex = 0;
    UINT1               u1RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered ProcessDhcpCOffer fn\n");

    if ((pDhcpCNode->DhcIfInfo).u1State != S_SELECTING)
    {
        DHCPC_TRC2 (DEBUG_TRC,
                    "Offer Rejected.State for %x intf in %d state \n",
                    pPktInfo->u4IfIndex, (pDhcpCNode->DhcIfInfo).u1State);
        return;
    }

    /* Increment the offers received count for the interface */
    (pDhcpCNode->TxRxStats).u4DhcpCNoOfOffersRcvd++;

    /* Same subnet address shouldn't be allowed for multiple 
     * interfaces */

    if (DhcpCGetOption (DHCP_OPT_SUBNET_MASK, pPktInfo) == DHCP_FOUND)
    {
        MEMCPY (&u4SubnetMask, DhcpCTag.Val, DhcpCTag.Len);
        u4SubnetMask = (UINT4) OSIX_NTOHL (u4SubnetMask);
    }
    else
    {
        /* Get the default Subnet mask */
        u4SubnetMask =
            (UINT4) DhcpCGetDefaultNetMask (pPktInfo->DhcpMsg.yiaddr);
    }

    if (NetIpv4GetCfaIfIndexFromPort (pPktInfo->u4IfIndex, &u4CfaIfIndex)
        == NETIPV4_FAILURE)
    {
        DHCPC_TRC1 (DEBUG_TRC,
                    "NetIpv4GetCfaIfIndexFromPort for index %d Failed",
                    pPktInfo->u4IfIndex);
        return;
    }

    if (DhcpCValidateIfIpAndMask (pPktInfo->DhcpMsg.yiaddr, u4SubnetMask)
        == DHCP_FAILURE)
    {
        DHCPC_TRC2 (DEBUG_TRC, "Offer for IP Address %x with NetMask %x "
                    "rejected due to invalid IP/Netmask \n",
                    pPktInfo->DhcpMsg.yiaddr, u4SubnetMask);
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;
        return;
    }

    if (DhcpCValidateClientMac (pPktInfo->u4IfIndex, pPktInfo->DhcpMsg.chaddr)
        == DHCP_FAILURE)
    {
        DHCPC_TRC2 (DEBUG_TRC, "Offer for IP Address %x with NetMask %x "
                    "rejected due to invalid Client Mac \n",
                    pPktInfo->DhcpMsg.yiaddr, u4SubnetMask);
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;
        return;
    }

    if (CfaIpIfValidateIfIpSubnet (u4CfaIfIndex,
                                   pPktInfo->DhcpMsg.yiaddr, u4SubnetMask)
        == CFA_FAILURE)
    {
        DHCPC_TRC2 (DEBUG_TRC, "Offer for IP Address %x with NetMask %x "
                    "rejected. Same subnet address shouldn't be allowed for "
                    "multiple interfaces\n", pPktInfo->DhcpMsg.yiaddr,
                    u4SubnetMask);
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;
        return;
    }

    if ((pTempBestOffer =
         (tBestOffer *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                        DhcpCBestOffPoolId)) == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "Memallocation Failure\n");
        return;
    }

    DhcpCGetReqOptSupported (pPktInfo, pDhcpCNode, pTempBestOffer);

    if ((pDhcpCNode->DhcIfInfo).pBestOffer == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "First Offer received\n");

        /* When there is no offer till now received, the received 
           offer is the best offer */
        (pDhcpCNode->DhcIfInfo).pBestOffer = (tBestOffer *) pTempBestOffer;

        MEMCPY (&((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo),
                pPktInfo, sizeof (tDhcpPktInfo));

        (pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.DhcpMsg.pOptions
            = (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                        DhcpCDhcpMsgOptPoolId);

        MEMCPY (((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.
                 DhcpMsg.pOptions), pPktInfo->DhcpMsg.pOptions,
                (sizeof (UINT1) * pPktInfo->u2Len));

        DhcpCRemoveOptionType (DHCP_OPT_MSG_TYPE,
                               &((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo));
        /* Check whether the single offer timer whether it is still 
           running or not */
        u1RetVal = (UINT1) DhcpCGetRemainingTime (DhcpCTimerListId,
                                                  &(pDhcpCNode->ReplyWaitTimer.
                                                    DhcpAppTimer), &u4TimeLeft);
        /* Stop the single offer timer if it is running */
        if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
        {
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));

        }
        /* Reset the retry count */
        pDhcpCNode->u1ReTxCount = 0;

        /* Multiple offer timer started */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_MULTIPLE_OFFER_TIMER_ID,
                            DHCPC_MULTIPLE_OFFER_TIME_OUT) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting MULTIPLE_OFFER_TIMER failed \r\n");
            DhcpCInitState (pDhcpCNode);
        }
    }
    else
    {                            /* when the best offer pointer is not null */

        /* When offer received has more options than the 
           best offer available in the global interface table , then 
           drop the available and take the received offer */

        /* The best offer is selected when the total no of options
         * requested are supported plus when the received offer has more option
         * other than the requested */

        DHCPC_TRC (DEBUG_TRC, "More Offers are rcvd\n");

        if ((pTempBestOffer->u1NoOfReqOptSupported >
             ((pDhcpCNode->DhcIfInfo).pBestOffer->u1NoOfReqOptSupported)) ||
            ((pTempBestOffer->u1NoOfReqOptSupported ==
              ((pDhcpCNode->DhcIfInfo).pBestOffer->u1NoOfReqOptSupported)) &&
             (pTempBestOffer->u1TotalNoOfOptionPresent >
              ((pDhcpCNode->DhcIfInfo).pBestOffer->u1TotalNoOfOptionPresent))))
        {
            DHCPC_TRC (DEBUG_TRC, "Better Offer rcvd\n");

            DhcpCFlushOffer (pDhcpCNode);

            (pDhcpCNode->DhcIfInfo).pBestOffer = (tBestOffer *) pTempBestOffer;

            MEMCPY (&((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo),
                    pPktInfo, sizeof (tDhcpPktInfo));

            (pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.DhcpMsg.pOptions
                = (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                            DhcpCDhcpMsgOptPoolId);

            MEMCPY (((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.
                     DhcpMsg.pOptions), pPktInfo->DhcpMsg.pOptions,
                    (sizeof (UINT1) * pPktInfo->u2Len));

            DhcpCRemoveOptionType (DHCP_OPT_MSG_TYPE,
                                   &(pDhcpCNode->DhcIfInfo.
                                     pBestOffer->PktInfo));
        }
        else
        {
            MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCBestOffPoolId,
                                (UINT1 *) pTempBestOffer);
            DHCPC_TRC (DEBUG_TRC,
                       "Offer rcvd. Not better than available offer\n");
        }
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting ProcessDhcpCOffer fn\n");
    return;
}                                /* End of ProcessDhcpCOffer function */

/************************************************************************/
/*  Function Name   : DhcpCGetReqOptSupported                           */
/*  Description     : This function gets the requested option supported */
/*                    and the total no.of options present in the recived*/
/*                    offer                                             */
/*  Input(s)        : pTempBestOffer-> Temporary best offer pointer     */
/*                    pPktInfo -> Pointer to the packet received info   */
/*                    pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None                                              */
/*  Returns         : pTempBestOffer->u1NoOfReqOptSupported - No of     */
/*                    requested options                                 */
/*                    pTempBestOffer->u1TotalNoOfOptionPresent -No of   */
/*                    requested options supported                       */
/************************************************************************/
VOID
DhcpCGetReqOptSupported (tDhcpPktInfo * pPktInfo,
                         tDhcpCRec * pDhcpCNode, tBestOffer * pTempBestOffer)
{
    UINT1              *pReqOptions = NULL;
    UINT1               u1NoOfReqOptSupported = 0;
    UINT1               u1Type = 0xFF;
    UINT2               u2DefaultOptLen = 0;
    UINT1               u1TotalNoOfOptionPresent = 0;
    UINT2               u2Offset = 0, u2Len = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCGetReqOptSupported fn\n");

    /* Get the option pointer which has been requested */

    pReqOptions = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions;
    u2DefaultOptLen = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions;

    if (pReqOptions != NULL)
    {
        while (u2Offset < u2DefaultOptLen)
        {
            u1Type = (UINT1) pReqOptions[u2Offset];    /* Type of the option */
            u2Len = (UINT2) pReqOptions[u2Offset + 1];    /* length of the option */

            /* Check whether the option is supported or not */
            if ((DhcpCGetOption (u1Type, pPktInfo)) == DHCP_FOUND)
                u1NoOfReqOptSupported++;

            u2Offset += u2Len + 2;    /* 2 is added for the length of tag and
                                       length of length in the packet */
        }

        pTempBestOffer->u1NoOfReqOptSupported = (UINT1) u1NoOfReqOptSupported;
    }

    /* Now find the total no of options present in the packet recvd */
    u2Offset = 0;

    pReqOptions = (UINT1 *) pPktInfo->DhcpMsg.pOptions;

    while (u2Offset < pPktInfo->u2Len)
    {
        u1Type = (UINT1) pReqOptions[u2Offset];    /* Type of the option */
        u2Len = (UINT2) pReqOptions[u2Offset + 1];    /* length of the option */

        if ((u1Type != DHCP_OPT_END) && (u1Type != DHCP_OPT_PAD) &&
            (u1Type != DHCP_OPT_MSG_TYPE))
            u1TotalNoOfOptionPresent++;

        u2Offset += u2Len + 2;    /* 2 is added for the length of tag and                                            length of length in the packet */
    }

    pTempBestOffer->u1TotalNoOfOptionPresent = u1TotalNoOfOptionPresent;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCGetReqOptSupported fn\n");
}                                /* End of DhcpCGetOptSupported Function */

/************************************************************************/
/*  Function Name   : DhcpCSendDecline                                  */
/*  Description     : This function BROADCASTS the DHCP_DECLINE message */
/*  Input(s)        : pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCSendDecline (tDhcpCRec * pDhcpCNode, UINT4 u4LeasedIpAddr, UINT4 u4DstIp)
{
    UINT4               u4Temp;
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr = NULL;
    UINT4               u4RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCSendDecline fn\n");

    if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
    {
        DHCPC_TRC (FAIL_TRC, "Packet Allocation FAILURE, DhcpCSendDecline\n");
        return DHCP_FAILURE;
    }

    pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;
    pOutPkt->u4DstIpAddr = u4DstIp;

    pOutHdr = &(pOutPkt->DhcpMsg);

    pOutHdr->Op = BOOTREQUEST;
    pOutHdr->htype = HWTYPE_ETHERNET;
    /* since a default interface alone is considered */

    pOutHdr->hlen = ENET_ADDR_LEN;
    pOutHdr->hops = 0;

    /* Fill the Transation ID in the packet and record the transaction ID in        the global Interface Table */

    pOutHdr->flags = DHCP_BROADCAST_MASK;    /* Broadcast the packet */

    pOutHdr->ciaddr = 0;
    pOutHdr->siaddr = 0;
    pOutHdr->yiaddr = 0;
    pOutHdr->giaddr = 0;

    DhcpCfaGddGetHwAddr (pDhcpCNode->u4IpIfIndex, pOutHdr->chaddr);
    DhcpCGetTransId (pOutHdr->chaddr, &pOutHdr->xid);

    MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
    MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);

    /* Add Requested IP Address option */
    DhcpCTag.Type = DHCP_OPT_REQUESTED_IP;
    DhcpCTag.Len = 4;            /* Requested IP Addr option length */
    u4Temp = OSIX_HTONL (u4LeasedIpAddr);
    DhcpCTag.Val = (UINT1 *) &u4Temp;
    DhcpCAddOption (pOutPkt, &DhcpCTag);

    /* Add server identifier option */
    DhcpCTag.Type = DHCP_OPT_SERVER_ID;
    DhcpCTag.Len = 4;            /* Server Identifier option length */
    u4Temp = OSIX_HTONL (u4DstIp);
    DhcpCTag.Val = (UINT1 *) &u4Temp;
    DhcpCAddOption (pOutPkt, &DhcpCTag);

    u4RetVal = DhcpCSendMessage (pOutPkt, DHCP_DECLINE);

    if (u4RetVal == DHCP_SUCCESS)
    {
        DHCPC_TRC1 (EVENT_TRC, "DECLINE Msg is successfully sent in Intf %x\n",
                    pDhcpCNode->u4IpIfIndex);
        /* Increment the statistics */
        (pDhcpCNode->TxRxStats).u4DhcpCDeclineSent++;
    }
    else
    {
        DHCPC_TRC1 (FAIL_TRC, "Unable to send the DECLINE Msg in Intf %x\n",
                    pDhcpCNode->u4IpIfIndex);
    }

    /* free OutPkt Message Structure */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                        (UINT1 *) pOutPkt);

    DHCPC_TRC (DEBUG_TRC, "Exited DhcpCSendDecline fn\n");

    return (UINT1) u4RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpCSendDiscover                                 */
/*  Description     : This function broadcasts the DHCP_DISCOVER message*/
/*  Input(s)        : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCSendDiscover (tDhcpCRec * pDhcpCNode)
{
    tDhcpPktInfo       *pOutPkt;
    UINT2               u2OptionLen;
    tDhcpMsgHdr        *pOutHdr;
    UINT4               u4CurrentTime;
    UINT1              *pReqOptions;
    UINT1               u1RetVal;
    UINT4               u4Xid;
    UINT4               u4CfaIfIndex;
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCSendDiscover fn\n");
    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                      &u4CfaIfIndex) != NETIPV4_SUCCESS)
    {
        return DHCP_FAILURE;
    }

    if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
    {
        DHCPC_TRC (FAIL_TRC, "Packet Allocation FAILURE, DhcpCSendDiscover\n");
        return DHCP_FAILURE;
    }

    pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;
    /* fill the fixed header */
    pOutHdr = &(pOutPkt->DhcpMsg);

    pOutHdr->Op = BOOTREQUEST;
    pOutHdr->htype = HWTYPE_ETHERNET;    /* since a default interface alone is                                              considered */
    pOutHdr->hlen = ENET_ADDR_LEN;
    pOutHdr->hops = 0;

/* Fill the secs field in the packet and record the secs in the global 
   Interface Table */
    OsixGetSysTime (&u4CurrentTime);
    u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    (pDhcpCNode->DhcIfInfo).u4Secs = u4CurrentTime;
    pOutHdr->secs = (UINT2) u4CurrentTime;
    pOutHdr->flags = DHCP_BROADCAST_MASK;    /* Broad cast the packet */

    pOutHdr->ciaddr = 0;
    pOutHdr->siaddr = 0;
    pOutHdr->yiaddr = 0;
    pOutHdr->giaddr = 0;

    DhcpCfaGddGetHwAddr (pDhcpCNode->u4IpIfIndex, pOutHdr->chaddr);

    /* Fill the Transation ID in the packet and record the transaction ID in        the global Interface Table */
    DhcpCGetTransId (pOutHdr->chaddr, &u4Xid);

    pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid = u4Xid;

    MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
    MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);

    /* Requesting of DHCP Default options */
    DhcpCopyDefaultOpt (pDhcpCNode);

    pReqOptions = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions;

    u2OptionLen = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions;

    DhcpCFillRequiredOptions (pOutPkt, pReqOptions, u2OptionLen);

    /* ADD OPTION 60 if it is available for this interface */
    pDhcpCReqOption = (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                                DHCP_OPT_CVENDOR_SPECIFIC);
    if (pDhcpCReqOption != NULL)
    {
        DhcpCTag.Type = DHCP_OPT_CVENDOR_SPECIFIC;
        DhcpCTag.Len = (INT4) pDhcpCReqOption->u1Len;
        DhcpCTag.Val = pDhcpCReqOption->au1OptionVal;
        DhcpCAddOption (pOutPkt, &DhcpCTag);
    }
    u1RetVal = (UINT1) DhcpCSendMessage (pOutPkt, DHCP_DISCOVER);

    if (u1RetVal == DHCP_SUCCESS)
    {
        /* DHCPC_TRC : */
        DHCPC_TRC1 (DEBUG_TRC, "DISCOVER Msg is sent in Intf %x\n",
                    pOutPkt->u4IfIndex);

        /* Increment the statistics */
        (pDhcpCNode->TxRxStats).u4DhcpCDiscoverSent++;

        (pDhcpCNode->DhcIfInfo).u1State = S_SELECTING;

        /* Start a single offer timer within which a DHCP_OFFER should 
           have received */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_SINGLE_OFFER_TIMER_ID,
                            gDhcpCFastAcc.u4FastAccDisTimeOut) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting SINGLE_OFFER_TIMER failed \r\n");
            DhcpCInitState (pDhcpCNode);
        }
    }
    else
    {
        DHCPC_TRC1 (FAIL_TRC, "DHCP_DISCOVER Send Msg FAILURE in Intf %x\n",
                    pOutPkt->u4IfIndex);
    }

    /* free OutPkt Message Structure */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                        (UINT1 *) pOutPkt);

    DHCPC_TRC (DEBUG_TRC, "Exited DhcpCSendDiscover fn\n");

    return u1RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpCFillRequiredOptions                          */
/*  Description     : This function broadcasts the DHCP_DISCOVER message*/
/*  Input(s)        : pOutPkt - Pointer to the outgoing packet          */
/*                    pSrcOpt - Pointer to the Source Options           */
/*                    u4OptLen- Option Length to be copied to pOutPkt   */
/*  Output(s)       : pOutPkt - Contains the pOptions copied            */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpCFillRequiredOptions (tDhcpPktInfo * pOutPkt, UINT1 *pSrcOpt,
                          UINT2 u2OptionLen)
{
    UINT1               u2Len;

    UINT1              *pDstOpt = pOutPkt->DhcpMsg.pOptions;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCFillRequiredOptions fn\n");

    if (pSrcOpt == NULL || pDstOpt == NULL)
    {
        DHCPC_TRC (DEBUG_TRC, "Required Option is NULL\n");
        return;
    }

    /* Copy the contents of pSrcOpt to the outgoing packet Options field */
    for (u2Len = 0; u2Len <= u2OptionLen; u2Len++)
    {
        *pDstOpt = *pSrcOpt;    /* Copy the contents */

        pDstOpt++;                /* Increment the Dest. Option Pointer */
        pSrcOpt++;
    }

    pOutPkt->u2Len += u2OptionLen;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCFillRequiredOptions fn\n");

}                                /* End of DhcpCFillRequiredOptions functions */

/************************************************************************/
/*  Function Name   : DhcpAddOption                                     */
/*  Description     : Add DHCP option to the outgoing packet. Byte order*/
/*                    conversion is not done here                       */
/*  Input(s)        : pOutPkt - Local structure for DHCP response       */
/*                    pTag - pointer to the option structure to be added*/
/*  Output(s)       : pOutPkt                                           */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpCAddOption (tDhcpPktInfo * pOutPkt, tDhcpCTag * pTag)
{
    tDhcpMsgHdr        *pDhcpMsg;
    UINT1              *pu1TempOptBuff = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCAddOption fn\n");

    /* Check whether the option is present already.if yes return . */

    if (DhcpCGetOption (pTag->Type, pOutPkt) == DHCP_FOUND)
    {
        DHCPC_TRC (FAIL_TRC, "Already the option is present\n");
        return;
    }
    pDhcpMsg = &(pOutPkt->DhcpMsg);

    /* Compare the length of the option with available options field
     * length. 7 is the extra length for messagetype option (3), overload
     * option (3) and the end option (1)*/
    if (pOutPkt->u2Len + pTag->Len + DHCP_LEN_TYPE_LEN + 7 >
        pOutPkt->u2MaxMessageSize)
    {
        if (STRLEN (pDhcpMsg->file) == 0)
        {
            if (pTag->Len + DHCP_LEN_TYPE_LEN + 1 < 128)
            {
                pDhcpMsg->file[0] = pTag->Type;
                pDhcpMsg->file[1] = pTag->Len;
                MEMCPY (pDhcpMsg->file + 2, pTag->Val,
                        MEM_MAX_BYTES (pTag->Len, 126));
            }
            if (pTag->Len < 128)
                pDhcpMsg->file[pTag->Len] = DHCP_OPT_END;
            pOutPkt->u1OverLoad |= 0x01;
        }
        else if (STRLEN (pDhcpMsg->sname) == 0)
        {
            if (pTag->Len + DHCP_LEN_TYPE_LEN + 1 < 64)
            {
                pDhcpMsg->sname[0] = pTag->Type;
                pDhcpMsg->sname[1] = pTag->Len;
                MEMCPY (pDhcpMsg->sname + 2, pTag->Val,
                        MEM_MAX_BYTES (pTag->Len, 62));
            }
            if (pTag->Len < 128)
                pDhcpMsg->file[pTag->Len] = DHCP_OPT_END;
            pOutPkt->u1OverLoad |= 0x02;

        }
        else
            return;
    }

    if (pOutPkt->u2Len + pTag->Len + DHCP_LEN_TYPE_LEN > pOutPkt->u2BufLen)
    {
        UINT2               u2NewLen;
        u2NewLen = (UINT2) (pOutPkt->u2BufLen + pTag->Len + DHCP_LEN_TYPE_LEN);
        pu1TempOptBuff =
            (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                      DhcpCDhcpMsgOptPoolId);

        if (pu1TempOptBuff == NULL)
        {
            DHCPC_TRC (DEBUG_TRC, "\n Memory Allocation is failed !! \n");
            return;
        }

        MEMCPY (pu1TempOptBuff, pDhcpMsg->pOptions, pOutPkt->u2BufLen);

        /* free the previously allocated buffer */
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                            (UINT1 *) pDhcpMsg->pOptions);

        /* assign the newly allocated buffer to the DHCP options list
         */
        pDhcpMsg->pOptions = pu1TempOptBuff;

        pOutPkt->u2BufLen = u2NewLen;
    }

    if (pOutPkt->u2Len >= DHCP_DEF_MAX_MSGLEN)
        return;
    /*Add the Option type */
    *(pDhcpMsg->pOptions + pOutPkt->u2Len) = pTag->Type;

    /* if it is PAD or END Option return here */
    if ((pTag->Type == DHCP_OPT_PAD) || (pTag->Type == DHCP_OPT_END))
    {
        return;
    }

    if ((pOutPkt->u2Len + 1) >= DHCP_DEF_MAX_MSGLEN)
        return;
    /*Add the Option Length */
    *(pDhcpMsg->pOptions + pOutPkt->u2Len + 1) = pTag->Len;

    if (pTag->Len)
        MEMCPY ((pDhcpMsg->pOptions + pOutPkt->u2Len + DHCP_LEN_TYPE_LEN),
                pTag->Val, pTag->Len);

    /* Length is incremented by option type field (1 byte ) +
     * option length field (1 byte) +  option length 
     */

    pOutPkt->u2Len += pTag->Len + DHCP_LEN_TYPE_LEN;

    DHCPC_TRC (DEBUG_TRC, "\n Exiting DhcpCAddOption fn \n");
    return;
}                                /* End of DhcpCAddOption function */

/************************************************************************/
/*  Function Name   : DhcpCGetOption                                    */
/*  Description     : This function checks whether the option is        */
/*                  : present. If it is present DhcpCTag.Val will       */
/*                    points to th eoption value. Network to Host byte  */
/*                    order conversion is not done for the option value */
/*  Input(s)        : u1TagType - Option type                           */
/*                    pPktInfo - Local structure containing information */
/*                               about received packet                  */
/*  Output(s)       : DhcpCTag ( Global )                               */
/*  Returns         : DHCP_FOUND or DHCP_NOT_FOUND                      */
/************************************************************************/
INT4
DhcpCGetOption (UINT1 u1TagType, tDhcpPktInfo * pPkt)
{
    UINT1              *pOptions;
    UINT2               Offset = 0;    /* points to the options */
    UINT1               u1Type, u1Len, u1OptOverLoad = 0;
    UINT1               u1TempLen = 0;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCGetOption fn\n");

    pOptions = pPkt->DhcpMsg.pOptions;

    while (Offset < pPkt->u2Len)
    {
        u1Type = pOptions[Offset];    /* Type of the option */
        u1Len = pOptions[Offset + 1];    /* length of the option */

        if (u1TagType == u1Type)
        {
            DhcpCTag.Len = u1Len;
            DhcpCTag.Val = pOptions + Offset + 2;    /* option value pointer */

            DHCPC_TRC1 (DEBUG_TRC,
                        "Exiting DhcpCGetOption fn. Option %d Found\n",
                        u1TagType);
            return DHCP_FOUND;
        }
        else
        {
            if (u1Type == DHCP_OPT_PAD)
            {
                ++Offset;
                continue;        /* ignore PAD options */
            }

            if (u1Type == DHCP_OPT_END)
                break;            /* no more options after end_opt */

            if (u1Type == DHCP_OPT_OVERLOAD)
            {

                u1OptOverLoad = pOptions[Offset + DHCP_LEN_TYPE_LEN];
                /*  <L2L3Switch_P03>:<31/05/2002>  */
                if (u1OptOverLoad == 0 || u1OptOverLoad > 3)
                {

                    DHCPC_TRC1 (DEBUG_TRC,
                                "Exiting DhcpCGetOption fn. Option %d not found\n",
                                u1TagType);
                    return DHCP_NOT_FOUND;
                }
            }

            /* Take the next option */
            Offset += u1Len + DHCP_LEN_TYPE_LEN;
        }

    }
/******************************************************************************/
/*  <L2L3Switch_P03>:<31/05/2002>                                              */
/*  if (u1OptOverLoad == 0 || u1OptOverLoad > 3)                              */
/*  {                                                                         */
/*      DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCGetOption fn. Option not found\n");*/
/*      return DHCP_NOT_FOUND;                                                */
/*  }                                                                         */
/*  This check has been moved above since this check should be made when      */
/*  u1Type matches the DHCP_OPT_OVERLOAD option.                              */
/******************************************************************************/
    if (u1OptOverLoad == 1)
    {
        u1TempLen = 128;
        pOptions = pPkt->DhcpMsg.file;
    }
    else if (u1OptOverLoad == 2)
    {
        u1TempLen = 64;
        pOptions = pPkt->DhcpMsg.sname;
    }
    else if (u1OptOverLoad == 3)
    {
        u1TempLen = 128 + 64;
        pOptions = pPkt->DhcpMsg.sname;

    }

    Offset = 0;
    while (Offset < u1TempLen)
    {
        u1Type = pOptions[Offset];    /* Type of the option */
        u1Len = pOptions[Offset + 1];    /* length of the option */

        if (u1TagType == u1Type)
        {
            DhcpCTag.Len = u1Len;
            DhcpCTag.Val = pOptions + Offset + 2;    /* option value pointer */

            DHCPC_TRC1 (DEBUG_TRC,
                        "Exiting DhcpCGetOption fn. Option %d Found\n",
                        u1TagType);
            return DHCP_FOUND;
        }
        else
        {
            if (u1Type == DHCP_OPT_PAD)
            {
                ++Offset;
                continue;        /* ignore PAD options */
            }

            if (u1Type == DHCP_OPT_END)
            {
                if (u1OptOverLoad == 3)
                {
                    pOptions = pPkt->DhcpMsg.file;
                    u1TempLen = 128;
                    u1OptOverLoad = 1;
                }
                else
                    break;
            }
            /* Take the next option */
            Offset += u1Len + DHCP_LEN_TYPE_LEN;
        }
    }

    DHCPC_TRC1 (DEBUG_TRC, "Exiting DhcpCGetOption fn. Option %d not found\n",
                u1TagType);

    return DHCP_NOT_FOUND;
}                                /* End of DhcpCGetOption function */

/************************************************************************/
/*  Function Name   : DhcpCCheckOptionIsPresent                         */
/*  Description     : This function will check whether the requested    */
/*                    option is present in the recevied Offer           */
/*  Input(s)        : tDhcpPktInfo - Local Stricture containing packet  */
/*                                   information                        */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpCCheckOptionIsPresent (tDhcpPktInfo * pPkt)
{
    INT4                ai4OptionBitMask[DHCP_OPTION_BLOCKS];
    tDhcpDiscOption    *pDhcpDiscOptionNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    INT1                iCnt = 0;
    INT1                ai1Data[DHCP_OPTION_BLOCKS * DHCP_OPTION_BLOCK_SIZE] =
        { 0 };

    MEMSET (ai4OptionBitMask, 0, (sizeof (INT4) * DHCP_OPTION_BLOCKS));
    MEMSET (ai1Data, 0, sizeof (ai1Data));

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCCheckOptionIsPresent fn\n");

    for (iCnt = 0; iCnt < gRegDhcpDiscOption.i1OptionCnt; iCnt++)
    {
        if (DHCP_FOUND !=
            DhcpCGetOption ((UINT1) gRegDhcpDiscOption.au1Data[iCnt], pPkt))
        {
            continue;
        }

        pNode = TMO_SLL_First (&gDhcpDiscOptionList);
        TMO_SLL_Scan (&gDhcpDiscOptionList, pNode, tTMO_SLL_NODE *)
        {
            pDhcpDiscOptionNode = (tDhcpDiscOption *) pNode;
            if (DHCP_FOUND ==
                DhcpClientUtilCheckOption (gRegDhcpDiscOption.au1Data[iCnt],
                                           pDhcpDiscOptionNode))
            {
                (pDhcpDiscOptionNode->pDhcDiscoveryCbFn) (DhcpCTag.Len,
                                                          DhcpCTag.Val,
                                                          OFFER_RECEVIED_WITH_OPTION);
            }
        }
    }

    pNode = TMO_SLL_First (&gDhcpDiscOptionList);
    TMO_SLL_Scan (&gDhcpDiscOptionList, pNode, tTMO_SLL_NODE *)
    {
        pDhcpDiscOptionNode = (tDhcpDiscOption *) pNode;
        (pDhcpDiscOptionNode->pDhcDiscoveryCbFn) (0, 0, OFFER_COMPLETED);
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCSendInform                                   */
/*  Description     : This function BROADCASTS the DHCP_INFORM message  */
/*  Input(s)        : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT1
DhcpCSendInform (tDhcpCRec * pDhcpCNode)
{
    tDhcpPktInfo       *pOutPkt = NULL;
    UINT4               u4CurrentTime = 0;
    tDhcpMsgHdr        *pOutHdr = NULL;
    UINT1              *pReqOptions = NULL;
    UINT2               u2OptionLen = 0;
    INT1                i1RetVal;
    UINT4               u4TimeLeft = 0;
    UINT1               u1TimerRetVal;
    UINT4               u4Xid;
    UINT1               u1State;
    UINT4               u4CfaIfIndex;
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCSendInform fn \n");

    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                      &u4CfaIfIndex) != NETIPV4_SUCCESS)
    {
        return DHCP_FAILURE;
    }

    if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
    {
        DHCPC_TRC (FAIL_TRC, "Packet Allocation FAILURE, DhcpCSendInform\n");
        return DHCP_FAILURE;
    }

    pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;
    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    pOutHdr = &(pOutPkt->DhcpMsg);

    pOutHdr->Op = BOOTREQUEST;
    pOutHdr->htype = HWTYPE_ETHERNET;    /* since a default interface alone is                          considered */
    pOutHdr->hlen = ENET_ADDR_LEN;
    pOutHdr->hops = 0;

    /* Fill the Transation ID in the packet and record the transaction ID in        the global Interface Table */

    /* Fill the secs field in the packet and record the secs in the global 
       Interface Table */
    DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

    (pDhcpCNode->DhcIfInfo).u4Secs = u4CurrentTime;
    pOutHdr->secs = (UINT2) u4CurrentTime;

    pOutHdr->flags = DHCP_BROADCAST_MASK;    /* Broadcast the packet */

    if ((u1State == S_RENEWING) || (u1State == S_BOUND))
    {
        /* Should fill the destination IP address of the server,
           since it is going to be a unicast packet */
        pOutPkt->u4DstIpAddr = (pDhcpCNode->DhcIfInfo).u4ServerOfferred;
        pOutHdr->flags = DHCP_UNICAST_MASK;
    }
    pOutHdr->ciaddr = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;

    pOutHdr->siaddr = 0;
    pOutHdr->yiaddr = 0;
    pOutHdr->giaddr = 0;

    DhcpCfaGddGetHwAddr (pOutPkt->u4IfIndex, pOutHdr->chaddr);
    DhcpCGetTransId (pOutHdr->chaddr, &u4Xid);
    pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid = u4Xid;

    MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
    MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);

    pReqOptions = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions;

    u2OptionLen = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions;

    DhcpCFillRequiredOptions (pOutPkt, pReqOptions, u2OptionLen);

    pDhcpCReqOption = (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                                DHCP_OPT_CVENDOR_SPECIFIC);
    if (pDhcpCReqOption != NULL)
    {
        DhcpCTag.Type = DHCP_OPT_CVENDOR_SPECIFIC;
        DhcpCTag.Len = (INT4) pDhcpCReqOption->u1Len;
        DhcpCTag.Val = pDhcpCReqOption->au1OptionVal;
        DhcpCAddOption (pOutPkt, &DhcpCTag);
    }
    DhcpCRemoveOptionType (DHCP_OPT_SERVER_ID, pOutPkt);
    DhcpCRemoveOptionType (DHCP_OPT_REQUESTED_IP, pOutPkt);
    DhcpCRemoveOptionType (DHCP_OPT_LEASE_TIME, pOutPkt);

    i1RetVal = (INT1) DhcpCSendMessage (pOutPkt, DHCP_INFORM);

    if (i1RetVal == DHCP_SUCCESS)
    {
        DHCPC_TRC1 (EVENT_TRC, "INFORM Msg Sent in Intf %x\n",
                    pOutPkt->u4IfIndex);

        /* Set the global flag which is used to indicate that a inform 
           message is sent. This is used to differentiate DHCP_ACK, DHCP_NACK 
           message is received from the server for DHCP_INFORM message or any 
           other message */
        pDhcpCNode->u1DhcpCInformMsgSent = DHCPC_INFORM_MESSAGE_SENT;

        /* Increment the statistics */
        (pDhcpCNode->TxRxStats).u4DhcpCInformSent++;

        /* Check whether the any request timer whether it is still 
           running or not */
        u1TimerRetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                               &(pDhcpCNode->ReplyWaitTimer.
                                                 DhcpAppTimer), &u4TimeLeft);
        /* Stop the timer if it is running */
        if ((u1TimerRetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
        {
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
        }

        /* Reset the retry count */
        pDhcpCNode->u1ReTxCount = 0;

        /* Stop the Timers which are used for DHCP Client */
        u1TimerRetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                               &(pDhcpCNode->LeaseTimer.
                                                 DhcpAppTimer), &u4TimeLeft);

        if ((u1TimerRetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            /* Stop the Lease timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        u1TimerRetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                               &(pDhcpCNode->ArpCheckTimer.
                                                 DhcpAppTimer), &u4TimeLeft);

        if ((u1TimerRetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            /* Stop the Arp Check timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer));

        /* Reply wait timer started to recieve an ACK */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_INFORM_MSG_TIMER_ID,
                            DHCPC_INFORM_MSG_TIME_OUT) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting DHCPC_INFORM_MSG_TIMER failed \r\n");
        }
    }
    else
    {
        DHCPC_TRC1 (FAIL_TRC, "INFORM Msg send FAILURE in Intf %x\n",
                    pOutPkt->u4IfIndex);
    }
    /* free OutPkt Message Structure */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                        (UINT1 *) pOutPkt);

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCSendInformMsg fn \n");

    return i1RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpCSendReleaseMsg                               */
/*  Description     : This function UNICASTS the DHCP_RELEASE message   */
/*  Input(s)        : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS or FAILURE                                */
/************************************************************************/
INT1
DhcpCSendReleaseMsg (tDhcpCRec * pDhcpCNode)
{
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr = NULL;
    tBestOffer         *pBestOffer = NULL;
    INT1                i1RetVal;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCSendReleaseMsg fn\n");

    if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
    {
        DHCPC_TRC (FAIL_TRC,
                   "Packet Allocation FAILURE, DhcpCSendReleaseMsg\n");
        return FAILURE;
    }

    pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;
    pOutPkt->u4DstIpAddr = (pDhcpCNode->DhcIfInfo).u4ServerOfferred;
    pBestOffer = (pDhcpCNode->DhcIfInfo).pBestOffer;

    pOutHdr = &(pOutPkt->DhcpMsg);

    pOutHdr->Op = BOOTREQUEST;
    pOutHdr->htype = HWTYPE_ETHERNET;    /* since a default interface alone is                          considered */
    pOutHdr->hlen = ENET_ADDR_LEN;
    pOutHdr->hops = 0;

    /* Fill the Transation ID in the packet and record the transaction ID in        the global Interface Table */

    pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid;

    pOutHdr->flags = DHCP_UNICAST_MASK;    /* Unicast the packet */

    pOutHdr->ciaddr = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;
    pOutHdr->siaddr = 0;
    pOutHdr->yiaddr = 0;
    pOutHdr->giaddr = 0;

    DhcpCfaGddGetHwAddr (pOutPkt->u4IfIndex, pOutHdr->chaddr);

    MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
    MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);

    if (pBestOffer != NULL)
    {
        DhcpCFillRequiredOptions (pOutPkt,
                                  pBestOffer->PktInfo.DhcpMsg.pOptions,
                                  pBestOffer->PktInfo.u2Len);
    }

    i1RetVal = (INT1) DhcpCSendMessage (pOutPkt, DHCP_RELEASE);

    if (i1RetVal == DHCP_SUCCESS)
    {

        /* Increment the statistics */
        (pDhcpCNode->TxRxStats).u4DhcpCReleaseSent++;
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;
        (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = DHCPC_RESET;
        (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = DHCPC_RESET;

        (pDhcpCNode->DhcIfInfo).u1State = S_UNUSED;

        /* Now bound the IP address to DHCPC_AUTO_CONFIG_IP_ADDRESS */
        (pDhcpCNode->ReleaseWaitTimer).u4IfIndex = pDhcpCNode->u4IpIfIndex;
        (pDhcpCNode->ReleaseWaitTimer).u4TimerId = DHCPC_RELEASE_MSG_TIMER_ID;
        (pDhcpCNode->ReleaseWaitTimer).pEntry = (VOID *) pDhcpCNode;

        i1RetVal = (UINT1) DhcpCStartTimer (DhcpCTimerListId,
                                            &(pDhcpCNode->ReleaseWaitTimer.
                                              DhcpAppTimer),
                                            DHCPC_RELEASE_TIME_OUT);

        if (i1RetVal == DHCP_FAILURE)
        {
            /* free OutPkt Message Structure */
            MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                                (UINT1 *) pOutPkt->DhcpMsg.pOptions);
            MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                                (UINT1 *) pOutPkt);
            return DHCP_FAILURE;
        }
    }
    else
    {
        DHCPC_TRC1 (FAIL_TRC, "RELEASE Msg Send FAILURE in Intf %x\n",
                    pOutPkt->u4IfIndex);
    }

    /* free OutPkt Message Structure */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                        (UINT1 *) pOutPkt);
    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCSendReleaseMsg fn\n");

    return i1RetVal;
}

/************************************************************************/
/*  Function Name   : CalRenewRebindTime                                */
/*  Description     : Calculates the Renewal, RebindTime                */
/*  Input(s)        : u4Secs -> IP Address Acquisition start time       */
/*                    u4LeaseTime -> Lease Time Offfered                */
/*                    pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None                                              */
/*  Returns         : State of Interface to which the interface should  */
/*                    be transitioned                                   */
/************************************************************************/
UINT1
CalRenewRebindTime (tDhcpCRec * pDhcpCNode, UINT4 u4Secs, UINT4 u4LeaseTime)
{

    UINT4               u4CurrentTime;
    UINT4               u4RemainingTime = 0;
    UINT4               u4AvailRenewTime;
    INT4                i4CurrentRenewTime;
    INT4                i4CurrentRebindTime;
    INT4                i4RemainingPeriod;
    INT4                i4AvailRebindTime;
    UINT1               u1RetVal;

    /* Get the current time in secs */
    OsixGetSysTime (&u4CurrentTime);
    u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    /* 
       Remaining time period of lease = Time of Expiration of Lease - Current time     Time Of Expiration of Lease    = u4Secs + u4LeaseTime 
     */

    /* Here Remaining time period of lease should be greater than 0 
       for calculating the renewal and rebind time period */

    i4RemainingPeriod = u4Secs + u4LeaseTime - u4CurrentTime;

    if (i4RemainingPeriod > 0)
    {
        /* As per RFC 2131 which says that the renewal time is 0.5 
           of Lease */
        i4CurrentRenewTime = (INT4) (u4Secs + (u4LeaseTime * 0.5));

        u4AvailRenewTime = (UINT4) i4CurrentRenewTime - u4CurrentTime;
        /* Current Renew Lease time should be greater than 0 */
        if (u4AvailRenewTime > 0)
        {
            pDhcpCNode->u2RenewTimerCount =
                (UINT2) (u4AvailRenewTime / DHCPC_MAX_TIME_OUT);
            if (pDhcpCNode->u2RenewTimerCount != 0)
            {
                /*Fsap Timer supports only for 7 days, if renew time is more than 
                 * 7 days, u2RenewTimerCount is used. u2RenewTimerCount is 
                 * (Configured Val/ Max Fsap supported value). On timer expiry u2RenewTimerCount will
                 * be decremented and the corresponding action for the timer expiry will be done
                 * only when the u2RenewTimerCount becomes zero, until then the timer will 
                 * be restarted.*/

                u4RemainingTime = u4AvailRenewTime % DHCPC_MAX_TIME_OUT;
                if (u4RemainingTime == 0)
                {
                    u4AvailRenewTime = DHCPC_MAX_TIME_OUT;
                    pDhcpCNode->u2RenewTimerCount =
                        (UINT2) (pDhcpCNode->u2RenewTimerCount - 1);
                }
                else
                {
                    u4AvailRenewTime = u4RemainingTime;
                }
            }
            /* Start a timer with available renew time */
            if (DhcpCInitTimer (pDhcpCNode, DHCPC_RENEWAL_TIMER_ID,
                                u4AvailRenewTime) == DHCP_FAILURE)
            {
                DHCPC_TRC (FAIL_TRC, "Starting RENEWAL_TIMER failed \r\n");
                return (S_INIT);
            }
            return (S_BOUND);    /* Return the function with the state of the 
                                   Interface */
        }
        else
            /* This case is Lease time has past the renewal time but 
               less than the total lease time offerred */
        {
            /* As per RFC 2131 which says that the rebind time is 0.875 
               of Lease */
            i4CurrentRebindTime = (INT4) (u4Secs + (u4LeaseTime * 0.875));

            /* Currently available Rebind Time */
            i4AvailRebindTime = i4CurrentRebindTime - u4CurrentTime;

            if (i4AvailRebindTime > 0)
            {
                /* Start a timer with available rebind time */
                pDhcpCNode->u2RenewTimerCount =
                    (UINT2) (i4AvailRebindTime / DHCPC_MAX_TIME_OUT);
                if (pDhcpCNode->u2RenewTimerCount != 0)
                {
                    /*Fsap Timer supports only for 7 days, if renew time is more than 
                     * 7 days, u2RenewTimerCount is used. u2RenewTimerCount is 
                     * (Configured Val/ Max Fsap supported value). On timer expiry u2RenewTimerCount will
                     * be decremented and the corresponding action for the timer expiry will be done
                     * only when the u2RenewTimerCount becomes zero, until then the timer will 
                     * be restarted.*/

                    u4RemainingTime =
                        (UINT4) i4AvailRebindTime % DHCPC_MAX_TIME_OUT;
                    if (u4RemainingTime == 0)
                    {
                        i4AvailRebindTime = DHCPC_MAX_TIME_OUT;
                        pDhcpCNode->u2RenewTimerCount =
                            (UINT2) (pDhcpCNode->u2RenewTimerCount - 1);
                    }
                    else
                    {
                        i4AvailRebindTime = (UINT4) u4RemainingTime;
                    }
                }

                if (DhcpCInitTimer (pDhcpCNode, DHCPC_REBIND_TIMER_ID,
                                    (UINT4) i4AvailRebindTime) == DHCP_FAILURE)
                {
                    DHCPC_TRC (FAIL_TRC, "Starting REBIND_TIMER failed \r\n");
                    return (S_INIT);
                }

                /* Send a DHCP_REQUEST to the leasing server to 
                   extend the lease time available */
                u1RetVal = DhcpCSendRequest (pDhcpCNode);

                if (u1RetVal == DHCP_FAILURE)
                {
                    DHCPC_TRC1 (FAIL_TRC,
                                "REQUEST FAILURE in CalRenewRebindTime fn for %x intf\n",
                                pDhcpCNode->u4IpIfIndex);

                    return (S_INIT);
                }

                return (S_RENEWING);    /* Go to RENEWING state and wait for 
                                           extension of the lease time from 
                                           the server in the DHCP_ACK msg */
            }
            else
                return (S_INIT);
        }
    }                            /* Remainging Lease time has already expired */
    else
    {
        return (S_INIT);
    }
}                                /* End of CalRenewRebindTime function */

/************************************************************************/
/*  Function Name   : ProcessDhcpCNAck                                  */
/*  Description     : This function processes the DHCP_NACK msg rcvd    */
/*  Input(s)        : pPktInfo -> Pointer to the packet information rcvd*/
/*                    pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ProcessDhcpCNAck (tDhcpPktInfo * pPktInfo, tDhcpCRec * pDhcpCNode)
{
    UINT4               u4TimeLeft = 0;
    UINT1               u1RetVal;
    UINT1               u1State;

    DHCPC_TRC (DEBUG_TRC, "Entering ProcessDhcpCNAck fn\n");

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    /* Fill the statistics depending on the state of the interface */
    if (u1State == S_REQUESTING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREQ++;
    }
    else if (u1State == S_RENEWING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInRENEW++;
    }
    else if (u1State == S_REBINDING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREBIND++;
    }
    else if (u1State == S_REBOOTING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREBOOT++;
    }

    if (DhcpCValidateClientMac (pPktInfo->u4IfIndex, pPktInfo->DhcpMsg.chaddr)
        == DHCP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "Packet dropped  due to invalid Client Mac \n");
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;
        return;
    }

    if ((u1State == S_REQUESTING)
        || (u1State == S_REBOOTING)
        || (u1State == S_REBINDING)
        || (u1State == S_RENEWING)
        || (pDhcpCNode->u1DhcpCInformMsgSent == DHCPC_INFORM_MESSAGE_SENT))
    {
        if (pDhcpCNode->u4ProcessedAck == ACK_MESSAGE_RECIEVED)
        {

            DHCPC_TRC (DEBUG_TRC, "Ack Msg rcvd Before. NACK Rejected\n");
            return;
        }
        /* Check whether the request timer is still running or not */
        u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->ReplyWaitTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
        {
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));

        }

        /* Check whether the rebind timer is running or not */
        u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->LeaseTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            /* Stop the Rebind timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        /* Check whether the Arp timer is running or not */
        u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->ArpCheckTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            /* Stop the Arp timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ArpCheckTimer.DhcpAppTimer));

        /* Reset the retry count */
        pDhcpCNode->u1ReTxCount = 0;
        pDhcpCNode->u1DhcpCInformMsgSent = DHCPC_RESET;

        DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);

        (pDhcpCNode->DhcIfInfo).u1State = S_INIT;

        /* Intimate DHCP standby peers about the deletion of binding */
        DhCRedSendDynamicClientInfo (pDhcpCNode);

        DhcpCStartProcessing (pPktInfo->u4IfIndex);

        DHCPC_TRC (DEBUG_TRC, "Sent DHCP_CLIENT_START_PROCESSING Event\n");

    }
    else
    {
        DHCPC_TRC2 (FAIL_TRC,
                    "NACK msg rcvd in wrong state for %x intf in %d state",
                    pPktInfo->u4IfIndex, (u1State));
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting ProcessDhcpCNAck fn\n");

    return;
}                                /* End of ProcessDhcpCNACK function */

/************************************************************************/
/*  Function Name   : DhcpCSendRequest                                  */
/*  Description     : This function sends the DHCP_REQUEST message      */
/*  Input(s)        : pDhcpCNode - DHCPC Interface Record               */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS or FAILURE                                */
/************************************************************************/
UINT1
DhcpCSendRequest (tDhcpCRec * pDhcpCNode)
{
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr = NULL;
    tBestOffer         *pBestOffer = NULL;
    tSNMP_OCTET_STRING_TYPE DhcpClientIdEntry;
    INT4                i4Retval = 0;
    UINT4               u4CurrentTime;
    UINT4               u4Temp;
    UINT1               u1RetVal;
    UINT1               u1State;
    UINT4               u4Xid;
    UINT2               u2OptionLen;
    UINT1              *pReqOptions;
    UINT4               u4CfaIfIndex;
    UINT1               au1ClientId[DHCP_MAX_CLIENTID_LEN];
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entering DhcpCSendRequest fn\n");
    MEMSET (&DhcpClientIdEntry, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpClientIdEntry.pu1_OctetList = &au1ClientId[0];
    MEMSET (DhcpClientIdEntry.pu1_OctetList, 0, DHCP_MAX_CLIENTID_LEN);
    DhcpClientIdEntry.i4_Length = 0;

    u1State = (pDhcpCNode->DhcIfInfo).u1State;
    pBestOffer = (pDhcpCNode->DhcIfInfo).pBestOffer;

    if (pBestOffer == NULL)
    {
        DHCPC_TRC (FAIL_TRC, "No prior offer is available \r\n");
        return DHCP_FAILURE;
    }
    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                      &u4CfaIfIndex) != NETIPV4_SUCCESS)
    {
        return DHCP_FAILURE;
    }
    if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
    {
        DHCPC_TRC (FAIL_TRC, "Packet Allocation FAILURE, DhcpCSendRequest\n");
        return DHCP_FAILURE;
    }

    pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;

    if ((u1State == S_RENEWING) || (u1State == S_BOUND))
    {
        /* Should fill the destination IP address of the server, 
           since it is going to be a unicast packet */
        pOutPkt->u4DstIpAddr = (pDhcpCNode->DhcIfInfo).u4ServerOfferred;

    }

    pOutHdr = &(pOutPkt->DhcpMsg);
    /* Start filling the header of DHCP for DHCP_REQUEST packet */

    pOutHdr->Op = BOOTREQUEST;
    pOutHdr->htype = DHCPC_GET_HW_TYPE (u4IfIndex);
    pOutHdr->hlen = DHCPC_GET_HW_TYPE_LEN (u4IfIndex);
    pOutHdr->hops = 0;

    /* Broadcast the packet , if the state of the interface needs unicasting
     * the flags is overwritten in the switch statement given below */
    pOutHdr->flags = DHCP_BROADCAST_MASK;
    pOutHdr->giaddr = 0;

    /* Xid will be filled in the following SWITCH CASE statement */

    /* ciaddr, siaddr, yiaddr will be over written by the below switch 
       statement depending on the state of the interface. This is done just 
       for the safety purpose */

    pOutHdr->ciaddr = 0;
    pOutHdr->siaddr = 0;
    pOutHdr->yiaddr = 0;

    DhcpCfaGddGetHwAddr (pOutPkt->u4IfIndex, pOutHdr->chaddr);

    MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
    MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);

    if (u1State != S_INITREBOOT)
    {
        pOutHdr->secs = (UINT2) (pDhcpCNode->DhcIfInfo).u4Secs;
    }

    /* Fill the outgoing packet depending on the state of the interface */

    if ((u1State == S_SELECTING) || (u1State == S_REQUESTING)
        || (u1State == S_REBOOTING))
    {
        if (u1State == S_SELECTING)
        {
            DHCPC_TRC (DEBUG_TRC,
                       "\n In the process of sending the DHCP_REQUEST"
                       "message in S_SELECTING State \n");
        }
        else if (u1State == S_REQUESTING)
        {
            DHCPC_TRC (DEBUG_TRC,
                       "\n In the process of sending the DHCP_REQUEST"
                       "message in S_REQUESTING State \n");
        }
        else
        {
            DHCPC_TRC (DEBUG_TRC,
                       "\n In the process of sending the DHCP_REQUEST"
                       "message in S_REBOOTING State \n");
        }

        pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid;

        /* Fill the required options in the outgoing packet */
        DhcpCFillRequiredOptions (pOutPkt,
                                  pBestOffer->PktInfo.DhcpMsg.
                                  pOptions, pBestOffer->PktInfo.u2Len);

        /* Add Requested IP Address option */
        DhcpCTag.Type = DHCP_OPT_REQUESTED_IP;
        DhcpCTag.Len = 4;        /* Requested IP Addr option length */

        u4Temp = OSIX_HTONL (pBestOffer->PktInfo.DhcpMsg.yiaddr);
        DhcpCTag.Val = (UINT1 *) &u4Temp;
        DhcpCAddOption (pOutPkt, &DhcpCTag);
    }
    else if (u1State == S_INITREBOOT)
    {

        DhcpCGetTransId (pOutHdr->chaddr, &u4Xid);
        pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid = u4Xid;

        DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

        (pDhcpCNode->DhcIfInfo).u4Secs = u4CurrentTime;

        /* Fill the secs field in the outgoing packet */
        pOutHdr->secs = (UINT2) u4CurrentTime;

        /* Fill the required options in the outgoing packet */
        DhcpCFillRequiredOptions (pOutPkt,
                                  pBestOffer->PktInfo.DhcpMsg.pOptions,
                                  pBestOffer->PktInfo.u2Len);

        /* Add Requested IP Address option */
        DhcpCTag.Type = DHCP_OPT_REQUESTED_IP;
        DhcpCTag.Len = 4;        /* Requested IP Addr option length */

        u4Temp = OSIX_NTOHL ((pDhcpCNode->DhcIfInfo).u4IpAddressOfferred);
        DhcpCTag.Val = (UINT1 *) &u4Temp;
        DhcpCAddOption (pOutPkt, &DhcpCTag);

        DhcpCRemoveOptionType (DHCP_OPT_SERVER_ID, pOutPkt);
    }
    else if ((u1State == S_RENEWING) || (u1State == S_BOUND)
             || (u1State == S_REBINDING))
    {
        if ((u1State == S_RENEWING) || (u1State == S_BOUND))
        {
            /* Overwrite the flags with UNICAST mask for renewing state */
            pOutHdr->flags = DHCP_UNICAST_MASK;
        }
        DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

        (pDhcpCNode->DhcIfInfo).u4Secs = u4CurrentTime;

        /* Fill the secs field in the outgoing packet */
        pOutHdr->secs = (UINT2) u4CurrentTime;

        pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid;
        pOutHdr->ciaddr = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;

        /* Fill the required options in the outgoing packet */
        DhcpCFillRequiredOptions (pOutPkt,
                                  pBestOffer->PktInfo.DhcpMsg.
                                  pOptions, pBestOffer->PktInfo.u2Len);
        DhcpCRemoveOptionType (DHCP_OPT_SERVER_ID, pOutPkt);
        DhcpCRemoveOptionType (DHCP_OPT_REQUESTED_IP, pOutPkt);
    }
    else
    {

        DHCPC_TRC2 (DEBUG_TRC,
                    "Wrong State in DhcpCSendRequest for Intf %x in %d state \n",
                    pDhcpCNode->u4IpIfIndex, u1State);

        /* free OutPkt Message Structure */
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                            (UINT1 *) pOutPkt->DhcpMsg.pOptions);
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                            (UINT1 *) pOutPkt);
        return DHCP_FAILURE;
    }

    /* Add Parameter request List  option */
    DhcpCopyDefaultOpt (pDhcpCNode);

    i4Retval = DhcpCGetClientIdentifer (u4CfaIfIndex,
                                        DhcpClientIdEntry.pu1_OctetList,
                                        &(DhcpClientIdEntry.i4_Length));
    if (i4Retval == DHCP_SUCCESS && DhcpClientIdEntry.i4_Length != 0)
    {

        DhcpCTag.Type = DHCP_OPT_CLIENT_ID;
        DhcpCTag.Len = (UINT1) DhcpClientIdEntry.i4_Length;
        DhcpCTag.Val = DhcpClientIdEntry.pu1_OctetList;
        DhcpCAddOption (pOutPkt, &DhcpCTag);
    }

    pReqOptions = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions;

    u2OptionLen = (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions;

    DhcpCTag.Type = DHCP_OPT_PARAMETER_LIST;
    DhcpCTag.Len = (UINT1) (u2OptionLen - 2);

    DhcpCTag.Val = (UINT1 *) (pReqOptions + 2);
    DhcpCAddOption (pOutPkt, &DhcpCTag);
    pDhcpCReqOption = (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                                DHCP_OPT_CVENDOR_SPECIFIC);
    if (pDhcpCReqOption != NULL)
    {
        DhcpCTag.Type = DHCP_OPT_CVENDOR_SPECIFIC;
        DhcpCTag.Len = (INT4) pDhcpCReqOption->u1Len;
        DhcpCTag.Val = pDhcpCReqOption->au1OptionVal;
        DhcpCAddOption (pOutPkt, &DhcpCTag);
    }
    u1RetVal = (UINT1) DhcpCSendMessage (pOutPkt, DHCP_REQUEST);

    if (u1RetVal == DHCP_SUCCESS)
    {
        /* DHCPC_TRC : */
        DHCPC_TRC2 (DEBUG_TRC, "DHCP_REQUEST Msg Sent in Intf %x in %d state\n",
                    pDhcpCNode->u4IpIfIndex, u1State);
        /* Increment the statistics */
        (pDhcpCNode->TxRxStats).u4DhcpCRequestSent++;

        /* Start a request timer within which a DHCP_ACK or DHCP_NACK should 
           have received */
        if (DhcpCInitTimer (pDhcpCNode, DHCPC_REQUEST_TIMER_ID,
                            DHCPC_REQUEST_RETX_TIME_OUT) == DHCP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Starting REQUEST_TIMER failed \r\n");
            DhcpCInitState (pDhcpCNode);
        }
    }
    else
    {
        DHCPC_TRC2 (FAIL_TRC,
                    "REQUEST Msg send FAILURE in Intf %x in %d state\n",
                    pDhcpCNode->u4IpIfIndex, u1State);
    }

    /* free OutPkt Message Structure */
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);
    MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                        (UINT1 *) pOutPkt);

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCSendRequest fn\n");

    return u1RetVal;
}                                /* End of DhcpCSendRequest function */

/************************************************************************/
/*  Function Name   : ProcessDhcpCAck                                   */
/*  Description     : This function processes the DHCP_ACK msg rcvd     */
/*  Input(s)        : pPktInfo -> Pointer to the packet information     */
/*                    received                                          */
/*                    pDhcpCNode - DHCP Interface Record                */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
VOID
ProcessDhcpCAck (tDhcpPktInfo * pPktInfo, tDhcpCRec * pDhcpCNode)
{
    INT4                i4RetVal;
    UINT4               u4StartTime;    /* IP Address acquisition start Time */
    UINT4               u4LeaseTime;
    UINT4               u4Temp;
    UINT1               u1State;
    UINT1               u1NextState;
    UINT1               u1RetVal;
    UINT4               u4TimeLeft = 0;
    UINT4               u4CfaIfIndex;
    UINT4               u4SubnetMask;
    UINT4               u4OfferSubnetMask;
    INT4                i4RetValue = 0;
    tBestOffer        **ppBestOffer = NULL;
    tDhcpCOptions      *pDhcpCReqOption = NULL;
    UINT4               u4Type = 0;
    UINT1               au1OptVal[DHCP_MAX_OPT_LEN + 1];
    UINT1               u1Len = 0;

    UINT4               u4PriSntpServerAddr = 0;
    UINT4               u4SecSntpServerAddr = 0;
#ifdef SNTP_WANTED
    tSntpParams         SntpParams;
#endif

    UINT1               au1SysName[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE SwitchName;
    MEMSET (&SwitchName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SysName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1OptVal, 0, DHCP_MAX_OPT_LEN + 1);
    SwitchName.pu1_OctetList = au1SysName;
    DHCPC_TRC (DEBUG_TRC, "Entered ProcessDhcpCAck fn\n");

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    /* Stop the reply wait timer if it is running */
    u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ReplyWaitTimer.
                                        DhcpAppTimer), &u4TimeLeft);

    if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
    }

    /* Reset the retry count */
    pDhcpCNode->u1ReTxCount = DHCPC_RESET;

    /* Fill the statistics depending on the state of the interface */
    if (u1State == S_REQUESTING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREQ++;
    }
    else if (u1State == S_RENEWING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInRENEW++;
    }
    else if (u1State == S_REBINDING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREBIND++;
    }
    else if (u1State == S_REBOOTING)
    {
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREBOOT++;
    }
    else
    {
        DHCPC_TRC (FAIL_TRC, "Invalid client state for DHCP ACK reception\r\n");
        return;
    }

    if (DhcpCGetOption (DHCP_OPT_SUBNET_MASK, pPktInfo) == DHCP_FOUND)
    {
        MEMCPY (&u4SubnetMask, DhcpCTag.Val, DhcpCTag.Len);
        u4SubnetMask = OSIX_NTOHL (u4SubnetMask);
    }
    else
    {
        /* Get the default Subnet mask */
        u4SubnetMask = DhcpCGetDefaultNetMask (pPktInfo->DhcpMsg.yiaddr);
    }
    /*get server offer subnet mask */
    if (DhcpCGetOption
        (DHCP_OPT_SUBNET_MASK,
         &((pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo)) == DHCP_FOUND)
    {
        MEMCPY (&u4OfferSubnetMask, DhcpCTag.Val, DhcpCTag.Len);
        u4OfferSubnetMask = OSIX_NTOHL (u4OfferSubnetMask);
    }
    else
    {
        /* Get the default Subnet mask */
        u4OfferSubnetMask =
            DhcpCGetDefaultNetMask ((pDhcpCNode->DhcIfInfo).pBestOffer->
                                    PktInfo.DhcpMsg.yiaddr);
    }
    if (DhcpCValidateIfIpAndMask (pPktInfo->DhcpMsg.yiaddr, u4SubnetMask)
        == DHCP_FAILURE)
    {
        DHCPC_TRC2 (DEBUG_TRC, "Offer for IP Address %x with NetMask %x "
                    "rejected due to invalid IP/Netmask \n",
                    pPktInfo->DhcpMsg.yiaddr, u4SubnetMask);
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;
        return;
    }
    if ((pPktInfo->DhcpMsg.yiaddr !=
         (pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.DhcpMsg.yiaddr)
        || (u4SubnetMask != u4OfferSubnetMask))
    {
        DHCPC_TRC (DEBUG_TRC,
                   "Packet dropped as the IP/Netmask given in the  ack is different from the one given in the  offer\n");
        return;
    }
    if (DhcpCValidateClientMac (pPktInfo->u4IfIndex, pPktInfo->DhcpMsg.chaddr)
        == DHCP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "Packet dropped  due to invalid Client Mac \n");
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader++;
        return;
    }

    if (((u1State) == S_REBOOTING)
        || (pDhcpCNode->u1DhcpCInformMsgSent == DHCPC_INFORM_MESSAGE_SENT))
    {
        ppBestOffer = &((pDhcpCNode->DhcIfInfo).pBestOffer);

        u1RetVal = DhcpCOfferWithoutSelect (pPktInfo, ppBestOffer);
        if (u1RetVal == DHCP_SUCCESS)
        {
            DHCPC_TRC (DEBUG_TRC, "Offer Accepted without selection\n");
        }
        else
        {
            DHCPC_TRC (DEBUG_TRC, "Offer rejected without selection\n");
            return;
        }
    }

    if (pDhcpCNode->u1DhcpCInformMsgSent == DHCPC_INFORM_MESSAGE_SENT)
    {
        /* For the DHCP_ACK message received for DHCP_INFORM message sent */
        pDhcpCNode->u1DhcpCInformMsgSent = DHCPC_RESET;
        return;
    }

    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return;
    }
    if (DhcpCGetOption (DHCP_OPT_HOST_NAME, pPktInfo) == DHCP_FOUND)

    {

        STRNCPY (SwitchName.pu1_OctetList, DhcpCTag.Val, DhcpCTag.Len);
        SwitchName.i4_Length = DhcpCTag.Len;

        if (SnmpApiSetSysName (&SwitchName) == SNMP_FAILURE)
        {
            DHCPC_TRC (FAIL_TRC, "Hostname configuration failed!!!!!!!\n");

        }
    }

    if ((u1State == S_REQUESTING) || (u1State == S_REBOOTING))
    {
#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
        if (ISS_HW_SUPPORTED ==
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
            i4RetVal = DhcpCArpRespSendEvent (pPktInfo);

            if (i4RetVal == DHCP_FAILURE)
            {
                DHCPC_TRC2 (FAIL_TRC,
                            "ARP response process send event failed for %x Intf in %d state \n",
                            pPktInfo->u4IfIndex, u1State);
                return;
            }
        }
#endif

        /* Do a arp check for the Client Ip address */
        i4RetVal = DhcpCArpCheck (pPktInfo, pDhcpCNode);

        if (i4RetVal == DHCP_FAILURE)
        {
            DHCPC_TRC2 (FAIL_TRC,
                        "ARP check fn failed for %x Intf in %d state \n",
                        pPktInfo->u4IfIndex, u1State);
            return;
        }
        /* Flag to indicate that the Ack message has processed, useful to 
         * reject if any more ACKS or NACKS are received 
         */
        pDhcpCNode->u4ProcessedAck = ACK_MESSAGE_RECIEVED;

        if (DhcpCGetOption (DHCP_OPT_NTP_SERVERS, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_NTP_SERVERS);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    /* Storing the received NTP server information in RBTree
                     * Node for later use */
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                    MEMCPY (&u4PriSntpServerAddr, DhcpCTag.Val, sizeof (UINT4));
                    u4PriSntpServerAddr = OSIX_NTOHL (u4PriSntpServerAddr);
                    if (DhcpCTag.Len >= 8)
                    {
                        /*Secondary SNTP  address offered */
                        MEMCPY (&u4SecSntpServerAddr, (DhcpCTag.Val + 4),
                                sizeof (UINT4));
                        u4SecSntpServerAddr = OSIX_NTOHL (u4SecSntpServerAddr);
                    }
#ifdef SNTP_WANTED
                    MEMSET (&SntpParams, 0, sizeof (tSntpParams));
                    /* Set the Name Server IP address */
                    SntpParams.u4SntpPriAddr = u4PriSntpServerAddr;
                    SntpParams.u4SntpSecAddr = u4SecSntpServerAddr;

                    /* Inform SNTP about the NTP servers  */
                    SntpServerParamsFromDhcpc (&SntpParams);
#else
                    DhcpcProcessSntpServerParams (u4PriSntpServerAddr,
                                                  u4SecSntpServerAddr);
#endif
                }
            }
        }
        /* Check if the Registered Options are present in the dhcp 
         * packet and if so call the application registered interface */
        DhcpCParseRegisteredOptions (u4CfaIfIndex, pPktInfo);

        /* Scan the Option list  and call the appropriate function pointers
         * and send a event to the respective modules*/
        DhcpCCheckOptionIsPresent (pPktInfo);
        /* Store the tftp-server-name if it is requested 
         * by this client */
#ifdef WTP_WANTED
        /* Addition of Default Name Server and Domain Name Option */
        if (DhcpCGetOption (DHCP_OPT_DNS_NS, (pPktInfo)) == DHCP_FOUND)
        {
            /* CALL CAPWAP DNS API */
            CapwapDnsSrvParamsFromDHCP (DHCP_OPT_DNS_NS, DhcpCTag.Val,
                                        DhcpCTag.Len);
        }
        if (DhcpCGetOption (DHCP_OPT_DNS_NAME, (pPktInfo)) == DHCP_FOUND)
        {
            /* CALL CAPWAP DNS API */
            CapwapDnsSrvParamsFromDHCP (DHCP_OPT_DNS_NAME, DhcpCTag.Val,
                                        DhcpCTag.Len);

        }
#endif

        if (DhcpCGetOption (DHCP_OPT_TFTP_SNAME, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_TFTP_SNAME);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                }
            }
        }

        /* Store the boot-file-name if it is requested 
         * by this client */
        if (DhcpCGetOption (DHCP_OPT_BOOT_FILE_NAME, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_BOOT_FILE_NAME);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                }
            }
        }
        if (DhcpCGetOption (DHCP_OPT_SIP_SERVER, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_SIP_SERVER);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    u1RetVal =
                        (UINT1) DhcpCParseSipInfo (&u1Len, au1OptVal, &u4Type);
                    if (u1RetVal == DHCP_FAILURE)
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "FAILURE IN PARSE SIP INFO FUNCTION\n");
                    }
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = u1Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, au1OptVal,
                            pDhcpCReqOption->u1Len);

                }
            }
        }
        if (DhcpCGetOption (DHCP_OPT_240, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_240);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                }
            }
        }

    }
    else if ((u1State == S_RENEWING) || (u1State == S_REBINDING))
    {
        /* Check whether the rebind timer is running or not */
        u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->LeaseTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        if ((u1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
            /* Stop the Rebind timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        u4StartTime = (pDhcpCNode->DhcIfInfo).u4Secs;

        i4RetValue = (INT4) DhcpCGetOption (DHCP_OPT_LEASE_TIME, pPktInfo);
        MEMCPY (&u4LeaseTime, DhcpCTag.Val, DhcpCTag.Len);
        u4LeaseTime = OSIX_NTOHL (u4LeaseTime);

        if (DhcpCGetOption (DHCP_OPT_NTP_SERVERS, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_NTP_SERVERS);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    /* Storing the received NTP server information in RBTree
                     * Node for later use */
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                    MEMCPY (&u4PriSntpServerAddr, DhcpCTag.Val, sizeof (UINT4));
                    u4PriSntpServerAddr = OSIX_NTOHL (u4PriSntpServerAddr);
                    if (DhcpCTag.Len >= 8)
                    {
                        /*Secondary SNTP  address offered */
                        MEMCPY (&u4SecSntpServerAddr, (DhcpCTag.Val + 4),
                                sizeof (UINT4));
                        u4SecSntpServerAddr = OSIX_NTOHL (u4SecSntpServerAddr);
                    }
#ifdef SNTP_WANTED
                    MEMSET (&SntpParams, 0, sizeof (tSntpParams));
                    /* Set the Name Server IP address */
                    SntpParams.u4SntpPriAddr = u4PriSntpServerAddr;
                    SntpParams.u4SntpSecAddr = u4SecSntpServerAddr;

                    /* Inform SNTP about the NTP servers  */
                    SntpServerParamsFromDhcpc (&SntpParams);
#else
                    DhcpcProcessSntpServerParams (u4PriSntpServerAddr,
                                                  u4SecSntpServerAddr);
#endif
                }
            }
        }

        /* Check if the Registered Options are present in the dhcp
         * packet and if so call the application registered interface */
        DhcpCParseRegisteredOptions (u4CfaIfIndex, pPktInfo);

        /* Store the tftp-server-name if it is requested 
         * by this client */
        if (DhcpCGetOption (DHCP_OPT_TFTP_SNAME, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_TFTP_SNAME);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                }
            }
        }

        /* Store the boot-file-name if it is requested 
         * by this client */
        if (DhcpCGetOption (DHCP_OPT_BOOT_FILE_NAME, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_BOOT_FILE_NAME);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                }
            }
        }

        if (DhcpCGetOption (DHCP_OPT_SIP_SERVER, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_SIP_SERVER);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    u1RetVal =
                        (UINT1) DhcpCParseSipInfo (&u1Len, au1OptVal, &u4Type);
                    if (u1RetVal == DHCP_FAILURE)
                    {
                        DHCPC_TRC (DEBUG_TRC,
                                   "FAILURE IN PARSE SIP INFO FUNCTION\n");
                    }
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = u1Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, au1OptVal,
                            pDhcpCReqOption->u1Len);

                }
            }
        }
        if (DhcpCGetOption (DHCP_OPT_240, pPktInfo) == DHCP_FOUND)
        {
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                          DHCP_OPT_240);
            if (pDhcpCReqOption != NULL)
            {
                if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
                {
                    MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
                    pDhcpCReqOption->u1Len = DhcpCTag.Len;
                    MEMCPY (pDhcpCReqOption->au1OptionVal, DhcpCTag.Val,
                            pDhcpCReqOption->u1Len);
                }
            }
        }

        u1NextState = CalRenewRebindTime (pDhcpCNode, u4StartTime, u4LeaseTime);
        (pDhcpCNode->DhcIfInfo).u1State = u1NextState;

        if (u1NextState == S_BOUND)
        {
            /* Write the availabe lease info into config file */
            /* Configure the Interface with the Auto Configuration IP Address */

            if ((DhcpCfaInterfaceIpAddrConfig (pDhcpCNode,
                                               pPktInfo->DhcpMsg.yiaddr)) ==
                DHCP_SUCCESS)
            {
                DHCPC_TRC (FAIL_TRC, "Configured the IP ADDRESS Leased \n");
            }
            else
            {
                DHCPC_TRC (FAIL_TRC, "FAILED in Configuring the IP ADDRESS \n");
            }
            /* Store the lease information in the interface table */
            (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred =
                pPktInfo->DhcpMsg.yiaddr;
            (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = u4LeaseTime;

            (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.
                u4StartOfLeaseTime = u4StartTime;

            i4RetValue = (INT4) DhcpCGetOption (DHCP_OPT_SERVER_ID, pPktInfo);
            UNUSED_PARAM (i4RetValue);

            MEMCPY (&u4Temp, DhcpCTag.Val, DhcpCTag.Len);
            u4Temp = OSIX_NTOHL (u4Temp);

            (pDhcpCNode->DhcIfInfo).u4ServerOfferred = u4Temp;

            /* print the current bindings; only if the trace level is enabled */
            if (gu4DhcpCDebugMask & BIND_TRC)
            {
                DhcpCShowBinding (pPktInfo->u4IfIndex);
            }

            /* Intimate DHCP standby peers about the new binding */
            DhCRedSendDynamicClientInfo (pDhcpCNode);

        }
        else if (u1NextState == S_INIT)
        {
            DhcpCStartProcessing (pPktInfo->u4IfIndex);
        }

    }
    else                        /* Ack message is received in a wrong state */
    {
        DHCPC_TRC2 (EVENT_TRC, "ACK msg rcvd in %x intf in wrong state %d\n",
                    pPktInfo->u4IfIndex, (u1State));
        return;
    }

    DHCPC_TRC (DEBUG_TRC, "Exiting ProcessDhcpCAck fn\n");

    return;
}                                /* End of ProcessDhcpCACK function */

/************************************************************************/
/*  Function Name   : DhcpCRemoveOptionType                             */
/*  Description     : This function removes the type of option from the */
/*                    packet received                                   */
/*  Input(s)        : u1TagType-> Type of Option to be removed          */
/*                    pPktInfo -> Pointer to the packet information     */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
VOID
DhcpCRemoveOptionType (UINT1 u1TagType, tDhcpPktInfo * pPktInfo)
{
    UINT2               u2PktOptLen;
    UINT2               u2LenOfOptions;
    UINT2               u2TempOptLen;
    UINT1              *pTempOption;
    UINT1              *pPktInfoOption;
    UINT4               u4TempLen;
    UINT4               u4CopyLen;

    if ((DhcpCGetOption (u1TagType, pPktInfo) == DHCP_NOT_FOUND))
    {
        DHCPC_TRC (FAIL_TRC, "Option not present\n");
        return;
    }
    pPktInfoOption = pTempOption = pPktInfo->DhcpMsg.pOptions;

    /* Now the DhcpCTag.Len has the len of the option which is to 
       be removed */

    u2LenOfOptions = pPktInfo->u2Len;

    u2PktOptLen = 0;
    u2TempOptLen = 0;

    while (u2TempOptLen < u2LenOfOptions)
    {
        if ((*(pTempOption + u2TempOptLen) == u1TagType))
        {
            pPktInfo->u2Len -= DhcpCTag.Len + 2;
            u2TempOptLen += DhcpCTag.Len + 2;
            continue;
        }
        else if ((*(pTempOption + u2TempOptLen) == DHCP_OPT_PAD))
        {
            pPktInfo->u2Len -= 1;
            u2TempOptLen += 1;
            continue;
        }
        else if ((*(pTempOption + u2TempOptLen) == DHCP_OPT_END))
        {
            pPktInfo->u2Len -= 1;
            u2TempOptLen += 1;
            continue;
        }
        else
        {
            /* Copy for the length of the options ,the type and its length */
            u4CopyLen = *(pTempOption + u2TempOptLen + 1) + 2;

            for (u4TempLen = 0; u4TempLen < u4CopyLen; u4TempLen++)
            {
                *(pPktInfoOption + u2PktOptLen) = *(pTempOption + u2TempOptLen);
                u2TempOptLen++;
                u2PktOptLen++;
            }
        }
    }                            /* End of While loop */

}                                /* End of the DhcpCRemoveOptionType function */

/************************************************************************/
/*  Function Name   : DhcpCOfferWithoutSelect                           */
/*  Description     : This function processes DHCP_OFFER recived in the */
/*                    non selecting state                               */
/*  Input(s)        : pPktInfo -> Pointer to the packet information rcvd*/
/*                    pBestOffer->Best Offer Relevant to the Interface  */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCOfferWithoutSelect (tDhcpPktInfo * pPktInfo, tBestOffer ** pOffer)
{
    tBestOffer         *pBestOffer;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCOfferWithoutSelect fn\n");

    if (NULL == *pOffer)
    {
        pBestOffer =
            (tBestOffer *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                           DhcpCBestOffPoolId);

        if (pBestOffer == NULL)
        {
            DHCPC_TRC (DEBUG_TRC, "FAILURE in MemAlloc for pBestOffer\r\n");
            return DHCP_FAILURE;
        }

        MEMCPY (&(pBestOffer->PktInfo), pPktInfo, sizeof (tDhcpPktInfo));

        pBestOffer->PktInfo.DhcpMsg.pOptions =
            (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                      DhcpCDhcpMsgOptPoolId);

        if (pBestOffer->PktInfo.DhcpMsg.pOptions == NULL)
        {
            DHCPC_TRC (DEBUG_TRC,
                       "FAILURE in MemAlloc for Best offer option\r\n");
            MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCBestOffPoolId,
                                (UINT1 *) pBestOffer);
            return DHCP_FAILURE;
        }
        MEMCPY ((pBestOffer->PktInfo.DhcpMsg.pOptions),
                pPktInfo->DhcpMsg.pOptions, (sizeof (UINT1) * pPktInfo->u2Len));

    }
    else
    {
        pBestOffer = *pOffer;
    }
    DhcpCRemoveOptionType (DHCP_OPT_MSG_TYPE, &(pBestOffer->PktInfo));

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCOfferWithoutSelect fn\n");

    *pOffer = pBestOffer;

    return DHCP_SUCCESS;
}                                /* End of DhcpCOfferWithoutSelect function */

/************************************************************************/
/*  Function Name   : DhcpCRouteAdd                                     */
/*  Description     : Adds a static Route                               */
/*  Input(s)        : u4IpAddress - Ip address of the destination       */
/*                    u4NetMask   - NetMask                             */
/*                    u4NextHop   - NextHop                             */
/*                    i4Metric    - metric for the route                */
/*                    u4IfIndex   - Index of the interface  through     */
/*                                  which destination is reachable      */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCRouteAdd (UINT4 u4IpAddress, UINT4 u4NetMask,
               UINT4 u4NextHop, INT4 i4Metric, UINT4 u4IfIndex)
{
    tNetIpv4RtInfo      NetRtInfo;
    UINT1               u1CmdType;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4DestNet = u4IpAddress;
    NetRtInfo.u4DestMask = u4NetMask;
    NetRtInfo.u4NextHop = u4NextHop;
    NetRtInfo.u4RtIfIndx = u4IfIndex;
    NetRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u2RtProto = OTHERS_ID;
    NetRtInfo.i4Metric1 = i4Metric;
    NetRtInfo.u4RowStatus = ACTIVE;
    NetRtInfo.u4ContextId = VCM_DEFAULT_CONTEXT;
    NetRtInfo.u2Weight = IP_DEFAULT_WEIGHT;
    u1CmdType = NETIPV4_ADD_ROUTE;

    if (NetIpv4LeakRoute (u1CmdType, &NetRtInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCRouteAdd fn\n");

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCRouteDel                                     */
/*  Description     : Adds a static Route                               */
/*  Input(s)        : u4IpAddress - Ip address of the destination       */
/*                    u4NetMask   - NetMask                             */
/*                    u4NextHop   - NextHop                             */
/*                    i4Metric    - metric for the route                */
/*                    u4IfIndex   - Index of the interface  through     */
/*                                  which destination is reachable      */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
UINT1
DhcpCRouteDel (UINT4 u4IpAddress, UINT4 u4NetMask,
               UINT4 u4NextHop, INT4 i4Metric, UINT4 u4IfIndex)
{
    tNetIpv4RtInfo      NetRtInfo;
    UINT1               u1CmdType;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4DestNet = u4IpAddress;
    NetRtInfo.u4DestMask = u4NetMask;
    NetRtInfo.u4NextHop = u4NextHop;
    NetRtInfo.u4RtIfIndx = u4IfIndex;
    NetRtInfo.u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u2RtProto = OTHERS_ID;
    NetRtInfo.i4Metric1 = i4Metric;
    NetRtInfo.u4RowStatus = ACTIVE;
    NetRtInfo.u4ContextId = VCM_DEFAULT_CONTEXT;
    NetRtInfo.u2Weight = IP_DEFAULT_WEIGHT;
    u1CmdType = NETIPV4_DELETE_ROUTE;

    if (NetIpv4LeakRoute (u1CmdType, &NetRtInfo) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }
    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCRouteDel fn\n");

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCProcessRelease                               */
/*  Description     : Stops the timers and sends release message        */
/*  Input(s)        : Port Number                                       */
/*                    u1Flag - Indicates sending RELEASE Message        */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCProcessRelease (tDhcpCRec * pDhcpCNode, UINT1 u1Flag)
{
    INT1                i1RetVal;
    UINT4               u4TimeLeft = 0;
    UINT1               u1State;
    UINT4               u4CfaIfIndex;
    UINT1               u1AllocProto;
    UINT1               u1AllocMethod = 0;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCProcessRelease function\n");
    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex, &u4CfaIfIndex)
        != NETIPV4_SUCCESS)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in NetIpv4GetCfaIfIndexFromPort fn\n");
        return DHCP_FAILURE;
    }
    if (CfaIfGetIpAllocProto (u4CfaIfIndex, &u1AllocProto) == CFA_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in CfaIfGetIpAllocProto fn\n");
        return DHCP_FAILURE;
    }

    if (CfaIfGetIpAllocMethod (u4CfaIfIndex, &u1AllocMethod) == CFA_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in CfaIfGetIpAllocMethod fn\n");
        return DHCP_FAILURE;
    }

    if (u1AllocProto != CFA_PROTO_DHCP)
    {
        DHCPC_TRC (DEBUG_TRC, "Protocol used is not DHCP\n");
        return DHCP_FAILURE;
    }

    if ((DHCPC_RM_GET_NODE_STATUS () == RM_STANDBY)
        && (u1AllocMethod != CFA_IP_ALLOC_MAN))
    {
        /* IF RMGR is enabled and the node is elected as standby */
        /* then */
        /* 1. Configure the IP address immediately */
        /* 2. Update the state information */
        /* 3. Update the client record */

        DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);
        (pDhcpCNode->DhcIfInfo).u1State = S_UNUSED;
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;
        (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred = DHCPC_RESET;
        (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred = DHCPC_RESET;

        DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCProcessRelease function\n");
        return DHCP_SUCCESS;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;
    /* Check whether the rebind timer is running or not */
    i1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->LeaseTimer.DhcpAppTimer),
                                      &u4TimeLeft);

    if ((i1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
        /* Stop the Renewal timer */
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

    /* Stop the reply wait timer if it is running */
    i1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                      &(pDhcpCNode->ReplyWaitTimer.
                                        DhcpAppTimer), &u4TimeLeft);
    if ((i1RetVal == DHCP_SUCCESS) && (u4TimeLeft != 0))
    {
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
    }

    if (((u1State == S_BOUND) ||
         (u1State == S_RENEWING) || (u1State == S_REBINDING)))
    {
        /* On Interface deletion,Dont send the RELEASE Message */
        /* If RELEASE Message need to send configure IP address after 
           RELEASE_TIMOUT_TIMER Timer Expiry */
        if (u1Flag == OSIX_TRUE)
        {
            i1RetVal = DhcpCSendReleaseMsg (pDhcpCNode);

            if (i1RetVal != DHCP_SUCCESS)
            {
                DHCPC_TRC1 (FAIL_TRC,
                            "FAILURE in sending DHCP_RELEASE for Interface %x\n",
                            pDhcpCNode->u4IpIfIndex);
                return DHCP_FAILURE;
            }
        }
    }
    /* If RELEASE Message need not to send configure IP address immediately */
    if (u1Flag != OSIX_TRUE)
    {
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;

        /* Now bound the IP address to DHCPC_AUTO_CONFIG_IP_ADDRESS */
        (pDhcpCNode->DhcIfInfo).u1State = S_BOUND;
        /* Make interface IP Address to DHCPC_AUTO_CONFIG_IP_ADDRESS */
        DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);
        (pDhcpCNode->DhcIfInfo).u1State = S_UNUSED;

    }
    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCProcessRelease function\n");
    return DHCP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : DhcpCGetTransId
 *
 *    Description         : This function returns the Transation ID.
 *                          
 *    Input(s)            : pHwAddr - Mac address. 
 *
 *    Output(s)           : u4Xid - Transaction Id.
 *
 *    Global Variables Referred : gIssSysGroupInfo.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None 
 *                         
 *
 *****************************************************************************/

VOID
DhcpCGetTransId (UINT1 *pHwAddr, UINT4 *pu4Xid)
{

    UINT2               u2ByteIndex;

    /* The transaction ID is calculated by XOR'ing the random generared value 
     * with the last three bytes of the switch Mac*/

    *pu4Xid = random ();
    for (u2ByteIndex = 3; u2ByteIndex < MAC_ADDR_LEN; u2ByteIndex++)
    {
        *pu4Xid ^= pHwAddr[u2ByteIndex];
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpCSetReqOptionType                             */
/*  Description     : This API sets the given option type in the        */
/*                    requested option list. These requested options    */
/*                    will be appended in client packets                */
/*  Input(s)        : u4IfIndex - Interface Index                       */
/*                    u4OptType - Option type number                    */
/*                    u4OptStatus - Set/Reset                           */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCSetReqOptionType (UINT4 u4IfIndex, UINT4 u4OptType, UINT4 u4OptStatus)
{
    INT4                i4RetVal = DHCP_FAILURE;

    DHCPC_PROTO_LOCK ();
    i4RetVal = DhcpcConfReqOptionType (u4IfIndex, u4OptType, u4OptStatus);
    DHCPC_PROTO_UNLOCK ();
    return (i4RetVal);
}

/************************************************************************/
/*  Function Name   : DhcpcConfReqOptionType                            */
/*  Description     : This function sets the given option type in the   */
/*                    requested option list. These requested options    */
/*                    will be appended in client packets                */
/*  Input(s)        : u4IfIndex - Interface Index                       */
/*                    u4OptType - Option type number                    */
/*                    u4OptStatus - Set/Reset                           */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpcConfReqOptionType (UINT4 u4IfIndex, UINT4 u4OptType, UINT4 u4OptStatus)
{

    tDhcpCOptions      *pDhcpCReqOption = NULL;
    tDhcpCOptions       DhcpCReqOption;
    UINT4               u4Port;
    UINT4               u4RetVal = 0;

    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    if (u4OptStatus == DHCPC_SET_OPTION)
    {

        /* Check entry is already present or not */
        MEMSET (&DhcpCReqOption, 0, sizeof (tDhcpCOptions));
        DhcpCReqOption.u4IfIndex = u4IfIndex;
        DhcpCReqOption.u1Type = (UINT1) u4OptType;
        pDhcpCReqOption =
            (tDhcpCOptions *) RBTreeGet (gDhcpCReqOptionList,
                                         (tRBElem *) & DhcpCReqOption);
        if (pDhcpCReqOption != NULL)
        {
            /* Entry is present */
            DHCPC_TRC (DEBUG_TRC, "DHCP Req Option Entry already present\n");
            return DHCP_SUCCESS;
        }

        /* Add new entry */
        pDhcpCReqOption = NULL;
        pDhcpCReqOption =
            (tDhcpCOptions *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                              DhcpCOptMemPoolId);
        if (pDhcpCReqOption == NULL)
        {
            DHCPC_TRC (DEBUG_TRC, "DHCP Req Option Entry already present\n");
            return DHCP_FAILURE;
        }

        MEMSET (pDhcpCReqOption, 0, sizeof (tDhcpCOptions));

        pDhcpCReqOption->u4IfIndex = u4IfIndex;
        pDhcpCReqOption->u1Type = (UINT1) u4OptType;
        pDhcpCReqOption->u1Status = DHCP_DISABLE;
        u4RetVal = RBTreeAdd (gDhcpCReqOptionList, (tRBElem *) pDhcpCReqOption);
        UNUSED_PARAM (u4RetVal);
        return DHCP_SUCCESS;
    }
    else if (u4OptStatus == DHCPC_RESET_OPTION)
    {
        MEMSET (&DhcpCReqOption, 0, sizeof (tDhcpCOptions));
        DhcpCReqOption.u4IfIndex = u4IfIndex;
        DhcpCReqOption.u1Type = (UINT1) u4OptType;

        pDhcpCReqOption =
            (tDhcpCOptions *) RBTreeGet (gDhcpCReqOptionList,
                                         (tRBElem *) & DhcpCReqOption);
        if (pDhcpCReqOption != NULL)
        {
            RBTreeRemove (gDhcpCReqOptionList, (tRBElem *) pDhcpCReqOption);
            MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOptMemPoolId,
                                (UINT1 *) pDhcpCReqOption);
            return DHCP_SUCCESS;
        }
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpCGetReqOptionEntry                            */
/*  Description     : Get requested option entry for the corresponding  */
/*                    interface index and option type                   */
/*  Input(s)        : u4IfIndex - Interface index                       */
/*                    u4OptType - Option type number                    */
/*  Output(s)       : None                                              */
/*  Returns         : pDhcpCReqOption - Requested Option Entry          */
/************************************************************************/

tDhcpCOptions      *
DhcpCGetReqOptionEntry (UINT4 u4IfIndex, UINT4 u4OptType)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;
    tDhcpCOptions       DhcpCReqOption;

    MEMSET (&DhcpCReqOption, 0, sizeof (tDhcpCOptions));
    DhcpCReqOption.u4IfIndex = u4IfIndex;
    DhcpCReqOption.u1Type = (UINT1) u4OptType;
    pDhcpCReqOption =
        (tDhcpCOptions *) RBTreeGet (gDhcpCReqOptionList,
                                     (tRBElem *) & DhcpCReqOption);
    return pDhcpCReqOption;
}

/************************************************************************/
/*  Function Name   : DhcpCGetFirstIndexReqOptionEntry                  */
/*  Description     : Gives first valid entry in dhcp requested options */
/*                    list                                              */
/*  Input(s)        : None                                              */
/*  Output(s)       : pu4IfIndex - First valid interface index          */
/*                    pu4OptType - First valid option type              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCGetFirstIndexReqOptionEntry (UINT4 *pu4IfIndex, UINT4 *pu4OptType)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    pDhcpCReqOption = (tDhcpCOptions *) RBTreeGetFirst (gDhcpCReqOptionList);
    if (pDhcpCReqOption != NULL)
    {
        *pu4IfIndex = pDhcpCReqOption->u4IfIndex;
        *pu4OptType = (UINT4) pDhcpCReqOption->u1Type;
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpCGetNextIndexReqOptionEntry                   */
/*  Description     : Gives next valid entry in dhcp requested options  */
/*                    list                                              */
/*  Input(s)        : u4IfIndex - Interface index                       */
/*                    u4OptType - Option type number                    */
/*  Output(s)       : pu4NextIfIndex - Next Valid Interface Index       */
/*                    pu4NextOptType - Next valid option type           */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpCGetNextIndexReqOptionEntry (UINT4 u4IfIndex, UINT4 *pu4NextIfIndex,
                                 UINT4 u4OptType, UINT4 *pu4NextOptType)
{
    tDhcpCOptions       DhcpCReqOption;
    tDhcpCOptions      *pNextReqOptionList = NULL;

    MEMSET (&DhcpCReqOption, 0, sizeof (tDhcpCOptions));
    DhcpCReqOption.u4IfIndex = u4IfIndex;
    DhcpCReqOption.u1Type = (UINT1) u4OptType;
    pNextReqOptionList =
        (tDhcpCOptions *) RBTreeGetNext (gDhcpCReqOptionList,
                                         (tRBElem *) & DhcpCReqOption, NULL);
    if (pNextReqOptionList != NULL)
    {
        *pu4NextIfIndex = pNextReqOptionList->u4IfIndex;
        *pu4NextOptType = (UINT4) pNextReqOptionList->u1Type;
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpCSetClientIdentifier                          */
/*  Description     : Configures the given client identifier string     */
/*                    for this particular interface                     */
/*  Input(s)        : u4IfIndex - Interface Index                       */
/*                    pu1ClientIdentifier - Reference to                */
/*                                    clientIdentifier to be configured */
/*                    i4Len - Length of client identifier string        */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCSetClientIdentifier (UINT4 u4IfIndex,
                          UINT1 *pu1ClientIdentifier, INT4 i4Len)
{
    tClientIdentifier  *pDhcpClientId = NULL;
    tClientIdentifier  *pNewDhcpClientId = NULL;
    tClientIdentifier   DhcpClientId;
    UINT4               u4RetVal = 0;

    if (pu1ClientIdentifier == NULL)
        return DHCP_FAILURE;

    MEMSET (&DhcpClientId, 0, sizeof (tClientIdentifier));
    DhcpClientId.u4IfIndex = u4IfIndex;

    pDhcpClientId =
        (tClientIdentifier *) RBTreeGet (gDhcpCidList,
                                         (tRBElem *) & DhcpClientId);
    if (pDhcpClientId != NULL)
    {
        /* Entry is present. Updating with the new id */
        pDhcpClientId->u1Len = MEM_MAX_BYTES (i4Len, DHCP_MAX_CLIENTID_LEN);
        MEMSET (pDhcpClientId->au1String, 0, DHCP_MAX_CLIENTID_LEN);
        MEMCPY (pDhcpClientId->au1String, pu1ClientIdentifier,
                pDhcpClientId->u1Len);
        return DHCP_SUCCESS;
    }

    /* Creating new entry in Client Identifier List */
    pNewDhcpClientId =
        (tClientIdentifier *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                              DhcpCIdMemPoolId);

    if (pNewDhcpClientId != NULL)
    {
        MEMSET (pNewDhcpClientId, 0, sizeof (tClientIdentifier));
        pNewDhcpClientId->u4IfIndex = u4IfIndex;
        pNewDhcpClientId->u1Len = MEM_MAX_BYTES (i4Len, DHCP_MAX_CLIENTID_LEN);
        MEMCPY (pNewDhcpClientId->au1String, pu1ClientIdentifier,
                pNewDhcpClientId->u1Len);
        /* Add New clientId node into the list */
        u4RetVal = RBTreeAdd (gDhcpCidList, (tRBElem *) pNewDhcpClientId);
        UNUSED_PARAM (u4RetVal);
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpcConfigClientId                               */
/*  Description     : API to set client Identifier for a particular     */
/*                    interface                                         */
/*  Input(s)        : u4IfIndex - Interface Index                       */
/*                    pu1ClientIdentifier - Reference to                */
/*                                    clientIdentifier to be configured */
/*                    i4Len - Length of client identifier string        */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpcConfigClientId (UINT4 u4IfIndex, UINT1 *pu1ClientIdentifier, INT4 i4Len)
{
    INT4                i4RetVal = DHCP_FAILURE;

    DHCPC_PROTO_LOCK ();
    i4RetVal = DhcpCSetClientIdentifier (u4IfIndex, pu1ClientIdentifier, i4Len);
    DHCPC_PROTO_UNLOCK ();
    return (i4RetVal);
}

/************************************************************************/
/*  Function Name   : DhcpCGetClientIdentifer                           */
/*  Description     : Thid function gives Client identifier string      */
/*  Input(s)        : u4IfIndex - Interface index                       */
/*  Output(s)       : pu1ClientIdStr - Value of Client identifier       */
/*                    pi4Len -  length of Client identifier string      */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCGetClientIdentifer (UINT4 u4IfIndex, UINT1 *pu1ClientIdStr, INT4 *pi4Len)
{
    tClientIdentifier  *pDhcpClientId = NULL;
    tClientIdentifier   DhcpClientId;

    MEMSET (&DhcpClientId, 0, sizeof (tClientIdentifier));
    DhcpClientId.u4IfIndex = u4IfIndex;

    pDhcpClientId =
        (tClientIdentifier *) RBTreeGet (gDhcpCidList,
                                         (tRBElem *) & DhcpClientId);
    if (pDhcpClientId != NULL)
    {
        *pi4Len = (INT4) pDhcpClientId->u1Len;
        MEMSET (pu1ClientIdStr, 0, DHCP_MAX_CLIENTID_LEN);
        MEMCPY (pu1ClientIdStr, pDhcpClientId->au1String, *pi4Len);
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpCGetReqOptionValue                            */
/*  Description     : API to read the requested option values           */
/*  Input(s)        : u4IfIndex - Interface index                       */
/*                    u4OptType - Option type number                    */
/*  Output(s)       : pu1OptVal - Value of particular option type       */
/*                    pu1ErrState - client dhcp state, when there is    */
/*                                  option value is not avilable        */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpCGetReqOptionValue (UINT4 u4IfIndex, UINT4 u4OptType,
                        UINT1 *pu1OptVal, UINT1 *pu1ErrState)
{
    tSNMP_OCTET_STRING_TYPE DhcpcOptVal;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT4               u4Port;
    UINT1               au1OptVal[DHCP_MAX_OPT_LEN];

    MEMSET (au1OptVal, 0, DHCP_MAX_OPT_LEN);
    MEMSET (&DhcpcOptVal, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpcOptVal.pu1_OctetList = &au1OptVal[0];
    MEMSET (DhcpcOptVal.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);
    DhcpcOptVal.i4_Length = 0;

    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) == NETIPV4_FAILURE)
    {
        return DHCP_FAILURE;
    }

    DHCPC_PROTO_LOCK ();
    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        DHCPC_PROTO_UNLOCK ();
        return DHCP_FAILURE;
    }

    if (nmhGetDhcpClientOptVal (u4IfIndex, u4OptType, &DhcpcOptVal)
        == SNMP_SUCCESS)
    {
        if (DhcpcOptVal.i4_Length == 0)
        {
            *pu1ErrState = (pDhcpCNode->DhcIfInfo).u1State;
            DHCPC_PROTO_UNLOCK ();
            return DHCP_FAILURE;
        }
        MEMCPY (pu1OptVal, DhcpcOptVal.pu1_OctetList, DhcpcOptVal.i4_Length);
    }
    DHCPC_PROTO_UNLOCK ();
    return DHCP_SUCCESS;
}

INT4
DhcpCReqOptEntryCmp (tRBElem * pDhcpReqEntryNode, tRBElem * pDhcpReqEntryIn)
{
    /* Compare the Interface index */
    if (((tDhcpCOptions *) pDhcpReqEntryNode)->u4IfIndex
        < ((tDhcpCOptions *) pDhcpReqEntryIn)->u4IfIndex)
    {
        return -1;
    }
    else if (((tDhcpCOptions *) pDhcpReqEntryNode)->u4IfIndex
             > ((tDhcpCOptions *) pDhcpReqEntryIn)->u4IfIndex)
    {
        return 1;
    }

    /* Compare the dhcp option type */
    if (((tDhcpCOptions *) pDhcpReqEntryNode)->u1Type
        < ((tDhcpCOptions *) pDhcpReqEntryIn)->u1Type)
    {
        return -1;
    }
    else if (((tDhcpCOptions *) pDhcpReqEntryNode)->u1Type
             > ((tDhcpCOptions *) pDhcpReqEntryIn)->u1Type)
    {
        return 1;
    }
    return 0;

}

INT4
DhcpCReqOptEntryFree (tRBElem * pDhcpReqEntryNode)
{
    if (pDhcpReqEntryNode != NULL)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOptMemPoolId,
                            (UINT1 *) pDhcpReqEntryNode);
        pDhcpReqEntryNode = NULL;
    }
    return DHCP_SUCCESS;
}

INT4
DhcpCidEntryFree (tRBElem * pDhcpCidEntryNode)
{
    if (pDhcpCidEntryNode != NULL)
    {
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCIdMemPoolId,
                            (UINT1 *) (pDhcpCidEntryNode));
        pDhcpCidEntryNode = NULL;
    }
    return DHCP_SUCCESS;
}

INT4
DhcpCidEntryCmp (tRBElem * pDhcpCidEntryNode, tRBElem * pDhcpCidentryIn)
{
    /* Compare the Interface index */
    if (((tClientIdentifier *) pDhcpCidEntryNode)->u4IfIndex
        < ((tClientIdentifier *) pDhcpCidentryIn)->u4IfIndex)
    {
        return -1;
    }
    else if (((tClientIdentifier *) pDhcpCidEntryNode)->u4IfIndex
             > ((tClientIdentifier *) pDhcpCidentryIn)->u4IfIndex)
    {
        return 1;
    }
    return 0;

}

/*****************************************************************************
 *
 *    Function Name             : DhcpCValidateIfIpAndMask
 *
 *    Description               : This function validates the Ip addr and 
 *                                subnet mask offered  
 *                                in the Offer packet.
 *
 *    Input(s)                  : Ip Addr and Netmask.
 *
 *
 *    Output(s)                 : None. 
 *
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : DHCP_SUCCESS or DHCP_FAILURE.
 *****************************************************************************/

INT4
DhcpCValidateIfIpAndMask (UINT4 u4IpAddr, UINT4 u4NetMask)
{
    UINT1               u1Counter;
    UINT4               u4BroadCastAddr;
    UINT4               u4NetworkIpAddr;

    if (u4IpAddr == 0 || u4IpAddr == 0xffffffff ||
        u4NetMask == 0 || u4NetMask == 0xffffffff)
    {
        return DHCP_FAILURE;
    }

    if (IP_IS_ADDR_CLASS_D (u4IpAddr))
    {
        return DHCP_FAILURE;
    }

    if (IP_IS_ADDR_CLASS_E (u4IpAddr) || ((u4IpAddr & 0xFF000000) == 0)
        ||
        (!((IP_IS_ADDR_CLASS_A (u4IpAddr)) || (IP_IS_ADDR_CLASS_B (u4IpAddr))
           || (IP_IS_ADDR_CLASS_C (u4IpAddr)))))
    {
        return (DHCP_FAILURE);

    }
    /* The valid subnet address is from 0 to 32 */

    for (u1Counter = 0; u1Counter <= DHCP_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4NetMask)
        {
            break;
        }
    }

    if (u1Counter > DHCP_MAX_CIDR)
    {
        return DHCP_FAILURE;
    }
    /* ipaddress should not be Broadcast Address */
    u4BroadCastAddr = u4IpAddr | (~(u4NetMask));
    if (u4IpAddr == u4BroadCastAddr)
    {
        return DHCP_FAILURE;
    }

    /* ipaddress should not be Network Address */
    u4NetworkIpAddr = u4IpAddr & (~(u4NetMask));
    if (u4NetworkIpAddr == 0)
    {
        return DHCP_FAILURE;
    }

    return DHCP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name             : DhcpCValidateClientMac
 *
 *    Description               : This function validates the client mac 
 *                                in the received packet from the server
 *
 *    Input(s)                  : Ip Port number and Client Mac.
 *
 *
 *    Output(s)                 : None. 
 *
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : DHCP_SUCCESS or DHCP_FAILURE.
 *****************************************************************************/

INT4
DhcpCValidateClientMac (UINT4 u4Port, UINT1 *pHwAddr)
{

    UINT1               au1HwAddr[ENET_ADDR_LEN];

    MEMSET (au1HwAddr, 0, ENET_ADDR_LEN);
    DhcpCfaGddGetHwAddr (u4Port, au1HwAddr);

    if (MEMCMP (pHwAddr, au1HwAddr, ENET_ADDR_LEN) == 0)
    {
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name             : DhcpCProcessOptions
 *
 *    Description               : This function update the option list 
 *                                requested by the application  
 *
 *    Input(s)                  : None.
 *
 *
 *    Output(s)                 : None. 
 *
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : DHCP_SUCCESS or DHCP_FAILURE.
 *****************************************************************************/

VOID
DhcpCProcessOptions (tDhcpQMsg * pMsg)
{
    tDhcpDiscOption    *pDhcpDiscOptionNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    INT1                iCnt = 0;
    tDhcpCDiscCbInfo   *pCbInfo = NULL;

    if (NULL == pMsg)
    {
        return;
    }

    MEMSET (&gRegDhcpDiscOption, 0, sizeof (tRegDhcpDiscOption));

    pCbInfo = &(pMsg->unDhcpMsgIfParam.AppDhcpCDiscCbInfo);

    if ((pDhcpDiscOptionNode =
         (tDhcpDiscOption *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                             DhcpCDiscPoolId)) == NULL)
    {
        return;
    }

    MEMCPY (pDhcpDiscOptionNode->aiOptionBitMask,
            pCbInfo->aiOptionBitMask, (sizeof (INT4) * DHCP_OPTION_BLOCKS));

    pDhcpDiscOptionNode->iTaskId = pCbInfo->iTaskId;
    pDhcpDiscOptionNode->pDhcDiscoveryCbFn = pCbInfo->pDhcDiscoveryCbFn;

    TMO_SLL_Add (&gDhcpDiscOptionList, (tTMO_SLL_NODE *) pDhcpDiscOptionNode);
    TMO_SLL_Scan (&gDhcpDiscOptionList, pNode, tTMO_SLL_NODE *)
    {
        pDhcpDiscOptionNode = (tDhcpDiscOption *) pNode;
        for (iCnt = 0; iCnt < DHCP_OPTION_BLOCKS; iCnt++)
        {
            gRegDhcpDiscOption.ai4OptionBitMask[iCnt] |=
                pDhcpDiscOptionNode->aiOptionBitMask[iCnt];
        }
    }
    /* Convert the Bitmask into a array of Options */
    DhcpClientUtilConvertBitMasktoOption (gRegDhcpDiscOption.ai4OptionBitMask,
                                          gRegDhcpDiscOption.au1Data,
                                          &gRegDhcpDiscOption.i1OptionCnt);

    return;
}
