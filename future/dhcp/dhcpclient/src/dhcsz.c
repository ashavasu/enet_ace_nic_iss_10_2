/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcsz.c,v 1.5 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _DHCSZ_C
#include "dhccmn.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DhcSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DHC_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsDHCSizingParams[i4SizingId].u4StructSize,
                                     FsDHCSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(DHCMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DhcSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DhcSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDHCSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DHCMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DhcSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DHC_MAX_SIZING_ID; i4SizingId++)
    {
        if (DHCMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DHCMemPoolIds[i4SizingId]);
            DHCMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
