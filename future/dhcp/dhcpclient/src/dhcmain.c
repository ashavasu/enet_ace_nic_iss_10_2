/************************************************************************
 * $Id: dhcmain.c,v 1.28 2015/12/29 11:59:31 siva Exp $ *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                          *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       *
 * FILE NAME    : dhcmain.c                                             *
 * LANGUAGE     : C Language                                            *
 * TARGET       : LINUX                                                 *
 * AUTHOR       : A.S.Musthafa                                          *
 * DESCRIPTION  : Contains entry functions for DHCP CLIENT              *
 ************************************************************************/

/************************************************************************
 * GLOBAL MACRO need to be defined here                                 *
 ************************************************************************/
#define _DHCPC_GLOB_VAR

/************************************************************************
 * Include files                                                         
 ************************************************************************/
#include "dhccmn.h"

extern tRegDhcpDiscOption gRegDhcpDiscOption;
/************************************************************************/
/*  Function Name   : DhcpClientShutDown                                */
/*  Description     : This function frees all the resources allocated   */
/*                    for Dhcp Client module.                           */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpClientShutDown (VOID)
{
    DHCPC_TRC (DEBUG_TRC, "Entered DhcpClientShutDown\n");

    OsixSemDel (gDhcpCSemId);
    gDhcpCSemId = 0;

    OsixQueDel (gDhcpCMsgQId);
    gDhcpCMsgQId = 0;

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
	if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
	{
		OsixQueDel (gDhcpCArpMsgQId);
		gDhcpCArpMsgQId = 0;
	}
#endif

    if (TmrDeleteTimerList (DhcpCTimerListId) != TMR_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC, "FAILURE in TmrDeleteTimerList\n");
    }
    DhcpCTimerListId = 0;

    if (DhcpCSocketClose () != DHCP_SUCCESS)
    {
        DHCPC_TRC (FAIL_TRC, "FAILURE in closing the DHCP client socket \n");
    }

    OsixTskDel (gu4DhcpCTaskId);
    gu4DhcpCTaskId = 0;

    OsixTskDel (gu4DhcpCSelectTskId);
    gu4DhcpCSelectTskId = 0;

#if defined (LNXIP4_WANTED) && !defined (KERNEL_WANTED)
	if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
	{
		OsixTskDel (gu4DhcpCArpTaskId);
		gu4DhcpCArpTaskId = 0;
	}
#endif

    DhCRedDeInitGlobalInfo ();

    /* Delete All the DHCP CLient Records */
    DhcpCMemDeInit ();

    DHCPC_TRC (DEBUG_TRC, "\nExitting DhcpClientShutDown");

    return;
}                                /* End of DhcpClientShutDown function */

/************************************************************************/
/*  Function Name   : DhcpCopyDefaultOpt                                */
/*  Description     : Copy the default option into the Client Conf      */
/*                    structure in the global interface table           */
/*  Input(s)        : pDhcpCNode - DHCP Clinet Interface Record         */
/*  Output(s)       : DhcpClientConfInfo.DefaultOpt.u4LenOfOptions      */
/*  Return          : None                                              */
/************************************************************************/
VOID
DhcpCopyDefaultOpt (tDhcpCRec * pDhcpCNode)
{
    tSNMP_OCTET_STRING_TYPE DhcpClientIdEntry;
    UINT4               u4CfaIfIndex = 0;
    tDhcpCOptions      *pDefaultOptNode;
    UINT1               u1Offset = 0;
    UINT1              *pTempDefaultOpt;
    UINT1               u1LengthOffset = 0;
    tTMO_SLL            DhcpConfigOptList;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT1               au1ClientId[DHCP_MAX_CLIENTID_LEN];
    INT1                iCnt = 0;
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    DHCPC_TRC (DEBUG_TRC, "Entered DhcpCopyDefaultOpt fn\n");

    DhcpConfigOptList =
        (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.DefaultOptList;

    /* Default options are hardcoded here */
    if ((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions == NULL)
    {
        pTempDefaultOpt =
            (UINT1 *) MemAllocMemBlk (gDhcpClientMemPoolId.
                                      DhcpCClientDefOptPoolId);
        if (pTempDefaultOpt == NULL)
        {
            return;
        }
        (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions =
            pTempDefaultOpt;
    }
    else
    {
        pTempDefaultOpt =
            (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.pDefaultOptions;
    }

    /*  Default DHCP Client requesting parameter list */
    if (NetIpv4GetCfaIfIndexFromPort (pDhcpCNode->u4IpIfIndex,
                                      &u4CfaIfIndex) != NETIPV4_SUCCESS)
    {
        return;
    }

    MEMSET (&DhcpClientIdEntry, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpClientIdEntry.pu1_OctetList = &au1ClientId[0];
    MEMSET (DhcpClientIdEntry.pu1_OctetList, 0, DHCP_MAX_CLIENTID_LEN);
    DhcpClientIdEntry.i4_Length = 0;
    DhcpCGetClientIdentifer (u4CfaIfIndex,
                             DhcpClientIdEntry.pu1_OctetList,
                             &(DhcpClientIdEntry.i4_Length));

    if (DhcpClientIdEntry.i4_Length != 0)
    {
        *(pTempDefaultOpt + u1Offset) = DHCP_OPT_CLIENT_ID;
        u1Offset++;
        *(pTempDefaultOpt + u1Offset) = (UINT1) DhcpClientIdEntry.i4_Length;
        u1Offset++;
        MEMCPY ((pTempDefaultOpt + u1Offset), DhcpClientIdEntry.pu1_OctetList,
                DhcpClientIdEntry.i4_Length);
        u1Offset = (UINT1) (u1Offset + DhcpClientIdEntry.i4_Length);
    }

    /* Storing the subnet mask and other options */
    *(pTempDefaultOpt + u1Offset) = DHCP_OPT_PARAMETER_LIST;
    u1Offset++;

    /* Default Parameter Request List length is 4. 
     * if any configurable option is enabled means,
     * update the length of Parameter Request List */
    *(pTempDefaultOpt + u1Offset) = DHCP_DEF_PARAMETERS_LEN;
    u1LengthOffset = u1Offset;
    u1Offset++;

    *(pTempDefaultOpt + u1Offset) = DHCP_OPT_SUBNET_MASK;
    u1Offset++;
    *(pTempDefaultOpt + u1Offset) = DHCP_OPT_DNS_NAME;
    u1Offset++;

    for (iCnt = 0; iCnt < gRegDhcpDiscOption.i1OptionCnt; iCnt++)
    {
        *(pTempDefaultOpt + u1Offset) =
            (UINT1) gRegDhcpDiscOption.au1Data[iCnt];
        u1Offset++;
        *(pTempDefaultOpt + u1LengthOffset) =
            (UINT1) (*(pTempDefaultOpt + u1LengthOffset) + 1);
    }

    /* Get the required options from the SLL present */
    pDefaultOptNode = (tDhcpCOptions *) TMO_SLL_First (&DhcpConfigOptList);

    if (pDefaultOptNode != NULL)
    {
        TMO_SLL_Scan (&DhcpConfigOptList, pDefaultOptNode, tDhcpCOptions *)
        {
            if (u1Offset < DHCP_MAX_OPT_LEN)
            {
                *(pTempDefaultOpt + u1Offset) = (UINT1) pDefaultOptNode->u1Type;
                u1Offset++;
            }
            if (u1Offset < DHCP_MAX_OPT_LEN)
            {
                *(pTempDefaultOpt + u1Offset) = (UINT1) pDefaultOptNode->u1Len;
                u1Offset++;
            }
            MEMCPY (pTempDefaultOpt + u1Offset, pDefaultOptNode->au1OptionVal,
                    pDefaultOptNode->u1Len);
            u1Offset = (UINT1) (u1Offset + (pDefaultOptNode->u1Len));
        }

        DHCPC_TRC (DEBUG_TRC, "Copied Default Options successfully\n");
    }
    else
    {
        DHCPC_TRC (DEBUG_TRC,
                   "No Required option present. Exiting DhcpCopyDefaultOpt fn\n");
    }

    /* ADD DNS AND SNTP TO THE REQ OPTION LIST */
    if (DhcpcConfReqOptionType (u4CfaIfIndex, DHCP_OPT_DNS_NS, DHCPC_SET_OPTION)
        == DHCP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC,
                   "DNS option cant be added in the request option list\n");
        return;
    }
    else
    {
        pDhcpCReqOption =
            (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                      DHCP_OPT_DNS_NS);
        if (pDhcpCReqOption != NULL)
            pDhcpCReqOption->u1Status = DHCP_ENABLE;
    }

    if (DhcpcConfReqOptionType (u4CfaIfIndex, DHCP_OPT_ROUTER_OPTION, DHCPC_SET_OPTION)
        == DHCP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC,
                   "DHCP_OPT_ROUTER_OPTION option cant be added in the request option list\n");
        return;
    }
    else
    {
        pDhcpCReqOption =
            (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                      DHCP_OPT_ROUTER_OPTION);
        if (pDhcpCReqOption != NULL)
            pDhcpCReqOption->u1Status = DHCP_ENABLE;
    }


    if (DhcpcConfReqOptionType
        (u4CfaIfIndex, DHCP_OPT_NTP_SERVERS, DHCPC_SET_OPTION) == DHCP_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC,
                   "NTP option cant be added in the request option list\n");
        return;
    }
    else
    {
        pDhcpCReqOption =
            (tDhcpCOptions *) DhcpCGetReqOptionEntry (u4CfaIfIndex,
                                                      DHCP_OPT_NTP_SERVERS);
        if (pDhcpCReqOption != NULL)
            pDhcpCReqOption->u1Status = DHCP_ENABLE;
    }

    pRBElem = RBTreeGetFirst (gDhcpCReqOptionList);

    while (pRBElem != NULL)
    {
        pDefaultOptNode = (tDhcpCOptions *) pRBElem;

        if ((u4CfaIfIndex == pDefaultOptNode->u4IfIndex)
            && (u1Offset < DHCP_MAX_OPT_LEN))
        {
            /* Add all requested options other than option 60,
             * as its TLV will be added separately */
            if (pDefaultOptNode->u1Type != DHCP_OPT_CVENDOR_SPECIFIC)
            {
                *(pTempDefaultOpt + u1Offset) = (UINT1) pDefaultOptNode->u1Type;
                u1Offset++;
                /* Increment the Parameter Request List length */
                *(pTempDefaultOpt + u1LengthOffset) =
                    (UINT1) (*(pTempDefaultOpt + u1LengthOffset) + 1);
            }
        }
        pRBNextElem = RBTreeGetNext (gDhcpCReqOptionList, pRBElem, NULL);
        pRBElem = pRBNextElem;
    }

    (pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.u2LenOfOptions =
        (UINT2) u1Offset;

    DHCPC_TRC (DEBUG_TRC, "Exiting DhcpCopyDefaultOpt fn\n");

    return;
}                                /* End of DhcpCopyDefaultOpt function */

/************************************************************************/
/*  Function Name   : DhcpCValidateIpAddress                            */
/*  Description     : Validate the IP Address                           */
/*  Input(s)        : u4IpAddr-> IP Address read from the config file   */
/*  Output(s)       : None                                              */
/*  Return          : S_INIT or S_INITREBOOT                            */
/************************************************************************/
UINT1
DhcpCValidateIpAddress (UINT4 u4IpAddr)
{
    if ((u4IpAddr == 0) || (u4IpAddr == DHCPC_AUTO_CONFIG_IP_ADDRESS))
    {
        return S_INIT;
    }

    if (((u4IpAddr & 0xff000000) == IP_LOOPBACK_ADDRESS))
    {
        return S_INIT;
    }

    if ((IP_IS_ADDR_CLASS_E (u4IpAddr) == TRUE) ||
        (((u4IpAddr & 0xf8000000) == 0xf8000000) &&
         u4IpAddr != IP_GEN_BCAST_ADDR))
    {
        return S_INIT;
    }

    if (IP_ADDRESS_ABOVE_CLASS_C (u4IpAddr))
    {
        /* IP Address > Class C address */
        return S_INIT;
    }

    return S_INITREBOOT;
}                                /* End of DhcpCValidateIpAddress function */
