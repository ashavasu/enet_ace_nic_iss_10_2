/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: fsdhclow.c,v 1.46 2015/11/20 10:20:00 siva Exp $
 * 
 * DESCRIPTION    : Contains low level routines for DHCP CLIENT          
 ********************************************************************/

/************************************************************************
 * Include files                                                        *
 ************************************************************************/
# include "snmctdfs.h"
# include "snmccons.h"
# include "fsdhclwr.h"
# include "dhccmn.h"
# include "midconst.h"
# include "dhcpccli.h"

extern UINT4        DhcpClientFastAccessDiscoverTimeOut[10];
extern UINT4        DhcpClientFastAccessArpCheckTimeOut[10];
extern UINT4        DhcpClientFastAccessNullStateTimeOut[10];

/* LOW LEVEL Routines for Table : DhcpClientConfigTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpClientConfigTable
 Input       :  The Indices
                DhcpClientConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDhcpClientConfigTable (INT4 i4DhcpClientConfigIfIndex)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pDhcpCNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpClientConfigTable
 Input       :  The Indices
                DhcpClientConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDhcpClientConfigTable (INT4 *pi4DhcpClientConfigIfIndex)
{
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4NextCfaIfIndex;

    if (DhcpCGetNextMgmtIfIndex (u4CfaIfIndex, &u4NextCfaIfIndex)
        == SNMP_SUCCESS)
    {
        *pi4DhcpClientConfigIfIndex = (INT4) u4NextCfaIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpClientConfigTable
 Input       :  The Indices
                DhcpClientConfigIfIndex
                nextDhcpClientConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDhcpClientConfigTable (INT4 i4DhcpClientConfigIfIndex,
                                      INT4 *pi4NextDhcpClientConfigIfIndex)
{
    UINT4               u4CfaIfIndex = (UINT4) i4DhcpClientConfigIfIndex;
    UINT4               u4NextCfaIfIndex;

    if (DhcpCGetNextMgmtIfIndex (u4CfaIfIndex, &u4NextCfaIfIndex)
        == SNMP_SUCCESS)
    {
        *pi4NextDhcpClientConfigIfIndex = (INT4) u4NextCfaIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientRenew
 Input       :  The Indices
                DhcpClientConfigIfIndex

                The Object 
                retValDhcpClientRenew
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientRenew (INT4 i4DhcpClientConfigIfIndex,
                       INT4 *pi4RetValDhcpClientRenew)
{
    UINT1               u1State = S_UNUSED;
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;
    *pi4RetValDhcpClientRenew =
        (u1State == S_RENEWING) ? DHCP_SET : DHCP_NOT_SET;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientRebind
 Input       :  The Indices

                The Object 
                retValDhcpClientRebind
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientRebind (INT4 i4DhcpClientConfigIfIndex,
                        INT4 *pi4RetValDhcpClientRebind)
{
    UINT1               u1State = S_UNUSED;
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    *pi4RetValDhcpClientRebind =
        (u1State == S_REBINDING) ? DHCP_SET : DHCP_NOT_SET;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientInform
 Input       :  The Indices

                The Object 
                retValDhcpClientInform
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientInform (INT4 i4DhcpClientConfigIfIndex,
                        INT4 *pi4RetValDhcpClientInform)
{
    UINT1               u1State = S_UNUSED;
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    *pi4RetValDhcpClientInform =
        (u1State == S_REQUESTING) ? DHCP_SET : DHCP_NOT_SET;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientRelease
 Input       :  The Indices

                The Object 
                retValDhcpClientRelease
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientRelease (INT4 i4DhcpClientConfigIfIndex,
                         INT4 *pi4RetValDhcpClientRelease)
{
    UINT1               u1State = S_UNUSED;
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    *pi4RetValDhcpClientRelease = (u1State == S_INIT) ? DHCP_SET : DHCP_NOT_SET;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientIdentifier
 Input       :  The Indices
                DhcpClientConfigIfIndex

                The Object 
                retValDhcpClientIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientIdentifier (INT4 i4DhcpClientConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValDhcpClientIdentifier)
{
    return (DhcpCGetClientIdentifer ((UINT4) i4DhcpClientConfigIfIndex,
                                     pRetValDhcpClientIdentifier->pu1_OctetList,
                                     &(pRetValDhcpClientIdentifier->
                                       i4_Length)));

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpClientRenew
 Input       :  The Indices

                The Object 
                setValDhcpClientRenew
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientRenew (INT4 i4DhcpClientConfigIfIndex,
                       INT4 i4SetValDhcpClientRenew)
{
    UINT4               u4TimeLeft;
    UINT4               u4LeaseTime;
    UINT4               u4Secs;
    UINT4               u4AvailRebindTime;
    UINT4               u4CurrentRebindTime;
    UINT4               u4CurrentTime;
    UINT1               u1RetVal;
    UINT4               u4Port;
    UINT4               u4RemainingTime;
    UINT1               u1State;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1AllocProto;

    UNUSED_PARAM (i4SetValDhcpClientRenew);

    if (CfaIfGetIpAllocProto (i4DhcpClientConfigIfIndex, &u1AllocProto)
        == CFA_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in CfaIfGetIpAllocProto fn\n");
        return SNMP_FAILURE;
    }

    if (u1AllocProto != CFA_PROTO_DHCP)
    {
        DHCPC_TRC (DEBUG_TRC, "Protocol used is not DHCP\n");
        return SNMP_FAILURE;
    }
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    if ((u1State != S_BOUND) && (u1State != S_RENEWING) && (u1State != S_INIT))
    {
        DHCPC_TRC (FAIL_TRC,
                   "Exit nmhSetDhcpClientRenew State not in S_BOUND\n");
        return SNMP_FAILURE;
    }
    if ((u1State == S_INIT) || (u1State == S_RENEWING))
    {
        (pDhcpCNode->DhcIfInfo).u1State = S_INIT;
        /* Stop if there is any Lease Timer or Reply timers are running !!
         */
        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        DhcpCStopTimer (DhcpCTimerListId,
                        &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));

        DhcpCfaInterfaceIpAddrConfig (pDhcpCNode, DHCPC_AUTO_CONFIG_IP_ADDRESS);

        DhcpCNotify (pDhcpCNode->u4IpIfIndex, DHCPC_ACQUIRE_IP);
        return SNMP_SUCCESS;
    }
    else
    {

        /* Check whether the rebind timer is running or not */
        u1RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->LeaseTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        if (u1RetVal == DHCP_SUCCESS)
            /* Stop the Renewal timer */
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        /* Get the secs at which the IP Acquisition has started */
        u4Secs = (pDhcpCNode->DhcIfInfo).u4Secs;

        /* Get the active lease time offerred by the server */
        u4LeaseTime = (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred;

        /* As per RFC 2131 which says that the rebind time is 0.875 of Lease */
        u4CurrentRebindTime = (UINT4) (u4Secs + (u4LeaseTime * 0.875));

        /* Get the current time in secs */
        DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

        /* Currently available Rebind Time */
        u4AvailRebindTime = u4CurrentRebindTime - u4CurrentTime;

        if (u4AvailRebindTime > 0)
        {
            pDhcpCNode->u2RenewTimerCount =
                (UINT2) (u4AvailRebindTime / DHCPC_MAX_TIME_OUT);
            if (pDhcpCNode->u2RenewTimerCount != 0)
            {

                /*Fsap Timer supports only for 7 days, if renew time is more than 
                 * 7 days, u2RenewTimerCount is used. u2RenewTimerCount is 
                 * (Configured Val/ Max Fsap supported value). On timer expiry u2RenewTimerCount will
                 * be decremented and the corresponding action for the timer expiry will be done
                 * only when the u2RenewTimerCount becomes zero, until then the timer will 
                 * be restarted.*/

                u4RemainingTime = u4AvailRebindTime % DHCPC_MAX_TIME_OUT;
                if (u4RemainingTime == 0)
                {
                    u4AvailRebindTime = DHCPC_MAX_TIME_OUT;
                    pDhcpCNode->u2RenewTimerCount =
                        (UINT2) (pDhcpCNode->u2RenewTimerCount - 1);
                }
                else
                {
                    u4AvailRebindTime = u4RemainingTime;
                }
            }

            /* Start a timer with available rebind time */
            if (DhcpCInitTimer (pDhcpCNode, DHCPC_REBIND_TIMER_ID,
                                u4AvailRebindTime) == DHCP_SUCCESS)
            {
                /* Send a DHCP_REQUEST to the leasing server to extend the 
                   lease time available */
                DhcpCSendRequest (pDhcpCNode);
                (pDhcpCNode->DhcIfInfo).u1State = S_RENEWING;
            }
            else
            {
                DHCPC_TRC (FAIL_TRC, "Starting REBIND_TIMER failed \r\n");
                DhcpCInitState (pDhcpCNode);
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* stop if there is any timers are running, this check useful
             *  when user tryes to do repeated release and renew
             */

            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));

            DHCPC_TRC (DEBUG_TRC,
                       "Exiting nmhSetDhcpClientRenew. Since Rebind Time Available is less than 0\n");
            (pDhcpCNode->DhcIfInfo).u1State = S_INIT;
            DhcpCNotify (pDhcpCNode->u4IpIfIndex, DHCPC_ACQUIRE_IP);
        }

        DHCPC_TRC (DEBUG_TRC, "Exiting nmhSetDhcpClientRenew fn\n");

        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetDhcpClientRebind
 Input       :  The Indices

                The Object 
                setValDhcpClientRebind
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientRebind (INT4 i4DhcpClientConfigIfIndex,
                        INT4 i4SetValDhcpClientRebind)
{
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr;
    UINT4               u4CurrentTime;
    UINT4               u4RetVal = DHCP_FAILURE;
    UINT4               u4TimeLeft;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4Port;
    UINT1               u1State;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1AllocProto;

    DHCPC_TRC (DEBUG_TRC, "Entering nmhSetDhcpClientRebind function\n");

    UNUSED_PARAM (i4SetValDhcpClientRebind);

    if (CfaIfGetIpAllocProto (i4DhcpClientConfigIfIndex, &u1AllocProto)
        == CFA_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in CfaIfGetIpAllocProto fn\n");
        return SNMP_FAILURE;
    }

    if (u1AllocProto != CFA_PROTO_DHCP)
    {
        DHCPC_TRC (DEBUG_TRC, "Protocol used is not DHCP\n");
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    /* Interface index - temporarily asssigned since single interface
     * support */
    if ((u1State == S_RENEWING) ||
        (u1State == S_BOUND) || (u1State == S_REBINDING))
    {
        /* Check whether the lease timer is running or not */
        u4RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->LeaseTimer.
                                            DhcpAppTimer), &u4TimeLeft);

        /* Stop the lease timer */
        if (u4RetVal == DHCP_SUCCESS)
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->LeaseTimer.DhcpAppTimer));

        /* Stop the reply wait timer if it is running */
        u4RetVal = DhcpCGetRemainingTime (DhcpCTimerListId,
                                          &(pDhcpCNode->ReplyWaitTimer.
                                            DhcpAppTimer), &u4TimeLeft);
        if (u4RetVal == DHCP_SUCCESS)
        {
            DhcpCStopTimer (DhcpCTimerListId,
                            &(pDhcpCNode->ReplyWaitTimer.DhcpAppTimer));
            /* Reset the retry count */
            pDhcpCNode->u1ReTxCount = DHCPC_RESET;
        }

        if ((pOutPkt = DhcpCCreateOutPkt ()) == NULL)
        {
            DHCPC_TRC (FAIL_TRC,
                       "Outgoing packet create FAILURE in nmhSetDhcpClientRebind\n");
            return SNMP_FAILURE;
        }

        pOutPkt->u4IfIndex = pDhcpCNode->u4IpIfIndex;

        pOutHdr = &(pOutPkt->DhcpMsg);

        /* Start filling the header of DHCP for DHCP_REQUEST packet */
        pOutHdr->Op = BOOTREQUEST;
        pOutHdr->htype = DHCPC_GET_HW_TYPE (u2Port);
        pOutHdr->hlen = DHCPC_GET_HW_TYPE_LEN (u2Port);
        pOutHdr->hops = 0;

        /* Broadcast the packet , if the state of the interface needs unicasting       the flags is overwritten in the switch statement given below */

        pOutHdr->flags = DHCP_BROADCAST_MASK;
        pOutHdr->giaddr = 0;

        DHCPC_GET_TIME_IN_SECS (u4CurrentTime);

        (pDhcpCNode->DhcIfInfo).u4Secs = u4CurrentTime;

        /* Fill the secs field in the outgoing packet */
        pOutHdr->secs = (UINT2) u4CurrentTime;

        pOutHdr->ciaddr = 0;
        pOutHdr->siaddr = 0;
        pOutHdr->yiaddr = 0;

        DhcpCfaGddGetHwAddr (pOutPkt->u4IfIndex, pOutHdr->chaddr);

        MEMSET (pOutHdr->sname, 0, DHCP_SERVER_NAME_LEN);
        MEMSET (pOutHdr->file, 0, DHCP_FILE_NAME_LEN);

        pOutHdr->xid = (pDhcpCNode->DhcIfInfo).u4Xid;
        pOutHdr->ciaddr = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;

        /* Fill the required options in the outgoing packet */
        DhcpCFillRequiredOptions (pOutPkt,
                                  (pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.
                                  DhcpMsg.pOptions,
                                  (pDhcpCNode->DhcIfInfo).pBestOffer->PktInfo.
                                  u2Len);

        DhcpCRemoveOptionType (DHCP_OPT_SERVER_ID, pOutPkt);
        DhcpCRemoveOptionType (DHCP_OPT_REQUESTED_IP, pOutPkt);

        u4RetVal = DhcpCSendMessage (pOutPkt, DHCP_REQUEST);

        if (u4RetVal == DHCP_SUCCESS)
        {
            /* DHCPC_TRC : */
            DHCPC_TRC2 (DEBUG_TRC,
                        "DHCP_Request Message is Successfully Sent in the Interface %x in the %d state \n",
                        pDhcpCNode->u4IpIfIndex,
                        (pDhcpCNode->DhcIfInfo).u1State);
            /* Increment the statistics */
            (pDhcpCNode->TxRxStats).u4DhcpCRequestSent++;

            (pDhcpCNode->DhcIfInfo).u1State = S_REBINDING;

            /* Start a request timer within which a DHCP_ACK or DHCP_NACK 
             * should have recieved */
            if (DhcpCInitTimer (pDhcpCNode, DHCPC_REQUEST_TIMER_ID,
                                DHCPC_REQUEST_RETX_TIME_OUT) == DHCP_FAILURE)
            {
                DHCPC_TRC (FAIL_TRC,
                           "Starting DHCPC_REQUEST_TIMER_ID failed \r\n");
                MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                                    (UINT1 *) pOutPkt->DhcpMsg.pOptions);
                MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                                    (UINT1 *) pOutPkt);
                DhcpCInitState (pDhcpCNode);
                return SNMP_FAILURE;
            }
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            DHCPC_TRC2 (FAIL_TRC,
                        "FAILURE in sending DHCP_REQUEST Message in Interface %x in %d state\n",
                        pDhcpCNode->u4IpIfIndex,
                        (pDhcpCNode->DhcIfInfo).u1State);

            i1RetVal = SNMP_FAILURE;
        }

        /* free OutPkt Message Structure */
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCDhcpMsgOptPoolId,
                            (UINT1 *) pOutPkt->DhcpMsg.pOptions);
        MemReleaseMemBlock (gDhcpClientMemPoolId.DhcpCOutPktPoolId,
                            (UINT1 *) pOutPkt);

        DHCPC_TRC (DEBUG_TRC, "Exiting nmhSetDhcpClientRebind function\n");

        return i1RetVal;
    }
    else
    {
        DHCPC_TRC (FAIL_TRC,
                   "Not in S_BOUND or S_RENEWING or S_BINDING state\n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetDhcpClientInform
 Input       :  The Indices

                The Object 
                setValDhcpClientInform
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientInform (INT4 i4DhcpClientConfigIfIndex,
                        INT4 i4SetValDhcpClientInform)
{
    UINT1               u1RetVal;
    UINT1               u1State;
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1AllocProto;

    /* This function assumes that the IP Address is configured externally
     * for the interface so take care */

    DHCPC_TRC (DEBUG_TRC, "Entering nmhSetDhcpClientInform function\n");

    UNUSED_PARAM (i4SetValDhcpClientInform);

    if (CfaIfGetIpAllocProto (i4DhcpClientConfigIfIndex, &u1AllocProto)
        == CFA_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in CfaIfGetIpAllocProto fn\n");
        return SNMP_FAILURE;
    }

    if (u1AllocProto != CFA_PROTO_DHCP)
    {
        DHCPC_TRC (DEBUG_TRC, "Protocol used is not DHCP\n");
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    /* If a valid IP Address is associated with the Interface
     *  Send Inform Message */

    if (u1State == S_BOUND)
    {
        /* No Need to stop the reply wait timer, because it is taken care in
         * DhcpCSendInform function */
        pDhcpCNode->u1ReTxCount = DHCPC_RESET;

        u1RetVal = DhcpCSendInform (pDhcpCNode);

        if (u1RetVal != DHCP_SUCCESS)
        {
            DHCPC_TRC1 (FAIL_TRC, "FAILURE in sending INFORM for Intf %x\n",
                        pDhcpCNode->u4IpIfIndex);
            return SNMP_FAILURE;
        }
        (pDhcpCNode->DhcIfInfo).u1State = S_REBOOTING;

        DHCPC_TRC (DEBUG_TRC, "Exiting nmhSetDhcpClientInform fn\n");

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDhcpClientRelease
 Input       :  The Indices

                The Object 
                setValDhcpClientRelease
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientRelease (i4DhcpClientConfigIfIndex, i4SetValDhcpClientRelease)
     INT4                i4DhcpClientConfigIfIndex;
     INT4                i4SetValDhcpClientRelease;

{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1AllocProto;

    UNUSED_PARAM (i4SetValDhcpClientRelease);

    if (CfaIfGetIpAllocProto (i4DhcpClientConfigIfIndex, &u1AllocProto)
        == CFA_FAILURE)
    {
        DHCPC_TRC (DEBUG_TRC, "FAILURE in CfaIfGetIpAllocProto fn\n");
        return SNMP_FAILURE;
    }

    if (u1AllocProto != CFA_PROTO_DHCP)
    {
        DHCPC_TRC (DEBUG_TRC, "Protocol used is not DHCP\n");
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (DhcpCProcessRelease (pDhcpCNode, OSIX_TRUE) != DHCP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpClientIdentifier
 Input       :  The Indices
                DhcpClientConfigIfIndex

                The Object 
                setValDhcpClientIdentifier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientIdentifier (INT4 i4DhcpClientConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValDhcpClientIdentifier)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = DhcpCSetClientIdentifier ((UINT4) i4DhcpClientConfigIfIndex,
                                            pSetValDhcpClientIdentifier->
                                            pu1_OctetList,
                                            pSetValDhcpClientIdentifier->
                                            i4_Length);
    if (i4RetStatus == DHCP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpClientRenew
 Input       :  The Indices

                The Object 
                testValDhcpClientRenew
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientRenew (UINT4 *pu4ErrorCode, INT4 i4DhcpClientConfigIfIndex,
                          INT4 i4TestValDhcpClientRenew)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1State;

    if (i4TestValDhcpClientRenew != DHCP_SET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPC_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        CLI_SET_ERR (CLI_DHCPC_DIS_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPC_DIS_STATUS_ERR);
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    if ((u1State != S_BOUND) && (u1State != S_RENEWING) && (u1State != S_INIT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPC_INV_RENEW_STATE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpClientRebind
 Input       :  The Indices

                The Object 
                testValDhcpClientRebind
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientRebind (UINT4 *pu4ErrorCode, INT4 i4DhcpClientConfigIfIndex,
                           INT4 i4TestValDhcpClientRebind)
{
    UNUSED_PARAM (i4DhcpClientConfigIfIndex);

    if (i4TestValDhcpClientRebind != DHCP_SET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpClientInform
 Input       :  The Indices

                The Object 
                testValDhcpClientInform
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientInform (UINT4 *pu4ErrorCode, INT4 i4DhcpClientConfigIfIndex,
                           INT4 i4TestValDhcpClientInform)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1State;

    UNUSED_PARAM (i4DhcpClientConfigIfIndex);

    if (i4TestValDhcpClientInform != DHCP_SET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    if (u1State != S_BOUND)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpClientRelease
 Input       :  The Indices

                The Object 
                testValDhcpClientRelease
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientRelease (UINT4 *pu4ErrorCode, INT4 i4DhcpClientConfigIfIndex,
                            INT4 i4TestValDhcpClientRelease)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;
    UINT1               u1State;

    if (i4TestValDhcpClientRelease != DHCP_SET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPC_INV_STATUS);
        return SNMP_FAILURE;
    }
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        CLI_SET_ERR (CLI_DHCPC_DIS_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPC_DIS_STATUS_ERR);
        return SNMP_FAILURE;
    }

    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    if (((u1State != S_BOUND) ||
         (u1State == S_RENEWING) || (u1State == S_REBINDING)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPC_INV_RELEASE_STATE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpClientIdentifier
 Input       :  The Indices
                DhcpClientConfigIfIndex

                The Object 
                testValDhcpClientIdentifier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientIdentifier (UINT4 *pu4ErrorCode,
                               INT4 i4DhcpClientConfigIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValDhcpClientIdentifier)
{
    UINT4               u4Port;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientConfigIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        return SNMP_FAILURE;
    }

    if (pTestValDhcpClientIdentifier->i4_Length < 0
        || pTestValDhcpClientIdentifier->i4_Length > DHCP_MAX_CLIENTID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpClientConfigTable
 Input       :  The Indices
                DhcpClientConfigIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpClientDebugTrace
 Input       :  The Indices

                The Object 
                retValDhcpClientDebugTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientDebugTrace (INT4 *pi4RetValDhcpClientDebugTrace)
{
    *pi4RetValDhcpClientDebugTrace = gu4DhcpCDebugMask;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpClientDebugTrace
 Input       :  The Indices

                The Object 
                setValDhcpClientDebugTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientDebugTrace (INT4 i4SetValDhcpClientDebugTrace)
{
    gu4DhcpCDebugMask = i4SetValDhcpClientDebugTrace;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpClientDebugTrace
 Input       : 
                The Object 
                testValDhcpClientDebugTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientDebugTrace (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDhcpClientDebugTrace)
{
    if ((i4TestValDhcpClientDebugTrace >= DISABLE_ALL_TRC) &&
        (i4TestValDhcpClientDebugTrace <= ENABLE_ALL_TRC))
        return SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_DHCPC_INV_TRC);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpClientDebugTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientDebugTrace (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpClientCounterTable. */

/* LOW LEVEL Routines for Table : DhcpClientCounterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpClientCounterTable
 Input       :  The Indices
                DhcpClientIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDhcpClientCounterTable (INT4 i4DhcpClientIfIndex)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pDhcpCNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpClientCounterTable
 Input       :  The Indices
                DhcpClientIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDhcpClientCounterTable (INT4 *pi4DhcpClientIfIndex)
{
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4NextCfaIfIndex;

    if (DhcpCGetNextMgmtIfIndex (u4CfaIfIndex, &u4NextCfaIfIndex)
        == SNMP_SUCCESS)
    {
        *pi4DhcpClientIfIndex = (INT4) u4NextCfaIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpClientCounterTable
 Input       :  The Indices
                DhcpClientIfIndex
                nextDhcpClientIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDhcpClientCounterTable (INT4 i4DhcpClientIfIndex,
                                       INT4 *pi4NextDhcpClientIfIndex)
{
    UINT4               u4CfaIfIndex = (UINT4) i4DhcpClientIfIndex;
    UINT4               u4NextCfaIfIndex;

    if (DhcpCGetNextMgmtIfIndex (u4CfaIfIndex, &u4NextCfaIfIndex)
        == SNMP_SUCCESS)
    {
        *pi4NextDhcpClientIfIndex = (INT4) u4NextCfaIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountDiscovers
 Input       :  The Indices

                The Object 
                retValDhcpCountDiscovers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCountDiscovers (INT4 i4DhcpClientIfIndex,
                                UINT4 *pu4RetValDhcpCountDiscovers)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDhcpCountDiscovers = (pDhcpCNode->TxRxStats).u4DhcpCDiscoverSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountRequests
 Input       :  The Indices

                The Object 
                retValDhcpCountRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCountRequests (INT4 i4DhcpClientIfIndex,
                               UINT4 *pu4RetValDhcpCountRequests)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDhcpCountRequests = (pDhcpCNode->TxRxStats).u4DhcpCRequestSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountReleases
 Input       :  The Indices

                The Object 
                retValDhcpCountReleases
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCountReleases (INT4 i4DhcpClientIfIndex,
                               UINT4 *pu4RetValDhcpCountReleases)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountReleases = (pDhcpCNode->TxRxStats).u4DhcpCReleaseSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountDeclines
 Input       :  The Indices

                The Object 
                retValDhcpCountDeclines
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCountDeclines (INT4 i4DhcpClientIfIndex,
                               UINT4 *pu4RetValDhcpCountDeclines)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountDeclines = (pDhcpCNode->TxRxStats).u4DhcpCDeclineSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountInforms
 Input       :  The Indices

                The Object 
                retValDhcpCountInforms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCountInforms (INT4 i4DhcpClientIfIndex,
                              UINT4 *pu4RetValDhcpCountInforms)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountInforms = (pDhcpCNode->TxRxStats).u4DhcpCInformSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountOffers
 Input       :  The Indices

                The Object 
                retValDhcpCountOffers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCountOffers (INT4 i4DhcpClientIfIndex,
                             UINT4 *pu4RetValDhcpCountOffers)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDhcpCountOffers = (pDhcpCNode->TxRxStats).u4DhcpCNoOfOffersRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountAcksInReqState
 Input       :  The Indices

                The Object 
                retValDhcpAcksInReqState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountAcksInReqState (INT4 i4DhcpClientIfIndex,
                               UINT4 *pu4RetValDhcpCountAcksInReqState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex, &u4Port)
        == IP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountAcksInReqState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREQ;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountNacksInReqState
 Input       :  The Indices

                The Object 
                retValDhcpCountNacksInReqState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountNacksInReqState (INT4 i4DhcpClientIfIndex,
                                UINT4 *pu4RetValDhcpCountNacksInReqState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountNacksInReqState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREQ;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountAcksInRenewState
 Input       :  The Indices

                The Object 
                retValDhcpCountAcksInRenewState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountAcksInRenewState (INT4 i4DhcpClientIfIndex,
                                 UINT4 *pu4RetValDhcpCountAcksInRenewState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountAcksInRenewState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInRENEW;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountNacksInRenewState
 Input       :  The Indices

                The Object 
                retValDhcpCountNacksInRenewState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountNacksInRenewState (INT4 i4DhcpClientIfIndex,
                                  UINT4 *pu4RetValDhcpCountNacksInRenewState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDhcpCountNacksInRenewState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInRENEW;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountAcksInRebindState
 Input       :  The Indices

                The Object 
                retValDhcpCountAcksInRebindState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountAcksInRebindState (INT4 i4DhcpClientIfIndex,
                                  UINT4 *pu4RetValDhcpCountAcksInRebindState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountAcksInRebindState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREBIND;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountNacksInRebindState
 Input       :  The Indices

                The Object 
                retValDhcpCountNacksInRebindState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountNacksInRebindState (INT4 i4DhcpClientIfIndex,
                                   UINT4 *pu4RetValDhcpCountNacksInRebindState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountNacksInRebindState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREBIND;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountAcksInRebootState
 Input       :  The Indices

                The Object 
                retValDhcpCountAcksInRebootState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountAcksInRebootState (INT4 i4DhcpClientIfIndex,
                                  UINT4 *pu4RetValDhcpCountAcksInRebootState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountAcksInRebootState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfAckRcvdInREBOOT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountNacksInRebootState
 Input       :  The Indices

                The Object 
                retValDhcpCountNacksInRebootState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountNacksInRebootState (INT4 i4DhcpClientIfIndex,
                                   UINT4 *pu4RetValDhcpCountNacksInRebootState)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountNacksInRebootState =
        (pDhcpCNode->TxRxStats).u4DhcpCNoOfNAckRcvdInREBOOT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountErrorInHeader
 Input       :  The Indices

                The Object 
                retValDhcpCountErrorInHeader
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountErrorInHeader (INT4 i4DhcpClientIfIndex,
                              UINT4 *pu4RetValDhcpCountErrorInHeader)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountErrorInHeader =
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientCountErrorInXid
 Input       :  The Indices

                The Object 
                retValDhcpCountErrorInXid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountErrorInXid (INT4 i4DhcpClientIfIndex,
                           UINT4 *pu4RetValDhcpCountErrorInXid)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountErrorInXid = (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInXid;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountErrorInOptions
 Input       :  The Indices

                The Object 
                retValDhcpCountErrorInOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountErrorInOptions (INT4 i4DhcpClientIfIndex,
                               UINT4 *pu4RetValDhcpCountErrorInOptions)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpCountErrorInOptions =
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInOptions;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientIpAddress
 Input       :  The Indices
                DhcpClientIfIndex

                The Object 
                retValDhcpClientIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientIpAddress (INT4 i4DhcpClientIfIndex,
                           UINT4 *pu4RetValDhcpClientIpAddress)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDhcpClientIpAddress = (pDhcpCNode->DhcIfInfo).u4IpAddressOfferred;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpClientLeaseTime
 Input       :  The Indices
                DhcpClientIfIndex

                The Object 
                retValDhcpClientLeaseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientLeaseTime (INT4 i4DhcpClientIfIndex,
                           INT4 *pi4RetValDhcpClientLeaseTime)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDhcpClientLeaseTime = (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpClientCounterReset
 Input       :  The Indices
                DhcpClientIfIndex

                The Object 
                retValDhcpClientCounterReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientCounterReset (INT4 i4DhcpClientIfIndex,
                              INT4 *pi4RetValDhcpClientCounterReset)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDhcpClientCounterReset =
        (pDhcpCNode->TxRxStats).u4DhcpCCounterReset;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDhcpClientRemainLeaseTime
 Input       :  The Indices
                DhcpClientIfIndex

                The Object 
                retValDhcpClientRemainLeaseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientRemainLeaseTime (INT4 i4DhcpClientIfIndex,
                                 INT4 *pi4RetValDhcpClientRemainLeaseTime)
{
    UINT4               u4CurrentTime;
    UINT4               u4Port;
    UINT1               u1State;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDhcpClientRemainLeaseTime = 0;
    u1State = (pDhcpCNode->DhcIfInfo).u1State;

    if ((u1State != S_BOUND) &&
        (u1State != S_RENEWING) && (u1State != S_REBINDING))
    {
        /* Since State is not proper, default value is returned */
        return SNMP_SUCCESS;
    }

#ifdef ALL_TRACE_WANTED
    DhcpCShowBinding (u2Port);
#endif

    /* Get the current time in secs */
    OsixGetSysTime (&u4CurrentTime);
    u4CurrentTime = (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    /* 
       Remaining time period of lease = Time of Expiration of Lease - Current time     Time Of Expiration of Lease    = u4StartTime + u4LeaseTime 
     */

    /* Here Remaining time period of lease should be greater than 0 
       for calculating the renewal and rebind time period */

    if (u4CurrentTime >
        ((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.
         u4StartOfLeaseTime + (pDhcpCNode->DhcIfInfo).u4LeaseTimeOfferred))
    {

        *pi4RetValDhcpClientRemainLeaseTime = 0;
    }
    else
    {
        *pi4RetValDhcpClientRemainLeaseTime =
            (INT4) ((pDhcpCNode->DhcIfInfo).DhcpClientConfInfo.
                    u4StartOfLeaseTime + (pDhcpCNode->DhcIfInfo).
                    u4LeaseTimeOfferred - u4CurrentTime);

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpClientCounterReset
 Input       :  The Indices
                DhcpClientIfIndex

                The Object 
                setValDhcpClientCounterReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientCounterReset (INT4 i4DhcpClientIfIndex,
                              INT4 i4SetValDhcpClientCounterReset)
{
    UINT4               u4Port;
    tDhcpCRec          *pDhcpCNode = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pDhcpCNode = DhcpCGetIntfRec (u4Port)) == NULL)
    {
        return SNMP_FAILURE;
    }
    (pDhcpCNode->TxRxStats).u4DhcpCCounterReset =
        i4SetValDhcpClientCounterReset;

    if ((pDhcpCNode->TxRxStats).u4DhcpCCounterReset == DHCP_SET)
    {
        DhcpCResetTxRxCounts (pDhcpCNode);
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInHeader = 0;
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInXid = 0;
        (pDhcpCNode->DhcpCErrCnt).u4DhcpCErrInOptions = 0;
        (pDhcpCNode->TxRxStats).u4DhcpCCounterReset = DHCP_NOT_SET;
        return SNMP_SUCCESS;
    }

    else if ((pDhcpCNode->TxRxStats).u4DhcpCCounterReset == DHCP_NOT_SET)
        return SNMP_SUCCESS;    /* Do Nothing */

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpClientCounterReset
 Input       :  The Indices
                DhcpClientIfIndex

                The Object 
                testValDhcpClientCounterReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientCounterReset (UINT4 *pu4ErrorCode, INT4 i4DhcpClientIfIndex,
                                 INT4 i4TestValDhcpClientCounterReset)
{
    UNUSED_PARAM (i4DhcpClientIfIndex);

    if ((i4TestValDhcpClientCounterReset == DHCP_SET) ||
        (i4TestValDhcpClientCounterReset == DHCP_NOT_SET))
        return SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function  :  nmhGetDhcpClientFastAccess
 Input     :  The Indices

              The Object
              retValDhcpClientFastAccess
 Output    :  The Get Low Lev Routine Take the Indices &
              store the Value requested in the Return val.
 Returns   :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientFastAccess (INT4 *pi4RetValDhcpClientFastAccess)
{
    *pi4RetValDhcpClientFastAccess = gDhcpCFastAcc.u1FastAccess;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function  :  nmhGetDhcpClientFastAccessDiscoverTimeOut
 Input     :  The Indices
 
              The Object
              retValDhcpClientFastAccessDiscoverTimeOut
 Output    :  The Get Low Lev Routine Take the Indices &
              store the Value requested in the Return val.
 Returns   :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetDhcpClientFastAccessDiscoverTimeOut (INT4
                                           *pi4RetValDhcpClientFastAccessDiscoverTimeOut)
{
    *pi4RetValDhcpClientFastAccessDiscoverTimeOut =
        gDhcpCFastAcc.u4FastAccDisTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetDhcpClientFastAccessNullStateTimeOut
 *  Input       :  The Indices
 *
 *                 The Object
 *                 retValDhcpClientFastAccessNullStateTimeOut
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetDhcpClientFastAccessNullStateTimeOut (INT4
                                            *pi4RetValDhcpClientFastAccessNullStateTimeOut)
{
    *pi4RetValDhcpClientFastAccessNullStateTimeOut =
        gDhcpCFastAcc.u4FastAccNullStTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetDhcpClientFastAccessArpCheckTimeOut
 *  Input       :  The Indices
 *    
 *                        The Object
 *                         retValDhcpClientFastAccessArpCheckTimeOut
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetDhcpClientFastAccessArpCheckTimeOut (INT4
                                           *pi4RetValDhcpClientFastAccessArpCheckTimeOut)
{
    *pi4RetValDhcpClientFastAccessArpCheckTimeOut =
        gDhcpCFastAcc.u4FastAccArpCheckTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetDhcpClientFastAccess
 *  Input       :  The Indices
 *
 *                 The Object
 *                 setValDhcpClientFastAccess
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetDhcpClientFastAccess (INT4 i4SetValDhcpClientFastAccess)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    if (i4SetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
    {
        gDhcpCFastAcc.u1FastAccess = DHCPC_FAST_ACC_ENABLE;
        gDhcpCFastAcc.u4FastAccNullStTimeOut = DHCPC_FAST_ACC_NULL_ST_TIMEOUT;
        gDhcpCFastAcc.u4FastAccDisTimeOut = DHCPC_FAST_ACC_DISCOVERY_TIME_OUT;
        gDhcpCFastAcc.u4FastAccArpCheckTimeOut =
            DHCPC_FAST_ACC_ARP_CHECK_MAX_TIMEOUT;
    }
    else
    {
        gDhcpCFastAcc.u1FastAccess = DHCPC_FAST_ACC_DISABLE;
        gDhcpCFastAcc.u4FastAccNullStTimeOut = DHCPC_NULL_STATE_WAIT_TIME_OUT;
        gDhcpCFastAcc.u4FastAccDisTimeOut = DHCPC_DISCOVER_RETX_TIME_OUT;
        gDhcpCFastAcc.u4FastAccArpCheckTimeOut = DHCPC_ARP_CHECK_TIME_OUT;
    }
/*  DhcpClientFastAccessNullStateTimeOut */
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, DhcpClientFastAccessNullStateTimeOut,
                          u4SeqNum, FALSE, DHCPC_PROTO_LOCK, DHCPC_PROTO_UNLOCK,
                          0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      gDhcpCFastAcc.u4FastAccNullStTimeOut));

/*  DhcpClientFastAccessDiscoverTimeOut */
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, DhcpClientFastAccessDiscoverTimeOut,
                          u4SeqNum, FALSE, DHCPC_PROTO_LOCK, DHCPC_PROTO_UNLOCK,
                          0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", gDhcpCFastAcc.u4FastAccDisTimeOut));

/*  DhcpClientFastAccessArpCheckTimeOut */
    RM_GET_SEQ_NUM (&u4SeqNum);
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, DhcpClientFastAccessArpCheckTimeOut,
                          u4SeqNum, FALSE, DHCPC_PROTO_LOCK, DHCPC_PROTO_UNLOCK,
                          0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      gDhcpCFastAcc.u4FastAccArpCheckTimeOut));
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetDhcpClientFastAccessDiscoverTimeOut
 *  Input       :  The Indices
 *
 *                 The Object
 *                 setValDhcpClientFastAccessDiscoverTimeOut
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetDhcpClientFastAccessDiscoverTimeOut (INT4
                                           i4SetValDhcpClientFastAccessDiscoverTimeOut)
{
    gDhcpCFastAcc.u4FastAccDisTimeOut =
        i4SetValDhcpClientFastAccessDiscoverTimeOut;
    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhSetDhcpClientFastAccessNullStateTimeOut
 *  Input       :  The Indices
 *                 The Object
 *                 setValDhcpClientFastAccessNullStateTimeOut
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetDhcpClientFastAccessNullStateTimeOut (INT4
                                            i4SetValDhcpClientFastAccessNullStateTimeOut)
{
    gDhcpCFastAcc.u4FastAccNullStTimeOut =
        i4SetValDhcpClientFastAccessNullStateTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetDhcpClientFastAccessArpCheckTimeOut
 *  Input       :  The Indices
 *
 *                 The Object
 *                 setValDhcpClientFastAccessArpCheckTimeOut
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDhcpClientFastAccessArpCheckTimeOut (INT4
                                           i4SetValDhcpClientFastAccessArpCheckTimeOut)
{
    gDhcpCFastAcc.u4FastAccArpCheckTimeOut =
        i4SetValDhcpClientFastAccessArpCheckTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhTestv2DhcpClientFastAccess
 *  Input       :  The Indices
 *
 *                 The Object
 *                 testValDhcpClientFastAccess
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *                 Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2DhcpClientFastAccess (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDhcpClientFastAccess)
{
    if ((i4TestValDhcpClientFastAccess != DHCPC_FAST_ACC_ENABLE) &&
        (i4TestValDhcpClientFastAccess != DHCPC_FAST_ACC_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
        return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhTestv2DhcpClientFastAccessDiscoverTimeOut
 *  Input       :  The Indices
 *
 *                 The Object
 *                 testValDhcpClientFastAccessDiscoverTimeOut
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *                 Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2DhcpClientFastAccessDiscoverTimeOut (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4TestValDhcpClientFastAccessDiscoverTimeOut)
{
    INT4                i4RetValDhcpClientFastAccess = 0;
    INT1                i1RetVal = 0;

    nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
    if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
    {
        if ((i4TestValDhcpClientFastAccessDiscoverTimeOut <
             DHCPC_FAST_ACC_DISCOVERY_MIN_TIMEOUT)
            || (i4TestValDhcpClientFastAccessDiscoverTimeOut >
                DHCPC_FAST_ACC_DISCOVERY_MAX_TIMEOUT))
        {

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPC_DISCOVER_FAST_ACCESS_DISABLE_ERR);
        i1RetVal = SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhTestv2DhcpClientFastAccessNullStateTimeOut
 *  Input       :  The Indices
 *
 *                 The Object
 *                 testValDhcpClientFastAccessNullStateTimeOut
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *                 Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2DhcpClientFastAccessNullStateTimeOut (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4TestValDhcpClientFastAccessNullStateTimeOut)
{
    INT4                i4RetValDhcpClientFastAccess = 0;
    INT1                i1RetVal = 0;

    nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
    if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
    {
        if ((i4TestValDhcpClientFastAccessNullStateTimeOut <
             DHCPC_FAST_ACC_NULL_ST_TIMEOUT)
            || (i4TestValDhcpClientFastAccessNullStateTimeOut >
                DHCPC_FAST_ACC_NULL_ST_MAX_TIMEOUT))
        {

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_DISABLE)
    {
        if (i4TestValDhcpClientFastAccessNullStateTimeOut !=
            DHCPC_NULL_STATE_WAIT_TIME_OUT)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_DHCPC_IDLE_FAST_ACCESS_DISABLE_ERR);
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_DHCPC_IDLE_FAST_ACCESS_DISABLE_ERR);
            i1RetVal = SNMP_FAILURE;
        }
    }
    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhTestv2DhcpClientFastAccessArpCheckTimeOut
 *  Input       :  The Indices
 *
 *             The Object
 *             testValDhcpClientFastAccessArpCheckTimeOut
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val.
 *  Error Codes :  The following error codes are to be returned
 *                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2DhcpClientFastAccessArpCheckTimeOut (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4TestValDhcpClientFastAccessArpCheckTimeOut)
{
    INT4                i4RetValDhcpClientFastAccess = 0;
    INT1                i1RetVal = 0;

    nmhGetDhcpClientFastAccess (&i4RetValDhcpClientFastAccess);
    if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_ENABLE)
    {
        if ((i4TestValDhcpClientFastAccessArpCheckTimeOut <
             DHCPC_FAST_ACC_ARP_CHECK_MIN_TIMEOUT)
            || (i4TestValDhcpClientFastAccessArpCheckTimeOut >
                DHCPC_FAST_ACC_ARP_CHECK_MAX_TIMEOUT))
        {

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4RetValDhcpClientFastAccess == DHCPC_FAST_ACC_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPC_ARP_CHECK_FAST_ACCESS_DISABLE_ERR);
        i1RetVal = SNMP_FAILURE;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpClientFastAccess
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientFastAccess (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpClientFastAccessDiscoverTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientFastAccessDiscoverTimeOut (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpClientFastAccessNullStateTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientFastAccessNullStateTimeOut (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhDepv2DhcpClientFastAccessArpCheckTimeOut
 *  Output      :  The Dependency Low Lev Routine Take the Indices &
 *                 check whether dependency is met or not.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhDepv2DhcpClientFastAccessArpCheckTimeOut (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************/

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpClientCounterTable
 Input       :  The Indices
                DhcpClientIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientCounterTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCGetDefaultNetMask                             */
/*  Description     : This function get the default netmask for the     */
/*                    given ip address.                                 */
/*  Input(s)        : u4IpAddress - ip address .                        */
/*  Output(s)       : None.                                             */
/*  Returns         : default netmask for the given ip address.         */
/************************************************************************/
UINT4
DhcpCGetDefaultNetMask (UINT4 u4IpAddress)
{
    UINT4               u4NetMask;

    if (IP_IS_ADDR_CLASS_A (u4IpAddress))
    {
        u4NetMask = DHCP_NETMASK_CLASS_A;
    }
    else if (IP_IS_ADDR_CLASS_B (u4IpAddress))
    {
        u4NetMask = DHCP_NETMASK_CLASS_B;
    }
    else if (IP_IS_ADDR_CLASS_C (u4IpAddress))
    {
        u4NetMask = DHCP_NETMASK_CLASS_C;
    }
    else
    {
        u4NetMask = DHCP_NETMASK_DEFAULT;
    }

    return u4NetMask;

}

/* LOW LEVEL Routines for Table : DhcpClientOptTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpClientOptTable
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpClientOptTable (INT4 i4DhcpClientOptIfIndex,
                                            INT4 i4DhcpClientOptType)
{
    UINT4               u4Port;

    if (NetIpv4GetPortFromIfIndex (i4DhcpClientOptIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4DhcpClientOptType != DHCP_OPT_TFTP_SNAME) &&
        (i4DhcpClientOptType != DHCP_OPT_BOOT_FILE_NAME) &&
        (i4DhcpClientOptType != DHCP_OPT_NTP_SERVERS) &&
        (i4DhcpClientOptType != DHCP_OPT_DNS_NS) &&
        (i4DhcpClientOptType != DHCP_OPT_SIP_SERVER) &&
        (i4DhcpClientOptType != DHCP_OPT_240) &&
        (i4DhcpClientOptType != DHCP_OPT_SVENDOR_SPECIFIC) &&
        (i4DhcpClientOptType != DHCP_OPT_CVENDOR_SPECIFIC) &&
        (i4DhcpClientOptType != DHCP_OPT_ROUTER_OPTION) &&
        (i4DhcpClientOptType != DHCP_OPT_CLIENT_ID))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpClientOptTable
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpClientOptTable (INT4 *pi4DhcpClientOptIfIndex,
                                    INT4 *pi4DhcpClientOptType)
{
    if (DhcpCGetFirstIndexReqOptionEntry ((UINT4 *) pi4DhcpClientOptIfIndex,
                                          (UINT4 *) pi4DhcpClientOptType)
        == DHCP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpClientOptTable
 Input       :  The Indices
                DhcpClientOptIfIndex
                nextDhcpClientOptIfIndex
                DhcpClientOptType
                nextDhcpClientOptType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpClientOptTable (INT4 i4DhcpClientOptIfIndex,
                                   INT4 *pi4NextDhcpClientOptIfIndex,
                                   INT4 i4DhcpClientOptType,
                                   INT4 *pi4NextDhcpClientOptType)
{
    INT4                i4RetVal = DHCP_FAILURE;

    i4RetVal =
        DhcpCGetNextIndexReqOptionEntry ((UINT4) i4DhcpClientOptIfIndex,
                                         (UINT4 *) pi4NextDhcpClientOptIfIndex,
                                         (UINT4) i4DhcpClientOptType,
                                         (UINT4 *) pi4NextDhcpClientOptType);
    if (i4RetVal == DHCP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpClientOptLen
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientOptLen (INT4 i4DhcpClientOptIfIndex,
                        INT4 i4DhcpClientOptType,
                        INT4 *pi4RetValDhcpClientOptLen)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    pDhcpCReqOption = DhcpCGetReqOptionEntry ((UINT4) i4DhcpClientOptIfIndex,
                                              (UINT4) i4DhcpClientOptType);
    if (pDhcpCReqOption != NULL)
    {
        *pi4RetValDhcpClientOptLen = (INT4) pDhcpCReqOption->u1Len;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpClientOptLen
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientOptLen (UINT4 *pu4ErrorCode,
                           INT4 i4DhcpClientOptIfIndex,
                           INT4 i4DhcpClientOptType, INT4 i4DhcpClientOptLen)
{

    UINT4               u4Port;

    if (i4DhcpClientOptType != DHCP_OPT_CVENDOR_SPECIFIC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientOptIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        return SNMP_FAILURE;
    }

    if (i4DhcpClientOptLen < 1 || i4DhcpClientOptLen > DHCP_MAX_OPT_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpClientOptLen
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientOptLen (INT4 i4DhcpClientOptIfIndex,
                        INT4 i4DhcpClientOptType, INT4 i4DhcpClientOptLen)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    if (i4DhcpClientOptType != DHCP_OPT_CVENDOR_SPECIFIC)
    {
        return SNMP_FAILURE;
    }

    pDhcpCReqOption =
        (tDhcpCOptions *) DhcpCGetReqOptionEntry ((UINT4)
                                                  i4DhcpClientOptIfIndex,
                                                  (UINT4) i4DhcpClientOptType);
    if (pDhcpCReqOption != NULL)
    {
        pDhcpCReqOption->u1Len = (UINT1) i4DhcpClientOptLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetDhcpClientOptVal
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientOptVal (INT4 i4DhcpClientOptIfIndex,
                        INT4 i4DhcpClientOptType,
                        tSNMP_OCTET_STRING_TYPE * pRetValDhcpClientOptVal)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    pDhcpCReqOption =
        (tDhcpCOptions *) DhcpCGetReqOptionEntry ((UINT4)
                                                  i4DhcpClientOptIfIndex,
                                                  (UINT4) i4DhcpClientOptType);
    if (pDhcpCReqOption != NULL)
    {
        pRetValDhcpClientOptVal->i4_Length = (INT4) pDhcpCReqOption->u1Len;
        MEMSET (pRetValDhcpClientOptVal->pu1_OctetList, 0, DHCP_MAX_OPT_LEN);
        MEMCPY (pRetValDhcpClientOptVal->pu1_OctetList,
                pDhcpCReqOption->au1OptionVal,
                pRetValDhcpClientOptVal->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpClientOptVal
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientOptVal (UINT4 *pu4ErrorCode,
                           INT4 i4DhcpClientOptIfIndex,
                           INT4 i4DhcpClientOptType,
                           tSNMP_OCTET_STRING_TYPE * pDhcpClientOptVal)
{

    UINT4               u4Port = 0;

    if (i4DhcpClientOptType != DHCP_OPT_CVENDOR_SPECIFIC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientOptIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        return SNMP_FAILURE;
    }

    if (pDhcpClientOptVal == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpClientOptVal
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientOptVal (INT4 i4DhcpClientOptIfIndex,
                        INT4 i4DhcpClientOptType,
                        tSNMP_OCTET_STRING_TYPE * pDhcpClientOptVal)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    if (i4DhcpClientOptType != DHCP_OPT_CVENDOR_SPECIFIC)
    {
        return SNMP_FAILURE;
    }

    pDhcpCReqOption =
        (tDhcpCOptions *) DhcpCGetReqOptionEntry ((UINT4)
                                                  i4DhcpClientOptIfIndex,
                                                  (UINT4) i4DhcpClientOptType);
    if (pDhcpCReqOption != NULL)
    {
        MEMSET (pDhcpCReqOption->au1OptionVal, 0, DHCP_MAX_OPT_LEN);
        MEMCPY (pDhcpCReqOption->au1OptionVal,
                pDhcpClientOptVal->pu1_OctetList, pDhcpCReqOption->u1Len);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpClientOptRowStatus
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                retValDhcpClientOptRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpClientOptRowStatus (INT4 i4DhcpClientOptIfIndex,
                              INT4 i4DhcpClientOptType,
                              INT4 *pi4RetValDhcpClientOptRowStatus)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    pDhcpCReqOption =
        (tDhcpCOptions *) DhcpCGetReqOptionEntry ((UINT4)
                                                  i4DhcpClientOptIfIndex,
                                                  (UINT4) i4DhcpClientOptType);
    if (pDhcpCReqOption != NULL)
    {
        if (pDhcpCReqOption->u1Status == DHCP_ENABLE)
        {
            *pi4RetValDhcpClientOptRowStatus = ACTIVE;
        }
        else
        {
            *pi4RetValDhcpClientOptRowStatus = CREATE_AND_WAIT;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpClientOptRowStatus
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                setValDhcpClientOptRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpClientOptRowStatus (INT4 i4DhcpClientOptIfIndex,
                              INT4 i4DhcpClientOptType,
                              INT4 i4SetValDhcpClientOptRowStatus)
{
    tDhcpCOptions      *pDhcpCReqOption = NULL;

    switch (i4SetValDhcpClientOptRowStatus)
    {

        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            DhcpcConfReqOptionType ((UINT4) i4DhcpClientOptIfIndex,
                                    (UINT1) i4DhcpClientOptType,
                                    DHCPC_SET_OPTION);
            return SNMP_SUCCESS;

        case ACTIVE:
            pDhcpCReqOption =
                (tDhcpCOptions *) DhcpCGetReqOptionEntry ((UINT4)
                                                          i4DhcpClientOptIfIndex,
                                                          (UINT4)
                                                          i4DhcpClientOptType);
            if (pDhcpCReqOption != NULL)
                pDhcpCReqOption->u1Status = DHCP_ENABLE;
            return SNMP_SUCCESS;

        case DESTROY:
            DhcpcConfReqOptionType ((UINT4) i4DhcpClientOptIfIndex,
                                    (UINT4) i4DhcpClientOptType,
                                    DHCPC_RESET_OPTION);
            return SNMP_SUCCESS;
        default:
            break;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpClientOptRowStatus
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType

                The Object 
                testValDhcpClientOptRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpClientOptRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4DhcpClientOptIfIndex,
                                 INT4 i4DhcpClientOptType,
                                 INT4 i4TestValDhcpClientOptRowStatus)
{
    UINT4               u4Port;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4DhcpClientOptIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        return SNMP_FAILURE;
    }

    if ((i4DhcpClientOptType != DHCP_OPT_TFTP_SNAME) &&
        (i4DhcpClientOptType != DHCP_OPT_BOOT_FILE_NAME) &&
        (i4DhcpClientOptType != DHCP_OPT_SIP_SERVER) &&
        (i4DhcpClientOptType != DHCP_OPT_SVENDOR_SPECIFIC) &&
        (i4DhcpClientOptType != DHCP_OPT_240) &&
        (i4DhcpClientOptType != DHCP_OPT_CVENDOR_SPECIFIC))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDhcpClientOptRowStatus == CREATE_AND_WAIT)
        || (i4TestValDhcpClientOptRowStatus == CREATE_AND_GO)
        || (i4TestValDhcpClientOptRowStatus == ACTIVE)
        || (i4TestValDhcpClientOptRowStatus == DESTROY))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpClientOptTable
 Input       :  The Indices
                DhcpClientOptIfIndex
                DhcpClientOptType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpClientOptTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
