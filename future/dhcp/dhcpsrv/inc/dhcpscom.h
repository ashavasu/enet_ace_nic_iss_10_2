/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpscom.h,v 1.21 2013/03/28 11:54:01 siva Exp $ 
 *
 * Description: Common include files for dhcp server module.
 *******************************************************************/

#ifndef  __DHCPS_COM_H
#define  __DHCPS_COM_H
#include    "dhcpsport.h"
#include    "dhcp.h"
#include    "cfa.h"
#include    "arp.h"
#include    "rtm.h"
#include    "rmgr.h"
#include    "fssyslog.h"

#ifdef L3_SWITCHING_WANTED
#include "npapi.h"
#include "ipnp.h"
#endif
#include    "dhcpsdef.h"
#include    "dhcpstdf.h"
#include    "dhcpsred.h"
#include    "dhcpsrvsz.h"
#include    "dhcpsglob.h"
#ifdef NPAPI_WANTED
#include "dhcpsnpwr.h"
#endif /* NPAPI_WANTED */
#endif
