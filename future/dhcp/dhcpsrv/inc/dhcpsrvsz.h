/*  $Id: dhcpsrvsz.h,v 1.5 2013/04/25 11:12:07 siva Exp $*/

enum {
    MAX_DHCP_MAX_MTU_SIZING_ID,
    MAX_DHCP_MAX_OUTMSG_SIZE_SIZING_ID,
    MAX_DHSRV_EXCL_NODE_SIZING_ID,
    MAX_DHSRV_FREE_BIND_REC_SIZING_ID,
    MAX_DHSRV_FREE_HOST_REC_SIZING_ID,
    MAX_DHSRV_FREE_POOL_REC_SIZING_ID,
    
    MAX_DHSRV_OPTION_SIZING_ID,
    MAX_DHSRV_OUT_PKT_SIZING_ID,

    MAX_DHSRV_TEMP_OPT_PKT_SIZING_ID,
    MAX_DHSRV_ICMP_NODE_SIZING_ID,
 MAX_DHSRV_QUEUE_MSG_SIZING_ID,
    DHCPSRV_MAX_SIZING_ID
};


#ifdef  _DHCPSRVSZ_C
tMemPoolId DHCPSRVMemPoolIds[ DHCPSRV_MAX_SIZING_ID];
INT4  DhcpsrvSizingMemCreateMemPools(VOID);
VOID  DhcpsrvSizingMemDeleteMemPools(VOID);
INT4  DhcpsrvSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _DHCPSRVSZ_C  */
extern tMemPoolId DHCPSRVMemPoolIds[ ];
extern INT4  DhcpsrvSizingMemCreateMemPools(VOID);
extern VOID  DhcpsrvSizingMemDeleteMemPools(VOID);
extern INT4  DhcpsrvSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _DHCPSRVSZ_C  */


#ifdef  _DHCPSRVSZ_C
tFsModSizingParams FsDHCPSRVSizingParams [] = {
{ "tDhcpRcvPkt", "MAX_DHCP_MAX_MTU", sizeof(tDhcpRcvPkt),MAX_DHCP_MAX_MTU, MAX_DHCP_MAX_MTU,0 },
{ "tDhcpSrvMessage", "MAX_DHCP_MAX_OUTMSG_SIZE", sizeof(tDhcpSrvMessage),MAX_DHCP_MAX_OUTMSG_SIZE, MAX_DHCP_MAX_OUTMSG_SIZE,0 },
{ "tExcludeIpAddr", "MAX_DHSRV_EXCL_NODE", sizeof(tExcludeIpAddr),MAX_DHSRV_EXCL_NODE, MAX_DHSRV_EXCL_NODE,0 },
{ "tDhcpSrvBindRec", "MAX_DHSRV_FREE_BIND_REC", sizeof(tDhcpSrvBindRec),MAX_DHSRV_FREE_BIND_REC, MAX_DHSRV_FREE_BIND_REC,0 },
{ "tDhcpSrvHostRec", "MAX_DHSRV_FREE_HOST_REC", sizeof(tDhcpSrvHostRec),MAX_DHSRV_FREE_HOST_REC, MAX_DHSRV_FREE_HOST_REC,0 },
{ "tDhcpSrvPoolRec", "MAX_DHSRV_FREE_POOL_REC", sizeof(tDhcpSrvPoolRec),MAX_DHSRV_FREE_POOL_REC, MAX_DHSRV_FREE_POOL_REC,0 },

{ "tDhcpOptions", "MAX_DHSRV_OPTION", sizeof(tDhcpOptions),MAX_DHSRV_OPTION, MAX_DHSRV_OPTION,0 },
{ "tDhcpPktInfo", "MAX_DHSRV_OUT_PKT", sizeof(tDhcpPktInfo),MAX_DHSRV_OUT_PKT, MAX_DHSRV_OUT_PKT,0 },

{ "tDhcpSrvTempOptPkt", "MAX_DHSRV_TEMP_OPT_PKT", sizeof(tDhcpSrvTempOptPkt),MAX_DHSRV_TEMP_OPT_PKT, MAX_DHSRV_TEMP_OPT_PKT,0 },
{ "tDhcpIcmpNode", "MAX_DHSRV_ICMP_NODE", sizeof(tDhcpIcmpNode),MAX_DHSRV_ICMP_NODE, MAX_DHSRV_ICMP_NODE,0 },
{ "tDhcpSQMsg", "MAX_DHSRV_QUEUE_MSG", sizeof(tDhcpSQMsg), MAX_DHSRV_QUEUE_MSG, MAX_DHSRV_QUEUE_MSG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _DHCPSRVSZ_C  */
extern tFsModSizingParams FsDHCPSRVSizingParams [];
#endif /*  _DHCPSRVSZ_C  */


