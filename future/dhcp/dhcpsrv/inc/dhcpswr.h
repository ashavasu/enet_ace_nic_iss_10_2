/* $Id: dhcpswr.h,v 1.4 2013/07/09 12:28:55 siva Exp $*/
#ifndef _FSDHCPWR_H
#define _FSDHCPWR_H

VOID RegisterDHCPS(VOID);
VOID RegisterFSDHCP(VOID);
VOID UnRegisterFSDHCP(VOID);

INT4 DhcpSrvEnableGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvDebugLevelGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvOfferReuseTimeOutGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvIcmpEchoEnableGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBootServerAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvDefBootFilenameGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBootpClientsSupportedGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvAutomaticBootpEnabledGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvEnableSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvDebugLevelSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvOfferReuseTimeOutSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvIcmpEchoEnableSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBootServerAddressSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvDefBootFilenameSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBootpClientsSupportedSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvAutomaticBootpEnabledSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvDebugLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvOfferReuseTimeOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvIcmpEchoEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBootServerAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvDefBootFilenameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBootpClientsSupportedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvAutomaticBootpEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvDebugLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvOfferReuseTimeOutDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvIcmpEchoEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvBootServerAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvDefBootFilenameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvBootpClientsSupportedDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 DhcpSrvAutomaticBootpEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);








INT4 GetNextIndexDhcpSrvSubnetPoolConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvSubnetSubnetGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetMaskGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetStartIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetEndIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetLeaseTimeGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolNameGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetUtlThresholdGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetSubnetSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPortNumberSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetMaskSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetStartIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetEndIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetLeaseTimeSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolNameSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetUtlThresholdSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetSubnetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPortNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetStartIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetEndIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetLeaseTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetUtlThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetPoolConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);









INT4 GetNextIndexDhcpSrvExcludeIpAddressTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvExcludeEndIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvExcludeAddressRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvExcludeEndIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvExcludeAddressRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvExcludeEndIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvExcludeAddressRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvExcludeIpAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDhcpSrvGblOptTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvGblOptLenGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptValGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptLenSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptValSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptValTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvGblOptTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDhcpSrvSubnetOptTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvSubnetOptLenGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptValGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptLenSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptValSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptValTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvSubnetOptTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDhcpSrvHostOptTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvHostOptLenGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptValGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptLenSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptValSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptValTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostOptTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDhcpSrvHostConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvHostIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostPoolNameGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostBootFileNameGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostBootServerAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostConfigRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostPoolNameSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostBootFileNameSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostBootServerAddressSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostConfigRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostPoolNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostBootFileNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostBootServerAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostConfigRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvHostConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexDhcpSrvBindingTable(tSnmpIndex *, tSnmpIndex *);
INT4 DhcpSrvBindHwTypeGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindHwAddressGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindExpireTimeGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindAllocMethodGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindStateGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindXidGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpSrvBindingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 DhcpCountDiscoversGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountRequestsGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountReleasesGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountDeclinesGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountInformsGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountInvalidsGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountOffersGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountAcksGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountNacksGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountDroppedUnknownClientGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountDroppedNotServingSubnetGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountResetCountersGet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountResetCountersSet(tSnmpIndex *, tRetVal *);
INT4 DhcpCountResetCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 DhcpCountResetCountersDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSDHCPWR_H */
