/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dhcpslow.h,v 1.7 2013/07/09 12:28:55 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvEnable ARG_LIST((INT4 *));

INT1
nmhGetDhcpSrvDebugLevel ARG_LIST((INT4 *));

INT1
nmhGetDhcpSrvOfferReuseTimeOut ARG_LIST((UINT4 *));

INT1
nmhGetDhcpSrvIcmpEchoEnable ARG_LIST((INT4 *));

INT1
nmhGetDhcpSrvBootServerAddress ARG_LIST((UINT4 *));

INT1
nmhGetDhcpSrvDefBootFilename ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvBootpClientsSupported ARG_LIST((INT4 *));

INT1
nmhGetDhcpSrvAutomaticBootpEnabled ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvEnable ARG_LIST((INT4 ));

INT1
nmhSetDhcpSrvDebugLevel ARG_LIST((INT4 ));

INT1
nmhSetDhcpSrvOfferReuseTimeOut ARG_LIST((UINT4 ));

INT1
nmhSetDhcpSrvIcmpEchoEnable ARG_LIST((INT4 ));

INT1
nmhSetDhcpSrvBootServerAddress ARG_LIST((UINT4 ));

INT1
nmhSetDhcpSrvDefBootFilename ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpSrvBootpClientsSupported ARG_LIST((INT4 ));

INT1
nmhSetDhcpSrvAutomaticBootpEnabled ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpSrvDebugLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpSrvOfferReuseTimeOut ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2DhcpSrvIcmpEchoEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpSrvBootServerAddress ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2DhcpSrvDefBootFilename ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpSrvBootpClientsSupported ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DhcpSrvAutomaticBootpEnabled ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvOfferReuseTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvIcmpEchoEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvBootServerAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvDefBootFilename ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvBootpClientsSupported ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DhcpSrvAutomaticBootpEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvSubnetPoolConfigTable. */
INT1
nmhValidateIndexInstanceDhcpSrvSubnetPoolConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvSubnetPoolConfigTable  */

INT1
nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvSubnetPoolConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvSubnetSubnet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpSrvSubnetPortNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpSrvSubnetMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpSrvSubnetStartIpAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpSrvSubnetEndIpAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDhcpSrvSubnetLeaseTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpSrvSubnetPoolName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvSubnetUtlThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpSrvSubnetPoolRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvSubnetSubnet ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDhcpSrvSubnetPortNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpSrvSubnetMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDhcpSrvSubnetStartIpAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDhcpSrvSubnetEndIpAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDhcpSrvSubnetLeaseTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpSrvSubnetPoolName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpSrvSubnetUtlThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpSrvSubnetPoolRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvSubnetSubnet ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2DhcpSrvSubnetPortNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvSubnetMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2DhcpSrvSubnetStartIpAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2DhcpSrvSubnetEndIpAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2DhcpSrvSubnetLeaseTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvSubnetPoolName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpSrvSubnetUtlThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvSubnetPoolRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvSubnetPoolConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvExcludeIpAddressTable. */
INT1
nmhValidateIndexInstanceDhcpSrvExcludeIpAddressTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvExcludeIpAddressTable  */

INT1
nmhGetFirstIndexDhcpSrvExcludeIpAddressTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvExcludeIpAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvExcludeEndIpAddress ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDhcpSrvExcludeAddressRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvExcludeEndIpAddress ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDhcpSrvExcludeAddressRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvExcludeEndIpAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2DhcpSrvExcludeAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvExcludeIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvGblOptTable. */
INT1
nmhValidateIndexInstanceDhcpSrvGblOptTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvGblOptTable  */

INT1
nmhGetFirstIndexDhcpSrvGblOptTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvGblOptTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvGblOptLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDhcpSrvGblOptVal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvGblOptRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvGblOptLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDhcpSrvGblOptVal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpSrvGblOptRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvGblOptLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvGblOptVal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpSrvGblOptRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvGblOptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvSubnetOptTable. */
INT1
nmhValidateIndexInstanceDhcpSrvSubnetOptTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvSubnetOptTable  */

INT1
nmhGetFirstIndexDhcpSrvSubnetOptTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvSubnetOptTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvSubnetOptLen ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDhcpSrvSubnetOptVal ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvSubnetOptRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvSubnetOptLen ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDhcpSrvSubnetOptVal ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpSrvSubnetOptRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvSubnetOptLen ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvSubnetOptVal ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpSrvSubnetOptRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvSubnetOptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvHostOptTable. */
INT1
nmhValidateIndexInstanceDhcpSrvHostOptTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvHostOptTable  */

INT1
nmhGetFirstIndexDhcpSrvHostOptTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvHostOptTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvHostOptLen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetDhcpSrvHostOptVal ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvHostOptRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvHostOptLen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetDhcpSrvHostOptVal ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpSrvHostOptRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvHostOptLen ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvHostOptVal ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpSrvHostOptRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvHostOptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvHostConfigTable. */
INT1
nmhValidateIndexInstanceDhcpSrvHostConfigTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvHostConfigTable  */

INT1
nmhGetFirstIndexDhcpSrvHostConfigTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvHostConfigTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvHostIpAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDhcpSrvHostPoolName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetDhcpSrvHostBootFileName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvHostBootServerAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetDhcpSrvHostConfigRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvHostIpAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,UINT4 ));

INT1
nmhSetDhcpSrvHostPoolName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetDhcpSrvHostBootFileName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDhcpSrvHostBootServerAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetDhcpSrvHostConfigRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvHostIpAddress ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,UINT4 ));

INT1
nmhTestv2DhcpSrvHostPoolName ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvHostBootFileName ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DhcpSrvHostBootServerAddress ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2DhcpSrvHostConfigRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvHostConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DhcpSrvBindingTable. */
INT1
nmhValidateIndexInstanceDhcpSrvBindingTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DhcpSrvBindingTable  */

INT1
nmhGetFirstIndexDhcpSrvBindingTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvBindingTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpSrvBindHwType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDhcpSrvBindHwAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDhcpSrvBindExpireTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDhcpSrvBindAllocMethod ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDhcpSrvBindState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDhcpSrvBindXid ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDhcpSrvBindEntryStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpSrvBindEntryStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpSrvBindEntryStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpSrvBindingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDhcpCountDiscovers ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountRequests ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountReleases ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountDeclines ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountInforms ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountInvalids ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountOffers ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountAcks ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountNacks ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountDroppedUnknownClient ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountDroppedNotServingSubnet ARG_LIST((UINT4 *));

INT1
nmhGetDhcpCountResetCounters ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDhcpCountResetCounters ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DhcpCountResetCounters ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DhcpCountResetCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
