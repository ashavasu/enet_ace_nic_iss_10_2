/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsred.h,v 1.4 2014/02/14 13:55:13 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for DHCP Server module.
 *              
 *******************************************************************/
#ifndef __DHCPS_RED_H
#define __DHCPS_RED_H


typedef struct _sDhcpSRedGlobalInfo{
 UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
 UINT1   u1NumPeersUp;     /* Indicates number of standby nodes 
                              that are up. */
 BOOL1   bBulkReqRcvd;     /* To check whether bulk request recieved 
                              from standby before RM_STANDBY_UP event. */
 UINT1   au1Padding[1];    /* Reserved */
}tDhcpSRedGlobalInfo;


/* Represents the message types encoded in the update messages */
typedef enum {
    DHCPS_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    DHCPS_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    DHCPS_RED_BULK_BINDING_INFO,
    DHCPS_RED_BINDING_INFO
}eDhcpSRedRmMsgType;

/* Macro Definitions for DHCP Server Redundancy */

#define DHCPS_RM_NODE_STATUS()  gDhcpSRedGlobalInfo.u1NodeStatus

#define DHCPS_RM_GET_NUM_STANDBY_NODES_UP() \
          gDhcpSRedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define DHCPS_NUM_STANDBY_NODES() gDhcpSRedGlobalInfo.u1NumPeersUp

#define DHCPS_RM_BULK_REQ_RCVD() gDhcpSRedGlobalInfo.bBulkReqRcvd

#define DHCPS_IS_STANDBY_UP() \
          ((gDhcpSRedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */
#ifdef L2RED_WANTED
#define DHCPS_RM_GET_NODE_STATUS()  RmGetNodeState ()
#else
#define DHCPS_RM_GET_NODE_STATUS()  RM_ACTIVE
#endif

#define DHCPS_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define DHCPS_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define DHCPS_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define DHCPS_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define DHCPS_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define DHCPS_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define DHCPS_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define DHCPS_RM_GET_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pdest, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define DHCPS_RED_MAX_MSG_SIZE        1500
#define DHCPS_RED_TYPE_FIELD_SIZE     1
#define DHCPS_RED_LEN_FIELD_SIZE      2
#define DHCPS_RED_DYN_INFO_SIZE        (DHCP_MAX_CID_LEN + /* Bind MAC */          \
                                       (sizeof(UINT1)) +   /* HW Type  */          \
                                       (sizeof(UINT1)) +   /* HostID Length */     \
                                       (sizeof(UINT1)) +   /* Mask */              \
                                       (sizeof(UINT4)) +   /* IP Address */        \
                                       (sizeof(UINT4)) +   /* Subnet */            \
                                       (sizeof(UINT4)) +   /* Lease Expiry Time */ \
                                       (sizeof(UINT4)) +   /* Pool ID */           \
                                       (sizeof(UINT4)))    /* Allocation Method */




#define DHCPS_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define DHCPS_RED_BULK_REQ_MSG_SIZE            3

#define DHCPS_RED_BULQ_REQ_SIZE       3

/* Function prototypes for DHCP Server Redundancy */

INT4  DhcpSRedInitGlobalInfo PROTO ((VOID));
INT4  DhcpSRedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));
INT4  DhcpSRedRmDeRegisterProtocols PROTO ((VOID));
INT4  DhcpSRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID  DhcpSRedHandleRmEvents PROTO ((tDhcpSQMsg *pMsg));
VOID  DhcpSRedHandleGoActive PROTO ((VOID));
VOID  DhcpSRedHandleGoStandby PROTO ((VOID));
VOID  DhcpSRedHandleStandbyToActive PROTO ((VOID));
VOID  DhcpSRedHandleActiveToStandby PROTO ((VOID));
VOID  DhcpSRedHandleIdleToActive PROTO ((VOID));
VOID  DhcpSRedHandleIdleToStandby PROTO ((VOID));
VOID  DhcpSRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
VOID  DhcpSRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
UINT4 DhcpSRedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
INT4  DhcpSRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
INT4  DhcpSRedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));
VOID  DhcpSRedSendBulkReqMsg PROTO ((VOID));
INT4  DhcpsRedDeInitGlobalInfo PROTO ((VOID));
VOID  DhcpSRedSendDynamicBulkMsg PROTO ((VOID));
VOID  DhcpSRedSendBindingInfoBulk PROTO ((VOID));
VOID  DhcpSRedSendBulkUpdTailMsg PROTO ((VOID));
VOID  DhcpSRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
VOID  DhcpSRedSendDynamicBindingInfo PROTO ((tDhcpBinding * pDhcpBindingInfo));
VOID  DhcpSRedProcessDynamicBindingInfo PROTO ((tRmMsg * pMsg, UINT2 *pu4OffSet));

#endif /* __DHCPS_RED_H */
