/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsglob.h,v 1.32 2013/11/14 11:27:35 siva Exp $ 
 *
 * Description: This file contains declaration of global variables 
 *              and function prototypes of dhcp server module.
 *******************************************************************/

#ifndef  __DHCPS_GLOB_H
#define  __DHCPS_GLOB_H

#ifdef _DHCPS_GLOB_VAR

UINT4 gu4DhcpsEnable;  /* Dhcp Server is enabled only when this is set */

/* Time before which an address is put back into free pool after the server has
 * * offered the address and is awaiting a DHCP_REQUEST
 * */

UINT4   gu4DhcpOfferTimeOut;

INT1    gi1DhcpsIcmpEchoEnable;

INT4    gi4DhcpDebugMask;
INT4    gi4DhcpSysLogId;

INT1    gi1BootpSupported;

UINT4    gu4DhcpSelectServTskId;

UINT4   gu4PingTimeOut;  /*ping Time out value*/

UINT4    gu4PingCount;   /*ICMP Ping Count */

INT4  gi4DhcpSrvICMPSocket;  /* Dhcp Server ICMP Socket */

UINT2 gu2DhcpIcmpSeqNo;    /* DhcpIcmp PKt Seq No */

tOsixSemId          gDhcpServSemId;
tOsixQId            gDhcpSMsgQId;

tDhcpSRedGlobalInfo gDhcpSRedGlobalInfo;

/* Global Counter in DHCP */
tDhcpCounters   gDhcpCounters;

tDhcpOptions    *gpDhcpOptions;

tDhcpPool    *gpDhcpPool;
tDhcpBinding *gpDhcpBinding;
tDhcpHostEntry  *gpDhcpHostEntry;

tDhcpPool    *gpDhcpFreePoolRec;
tDhcpBinding *gpDhcpFreeBindRec;
tDhcpHostEntry *gpDhcpFreeHostRec;


tDhcpConfig gDhcpServConfig;
const UINT1 gau1DhcpMagicCookie[] = { 99,130,83,99};

const char *gDHCPDegMsg[] = { "BOOTPREQUEST",
                              "DHCPDISCOVER",
                              "DHCPOFFER",
                              "DHCPREQUEST",
                              "DHCPDECLINE",
                              "DHCPACK",
                              "DHCPNAK",
                              "DHCPRELEASE",
                              "DHCPINFORM",
                              "BOOTP" };


/* Timer list used for Offer reuse, Lease expiry and ICMP ping timeout */
tTimerListId DhcpTimerListId;

INT4 gi4DhcpSrvSockDesc;

tTmrAppTimer    gDhcpOfferReuseTimer;
tTmrAppTimer    gDhcpLeaseExpTimer;
UINT4           gu4DhcpServTskId;
tOsixQId        gDhcpServQId;

tDhcpTag    DhcpTag;
tDhcpIcmpNode   *gpDhcpIcmpList;
UINT4  gDhcpBootServerAddress;
UINT1  gau1DhcpBootFileName[DHCP_MAX_NAME_LEN + 1];
UINT1 gu1DhcpAutomaticBootpEnabled;
UINT1 gu1DhcpBootpClientSupported;
#else

extern UINT4 gu4DhcpsEnable; 
extern UINT4 gu4DhcpOfferTimeOut;
extern INT1  gi1DhcpsIcmpEchoEnable;
extern tOsixQId        gDhcpServQId;
extern tOsixQId        gDhcpSMsgQId;

extern UINT4    gu4DhcpSelectServTskId;
extern INT4 gi4DhcpDebugMask;
extern INT4    gi4DhcpSysLogId;

extern INT1    gi1BootpSupported;

extern tDhcpCounters   gDhcpCounters;

extern tDhcpOptions    *gpDhcpOptions;
extern tDhcpHostEntry  *gpDhcpHostEntry;

extern tDhcpBinding *gpDhcpBinding;
extern tDhcpPool    *gpDhcpPool;

extern tDhcpPool    *gpDhcpFreePoolRec;
extern tDhcpBinding *gpDhcpFreeBindRec;
extern tDhcpHostEntry *gpDhcpFreeHostRec;

extern tDhcpConfig gDhcpServConfig;
extern const UINT1 *gDHCPDegMsg[];
extern const UINT1 gau1DhcpMagicCookie[];

extern tTimerListId DhcpTimerListId;

extern INT4 gi4DhcpSrvSockDesc;

extern tTmrAppTimer    gDhcpOfferReuseTimer;
extern tTmrAppTimer    gDhcpLeaseExpTimer;
extern UINT4               gu4DhcpServTskId;

extern tDhcpTag    DhcpTag;
extern tDhcpIcmpNode   *gpDhcpIcmpList;

extern UINT4   gu4PingTimeOut;
extern UINT4   gu4PingCount;
extern INT4  gi4DhcpSrvICMPSocket;  /* Dhcp Server ICMP Socket */
extern UINT2 gu2DhcpIcmpSeqNo;    /* DhcpIcmp PKt Seq No */

extern UINT4  gDhcpBootServerAddress;
extern UINT1  gau1DhcpBootFileName[];
extern UINT1  gu1DhcpAutomaticBootpEnabled;
extern UINT1  gu1DhcpBootpClientSupported;
extern tDhcpSRedGlobalInfo gDhcpSRedGlobalInfo;

/* MemPool ID's */
extern INT4 i4DhcpSrvPoolRecId;
extern INT4 i4DhcpSrvBindRecId;
extern INT4 i4DhcpSrvHostRecId;
extern INT4 i4DhcpSrvOptId;
extern INT4 i4DhcpSrvExclAddrId;
extern INT4 i4DhcpSrvOutPktId;
extern INT4 i4DhcpSrvTempOptId;
extern INT4 i4DhcpSrvIcmpNodeId;
extern INT4 i4DhcpSrvQueueMsgId;
extern INT4 i4DhcpSrvMessageId;
extern INT4 i4DhcpSrvRecvBuffId;
#endif

/* Function prototypes */

VOID DhcpFillSendMessage(tDhcpPktInfo *,INT1 *, UINT1 ,UINT2   *);
INT4 DhcpTaskInit(VOID);  
INT4 DhcpSrvSockInit (VOID);
VOID DhcpProcessTmrExpiry(VOID);
VOID DhcpProcessPkt(UINT1 *,UINT4 ,UINT4, UINT2 );
VOID DhcpGetPktInfo(tDhcpPktInfo *,UINT1 *, UINT2 u2Len);
VOID DhcpCopyHeader(tDhcpMsgHdr *, UINT1 *);
VOID DhcpAddOption(tDhcpPktInfo  *,tDhcpTag *);
VOID DhcpRcvdDiscover(tDhcpPktInfo *, tDhcpPool *, tDhcpHostEntry *);
VOID DhcpRcvdRequest(tDhcpPktInfo *,tDhcpPool *, tDhcpHostEntry *);
VOID DhcpRcvdRelease(tDhcpPktInfo *,tDhcpPool *, tDhcpHostEntry *);
VOID DhcpRcvdDecline(tDhcpPktInfo *,tDhcpPool *, tDhcpHostEntry *);
VOID DhcpRcvdInform(tDhcpPktInfo *,tDhcpPool *, tDhcpHostEntry *);
VOID DhcpRcvdBootp(tDhcpPktInfo *,tDhcpPool *, tDhcpHostEntry *);
VOID DhcpSvPktInSocket (INT4);
VOID DhcpRcvAndProcessPkt(VOID);
INT4  DhcpSrvInit(VOID);
INT4 DhcpServerEnable(VOID);
INT4 DhcpServerDisable(VOID);
VOID DhcpSendOffer(tDhcpPktInfo *, tDhcpPool *, tDhcpHostEntry  *);
VOID DhcpAddParamsListOptions(tDhcpPktInfo *,tDhcpPktInfo *, tDhcpPool *,tDhcpHostEntry *);
VOID DhcpAddPoolEntry(tDhcpPool  *);
VOID DhcpRemovePoolEntry(tDhcpPool   *);
VOID DhcpAddBinding(tDhcpBinding  *);
VOID DhcpRemoveBinding(tDhcpBinding   *);
VOID DhcpDeleteBinding(tDhcpBinding   *);
VOID DhcpAddHostEntry(tDhcpHostEntry  *);
VOID DhcpRemoveHostEntry(tDhcpHostEntry   *);
VOID DhcpAddExcludedListNode(tExcludeIpAddr  **pList, tExcludeIpAddr *);
VOID DhcpRemoveExcludedListNode(tExcludeIpAddr **pList,tExcludeIpAddr *);
VOID DhcpAddOptionToList(tDhcpOptions  **pList, tDhcpOptions *);
VOID DhcpRemoveOptionFromList(tDhcpOptions **pList,tDhcpOptions *);
INT4 DhcpInitPoolRec(VOID);
VOID DhcpFreePoolRec(tDhcpPool *);
INT4 DhcpInitBindRec(VOID);
VOID DhcpFreeBindRec(tDhcpBinding *);
INT4 DhcpInitHostRec(VOID);
VOID DhcpFreeHostRec(tDhcpHostEntry *);
VOID DhcpFreeOptionRec(tDhcpOptions *);
VOID DhcpFreeSubnetPoolIcmpList(INT4);
VOID DhcpFreeSubnetPoolBindingList(INT4);
INT4 DhcpFreeSubnetPoolOptionList(INT4);
INT4 DhcpFreeSubnetPoolExcludeList(INT4, UINT1);
INT4 DhcpSrvCalculateIpCount(tDhcpPool *);
INT4 DhcpSCheckThreshold(tDhcpPool *);
INT4 DhcpIsHostIpAllocated (UINT4, UINT1 *, UINT4, UINT4, UINT4);
VOID DhcpSProcessQMsg (VOID);

INT4 DhcpSendBroadcast(INT1 *,UINT2 ,UINT4 );
INT4 DhcpSendUnicast (INT1 *, UINT2 , UINT4 ,  UINT4 , UINT2);
INT4 DhcpSendMessage(tDhcpPktInfo *, UINT4 ,UINT1 );
INT4 DhcpSendToSli(INT1 *,UINT2 ,UINT4  , UINT2    );
INT4 DhcpGetInterface(UINT4 ,UINT4 *);
INT4 DhcpGetOption(UINT1 ,tDhcpPktInfo *);
INT4 DhcpValidatePkt(UINT1 *,UINT2 );


INT1 DhcpGetBinding(tDhcpBinding **,INT1 );
tDhcpPool   *DhcpGetPoolEntryFromPkt(tDhcpPktInfo *);
tDhcpPool *DhcpGetPoolEntry(UINT4 u4PoolId);
tDhcpHostEntry *DhcpGetHostEntry(UINT4 , UINT1 *,UINT4 ,UINT4 u4PoolId);
tDhcpOptions   *DhcpGetOptionFromList(tDhcpOptions *pList,UINT4    u4Type);
tDhcpPool *DhcpGetFreePoolRec(VOID);
tDhcpBinding *DhcpGetFreeBindRec(VOID);
tDhcpHostEntry *DhcpGetFreeHostRec(VOID);
tDhcpOptions *DhcpGetFreeOptionRec(VOID);
tDhcpHostEntry *DhcpGetHostFromPkt(tDhcpPktInfo *pPkt,UINT4 u4PoolId);
UINT4 DhcpSelectIpAddress(tDhcpPool *pPoolEntry, UINT4 );
tDhcpBinding *DhcpGetBindingFromPkt(tDhcpPktInfo *,UINT4,INT1);
VOID DhcpAddConfiguredOptions(tDhcpPktInfo *pOut,tDhcpPktInfo *pIn, tDhcpPool *pPoolEntry,tDhcpHostEntry *pHostEntry);
UINT4 DhcpGetServerIdentifier(UINT4 u4IfIndex);

VOID DhcpProcessOfferReuseTimeOut(VOID);
VOID DhcpProcessLeaseExpireTimeOut(VOID);
VOID DhcpProcessIcmpProbeTimeOut(tDhcpIcmpNode * , INT1);

INT4 DhcpStartTimer(tTimerListId , tTmrAppTimer * , UINT4 );
INT4 DhcpStopTimer (tTimerListId , tTmrAppTimer * );
INT4 DhcpReStartTimer (tTimerListId , tTmrAppTimer * , UINT4 );
UINT4 DhcpLeaseExprTime(UINT4 u4LeaseTime);
INT4 DhcpValidateBootRequest(tDhcpPktInfo * pPktInfo);
UINT1 DhcpFindRequestState (tDhcpPktInfo * , UINT4 *, UINT4 *);
INT4 DHCPSendNAK (tDhcpPktInfo *);
INT4 DHCPSendACK (tDhcpPktInfo *,tDhcpHostEntry *,tDhcpPool *,UINT1 ,UINT4 );
tDhcpPktInfo *DhcpCreateOutPkt(VOID);
INT4 DhcpValidateConfOptions(UINT1 u1Type,UINT4 ,UINT1 *pValue);
INT4 DhcpProbeAddressAndSendOffer(UINT4 ,tDhcpPktInfo *,tDhcpPktInfo *, tDhcpPool *);
VOID DhcpNotifyWrongAddress(tDhcpPool *pPool,UINT4 u4Addr);
INT4 DhcpHostCompare(tDhcpHostEntry *pHost1,tDhcpHostEntry *pHost2);
INT4 DhcpHostOptCompare (tDhcpHostEntry * ,UINT4 ,tDhcpHostEntry * , UINT4 );
VOID DhcpShowBinding(VOID);
VOID DhcpRemoveIcmpProbeNode(tDhcpIcmpNode *pWaitNode);
INT4 DhcpIsCorrectSubnet(tDhcpPool *, UINT4 );
VOID DhcpSetDefNetmask(tDhcpPool *);
tDhcpBinding *DhcpGetBindingFromAddress(UINT4 );
UINT4 DhcpGetDefaultNetMask(UINT4 u4IpAddress);
const UINT1 *DhcpGetSysName(VOID);
UINT1 *DhcpGetBootFileName(tDhcpHostEntry *, UINT1 *);
tDhcpOptions * DhcpGetCofiguredOption(tDhcpHostEntry *,tDhcpPool *, UINT1 );
VOID DhcpStopProbingAddress(UINT4 u4IpAddress);
INT4 DhcpCheckIpAddress(tDhcpPool *pPool,UINT4 u4IpAddr);
tDhcpOptions  *DhcpGetActiveOptionFromList (tDhcpOptions *, UINT4 );

/*  Prototype for Functions used for ICMP Echo */
INT4 DhcpSOpenICMPSocket(VOID);
INT4 DhcpSCloseICMPSocket(VOID);
INT4 DhcpIcmpEchoRequest(UINT4 u4IpAddr);
VOID DhcpPktInIcmpSocket(INT4 i4SockFd);
VOID  DhcpProcessIcmpResponse(VOID);
tDhcpIcmpNode * DhcpGetIcmpNode(UINT4 u4IpAddr);


#endif
