/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsport.h,v 1.17 2011/12/06 10:37:49 siva Exp $ 
 *
 * Description: This file contains macro definitions and function    
 *              prototypes,which may change during porting.
 *******************************************************************/

#ifndef  __DHCPS_PORT_H
#define  __DHCPS_PORT_H

#include "lr.h"
#include "ip.h"
#include "fssocket.h"

/* Task and event related constants */

#define  DHCP_SERVER_TASK_NAME         (UINT1 *) "DHS"                               
#define  DHCP_SELECT_SERVER_TASK_NAME  (UINT1 *) "DSS"                               
#define  DHCP_PACKET_ARRIVAL_Q_NAME    (UINT1 *) "DHQ"                               
#define  DHCP_PACKET_ARRIVAL_Q_DEPTH  20
#define  DHCP_SERVER_TASK_PRIORITY    150

#define  DHCP_SERVER_TASK_MODE         OSIX_DEFAULT_TASK_MODE
#define  DHCP_PACKET_ARRIVAL_EVENT     0x00000001
#define  DHCP_TIMER_EXPIRY             0x00000002
#define  DHCP_Q_MSG          0x00000004
#define  DHCP_ICMP_PKT_ARRIVAL_EVENT   0x00000008

/* DHCP_MAX_SUBNETS = Number of interfaces in the system assuming we define one
 * subnet per interface. DHCP_MAX_HOST_IN_SUBNET = Number of Hosts or machines
 * in the LAN which use DHCP protocol.
 */

#define  DHCP_MAX_SUBNETS         DHCP_SRV_MAX_POOLS  
#define  DHCP_MAX_HOST_IN_SUBNET  DHCP_SRV_MAX_HOST_PER_POOL

/* Do not change these parameters without understanding the implications 
 * Number of host records is defined to maximum possible hosts in the LAN
 * assuming we configure static parameters for every host on all subnets.
 */

#define  DHCP_MAX_POOL_REC  DHCP_MAX_SUBNETS                              
#define  DHCP_MAX_BIND_REC  (DHCP_MAX_SUBNETS * DHCP_MAX_HOST_IN_SUBNET ) 
#define  DHCP_MAX_HOST_REC  (DHCP_MAX_SUBNETS * DHCP_MAX_HOST_IN_SUBNET ) 

/* Socket Related Calls */
#define dhcps_sockaddr_in struct sockaddr_in
#define dhcps_sockaddr struct sockaddr
#define dhcps_msghdr struct msghdr
#define dhcps_cmsghdr struct cmsghdr
#define dhcps_in_pktinfo struct in_pktinfo
#define dhcps_fd_set fd_set
#define dhcps_select select
#define dhcps_recvfrom recvfrom
#define dhcps_recvmsg recvmsg
#define dhcps_socket socket
#define dhcps_bind bind
#define dhcps_close close
#define dhcps_sendmsg  sendmsg
#define dhcps_sendto sendto
#define dhcps_setsockopt setsockopt

#define DHCPS_PROT_MUTEX_SEMA4          ((const UINT1 *)"DSPS")
#define DHCPS_SEM_CREATE_INIT_CNT       1
#define DHCP_SEM_FLAGS                  OSIX_DEFAULT_SEM_MODE 
/*end of Mutex definitions*/ 
#endif
