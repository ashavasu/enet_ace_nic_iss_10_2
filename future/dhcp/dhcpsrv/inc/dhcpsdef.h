/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsdef.h,v 1.27 2013/11/14 11:27:35 siva Exp $ 
 *
 * Description: This file contains macro definitions for dhcp module.
 *******************************************************************/

#ifndef  __DHCPS_DEF_H
#define  __DHCPS_DEF_H


#define  DHCP_SELECT_TIMEOUT_VAL       1

#define  DHCP_PROBE_TIMEOUT       (1+DHCP_PING_RESPONSE_TIME*DHCP_PING_RETRIES) 

#define  DHCP_ICMP_PROBE_SUCCESS       1
#define  DHCP_ICMP_PROBE_FAIL          2
#define  DHCP_ICMP_PROBE_PENDING       3

#define  DHCP_MAX_OFFER_TIMEOUT        120 /* Client's Multiple Timer Expiry 
          (60 sec) + Max Requests (4) * 
          interval of each request (15 sec) 
           */

#define  DHCP_MAX_NAME_LEN             64
#define  DHCPS_MAX_POOLTHRESHOLD       100
#define  DHCPS_DEF_POOLTHRESHOLD       75
#define  DHCPS_MIN_POOLTHRESHOLD       0

#define  DHCP_CHADDR_MAC_LEN           6 /*Length for ethernet MAC*/

#define  DHCP_DEF_CMP_PARAM            0
#define  DHCP_TRANS_ID                 1 
#define  DHCP_IP_ADDRESS               2
#define  DHCP_XID_AND_IP_ADDR          3
#define  DHCP_MAC_IPPORT              4

#define  DHCP_RESET                     1
#define  DHCP_NO_RESET                  2
/* Macros used for ICMP message */
#define   DHCPS_ICMP_ECHO              8
#define   DHCPS_ICMP_IDENTIFIER        23457

#define  DHCPS_SET                     1 
#define  DHCPS_NOT_SET                 0 
#define  DHCP_YES                      1 
#define  DHCP_NO                       0 
#define  DHCP_ENABLED                  1 
#define  DHCP_DISABLED                 2 
#define  DHCP_SNMP                     1 
#define  DHCP_NOT_SNMP                 0 

#define  DHCP_OFFER_REUSE              0 
#define  DHCP_LEASE_EXPIRE             1 
#define  DHCP_ICMP_PROBE_EXPIRE        2

#define  DHCP_ALLOC_DYNAMIC            1 
#define  DHCP_ALLOC_MANUAL             2 

#define  DHCP_OFFERED                  1 
#define  DHCP_ASSIGNED                 2 
#define  DHCP_FREE                     3 
#define  DHCP_EXPIRED                  4 
#define  DHCP_PROBING                  5 
#define  DHCP_BOOTP                    6 

/* values for the state in which in the client 
 * has generated dhcp request */
#define  DHCP_UNKNOWN                  1
#define  DHCP_SELECTING                1
#define  DHCP_INIT_REBOOT              2
#define  DHCP_RENEWING                 3
#define  DHCP_REBINDING                4

#define  DHCP_OPTION_DEF_GATEWAY       3

#define  ENTRY_ALREADY_EXISTS          1

#define  DHCP_DEF_LEASE_TIME           3600
#define  DHCP_GLOBAL_LEASE_TIMEOUT     60
#define  DHCP_DEF_OFFER_REUSE_TIME     5
#define  DHCP_DEF_BOOT_FILE            ""
#define  DHCP_DEF_ENABLE           DHCP_DISABLED

/* Macros ... */
#define  DHCP_MIN(x,y)                 (((x)<(y))?(x):(y))

#define  DHCP_HW_ADDR_TYPE(pPkt)       (pPkt->DhcpMsg.htype)                 
#define  DHCP_HW_ADDRESS(pPkt)         (pPkt->DhcpMsg.chaddr)                
#define  DHCP_HW_ADDR_LEN(pPkt)        (pPkt->DhcpMsg.hlen)                  


#define DHCPSRV_CMSG_LEN  (sizeof (struct cmsghdr) + sizeof (struct in_pktinfo))
#define DHCP_SERVER_Q_NAME             (UINT1 *) "DHCPSQ"
               
#define DHCP_IN_RANGE(StartRange,EndRange,Address)  \
                ( ((Address>=StartRange)&&(Address<=EndRange)) ? 1 : 0 )

/* 
 * 
 * DHCP_TRC# macros - mapped to UtlTrc(); 
 * Macros for Trace levels
 * */
#define  MGMT             0x00000004
#define  DHCP_PKT_TRC     0X00000008
#define  DEBUG_NONE       0
#define  DEBUG_TRC        0x00001000

#define  DHCP_NAME        "DHCP SRV" /* name printed with every message
                                      DHCP SRV: Your message.
                                      */

#define  DHCP_MASK        (UINT4) gi4DhcpDebugMask

#define DHCP_IS_ZERO_IP(u4Addr)  (u4Addr == 0)          

/* Following Macros are Mib specififed 
 * Debug Values 
 */  

#define DHCP_TRC(mask,fmt)\
    MOD_TRC(DHCP_MASK,mask,DHCP_NAME,fmt)
#define DHCP_TRC1(mask,fmt,arg1)\
    MOD_TRC_ARG1(DHCP_MASK,mask,DHCP_NAME,fmt,arg1)
#define DHCP_TRC2(mask,fmt,arg1,arg2)\
    MOD_TRC_ARG2(DHCP_MASK,mask,DHCP_NAME,fmt,arg1,arg2)
#define DHCP_TRC3(mask,fmt,arg1,arg2,arg3)\
    MOD_TRC_ARG3(DHCP_MASK,mask,DHCP_NAME,fmt,arg1,arg2,arg3)
#define DHCP_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
     MOD_TRC_ARG4(DHCPC_MASK,mask,DHCPC_NAME,fmt,arg1,arg2,arg3,arg4)
#define DHCP_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
    MOD_TRC_ARG5(DHCP_MASK,mask,DHCP_NAME,fmt,arg1,arg2,arg3,arg4,arg5)
#define DHCP_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
    MOD_TRC_ARG6(DHCP_MASK,mask,DHCP_NAME,fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#endif /* __DHCPS_DEF_H */
