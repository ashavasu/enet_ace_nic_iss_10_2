/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpstrap.h,v 1.8 2011/09/26 10:44:25 siva Exp $
 *
 * Description:This file contains the variable definitions   
 *             for the DHCP Trap system.              
 *
 *******************************************************************/
#ifndef _DHCPSTRAP_H
#define _DHCPSTRAP_H

#define DHCP_POOL_UTIL_TRAPS_OID "1.3.6.1.4.1.2076.84.4.0"

#define DHCP_TRAPS_OID_LEN       12 
#define DHCP_MAX_OBJ_LEN         256

enum
{
    DHCPS_POOL_UTIL_TRAP=1,
    DHCPS_POOL_INVALID_TRAP
};

typedef struct
{
    INT4  i4PoolId;
    INT4  i4Threshold;
} tDhcpPoolUtlTrap;

PUBLIC VOID
DhpSSnmpSendPoolUtlTrap PROTO ((INT4 i4PoolId, INT4 i4Threshold,
                                INT1 *pi1TrapsOid, UINT1 u1TrapOidLen));

VOID DhcpSSnmpSendTrap PROTO ((UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo));

tSNMP_OID_TYPE     *DhcpSMakeObjIdFromDotNew PROTO ((INT1 *pi1TextStr));

UINT1 *DhcpSParseSubIdNew PROTO ((UINT1 *pu1TempPtr, UINT4 *pu4Value));




extern VOID SNMP_AGT_RIF_Notify_V1_Or_V2_Trap (UINT4 u4_Version,
                                   tSNMP_OID_TYPE * pEnterpriseOid,
                                   UINT4 u4_gen_trap_type,
                                   UINT4 u4_spec_trap_type,
                                   tSNMP_VAR_BIND * vb_list);



#endif /* _MBSTRAP_H */
