/* $Id: dhsclipt.h,v 1.13 2016/05/09 11:37:38 siva Exp $*/
#ifndef _DHCPCLIPT_H
#define _DHCPCLIPT_H


#define DHCPS_DEF_BOOTSRV_IP  0x00000000
#define DHCP_POOL_DEF_ENDIP   0xffffffff
#define DHCP_DEF_SUBNET_MASK  0xff000000

/* Subnet options codes */

#define DNSSRV_OPT                     6
#define DOMAINNAME_OPT                15
#define NETBIOS_NAMESRV_OPT           44
#define NETBIOS_NODETYPE_OPT          46
#define DEFAULT_ROUTER_OPT             3
#define SUBNET_MASK_OPT                1

/* Function Prototypes */

UINT4 DhcpCliGetAddrPoolCount (VOID);
INT4 DhcpSrvProcessShowBinds (tCliHandle);
INT4 DhcpSrvProcessShowStats (tCliHandle);
INT4 DhcpSrvProcessShowConfig (tCliHandle);
INT4 DhcpSrvProcessShowGblOpts (tCliHandle);
VOID DhcpSrvSubnetOptTableInfo (tCliHandle CliHandle, INT4 i4PoolIndex,
                                INT4 i4OptType,INT4 *pi4PageStatus);
INT4 DhcpSrvProcessShowServerPools (tCliHandle);
INT4 DhcpSrvProcessResetBootServerFileName (tCliHandle);
INT4 DhcpSrvShowRunningConfig(tCliHandle CliHandle);
INT4 DhcpSrvShowRunningConfigScalar(tCliHandle CliHandle);
INT4 DhcpSrvShowRunningConfigTable(tCliHandle cliHandle);


INT4 DhcpSrvProcessDelBinding (tCliHandle, UINT4);
INT4 DhcpSrvProcessSetBootServer (tCliHandle, UINT4);
INT4 DhcpSrvProcessDelAddressPool (tCliHandle, UINT4);
INT4 DhcpSrvProcessIcmpEchoEnable (tCliHandle, UINT4);
INT4 DhcpSrvProcessDelGlobalOption (tCliHandle, UINT4);
INT4 DhcpSrvProcessSetServerEnable (tCliHandle, UINT4);
INT4 DhcpSrvProcessCreateAddressPool (tCliHandle, UINT4, UINT1 *);
INT4 DhcpSrvProcessDestroyAddressPool (tCliHandle, UINT4);
INT4 DhcpSrvProcessSetLeaseTime (tCliHandle, UINT4, INT4);
INT4 DhcpSrvProcessSetPoolThreshold (tCliHandle, UINT4, INT4);
INT4 DhcpSrvProcessSetTraceLevel (tCliHandle, INT4, UINT1);
INT4 DhcpSrvProcessSetAddrReuseTimeOut (tCliHandle, UINT4);
INT4 DhcpSrvProcessDelSubnetOption (tCliHandle, UINT4, UINT4);
INT4 DhcpSrvProcessSetBootServerFileName (tCliHandle, UINT1 *);
INT4 DhcpSrvProcessSetGlobalOption (tCliHandle, UINT4, UINT1 *, UINT4, UINT4);
INT4 DhcpSrvProcessAddExcludeAddrPool (tCliHandle, UINT4, UINT4, UINT4);
INT4 DhcpSrvProcessDelExcludeAddrPool (tCliHandle, UINT4, UINT4, UINT4);
INT4 DhcpSrvProcessAddExcludeAddrPoolAll (tCliHandle, UINT4, UINT4);
INT4 DhcpSrvProcessDelExcludeAddrPoolAll (tCliHandle, UINT4, UINT4);
INT4 DhcpSrvProcessDelHostOption (tCliHandle, UINT4, UINT4, UINT1 *, UINT4);
INT4 DhcpSrvProcessAddSubnetOption (tCliHandle, UINT4, UINT4, UINT1 *, UINT4, UINT4);
INT4 DhcpSrvProcessValidateOption (tCliHandle, UINT4, UINT4 *, UINT1 *, UINT1, UINT1 *);
INT4 DhcpSrvProcessAddHostOption (tCliHandle, UINT4, UINT4, UINT1 *, UINT4, UINT1 *, UINT4);
INT4 DhcpSrvProcessAddAddressPool (tCliHandle, UINT4, UINT4, UINT4, UINT4, UINT4);
VOID IssDhcpSrvShowDebugging (tCliHandle);
VOID DhcpSrvDisplayOptionType(tCliHandle, INT4, UINT1 *, UINT4);
UINT4 DhcpSrvGetExpiryTime(UINT1 *, UINT4);
INT4 DhcpSrvProcessAddHostIpConfig (tCliHandle, UINT4, UINT1 *, UINT4, UINT4);
INT4 DhcpSrvProcessDelHostIpConfig (tCliHandle, UINT4, UINT1 *, UINT4);
INT4 DhcpSrvProcessDelHostConfig (tCliHandle, UINT4, UINT1 *, UINT4);
INT4 DhcpSrvProcessSetClearCounters (tCliHandle CliHandle);

#endif /* _DHCPCLIPT_H */


