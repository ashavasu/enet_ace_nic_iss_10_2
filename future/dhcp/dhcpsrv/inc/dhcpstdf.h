/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpstdf.h,v 1.24 2016/05/09 11:37:38 siva Exp $ 
 *
 * Description: This file contains all new structure definitions for
 *              dhcp module.
 *******************************************************************/

#ifndef  __DHCPS_TDF_H
#define  __DHCPS_TDF_H

/* Ip-address range to be excluded */
typedef struct excludeaddr {
    UINT4   u4StartIpAddr;
    UINT4   u4EndIpAddr;
    UINT4   u4Method;
    struct excludeaddr  *pNext;
}tExcludeIpAddr;

/* dhcp option structure */
typedef struct sdhcpsoption {
  UINT4 u4Type;
  UINT4 u4Len;
  UINT1 u1Value[DHCP_MAX_OPT_LEN+1];
  UINT4 u4Status;
  UINT4 u4CmdType;
  struct sdhcpsoption *pNext;
}tDhcpOptions; 

/* host specific entry  */
typedef struct sdhcpshost {
   tDhcpOptions *pOptions; /* option list  */
   struct sdhcpshost *pNext;
   UINT1 aClientMac[DHCP_MAX_CID_LEN];
   UINT4 u4StaticIpAddr;   /* A static IP address assigned for a specific MAC */
   UINT4 u4PoolId;
   UINT4 u4Status; /* SNMP RowStatus of the entry */
   UINT4 u4AllocOrFree;     /* If set to 1 entry is allocated. Otherwise free */
   UINT4 u4BootSrvIpAddr;
   UINT1 u1HwType; /* hardware address type. value 0 for client id */
   UINT1 u1HostIdLen;
   UINT1 au1BootFile[DHCP_MAX_NAME_LEN + 1];
   UINT1 au1Pad;
}tDhcpHostEntry;

typedef struct sdhcpspool {

   tDhcpOptions   *pOptions;
   tExcludeIpAddr *pExcludeIpAddr;
   struct sdhcpspool *pNext;
   UINT4 u4PoolId;        /* Unique Identifier that identifies a Pool */
   UINT4 u4Subnet;        /* Subnet - IP Address assoicated e.g 70.0.0.0 */
   UINT4 u4NetMask;
   UINT4 u4Status;
   UINT4 u4IfIndex;
   UINT4 u4StartIpAddr;   /* Beginning of IP Address Pool e.g 70.0.0.1 */
   UINT4 u4EndIpAddr;     /* End of IP Address Pool e.g. 70.0.0.255 */
   UINT4 u4LeaseTime;
   UINT4 u4Threshold;     /*Threshold level for this pool*/
   UINT4 u4IpCount;       /*Total number of IP addresses available in the pool*/
   UINT4 u4BoundIpCount;  /*Total number of IP addresses already allocated */
                          /*from the pool*/
   UINT1 au1PoolName[DHCP_MAX_NAME_LEN + 1];/* Null terminated unique name for the pool */
   UINT1 au1Pad[3];
   
}tDhcpPool;

typedef struct sdhcpsbinding {
   UINT1 aBindMac[DHCP_MAX_CID_LEN];
   UINT1 u1HwType;
   UINT1 u1HostIdLen;
   UINT1 u1Mask;           /* If set ,the address is reserved for allocation */
   UINT1 Padding;

   UINT4 u4IpAddress;        /* Allocated IP address */
   UINT4 u4Subnet;           /* Subnet if the pool from which IP is allocated*/
   UINT4 u4LeaseExprTime;    /* Time Remaining for the lease to expire */

   UINT4 u4PoolId;
   UINT4 u4AllocMethod;    /* Manual or Dynamic */
   UINT4 u4xid;

   struct sdhcpsbinding *pNext;
} tDhcpBinding;


/* dhcpSrvCounters */
typedef struct sdhcpCounters {
UINT4   u4DhcpCountDiscovers;
UINT4   u4DhcpCountRequests;
UINT4   u4DhcpCountReleases;
UINT4   u4DhcpCountDeclines;
UINT4   u4DhcpCountInforms;
UINT4   u4DhcpCountInvalids;
UINT4   u4DhcpCountOffers;
UINT4   u4DhcpCountAcks;
UINT4   u4DhcpCountNacks;
UINT4   u4DhcpCountDroppedUnKnownClient;
UINT4   u4DhcpCountDroppedNotServingSubnet;
UINT4   u4DhcpCountResetCounters;
} tDhcpCounters;

/* Info about received/outgoing dhcp packet. */
typedef struct sdhcppktInfo {

    tDhcpMsgHdr DhcpMsg;   /* dhcp message header and options           */
    UINT4       u4IfIndex; /* Interface number for the dhcp packet      */
    UINT4       u4IpAddr;  /* destination Ip-Address of the packet      */
    UINT2       u2Len;     /* length of the options in the dhcp message */
    UINT2       u2BufLen;  /* length of the dhcp message */

    UINT2       u2MaxMessageSize;    /* Max DHCP Message size */

    UINT1       u1Type;    /* DHCP Message type */
    UINT1       u1OverLoad;    /* DHCP Message type */

} tDhcpPktInfo;


/* used for adding/extracting options to/from dhcp message */    
typedef struct sdhcptag {
    UINT1   Type;
    UINT1   Len;
    UINT2   Padding;
    UINT1   *Val;
} tDhcpTag;


/* icmp probe node */
typedef struct sdhcptmrnod {
    tTmrAppTimer        TmrNode;
    UINT4               u4ProbeAddr;   /* ip-Address offered       */
    UINT4               ciaddr;        /* client ip-address        */
    tDhcpPktInfo       *pPktInfo;      /* Outgoing packet info     */
    tDhcpPool          *pPoolEntry;    /* pool entry for the offer */
    tDhcpOptions        ClientId;      /* clinet identifier        */
    UINT2               u2IcmpSeqId;  /* Sequence ID of last Echo Sent */
    UINT2               u2EchoSentCnt; /* Number of ICMP Echo Sent */
    struct sdhcptmrnod *pNext;
    }tDhcpIcmpNode;


/* Max sizes of mem pools for Subnet pool, Binding and host entries */
typedef struct sdhcpconfig {
   UINT4 u4MaxPoolRec;
   UINT4 u4MaxBindRec;
   UINT4 u4MaxHostRec;
   tDhcpPool    *pSubnetMemPool;
   tDhcpHostEntry *pHostMemPool;
   tDhcpBinding  *pBindMemPool;
}tDhcpConfig;

typedef struct sDhcpIcmpHeader
{
    INT1       i1Type;
    INT1       i1Code;
    UINT2      u2Cksum;
    tIcmpArgs  args;
} tDhcpSIcmpHdr;

typedef struct sdhcpserverpoolrec 
{
    tDhcpPool      asDhcpSrvPoolRec[DHCP_MAX_POOL_REC];
} tDhcpSrvPoolRec;
#ifdef L2RED_WANTED
typedef struct _DhcpSRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tDhcpSRmCtrlMsg;
#endif /* L2RED_WANTED */

typedef struct sdhcpserverhostrec 
{
    tDhcpHostEntry      asDhcpSrvHostRec[DHCP_MAX_HOST_REC];
} tDhcpSrvHostRec;

typedef struct _DhcpSQMsg {
 UINT4 u4MsgType;
#ifdef L2RED_WANTED
     tDhcpSRmCtrlMsg RmCtrlMsg; /* Control message from RM */
#endif
} tDhcpSQMsg;

typedef struct sdhcpserverbindrec 
{
    tDhcpBinding      asDhcpSrvBindRec[DHCP_MAX_BIND_REC];
} tDhcpSrvBindRec;

typedef struct sdhcpservertempoptpkt 
{
    UINT1      asDhcpSrvTmpOptPkt[DHCP_MAX_OUT_OPTION_LEN];
} tDhcpSrvTempOptPkt;

/* Max message size */
typedef struct sdhcpservermessage
{
    INT1    asDhcpSrvMessage[DHCP_MAX_OUTMSG_SIZE];
} tDhcpSrvMessage;

typedef struct sdhcprcvpkt
{
    INT1                i1RecvBuff[DHCP_MAX_MTU];
} tDhcpRcvPkt;


#endif
