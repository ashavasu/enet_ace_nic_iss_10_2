
# ifndef fsdhcOGP_H
# define fsdhcOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_DHCPSRVCONFIG                                (0)
# define SNMP_OGP_INDEX_DHCPSRVSUBNETPOOLCONFIGTABLE                 (1)
# define SNMP_OGP_INDEX_DHCPSRVEXCLUDEIPADDRESSTABLE                 (2)
# define SNMP_OGP_INDEX_DHCPSRVGBLOPTTABLE                           (3)
# define SNMP_OGP_INDEX_DHCPSRVSUBNETOPTTABLE                        (4)
# define SNMP_OGP_INDEX_DHCPSRVHOSTOPTTABLE                          (5)
# define SNMP_OGP_INDEX_DHCPSRVHOSTCONFIGTABLE                       (6)
# define SNMP_OGP_INDEX_DHCPSRVBINDINGTABLE                          (7)
# define SNMP_OGP_INDEX_DHCPSRVCOUNTERS                              (8)

#endif /*  fsdhcpsOGP_H  */
