# include  "include.h"
# include  "dhcpsmid.h"
# include  "dhcpslow.h"
# include  "dhcpscon.h"
# include  "dhcpsogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsdhcpscli.h"

/****************************************************************************
 Function   : dhcpSrvConfigGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvConfigGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_dhcpSrvConfig_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

    UINT4               u4_addr_ret_val_dhcpSrvBootServerAddress;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvDefBootFilename = NULL;

/*** DECLARATION_END ***/

    LEN_dhcpSrvConfig_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_dhcpSrvConfig_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_dhcpSrvConfig_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_dhcpSrvConfig_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case DHCPSRVENABLE:
        {
            i1_ret_val = nmhGetDhcpSrvEnable (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVDEBUGLEVEL:
        {
            i1_ret_val = nmhGetDhcpSrvDebugLevel (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVOFFERREUSETIMEOUT:
        {
            i1_ret_val = nmhGetDhcpSrvOfferReuseTimeOut (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVICMPECHOENABLE:
        {
            i1_ret_val = nmhGetDhcpSrvIcmpEchoEnable (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBOOTSERVERADDRESS:
        {
            i1_ret_val =
                nmhGetDhcpSrvBootServerAddress
                (&u4_addr_ret_val_dhcpSrvBootServerAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvBootServerAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVDEFBOOTFILENAME:
        {
            poctet_retval_dhcpSrvDefBootFilename =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvDefBootFilename == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvDefBootFilename
                (poctet_retval_dhcpSrvDefBootFilename);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvDefBootFilename;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvDefBootFilename);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBOOTPCLIENTSSUPPORTED:
        {
            i1_ret_val = nmhGetDhcpSrvBootpClientsSupported (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVAUTOMATICBOOTPENABLED:
        {
            i1_ret_val = nmhGetDhcpSrvAutomaticBootpEnabled (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvConfigSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvConfigSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvBootServerAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVENABLE:
        {
            i1_ret_val = nmhSetDhcpSrvEnable (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVDEBUGLEVEL:
        {
            i1_ret_val = nmhSetDhcpSrvDebugLevel (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVOFFERREUSETIMEOUT:
        {
            i1_ret_val =
                nmhSetDhcpSrvOfferReuseTimeOut (p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVICMPECHOENABLE:
        {
            i1_ret_val = nmhSetDhcpSrvIcmpEchoEnable (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVBOOTSERVERADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvBootServerAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvBootServerAddress
                (u4_addr_val_dhcpSrvBootServerAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVDEFBOOTFILENAME:
        {
            i1_ret_val = nmhSetDhcpSrvDefBootFilename (p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVBOOTPCLIENTSSUPPORTED:
        {
            i1_ret_val =
                nmhSetDhcpSrvBootpClientsSupported (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVAUTOMATICBOOTPENABLED:
        {
            i1_ret_val =
                nmhSetDhcpSrvAutomaticBootpEnabled (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvConfigTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvConfigTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvBootServerAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case DHCPSRVENABLE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvEnable (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVDEBUGLEVEL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvDebugLevel (&u4ErrorCode,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVOFFERREUSETIMEOUT:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_TIME_TICKS)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvOfferReuseTimeOut (&u4ErrorCode,
                                                   p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVICMPECHOENABLE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvIcmpEchoEnable (&u4ErrorCode,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVBOOTSERVERADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvBootServerAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvBootServerAddress (&u4ErrorCode,
                                                   u4_addr_val_dhcpSrvBootServerAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVDEFBOOTFILENAME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvDefBootFilename (&u4ErrorCode,
                                                 p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVBOOTPCLIENTSSUPPORTED:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvBootpClientsSupported (&u4ErrorCode,
                                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVAUTOMATICBOOTPENABLED:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvAutomaticBootpEnabled (&u4ErrorCode,
                                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvSubnetPoolConfigEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvSubnetPoolConfigEntryGet (tSNMP_OID_TYPE * p_in_db,
                                 tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                 UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvSubnetPoolConfigTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;
    INT4                i4_next_dhcpSrvSubnetPoolIndex = FALSE;

    UINT4               u4_addr_ret_val_dhcpSrvSubnetSubnet;
    UINT4               u4_addr_ret_val_dhcpSrvSubnetMask;
    UINT4               u4_addr_ret_val_dhcpSrvSubnetStartIpAddress;
    UINT4               u4_addr_ret_val_dhcpSrvSubnetEndIpAddress;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvSubnetPoolName = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvSubnetPoolConfigTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_dhcpSrvSubnetPoolConfigTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_dhcpSrvSubnetPoolIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvSubnetPoolConfigTable
                     (i4_dhcpSrvSubnetPoolIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvSubnetPoolIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable
                     (&i4_dhcpSrvSubnetPoolIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvSubnetPoolIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvSubnetPoolIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvSubnetPoolConfigTable
                     (i4_dhcpSrvSubnetPoolIndex,
                      &i4_next_dhcpSrvSubnetPoolIndex)) == SNMP_SUCCESS)
                {
                    i4_dhcpSrvSubnetPoolIndex = i4_next_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvSubnetPoolIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVSUBNETPOOLINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dhcpSrvSubnetPoolIndex;
            }
            else
            {
                i4_return_val = i4_next_dhcpSrvSubnetPoolIndex;
            }
            break;
        }
        case DHCPSRVSUBNETSUBNET:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetSubnet (i4_dhcpSrvSubnetPoolIndex,
                                           &u4_addr_ret_val_dhcpSrvSubnetSubnet);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvSubnetSubnet);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETPORTNUMBER:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetPortNumber (i4_dhcpSrvSubnetPoolIndex,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETMASK:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetMask (i4_dhcpSrvSubnetPoolIndex,
                                         &u4_addr_ret_val_dhcpSrvSubnetMask);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvSubnetMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETSTARTIPADDRESS:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetStartIpAddress (i4_dhcpSrvSubnetPoolIndex,
                                                   &u4_addr_ret_val_dhcpSrvSubnetStartIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvSubnetStartIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETENDIPADDRESS:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetEndIpAddress (i4_dhcpSrvSubnetPoolIndex,
                                                 &u4_addr_ret_val_dhcpSrvSubnetEndIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvSubnetEndIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETLEASETIME:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetLeaseTime (i4_dhcpSrvSubnetPoolIndex,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETPOOLNAME:
        {
            poctet_retval_dhcpSrvSubnetPoolName =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvSubnetPoolName == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvSubnetPoolName (i4_dhcpSrvSubnetPoolIndex,
                                             poctet_retval_dhcpSrvSubnetPoolName);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvSubnetPoolName;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvSubnetPoolName);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETPOOLROWSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetPoolRowStatus (i4_dhcpSrvSubnetPoolIndex,
                                                  &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvSubnetPoolConfigEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvSubnetPoolConfigEntrySet (tSNMP_OID_TYPE * p_in_db,
                                 tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                 tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvSubnetSubnet;
    UINT4               u4_addr_val_dhcpSrvSubnetMask;
    UINT4               u4_addr_val_dhcpSrvSubnetStartIpAddress;
    UINT4               u4_addr_val_dhcpSrvSubnetEndIpAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVSUBNETSUBNET:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetSubnet =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvSubnetSubnet (i4_dhcpSrvSubnetPoolIndex,
                                           u4_addr_val_dhcpSrvSubnetSubnet);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETPORTNUMBER:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetPortNumber (i4_dhcpSrvSubnetPoolIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETMASK:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetMask =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvSubnetMask (i4_dhcpSrvSubnetPoolIndex,
                                         u4_addr_val_dhcpSrvSubnetMask);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETSTARTIPADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetStartIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvSubnetStartIpAddress (i4_dhcpSrvSubnetPoolIndex,
                                                   u4_addr_val_dhcpSrvSubnetStartIpAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETENDIPADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetEndIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvSubnetEndIpAddress (i4_dhcpSrvSubnetPoolIndex,
                                                 u4_addr_val_dhcpSrvSubnetEndIpAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETLEASETIME:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetLeaseTime (i4_dhcpSrvSubnetPoolIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETPOOLNAME:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetPoolName (i4_dhcpSrvSubnetPoolIndex,
                                             p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETPOOLROWSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetPoolRowStatus (i4_dhcpSrvSubnetPoolIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPSRVSUBNETPOOLINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvSubnetPoolConfigEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvSubnetPoolConfigEntryTest (tSNMP_OID_TYPE * p_in_db,
                                  tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                  tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvSubnetSubnet;
    UINT4               u4_addr_val_dhcpSrvSubnetMask;
    UINT4               u4_addr_val_dhcpSrvSubnetStartIpAddress;
    UINT4               u4_addr_val_dhcpSrvSubnetEndIpAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvSubnetPoolConfigTable(i4_dhcpSrvSubnetPoolIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVSUBNETSUBNET:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetSubnet =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetSubnet (&u4ErrorCode,
                                              i4_dhcpSrvSubnetPoolIndex,
                                              u4_addr_val_dhcpSrvSubnetSubnet);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETPORTNUMBER:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetPortNumber (&u4ErrorCode,
                                                  i4_dhcpSrvSubnetPoolIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETMASK:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetMask =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetMask (&u4ErrorCode,
                                            i4_dhcpSrvSubnetPoolIndex,
                                            u4_addr_val_dhcpSrvSubnetMask);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETSTARTIPADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetStartIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetStartIpAddress (&u4ErrorCode,
                                                      i4_dhcpSrvSubnetPoolIndex,
                                                      u4_addr_val_dhcpSrvSubnetStartIpAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETENDIPADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvSubnetEndIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetEndIpAddress (&u4ErrorCode,
                                                    i4_dhcpSrvSubnetPoolIndex,
                                                    u4_addr_val_dhcpSrvSubnetEndIpAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETLEASETIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetLeaseTime (&u4ErrorCode,
                                                 i4_dhcpSrvSubnetPoolIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETPOOLNAME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetPoolName (&u4ErrorCode,
                                                i4_dhcpSrvSubnetPoolIndex,
                                                p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETPOOLROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetPoolRowStatus (&u4ErrorCode,
                                                     i4_dhcpSrvSubnetPoolIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPSRVSUBNETPOOLINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvExcludeIpAddressEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvExcludeIpAddressEntryGet (tSNMP_OID_TYPE * p_in_db,
                                 tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                 UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvExcludeIpAddressTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;
    INT4                i4_next_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dhcpSrvExcludeStartIpAddress = FALSE;
    UINT4               u4_addr_next_dhcpSrvExcludeStartIpAddress = FALSE;
    UINT1               u1_addr_dhcpSrvExcludeStartIpAddress[ADDR_LEN] =
        NULL_STRING;
    UINT1               u1_addr_next_dhcpSrvExcludeStartIpAddress[ADDR_LEN] =
        NULL_STRING;

    UINT4               u4_addr_ret_val_dhcpSrvExcludeEndIpAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvExcludeIpAddressTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + ADDR_LEN;

            if (LEN_dhcpSrvExcludeIpAddressTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_dhcpSrvSubnetPoolIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dhcpSrvExcludeStartIpAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dhcpSrvExcludeStartIpAddress =
                    OSIX_NTOHL (*
                                ((UINT4
                                  *) (u1_addr_dhcpSrvExcludeStartIpAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvExcludeIpAddressTable
                     (i4_dhcpSrvSubnetPoolIndex,
                      u4_addr_dhcpSrvExcludeStartIpAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvSubnetPoolIndex;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dhcpSrvExcludeStartIpAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvExcludeIpAddressTable
                     (&i4_dhcpSrvSubnetPoolIndex,
                      &u4_addr_dhcpSrvExcludeStartIpAddress)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvSubnetPoolIndex;
                    *((UINT4 *) (u1_addr_dhcpSrvExcludeStartIpAddress)) =
                        OSIX_HTONL (u4_addr_dhcpSrvExcludeStartIpAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dhcpSrvExcludeStartIpAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvSubnetPoolIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dhcpSrvExcludeStartIpAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dhcpSrvExcludeStartIpAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *)
                                     (u1_addr_dhcpSrvExcludeStartIpAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvExcludeIpAddressTable
                     (i4_dhcpSrvSubnetPoolIndex,
                      &i4_next_dhcpSrvSubnetPoolIndex,
                      u4_addr_dhcpSrvExcludeStartIpAddress,
                      &u4_addr_next_dhcpSrvExcludeStartIpAddress)) ==
                    SNMP_SUCCESS)
                {
                    i4_dhcpSrvSubnetPoolIndex = i4_next_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvSubnetPoolIndex;
                    u4_addr_dhcpSrvExcludeStartIpAddress =
                        u4_addr_next_dhcpSrvExcludeStartIpAddress;
                    *((UINT4 *) (u1_addr_next_dhcpSrvExcludeStartIpAddress)) =
                        OSIX_HTONL (u4_addr_dhcpSrvExcludeStartIpAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_dhcpSrvExcludeStartIpAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVEXCLUDESTARTIPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_dhcpSrvExcludeStartIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_dhcpSrvExcludeStartIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DHCPSRVEXCLUDEENDIPADDRESS:
        {
            i1_ret_val =
                nmhGetDhcpSrvExcludeEndIpAddress (i4_dhcpSrvSubnetPoolIndex,
                                                  u4_addr_dhcpSrvExcludeStartIpAddress,
                                                  &u4_addr_ret_val_dhcpSrvExcludeEndIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvExcludeEndIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVEXCLUDEADDRESSROWSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvExcludeAddressRowStatus (i4_dhcpSrvSubnetPoolIndex,
                                                      u4_addr_dhcpSrvExcludeStartIpAddress,
                                                      &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvExcludeIpAddressEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvExcludeIpAddressEntrySet (tSNMP_OID_TYPE * p_in_db,
                                 tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                 tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dhcpSrvExcludeStartIpAddress = FALSE;
    UINT1               u1_addr_dhcpSrvExcludeStartIpAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvExcludeEndIpAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += ADDR_LEN;
        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_dhcpSrvExcludeStartIpAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_dhcpSrvExcludeStartIpAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_dhcpSrvExcludeStartIpAddress)));

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVEXCLUDEENDIPADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvExcludeEndIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvExcludeEndIpAddress (i4_dhcpSrvSubnetPoolIndex,
                                                  u4_addr_dhcpSrvExcludeStartIpAddress,
                                                  u4_addr_val_dhcpSrvExcludeEndIpAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVEXCLUDEADDRESSROWSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvExcludeAddressRowStatus (i4_dhcpSrvSubnetPoolIndex,
                                                      u4_addr_dhcpSrvExcludeStartIpAddress,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPSRVEXCLUDESTARTIPADDRESS:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvExcludeIpAddressEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvExcludeIpAddressEntryTest (tSNMP_OID_TYPE * p_in_db,
                                  tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                                  tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dhcpSrvExcludeStartIpAddress = FALSE;
    UINT1               u1_addr_dhcpSrvExcludeStartIpAddress[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvExcludeEndIpAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_dhcpSrvExcludeStartIpAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_dhcpSrvExcludeStartIpAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_dhcpSrvExcludeStartIpAddress)));

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvExcludeIpAddressTable(i4_dhcpSrvSubnetPoolIndex , u4_addr_dhcpSrvExcludeStartIpAddress)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVEXCLUDEENDIPADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvExcludeEndIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvExcludeEndIpAddress (&u4ErrorCode,
                                                     i4_dhcpSrvSubnetPoolIndex,
                                                     u4_addr_dhcpSrvExcludeStartIpAddress,
                                                     u4_addr_val_dhcpSrvExcludeEndIpAddress);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVEXCLUDEADDRESSROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvExcludeAddressRowStatus (&u4ErrorCode,
                                                         i4_dhcpSrvSubnetPoolIndex,
                                                         u4_addr_dhcpSrvExcludeStartIpAddress,
                                                         p_value->
                                                         i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPSRVEXCLUDESTARTIPADDRESS:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvGblOptEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvGblOptEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvGblOptTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_dhcpSrvGblOptType = FALSE;
    INT4                i4_next_dhcpSrvGblOptType = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvGblOptVal = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvGblOptTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_dhcpSrvGblOptTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_dhcpSrvGblOptType =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvGblOptTable
                     (i4_dhcpSrvGblOptType)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvGblOptType;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvGblOptTable (&i4_dhcpSrvGblOptType))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvGblOptType;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvGblOptType =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvGblOptTable (i4_dhcpSrvGblOptType,
                                                        &i4_next_dhcpSrvGblOptType))
                    == SNMP_SUCCESS)
                {
                    i4_dhcpSrvGblOptType = i4_next_dhcpSrvGblOptType;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvGblOptType;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVGBLOPTTYPE:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dhcpSrvGblOptType;
            }
            else
            {
                i4_return_val = i4_next_dhcpSrvGblOptType;
            }
            break;
        }
        case DHCPSRVGBLOPTLEN:
        {
            i1_ret_val =
                nmhGetDhcpSrvGblOptLen (i4_dhcpSrvGblOptType, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVGBLOPTVAL:
        {
            poctet_retval_dhcpSrvGblOptVal =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvGblOptVal == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvGblOptVal (i4_dhcpSrvGblOptType,
                                        poctet_retval_dhcpSrvGblOptVal);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvGblOptVal;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvGblOptVal);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVGBLOPTROWSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvGblOptRowStatus (i4_dhcpSrvGblOptType,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvGblOptEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvGblOptEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_dhcpSrvGblOptType = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_dhcpSrvGblOptType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVGBLOPTLEN:
        {
            i1_ret_val =
                nmhSetDhcpSrvGblOptLen (i4_dhcpSrvGblOptType,
                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVGBLOPTVAL:
        {
            i1_ret_val =
                nmhSetDhcpSrvGblOptVal (i4_dhcpSrvGblOptType,
                                        p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVGBLOPTROWSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvGblOptRowStatus (i4_dhcpSrvGblOptType,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPSRVGBLOPTTYPE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvGblOptEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvGblOptEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_dhcpSrvGblOptType = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvGblOptType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvGblOptTable(i4_dhcpSrvGblOptType)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVGBLOPTLEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvGblOptLen (&u4ErrorCode, i4_dhcpSrvGblOptType,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVGBLOPTVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvGblOptVal (&u4ErrorCode, i4_dhcpSrvGblOptType,
                                           p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVGBLOPTROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvGblOptRowStatus (&u4ErrorCode,
                                                 i4_dhcpSrvGblOptType,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPSRVGBLOPTTYPE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvSubnetOptEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvSubnetOptEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvSubnetOptTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;
    INT4                i4_next_dhcpSrvSubnetPoolIndex = FALSE;

    INT4                i4_dhcpSrvSubnetOptType = FALSE;
    INT4                i4_next_dhcpSrvSubnetOptType = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvSubnetOptVal = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvSubnetOptTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + INTEGER_LEN;

            if (LEN_dhcpSrvSubnetOptTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_dhcpSrvSubnetPoolIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_dhcpSrvSubnetOptType =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvSubnetOptTable
                     (i4_dhcpSrvSubnetPoolIndex,
                      i4_dhcpSrvSubnetOptType)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvSubnetPoolIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvSubnetOptType;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvSubnetOptTable
                     (&i4_dhcpSrvSubnetPoolIndex,
                      &i4_dhcpSrvSubnetOptType)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvSubnetOptType;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvSubnetPoolIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvSubnetOptType =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvSubnetOptTable
                     (i4_dhcpSrvSubnetPoolIndex,
                      &i4_next_dhcpSrvSubnetPoolIndex, i4_dhcpSrvSubnetOptType,
                      &i4_next_dhcpSrvSubnetOptType)) == SNMP_SUCCESS)
                {
                    i4_dhcpSrvSubnetPoolIndex = i4_next_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvSubnetPoolIndex;
                    i4_dhcpSrvSubnetOptType = i4_next_dhcpSrvSubnetOptType;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvSubnetOptType;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVSUBNETOPTTYPE:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dhcpSrvSubnetOptType;
            }
            else
            {
                i4_return_val = i4_next_dhcpSrvSubnetOptType;
            }
            break;
        }
        case DHCPSRVSUBNETOPTLEN:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetOptLen (i4_dhcpSrvSubnetPoolIndex,
                                           i4_dhcpSrvSubnetOptType,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETOPTVAL:
        {
            poctet_retval_dhcpSrvSubnetOptVal =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvSubnetOptVal == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvSubnetOptVal (i4_dhcpSrvSubnetPoolIndex,
                                           i4_dhcpSrvSubnetOptType,
                                           poctet_retval_dhcpSrvSubnetOptVal);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvSubnetOptVal;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvSubnetOptVal);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVSUBNETOPTROWSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvSubnetOptRowStatus (i4_dhcpSrvSubnetPoolIndex,
                                                 i4_dhcpSrvSubnetOptType,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvSubnetOptEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvSubnetOptEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    INT4                i4_dhcpSrvSubnetOptType = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetOptType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVSUBNETOPTLEN:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetOptLen (i4_dhcpSrvSubnetPoolIndex,
                                           i4_dhcpSrvSubnetOptType,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETOPTVAL:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetOptVal (i4_dhcpSrvSubnetPoolIndex,
                                           i4_dhcpSrvSubnetOptType,
                                           p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVSUBNETOPTROWSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvSubnetOptRowStatus (i4_dhcpSrvSubnetPoolIndex,
                                                 i4_dhcpSrvSubnetOptType,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPSRVSUBNETOPTTYPE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvSubnetOptEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvSubnetOptEntryTest (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    INT4                i4_dhcpSrvSubnetOptType = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetOptType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvSubnetOptTable(i4_dhcpSrvSubnetPoolIndex , i4_dhcpSrvSubnetOptType)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVSUBNETOPTLEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetOptLen (&u4ErrorCode,
                                              i4_dhcpSrvSubnetPoolIndex,
                                              i4_dhcpSrvSubnetOptType,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETOPTVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetOptVal (&u4ErrorCode,
                                              i4_dhcpSrvSubnetPoolIndex,
                                              i4_dhcpSrvSubnetOptType,
                                              p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVSUBNETOPTROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvSubnetOptRowStatus (&u4ErrorCode,
                                                    i4_dhcpSrvSubnetPoolIndex,
                                                    i4_dhcpSrvSubnetOptType,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPSRVSUBNETOPTTYPE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvHostOptEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvHostOptEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvHostOptTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvHostType = FALSE;
    INT4                i4_next_dhcpSrvHostType = FALSE;

    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_dhcpSrvHostId = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_dhcpSrvHostId = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_dhcpSrvHostId = NULL;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;
    INT4                i4_next_dhcpSrvSubnetPoolIndex = FALSE;

    INT4                i4_dhcpSrvHostOptType = FALSE;
    INT4                i4_next_dhcpSrvHostOptType = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvHostOptVal = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_len_Octet_index_dhcpSrvHostId =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_dhcpSrvHostId;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvHostOptTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN +
                i4_len_Octet_index_dhcpSrvHostId + LEN_OF_VARIABLE_LEN_INDEX +
                INTEGER_LEN + INTEGER_LEN;

            if (LEN_dhcpSrvHostOptTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_dhcpSrvHostId =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
                if ((poctet_dhcpSrvHostId == NULL))
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /* Extracting The Integer Index. */
                i4_dhcpSrvHostType =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_dhcpSrvHostId;
                     i4_count++, i4_offset++)
                    poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /* Extracting The Integer Index. */
                i4_dhcpSrvSubnetPoolIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_dhcpSrvHostOptType =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvHostOptTable
                     (i4_dhcpSrvHostType, poctet_dhcpSrvHostId,
                      i4_dhcpSrvSubnetPoolIndex,
                      i4_dhcpSrvHostOptType)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvHostType;
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_dhcpSrvHostId->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_dhcpSrvHostId->i4_Length; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_dhcpSrvHostId->pu1_OctetList[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvSubnetPoolIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvHostOptType;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_dhcpSrvHostId =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (65);
                if ((poctet_dhcpSrvHostId == NULL))
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvHostOptTable (&i4_dhcpSrvHostType,
                                                          poctet_dhcpSrvHostId,
                                                          &i4_dhcpSrvSubnetPoolIndex,
                                                          &i4_dhcpSrvHostOptType))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvHostType;
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_dhcpSrvHostId->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count < poctet_dhcpSrvHostId->i4_Length; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_dhcpSrvHostId->
                            pu1_OctetList[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvHostOptType;
                }
                else
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvHostType =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_dhcpSrvHostId =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len += i4_len_Octet_index_dhcpSrvHostId;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_dhcpSrvHostId =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
                    poctet_next_dhcpSrvHostId =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (65);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_dhcpSrvHostId == NULL)
                        || (poctet_next_dhcpSrvHostId == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_dhcpSrvHostId);
                        free_octetstring (poctet_next_dhcpSrvHostId);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count < i4_len_Octet_index_dhcpSrvHostId;
                         i4_count++, i4_offset++)
                        poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_dhcpSrvHostId = allocmem_octetstring (65);
                    poctet_next_dhcpSrvHostId = allocmem_octetstring (65);
                    if ((poctet_dhcpSrvHostId == NULL)
                        || (poctet_next_dhcpSrvHostId == NULL))
                    {
                        free_octetstring (poctet_dhcpSrvHostId);
                        free_octetstring (poctet_next_dhcpSrvHostId);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvSubnetPoolIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvHostOptType =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvHostOptTable (i4_dhcpSrvHostType,
                                                         &i4_next_dhcpSrvHostType,
                                                         poctet_dhcpSrvHostId,
                                                         poctet_next_dhcpSrvHostId,
                                                         i4_dhcpSrvSubnetPoolIndex,
                                                         &i4_next_dhcpSrvSubnetPoolIndex,
                                                         i4_dhcpSrvHostOptType,
                                                         &i4_next_dhcpSrvHostOptType))
                    == SNMP_SUCCESS)
                {
                    i4_dhcpSrvHostType = i4_next_dhcpSrvHostType;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvHostType;
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_dhcpSrvHostId->i4_Length;
                    for (i4_count = FALSE;
                         i4_count < poctet_next_dhcpSrvHostId->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_dhcpSrvHostId->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_dhcpSrvHostId);
                    poctet_dhcpSrvHostId = poctet_next_dhcpSrvHostId;
                    i4_dhcpSrvSubnetPoolIndex = i4_next_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvSubnetPoolIndex;
                    i4_dhcpSrvHostOptType = i4_next_dhcpSrvHostOptType;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvHostOptType;
                }
                else
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    free_octetstring (poctet_next_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVHOSTTYPE:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dhcpSrvHostType;
            }
            else
            {
                i4_return_val = i4_next_dhcpSrvHostType;
            }
            free_octetstring (poctet_dhcpSrvHostId);
            break;
        }
        case DHCPSRVHOSTID:
        {
            i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                poctet_string = poctet_dhcpSrvHostId;
            }
            else
            {
                poctet_string = poctet_next_dhcpSrvHostId;
            }
            break;
        }
        case DHCPSRVHOSTOPTTYPE:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dhcpSrvHostOptType;
            }
            else
            {
                i4_return_val = i4_next_dhcpSrvHostOptType;
            }
            free_octetstring (poctet_dhcpSrvHostId);
            break;
        }
        case DHCPSRVHOSTOPTLEN:
        {
            i1_ret_val =
                nmhGetDhcpSrvHostOptLen (i4_dhcpSrvHostType,
                                         poctet_dhcpSrvHostId,
                                         i4_dhcpSrvSubnetPoolIndex,
                                         i4_dhcpSrvHostOptType, &i4_return_val);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVHOSTOPTVAL:
        {
            poctet_retval_dhcpSrvHostOptVal =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvHostOptVal == NULL)
            {
                free_octetstring (poctet_dhcpSrvHostId);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvHostOptVal (i4_dhcpSrvHostType,
                                         poctet_dhcpSrvHostId,
                                         i4_dhcpSrvSubnetPoolIndex,
                                         i4_dhcpSrvHostOptType,
                                         poctet_retval_dhcpSrvHostOptVal);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvHostOptVal;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvHostOptVal);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVHOSTOPTROWSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvHostOptRowStatus (i4_dhcpSrvHostType,
                                               poctet_dhcpSrvHostId,
                                               i4_dhcpSrvSubnetPoolIndex,
                                               i4_dhcpSrvHostOptType,
                                               &i4_return_val);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_dhcpSrvHostId);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvHostOptEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvHostOptEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvHostType = FALSE;

    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_dhcpSrvHostId = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_dhcpSrvHostId = NULL;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    INT4                i4_dhcpSrvHostOptType = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_len_Octet_index_dhcpSrvHostId =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_dhcpSrvHostId;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_dhcpSrvHostId =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
        if ((poctet_dhcpSrvHostId == NULL))
        {
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_GEN_ERR);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvHostType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_dhcpSrvHostId;
             i4_count++, i4_offset++)
            poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_dhcpSrvHostOptType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVHOSTOPTLEN:
        {
            i1_ret_val =
                nmhSetDhcpSrvHostOptLen (i4_dhcpSrvHostType,
                                         poctet_dhcpSrvHostId,
                                         i4_dhcpSrvSubnetPoolIndex,
                                         i4_dhcpSrvHostOptType,
                                         p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVHOSTOPTVAL:
        {
            i1_ret_val =
                nmhSetDhcpSrvHostOptVal (i4_dhcpSrvHostType,
                                         poctet_dhcpSrvHostId,
                                         i4_dhcpSrvSubnetPoolIndex,
                                         i4_dhcpSrvHostOptType,
                                         p_value->pOctetStrValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVHOSTOPTROWSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvHostOptRowStatus (i4_dhcpSrvHostType,
                                               poctet_dhcpSrvHostId,
                                               i4_dhcpSrvSubnetPoolIndex,
                                               i4_dhcpSrvHostOptType,
                                               p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPSRVHOSTTYPE:
            /*  Read Only Variables. */
        case DHCPSRVHOSTID:
            /*  Read Only Variables. */
        case DHCPSRVHOSTOPTTYPE:
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvHostOptEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvHostOptEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvHostType = FALSE;

    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_dhcpSrvHostId = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_dhcpSrvHostId = NULL;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    INT4                i4_dhcpSrvHostOptType = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_len_Octet_index_dhcpSrvHostId =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_dhcpSrvHostId;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_dhcpSrvHostId =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
        if ((poctet_dhcpSrvHostId == NULL))
        {
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_GEN_ERR);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvHostType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_dhcpSrvHostId;
             i4_count++, i4_offset++)
            poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_dhcpSrvHostOptType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvHostOptTable(i4_dhcpSrvHostType , poctet_dhcpSrvHostId , i4_dhcpSrvSubnetPoolIndex , i4_dhcpSrvHostOptType)) != SNMP_SUCCESS) {
       free_octetstring(poctet_dhcpSrvHostId);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVHOSTOPTLEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvHostOptLen (&u4ErrorCode, i4_dhcpSrvHostType,
                                            poctet_dhcpSrvHostId,
                                            i4_dhcpSrvSubnetPoolIndex,
                                            i4_dhcpSrvHostOptType,
                                            p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVHOSTOPTVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvHostOptVal (&u4ErrorCode, i4_dhcpSrvHostType,
                                            poctet_dhcpSrvHostId,
                                            i4_dhcpSrvSubnetPoolIndex,
                                            i4_dhcpSrvHostOptType,
                                            p_value->pOctetStrValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVHOSTOPTROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvHostOptRowStatus (&u4ErrorCode,
                                                  i4_dhcpSrvHostType,
                                                  poctet_dhcpSrvHostId,
                                                  i4_dhcpSrvSubnetPoolIndex,
                                                  i4_dhcpSrvHostOptType,
                                                  p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPSRVHOSTTYPE:
        case DHCPSRVHOSTID:
        case DHCPSRVHOSTOPTTYPE:
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvHostConfigEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvHostConfigEntryGet (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvHostConfigTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvHostType = FALSE;
    INT4                i4_next_dhcpSrvHostType = FALSE;

    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_dhcpSrvHostId = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_dhcpSrvHostId = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_dhcpSrvHostId = NULL;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;
    INT4                i4_next_dhcpSrvSubnetPoolIndex = FALSE;

    UINT4               u4_addr_ret_val_dhcpSrvHostIpAddress;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvHostBootFileName = NULL;
    UINT4               u4_addr_ret_val_dhcpSrvHostBootServerAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_len_Octet_index_dhcpSrvHostId =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_dhcpSrvHostId;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvHostConfigTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN +
                i4_len_Octet_index_dhcpSrvHostId + LEN_OF_VARIABLE_LEN_INDEX +
                INTEGER_LEN;

            if (LEN_dhcpSrvHostConfigTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_dhcpSrvHostId =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
                if ((poctet_dhcpSrvHostId == NULL))
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /* Extracting The Integer Index. */
                i4_dhcpSrvHostType =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_dhcpSrvHostId;
                     i4_count++, i4_offset++)
                    poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /* Extracting The Integer Index. */
                i4_dhcpSrvSubnetPoolIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvHostConfigTable
                     (i4_dhcpSrvHostType, poctet_dhcpSrvHostId,
                      i4_dhcpSrvSubnetPoolIndex)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvHostType;
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_dhcpSrvHostId->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_dhcpSrvHostId->i4_Length; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_dhcpSrvHostId->pu1_OctetList[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dhcpSrvSubnetPoolIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_dhcpSrvHostId =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (65);
                if ((poctet_dhcpSrvHostId == NULL))
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvHostConfigTable
                     (&i4_dhcpSrvHostType, poctet_dhcpSrvHostId,
                      &i4_dhcpSrvSubnetPoolIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvHostType;
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_dhcpSrvHostId->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count < poctet_dhcpSrvHostId->i4_Length; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_dhcpSrvHostId->
                            pu1_OctetList[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dhcpSrvSubnetPoolIndex;
                }
                else
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvHostType =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_dhcpSrvHostId =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len += i4_len_Octet_index_dhcpSrvHostId;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_dhcpSrvHostId =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
                    poctet_next_dhcpSrvHostId =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (65);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_dhcpSrvHostId == NULL)
                        || (poctet_next_dhcpSrvHostId == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_dhcpSrvHostId);
                        free_octetstring (poctet_next_dhcpSrvHostId);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count < i4_len_Octet_index_dhcpSrvHostId;
                         i4_count++, i4_offset++)
                        poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_dhcpSrvHostId = allocmem_octetstring (65);
                    poctet_next_dhcpSrvHostId = allocmem_octetstring (65);
                    if ((poctet_dhcpSrvHostId == NULL)
                        || (poctet_next_dhcpSrvHostId == NULL))
                    {
                        free_octetstring (poctet_dhcpSrvHostId);
                        free_octetstring (poctet_next_dhcpSrvHostId);
                        free_octetstring (poctet_dhcpSrvHostId);
                        free_octetstring (poctet_next_dhcpSrvHostId);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dhcpSrvSubnetPoolIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvHostConfigTable (i4_dhcpSrvHostType,
                                                            &i4_next_dhcpSrvHostType,
                                                            poctet_dhcpSrvHostId,
                                                            poctet_next_dhcpSrvHostId,
                                                            i4_dhcpSrvSubnetPoolIndex,
                                                            &i4_next_dhcpSrvSubnetPoolIndex))
                    == SNMP_SUCCESS)
                {
                    i4_dhcpSrvHostType = i4_next_dhcpSrvHostType;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvHostType;
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_dhcpSrvHostId->i4_Length;
                    for (i4_count = FALSE;
                         i4_count < poctet_next_dhcpSrvHostId->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_dhcpSrvHostId->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_dhcpSrvHostId);
                    poctet_dhcpSrvHostId = poctet_next_dhcpSrvHostId;
                    i4_dhcpSrvSubnetPoolIndex = i4_next_dhcpSrvSubnetPoolIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dhcpSrvSubnetPoolIndex;
                }
                else
                {
                    free_octetstring (poctet_dhcpSrvHostId);
                    free_octetstring (poctet_next_dhcpSrvHostId);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVHOSTIPADDRESS:
        {
            i1_ret_val =
                nmhGetDhcpSrvHostIpAddress (i4_dhcpSrvHostType,
                                            poctet_dhcpSrvHostId,
                                            i4_dhcpSrvSubnetPoolIndex,
                                            &u4_addr_ret_val_dhcpSrvHostIpAddress);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvHostIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVHOSTPOOLNAME:
        {
            i1_ret_val =
                nmhGetDhcpSrvHostPoolName (i4_dhcpSrvHostType,
                                           poctet_dhcpSrvHostId,
                                           i4_dhcpSrvSubnetPoolIndex,
                                           &i4_return_val);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVHOSTBOOTFILENAME:
        {
            poctet_retval_dhcpSrvHostBootFileName =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvHostBootFileName == NULL)
            {
                free_octetstring (poctet_dhcpSrvHostId);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvHostBootFileName (i4_dhcpSrvHostType,
                                               poctet_dhcpSrvHostId,
                                               i4_dhcpSrvSubnetPoolIndex,
                                               poctet_retval_dhcpSrvHostBootFileName);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvHostBootFileName;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvHostBootFileName);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVHOSTBOOTSERVERADDRESS:
        {
            i1_ret_val =
                nmhGetDhcpSrvHostBootServerAddress (i4_dhcpSrvHostType,
                                                    poctet_dhcpSrvHostId,
                                                    i4_dhcpSrvSubnetPoolIndex,
                                                    &u4_addr_ret_val_dhcpSrvHostBootServerAddress);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_dhcpSrvHostBootServerAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVHOSTCONFIGROWSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvHostConfigRowStatus (i4_dhcpSrvHostType,
                                                  poctet_dhcpSrvHostId,
                                                  i4_dhcpSrvSubnetPoolIndex,
                                                  &i4_return_val);
            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_dhcpSrvHostId);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvHostConfigEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvHostConfigEntrySet (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvHostType = FALSE;

    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_dhcpSrvHostId = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_dhcpSrvHostId = NULL;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvHostIpAddress;
    UINT4               u4_addr_val_dhcpSrvHostBootServerAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_len_Octet_index_dhcpSrvHostId =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_dhcpSrvHostId;
        i4_size_offset += INTEGER_LEN;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_dhcpSrvHostId =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
        if ((poctet_dhcpSrvHostId == NULL))
        {
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_GEN_ERR);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvHostType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_dhcpSrvHostId;
             i4_count++, i4_offset++)
            poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVHOSTIPADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvHostIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvHostIpAddress (i4_dhcpSrvHostType,
                                            poctet_dhcpSrvHostId,
                                            i4_dhcpSrvSubnetPoolIndex,
                                            u4_addr_val_dhcpSrvHostIpAddress);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVHOSTPOOLNAME:
        {
            i1_ret_val =
                nmhSetDhcpSrvHostPoolName (i4_dhcpSrvHostType,
                                           poctet_dhcpSrvHostId,
                                           i4_dhcpSrvSubnetPoolIndex,
                                           p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVHOSTBOOTFILENAME:
        {
            i1_ret_val =
                nmhSetDhcpSrvHostBootFileName (i4_dhcpSrvHostType,
                                               poctet_dhcpSrvHostId,
                                               i4_dhcpSrvSubnetPoolIndex,
                                               p_value->pOctetStrValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVHOSTBOOTSERVERADDRESS:
        {
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvHostBootServerAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));
            i1_ret_val =
                nmhSetDhcpSrvHostBootServerAddress (i4_dhcpSrvHostType,
                                                    poctet_dhcpSrvHostId,
                                                    i4_dhcpSrvSubnetPoolIndex,
                                                    u4_addr_val_dhcpSrvHostBootServerAddress);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DHCPSRVHOSTCONFIGROWSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvHostConfigRowStatus (i4_dhcpSrvHostType,
                                                  poctet_dhcpSrvHostId,
                                                  i4_dhcpSrvSubnetPoolIndex,
                                                  p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            free_octetstring (poctet_dhcpSrvHostId);
        default:
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvHostConfigEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvHostConfigEntryTest (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;

    INT4                i4_count = FALSE;
    INT4                i4_dhcpSrvHostType = FALSE;

    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_dhcpSrvHostId = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_dhcpSrvHostId = NULL;

    INT4                i4_dhcpSrvSubnetPoolIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
    UINT4               u4_addr_val_dhcpSrvHostIpAddress;
    UINT4               u4_addr_val_dhcpSrvHostBootServerAddress;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_len_Octet_index_dhcpSrvHostId =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_dhcpSrvHostId;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_dhcpSrvHostId =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_dhcpSrvHostId);
        if ((poctet_dhcpSrvHostId == NULL))
        {
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_GEN_ERR);
        }
        /* Extracting The Integer Index. */
        i4_dhcpSrvHostType =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_dhcpSrvHostId;
             i4_count++, i4_offset++)
            poctet_dhcpSrvHostId->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_dhcpSrvSubnetPoolIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvHostConfigTable(i4_dhcpSrvHostType , poctet_dhcpSrvHostId , i4_dhcpSrvSubnetPoolIndex)) != SNMP_SUCCESS) {
       free_octetstring(poctet_dhcpSrvHostId);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVHOSTIPADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvHostIpAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvHostIpAddress (&u4ErrorCode, i4_dhcpSrvHostType,
                                               poctet_dhcpSrvHostId,
                                               i4_dhcpSrvSubnetPoolIndex,
                                               u4_addr_val_dhcpSrvHostIpAddress);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVHOSTPOOLNAME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvHostPoolName (&u4ErrorCode, i4_dhcpSrvHostType,
                                              poctet_dhcpSrvHostId,
                                              i4_dhcpSrvSubnetPoolIndex,
                                              p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVHOSTBOOTFILENAME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvHostBootFileName (&u4ErrorCode,
                                                  i4_dhcpSrvHostType,
                                                  poctet_dhcpSrvHostId,
                                                  i4_dhcpSrvSubnetPoolIndex,
                                                  p_value->pOctetStrValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVHOSTBOOTSERVERADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_IP_ADDR_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }
            for (i4_offset = FALSE; i4_offset < ADDR_LEN; i4_offset++)
                u1_octet_string[i4_offset] =
                    p_value->pOctetStrValue->pu1_OctetList[i4_offset];
            u4_addr_val_dhcpSrvHostBootServerAddress =
                OSIX_NTOHL (*((UINT4 *) (u1_octet_string)));

            i1_ret_val =
                nmhTestv2DhcpSrvHostBootServerAddress (&u4ErrorCode,
                                                       i4_dhcpSrvHostType,
                                                       poctet_dhcpSrvHostId,
                                                       i4_dhcpSrvSubnetPoolIndex,
                                                       u4_addr_val_dhcpSrvHostBootServerAddress);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DHCPSRVHOSTCONFIGROWSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode,
                                                     i4_dhcpSrvHostType,
                                                     poctet_dhcpSrvHostId,
                                                     i4_dhcpSrvSubnetPoolIndex,
                                                     p_value->i4_SLongValue);

            free_octetstring (poctet_dhcpSrvHostId);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_dhcpSrvHostId);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvBindingEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvBindingEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dhcpSrvBindingTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dhcpSrvBindIpAddress = FALSE;
    UINT4               u4_addr_next_dhcpSrvBindIpAddress = FALSE;
    UINT1               u1_addr_dhcpSrvBindIpAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_dhcpSrvBindIpAddress[ADDR_LEN] =
        NULL_STRING;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_dhcpSrvBindHwAddress = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dhcpSrvBindingTable_INDEX = p_in_db->u4_Length + ADDR_LEN;

            if (LEN_dhcpSrvBindingTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dhcpSrvBindIpAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dhcpSrvBindIpAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_dhcpSrvBindIpAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDhcpSrvBindingTable
                     (u4_addr_dhcpSrvBindIpAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dhcpSrvBindIpAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDhcpSrvBindingTable
                     (&u4_addr_dhcpSrvBindIpAddress)) == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_dhcpSrvBindIpAddress)) =
                        OSIX_HTONL (u4_addr_dhcpSrvBindIpAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dhcpSrvBindIpAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dhcpSrvBindIpAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dhcpSrvBindIpAddress =
                        OSIX_NTOHL (*
                                    ((UINT4 *) (u1_addr_dhcpSrvBindIpAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDhcpSrvBindingTable
                     (u4_addr_dhcpSrvBindIpAddress,
                      &u4_addr_next_dhcpSrvBindIpAddress)) == SNMP_SUCCESS)
                {
                    u4_addr_dhcpSrvBindIpAddress =
                        u4_addr_next_dhcpSrvBindIpAddress;
                    *((UINT4 *) (u1_addr_next_dhcpSrvBindIpAddress)) =
                        OSIX_HTONL (u4_addr_dhcpSrvBindIpAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_dhcpSrvBindIpAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DHCPSRVBINDIPADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_dhcpSrvBindIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_dhcpSrvBindIpAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DHCPSRVBINDHWTYPE:
        {
            i1_ret_val =
                nmhGetDhcpSrvBindHwType (u4_addr_dhcpSrvBindIpAddress,
                                         &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBINDHWADDRESS:
        {
            poctet_retval_dhcpSrvBindHwAddress =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_dhcpSrvBindHwAddress == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDhcpSrvBindHwAddress (u4_addr_dhcpSrvBindIpAddress,
                                            poctet_retval_dhcpSrvBindHwAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dhcpSrvBindHwAddress;
            }
            else
            {
                free_octetstring (poctet_retval_dhcpSrvBindHwAddress);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBINDEXPIRETIME:
        {
            i1_ret_val =
                nmhGetDhcpSrvBindExpireTime (u4_addr_dhcpSrvBindIpAddress,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBINDALLOCMETHOD:
        {
            i1_ret_val =
                nmhGetDhcpSrvBindAllocMethod (u4_addr_dhcpSrvBindIpAddress,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBINDSTATE:
        {
            i1_ret_val =
                nmhGetDhcpSrvBindState (u4_addr_dhcpSrvBindIpAddress,
                                        &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPSRVBINDENTRYSTATUS:
        {
            i1_ret_val =
                nmhGetDhcpSrvBindEntryStatus (u4_addr_dhcpSrvBindIpAddress,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dhcpSrvBindingEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
dhcpSrvBindingEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dhcpSrvBindIpAddress = FALSE;
    UINT1               u1_addr_dhcpSrvBindIpAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_dhcpSrvBindIpAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_dhcpSrvBindIpAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_dhcpSrvBindIpAddress)));

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DHCPSRVBINDENTRYSTATUS:
        {
            i1_ret_val =
                nmhSetDhcpSrvBindEntryStatus (u4_addr_dhcpSrvBindIpAddress,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DHCPSRVBINDIPADDRESS:
            /*  Read Only Variables. */
        case DHCPSRVBINDHWTYPE:
            /*  Read Only Variables. */
        case DHCPSRVBINDHWADDRESS:
            /*  Read Only Variables. */
        case DHCPSRVBINDEXPIRETIME:
            /*  Read Only Variables. */
        case DHCPSRVBINDALLOCMETHOD:
            /*  Read Only Variables. */
        case DHCPSRVBINDSTATE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dhcpSrvBindingEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
dhcpSrvBindingEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dhcpSrvBindIpAddress = FALSE;
    UINT1               u1_addr_dhcpSrvBindIpAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_dhcpSrvBindIpAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_dhcpSrvBindIpAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_dhcpSrvBindIpAddress)));

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceDhcpSrvBindingTable(u4_addr_dhcpSrvBindIpAddress)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case DHCPSRVBINDENTRYSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DhcpSrvBindEntryStatus (&u4ErrorCode,
                                                 u4_addr_dhcpSrvBindIpAddress,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DHCPSRVBINDIPADDRESS:
        case DHCPSRVBINDHWTYPE:
        case DHCPSRVBINDHWADDRESS:
        case DHCPSRVBINDEXPIRETIME:
        case DHCPSRVBINDALLOCMETHOD:
        case DHCPSRVBINDSTATE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : dhcpSrvCountersGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
dhcpSrvCountersGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                    UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_dhcpSrvCounters_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_dhcpSrvCounters_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_dhcpSrvCounters_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_dhcpSrvCounters_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_dhcpSrvCounters_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case DHCPCOUNTDISCOVERS:
        {
            i1_ret_val = nmhGetDhcpCountDiscovers (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTREQUESTS:
        {
            i1_ret_val = nmhGetDhcpCountRequests (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTRELEASES:
        {
            i1_ret_val = nmhGetDhcpCountReleases (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTDECLINES:
        {
            i1_ret_val = nmhGetDhcpCountDeclines (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTINFORMS:
        {
            i1_ret_val = nmhGetDhcpCountInforms (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTINVALIDS:
        {
            i1_ret_val = nmhGetDhcpCountInvalids (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTOFFERS:
        {
            i1_ret_val = nmhGetDhcpCountOffers (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTACKS:
        {
            i1_ret_val = nmhGetDhcpCountAcks (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTNACKS:
        {
            i1_ret_val = nmhGetDhcpCountNacks (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTDROPPEDUNKNOWNCLIENT:
        {
            i1_ret_val = nmhGetDhcpCountDroppedUnknownClient (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DHCPCOUNTDROPPEDNOTSERVINGSUBNET:
        {
            i1_ret_val =
                nmhGetDhcpCountDroppedNotServingSubnet (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */
