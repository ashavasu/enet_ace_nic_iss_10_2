
# ifndef fsdhcOCON_H
# define fsdhcOCON_H
/*
 *  The Constant Declarations for
 *  dhcpSrvConfig
 */

# define DHCPSRVENABLE                                     (1)
# define DHCPSRVDEBUGLEVEL                                 (2)
# define DHCPSRVOFFERREUSETIMEOUT                          (3)
# define DHCPSRVICMPECHOENABLE                             (4)
# define DHCPSRVBOOTSERVERADDRESS                          (5)
# define DHCPSRVDEFBOOTFILENAME                            (6)
# define DHCPSRVBOOTPCLIENTSSUPPORTED                      (7)
# define DHCPSRVAUTOMATICBOOTPENABLED                      (8)

/*
 *  The Constant Declarations for
 *  dhcpSrvSubnetPoolConfigTable
 */

# define DHCPSRVSUBNETPOOLINDEX                            (1)
# define DHCPSRVSUBNETSUBNET                               (2)
# define DHCPSRVSUBNETPORTNUMBER                           (3)
# define DHCPSRVSUBNETMASK                                 (4)
# define DHCPSRVSUBNETSTARTIPADDRESS                       (5)
# define DHCPSRVSUBNETENDIPADDRESS                         (6)
# define DHCPSRVSUBNETLEASETIME                            (7)
# define DHCPSRVSUBNETPOOLNAME                             (8)
# define DHCPSRVSUBNETPOOLROWSTATUS                        (9)

/*
 *  The Constant Declarations for
 *  dhcpSrvExcludeIpAddressTable
 */

# define DHCPSRVEXCLUDESTARTIPADDRESS                      (1)
# define DHCPSRVEXCLUDEENDIPADDRESS                        (2)
# define DHCPSRVEXCLUDEADDRESSROWSTATUS                    (3)

/*
 *  The Constant Declarations for
 *  dhcpSrvGblOptTable
 */

# define DHCPSRVGBLOPTTYPE                                 (1)
# define DHCPSRVGBLOPTLEN                                  (2)
# define DHCPSRVGBLOPTVAL                                  (3)
# define DHCPSRVGBLOPTROWSTATUS                            (4)

/*
 *  The Constant Declarations for
 *  dhcpSrvSubnetOptTable
 */

# define DHCPSRVSUBNETOPTTYPE                              (1)
# define DHCPSRVSUBNETOPTLEN                               (2)
# define DHCPSRVSUBNETOPTVAL                               (3)
# define DHCPSRVSUBNETOPTROWSTATUS                         (4)

/*
 *  The Constant Declarations for
 *  dhcpSrvHostOptTable
 */

# define DHCPSRVHOSTTYPE                                   (1)
# define DHCPSRVHOSTID                                     (2)
# define DHCPSRVHOSTOPTTYPE                                (3)
# define DHCPSRVHOSTOPTLEN                                 (4)
# define DHCPSRVHOSTOPTVAL                                 (5)
# define DHCPSRVHOSTOPTROWSTATUS                           (6)

/*
 *  The Constant Declarations for
 *  dhcpSrvHostConfigTable
 */

# define DHCPSRVHOSTIPADDRESS                              (1)
# define DHCPSRVHOSTPOOLNAME                               (2)
# define DHCPSRVHOSTBOOTFILENAME                           (3)
# define DHCPSRVHOSTBOOTSERVERADDRESS                      (4)
# define DHCPSRVHOSTCONFIGROWSTATUS                        (5)

/*
 *  The Constant Declarations for
 *  dhcpSrvBindingTable
 */

# define DHCPSRVBINDIPADDRESS                              (1)
# define DHCPSRVBINDHWTYPE                                 (2)
# define DHCPSRVBINDHWADDRESS                              (3)
# define DHCPSRVBINDEXPIRETIME                             (4)
# define DHCPSRVBINDALLOCMETHOD                            (5)
# define DHCPSRVBINDSTATE                                  (6)
# define DHCPSRVBINDENTRYSTATUS                            (7)

/*
 *  The Constant Declarations for
 *  dhcpSrvCounters
 */

# define DHCPCOUNTDISCOVERS                                (1)
# define DHCPCOUNTREQUESTS                                 (2)
# define DHCPCOUNTRELEASES                                 (3)
# define DHCPCOUNTDECLINES                                 (4)
# define DHCPCOUNTINFORMS                                  (5)
# define DHCPCOUNTINVALIDS                                 (6)
# define DHCPCOUNTOFFERS                                   (7)
# define DHCPCOUNTACKS                                     (8)
# define DHCPCOUNTNACKS                                    (9)
# define DHCPCOUNTDROPPEDUNKNOWNCLIENT                     (10)
# define DHCPCOUNTDROPPEDNOTSERVINGSUBNET                  (11)

#endif /*  fsdhcpsOCON_H  */
