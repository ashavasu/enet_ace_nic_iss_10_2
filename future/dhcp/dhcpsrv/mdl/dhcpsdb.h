/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dhcpsdb.h,v 1.7 2014/03/18 11:57:08 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _DHCPSDB_H
#define _DHCPSDB_H

UINT1 DhcpSrvSubnetPoolConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 DhcpSrvExcludeIpAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DhcpSrvGblOptTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 DhcpSrvSubnetOptTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 DhcpSrvHostOptTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 DhcpSrvHostConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 DhcpSrvBindingTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 dhcps [] ={1,3,6,1,4,1,2076,84};
tSNMP_OID_TYPE dhcpsOID = {8, dhcps};


UINT4 DhcpSrvEnable [ ] ={1,3,6,1,4,1,2076,84,1,1};
UINT4 DhcpSrvDebugLevel [ ] ={1,3,6,1,4,1,2076,84,1,2};
UINT4 DhcpSrvOfferReuseTimeOut [ ] ={1,3,6,1,4,1,2076,84,1,3};
UINT4 DhcpSrvIcmpEchoEnable [ ] ={1,3,6,1,4,1,2076,84,1,4};
UINT4 DhcpSrvBootServerAddress [ ] ={1,3,6,1,4,1,2076,84,1,5};
UINT4 DhcpSrvDefBootFilename [ ] ={1,3,6,1,4,1,2076,84,1,6};
UINT4 DhcpSrvBootpClientsSupported [ ] ={1,3,6,1,4,1,2076,84,1,7};
UINT4 DhcpSrvAutomaticBootpEnabled [ ] ={1,3,6,1,4,1,2076,84,1,8};
UINT4 DhcpSrvSubnetPoolIndex [ ] ={1,3,6,1,4,1,2076,84,1,9,1,1};
UINT4 DhcpSrvSubnetSubnet [ ] ={1,3,6,1,4,1,2076,84,1,9,1,2};
UINT4 DhcpSrvSubnetPortNumber [ ] ={1,3,6,1,4,1,2076,84,1,9,1,3};
UINT4 DhcpSrvSubnetMask [ ] ={1,3,6,1,4,1,2076,84,1,9,1,4};
UINT4 DhcpSrvSubnetStartIpAddress [ ] ={1,3,6,1,4,1,2076,84,1,9,1,5};
UINT4 DhcpSrvSubnetEndIpAddress [ ] ={1,3,6,1,4,1,2076,84,1,9,1,6};
UINT4 DhcpSrvSubnetLeaseTime [ ] ={1,3,6,1,4,1,2076,84,1,9,1,7};
UINT4 DhcpSrvSubnetPoolName [ ] ={1,3,6,1,4,1,2076,84,1,9,1,8};
UINT4 DhcpSrvSubnetUtlThreshold [ ] ={1,3,6,1,4,1,2076,84,1,9,1,9};
UINT4 DhcpSrvSubnetPoolRowStatus [ ] ={1,3,6,1,4,1,2076,84,1,9,1,10};
UINT4 DhcpSrvExcludeStartIpAddress [ ] ={1,3,6,1,4,1,2076,84,1,10,1,1};
UINT4 DhcpSrvExcludeEndIpAddress [ ] ={1,3,6,1,4,1,2076,84,1,10,1,2};
UINT4 DhcpSrvExcludeAddressRowStatus [ ] ={1,3,6,1,4,1,2076,84,1,10,1,3};
UINT4 DhcpSrvGblOptType [ ] ={1,3,6,1,4,1,2076,84,1,11,1,1};
UINT4 DhcpSrvGblOptLen [ ] ={1,3,6,1,4,1,2076,84,1,11,1,2};
UINT4 DhcpSrvGblOptVal [ ] ={1,3,6,1,4,1,2076,84,1,11,1,3};
UINT4 DhcpSrvGblOptRowStatus [ ] ={1,3,6,1,4,1,2076,84,1,11,1,4};
UINT4 DhcpSrvSubnetOptType [ ] ={1,3,6,1,4,1,2076,84,1,12,1,1};
UINT4 DhcpSrvSubnetOptLen [ ] ={1,3,6,1,4,1,2076,84,1,12,1,2};
UINT4 DhcpSrvSubnetOptVal [ ] ={1,3,6,1,4,1,2076,84,1,12,1,3};
UINT4 DhcpSrvSubnetOptRowStatus [ ] ={1,3,6,1,4,1,2076,84,1,12,1,4};
UINT4 DhcpSrvHostType [ ] ={1,3,6,1,4,1,2076,84,1,13,1,1};
UINT4 DhcpSrvHostId [ ] ={1,3,6,1,4,1,2076,84,1,13,1,2};
UINT4 DhcpSrvHostOptType [ ] ={1,3,6,1,4,1,2076,84,1,13,1,3};
UINT4 DhcpSrvHostOptLen [ ] ={1,3,6,1,4,1,2076,84,1,13,1,4};
UINT4 DhcpSrvHostOptVal [ ] ={1,3,6,1,4,1,2076,84,1,13,1,5};
UINT4 DhcpSrvHostOptRowStatus [ ] ={1,3,6,1,4,1,2076,84,1,13,1,6};
UINT4 DhcpSrvHostIpAddress [ ] ={1,3,6,1,4,1,2076,84,1,14,1,1};
UINT4 DhcpSrvHostPoolName [ ] ={1,3,6,1,4,1,2076,84,1,14,1,2};
UINT4 DhcpSrvHostBootFileName [ ] ={1,3,6,1,4,1,2076,84,1,14,1,3};
UINT4 DhcpSrvHostBootServerAddress [ ] ={1,3,6,1,4,1,2076,84,1,14,1,4};
UINT4 DhcpSrvHostConfigRowStatus [ ] ={1,3,6,1,4,1,2076,84,1,14,1,5};
UINT4 DhcpSrvBindIpAddress [ ] ={1,3,6,1,4,1,2076,84,2,1,1,1};
UINT4 DhcpSrvBindHwType [ ] ={1,3,6,1,4,1,2076,84,2,1,1,2};
UINT4 DhcpSrvBindHwAddress [ ] ={1,3,6,1,4,1,2076,84,2,1,1,3};
UINT4 DhcpSrvBindExpireTime [ ] ={1,3,6,1,4,1,2076,84,2,1,1,4};
UINT4 DhcpSrvBindAllocMethod [ ] ={1,3,6,1,4,1,2076,84,2,1,1,5};
UINT4 DhcpSrvBindState [ ] ={1,3,6,1,4,1,2076,84,2,1,1,6};
UINT4 DhcpSrvBindXid [ ] ={1,3,6,1,4,1,2076,84,2,1,1,7};
UINT4 DhcpSrvBindEntryStatus [ ] ={1,3,6,1,4,1,2076,84,2,1,1,8};
UINT4 DhcpCountDiscovers [ ] ={1,3,6,1,4,1,2076,84,3,1};
UINT4 DhcpCountRequests [ ] ={1,3,6,1,4,1,2076,84,3,2};
UINT4 DhcpCountReleases [ ] ={1,3,6,1,4,1,2076,84,3,3};
UINT4 DhcpCountDeclines [ ] ={1,3,6,1,4,1,2076,84,3,4};
UINT4 DhcpCountInforms [ ] ={1,3,6,1,4,1,2076,84,3,5};
UINT4 DhcpCountInvalids [ ] ={1,3,6,1,4,1,2076,84,3,6};
UINT4 DhcpCountOffers [ ] ={1,3,6,1,4,1,2076,84,3,7};
UINT4 DhcpCountAcks [ ] ={1,3,6,1,4,1,2076,84,3,8};
UINT4 DhcpCountNacks [ ] ={1,3,6,1,4,1,2076,84,3,9};
UINT4 DhcpCountDroppedUnknownClient [ ] ={1,3,6,1,4,1,2076,84,3,10};
UINT4 DhcpCountDroppedNotServingSubnet [ ] ={1,3,6,1,4,1,2076,84,3,11};
UINT4 DhcpCountResetCounters [ ] ={1,3,6,1,4,1,2076,84,3,12};


tMbDbEntry dhcpsMibEntry[]= {

{{10,DhcpSrvEnable}, NULL, DhcpSrvEnableGet, DhcpSrvEnableSet, DhcpSrvEnableTest, DhcpSrvEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DhcpSrvDebugLevel}, NULL, DhcpSrvDebugLevelGet, DhcpSrvDebugLevelSet, DhcpSrvDebugLevelTest, DhcpSrvDebugLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,DhcpSrvOfferReuseTimeOut}, NULL, DhcpSrvOfferReuseTimeOutGet, DhcpSrvOfferReuseTimeOutSet, DhcpSrvOfferReuseTimeOutTest, DhcpSrvOfferReuseTimeOutDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,DhcpSrvIcmpEchoEnable}, NULL, DhcpSrvIcmpEchoEnableGet, DhcpSrvIcmpEchoEnableSet, DhcpSrvIcmpEchoEnableTest, DhcpSrvIcmpEchoEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,DhcpSrvBootServerAddress}, NULL, DhcpSrvBootServerAddressGet, DhcpSrvBootServerAddressSet, DhcpSrvBootServerAddressTest, DhcpSrvBootServerAddressDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DhcpSrvDefBootFilename}, NULL, DhcpSrvDefBootFilenameGet, DhcpSrvDefBootFilenameSet, DhcpSrvDefBootFilenameTest, DhcpSrvDefBootFilenameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DhcpSrvBootpClientsSupported}, NULL, DhcpSrvBootpClientsSupportedGet, DhcpSrvBootpClientsSupportedSet, DhcpSrvBootpClientsSupportedTest, DhcpSrvBootpClientsSupportedDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,DhcpSrvAutomaticBootpEnabled}, NULL, DhcpSrvAutomaticBootpEnabledGet, DhcpSrvAutomaticBootpEnabledSet, DhcpSrvAutomaticBootpEnabledTest, DhcpSrvAutomaticBootpEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,DhcpSrvSubnetPoolIndex}, GetNextIndexDhcpSrvSubnetPoolConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetSubnet}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetSubnetGet, DhcpSrvSubnetSubnetSet, DhcpSrvSubnetSubnetTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetPortNumber}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetPortNumberGet, DhcpSrvSubnetPortNumberSet, DhcpSrvSubnetPortNumberTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetMask}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetMaskGet, DhcpSrvSubnetMaskSet, DhcpSrvSubnetMaskTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetStartIpAddress}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetStartIpAddressGet, DhcpSrvSubnetStartIpAddressSet, DhcpSrvSubnetStartIpAddressTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetEndIpAddress}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetEndIpAddressGet, DhcpSrvSubnetEndIpAddressSet, DhcpSrvSubnetEndIpAddressTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetLeaseTime}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetLeaseTimeGet, DhcpSrvSubnetLeaseTimeSet, DhcpSrvSubnetLeaseTimeTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetPoolName}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetPoolNameGet, DhcpSrvSubnetPoolNameSet, DhcpSrvSubnetPoolNameTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvSubnetUtlThreshold}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetUtlThresholdGet, DhcpSrvSubnetUtlThresholdSet, DhcpSrvSubnetUtlThresholdTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 0, "75"},

{{12,DhcpSrvSubnetPoolRowStatus}, GetNextIndexDhcpSrvSubnetPoolConfigTable, DhcpSrvSubnetPoolRowStatusGet, DhcpSrvSubnetPoolRowStatusSet, DhcpSrvSubnetPoolRowStatusTest, DhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvSubnetPoolConfigTableINDEX, 1, 0, 1, NULL},

{{12,DhcpSrvExcludeStartIpAddress}, GetNextIndexDhcpSrvExcludeIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DhcpSrvExcludeIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,DhcpSrvExcludeEndIpAddress}, GetNextIndexDhcpSrvExcludeIpAddressTable, DhcpSrvExcludeEndIpAddressGet, DhcpSrvExcludeEndIpAddressSet, DhcpSrvExcludeEndIpAddressTest, DhcpSrvExcludeIpAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvExcludeIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,DhcpSrvExcludeAddressRowStatus}, GetNextIndexDhcpSrvExcludeIpAddressTable, DhcpSrvExcludeAddressRowStatusGet, DhcpSrvExcludeAddressRowStatusSet, DhcpSrvExcludeAddressRowStatusTest, DhcpSrvExcludeIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvExcludeIpAddressTableINDEX, 2, 0, 1, NULL},

{{12,DhcpSrvGblOptType}, GetNextIndexDhcpSrvGblOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpSrvGblOptTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvGblOptLen}, GetNextIndexDhcpSrvGblOptTable, DhcpSrvGblOptLenGet, DhcpSrvGblOptLenSet, DhcpSrvGblOptLenTest, DhcpSrvGblOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvGblOptTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvGblOptVal}, GetNextIndexDhcpSrvGblOptTable, DhcpSrvGblOptValGet, DhcpSrvGblOptValSet, DhcpSrvGblOptValTest, DhcpSrvGblOptTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpSrvGblOptTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvGblOptRowStatus}, GetNextIndexDhcpSrvGblOptTable, DhcpSrvGblOptRowStatusGet, DhcpSrvGblOptRowStatusSet, DhcpSrvGblOptRowStatusTest, DhcpSrvGblOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvGblOptTableINDEX, 1, 0, 1, NULL},

{{12,DhcpSrvSubnetOptType}, GetNextIndexDhcpSrvSubnetOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpSrvSubnetOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpSrvSubnetOptLen}, GetNextIndexDhcpSrvSubnetOptTable, DhcpSrvSubnetOptLenGet, DhcpSrvSubnetOptLenSet, DhcpSrvSubnetOptLenTest, DhcpSrvSubnetOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvSubnetOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpSrvSubnetOptVal}, GetNextIndexDhcpSrvSubnetOptTable, DhcpSrvSubnetOptValGet, DhcpSrvSubnetOptValSet, DhcpSrvSubnetOptValTest, DhcpSrvSubnetOptTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpSrvSubnetOptTableINDEX, 2, 0, 0, NULL},

{{12,DhcpSrvSubnetOptRowStatus}, GetNextIndexDhcpSrvSubnetOptTable, DhcpSrvSubnetOptRowStatusGet, DhcpSrvSubnetOptRowStatusSet, DhcpSrvSubnetOptRowStatusTest, DhcpSrvSubnetOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvSubnetOptTableINDEX, 2, 0, 1, NULL},

{{12,DhcpSrvHostType}, GetNextIndexDhcpSrvHostOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpSrvHostOptTableINDEX, 4, 0, 0, NULL},

{{12,DhcpSrvHostId}, GetNextIndexDhcpSrvHostOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DhcpSrvHostOptTableINDEX, 4, 0, 0, NULL},

{{12,DhcpSrvHostOptType}, GetNextIndexDhcpSrvHostOptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DhcpSrvHostOptTableINDEX, 4, 0, 0, NULL},

{{12,DhcpSrvHostOptLen}, GetNextIndexDhcpSrvHostOptTable, DhcpSrvHostOptLenGet, DhcpSrvHostOptLenSet, DhcpSrvHostOptLenTest, DhcpSrvHostOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvHostOptTableINDEX, 4, 0, 0, NULL},

{{12,DhcpSrvHostOptVal}, GetNextIndexDhcpSrvHostOptTable, DhcpSrvHostOptValGet, DhcpSrvHostOptValSet, DhcpSrvHostOptValTest, DhcpSrvHostOptTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpSrvHostOptTableINDEX, 4, 0, 0, NULL},

{{12,DhcpSrvHostOptRowStatus}, GetNextIndexDhcpSrvHostOptTable, DhcpSrvHostOptRowStatusGet, DhcpSrvHostOptRowStatusSet, DhcpSrvHostOptRowStatusTest, DhcpSrvHostOptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvHostOptTableINDEX, 4, 0, 1, NULL},

{{12,DhcpSrvHostIpAddress}, GetNextIndexDhcpSrvHostConfigTable, DhcpSrvHostIpAddressGet, DhcpSrvHostIpAddressSet, DhcpSrvHostIpAddressTest, DhcpSrvHostConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvHostConfigTableINDEX, 3, 0, 0, NULL},

{{12,DhcpSrvHostPoolName}, GetNextIndexDhcpSrvHostConfigTable, DhcpSrvHostPoolNameGet, DhcpSrvHostPoolNameSet, DhcpSrvHostPoolNameTest, DhcpSrvHostConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvHostConfigTableINDEX, 3, 0, 0, NULL},

{{12,DhcpSrvHostBootFileName}, GetNextIndexDhcpSrvHostConfigTable, DhcpSrvHostBootFileNameGet, DhcpSrvHostBootFileNameSet, DhcpSrvHostBootFileNameTest, DhcpSrvHostConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DhcpSrvHostConfigTableINDEX, 3, 0, 0, NULL},

{{12,DhcpSrvHostBootServerAddress}, GetNextIndexDhcpSrvHostConfigTable, DhcpSrvHostBootServerAddressGet, DhcpSrvHostBootServerAddressSet, DhcpSrvHostBootServerAddressTest, DhcpSrvHostConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, DhcpSrvHostConfigTableINDEX, 3, 0, 0, NULL},

{{12,DhcpSrvHostConfigRowStatus}, GetNextIndexDhcpSrvHostConfigTable, DhcpSrvHostConfigRowStatusGet, DhcpSrvHostConfigRowStatusSet, DhcpSrvHostConfigRowStatusTest, DhcpSrvHostConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvHostConfigTableINDEX, 3, 0, 1, NULL},

{{12,DhcpSrvBindIpAddress}, GetNextIndexDhcpSrvBindingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindHwType}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindHwTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindHwAddress}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindHwAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindExpireTime}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindExpireTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindAllocMethod}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindAllocMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindState}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindXid}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindXidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, DhcpSrvBindingTableINDEX, 1, 0, 0, NULL},

{{12,DhcpSrvBindEntryStatus}, GetNextIndexDhcpSrvBindingTable, DhcpSrvBindEntryStatusGet, DhcpSrvBindEntryStatusSet, DhcpSrvBindEntryStatusTest, DhcpSrvBindingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DhcpSrvBindingTableINDEX, 1, 0, 1, NULL},

{{10,DhcpCountDiscovers}, NULL, DhcpCountDiscoversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountRequests}, NULL, DhcpCountRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountReleases}, NULL, DhcpCountReleasesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountDeclines}, NULL, DhcpCountDeclinesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountInforms}, NULL, DhcpCountInformsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountInvalids}, NULL, DhcpCountInvalidsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountOffers}, NULL, DhcpCountOffersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountAcks}, NULL, DhcpCountAcksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountNacks}, NULL, DhcpCountNacksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountDroppedUnknownClient}, NULL, DhcpCountDroppedUnknownClientGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountDroppedNotServingSubnet}, NULL, DhcpCountDroppedNotServingSubnetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DhcpCountResetCounters}, NULL, DhcpCountResetCountersGet, DhcpCountResetCountersSet, DhcpCountResetCountersTest, DhcpCountResetCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData dhcpsEntry = { 60, dhcpsMibEntry };
#endif /* _DHCPSDB_H */

