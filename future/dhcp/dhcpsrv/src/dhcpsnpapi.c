
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsnpapi.c,v 1.1 2013/03/28 11:56:12 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of DHCP-SRV module.
 ******************************************************************************/

#ifndef __DHCPS_NPAPI_C__
#define __DHCPS_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : DhcpsFsNpDhcpSrvInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpSrvInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpSrvInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DhcpsFsNpDhcpSrvInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_SRV_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DhcpsFsNpDhcpSrvDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpSrvDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpSrvDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DhcpsFsNpDhcpSrvDeInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_SRV_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* __DHCPS_NPAPI_C__ */
