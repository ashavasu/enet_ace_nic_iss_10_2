/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsrvsz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _DHCPSRVSZ_C
#include "dhcpscom.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DhcpsrvSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DHCPSRV_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsDHCPSRVSizingParams[i4SizingId].u4StructSize,
                              FsDHCPSRVSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(DHCPSRVMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DhcpsrvSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DhcpsrvSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDHCPSRVSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DHCPSRVMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DhcpsrvSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DHCPSRV_MAX_SIZING_ID; i4SizingId++)
    {
        if (DHCPSRVMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DHCPSRVMemPoolIds[i4SizingId]);
            DHCPSRVMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
