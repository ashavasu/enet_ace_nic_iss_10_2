/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpcli.c,v 1.86 2017/12/22 09:26:45 siva Exp $
 *
 * Description: This file contains CLI SET/GET/TEST and GETNEXT
 *              routines for the MIB objects specified in dhcps.mib
 *******************************************************************/

#ifndef __DHCPCLI_C__
#define __DHCPCLI_C__

#ifdef DHCP_SRV_WANTED
#include "lr.h"
#include "dhcpscom.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "dhcpslow.h"
#include "dhcpswr.h"
#include "dhcpcli.h"
#include "dhsclipt.h"
#include "cli.h"
#include "cfa.h"
#include "fsdhcpscli.h"

static UINT1        au1DnsStrOpt[DHCP_MAX_OPT_LEN];
/**************************************************************************/
/*  Function Name   : cli_process_dhcp_cmd                                */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_dhcp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[DHCPS_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode;
    UINT4               u4CmdType;
    UINT4               u4NetworkId;
    UINT4               u4NewNetworkIp;
    UINT4               u4NetMask = 0;
    UINT4               u4PoolIndex;
    UINT4               u4StartIp;
    UINT4               u4EndIp;
    UINT4               u4NodeTypeVal;
    UINT4               u4IpAddress;
    INT4                i4LeaseTime;
    INT4                i4RetStatus = CLI_SUCCESS;
    CHR1               *pu1OptVal;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN];
    UINT1              *pu1Inst;

    /* Variables defined for multi IP/DN implementation */
    UINT1               u1InputNo;
    UINT1               u1CurIndex;
    UINT1               au1InData[DHCP_MAX_OPT_LEN];
    MEMSET (au1InData, 0, DHCP_MAX_OPT_LEN);

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store DHCPS_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == DHCPS_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, DhcpSProtocolLock, DhcpSProtocolUnLock);
    DHCPS_PROTO_LOCK ();

    switch (u4Command)
    {
        case CLI_DHCPSRV_SHOW_INFO:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpSrvProcessShowConfig (CliHandle);
            break;

        case CLI_DHCPSRV_SHOW_POOLS:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpSrvProcessShowServerPools (CliHandle);
            break;

        case CLI_DHCPSRV_SHOW_BINDING:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpSrvProcessShowBinds (CliHandle);
            break;

        case CLI_DHCPSRV_SHOW_STATS:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpSrvProcessShowStats (CliHandle);
            break;

        case CLI_DHCPSRV_SERVICE:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus =
                DhcpSrvProcessSetServerEnable (CliHandle, DHCP_ENABLED);
            break;
        case CLI_DHCPSRV_NO_SERVICE:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus =
                DhcpSrvProcessSetServerEnable (CliHandle, DHCP_DISABLED);
            break;

        case CLI_DHCPSRV_POOL:
            /* args[0] -  DHCP Server Pool ID to create */
            i4RetStatus = (INT4)
                DhcpSrvProcessCreateAddressPool (CliHandle, *(INT4 *) args[0],
                                                 (UINT1 *) args[1]);
            if (i4RetStatus == CLI_SUCCESS)
            {
                SPRINTF ((CHR1 *) au1Cmd, "%s%d", CLI_DHCP_POOL,
                         *(INT4 *) args[0]);
                CliChangePath ((CONST CHR1 *) au1Cmd);
            }
            break;

        case CLI_DHCPSRV_NO_POOL:
            /* args[0] -  DHCP Server Pool ID to delete */
            i4RetStatus =
                DhcpSrvProcessDestroyAddressPool (CliHandle, *(INT4 *) args[0]);
            break;

        case CLI_DHCPSRV_NEXT_SERVER:
            /* args[0] -  Boot Server IP to set */
            i4RetStatus = DhcpSrvProcessSetBootServer (CliHandle, *args[0]);
            break;

        case CLI_DHCPSRV_NO_NEXT_SERVER:
            /* args[0] -  Boot Server IP to re-set */
            i4RetStatus =
                DhcpSrvProcessSetBootServer (CliHandle, DHCPS_DEF_BOOTSRV_IP);
            break;

        case CLI_DHCPSRV_BOOTFILE:
            /* args[0] -  Boot Server file name */
            i4RetStatus =
                DhcpSrvProcessSetBootServerFileName (CliHandle, (UINT1 *)
                                                     args[0]);
            break;

        case CLI_DHCPSRV_NO_BOOTFILE:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpSrvProcessResetBootServerFileName (CliHandle);
            break;

        case CLI_DHCPSRV_PARAMS:
            /* args[0] -  Command Type  DHCPSRV_PING_PACKETS/DHCPSRV_OFFER_REUSE 
             * args[1] -  Offer reuse time out if cmd type is DHCPSRV_OFFER_REUSE
             */
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            if (u4CmdType == DHCPSRV_PING_PACKETS)
            {
                i4RetStatus =
                    DhcpSrvProcessIcmpEchoEnable (CliHandle, DHCP_ENABLED);
            }
            else if (u4CmdType == DHCPSRV_OFFER_REUSE)
            {
                i4RetStatus =
                    DhcpSrvProcessSetAddrReuseTimeOut (CliHandle, *args[1]);
            }
            break;

        case CLI_DHCPSRV_NO_PARAMS:
            /* args[0] -  Cmd Type  DHCPSRV_PING_PACKETS/DHCPSRV_OFFER_REUSE/
             *                      DHCPSRV_BINDING_ADDR 
             * args[1] -  Address to be removed from the binding 
             *            if cmdtype is DHCPSRV_BINDING_ADDR
             */
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            switch (u4CmdType)
            {
                case DHCPSRV_PING_PACKETS:
                    i4RetStatus =
                        DhcpSrvProcessIcmpEchoEnable (CliHandle, DHCP_DISABLED);
                    break;
                case DHCPSRV_OFFER_REUSE:
                    i4RetStatus =
                        DhcpSrvProcessSetAddrReuseTimeOut (CliHandle,
                                                           DHCP_DEF_OFFER_REUSE_TIME);
                    break;
                case DHCPSRV_BINDING_ADDR:
                    i4RetStatus =
                        DhcpSrvProcessDelBinding (CliHandle, *args[1]);
                    break;
                default:
                    break;
            }
            break;

        case CLI_DHCPSRV_OPTION:
            /* args[0] -  Option Code
             * args[1] -  Sub-Option type
             * args[2] -  Option value
             */
            u4CmdType = CLI_PTR_TO_U4 (args[1]);

            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                if (*args[0] == DHCP_OPT_SIP_SERVER)
                {
                    au1InData[0] = 1;
                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[2]);
                    MEMCPY (&au1InData[1], pu1OptVal, STRLEN (pu1OptVal));
                    i4RetStatus = (INT4)
                        DhcpSrvProcessSetGlobalOption (CliHandle,
                                                       DHCP_OPT_SIP_SERVER,
                                                       (UINT1 *) au1InData,
                                                       STRLEN (pu1OptVal),
                                                       u4CmdType);

                }
                else
                {

                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[2]);
                    i4RetStatus =
                        DhcpSrvProcessSetGlobalOption (CliHandle,
                                                       *args[0],
                                                       (UINT1 *) pu1OptVal,
                                                       STRLEN (pu1OptVal),
                                                       u4CmdType);
                }
            }
            else
            {
                i4RetStatus =
                    DhcpSrvProcessSetGlobalOption (CliHandle,
                                                   *args[0], (UINT1 *) args[2],
                                                   STRLEN (args[2]), u4CmdType);
            }

            break;

        case CLI_DHCPSRV_NO_OPTION:
            /* args[0] -  Option Code */
            i4RetStatus = DhcpSrvProcessDelGlobalOption (CliHandle, *args[0]);
            break;

        case CLI_DHCPSRV_SIP_SERVER_GLOBAL:
            /* SIP SERVER - OPTION 120
             * args[0] -  Subnet Option Code
             * args[1] -  Sub-Option type
             * args[2]->args[n] -  Subnet Option value
             */

            u4CmdType = (UINT4) CLI_PTR_TO_U4 (args[1]);

            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                /* enc bit should be 1 incase of IP */
                au1InData[0] = 1;
                u1CurIndex = 1;
                u1InputNo = 2;
                while (args[u1InputNo] != NULL)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[(u1InputNo)]);
                    MEMCPY (&au1InData[u1CurIndex], pu1OptVal,
                            STRLEN (pu1OptVal));
                    u1CurIndex = (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));    /* NULL SEPERATE THE INPUTS */
                    u1InputNo++;
                }
                i4RetStatus = (INT4)
                    DhcpSrvProcessSetGlobalOption (CliHandle,
                                                   DHCP_OPT_SIP_SERVER,
                                                   (UINT1 *) au1InData,
                                                   (UINT4) (u1CurIndex - 1), 0);
            }
            else
            {
                /* enc bit should be 0 incase of domain names */
                au1InData[0] = 0;
                u1CurIndex = 1;    /* START AFTER enc byte */
                u1InputNo = 2;
                while (args[u1InputNo] != NULL)
                {
                    MEMCPY (&au1InData[u1CurIndex], args[u1InputNo],
                            STRLEN (args[u1InputNo]));
                    u1CurIndex =
                        (UINT1) (u1CurIndex + ((STRLEN (args[u1InputNo]) + 1)));
                    u1InputNo++;
                }
                i4RetStatus = (INT4)
                    DhcpSrvProcessSetGlobalOption (CliHandle,
                                                   DHCP_OPT_SIP_SERVER,
                                                   (UINT1 *) au1InData,
                                                   (UINT4) (u1CurIndex - 1), 0);
            }
            break;

        case CLI_DHCPSRV_NO_SIP_SERVER_GLOBAL:
            /* DELETE OPTION 120 - SIP SERVER */
            i4RetStatus = DhcpSrvProcessDelGlobalOption (CliHandle,
                                                         DHCP_OPT_SIP_SERVER);
            break;

        case CLI_DHCPSRV_NETWORK:
            /* args[0] -  Pool Subnet.
             * args[1] -  Subnet mask input type.
             * args[2] -  Subnet mask/Bits.
             * args[3] -  End IP for that subnet pool.
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4NetworkId = *args[0];

            if (CLI_PTR_TO_U4 (args[1]) == NETWORK_MASK_BITS)
            {
                u4NetMask = ~(0xffffffff >> *args[2]);
            }
            else if (CLI_PTR_TO_U4 (args[1]) == NETWORK_MASK_DECIMAL)
            {
                u4NetMask = *args[2];
            }
            else if (CLI_PTR_TO_U4 (args[1]) == NETWORK_DEFAULT_MASK)
            {
                u4NetMask = DHCP_DEF_SUBNET_MASK;
            }
            if (args[3] != NULL)
            {
                u4EndIp = *args[3];
            }
            else
            {
                u4EndIp =
                    (u4NetworkId & u4NetMask) + (0xffffffff & (~u4NetMask));
                u4EndIp -= 0x00000001;
            }
            u4NewNetworkIp = (u4NetworkId & u4NetMask);
            if (u4NetworkId == u4NewNetworkIp)
            {
                u4StartIp = u4NewNetworkIp + 1;
            }
            else
            {
                u4StartIp = u4NetworkId;
            }

            if (!(u4StartIp & 0x000000ff))
            {
                /* If the start IP begins in the format of x.x.x.0, then the start
                   IP should start from x.x.x.1 */
                u4StartIp = u4StartIp + 1;
            }

            u4NetworkId = (u4NetworkId & u4NetMask);
            if (((u4StartIp & u4NetMask) != u4NetworkId))

            {
                CliPrintf (CliHandle,
                           "\r%% Invalid Network & Subnet Mask combination\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            else if ((u4EndIp & u4NetMask) != u4NetworkId)
            {
                CliPrintf (CliHandle, "\r%% Invalid Subnet. "
                           "Check if End IP addresses belong to this subnet.\r\n ");
                i4RetStatus = CLI_FAILURE;
            }

            if (i4RetStatus != CLI_FAILURE)
            {
                i4RetStatus =
                    DhcpSrvProcessAddAddressPool (CliHandle, u4PoolIndex,
                                                  u4NetworkId, u4NetMask,
                                                  u4StartIp, u4EndIp);
            }
            break;

        case CLI_DHCPSRV_NO_NETWORK:
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelAddressPool (CliHandle, u4PoolIndex);
            break;

        case CLI_DHCPSRV_EXCLUDED_ADDR:
            /* args[0] -  Start Ip for exclude address pool.
             * args[1] -  End IP for exclude address pool.
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4StartIp = *args[0];
            u4EndIp = *args[1];

            i4RetStatus =
                DhcpSrvProcessAddExcludeAddrPool (CliHandle, u4PoolIndex,
                                                  u4StartIp, u4EndIp);

            break;

        case CLI_DHCPSRV_EXCLUDED_ADDR_ALL:
            /* args[0] -  Start Ip for exclude address pool.
             * args[1] -  End IP for exclude address pool.
             */
            MEMCPY (&u4StartIp, args[0], sizeof (UINT4));
            if (args[1] != NULL)
            {
                MEMCPY (&u4EndIp, args[1], sizeof (UINT4));
            }
            else
            {
                MEMCPY (&u4EndIp, args[0], sizeof (UINT4));
            }

            i4RetStatus =
                DhcpSrvProcessAddExcludeAddrPoolAll (CliHandle, u4StartIp,
                                                     u4EndIp);

            break;

        case CLI_DHCPSRV_NO_EXCLUDED_ADDR:
            /* args[0] -  Start Ip for exclude address pool.
             * args[1] -  End IP for exclude address pool.
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4StartIp = *args[0];
            if (args[1] != NULL)
            {
                u4EndIp = *args[1];
            }
            else
            {
                u4EndIp = DHCP_POOL_DEF_ENDIP;
            }

            i4RetStatus =
                DhcpSrvProcessDelExcludeAddrPool (CliHandle, u4PoolIndex,
                                                  u4StartIp, u4EndIp);

            break;

        case CLI_DHCPSRV_NO_EXCLUDED_ADDR_ALL:
            /* args[0] -  Start Ip for exclude address pool.
             * args[1] -  End IP for exclude address pool.
             */
            MEMCPY (&u4StartIp, args[0], sizeof (UINT4));
            if (args[1] != NULL)
            {
                MEMCPY (&u4EndIp, args[1], sizeof (UINT4));
            }
            else
            {
                u4EndIp = DHCP_POOL_DEF_ENDIP;
            }

            i4RetStatus =
                DhcpSrvProcessDelExcludeAddrPoolAll (CliHandle, u4StartIp,
                                                     u4EndIp);

            break;

        case CLI_DHCPSRV_DOMAIN_NAME:
            /* args[0] -  Domain name Subnet option.
             */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                         DOMAINNAME_OPT,
                                                         (UINT1 *) args[0],
                                                         STRLEN (args[0]), 0);
            break;

        case CLI_DHCPSRV_NO_DOMAIN_NAME:
            /* No addition command arguments, so ignore the args[] array */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessDelSubnetOption (CliHandle, u4PoolIndex,
                                                         DOMAINNAME_OPT);

            break;

        case CLI_DHCPSRV_DNS_SERVER:
            /* args[0] -  Dns server Ip Subnet option.
             */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u1CurIndex = 0;
            u1InputNo = 0;
            MEMSET (au1InData, 0, DHCP_MAX_OPT_LEN);
            while (args[u1InputNo] != NULL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[u1InputNo]);
                MEMCPY (&au1InData[u1CurIndex], pu1OptVal, STRLEN (pu1OptVal));
                u1CurIndex = (UINT1) (u1CurIndex + ((STRLEN (pu1OptVal) + 1)));
                u1InputNo++;
            }
            i4RetStatus = DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                         DNSSRV_OPT,
                                                         (UINT1 *) au1InData,
                                                         (UINT4) (u1CurIndex -
                                                                  1), 0);

            break;

        case CLI_DHCPSRV_NO_DNS_SERVER:
            /* No addition command arguments, so ignore the args[] array */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessDelSubnetOption
                (CliHandle, u4PoolIndex, DNSSRV_OPT);
            break;

        case CLI_DHCPSRV_DNS_SERVER_GLOBAL:
            /* args[0] -  Dns server Ip Subnet option.
             * args[1] -  Dns server Ip Subnet option.
             */

            u1CurIndex = 0;
            u1InputNo = 0;
            MEMSET (au1InData, 0, DHCP_MAX_OPT_LEN);
            while (args[u1InputNo] != NULL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[u1InputNo]);
                MEMCPY (&au1InData[u1CurIndex], pu1OptVal, STRLEN (pu1OptVal));
                u1CurIndex = (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                u1InputNo++;
            }
            i4RetStatus = DhcpSrvProcessSetGlobalOption (CliHandle,
                                                         DNSSRV_OPT,
                                                         (UINT1 *) au1InData,
                                                         (UINT4) (u1CurIndex -
                                                                  1), 0);

            break;

        case CLI_DHCPSRV_NO_DNS_SERVER_GLOBAL:
            /* No addition command arguments, so ignore the args[] array */
            i4RetStatus = DhcpSrvProcessDelGlobalOption (CliHandle, DNSSRV_OPT);
            break;

        case CLI_DHCPSRV_NETBIOS_SERVER:
            /* args[0] -  netbios-name server Ip Subnet option.
             */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[0]);

            i4RetStatus = DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                         NETBIOS_NAMESRV_OPT,
                                                         (UINT1 *) pu1OptVal,
                                                         4, 0);
            break;

        case CLI_DHCPSRV_NO_NETBIOS_SERVER:
            /* No addition command arguments, so ignore the args[] array */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessDelSubnetOption
                (CliHandle, u4PoolIndex, NETBIOS_NAMESRV_OPT);
            break;

        case CLI_DHCPSRV_NETBIOS_NODETYPE:
            /* args[0] -  netbios-node type Subnet option type.
             * args[1] -  netbios-node type Subnet option value.
             */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            if (args[1] != NULL)
            {
                u4NodeTypeVal = *args[1];
            }
            else
            {
                u4NodeTypeVal = CLI_PTR_TO_U4 (args[0]);
            }
            MEMSET (au1OptValue, 0, DHCP_MAX_OPT_LEN);
            SPRINTF ((CHR1 *) au1OptValue, "%u", u4NodeTypeVal);

            i4RetStatus = DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                         NETBIOS_NODETYPE_OPT,
                                                         au1OptValue, 1, 0);
            break;

        case CLI_DHCPSRV_NO_NETBIOS_NODETYPE:
            /* No addition command arguments, so ignore the args[] array */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessDelSubnetOption
                (CliHandle, u4PoolIndex, NETBIOS_NODETYPE_OPT);
            break;

        case CLI_DHCPSRV_DEFAULT_ROUTER:
            /* args[0] -  default-router Ip Subnet option.
             */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[0]);

            i4RetStatus = DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                         DEFAULT_ROUTER_OPT,
                                                         (UINT1 *) pu1OptVal,
                                                         4, 0);

            break;

        case CLI_DHCPSRV_NO_DEFAULT_ROUTER:
            /* No addition command arguments, so ignore the args[] array */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessDelSubnetOption
                (CliHandle, u4PoolIndex, DEFAULT_ROUTER_OPT);
            break;

        case CLI_DHCPSRV_POOL_OPTION:
            /* args[0] -  Subnet Option Code
             * args[1] -  Sub-Option type
             * args[2] -  Subnet Option value
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4CmdType = CLI_PTR_TO_U4 (args[1]);

            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                if (CFA_IS_LOOPBACK_RANGE (*args[2]))
                {
                    CliPrintf (CliHandle, "\r%% Invalid option value."
                               " Value should not be a Loopback address.\r\n");
                    i4RetStatus = CLI_FAILURE;
                }
                else
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[2]);
                    i4RetStatus =
                        DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                       *args[0],
                                                       (UINT1 *) pu1OptVal, 4,
                                                       u4CmdType);
                }
            }
            else
            {
                i4RetStatus =
                    DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                   *args[0], (UINT1 *) args[2],
                                                   STRLEN (args[2]), u4CmdType);
            }

            break;

        case CLI_DHCPSRV_SIP_SERVER:
            /* SIP SERVER - OPTION 120
             * args[0] -  Subnet Option Code
             * args[1] -  Sub-Option type
             * args[2]->args[n] -  Subnet Option value
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4CmdType = CLI_PTR_TO_U4 (args[1]);

            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                /* enc bit should be 1 incase of IP */
                au1InData[0] = 1;
                u1CurIndex = 1;
                u1InputNo = 2;
                while (args[u1InputNo] != NULL)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[(u1InputNo)]);
                    MEMCPY (&au1InData[u1CurIndex], pu1OptVal,
                            STRLEN (pu1OptVal));
                    u1CurIndex =
                        (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                    u1InputNo++;
                }
                i4RetStatus =
                    DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                   DHCP_OPT_SIP_SERVER,
                                                   (UINT1 *) au1InData,
                                                   (UINT4) (u1CurIndex - 1), 0);
            }
            else
            {

                /* enc bit should be 0 incase of domain names */
                au1InData[0] = 0;
                u1CurIndex = 1;    /* START AFTER enc bit */
                u1InputNo = 2;
                while (args[u1InputNo] != NULL)
                {
                    MEMCPY (&au1InData[u1CurIndex], args[u1InputNo],
                            STRLEN (args[u1InputNo]));
                    u1CurIndex =
                        (UINT1) (u1CurIndex + (STRLEN (args[u1InputNo]) + 1));
                    u1InputNo++;
                }
                i4RetStatus =
                    DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                                   DHCP_OPT_SIP_SERVER,
                                                   (UINT1 *) au1InData,
                                                   (UINT4) (u1CurIndex - 1), 0);
            }

            break;

        case CLI_DHCPSRV_NO_SIP_SERVER:
            /* DELETE OPTION 120 - SIP SERVER */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelSubnetOption (CliHandle, u4PoolIndex,
                                                         DHCP_OPT_SIP_SERVER);
            break;

        case CLI_DHCPSRV_VENDOR_SPECIFIC:
            /* VENDOR SPECIFIC - 43
             * args[0] - STRING INPUT - ONLY EXPECTS SINGLE STRING
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus =
                DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                               DHCP_OPT_SVENDOR_SPECIFIC,
                                               (UINT1 *) args[0],
                                               STRLEN (args[0]), 0);
            break;

        case CLI_DHCPSRV_NO_VENDOR_SPECIFIC:
            /* DELETE OPTION 43 - VENDOR SPECIFIC 43 */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelSubnetOption (CliHandle, u4PoolIndex,
                                                         DHCP_OPT_SVENDOR_SPECIFIC);
            break;

        case CLI_DHCPSRV_NTP_SERVER:
            /* OPTION 42 - SNTP
             * args[0],args[1] -  IP input
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u1CurIndex = 0;
            u1InputNo = 0;
            while (args[u1InputNo] != NULL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[u1InputNo]);
                MEMCPY (&au1InData[u1CurIndex], pu1OptVal, STRLEN (pu1OptVal));
                u1CurIndex = (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                u1InputNo++;
            }
            i4RetStatus =
                DhcpSrvProcessAddSubnetOption (CliHandle, u4PoolIndex,
                                               DHCP_OPT_NTP_SERVERS,
                                               (UINT1 *) au1InData,
                                               (UINT4) (u1CurIndex - 1), 0);
            break;

        case CLI_DHCPSRV_NO_NTP_SERVER:
            /* DELETE OPTION 42 - NTP SERVER */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelSubnetOption (CliHandle, u4PoolIndex,
                                                         DHCP_OPT_NTP_SERVERS);
            break;

        case CLI_DHCPSRV_NTP_SERVER_GLOBAL:
            /* OPTION 42 - SNTP
             * args[0],args[1] -  IP input
             */

            u1CurIndex = 0;
            u1InputNo = 0;
            while (args[u1InputNo] != NULL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[u1InputNo]);
                MEMCPY (&au1InData[u1CurIndex], pu1OptVal, STRLEN (pu1OptVal));
                u1CurIndex = (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                u1InputNo++;
            }
            i4RetStatus =
                DhcpSrvProcessSetGlobalOption (CliHandle,
                                               DHCP_OPT_NTP_SERVERS,
                                               (UINT1 *) au1InData,
                                               (UINT4) (u1CurIndex - 1), 0);
            break;

        case CLI_DHCPSRV_NO_NTP_SERVER_GLOBAL:
            /* DELETE OPTION 42 - SNTP SERVER */
            i4RetStatus = DhcpSrvProcessDelGlobalOption (CliHandle,
                                                         DHCP_OPT_NTP_SERVERS);
            break;

        case CLI_DHCPSRV_NO_POOL_OPTION:
            /* args[0] -  Subnet Option Code */
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessDelSubnetOption (CliHandle, u4PoolIndex,
                                                         *args[0]);
            break;

        case CLI_DHCPSRV_LEASE:
            /* args[0] -  Lease time in days.
             * args[1] -  Lease time in hours.
             * args[2] -  Lease time in minutes.
             * args[3] -  Infinite Lease
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4LeaseTime = 0;
            if (args[0] != NULL)
            {
                i4LeaseTime = (*(INT4 *) args[0]) * 24 * 60 * 60;
                if (args[1] != NULL)
                {
                    i4LeaseTime += (*(INT4 *) args[1]) * 60 * 60;
                }
                if (args[2] != NULL)
                {
                    i4LeaseTime += (*(INT4 *) args[2]) * 60;
                }
            }
            else
            {
                i4LeaseTime = 0x7fffffff;
            }

            i4RetStatus =
                DhcpSrvProcessSetLeaseTime (CliHandle, u4PoolIndex,
                                            i4LeaseTime);
            break;

        case CLI_DHCPSRV_NO_LEASE:

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessSetLeaseTime (CliHandle, u4PoolIndex,
                                                      (INT4)
                                                      DHCPSRV_LEASE_PERIOD);
            break;

        case CLI_DHCPSRV_THRESHOLD:

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessSetPoolThreshold (CliHandle,
                                                          u4PoolIndex,
                                                          *(INT4 *) args[0]);

            break;

        case CLI_DHCPSRV_NO_THRESHOLD:
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            i4RetStatus = DhcpSrvProcessSetPoolThreshold (CliHandle,
                                                          u4PoolIndex,
                                                          DHCPS_DEF_POOLTHRESHOLD);
            break;

        case CLI_DHCPSRV_HOST_IP:

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4IpAddress = *args[2];

            /*args[0] = Host type *
             * args[1] = Host ID *
             * args [2] = IP address */
            if (CFA_IS_LOOPBACK_RANGE (u4IpAddress))
            {
                CliPrintf (CliHandle, "\r%% Invalid option value."
                           " Value should not be a Loopback address.\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                i4RetStatus = DhcpSrvProcessAddHostIpConfig (CliHandle,
                                                             *args[0],
                                                             (UINT1 *) args[1],
                                                             u4PoolIndex,
                                                             u4IpAddress);
            }
            break;

        case CLI_DHCPSRV_NO_HOST_IP:
            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            /*args[0] = Host type *
             * args[1] = Host ID */
            i4RetStatus = DhcpSrvProcessDelHostIpConfig (CliHandle,
                                                         *args[0],
                                                         (UINT1 *) args[1],
                                                         u4PoolIndex);
            break;

        case CLI_DHCPSRV_NO_HOST:

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            /*args[0] = Host type *
             * args[1] = Host ID */
            i4RetStatus = DhcpSrvProcessDelHostConfig (CliHandle,
                                                       *args[0],
                                                       (UINT1 *) args[1],
                                                       u4PoolIndex);
            break;

        case CLI_DHCPSRV_HOST_OPTION:
            /* args[0] -  HOst Type 
             * args[1] -  Host Id         
             * args[2] -  Host Option Code
             * args[3] -  Host Option Sub-type
             * args[4] -  Host Option value
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4CmdType = CLI_PTR_TO_U4 (args[3]);

            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                if ((*args[2] == SUBNET_MASK_OPT)
                    && (!DHCP_IS_VALID_NETMASK (*args[4])))
                {
                    CliPrintf (CliHandle,
                               "\r%% Invalid value for option 1. Value should be a subnet mask.\r\n");
                    i4RetStatus = CLI_FAILURE;
                }
                else
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[4]);
                    i4RetStatus =
                        DhcpSrvProcessAddHostOption (CliHandle, u4PoolIndex,
                                                     *args[0],
                                                     (UINT1 *) args[1],
                                                     *args[2],
                                                     (UINT1 *) pu1OptVal, 4);
                }
            }
            else
            {
                i4RetStatus =
                    DhcpSrvProcessAddHostOption (CliHandle, u4PoolIndex,
                                                 *args[0], (UINT1 *) args[1],
                                                 *args[2], (UINT1 *) args[4],
                                                 STRLEN (args[4]));
            }
            break;

        case CLI_DHCPSRV_NO_HOST_OPTION:
            /* args[0] -  HOst Type 
             * args[1] -  Host Id         
             * args[2] -  Host Option Code
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelHostOption (CliHandle, u4PoolIndex,
                                                       *args[0],
                                                       (UINT1 *) args[1],
                                                       *args[2]);

            break;

        case CLI_DHCPSRV_SIP_SERVER_HOST:
            /* SIP SERVER - OPTION 120
             * args[0] -  HOst Type 
             * args[1] -  Host Id         
             * args[2] -  Host Option Code (120) 
             * args[3] -  Host Option Sub-type
             * args[4]->args[n] -  Subnet Option value
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            u4CmdType = CLI_PTR_TO_U4 (args[3]);

            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                /* enc type should be 1 incase of IP */
                au1InData[0] = 1;
                u1CurIndex = 1;
                u1InputNo = 4;
                while (args[u1InputNo] != NULL)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[(u1InputNo)]);
                    MEMCPY (&au1InData[u1CurIndex], pu1OptVal,
                            STRLEN (pu1OptVal));
                    u1CurIndex =
                        (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                    u1InputNo++;
                }
                i4RetStatus =
                    DhcpSrvProcessAddHostOption (CliHandle, u4PoolIndex,
                                                 *args[0], (UINT1 *) args[1],
                                                 DHCP_OPT_SIP_SERVER,
                                                 (UINT1 *) au1InData,
                                                 (UINT4) (u1CurIndex - 1));
            }
            else
            {

                /* enc type should be 0 incase of domain names */
                au1InData[0] = 0;
                u1CurIndex = 1;    /* Start copying after enc */
                u1InputNo = 4;
                while (args[u1InputNo] != NULL)
                {
                    MEMCPY (&au1InData[u1CurIndex], args[u1InputNo],
                            STRLEN (args[u1InputNo]));
                    u1CurIndex =
                        (UINT1) (u1CurIndex + (STRLEN (args[u1InputNo]) + 1));
                    u1InputNo++;
                }
                i4RetStatus =
                    DhcpSrvProcessAddHostOption (CliHandle, u4PoolIndex,
                                                 *args[0], (UINT1 *) args[1],
                                                 DHCP_OPT_SIP_SERVER,
                                                 (UINT1 *) au1InData,
                                                 (UINT4) (u1CurIndex - 1));
            }

            break;

        case CLI_DHCPSRV_NO_SIP_SERVER_HOST:
            /* DELETE OPTION 120 - SIP SERVER */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelHostOption (CliHandle, u4PoolIndex,
                                                       *args[0],
                                                       (UINT1 *) args[1],
                                                       DHCP_OPT_SIP_SERVER);
            break;

        case CLI_DHCPSRV_DNS_SERVER_HOST:
            /* DNS SERVER - OPTION 6
             * args[0] -  HOst Type 
             * args[1] -  Host Id         
             * args[2]->args[n] -  Subnet Option value
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            u1CurIndex = 0;
            u1InputNo = 2;
            while (args[u1InputNo] != NULL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[(u1InputNo)]);
                MEMCPY (&au1InData[u1CurIndex], pu1OptVal, STRLEN (pu1OptVal));
                u1CurIndex = (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                u1InputNo++;
            }
            i4RetStatus =
                DhcpSrvProcessAddHostOption (CliHandle, u4PoolIndex,
                                             *args[0], (UINT1 *) args[1],
                                             DHCP_OPT_DNS_NS,
                                             (UINT1 *) au1InData,
                                             (UINT4) (u1CurIndex - 1));
            break;

        case CLI_DHCPSRV_NO_DNS_SERVER_HOST:
            /* DELETE OPTION 6 - DNS SERVER */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelHostOption (CliHandle, u4PoolIndex,
                                                       *args[0],
                                                       (UINT1 *) args[1],
                                                       DHCP_OPT_DNS_NS);
            break;

        case CLI_DHCPSRV_NTP_SERVER_HOST:
            /* NTP SERVER - OPTION 42
             * args[0] -  HOst Type 
             * args[1] -  Host Id         
             * args[2]->args[n] -  Subnet Option value
             */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();

            u1CurIndex = 0;
            u1InputNo = 2;
            while (args[u1InputNo] != NULL)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, *args[(u1InputNo)]);
                MEMCPY (&au1InData[u1CurIndex], pu1OptVal, STRLEN (pu1OptVal));
                u1CurIndex = (UINT1) (u1CurIndex + (STRLEN (pu1OptVal) + 1));
                u1InputNo++;
            }
            i4RetStatus =
                DhcpSrvProcessAddHostOption (CliHandle, u4PoolIndex,
                                             *args[0], (UINT1 *) args[1],
                                             DHCP_OPT_NTP_SERVERS,
                                             (UINT1 *) au1InData,
                                             (UINT4) (u1CurIndex - 1));
            break;

        case CLI_DHCPSRV_NO_NTP_SERVER_HOST:
            /* DELETE OPTION 6 - DNS SERVER */

            u4PoolIndex = (UINT4) CLI_GET_DHCPPOOLID ();
            i4RetStatus = DhcpSrvProcessDelHostOption (CliHandle, u4PoolIndex,
                                                       *args[0],
                                                       (UINT1 *) args[1],
                                                       DHCP_OPT_NTP_SERVERS);
            break;

        case CLI_DHCPSRV_DEBUG:
            /* args[0] -  Debug Trace level to set */
            i4RetStatus =
                DhcpSrvProcessSetTraceLevel (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                             CLI_ENABLE);
            break;
        case CLI_DHCPSRV_NO_DEBUG:
            /* args[0] -  Debug Trace level to re-set */
            i4RetStatus =
                DhcpSrvProcessSetTraceLevel (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                             CLI_DISABLE);
            break;

        case CLI_DHCPSRV_CLEAR_STATS:
            i4RetStatus = DhcpSrvProcessSetClearCounters (CliHandle);
            break;

        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DHCPS_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", DhcpSrvCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);
    DHCPS_PROTO_UNLOCK ();

    UNUSED_PARAM (pu1Inst);

    return i4RetStatus;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetServerEnable                          */
/*                                                                            */
/* Description       : For Setting DHCP Server Service Enable/Disable         */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4Status   - DHCP_ENABLED/DHCP_DISABLED                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetServerEnable (tCliHandle CliHandle, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpSrvEnable (&u4ErrCode, u4Status) == SNMP_SUCCESS)
    {
#ifdef DHCP_RLY_WANTED
        if (u4Status == DHCP_ENABLED)
        {
            if (DhcpIsRlyEnabledInAnyCxt ())
            {
                CliPrintf (CliHandle,
                           "\r%% Invalid Operation. Disable DHCP Relay before "
                           "enabling DHCP server\r\n");
                return CLI_FAILURE;
            }
        }
#endif
        if (nmhSetDhcpSrvEnable (u4Status) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetAddrReuseTimeOut                      */
/*                                                                            */
/* Description       : For Setting Address Reuse Timeout Value                */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4AddrReuseTimeout - Address reuse timeout value       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetAddrReuseTimeOut (tCliHandle CliHandle,
                                   UINT4 u4AddrReuseTimeout)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2DhcpSrvOfferReuseTimeOut (&u4ErrorCode, u4AddrReuseTimeout) ==
        SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvOfferReuseTimeOut (u4AddrReuseTimeout) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessIcmpEchoEnable                           */
/*                                                                            */
/* Description       : For Setting Icmp Echo Enable/Disable                   */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4Status   - DHCP_ENABLED/DHCP_DISABLED                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessIcmpEchoEnable (tCliHandle CliHandle, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpSrvIcmpEchoEnable (&u4ErrCode, u4Status) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvIcmpEchoEnable (u4Status) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetBootServer                            */
/*                                                                            */
/* Description       : For Setting Dhcp Next Server IP address                */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4BootSrvIpAddr - Boot Server IP                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetBootServer (tCliHandle CliHandle, UINT4 u4BootSrvIpAddr)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2DhcpSrvBootServerAddress (&u4ErrorCode, u4BootSrvIpAddr) ==
        SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvBootServerAddress (u4BootSrvIpAddr) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetBootServerFileName                    */
/*                                                                            */
/* Description       : For Setting Dhcp Boot Server File Name                 */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     pu1BootSrvFileName - Boot Server file name             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetBootServerFileName (tCliHandle CliHandle,
                                     UINT1 *pu1BootSrvFileName)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE OctetFilename;

    OctetFilename.i4_Length = STRLEN (pu1BootSrvFileName);
    OctetFilename.pu1_OctetList = pu1BootSrvFileName;

    if (nmhTestv2DhcpSrvDefBootFilename (&u4ErrorCode, &OctetFilename) ==
        SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvDefBootFilename (&OctetFilename) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessResetBootServerFileName                  */
/*                                                                            */
/* Description       : To Set Dhcp Boot Server File Name to default file      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessResetBootServerFileName (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OctetFilename;
    UINT1               au1DefBootFileName[DHCP_MAX_NAME_LEN];
    if (STRLEN (DHCP_DEF_BOOT_FILE) != 0)
    {
        STRNCPY (gau1DhcpBootFileName, DHCP_DEF_BOOT_FILE,
                 STRLEN (DHCP_DEF_BOOT_FILE));
    }
    au1DefBootFileName[STRLEN (DHCP_DEF_BOOT_FILE)] = '\0';
    OctetFilename.i4_Length = STRLEN (DHCP_DEF_BOOT_FILE);
    OctetFilename.pu1_OctetList = au1DefBootFileName;

    if (nmhSetDhcpSrvDefBootFilename (&OctetFilename) == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CLI_FATAL_ERROR (CliHandle);
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetGlobalOption                          */
/*                                                                            */
/* Description       : Configures DHCP server Global Option parameters.       */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4Type     - Option Code                               */
/*                     pu1OptVal  - Option Value                              */
/*                     u4OptLen   - Option Length                             */
/*                     u4Cmd      - Ascii/Hex/IP                              */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetGlobalOption (tCliHandle CliHandle, UINT4 u4Type,
                               UINT1 *pu1OptVal, UINT4 u4OptLen, UINT4 u4Cmd)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               bIsCreated = FALSE;
    UINT1               u1GlobalOptn[DHCP_MAX_OPT_LEN];

    tDhcpOptions       *pOptions = NULL;
    tSNMP_OCTET_STRING_TYPE OctetValue;

    OctetValue.pu1_OctetList = u1GlobalOptn;

    /* Validate DHCP option and convert the option value to Octet string */
    i4RetVal = DhcpSrvProcessValidateOption (CliHandle, (UINT1) u4Type,
                                             &u4OptLen,
                                             pu1OptVal, TRUE,
                                             OctetValue.pu1_OctetList);
    if (i4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* update octet string length */
    OctetValue.i4_Length = u4OptLen;

    i4RetVal = nmhGetDhcpSrvGblOptRowStatus (u4Type, &i4RowStatus);

    /* If the row already exists then we need to make the row inactive before 
     * trying to perform any operation.
     */

    if ((i4RetVal == SNMP_SUCCESS) && (i4RowStatus == ACTIVE))
    {
        /* Scenario when row already exists and is active */

        nmhSetDhcpSrvGblOptRowStatus (u4Type, NOT_IN_SERVICE);
    }
    else if (i4RetVal == SNMP_FAILURE)
    {
        /* Scenario when no row exists */

        if (nmhSetDhcpSrvGblOptRowStatus (u4Type,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Global option pool is full\r\n ");
            return CLI_FAILURE;
        }
        bIsCreated = TRUE;
    }

    /* Test for Global option length. If a new entry has been created we 
     * remove the same from the table.
     */

    i4RetVal = nmhTestv2DhcpSrvGblOptLen (&u4ErrorCode, u4Type, u4OptLen);

    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvGblOptRowStatus (u4Type, DESTROY);
        }
        return CLI_FAILURE;
    }

    /* Processing for global option value */

    i4RetVal = nmhTestv2DhcpSrvGblOptVal (&u4ErrorCode, u4Type, &OctetValue);

    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvGblOptRowStatus (u4Type, DESTROY);
        }
        return CLI_FAILURE;
    }
    else
    {
        nmhSetDhcpSrvGblOptLen (u4Type, u4OptLen);

        nmhSetDhcpSrvGblOptVal (u4Type, &OctetValue);

        pOptions = DhcpGetOptionFromList (gpDhcpOptions, u4Type);

        if (pOptions == NULL)
        {
            return SNMP_FAILURE;
        }
        pOptions->u4CmdType = u4Cmd;
    }

    /* We have tested both option len and the option type. 
     * Call the set functions for both of them. */

    i4RetVal = nmhSetDhcpSrvGblOptRowStatus (u4Type, ACTIVE);

    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvGblOptRowStatus (u4Type, DESTROY);
        }
        CliPrintf (CliHandle, "\r%% Invalid Option specified\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelGlobalOption                          */
/*                                                                            */
/* Description       : Deletes DHCP server Global Option parameters.          */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4GlobalOptType - Option Code                          */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelGlobalOption (tCliHandle CliHandle, UINT4 u4GlobalOptType)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2DhcpSrvGblOptRowStatus (&u4ErrorCode, u4GlobalOptType, DESTROY)
        == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvGblOptRowStatus (u4GlobalOptType, DESTROY) ==
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessAddSubnetOption                          */
/*                                                                            */
/* Description       : Configures DHCP server Subnet Option parameters.       */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpSrvPoolCfgIndex - DHCP Subnet Pool Index         */
/*                     u4Type     - Option Code                               */
/*                     pu1OptVal  - Option Value                              */
/*                     u4Len      - Option Length                             */
/*                     u4Cmd      - Ascii/Hex/IP                              */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessAddSubnetOption (tCliHandle CliHandle,
                               UINT4 u4DhcpSrvPoolCfgIndex, UINT4 u4Type,
                               UINT1 *pu1OptValue, UINT4 u4Len, UINT4 u4Cmd)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               bIsCreated = FALSE;
    UINT1               u1SubnetOptn[DHCP_MAX_OPT_LEN];
    tDhcpPool          *pEntry = NULL;
    tDhcpOptions       *pOptions = NULL;
    tSNMP_OCTET_STRING_TYPE OctetValue;
    MEMSET (u1SubnetOptn, 0, DHCP_MAX_OPT_LEN);
    OctetValue.pu1_OctetList = u1SubnetOptn;

    /* Validate DHCP option and convert the option value to Octet string */
    i4RetVal = DhcpSrvProcessValidateOption (CliHandle, u4Type,
                                             &u4Len,
                                             pu1OptValue, TRUE,
                                             OctetValue.pu1_OctetList);

    if (i4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* update octet string length */
    OctetValue.i4_Length = u4Len;

    i4RetVal =
        nmhGetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex,
                                         u4Type, &i4RowStatus);

    /* check for existing row with given index */
    if ((i4RetVal == SNMP_SUCCESS) && (i4RowStatus == ACTIVE))
    {
        /* Chage row status from ACTIVE to NOT IN SERVICE */
        nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex, u4Type,
                                         NOT_IN_SERVICE);
    }
    else if (i4RetVal == SNMP_FAILURE)
    {
        /* Create new row when row is not existed */

        if (nmhSetDhcpSrvSubnetOptRowStatus
            (u4DhcpSrvPoolCfgIndex, u4Type, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r Subnet option table is full\r\n ");
            return CLI_FAILURE;
        }
        bIsCreated = TRUE;
    }

    /*  validate the option length */
    i4RetVal =
        nmhTestv2DhcpSrvSubnetOptLen (&u4ErrorCode, u4DhcpSrvPoolCfgIndex,
                                      u4Type, u4Len);

    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex, u4Type,
                                             DESTROY);
        }
        return CLI_FAILURE;
    }

    /* validate option value */
    i4RetVal =
        nmhTestv2DhcpSrvSubnetOptVal (&u4ErrorCode, u4DhcpSrvPoolCfgIndex,
                                      u4Type, &OctetValue);
    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex, u4Type,
                                             DESTROY);
        }
        return CLI_FAILURE;
    }

    /* set the parameters */
    if ((nmhSetDhcpSrvSubnetOptVal (u4DhcpSrvPoolCfgIndex, u4Type, &OctetValue)
         == SNMP_FAILURE)
        || (nmhSetDhcpSrvSubnetOptLen (u4DhcpSrvPoolCfgIndex, u4Type, u4Len) ==
            SNMP_FAILURE))
    {
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex, u4Type,
                                             DESTROY);
        }
        return CLI_FAILURE;
    }

    pEntry = DhcpGetPoolEntry (u4DhcpSrvPoolCfgIndex);

    if (pEntry == NULL)
    {
        return CLI_FAILURE;
    }

    pOptions = DhcpGetOptionFromList (pEntry->pOptions, u4Type);

    if (pOptions == NULL)
    {
        return CLI_FAILURE;
    }

    pOptions->u4CmdType = u4Cmd;

    /* make the row status active */
    i4RetVal =
        nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex, u4Type, ACTIVE);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Value for Option Length/Value\r\n");
        if (bIsCreated == TRUE)
        {
            nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpSrvPoolCfgIndex, u4Type,
                                             DESTROY);
        }
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelSubnetOption                          */
/*                                                                            */
/* Description       : Deletes DHCP server Subnet Option parameters.          */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpSrvPoolCfgIndex - DHCP Subnet Pool Index         */
/*                     u4Type     - Option Code                               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelSubnetOption (tCliHandle CliHandle,
                               UINT4 u4DhcpSrvPoolCfgIndex, UINT4 u4Type)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpSrvSubnetOptRowStatus
        (&u4ErrCode, u4DhcpSrvPoolCfgIndex, u4Type, DESTROY) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvSubnetOptRowStatus
            (u4DhcpSrvPoolCfgIndex, u4Type, DESTROY) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessCreateAddressPool                        */
/*                                                                            */
/* Description       : Configures DHCP Subnet Pool                            */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpSrvPoolCfgIndex - DHCP Subnet Pool Index         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessCreateAddressPool (tCliHandle CliHandle,
                                 UINT4 u4DhcpSrvPoolCfgIndex,
                                 UINT1 *u1DhcpSrvPoolName)
{
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE DhcpSrvPoolName;

    /* check for existing row */
    i4RetVal = (INT4)
        nmhGetDhcpSrvSubnetPoolRowStatus ((INT4) u4DhcpSrvPoolCfgIndex,
                                          &i4RowStatus);

    if (i4RetVal != SNMP_FAILURE)
    {
        /* A row already exists */
        /* Test and Set Pool Name if it is passed as input */
        if (u1DhcpSrvPoolName != NULL)
        {
            /* Move Pool Name to Standard Data structure */
            DhcpSrvPoolName.pu1_OctetList = (UINT1 *) u1DhcpSrvPoolName;
            DhcpSrvPoolName.i4_Length = (INT4) STRLEN (u1DhcpSrvPoolName);
            if (nmhTestv2DhcpSrvSubnetPoolName (&u4ErrorCode,
                                                (INT4) u4DhcpSrvPoolCfgIndex,
                                                &DhcpSrvPoolName) ==
                SNMP_SUCCESS)
            {
                if (nmhSetDhcpSrvSubnetPoolName
                    ((INT4) u4DhcpSrvPoolCfgIndex,
                     &DhcpSrvPoolName) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }
    /*  Create new row and make it NOT_IN_SERVICE when it doesn't exists */
    if (nmhSetDhcpSrvSubnetPoolRowStatus
        (u4DhcpSrvPoolCfgIndex, CREATE_AND_WAIT) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvSubnetPoolRowStatus
            (u4DhcpSrvPoolCfgIndex, NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            /* Test and Set Pool Name if it is passed as input */
            if (u1DhcpSrvPoolName != NULL)
            {
                /* Move Pool Name to Standard Data structure */
                DhcpSrvPoolName.pu1_OctetList = (UINT1 *) u1DhcpSrvPoolName;
                DhcpSrvPoolName.i4_Length = (INT4) STRLEN (u1DhcpSrvPoolName);
                if (nmhTestv2DhcpSrvSubnetPoolName (&u4ErrorCode,
                                                    (INT4)
                                                    u4DhcpSrvPoolCfgIndex,
                                                    &DhcpSrvPoolName) ==
                    SNMP_SUCCESS)
                {
                    if (nmhSetDhcpSrvSubnetPoolName
                        (u4DhcpSrvPoolCfgIndex,
                         &DhcpSrvPoolName) != SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                }
                else
                {
                    return CLI_FAILURE;
                }
            }
            return CLI_SUCCESS;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Subnet Option Table is full\r\n");
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDestroyAddressPool                       */
/*                                                                            */
/* Description       : Deletes the DHCP Subnet Pool                           */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpSrvPoolCfgIndex - DHCP Subnet Pool Index         */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDestroyAddressPool (tCliHandle CliHandle,
                                  UINT4 u4DhcpSrvPoolCfgIndex)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpSrvSubnetPoolRowStatus
        (&u4ErrCode, (INT4) u4DhcpSrvPoolCfgIndex, DESTROY) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpSrvPoolCfgIndex, DESTROY) ==
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelBinding                               */
/*                                                                            */
/* Description       : Deletes the bind entry from binding table              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DelBindIpAddr - Bind Address to delete               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelBinding (tCliHandle CliHandle, UINT4 u4DelBindIpAddr)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpSrvBindEntryStatus (&u4ErrCode, u4DelBindIpAddr, DESTROY)
        == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvBindEntryStatus (u4DelBindIpAddr, DESTROY) ==
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }

    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessShowGblOpts                              */
/*                                                                            */
/* Description       : Displays DHCP Server Global Options                    */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessShowGblOpts (tCliHandle CliHandle)
{
    INT1                i1Flag = DHCP_SUCCESS;
    INT4                i4FirstOptType = 0;
    INT4                i4NextOptType = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4Length = 0;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN];

    i4RetVal = nmhGetFirstIndexDhcpSrvGblOptTable (&i4FirstOptType);

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    i4NextOptType = i4FirstOptType;

    OptionValue.pu1_OctetList = au1OptValue;

    do
    {
        i4FirstOptType = i4NextOptType;

        nmhGetDhcpSrvGblOptLen (i4FirstOptType, (INT4 *) &u4Length);

        MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);

        nmhGetDhcpSrvGblOptVal (i4FirstOptType, &OptionValue);

        DhcpSrvProcessValidateOption (CliHandle, (UINT1) i4FirstOptType,
                                      &u4Length,
                                      OptionValue.pu1_OctetList, FALSE,
                                      au1OptionStr);

        /*Print the Header only once */
        if (i1Flag == DHCP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rGlobal Options\r\n");
            CliPrintf (CliHandle, "\r--------------\r\n\r\n");
            i1Flag = DHCP_FAILURE;
        }
        CliPrintf (CliHandle, "%-10s : %5d, ", "Code", i4FirstOptType);
        i4PageStatus =
            CliPrintf (CliHandle, "%-10s : %s", "Value", au1OptionStr);

        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

    }
    while (nmhGetNextIndexDhcpSrvGblOptTable (i4FirstOptType, &i4NextOptType) ==
           SNMP_SUCCESS);

    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : DhcpSrvSubnetOptTableInfo                              */
/*                                                                            */
/* Description       : For displaying Dhcp Subnet options                     */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS                                            */
/******************************************************************************/
VOID
DhcpSrvSubnetOptTableInfo (tCliHandle CliHandle, INT4 i4PoolIndex,
                           INT4 i4OptType, INT4 *pi4PageStatus)
{
    INT4                i4OptLen;
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN];
    tSNMP_OCTET_STRING_TYPE OptionValue;

    OptionValue.pu1_OctetList = au1OptValue;
    MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);

    nmhGetDhcpSrvSubnetOptLen (i4PoolIndex, i4OptType, &i4OptLen);

    nmhGetDhcpSrvSubnetOptVal (i4PoolIndex, i4OptType, &OptionValue);
    MEMCPY (au1OptionStr, OptionValue.pu1_OctetList, i4OptLen);

    /* change octet string to display string */
    DhcpSrvProcessValidateOption (CliHandle,
                                  (UINT1) i4OptType,
                                  (UINT4 *) &i4OptLen,
                                  OptionValue.pu1_OctetList,
                                  (UINT1) FALSE, au1OptionStr);

    CliPrintf (CliHandle, "%-10s : %5d, ", "Code", i4OptType);

    *pi4PageStatus = CliPrintf (CliHandle, "%-10s : %s", "Value", au1OptionStr);
    return;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessShowServerPools                          */
/*                                                                            */
/* Description       : For displaying Dhcp Address pool information,          */
/*                     Subnet options and Host Options                        */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS                                            */
/******************************************************************************/

INT4
DhcpSrvProcessShowServerPools (tCliHandle CliHandle)
{
    INT1                i1OutCome;
    INT1                i1SubnetStat = DHCP_FAILURE;
    INT1                i1HostStat = DHCP_FAILURE;
    INT4                i4RetVal;
    INT4                i4FirstSubnetPoolIndex = 0;
    INT4                i4NextSubnetPoolIndex = 0;
    INT4                i4TempFirstPoolIndex = 0;
    INT4                i4TempNextPoolIndex = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    INT4                i4PoolIndex = 0;
    INT4                i4PrevPoolIndex = 0;
    INT4                i4PrevOptType = 0;
    INT4                i4OptType = 0;
    INT4                i4FirstOptType = 0;
    INT4                i4NextOptType = 0;
    INT4                i4FirstHostType = 0;
    INT4                i4NextHostType = 0;
    UINT4               u4OptionLen = 0;
    UINT4               u4SubnetSubnet = 0;
    UINT4               u4SubnetMask = 0;
    UINT4               u4StartIpAddr = 0;
    UINT4               u4EndIpAddr = 0;
    UINT4               u4LeaseTime = 0;
    UINT4               u4Threshold = 0;
    UINT4               u4FirstExStartIp = 0;
    UINT4               u4NextExStartIp = 0;
    UINT4               u4ExEndIp = 0;
    UINT4               u4HostIpAddress = 0;
    CHR1               *pu1IpAddr = NULL;
    UINT1               au1OptValue[DHCP_MAX_OPT_LEN];
    UINT1               au1PoolName[(DHCP_MAX_NAME_LEN + 1)];
    UINT1               au1MacStr[DHCP_MAX_CID_LEN];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN];
    UINT1               au1FisrtHostClientMac[DHCP_MAX_CID_LEN];
    UINT1               au1NextHostClientMac[DHCP_MAX_CID_LEN];

    tSNMP_OCTET_STRING_TYPE FirstHostId;
    tSNMP_OCTET_STRING_TYPE NextHostId;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    tSNMP_OCTET_STRING_TYPE PoolName;

    MEMSET (au1FisrtHostClientMac, 0, DHCP_MAX_CID_LEN);
    MEMSET (au1NextHostClientMac, 0, DHCP_MAX_CID_LEN);
    MEMSET (au1PoolName, 0, (DHCP_MAX_NAME_LEN + 1));

    FirstHostId.pu1_OctetList = au1FisrtHostClientMac;
    NextHostId.pu1_OctetList = au1NextHostClientMac;
    OptionValue.pu1_OctetList = au1OptValue;
    PoolName.pu1_OctetList = au1PoolName;
/*We are taking the lock inside this function. No need to take it here*/
    if (DhcpSrvProcessShowGblOpts (CliHandle) == CLI_FAILURE)
        return CLI_SUCCESS;

    i4RetVal =
        nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (&i4FirstSubnetPoolIndex);

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    i4NextSubnetPoolIndex = i4FirstSubnetPoolIndex;    /* reverse assignment
                                                       in loop */
    do
    {
        /* update for next entry */
        i4FirstSubnetPoolIndex = i4NextSubnetPoolIndex;
        /* get the configuration parameters for each pool */
        if ((nmhGetDhcpSrvSubnetPoolRowStatus
             (i4FirstSubnetPoolIndex, &i4RetVal) != SNMP_FAILURE) &&
            (i4RetVal == ACTIVE))
        {
            nmhGetDhcpSrvSubnetPoolName (i4FirstSubnetPoolIndex, &PoolName);
            nmhGetDhcpSrvSubnetSubnet (i4FirstSubnetPoolIndex, &u4SubnetSubnet);
            nmhGetDhcpSrvSubnetMask (i4FirstSubnetPoolIndex, &u4SubnetMask);
            nmhGetDhcpSrvSubnetLeaseTime (i4FirstSubnetPoolIndex,
                                          (INT4 *) &u4LeaseTime);
            nmhGetDhcpSrvSubnetUtlThreshold (i4FirstSubnetPoolIndex,
                                             (INT4 *) &u4Threshold);
            nmhGetDhcpSrvSubnetStartIpAddress
                (i4FirstSubnetPoolIndex, &u4StartIpAddr);
            nmhGetDhcpSrvSubnetEndIpAddress (i4FirstSubnetPoolIndex,
                                             &u4EndIpAddr);

            CliPrintf (CliHandle, "\r\n%-30s : %d\r\n", "Pool Id",
                       i4FirstSubnetPoolIndex);
            CliPrintf (CliHandle,
                       "-------------------------------------------\r\n");

            if (PoolName.i4_Length != 0)
            {
                CliPrintf (CliHandle, "%-30s : %s\r\n", "Pool Name",
                           PoolName.pu1_OctetList);
            }
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4SubnetSubnet);
            CliPrintf (CliHandle, "%-30s : %s\r\n", "Subnet", pu1IpAddr);

            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4SubnetMask);
            CliPrintf (CliHandle, "%-30s : %s\r\n", "Subnet Mask", pu1IpAddr);

            CliPrintf (CliHandle, "%-30s : %d secs\r\n", "Lease time",
                       u4LeaseTime);

            CliPrintf (CliHandle, "%-30s : %d%%\r\n",
                       "Utilization threshold", u4Threshold);

            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4StartIpAddr);
            CliPrintf (CliHandle, "%-30s : %s\r\n", "Start Ip", pu1IpAddr);

            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4EndIpAddr);
            i4PageStatus =
                CliPrintf (CliHandle, "%-30s : %s\r\n", "End Ip", pu1IpAddr);

            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            if (nmhGetFirstIndexDhcpSrvExcludeIpAddressTable
                (&i4TempFirstPoolIndex, &u4FirstExStartIp) == SNMP_SUCCESS)
            {

                i4TempNextPoolIndex = i4TempFirstPoolIndex;

                u4NextExStartIp = u4FirstExStartIp;

                do
                {
                    i4TempFirstPoolIndex = i4TempNextPoolIndex;

                    u4FirstExStartIp = u4NextExStartIp;

                    if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                    {
                        /* Print Exclude address pool info of current Pool Id */
                        continue;
                    }
                    u4ExEndIp = 0;
                    nmhGetDhcpSrvExcludeEndIpAddress (i4TempFirstPoolIndex,
                                                      u4FirstExStartIp,
                                                      &u4ExEndIp);

                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4FirstExStartIp);
                    CliPrintf (CliHandle, "%-30s : %s\r\n",
                               "Exclude Address Start IP", pu1IpAddr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4ExEndIp);
                    i4PageStatus =
                        CliPrintf (CliHandle, "%-30s : %s\r\n",
                                   "Exclude Address End IP", pu1IpAddr);

                    if (i4PageStatus == CLI_FAILURE)
                    {
                        break;
                    }
                }
                while (nmhGetNextIndexDhcpSrvExcludeIpAddressTable
                       (i4TempFirstPoolIndex, &i4TempNextPoolIndex,
                        u4FirstExStartIp, &u4NextExStartIp) == SNMP_SUCCESS);
            }

            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }
            /*dhcpSrvSubnetOptTable */
            i1OutCome =
                nmhGetFirstIndexDhcpSrvSubnetOptTable (&i4PoolIndex,
                                                       &i4OptType);
            i1SubnetStat = DHCP_SUCCESS;
            while (i1OutCome != SNMP_FAILURE)
            {
                if (i4PoolIndex == i4FirstSubnetPoolIndex)
                {
                    if (i1SubnetStat == DHCP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nSubnet Options\r\n");
                        CliPrintf (CliHandle, "--------------\r\n");
                        i1SubnetStat = DHCP_FAILURE;
                    }
                    DhcpSrvSubnetOptTableInfo (CliHandle, i4PoolIndex,
                                               i4OptType, &i4PageStatus);
                }

                i4PrevPoolIndex = i4PoolIndex;
                i4PrevOptType = i4OptType;
                i1OutCome =
                    nmhGetNextIndexDhcpSrvSubnetOptTable (i4PrevPoolIndex,
                                                          &i4PoolIndex,
                                                          i4PrevOptType,
                                                          &i4OptType);
            }
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }
        }

        /* Display Host configuration table */
        if (nmhGetFirstIndexDhcpSrvHostConfigTable (&i4FirstHostType,
                                                    &FirstHostId,
                                                    &i4TempFirstPoolIndex) ==
            SNMP_SUCCESS)
        {
            i4TempNextPoolIndex = i4TempFirstPoolIndex;
            i4NextHostType = i4FirstHostType;

            NextHostId.i4_Length = FirstHostId.i4_Length;
            MEMCPY (NextHostId.pu1_OctetList,
                    FirstHostId.pu1_OctetList, FirstHostId.i4_Length);

            i1HostStat = DHCP_SUCCESS;
            do
            {
                i4TempFirstPoolIndex = i4TempNextPoolIndex;

                i4FirstHostType = i4NextHostType;
                FirstHostId.i4_Length = NextHostId.i4_Length;
                MEMCPY (FirstHostId.pu1_OctetList,
                        NextHostId.pu1_OctetList, NextHostId.i4_Length);

                if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                {
                    /* Print Host configuration info only for current Pool */
                    continue;
                }
                u4HostIpAddress = 0;

                nmhGetDhcpSrvHostIpAddress (i4FirstHostType,
                                            &FirstHostId,
                                            i4TempFirstPoolIndex,
                                            &u4HostIpAddress);

                if (i4FirstHostType == 1)
                {
                    CLI_CONVERT_MAC_TO_DOT_STR (FirstHostId.pu1_OctetList,
                                                au1MacStr);
                }
                else
                {
                    MEMCPY (au1MacStr, FirstHostId.pu1_OctetList,
                            FirstHostId.i4_Length);
                }
                if ((i1HostStat == DHCP_SUCCESS) && (u4HostIpAddress != 0))
                {
                    CliPrintf (CliHandle, "\r\nHost Configurations\r\n");
                    CliPrintf (CliHandle, "-------------------\r\n");
                    CliPrintf (CliHandle, "%-30s", "Client Identifier");
                    CliPrintf (CliHandle, "%-16s", "IP address");
                    CliPrintf (CliHandle, "\r\n");
                    i1HostStat = DHCP_FAILURE;
                }

                if (u4HostIpAddress != 0)
                {
                    CliPrintf (CliHandle, "%-30s", au1MacStr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4HostIpAddress);
                    CliPrintf (CliHandle, "%-16s", pu1IpAddr);
                    CliPrintf (CliHandle, "\r\n");
                }

            }
            while (nmhGetNextIndexDhcpSrvHostConfigTable (i4FirstHostType,
                                                          &i4NextHostType,
                                                          &FirstHostId,
                                                          &NextHostId,
                                                          i4TempFirstPoolIndex,
                                                          &i4TempNextPoolIndex)
                   == SNMP_SUCCESS);

        }

        if (nmhGetFirstIndexDhcpSrvHostOptTable (&i4FirstHostType, &FirstHostId,
                                                 &i4TempFirstPoolIndex,
                                                 &i4FirstOptType)
            == SNMP_SUCCESS)
        {
            i4TempNextPoolIndex = i4TempFirstPoolIndex;
            i4NextHostType = i4FirstHostType;
            i4NextOptType = i4FirstOptType;

            NextHostId.i4_Length = FirstHostId.i4_Length;
            MEMCPY (NextHostId.pu1_OctetList,
                    FirstHostId.pu1_OctetList, FirstHostId.i4_Length);
            OptionValue.pu1_OctetList = au1OptValue;

            i1HostStat = DHCP_SUCCESS;

            do
            {
                i4TempFirstPoolIndex = i4TempNextPoolIndex;

                i4FirstHostType = i4NextHostType;
                i4FirstOptType = i4NextOptType;
                FirstHostId.i4_Length = NextHostId.i4_Length;
                MEMCPY (FirstHostId.pu1_OctetList,
                        NextHostId.pu1_OctetList, NextHostId.i4_Length);

                if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                {
                    /* Print Host option info of current Pool Id */
                    continue;
                }

                nmhGetDhcpSrvHostOptLen (i4FirstHostType,
                                         &FirstHostId,
                                         i4TempFirstPoolIndex,
                                         i4FirstOptType, (INT4 *) &u4OptionLen);

                MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);

                nmhGetDhcpSrvHostOptVal (i4FirstHostType, &FirstHostId,
                                         i4TempFirstPoolIndex, i4FirstOptType,
                                         &OptionValue);

                if (i4FirstHostType == 1)
                {
                    CLI_CONVERT_MAC_TO_DOT_STR (FirstHostId.pu1_OctetList,
                                                au1MacStr);
                }
                else
                {
                    MEMCPY (au1MacStr, FirstHostId.pu1_OctetList,
                            FirstHostId.i4_Length);
                }

                MEMCPY (au1OptionStr, OptionValue.pu1_OctetList, u4OptionLen);

                /* change octet string to display string */
                DhcpSrvProcessValidateOption (CliHandle, (UINT1) i4FirstOptType,
                                              &u4OptionLen,
                                              OptionValue.pu1_OctetList, FALSE,
                                              au1OptionStr);

                if (i1HostStat == DHCP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nHost Options\r\n");
                    CliPrintf (CliHandle, "------------\r\n");
                    CliPrintf (CliHandle, "%-30s ", "Client Identifier");
                    CliPrintf (CliHandle, "%-20s ", "Hardware type");
                    CliPrintf (CliHandle, "%-10s ", "Code");
                    CliPrintf (CliHandle, "%-10s ", "Value");
                    CliPrintf (CliHandle, "\r\n");
                    i1HostStat = DHCP_FAILURE;
                }

                CliPrintf (CliHandle, "%-30s ", au1MacStr);
                CliPrintf (CliHandle, "%-20d ", i4FirstHostType);
                CliPrintf (CliHandle, "%-10d ", i4FirstOptType);

                i4PageStatus = CliPrintf (CliHandle, "%-10s", au1OptionStr);

                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }

                CliPrintf (CliHandle, "\r");
            }
            while (nmhGetNextIndexDhcpSrvHostOptTable
                   (i4FirstHostType, &i4NextHostType,
                    &FirstHostId, &NextHostId,
                    i4TempFirstPoolIndex, &i4TempNextPoolIndex,
                    i4FirstOptType, &i4NextOptType) == SNMP_SUCCESS);

        }
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while (nmhGetNextIndexDhcpSrvSubnetPoolConfigTable
           (i4FirstSubnetPoolIndex, &i4NextSubnetPoolIndex) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetTraceLevel                            */
/*                                                                            */
/* Description       : for Setting/Re-setting DHCP Server Trace Level         */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4TraceLevel - Trace Level                             */
/*                     u1TraceFlag  - Trace Flag CLI_ENABLE/CLI_DISABLE       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetTraceLevel (tCliHandle CliHandle, INT4 i4TraceLevel,
                             UINT1 u1TraceFlag)
{
    UINT4               u4ErrCode;
    INT4                i4DhcpSrvTrace;

    nmhGetDhcpSrvDebugLevel (&i4DhcpSrvTrace);

    if (u1TraceFlag == CLI_DISABLE)
    {
        /* for reset of all trace command i4TraceLevel will have all bits sets
         * and doing a complement will reset all bit positions.
         */
        i4TraceLevel = i4DhcpSrvTrace & (~i4TraceLevel);
    }
    else
    {
        i4TraceLevel = i4DhcpSrvTrace | i4TraceLevel;
    }

    if (nmhTestv2DhcpSrvDebugLevel (&u4ErrCode, i4TraceLevel) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvDebugLevel (i4TraceLevel) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessValidateOption                           */
/*                                                                            */
/* Description       : This function validate the options depending           */
/*                     upon the length and type as specified in RFC 2132      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4Type - type of the option                            */
/*                     pu4Len - Length of the option                          */
/*                     pu1InStr - Option Value                                */
/*                     u1ConfFlag - Flag to indicate whether it's for display */
/*                     pRetStr  - Formatted Option Value                      */
/*                                                                            */
/* Output Parameters : Returns the length of the option in pu4Len for         */
/*                     specific option types.                                 */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessValidateOption (tCliHandle CliHandle, UINT4 u4Type,
                              UINT4 *pu4Len, UINT1 *pu1InStr,
                              UINT1 u1ConfFlag, UINT1 *pRetStr)
{
    UINT1              *pu1TempPtr;
    CHR1               *pu1TempIp = NULL;
    UINT4               u4IpAddr = 0;
    tUtlInAddr          IpAddr;
    UINT1               u1CurIndex;
    UINT1               u1OutBufIndex;
    UINT1               u1LenIndex;
    INT1                u1LenCurstr;
    UINT4               u4InStrLen = *pu4Len;
    UINT1               u1TempLen = 0;

    /* NEW MULTI IP HANDLING VARIABLES */
    UINT1               au1OutData[DHCP_MAX_OPT_LEN];
    UINT1               au1TempIp[DHCP_MAX_STR_IP_LEN + 1];

    MEMSET (au1OutData, 0, DHCP_MAX_OPT_LEN);
    MEMSET (au1TempIp, 0, DHCP_MAX_STR_IP_LEN + 1);

    pu1TempPtr = pRetStr;

    switch (u4Type)
    {
            /* Options With Specific Values */
        case 19:
        case 20:
        case 27:
        case 29:
        case 30:
        case 31:
        case 34:
        case 36:
        case 39:
            if (*pu4Len != 1)
            {
                CliPrintf (CliHandle,
                           "\r%% Configurable Option's Length must be 1 \r\n");
                return (CLI_FAILURE);
            }

            if (u1ConfFlag)
            {
                pu1InStr[0] = (UINT1) CLI_ATOI (pu1InStr);

                if ((pu1InStr[0] == 0) || (pu1InStr[0] == 1))
                {
                    MEMCPY (pRetStr, &pu1InStr[0], 1);
                    *(pRetStr + 1) = '\0';
                    return (CLI_SUCCESS);
                }
                return (CLI_FAILURE);
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%d\r\n", pu1InStr[0]);
                return (CLI_SUCCESS);
            }
            break;

        case 46:

            if (*pu4Len != 1)
            {
                CliPrintf (CliHandle,
                           "\r%% Configurable Option's Length must be 1 \r\n");
                return (CLI_FAILURE);
            }

            if (u1ConfFlag)
            {
                pu1InStr[0] = (UINT1) CLI_ATOI (pu1InStr);

                MEMCPY (pRetStr, &pu1InStr[0], 1);
                *(pRetStr + 1) = '\0';
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%d\r\n", pu1InStr[0]);
            }
            return (CLI_SUCCESS);

            /* Options With Minimum Length 1 */
        case 12:
        case 14:
        case 15:
        case 17:
        case 18:
        case 40:
        case 43:
        case 47:
        case 64:
        case 66:
        case 67:
        case 240:
            if (*pu4Len < 1)
            {
                CliPrintf (CliHandle, "\r%% Option's Length must be >= 1 \r\n");
                return (CLI_FAILURE);
            }
            if (*pu4Len > DHCP_MAX_OPT_LEN)
            {
                CliPrintf (CliHandle,
                           "\r%% Length of string exceeds maximum value 255 \r\n");
                return (CLI_FAILURE);
            }
            if (u1ConfFlag)
            {
                MEMCPY (pRetStr, pu1InStr, STRLEN (pu1InStr));
                *pu4Len = STRLEN (pu1InStr);
            }
            else
            {
                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);
            }
            return (CLI_SUCCESS);

            /* Options With Minimum Length 4 */
        case 16:
            if (u1ConfFlag == TRUE)
            {
                if (!(CLI_INET_ATON (pu1InStr, &IpAddr)))
                {
                    CliPrintf (CliHandle, "\r%% Invalid option value."
                               " Value should be an IP address.\r\n");
                    return (CLI_FAILURE);
                }
                u4IpAddr = IpAddr.u4Addr;
                CLI_CONVERT_VAL_TO_STR (pu1TempPtr, u4IpAddr);

                /* return the length of the option in the pointer
                 * as it should be the length of the option value and 
                 * not the length of the given string
                 */
                *pu4Len = sizeof (UINT4);
            }
            else
            {
                MEMCPY (&u4IpAddr, pu1InStr, sizeof (UINT4));

                IpAddr.u4Addr = u4IpAddr;
                pu1TempIp = UtlInetNtoa (IpAddr);

                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1TempIp);
            }
            return (CLI_SUCCESS);
            /* Options With Length 2 and (2*n) */

        case 25:

            if ((*pu4Len != 0) && (*pu4Len % 2 == 0))
            {
                if (u1ConfFlag == TRUE)
                {
                    MEMCPY (pu1TempPtr, pu1InStr, *pu4Len);
                }
                else
                {
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);

                }
                return (CLI_SUCCESS);
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Option's Length must be Multiple of 2\r\n");
                return (CLI_FAILURE);
            }

            /* Options With Minimum Length 0 and (4*n) */

        case 68:
            if (u1ConfFlag == TRUE)
            {
                if (!(CLI_INET_ATON (pu1InStr, &IpAddr)))
                {
                    CliPrintf (CliHandle, "\r%% Invalid option value."
                               " Value should be an IP address.\r\n");
                    return (CLI_FAILURE);
                }
                u4IpAddr = IpAddr.u4Addr;

                CLI_CONVERT_VAL_TO_STR (pu1TempPtr, u4IpAddr);

                /* return the length of the option in the pointer
                 * as it should be the length of the option value and 
                 * not the length of the given string
                 */
                *pu4Len = sizeof (UINT4);
            }
            else
            {
                MEMCPY (&u4IpAddr, pu1InStr, sizeof (UINT4));
                IpAddr.u4Addr = u4IpAddr;
                pu1TempIp = UtlInetNtoa (IpAddr);

                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1TempIp);
            }
            return (CLI_SUCCESS);
            /* With Minimum Length 4 and (4*n) */

        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 41:
        case 44:
        case 45:
        case 48:
        case 49:
        case 65:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:
            if (u1ConfFlag == TRUE)
            {
                if (!(CLI_INET_ATON (pu1InStr, &IpAddr)))
                {
                    CliPrintf (CliHandle, "\r%% Invalid option value."
                               " Value should be an IP address.\r\n");
                    return (CLI_FAILURE);
                }
                u4IpAddr = IpAddr.u4Addr;

                CLI_CONVERT_VAL_TO_STR (pu1TempPtr, u4IpAddr);

                /* return the length of the option in the pointer
                 * as it should be the length of the option value and 
                 * not the length of the given string
                 */
                *pu4Len = sizeof (UINT4);
            }
            else
            {
                MEMCPY (&u4IpAddr, pu1InStr, sizeof (UINT4));

                IpAddr.u4Addr = u4IpAddr;
                pu1TempIp = UtlInetNtoa (IpAddr);

                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1TempIp);
            }
            return (CLI_SUCCESS);

            /* With Minimum Length 8 and (8*n) */
        case 21:
        case 33:
            if ((*pu4Len != 0) && (*pu4Len % 8 == 0))
            {
                if (u1ConfFlag == TRUE)
                {
                    MEMCPY (pu1TempPtr, pu1InStr, *pu4Len);
                }
                else
                {
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", pu1InStr);

                }
                return (CLI_SUCCESS);
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Option's Length must be Multiple of 8\r\n");
                return (CLI_FAILURE);
            }
        case 6:
        case 42:
            /* DNS SERVER HANDLED HERE */
            /* SNTP ALSO HANDLED HERE  */
            if (u1ConfFlag == TRUE)
            {                    /* CONFIGURE */
                *pu4Len = 0;
                for (u1CurIndex = 0; u1CurIndex < u4InStrLen;
                     u1CurIndex =
                     (UINT1) (u1CurIndex + (STRLEN (au1TempIp) + 1)))
                {
                    /* check each IP to see if it is an IP */
                    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                    MEMSET (au1TempIp, 0, DHCP_MAX_STR_IP_LEN);
                    MEMCPY (au1TempIp, &pu1InStr[u1CurIndex],
                            STRLEN (&pu1InStr[u1CurIndex]));
                    if (!(CLI_INET_ATON (au1TempIp, &IpAddr)))
                    {
                        CliPrintf (CliHandle, "\r%% Invalid option value."
                                   "Value should be an IP address.\r\n");
                        return (CLI_FAILURE);
                    }
                    u4IpAddr = IpAddr.u4Addr;
                    CLI_CONVERT_VAL_TO_STR ((UINT1 *) (&au1OutData[(*pu4Len)]),
                                            u4IpAddr);
                    *pu4Len += sizeof (UINT4);
                }
                /* COPY OUTPUT TO RETURN STRING */
                MEMCPY (pu1TempPtr, au1OutData, *pu4Len);
            }
            else
            {                    /* PRINT */
                /* pu1TempPtr is the printed value */
                u1OutBufIndex = 0;
                for (u1CurIndex = 0; u1CurIndex < u4InStrLen;
                     u1CurIndex = (UINT1) (u1CurIndex + sizeof (UINT4)))
                {
                    if (u1OutBufIndex >= DHCP_MAX_ALLOWED_LEN)
                    {
                        /* Exceeded output buffer allowed size */
                        break;
                    }
                    if (u1OutBufIndex != 0)
                    {
                        au1OutData[u1OutBufIndex] = ',';
                        u1OutBufIndex++;
                    }
                    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                    MEMSET (&u4IpAddr, 0, sizeof (UINT4));
                    MEMCPY (&u4IpAddr, &pu1InStr[u1CurIndex], sizeof (UINT4));
                    IpAddr.u4Addr = u4IpAddr;
                    pu1TempIp = UtlInetNtoa (IpAddr);
                    MEMCPY (&au1OutData[u1OutBufIndex], pu1TempIp,
                            STRLEN (pu1TempIp));
                    u1OutBufIndex =
                        (UINT1) (u1OutBufIndex + STRLEN (pu1TempIp));
                }
                SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", au1OutData);
            }
            return (CLI_SUCCESS);

        case 120:
            if (u1ConfFlag == TRUE)
            {
                if (pu1InStr[0] == 1)
                {
                    /* SET FIRST BIT AS 1 because it is IP */
                    au1OutData[0] = 1;
                    *pu4Len = 1;    /* UPDATE LENGTH ALSO */

                    for (u1CurIndex = 1; u1CurIndex < u4InStrLen;
                         u1CurIndex =
                         (UINT1) (u1CurIndex + STRLEN (au1TempIp) + 1))
                    {            /* check each IP to see if it is an IP */
                        MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                        MEMSET (au1TempIp, 0, DHCP_MAX_STR_IP_LEN + 1);
                        u1TempLen =
                            (UINT1) (STRLEN (&pu1InStr[u1CurIndex]) + 1);
                        if (u1TempLen >= DHCP_MAX_STR_IP_LEN + 1)
                        {
                            CliPrintf (CliHandle, "\r%% Invalid option value."
                                       "IP address length is too high.\r\n");
                            return (CLI_FAILURE);
                        }
                        MEMCPY (au1TempIp, &pu1InStr[u1CurIndex], u1TempLen);
                        if (!(CLI_INET_ATON (au1TempIp, &IpAddr)))
                        {
                            CliPrintf (CliHandle, "\r%% Invalid option value."
                                       "Value should be an IP address.\r\n");
                            return (CLI_FAILURE);
                        }
                        u4IpAddr = IpAddr.u4Addr;
                        CLI_CONVERT_VAL_TO_STR ((UINT1
                                                 *) (&au1OutData[(*pu4Len)]),
                                                u4IpAddr);
                        *pu4Len += sizeof (UINT4);
                    }
                    /* COPY OUTPUT TO RETURN STRING */
                    MEMCPY (pu1TempPtr, au1OutData, *pu4Len);
                }
                else if (pu1InStr[0] == 0)
                {
                    /* CONFIG DOMAIN NAME */
                    if ((u4InStrLen - 1) < 1)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Option's Length must be >= 1 \r\n");
                        return (CLI_FAILURE);
                    }
                    if ((u4InStrLen - 1) > DHCP_MAX_OPT_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Length of string exceeds maximum value 255 \r\n");
                        return (CLI_FAILURE);
                    }
                    /* if (u1ConfFlag) */
                    u1LenIndex = 0;
                    u1OutBufIndex = 1;
                    u1CurIndex = 1;
                    u1LenCurstr = 0;    /* reset len of cur str */

                    au1OutData[0] = 0;    /* Set enc bit for string */
                    /* appending length for first substring */
                    u1LenIndex = u1OutBufIndex;
                    u1OutBufIndex++;

                    while (u1CurIndex < (u4InStrLen + 1))
                    {
                        /* read each char from in_buf to au1OutData till reach \0 or . */
                        if (pu1InStr[u1CurIndex] == '.')
                        {
                            /* in this case we replace '.' so we set u1LenIndex to location OF '.' */
                            au1OutData[u1LenIndex] = (UINT1) (u1LenCurstr);
                            u1LenCurstr = -1;    /* reset len of cur str *//* start @ -1 becos it incremenets every iteration */
                            u1LenIndex = u1OutBufIndex;
                        }
                        if (pu1InStr[u1CurIndex] == 0)
                        {
                            /* in this case we dont replace '0' so we set u1LenIndex to location AFTER '0' */
                            au1OutData[u1LenIndex] = (UINT1) (u1LenCurstr);    /* UINT1 48 -- '0' */
                            u1LenCurstr = -1;    /* reset len of cur str */
                            u1LenIndex = (UINT1) (u1OutBufIndex + 1);
                            u1OutBufIndex++;
                        }
                        au1OutData[u1OutBufIndex] = pu1InStr[u1CurIndex];
                        u1CurIndex++;
                        u1OutBufIndex++;
                        u1LenCurstr++;    /* increment len of current string */
                    }
                    /* length of au1OutData is u1OutBufIndex */
                    *pu4Len = u1OutBufIndex;
                    MEMCPY (pRetStr, au1OutData, u1OutBufIndex);
                    return (CLI_SUCCESS);
                }
                else
                {
                    /* Unexpected input value i.e enc bit is set wrong */
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (pu1InStr[0] == 1)
                {
                    /* pu1TempPtr is the printed value */
                    u1OutBufIndex = 0;
                    for (u1CurIndex = 0; u1CurIndex < (u4InStrLen - 1);
                         u1CurIndex = (UINT1) (u1CurIndex + sizeof (UINT4)))
                    {
                        if (u1OutBufIndex != 0)
                        {
                            au1OutData[u1OutBufIndex] = ',';
                            u1OutBufIndex++;
                        }
                        MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
                        MEMSET (&u4IpAddr, 0, sizeof (UINT4));
                        MEMCPY (&u4IpAddr, &pu1InStr[(u1CurIndex + 1)],
                                sizeof (UINT4));
                        IpAddr.u4Addr = u4IpAddr;
                        pu1TempIp = UtlInetNtoa (IpAddr);
                        MEMCPY (&au1OutData[u1OutBufIndex], pu1TempIp,
                                STRLEN (pu1TempIp));
                        u1OutBufIndex =
                            (UINT1) (u1OutBufIndex + STRLEN (pu1TempIp));
                    }
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", au1OutData);
                }
                else
                {
                    /* PRINT DOMAIN NAME */
                    /* modded code for multi input */
                    MEMSET (au1OutData, 0, DHCP_MAX_OPT_LEN);
                    u1OutBufIndex = 0;
                    u1CurIndex = 1;
                    u1LenCurstr = 0;
                    while (u1CurIndex < (*pu4Len))
                    {
                        u1LenCurstr = (INT1) (pu1InStr[u1CurIndex]);
                        u1CurIndex++;
                        MEMCPY (&au1OutData[u1OutBufIndex],
                                &pu1InStr[u1CurIndex], u1LenCurstr);
                        u1OutBufIndex = (UINT1) (u1OutBufIndex + u1LenCurstr);
                        u1CurIndex = (UINT1) (u1CurIndex + u1LenCurstr);
                        if (pu1InStr[u1CurIndex] == 0)
                        {
                            if (au1OutData[u1OutBufIndex - 1] == ',')
                            {    /* If no inputs were recieved last time exit loop and remove ',' */
                                au1OutData[u1OutBufIndex - 1] = 0;
                                break;
                            }
                            au1OutData[u1OutBufIndex] = ',';
                            u1OutBufIndex++;
                            u1CurIndex++;
                        }
                        else
                        {
                            au1OutData[u1OutBufIndex] = '.';
                            u1OutBufIndex++;
                        }
                    }
                    SPRINTF ((CHR1 *) pu1TempPtr, "%s\r\n", au1OutData);
                }
            }
            return (CLI_SUCCESS);

            /* Should Not Allow Configuration */
        case 0:
        case 255:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:
        case 58:
        case 59:
        case 60:

            CliPrintf (CliHandle, "\r%% Not Configurable Option\r\n");
            return (CLI_FAILURE);

        default:
            CliPrintf (CliHandle, "\r%% Not Configurable Option\r\n");
            return (CLI_FAILURE);
    }
    /* *INDENT-ON* */
}

/******************************************************************************/
/* Function Name     : DhcpSrvGetPoolCfgPrompt                                */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/

INT1
DhcpSrvGetPoolCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4PoolIndex;
    UINT4               u4Len;

    if (!(pi1ModeName) || !(pi1DispStr))
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_DHCP_POOL);
    if (STRNCMP (pi1ModeName, CLI_DHCP_POOL, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4PoolIndex = CLI_ATOI (pi1ModeName);

    if (DhcpGetPoolEntry (u4PoolIndex) == NULL)
    {
        return FALSE;
    }

    if (((UINT4) CLI_SET_DHCPPOOLID ((INT4) u4PoolIndex)) != u4PoolIndex)
        return FALSE;

    STRNCPY (pi1DispStr, "(dhcp-config)#", STRLEN ("(dhcp-config)#"));
    pi1DispStr[STRLEN ("(dhcp-config)#")] = '\0';

    return TRUE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessAddAddressPool                           */
/*                                                                            */
/* Description       : This function configures the DHCP Subnet Pool          */
/*                     information for the give subnet                        */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Subnet Pool index               */
/*                     u4Subnet  - Subnet for that Pool                       */
/*                     u4SubnetMask - Mask to be used along with the subnet   */
/*                     u4StartIp    - Start IP address for that subnet pool   */
/*                     u4EndIp      - End IP address for that subnet pool     */
/*                     u4LeaseTime  - Lease time till which the Subnet Pool   */
/*                                    information can be held                 */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessAddAddressPool (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                              UINT4 u4Subnet, UINT4 u4SubnetMask,
                              UINT4 u4StartIp, UINT4 u4EndIp)
{
    CHR1               *pu1OptVal;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = SNMP_SUCCESS;

    /* check for existing row */
    i4RetVal = nmhGetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, &i4RowStatus);

    if ((i4RetVal == SNMP_SUCCESS) && (i4RowStatus == ACTIVE))
    {
        /* if row already existed, then change the row status 
         * to Not_in_Service */

        DhcpSrvProcessDelAddressPool (CliHandle, u4DhcpPoolIndex);
    }
    else if (i4RetVal == SNMP_FAILURE)
    {
        /*  Create new row  when not existed */
        i4RetVal =
            nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, CREATE_AND_WAIT);

        if (i4RetVal == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Validate Subnet */
    i4RetVal = nmhTestv2DhcpSrvSubnetSubnet (&u4ErrorCode,
                                             u4DhcpPoolIndex, u4Subnet);

    if ((i4RetVal == SNMP_FAILURE))
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhSetDhcpSrvSubnetSubnet (u4DhcpPoolIndex, u4Subnet);
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return CLI_FAILURE;
    }

    /* Validation of Subnet Mask */
    i4RetVal =
        nmhTestv2DhcpSrvSubnetMask (&u4ErrorCode,
                                    u4DhcpPoolIndex, u4SubnetMask);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhSetDhcpSrvSubnetMask (u4DhcpPoolIndex, u4SubnetMask);
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return CLI_FAILURE;
    }

    /* Validation for Start IP Address */
    i4RetVal =
        nmhTestv2DhcpSrvSubnetStartIpAddress (&u4ErrorCode,
                                              u4DhcpPoolIndex, u4StartIp);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhSetDhcpSrvSubnetStartIpAddress (u4DhcpPoolIndex, u4StartIp);
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return CLI_FAILURE;
    }

    /* Validation for End Ip Address */
    i4RetVal =
        nmhTestv2DhcpSrvSubnetEndIpAddress (&u4ErrorCode, u4DhcpPoolIndex,
                                            u4EndIp);

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhSetDhcpSrvSubnetEndIpAddress (u4DhcpPoolIndex, u4EndIp);
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return CLI_FAILURE;
    }

    /* After configring all the objects, set the Row status active */
    i4RetVal = nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, ACTIVE);

    if (i4RetVal == SNMP_FAILURE)
    {
        i4RetVal = nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, DESTROY);
        return CLI_FAILURE;
    }
    /*Add a subnet option for subnet mask. The clients should be offered
     * with this Netmask*/
    CLI_CONVERT_IPADDR_TO_STR (pu1OptVal, u4SubnetMask);

    i4RetVal = DhcpSrvProcessAddSubnetOption (CliHandle, u4DhcpPoolIndex,
                                              SUBNET_MASK_OPT,
                                              (UINT1 *) pu1OptVal, 4, 0);
    return i4RetVal;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelAddressPool                           */
/*                                                                            */
/* Description       : This function deletes the Subnet Pool configurations   */
/*                     made for the given Subnet Pool index                   */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Subnet Pool index               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelAddressPool (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex)
{
    UINT4               u4ErrorCode = 0;
    tDhcpPool          *pEntry = NULL;
    tDhcpOptions       *pOptions = NULL;
    tExcludeIpAddr     *pExclude = NULL;
    tDhcpBinding       *pBinding = NULL;
    UINT4               u4DhcpSrvExcludeStartIpAddress = 0;
    UINT4               u4DhcpSrvSubnetOptType = 0;
    UINT4               u4DhcpSrvBindIpAddress = 0;

    /* check for whether row is existed for deletion */
    if (nmhTestv2DhcpSrvSubnetPoolRowStatus
        (&u4ErrorCode, u4DhcpPoolIndex, NOT_IN_SERVICE) == SNMP_SUCCESS)
    {
        if (nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, NOT_IN_SERVICE)
            == SNMP_SUCCESS)
        {
            /* Set Pools network parameters to default values
             */
            nmhSetDhcpSrvSubnetSubnet (u4DhcpPoolIndex, 0);
            nmhSetDhcpSrvSubnetMask (u4DhcpPoolIndex, 0);
            nmhSetDhcpSrvSubnetStartIpAddress (u4DhcpPoolIndex, 0);
            nmhSetDhcpSrvSubnetEndIpAddress (u4DhcpPoolIndex, 0);

            /* remove all the entries in the ICMP wait list */
            DhcpFreeSubnetPoolIcmpList (u4DhcpPoolIndex);

            /* remove all the entries in the binding list */

            pBinding = gpDhcpBinding;

            while (pBinding != NULL)
            {
                if (pBinding->u4PoolId == u4DhcpPoolIndex)
                {
                    u4DhcpSrvBindIpAddress = pBinding->u4IpAddress;
                    pBinding = pBinding->pNext;
                    nmhSetDhcpSrvBindEntryStatus (u4DhcpSrvBindIpAddress,
                                                  DESTROY);
                }
                else
                {
                    pBinding = pBinding->pNext;
                }
            }

            /* remove all the options created for this Pool */

            pEntry = DhcpGetPoolEntry (u4DhcpPoolIndex);

            if (pEntry == NULL)
            {
                return CLI_FAILURE;
            }

            for (pOptions = pEntry->pOptions; pOptions != NULL;)
            {
                u4DhcpSrvSubnetOptType = pOptions->u4Type;
                pOptions = pOptions->pNext;
                nmhSetDhcpSrvSubnetOptRowStatus (u4DhcpPoolIndex,
                                                 u4DhcpSrvSubnetOptType,
                                                 DESTROY);
            }

            /* remove all the excluded ip address nodes for 
             * this Pool */

            for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;)
            {
                u4DhcpSrvExcludeStartIpAddress = pExclude->u4StartIpAddr;
                pExclude = pExclude->pNext;
                nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                      u4DhcpSrvExcludeStartIpAddress,
                                                      DESTROY);
            }
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessAddExcludeAddrPool                       */
/*                                                                            */
/* Description       : This function Configures the Exclude address Pool      */
/*                     table information for the given Subnet Pool index      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Subnet Pool index               */
/*                     u4StartIp       - Exclude address start IP             */
/*                     u4EndIp         - Exclude address End IP               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessAddExcludeAddrPool (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                                  UINT4 u4StartIp, UINT4 u4EndIp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;

    if ((nmhGetDhcpSrvSubnetPoolRowStatus
         (u4DhcpPoolIndex, &i4RetVal) == SNMP_FAILURE) || (i4RetVal != ACTIVE))
    {
        CliPrintf (CliHandle, "\r%% No subnet pool configured\r\n");
        return CLI_FAILURE;
    }

    /* Check for existing row */
    i4RetVal = nmhGetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                     u4StartIp, &i4RowStatus);

    /* Low level routines are not implemented to make row status to Not in 
       service */

    if (i4RetVal == SNMP_FAILURE)
    {
        if (nmhTestv2DhcpSrvExcludeAddressRowStatus
            (&u4ErrorCode, u4DhcpPoolIndex, u4StartIp,
             CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            if (u4ErrorCode == SNMP_ERR_RESOURCE_UNAVAILABLE)
            {
                CliPrintf (CliHandle,
                           "\r%% Exclude IP Address range reached to maximum value %d \r\n",
                           MAX_DHSRV_EXCL_NODE);
            }
            return CLI_FAILURE;
        }
        if (nmhSetDhcpSrvExcludeAddressRowStatus
            (u4DhcpPoolIndex, u4StartIp, CREATE_AND_WAIT) != SNMP_SUCCESS)
        {

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Validation of Exclude End IP Address */
    if (nmhTestv2DhcpSrvExcludeEndIpAddress
        (&u4ErrorCode, u4DhcpPoolIndex, u4StartIp, u4EndIp) != SNMP_SUCCESS)
    {
        nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex, u4StartIp,
                                              DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetDhcpSrvExcludeEndIpAddress (u4DhcpPoolIndex, u4StartIp, u4EndIp)
        != SNMP_SUCCESS)
    {
        nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex, u4StartIp,
                                              DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Make the row status active */
    i4RetVal =
        nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex, u4StartIp,
                                              ACTIVE);
    if (i4RetVal == SNMP_FAILURE)
    {
        nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex, u4StartIp,
                                              DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelExcludeAddrPool                       */
/*                                                                            */
/* Description       : This function deletes the Exclude address Pool         */
/*                     table information for the given Subnet Pool index      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Subnet Pool index               */
/*                     u4StartIp       - Exclude address start IP             */
/*                     u4EndIp         - Exclude address End IP               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelExcludeAddrPool (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                                  UINT4 u4StartIp, UINT4 u4EndIp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4ExclPoolEndIp;
    INT4                i4RetVal = SNMP_FAILURE;

    if ((nmhGetDhcpSrvSubnetPoolRowStatus
         (u4DhcpPoolIndex, &i4RetVal) == SNMP_FAILURE) || (i4RetVal != ACTIVE))
    {
        CliPrintf (CliHandle, "\r%% No subnet pool configured\r\n");
        return CLI_FAILURE;
    }

    i4RetVal =
        nmhTestv2DhcpSrvExcludeAddressRowStatus (&u4ErrorCode, u4DhcpPoolIndex,
                                                 u4StartIp, DESTROY);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* If the EndIp is a valid ucast_addr then check for the exact endIp from the table 
     * and if it matches, then allow to delete the entry from exclude address pool
     */
    if (u4EndIp != DHCP_POOL_DEF_ENDIP)
    {
        nmhGetDhcpSrvExcludeEndIpAddress (u4DhcpPoolIndex, u4StartIp,
                                          &u4ExclPoolEndIp);
        if (u4EndIp != u4ExclPoolEndIp)
        {
            CliPrintf (CliHandle,
                       "\r%% Exclude pool EndIp doesn't matches with given EndIp\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhSetDhcpSrvExcludeAddressRowStatus
        (u4DhcpPoolIndex, u4StartIp, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot remove Exclude pool, invalid method\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessAddExcludeAddrPoolAll                    */
/*                                                                            */
/* Description       : This function Configures the Exclude address Pool      */
/*                     table information for all the Subnet Pool index        */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4StartIp       - Exclude address start IP             */
/*                     u4EndIp         - Exclude address End IP               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessAddExcludeAddrPoolAll (tCliHandle CliHandle, UINT4 u4StartIp,
                                     UINT4 u4EndIp)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4DhcpPoolIndex = 0;
    INT4                i4NextDhcpPoolIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NextIpAddr = 0;
    UINT4               u4Count = 0;
    UINT4               u4PoolCount = 0;

    if (nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (&i4NextDhcpPoolIndex)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% DHCP Pool Not Configured\r\n");
        return CLI_FAILURE;
    }
    if ((nmhGetDhcpSrvSubnetPoolRowStatus
         (i4NextDhcpPoolIndex, &i4RetVal) == SNMP_FAILURE)
        || (i4RetVal != ACTIVE))
    {
        CliPrintf (CliHandle, "\r%% No subnet pool configured\r\n");
        return CLI_FAILURE;
    }

    u4NextIpAddr = u4IpAddr;

    do
    {
        u4DhcpPoolIndex = i4NextDhcpPoolIndex;

        u4PoolCount++;
        if (u4NextIpAddr != u4StartIp)
        {
            u4IpAddr = u4NextIpAddr;

            /* Check for existing row */
            i4RetVal = nmhGetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                             u4StartIp,
                                                             &i4RowStatus);

            /* Low level routines are not implemented to make row status to Not in 
               service */

            if (i4RetVal == SNMP_FAILURE)
            {
                if (nmhTestv2DhcpSrvExcludeAddressRowStatus
                    (&u4ErrorCode, u4DhcpPoolIndex, u4StartIp,
                     CREATE_AND_WAIT) != SNMP_SUCCESS)
                {
                    if (u4ErrorCode == SNMP_ERR_RESOURCE_UNAVAILABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Exclude IP Address range reached to maximum value %d \r\n",
                                   MAX_DHSRV_EXCL_NODE);
                    }
                    else if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
                    {
                        u4Count++;
                    }
                    continue;
                }

                if (nmhSetDhcpSrvExcludeAddressRowStatus
                    (u4DhcpPoolIndex, u4StartIp, CREATE_AND_WAIT)
                    != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Cannot create Exclude "
                               "pool for pool id %d\r\n", u4DhcpPoolIndex);
                    u4Count++;
                    continue;
                }
            }

            /* Validation of Exclude End IP Address */
            if (nmhTestv2DhcpSrvExcludeEndIpAddress
                (&u4ErrorCode, u4DhcpPoolIndex, u4StartIp, u4EndIp)
                != SNMP_SUCCESS)
            {
                u4Count++;
                nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                      u4StartIp, DESTROY);
                continue;
            }

            if (nmhSetDhcpSrvExcludeEndIpAddress (u4DhcpPoolIndex, u4StartIp,
                                                  u4EndIp) != SNMP_SUCCESS)
            {
                nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                      u4StartIp, DESTROY);
                continue;
            }

            /* Make the row status active */
            i4RetVal =
                nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                      u4StartIp, ACTIVE);
            if (i4RetVal == SNMP_FAILURE)
            {
                nmhSetDhcpSrvExcludeAddressRowStatus (u4DhcpPoolIndex,
                                                      u4StartIp, DESTROY);
                CliPrintf (CliHandle,
                           "\r%% Cannot create Exclude "
                           "pool for pool id %d\r\n", u4DhcpPoolIndex);
                continue;
            }
        }
        else
        {
            u4IpAddr = u4NextIpAddr;
        }
    }
    while (nmhGetNextIndexDhcpSrvExcludeIpAddressTable
           (u4DhcpPoolIndex, &i4NextDhcpPoolIndex, u4IpAddr,
            &u4NextIpAddr) != SNMP_FAILURE);

    if (u4Count == u4PoolCount)
    {
        CliPrintf (CliHandle, "\r%%Invalid Exclude Address for DHCP Pool \r\n");
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelExcludeAddrPoolAll                    */
/*                                                                            */
/* Description       : This function deletes the Exclude address Pool         */
/*                     table information for all the Subnet Pool indices      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4StartIp       - Exclude address start IP             */
/*                     u4EndIp         - Exclude address End IP               */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelExcludeAddrPoolAll (tCliHandle CliHandle, UINT4 u4StartIp,
                                     UINT4 u4EndIp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4ExclPoolEndIp;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4DhcpPoolIndex = 0;
    INT4                i4NextDhcpPoolIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NextIpAddr = 0;
    UINT4               u4Count = 0;
    UINT4               u4PoolCount = 0;

    if (nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (&i4NextDhcpPoolIndex)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% DHCP Pool Not Configured\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetFirstIndexDhcpSrvExcludeIpAddressTable (&i4NextDhcpPoolIndex,
                                                      &u4IpAddr)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Exclude IP Address\r\n");
        return CLI_SUCCESS;
    }

    u4NextIpAddr = u4IpAddr;

    do
    {
        u4DhcpPoolIndex = i4NextDhcpPoolIndex;
        u4IpAddr = u4NextIpAddr;
        u4PoolCount++;
        if (u4NextIpAddr != u4StartIp)
        {
            u4Count++;
            continue;
        }

        i4RetVal =
            nmhTestv2DhcpSrvExcludeAddressRowStatus (&u4ErrorCode,
                                                     u4DhcpPoolIndex,
                                                     u4StartIp, DESTROY);
        if (i4RetVal == SNMP_FAILURE)
        {
            continue;
        }

        /* If the EndIp is a valid ucast_addr then check for the exact endIp from the table 
         * and if it matches, then allow to delete the entry from exclude address pool
         */
        if (u4EndIp != DHCP_POOL_DEF_ENDIP)
        {
            nmhGetDhcpSrvExcludeEndIpAddress (u4DhcpPoolIndex, u4StartIp,
                                              &u4ExclPoolEndIp);
            if (u4EndIp != u4ExclPoolEndIp)
            {
                continue;
            }
        }
        if (nmhSetDhcpSrvExcludeAddressRowStatus
            (u4DhcpPoolIndex, u4StartIp, DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot remove Exclude pool for pool id %d\r\n",
                       u4DhcpPoolIndex);
        }
    }
    while (nmhGetNextIndexDhcpSrvExcludeIpAddressTable
           (u4DhcpPoolIndex, &i4NextDhcpPoolIndex, u4IpAddr,
            &u4NextIpAddr) == SNMP_SUCCESS);
    if (u4Count == u4PoolCount)
    {
        CliPrintf (CliHandle, "\r%% Invalid Exclude IP Address\r\n");
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetLeaseTime                             */
/*                                                                            */
/* Description       : This function sets the lease time for the given        */
/*                     subnet pool index                                      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Subnet Pool index               */
/*                     i4LeaseTime     - Lease time for the Pool              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetLeaseTime (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                            INT4 i4LeaseTime)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    BOOL1               bIsStatusChanged = FALSE;

    /* check for existing row */
    i4RetVal = nmhGetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, &i4RowStatus);

    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        /* if row already existed, then change the row status 
         * to Not_in_Service */

        nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, NOT_IN_SERVICE);
        bIsStatusChanged = TRUE;
    }

    /* Value check for Lease Time */
    if (nmhTestv2DhcpSrvSubnetLeaseTime
        (&u4ErrorCode, u4DhcpPoolIndex, i4LeaseTime) == SNMP_SUCCESS)
    {
        nmhSetDhcpSrvSubnetLeaseTime (u4DhcpPoolIndex, i4LeaseTime);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        i4RetVal = CLI_FAILURE;
    }

    if (bIsStatusChanged == TRUE)
    {
        nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, ACTIVE);
    }
    UNUSED_PARAM (CliHandle);
    return i4RetVal;
}

/******************************************************************************/
/* Function Name     :  DhcpSrvProcessSetPoolThreshold                        */
/*                                                                            */
/* Description       : This function sets the lease time for the given        */
/*                     subnet pool index                                      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Subnet Pool index               */
/*                     i4ThresholdLevel - DHCP pool threshold level           */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetPoolThreshold (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                                INT4 i4ThresholdLevel)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    BOOL1               bIsStatusChanged = FALSE;

    /* check for existing row */
    i4RetVal = nmhGetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, &i4RowStatus);

    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        /* if row already existed, then change the row status 
         * to Not_in_Service */

        nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, NOT_IN_SERVICE);
        bIsStatusChanged = TRUE;
    }

    /* Value check for Lease Time */
    if (nmhTestv2DhcpSrvSubnetUtlThreshold (&u4ErrorCode,
                                            (INT4) u4DhcpPoolIndex,
                                            i4ThresholdLevel) == SNMP_SUCCESS)
    {
        nmhSetDhcpSrvSubnetUtlThreshold (u4DhcpPoolIndex, i4ThresholdLevel);
        i4RetVal = CLI_SUCCESS;
    }
    else
    {
        i4RetVal = CLI_FAILURE;
    }

    if (bIsStatusChanged == TRUE)
    {
        nmhSetDhcpSrvSubnetPoolRowStatus (u4DhcpPoolIndex, ACTIVE);
    }
    UNUSED_PARAM (CliHandle);
    return i4RetVal;

}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessShowConfig                               */
/*                                                                            */
/* Description       : This function displays the DHCP Server Configuration   */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS                                            */
/******************************************************************************/

INT4
DhcpSrvProcessShowConfig (tCliHandle CliHandle)
{
    INT4                i4Status;
    INT4                i4IcmpEcho;
    INT4                i4TrcLevel;
    UINT4               u4AddrReUseTimeOut;
    UINT4               u4NextSrvAddr;
    UINT1               au1BootFileName[DHCP_MAX_NAME_LEN];
    CHR1               *pu1IpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE BootFile;

    MEMSET (au1BootFileName, 0, DHCP_MAX_NAME_LEN);
    BootFile.pu1_OctetList = au1BootFileName;

    nmhGetDhcpSrvEnable (&i4Status);
    nmhGetDhcpSrvIcmpEchoEnable (&i4IcmpEcho);
    nmhGetDhcpSrvDebugLevel (&i4TrcLevel);
    nmhGetDhcpSrvOfferReuseTimeOut (&u4AddrReUseTimeOut);
    nmhGetDhcpSrvBootServerAddress (&u4NextSrvAddr);
    nmhGetDhcpSrvDefBootFilename (&BootFile);

    CliPrintf (CliHandle, "\r\n%-35s: %s\r\n", "DHCP server status",
               ((i4Status == DHCP_ENABLED) ? "Enable" : "Disable"));

    CliPrintf (CliHandle, "%-35s: %s\r\n", "Send Ping Packets",
               ((i4IcmpEcho == 1) ? "Enable" : "Disable"));

    CliPrintf (CliHandle, "%-35s: ", "Debug level");

    if (i4TrcLevel == (INT4) DEBUG_DHCPSRV_ALL)
    {
        CliPrintf (CliHandle, "All");
    }
    else
    {
        if ((i4TrcLevel & DEBUG_DHCPSRV_EVENTS) == DEBUG_DHCPSRV_EVENTS)
        {
            CliPrintf (CliHandle, "Events ");
        }
        if ((i4TrcLevel & DEBUG_DHCPSRV_PACKETS) == DEBUG_DHCPSRV_PACKETS)
        {
            CliPrintf (CliHandle, "Packets ");
        }
        if ((i4TrcLevel & DEBUG_DHCPSRV_ERRORS) == DEBUG_DHCPSRV_ERRORS)
        {
            CliPrintf (CliHandle, "Errors ");
        }
        if ((i4TrcLevel & DEBUG_DHCPSRV_BIND) == DEBUG_DHCPSRV_BIND)
        {
            CliPrintf (CliHandle, "Bind ");
        }
        if (!(i4TrcLevel & (DEBUG_DHCPSRV_EVENTS | DEBUG_DHCPSRV_PACKETS |
                            DEBUG_DHCPSRV_ERRORS | DEBUG_DHCPSRV_BIND)))
        {
            CliPrintf (CliHandle, "None");
        }
    }

    CliPrintf (CliHandle, "\r\n%-35s: %u secs\r\n",
               "Server Address Reuse Timeout", u4AddrReUseTimeOut);

    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4NextSrvAddr);

    CliPrintf (CliHandle, "%-35s: %s\r\n", "Next Server Address", pu1IpAddr);
    CliPrintf (CliHandle, "%-35s: %s\r\n\r\n", "Boot file name",
               BootFile.pu1_OctetList);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessAddHostConfig                            */
/*                                                                            */
/* Description       : This function Configures the DHCP Server Host-IP       */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4HostType      - Host Type                            */
/*                     pu1HostId       - Host ID                              */
/*                     u4DhcpPoolIndex - DHCP Pool Index                      */
/*                     u4IpAddress     - Host IpAddress                       */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessAddHostIpConfig (tCliHandle CliHandle, UINT4 u4HostType,
                               UINT1 *pu1HostId, UINT4 u4DhcpPoolIndex,
                               UINT4 u4IpAddress)
{

    tSNMP_OCTET_STRING_TYPE OctetClientId;
    UINT1               au1ClientId[CLI_MAC_ADDR_SIZE];
    UINT1               u1NewRowStatus = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    OctetClientId.pu1_OctetList = au1ClientId;
    CLI_CONVERT_DOT_STR_TO_MAC (pu1HostId, OctetClientId.pu1_OctetList);

    /* Client Id should be MAC Address */
    OctetClientId.i4_Length = CLI_MAC_ADDR_SIZE;

    i4RetVal = nmhGetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                                 u4DhcpPoolIndex, &i4RowStatus);

    u1NewRowStatus =
        (i4RetVal == SNMP_SUCCESS) ? NOT_IN_SERVICE : CREATE_AND_WAIT;

    if (nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode, u4HostType,
                                             &OctetClientId, u4DhcpPoolIndex,
                                             u1NewRowStatus) == SNMP_FAILURE)
    {
        if (u1NewRowStatus == NOT_IN_SERVICE)
        {
            CliPrintf (CliHandle,
                       "\r%% Host configuration entry modification failed\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% Host configuration entry creation failed\r\n");
        }
        return CLI_FAILURE;
    }

    if (nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex,
                                          u1NewRowStatus) == SNMP_FAILURE)
    {
        if (u1NewRowStatus == NOT_IN_SERVICE)
        {
            CliPrintf (CliHandle,
                       "\r%% Host configuration entry modification failed\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% Host configuration entry creation failed\r\n");
        }
        return CLI_FAILURE;
    }

    /*Row status for restoration of the row in configuration failure cases */
    u1NewRowStatus = (i4RetVal == SNMP_SUCCESS) ? (UINT1) i4RowStatus : DESTROY;

    if (nmhTestv2DhcpSrvHostIpAddress (&u4ErrorCode, u4HostType,
                                       &OctetClientId, u4DhcpPoolIndex,
                                       u4IpAddress) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, u1NewRowStatus);
        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle,
                       "\r%% Wrong Value or IP address Already allocated."
                       "Release that allocation first\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% IP address is already mapped for an existing context\r\n");
            CliPrintf (CliHandle,
                       "\r%% Configuring IP address to host failed\r\n");
        }
        return CLI_FAILURE;
    }

    if (nmhSetDhcpSrvHostIpAddress (u4HostType, &OctetClientId, u4DhcpPoolIndex,
                                    u4IpAddress) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, u1NewRowStatus);
        CliPrintf (CliHandle, "\r%% Configuring IP address to host failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode, u4HostType,
                                             &OctetClientId,
                                             u4DhcpPoolIndex,
                                             ACTIVE) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, u1NewRowStatus);
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry creation failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, u1NewRowStatus);
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry creation failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelHostIpConfig                          */
/*                                                                            */
/* Description       : This function Deletes the DHCP Server Host-IP          */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4HostType      - Host Type                            */
/*                     pu1HostId       - Host ID                              */
/*                     u4DhcpPoolIndex - DHCP Pool Index                      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelHostIpConfig (tCliHandle CliHandle, UINT4 u4HostType,
                               UINT1 *pu1HostId, UINT4 u4DhcpPoolIndex)
{

    tSNMP_OCTET_STRING_TYPE OctetClientId;
    UINT1               au1ClientId[CLI_MAC_ADDR_SIZE];
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    OctetClientId.pu1_OctetList = au1ClientId;
    CLI_CONVERT_DOT_STR_TO_MAC (pu1HostId, OctetClientId.pu1_OctetList);

    /* Client Id should be MAC Address */
    OctetClientId.i4_Length = CLI_MAC_ADDR_SIZE;

    i4RetVal = nmhGetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                                 u4DhcpPoolIndex, &i4RowStatus);

    /* Scenario when the row already exists */
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Host configuration deletion failed. Entry does not exist \r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode, u4HostType,
                                             &OctetClientId,
                                             u4DhcpPoolIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry deletion failed\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry deletion failed\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetDhcpSrvHostIpAddress (u4HostType, &OctetClientId, u4DhcpPoolIndex,
                                    0) == SNMP_FAILURE)
    {
        /*Restore the row status to i4RowStatus */
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, i4RowStatus);
        CliPrintf (CliHandle,
                   "\r%% Configuring 0 IP address to host failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode, u4HostType,
                                             &OctetClientId,
                                             u4DhcpPoolIndex,
                                             ACTIVE) == SNMP_FAILURE)
    {
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, i4RowStatus);
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry creation failed\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, ACTIVE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry creation failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelHostConfig                            */
/*                                                                            */
/* Description       : This function Deletes DHCP Server Host configuration   */
/*                     Entry                                                  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4HostType      - Host Type                            */
/*                     pu1HostId       - Host ID                              */
/*                     u4DhcpPoolIndex - DHCP Pool Index                      */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelHostConfig (tCliHandle CliHandle, UINT4 u4HostType,
                             UINT1 *pu1HostId, UINT4 u4DhcpPoolIndex)
{

    tSNMP_OCTET_STRING_TYPE OctetClientId;
    UINT1               au1ClientId[DHCP_MAX_CID_LEN];
    UINT4               u4ErrorCode = 0;

    OctetClientId.pu1_OctetList = au1ClientId;
    CLI_CONVERT_DOT_STR_TO_MAC (pu1HostId, OctetClientId.pu1_OctetList);

    /* Client Id should be MAC Address */
    OctetClientId.i4_Length = CLI_MAC_ADDR_SIZE;

    if (nmhTestv2DhcpSrvHostConfigRowStatus (&u4ErrorCode, u4HostType,
                                             &OctetClientId, u4DhcpPoolIndex,
                                             DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry deletion failed\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, DESTROY) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Host configuration entry deletion failed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessAddHostOption                            */
/*                                                                            */
/* Description       : This function Configures the DHCP Server Host Options  */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Pool Index                      */
/*                     u4HostType      - Host Type                            */
/*                     pu1HostId       - Host ID                              */
/*                     u4Type          - Host Option Code                     */
/*                     pu1OptValue     - Host Option Value                    */
/*                     u4Len           - Host Option Length                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessAddHostOption (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                             UINT4 u4HostType, UINT1 *pu1HostId, UINT4 u4Type,
                             UINT1 *pu1OptValue, UINT4 u4Len)
{
    UINT1               bIsFirstCreated = FALSE;
    UINT1               bIsSecondCreated = FALSE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               au1HostOptn[DHCP_MAX_OPT_LEN];
    UINT1               au1ClientId[DHCP_MAX_CID_LEN];
    tSNMP_OCTET_STRING_TYPE OctetValue;
    tSNMP_OCTET_STRING_TYPE OctetClientId;

    OctetValue.pu1_OctetList = au1HostOptn;

    /* Validate DHCP option and convert the option value to Octet string */

    i4RetVal = DhcpSrvProcessValidateOption (CliHandle, (UINT1) u4Type,
                                             &u4Len, pu1OptValue, TRUE,
                                             OctetValue.pu1_OctetList);

    if (i4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    OctetValue.i4_Length = u4Len;

    /* Process client Id */
    OctetClientId.pu1_OctetList = au1ClientId;

    if ((u4HostType != 0))
    {
        /* Client ID can be a MAC address.The format is 00:00:00:00:00:02 .
         * The length of the string should be reduced by 5 or 3 ( 5 or 3 dots )
         * Validation done in CLI framework
         */

        CLI_CONVERT_DOT_STR_TO_MAC (pu1HostId, OctetClientId.pu1_OctetList);

        /* Client Id should be MAC Address */
        OctetClientId.i4_Length = CLI_MAC_ADDR_SIZE;
    }
    else
    {
        OctetClientId.i4_Length = STRLEN (pu1HostId);

        MEMCPY (OctetClientId.pu1_OctetList, pu1HostId,
                OctetClientId.i4_Length);
    }

    /* Before configuring host options, check whether host configuration
     * already exists 
     */

    i4RetVal = nmhGetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                                 u4DhcpPoolIndex, &i4RowStatus);

    /* Scenario when the row already exists */
    if (i4RetVal == SNMP_SUCCESS)
    {
        nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                          u4DhcpPoolIndex, NOT_IN_SERVICE);

    }
    else
    {
        /* Scenario when row is not existed */
        if (nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                              u4DhcpPoolIndex,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            CliPrintf (CliHandle, "\r%% Host configuration table full\r\n");
            return CLI_FAILURE;
        }
        bIsFirstCreated = TRUE;
    }

    /* check for host option existing row , if already created */

    i4RetVal = nmhGetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId,
                                              u4DhcpPoolIndex,
                                              u4Type, &i4RowStatus);

    /* Scenario when the row is existed */
    if (i4RetVal == SNMP_SUCCESS)
    {
        if (i4RowStatus == ACTIVE)
        {
            nmhSetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId,
                                           u4DhcpPoolIndex, u4Type,
                                           NOT_IN_SERVICE);
        }
        bIsSecondCreated = FALSE;
    }
    else
    {
        /* Create  host option row  */
        i4RetVal = nmhSetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId,
                                                  u4DhcpPoolIndex, u4Type,
                                                  CREATE_AND_WAIT);
        if (i4RetVal == SNMP_FAILURE)
        {
            if (bIsFirstCreated)
            {
                /* Delete the configuration host table */
                nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                                  u4DhcpPoolIndex, DESTROY);
            }
            CliPrintf (CliHandle, "\r%% Host Option Table is full\r\n");
            return CLI_FAILURE;
        }
        bIsSecondCreated = TRUE;
    }
    i4RetVal =
        nmhTestv2DhcpSrvHostOptLen (&u4ErrorCode, u4HostType, &OctetClientId,
                                    u4DhcpPoolIndex, u4Type, u4Len);
    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsSecondCreated)
        {
            nmhSetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId,
                                           u4DhcpPoolIndex, u4Type, DESTROY);
        }

        if (bIsFirstCreated)
        {
            nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                              u4DhcpPoolIndex, DESTROY);
        }
        return CLI_FAILURE;
    }

    i4RetVal =
        nmhTestv2DhcpSrvHostOptVal (&u4ErrorCode, u4HostType, &OctetClientId,
                                    u4DhcpPoolIndex, u4Type, &OctetValue);
    if (i4RetVal == SNMP_FAILURE)
    {
        if (bIsSecondCreated)
        {
            nmhSetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId,
                                           u4DhcpPoolIndex, u4Type, DESTROY);
        }

        if (bIsFirstCreated)
        {
            nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                              u4DhcpPoolIndex, DESTROY);
        }
        return CLI_FAILURE;
    }
    else
    {
        nmhSetDhcpSrvHostOptLen (u4HostType, &OctetClientId, u4DhcpPoolIndex,
                                 u4Type, u4Len);

        nmhSetDhcpSrvHostOptVal (u4HostType, &OctetClientId, u4DhcpPoolIndex,
                                 u4Type, &OctetValue);
    }

    i4RetVal = nmhSetDhcpSrvHostConfigRowStatus (u4HostType, &OctetClientId,
                                                 u4DhcpPoolIndex, ACTIVE);
    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    i4RetVal = nmhSetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId,
                                              u4DhcpPoolIndex, u4Type, ACTIVE);

    if (i4RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessDelHostOption                            */
/*                                                                            */
/* Description       : This function Deletes the DHCP Server Host Options     */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     u4DhcpPoolIndex - DHCP Pool Index                      */
/*                     u4HostType      - Host Type                            */
/*                     pu1HostId       - Host ID                              */
/*                     u4Type          - Host Option Code                     */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessDelHostOption (tCliHandle CliHandle, UINT4 u4DhcpPoolIndex,
                             UINT4 u4HostType, UINT1 *pu1HostId, UINT4 u4Type)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE OctetClientId;
    UINT1               au1ClientId[DHCP_MAX_CID_LEN];

    /* Process client Id */
    MEMSET (au1ClientId, 0, DHCP_MAX_CID_LEN);
    OctetClientId.pu1_OctetList = au1ClientId;

    if (u4HostType != 0)
    {
        /* Client ID can be a MAC address.The format is 00:00:00:00:00:02 .
         * The length of the string should be reduced by 5 or 3 ( 5 or 3 dots )
         * Validation done in CLI framework
         */

        CLI_CONVERT_DOT_STR_TO_MAC (pu1HostId, OctetClientId.pu1_OctetList);

        /* Client Id should be MAC Address */
        OctetClientId.i4_Length = CLI_MAC_ADDR_SIZE;

    }
    else
    {
        OctetClientId.i4_Length = (INT4) STRLEN (pu1HostId);

        MEMCPY (OctetClientId.pu1_OctetList, pu1HostId,
                OctetClientId.i4_Length);
    }

    i4RetVal =
        nmhTestv2DhcpSrvHostOptRowStatus (&u4ErrorCode, u4HostType,
                                          &OctetClientId, u4DhcpPoolIndex,
                                          u4Type, DESTROY);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetDhcpSrvHostOptRowStatus (u4HostType, &OctetClientId, u4DhcpPoolIndex,
                                   u4Type, DESTROY);

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessShowBinds                                */
/*                                                                            */
/* Description       : This function Displays the DHCP Server Binding info.   */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS                                            */
/******************************************************************************/

INT4
DhcpSrvProcessShowBinds (tCliHandle CliHandle)
{
    UINT4               u4FirstBindIpAddr = 0;
    UINT4               u4NextBindIpAddr = 0;
    INT1                i1Status = DHCP_FAILURE;
    INT4                i4HwType = 0;
    INT4                i4ExpireTime = 0;
    INT4                i4AllocMethod = 0;
    INT4                i4BindState = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4PageStatus = CLI_SUCCESS;
    CHR1               *pu1IpAddr = NULL;
    UINT1               au1HwAddr[DHCP_MAX_CID_LEN];
    UINT1               au1Str[HW_ADDR_STR_LEN];
    tSNMP_OCTET_STRING_TYPE BindHwAddr;
    UINT1               au1TimeStr[MAX_DATE_LEN];

    i4RetVal = nmhGetFirstIndexDhcpSrvBindingTable (&u4FirstBindIpAddr);

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    BindHwAddr.pu1_OctetList = au1HwAddr;

    u4NextBindIpAddr = u4FirstBindIpAddr;

    do
    {
        u4FirstBindIpAddr = u4NextBindIpAddr;

        nmhGetDhcpSrvBindHwType (u4FirstBindIpAddr, &i4HwType);

        MEMSET (BindHwAddr.pu1_OctetList, 0, DHCP_MAX_CID_LEN);

        nmhGetDhcpSrvBindHwAddress (u4FirstBindIpAddr, &BindHwAddr);

        nmhGetDhcpSrvBindExpireTime (u4FirstBindIpAddr, &i4ExpireTime);

        nmhGetDhcpSrvBindAllocMethod (u4FirstBindIpAddr, &i4AllocMethod);

        nmhGetDhcpSrvBindState (u4FirstBindIpAddr, &i4BindState);

        MEMSET (au1TimeStr, 0, MAX_DATE_LEN);

        DhcpSrvGetExpiryTime (au1TimeStr, i4ExpireTime);

        if (i1Status == DHCP_FAILURE)    /*print only once */
        {
            CliPrintf (CliHandle, "\r\n%-15s %-10s %-18s %-9s %-12s\r\n",
                       "Ip", "Hw", "Hw", "Binding", "Expire");

            CliPrintf (CliHandle, "%-15s %-10s %-18s %-9s %-12s\r\n",
                       "Address", "Type", "Address", "State", "Time");

            CliPrintf (CliHandle, "%-15s %-10s %-18s %-9s %-12s\r\n",
                       "-------", "-------", "------", "--------",
                       "----------");
            i1Status = DHCP_SUCCESS;
        }
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4FirstBindIpAddr);

        CliPrintf (CliHandle, "%-15s ", pu1IpAddr);

        CliPrintf (CliHandle, "%-10s ",
                   ((i4HwType == 0) ? "ClientId" : "Ethernet"));

        CLI_CONVERT_MAC_TO_DOT_STR (BindHwAddr.pu1_OctetList, au1Str);

        CliPrintf (CliHandle, "%-18s ", au1Str);

        if (i4BindState == DHCPSRV_BIND_OFFERD)
        {
            CliPrintf (CliHandle, "%-9s ", "Offered");
        }
        else if (i4BindState == DHCPSRV_BIND_ASSIGND)
        {
            CliPrintf (CliHandle, "%-9s ", "Assigned");
        }
        else
        {
            CliPrintf (CliHandle, "%-9s ", "Probing");
        }

        CliPrintf (CliHandle, "%-12s ", au1TimeStr);

        i4PageStatus = CliPrintf (CliHandle, "\r\n");

        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while (nmhGetNextIndexDhcpSrvBindingTable
           (u4FirstBindIpAddr, &u4NextBindIpAddr) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : DhcpCliGetAddrPoolCount                                */
/*                                                                            */
/* Description       : This function is used to get the number of             */
/*                     DHCP Subnet Pools                                      */
/*                                                                            */
/* Input Parameters  : VOID                                                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : Subnet Pool Count                                      */
/******************************************************************************/

UINT4
DhcpCliGetAddrPoolCount (VOID)
{
    INT4                i4FirstSubnetPoolIndex = 0;
    INT4                i4NextSubnetPoolIndex = 0;
    UINT4               u4PoolCount = 0;

    if (nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (&i4FirstSubnetPoolIndex)
        == SNMP_FAILURE)
    {
        return u4PoolCount;
    }

    i4NextSubnetPoolIndex = i4FirstSubnetPoolIndex;    /* reverse assignment
                                                       in loop */
    do
    {
        /* update for next entry */
        i4FirstSubnetPoolIndex = i4NextSubnetPoolIndex;
        u4PoolCount++;
    }
    while (nmhGetNextIndexDhcpSrvSubnetPoolConfigTable
           (i4FirstSubnetPoolIndex, &i4NextSubnetPoolIndex) == SNMP_SUCCESS);

    return u4PoolCount;
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessShowStats                                */
/*                                                                            */
/* Description       : This function Displays the DHCP Server Statistics      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS                                            */
/******************************************************************************/

INT4
DhcpSrvProcessShowStats (tCliHandle CliHandle)
{
    UINT4               u4Discovers = 0;
    UINT4               u4Requests = 0;
    UINT4               u4Releases = 0;
    UINT4               u4Declines = 0;
    UINT4               u4Informs = 0;
    UINT4               u4Invalids = 0;
    UINT4               u4Offers = 0;
    UINT4               u4Acks = 0;
    UINT4               u4Nacks = 0;
    UINT4               u4DropClients = 0;
    UINT4               u4DropSubnet = 0;
    UINT4               u4AddressPoolCount;

    u4AddressPoolCount = DhcpCliGetAddrPoolCount ();

    nmhGetDhcpCountDiscovers (&u4Discovers);

    nmhGetDhcpCountRequests (&u4Requests);

    nmhGetDhcpCountReleases (&u4Releases);

    nmhGetDhcpCountDeclines (&u4Declines);

    nmhGetDhcpCountInforms (&u4Informs);

    nmhGetDhcpCountInvalids (&u4Invalids);

    nmhGetDhcpCountOffers (&u4Offers);

    nmhGetDhcpCountAcks (&u4Acks);

    nmhGetDhcpCountNacks (&u4Nacks);

    nmhGetDhcpCountDroppedUnknownClient (&u4DropClients);

    nmhGetDhcpCountDroppedNotServingSubnet (&u4DropSubnet);

    CliPrintf (CliHandle, "\r\nAddress pools : %d\r\n", u4AddressPoolCount);

    CliPrintf (CliHandle, "\r\nMessage                Received\r\n");
    CliPrintf (CliHandle, "-------                --------\r\n");

    CliPrintf (CliHandle, "DHCPDISCOVER           %d\r\n", u4Discovers);
    CliPrintf (CliHandle, "DHCPREQUEST            %d\r\n", u4Requests);
    CliPrintf (CliHandle, "DHCPDECLINE            %d\r\n", u4Declines);
    CliPrintf (CliHandle, "DHCPRELEASE            %d\r\n", u4Releases);
    CliPrintf (CliHandle, "DHCPINFORM             %d\r\n", u4Informs);

    CliPrintf (CliHandle, "\r\nMessage                Sent\r\n");
    CliPrintf (CliHandle, "-------                ----\r\n");

    CliPrintf (CliHandle, "DHCPOFFER              %d\r\n", u4Offers);
    CliPrintf (CliHandle, "DHCPACK                %d\r\n", u4Acks);
    CliPrintf (CliHandle, "DHCPNAK                %d\r\n\r\n", u4Nacks);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpSrvShowRunningConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        DHCP server                                        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DhcpSrvShowRunningConfig (tCliHandle CliHandle)
{
    CliRegisterLock (CliHandle, DhcpSProtocolLock, DhcpSProtocolUnLock);
    DHCPS_PROTO_LOCK ();

    DhcpSrvShowRunningConfigScalar (CliHandle);
    DhcpSrvShowRunningConfigTable (CliHandle);

    DHCPS_PROTO_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpSrvShowRunningConfigScalar                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        for DHCP server scalars                            */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DhcpSrvShowRunningConfigScalar (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;
    UINT4               u4RetVal = 0;
    UINT1               au1BootFileName[DHCP_MAX_NAME_LEN];
    CHR1               *pu1IpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE BootFile;

    MEMSET (au1BootFileName, 0, DHCP_MAX_NAME_LEN);
    BootFile.pu1_OctetList = au1BootFileName;

    nmhGetDhcpSrvEnable (&i4RetVal);

    if (i4RetVal != DHCP_DEF_ENABLE)
    {
        CliPrintf (CliHandle, "service dhcp-server\r\n");
    }

    nmhGetDhcpSrvOfferReuseTimeOut (&u4RetVal);

    if (u4RetVal != DHCP_DEF_OFFER_REUSE_TIME)
    {
        CliPrintf (CliHandle, "ip dhcp server offer-reuse %d\r\n", u4RetVal);
    }

    nmhGetDhcpSrvIcmpEchoEnable (&i4RetVal);

    if (i4RetVal != DHCP_DEF_ENABLE)
    {
        CliPrintf (CliHandle, "ip dhcp ping packets\r\n");
    }

    nmhGetDhcpSrvBootServerAddress (&u4RetVal);

    if (u4RetVal != DHCPS_DEF_BOOTSRV_IP)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RetVal);
        CliPrintf (CliHandle, "ip dhcp next-server %s\r\n", pu1IpAddr);
    }

    nmhGetDhcpSrvDefBootFilename (&BootFile);

    if (STRCMP (BootFile.pu1_OctetList, DHCP_DEF_BOOT_FILE) != 0)
    {
        CliPrintf (CliHandle, "ip dhcp bootfile %s\r\n",
                   BootFile.pu1_OctetList);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpSrvShowRunningConfigTable                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current configuration   */
/*                        for DHCP server tables                             */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DhcpSrvShowRunningConfigTable (tCliHandle CliHandle)
{
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;
    INT4                i4FirstSubnetPoolIndex = 0;
    INT4                i4NextSubnetPoolIndex = 0;
    INT4                i4TempFirstPoolIndex = 0;
    INT4                i4TempNextPoolIndex = 0;
    INT4                i4FirstOptType = 0;
    INT4                i4NextOptType = 0;
    INT4                i4FirstHostType = 0;
    INT4                i4NextHostType = 0;
    INT1                i1RetVal = 0;

    UINT4               u4OptionLen = 0;
    UINT4               u4SubnetSubnet = 0;
    UINT4               u4SubnetMask = 0;
    UINT4               u4EndIpAddr = 0;
    UINT4               u4StartIpAddr = 0;
    UINT4               u4LeaseTime = 0;
    UINT4               u4Threshold = 0;
    UINT4               u4FirstExStartIp = 0;
    UINT4               u4NextExStartIp = 0;
    UINT4               u4ExEndIp = 0;
    UINT4               u4Length = 0;
    UINT4               u4NodeType = 0;
    UINT4               u4DefaultEndIp = 0;
    UINT4               u4SubnetOptVal = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4Cmd = 0;

    tDhcpOptions       *pOptions = NULL;
    tDhcpPool          *pEntry = NULL;
    tExcludeIpAddr     *pExclude = NULL;

    CHR1               *pu1IpAddr = NULL;

    UINT1               au1OptValue[DHCP_MAX_OPT_LEN];
    UINT1               au1MacStr[DHCP_MAX_CID_LEN];
    UINT1               au1OptionStr[DHCP_MAX_OPT_LEN];
    CHR1               *au1StrSubOpn = NULL;

    UINT1               au1FirstHostClientMac[DHCP_MAX_CID_LEN];
    UINT1               au1NextHostClientMac[DHCP_MAX_CID_LEN];
    UINT1               au1PoolName[(DHCP_MAX_NAME_LEN + 1)];

    tSNMP_OCTET_STRING_TYPE FirstHostId;
    tSNMP_OCTET_STRING_TYPE NextHostId;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    tSNMP_OCTET_STRING_TYPE PoolName;

    MEMSET (au1FirstHostClientMac, 0, DHCP_MAX_CID_LEN);
    MEMSET (au1NextHostClientMac, 0, DHCP_MAX_CID_LEN);
    MEMSET (au1OptValue, 0, DHCP_MAX_OPT_LEN);
    MEMSET (au1MacStr, 0, DHCP_MAX_CID_LEN);
    MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN);
    MEMSET (au1PoolName, 0, DHCP_MAX_NAME_LEN);
    MEMSET (au1DnsStrOpt, 0, DHCP_MAX_OPT_LEN);

    FirstHostId.pu1_OctetList = au1FirstHostClientMac;
    NextHostId.pu1_OctetList = au1NextHostClientMac;
    OptionValue.pu1_OctetList = au1OptValue;
    PoolName.pu1_OctetList = au1PoolName;

    if (nmhGetFirstIndexDhcpSrvGblOptTable (&i4FirstOptType) == SNMP_SUCCESS)
    {
        i4NextOptType = i4FirstOptType;
        do
        {
            i4FirstOptType = i4NextOptType;

            nmhGetDhcpSrvGblOptRowStatus (i4FirstOptType, &i4RetVal);

            if (i4RetVal == ACTIVE)
            {
                nmhGetDhcpSrvGblOptLen (i4FirstOptType, (INT4 *) &u4Length);

                MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);

                nmhGetDhcpSrvGblOptVal (i4FirstOptType, &OptionValue);

                MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN);

                pOptions =
                    DhcpGetOptionFromList (gpDhcpOptions,
                                           (UINT4) i4FirstOptType);

                if (pOptions == NULL)
                {
                    return SNMP_FAILURE;
                }

                u4Cmd = pOptions->u4CmdType;
                DhcpSrvProcessValidateOption (CliHandle,
                                              (UINT1) i4FirstOptType,
                                              &u4Length,
                                              OptionValue.pu1_OctetList,
                                              FALSE, au1OptionStr);

                CliPrintf (CliHandle, "ip dhcp option %d", i4FirstOptType);

                DhcpSrvDisplayOptionType (CliHandle, i4FirstOptType,
                                          au1OptionStr, u4Cmd);

            }
        }
        while (nmhGetNextIndexDhcpSrvGblOptTable
               (i4FirstOptType, &i4NextOptType) == SNMP_SUCCESS);
    }

    if (nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable
        (&i4FirstSubnetPoolIndex) == SNMP_SUCCESS)
    {
        i4NextSubnetPoolIndex = i4FirstSubnetPoolIndex;
        do
        {
            /* update for next entry */
            i4FirstSubnetPoolIndex = i4NextSubnetPoolIndex;

            CliPrintf (CliHandle, "ip dhcp pool %d", i4FirstSubnetPoolIndex);

            /* get the configuration parameters for each pool */
            i1RetVal = nmhGetDhcpSrvSubnetPoolRowStatus (i4FirstSubnetPoolIndex,
                                                         &i4RetVal);
            nmhGetDhcpSrvSubnetPoolName (i4FirstSubnetPoolIndex, &PoolName);
            if (PoolName.i4_Length != 0)
            {
                CliPrintf (CliHandle, " %s\r\n", PoolName.pu1_OctetList);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            UNUSED_PARAM (i1RetVal);
            if (i4RetVal == ACTIVE)
            {
                nmhGetDhcpSrvSubnetSubnet (i4FirstSubnetPoolIndex,
                                           &u4SubnetSubnet);
                nmhGetDhcpSrvSubnetMask (i4FirstSubnetPoolIndex, &u4SubnetMask);
                nmhGetDhcpSrvSubnetLeaseTime (i4FirstSubnetPoolIndex,
                                              (INT4 *) &u4LeaseTime);
                nmhGetDhcpSrvSubnetUtlThreshold (i4FirstSubnetPoolIndex,
                                                 (INT4 *) &u4Threshold);
                nmhGetDhcpSrvSubnetEndIpAddress (i4FirstSubnetPoolIndex,
                                                 &u4EndIpAddr);
                nmhGetDhcpSrvSubnetStartIpAddress
                    (i4FirstSubnetPoolIndex, &u4StartIpAddr);
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4StartIpAddr);

                CliPrintf (CliHandle, "  network %s", pu1IpAddr);

                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4SubnetMask);
                CliPrintf (CliHandle, " %s", pu1IpAddr);

                u4DefaultEndIp = (u4SubnetSubnet & u4SubnetMask) +
                    (0xffffffff & (~u4SubnetMask));

                if (u4EndIpAddr != u4DefaultEndIp)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4EndIpAddr);

                    CliPrintf (CliHandle, " %s", pu1IpAddr);
                }
                CliPrintf (CliHandle, "\r\n");

                if (u4LeaseTime == 0x7fffffff)
                {
                    CliPrintf (CliHandle, "  lease infinite\r\n");
                }
                else if (u4LeaseTime != DHCP_DEF_LEASE_TIME)
                {
                    CliPrintf (CliHandle, "  lease %d",
                               u4LeaseTime / (24 * 60 * 60));

                    u4LeaseTime = u4LeaseTime % (24 * 60 * 60);

                    if (u4LeaseTime > 0)
                        CliPrintf (CliHandle, " %d", u4LeaseTime / (60 * 60));

                    u4LeaseTime = u4LeaseTime % (60 * 60);

                    if (u4LeaseTime > 0)
                        CliPrintf (CliHandle, " %d", u4LeaseTime / 60);
                    CliPrintf (CliHandle, "\r\n");
                }
                if (u4Threshold != DHCPS_DEF_POOLTHRESHOLD)
                {
                    CliPrintf (CliHandle, "  utilization threshold %d\r\n",
                               u4Threshold);
                }
            }

            if (nmhGetFirstIndexDhcpSrvExcludeIpAddressTable
                (&i4TempFirstPoolIndex, &u4FirstExStartIp) == SNMP_SUCCESS)
            {
                i4TempNextPoolIndex = i4TempFirstPoolIndex;

                u4NextExStartIp = u4FirstExStartIp;

                do
                {
                    i4TempFirstPoolIndex = i4TempNextPoolIndex;

                    u4FirstExStartIp = u4NextExStartIp;

                    if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                    {
                        /* Print Exclude address pool info of current Pool Id */
                        continue;
                    }

                    nmhGetDhcpSrvExcludeAddressRowStatus (i4TempFirstPoolIndex,
                                                          u4FirstExStartIp,
                                                          &i4RetVal);
                    if (i4RetVal == ACTIVE)
                    {
                        nmhGetDhcpSrvExcludeEndIpAddress
                            (i4TempFirstPoolIndex,
                             u4FirstExStartIp, &u4ExEndIp);

                        pEntry = DhcpGetPoolEntry (i4TempFirstPoolIndex);
                        if (pEntry != NULL)
                        {

                            for (pExclude = pEntry->pExcludeIpAddr;
                                 pExclude != NULL; pExclude = pExclude->pNext)
                            {
                                if ((u4FirstExStartIp ==
                                     pExclude->u4StartIpAddr)
                                    && (pExclude->u4Method == DHCP_SNMP))
                                {
                                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                               u4FirstExStartIp);
                                    CliPrintf (CliHandle,
                                               "  excluded-address %s",
                                               pu1IpAddr);

                                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                               u4ExEndIp);
                                    CliPrintf (CliHandle, " %s\r\n", pu1IpAddr);
                                }
                            }

                        }
                    }
                }
                while (nmhGetNextIndexDhcpSrvExcludeIpAddressTable
                       (i4TempFirstPoolIndex, &i4TempNextPoolIndex,
                        u4FirstExStartIp, &u4NextExStartIp) == SNMP_SUCCESS);
            }

            if (nmhGetFirstIndexDhcpSrvSubnetOptTable (&i4TempFirstPoolIndex,
                                                       &i4FirstOptType)
                == SNMP_SUCCESS)
            {
                i4TempNextPoolIndex = i4TempFirstPoolIndex;
                i4NextOptType = i4FirstOptType;

                do
                {
                    i4TempFirstPoolIndex = i4TempNextPoolIndex;
                    i4FirstOptType = i4NextOptType;
                    if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                    {
                        /* Print Subnet option info of current Pool Id */
                        continue;
                    }

                    nmhGetDhcpSrvSubnetOptRowStatus (i4TempFirstPoolIndex,
                                                     i4FirstOptType, &i4RetVal);
                    if (i4RetVal == ACTIVE)
                    {
                        nmhGetDhcpSrvSubnetOptLen (i4TempFirstPoolIndex,
                                                   i4FirstOptType,
                                                   (INT4 *) &u4OptionLen);

                        MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);

                        nmhGetDhcpSrvSubnetOptVal (i4TempFirstPoolIndex,
                                                   i4FirstOptType,
                                                   &OptionValue);

                        MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN);

                        MEMCPY (au1OptionStr, OptionValue.pu1_OctetList,
                                u4OptionLen);

                        pEntry =
                            DhcpGetPoolEntry ((UINT4) i4TempFirstPoolIndex);

                        if (pEntry == NULL)
                        {
                            return CLI_FAILURE;
                        }

                        pOptions =
                            DhcpGetOptionFromList (pEntry->pOptions,
                                                   (UINT4) i4FirstOptType);

                        if (pOptions == NULL)
                        {
                            return CLI_FAILURE;
                        }

                        u4Cmd = pOptions->u4CmdType;

                        /* change octet string to display string */
                        DhcpSrvProcessValidateOption (CliHandle,
                                                      (UINT1) i4FirstOptType,
                                                      &u4OptionLen,
                                                      OptionValue.pu1_OctetList,
                                                      FALSE, au1OptionStr);

                        switch (i4FirstOptType)
                        {
                            case DNSSRV_OPT:

                                CliPrintf (CliHandle, "dns-server ");
                                au1StrSubOpn = STRSTR (au1OptionStr, ",");
                                if (au1StrSubOpn != NULL)
                                {
                                    MEMCPY (au1DnsStrOpt, au1OptionStr,
                                            (STRLEN (au1OptionStr) -
                                             STRLEN (au1StrSubOpn)));
                                    CliPrintf (CliHandle, "%s ", au1DnsStrOpt);
                                    CliPrintf (CliHandle, "%s ",
                                               ++au1StrSubOpn);
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%s ", au1OptionStr);
                                }

                                break;
                            case DOMAINNAME_OPT:
                                CliPrintf (CliHandle, "  domain-name %s",
                                           au1OptionStr);
                                break;
                            case NETBIOS_NAMESRV_OPT:
                                CliPrintf (CliHandle,
                                           "  netbios-name-server %s",
                                           au1OptionStr);
                                break;
                            case NETBIOS_NODETYPE_OPT:
                                u4NodeType = ATOI (&au1OptionStr);
                                if (u4NodeType == NETBIOS_B_NODE)
                                {
                                    CliPrintf (CliHandle,
                                               "  netbios-node-type b-node\n");
                                    break;
                                }
                                if (u4NodeType == NETBIOS_H_NODE)
                                {
                                    CliPrintf (CliHandle,
                                               "  netbios-node-type h-node\n");
                                    break;
                                }
                                if (u4NodeType == NETBIOS_M_NODE)
                                {
                                    CliPrintf (CliHandle,
                                               "  netbios-node-type m-node\n");
                                    break;
                                }
                                if (u4NodeType == NETBIOS_P_NODE)
                                {
                                    CliPrintf (CliHandle,
                                               "  netbios-node-type p-node\n");
                                    break;
                                }

                                CliPrintf (CliHandle,
                                           "  netbios-node-type 0x%x\n",
                                           u4NodeType);
                                break;

                            case DEFAULT_ROUTER_OPT:
                                CliPrintf (CliHandle, "  default-router %s",
                                           au1OptionStr);
                                break;

                            default:

                                /* While configuring network address for the pool,
                                 * by default subnet mask option gets added with 
                                 * option value the same as the network mask. 
                                 * This entry is a default entry. So don't display
                                 * this entry. If user have upated this entry,
                                 * display the same.
                                 */

                                if (i4FirstOptType == SUBNET_MASK_OPT)
                                {
                                    MEMCPY ((UINT1 *) (&u4SubnetOptVal),
                                            OptionValue.pu1_OctetList,
                                            u4OptionLen);
                                    u4SubnetOptVal =
                                        OSIX_NTOHL (u4SubnetOptVal);

                                    if (u4SubnetOptVal != u4SubnetMask)
                                    {
                                        CliPrintf (CliHandle, "  option %d",
                                                   i4FirstOptType);

                                        DhcpSrvDisplayOptionType (CliHandle,
                                                                  i4FirstOptType,
                                                                  au1OptionStr,
                                                                  u4Cmd);
                                    }
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "  option %d",
                                               i4FirstOptType);

                                    DhcpSrvDisplayOptionType (CliHandle,
                                                              i4FirstOptType,
                                                              au1OptionStr,
                                                              u4Cmd);
                                }

                                break;
                        }
                        CliPrintf (CliHandle, "\r");
                    }
                }
                while (nmhGetNextIndexDhcpSrvSubnetOptTable
                       (i4TempFirstPoolIndex,
                        &i4TempNextPoolIndex, i4FirstOptType, &i4NextOptType)
                       == SNMP_SUCCESS);
            }

            MEMSET (FirstHostId.pu1_OctetList, 0,
                    sizeof (au1FirstHostClientMac));
            MEMSET (NextHostId.pu1_OctetList, 0, sizeof (au1NextHostClientMac));

            if (nmhGetFirstIndexDhcpSrvHostConfigTable (&i4FirstHostType,
                                                        &FirstHostId,
                                                        &i4TempFirstPoolIndex)
                == SNMP_SUCCESS)
            {

                i4TempNextPoolIndex = i4TempFirstPoolIndex;
                i4NextHostType = i4FirstHostType;
                NextHostId.i4_Length = FirstHostId.i4_Length;
                MEMCPY (NextHostId.pu1_OctetList,
                        FirstHostId.pu1_OctetList, FirstHostId.i4_Length);
                do
                {
                    i4TempFirstPoolIndex = i4TempNextPoolIndex;
                    i4FirstHostType = i4NextHostType;
                    MEMSET (FirstHostId.pu1_OctetList, 0,
                            sizeof (au1FirstHostClientMac));
                    FirstHostId.i4_Length = NextHostId.i4_Length;
                    MEMCPY (FirstHostId.pu1_OctetList,
                            NextHostId.pu1_OctetList, NextHostId.i4_Length);

                    /*Print host config table */
                    if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                    {
                        continue;
                    }
                    if (nmhGetDhcpSrvHostConfigRowStatus (i4FirstHostType,
                                                          &FirstHostId,
                                                          i4TempFirstPoolIndex,
                                                          &i4RowStatus) ==
                        SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (i4RowStatus != ACTIVE)
                    {
                        continue;
                    }
                    nmhGetDhcpSrvHostIpAddress (i4FirstHostType, &FirstHostId,
                                                i4TempFirstPoolIndex,
                                                &u4IpAddress);
                    if (u4IpAddress != 0)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddress);

                        MEMSET (au1MacStr, 0, DHCP_MAX_CID_LEN);
                        if (i4FirstHostType == 1)
                        {
                            CLI_CONVERT_MAC_TO_DOT_STR
                                (FirstHostId.pu1_OctetList, au1MacStr);
                        }
                        else
                        {
                            MEMCPY (au1MacStr, FirstHostId.pu1_OctetList,
                                    FirstHostId.i4_Length);
                        }

                        CliPrintf (CliHandle,
                                   "  host hardware-type %ld client-identifier %s ip %s\r\n",
                                   i4FirstHostType, au1MacStr, pu1IpAddr);
                    }
                }
                while (nmhGetNextIndexDhcpSrvHostConfigTable (i4FirstHostType,
                                                              &i4NextHostType,
                                                              &FirstHostId,
                                                              &NextHostId,
                                                              i4TempFirstPoolIndex,
                                                              &i4TempNextPoolIndex));

            }
            MEMSET (FirstHostId.pu1_OctetList, 0,
                    sizeof (au1FirstHostClientMac));
            MEMSET (NextHostId.pu1_OctetList, 0, sizeof (au1NextHostClientMac));

            if (nmhGetFirstIndexDhcpSrvHostOptTable
                (&i4FirstHostType, &FirstHostId, &i4TempFirstPoolIndex,
                 &i4FirstOptType) == SNMP_SUCCESS)
            {
                i4TempNextPoolIndex = i4TempFirstPoolIndex;
                i4NextHostType = i4FirstHostType;
                i4NextOptType = i4FirstOptType;
                NextHostId.i4_Length = FirstHostId.i4_Length;
                MEMCPY (NextHostId.pu1_OctetList,
                        FirstHostId.pu1_OctetList, FirstHostId.i4_Length);

                do
                {
                    i4TempFirstPoolIndex = i4TempNextPoolIndex;
                    i4FirstHostType = i4NextHostType;
                    i4FirstOptType = i4NextOptType;

                    MEMSET (FirstHostId.pu1_OctetList, 0,
                            sizeof (au1FirstHostClientMac));

                    FirstHostId.i4_Length = NextHostId.i4_Length;
                    MEMCPY (FirstHostId.pu1_OctetList,
                            NextHostId.pu1_OctetList, NextHostId.i4_Length);

                    if (i4TempFirstPoolIndex != i4FirstSubnetPoolIndex)
                    {
                        /* Print Host option info of current Pool Id */
                        continue;
                    }
                    nmhGetDhcpSrvHostOptRowStatus (i4FirstHostType,
                                                   &FirstHostId,
                                                   i4TempFirstPoolIndex,
                                                   i4FirstOptType, &i4RetVal);
                    if (i4RetVal == ACTIVE)
                    {
                        nmhGetDhcpSrvHostOptLen (i4FirstHostType,
                                                 &FirstHostId,
                                                 i4TempFirstPoolIndex,
                                                 i4FirstOptType,
                                                 (INT4 *) &u4OptionLen);

                        MEMSET (OptionValue.pu1_OctetList, 0, DHCP_MAX_OPT_LEN);
                        nmhGetDhcpSrvHostOptVal (i4FirstHostType,
                                                 &FirstHostId,
                                                 i4TempFirstPoolIndex,
                                                 i4FirstOptType, &OptionValue);

                        MEMSET (au1MacStr, 0, DHCP_MAX_CID_LEN);

                        if (i4FirstHostType == 1)
                        {
                            CLI_CONVERT_MAC_TO_DOT_STR
                                (FirstHostId.pu1_OctetList, au1MacStr);
                        }
                        else
                        {
                            MEMCPY (au1MacStr, FirstHostId.pu1_OctetList,
                                    FirstHostId.i4_Length);
                        }

                        MEMSET (au1OptionStr, 0, DHCP_MAX_OPT_LEN);

                        MEMCPY (au1OptionStr, OptionValue.pu1_OctetList,
                                u4OptionLen);

                        /* change octet string to display string */
                        DhcpSrvProcessValidateOption (CliHandle,
                                                      (UINT1) i4FirstOptType,
                                                      &u4OptionLen,
                                                      OptionValue.pu1_OctetList,
                                                      FALSE, au1OptionStr);

                        CliPrintf (CliHandle,
                                   "  host hardware-type %d client-identifier %s",
                                   i4FirstHostType, au1MacStr);

                        if (i4FirstOptType == DHCP_OPT_DNS_NS)
                        {
                            CliPrintf (CliHandle, " dns-server ");
                        }
                        else if (i4FirstOptType == DHCP_OPT_NTP_SERVERS)
                        {
                            CliPrintf (CliHandle, " ntp-server ");
                        }
                        else if (i4FirstOptType == DHCP_OPT_SIP_SERVER)
                        {
                            CliPrintf (CliHandle, " sip-server ");
                        }
                        else
                        {
                            CliPrintf (CliHandle, " option %d", i4FirstOptType);
                        }

                        DhcpSrvDisplayOptionType (CliHandle, i4FirstOptType,
                                                  au1OptionStr, u4Cmd);
                    }
                    MEMSET (NextHostId.pu1_OctetList, 0,
                            sizeof (au1NextHostClientMac));

                }
                while (nmhGetNextIndexDhcpSrvHostOptTable
                       (i4FirstHostType, &i4NextHostType,
                        &FirstHostId, &NextHostId,
                        i4TempFirstPoolIndex, &i4TempNextPoolIndex,
                        i4FirstOptType, &i4NextOptType) == SNMP_SUCCESS);

            }
            CliPrintf (CliHandle, "!\r\n");
        }
        while (nmhGetNextIndexDhcpSrvSubnetPoolConfigTable
               (i4FirstSubnetPoolIndex, &i4NextSubnetPoolIndex)
               == SNMP_SUCCESS);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssDhcpSrvShowDebugging                            */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DHCP server   debug level */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssDhcpSrvShowDebugging (tCliHandle CliHandle)
{

    INT4                i4DbgLevel = 0;

    nmhGetDhcpSrvDebugLevel (&i4DbgLevel);
    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "DHCP SERVER:\r");

    if ((i4DbgLevel & DEBUG_DHCPSRV_EVENTS) != 0)
    {
        CliPrintf (CliHandle, "\r\nDHCP SERVER events debugging is on\r");
    }
    if ((i4DbgLevel & DEBUG_DHCPSRV_PACKETS) != 0)
    {
        CliPrintf (CliHandle, "\r\nDHCP SERVER packets debugging is on\r");
    }
    if ((i4DbgLevel & DEBUG_DHCPSRV_ERRORS) != 0)
    {
        CliPrintf (CliHandle, "\r\nDHCP SERVER errors debugging is on\r");
    }
    if ((i4DbgLevel & DEBUG_DHCPSRV_BIND) != 0)
    {
        CliPrintf (CliHandle, "\r\nDHCP SERVER bind debugging is on\r");
    }

    CliPrintf (CliHandle, "\r\n");

    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DhcpSrvDisplayOptionType                           */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Formatted Option Value    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4FirstOptType - Option Type                       */
/*                        au1OptionStr   - Formatted Option Value            */
/*                        u4CmdType - Hex/Ascii/IP format                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
DhcpSrvDisplayOptionType (tCliHandle CliHandle, INT4 i4FirstOptType,
                          UINT1 *au1OptionStr, UINT4 u4CmdType)
{

    tUtlInAddr          IpAddr;
    CHR1               *au1StrSubOpn = NULL;
    UINT1               au1StrSubOpn1[DHCP_MAX_OPT_LEN];
    MEMSET (au1StrSubOpn1, 0, DHCP_MAX_OPT_LEN);

    switch (i4FirstOptType)
    {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 41:
        case 42:
        case 44:
        case 45:
        case 48:
        case 49:
        case 65:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:

        case 16:
        case 68:
            au1StrSubOpn = STRSTR (au1OptionStr, ",");

            if (au1StrSubOpn != NULL)
            {
                if (!(CLI_INET_ATON (au1StrSubOpn, &IpAddr)))
                {
                    CliPrintf (CliHandle, "ip ");
                    MEMCPY (au1StrSubOpn1, au1OptionStr,
                            (STRLEN (au1OptionStr) - STRLEN (au1StrSubOpn)));
                    CliPrintf (CliHandle, "%s ", au1StrSubOpn1);
                    CliPrintf (CliHandle, "%s ", ++au1StrSubOpn);
                }
            }
            else if (au1StrSubOpn == NULL)
            {
                CliPrintf (CliHandle, "ip %s", au1OptionStr);
            }

            break;
        case 120:
            au1StrSubOpn = STRSTR (au1OptionStr, ",");

            if (au1StrSubOpn != NULL)
            {
                if (!(CLI_INET_ATON (au1StrSubOpn, &IpAddr)))
                {
                    CliPrintf (CliHandle, "ip ");
                    MEMCPY (au1StrSubOpn1, au1OptionStr,
                            (STRLEN (au1OptionStr) - STRLEN (au1StrSubOpn)));
                    CliPrintf (CliHandle, "%s ", au1StrSubOpn1);
                    CliPrintf (CliHandle, "%s ", ++au1StrSubOpn);
                }
            }
            else if (CLI_INET_ATON (au1OptionStr, &IpAddr))
            {
                CliPrintf (CliHandle, "ip %s", au1OptionStr);
            }
            else
            {
                CliPrintf (CliHandle, "domain %s", au1OptionStr);
            }

            break;
        default:
            if (u4CmdType == DHCPSRV_OPTION_IP)
            {
                CliPrintf (CliHandle, " ip %s", au1OptionStr);
            }

            else if (u4CmdType == DHCPSRV_OPTION_HEX)
            {
                CliPrintf (CliHandle, " hex %s", au1OptionStr);
            }

            else
            {
                CliPrintf (CliHandle, " ascii %s", au1OptionStr);
            }
            break;
    }

}

/******************************************************************************/
/* Function Name     : DhcpSrvGetExpiryTime                                   */
/*                                                                            */
/* Description       : This function calculates the lease expiry time in      */
/*                     format of "Mon Jan 1 00:00:01 2000"                    */
/*                                                                            */
/* Input Parameters  : u1TimeStr  - Pointer to Time string to return          */
/*                     u4RemainingLease - Lease time remaining for this       */
/*                                        address                             */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

UINT4
DhcpSrvGetExpiryTime (UINT1 *pu1TimeStr, UINT4 u4RemainingLease)
{
    UINT4               u4Ticks = 0;

    /* Offset for calculating time = Current ticks + no of ticks in 
       remaining lease time */
    OsixGetSysTime (&u4Ticks);
    u4Ticks += (u4RemainingLease * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    /*UtlGetTimeStrForTicks will provide us the time string 
     * from ticks since the router is up*/
    UtlGetTimeStrForTicks (u4Ticks, (CHR1 *) pu1TimeStr);
    return (CLI_SUCCESS);
}

/******************************************************************************/
/* Function Name     : DhcpSrvProcessSetClearCounters                         */
/*                                                                            */
/* Description       : This function clears the dhcp server counters.         */
/*                                                                            */
/* Input Parameters  : CliHandle                                              */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
DhcpSrvProcessSetClearCounters (tCliHandle CliHandle)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DhcpCountResetCounters
        (&u4ErrCode, DHCP_RESET) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDhcpCountResetCounters (DHCP_RESET) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
#endif /* DHCP_SRV_WANTED */
