/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsscan.c,v 1.16 2012/01/13 12:19:31 siva Exp $ 
 *
 * Description: This file contains functions for Initialising the 
 *              Mempools and Linked list addition/removal/retrieval
 *              routines.
 *******************************************************************/

#include "dhcpscom.h"

/************************************************************************/
/*  Function Name   : DhcpGetPoolEntry                                  */
/*  Description     : Get the pool entry from the List.returns NULL if  */
/*                  : it is not present in the list                     */
/*  Input(s)        : u4PoolId - Index to the pool                      */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the node if present else NULL !        */
/************************************************************************/
tDhcpPool          *
DhcpGetPoolEntry (UINT4 u4PoolId)
{
    tDhcpPool          *pEntry = NULL;

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if (pEntry->u4PoolId == u4PoolId)
        {
            break;
        }
    }
    return (pEntry);
}

/************************************************************************/
/*  Function Name   : DhcpAddPoolEntry                                  */
/*  Description     : Add a new pool to gpDhcpPool.                     */
/*  Input(s)        : pNode - pool to be added.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpAddPoolEntry (tDhcpPool * pNode)
{
    pNode->pNext = gpDhcpPool;
    gpDhcpPool = pNode;
}

/************************************************************************/
/*  Function Name   : DhcpRemovePoolEntry                               */
/*  Description     : Removes a pool entry form the list.               */
/*  Input(s)        : pNode - Entry to be removed                       */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpRemovePoolEntry (tDhcpPool * pNode)
{
    tDhcpPool          *pCurrNode;
    tDhcpPool          *pPrevNode;

    if (pNode == gpDhcpPool)
    {
        gpDhcpPool = (gpDhcpPool != NULL) ? gpDhcpPool->pNext : NULL;
        return;
    }

    pPrevNode = gpDhcpPool;

    for (pCurrNode = gpDhcpPool; pCurrNode != NULL;
         pCurrNode = pCurrNode->pNext)
    {
        if (pNode == pCurrNode)
        {
            pPrevNode->pNext = pCurrNode->pNext;
            return;
        }
        pPrevNode = pCurrNode;

    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpAddBinding                                    */
/*  Description     : Adds New binding entry to the list.               */
/*  Input(s)        : pNode - binding entry to be added.                */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpAddBinding (tDhcpBinding * pNode)
{
    pNode->pNext = gpDhcpBinding;
    gpDhcpBinding = pNode;
}

/************************************************************************/
/*  Function Name   : DhcpRemoveBinding                                 */
/*  Description     : Removes binding entry form the list. does not     */
/*                    free the entry.                                   */
/*  Input(s)        : pNode - Entry to be removed from the list.        */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpRemoveBinding (tDhcpBinding * pNode)
{
    tDhcpBinding       *pCurrNode;
    tDhcpBinding       *pPrevNode;

    if (pNode == gpDhcpBinding)
    {
        gpDhcpBinding = (gpDhcpBinding != NULL) ? gpDhcpBinding->pNext : NULL;
        return;
    }

    pPrevNode = gpDhcpBinding;

    for (pCurrNode = gpDhcpBinding; pCurrNode != NULL;
         pCurrNode = pCurrNode->pNext)
    {
        if (pNode == pCurrNode)
        {
            pPrevNode->pNext = pCurrNode->pNext;
            return;
        }
        pPrevNode = pCurrNode;

    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpGetHostEntry                                  */
/*  Description     : Gets a Host specific entry for the received packet*/
/*                    if present                                        */
/*  Input(s)        : u1HwType - hardware type of the rcvd dhcp packet. */
/*                    pMacAddr - hardware address or client identifier. */
/*                    u1HostIdLen - length of pMacAddr.                 */
/*                    u4PoolId - subnet pool id.                        */
/*  Output(s)       : None.                                             */
/*  Returns         : host entry if present else NULL                   */
/************************************************************************/

tDhcpHostEntry     *
DhcpGetHostEntry (UINT4 u4HwType, UINT1 *pMacAddr, UINT4 u4HostIdLen,
                  UINT4 u4PoolId)
{
    tDhcpHostEntry     *pEntry = NULL;

    for (pEntry = gpDhcpHostEntry; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if ((pEntry->u4PoolId == u4PoolId) &&
            (pEntry->u1HwType == u4HwType) &&
            (MEMCMP (pMacAddr, pEntry->aClientMac, u4HostIdLen) == 0))

        {
            break;
        }
    }
    return pEntry;
}

/************************************************************************/
/*  Function Name   : DhcpAddHostEntry                                  */
/*  Description     : Add host specific entry to the list.              */
/*  Input(s)        : pNode - new entry to be added.                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpAddHostEntry (tDhcpHostEntry * pNode)
{
    pNode->pNext = gpDhcpHostEntry;
    gpDhcpHostEntry = pNode;
}

/************************************************************************/
/*  Function Name   : DhcpRemoveHostEntry                               */
/*  Description     : Removes a host entry form the list.does not free  */
/*                    the entry.                                        */
/*  Input(s)        : pNode - Entry to be removed.                      */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpRemoveHostEntry (tDhcpHostEntry * pNode)
{
    tDhcpHostEntry     *pCurrNode;
    tDhcpHostEntry     *pPrevNode;

    if (pNode == gpDhcpHostEntry)
    {
        gpDhcpHostEntry = (gpDhcpHostEntry) ? gpDhcpHostEntry->pNext : NULL;
        return;
    }

    pPrevNode = gpDhcpHostEntry;

    for (pCurrNode = gpDhcpHostEntry; pCurrNode != NULL;
         pCurrNode = pCurrNode->pNext)
    {
        if (pNode == pCurrNode)
        {
            pPrevNode->pNext = pCurrNode->pNext;
            return;
        }
        pPrevNode = pCurrNode;

    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpAddExcludedListNode                           */
/*  Description     : Add an exclude ip-address node to the list.       */
/*  Input(s)        : pList - pointer to the list pointer.              */
/*                    pNode - Entry to be added.                        */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpAddExcludedListNode (tExcludeIpAddr ** pList, tExcludeIpAddr * pNode)
{
    pNode->pNext = *pList;
    *pList = pNode;
}

/************************************************************************/
/*  Function Name   : DhcpRemoveExcludedListNode                        */
/*  Description     : Removes the entry from the list.Does not free the */
/*                    entry.                                            */
/*  Input(s)        : pList - pointer to the list pointer.              */
/*                    pNode - entry to be removed                       */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpRemoveExcludedListNode (tExcludeIpAddr ** pList, tExcludeIpAddr * pNode)
{
    tExcludeIpAddr     *pCurrNode;
    tExcludeIpAddr     *pPrevNode = NULL;

    if (pNode == *pList)
    {
        *pList = (*pList) ? (*pList)->pNext : NULL;
        return;
    }

    pPrevNode = (*pList);
    for (pCurrNode = (*pList)->pNext; pCurrNode != NULL;
         pCurrNode = pCurrNode->pNext)
    {
        if (pNode == pCurrNode)
        {
            pPrevNode->pNext = pCurrNode->pNext;
            return;
        }
        pPrevNode = pCurrNode;

    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpGetOptionFromList                             */
/*  Description     : Get the option entry from the List.               */
/*  Input(s)        : pList - pointer to the option list.               */
/*                    u4Type - option type.                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the option node if present else NULL   */
/************************************************************************/

tDhcpOptions       *
DhcpGetOptionFromList (tDhcpOptions * pList, UINT4 u4Type)
{
    tDhcpOptions       *pNode = NULL;

    for (pNode = pList; pNode != NULL; pNode = pNode->pNext)
    {
        if (pNode->u4Type == u4Type)
            break;
    }
    return pNode;
}

/************************************************************************/
/*  Function Name   : DhcpGetActiveOptionFromList                       */
/*  Description     : Get the Active option entry from the List.        */
/*  Input(s)        : pList - pointer to the option list.               */
/*                    u4Type - option type.                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the option node if present else NULL   */
/************************************************************************/

tDhcpOptions       *
DhcpGetActiveOptionFromList (tDhcpOptions * pList, UINT4 u4Type)
{
    tDhcpOptions       *pNode;

    pNode = DhcpGetOptionFromList (pList, u4Type);

    if (pNode && pNode->u4Status == ACTIVE)
        return pNode;

    return NULL;

}

/************************************************************************/
/*  Function Name   : DhcpAddOptionToList                               */
/*  Description     : Add an option to the list.                        */
/*  Input(s)        : pList - pointer to the option list pointer.       */
/*                    pNode - option node to be added.                  */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpAddOptionToList (tDhcpOptions ** pList, tDhcpOptions * pNode)
{
    pNode->pNext = *pList;
    *pList = pNode;
}

/************************************************************************/
/*  Function Name   : DhcpRemoveOptionFromList                          */
/*  Description     : Removes an option entry from the list if present. */
/*                    Does not free the entry.                          */
/*  Input(s)        : pList - pointer to the option list.               */
/*                    pNode - option node to be removed.                */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpRemoveOptionFromList (tDhcpOptions ** pList, tDhcpOptions * pNode)
{
    tDhcpOptions       *pCurrNode;
    tDhcpOptions       *pPrevNode = NULL;

    if (pNode == *pList)
    {
        *pList = (*pList) ? (*pList)->pNext : NULL;
        return;
    }
    pPrevNode = (*pList);
    for (pCurrNode = (*pList)->pNext; pCurrNode != NULL;
         pCurrNode = pCurrNode->pNext)
    {
        if (pNode == pCurrNode)
        {
            pPrevNode->pNext = pCurrNode->pNext;
            return;
        }
        pPrevNode = pCurrNode;

    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpInitPoolRec                                   */
/*  Description     : Memory is allocated for Pool Records from the     */
/*                  : system memory.                                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpInitPoolRec ()
{

    tDhcpPool          *pEntry = NULL;
    UINT4               u4Count = 1;

    gpDhcpFreePoolRec = pEntry = gDhcpServConfig.pSubnetMemPool =
        (tDhcpPool *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvPoolRecId);

    if (gpDhcpFreePoolRec == NULL)
    {
        return (DHCP_FAILURE);
    }

    while (u4Count < gDhcpServConfig.u4MaxPoolRec)
    {
        pEntry->pNext = pEntry + 1;
        ++pEntry;
        ++u4Count;
    }
    pEntry->pNext = NULL;

    return (DHCP_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpFreePoolRec                                   */
/*  Description     : PoolEntry is added to the free pool list.         */
/*  Input(s)        : pFreeEntry - Pool record to be freed.             */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpFreePoolRec (tDhcpPool * pFreeEntry)
{

    if (pFreeEntry != NULL)
    {
        pFreeEntry->pNext = gpDhcpFreePoolRec;
        gpDhcpFreePoolRec = pFreeEntry;
    }
}

/************************************************************************/
/*  Function Name   : DhcpGetFreePoolRec                                */
/*  Description     : Get new pool record from the free pool entry.     */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the pool entry or NULL.                */
/************************************************************************/

tDhcpPool          *
DhcpGetFreePoolRec ()
{
    tDhcpPool          *pEntry;

    if (gpDhcpFreePoolRec == NULL)
        return (NULL);

    pEntry = gpDhcpFreePoolRec;
    gpDhcpFreePoolRec = gpDhcpFreePoolRec->pNext;
    pEntry->pNext = NULL;

    /* Initialize the Entry  */
    MEMSET (pEntry, 0, sizeof (tDhcpPool));
    pEntry->u4LeaseTime = DHCP_DEF_LEASE_TIME;

    return (pEntry);
}

/************************************************************************/
/*  Function Name   : DhcpInitBindRec                                   */
/*  Description     : Memory is allocated for Binding Records from the  */
/*                  : system memory.                                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpInitBindRec ()
{

    tDhcpBinding       *pEntry = NULL;
    UINT4               u4Count = 1;

    gpDhcpFreeBindRec = pEntry = gDhcpServConfig.pBindMemPool =
        (tDhcpBinding *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvBindRecId);

    if (gpDhcpFreeBindRec == NULL)
    {
        return (DHCP_FAILURE);
    }

    while (u4Count < gDhcpServConfig.u4MaxBindRec)
    {
        pEntry->pNext = pEntry + 1;
        ++pEntry;
        ++u4Count;
    }

    pEntry->pNext = NULL;

    return (DHCP_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpFreeBindRec                                   */
/*  Description     : Free the Binding record. Adds to the free pool.   */
/*  Input(s)        : pFreeEntry - entry to be freed.                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpFreeBindRec (tDhcpBinding * pFreeEntry)
{

    if (pFreeEntry != NULL)
    {
        pFreeEntry->pNext = gpDhcpFreeBindRec;
        gpDhcpFreeBindRec = pFreeEntry;
    }
}

/************************************************************************/
/*  Function Name   : DhcpGetFreeBindRec                                */
/*  Description     : Get a bind record from the free pool.             */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the new binding record or NULL         */
/************************************************************************/

tDhcpBinding       *
DhcpGetFreeBindRec ()
{
    tDhcpBinding       *pEntry;

    if (gpDhcpFreeBindRec == NULL)
        return (NULL);

    pEntry = gpDhcpFreeBindRec;
    gpDhcpFreeBindRec = gpDhcpFreeBindRec->pNext;
    pEntry->pNext = NULL;

    /* Initialize the Entry  */
    MEMSET (pEntry, 0, sizeof (tDhcpBinding));

    return (pEntry);
}

/************************************************************************/
/*  Function Name   : DhcpInitHostRec                                   */
/*  Description     : Memory is allocated for Host Records from the     */
/*                  : system memory.                                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpInitHostRec ()
{

    tDhcpHostEntry     *pEntry = NULL;
    UINT4               u4Count = 1;

    gpDhcpFreeHostRec = pEntry = gDhcpServConfig.pHostMemPool =
        (tDhcpHostEntry *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvHostRecId);

    if (gpDhcpFreeHostRec == NULL)
    {
        return (DHCP_FAILURE);
    }

    while (u4Count < gDhcpServConfig.u4MaxHostRec)
    {
        pEntry->pNext = pEntry + 1;
        ++pEntry;
        ++u4Count;
    }

    pEntry->pNext = NULL;

    return (DHCP_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpFreeHostRec                                   */
/*  Description     : Adds an entry to the DHCP Host Free pool          */
/*  Input(s)        : pFreeEntry - Pointer to entry to be added         */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpFreeHostRec (tDhcpHostEntry * pFreeEntry)
{
    tDhcpOptions       *pOptions, *pTempOption;

    if (pFreeEntry != NULL)
    {
        /* remove the options configured for this host */
        for (pOptions = pFreeEntry->pOptions; pOptions != NULL;
             pOptions = pTempOption)
        {
            pTempOption = pOptions->pNext;
            DhcpFreeOptionRec (pOptions);
        }

        pFreeEntry->pNext = gpDhcpFreeHostRec;
        gpDhcpFreeHostRec = pFreeEntry;
    }
}

/************************************************************************/
/*  Function Name   : DhcpGetFreeHostRec                                */
/*  Description     : Get the host entry from the List.returns NULL if  */
/*                  : it is not present in the list                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the new host record or NULL            */
/************************************************************************/

tDhcpHostEntry     *
DhcpGetFreeHostRec ()
{
    tDhcpHostEntry     *pEntry;

    if (gpDhcpFreeHostRec == NULL)
        return (NULL);

    pEntry = gpDhcpFreeHostRec;
    gpDhcpFreeHostRec = gpDhcpFreeHostRec->pNext;
    pEntry->pNext = NULL;

    /* Initialize the Entry  */
    MEMSET (pEntry, 0, sizeof (tDhcpHostEntry));

    return (pEntry);
}

/************************************************************************/
/*  Function Name   : DhcpFreeOptionRec                                 */
/*  Description     : release the memory allocated for the option entry.*/
/*  Input(s)        : pFreeEntry - Entry to be freed.                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpFreeOptionRec (tDhcpOptions * pFreeEntry)
{
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOptId, (UINT1 *) pFreeEntry);
}

/************************************************************************/
/*  Function Name   : DhcpGetFreeOptionRec                              */
/*  Description     : Allocates memory for the option record.           */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the option node or NULL.               */
/************************************************************************/

tDhcpOptions       *
DhcpGetFreeOptionRec ()
{
    tDhcpOptions       *pEntry;

    pEntry = (tDhcpOptions *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvOptId);
    if (pEntry == NULL)
        return NULL;
    /* Initialize the Entry  */
    MEMSET (pEntry, 0, sizeof (tDhcpOptions));

    return (pEntry);
}

/************************************************************************/
/*  Function Name   : DhcpFreeSubnetPoolIcmpList                        */
/*  Description     : Removes the ICMP list for a subnet pool           */
/*  Input(s)        : i4DhcpSrvSubnetPoolIndex - Subnet Pool Index      */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpFreeSubnetPoolIcmpList (INT4 i4DhcpSrvSubnetPoolIndex)
{
    tDhcpIcmpNode      *pWaitNode, *pTempNode;

    /* remove all the entries in the ICMP wait list */
    for (pTempNode = pWaitNode = gpDhcpIcmpList; pWaitNode != NULL;
         pWaitNode = pTempNode)
    {
        if (pWaitNode->pPoolEntry->u4PoolId == (UINT4) i4DhcpSrvSubnetPoolIndex)
        {
            if (pWaitNode == gpDhcpIcmpList)
                gpDhcpIcmpList = gpDhcpIcmpList->pNext;
            pTempNode = pWaitNode->pNext;
            DhcpRemoveIcmpProbeNode (pWaitNode);
        }
        else
            pTempNode = pWaitNode->pNext;
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpFreeSubnetPoolBindingList                     */
/*  Description     : Removes the Binding list for a subnet pool        */
/*  Input(s)        : i4DhcpSrvSubnetPoolIndex - Subnet Pool Index      */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpFreeSubnetPoolBindingList (INT4 i4DhcpSrvSubnetPoolIndex)
{
    tDhcpBinding       *pBinding, *pTempBind;

    /* remove all the entries in the binding list */
    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pTempBind)
    {
        pTempBind = pBinding->pNext;
        if (pBinding->u4PoolId == (UINT4) i4DhcpSrvSubnetPoolIndex)
        {
            DhcpDeleteBinding (pBinding);
        }
    }
    return;
}

/*************************************************************************/
/*  Function Name   : DhcpFreeSubnetPoolOptionList                       */
/*  Description     : Removes the Subnet Options of a subnet pool        */
/*  Input(s)        : i4DhcpSrvSubnetPoolIndex - Subnet Pool Index       */
/*  Output(s)       : None.                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                          */
/*************************************************************************/

INT4
DhcpFreeSubnetPoolOptionList (INT4 i4DhcpSrvSubnetPoolIndex)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions, *pTempOption;

    pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex);

    if (pEntry == NULL)
    {
        return (DHCP_FAILURE);
    }

    /* remove all the options created for this Pool */
    for (pOptions = pEntry->pOptions; pOptions != NULL; pOptions = pTempOption)
    {
        pTempOption = pOptions->pNext;
        DhcpFreeOptionRec (pOptions);
    }
    pEntry->pOptions = NULL;
    return (DHCP_SUCCESS);
}

/*************************************************************************/
/*  Function Name   : DhcpFreeSubnetPoolOptionList                       */
/*  Description     : Removes the Exclude address pool of a subnet pool  */
/*  Input(s)        : i4DhcpSrvSubnetPoolIndex - Subnet Pool Index       */
/*  Output(s)       : None.                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                          */
/*************************************************************************/

INT4
DhcpFreeSubnetPoolExcludeList (INT4 i4DhcpSrvSubnetPoolIndex, UINT1 u1EntryType)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExclude, *pTempExclude;
    pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex);

    if (pEntry == NULL)
    {
        return (DHCP_FAILURE);
    }

    /* remove all the statically configured excluded ip address nodes for 
     * this Pool */
    if (u1EntryType == DHCP_SNMP)
    {
        for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
             pExclude = pTempExclude)
        {
            pTempExclude = pExclude->pNext;
            MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvExclAddrId,
                                (UINT1 *) pExclude);
        }

        pEntry->pExcludeIpAddr = NULL;
    }
    else
    {
        /* Remove all the excluded ip address nodes for
         * this Pool which were learnt dynamically by using
         *  DHCP Decline message*/

        for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
             pExclude = pExclude->pNext)
        {
            if (pExclude->u4Method == DHCP_NOT_SNMP)
            {
                DhcpRemoveExcludedListNode (&pEntry->pExcludeIpAddr, pExclude);
                MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvExclAddrId,
                                    (UINT1 *) pExclude);
                /* Recalculate the available IP count in the pool */
                DhcpSrvCalculateIpCount (pEntry);
            }
        }
    }
    return (DHCP_SUCCESS);
}

/*************************************************************************/
/*  Function Name   : DhcpSrvCalculateIpCount                            */
/*  Description     : Calculates the total no of IP addresses available  */
/*                    in a pool                                          */
/*  Input(s)        : pEntry - Pool entry                                */
/*  Output(s)       : None.                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                          */
/*************************************************************************/

INT4
DhcpSrvCalculateIpCount (tDhcpPool * pPoolEntry)
{
    UINT4               u4IpAddress;
    INT4                i4Found = DHCP_FAILURE;
    tExcludeIpAddr     *pExclude;

    pPoolEntry->u4IpCount = 0;

    for (u4IpAddress = pPoolEntry->u4StartIpAddr;
         u4IpAddress <= pPoolEntry->u4EndIpAddr; u4IpAddress++)
    {
        i4Found = DHCP_FAILURE;
        for (pExclude = pPoolEntry->pExcludeIpAddr; pExclude != NULL;
             pExclude = pExclude->pNext)
        {
            if ((pExclude->u4StartIpAddr <= u4IpAddress) &&
                (pExclude->u4EndIpAddr >= u4IpAddress))
            {
                i4Found = DHCP_SUCCESS;    /*This address is in the excluded IP */
                /*table. So exclude this from available */
                /*IP count */
                break;
            }
        }
        if (i4Found == DHCP_FAILURE)
        {
            pPoolEntry->u4IpCount++;
        }
    }
    return (DHCP_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : DhcpIsHostIpAllocated                               */
/*                                                                        */
/*  Description     : Checks whether a given IP is allocated to any host  */
/*                    other than the given host. It scans both host       */
/*                    config table and binding table                      */
/*                                                                        */
/*  Input(s)        : u4HwType  - Hardware type of the host               */
/*                    pMacAddr  - Mac address of the host                 */
/*                    u4HostIdLen - Length of Mac address                 */
/*                    u4PoolId    - Pool ID to be scanned                 */
/*                    u4IpAddress - IP address to be checked              */
/*                                                                        */
/*  Output(s)       : None.                                               */
/*                                                                        */
/*  Returns         : DHCP_SUCCESS - If IP is leased/reserved to some host*/
/*                    DHCP_FAILURE - Otherwise                            */
/**************************************************************************/
INT4
DhcpIsHostIpAllocated (UINT4 u4HwType, UINT1 *pMacAddr, UINT4 u4HostIdLen,
                       UINT4 u4PoolId, UINT4 u4IpAddress)
{

    tDhcpHostEntry     *pHostNode = NULL;
    tDhcpBinding       *pBindNode;

    for (pHostNode = gpDhcpHostEntry; pHostNode != NULL;
         pHostNode = pHostNode->pNext)
    {
        if (pHostNode->u4StaticIpAddr == u4IpAddress)
        {
            if ((pHostNode->u4PoolId == u4PoolId) &&
                (pHostNode->u1HwType == u4HwType) &&
                (MEMCMP (pMacAddr, pHostNode->aClientMac, u4HostIdLen) == 0))
            {
                /*IP is allocated to the requested HOST. Let us continue */
                continue;
            }
            else
            {
                return DHCP_SUCCESS;
            }
        }
    }

    for (pBindNode = gpDhcpBinding; pBindNode != NULL;
         pBindNode = pBindNode->pNext)
    {
        if (pBindNode->u4IpAddress == u4IpAddress)
        {
            if ((pBindNode->u4PoolId == u4PoolId) &&
                (pBindNode->u1HwType == u4HwType) &&
                (MEMCMP (pMacAddr, pBindNode->aBindMac, u4HostIdLen) == 0))
            {
                /*IP is allocated to the requested HOST. Let us continue */
                continue;
            }
            else
            {
                return DHCP_SUCCESS;
            }
        }
    }
    return DHCP_FAILURE;
}
