/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: dhcpsicmp.c,v 1.15 2014/02/14 13:55:10 siva Exp $
 * 
 ********************************************************************/
#include "dhcpscom.h"
/************************************************************************/
/*  Function Name   : DhcpOpenICMPSocket                                */
/*  Description     : Opens the ICMP Socket                             */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
DhcpSOpenICMPSocket ()
{
    struct sockaddr_in  Sin;

    /* Open the RawSocket for sending EchoRequests and 
       Receving EchoReplies */
    gi4DhcpSrvICMPSocket = socket (AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (gi4DhcpSrvICMPSocket < 0)
    {
        DHCP_TRC (ALL_FAILURE_TRC, "Creating ICMP Socket for dhcp failed \r\n");
        return DHCP_FAILURE;
    }

    MEMSET (&Sin, 0, sizeof (Sin));
    Sin.sin_family = AF_INET;
    Sin.sin_port = DHCPS_ICMP_IDENTIFIER;
    if (bind (gi4DhcpSrvICMPSocket, (struct sockaddr *) &Sin, sizeof (Sin)) < 0)
    {
        DHCP_TRC (ALL_FAILURE_TRC, "Binding Socket  failed !!!! \r\n");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpCloseICMPSocket                                */
/*  Description     : Closes the ICMP Socket                             */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
DhcpSCloseICMPSocket ()
{
    INT4                i4RetVal;

    DHCP_TRC (ALL_TRC, "Entered in DhcpSCloseICMPSocket fn\n");

    if (gi4DhcpSrvICMPSocket != -1)
    {
        SelRemoveFd (gi4DhcpSrvICMPSocket);

        i4RetVal = dhcps_close (gi4DhcpSrvICMPSocket);

        if (i4RetVal < 0)
        {
            /* DHCP Client socket close failed */
            DHCP_TRC (FAIL_TRC,
                      "DHCP Server ICMP Socket Close is not successful\n");
            return (DHCP_FAILURE);
        }
    }

    gi4DhcpSrvICMPSocket = -1;
    DHCP_TRC (ALL_TRC, "Socket for sending ICMP packet closed\n");
    return (DHCP_SUCCESS);
}

/************************************************************************/
/*  Function Name   : DhcpIcmpEchoRequest                               */
/*  Description     : Sends the ICMP Request                            */
/*  Input(s)        : u4IpAddr - Destination IpAddress                  */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpIcmpEchoRequest (UINT4 u4IpAddr)
{
    dhcps_sockaddr_in   ToDest;
    tDhcpSIcmpHdr       DhcpsIcmp;
    INT4                i4Status;

    if (SelAddFd (gi4DhcpSrvICMPSocket, DhcpPktInIcmpSocket) != OSIX_SUCCESS)
    {
        DHCP_TRC (ALL_FAILURE_TRC,
                  "Adding Socket with Select utility for listening failed !!!! \r\n");
        return DHCP_FAILURE;
    }

    MEMSET (&ToDest, 0, sizeof (ToDest));
    ToDest.sin_family = AF_INET;
    ToDest.sin_port = 0;

    ToDest.sin_addr.s_addr = OSIX_HTONL (u4IpAddr);
    DhcpsIcmp.i1Type = DHCPS_ICMP_ECHO;
    DhcpsIcmp.i1Code = 0;
    DhcpsIcmp.u2Cksum = 0;
    gu2DhcpIcmpSeqNo++;
    DhcpsIcmp.args.Identification.i2Seq = gu2DhcpIcmpSeqNo;
    DhcpsIcmp.args.Identification.i2Id = DHCPS_ICMP_IDENTIFIER;

    DhcpsIcmp.u2Cksum =
        OSIX_NTOHS (UtlIpCSumLinBuf
                    ((INT1 *) &DhcpsIcmp, sizeof (tDhcpSIcmpHdr)));

    i4Status =
        sendto (gi4DhcpSrvICMPSocket, (INT1 *) &DhcpsIcmp,
                sizeof (tDhcpSIcmpHdr), 0,
                (dhcps_sockaddr *) & ToDest, sizeof (ToDest));
    if (i4Status < 0)
    {
        DHCP_TRC (ALL_FAILURE_TRC, "Sending ICMP Packet Failed\r\n");
        return DHCP_FAILURE;
    }
    DHCP_TRC (ALL_TRC, "ICMP echo Request Sent !!!!!!\r\n");
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpPktInIcmpSocket                               */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/

VOID
DhcpPktInIcmpSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    OsixEvtSend (gu4DhcpServTskId, DHCP_ICMP_PKT_ARRIVAL_EVENT);
}

/************************************************************************/
/*  Function Name   : DhcpProcessIcmpResponse                           */
/*  Description     : Processes ICMP Response                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/

VOID
DhcpProcessIcmpResponse ()
{
    tDhcpSIcmpHdr      *pICMP;
    t_IP_HEADER        *pIpHdr;
    tDhcpIcmpNode      *pWaitNode = NULL;
    dhcps_sockaddr_in   PeerAddr;
    UINT2               u2IpHdrLen;
    INT4                i4AddrLen = sizeof (PeerAddr);
    INT4                i4DataLen;
    UINT4               u4SrcIp;
    tDhcpRcvPkt        *pRecvBuff = NULL;
    pRecvBuff =
        (tDhcpRcvPkt *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvRecvBuffId);
    if (pRecvBuff == NULL)
    {
        DHCP_TRC (EVENT_TRC, "MemAlloc failed:: DhcpProcessIcmpResponse \r\n");
        return;
    }
    MEMSET (pRecvBuff, 0, sizeof (tDhcpRcvPkt));

    MEMSET (&PeerAddr, 0, sizeof (PeerAddr));
    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_port = DHCPS_ICMP_IDENTIFIER;

    /* Call recvfrom to receive the packet */
    if ((i4DataLen =
         dhcps_recvfrom (gi4DhcpSrvICMPSocket, pRecvBuff->i1RecvBuff,
                         (INT4) DHCP_MAX_MTU, 0, (dhcps_sockaddr *) & PeerAddr,
                         &i4AddrLen)) > 0)
    {
        DHCP_TRC (ALL_TRC,
                  "DHCP Server - packet retrieved on ICMP socket.**\n");
    }

    /*Take the lock */
    DHCPS_PROTO_LOCK ();

    pIpHdr = (t_IP_HEADER *) (VOID *) pRecvBuff->i1RecvBuff;

    u2IpHdrLen = (UINT2) ((pIpHdr->u1Ver_hdrlen & 0x0f) * sizeof (UINT4));

    u4SrcIp = OSIX_HTONL (pIpHdr->u4Src);

    pICMP = (tDhcpSIcmpHdr *) (VOID *) (pRecvBuff->i1RecvBuff + u2IpHdrLen);

    pWaitNode = DhcpGetIcmpNode (u4SrcIp);

    /* Check whether the response is for Probed Address */
    if (pWaitNode == NULL)
    {
        DHCP_TRC (ALL_TRC,
                  "DHCP Server - Response for Address thats not Probed !!!!**\n");
        /*Release the lock */
        DHCPS_PROTO_UNLOCK ();
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvRecvBuffId,
                            (UINT1 *) pRecvBuff->i1RecvBuff);
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvRecvBuffId,
                            (UINT1 *) pRecvBuff);
        return;
    }
    if ((pICMP->args.Identification.i2Seq > pWaitNode->u2IcmpSeqId) ||
        (pICMP->args.Identification.i2Seq <
         pWaitNode->u2IcmpSeqId - (INT2) gu4PingCount))
    {
        DHCP_TRC (ALL_TRC, "DHCALL_ICMP Response SeqId MisMatch !!!!**\n");
        /*Release the lock */
        DHCPS_PROTO_UNLOCK ();
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvRecvBuffId,
                            (UINT1 *) pRecvBuff->i1RecvBuff);
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvRecvBuffId,
                            (UINT1 *) pRecvBuff);
        return;
    }
    DhcpStopTimer (DhcpTimerListId, &pWaitNode->TmrNode);
    DhcpProcessIcmpProbeTimeOut (pWaitNode, DHCP_ICMP_PROBE_SUCCESS);

    /*Release the lock */
    DHCPS_PROTO_UNLOCK ();
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvRecvBuffId, (UINT1 *) pRecvBuff);
    return;
}

/************************************************************************/
/*  Function Name   : DhcpGetIcmpNode                                   */
/*  Description     : Returns the Free ICMP Node                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/

tDhcpIcmpNode      *
DhcpGetIcmpNode (UINT4 u4IpAddr)
{
    tDhcpIcmpNode      *pTempNode = NULL;
    for (pTempNode = gpDhcpIcmpList; pTempNode != NULL;
         pTempNode = pTempNode->pNext)
    {
        if (pTempNode->u4ProbeAddr == u4IpAddr)
        {
            return (pTempNode);
        }
    }
    return NULL;
}
