/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpspool.c,v 1.26 2014/06/14 11:01:52 siva Exp $ 
 *
 * Description: This file contains functions related to the subent    
 *              pools and address selection.
 *******************************************************************/

#include "dhcpscom.h"
extern UINT4        u4CidrSubnetMask[];
/************************************************************************/
/*  Function Name   : DhcpGetPoolEntryFromPkt                           */
/*  Description     : This function gets the subnet pool entry for the  */
/*                  : received dhcp packet.                             */
/*  Input(s)        : pPkt - received dhcp packet.                      */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP Subnet pool entry                            */
/************************************************************************/
tDhcpPool          *
DhcpGetPoolEntryFromPkt (tDhcpPktInfo * pPkt)
{
    UINT4               u4Subnet, u4IpAddress;
    tDhcpPool          *pEntry = NULL;

    /* If giaddr field is not zero, select a pool which matches with
     * giaddr subnet. Else if the packet is unicast and ciaddr is present
     * select a subnet pool which matches with ciaddr field. Else select 
     * pool matches with subnet of the received interface.
     * */

    if (pPkt->DhcpMsg.giaddr != 0)
    {
        u4IpAddress = pPkt->DhcpMsg.giaddr;
    }
    else
    {
        if ((pPkt->DhcpMsg.ciaddr != 0) && (pPkt->u4IpAddr != 0xFFFFFFFF))
        {
            u4IpAddress = pPkt->DhcpMsg.ciaddr;
        }
        else
        {
            u4IpAddress = DhcpGetServerIdentifier (pPkt->u4IfIndex);
        }
    }

    /* If packet has vendor specific info select pool based on that */
    if (DhcpGetOption (DHCP_OPT_CVENDOR_SPECIFIC, pPkt) == DHCP_FOUND)
    {
        UINT1              *pu1VSData;
        pu1VSData = DhcpTag.Val;
        for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
        {
            if (MEMCMP (pu1VSData, pEntry->au1PoolName, DhcpTag.Len) == 0)
            {
                break;
            }
        }
        if (pEntry != NULL)
        {                        /* Found a Pool with name matching incoming Vendor Specific Data */
            u4Subnet = u4IpAddress & pEntry->u4NetMask;
            if (pEntry->u4Status == ACTIVE)
            {
                return pEntry;
            }
        }
        else
        {                        /* No Pool Found, search for default pool */
            for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
            {
                if (STRNCMP ("DEFAULT", pEntry->au1PoolName, 7) == 0)
                {
                    break;
                }
            }
            if (pEntry != NULL)
            {                    /* Found default pool */
                u4Subnet = u4IpAddress & pEntry->u4NetMask;
                if (pEntry->u4Status == ACTIVE)
                {
                    return pEntry;
                }
            }
        }
        /* No matching or default pool found */
        /* No matching or default pool found for the given vendor specific option
         * shall be able to map to the default pools that are configured
         * This is according to RFC 2132
         * Servers not equipped to interpret the clas-specific information
         * sent by a client MUST ignore it (although it may be reported)
         * Hence if no match is found, allowed to go for normal pool check
         * This is happening with Windows PC. Sending default option 60, but
         * if not configured on server, still it shall be assigning IP address
         * If configured on server, can be giving from the pool which is matching
         * with Vendor specific option */

    }

    /* If user class option is present, try to select the pool
     * using that. */

    if (DhcpGetOption (DHCP_OPT_USER_CLASS, pPkt) == DHCP_FOUND)
    {
        UINT4               u4OptLen;
        UINT1               u1UserClassLen, OffSet;
        UINT1              *pUserClassData;

        u4OptLen = DhcpTag.Len;

        for (OffSet = 2; OffSet < u4OptLen; OffSet += u1UserClassLen + 1)
        {
            pEntry = NULL;
            u1UserClassLen = DhcpTag.Val[OffSet];
            pUserClassData = DhcpTag.Val + OffSet + 1;
            for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
            {
                if (MEMCMP (pUserClassData, pEntry->au1PoolName,
                            u1UserClassLen) == 0)
                    break;
            }
            if (pEntry != NULL)
            {
                u4Subnet = u4IpAddress & pEntry->u4NetMask;

                if ((pEntry->u4Subnet == u4Subnet) &&
                    (pEntry->u4Status == ACTIVE))
                {
                    return pEntry;
                }
            }
        }

    }

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        u4Subnet = u4IpAddress & pEntry->u4NetMask;
        if ((pEntry->u4Subnet == u4Subnet) && (pEntry->u4Status == ACTIVE))
            break;
    }

    return pEntry;

}

/************************************************************************/
/*  Function Name   : DhcpSelectIpAddress                               */
/*  Description     : This function will try to get a free ip address.  */
/*                    If requested Ip address is  not  zero   it will   */
/*                    try to assign that address otherwise it will try  */
/*                    get a free Ip-Address from the subnet poll.       */
/*  Input(s)        : pPool - subnet pool.                              */
/*                    u4RequestedIp  - requested Ip address.            */
/*  Output(s)       : None.                                             */
/*  Returns         : selected Ip-address or 0 incase of failure        */
/************************************************************************/
UINT4
DhcpSelectIpAddress (tDhcpPool * pPool, UINT4 u4RequestedIp)
{
    UINT4               u4SelectedIp;
    tDhcpBinding       *pBinding;
    tExcludeIpAddr     *pExclude;
    tDhcpHostEntry     *pHost;
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;
    tUtlInAddr          IpAddr;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = 0;
    tIpIfRecord         IpIntfInfo;
    tIpIfRecord        *pIpIntf = NULL;

    pExclude = pPool->pExcludeIpAddr;

    if (u4RequestedIp != 0)
    {

        /* check whether we can allocate the requested ip-address 
         * if yes, return the requested address */
        if (NetIpv4IfIsOurAddress (u4RequestedIp) == NETIPV4_SUCCESS)
        {
            goto request_not_possible;
        }
        if (DHCP_IN_RANGE (pPool->u4StartIpAddr, pPool->u4EndIpAddr,
                           u4RequestedIp))
        {
            for (pBinding = gpDhcpBinding; pBinding != NULL;
                 pBinding = pBinding->pNext)
            {
                if (pBinding->u4IpAddress == u4RequestedIp)
                    goto request_not_possible;
            }
            while (pExclude != NULL)
            {
                if ((pExclude->u4StartIpAddr <= u4RequestedIp)
                    && (pExclude->u4EndIpAddr >= u4RequestedIp))
                    goto request_not_possible;
                pExclude = pExclude->pNext;
            }

            /* Address configured for specific hosts should not be
             * allocated for dynamic entries. */
            for (pHost = gpDhcpHostEntry; pHost != NULL; pHost = pHost->pNext)
            {
                if (u4RequestedIp == pHost->u4StaticIpAddr)
                    goto request_not_possible;
            }

            return u4RequestedIp;
        }
    }

  request_not_possible:

    /* Try to get some other address from the address pool. */

    u4SelectedIp = pPool->u4StartIpAddr;

    /* Get the broadcast IP of the given pool */
    i4RetVal =
        CfaIpIfGetIfIndexFromHostIpAddressInCxt (u4ContextId, u4SelectedIp,
                                                 &u4IfIndex);
    UNUSED_PARAM (i4RetVal);
    IpIntfInfo.u4IfIndex = u4IfIndex;
    pIpIntf = RBTreeGet (gIpIfInfo.pIpIfTable, &IpIntfInfo);

  select_new_address:

    /* search in the current binding list */
    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pBinding->pNext)
    {
        if (pBinding->u4IpAddress == u4SelectedIp)
        {
            ++u4SelectedIp;
            goto select_new_address;
        }
    }
    /* search in the exclude ip address list */
    pExclude = pPool->pExcludeIpAddr;
    while (pExclude != NULL)
    {
        if ((pExclude->u4StartIpAddr <= u4SelectedIp)
            && (pExclude->u4EndIpAddr >= u4SelectedIp))
        {
            u4SelectedIp = pExclude->u4EndIpAddr + 1;
            goto select_new_address;
        }
        pExclude = pExclude->pNext;
    }

    /* Address configured for specific hosts should not be allocated
     * for dynamic entries. */
    for (pHost = gpDhcpHostEntry; pHost != NULL; pHost = pHost->pNext)
    {
        if (u4SelectedIp == pHost->u4StaticIpAddr)
        {
            ++u4SelectedIp;
            goto select_new_address;
        }
    }

    /* Address configured for Subnet option should not be allocated
     * gateway ip address */

    if ((pEntry = DhcpGetPoolEntry (pPool->u4PoolId)) != NULL)
    {
        if ((pOptions = DhcpGetOptionFromList (pEntry->pOptions,
                                               pEntry->pOptions->u4Type)) !=
            NULL)
        {
            for (; pOptions != NULL; pOptions = pOptions->pNext)
            {
                MEMCPY (&IpAddr.u4Addr, pOptions->u1Value, sizeof (INT4));
                IpAddr.u4Addr = OSIX_NTOHL (IpAddr.u4Addr);
                if ((pOptions->u4Type == DHCP_OPTION_DEF_GATEWAY) &&
                    (u4SelectedIp == IpAddr.u4Addr))
                {
                    ++u4SelectedIp;
                    goto select_new_address;
                }
            }
        }
    }

    /* If incoming ip is an network address, increment by one
     * example IP address 12.18.35.64
     * is unusable as a host address with subnet mask 255.255.255.224*/
    if (((pIpIntf != NULL) &&
         (u4CidrSubnetMask[pIpIntf->u1SubnetMask] & u4SelectedIp)) ==
        u4SelectedIp)
    {
        u4SelectedIp++;
        goto select_new_address;
    }

    /* Check if the selected IP is either the self IP or the broadcast IP */
    if (NetIpv4IfIsOurAddress (u4SelectedIp) == NETIPV4_SUCCESS ||
        ((pIpIntf != NULL) && (pIpIntf->u4BcastAddr == u4SelectedIp)))
    {
        u4SelectedIp++;
        goto select_new_address;
    }

    return ((u4SelectedIp <= pPool->u4EndIpAddr) ? u4SelectedIp : 0);
}

/********************************************************************************/
/*  Function Name   : DhcpGetBinding                                            */
/*  Description     : This funtion returns a matching entry in binding          */
/*                    table for the  binding structure with key fields      .   */
/*                                                                              */
/*  Input(s)        : pKey - binding structure with key fields filled           */
/*                     u1Hwtype    - hardware type of client                    */
/*                     aBindMac    - macaddress of client                       */
/*                     u1HostIdLen - Host id length                             */
/*                     u4PoolId    - pool id                                    */
/*                     u4xid       - transaction id                             */
/*                     u4IpAddress - Ipaddress                                  */
/*                                                                              */
/*                   i1Flag      - 1/0 ->enable/disable check for ip address    */
/*                                                                              */
/*  Output(s)       : Binding Entry(pEntry) from Binding Table,if a match is    */
/*                    found. NULL otherwise                                     */
/*  Returns         : DHCP_SUCCESS,if binding found,DHCP_FAILURE otherwise      */
/********************************************************************************/

INT1
DhcpGetBinding (tDhcpBinding ** pKey, INT1 i1Flag)
{
    INT1                i1Result = DHCP_FAILURE;
    tDhcpBinding       *pEntry = NULL;

    /* Scan the Binding Table and do the comparision based on the input 
       flag.If no specific comparision is needed,do the comparision                    based on MAC address only */
    for (pEntry = gpDhcpBinding; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if ((pEntry->u1HwType == (*pKey)->u1HwType) &&
            (MEMCMP ((*pKey)->aBindMac, pEntry->aBindMac,
                     MEM_MAX_BYTES (pEntry->u1HostIdLen, DHCP_MAX_CID_LEN))
             == 0))
        {
            if (i1Flag == DHCP_MAC_IPPORT)
            {
                /* Check if the mac-address and IP subnet(Port on which the 
                 * request is recieved) are matching with that of the 
                 * bindings */
                if ((*pKey)->u4Subnet == pEntry->u4Subnet)
                {
                    i1Result = DHCP_SUCCESS;
                    break;
                }
            }
            else if (i1Flag == DHCP_TRANS_ID)
            {
                if ((*pKey)->u4xid == pEntry->u4xid)
                {
                    i1Result = DHCP_SUCCESS;
                    break;
                }
            }
            else if (i1Flag == DHCP_IP_ADDRESS)
            {
                if ((pEntry->u4IpAddress == (*pKey)->u4IpAddress)
                    && (pEntry->u4PoolId == (*pKey)->u4PoolId))
                {
                    i1Result = DHCP_SUCCESS;
                    break;
                }
            }
            else if (i1Flag == DHCP_XID_AND_IP_ADDR)
            {
                if ((pEntry->u4IpAddress == (*pKey)->u4IpAddress)
                    && ((*pKey)->u4Subnet == pEntry->u4Subnet)
                    && ((pEntry->u4PoolId == (*pKey)->u4PoolId)))
                {
                    i1Result = DHCP_SUCCESS;
                    break;
                }
            }
            else
            {
                i1Result = DHCP_SUCCESS;
                break;
            }
        }
    }
    /* free the memory used by *pkey (Allocated when Fn is getting Called */
    DhcpFreeBindRec (*pKey);

    /*Update *pkey with the record from Binding Table */
    *pKey = pEntry;

    return (i1Result);

}

/************************************************************************/
/*  Function Name   : DhcpGetBindingFromAddress                         */
/*  Description     : This funtion returns an existing binding entry    */
/*                    for the specified ip address if present.          */
/*  Input(s)        : u4IpAddress - ip address of the binding.          */
/*  Output(s)       : None.                                             */
/*  Returns         : binding entry if present else NULL.               */
/************************************************************************/

tDhcpBinding       *
DhcpGetBindingFromAddress (UINT4 u4IpAddress)
{
    tDhcpBinding       *pEntry = NULL;

    for (pEntry = gpDhcpBinding; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if (pEntry->u4IpAddress == u4IpAddress)
            break;
    }

    return (pEntry);

}

/************************************************************************/
/*  Function Name   : DhcpCheckIpAddress                                */
/*  Description     : This funtion checks validity of the ip-address in */
/*                    the subnet pool entry.                            */
/*  Input(s)        : pPool       - Subnet pool entry.                  */
/*                    u4IpAddr    - ip address to be checked.           */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE.                        */
/************************************************************************/
INT4
DhcpCheckIpAddress (tDhcpPool * pPool, UINT4 u4IpAddr)
{
    tExcludeIpAddr     *pExclude;

    pExclude = pPool->pExcludeIpAddr;

    if (!DHCP_IN_RANGE (pPool->u4StartIpAddr, pPool->u4EndIpAddr, u4IpAddr))
        return DHCP_FAILURE;

    while (pExclude != NULL)
    {
        if ((pExclude->u4StartIpAddr <= u4IpAddr)
            && (pExclude->u4EndIpAddr >= u4IpAddr))
            return DHCP_FAILURE;

        pExclude = pExclude->pNext;
    }

    return DHCP_SUCCESS;
}
