/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsprot.c,v 1.33 2015/02/19 12:17:38 siva Exp $ 
 *
 * Description: This file contains functions handling DHCP protocol   
 *              messages.                      
 *******************************************************************/

#include "dhcpscom.h"
#include "dhcpstrap.h"

/************************************************************************/
/*  Function Name   : DhcpRcvdDiscover                                  */
/*  Description     : This function process incoming DHCP DISCOVER      */
/*                    packet.                                           */
/*  Input(s)        : pPktInfo - Received DHCP packet                   */
/*                    pPoolEntry - DHCP pool for the received interface */
/*                    pHostEntry - host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpRcvdDiscover (tDhcpPktInfo * pPktInfo, tDhcpPool * pPoolEntry,
                  tDhcpHostEntry * pHostEntry)
{

    /* Check whether free binding records are available */

    if (gpDhcpFreeBindRec == NULL)
    {
        DHCP_TRC (FAIL_TRC,
                  "Binding is full. Can not process the request now\n");
        ++gDhcpCounters.u4DhcpCountDroppedNotServingSubnet;
        return;
    }

    DhcpSendOffer (pPktInfo, pPoolEntry, pHostEntry);

}

/************************************************************************/
/*  Function Name   : DhcpRcvdRequest                                   */
/*  Description     : This function process incoming DHCP REQUEST       */
/*                    packet.                                           */
/*  Input(s)        : pPktInfo - Received DHCP packet                   */
/*                    pPoolEntry - DHCP pool for the received interface */
/*                    pHostEntry - host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpRcvdRequest (tDhcpPktInfo * pPktInfo, tDhcpPool * pPoolEntry,
                 tDhcpHostEntry * pHostEntry)
{
    tDhcpBinding       *pBinding = NULL;
    UINT1               u1RequestState;
    UINT4               u4SelectedAddr = 0;

    UINT4               u4ServerIp, u4RequestedIp;

    /* determine the DHCP request state - 
     * SELECTING/INIT-REBOOT/RENEWING/REBINDING
     * */

    u1RequestState = DhcpFindRequestState (pPktInfo, &u4ServerIp,
                                           &u4RequestedIp);

    switch (u1RequestState)
    {
        case DHCP_SELECTING:

            pBinding =
                DhcpGetBindingFromPkt (pPktInfo, u4RequestedIp,
                                       DHCP_XID_AND_IP_ADDR);

            DHCP_TRC (EVENT_TRC, "DHCP REQUEST in SELECTING state\n");

            if (NetIpv4IfIsOurAddress (u4ServerIp) == NETIPV4_FAILURE)
            {
                DHCP_TRC (EVENT_TRC, "Client has declined the offer..\n");
                return;
            }

            if ((pBinding == NULL) || (pBinding->u4IpAddress != u4RequestedIp))
            {
                /* here we have 2 conditions:
                 * [1] request came very late. 
                 * [2] Invalid request.
                 * */
                DHCPSendNAK (pPktInfo);
                ++gDhcpCounters.u4DhcpCountDroppedUnKnownClient;
                return;
            }
            /* send DHCPACK with current binding address */
            u4SelectedAddr = pBinding->u4IpAddress;
            break;

        case DHCP_INIT_REBOOT:

            DHCP_TRC (EVENT_TRC, "DHCP REQUEST in INIT-REBOOT state\n");

            /* If a server receives a DHCPREQUEST message with an 
             * invalid 'requested IP address', the server SHOULD 
             * respond to the client with a DHCPNAK
             * */

            if ((u4ServerIp != 0) || (u4RequestedIp == 0)
                || (pPktInfo->DhcpMsg.ciaddr != 0))
            {
                DHCP_TRC (FAIL_TRC, "Invalid DHCP Request in state\n");
                ++gDhcpCounters.u4DhcpCountDroppedUnKnownClient;
                return;
            }

            pBinding =
                DhcpGetBindingFromPkt (pPktInfo, u4RequestedIp,
                                       DHCP_IP_ADDRESS);
            if (pBinding == NULL)
            {
                DHCP_TRC1 (FAIL_TRC,
                           "Binding is NULL for %x !!!\r\n", u4RequestedIp);
                DHCPSendNAK (pPktInfo);
                return;
            }
            u4SelectedAddr = pBinding->u4IpAddress;
            if (NetIpv4IfIsOurAddress (u4SelectedAddr) == NETIPV4_SUCCESS)
            {
                DhcpDeleteBinding (pBinding);
                DHCPSendNAK (pPktInfo);
                return;
            }

            if (((pHostEntry != NULL) && (pHostEntry->u4StaticIpAddr != 0) &&
                 (pHostEntry->u4StaticIpAddr != u4SelectedAddr))
                || (DhcpCheckIpAddress (pPoolEntry, u4SelectedAddr)
                    == DHCP_FAILURE))
            {
                /* client's IP is in exclude pool. */
                DhcpDeleteBinding (pBinding);
                DHCPSendNAK (pPktInfo);
                return;
            }
            break;

        case DHCP_RENEWING:
        case DHCP_REBINDING:
            pBinding =
                DhcpGetBindingFromPkt (pPktInfo, pPktInfo->DhcpMsg.ciaddr,
                                       DHCP_IP_ADDRESS);
            if ((u4ServerIp != 0) || (u4RequestedIp != 0)
                || (pPktInfo->DhcpMsg.ciaddr == 0))
            {
                DHCP_TRC (FAIL_TRC,
                          "Invalid Request in RENEWING/REBINDING state\n");
                ++gDhcpCounters.u4DhcpCountDroppedUnKnownClient;
                return;
            }
            if (pBinding == NULL)
            {
                DHCP_TRC1 (FAIL_TRC,
                           "Binding is NULL for %x !!!\r\n", u4RequestedIp);
                DHCPSendNAK (pPktInfo);
                return;
            }
            u4SelectedAddr = pBinding->u4IpAddress;
            if (pPoolEntry->pExcludeIpAddr != NULL)
            {
                if (DHCP_IN_RANGE (pPoolEntry->pExcludeIpAddr->u4StartIpAddr,
                                   pPoolEntry->pExcludeIpAddr->u4EndIpAddr,
                                   u4SelectedAddr))
                {
                    DHCP_TRC (FAIL_TRC,
                              "Sending NAK & Deleting Binding since Request Ip is in Exclude Pool.\r\n");

                    /* client's IP is in exclude pool. */
                    DhcpDeleteBinding (pBinding);
                    DHCPSendNAK (pPktInfo);
                    return;
                }
            }

            if (((pHostEntry != NULL) && (pHostEntry->u4StaticIpAddr != 0) &&
                 (pHostEntry->u4StaticIpAddr != u4SelectedAddr))
                || (DhcpCheckIpAddress (pPoolEntry, u4SelectedAddr)
                    == DHCP_FAILURE))
            {
                return;
            }
            break;
        default:
            return;
    }

    if (DHCPSendACK
        (pPktInfo, pHostEntry, pPoolEntry, u1RequestState,
         u4SelectedAddr) == DHCP_SUCCESS)
    {
        DHCP_TRC1 (EVENT_TRC, "Sending DHCP ACK with addr %x\n",
                   u4SelectedAddr);

        /* for RENEWING/REBINDING/INIT_REBOOT and SELECTING state we are 
         * adopting the same policy now... */
        /* calculate the expiry time from the lease duration */
        pBinding->u4LeaseExprTime = DhcpLeaseExprTime (pPoolEntry->u4LeaseTime);

        /* mark the binding entry as assigned */
        pBinding->u1Mask = DHCP_ASSIGNED;

        /* Inform about the binding to standby peers */
        DhcpSRedSendDynamicBindingInfo (pBinding);

    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpRcvdRelease                                   */
/*  Description     : This function process incoming DHCP RELEASE       */
/*                    packet.                                           */
/*  Input(s)        : pPktInfo - Received DHCP packet                   */
/*                    pPoolEntry - DHCP pool for the received interface */
/*                    pHostEntry - host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpRcvdRelease (tDhcpPktInfo * pPktInfo, tDhcpPool * pPoolEntry,
                 tDhcpHostEntry * pHostEntry)
{
    tDhcpBinding       *pBinding = NULL;

    /*get binding using the 'ciaddr' field */
    if ((pBinding =
         DhcpGetBindingFromPkt (pPktInfo, pPktInfo->DhcpMsg.ciaddr,
                                DHCP_IP_ADDRESS)) == NULL)
    {
        ++gDhcpCounters.u4DhcpCountDroppedUnKnownClient;
        return;
    }

    if (pHostEntry)
        pHostEntry->u4AllocOrFree = DHCP_FREE;

    pPoolEntry->u4BoundIpCount--;
    DhcpRemoveBinding (pBinding);
    DhcpFreeBindRec (pBinding);
    return;

}

/************************************************************************/
/*  Function Name   : DhcpRcvdDecline                                   */
/*  Description     : This function process incoming DHCP DECLINE       */
/*                    packet.                                           */
/*  Input(s)        : pPktInfo - Received DHCP packet                   */
/*                    pPoolEntry - DHCP pool for the received interface */
/*                    pHostEntry - host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpRcvdDecline (tDhcpPktInfo * pPktInfo, tDhcpPool * pPoolEntry,
                 tDhcpHostEntry * pHostEntry)
{
    tDhcpBinding       *pBinding = NULL;
    UINT4               u4ServerIp, u4RequestedIp;

    if (DhcpGetOption (DHCP_OPT_REQUESTED_IP, pPktInfo) != DHCP_FOUND)
        return;

    MEMCPY (&u4RequestedIp, DhcpTag.Val, 4);
    u4RequestedIp = OSIX_NTOHL (u4RequestedIp);

    if (DhcpGetOption (DHCP_OPT_SERVER_ID, pPktInfo) != DHCP_FOUND)
        return;
    MEMCPY (&u4ServerIp, DhcpTag.Val, 4);
    u4ServerIp = OSIX_NTOHL (u4ServerIp);

    if (NetIpv4IfIsOurAddress (u4ServerIp) == NETIPV4_FAILURE)
    {
        /* not for us */
        return;
    }

    pHostEntry = DhcpGetHostFromPkt (pPktInfo, pPoolEntry->u4PoolId);
    if (pHostEntry)
        pHostEntry->u4AllocOrFree = DHCP_FREE;

    /* get binding using the requested ip address */
    pBinding = DhcpGetBindingFromPkt (pPktInfo, u4RequestedIp, DHCP_IP_ADDRESS);

    if ((pBinding == NULL) || (pBinding->u4IpAddress != u4RequestedIp))
        return;

    pPoolEntry->u4BoundIpCount--;
    DhcpRemoveBinding (pBinding);
    DhcpFreeBindRec (pBinding);
    DhcpNotifyWrongAddress (pPoolEntry, pBinding->u4IpAddress);
    return;

}

/************************************************************************/
/*  Function Name   : DhcpRcvdInform                                    */
/*  Description     : This function process incoming DHCP INFORM        */
/*                    packet.                                           */
/*  Input(s)        : pPktInfo - Received DHCP packet                   */
/*                    pPoolEntry - DHCP pool for the received interface */
/*                    pHostEntry - host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpRcvdInform (tDhcpPktInfo * pPktInfo, tDhcpPool * pPoolEntry,
                tDhcpHostEntry * pHostEntry)
{
    DHCPSendACK (pPktInfo, pHostEntry, pPoolEntry, 0, 0);
}

/************************************************************************/
/*  Function Name   : DhcpRcvdBootp                                     */
/*  Description     : This function process incoming BOOTP              */
/*                    packet.                                           */
/*  Input(s)        : pPktInfo - Received DHCP packet                   */
/*                    pPoolEntry - DHCP pool for the received interface */
/*                    pHostEntry - host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpRcvdBootp (tDhcpPktInfo * pPktInfo, tDhcpPool * pPoolEntry,
               tDhcpHostEntry * pHostEntry)
{
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr;
    tDhcpMsgHdr        *pRcvdHdr;
    tDhcpBinding       *pBinding;
    const UINT1        *pu1MyName;
    UINT4               u4IpAddress = 0, u4ServerIpAddr = 0;
    UINT1              *pFileName = NULL;
    UINT4               u4Temp = 0;
    UINT4               u4SeverIdentifier = 0;
    UINT4               u4Len = 0;
    INT1                i1Flag = 0;

    if (gu1DhcpBootpClientSupported == DHCP_DISABLED)
    {
        DHCP_TRC (FAIL_TRC, "Bootp clients are not supported now\n");
        return;
    }

    pu1MyName = DhcpGetSysName ();

    u4SeverIdentifier = DhcpGetServerIdentifier (pPktInfo->u4IfIndex);
    if ((STRLEN (pPktInfo->DhcpMsg.sname) != 0)
        && (STRCMP (pPktInfo->DhcpMsg.sname, pu1MyName) != 0))
    {
        DHCP_TRC (FAIL_TRC,
                  "Server name does not match. Droping the request\n");
        return;
    }

    if (pPktInfo->DhcpMsg.ciaddr == 0)
    {
        /* select an ip-address for the client */

        if ((pHostEntry != NULL) && (pHostEntry->u4StaticIpAddr != 0))
        {
            u4IpAddress = pHostEntry->u4StaticIpAddr;
        }
        else
        {
            if (gu1DhcpAutomaticBootpEnabled == DHCP_DISABLED)
                return;
            u4IpAddress = DhcpSelectIpAddress (pPoolEntry, 0);
        }
    }
    else
    {
        u4IpAddress = pPktInfo->DhcpMsg.ciaddr;
    }

    pFileName = DhcpGetBootFileName (pHostEntry, pPktInfo->DhcpMsg.file);

    if (pFileName == NULL)
    {
        DHCP_TRC (FAIL_TRC,
                  "Client is asking for a file we don't have.Dropping.\n");
        return;
    }

    u4ServerIpAddr = pHostEntry ? pHostEntry->u4BootSrvIpAddr :
        gDhcpBootServerAddress;

    if ((pBinding =
         DhcpGetBindingFromPkt (pPktInfo, 0, DHCP_MAC_IPPORT)) == NULL)
    {
        if ((pBinding = DhcpGetFreeBindRec ()) == NULL)
        {
            DHCP_TRC (FAIL_TRC, "No Binding Records available\n");
            return;
        }
    }
    else
    {
        u4IpAddress = pBinding->u4IpAddress;
        i1Flag = ENTRY_ALREADY_EXISTS;
    }
    if ((pOutPkt = DhcpCreateOutPkt ()) == NULL)
    {
        return;
    }

    /* fill the fixed header */
    pOutHdr = &(pOutPkt->DhcpMsg);
    pRcvdHdr = &(pPktInfo->DhcpMsg);

    pOutHdr->Op = BOOTREPLY;
    pOutHdr->htype = pRcvdHdr->htype;
    pOutHdr->hlen = pRcvdHdr->hlen;
    pOutHdr->hops = 0;
    pOutHdr->xid = pRcvdHdr->xid;
    pOutHdr->secs = 0;
    pOutHdr->flags = pRcvdHdr->flags;
    pOutHdr->ciaddr = pRcvdHdr->ciaddr;
    pOutHdr->yiaddr = u4IpAddress;
    pOutHdr->siaddr = u4ServerIpAddr;
    pOutHdr->giaddr = pRcvdHdr->giaddr;

    MEMCPY (pOutHdr->chaddr, pRcvdHdr->chaddr, 16);
    STRNCPY (pOutHdr->sname, pRcvdHdr->sname, STRLEN(pRcvdHdr->sname));
    pOutHdr->sname[STRLEN(pRcvdHdr->sname)] = '\0';
    u4Len =
        ((STRLEN (pFileName) <
          sizeof (pOutHdr->file)) ? STRLEN (pFileName) : sizeof (pOutHdr->
                                                                 file) - 1);
    STRNCPY (pOutHdr->file, pFileName, u4Len);
    pOutHdr->file[u4Len] = '\0';

    /* Add server identifier option */
    DhcpTag.Type = DHCP_OPT_SERVER_ID;
    DhcpTag.Len = 4;
    u4Temp = OSIX_HTONL (u4SeverIdentifier);
    DhcpTag.Val = (UINT1 *) &u4Temp;
    DhcpAddOption (pOutPkt, &DhcpTag);

    DhcpAddParamsListOptions (pOutPkt, pPktInfo, pPoolEntry, pHostEntry);
    DhcpAddConfiguredOptions (pOutPkt, pPktInfo, pPoolEntry, pHostEntry);
#ifdef BSDCOMP_SLI_WANTED
    pOutPkt->u4IfIndex = pPktInfo->u4IfIndex;
#endif

    if (DhcpSendMessage (pOutPkt, pRcvdHdr->ciaddr, BOOTP_PKT) == DHCP_SUCCESS)
    {
        if (i1Flag != ENTRY_ALREADY_EXISTS)
        {
            DHCP_TRC (ALL_TRC, "BOOTP Reply sent successfully.\n");

            pBinding->u4Subnet = pPoolEntry->u4Subnet;

            pBinding->u4IpAddress = u4IpAddress;
            pBinding->u4Subnet = pPoolEntry->u4Subnet;

            pBinding->u4LeaseExprTime = 0xffffffff;

            pBinding->u4PoolId = pPoolEntry->u4PoolId;

            pBinding->u4xid = pOutHdr->xid;

            pBinding->u1Mask = DHCP_BOOTP;

            if (pHostEntry != NULL)
            {
                pHostEntry->u4AllocOrFree = DHCP_ASSIGNED;
                pBinding->u1HwType = pHostEntry->u1HwType;
                pBinding->u1HostIdLen = pHostEntry->u1HostIdLen;
                MEMCPY (pBinding->aBindMac, pHostEntry->aClientMac,
                        DHCP_MAX_CID_LEN);
                pBinding->u4AllocMethod = DHCP_ALLOC_MANUAL;
            }
            else
            {
                if (DhcpGetOption (DHCP_OPT_CLIENT_ID, pPktInfo) == DHCP_FOUND)
                {
                    pBinding->u1HwType = pPktInfo->DhcpMsg.htype;
                    MEMCPY (pBinding->aBindMac, DhcpTag.Val, DhcpTag.Len);
                    pBinding->u1HostIdLen = DhcpTag.Len;
                }
                else
                {
                    pBinding->u1HwType = pPktInfo->DhcpMsg.htype;
                    MEMCPY (pBinding->aBindMac, pPktInfo->DhcpMsg.chaddr,
                            MEM_MAX_BYTES (pPktInfo->DhcpMsg.hlen, 16));
                    pBinding->u1HostIdLen = pPktInfo->DhcpMsg.hlen;
                }

                pBinding->u4AllocMethod = DHCP_ALLOC_DYNAMIC;
            }
            /* add to the binding list */
            DhcpAddBinding (pBinding);
            pPoolEntry->u4BoundIpCount++;
            DhcpSCheckThreshold (pPoolEntry);
        }
    }
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);

    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId, (UINT1 *) pOutPkt);
    DhcpTag.Val = NULL;
    return;
}

/************************************************************************/
/*  Function Name   : DhcpGetBindingFromPkt                             */
/*  Description     : This function returns the binding entry for the   */
/*                  : received DHCP Message if present.                 */
/*  Input(s)        : pPkt - received DHCP packet                       */
/*                    u4IpAddr - Ip Address of the binding entry        */
/*                    i1Flag - Flag indicating ip-address checking.     */
/*  Output(s)       : None.                                             */
/*  Returns         : Binding entry if present else  NULL               */
/************************************************************************/

tDhcpBinding       *
DhcpGetBindingFromPkt (tDhcpPktInfo * pPkt, UINT4 u4IpAddr, INT1 i1Flag)
{
    tDhcpPool          *pPoolEntry;
    tDhcpBinding       *pBinding = NULL;

    /*create a binding structure and fill search key fields */
    pBinding = DhcpGetFreeBindRec ();

    if (pBinding == NULL)
    {
        DHCP_TRC (FAIL_TRC, "No Binding Records available\n");
        return (NULL);
    }

    /* get the pool Id from  IfIndex */
    pPoolEntry = DhcpGetPoolEntryFromPkt (pPkt);
    if (pPoolEntry == NULL)
        return NULL;

    pBinding->u4PoolId = pPoolEntry->u4PoolId;
    pBinding->u4Subnet = pPoolEntry->u4Subnet;
    pBinding->u4IpAddress = u4IpAddr;
    pBinding->u4Subnet = pPoolEntry->u4Subnet;
    pBinding->u4xid = pPkt->DhcpMsg.xid;
    /* check for client identifier option */
    if (DhcpGetOption (DHCP_OPT_CLIENT_ID, pPkt) == DHCP_FOUND)
    {
        /* Key is Client identifier */
        pBinding->u1HwType = pPkt->DhcpMsg.htype;
        MEMCPY (pBinding->aBindMac, DhcpTag.Val,
                MEM_MAX_BYTES (DhcpTag.Len, DHCP_MAX_CID_LEN));
        pBinding->u1HostIdLen = DhcpTag.Len;
    }
    else
    {

        /* Key is Client HwAddress */
        pBinding->u1HwType = pPkt->DhcpMsg.htype;
        MEMCPY (pBinding->aBindMac, pPkt->DhcpMsg.chaddr,
                MEM_MAX_BYTES (pPkt->DhcpMsg.hlen, 16));
        pBinding->u1HostIdLen = pPkt->DhcpMsg.hlen;

    }

    /* After calling the function DhcpGetBinding, pBinding will be updated with
       the pointer to record in the binding table(Corresponding to the Key).
       Memory allocated for pBinding in this function (DhcpProcessIcmpProbeTimeOut())
       will get freed inside DhcpGetBinding function */

    if (DhcpGetBinding (&pBinding, i1Flag) == DHCP_FAILURE)
    {
        DHCP_TRC (FAIL_TRC, "No current Binding for this packet\n");
    }
    return (pBinding);
}

/************************************************************************/
/*  Function Name   : DhcpGetHostFromPkt                                */
/*  Description     : This function returns the host entry for the      */
/*                  : received DHCP Message if present.                 */
/*  Input(s)        : pPkt - received DHCP packet                       */
/*                    u4PoolId -  Pool Id for the received dhcp message */
/*  Output(s)       : None.                                             */
/*  Returns         : host entry if present else  NULL                  */
/************************************************************************/

tDhcpHostEntry     *
DhcpGetHostFromPkt (tDhcpPktInfo * pPkt, UINT4 u4PoolId)
{
    tDhcpHostEntry     *pHost = NULL;

    /* check for client identifier option */
    if (DhcpGetOption (DHCP_OPT_CLIENT_ID, pPkt) == DHCP_FOUND)
    {
        /* Key is client identifier */
        pHost = DhcpGetHostEntry (pPkt->DhcpMsg.htype,
                                  DhcpTag.Val, DhcpTag.Len, u4PoolId);
    }
    else
    {
        /* Key is Client HwAddress */
        pHost =
            DhcpGetHostEntry (pPkt->DhcpMsg.htype, pPkt->DhcpMsg.chaddr,
                              pPkt->DhcpMsg.hlen, u4PoolId);
    }
    return pHost;
}

/************************************************************************/
/*  Function Name   : DhcpSendOffer                                     */
/*  Description     : This function send a DHCP offer for a received    */
/*                    dhcp discover message, if possible. Binding entry */
/*                    is added . If u1IcmpFlag is set dhcp-offer is sent*/
/*                    directly to the client ,otherwise selected address*/
/*                    is probed for consistency                         */
/*  Input(s)        : pRcvdPKt - received DHCP packet                   */
/*                    pPoolEntry - pool entry for the received message  */
/*                    pHostEntry - host specific entry ,if present      */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpSendOffer (tDhcpPktInfo * pRcvdPkt, tDhcpPool * pPoolEntry,
               tDhcpHostEntry * pHostEntry)
{
    UINT4               u4Temp;
    UINT4               u4SeverIdentifier;
    UINT4               u4IpAddr = 0;
    UINT4               u4RequestedIp = 0;
    UINT4               u4Len = 0;
    UINT1               u1IcmpFlag;
    INT4                i4RetVal = DHCP_SUCCESS;

    UINT1              *pFileName;
    tDhcpMsgHdr        *pOutHdr;
    tDhcpMsgHdr        *pRcvdHdr;
    tDhcpOptions       *pOption;
    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpBinding       *pBinding = NULL;

    if ((pOutPkt = DhcpCreateOutPkt ()) == NULL)
    {
        return;
    }

    pOutPkt->u4IfIndex = pRcvdPkt->u4IfIndex;
    pOutPkt->u2MaxMessageSize = pRcvdPkt->u2MaxMessageSize;
    pOutPkt->u1OverLoad = 0;

    u4SeverIdentifier = DhcpGetServerIdentifier (pRcvdPkt->u4IfIndex);

    u1IcmpFlag = (gi1DhcpsIcmpEchoEnable == DHCP_ENABLED) ? 1 : 0;

    if ((pBinding = DhcpGetBindingFromPkt (pRcvdPkt, 0, DHCP_MAC_IPPORT)))
    {
        /* Delete the existing Binding for the client */
        if (pBinding->u1Mask == DHCP_PROBING)
        {
            /* Remove the probing node from the list. We will be probing
             * it again. */
            DhcpStopProbingAddress (pBinding->u4IpAddress);
        }
        else
            u1IcmpFlag = 0;

        /* Try to give the same ip-address again. */
        u4RequestedIp = pBinding->u4IpAddress;

        DhcpDeleteBinding (pBinding);
    }

    if ((pHostEntry != NULL) && (pHostEntry->u4StaticIpAddr != 0))
    {
        u1IcmpFlag = 0;
        u4IpAddr = pHostEntry->u4StaticIpAddr;
    }
    else
    {
        if (DhcpGetOption (DHCP_OPT_REQUESTED_IP, pRcvdPkt) == DHCP_FOUND)
        {
            MEMCPY (&u4RequestedIp, DhcpTag.Val, 4);
            u4RequestedIp = OSIX_NTOHL (u4RequestedIp);
            u4IpAddr = DhcpSelectIpAddress (pPoolEntry, u4RequestedIp);
        }
        else
        {
            u4IpAddr = DhcpSelectIpAddress (pPoolEntry, u4RequestedIp);
        }

        if (u4IpAddr == 0)
        {
            /* Unable to get any free IP address */

            DHCP_TRC (FAIL_TRC, "No free address available in the pool\n");
            ++gDhcpCounters.u4DhcpCountDroppedNotServingSubnet;
            MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                                (UINT1 *) pOutPkt->DhcpMsg.pOptions);

            MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId,
                                (UINT1 *) pOutPkt);
            return;
        }

        DHCP_TRC2 (EVENT_TRC, "Offering address %x from pool %d\n",
                   u4IpAddr, pPoolEntry->u4PoolId);

        if (u4IpAddr != u4RequestedIp)
        {
            u1IcmpFlag = (gi1DhcpsIcmpEchoEnable == DHCP_ENABLED) ? 1 : 0;
        }

    }

    /* fill the fixed header */
    pOutHdr = &(pOutPkt->DhcpMsg);
    pRcvdHdr = &(pRcvdPkt->DhcpMsg);

    pOutHdr->Op = BOOTREPLY;
    pOutHdr->htype = pRcvdHdr->htype;
    pOutHdr->hlen = pRcvdHdr->hlen;
    pOutHdr->hops = 0;
    pOutHdr->xid = pRcvdHdr->xid;
    pOutHdr->secs = 0;
    pOutHdr->flags = pRcvdHdr->flags;
    pOutHdr->ciaddr = 0;
    pOutHdr->yiaddr = u4IpAddr;
    pOutHdr->siaddr = pHostEntry ? pHostEntry->u4BootSrvIpAddr :
        gDhcpBootServerAddress;
    pOutHdr->giaddr = pRcvdHdr->giaddr;

    MEMCPY (pOutHdr->chaddr, pRcvdHdr->chaddr, 16);
    /* set boot server hostname and boot file name as NULL */

    MEMSET (pOutHdr->sname, 0, 64);
    /* If the tftp server name option is configured add it in the 
     * sname field. */
    if ((pOption = DhcpGetCofiguredOption (pHostEntry, pPoolEntry,
                                           DHCP_OPT_TFTP_SNAME)) != NULL)
    {
        MEMCPY (pOutHdr->sname, pOption->u1Value, pOption->u4Len);
    }

    MEMSET (pOutHdr->file, 0, 128);
    if ((pFileName = DhcpGetBootFileName (pHostEntry, pRcvdHdr->file)) != NULL)
    {
        u4Len =
            ((STRLEN (pFileName) <
              sizeof (pOutHdr->file)) ? STRLEN (pFileName) : sizeof (pOutHdr->
                                                                     file) - 1);
        STRNCPY (pOutHdr->file, pFileName, u4Len);
        pOutHdr->file[u4Len] = '\0';
    }

    /* Add Lease time option */
    DhcpTag.Type = DHCP_OPT_LEASE_TIME;
    DhcpTag.Len = 4;
    u4Temp = OSIX_HTONL (pPoolEntry->u4LeaseTime);
    DhcpTag.Val = (UINT1 *) &u4Temp;
    DhcpAddOption (pOutPkt, &DhcpTag);

    /* Add server identifier option */
    DhcpTag.Type = DHCP_OPT_SERVER_ID;
    DhcpTag.Len = 4;
    u4Temp = OSIX_HTONL (u4SeverIdentifier);
    DhcpTag.Val = (UINT1 *) &u4Temp;
    DhcpAddOption (pOutPkt, &DhcpTag);

    DhcpAddParamsListOptions (pOutPkt, pRcvdPkt, pPoolEntry, pHostEntry);
    DhcpAddConfiguredOptions (pOutPkt, pRcvdPkt, pPoolEntry, pHostEntry);

    if (u1IcmpFlag == 1)
    {
        DHCP_TRC1 (EVENT_TRC, "probing address %x with icmp request\n",
                   u4IpAddr);
        i4RetVal = DhcpProbeAddressAndSendOffer (u4IpAddr, pOutPkt,
                                                 pRcvdPkt, pPoolEntry);
    }
    else
    {
        i4RetVal = DhcpSendMessage (pOutPkt, pRcvdHdr->ciaddr, DHCP_OFFER);
        /* free OutPkt Message Structure */

        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                            (UINT1 *) pOutPkt->DhcpMsg.pOptions);

        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId, (UINT1 *) pOutPkt);
    }

    if (i4RetVal == DHCP_SUCCESS)
    {

        pBinding = DhcpGetFreeBindRec ();
        if (pBinding == NULL)
        {
            DHCP_TRC (FAIL_TRC, "No Binding Records available\n");
            DhcpTag.Val = NULL;
            return;
        }

        pBinding->u4IpAddress = u4IpAddr;

        /* calculate the offer reuse time */
        /* bindings entries int the DHCP_PROBING states will be cleared
         * when the Dhcp icmp timer expires
         * */
        pBinding->u4LeaseExprTime = DhcpLeaseExprTime (gu4DhcpOfferTimeOut);

        pBinding->u4PoolId = pPoolEntry->u4PoolId;

        pBinding->u4Subnet = pPoolEntry->u4Subnet;

        pBinding->u1Mask = (u1IcmpFlag == 0) ? DHCP_OFFERED : DHCP_PROBING;

        pBinding->u4xid = pRcvdPkt->DhcpMsg.xid;

        if (pHostEntry != NULL)
        {
            pHostEntry->u4AllocOrFree = DHCP_ASSIGNED;
            pBinding->u1HwType = pHostEntry->u1HwType;
            pBinding->u1HostIdLen = pHostEntry->u1HostIdLen;
            MEMCPY (pBinding->aBindMac, pHostEntry->aClientMac,
                    DHCP_MAX_CID_LEN);
            pBinding->u4AllocMethod = DHCP_ALLOC_MANUAL;
        }
        else
        {
            if (DhcpGetOption (DHCP_OPT_CLIENT_ID, pRcvdPkt) == DHCP_FOUND)
            {
                pBinding->u1HwType = pRcvdPkt->DhcpMsg.htype;
                MEMCPY (pBinding->aBindMac, DhcpTag.Val, DhcpTag.Len);
                pBinding->u1HostIdLen = DhcpTag.Len;
            }
            else
            {
                pBinding->u1HwType = pRcvdPkt->DhcpMsg.htype;
                MEMCPY (pBinding->aBindMac, pRcvdPkt->DhcpMsg.chaddr,
                        MEM_MAX_BYTES (pRcvdPkt->DhcpMsg.hlen, 16));
                pBinding->u1HostIdLen = pRcvdPkt->DhcpMsg.hlen;
            }

            pBinding->u4AllocMethod = DHCP_ALLOC_DYNAMIC;
        }
        /* add to the binding list */
        DhcpAddBinding (pBinding);
        pPoolEntry->u4BoundIpCount++;
        DhcpSCheckThreshold (pPoolEntry);
    }

    DhcpTag.Val = NULL;
}

/************************************************************************/
/*  Function Name   : DhcpAddParamsListOptions                          */
/*  Description     : This function adds the requested options in the   */
/*                  : 'parmeter request list' option (if present).      */
/*  Input(s)        : pOut - DHCP out packet                            */
/*                    pIn - DHCP received packet                        */
/*                    pPoolEntry - Pool Enrtry for the received Message */
/*                    pHostEntry - Host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpAddParamsListOptions (tDhcpPktInfo * pOut, tDhcpPktInfo * pIn,
                          tDhcpPool * pPoolEntry, tDhcpHostEntry * pHostEntry)
{
    UINT1               u1ReqType;

    /* check for parameter request list */

    if (DhcpGetOption (DHCP_OPT_PARAMETER_LIST, pIn) == DHCP_NOT_FOUND)
    {
        return;
    }
    else
    {
        tDhcpOptions       *pOptions = NULL;
        tDhcpTag            TempTag;
        UINT1               OffSet = 0;

        while (OffSet <= DhcpTag.Len)
        {
            pOptions = NULL;

            u1ReqType = DhcpTag.Val[OffSet];

            if ((pHostEntry == NULL) ||
                ((pHostEntry != NULL) &&
                 (pOptions =
                  DhcpGetActiveOptionFromList (pHostEntry->pOptions,
                                               u1ReqType)) == NULL))
            {
                if ((pOptions =
                     DhcpGetActiveOptionFromList (pPoolEntry->pOptions,
                                                  u1ReqType)) == NULL)
                {
                    pOptions =
                        DhcpGetActiveOptionFromList (gpDhcpOptions, u1ReqType);
                }
            }

            if (pOptions != NULL)
            {
                TempTag.Type = (UINT1) pOptions->u4Type;
                TempTag.Len = (UINT1) pOptions->u4Len;
                TempTag.Val = (UINT1 *) pOptions->u1Value;
                DhcpAddOption (pOut, &TempTag);
            }
            ++OffSet;
        }                        /* while */
    }
    return;
}

/************************************************************************/
/*  Function Name   : DhcpAddConfiguredOptions                          */
/*  Description     : This function adds the configured options to the  */
/*                  : outgoing dhcp-message if the option is requested  */
/*                  : by the clinet                                     */
/*  Input(s)        : pOut - DHCP out packet                            */
/*                    pIn - DHCP received packetol                      */
/*                    pPoolEntry - Pool Enrtry for the received Message */
/*                    pHostEntry - Host specific entry if present       */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpAddConfiguredOptions (tDhcpPktInfo * pOut, tDhcpPktInfo * pIn,
                          tDhcpPool * pPoolEntry, tDhcpHostEntry * pHostEntry)
{
    tDhcpOptions       *pOptions = NULL;
    tDhcpTag            TempTag;
    UINT2               OffSet = 0;
    UINT1              *pRcvdOptions;
    UINT1               u1ReqType, u1Len;

    pRcvdOptions = pIn->DhcpMsg.pOptions;

    while (OffSet < pIn->u2Len)
    {
        u1ReqType = pRcvdOptions[OffSet];    /* Type of the option */
        u1Len = pRcvdOptions[OffSet + 1];    /* length of the option */

        pOptions = NULL;

        u1ReqType = pIn->DhcpMsg.pOptions[OffSet];

        if (u1ReqType == DHCP_OPT_PAD)
        {
            ++OffSet;
            continue;            /* ignore PAD options */
        }

        if (u1ReqType == DHCP_OPT_END)
            break;                /* no more options after end_opt */

        if ((pHostEntry == NULL) ||
            ((pHostEntry != NULL) &&
             (pOptions =
              DhcpGetActiveOptionFromList (pHostEntry->pOptions,
                                           u1ReqType)) == NULL))
        {
            if ((pOptions = DhcpGetActiveOptionFromList (pPoolEntry->pOptions,
                                                         u1ReqType)) == NULL)
            {
                pOptions =
                    DhcpGetActiveOptionFromList (gpDhcpOptions, u1ReqType);
            }
        }

        if (pOptions != NULL)
        {
            TempTag.Type = (UINT1) pOptions->u4Type;
            TempTag.Len = (UINT1) pOptions->u4Len;
            TempTag.Val = (UINT1 *) pOptions->u1Value;
            DhcpAddOption (pOut, &TempTag);
        }
        /* Take the next option */
        OffSet = (UINT2) (OffSet + (u1Len + DHCP_LEN_TYPE_LEN));

    }                            /* while */

    if (pHostEntry == NULL)
        return;

    /* If there is any host specific options configured add to the 
     * dhcp reply. DhcpAddOption will take care of avoiding already
     * configured options.
     * */
    for (pOptions = pHostEntry->pOptions; pOptions != NULL;
         pOptions = pOptions->pNext)
    {
        TempTag.Type = (UINT1) pOptions->u4Type;
        TempTag.Len = (UINT1) pOptions->u4Len;
        TempTag.Val = (UINT1 *) pOptions->u1Value;
        DhcpAddOption (pOut, &TempTag);
    }

    /* Add subnet specific options configured */
    for (pOptions = pPoolEntry->pOptions; pOptions != NULL;
         pOptions = pOptions->pNext)
    {
        TempTag.Type = (UINT1) pOptions->u4Type;
        TempTag.Len = (UINT1) pOptions->u4Len;
        TempTag.Val = (UINT1 *) pOptions->u1Value;
        DhcpAddOption (pOut, &TempTag);
    }

    return;

}

/************************************************************************/
/*  Function Name   : DhcpValidateBootRequest                           */
/*  Description     : This function validates each received packet      */
/*                    based on message type as per RFC 2131.            */
/*  Input(s)        : pPktInfo - Local structure containing information */
/*                               about received packet                  */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE based on validation  */
/************************************************************************/
INT4
DhcpValidateBootRequest (tDhcpPktInfo * pPktInfo)
{
    INT4                i4RetVal = DHCP_SUCCESS;
    tMacAddr            ZeroAddr;
    tMacAddr            BcastMacAddr;

    /* Verification is according to RFC 2131 section 4.4.1 Table 5. */

    if (pPktInfo->DhcpMsg.hlen != DHCP_CHADDR_MAC_LEN)
    {
        DHCP_TRC (FAIL_TRC, "Unsupported client hardware address length\n");
        gDhcpCounters.u4DhcpCountInvalids++;
        return DHCP_FAILURE;

    }

    MEMSET (ZeroAddr, 0, DHCP_CHADDR_MAC_LEN);
    MEMSET (BcastMacAddr, 0xff, DHCP_CHADDR_MAC_LEN);

    /* The client hardware address should not be zero or broadcast
     * address and should not have reserved bit set*/
    if ((!MEMCMP (pPktInfo->DhcpMsg.chaddr, ZeroAddr,
                  DHCP_CHADDR_MAC_LEN)) ||
        (pPktInfo->DhcpMsg.chaddr[0] & 0x01) ||
        (!MEMCMP (pPktInfo->DhcpMsg.chaddr, BcastMacAddr, DHCP_CHADDR_MAC_LEN)))
    {
        DHCP_TRC (FAIL_TRC, "Invalid client hardware address\n");
        gDhcpCounters.u4DhcpCountInvalids++;
        return DHCP_FAILURE;
    }

    if ((pPktInfo->DhcpMsg.yiaddr != 0) || (pPktInfo->DhcpMsg.siaddr != 0))
    {
        DHCP_TRC (FAIL_TRC, "Some header fields are not proper");
        return DHCP_FAILURE;
    }

    switch (pPktInfo->u1Type)
    {
        case DHCP_REQUEST:
            break;

        case DHCP_INFORM:
            if ((DhcpGetOption (DHCP_OPT_REQUESTED_IP, pPktInfo) == DHCP_FOUND)
                || (DhcpGetOption (DHCP_OPT_LEASE_TIME, pPktInfo) ==
                    DHCP_FOUND))
            {
                DHCP_TRC (FAIL_TRC,
                          "Options which are not allowed is present\n");
                i4RetVal = DHCP_FAILURE;
                break;
            }
            /* !!! No break statement . Fall through */

        case DHCP_DISCOVER:
            if (DhcpGetOption (DHCP_OPT_SERVER_ID, pPktInfo) == DHCP_FOUND)
            {
                DHCP_TRC (FAIL_TRC, "Server Identifier is not zero\n");
                i4RetVal = DHCP_FAILURE;
            }
            break;

        case DHCP_DECLINE:
        case DHCP_RELEASE:

            if ((pPktInfo->u1Type == DHCP_DECLINE)
                && (DhcpGetOption (DHCP_OPT_SERVER_ID, pPktInfo) != DHCP_FOUND))
            {
                DHCP_TRC (FAIL_TRC, "Server ID option present in DECLINE\n");
                i4RetVal = DHCP_FAILURE;
                break;
            }
            if ((pPktInfo->u1Type == DHCP_RELEASE)
                && (DhcpGetOption (DHCP_OPT_REQUESTED_IP, pPktInfo)
                    == DHCP_FOUND))
            {
                DHCP_TRC (FAIL_TRC,
                          "Req.Ip-Address option present in RELEASE\n");
                i4RetVal = DHCP_FAILURE;
                break;
            }

            if ((DhcpGetOption (DHCP_OPT_MAX_MESSAGE_SIZE, pPktInfo) ==
                 DHCP_FOUND)
                || (DhcpGetOption (DHCP_OPT_PARAMETER_LIST, pPktInfo) ==
                    DHCP_FOUND))
            {
                DHCP_TRC (FAIL_TRC,
                          "Options which are not allowed is present\n");
                i4RetVal = DHCP_FAILURE;
            }
            break;
        default:
            break;
    }

    return (i4RetVal);
}

/************************************************************************/
/*  Function Name   : DhcpFindRequestState                              */
/*  Description     : This function determines the received DHCP request*/
/*                  : state. Also determines server-ip and requested ip */
/*                  : address from the message                          */
/*  Input(s)        : pPkt  - received dhcp packet                      */
/*                    pServerIp - pointer to the server ip address      */
/*  Input(s)        : pPkt  - received dhcp packet                      */
/*  Output(s)       : None.                                             */
/*  Returns         : request state                                     */
/************************************************************************/
UINT1
DhcpFindRequestState (tDhcpPktInfo * pPkt, UINT4 *pServerIp,
                      UINT4 *pRequestedIp)
{
    UINT4               u4ServerIp, u4ReqIp;

    u4ServerIp = u4ReqIp = 0;

    if (DhcpGetOption (DHCP_OPT_REQUESTED_IP, pPkt) == DHCP_FOUND)
    {
        MEMCPY (&u4ReqIp, DhcpTag.Val, 4);
        u4ReqIp = OSIX_NTOHL (u4ReqIp);
    }

    if (DhcpGetOption (DHCP_OPT_SERVER_ID, pPkt) == DHCP_FOUND)
    {
        MEMCPY (&u4ServerIp, DhcpTag.Val, 4);
        u4ServerIp = OSIX_NTOHL (u4ServerIp);
    }
    *pServerIp = u4ServerIp;
    *pRequestedIp = u4ReqIp;

    /* Refer RFC 2131 section 4.3.6 */
    /* if the server identifier option is present it is in the 
     * SELECTING state */
    if (u4ServerIp != 0)
    {
        if ((u4ReqIp == 0) || (pPkt->DhcpMsg.ciaddr != 0))
            return DHCP_UNKNOWN;
        else
            return DHCP_SELECTING;
    }

    /* else if requested -ip is present it is in the INIT-REBOOT 
     * state */
    if (u4ReqIp != 0)
    {
        if (pPkt->DhcpMsg.ciaddr != 0)
            return DHCP_UNKNOWN;
        else
            return DHCP_INIT_REBOOT;
    }

    /* Now the Request is either in RENEWING or REBINDING state */
    /* if not broadcast, it is in RENEWING state */
    if (pPkt->DhcpMsg.ciaddr == 0)
        return DHCP_UNKNOWN;

    if (pPkt->u4IpAddr == 0xffffffff)
    {
        DHCP_TRC (EVENT_TRC, "DHCP REQUEST in REBINDING state\n");
        return DHCP_REBINDING;

    }
    else
    {
        DHCP_TRC (EVENT_TRC, "DHCP REQUEST in RENEWING state\n");
        return DHCP_RENEWING;
    }

}

/************************************************************************/
/*  Function Name   : DHCPSendACK                                       */
/*  Description     : This function will send a DHCP ACK.               */
/*  Input(s)        : pRcvdPkt - received dhcp packet.                  */
/*                    pHostEntry - host specific entry if present.      */
/*                    pPoolEntry - subnet pool entry.                   */
/*                    u1reqType - request state of the recieved dhcp    */
/*                    request message.                                  */
/*                    u4IpAddr - assigned Ip address.                   */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DHCPSendACK (tDhcpPktInfo * pRcvdPkt, tDhcpHostEntry * pHostEntry,
             tDhcpPool * pPoolEntry, UINT1 u1reqType, UINT4 u4IpAddr)
{
    UINT4               u4Temp;
    UINT4               u4SeverIdentifier;
    UINT4               u4Len = 0;

    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr;
    tDhcpMsgHdr        *pRcvdHdr;
    tDhcpOptions       *pOption;
    UINT1              *pFileName;
    INT4                i4RetVal;

    UNUSED_PARAM (u1reqType);

    if ((pOutPkt = DhcpCreateOutPkt ()) == NULL)
    {
        return DHCP_FAILURE;
    }

    pOutPkt->u4IfIndex = pRcvdPkt->u4IfIndex;
    pOutPkt->u2MaxMessageSize = pRcvdPkt->u2MaxMessageSize;
    pOutPkt->u1OverLoad = 0;

    u4SeverIdentifier = DhcpGetServerIdentifier (pRcvdPkt->u4IfIndex);
    pOutPkt->u2MaxMessageSize = pRcvdPkt->u2MaxMessageSize;
    pOutPkt->u1OverLoad = 0;

    pOutHdr = &(pOutPkt->DhcpMsg);
    pRcvdHdr = &(pRcvdPkt->DhcpMsg);

    pOutHdr->Op = BOOTREPLY;
    pOutHdr->htype = pRcvdHdr->htype;
    pOutHdr->hlen = pRcvdHdr->hlen;
    pOutHdr->hops = 0;
    pOutHdr->xid = pRcvdHdr->xid;
    pOutHdr->secs = 0;
    pOutHdr->flags = pRcvdHdr->flags;
    pOutHdr->ciaddr = pRcvdHdr->ciaddr;
    pOutHdr->yiaddr = u4IpAddr;
    pOutHdr->siaddr = pHostEntry ? pHostEntry->u4BootSrvIpAddr :
        gDhcpBootServerAddress;
    pOutHdr->giaddr = pRcvdHdr->giaddr;

    MEMCPY (pOutHdr->chaddr, pRcvdHdr->chaddr, 16);
    /* set boot server hostname and boot file name as NULL */

    MEMSET (pOutHdr->sname, 0, 64);
    /* If the tftp server name option is configured add it in the 
     * sname field. */
    if ((pOption = DhcpGetCofiguredOption (pHostEntry, pPoolEntry,
                                           DHCP_OPT_TFTP_SNAME)) != NULL)
    {
        MEMCPY (pOutHdr->sname, pOption->u1Value, pOption->u4Len);
    }

    MEMSET (pOutHdr->file, 0, 128);
    if ((pFileName = DhcpGetBootFileName (pHostEntry, pRcvdHdr->file)) != NULL)
    {
        u4Len = ((STRLEN (pFileName) < sizeof (pOutHdr->file)) ?
                 STRLEN (pFileName) : sizeof (pOutHdr->file) - 1);
        STRNCPY (pOutHdr->file, pFileName, u4Len);
        pOutHdr->file[u4Len] = '\0';
    }

    if (u4IpAddr != 0)
    {
        /* response to a DHCP request */
        /* Add Lease time option */
        DhcpTag.Type = DHCP_OPT_LEASE_TIME;
        DhcpTag.Len = 4;
        u4Temp = OSIX_HTONL (pPoolEntry->u4LeaseTime);
        DhcpTag.Val = (UINT1 *) &u4Temp;
        DhcpAddOption (pOutPkt, &DhcpTag);
    }

    /* Add server identifier option */
    DhcpTag.Type = DHCP_OPT_SERVER_ID;
    DhcpTag.Len = 4;
    u4Temp = OSIX_HTONL (u4SeverIdentifier);
    DhcpTag.Val = (UINT1 *) &u4Temp;
    DhcpAddOption (pOutPkt, &DhcpTag);

    DhcpAddParamsListOptions (pOutPkt, pRcvdPkt, pPoolEntry, pHostEntry);
    DhcpAddConfiguredOptions (pOutPkt, pRcvdPkt, pPoolEntry, pHostEntry);

    i4RetVal = DhcpSendMessage (pOutPkt, pRcvdHdr->ciaddr, DHCP_ACK);

    /* free OutPkt Message Structure */

    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);

    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId, (UINT1 *) pOutPkt);
    DhcpTag.Val = NULL;
    return i4RetVal;

}

/************************************************************************/
/*  Function Name   : DHCPSendNAK                                       */
/*  Description     : This function send a DHCP NAK message             */
/*  Input(s)        : pRcvdPkt - received dhcp packet                   */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DHCPSendNAK (tDhcpPktInfo * pRcvdPkt)
{
    UINT4               u4Temp;
    UINT4               u4SeverIdentifier;

    tDhcpPktInfo       *pOutPkt = NULL;
    tDhcpMsgHdr        *pOutHdr;
    tDhcpMsgHdr        *pRcvdHdr;
    INT4                i4RetVal;

    if ((pOutPkt = DhcpCreateOutPkt ()) == NULL)
    {
        return DHCP_FAILURE;
    }

    DHCP_TRC (EVENT_TRC, "Sending DHCP NAK. \n");

    u4SeverIdentifier = DhcpGetServerIdentifier (pRcvdPkt->u4IfIndex);

    pOutPkt->u2MaxMessageSize = pRcvdPkt->u2MaxMessageSize;
    pOutPkt->u1OverLoad = 0;

    pOutHdr = &(pOutPkt->DhcpMsg);
    pRcvdHdr = &(pRcvdPkt->DhcpMsg);

    pOutHdr->Op = BOOTREPLY;
    pOutHdr->htype = pRcvdHdr->htype;
    pOutHdr->hlen = pRcvdHdr->hlen;
    pOutHdr->hops = 0;
    pOutHdr->xid = pRcvdHdr->xid;
    pOutHdr->secs = 0;
    pOutHdr->flags = pRcvdHdr->flags;
    pOutHdr->ciaddr = 0;
    pOutHdr->yiaddr = 0;
    pOutHdr->siaddr = 0;
    pOutHdr->giaddr = pRcvdHdr->giaddr;

    MEMCPY (pOutHdr->chaddr, pRcvdHdr->chaddr, 16);
    /* set boot server hostname and boot file name as NULL 
     * - note: in this case these fields are unused. */
    MEMSET (pOutHdr->sname, 0, 64);
    MEMSET (pOutHdr->file, 0, 128);

    /* Set the broadcast flag when sending NAK through Relay agent. */
    if (pRcvdHdr->giaddr != 0)
        pOutHdr->flags |= DHCP_BROADCAST_MASK;

    /* Add server identifier option */
    DhcpTag.Type = DHCP_OPT_SERVER_ID;
    DhcpTag.Len = 4;
    u4Temp = OSIX_HTONL (u4SeverIdentifier);
    DhcpTag.Val = (UINT1 *) &u4Temp;
    DhcpAddOption (pOutPkt, &DhcpTag);

    pOutPkt->u4IfIndex = pRcvdPkt->u4IfIndex;
    i4RetVal = DhcpSendMessage (pOutPkt, pRcvdHdr->ciaddr, DHCP_NACK);

    /* free OutPkt Message Structure */
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                        (UINT1 *) pOutPkt->DhcpMsg.pOptions);

    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId, (UINT1 *) pOutPkt);
    DhcpTag.Val = NULL;
    return i4RetVal;

}

/************************************************************************/
/*  Function Name   : DhcpCreateOutPkt                                  */
/*  Description     : This function allocate memory for the outgoing    */
/*                    dhcp packet.                                      */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the outgoing dhcp packet or NULL       */
/************************************************************************/

tDhcpPktInfo       *
DhcpCreateOutPkt ()
{
    tDhcpPktInfo       *pOutPkt = NULL;

    if ((pOutPkt =
         (tDhcpPktInfo *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvOutPktId)) ==
        NULL)
    {
        return NULL;
    }

    if ((pOutPkt->DhcpMsg.pOptions =
         (UINT1 *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvTempOptId)) == NULL)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId, (UINT1 *) pOutPkt);
        return NULL;
    }
    /* Initialize the OutPkt */
    MEMSET (pOutPkt->DhcpMsg.pOptions, 0, DHCP_DEF_MAX_MSGLEN);
    pOutPkt->u2BufLen = DHCP_DEF_MAX_MSGLEN;
    pOutPkt->u2Len = 0;
    return pOutPkt;

}

/************************************************************************/
/*  Function Name   : DhcpSCheckThreshold                               */
/*  Description     : This function checks the threshold level of pool  */
/*                    and sends snmp trap and syslog event              */
/*  Input(s)        : pPoolEntry - Pool entry for which threshold needs */
/*                    to be checked                                     */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpSCheckThreshold (tDhcpPool * pPoolEntry)
{
    UINT4               u4Utilization = 0;

    if ((pPoolEntry->u4Threshold == 0) || (pPoolEntry->u4IpCount == 0))
    {
        return (DHCP_SUCCESS);
    }

    u4Utilization = pPoolEntry->u4BoundIpCount * 100 / pPoolEntry->u4IpCount;
    if (u4Utilization >= pPoolEntry->u4Threshold)
    {
        DhpSSnmpSendPoolUtlTrap ((INT4) pPoolEntry->u4PoolId,
                                 (INT4) pPoolEntry->u4Threshold,
                                 (INT1 *) DHCP_POOL_UTIL_TRAPS_OID,
                                 (UINT1) DHCP_TRAPS_OID_LEN);

        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gi4DhcpSysLogId,
                      "Pool %d utilization exceeded threshold level of %d%%",
                      pPoolEntry->u4PoolId, pPoolEntry->u4Threshold));
    }
    return DHCP_SUCCESS;
}
