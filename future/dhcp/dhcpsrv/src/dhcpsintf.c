/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsintf.c,v 1.58 2016/02/12 10:46:37 siva Exp $ 
 *
 * Description: This file contains external interface functions for
 *              FutureDHCP modules. FSAP2,FutureIp and FutureSLI   
 *              functions and macros are used in these functions.
 *******************************************************************/

#include "dhcpscom.h"

/************************************************************************/
/*  Function Name   : DhcpTaskInit                                      */
/*  Description     : This function creates task Queue and              */
/*                    starts the offer reuse and lease                  */
/*                  : timer.                                            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpTaskInit ()
{
    if (DhcpSrvInit () != DHCP_SUCCESS)
    {
        return DHCP_FAILURE;
    }

    if (TmrCreateTimerList (DHCP_SERVER_TASK_NAME,
                            DHCP_TIMER_EXPIRY,
                            NULL, &DhcpTimerListId) != TMR_SUCCESS)
    {
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpServerEnable                                    */
/*  Description     : This function creates socket and                  */
/*                    starts the offer reuse and lease                  */
/*                  : timer.                                            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpServerEnable (VOID)
{
    /* Start global timers for offer reuse and lease expiry */
    gDhcpOfferReuseTimer.u4Data = 0;
    if (DhcpStartTimer (DhcpTimerListId, &gDhcpOfferReuseTimer,
                        gu4DhcpOfferTimeOut) == DHCP_FAILURE)
    {
        return DHCP_FAILURE;
    }

    gDhcpLeaseExpTimer.u4Data = 1;
    if (DhcpStartTimer (DhcpTimerListId, &gDhcpLeaseExpTimer,
                        DHCP_GLOBAL_LEASE_TIMEOUT) == DHCP_FAILURE)
    {
        return DHCP_FAILURE;
    }
#ifdef NPAPI_WANTED
#ifdef L3_SWITCHING_WANTED
    if (DhcpsFsNpDhcpSrvInit () != FNP_SUCCESS)
    {
        return DHCP_FAILURE;
    }
#endif
#endif
    /*Create and bind the socket */
    if (DhcpSrvSockInit () != DHCP_SUCCESS)
    {
        return DHCP_FAILURE;
    }
    gi4DhcpSysLogId = SYS_LOG_REGISTER (DHCP_SERVER_TASK_NAME,
                                        SYSLOG_CRITICAL_LEVEL);
    gu4DhcpsEnable = DHCP_ENABLED;
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpServerDisable                                   */
/*  Description     : This function remove socket descriptor and        */
/*                    stops the offer reuse and lease timer             */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/
INT4
DhcpServerDisable (VOID)
{
    INT4                i4RetVal = DHCP_SUCCESS;
    tDhcpBinding       *pBinding,*pTempBind;

    /* Stop the Timer lists used by DHCP Server */
    DhcpStopTimer (DhcpTimerListId, &gDhcpOfferReuseTimer);
    DhcpStopTimer (DhcpTimerListId, &gDhcpLeaseExpTimer);

#ifdef NPAPI_WANTED
#ifdef L3_SWITCHING_WANTED
    DhcpsFsNpDhcpSrvDeInit ();
#endif
#endif
    /* Remove the Socket Descriptor added to Select utility 
     * for Packet Reception */
    SelRemoveFd (gi4DhcpSrvSockDesc);

    /* Close the DHCP Server Socket */
    if (gi4DhcpSrvSockDesc != -1)
    {
        if (close (gi4DhcpSrvSockDesc) != OSIX_SUCCESS)
        {
            DHCP_TRC (FAIL_TRC, "Closing of DHCP Server socket failed \n");
            i4RetVal = DHCP_FAILURE;
        }
    }
    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pTempBind)
    {
        pTempBind = pBinding->pNext;
        /* Delete the binding entry */
        DhcpDeleteBinding (pBinding);
    }


    gi4DhcpSrvSockDesc = -1;
    SYS_LOG_DEREGISTER (gi4DhcpSysLogId);
    gu4DhcpsEnable = DHCP_DISABLED;
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpTaskMain                                      */
/*  Description     : This function waits for the packet arrival event. */
/*                    Dequeued DHCP messages will be processed upon     */
/*                    receiving of packet arrival event from the DHCP   */
/*                    Select Task.                                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpTaskMain (INT1 *i1pParam)
{

    UINT4               u4Event = 0;

    UNUSED_PARAM (i1pParam);
    if (DhcpTaskInit () != DHCP_SUCCESS)
    {
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
    }
    if (OsixTskIdSelf (&gu4DhcpServTskId) == OSIX_FAILURE)
    {
        DHCP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    DHCP_INIT_COMPLETE (OSIX_SUCCESS);
    while (1)
    {

        OsixEvtRecv (gu4DhcpServTskId,
                     DHCP_TIMER_EXPIRY | DHCP_PACKET_ARRIVAL_EVENT |
                     DHCP_ICMP_PKT_ARRIVAL_EVENT | DHCP_Q_MSG, OSIX_WAIT,
                     &u4Event);

        if (u4Event & DHCP_TIMER_EXPIRY)
        {
            DhcpProcessTmrExpiry ();
        }
        if (u4Event & DHCP_ICMP_PKT_ARRIVAL_EVENT)
        {
            DHCP_TRC (ALL_TRC,
                      "DHCP Server - ICMP Echo Response Received !!!**\n");
            DhcpProcessIcmpResponse ();
        }
        /* Add the Socket Descriptor to Select utility
           for Packet Reception */
        if (u4Event & DHCP_PACKET_ARRIVAL_EVENT)
        {
            DhcpRcvAndProcessPkt ();
            if (gi4DhcpSrvSockDesc != -1)
            {
                SelAddFd (gi4DhcpSrvSockDesc, DhcpSvPktInSocket);
            }
        }
        if (u4Event & DHCP_Q_MSG)
        {
            DhcpSProcessQMsg ();
        }
    }
}

/************************************************************************/
/*  Function Name   : DhcpSProcessQMsg                                  */
/*  Description     : This function processes the Q Msg                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpSProcessQMsg (VOID)
{
    tDhcpSQMsg         *pDhcpSQMsg = NULL;
    while (OsixQueRecv (gDhcpSMsgQId, (UINT1 *) (&pDhcpSQMsg),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        DHCP_TRC1 (DEBUG_TRC, "Recvd Msg %x \n", pDhcpSQMsg);

        /* Take the Lock */
        DHCPS_PROTO_LOCK ();
        switch (pDhcpSQMsg->u4MsgType)
        {
            case DHCPS_RM_MSG:
                /* DHCP Server RM Msg and Event Handler */
                DhcpSRedHandleRmEvents (pDhcpSQMsg);
                break;
            default:
                break;
        }
        /* Release the Lock */
        DHCPS_PROTO_UNLOCK ();
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvQueueMsgId,
                            (UINT1 *) pDhcpSQMsg);
    }
}

/*********************************************************************************    
 * Function Name   : DhcpRlPktInSocket *   
 * Description     : Call back function from SelAddFd(), when DHCP packet is
 *                   received on DHCP Server socket.
 * Global Varibles : DHCP_TASK_ID
 * Inputs          : None
 * Output          : Sends an event to DHCP Server task
 * Returns         : None.
  ******************************************************************************/
VOID
DhcpSvPktInSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    OsixEvtSend (gu4DhcpServTskId, DHCP_PACKET_ARRIVAL_EVENT);
}

/************************************************************************/
/*  Function Name   : DhcpRcvAndProcessPkt                              */
/*  Description     : The function process the packet                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpRcvAndProcessPkt ()
{
    struct sockaddr_in  PeerAddr;
    INT4                i4DataLen;
    tDhcpRcvPkt        *pRecvBuff = NULL;
#ifdef SLI_WANTED
    INT4                i4AddrLen = sizeof (PeerAddr);
#endif
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
#ifdef SLI_WANTED
    struct cmsghdr      CmsgInfo;
#endif
#ifdef BSDCOMP_SLI_WANTED
    UINT1               au1Cmsg[DHCPSRV_CMSG_LEN];    /* For storing Auxillary Data - 
                                                       IP Packet INFO. */
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    pRecvBuff =
        (tDhcpRcvPkt *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvRecvBuffId);
    if (pRecvBuff == NULL)
    {
        DHCP_TRC (EVENT_TRC, "MemAlloc failed:: DhcpRcvAndProcessPkt \r\n");
        return;
    }
    MEMSET (pRecvBuff, 0, sizeof (tDhcpRcvPkt));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
#ifdef SLI_WANTED
    PktInfo.msg_control = (VOID *) &CmsgInfo;
    PktInfo.msg_controllen = 2 * sizeof (CmsgInfo);
    /* Call recvfrom to receive the packet */
    if ((i4DataLen =
         recvfrom (gi4DhcpSrvSockDesc, pRecvBuff->i1RecvBuff,
                   (INT4) DHCP_MAX_MTU, 0,
                   (struct sockaddr *) &PeerAddr, &i4AddrLen)) < 0)
    {
        perror ("recvfrom failed\r\n");
    }
    else
    {
        /* Get the interface index from which the packet is 
         * received
         */
        if (recvmsg (gi4DhcpSrvSockDesc, &PktInfo, 0) < 0)
        {
            perror ("recvmsg failed\r\n");
        }
#else /* SLI_WANTED */
    MEMSET (pRecvBuff->i1RecvBuff, 0, sizeof (tDhcpRcvPkt));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pRecvBuff->i1RecvBuff;
    Iov.iov_len = sizeof (tDhcpRcvPkt);
    PktInfo.msg_iov = &Iov;

    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    if ((i4DataLen = recvmsg (gi4DhcpSrvSockDesc, &PktInfo, 0)) < 0)
    {
        perror ("recvmsg error\r\n");
    }
    else
    {
#endif
        pIpPktInfo = (struct in_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

        DhcpProcessPkt ((UINT1 *) pRecvBuff->i1RecvBuff,
                        pIpPktInfo->ipi_ifindex, pIpPktInfo->ipi_addr.s_addr,
                        (UINT2) i4DataLen);
    }
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvRecvBuffId, (UINT1 *) pRecvBuff);

}

/************************************************************************/
/*  Function Name   : DhcpSrvSockInit                                   */
/*                                                                      */
/*  Description     : This function creates a socket for UDP port 67    */
/*                    and binds an address to the port.                 */
/*                                                                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : DHCP_FAILURE/DHCP_SUCCESS                         */
/************************************************************************/
INT4
DhcpSrvSockInit ()
{
    struct sockaddr_in  DhcpSrvAddr;
    INT4                i4RetVal;
    INT4                i4Flags;
    INT4                i4OpnVal = DHCP_ENABLE_STATUS;

    gi4DhcpSrvSockDesc = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (gi4DhcpSrvSockDesc < 0)
    {
        /* Opening of DHCP SRV Socket failed */
        return DHCP_FAILURE;
    }
    if ((i4Flags = fcntl (gi4DhcpSrvSockDesc, F_GETFL, 0)) < 0)
    {
        return DHCP_FAILURE;
    }

    i4Flags |= O_NONBLOCK;
    if (fcntl (gi4DhcpSrvSockDesc, F_SETFL, i4Flags) < 0)
    {
        return DHCP_FAILURE;
    }
    /* Bind a address and port to the socket */
    DhcpSrvAddr.sin_family = AF_INET;
    DhcpSrvAddr.sin_addr.s_addr = 0;    /*INADDR_ANY */
    DhcpSrvAddr.sin_port = OSIX_HTONS (DHCP_PORT_SERVER);

    i4RetVal = bind (gi4DhcpSrvSockDesc,
                     (struct sockaddr *) &DhcpSrvAddr, sizeof (DhcpSrvAddr));

    if (i4RetVal != OSIX_SUCCESS)
    {
        /* Binding of DHCP SRV socket failed */
        return DHCP_FAILURE;
    }
    if (setsockopt (gi4DhcpSrvSockDesc, SOL_SOCKET,
                    SO_BROADCAST, &i4OpnVal, sizeof (i4OpnVal)) < 0)
    {
        perror ("setsockopt fail!!!\r\n");
        return DHCP_FAILURE;
    }

    /* Packet received on DHCP Server port - 67 */
    /* set the IP level option */
    if (setsockopt (gi4DhcpSrvSockDesc, IPPROTO_IP,
                    IP_PKTINFO, &i4OpnVal, sizeof (i4OpnVal)) < 0)
    {
        perror ("setsockopt fail!!!\r\n");
        return DHCP_FAILURE;
    }

    SelAddFd (gi4DhcpSrvSockDesc, DhcpSvPktInSocket);
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpSendMessage                                   */
/*  Description     : This function sends DHCP Message - either unicast */
/*                    to 'ciaddr' address or limited broadcat address   */
/*                    depending upon the u1MessageType and 'giaddr'.    */
/*                    This function calls DhcpSendToSli() function for  */
/*                    Transmitting UDP messages.                        */
/*                                                                      */
/*  Input(s)        : pOutPkt - Outgoing DHCP packet                    */
/*                    u4ciaddr - destination address                    */
/*                    u1MessageType - DHCP OFFER/ACK/NAK                */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/
INT4
DhcpSendMessage (tDhcpPktInfo * pOutPkt, UINT4 u4ciaddr, UINT1 u1MessageType)
{
    UINT4               u4giaddr, u4yiaddr;
    UINT4               u4IfIndex;
    UINT2               u2Size;
    UINT1               u1BcastFlag;
    INT4                i4Retval = DHCP_FAILURE;

    tDhcpSrvMessage    *pMessage = NULL;

    tARP_DATA           ArpData;
#ifdef LNXIP4_WANTED
    tNetIpv4IfInfo      NetIpIfInfo;
#endif

    pMessage =
        (tDhcpSrvMessage *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvMessageId);

    if (pMessage == NULL)
    {
        DHCP_TRC (EVENT_TRC, "MemAlloc failed:: DhcpSendMessage\r\n");
        return DHCP_FAILURE;
    }

    MEMSET (pMessage, 0, sizeof (tDhcpSrvMessage));

    DHCP_TRC2 (EVENT_TRC, "Sending %s with address %x \n",
               gDHCPDegMsg[u1MessageType], pOutPkt->DhcpMsg.yiaddr);
    /* update the counters here */
    (u1MessageType == DHCP_OFFER) ?
        (++gDhcpCounters.u4DhcpCountOffers) :
        ((u1MessageType == DHCP_ACK) ? (++gDhcpCounters.u4DhcpCountAcks) :
         (u1MessageType ==
          DHCP_NACK ? ++gDhcpCounters.u4DhcpCountNacks : u1MessageType));

    DhcpFillSendMessage (pOutPkt, pMessage->asDhcpSrvMessage, u1MessageType,
                         &u2Size);

    u4giaddr = pOutPkt->DhcpMsg.giaddr;
    u4yiaddr = pOutPkt->DhcpMsg.yiaddr;

    if (pOutPkt->DhcpMsg.flags & DHCP_BROADCAST_MASK)
        u1BcastFlag = DHCPS_SET;
    else
        u1BcastFlag = DHCPS_NOT_SET;

    if (u4giaddr != 0)
    {
        i4Retval =
            DhcpSendUnicast (pMessage->asDhcpSrvMessage, u2Size, u4giaddr,
                             pOutPkt->u4IfIndex, DHCP_PORT_SERVER);
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvMessageId,
                            (UINT1 *) pMessage);

        return i4Retval;
    }
    else
    {
        /* giaddr == 0 */
        if (u1MessageType == DHCP_NACK)
        {
            /* broadcast to 0xfffffff */
            i4Retval =
                DhcpSendBroadcast (pMessage->asDhcpSrvMessage, u2Size,
                                   pOutPkt->u4IfIndex);
            MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvMessageId,
                                (UINT1 *) pMessage);

            return i4Retval;
        }
        if (u4ciaddr != 0)
        {
            /* unicasts DHCPOFFER and DHCPACK messages to the 
             * address in 'ciaddr; */
            /* set the IP Level option */
            i4Retval =
                DhcpSendUnicast (pMessage->asDhcpSrvMessage, u2Size, u4ciaddr,
                                 pOutPkt->u4IfIndex, DHCP_PORT_CLIENT);
            MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvMessageId,
                                (UINT1 *) pMessage);

            return i4Retval;

        }
        else
        {
            if (u1BcastFlag == DHCPS_SET)
            {
                /*  server broadcasts DHCPOFFER and DHCPACK messages to
                 *  0xffffffff */
                if (DhcpGetInterface (u4yiaddr, &u4IfIndex) == DHCP_FAILURE)
                {
                    /* reply can not be sent */
                    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvMessageId,
                                        (UINT1 *) pMessage);

                    return DHCP_FAILURE;
                }

                i4Retval =
                    DhcpSendBroadcast (pMessage->asDhcpSrvMessage, u2Size,
                                       u4IfIndex);
                MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvMessageId,
                                    (UINT1 *) pMessage);

                return i4Retval;
            }
            else
            {
                /* BROADCAST FLAG not set */
                /*unicasts DHCPOFFER and DHCPACK messages to the client's
                 * hardware address and 'yiaddr' address*/

                /* Add ARP entry. This is required as IP address is not
                 * yet assigned to client and hence we may not get response for
                 * an ARP request. So we add a static entry
                 */
                MEMSET (&ArpData, 0, sizeof (tARP_DATA));
                ArpData.u4IpAddr = u4yiaddr;
                ArpData.u2Port = (UINT2) pOutPkt->u4IfIndex;
                ArpData.i2Hardware = DHCP_HW_ADDR_TYPE (pOutPkt);
                ArpData.u1EncapType = ARP_ENET_V2_ENCAP;
                ArpData.i1Hwalen = DHCP_HW_ADDR_LEN (pOutPkt);
                ArpData.i1State = ARP_DYNAMIC;
                MEMCPY (ArpData.i1Hw_addr,
                        (INT1 *) DHCP_HW_ADDRESS (pOutPkt), ArpData.i1Hwalen);
                ArpData.u1RowStatus = ACTIVE;
                if (arp_add (ArpData) == IP_FAILURE)
                {
                    /* now broadcast */
                    i4Retval =
                        DhcpSendBroadcast (pMessage->asDhcpSrvMessage, u2Size,
                                           pOutPkt->u4IfIndex);
                }
                else
                {
#ifdef LNXIP4_WANTED
                    NetIpv4GetIfInfo (pOutPkt->u4IfIndex, &NetIpIfInfo);
                    if (ArpUpdateKernelEntrywithCOM (ARP_STATIC,
                                              DHCP_HW_ADDRESS (pOutPkt),
                                              DHCP_HW_ADDR_LEN (pOutPkt),
                                              u4yiaddr, NetIpIfInfo.au1IfName)
                        == ARP_SUCCESS)
                    {
#endif
                        i4Retval =
                            DhcpSendUnicast (pMessage->asDhcpSrvMessage, u2Size,
                                             u4yiaddr, pOutPkt->u4IfIndex,
                                             DHCP_PORT_CLIENT);
#ifdef LNXIP4_WANTED
                        ArpUpdateKernelEntrywithCOM (ARP_INVALID,
                                              DHCP_HW_ADDRESS (pOutPkt),
                                              DHCP_HW_ADDR_LEN (pOutPkt),
                                              u4yiaddr, NetIpIfInfo.au1IfName);
                    }
#endif
                }
                MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvMessageId,
                                    (UINT1 *) pMessage);

                return i4Retval;

            }
        }
    }

}

/************************************************************************/
/*  Function Name   : DhcpSendBroadcast                                 */
/*  Description     : This function is called for broadcasting DHCP     */
/*                    Messages over the interface - u4IfIndex. Calls    */
/*                    DhcpSendToSli(), for transmitting UDP message.    */
/*  Input(s)        : pMessage - DHCP message to be sent.               */
/*                    u2Size - size of the message.                     */
/*                    u4IfIndex - Ip interface.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpSendBroadcast (INT1 *pMessage, UINT2 u2Size, UINT4 u4IfIndex)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
    struct cmsghdr     *pCmsgInfo;
    struct sockaddr_in  RemoteAddr;
    INT4                i4RetVal = DHCP_SUCCESS;
    UINT4               u4Dest = 0xffffffff;
    UINT2               u2Port = DHCP_PORT_CLIENT;
    UINT1               au1Cmsg[DHCPSRV_CMSG_LEN];    /* For storing Auxillary Data - 
                                                       IP Packet INFO. */
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    PktInfo.msg_name = (void *) &RemoteAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = (UINT1 *) pMessage;
    Iov.iov_len = u2Size;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pMessage);
    UNUSED_PARAM (u2Size);
#endif
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = (UINT2) u4IfIndex;
    pIpPktInfo->ipi_addr.s_addr = u4Dest;
    pIpPktInfo->ipi_spec_dst.s_addr = 0;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2Port);

    if (sendmsg (gi4DhcpSrvSockDesc, &PktInfo, 0) < 0)
    {
        perror ("sendmsg \r\n");
        i4RetVal = DHCP_FAILURE;
        return i4RetVal;
    }
#ifdef SLI_WANTED
    i4RetVal = DhcpSendToSli (pMessage, u2Size, u4Dest, u2Port);
#endif
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpSendUnicast                                   */
/*  Description     : This function is called for unicasting DHCP       */
/*                    Messages over the interface - u4IfIndex.          */
/*  Input(s)        : pMessage - DHCP message to be sent.               */
/*                    u2Size - size of the message.                     */
/*                    u4Dest - destination ip address                   */
/*                    u4IfIndex - Ip interface
 *                    u2Port - UDP destination port number              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpSendUnicast (INT1 *pMessage, UINT2 u2Size, UINT4 u4Dest,
                 UINT4 u4IfIndex, UINT2 u2Port)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo;
    struct sockaddr_in  RemoteAddr;
    INT4                i4RetVal = DHCP_SUCCESS;
    UINT1               au1Cmsg[DHCPSRV_CMSG_LEN];    /* For storing Auxillary Data - 
                                                       IP Packet INFO. */
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&RemoteAddr, 0, sizeof (struct sockaddr_in));
    PktInfo.msg_name = (void *) &RemoteAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = (UINT1 *) pMessage;
    Iov.iov_len = u2Size;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pMessage);
    UNUSED_PARAM (u2Size);
#endif
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = (INT4) u4IfIndex;
    pIpPktInfo->ipi_addr.s_addr = u4Dest;
    pIpPktInfo->ipi_spec_dst.s_addr = 0;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2Port);

    if (sendmsg (gi4DhcpSrvSockDesc, &PktInfo, 0) < 0)
    {
        DHCP_TRC (FAIL_TRC, "DhcpSendUnicast: sendmsg failed\r\n");
        i4RetVal = DHCP_FAILURE;
        return i4RetVal;
    }
#ifdef SLI_WANTED
    i4RetVal = DhcpSendToSli (pMessage, u2Size, u4Dest, u2Port);
#endif
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : DhcpSendToSli                                     */
/*  Description     : This function calls sendto() for sending UDP  */
/*                  : messages to 'u2DstPort' at 'u4DstIp' address.     */
/*  Input(s)        : pMessage - Message to be sent.                    */
/*                    u2Size - size of the message.                     */
/*                    u4DstIp - destination Ip-Address.                 */
/*                    u2DstPort - UDP destination port number.          */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCES or DHCP_FAILURE                       */
/************************************************************************/

INT4
DhcpSendToSli (INT1 *pMessage, UINT2 u2Size, UINT4 u4DstIp, UINT2 u2DstPort)
{
    struct sockaddr_in  RemoteAddr;

    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_addr.s_addr = OSIX_HTONL (u4DstIp);
    RemoteAddr.sin_port = (UINT2) OSIX_HTONS (u2DstPort);

    if (sendto (gi4DhcpSrvSockDesc, pMessage, u2Size, 0,
                (struct sockaddr *) &RemoteAddr, sizeof (RemoteAddr)) < 0)
    {
        perror ("sendto failed!!!\r\n");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpGetInterface                                  */
/*  Description     : This function gets the local interface to be used */
/*                    for sending DHCP messages to 'u4DstIp' address.   */
/*  Input(s)        : u4DstIp - destination Ip-Address.                 */
/*                    pu4IfIndex - pointer to interface number.         */
/*  Output(s)       : pu4IfIndex                                        */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpGetInterface (UINT4 u4DstIp, UINT4 *pu4IfIndex)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4DstIp;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        *pu4IfIndex = NetIpRtInfo.u4RtIfIndx;
        return DHCP_SUCCESS;
    }
    return DHCP_FAILURE;
}

/************************************************************************/
/*  Function Name   : DhcpGetServerIdentifier                           */
/*  Description     : This function gets the Server Identifier or       */
/*                    Ip-Address of the interface on which the DHCP     */
/*                    Message has been received.                        */
/*  Input(s)        : u4IfIndex-Interface index of the received message */
/*  Output(s)       : None                                              */
/*  Returns         : Server identifier or 0 in case of failure         */
/************************************************************************/
UINT4
DhcpGetServerIdentifier (UINT4 u4IfIndex)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return 0;
    }

    return (NetIpIfInfo.u4Addr);
}

/************************************************************************/
/*  Function Name   : DhcpStartTimer                                    */
/*  Description     : This function start a timer for 'u4Sec' duration. */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*                  : u4Sec - Timer expiry time in seconds.             */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec)
{
    UINT4               u4Ticks;

    /* This function start a timer for u4Sec Duration .. */

    if (DHCPS_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpStartTimer: Timer not initialised for STANDBY node \r\n");
        return DHCP_SUCCESS;
    }

    u4Ticks = u4Sec * SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (TmrStartTimer (TimerList, pTimer, u4Ticks) == TMR_FAILURE)
    {
        DHCP_TRC (FAIL_TRC, "Starting problem with Timer");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpReStartTimer                                  */
/*  Description     : This function restart a timer for u4Sec duration. */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*                  : u4Sec - Timer expiry time in seconds.             */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpReStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec)
{
    UINT4               u4Ticks;

    TmrStopTimer (TimerList, pTimer);

    u4Ticks = u4Sec * SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (DHCPS_RM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpStartTimer: Timer not initialised for STANDBY node \r\n");
        return DHCP_FAILURE;
    }

    if (TmrStartTimer (TimerList, pTimer, u4Ticks) == TMR_FAILURE)
    {
        DHCP_TRC (FAIL_TRC, "Starting problem with Timer");
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpStopTimer                                     */
/*  Description     : This function stop a timer                        */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer - pointer to timer node                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/
INT4
DhcpStopTimer (tTimerListId TimerList, tTmrAppTimer * pTimer)
{
    if (TmrStopTimer (TimerList, pTimer) != TMR_SUCCESS)
    {
        return DHCP_FAILURE;
    }
    return DHCP_SUCCESS;

}

/************************************************************************/
/*  Function Name   : DhcpProcessTmrExpiry                              */
/*  Description     : This function process the Timer Expiry Event.     */
/*                    Three events are processed -Offer reuse timeout,  */
/*                    Lease expiry timeout and Icmp Probe timeout.      */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/
VOID
DhcpProcessTmrExpiry ()
{
    tTmrAppTimer       *pTimer;
    tDhcpIcmpNode      *pWaitNode;

    /*Take the lock */
    DHCPS_PROTO_LOCK ();
    while ((pTimer = TmrGetNextExpiredTimer (DhcpTimerListId)) != NULL)
    {
        switch (pTimer->u4Data)
        {
            case DHCP_OFFER_REUSE:
                DHCP_TRC (ALL_TRC, "Offer reuse timer expired ...\n");
                DhcpProcessOfferReuseTimeOut ();
                DhcpStartTimer (DhcpTimerListId, pTimer, gu4DhcpOfferTimeOut);
                break;
            case DHCP_LEASE_EXPIRE:
                DHCP_TRC (ALL_TRC, "Lease Timer expired ...\n");
                DhcpProcessLeaseExpireTimeOut ();
                DhcpStartTimer (DhcpTimerListId, pTimer,
                                DHCP_GLOBAL_LEASE_TIMEOUT);
                break;
            case DHCP_ICMP_PROBE_EXPIRE:
                DHCP_TRC (ALL_TRC, "Icmp Probe Timer expired ...\n");
                pWaitNode = (tDhcpIcmpNode *) pTimer;
                pWaitNode->TmrNode.u4Data = DHCP_ICMP_PROBE_EXPIRE;
                /*Check the ping count. If less than max tries, retransmit the packet */
                if (pWaitNode->u2EchoSentCnt < gu4PingCount)
                {
                    DHCP_TRC (ALL_TRC, "Sending ICMP echo again ...\n");
                    if (DhcpIcmpEchoRequest (pWaitNode->u4ProbeAddr) ==
                        DHCP_SUCCESS)
                    {
                        pWaitNode->u2IcmpSeqId = gu2DhcpIcmpSeqNo;
                        (pWaitNode->u2EchoSentCnt)++;
                        DHCP_TRC1 (ALL_TRC,
                                   "ICMP Echo sent %d Time!!!\r\n",
                                   pWaitNode->u2EchoSentCnt);
                        DhcpStartTimer (DhcpTimerListId,
                                        &pWaitNode->TmrNode, gu4PingTimeOut);
                    }
                }

                else
                {
                    DhcpProcessIcmpProbeTimeOut ((tDhcpIcmpNode *) pTimer,
                                                 DHCP_ICMP_PROBE_FAIL);
                    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvIcmpNodeId,
                                        (UINT1 *) pTimer);
                }
                break;
            default:
                break;
        }

    }
    /*Release the lock */
    DHCPS_PROTO_UNLOCK ();
}

/************************************************************************/
/*  Function Name   : DhcpProcessOfferReuseTimeOut                      */
/*  Description     : This function removes all entries in the binding  */
/*                    table which are in the 'offered' state for more   */
/*                    than 'gu4DhcpOfferTimeOut'.                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpProcessOfferReuseTimeOut ()
{
    tDhcpBinding       *pBinding, *pTempBind;
    UINT4               u4CurrTime;

    /* get the current time. remove all binding recored which are in
     * offered state and LeaseExpire time less than current time */

    OsixGetSysTime (&u4CurrTime);
    u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    /* print the current bindings; only if the trace level is enabled */
    if ((gi4DhcpDebugMask & BIND_TRC) && (gpDhcpBinding != NULL))
        DhcpShowBinding ();

    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pTempBind)
    {
        pTempBind = pBinding->pNext;
        if ((pBinding->u1Mask == DHCP_OFFERED)
            && (pBinding->u4LeaseExprTime < u4CurrTime))
        {
            /* Delete the binding entry */
            DhcpDeleteBinding (pBinding);
        }
    }

}

/************************************************************************/
/*  Function Name   : DhcpProcessLeaseExpireTimeOut                     */
/*  Description     : This function removes all entries in the binding  */
/*                    table which are in the 'assigned' state for more  */
/*                    than Lease expire time.                           */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpProcessLeaseExpireTimeOut ()
{
    tDhcpBinding       *pBinding, *pTempBind;
    UINT4               u4CurrTime;
    UINT1               u1EntryType = DHCP_NOT_SNMP;
    /* get the current time. remove all binding recored which are in
     * assigned  state and LeaseExpire time less than current time */

    OsixGetSysTime (&u4CurrTime);
    u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pTempBind)
    {
        pTempBind = pBinding->pNext;
        if ((pBinding->u1Mask == DHCP_ASSIGNED)
            && (pBinding->u4LeaseExprTime < u4CurrTime))
        {
            /* Delete the binding entry */
            DhcpDeleteBinding (pBinding);
        }
        /* Delete the learnt excluded ips in this pool whenever the lease time
         * expires, so that they can be used now.*/
        DhcpFreeSubnetPoolExcludeList (pBinding->u4PoolId, u1EntryType);
    }
}

/************************************************************************/
/*  Function Name   : DhcpProcessIcmpProbeTimeOut                       */
/*  Description     : This function get the status of ICMP echo sent    */
/*                    and sends a DHCP offer if the status is success.  */
/*                    If the status is failure entry from the binding   */
/*                    table is deleted.                                 */
/*  Input(s)        : pWaitNode - Expired timer node                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpProcessIcmpProbeTimeOut (tDhcpIcmpNode * pWaitNode, INT1 i1Status)
{
    INT1                i1Flag;
    tDhcpIcmpNode      *pTempNode = NULL;
    tDhcpBinding       *pBinding = NULL;

    /*create a binding structure and fill the key fields for search */
    pBinding = DhcpGetFreeBindRec ();
    if (pBinding == NULL)
    {
        DHCP_TRC (FAIL_TRC, "No Binding Records available\n");
        return;
    }

    /* This will never occur . still.. */
    if (!gpDhcpIcmpList)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                            (UINT1 *) pWaitNode->pPktInfo->DhcpMsg.pOptions);

        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId,
                            (UINT1 *) pWaitNode->pPktInfo);

        if (DhcpSCloseICMPSocket () != DHCP_SUCCESS)
        {
            DHCP_TRC (ALL_TRC, "Failed to Close ICMP Socket..\r\n");
        }
        return;
    }

    /* Remove the expired node from the list */
    if (pWaitNode == gpDhcpIcmpList)
    {
        gpDhcpIcmpList = gpDhcpIcmpList->pNext;
    }
    else
    {

        for (pTempNode = gpDhcpIcmpList; pTempNode != NULL;
             pTempNode = pTempNode->pNext)
        {
            if (pTempNode->pNext == pWaitNode)
            {
                pTempNode->pNext = pWaitNode->pNext;
                break;
            }
        }
    }

    if (gpDhcpIcmpList == NULL)
    {
        if (DhcpSCloseICMPSocket () != DHCP_SUCCESS)
        {
            DHCP_TRC (ALL_TRC, "Failed to Close ICMP Socket..\r\n");
        }
    }
    /* Fill the Binding Record with key fields for searching */
    pBinding->u1HwType = pWaitNode->pPktInfo->DhcpMsg.htype;
    pBinding->u4PoolId = pWaitNode->pPoolEntry->u4PoolId;
    pBinding->u4Subnet = pWaitNode->pPoolEntry->u4Subnet;
    pBinding->u4IpAddress = pWaitNode->u4ProbeAddr;
    pBinding->u4xid = pWaitNode->pPktInfo->DhcpMsg.xid;

    /* Get the binding using either Client Identifier or MAC address */
    if (pWaitNode->ClientId.u4Len == 0)
    {
        MEMCPY (pBinding->aBindMac, pWaitNode->pPktInfo->DhcpMsg.chaddr,
                MEM_MAX_BYTES (pWaitNode->pPktInfo->DhcpMsg.hlen, 16));
        pBinding->u1HostIdLen = pWaitNode->pPktInfo->DhcpMsg.hlen;
    }
    else
    {
        MEMCPY (pBinding->aBindMac, pWaitNode->ClientId.u1Value,
                MEM_MAX_BYTES (pWaitNode->ClientId.u4Len, DHCP_MAX_CID_LEN));
        pBinding->u1HostIdLen = (UINT1) pWaitNode->ClientId.u4Len;
    }

    /* After calling the function DhcpGetBinding, pBinding will be updated with
       the pointer to record in the binding table(Corresponding to the Key).
       Memory allocated for pBinding in this function (DhcpProcessIcmpProbeTimeOut())
       will get freed inside DhcpGetBinding function */

    i1Flag = DhcpGetBinding (&pBinding, DHCP_XID_AND_IP_ADDR);

    if (i1Flag == DHCP_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                            (UINT1 *) pWaitNode->pPktInfo->DhcpMsg.pOptions);

        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId,
                            (UINT1 *) pWaitNode->pPktInfo);
        return;
    }

    if ((i1Status == DHCP_ICMP_PROBE_FAIL)
        && (DhcpSendMessage (pWaitNode->pPktInfo, pWaitNode->ciaddr,
                             DHCP_OFFER) == DHCP_SUCCESS))
    {
        /* calculate the offer reuse time */
        pBinding->u4LeaseExprTime = DhcpLeaseExprTime (gu4DhcpOfferTimeOut);
        pBinding->u1Mask = DHCP_OFFERED;

    }
    else
    {
        /* we have received atleast one ping reply . So we should not
         * send DHCP OFFER. Remove the Binding  from the list .Add this 
         * address to the Excluded Address list 
         * */
        pWaitNode->pPoolEntry->u4BoundIpCount--;
        DhcpRemoveBinding (pBinding);
        DhcpFreeBindRec (pBinding);
        DHCP_TRC (FAIL_TRC, "Address already in use by some machines\n");
        DhcpNotifyWrongAddress (pWaitNode->pPoolEntry, pWaitNode->u4ProbeAddr);

    }

    /* free OutPkt Message Structure */
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                        (UINT1 *) pWaitNode->pPktInfo->DhcpMsg.pOptions);
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId,
                        (UINT1 *) pWaitNode->pPktInfo);
    return;
}

/************************************************************************/
/*  Function Name   : DhcpLeaseExprTime                                 */
/*  Description     : Calculates the expiry time for the offer or lease */
/*  Input(s)        : u4LeaseTime - Lease time duration                 */
/*  Output(s)       : None.                                             */
/*  Returns         : Expiry time = Current time + Lease Time duration. */
/************************************************************************/
UINT4
DhcpLeaseExprTime (UINT4 u4LeaseTime)
{
    UINT4               u4CurrTime;
    OsixGetSysTime (&u4CurrTime);
    /* divide by the ticks per seconds */
    u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    return (u4CurrTime + u4LeaseTime);
}

/************************************************************************/
/*  Function Name   : DhcpProbeAddressAndSendOffer                      */
/*  Description     : This function probes the selected Ip-Address using*/
/*                    ICMP echo. Start a timer for checking the status. */
/*                    Also stores the packet informations about the     */
/*                    packet to be sent.                                */
/*  Input(s)        : u4IpAddr -  selected Ip-Address.                  */
/*                    pOutPkt - out going packet to be sent.            */
/*                    pRcvdPkt - received DHCP packet.                  */
/*                    pPoolEntry - Pool Entry for the received pakcet.  */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

INT4
DhcpProbeAddressAndSendOffer (UINT4 u4IpAddr, tDhcpPktInfo * pOutPkt,
                              tDhcpPktInfo * pRcvdPkt, tDhcpPool * pPoolEntry)
{
    tDhcpIcmpNode      *pWaitNode, *pTempNode = gpDhcpIcmpList;

    if ((pWaitNode =
         (tDhcpIcmpNode *) MemAllocMemBlk (i4DhcpSrvIcmpNodeId)) == NULL)
    {
        DHCP_TRC (ALL_TRC, "Memory Allocation for ICMP node failed ...\n");
        return DHCP_FAILURE;
    }
/*    if ((pWaitNode =
         MEM_MALLOC (sizeof (tDhcpIcmpNode), tDhcpIcmpNode)) == NULL)
    {
        DHCP_TRC (ALL_TRC, "Memory Allocation for ICMP node failed ...\n");
        return DHCP_FAILURE;
    }*/
    MEMSET (pWaitNode, 0, sizeof (tDhcpIcmpNode));
    pWaitNode->pPktInfo = pOutPkt;
    pWaitNode->pPoolEntry = pPoolEntry;
    pWaitNode->pNext = NULL;
    pWaitNode->ciaddr = pRcvdPkt->DhcpMsg.ciaddr;
    pWaitNode->u4ProbeAddr = u4IpAddr;
    pWaitNode->ClientId.u4Len = 0;
    pWaitNode->u2EchoSentCnt = 0;
    /* store the client identifier if present in the discovery */
    if (DhcpGetOption (DHCP_OPT_CLIENT_ID, pRcvdPkt) == DHCP_FOUND)
    {
        pWaitNode->ClientId.u4Type = DHCP_OPT_CLIENT_ID;
        pWaitNode->ClientId.u4Len = DhcpTag.Len;
        if (DhcpTag.Len > DHCP_MAX_CID_LEN)
            DhcpTag.Len = DHCP_MAX_CID_LEN;
        MEMCPY (pWaitNode->ClientId.u1Value, DhcpTag.Val, DhcpTag.Len);
    }

    /* insert the node into the end of the list , gpDhcpIcmpList */

    if (gpDhcpIcmpList != NULL)
    {
        while (1)
        {
            if (pTempNode->pNext == NULL)
            {
                pTempNode->pNext = pWaitNode;
                break;
            }
            pTempNode = pTempNode->pNext;
        }
    }
    else
    {
        /* Open the ICMP Socket for sending ICMP Echo */
        if (DhcpSOpenICMPSocket () != DHCP_SUCCESS)
        {
            DHCP_TRC (ALL_TRC,
                      "Failed to Open ICMP Socket..Cant enable ICMP Echo\r\n");
            MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvIcmpNodeId,
                                (UINT1 *) pWaitNode);
            return DHCP_FAILURE;
        }
        gpDhcpIcmpList = pWaitNode;
    }

    /* Send an ICMP Echo packet  */
    if (DhcpIcmpEchoRequest (u4IpAddr) == DHCP_SUCCESS)
    {
        pWaitNode->u2IcmpSeqId = gu2DhcpIcmpSeqNo;
        (pWaitNode->u2EchoSentCnt)++;
        DHCP_TRC1 (ALL_TRC, "ICMP Echo sent %d Time!!!\r\n",
                   pWaitNode->u2EchoSentCnt);
    }

    pWaitNode->TmrNode.u4Data = DHCP_ICMP_PROBE_EXPIRE;
    DhcpStartTimer (DhcpTimerListId, &pWaitNode->TmrNode, gu4PingTimeOut);
    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpGetSysName                                    */
/*  Description     : This function get the system name of the dhcp     */
/*                    server.                                           */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : System name.                                      */
/************************************************************************/

const UINT1        *
DhcpGetSysName ()
{
    return ((const UINT1 *) "Future");
}

/************************************************************************/
/*  Function Name   : DhcpIsSrvEnabled                                  */
/*  Description     : This function gets the status of the dhcp         */
/*                    server.                                           */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : 1 when enabled, 0 when disabled                   */
/************************************************************************/
BOOLEAN
DhcpIsSrvEnabled ()
{
    return (gu4DhcpsEnable == DHCP_ENABLED) ? 1 : 0;
}
