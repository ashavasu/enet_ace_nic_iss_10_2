/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsmain.c,v 1.38 2015/11/27 12:35:40 siva Exp $ 
 *
 * Description: This file contains functions for handling the binding,
 *              Initial configuration,etc.
 *******************************************************************/

#define _DHCPS_GLOB_VAR            /* For global variables definition */
#include   "dhcpscom.h"

#ifdef SNMP_2_WANTED
#include "dhcpswr.h"
#endif

/*Globals --- MemPool Id's */
INT4                i4DhcpSrvPoolRecId;
INT4                i4DhcpSrvBindRecId;
INT4                i4DhcpSrvHostRecId;
INT4                i4DhcpSrvOptId;
INT4                i4DhcpSrvExclAddrId;
INT4                i4DhcpSrvOutPktId;
INT4                i4DhcpSrvTempOptId;
INT4                i4DhcpSrvIcmpNodeId;
INT4                i4DhcpSrvQueueMsgId;
INT4                i4DhcpSrvMessageId;
INT4                i4DhcpSrvRecvBuffId;

/************************************************************************/
/*  Function Name   : DhcpSrvInit                                       */
/*  Description     : Initializes the global variables.Allocates memory */
/*                  : for all the DHCP records.                         */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/
INT4
DhcpSrvInit ()
{
    gDhcpServConfig.u4MaxPoolRec =
        FsDHCPSRVSizingParams[MAX_DHSRV_FREE_POOL_REC_SIZING_ID].
        u4PreAllocatedUnits;
    gDhcpServConfig.u4MaxBindRec =
        FsDHCPSRVSizingParams[MAX_DHSRV_FREE_BIND_REC_SIZING_ID].
        u4PreAllocatedUnits;
    gDhcpServConfig.u4MaxHostRec =
        FsDHCPSRVSizingParams[MAX_DHSRV_FREE_HOST_REC_SIZING_ID].
        u4PreAllocatedUnits;

    /* Creating Mem Blocks */
    DhcpsrvSizingMemCreateMemPools ();

    /* Mempool identifier Variable assignment */
    i4DhcpSrvPoolRecId = DHCPSRVMemPoolIds[MAX_DHSRV_FREE_POOL_REC_SIZING_ID];
    i4DhcpSrvBindRecId = DHCPSRVMemPoolIds[MAX_DHSRV_FREE_BIND_REC_SIZING_ID];
    i4DhcpSrvHostRecId = DHCPSRVMemPoolIds[MAX_DHSRV_FREE_HOST_REC_SIZING_ID];
    i4DhcpSrvOptId = DHCPSRVMemPoolIds[MAX_DHSRV_OPTION_SIZING_ID];
    i4DhcpSrvExclAddrId = DHCPSRVMemPoolIds[MAX_DHSRV_EXCL_NODE_SIZING_ID];
    i4DhcpSrvOutPktId = DHCPSRVMemPoolIds[MAX_DHSRV_OUT_PKT_SIZING_ID];
    i4DhcpSrvTempOptId = DHCPSRVMemPoolIds[MAX_DHSRV_TEMP_OPT_PKT_SIZING_ID];
    i4DhcpSrvIcmpNodeId = DHCPSRVMemPoolIds[MAX_DHSRV_ICMP_NODE_SIZING_ID];
    i4DhcpSrvQueueMsgId = DHCPSRVMemPoolIds[MAX_DHSRV_QUEUE_MSG_SIZING_ID];
    i4DhcpSrvMessageId = DHCPSRVMemPoolIds[MAX_DHCP_MAX_OUTMSG_SIZE_SIZING_ID];
    i4DhcpSrvRecvBuffId = DHCPSRVMemPoolIds[MAX_DHCP_MAX_MTU_SIZING_ID];

    if (OsixCreateSem (DHCPS_PROT_MUTEX_SEMA4, DHCPS_SEM_CREATE_INIT_CNT,
                       DHCP_SEM_FLAGS, &gDhcpServSemId) != OSIX_SUCCESS)
    {
        return DHCP_FAILURE;
    }

    if (DhcpInitHostRec () == DHCP_FAILURE)
    {
        return (DHCP_FAILURE);
    }

    if (DhcpInitBindRec () == DHCP_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvHostRecId,
                            (UINT1 *) gpDhcpFreeHostRec);
        return (DHCP_FAILURE);
    }

    if (DhcpInitPoolRec () == DHCP_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvHostRecId,
                            (UINT1 *) gpDhcpFreeHostRec);
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvBindRecId,
                            (UINT1 *) gpDhcpFreeBindRec);
        return (DHCP_FAILURE);
    }

    MEMSET (&gDhcpCounters, 0, sizeof (tDhcpCounters));

    /* Initialize the global variables */
    gu4DhcpsEnable = DHCP_DISABLED;
    gu4DhcpOfferTimeOut = DHCP_DEF_OFFER_REUSE_TIME;
    gi1DhcpsIcmpEchoEnable = DHCP_DISABLED;
    gi4DhcpDebugMask = DEBUG_NONE;
    gi1BootpSupported = DHCP_NO;

    /* Assign Invalid descriptor */
    gi4DhcpSrvSockDesc = -1;

    gu4PingTimeOut = 1;
    gu4PingCount = 3;

    gDhcpBootServerAddress = 0;
    if (STRLEN(DHCP_DEF_BOOT_FILE) != 0)
    {
        STRNCPY (gau1DhcpBootFileName, DHCP_DEF_BOOT_FILE, STRLEN(DHCP_DEF_BOOT_FILE));
    } 
    gau1DhcpBootFileName[STRLEN(DHCP_DEF_BOOT_FILE)] = '\0';
    gu1DhcpAutomaticBootpEnabled = DHCP_ENABLED;
    gu1DhcpBootpClientSupported = DHCP_ENABLED;

    gpDhcpOptions = NULL;
    gpDhcpPool = NULL;
    gpDhcpBinding = NULL;
    gpDhcpHostEntry = NULL;
    gpDhcpIcmpList = NULL;

    if (OsixQueCrt (DHCP_SERVER_Q_NAME, OSIX_MAX_Q_MSG_LEN, 30 /* Q depth */ ,
                    &gDhcpSMsgQId) != OSIX_SUCCESS)
    {
        DHCP_TRC (DEBUG_TRC, "DHCP Server Q creation failed\n");
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvHostRecId,
                            (UINT1 *) gpDhcpFreeHostRec);
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvBindRecId,
                            (UINT1 *) gpDhcpFreeBindRec);
        return DHCP_FAILURE;
    }

    /* Registering with RM */
    if (DhcpSRedInitGlobalInfo () == OSIX_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvHostRecId,
                            (UINT1 *) gpDhcpFreeHostRec);
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvBindRecId,
                            (UINT1 *) gpDhcpFreeBindRec);
        return (DHCP_FAILURE);
    }

#ifdef SNMP_2_WANTED
    RegisterDHCPS ();
#endif
    return (DHCP_SUCCESS);

}

/************************************************************************/
/*  Function Name   : DhcpProcessPkt                                    */
/*  Description     : The function validates the header and processes   */
/*                    message based on the message type                 */
/*  Input(s)        : pRcvdBuf - DHCP packet received                   */
/*                    u4IfIndex - Interface Index                       */
/*                    u2Len - Length of the received packet             */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpProcessPkt (UINT1 *pRcvdBuf, UINT4 u4IfIndex, UINT4 u4DestIp, UINT2 u2Len)
{
    tDhcpPktInfo        PktInfo;
    tDhcpPktInfo       *pPktInfo = &PktInfo;
    tDhcpHostEntry     *pHostEntry = NULL;
    tDhcpPool          *pPoolEntry = NULL;

    if (gu4DhcpsEnable != DHCP_ENABLED)
        return;

    pPktInfo->u4IfIndex = u4IfIndex;
    pPktInfo->u4IpAddr = u4DestIp;

    /* Perform a minimal header validation */

    if (DhcpValidatePkt (pRcvdBuf, u2Len) == DHCP_FAILURE)
    {
        DHCP_TRC (FAIL_TRC, "Invalid packet\n");
        gDhcpCounters.u4DhcpCountInvalids++;
        return;
    }

    DhcpGetPktInfo (pPktInfo, pRcvdBuf, u2Len);

    /* Perform a minimal length validation of BOOTP Request */
    if (pPktInfo->u1Type == BOOTP_PKT)
    {
        if (u2Len < BOOTP_MIN_LEN)
        {
            DHCP_TRC2 (FAIL_TRC,
                       "BOOTP Packet length is less than minimal length : "
                       "Minimum Pkt len= %d, Arrived Pkt len= %d\n",
                       BOOTP_MIN_LEN, u2Len);

            return;
        }
    }

    if (pPktInfo->u1Type >= (sizeof (gDHCPDegMsg) / sizeof (char *)))
    {
        /* u1Type value is beyond the limit */
        return;
    }

    DHCP_TRC2 (EVENT_TRC, "Rcvd %s on Interface : %d\n",
               gDHCPDegMsg[pPktInfo->u1Type], u4IfIndex);

    /* Validate the packet based on message type */

    if (DhcpValidateBootRequest (pPktInfo) == DHCP_FAILURE)
    {
        return;
    }

    if ((pPoolEntry = DhcpGetPoolEntryFromPkt (pPktInfo)) == NULL)
    {
        DHCP_TRC (FAIL_TRC, "Subnet pool not available...\n");
        return;
    }

    pHostEntry = DhcpGetHostFromPkt (pPktInfo, pPoolEntry->u4PoolId);

    switch (pPktInfo->u1Type)
    {
        case DHCP_DISCOVER:
            ++gDhcpCounters.u4DhcpCountDiscovers;
            DhcpRcvdDiscover (pPktInfo, pPoolEntry, pHostEntry);
            break;
        case DHCP_REQUEST:
            ++gDhcpCounters.u4DhcpCountRequests;
            DhcpRcvdRequest (pPktInfo, pPoolEntry, pHostEntry);
            break;
        case DHCP_RELEASE:
            ++gDhcpCounters.u4DhcpCountReleases;
            DhcpRcvdRelease (pPktInfo, pPoolEntry, pHostEntry);
            break;
        case DHCP_DECLINE:
            ++gDhcpCounters.u4DhcpCountDeclines;
            DhcpRcvdDecline (pPktInfo, pPoolEntry, pHostEntry);
            break;
        case DHCP_INFORM:
            ++gDhcpCounters.u4DhcpCountInforms;
            DhcpRcvdInform (pPktInfo, pPoolEntry, pHostEntry);
            break;
        case BOOTP_PKT:
            DhcpRcvdBootp (pPktInfo, pPoolEntry, pHostEntry);
            break;
        default:
            break;
    }
}

/************************************************************************/
/*  Function Name   : DhcpGetPktInfo                                    */
/*  Description     : This function copies the header and determines    */
/*                  : the packet is a DHCP packet or BOOTP packet       */
/*  Input(s)        : pPktInfo - Local structure to be filled with      */
/*                               header information                     */
/*                    pRcvBuf -  Received DHCP packet (Linear buffer)   */
/*                    u2Len   -  Length of received packet              */
/*  Output(s)       : pPktInfo                                          */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpGetPktInfo (tDhcpPktInfo * pPktInfo, UINT1 *pRcvdBuf, UINT2 u2Len)
{

    DhcpCopyHeader (&pPktInfo->DhcpMsg, pRcvdBuf);

    pPktInfo->u2Len = (UINT2) (u2Len - DHCP_LEN_FIXED_HDR);

    pPktInfo->u2MaxMessageSize = DHCP_DEF_MESSAGE_SIZE;
    pPktInfo->u1OverLoad = 0;

    if (DhcpGetOption (DHCP_OPT_MAX_MESSAGE_SIZE, pPktInfo) == DHCP_FOUND)
    {
        UINT2               u2Temp;
        MEMCPY (&u2Temp, DhcpTag.Val, 2);
        u2Temp = (UINT2) OSIX_NTOHS (u2Temp);

        if (u2Temp > DHCP_DEF_MESSAGE_SIZE)
            pPktInfo->u2MaxMessageSize = u2Temp;
    }

    /* Decide whether it is a DHCP or BOOTP packet.
     * If the message Type option is not present it's a BOOTP packet.
     */

    if (DhcpGetOption (DHCP_OPT_MSG_TYPE, pPktInfo) == DHCP_NOT_FOUND)
    {
        pPktInfo->u1Type = BOOTP_PKT;
    }
    else
    {
        pPktInfo->u1Type = DhcpTag.Val[0];    /* dhcp discover,req.,etc. */
    }

}

/************************************************************************/
/*  Function Name   : DhcpCopyHeader                                    */
/*  Description     : This function copies the DHCP header from the     */
/*                  : received packet                                   */
/*  Input(s)        : pHdr - Pointer to DHCP header                     */
/*                    pRcvdBuf - Received DHCP packet                   */
/*  Output(s)       : pHdr is filled with fields in DHCP header.        */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpCopyHeader (tDhcpMsgHdr * pHdr, UINT1 *pRcvdBuf)
{
    UINT2               i = 0;
    UINT2               u2Val = 0;
    UINT4               u4Val = 0;

    /*  Message op code / message type */
    pHdr->Op = pRcvdBuf[i];
    i++;

    /* Hardware address type */
    pHdr->htype = pRcvdBuf[i];
    i++;

    /*Hardware address length */
    pHdr->hlen = pRcvdBuf[i];
    i++;

    pHdr->hops = pRcvdBuf[i];
    i++;

    /*Transaction ID */
    MEMCPY (&pHdr->xid, (pRcvdBuf + i), 4);
    i += 4;

    /*seconds elapsed since client began address acquisition */
    MEMCPY (&u2Val, (pRcvdBuf + i), 2);
    pHdr->secs = (UINT2) OSIX_NTOHS (u2Val);
    i += 2;

    MEMCPY (&u2Val, (pRcvdBuf + i), 2);
    pHdr->flags = (UINT2) OSIX_NTOHS (u2Val);
    i += 2;

    /*Client IP address */
    MEMCPY (&u4Val, (pRcvdBuf + i), 4);
    pHdr->ciaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*'your' (client) IP address. */
    MEMCPY (&u4Val, (pRcvdBuf + i), 4);
    pHdr->yiaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*IP address of next server */
    MEMCPY (&u4Val, (pRcvdBuf + i), 4);
    pHdr->siaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*Relay agent IP address */
    MEMCPY (&u4Val, (pRcvdBuf + i), 4);
    pHdr->giaddr = OSIX_NTOHL (u4Val);
    i += 4;

    /*Client hardware address */
    MEMCPY (pHdr->chaddr, (pRcvdBuf + i), 16);
    i += 16;

    /*Optional server host name */
    MEMCPY (pHdr->sname, (pRcvdBuf + i), 64);
    i += 64;

    /*Boot file name */
    MEMCPY (pHdr->file, (pRcvdBuf + i), 128);
    i += 128;

    /*Optional parameters field */
    pHdr->pOptions = pRcvdBuf + i + DHCP_LEN_MAGIC;

}

/************************************************************************/
/*  Function Name   : DhcpGetOption                                     */
/*  Description     : This function checks whether the option is        */
/*                  : present. If it is present DhcpTag.Val will        */
/*                    points to th eoption value. Network to Host byte  */
/*                    order conversion is not done for the option value */
/*  Input(s)        : u1TagType - Option type                           */
/*                    pPktInfo - Local structure containing information */
/*                               about received packet                  */
/*  Output(s)       : DhcpTag ( Global )                                */
/*  Returns         : DHCP_FOUND or DHCP_NOT_FOUND                      */
/************************************************************************/

INT4
DhcpGetOption (UINT1 u1TagType, tDhcpPktInfo * pPkt)
{
    UINT1              *pOptions;
    UINT2               Offset = 0;    /* points to the options */
    UINT1               u1Type, u1Len, u1OptOverLoad = 0;
    UINT1               TempLen = 0;

    pOptions = pPkt->DhcpMsg.pOptions;

    while (Offset < pPkt->u2Len)
    {
        u1Type = pOptions[Offset];    /* Type of the option */

        u1Len = pOptions[Offset + 1];    /* length of the option */

        if (u1TagType == u1Type)
        {
            if (u1TagType == DHCP_OPT_CLIENT_ID)
            {
                /* Skip for Type,Len,HwType,Value */
                DhcpTag.Len = (UINT1) (u1Len - 1);
                DhcpTag.Val = pOptions + Offset + 3;
            }
            else
            {
                /* option value pointer */
                DhcpTag.Len = u1Len;
                DhcpTag.Val = pOptions + Offset + 2;
            }
            return DHCP_FOUND;

        }
        else
        {
            if (u1Type == DHCP_OPT_PAD)
            {
                ++Offset;
                continue;        /* ignore PAD options */
            }

            if (u1Type == DHCP_OPT_END)
                break;            /* no more options after end_opt */

            if (u1Type == DHCP_OPT_OVERLOAD)
                u1OptOverLoad = pOptions[Offset + DHCP_LEN_TYPE_LEN];

            /* Take the next option */
            Offset += u1Len + DHCP_LEN_TYPE_LEN;
        }

    }

    if (u1OptOverLoad == 0 || u1OptOverLoad > 3)
        return DHCP_NOT_FOUND;

    if (u1OptOverLoad == 1)
    {
        TempLen = (sizeof (pPkt->DhcpMsg.file) - 2);    /* 2 is reduced to provide array access and to avoid array OVERRUN */
        pOptions = pPkt->DhcpMsg.file;
    }
    else if ((u1OptOverLoad == 2) || (u1OptOverLoad == 3))
    {
        TempLen = (sizeof (pPkt->DhcpMsg.sname) - 2);    /* 2 is reduced to provide array access and to avoid array OVERRUN */
        pOptions = pPkt->DhcpMsg.sname;
    }

    Offset = 0;
    while (Offset < TempLen)
    {

        u1Type = pOptions[Offset];    /* Type of the option */

        u1Len = pOptions[Offset + 1];    /* length of the option */

        if (u1TagType == u1Type)
        {
            if (u1TagType == DHCP_OPT_CLIENT_ID)
            {
                DhcpTag.Len = (UINT1) (u1Len - 1);
                DhcpTag.Val = pOptions + Offset + 3;
            }
            else
            {
                DhcpTag.Len = u1Len;
                DhcpTag.Val = pOptions + Offset + 2;
            }
            return DHCP_FOUND;
        }
        else
        {
            if (u1Type == DHCP_OPT_PAD)
            {
                ++Offset;
                continue;        /* ignore PAD options */
            }

            if (u1Type == DHCP_OPT_END)
            {
                if (u1OptOverLoad == 3)
                {
                    pOptions = pPkt->DhcpMsg.file;
                    TempLen = 128;
                    u1OptOverLoad = 1;
                }
                else
                    break;
            }
            /* Take the next option */
            Offset += u1Len + DHCP_LEN_TYPE_LEN;
        }
    }

    return DHCP_NOT_FOUND;
}

/************************************************************************/
/*  Function Name   : DhcpAddOption                                     */
/*  Description     : Add DHCP option to the outgoing packet. Byte order*/
/*                    conversion is not done here                       */
/*  Input(s)        : pOutPkt - Local structure for DHCP response       */
/*                    pTag - pointer to the option structure to be added*/
/*  Output(s)       : pOutPkt                                           */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpAddOption (tDhcpPktInfo * pOutPkt, tDhcpTag * pTag)
{
    tDhcpMsgHdr        *pDhcpMsg;
    UINT1              *pu1TempOptBuff = NULL;

    /* Check whether the option is present already.if yes return . */

    if (DhcpGetOption (pTag->Type, pOutPkt) == DHCP_FOUND)
    {
        return;
    }
    pDhcpMsg = &(pOutPkt->DhcpMsg);

    /* Compare the length of the option with available options field
     * length. 7 is the extra length for messagetype option (3), overload
     * option (3) and the end option (1)*/
    if (pOutPkt->u2Len + pTag->Len + DHCP_LEN_TYPE_LEN + 7 >
        pOutPkt->u2MaxMessageSize)
    {
        if (STRLEN (pDhcpMsg->file) == 0)
        {
            if (pTag->Len + DHCP_LEN_TYPE_LEN + 1 < 128)
            {
                pDhcpMsg->file[0] = pTag->Type;
                pDhcpMsg->file[1] = pTag->Len;
                MEMCPY (pDhcpMsg->file + 2, pTag->Val,
                        MEM_MAX_BYTES (pTag->Len, 126));
            }
            if (pTag->Len < 128)
                pDhcpMsg->file[pTag->Len] = DHCP_OPT_END;
            pOutPkt->u1OverLoad |= 0x01;
        }
        else if (STRLEN (pDhcpMsg->sname) == 0)
        {
            if (pTag->Len + DHCP_LEN_TYPE_LEN + 1 < 64)
            {
                pDhcpMsg->sname[0] = pTag->Type;
                pDhcpMsg->sname[1] = pTag->Len;
                MEMCPY (pDhcpMsg->sname + 2, pTag->Val,
                        MEM_MAX_BYTES (pTag->Len, 62));
            }
            if (pTag->Len < 128)
                pDhcpMsg->file[pTag->Len] = DHCP_OPT_END;
            pOutPkt->u1OverLoad |= 0x02;

        }
        else
            return;

    }

    if (pOutPkt->u2Len + pTag->Len + DHCP_LEN_TYPE_LEN > pOutPkt->u2BufLen)
    {
        UINT2               u2NewLen;

        u2NewLen = (UINT2) (pOutPkt->u2BufLen + pTag->Len + DHCP_LEN_TYPE_LEN);

        pu1TempOptBuff =
            (UINT1 *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvTempOptId);
        if (pu1TempOptBuff == NULL)
        {
            DHCP_TRC (FAIL_TRC, "\n Memory Allocation is failed !! \n");
            return;
        }

        MEMCPY (pu1TempOptBuff, pDhcpMsg->pOptions, pOutPkt->u2BufLen);

        /* assign the newly allocated buffer to the DHCP options list
         */
        pDhcpMsg->pOptions = pu1TempOptBuff;

        pOutPkt->u2BufLen = u2NewLen;

    }

    if (pOutPkt->u2Len >= DHCP_DEF_MAX_MSGLEN)
        return;
    /*Add the Option type */
    *(pDhcpMsg->pOptions + pOutPkt->u2Len) = pTag->Type;

    /* if it is PAD or END Option return here */
    if ((pTag->Type == DHCP_OPT_PAD) || (pTag->Type == DHCP_OPT_END))
    {
        return;
    }

    if ((pOutPkt->u2Len + 1) >= DHCP_DEF_MAX_MSGLEN)
        return;
    /*Add the Option Length */
    *(pDhcpMsg->pOptions + pOutPkt->u2Len + 1) = pTag->Len;

    if (pTag->Len)
        MEMCPY ((pDhcpMsg->pOptions + pOutPkt->u2Len + DHCP_LEN_TYPE_LEN),
                pTag->Val, pTag->Len);

    /* Length is incremented by option type field (1 byte ) +
     * option length field (1 byte) +  option length 
     */

    pOutPkt->u2Len += pTag->Len + DHCP_LEN_TYPE_LEN;
    return;
}

/************************************************************************/
/*  Function Name   : DhcpValidatePkt                                   */
/*  Description     : Perform a minimal header validation               */
/*  Input(s)        : pDhcpPacket - received dhcp message               */
/*                    u2Len - received packet length                    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS or DHCP_FAILURE                      */
/************************************************************************/

INT4
DhcpValidatePkt (UINT1 *pDhcpPacket, UINT2 u2Len)
{
    UINT1              *pOptions = pDhcpPacket + DHCP_LEN_FIXED_HDR;
    if (u2Len < DHCP_LEN_FIXED_HDR)
        return DHCP_FAILURE;

    if (MEMCMP (pOptions - 4, gau1DhcpMagicCookie, DHCP_LEN_MAGIC))
    {
        DHCP_TRC (FAIL_TRC, " magic cookie mismatch !\n");
        return DHCP_FAILURE;
    }

    if (pDhcpPacket[0] != BOOTREQUEST)
    {
        DHCP_TRC (FAIL_TRC, " Message OpCode is not BOOTREQUEST (1) !\n");
        return DHCP_FAILURE;
    }

    return DHCP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpDeleteBinding                                 */
/*  Description     : Deletes an existing binding in the Binding Table  */
/*                  : bind record is freed to the FreeBind pool         */
/*  Input(s)        : pBinding - Binding entry to be deleted            */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpDeleteBinding (tDhcpBinding * pBinding)
{
    tDhcpPool          *pPoolEntry;
    /* Make the status of host entry free if the binding is for a static
     * entry
     */

    if (pBinding->u4AllocMethod == DHCP_ALLOC_MANUAL)
    {
        tDhcpHostEntry     *pHost = NULL;
        pHost = DhcpGetHostEntry (pBinding->u1HwType,
                                  pBinding->aBindMac,
                                  pBinding->u1HostIdLen, pBinding->u4PoolId);
        if (pHost)
            pHost->u4AllocOrFree = DHCP_FREE;
    }

    pPoolEntry = DhcpGetPoolEntry (pBinding->u4PoolId);
    if (pPoolEntry != NULL)
    {
        pPoolEntry->u4BoundIpCount--;
    }
    DhcpRemoveBinding (pBinding);
    DhcpFreeBindRec (pBinding);

}

/************************************************************************/
/*  Function Name   : DhcpFillSendMessage                               */
/*  Description     : This function copies pOutPkt info to pMessage     */
/*  Input(s)        : pOutPkt - Outgoing Dhcp Message (local structure) */
/*                    pMessage - Outgoing Linear buffer.                */
/*                    u1MessageType - DHCP message type                 */
/*                    pu2Size - Pointer to packet size                  */
/*  Output(s)       : pMessage , pu2Size                                */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpFillSendMessage (tDhcpPktInfo * pOutPkt, INT1 *pMessage,
                     UINT1 u1MessageType, UINT2 *pu2Size)
{
    UINT2               Offset = 0;
    UINT2               u2Val;
    UINT4               u4Val;
    tDhcpMsgHdr        *pHdr;
    pHdr = &(pOutPkt->DhcpMsg);

    pMessage[Offset++] = BOOTREPLY;
    pMessage[Offset++] = pHdr->htype;
    pMessage[Offset++] = pHdr->hlen;
    pMessage[Offset++] = pHdr->hops;

    MEMCPY ((pMessage + Offset), &pHdr->xid, 4);
    Offset += 4;

    u2Val = (UINT2) OSIX_HTONS (pHdr->secs);
    MEMCPY ((pMessage + Offset), &u2Val, 2);
    Offset += 2;

    u2Val = (UINT2) OSIX_HTONS (pHdr->flags);
    MEMCPY ((pMessage + Offset), &u2Val, 2);
    Offset += 2;

    u4Val = OSIX_HTONL (pHdr->ciaddr);
    MEMCPY ((pMessage + Offset), &u4Val, 4);
    Offset += 4;

    u4Val = OSIX_HTONL (pHdr->yiaddr);
    MEMCPY ((pMessage + Offset), &u4Val, 4);
    Offset += 4;

    u4Val = OSIX_HTONL (pHdr->siaddr);
    MEMCPY ((pMessage + Offset), &u4Val, 4);
    Offset += 4;

    u4Val = OSIX_HTONL (pHdr->giaddr);
    MEMCPY ((pMessage + Offset), &u4Val, 4);
    Offset += 4;

    MEMCPY (pMessage + Offset, pHdr->chaddr, 16);
    Offset += 16;
    MEMCPY (pMessage + Offset, pHdr->sname, 64);
    Offset += 64;
    MEMCPY (pMessage + Offset, pHdr->file, 128);
    Offset += 128;

    /* Add DHCP Magic cookie */
    MEMCPY (pMessage + Offset, gau1DhcpMagicCookie, DHCP_LEN_MAGIC);
    Offset += DHCP_LEN_MAGIC;

    if (u1MessageType != BOOTP_PKT)
    {
        /* Add DHCP message type option tag */
        pMessage[Offset] = DHCP_OPT_MSG_TYPE;
        pMessage[Offset + 1] = 1;
        pMessage[Offset + 2] = u1MessageType;
        Offset += 3;
    }

    /* copy the options field */
    MEMCPY (pMessage + Offset, pHdr->pOptions, pOutPkt->u2Len);
    Offset += pOutPkt->u2Len;

    /* Add the option-overload option if the filename sname field is 
     * overloaded. */
    if (pOutPkt->u1OverLoad != 0)
    {
        pMessage[Offset] = DHCP_OPT_OVERLOAD;
        pMessage[Offset + 1] = 1;
        pMessage[Offset + 2] = pOutPkt->u1OverLoad;
        Offset += 3;
    }

    /* Add the end option */
    pMessage[Offset++] = (INT1) DHCP_OPT_END;

    /* Add the padding */
    if (u1MessageType == BOOTP_PKT)
    {
        if (Offset < BOOTP_MIN_LEN)
        {
            while (Offset < BOOTP_MIN_LEN)
            {
                pMessage[Offset++] = (INT1) DHCP_OPTION_PAD;
            }
        }
    }

    *pu2Size = Offset;

}

/************************************************************************/
/*  Function Name   : DhcpNotifyWrongAddress                            */
/*  Description     : This function adds an IP address to the excluded  */
/*                  : list on excpetion conditions. e.g DHCP_DECLINE or */
/*                    when response is received for a ping packet       */
/*  Input(s)        : pPool - Pointer to pool record                    */
/*                    u4Addr - IP address responsible for exception     */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/

VOID
DhcpNotifyWrongAddress (tDhcpPool * pPool, UINT4 u4Addr)
{
    tExcludeIpAddr     *pNode;
    DHCP_TRC1 (FAIL_TRC, " Alert...\nIp Address : %x can not be assigned\n",
               u4Addr);

    for (pNode = pPool->pExcludeIpAddr; pNode != NULL; pNode = pNode->pNext)
    {
        if (DHCP_IN_RANGE (pNode->u4StartIpAddr, pNode->u4EndIpAddr, u4Addr))
        {
            return;
        }
    }

    pNode =
        (tExcludeIpAddr *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvExclAddrId);
    if (pNode)
    {
        pNode->u4StartIpAddr = pNode->u4EndIpAddr = u4Addr;
        pNode->u4Method = DHCP_NOT_SNMP;
        pNode->pNext = NULL;

        DhcpAddExcludedListNode (&pPool->pExcludeIpAddr, pNode);
    }

}

/************************************************************************/
/*  Function Name   : DhcpRemoveIcmpProbeNode                           */
/*  Description     : Removes the node and frees memory allocated for   */
/*                    Outgoing DHCP packet.Stops the timer also.        */
/*  Input(s)        : pWaitNode - Node to be removed                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpRemoveIcmpProbeNode (tDhcpIcmpNode * pWaitNode)
{
    DhcpStopTimer (DhcpTimerListId, &pWaitNode->TmrNode);
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvTempOptId,
                        (UINT1 *) pWaitNode->pPktInfo->DhcpMsg.pOptions);
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvOutPktId,
                        (UINT1 *) pWaitNode->pPktInfo);
    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvIcmpNodeId, (UINT1 *) pWaitNode);

}

/************************************************************************/
/*  Function Name   : DhcpIsCorrectSubnet                               */
/*  Description     : This function determines whether a client in the  */
/*                    INIT-REBOOT state is on the correct network.      */
/*  Input(s)        : pPool - Subnet pool entry.                        */
/*                    u4ReqIp - requested ip address in the request.    */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS/DHCP_FAILURE                         */
/************************************************************************/

INT4
DhcpIsCorrectSubnet (tDhcpPool * pPool, UINT4 u4ReqIp)
{
    if ((pPool->u4NetMask & u4ReqIp) == pPool->u4Subnet)
    {
        return DHCP_SUCCESS;
    }
    else
    {
        return DHCP_FAILURE;
    }
}

/************************************************************************/
/*  Function Name   : DhcpGetBootFileName                               */
/*  Description     : Selects the boot filename for the rcvd request.   */
/*  Input(s)        : pHostEntry - Host entry for the rcvd pkt or NULL. */
/*                    pReqFile - Requested file name.                   */
/*  Output(s)       : None.                                             */
/*  Returns         : Boot file name selected.                          */
/************************************************************************/
UINT1              *
DhcpGetBootFileName (tDhcpHostEntry * pHostEntry, UINT1 *pReqFile)
{
    UINT1              *pFileName = NULL;

    /* if the requeted file name is null return the host specific 
     * entry if present, or the default boot file name.
     * */
    if (pHostEntry)
        pFileName = pHostEntry->au1BootFile;

    if (pFileName == NULL)
        pFileName = gau1DhcpBootFileName;

    /* If the requested file field is not null, comapre with available
     * boot filenames. return NULL if no match is found.
     * */
    if (STRLEN (pReqFile) != 0)
    {
        if ((STRCMP (gau1DhcpBootFileName, pReqFile) == 0)
            || (STRCMP (pFileName, pReqFile) == 0))
            pFileName = pReqFile;
        else
            pFileName = NULL;

    }

    return pFileName;

}

/************************************************************************/
/*  Function Name   : DhcpGetSname                                      */
/*  Description     : Returns options if configured.                    */
/*  Input(s)        : pHostEntry - Host entry for the rcvd pkt or NULL. */
/*                    pPool - Subnet pool entry.                        */
/*                    u1ReqType - Option type.                          */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to option structure or NULL.              */
/************************************************************************/
tDhcpOptions       *
DhcpGetCofiguredOption (tDhcpHostEntry * pHostEntry, tDhcpPool * pPool,
                        UINT1 u1ReqType)
{
    tDhcpOptions       *pOptions = NULL;

    if ((pHostEntry == NULL) ||
        ((pHostEntry != NULL) &&
         (pOptions =
          DhcpGetActiveOptionFromList (pHostEntry->pOptions,
                                       u1ReqType)) == NULL))
    {
        if ((pOptions = DhcpGetActiveOptionFromList (pPool->pOptions,
                                                     u1ReqType)) == NULL)
        {
            pOptions = DhcpGetActiveOptionFromList (gpDhcpOptions, u1ReqType);
        }
    }
    return pOptions;

}

/************************************************************************/
/*  Function Name   : DhcpStopProbingAddress                            */
/*  Description     : This function removes all the icmp probing nodes  */
/*                    from the list.                                    */
/*  Input(s)        : u4IpAddress - Ip address of the probing node.     */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to option structure or NULL.              */
/************************************************************************/
VOID
DhcpStopProbingAddress (UINT4 u4IpAddress)
{
    tDhcpIcmpNode      *pWaitNode, *pPrevNode, *pNextNode;

    for (pPrevNode = pWaitNode = gpDhcpIcmpList; pWaitNode != NULL;
         pWaitNode = pNextNode)
    {

        if (pWaitNode->u4ProbeAddr == u4IpAddress)
        {

            pNextNode = pWaitNode->pNext;
            pPrevNode->pNext = pNextNode;

            if (pWaitNode == gpDhcpIcmpList)
                pPrevNode = gpDhcpIcmpList = gpDhcpIcmpList->pNext;

            DhcpRemoveIcmpProbeNode (pWaitNode);

        }
        else
        {
            pPrevNode = pWaitNode;
            pNextNode = pWaitNode->pNext;
        }
    }
    if (gpDhcpIcmpList == NULL)
    {
        if (DhcpSCloseICMPSocket () != DHCP_SUCCESS)
        {
            DHCP_TRC (ALL_TRC, "Failed to Close ICMP Socket..\r\n");
        }
    }
}

/************************************************************************/
/*  Function Name   : DhcpShowBinding                                   */
/*  Description     : Prints binding informations.                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
DhcpShowBinding ()
{
    tDhcpBinding       *pBinding;
    UINT1               i = 0, j = 0;
    UINT4               add;
    UINT1               mask;
    UINT4               currtime;
    INT4                i4TimeDiff = 0;
    char                clientid[32];
    char                u1log[256];

    /* handle the following lines very carefully. you need lot of 
     * time for correct formating of the prints
     * */

    SNPRINTF (u1log, sizeof (u1log), "Current DHCP Bindings are:\n");
    UtlTrcPrint (u1log);
    SNPRINTF (u1log, sizeof (u1log),
              " ______________________________________________________________________ \n");
    UtlTrcPrint (u1log);
    SNPRINTF (u1log, sizeof (u1log),
              "|      client-id         |   IpAddress  |  State        | expiretime   |\n");
    UtlTrcPrint (u1log);
    SNPRINTF (u1log, sizeof (u1log),
              "|      ~~~~~~~~~         |   ~~~~~~~~~  |  ~~~~~        | ~~~~~~~~~~   |\n");
    UtlTrcPrint (u1log);
    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pBinding->pNext)
    {
        mask = pBinding->u1Mask;

        if (mask != DHCP_BOOTP)
        {
            OsixGetSysTime (&currtime);

            currtime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
            i4TimeDiff = pBinding->u4LeaseExprTime - currtime;

            if (i4TimeDiff < 0)
            {
                /* Should not display the binding upon 
                 * Lease Time Expiry
                 */
                continue;
            }

        }
        else
        {
            currtime = 0;
        }

        for (i = 0, j = 0; i < 6; i++, j += 3)
            SPRINTF (clientid + j, "%02x:", pBinding->aBindMac[i]);
        j--;

        if (pBinding->u1HostIdLen > 6)
            SPRINTF (clientid + j, ":..");
        else
            SPRINTF (clientid + j, "   ");
        SNPRINTF (u1log, sizeof (u1log), "|%d> %s", pBinding->u1HwType,
                  clientid);
        UtlTrcPrint (u1log);

        add = pBinding->u4IpAddress;
        SNPRINTF (u1log, sizeof (u1log), " | %d.%d.%d.%d \t", add >> 24,
                  (add >> 16) & 0x000000ff, (add >> 8) & 0x000000ff,
                  add & 0x000000ff);
        UtlTrcPrint (u1log);

        SNPRINTF (u1log, sizeof (u1log), "|  %s \t",
                  (mask == 1) ? "OFFERED" : (mask ==
                                             2 ? "ASSIGNED" : (mask ==
                                                               DHCP_BOOTP ?
                                                               "BOOTP" :
                                                               "PROBING")));
        UtlTrcPrint (u1log);

        SNPRINTF (u1log, sizeof (u1log), "|   %04d       |\n",
                  pBinding->u4LeaseExprTime - currtime);
        UtlTrcPrint (u1log);
        SNPRINTF (u1log, sizeof (u1log),
                  "|________________________|______________|_______________|______________|\n");
        UtlTrcPrint (u1log);

    }
}

/* -------------------------------------------------------------------+
 *  Function           : DhcpSProtocolLock
 * 
 *  Input(s)           : None.
 * 
 *  Output(s)          : None.
 * 
 *  Returns            : None.
 * 
 *  Action             : Takes a task Sempaphore
 * +-------------------------------------------------------------------*/

INT4
DhcpSProtocolLock (VOID)
{
    if (OsixSemTake (gDhcpServSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------------+
 *  Function           : DhcpSProtocolUnlock
 * 
 *  Input(s)           : None.
 * 
 *  Output(s)          : None.
 * 
 *  Returns            : None.
 * 
 *  Action             : Releases a task Sempaphore
 * +-------------------------------------------------------------------*/

INT4
DhcpSProtocolUnLock (VOID)
{

    if (OsixSemGive (gDhcpServSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
