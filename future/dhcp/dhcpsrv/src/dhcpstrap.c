/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpstrap.c,v 1.14 2015/02/19 12:17:38 siva Exp $
 *
 * Description:This file contains the routines for SNMP Trap     
 *             for DHCP module.
 *
 *******************************************************************/
#include  "lr.h"
#include  "dhcp.h"
#include  "fssnmp.h"
#include "dhcpstrap.h"
#include "dhcpsdef.h"
#include "fsdhcps.h"
#include "snmputil.h"

static INT1         ai1TempBuffer[257];

/*****************************************************************************/
/*                                                                           */
/* Function     : DhcpSSnmpSendTrap                                           */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : i4TrapId   : Trap Identifier                               */
/*                pi1TrapOid : Pointer to the trap OID                       */
/*                u1OidLen   : OID Length                                    */
/*                pTrapInfo  : Pointer to the trap information.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
DhcpSSnmpSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tDhcpPoolUtlTrap   *pDhcpPoolUtlTrap;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[DHCP_MAX_OBJ_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;

    UNUSED_PARAM (u1OidLen);

    pEnterpriseOid = (tSNMP_OID_TYPE *) SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {

        case DHCPS_POOL_UTIL_TRAP:

            pDhcpPoolUtlTrap = (tDhcpPoolUtlTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "dhcpSrvSubnetUtlThreshold.0");
            pOid =
                (tSNMP_OID_TYPE *) DhcpSMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                free_oid (pEnterpriseOid);
                return;
            }
            pOid->pu4_OidList[DHCP_TRAPS_OID_LEN] = pDhcpPoolUtlTrap->i4PoolId;
            pOid->u4_Length = DHCP_TRAPS_OID_LEN + 1;
            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pDhcpPoolUtlTrap->i4Threshold,
                                      NULL, NULL, SnmpCounter64Type);
            pStartVb = pVbList;

            break;

        default:
            free_oid (pEnterpriseOid);
            return;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);

#else /* SNMP_3_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : DhpSSnmpSendPoolUtlTrap                                    */
/*                                                                           */
/* Description  : API to send trap to SNMP .                                 */
/*                                                                           */
/* Input        : i4PoolId       : Pool ID of the pool                       */
/*                i4Threshold    : Utilisation threshold of the pool         */
/*                pi1TrapOid     : Pointer to the trap OID                   */
/*                u1TrapOidLen   : OID Length                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DhpSSnmpSendPoolUtlTrap (INT4 i4PoolId, INT4 i4Threshold,
                         INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
#ifdef SNMP_3_WANTED
    tDhcpPoolUtlTrap    DhcpPoolUtlTrap;

    DhcpPoolUtlTrap.i4PoolId = i4PoolId;
    DhcpPoolUtlTrap.i4Threshold = i4Threshold;

    DhcpSSnmpSendTrap (DHCPS_POOL_UTIL_TRAP, pi1TrapsOid, u1TrapOidLen,
                       &DhcpPoolUtlTrap);

#else
    UNUSED_PARAM (i4Threshold);
    UNUSED_PARAM (i4PoolId);
    UNUSED_PARAM (pi1TrapsOid);
    UNUSED_PARAM (u1TrapOidLen);
#endif /* SNMP_3_WANTED */

}

/******************************************************************************
* Function : DhcpSMakeObjIdFromDotNew
* Input    : pi1TextStr
* Output   : NONE
* Returns  : pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
DhcpSMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;
    UINT4               u4Len = 0;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0;
             ((u2Index <
               (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
              && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                u4Len =
                    ((STRLEN (orig_mib_oid_table[u2Index].pNumber) <
                      sizeof (ai1TempBuffer)) ?
                     STRLEN (orig_mib_oid_table[u2Index].
                             pNumber) : sizeof (ai1TempBuffer) - 1);

                STRNCPY ((INT1 *) ai1TempBuffer,
                         orig_mib_oid_table[u2Index].pNumber, u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }

        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN (pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr, STRLEN((INT1 *) pi1TextStr));
	ai1TempBuffer[STRLEN((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; ((u2Index < 256) && (ai1TempBuffer[u2Index] != '\0'));
         u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }

    if ((pOidPtr = alloc_oid (sizeof (tSNMP_OID_TYPE))) == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pi1TempPtr = (INT1 *) DhcpSParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function : DhcpSParseSubIdNew
* Input    : ppu1TempPtr
* Output   : value of ppu1TempPtr
* Returns  : DHCP_SUCCESS or DHCP_FAILURE
*******************************************************************************/

UINT1              *
DhcpSParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}
