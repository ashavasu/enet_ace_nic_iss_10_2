/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsred.c,v 1.5 2014/02/21 13:45:40 siva Exp $
 *
 * Description: This file contains DHCP Server Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __DHCPSRED_C
#define __DHCPSRED_C

#include "dhcpscom.h"

/************************************************************************/
/*  Function Name   : DhcpSRedInitGlobalInfo                            */
/*                                                                      */
/*  Description     : This function is invoked by the DHCP module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
DhcpSRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedInitGlobalInfo \r\n");

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));
    MEMSET (&gDhcpSRedGlobalInfo, 0, sizeof (tDhcpSRedGlobalInfo));

    RmRegParams.u4EntId = RM_DHCPS_APP_ID;
    RmRegParams.pFnRcvPkt = DhcpSRedRmCallBack;

    if (DhcpSRedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedInitGlobalInfo: Registration with RM failed \r \n");
        return OSIX_FAILURE;
    }
    DHCPS_RM_NODE_STATUS () = RM_INIT;
    DHCPS_NUM_STANDBY_NODES () = 0;
    gDhcpSRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedInitGlobalInfo \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpSRedRmRegisterProtocols                       */
/*                                                                      */
/*  Description     : This function is invoked by the DHCP module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
DhcpSRedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedRmRegisterProtocols \r\n");
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedRmRegisterProtocols: Registration with RM failed \r \n");
        return OSIX_FAILURE;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedRmRegisterProtocols \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpSRmDeRegisterProtocols                        */
/*                                                                      */
/*  Description     : This function is invoked by the DHCP module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
DhcpSRedRmDeRegisterProtocols (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedRmDeRegisterProtocols \r\n");
    if (RmDeRegisterProtocols (RM_DHCPS_APP_ID) == RM_FAILURE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedDeRmRegisterProtocols: De-Registration with RM failed \r \n");
        return OSIX_FAILURE;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedRmDeRegisterProtocols \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedRmCallBack                              */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to DHCP server*/
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
DhcpSRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tDhcpSQMsg         *pMsg = NULL;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedRmCallBack \r\n");

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedRmCallBack: This event is not associated with RM \r\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedRmCallBack: Queue Message associated with the event"
                  "is not sent by RM \r\n");
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tDhcpSQMsg *) MemAllocMemBlk ((tMemPoolId) i4DhcpSrvQueueMsgId)) ==
        NULL)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedRmCallBack: Queue message allocation failure\n\r");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            DhcpSRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tDhcpSQMsg));

    pMsg->u4MsgType = DHCPS_RM_MSG;
    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    if (OsixQueSend (gDhcpSMsgQId,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvQueueMsgId, (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        DHCP_TRC (FAIL_TRC, "DhCRedRmCallBack: Q send failure\n\r");
        return OSIX_FAILURE;
    }

    OsixEvtSend (gu4DhcpServTskId, DHCP_Q_MSG);

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedRmCallBack \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleRmEvents                          */
/*                                                                      */
/* Description        : This function is invoked by the DHCP module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the DHCP Server Q Msg        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleRmEvents (tDhcpSQMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleRmEvents \r\n");
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    switch (pMsg->RmCtrlMsg.u1Event)
    {
        case GO_ACTIVE:
            DHCP_TRC (EVENT_TRC,
                      "DhcpSRedHandleRmEvents: Received GO_STANDBY event \r\n");
            DhcpSRedHandleGoActive ();
            break;
        case GO_STANDBY:
            DHCP_TRC (EVENT_TRC,
                      "DhcpSRedHandleRmEvents: Received GO_STANDBY event \r\n");
            DhcpSRedHandleGoStandby ();
            break;
        case RM_STANDBY_UP:
            DHCP_TRC (EVENT_TRC,
                      "DhcpSRedHandleRmEvents: Received RM_STANDBY_UP event \r\n");
            pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
            DHCPS_NUM_STANDBY_NODES () = pData->u1NumStandby;
            DhcpSRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            if (DHCPS_RM_BULK_REQ_RCVD () == OSIX_TRUE)
            {
                DHCPS_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                DhcpSRedSendDynamicBulkMsg ();
            }
            break;
        case RM_STANDBY_DOWN:
            DHCP_TRC (EVENT_TRC,
                      "DhcpSRedHandleRmEvents: Received RM_STANDBY_DOWN event \r\n");
            pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
            DHCPS_NUM_STANDBY_NODES () = pData->u1NumStandby;
            DhcpSRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;
        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                 pMsg->RmCtrlMsg.u2DataLen);
            DHCP_TRC (EVENT_TRC,
                      "DhcpSRedHandleRmEvents: Received RM_MESSAGE event \r\n");
            ProtoAck.u4AppId = RM_DHCPS_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gDhcpSRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                DhcpSRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                pMsg->RmCtrlMsg.u2DataLen);
            }
            else if (gDhcpSRedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {
                DhcpSRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                 pMsg->RmCtrlMsg.u2DataLen);
            }
            else
            {
                DHCP_TRC (FAIL_TRC,
                          "DhcpSRedHandleRmEvents: Sync-up message"
                          "received at Idle Node!!!!\r\n");
            }

            RM_FREE (pMsg->RmCtrlMsg.pData);
            RmApiSendProtoAckToRM (&ProtoAck);
            break;
        case RM_CONFIG_RESTORE_COMPLETE:
            if (gDhcpSRedGlobalInfo.u1NodeStatus == RM_INIT)
            {
                if (DHCPS_RM_GET_NODE_STATUS () == RM_STANDBY)
                {
                    DhcpSRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) ==
                        OSIX_FAILURE)
                    {
                        DHCP_TRC (FAIL_TRC,
                                  "DhcpSRedHandleRmEvents: Acknowledgement "
                                  "to RM for GO_STANDBY event failed!!!!"
                                  "\r\n");
                    }
                }
            }
            break;
        case L2_INITIATE_BULK_UPDATES:
            DHCP_TRC (EVENT_TRC,
                      "DhcpSRedHandleRmEvents: Received L2_INITIATE_BULK_UPDATES \r\n");
            DhcpSRedSendBulkReqMsg ();
            break;
        default:
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedHandleRmEvents: Invalid"
                      " RM event received\r\n");
            break;

    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleRmEvents \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleGoActive                          */
/*                                                                      */
/* Description        : This function is invoked by the DHCP server upon*/
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleGoActive \r\n");
    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (DHCPS_RM_NODE_STATUS () == RM_ACTIVE)
    {
        DHCP_TRC (EVENT_TRC,
                  "DhcpSRedHandleGoActive: GO_ACTIVE event reached when"
                  " node is already active \r\n");
        return;
    }
    if (DHCPS_RM_NODE_STATUS () == RM_INIT)
    {
        DHCP_TRC (EVENT_TRC,
                  "DhcpSRedHandleGoActive: Idle to Active transition..."
                  "\r\n");
        DhcpSRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (DHCPS_RM_NODE_STATUS () == RM_STANDBY)
    {
        DHCP_TRC (EVENT_TRC,
                  "DhcpSRedHandleGoActive: Standby to Active transition..."
                  "\r\n");
        DhcpSRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (DHCPS_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        DHCPS_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        DhcpSRedSendDynamicBulkMsg ();
    }
    if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedHandleGoActive: Acknowledgement to RM for"
                  "GO_ACTIVE event failed \r\n");
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleGoActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleGoStandby                         */
/*                                                                      */
/* Description        : This function is invoked by the DHCP server upon*/
/*                      receiving the GO_STANBY indication from RM      */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleGoStandby \r\n");

    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (DHCPS_RM_NODE_STATUS () == RM_STANDBY)
    {
        DHCP_TRC (EVENT_TRC,
                  "DhcpSRedHandleGoActive: GO_STANDBY event reached when"
                  "node is already in standby \r\n");
        return;
    }
    if (DHCPS_RM_NODE_STATUS () == RM_INIT)
    {
        DHCP_TRC (EVENT_TRC,
                  "DhcpSRedHandleGoActive: GO_STANDBY event reached when"
                  "node is already idle \r\n");

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else if (DHCPS_RM_NODE_STATUS () == ACTIVE)
    {
        DHCP_TRC (EVENT_TRC,
                  "DhcpSRedHandleGoActive: Active to Standby transition..."
                  " \r\n");
        DhcpSRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedHandleGoActive: Acknowledgement to RM for"
                      "GO_ACTIVE event failed \r\n");
        }
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleGoStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleIdleToActive                      */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleIdleToActive (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleIdleToActive \r\n");
    DHCPS_RM_NODE_STATUS () = RM_ACTIVE;
    DHCPS_RM_GET_NUM_STANDBY_NODES_UP ();
    DHCP_TRC (EVENT_TRC,
              "DhcpSRedHandleIdleToActive: Node Status Idle to Active\r\n");
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleIdleToActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleIdleToStandby                     */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleIdleToStandby (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleIdleToStandby \r\n");

    DHCPS_RM_NODE_STATUS () = RM_STANDBY;
    DHCPS_NUM_STANDBY_NODES () = 0;
    DHCP_TRC (EVENT_TRC,
              "DhcpSRedHandleIdleToActive: Node Status Idle to Standby\r\n");
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleIdleToStandby \r\n");

    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleStandbyToActive                   */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleStandbyToActive (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleStandbyToActive \r\n");

    DHCPS_RM_NODE_STATUS () = RM_ACTIVE;
    DHCPS_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Start global timers for offer reuse and lease expiry */
    gDhcpOfferReuseTimer.u4Data = 0;
    DhcpStartTimer (DhcpTimerListId, &gDhcpOfferReuseTimer,
                    gu4DhcpOfferTimeOut);
    gDhcpLeaseExpTimer.u4Data = 1;
    DhcpStartTimer (DhcpTimerListId, &gDhcpLeaseExpTimer,
                    DHCP_GLOBAL_LEASE_TIMEOUT);

    DHCP_TRC (EVENT_TRC,
              "DhcpSRedHandleStandbyToActive: Node Status Standby to Active\r\n");
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleStandbyToActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleActiveToStandby                   */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedHandleActiveToStandby (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedHandleActiveToStandby \r\n");

    /*update the statistics */
    DHCPS_RM_NODE_STATUS () = RM_STANDBY;
    DHCPS_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Stop the Timer lists used by DHCP Server */
    DhcpStopTimer (DhcpTimerListId, &gDhcpOfferReuseTimer);
    DhcpStopTimer (DhcpTimerListId, &gDhcpLeaseExpTimer);

    DHCP_TRC (EVENT_TRC,
              "DhcpSRedHandleActiveToStandby: Node Status Active to Standby\r\n");
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedHandleActiveToStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedProcessPeerMsgAtActive                  */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedProcessPeerMsgAtActive \r\n");

    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;

    DHCPS_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    DHCPS_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedProcessPeerMsgAtActive: Indication of error"
                      " to RM to process Bulk request failed!!!!\r\n");
        }
        return;
    }

    if (u2Length != DHCPS_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedProcessPeerMsgAtActive: Indication of error"
                      " to RM to process Bulk request" " failed!!!!\r\n");
        }
        return;
    }

    if (u1MsgType == DHCPS_RED_BULK_REQ_MESSAGE)
    {
        if (gDhcpSRedGlobalInfo.u1NumPeersUp == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gDhcpSRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        DhcpSRedSendDynamicBulkMsg ();
    }

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedProcessPeerMsgAtActive \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedProcessPeerMsgAtStandby                 */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedProcessPeerMsgAtStandby \r\n");

    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;
    u2MinLen = DHCPS_RED_TYPE_FIELD_SIZE + DHCPS_RED_LEN_FIELD_SIZE;

    while ((u2OffSet + u2MinLen) <= u2DataLen)
    {
        DHCPS_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        DHCPS_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u2OffSet += u2Length;
            continue;
        }

        u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

        if ((u2OffSet + u2RemMsgLen) > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing
             * with the next packet */
            u2OffSet = u2DataLen;
            continue;
        }
        switch (u1MsgType)
        {
            case DHCPS_RED_BULK_UPD_TAIL_MESSAGE:
                if (u2Length != DHCPS_RED_BULK_UPD_TAIL_MSG_SIZE)
                {
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;

                    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                    {
                        DHCP_TRC (FAIL_TRC,
                                  "DhcpSRedProcessPeerMsgAtStandby: Indication of error"
                                  " to RM to process Bulk update tail message"
                                  " failed!!!!\r\n");
                    }
                    u2Length = u2RemMsgLen;
                    u2OffSet += u2Length;

                    break;
                }
                DhcpSRedProcessBulkTailMsg (pMsg, &u2OffSet);
                break;
            case DHCPS_RED_BULK_BINDING_INFO:
                /* Intentional Fall through */
            case DHCPS_RED_BINDING_INFO:
                if (u2RemMsgLen != DHCPS_RED_DYN_INFO_SIZE)
                {
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;

                    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                    {
                        DHCP_TRC (FAIL_TRC,
                                  "DhcpSRedProcessPeerMsgAtStandby: "
                                  "Indication of error to RM to process "
                                  "Dynamic Server record failed!!!\r\n");
                    }
                    u2Length = u2RemMsgLen;
                    u2OffSet += u2Length;    /* Skip the attribute */
                    break;
                }
                DhcpSRedProcessDynamicBindingInfo (pMsg, &u2OffSet);
                break;
            default:
                u2Length = u2RemMsgLen;
                u2OffSet += u2Length;    /* Skip the attribute */
                break;

        }
    }

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedProcessPeerMsgAtStandby \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedRmHandleProtocolEvent                     */
/*                                                                      */
/* Description        : This function is invoked by the DHCP module to  */
/*                      intimate the RM module about the protocol       */
/*                      operations.                                     */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
DhcpSRedRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedRmHandleProtocolEvent \r\n");
    if (RmApiHandleProtocolEvent (pEvt) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedRmHandleProtocolEvent \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedRmReleaseMemoryForMsg                   */
/*                                                                      */
/* Description        : This function is invoked by the DHCP module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
DhcpSRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedRmReleaseMemoryForMsg \r\n");
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedRmReleaseMemoryForMsg \r\n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedRmReleaseMemoryForMsg                   */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
DhcpSRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedSendMsgToRm \r\n");

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_DHCPS_APP_ID, RM_DHCPS_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedSendMsgToRm \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedSendBulkReqMsg                          */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedSendBulkReqMsg \r\n");

    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    DHCPS Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (DHCPS_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedSendBulkReqMsg: RM Memory allocation failed\r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        DhcpSRedRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPS_RED_BULK_REQ_MESSAGE);
    DHCPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, DHCPS_RED_BULK_REQ_MSG_SIZE);

    DhcpSRedSendMsgToRm (pMsg, u2OffSet);
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedSendBulkReqMsg \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedDeInitGlobalInfo                        */
/*                                                                      */
/* Description        : This function is invoked by the DHCP module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register DHCP server with RM.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
DhcpsRedDeInitGlobalInfo (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedDeInitGlobalInfo \r\n");
    if (DhcpSRedRmDeRegisterProtocols () == OSIX_FAILURE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedDeInitGlobalInfo: De-Registration with RM failed \r\n");
        return OSIX_FAILURE;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpsRedDeInitGlobalInfo \r\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedSendDynamicBulkMsg                      */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedSendDynamicBulkMsg (VOID)
{
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedSendDynamicBulkMsg \r\n");
    if ((gDhcpSRedGlobalInfo.u1NumPeersUp == 0))
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedRmCallBack:" " No Standby node available\r\n");

        RmSetBulkUpdatesStatus (RM_DHCPS_APP_ID);
        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        DhcpSRedSendBulkUpdTailMsg ();
        return;
    }

    /* Actual Bulk msg to be sent */

    DhcpSRedSendBindingInfoBulk ();

    RmSetBulkUpdatesStatus (RM_DHCPS_APP_ID);
    /* Send the tail msg to indicate the completion of Bulk
     * update process.
     */
    DhcpSRedSendBulkUpdTailMsg ();
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedSendDynamicBulkMsg \r\n");
    return;

}

/************************************************************************/
/* Function Name      : DhcpSRedSendBindingInfoBulk                     */
/*                                                                      */
/* Description        : This function sends the Bulk update messages to */
/*                      the peer standby DHCP.                          */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedSendBindingInfoBulk (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    tDhcpBinding       *pDhcpBindingInfo = NULL;
    UINT2               u2MaxMsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT4               u4CurrTime = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedSendBindingInfoBulk \r\n");
    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    for (pDhcpBindingInfo = gpDhcpBinding; pDhcpBindingInfo != NULL;
         pDhcpBindingInfo = pDhcpBindingInfo->pNext)
    {
        u2MaxMsgLen = DHCPS_RED_TYPE_FIELD_SIZE + DHCPS_RED_LEN_FIELD_SIZE +
            DHCPS_RED_DYN_INFO_SIZE;

        if ((pMsg = RM_ALLOC_TX_BUF (DHCPS_RED_MAX_MSG_SIZE)) == NULL)
        {
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedSendDynamicBindingInfo: RM Memory allocation failed\r\n");
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            DhcpSRedRmHandleProtocolEvent (&ProtoEvt);
            return;
        }

        DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPS_RED_BINDING_INFO);

        /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
        u2LenOffSet = u2OffSet;
        DHCPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MaxMsgLen);

        DHCPS_RM_PUT_N_BYTE (pMsg, pDhcpBindingInfo->aBindMac, &u2OffSet, 64);
        DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u1HwType);
        DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u1HostIdLen);
        DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u1Mask);
        DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4IpAddress);
        DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4Subnet);
        OsixGetSysTime (&u4CurrTime);
        u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
        DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                             (pDhcpBindingInfo->u4LeaseExprTime - u4CurrTime));
        DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4PoolId);
        DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4AllocMethod);

        /* AFTER FILLING UP THE MSG, PUT THE ACTUAL LENGTH */
        DHCPS_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);

        if (DhcpSRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
            {
                DHCP_TRC (FAIL_TRC,
                          "DhcpSRedSendDynamicBindingInfo: "
                          "Ack to RM - Send to RM failed!!!!\r\n");
            }
        }
        u2OffSet = 0;
    }

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedSendBindingInfoBulk \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedSendBulkUpdTailMsg                      */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the DHCP server offers an IP address */
/*                      to the DHCP client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pDhcpBindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
DhcpSRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedSendBulkUpdTailMsg \r\n");

    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * DHCPS_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (DHCPS_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedSendBulkUpdTailMsg: RM Memory allocation failed\r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        DhcpSRedRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u2OffSet = 0;

    DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPS_RED_BULK_UPD_TAIL_MESSAGE);
    DHCPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, DHCPS_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (DhcpSRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedSendBulkUpdTailMsg: Ack to RM -"
                      " Send to RM failed!!!!\r\n");
        }
        return;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedSendBulkUpdTailMsg \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedProcessBulkTailMsg                      */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
DhcpSRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_DHCPS_APP_ID;

    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedProcessBulkTailMsg \r\n");

    DHCP_TRC (FAIL_TRC,
              "DhcpSRedProcessBulkTailMsg: Bulk Update Tail Message"
              " received at Standby node...\r\n");

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    if (RmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedProcessBulkTailMsg: Indication of error"
                  " to RM to process Bulk update completion" " failed!!!!\r\n");
    }

    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedProcessBulkTailMsg \r\n");

    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedSendDynamicBindingInfo                  */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the DHCP server offers an IP address */
/*                      to the DHCP client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pDhcpBindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC VOID
DhcpSRedSendDynamicBindingInfo (tDhcpBinding * pDhcpBindingInfo)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2MaxMsgLen = 0;
    UINT4               u4CurrTime = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedSendDynamicBindingInfo \r\n");
    if (pDhcpBindingInfo == NULL)
    {
        return;
    }

    /* Send only when a standby DHCP server is available */
    if ((gDhcpSRedGlobalInfo.u1NumPeersUp == 0))
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedSendDynamicBindingInfo:"
                  " No Standby node available\r\n");
        return;
    }

    u2MaxMsgLen = DHCPS_RED_TYPE_FIELD_SIZE + DHCPS_RED_LEN_FIELD_SIZE +
        DHCPS_RED_DYN_INFO_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (DHCPS_RED_MAX_MSG_SIZE)) == NULL)
    {
        DHCP_TRC (FAIL_TRC,
                  "DhcpSRedSendDynamicBindingInfo: RM Memory allocation failed\r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        DhcpSRedRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, DHCPS_RED_BINDING_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    DHCPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MaxMsgLen);

    DHCPS_RM_PUT_N_BYTE (pMsg, pDhcpBindingInfo->aBindMac, &u2OffSet, 64);
    DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u1HwType);
    DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u1HostIdLen);
    DHCPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u1Mask);
    DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4IpAddress);
    DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4Subnet);
    OsixGetSysTime (&u4CurrTime);
    u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                         (pDhcpBindingInfo->u4LeaseExprTime - u4CurrTime));
    DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4PoolId);
    DHCPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pDhcpBindingInfo->u4AllocMethod);

    /* AFTER FILLING UP THE MSG, PUT THE ACTUAL LENGTH */
    DHCPS_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);

    if (DhcpSRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (DhcpSRedRmHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            DHCP_TRC (FAIL_TRC,
                      "DhcpSRedSendDynamicBindingInfo: "
                      "Ack to RM - Send to RM failed!!!!\r\n");
        }
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedSendDynamicBindingInfo \r\n");
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedProcessDynamicBindingInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
DhcpSRedProcessDynamicBindingInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tDhcpBinding       *pDhcpBindingInfo = NULL;
    tDhcpBinding       *pDhcpBinding = NULL;
    UINT4               u4CurrTime = 0;
    UINT4               u4LeaseExprTime = 0;

    DHCP_TRC (DEBUG_TRC, "Entering DhcpSRedProcessDynamicBindingInfo \r\n");

    pDhcpBindingInfo = DhcpGetFreeBindRec ();

    if (pDhcpBindingInfo == NULL)
    {
        DHCP_TRC (FAIL_TRC, "No Binding Records available \r\n");
        return;
    }

    DHCPS_RM_GET_N_BYTE (pMsg, pDhcpBindingInfo->aBindMac, pu2OffSet, 64);
    DHCPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u1HwType);
    DHCPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u1HostIdLen);
    DHCPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u1Mask);
    DHCPS_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u4IpAddress);
    DHCPS_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u4Subnet);
    DHCPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4LeaseExprTime);
    OsixGetSysTime (&u4CurrTime);
    u4CurrTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    u4LeaseExprTime += u4CurrTime;
    DHCPS_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u4PoolId);
    DHCPS_RM_GET_4_BYTE (pMsg, pu2OffSet, pDhcpBindingInfo->u4AllocMethod);

    if ((pDhcpBinding = DhcpGetBindingFromAddress
         (pDhcpBindingInfo->u4IpAddress)) == NULL)
    {
        pDhcpBindingInfo->u4LeaseExprTime = u4LeaseExprTime;
        DhcpAddBinding (pDhcpBindingInfo);
    }
    else
    {
        pDhcpBinding->u4LeaseExprTime = u4LeaseExprTime;
    }
    DHCP_TRC (DEBUG_TRC, "Exiting DhcpSRedProcessDynamicBindingInfo \r\n");
    return;
}
#endif
