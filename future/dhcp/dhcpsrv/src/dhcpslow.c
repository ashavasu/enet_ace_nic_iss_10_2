/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpslow.c,v 1.59 2016/08/24 08:07:37 siva Exp $
 *
 * Description: This file contains SNMP SET/GET/TEST and GETNEXT 
 *              routines for the MIB objects specified in dhcps.mib
 *******************************************************************/

#include "dhcpscom.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "dhcpslow.h"
#include "dhcpcli.h"

/* LOW LEVEL Routines for Table : DhcpSrvSubnetPoolConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvSubnetPoolConfigTable (INT4
                                                      i4DhcpSrvSubnetPoolIndex)
{

    if ((DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) != NULL)
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvSubnetPoolConfigTable (INT4 *pi4DhcpSrvSubnetPoolIndex)
{
    tDhcpPool          *pEntry;
    UINT4               FirstIndex;

    if (gpDhcpPool)
        FirstIndex = gpDhcpPool->u4PoolId;
    else
        return SNMP_FAILURE;

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if (pEntry->u4PoolId < FirstIndex)
            FirstIndex = pEntry->u4PoolId;
    }
    *pi4DhcpSrvSubnetPoolIndex = FirstIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                nextDhcpSrvSubnetPoolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvSubnetPoolConfigTable (INT4 i4DhcpSrvSubnetPoolIndex,
                                             INT4
                                             *pi4NextDhcpSrvSubnetPoolIndex)
{
    tDhcpPool          *pEntry;
    UINT4               NextIndex = i4DhcpSrvSubnetPoolIndex;
    INT1                Flag = DHCP_NOT_FOUND;

    if (i4DhcpSrvSubnetPoolIndex < 0)
    {
        return SNMP_FAILURE;
    }

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if ((pEntry->u4PoolId > (UINT4) i4DhcpSrvSubnetPoolIndex)
            && ((Flag == DHCP_NOT_FOUND) || (pEntry->u4PoolId < NextIndex)))
        {
            NextIndex = pEntry->u4PoolId;
            Flag = DHCP_FOUND;
        }
    }

    if (Flag == DHCP_NOT_FOUND)
    {
        return SNMP_FAILURE;
    }
    *pi4NextDhcpSrvSubnetPoolIndex = NextIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetSubnet
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetSubnet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetSubnet (INT4 i4DhcpSrvSubnetPoolIndex,
                           UINT4 *pu4RetValDhcpSrvSubnetSubnet)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pu4RetValDhcpSrvSubnetSubnet = pEntry->u4Subnet;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetPortNumber
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetPortNumber (INT4 i4DhcpSrvSubnetPoolIndex,
                               INT4 *pi4RetValDhcpSrvSubnetPortNumber)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvSubnetPortNumber = pEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetMask
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetMask (INT4 i4DhcpSrvSubnetPoolIndex,
                         UINT4 *pu4RetValDhcpSrvSubnetMask)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pu4RetValDhcpSrvSubnetMask = pEntry->u4NetMask;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetStartIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetStartIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetStartIpAddress (INT4 i4DhcpSrvSubnetPoolIndex,
                                   UINT4 *pu4RetValDhcpSrvSubnetStartIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pu4RetValDhcpSrvSubnetStartIpAddress = pEntry->u4StartIpAddr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetEndIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetEndIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetEndIpAddress (INT4 i4DhcpSrvSubnetPoolIndex,
                                 UINT4 *pu4RetValDhcpSrvSubnetEndIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pu4RetValDhcpSrvSubnetEndIpAddress = pEntry->u4EndIpAddr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetLeaseTime
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetLeaseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetLeaseTime (INT4 i4DhcpSrvSubnetPoolIndex,
                              INT4 *pi4RetValDhcpSrvSubnetLeaseTime)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvSubnetLeaseTime = pEntry->u4LeaseTime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetUtlThreshold
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetUtlThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvSubnetUtlThreshold (INT4 i4DhcpSrvSubnetPoolIndex,
                                 INT4 *pi4RetValDhcpSrvSubnetUtlThreshold)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvSubnetUtlThreshold = pEntry->u4Threshold;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetPoolRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetPoolRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvSubnetPoolRowStatus (INT4 i4DhcpSrvSubnetPoolIndex,
                                  INT4 *pi4RetValDhcpSrvSubnetPoolRowStatus)
{
    tDhcpPool          *pEntry;
    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvSubnetPoolRowStatus = pEntry->u4Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetSubnet
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetSubnet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetSubnet (INT4 i4DhcpSrvSubnetPoolIndex,
                           UINT4 u4SetValDhcpSrvSubnetSubnet)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4Subnet = u4SetValDhcpSrvSubnetSubnet;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetPortNumber
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetPortNumber (INT4 i4DhcpSrvSubnetPoolIndex,
                               INT4 i4SetValDhcpSrvSubnetPortNumber)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)

        return SNMP_FAILURE;

    pEntry->u4IfIndex = i4SetValDhcpSrvSubnetPortNumber;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetMask
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetMask (INT4 i4DhcpSrvSubnetPoolIndex,
                         UINT4 u4SetValDhcpSrvSubnetMask)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4NetMask = u4SetValDhcpSrvSubnetMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetStartIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetStartIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetStartIpAddress (INT4 i4DhcpSrvSubnetPoolIndex,
                                   UINT4 u4SetValDhcpSrvSubnetStartIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4StartIpAddr = u4SetValDhcpSrvSubnetStartIpAddress;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetEndIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetEndIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetEndIpAddress (INT4 i4DhcpSrvSubnetPoolIndex,
                                 UINT4 u4SetValDhcpSrvSubnetEndIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4EndIpAddr = u4SetValDhcpSrvSubnetEndIpAddress;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetLeaseTime
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetLeaseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetLeaseTime (INT4 i4DhcpSrvSubnetPoolIndex,
                              INT4 i4SetValDhcpSrvSubnetLeaseTime)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4LeaseTime = i4SetValDhcpSrvSubnetLeaseTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetUtlThreshold
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetUtlThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetUtlThreshold (INT4 i4DhcpSrvSubnetPoolIndex,
                                 INT4 i4SetValDhcpSrvSubnetUtlThreshold)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4Threshold = i4SetValDhcpSrvSubnetUtlThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetPoolRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetPoolRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetPoolRowStatus (INT4 i4DhcpSrvSubnetPoolIndex,
                                  INT4 i4SetValDhcpSrvSubnetPoolRowStatus)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    UINT1               u1EntryType = DHCP_SNMP;

    switch (i4SetValDhcpSrvSubnetPoolRowStatus)
    {
        case CREATE_AND_WAIT:

            pEntry = DhcpGetFreePoolRec ();

            if (pEntry == NULL)
            {
                DHCP_TRC (MGMT, "No more pools can be created\n");
                return SNMP_FAILURE;
            }

            pEntry->u4PoolId = i4DhcpSrvSubnetPoolIndex;
            pEntry->u4IfIndex = 0xffffffff;
            pEntry->u4Status = NOT_READY;
            pEntry->u4LeaseTime = DHCPSRV_LEASE_PERIOD;
            pEntry->u4Threshold = DHCPS_DEF_POOLTHRESHOLD;
            pEntry->u4IpCount = 0;
            pEntry->u4BoundIpCount = 0;

            DhcpAddPoolEntry (pEntry);

            break;

        case ACTIVE:

            pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex);

            if (pEntry == NULL)
            {
                DHCP_TRC (MGMT, "Entry is not yet created\n");

                CLI_SET_ERR (CLI_DHCPS_ENTRY_NOT_CREATED);
                return (SNMP_FAILURE);
            }

            if (pEntry->u4StartIpAddr == 0)
            {
                DHCP_TRC (MGMT,
                          "should configure start Ip-Address for the pool\n");

                CLI_SET_ERR (CLI_DHCPS_CONFIGURE_START_IPADDR);

                return SNMP_FAILURE;
            }

            if (pEntry->u4EndIpAddr == 0)
            {
                DHCP_TRC (MGMT,
                          "should configure end Ip-Address for the pool\n");
                CLI_SET_ERR (CLI_DHCPS_CONFIGURE_END_IPADDR);
                return SNMP_FAILURE;
            }

            if (pEntry->u4StartIpAddr >= pEntry->u4EndIpAddr)
            {
                DHCP_TRC (MGMT,
                          "Start IP Address should be Less than End IP Address");

                CLI_SET_ERR (CLI_DHCPS_IPADDR_RANGE);
                return SNMP_FAILURE;
            }

            if (pEntry->u4LeaseTime == 0)
            {
                DHCP_TRC (MGMT, "should specify the lease time for the pool\n");

                CLI_SET_ERR (CLI_DHCPS_LEASETIME);
                return SNMP_FAILURE;
            }

            if (pEntry->u4NetMask == 0)
            {
                /* Configure the default Netmask and Subnet */
                DhcpSetDefNetmask (pEntry);
            }

            if (pEntry->u4Subnet == 0)
            {
                pEntry->u4Subnet = pEntry->u4StartIpAddr & pEntry->u4NetMask;
            }

            if ((pEntry->u4StartIpAddr & pEntry->u4NetMask) != pEntry->u4Subnet)
            {
                DHCP_TRC (MGMT,
                          "Start IpAddress doesn't belong to this subnet");

                CLI_SET_ERR (CLI_DHCPS_START_IPADDR_NOT_SUBNET);
                return SNMP_FAILURE;
            }

            if ((pEntry->u4EndIpAddr & pEntry->u4NetMask) != pEntry->u4Subnet)
            {
                DHCP_TRC (MGMT, "End IpAddress doesn't belong to this subnet");

                CLI_SET_ERR (CLI_DHCPS_END_IPADDR_NOT_SUBNET);
                return SNMP_FAILURE;
            }

            /* Add the Netmask to the subnet specific option list */

            if (pEntry->u4Status == NOT_READY)
            {
                UINT4               u4Value;
                pOptions =
                    DhcpGetOptionFromList (pEntry->pOptions,
                                           DHCP_OPT_SUBNET_MASK);
                if ((pOptions == NULL)
                    && ((pOptions = DhcpGetFreeOptionRec ()) != NULL))
                {
                    pOptions->u4Type = DHCP_OPT_SUBNET_MASK;
                    pOptions->u4Len = 4;
                    u4Value = OSIX_HTONL (pEntry->u4NetMask);
                    MEMCPY (pOptions->u1Value, &u4Value, 4);
                    DhcpAddOptionToList (&pEntry->pOptions, pOptions);
                    pOptions->u4Status = ACTIVE;
                }
            }
            /*Calculate the IP address available in this pool */
            DhcpSrvCalculateIpCount (pEntry);
            pEntry->u4Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex);

            if (pEntry == NULL)
            {
                DHCP_TRC (MGMT, "Entry is not yet created\n");
                return (SNMP_FAILURE);
            }

            pEntry->u4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:

            pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex);

            if (pEntry == NULL)
            {
                DHCP_TRC (MGMT, "Entry is not yet created\n");
                return (SNMP_FAILURE);
            }
            /* remove all the entries in the ICMP wait list */
            DhcpFreeSubnetPoolIcmpList (i4DhcpSrvSubnetPoolIndex);

            /* remove all the entries in the binding list */
            DhcpFreeSubnetPoolBindingList (i4DhcpSrvSubnetPoolIndex);

            /* remove all the options created for this Pool */
            DhcpFreeSubnetPoolOptionList (i4DhcpSrvSubnetPoolIndex);

            /* remove all the excluded ip address nodes for 
             * this Pool */

            DhcpFreeSubnetPoolExcludeList (i4DhcpSrvSubnetPoolIndex,
                                           u1EntryType);

            /* here , currently we are deleting the pool.
             * Another method is to make the status not in servvice
             * and delete the row after some time.
             */
            DhcpRemovePoolEntry (pEntry);
            DhcpFreePoolRec (pEntry);
            break;

    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetSubnet
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetSubnet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvSubnetSubnet (UINT4 *pu4ErrorCode,
                              INT4 i4DhcpSrvSubnetPoolIndex,
                              UINT4 u4TestValDhcpSrvSubnetSubnet)
{
    tDhcpPool          *pPool = NULL;
    tDhcpPool          *pEntry = NULL;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (!DHCP_IS_VALID_IP (u4TestValDhcpSrvSubnetSubnet))
    {
        DHCP_TRC (MGMT, "Invalid Subnet");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_IPADDR);
        return SNMP_FAILURE;
    }

    /* Check any other Address Pool with same subnet exists */
    for (pPool = gpDhcpPool; pPool != NULL; pPool = pPool->pNext)
    {
        if ((pPool->u4PoolId != pEntry->u4PoolId)
            && (pPool->u4Subnet == u4TestValDhcpSrvSubnetSubnet))
        {
            DHCP_TRC2 (MGMT, "Address Pool : %d (%s) has the same subnet",
                       pPool->u4PoolId, pPool->au1PoolName);
            DHCP_TRC (MGMT,
                      "Please specify some other subnet or modify previous");

            CLI_SET_ERR (CLI_DHCPS_ADDRESSPOOL_SUBNET_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetPortNumber
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2DhcpSrvSubnetPortNumber (UINT4 *pu4ErrorCode,
                                  INT4 i4DhcpSrvSubnetPoolIndex,
                                  INT4 i4TestValDhcpSrvSubnetPortNumber)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (i4TestValDhcpSrvSubnetPortNumber < 0)
    {
        DHCP_TRC (MGMT, "Invalid Port Number");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_IFINDEX);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetMask
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2DhcpSrvSubnetMask (UINT4 *pu4ErrorCode,
                            INT4 i4DhcpSrvSubnetPoolIndex,
                            UINT4 u4TestValDhcpSrvSubnetMask)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }
    if (!DHCP_IS_VALID_NETMASK (u4TestValDhcpSrvSubnetMask))
    {
        DHCP_TRC (MGMT, "InValid Subnet Mask");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_SUBNETMASK);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetStartIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetStartIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2DhcpSrvSubnetStartIpAddress (UINT4 *pu4ErrorCode,
                                      INT4 i4DhcpSrvSubnetPoolIndex,
                                      UINT4
                                      u4TestValDhcpSrvSubnetStartIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }
    if (!DHCP_IS_VALID_IP (u4TestValDhcpSrvSubnetStartIpAddress))
    {
        DHCP_TRC (MGMT, "InValid IP Address");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_IPADDR);
        return SNMP_FAILURE;
    }

    if ((u4TestValDhcpSrvSubnetStartIpAddress & pEntry->u4NetMask) !=
        pEntry->u4Subnet)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_IPADDR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetEndIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetEndIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2DhcpSrvSubnetEndIpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4DhcpSrvSubnetPoolIndex,
                                    UINT4 u4TestValDhcpSrvSubnetEndIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }
    if (!DHCP_IS_VALID_IP (u4TestValDhcpSrvSubnetEndIpAddress))
    {
        DHCP_TRC (MGMT, "InValid IP Address");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_IPADDR);
        return SNMP_FAILURE;
    }
    if ((u4TestValDhcpSrvSubnetEndIpAddress & pEntry->u4NetMask) !=
        pEntry->u4Subnet)
    {
        CLI_SET_ERR (CLI_DHCPS_INV_IPADDR);
        return SNMP_FAILURE;
    }
    /*Dhcp server should not be broadcast IP Addres */
    if (u4TestValDhcpSrvSubnetEndIpAddress == (pEntry->u4Subnet &
                                               pEntry->u4NetMask) +
        (0xffffffff & (~pEntry->u4NetMask)))
    {
        DHCP_TRC (MGMT, "InValid IP Address");
        CLI_SET_ERR (CLI_DHCPS_INV_IPADDR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetLeaseTime
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetLeaseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2DhcpSrvSubnetLeaseTime (UINT4 *pu4ErrorCode,
                                 INT4 i4DhcpSrvSubnetPoolIndex,
                                 INT4 i4TestValDhcpSrvSubnetLeaseTime)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }
    if (i4TestValDhcpSrvSubnetLeaseTime <= 0)
    {
        DHCP_TRC (MGMT, "InValid Lease Time");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_LEASETIME);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetUtlThreshold
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetUtlThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvSubnetUtlThreshold (UINT4 *pu4ErrorCode,
                                    INT4 i4DhcpSrvSubnetPoolIndex,
                                    INT4 i4TestValDhcpSrvSubnetUtlThreshold)
{
    tDhcpPool          *pEntry;

    if (i4TestValDhcpSrvSubnetUtlThreshold < DHCPS_MIN_POOLTHRESHOLD ||
        i4TestValDhcpSrvSubnetUtlThreshold > DHCPS_MAX_POOLTHRESHOLD)
    {
        DHCP_TRC (MGMT, "Invalid threshold value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_THRESHOLD);
        return SNMP_FAILURE;
    }

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "make the RowStatus NOT_IN_SERVICE and try\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetPoolRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetPoolRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2DhcpSrvSubnetPoolRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4DhcpSrvSubnetPoolIndex,
                                     INT4 i4TestValDhcpSrvSubnetPoolRowStatus)
{
    INT1                Flag;
    Flag =
        nmhValidateIndexInstanceDhcpSrvSubnetPoolConfigTable
        (i4DhcpSrvSubnetPoolIndex);
    if (Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDhcpSrvSubnetPoolRowStatus == ACTIVE)
            || (i4TestValDhcpSrvSubnetPoolRowStatus == DESTROY)
            || (i4TestValDhcpSrvSubnetPoolRowStatus == NOT_IN_SERVICE))
            return SNMP_SUCCESS;
    }
    else
    {
        if (i4TestValDhcpSrvSubnetPoolRowStatus == CREATE_AND_WAIT)
            return SNMP_SUCCESS;

    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvSubnetPoolConfigTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvExcludeIpAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvExcludeIpAddressTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvExcludeIpAddressTable (INT4
                                                      i4DhcpSrvSubnetPoolIndex,
                                                      UINT4
                                                      u4DhcpSrvExcludeStartIpAddress)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) != NULL)
    {
        tExcludeIpAddr     *pNode;
        for (pNode = pEntry->pExcludeIpAddr; pNode != NULL;
             pNode = pNode->pNext)
        {
            if (pNode->u4StartIpAddr == u4DhcpSrvExcludeStartIpAddress)
                return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvExcludeIpAddressTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvExcludeIpAddressTable (INT4 *pi4DhcpSrvSubnetPoolIndex,
                                              UINT4
                                              *pu4DhcpSrvExcludeStartIpAddress)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExclude;
    UINT4               FirstIndex1;
    UINT4               FirstIndex2 = 0;
    INT1                Index2Flag = DHCP_NOT_FOUND;

    if (gpDhcpPool)
        FirstIndex1 = gpDhcpPool->u4PoolId;
    else
        return SNMP_FAILURE;

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if ((pEntry->u4PoolId <= FirstIndex1) || (Index2Flag == DHCP_NOT_FOUND))
        {
            if (pEntry->pExcludeIpAddr == NULL)
                continue;
            else
            {
                FirstIndex1 = pEntry->u4PoolId;
                FirstIndex2 = pEntry->pExcludeIpAddr->u4StartIpAddr;
                Index2Flag = DHCP_FOUND;
            }

            for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
                 pExclude = pExclude->pNext)
            {
                if (pExclude->u4StartIpAddr < FirstIndex2)
                    FirstIndex2 = pExclude->u4StartIpAddr;
            }
        }
    }

    if (Index2Flag == DHCP_NOT_FOUND)
        return SNMP_FAILURE;

    *pi4DhcpSrvSubnetPoolIndex = FirstIndex1;
    *pu4DhcpSrvExcludeStartIpAddress = FirstIndex2;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvExcludeIpAddressTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                nextDhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress
                nextDhcpSrvExcludeStartIpAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexDhcpSrvExcludeIpAddressTable (INT4 i4DhcpSrvSubnetPoolIndex,
                                             INT4
                                             *pi4NextDhcpSrvSubnetPoolIndex,
                                             UINT4
                                             u4DhcpSrvExcludeStartIpAddress,
                                             UINT4
                                             *pu4NextDhcpSrvExcludeStartIpAddress)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExclude;
    UINT1               u1IsFound = DHCP_NOT_FOUND;
    UINT4               u4ComparePoolIndex = 0xffffffff;
    UINT4               u4CompareIpAddr = 0xffffffff;

    /* Check whether the Pool Id is a valid integer */
    if (i4DhcpSrvSubnetPoolIndex < 0)
    {
        DHCP_TRC (MGMT, "Pool Id must be an integer value\n");
        return SNMP_FAILURE;
    }

    /* If the given pool id is not present, get the next valid pool id */
    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
        {
            if ((pEntry->u4PoolId > (UINT4) i4DhcpSrvSubnetPoolIndex) &&
                (pEntry->u4PoolId < u4ComparePoolIndex))
            {
                u4ComparePoolIndex = pEntry->u4PoolId;
                u1IsFound = DHCP_FOUND;
            }
        }
        if (u1IsFound == DHCP_FOUND)
        {
            pEntry = DhcpGetPoolEntry (u4ComparePoolIndex);
        }
        /* If no more pools are present, return a failure */
        else
        {
            DHCP_TRC (MGMT,
                      "No more pools are exist after the given pool id\n");
            return SNMP_FAILURE;
        }
    }

    /* Get the next Excluded Ip range to be displayed on the screen */
    if (pEntry != NULL)
    {

        for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
             pExclude = pExclude->pNext)
        {
            if ((pExclude->u4StartIpAddr > u4DhcpSrvExcludeStartIpAddress) &&
                (pExclude->u4StartIpAddr < u4CompareIpAddr))
            {
                u4CompareIpAddr = pExclude->u4StartIpAddr;
                u1IsFound = DHCP_FOUND;
            }
        }

    }

    if (u1IsFound == DHCP_FOUND)
    {
        if (pEntry != NULL)
        {

            *pi4NextDhcpSrvSubnetPoolIndex = pEntry->u4PoolId;

        }
        *pu4NextDhcpSrvExcludeStartIpAddress = u4CompareIpAddr;
        return SNMP_SUCCESS;
    }
    /* If there are no more excluded IP in the given pool, 
     * get the next Pool Id */
    else
    {
        for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
        {
            /* Get the next Pool Id which has the least PoolIndex */
            if ((pEntry->u4PoolId > (UINT4) i4DhcpSrvSubnetPoolIndex) &&
                (pEntry->u4PoolId < u4ComparePoolIndex))
            {
                u4ComparePoolIndex = pEntry->u4PoolId;
                u1IsFound = DHCP_FOUND;
            }
        }
        /* If the next poolId is found, get the next least 
         * Excluded Start IP Address */
        if (u1IsFound == DHCP_FOUND)
        {
            if (((pEntry = DhcpGetPoolEntry (u4ComparePoolIndex)) != NULL) &&
                (pEntry->pExcludeIpAddr) != NULL)
            {
                for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
                     pExclude = pExclude->pNext)
                {
                    if (pExclude->u4StartIpAddr < u4CompareIpAddr)
                    {
                        u4CompareIpAddr = pExclude->u4StartIpAddr;
                    }
                }
            }
            else
            {
                u4CompareIpAddr = 0;
            }
            *pi4NextDhcpSrvSubnetPoolIndex = u4ComparePoolIndex;
            *pu4NextDhcpSrvExcludeStartIpAddress = u4CompareIpAddr;
            return SNMP_SUCCESS;
        }
        /* If no more pools are present, return SNMP_FAILURE */
        else
        {
            DHCP_TRC (MGMT, "No more pools are present\n");
            return SNMP_FAILURE;
        }
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvExcludeEndIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress

                The Object 
                retValDhcpSrvExcludeEndIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvExcludeEndIpAddress (INT4 i4DhcpSrvSubnetPoolIndex,
                                  UINT4 u4DhcpSrvExcludeStartIpAddress,
                                  UINT4 *pu4RetValDhcpSrvExcludeEndIpAddress)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExclude;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;
    for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
         pExclude = pExclude->pNext)
    {
        if (pExclude->u4StartIpAddr == u4DhcpSrvExcludeStartIpAddress)
        {
            *pu4RetValDhcpSrvExcludeEndIpAddress = pExclude->u4EndIpAddr;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvExcludeAddressRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress

                The Object 
                retValDhcpSrvExcludeAddressRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetDhcpSrvExcludeAddressRowStatus (INT4 i4DhcpSrvSubnetPoolIndex,
                                      UINT4 u4DhcpSrvExcludeStartIpAddress,
                                      INT4
                                      *pi4RetValDhcpSrvExcludeAddressRowStatus)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExclude;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) != NULL)
    {
        for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
             pExclude = pExclude->pNext)
        {
            if (pExclude->u4StartIpAddr == u4DhcpSrvExcludeStartIpAddress)
            {
                *pi4RetValDhcpSrvExcludeAddressRowStatus = ACTIVE;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvExcludeEndIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress

                The Object 
                setValDhcpSrvExcludeEndIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvExcludeEndIpAddress (INT4 i4DhcpSrvSubnetPoolIndex,
                                  UINT4 u4DhcpSrvExcludeStartIpAddress,
                                  UINT4 u4SetValDhcpSrvExcludeEndIpAddress)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExclude;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) != NULL)
    {
        for (pExclude = pEntry->pExcludeIpAddr; pExclude != NULL;
             pExclude = pExclude->pNext)
        {
            if (pExclude->u4StartIpAddr == u4DhcpSrvExcludeStartIpAddress)
            {
                if (pExclude->u4StartIpAddr <=
                    u4SetValDhcpSrvExcludeEndIpAddress)
                {
                    pExclude->u4EndIpAddr = u4SetValDhcpSrvExcludeEndIpAddress;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetDhcpSrvExcludeAddressRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress

                The Object 
                setValDhcpSrvExcludeAddressRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvExcludeAddressRowStatus (INT4 i4DhcpSrvSubnetPoolIndex,
                                      UINT4 u4DhcpSrvExcludeStartIpAddress,
                                      INT4
                                      i4SetValDhcpSrvExcludeAddressRowStatus)
{
    tDhcpPool          *pEntry;
    tExcludeIpAddr     *pExcludeNode;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Entry Found");
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    switch (i4SetValDhcpSrvExcludeAddressRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            if ((u4DhcpSrvExcludeStartIpAddress < pEntry->u4StartIpAddr)
                && (u4DhcpSrvExcludeStartIpAddress > pEntry->u4EndIpAddr))
            {
                DHCP_TRC (MGMT, "Invalid  Exclude Start Ip Address\n");
                return SNMP_FAILURE;
            }

            if ((pExcludeNode =
                 (tExcludeIpAddr *) MemAllocMemBlk ((tMemPoolId)
                                                    i4DhcpSrvExclAddrId)) ==
                NULL)
            {
                DHCP_TRC (MGMT, "Memory allocation failure\n");
                return SNMP_FAILURE;
            }
            pExcludeNode->u4StartIpAddr = pExcludeNode->u4EndIpAddr =
                u4DhcpSrvExcludeStartIpAddress;

            pExcludeNode->u4Method = DHCP_SNMP;

            DhcpAddExcludedListNode (&pEntry->pExcludeIpAddr, pExcludeNode);

            break;

        case ACTIVE:

            for (pExcludeNode = pEntry->pExcludeIpAddr; pExcludeNode != NULL;
                 pExcludeNode = pExcludeNode->pNext)
            {
                if ((pExcludeNode->u4StartIpAddr
                     == u4DhcpSrvExcludeStartIpAddress)
                    && (pExcludeNode->u4Method == DHCP_SNMP))
                {
                    /* Recalculate the available IP count in the pool */
                    DhcpSrvCalculateIpCount (pEntry);
                    return SNMP_SUCCESS;
                }
            }

            return SNMP_FAILURE;

        case DESTROY:
            for (pExcludeNode = pEntry->pExcludeIpAddr; pExcludeNode != NULL;
                 pExcludeNode = pExcludeNode->pNext)
            {
                if (pExcludeNode->u4StartIpAddr
                    == u4DhcpSrvExcludeStartIpAddress)
                {
                    DhcpRemoveExcludedListNode (&pEntry->pExcludeIpAddr,
                                                pExcludeNode);
                    MemReleaseMemBlock ((tMemPoolId) i4DhcpSrvExclAddrId,
                                        (UINT1 *) pExcludeNode);
                    /* Recalculate the available IP count in the pool */
                    DhcpSrvCalculateIpCount (pEntry);
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;

        default:
            return SNMP_FAILURE;

    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvExcludeEndIpAddress
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress

                The Object 
                testValDhcpSrvExcludeEndIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvExcludeEndIpAddress (UINT4 *pu4ErrorCode,
                                     INT4 i4DhcpSrvSubnetPoolIndex,
                                     UINT4 u4DhcpSrvExcludeStartIpAddress,
                                     UINT4 u4TestValDhcpSrvExcludeEndIpAddress)
{
    tDhcpPool          *pEntry;
    tDhcpHostEntry     *pHostEntry = NULL;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }
    if ((!DHCP_IS_VALID_IP (u4TestValDhcpSrvExcludeEndIpAddress))
        || (u4DhcpSrvExcludeStartIpAddress >
            u4TestValDhcpSrvExcludeEndIpAddress))
    {
        DHCP_TRC (MGMT, "InValid Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_ENDIP);
        return SNMP_FAILURE;
    }

    if (u4TestValDhcpSrvExcludeEndIpAddress > pEntry->u4EndIpAddr)

    {
        DHCP_TRC (MGMT, "InValid End Ip Address ");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_ENDIP);
        return SNMP_FAILURE;
    }

    /*Start IP is lessar than exclude pool range, Dont allow for programming */
    if (u4DhcpSrvExcludeStartIpAddress < pEntry->u4StartIpAddr)

    {
        DHCP_TRC (MGMT, "Invalid Start IP Address ");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STARTIP);
        return SNMP_FAILURE;
    }

    for (pHostEntry = gpDhcpHostEntry; pHostEntry != NULL;
         pHostEntry = pHostEntry->pNext)
    {
        if (pHostEntry->u4PoolId == (UINT4) i4DhcpSrvSubnetPoolIndex)
        {
            if ((u4DhcpSrvExcludeStartIpAddress <= pHostEntry->u4StaticIpAddr)
                && (u4TestValDhcpSrvExcludeEndIpAddress >=
                    pHostEntry->u4StaticIpAddr))
            {
                DHCP_TRC (MGMT, "Static IP clash in the excluded ip range");
                *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                CLI_SET_ERR (CLI_DHCPS_STATIC_HOST_OVERLAP);
                return SNMP_FAILURE;
            }

        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvExcludeAddressRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress

                The Object 
                testValDhcpSrvExcludeAddressRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvExcludeAddressRowStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4DhcpSrvSubnetPoolIndex,
                                         UINT4 u4DhcpSrvExcludeStartIpAddress,
                                         INT4
                                         i4TestValDhcpSrvExcludeAddressRowStatus)
{
    tDhcpPool          *pEntry;
    INT1                Flag;
    if ((MemGetFreeUnits (i4DhcpSrvExclAddrId) == 0)
        && ((i4TestValDhcpSrvExcludeAddressRowStatus == CREATE_AND_WAIT)
            || (i4TestValDhcpSrvExcludeAddressRowStatus == CREATE_AND_GO)))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_RESOURCE_UNAVAILABLE;
        DHCP_TRC (MGMT, "Max excluded pools Reached!\n");
        return SNMP_FAILURE;
    }

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_POOL_ID);
        return SNMP_FAILURE;
    }

    /* To check if we add an exclude address pool for an existing pool
     * but with different subnet 
     */
    if ((u4DhcpSrvExcludeStartIpAddress & pEntry->u4NetMask) !=
        pEntry->u4Subnet)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STARTIP);
        return SNMP_FAILURE;
    }

    Flag =
        nmhValidateIndexInstanceDhcpSrvExcludeIpAddressTable
        (i4DhcpSrvSubnetPoolIndex, u4DhcpSrvExcludeStartIpAddress);
    if (Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDhcpSrvExcludeAddressRowStatus == ACTIVE)
            || (i4TestValDhcpSrvExcludeAddressRowStatus == DESTROY))
            return SNMP_SUCCESS;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
    }
    else
    {
        if ((i4TestValDhcpSrvExcludeAddressRowStatus == CREATE_AND_WAIT)
            || (i4TestValDhcpSrvExcludeAddressRowStatus == CREATE_AND_GO))
            return SNMP_SUCCESS;
        CLI_SET_ERR (CLI_DHCPS_INV_EXCL_POOL);

    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvExcludeIpAddressTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvExcludeStartIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvExcludeIpAddressTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvGblOptTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvGblOptTable
 Input       :  The Indices
                DhcpSrvGblOptType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvGblOptTable (INT4 i4DhcpSrvGblOptType)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);
    if (pOptions != NULL)
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvGblOptTable
 Input       :  The Indices
                DhcpSrvGblOptType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvGblOptTable (INT4 *pi4DhcpSrvGblOptType)
{
    tDhcpOptions       *pEntry;
    UINT4               FirstIndex;

    if (gpDhcpOptions)
        FirstIndex = gpDhcpOptions->u4Type;
    else
        return SNMP_FAILURE;

    for (pEntry = gpDhcpOptions; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if (pEntry->u4Type < FirstIndex)
            FirstIndex = pEntry->u4Type;
    }
    *pi4DhcpSrvGblOptType = FirstIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvGblOptTable
 Input       :  The Indices
                DhcpSrvGblOptType
                nextDhcpSrvGblOptType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpSrvGblOptTable (INT4 i4DhcpSrvGblOptType,
                                   INT4 *pi4NextDhcpSrvGblOptType)
{
    tDhcpOptions       *pOptions;
    UINT4               NextIndex = i4DhcpSrvGblOptType;
    INT1                Flag = DHCP_NOT_FOUND;
    if (i4DhcpSrvGblOptType < 0)
    {
        return SNMP_FAILURE;
    }

    if (gpDhcpOptions)
        pOptions = gpDhcpOptions;
    else
        return SNMP_FAILURE;

    for (pOptions = gpDhcpOptions; pOptions != NULL; pOptions = pOptions->pNext)
    {
        if ((pOptions->u4Type > (UINT4) i4DhcpSrvGblOptType)
            && ((Flag == DHCP_NOT_FOUND) || (pOptions->u4Type < NextIndex)))
        {
            NextIndex = pOptions->u4Type;
            Flag = DHCP_FOUND;
        }
    }

    if (Flag == DHCP_NOT_FOUND)
        return SNMP_FAILURE;

    *pi4NextDhcpSrvGblOptType = NextIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvGblOptLen
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                retValDhcpSrvGblOptLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvGblOptLen (INT4 i4DhcpSrvGblOptType,
                        INT4 *pi4RetValDhcpSrvGblOptLen)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);
    if (pOptions == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvGblOptLen = pOptions->u4Len;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvGblOptVal
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                retValDhcpSrvGblOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvGblOptVal (INT4 i4DhcpSrvGblOptType,
                        tSNMP_OCTET_STRING_TYPE * pRetValDhcpSrvGblOptVal)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);
    if (pOptions == NULL)
        return SNMP_FAILURE;

    MEMCPY (pRetValDhcpSrvGblOptVal->pu1_OctetList,
            pOptions->u1Value, pOptions->u4Len);
    pRetValDhcpSrvGblOptVal->i4_Length = pOptions->u4Len;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvGblOptRowStatus
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                retValDhcpSrvGblOptRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvGblOptRowStatus (INT4 i4DhcpSrvGblOptType,
                              INT4 *pi4RetValDhcpSrvGblOptRowStatus)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);
    if (pOptions == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvGblOptRowStatus = pOptions->u4Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvGblOptLen
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                setValDhcpSrvGblOptLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvGblOptLen (INT4 i4DhcpSrvGblOptType, INT4 i4SetValDhcpSrvGblOptLen)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);

    if (pOptions == NULL)
    {
        return SNMP_FAILURE;
    }
    pOptions->u4Len = i4SetValDhcpSrvGblOptLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvGblOptVal
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                setValDhcpSrvGblOptVal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvGblOptVal (INT4 i4DhcpSrvGblOptType,
                        tSNMP_OCTET_STRING_TYPE * pSetValDhcpSrvGblOptVal)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);

    if (pOptions == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pOptions->u1Value, pSetValDhcpSrvGblOptVal->pu1_OctetList,
            pSetValDhcpSrvGblOptVal->i4_Length);
    pOptions->u4Len = pSetValDhcpSrvGblOptVal->i4_Length;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvGblOptRowStatus
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                setValDhcpSrvGblOptRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvGblOptRowStatus (INT4 i4DhcpSrvGblOptType,
                              INT4 i4SetValDhcpSrvGblOptRowStatus)
{
    tDhcpOptions       *pOptions = NULL;

    if (i4SetValDhcpSrvGblOptRowStatus != CREATE_AND_WAIT)
        pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);

    switch (i4SetValDhcpSrvGblOptRowStatus)
    {
        case CREATE_AND_WAIT:
            if ((pOptions = DhcpGetFreeOptionRec ()) == NULL)
            {
                DHCP_TRC (MGMT, "No more options can be created");
                return SNMP_FAILURE;
            }

            pOptions->u4Type = i4DhcpSrvGblOptType;
            pOptions->u4Status = NOT_READY;

            DhcpAddOptionToList (&gpDhcpOptions, pOptions);

            break;

        case ACTIVE:

            if (pOptions != NULL)
            {
                if (DhcpValidateConfOptions ((UINT1) i4DhcpSrvGblOptType,
                                             pOptions->u4Len,
                                             pOptions->u1Value) == DHCP_FAILURE)
                {
                    DhcpRemoveOptionFromList (&gpDhcpOptions, pOptions);
                    DhcpFreeOptionRec (pOptions);
                    return SNMP_FAILURE;
                }

                pOptions->u4Status = ACTIVE;
            }
            break;

        case NOT_IN_SERVICE:

            if (pOptions != NULL)
            {
                pOptions->u4Status = NOT_IN_SERVICE;
            }
            break;

        case DESTROY:

            DhcpRemoveOptionFromList (&gpDhcpOptions, pOptions);
            DhcpFreeOptionRec (pOptions);
            break;

    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvGblOptLen
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                testValDhcpSrvGblOptLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvGblOptLen (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvGblOptType,
                           INT4 i4TestValDhcpSrvGblOptLen)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);

    if (pOptions == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pOptions->u4Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }
    if ((i4TestValDhcpSrvGblOptLen > DHCP_MAX_OPT_LEN)
        || (i4TestValDhcpSrvGblOptLen <= 0))
    {
        DHCP_TRC (MGMT, "Wron Option Length\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_OPTION_LEN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvGblOptVal
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                testValDhcpSrvGblOptVal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvGblOptVal (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvGblOptType,
                           tSNMP_OCTET_STRING_TYPE * pTestValDhcpSrvGblOptVal)
{
    tDhcpOptions       *pOptions;

    pOptions = DhcpGetOptionFromList (gpDhcpOptions, i4DhcpSrvGblOptType);
    if (pOptions == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pOptions->u4Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    if ((pTestValDhcpSrvGblOptVal->i4_Length <= 0)
        || (pTestValDhcpSrvGblOptVal->i4_Length > DHCP_MAX_OPT_LEN))
    {
        DHCP_TRC (MGMT, "Wrong length");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_OPTION_LEN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvGblOptRowStatus
 Input       :  The Indices
                DhcpSrvGblOptType

                The Object 
                testValDhcpSrvGblOptRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvGblOptRowStatus (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvGblOptType,
                                 INT4 i4TestValDhcpSrvGblOptRowStatus)
{
    INT1                Flag;

    Flag = nmhValidateIndexInstanceDhcpSrvGblOptTable (i4DhcpSrvGblOptType);
    if (Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDhcpSrvGblOptRowStatus == ACTIVE)
            || (i4TestValDhcpSrvGblOptRowStatus == NOT_IN_SERVICE)
            || (i4TestValDhcpSrvGblOptRowStatus == DESTROY))
            return SNMP_SUCCESS;
    }
    else
    {
        if (i4TestValDhcpSrvGblOptRowStatus == CREATE_AND_WAIT)
            return SNMP_SUCCESS;

    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvGblOptTable
 Input       :  The Indices
                DhcpSrvGblOptType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvGblOptTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvSubnetOptTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvSubnetOptTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvSubnetOptTable (INT4 i4DhcpSrvSubnetPoolIndex,
                                               INT4 i4DhcpSrvSubnetOptType)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) != NULL)
    {
        tDhcpOptions       *pOptions;
        if ((pOptions =
             DhcpGetOptionFromList (pEntry->pOptions,
                                    i4DhcpSrvSubnetOptType)) != NULL)
        {
            UNUSED_PARAM (pOptions);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvSubnetOptTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvSubnetOptTable (INT4 *pi4DhcpSrvSubnetPoolIndex,
                                       INT4 *pi4DhcpSrvSubnetOptType)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;
    UINT4               FirstIndex1;
    UINT4               FirstIndex2 = 0;
    INT1                Index2Flag = DHCP_NOT_FOUND;

    if (gpDhcpPool)
        FirstIndex1 = gpDhcpPool->u4PoolId;
    else
        return SNMP_FAILURE;

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if ((pEntry->u4PoolId <= FirstIndex1) || (Index2Flag == DHCP_NOT_FOUND))
        {
            if (pEntry->pOptions == NULL)
                continue;
            else
            {
                FirstIndex1 = pEntry->u4PoolId;
                FirstIndex2 = pEntry->pOptions->u4Type;
                Index2Flag = DHCP_FOUND;
            }

            for (pOptions = pEntry->pOptions; pOptions != NULL;
                 pOptions = pOptions->pNext)
            {
                if (pOptions->u4Type < FirstIndex2)
                    FirstIndex2 = pOptions->u4Type;
            }
        }
    }

    if (Index2Flag == DHCP_NOT_FOUND)
        return SNMP_FAILURE;

    *pi4DhcpSrvSubnetPoolIndex = FirstIndex1;
    *pi4DhcpSrvSubnetOptType = FirstIndex2;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvSubnetOptTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                nextDhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType
                nextDhcpSrvSubnetOptType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpSrvSubnetOptTable (INT4 i4DhcpSrvSubnetPoolIndex,
                                      INT4 *pi4NextDhcpSrvSubnetPoolIndex,
                                      INT4 i4DhcpSrvSubnetOptType,
                                      INT4 *pi4NextDhcpSrvSubnetOptType)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    UINT1               u1IsFound = DHCP_NOT_FOUND;
    UINT4               u4ComparePoolIndex = 0;
    UINT4               u4CompareOptType = 0;

    if (i4DhcpSrvSubnetPoolIndex < 0)
    {
        return SNMP_FAILURE;
    }

    if (i4DhcpSrvSubnetOptType < 0)
    {
        i4DhcpSrvSubnetOptType = 0;
        i4DhcpSrvSubnetPoolIndex++;
    }

    for (pEntry = gpDhcpPool; pEntry != NULL; pEntry = pEntry->pNext)
    {
        for (pOptions = pEntry->pOptions; pOptions != NULL;
             pOptions = pOptions->pNext)
        {
            /* Checking if the entries are equal or if the entry is lesser than
             * the given entry !!*/
            if ((pEntry->u4PoolId == (UINT4) i4DhcpSrvSubnetPoolIndex) &&
                (pOptions->u4Type == (UINT4) i4DhcpSrvSubnetOptType))
            {
                continue;
            }
            if (pEntry->u4PoolId < (UINT4) i4DhcpSrvSubnetPoolIndex)
            {
                continue;
            }
            if ((pEntry->u4PoolId == (UINT4) i4DhcpSrvSubnetPoolIndex) &&
                (pOptions->u4Type < (UINT4) i4DhcpSrvSubnetOptType))
            {
                continue;
            }
            if (u1IsFound == DHCP_NOT_FOUND)
            {
                u4ComparePoolIndex = pEntry->u4PoolId;
                u4CompareOptType = pOptions->u4Type;
                u1IsFound = DHCP_FOUND;
            }
            if (u1IsFound == DHCP_FOUND)
            {
                /*Getting the Next entry in lexicographic order */

                if ((pEntry->u4PoolId >= (UINT4) i4DhcpSrvSubnetPoolIndex) &&
                    (pEntry->u4PoolId < u4ComparePoolIndex))
                {
                    u4ComparePoolIndex = pEntry->u4PoolId;
                    u4CompareOptType = pOptions->u4Type;
                    continue;
                }

                if ((pEntry->u4PoolId == u4ComparePoolIndex) &&
                    (pOptions->u4Type < u4CompareOptType))
                {
                    u4ComparePoolIndex = pEntry->u4PoolId;
                    u4CompareOptType = pOptions->u4Type;
                    continue;
                }
            }
        }
    }
    /* Fill up the next index */
    if (u1IsFound == DHCP_FOUND)
    {
        *pi4NextDhcpSrvSubnetPoolIndex = u4ComparePoolIndex;
        *pi4NextDhcpSrvSubnetOptType = u4CompareOptType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetOptLen
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                retValDhcpSrvSubnetOptLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvSubnetOptLen (INT4 i4DhcpSrvSubnetPoolIndex,
                           INT4 i4DhcpSrvSubnetOptType,
                           INT4 *pi4RetValDhcpSrvSubnetOptLen)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if ((pOptions =
         DhcpGetOptionFromList (pEntry->pOptions,
                                i4DhcpSrvSubnetOptType)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvSubnetOptLen = pOptions->u4Len;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetOptVal
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                retValDhcpSrvSubnetOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvSubnetOptVal (INT4 i4DhcpSrvSubnetPoolIndex,
                           INT4 i4DhcpSrvSubnetOptType,
                           tSNMP_OCTET_STRING_TYPE * pRetValDhcpSrvSubnetOptVal)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if ((pOptions =
         DhcpGetOptionFromList (pEntry->pOptions,
                                i4DhcpSrvSubnetOptType)) == NULL)
        return SNMP_FAILURE;

    MEMCPY (pRetValDhcpSrvSubnetOptVal->pu1_OctetList,
            pOptions->u1Value, pOptions->u4Len);

    pRetValDhcpSrvSubnetOptVal->i4_Length = pOptions->u4Len;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetOptRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                retValDhcpSrvSubnetOptRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvSubnetOptRowStatus (INT4 i4DhcpSrvSubnetPoolIndex,
                                 INT4 i4DhcpSrvSubnetOptType,
                                 INT4 *pi4RetValDhcpSrvSubnetOptRowStatus)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if ((pOptions =
         DhcpGetOptionFromList (pEntry->pOptions,
                                i4DhcpSrvSubnetOptType)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvSubnetOptRowStatus = pOptions->u4Status;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetOptLen
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                setValDhcpSrvSubnetOptLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetOptLen (INT4 i4DhcpSrvSubnetPoolIndex,
                           INT4 i4DhcpSrvSubnetOptType,
                           INT4 i4SetValDhcpSrvSubnetOptLen)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (((pOptions =
          DhcpGetOptionFromList (pEntry->pOptions,
                                 i4DhcpSrvSubnetOptType)) == NULL)
        || (pOptions->u4Status == ACTIVE))
        return SNMP_FAILURE;

    pOptions->u4Len = i4SetValDhcpSrvSubnetOptLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetOptVal
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                setValDhcpSrvSubnetOptVal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetOptVal (INT4 i4DhcpSrvSubnetPoolIndex,
                           INT4 i4DhcpSrvSubnetOptType,
                           tSNMP_OCTET_STRING_TYPE * pSetValDhcpSrvSubnetOptVal)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (((pOptions =
          DhcpGetOptionFromList (pEntry->pOptions,
                                 i4DhcpSrvSubnetOptType)) == NULL)
        || (pOptions->u4Status == ACTIVE))
        return SNMP_FAILURE;

    MEMCPY (pOptions->u1Value, pSetValDhcpSrvSubnetOptVal->pu1_OctetList,
            pSetValDhcpSrvSubnetOptVal->i4_Length);
    pOptions->u4Len = pSetValDhcpSrvSubnetOptVal->i4_Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetOptRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                setValDhcpSrvSubnetOptRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetOptRowStatus (INT4 i4DhcpSrvSubnetPoolIndex,
                                 INT4 i4DhcpSrvSubnetOptType,
                                 INT4 i4SetValDhcpSrvSubnetOptRowStatus)
{
    tDhcpOptions       *pOptions = NULL;
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (i4SetValDhcpSrvSubnetOptRowStatus != CREATE_AND_WAIT)
    {
        pOptions = DhcpGetOptionFromList (pEntry->pOptions,
                                          i4DhcpSrvSubnetOptType);
        if (pOptions == NULL)
        {
            return (SNMP_FAILURE);
        }
    }

    switch (i4SetValDhcpSrvSubnetOptRowStatus)
    {
        case CREATE_AND_WAIT:

            if ((pOptions = DhcpGetFreeOptionRec ()) == NULL)
            {
                return SNMP_FAILURE;
            }
            pOptions->u4Type = i4DhcpSrvSubnetOptType;
            pOptions->u4Status = NOT_READY;
            DhcpAddOptionToList (&pEntry->pOptions, pOptions);
            break;

        case ACTIVE:

            if (DhcpValidateConfOptions ((UINT1) i4DhcpSrvSubnetOptType,
                                         pOptions->u4Len,
                                         pOptions->u1Value) == DHCP_FAILURE)
            {
                DHCP_TRC (MGMT, "Invalid Values for Option Length/Value");
                return SNMP_FAILURE;
            }

            pOptions->u4Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pOptions->u4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            DhcpRemoveOptionFromList (&pEntry->pOptions, pOptions);
            DhcpFreeOptionRec (pOptions);
            break;

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetOptLen
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                testValDhcpSrvSubnetOptLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvSubnetOptLen (UINT4 *pu4ErrorCode,
                              INT4 i4DhcpSrvSubnetPoolIndex,
                              INT4 i4DhcpSrvSubnetOptType,
                              INT4 i4TestValDhcpSrvSubnetOptLen)
{
    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if (((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        || (pOptions = DhcpGetOptionFromList (pEntry->pOptions,
                                              i4DhcpSrvSubnetOptType)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pOptions->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    if ((i4TestValDhcpSrvSubnetOptLen > DHCP_MAX_OPT_LEN)
        || (i4TestValDhcpSrvSubnetOptLen <= 0))
    {
        DHCP_TRC (MGMT, "Wrong Option Length");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_OPTION_LEN);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetOptVal
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                testValDhcpSrvSubnetOptVal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvSubnetOptVal (UINT4 *pu4ErrorCode,
                              INT4 i4DhcpSrvSubnetPoolIndex,
                              INT4 i4DhcpSrvSubnetOptType,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValDhcpSrvSubnetOptVal)
{

    tDhcpPool          *pEntry;
    tDhcpOptions       *pOptions;

    if (((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL) ||
        (pOptions = DhcpGetOptionFromList (pEntry->pOptions,
                                           i4DhcpSrvSubnetOptType)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pOptions->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    if ((pTestValDhcpSrvSubnetOptVal->i4_Length > DHCP_MAX_OPT_LEN)
        || (pTestValDhcpSrvSubnetOptVal->i4_Length <= 0))
    {
        DHCP_TRC (MGMT, "Too big option value ... ");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_OPTION_LEN);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetOptRowStatus
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType

                The Object 
                testValDhcpSrvSubnetOptRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvSubnetOptRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4DhcpSrvSubnetPoolIndex,
                                    INT4 i4DhcpSrvSubnetOptType,
                                    INT4 i4TestValDhcpSrvSubnetOptRowStatus)
{
    INT1                Flag;

    Flag =
        nmhValidateIndexInstanceDhcpSrvSubnetOptTable (i4DhcpSrvSubnetPoolIndex,
                                                       i4DhcpSrvSubnetOptType);
    if (Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDhcpSrvSubnetOptRowStatus == ACTIVE)
            || (i4TestValDhcpSrvSubnetOptRowStatus == NOT_IN_SERVICE)
            || (i4TestValDhcpSrvSubnetOptRowStatus == DESTROY))
            return SNMP_SUCCESS;

        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
    }
    else
    {
        if (i4TestValDhcpSrvSubnetOptRowStatus == CREATE_AND_WAIT)
            return SNMP_SUCCESS;

        CLI_SET_ERR (CLI_DHCPS_INV_OPTION_TYPE);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvSubnetOptTable
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex
                DhcpSrvSubnetOptType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvSubnetOptTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvHostOptTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvHostOptTable
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvHostOptTable (INT4 i4DhcpSrvHostType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pDhcpSrvHostId,
                                             INT4 i4DhcpSrvSubnetPoolIndex,
                                             INT4 i4DhcpSrvHostOptType)
{
    tDhcpHostEntry     *pEntry;

    pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex);
    if (pEntry != NULL)
    {
        tDhcpOptions       *pOptions;
        if ((pOptions =
             DhcpGetOptionFromList (pEntry->pOptions,
                                    i4DhcpSrvHostOptType)) != NULL)
        {
            UNUSED_PARAM (pOptions);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvHostOptTable
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvHostOptTable (INT4 *pi4DhcpSrvHostType,
                                     tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                     INT4 *pi4DhcpSrvSubnetPoolIndex,
                                     INT4 *pi4DhcpSrvHostOptType)
{
    tDhcpHostEntry     *pHost, *pFirstHost;
    tDhcpOptions       *pOptions = NULL, *pFirstOption = NULL;
    INT1                Index4Flag = DHCP_NOT_FOUND;

    if (gpDhcpHostEntry)
        pFirstHost = gpDhcpHostEntry;
    else
        return SNMP_FAILURE;

    for (pHost = gpDhcpHostEntry; pHost != NULL; pHost = pHost->pNext)
    {
        if (DhcpHostCompare (pHost, pFirstHost) <= 0
            || (Index4Flag == DHCP_NOT_FOUND))
        {
            if (pHost->pOptions)
            {
                pFirstHost = pHost;
                Index4Flag = DHCP_FOUND;
                pFirstOption = pHost->pOptions;
            }
            else
                continue;

            for (pOptions = pHost->pOptions; pOptions;
                 pOptions = pOptions->pNext)
            {
                if (pOptions->u4Type < pFirstOption->u4Type)
                    pFirstOption = pOptions;
            }
        }
    }

    if (Index4Flag == DHCP_NOT_FOUND)
        return SNMP_FAILURE;

    *pi4DhcpSrvHostType = pFirstHost->u1HwType;
    MEMCPY (pDhcpSrvHostId->pu1_OctetList, pFirstHost->aClientMac,
            MEM_MAX_BYTES (pFirstHost->u1HostIdLen, DHCP_MAX_CID_LEN));

    pDhcpSrvHostId->i4_Length = pFirstHost->u1HostIdLen;
    *pi4DhcpSrvSubnetPoolIndex = pFirstHost->u4PoolId;
    *pi4DhcpSrvHostOptType = pFirstOption->u4Type;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvHostOptTable
 Input       :  The Indices
                DhcpSrvHostType
                nextDhcpSrvHostType
                DhcpSrvHostId
                nextDhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                nextDhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType
                nextDhcpSrvHostOptType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpSrvHostOptTable (INT4 i4DhcpSrvHostType,
                                    INT4 *pi4NextDhcpSrvHostType,
                                    tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextDhcpSrvHostId,
                                    INT4 i4DhcpSrvSubnetPoolIndex,
                                    INT4 *pi4NextDhcpSrvSubnetPoolIndex,
                                    INT4 i4DhcpSrvHostOptType,
                                    INT4 *pi4NextDhcpSrvHostOptType)
/* GET_NEXT Routine.  */
{
    tDhcpHostEntry     *pHost, *pScanHost, *pNextHost;
    tDhcpOptions       *pOptions;
    INT4                Flag = DHCP_NOT_FOUND;
    UINT4               NextOptType = 0;

    if (i4DhcpSrvHostType < 0)
    {
        return SNMP_FAILURE;
    }

    if (pDhcpSrvHostId->i4_Length < 0)
    {
        pDhcpSrvHostId->i4_Length = 0;
        i4DhcpSrvHostType++;
    }

    if (i4DhcpSrvSubnetPoolIndex < 0)
    {
        i4DhcpSrvSubnetPoolIndex = 0;
        /* increment the last byte of the previous octet string
         * to get next max 
         */
        pDhcpSrvHostId->pu1_OctetList[pDhcpSrvHostId->i4_Length - 1]++;
    }

    if (i4DhcpSrvHostOptType < 0)
    {
        i4DhcpSrvHostOptType = 0;
        i4DhcpSrvSubnetPoolIndex++;
    }

    pHost = DhcpGetHostEntry (i4DhcpSrvHostType, pDhcpSrvHostId->pu1_OctetList,
                              pDhcpSrvHostId->i4_Length,
                              i4DhcpSrvSubnetPoolIndex);
    if (pHost == NULL)
        return SNMP_FAILURE;
    pNextHost = pHost;
    NextOptType = i4DhcpSrvHostOptType;

    for (pScanHost = gpDhcpHostEntry; pScanHost != NULL;
         pScanHost = pScanHost->pNext)
    {
        for (pOptions = pScanHost->pOptions; pOptions != NULL;
             pOptions = pOptions->pNext)
        {
            if ((DhcpHostOptCompare (pScanHost, pOptions->u4Type, pHost,
                                     i4DhcpSrvHostOptType) > 0)
                && ((DhcpHostOptCompare (pScanHost, pOptions->u4Type,
                                         pNextHost, NextOptType) < 0)
                    || (Flag == DHCP_NOT_FOUND)))
            {
                pNextHost = pScanHost;
                NextOptType = pOptions->u4Type;
                Flag = DHCP_FOUND;
            }
        }
    }

    if ((pHost == pNextHost) && (NextOptType == (UINT4) i4DhcpSrvHostOptType))
        return SNMP_FAILURE;

    *pi4NextDhcpSrvHostType = pNextHost->u1HwType;
    MEMCPY (pNextDhcpSrvHostId->pu1_OctetList, pNextHost->aClientMac,
            MEM_MAX_BYTES (pNextHost->u1HostIdLen, DHCP_MAX_CID_LEN));

    pNextDhcpSrvHostId->i4_Length = pNextHost->u1HostIdLen;
    *pi4NextDhcpSrvSubnetPoolIndex = pNextHost->u4PoolId;
    *pi4NextDhcpSrvHostOptType = NextOptType;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDhcpSrvHostOptLen
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                retValDhcpSrvHostOptLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostOptLen (INT4 i4DhcpSrvHostType,
                         tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                         INT4 i4DhcpSrvSubnetPoolIndex,
                         INT4 i4DhcpSrvHostOptType,
                         INT4 *pi4RetValDhcpSrvHostOptLen)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;

    pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex);
    if (pEntry == NULL)
        return SNMP_FAILURE;
    pOptions = DhcpGetOptionFromList (pEntry->pOptions, i4DhcpSrvHostOptType);
    if (pOptions == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvHostOptLen = pOptions->u4Len;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostOptVal
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                retValDhcpSrvHostOptVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostOptVal (INT4 i4DhcpSrvHostType,
                         tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                         INT4 i4DhcpSrvSubnetPoolIndex,
                         INT4 i4DhcpSrvHostOptType,
                         tSNMP_OCTET_STRING_TYPE * pRetValDhcpSrvHostOptVal)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;
    pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex);
    if (pEntry == NULL)
        return SNMP_FAILURE;
    pOptions = DhcpGetOptionFromList (pEntry->pOptions, i4DhcpSrvHostOptType);
    if (pOptions == NULL)
        return SNMP_FAILURE;

    MEMCPY (pRetValDhcpSrvHostOptVal->pu1_OctetList,
            pOptions->u1Value, pOptions->u4Len);
    pRetValDhcpSrvHostOptVal->i4_Length = pOptions->u4Len;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostOptRowStatus
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                retValDhcpSrvHostOptRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostOptRowStatus (INT4 i4DhcpSrvHostType,
                               tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                               INT4 i4DhcpSrvSubnetPoolIndex,
                               INT4 i4DhcpSrvHostOptType,
                               INT4 *pi4RetValDhcpSrvHostOptRowStatus)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;

    pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex);
    if (pEntry == NULL)
        return SNMP_FAILURE;
    pOptions = DhcpGetOptionFromList (pEntry->pOptions, i4DhcpSrvHostOptType);
    if (pOptions == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvHostOptRowStatus = pOptions->u4Status;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostOptLen
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                setValDhcpSrvHostOptLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostOptLen (INT4 i4DhcpSrvHostType,
                         tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                         INT4 i4DhcpSrvSubnetPoolIndex,
                         INT4 i4DhcpSrvHostOptType,
                         INT4 i4SetValDhcpSrvHostOptLen)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;

    pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex);
    if (pEntry == NULL)
        return SNMP_FAILURE;
    pOptions = DhcpGetOptionFromList (pEntry->pOptions, i4DhcpSrvHostOptType);
    if ((pOptions == NULL) || (pOptions->u4Status == ACTIVE))
        return SNMP_FAILURE;

    pOptions->u4Len = i4SetValDhcpSrvHostOptLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostOptVal
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                setValDhcpSrvHostOptVal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostOptVal (INT4 i4DhcpSrvHostType,
                         tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                         INT4 i4DhcpSrvSubnetPoolIndex,
                         INT4 i4DhcpSrvHostOptType,
                         tSNMP_OCTET_STRING_TYPE * pSetValDhcpSrvHostOptVal)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;

    pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex);
    if (pEntry == NULL)
        return SNMP_FAILURE;

    pOptions = DhcpGetOptionFromList (pEntry->pOptions, i4DhcpSrvHostOptType);
    if ((pOptions == NULL) || (pOptions->u4Status == ACTIVE))
        return SNMP_FAILURE;

    MEMCPY (pOptions->u1Value, pSetValDhcpSrvHostOptVal->pu1_OctetList,
            pSetValDhcpSrvHostOptVal->i4_Length);
    pOptions->u4Len = pSetValDhcpSrvHostOptVal->i4_Length;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostOptRowStatus
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                setValDhcpSrvHostOptRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostOptRowStatus (INT4 i4DhcpSrvHostType,
                               tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                               INT4 i4DhcpSrvSubnetPoolIndex,
                               INT4 i4DhcpSrvHostOptType,
                               INT4 i4SetValDhcpSrvHostOptRowStatus)
{

    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions = NULL;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (i4SetValDhcpSrvHostOptRowStatus != CREATE_AND_WAIT)
    {
        pOptions =
            DhcpGetOptionFromList (pEntry->pOptions, i4DhcpSrvHostOptType);
        if (pOptions == NULL)
            return SNMP_FAILURE;
    }

    switch (i4SetValDhcpSrvHostOptRowStatus)
    {
        case CREATE_AND_WAIT:

            if ((pOptions = DhcpGetFreeOptionRec ()) == NULL)
                return SNMP_FAILURE;

            pOptions->u4Type = i4DhcpSrvHostOptType;
            pOptions->u4Status = NOT_READY;

            DhcpAddOptionToList (&pEntry->pOptions, pOptions);

            break;

        case ACTIVE:

            if (DhcpValidateConfOptions ((UINT1) i4DhcpSrvHostOptType,
                                         pOptions->u4Len,
                                         pOptions->u1Value) == DHCP_FAILURE)
            {
                DHCP_TRC (MGMT, "Invalid Values for Option Value/Length\n");
                return SNMP_FAILURE;
            }
            pOptions->u4Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pOptions->u4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            DhcpRemoveOptionFromList (&pEntry->pOptions, pOptions);
            DhcpFreeOptionRec (pOptions);
            break;

    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostOptLen
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                testValDhcpSrvHostOptLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostOptLen (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvHostType,
                            tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                            INT4 i4DhcpSrvSubnetPoolIndex,
                            INT4 i4DhcpSrvHostOptType,
                            INT4 i4TestValDhcpSrvHostOptLen)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;

    if ((i4DhcpSrvHostType < DHCP_MIN_HW_TYPE) ||
        (i4DhcpSrvHostType > DHCP_MAX_HW_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                     pDhcpSrvHostId->pu1_OctetList,
                                     pDhcpSrvHostId->i4_Length,
                                     i4DhcpSrvSubnetPoolIndex)) == NULL)
        || (pOptions =
            DhcpGetOptionFromList (pEntry->pOptions,
                                   i4DhcpSrvHostOptType)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pOptions->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (i4TestValDhcpSrvHostOptLen > DHCP_MAX_OPT_LEN)
    {
        DHCP_TRC (MGMT, "Option Length Should  be less than Max Opt Length");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_OPTION_LEN);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostOptVal
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                testValDhcpSrvHostOptVal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostOptVal (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvHostType,
                            tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                            INT4 i4DhcpSrvSubnetPoolIndex,
                            INT4 i4DhcpSrvHostOptType,
                            tSNMP_OCTET_STRING_TYPE * pTestValDhcpSrvHostOptVal)
{
    tDhcpHostEntry     *pEntry;
    tDhcpOptions       *pOptions;

    if ((i4DhcpSrvHostType < DHCP_MIN_HW_TYPE) ||
        (i4DhcpSrvHostType > DHCP_MAX_HW_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                     pDhcpSrvHostId->pu1_OctetList,
                                     pDhcpSrvHostId->i4_Length,
                                     i4DhcpSrvSubnetPoolIndex)) == NULL)
        || (pOptions =
            DhcpGetOptionFromList (pEntry->pOptions,
                                   i4DhcpSrvHostOptType)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pOptions->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValDhcpSrvHostOptVal->i4_Length > DHCP_MAX_OPT_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostOptRowStatus
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType

                The Object 
                testValDhcpSrvHostOptRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostOptRowStatus (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvHostType,
                                  tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                  INT4 i4DhcpSrvSubnetPoolIndex,
                                  INT4 i4DhcpSrvHostOptType,
                                  INT4 i4TestValDhcpSrvHostOptRowStatus)
{
    INT1                Flag;

    Flag =
        nmhValidateIndexInstanceDhcpSrvHostOptTable (i4DhcpSrvHostType,
                                                     pDhcpSrvHostId,
                                                     i4DhcpSrvSubnetPoolIndex,
                                                     i4DhcpSrvHostOptType);
    if (Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDhcpSrvHostOptRowStatus == ACTIVE)
            || (i4TestValDhcpSrvHostOptRowStatus == NOT_IN_SERVICE)
            || (i4TestValDhcpSrvHostOptRowStatus == DESTROY))
            return SNMP_SUCCESS;
    }
    else
    {
        if (i4TestValDhcpSrvHostOptRowStatus == CREATE_AND_WAIT)
            return SNMP_SUCCESS;

    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_DHCPS_INV_ENTRY);
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvHostOptTable
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                DhcpSrvHostOptType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvHostOptTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvHostConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvHostConfigTable
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvHostConfigTable (INT4 i4DhcpSrvHostType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pDhcpSrvHostId,
                                                INT4 i4DhcpSrvSubnetPoolIndex)
/* GET_EXACT Validate Index Instance Routine. */
{

    if ((DhcpGetHostEntry (i4DhcpSrvHostType,
                           pDhcpSrvHostId->pu1_OctetList,
                           pDhcpSrvHostId->i4_Length,
                           i4DhcpSrvSubnetPoolIndex)) != NULL)
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvHostConfigTable
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvHostConfigTable (INT4 *pi4DhcpSrvHostType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pDhcpSrvHostId,
                                        INT4 *pi4DhcpSrvSubnetPoolIndex)
{
    tDhcpHostEntry     *pHost, *pFirstHost;

    if (gpDhcpHostEntry)
        pFirstHost = gpDhcpHostEntry;
    else
        return SNMP_FAILURE;

    for (pHost = gpDhcpHostEntry; pHost != NULL; pHost = pHost->pNext)
    {
        if (DhcpHostCompare (pHost, pFirstHost) < 0)
            pFirstHost = pHost;
    }

    *pi4DhcpSrvHostType = pFirstHost->u1HwType;
    MEMCPY (pDhcpSrvHostId->pu1_OctetList, pFirstHost->aClientMac,
            MEM_MAX_BYTES (pFirstHost->u1HostIdLen, DHCP_MAX_CID_LEN));
    pDhcpSrvHostId->i4_Length = pFirstHost->u1HostIdLen;
    *pi4DhcpSrvSubnetPoolIndex = pFirstHost->u4PoolId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvHostConfigTable
 Input       :  The Indices
                DhcpSrvHostType
                nextDhcpSrvHostType
                DhcpSrvHostId
                nextDhcpSrvHostId
                DhcpSrvSubnetPoolIndex
                nextDhcpSrvSubnetPoolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpSrvHostConfigTable (INT4 i4DhcpSrvHostType,
                                       INT4 *pi4NextDhcpSrvHostType,
                                       tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextDhcpSrvHostId,
                                       INT4 i4DhcpSrvSubnetPoolIndex,
                                       INT4 *pi4NextDhcpSrvSubnetPoolIndex)
/* GET_NEXT Routine.  */
{
    tDhcpHostEntry     *pHost, *pScanHost, *pNextHost;
    INT1                Flag = DHCP_NOT_FOUND;

    if (i4DhcpSrvHostType < 0)
    {
        return SNMP_FAILURE;
    }

    pHost = DhcpGetHostEntry (i4DhcpSrvHostType, pDhcpSrvHostId->pu1_OctetList,
                              pDhcpSrvHostId->i4_Length,
                              i4DhcpSrvSubnetPoolIndex);
    if (pHost == NULL)
        return SNMP_FAILURE;
    pNextHost = pHost;

    for (pScanHost = gpDhcpHostEntry; pScanHost != NULL;
         pScanHost = pScanHost->pNext)
    {
        if ((DhcpHostCompare (pScanHost, pHost) > 0)
            && ((Flag == DHCP_NOT_FOUND) ||
                (DhcpHostCompare (pScanHost, pNextHost) < 0)))
        {
            pNextHost = pScanHost;
            Flag = DHCP_FOUND;
        }
    }
    if (pNextHost == pHost)
        return SNMP_FAILURE;
    *pi4NextDhcpSrvHostType = pNextHost->u1HwType;
    MEMCPY (pNextDhcpSrvHostId->pu1_OctetList, pNextHost->aClientMac,
            MEM_MAX_BYTES (pNextHost->u1HostIdLen, DHCP_MAX_CID_LEN));
    pNextDhcpSrvHostId->i4_Length = pNextHost->u1HostIdLen;
    *pi4NextDhcpSrvSubnetPoolIndex = pNextHost->u4PoolId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostIpAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvHostIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostIpAddress (INT4 i4DhcpSrvHostType,
                            tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                            INT4 i4DhcpSrvSubnetPoolIndex,
                            UINT4 *pu4RetValDhcpSrvHostIpAddress)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pu4RetValDhcpSrvHostIpAddress = pEntry->u4StaticIpAddr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostPoolName
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvHostPoolName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDhcpSrvHostPoolName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDhcpSrvHostPoolName (INT4 i4DhcpSrvHostType,
                           tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                           INT4 i4DhcpSrvSubnetPoolIndex,
                           INT4 *pi4RetValDhcpSrvHostPoolName)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvHostPoolName = pEntry->u4PoolId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostConfigRowStatus
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvHostConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostConfigRowStatus (INT4 i4DhcpSrvHostType,
                                  tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                  INT4 i4DhcpSrvSubnetPoolIndex,
                                  INT4 *pi4RetValDhcpSrvHostConfigRowStatus)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvHostConfigRowStatus = pEntry->u4Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostIpAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvHostIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostIpAddress (INT4 i4DhcpSrvHostType,
                            tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                            INT4 i4DhcpSrvSubnetPoolIndex,
                            UINT4 u4SetValDhcpSrvHostIpAddress)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (pEntry->u4Status == ACTIVE)
        return SNMP_FAILURE;
    pEntry->u4StaticIpAddr = u4SetValDhcpSrvHostIpAddress;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostPoolName
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvHostPoolName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDhcpSrvHostPoolName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetDhcpSrvHostPoolName (INT4 i4DhcpSrvHostType,
                           tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                           INT4 i4DhcpSrvSubnetPoolIndex,
                           INT4 i4SetValDhcpSrvHostPoolName)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (pEntry->u4Status == ACTIVE)
        return SNMP_FAILURE;

    pEntry->u4PoolId = i4SetValDhcpSrvHostPoolName;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostIpAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvHostConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostConfigRowStatus (INT4 i4DhcpSrvHostType,
                                  tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                  INT4 i4DhcpSrvSubnetPoolIndex,
                                  INT4 i4SetValDhcpSrvHostConfigRowStatus)
{
    tDhcpHostEntry     *pEntry = NULL;

    if (i4SetValDhcpSrvHostConfigRowStatus != CREATE_AND_WAIT)
    {
        pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                   pDhcpSrvHostId->pu1_OctetList,
                                   pDhcpSrvHostId->i4_Length,
                                   i4DhcpSrvSubnetPoolIndex);

        if (pEntry == NULL)
            return SNMP_FAILURE;
    }

    switch (i4SetValDhcpSrvHostConfigRowStatus)
    {
        case CREATE_AND_WAIT:

            if ((pEntry = DhcpGetFreeHostRec ()) == NULL)
                return SNMP_FAILURE;

            pEntry->u4Status = NOT_READY;
            pEntry->u4PoolId = i4DhcpSrvSubnetPoolIndex;
            pEntry->u1HwType = (UINT1) i4DhcpSrvHostType;
            pEntry->u1HostIdLen = (UINT1) pDhcpSrvHostId->i4_Length;
            MEMCPY (pEntry->aClientMac, pDhcpSrvHostId->pu1_OctetList,
                    pDhcpSrvHostId->i4_Length);
            if (STRLEN(DHCP_DEF_BOOT_FILE) != 0)
            {
                STRNCPY (gau1DhcpBootFileName, DHCP_DEF_BOOT_FILE, STRLEN(DHCP_DEF_BOOT_FILE));
            } 
    	    pEntry->au1BootFile[STRLEN(DHCP_DEF_BOOT_FILE)] = '\0';
            DhcpAddHostEntry (pEntry);

            break;

        case ACTIVE:
            pEntry->u4Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            pEntry->u4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            DhcpRemoveHostEntry (pEntry);
            DhcpFreeHostRec (pEntry);
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostIpAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvHostIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostIpAddress (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvHostType,
                               tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                               INT4 i4DhcpSrvSubnetPoolIndex,
                               UINT4 u4TestValDhcpSrvHostIpAddress)
{
    tDhcpHostEntry     *pEntry;
    tDhcpPool          *pPool;
    tExcludeIpAddr     *pExclude;
    UINT4              u4ContextId = 0;
      
    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (DhcpIsHostIpAllocated (i4DhcpSrvHostType,
                               pDhcpSrvHostId->pu1_OctetList,
                               pDhcpSrvHostId->i4_Length,
                               i4DhcpSrvSubnetPoolIndex,
                               u4TestValDhcpSrvHostIpAddress) == DHCP_SUCCESS)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!DHCP_IS_VALID_IP (u4TestValDhcpSrvHostIpAddress))
    {
        DHCP_TRC (MGMT, "InValid IP Address");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pPool = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "Error in getting pool entry");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pExclude = pPool->pExcludeIpAddr;

    while (pExclude != NULL)
    {
        if ((u4TestValDhcpSrvHostIpAddress >= pExclude->u4StartIpAddr) &&
            (u4TestValDhcpSrvHostIpAddress <= pExclude->u4EndIpAddr))
        {
            DHCP_TRC (MGMT, "Cannot configure: Excluded ip range");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        pExclude = pExclude->pNext;
    }

    if(CfaIpIfIsOurAddressInCxt (u4ContextId,u4TestValDhcpSrvHostIpAddress) == 0)
    {
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostPoolName
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvHostPoolName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2DhcpSrvHostPoolName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2DhcpSrvHostPoolName (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvHostType,
                              tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                              INT4 i4DhcpSrvSubnetPoolIndex,
                              INT4 i4TestValDhcpSrvHostPoolName)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4DhcpSrvSubnetPoolIndex != i4TestValDhcpSrvHostPoolName)
        return SNMP_FAILURE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostConfigRowStatus
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvHostConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostConfigRowStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4DhcpSrvHostType,
                                     tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                     INT4 i4DhcpSrvSubnetPoolIndex,
                                     INT4 i4TestValDhcpSrvHostConfigRowStatus)
{
    INT1                Flag;

    if ((i4DhcpSrvHostType < DHCP_MIN_HW_TYPE) ||
        (i4DhcpSrvHostType > DHCP_MAX_HW_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pDhcpSrvHostId->i4_Length >= DHCP_MAX_CID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    Flag =
        nmhValidateIndexInstanceDhcpSrvHostConfigTable (i4DhcpSrvHostType,
                                                        pDhcpSrvHostId,
                                                        i4DhcpSrvSubnetPoolIndex);
    if (Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDhcpSrvHostConfigRowStatus == ACTIVE)
            || (i4TestValDhcpSrvHostConfigRowStatus == NOT_IN_SERVICE)
            || (i4TestValDhcpSrvHostConfigRowStatus == DESTROY))
            return SNMP_SUCCESS;
    }
    else
    {
        if (i4TestValDhcpSrvHostConfigRowStatus == CREATE_AND_WAIT)
            return SNMP_SUCCESS;

    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvHostConfigTable
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvHostConfigTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvEnable
 Input       :  The Indices

                The Object 
                retValDhcpSrvEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvEnable (INT4 *pi4RetValDhcpSrvEnable)
{
    *pi4RetValDhcpSrvEnable = gu4DhcpsEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvDebugLevel
 Input       :  The Indices

                The Object 
                retValDhcpSrvDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvDebugLevel (INT4 *pi4RetValDhcpSrvDebugLevel)
{

    *pi4RetValDhcpSrvDebugLevel = gi4DhcpDebugMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvOfferReuseTimeOut
 Input       :  The Indices

                The Object 
                retValDhcpSrvOfferReuseTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvOfferReuseTimeOut (UINT4 *pu4RetValDhcpSrvOfferReuseTimeOut)
{
    *pu4RetValDhcpSrvOfferReuseTimeOut = gu4DhcpOfferTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvIcmpEchoEnable
 Input       :  The Indices

                The Object 
                retValDhcpSrvIcmpEchoEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvIcmpEchoEnable (INT4 *pi4RetValDhcpSrvIcmpEchoEnable)
{
    *pi4RetValDhcpSrvIcmpEchoEnable = gi1DhcpsIcmpEchoEnable;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvEnable
 Input       :  The Indices

                The Object 
                setValDhcpSrvEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvEnable (INT4 i4SetValDhcpSrvEnable)
{
    INT4                i4RetVal = SNMP_FAILURE;

    /* If the value is same just return. */
    if (gu4DhcpsEnable == (UINT4) i4SetValDhcpSrvEnable)
    {
        i4RetVal = SNMP_SUCCESS;
        return (INT1) i4RetVal;
    }

    if (i4SetValDhcpSrvEnable == DHCP_ENABLED)
    {
        /* Check whether DHCP Relay is running. If so, return failure */
#ifdef DHCP_RLY_WANTED
        if (DhcpIsRlyEnabledInAnyCxt())
        {
            DHCP_TRC (FAIL_TRC, "DHCP Relay is running \n");
            return (INT1) i4RetVal;
        }
#endif
        if (DhcpServerEnable () != DHCP_SUCCESS)
        {
            DHCP_TRC (FAIL_TRC, "Enabling DHCP Server failed\r\n");
            return (INT1) i4RetVal;
        }
    }
    else
    {
        if (DhcpServerDisable () != DHCP_SUCCESS)
        {
            DHCP_TRC (FAIL_TRC, "Disabling DHCP Server failed\r\n");
            return (INT1) i4RetVal;
        }
    }
    gDhcpCounters.u4DhcpCountDiscovers = 0;
    gDhcpCounters.u4DhcpCountRequests = 0;
    gDhcpCounters.u4DhcpCountReleases = 0;
    gDhcpCounters.u4DhcpCountDeclines = 0;
    gDhcpCounters.u4DhcpCountInforms = 0;
    gDhcpCounters.u4DhcpCountInvalids = 0;
    gDhcpCounters.u4DhcpCountOffers = 0;
    gDhcpCounters.u4DhcpCountAcks = 0;
    gDhcpCounters.u4DhcpCountNacks = 0;
    gDhcpCounters.u4DhcpCountDroppedUnKnownClient = 0;
    gDhcpCounters.u4DhcpCountDroppedNotServingSubnet = 0;
    i4RetVal = SNMP_SUCCESS;
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvDebugLevel
 Input       :  The Indices

                The Object 
                setValDhcpSrvDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvDebugLevel (INT4 i4SetValDhcpSrvDebugLevel)
{
    gi4DhcpDebugMask = i4SetValDhcpSrvDebugLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvOfferReuseTimeOut
 Input       :  The Indices

                The Object 
                setValDhcpSrvOfferReuseTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvOfferReuseTimeOut (UINT4 u4SetValDhcpSrvOfferReuseTimeOut)
{
    gu4DhcpOfferTimeOut = u4SetValDhcpSrvOfferReuseTimeOut;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvIcmpEchoEnable
 Input       :  The Indices

                The Object 
                setValDhcpSrvIcmpEchoEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvIcmpEchoEnable (INT4 i4SetValDhcpSrvIcmpEchoEnable)
{
    gi1DhcpsIcmpEchoEnable = (UINT1) i4SetValDhcpSrvIcmpEchoEnable;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvEnable
 Input       :  The Indices

                The Object 
                testValDhcpSrvEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvEnable (UINT4 *pu4ErrorCode, INT4 i4TestValDhcpSrvEnable)
{
    if ((i4TestValDhcpSrvEnable == DHCP_ENABLED)
        || (i4TestValDhcpSrvEnable == DHCP_DISABLED))
        return SNMP_SUCCESS;

    DHCP_TRC (MGMT, "The Value Should be Either 1 or 2\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvDebugLevel
 Input       :  The Indices

                The Object 
                testValDhcpSrvDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvDebugLevel (UINT4 *pu4ErrorCode,
                            INT4 i4TestValDhcpSrvDebugLevel)
{
    if ((i4TestValDhcpSrvDebugLevel < (INT4) DEBUG_NONE) ||
        (i4TestValDhcpSrvDebugLevel > (INT4) ALL_TRC))
    {
        DHCP_TRC (MGMT, "Invalid trace level\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_TRC);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvOfferReuseTimeOut
 Input       :  The Indices

                The Object 
                testValDhcpSrvOfferReuseTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvOfferReuseTimeOut (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValDhcpSrvOfferReuseTimeOut)
{
    if ((u4TestValDhcpSrvOfferReuseTimeOut == 0) ||
        (u4TestValDhcpSrvOfferReuseTimeOut > DHCP_MAX_OFFER_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_TIMEOUT_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvIcmpEchoEnable
 Input       :  The Indices

                The Object 
                testValDhcpSrvIcmpEchoEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvIcmpEchoEnable (UINT4 *pu4ErrorCode,
                                INT4 i4TestValDhcpSrvIcmpEchoEnable)
{
    if (i4TestValDhcpSrvIcmpEchoEnable == 1 ||
        i4TestValDhcpSrvIcmpEchoEnable == 2)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        DHCP_TRC (MGMT, "Value Should be Either 1 or 2");
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpCountDiscovers
 Input       :  The Indices

                The Object 
                retValDhcpCountDiscovers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountDiscovers (UINT4 *pu4RetValDhcpCountDiscovers)
{
    *pu4RetValDhcpCountDiscovers = gDhcpCounters.u4DhcpCountDiscovers;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountRequests
 Input       :  The Indices

                The Object 
                retValDhcpCountRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountRequests (UINT4 *pu4RetValDhcpCountRequests)
{
    *pu4RetValDhcpCountRequests = gDhcpCounters.u4DhcpCountRequests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountReleases
 Input       :  The Indices

                The Object 
                retValDhcpCountReleases
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountReleases (UINT4 *pu4RetValDhcpCountReleases)
{
    *pu4RetValDhcpCountReleases = gDhcpCounters.u4DhcpCountReleases;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountDeclines
 Input       :  The Indices

                The Object 
                retValDhcpCountDeclines
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountDeclines (UINT4 *pu4RetValDhcpCountDeclines)
{
    *pu4RetValDhcpCountDeclines = gDhcpCounters.u4DhcpCountDeclines;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountInforms
 Input       :  The Indices

                The Object 
                retValDhcpCountInforms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountInforms (UINT4 *pu4RetValDhcpCountInforms)
{
    *pu4RetValDhcpCountInforms = gDhcpCounters.u4DhcpCountInforms;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountInvalids
 Input       :  The Indices

                The Object 
                retValDhcpCountInvalids
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountInvalids (UINT4 *pu4RetValDhcpCountInvalids)
{
    *pu4RetValDhcpCountInvalids = gDhcpCounters.u4DhcpCountInvalids;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountOffers
 Input       :  The Indices

                The Object 
                retValDhcpCountOffers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountOffers (UINT4 *pu4RetValDhcpCountOffers)
{
    *pu4RetValDhcpCountOffers = gDhcpCounters.u4DhcpCountOffers;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountAcks
 Input       :  The Indices

                The Object 
                retValDhcpCountAcks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountAcks (UINT4 *pu4RetValDhcpCountAcks)
{
    *pu4RetValDhcpCountAcks = gDhcpCounters.u4DhcpCountAcks;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountNacks
 Input       :  The Indices

                The Object 
                retValDhcpCountNacks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountNacks (UINT4 *pu4RetValDhcpCountNacks)
{
    *pu4RetValDhcpCountNacks = gDhcpCounters.u4DhcpCountNacks;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountDroppedUnknownClient
 Input       :  The Indices

                The Object 
                retValDhcpCountDroppedUnknownClient
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountDroppedUnknownClient (UINT4
                                     *pu4RetValDhcpCountDroppedUnknownClient)
{
    *pu4RetValDhcpCountDroppedUnknownClient =
        gDhcpCounters.u4DhcpCountDroppedUnKnownClient;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountDroppedNotServingSubnet
 Input       :  The Indices

                The Object 
                retValDhcpCountDroppedNotServingSubnet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountDroppedNotServingSubnet (UINT4
                                        *pu4RetValDhcpCountDroppedNotServingSubnet)
{
    *pu4RetValDhcpCountDroppedNotServingSubnet =
        gDhcpCounters.u4DhcpCountDroppedNotServingSubnet;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvSubnetPoolConfigTable. */
/****************************************************************************
 Function    :  nmhGetDhcpSrvSubnetPoolName
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvSubnetPoolName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvSubnetPoolName (INT4 i4DhcpSrvSubnetPoolIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDhcpSrvSubnetPoolName)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pRetValDhcpSrvSubnetPoolName->i4_Length = STRLEN (pEntry->au1PoolName);
    STRNCPY (pRetValDhcpSrvSubnetPoolName->pu1_OctetList, pEntry->au1PoolName, STRLEN(pEntry->au1PoolName));
    pRetValDhcpSrvSubnetPoolName->pu1_OctetList[STRLEN(pEntry->au1PoolName)] = '\0';
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDhcpSrvSubnetPoolName
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvSubnetPoolName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvSubnetPoolName (INT4 i4DhcpSrvSubnetPoolIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValDhcpSrvSubnetPoolName)
{
    tDhcpPool          *pEntry;

    if ((pEntry = DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    MEMCPY (pEntry->au1PoolName, pSetValDhcpSrvSubnetPoolName->pu1_OctetList,
            pSetValDhcpSrvSubnetPoolName->i4_Length);
    pEntry->au1PoolName[pSetValDhcpSrvSubnetPoolName->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvSubnetPoolName
 Input       :  The Indices
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvSubnetPoolName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvSubnetPoolName (UINT4 *pu4ErrorCode,
                                INT4 i4DhcpSrvSubnetPoolIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValDhcpSrvSubnetPoolName)
{
    if ((DhcpGetPoolEntry (i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((pTestValDhcpSrvSubnetPoolName->i4_Length <= 0) ||
        pTestValDhcpSrvSubnetPoolName->i4_Length > DHCP_MAX_NAME_LEN)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValDhcpSrvSubnetPoolName->pu1_OctetList,
                              pTestValDhcpSrvSubnetPoolName->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostBootFileName
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvHostBootFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostBootFileName (INT4 i4DhcpSrvHostType,
                               tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                               INT4 i4DhcpSrvSubnetPoolIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValDhcpSrvHostBootFileName)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pRetValDhcpSrvHostBootFileName->i4_Length = STRLEN (pEntry->au1BootFile);
    STRNCPY (pRetValDhcpSrvHostBootFileName->pu1_OctetList, pEntry->au1BootFile, STRLEN(pEntry->au1BootFile));
    pRetValDhcpSrvHostBootFileName->pu1_OctetList[STRLEN(pEntry->au1BootFile)] = '\0';
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDhcpSrvHostBootServerAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                retValDhcpSrvHostBootServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvHostBootServerAddress (INT4 i4DhcpSrvHostType,
                                    tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                    INT4 i4DhcpSrvSubnetPoolIndex,
                                    INT4 *pi4RetValDhcpSrvHostBootServerAddress)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValDhcpSrvHostBootServerAddress = pEntry->u4BootSrvIpAddr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostBootFileName
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvHostBootFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostBootFileName (INT4 i4DhcpSrvHostType,
                               tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                               INT4 i4DhcpSrvSubnetPoolIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValDhcpSrvHostBootFileName)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    if (pEntry->u4Status == ACTIVE)
        return SNMP_FAILURE;

    MEMCPY (pEntry->au1BootFile, pSetValDhcpSrvHostBootFileName->pu1_OctetList,
            pSetValDhcpSrvHostBootFileName->i4_Length);
    pEntry->au1BootFile[pSetValDhcpSrvHostBootFileName->i4_Length] = '\0';

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDhcpSrvHostBootServerAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                setValDhcpSrvHostBootServerAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvHostBootServerAddress (INT4 i4DhcpSrvHostType,
                                    tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                    INT4 i4DhcpSrvSubnetPoolIndex,
                                    INT4 i4SetValDhcpSrvHostBootServerAddress)
{
    tDhcpHostEntry     *pEntry;

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
        return SNMP_FAILURE;

    pEntry->u4BootSrvIpAddr = i4SetValDhcpSrvHostBootServerAddress;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostBootFileName
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvHostBootFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostBootFileName (UINT4 *pu4ErrorCode, INT4 i4DhcpSrvHostType,
                                  tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                  INT4 i4DhcpSrvSubnetPoolIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValDhcpSrvHostBootFileName)
{
    tDhcpHostEntry     *pEntry;

    if ((pTestValDhcpSrvHostBootFileName->i4_Length <= 0)
        || (pTestValDhcpSrvHostBootFileName->i4_Length > DHCP_MAX_NAME_LEN))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValDhcpSrvHostBootFileName->pu1_OctetList,
                              pTestValDhcpSrvHostBootFileName->i4_Length)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if ((pEntry = DhcpGetHostEntry (i4DhcpSrvHostType,
                                    pDhcpSrvHostId->pu1_OctetList,
                                    pDhcpSrvHostId->i4_Length,
                                    i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pEntry->u4Status == ACTIVE)
    {
        DHCP_TRC (MGMT, "Cannot set, Entry is in Active State\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvHostBootServerAddress
 Input       :  The Indices
                DhcpSrvHostType
                DhcpSrvHostId
                DhcpSrvSubnetPoolIndex

                The Object 
                testValDhcpSrvHostBootServerAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvHostBootServerAddress (UINT4 *pu4ErrorCode,
                                       INT4 i4DhcpSrvHostType,
                                       tSNMP_OCTET_STRING_TYPE * pDhcpSrvHostId,
                                       INT4 i4DhcpSrvSubnetPoolIndex,
                                       INT4
                                       i4TestValDhcpSrvHostBootServerAddress)
{

    if ((DhcpGetHostEntry (i4DhcpSrvHostType,
                           pDhcpSrvHostId->pu1_OctetList,
                           pDhcpSrvHostId->i4_Length,
                           i4DhcpSrvSubnetPoolIndex)) == NULL)
    {
        DHCP_TRC (MGMT, "No Matching Index Found\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!DHCP_IS_VALID_IP (i4TestValDhcpSrvHostBootServerAddress))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DhcpSrvBindingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDhcpSrvBindingTable
 Input       :  The Indices
                DhcpSrvBindIpAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDhcpSrvBindingTable (UINT4 u4DhcpSrvBindIpAddress)
{
    tDhcpBinding       *pBinding;

    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDhcpSrvBindingTable
 Input       :  The Indices
                DhcpSrvBindIpAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDhcpSrvBindingTable (UINT4 *pu4DhcpSrvBindIpAddress)
{
    tDhcpBinding       *pBinding;
    UINT4               FirstIndex;

    if (gpDhcpBinding)
        FirstIndex = gpDhcpBinding->u4IpAddress;
    else
        return SNMP_FAILURE;

    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pBinding->pNext)
    {
        if (pBinding->u4IpAddress < FirstIndex)
            FirstIndex = pBinding->u4IpAddress;
    }
    *pu4DhcpSrvBindIpAddress = FirstIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDhcpSrvBindingTable
 Input       :  The Indices
                DhcpSrvBindIpAddress
                nextDhcpSrvBindIpAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDhcpSrvBindingTable (UINT4 u4DhcpSrvBindIpAddress,
                                    UINT4 *pu4NextDhcpSrvBindIpAddress)
{
    tDhcpBinding       *pBinding;
    UINT4               NextIndex = u4DhcpSrvBindIpAddress;
    INT1                Flag = DHCP_NOT_FOUND;

    for (pBinding = gpDhcpBinding; pBinding != NULL; pBinding = pBinding->pNext)
    {
        if ((pBinding->u4IpAddress > u4DhcpSrvBindIpAddress)
            && ((Flag == DHCP_NOT_FOUND)
                || (pBinding->u4IpAddress < NextIndex)))
        {
            NextIndex = pBinding->u4IpAddress;
            Flag = DHCP_FOUND;
        }
    }

    if (Flag == DHCP_NOT_FOUND)
    {
        return SNMP_FAILURE;
    }
    *pu4NextDhcpSrvBindIpAddress = NextIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindHwType
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindHwType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindHwType (UINT4 u4DhcpSrvBindIpAddress,
                         INT4 *pi4RetValDhcpSrvBindHwType)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        *pi4RetValDhcpSrvBindHwType = pBinding->u1HwType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindHwAddress
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindHwAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindHwAddress (UINT4 u4DhcpSrvBindIpAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValDhcpSrvBindHwAddress)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        pRetValDhcpSrvBindHwAddress->i4_Length = pBinding->u1HostIdLen;
        MEMCPY (pRetValDhcpSrvBindHwAddress->pu1_OctetList,
                pBinding->aBindMac, MEM_MAX_BYTES (pBinding->u1HostIdLen,
                                                   DHCP_MAX_CID_LEN));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindExpireTime
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindExpireTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindExpireTime (UINT4 u4DhcpSrvBindIpAddress,
                             INT4 *pi4RetValDhcpSrvBindExpireTime)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        UINT4               u4Time;
        OsixGetSysTime (&u4Time);
        u4Time /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
        *pi4RetValDhcpSrvBindExpireTime = pBinding->u4LeaseExprTime - u4Time;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindAllocMethod
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindAllocMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindAllocMethod (UINT4 u4DhcpSrvBindIpAddress,
                              INT4 *pi4RetValDhcpSrvBindAllocMethod)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        *pi4RetValDhcpSrvBindAllocMethod = pBinding->u4AllocMethod;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindState
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindState (UINT4 u4DhcpSrvBindIpAddress,
                        INT4 *pi4RetValDhcpSrvBindState)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        *pi4RetValDhcpSrvBindState = pBinding->u1Mask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindXid
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindXid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindXid (UINT4 u4DhcpSrvBindIpAddress,
                      UINT4 *pu4RetValDhcpSrvBindXid)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        *pu4RetValDhcpSrvBindXid = pBinding->u4xid;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBindEntryStatus
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                retValDhcpSrvBindEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBindEntryStatus (UINT4 u4DhcpSrvBindIpAddress,
                              INT4 *pi4RetValDhcpSrvBindEntryStatus)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        *pi4RetValDhcpSrvBindEntryStatus = ACTIVE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDhcpSrvBindEntryStatus
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                setValDhcpSrvBindEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvBindEntryStatus (UINT4 u4DhcpSrvBindIpAddress,
                              INT4 i4SetValDhcpSrvBindEntryStatus)
{
    tDhcpBinding       *pBinding;
    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding != NULL)
    {
        if (i4SetValDhcpSrvBindEntryStatus == DESTROY)
        {
            DhcpDeleteBinding (pBinding);
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvBindEntryStatus
 Input       :  The Indices
                DhcpSrvBindIpAddress

                The Object 
                testValDhcpSrvBindEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvBindEntryStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4DhcpSrvBindIpAddress,
                                 INT4 i4TestValDhcpSrvBindEntryStatus)
{
    tDhcpBinding       *pBinding;

    if (i4TestValDhcpSrvBindEntryStatus != DESTROY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_DHCPS_INV_STATUS);
        return SNMP_FAILURE;
    }

    pBinding = DhcpGetBindingFromAddress (u4DhcpSrvBindIpAddress);
    if (pBinding == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_DHCPS_INV_BIND_ADDR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvBindingTable
 Input       :  The Indices
                DhcpSrvBindIpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvBindingTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBootServerAddress
 Input       :  The Indices

                The Object 
                retValDhcpSrvBootServerAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBootServerAddress (UINT4 *pu4RetValDhcpSrvBootServerAddress)
{
    *pu4RetValDhcpSrvBootServerAddress = gDhcpBootServerAddress;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvDefBootFilename
 Input       :  The Indices

                The Object 
                retValDhcpSrvDefBootFilename
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvDefBootFilename (tSNMP_OCTET_STRING_TYPE *
                              pRetValDhcpSrvDefBootFilename)
{
    pRetValDhcpSrvDefBootFilename->i4_Length = STRLEN (gau1DhcpBootFileName);
    STRNCPY (pRetValDhcpSrvDefBootFilename->pu1_OctetList, gau1DhcpBootFileName, STRLEN(gau1DhcpBootFileName));
    pRetValDhcpSrvDefBootFilename->pu1_OctetList[STRLEN(gau1DhcpBootFileName)] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvBootpClientsSupported
 Input       :  The Indices

                The Object 
                retValDhcpSrvBootpClientsSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvBootpClientsSupported (INT4 *pi4RetValDhcpSrvBootpClientsSupported)
{
    *pi4RetValDhcpSrvBootpClientsSupported = gu1DhcpBootpClientSupported;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpSrvAutomaticBootpEnabled
 Input       :  The Indices

                The Object 
                retValDhcpSrvAutomaticBootpEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpSrvAutomaticBootpEnabled (INT4 *pi4RetValDhcpSrvAutomaticBootpEnabled)
{
    *pi4RetValDhcpSrvAutomaticBootpEnabled = gu1DhcpAutomaticBootpEnabled;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDhcpSrvBootServerAddress
 Input       :  The Indices

                The Object 
                setValDhcpSrvBootServerAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvBootServerAddress (UINT4 u4SetValDhcpSrvBootServerAddress)
{
    if ((DHCP_IS_ZERO_IP (u4SetValDhcpSrvBootServerAddress)) ||
        (DHCP_IS_VALID_IP (u4SetValDhcpSrvBootServerAddress)))
    {
        gDhcpBootServerAddress = u4SetValDhcpSrvBootServerAddress;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvDefBootFilename
 Input       :  The Indices

                The Object 
                setValDhcpSrvDefBootFilename
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvDefBootFilename (tSNMP_OCTET_STRING_TYPE *
                              pSetValDhcpSrvDefBootFilename)
{

    MEMCPY (gau1DhcpBootFileName, pSetValDhcpSrvDefBootFilename->pu1_OctetList,
            pSetValDhcpSrvDefBootFilename->i4_Length);
    gau1DhcpBootFileName[pSetValDhcpSrvDefBootFilename->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvBootpClientsSupported
 Input       :  The Indices

                The Object 
                setValDhcpSrvBootpClientsSupported
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvBootpClientsSupported (INT4 i4SetValDhcpSrvBootpClientsSupported)
{
    gu1DhcpBootpClientSupported = (UINT1) i4SetValDhcpSrvBootpClientsSupported;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpSrvAutomaticBootpEnabled
 Input       :  The Indices

                The Object 
                setValDhcpSrvAutomaticBootpEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpSrvAutomaticBootpEnabled (INT4 i4SetValDhcpSrvAutomaticBootpEnabled)
{
    gu1DhcpAutomaticBootpEnabled = (UINT1) i4SetValDhcpSrvAutomaticBootpEnabled;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2DhcpSrvBootServerAddress
 Input       :  The Indices

                The Object 
                testValDhcpSrvBootServerAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvBootServerAddress (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValDhcpSrvBootServerAddress)
{
    if ((DHCP_IS_ZERO_IP (u4TestValDhcpSrvBootServerAddress)) ||
        (DHCP_IS_VALID_IP (u4TestValDhcpSrvBootServerAddress)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_DHCPS_INV_BOOTSRV_ADDR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvDefBootFilename
 Input       :  The Indices

                The Object 
                testValDhcpSrvDefBootFilename
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvDefBootFilename (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValDhcpSrvDefBootFilename)
{
    if (pTestValDhcpSrvDefBootFilename->i4_Length <= 0
        || pTestValDhcpSrvDefBootFilename->i4_Length > DHCP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvBootpClientsSupported
 Input       :  The Indices

                The Object 
                testValDhcpSrvBootpClientsSupported
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvBootpClientsSupported (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValDhcpSrvBootpClientsSupported)
{
    if (i4TestValDhcpSrvBootpClientsSupported == 1
        || i4TestValDhcpSrvBootpClientsSupported == 2)
        return SNMP_SUCCESS;
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2DhcpSrvAutomaticBootpEnabled
 Input       :  The Indices

                The Object 
                testValDhcpSrvAutomaticBootpEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpSrvAutomaticBootpEnabled (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValDhcpSrvAutomaticBootpEnabled)
{
    if (i4TestValDhcpSrvAutomaticBootpEnabled == 1
        || i4TestValDhcpSrvAutomaticBootpEnabled == 2)
        return SNMP_SUCCESS;
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvDebugLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvOfferReuseTimeOut
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvOfferReuseTimeOut (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvIcmpEchoEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvIcmpEchoEnable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvBootServerAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvBootServerAddress (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvDefBootFilename
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvDefBootFilename (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvBootpClientsSupported
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvBootpClientsSupported (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2DhcpSrvAutomaticBootpEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpSrvAutomaticBootpEnabled (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDhcpCountResetCounters
 Input       :  The Indices

                The Object 
                retValDhcpCountResetCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDhcpCountResetCounters (INT4 *pi4RetValDhcpCountResetCounters)
{

    *pi4RetValDhcpCountResetCounters =
        (INT4) gDhcpCounters.u4DhcpCountResetCounters;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDhcpCountResetCounters
 Input       :  The Indices

                The Object 
                setValDhcpCountResetCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDhcpCountResetCounters (INT4 i4SetValDhcpCountResetCounters)
{

    if (i4SetValDhcpCountResetCounters == DHCP_RESET)
    {
        gDhcpCounters.u4DhcpCountDiscovers = 0;
        gDhcpCounters.u4DhcpCountRequests = 0;
        gDhcpCounters.u4DhcpCountReleases = 0;
        gDhcpCounters.u4DhcpCountDeclines = 0;
        gDhcpCounters.u4DhcpCountInforms = 0;
        gDhcpCounters.u4DhcpCountInvalids = 0;
        gDhcpCounters.u4DhcpCountOffers = 0;
        gDhcpCounters.u4DhcpCountAcks = 0;
        gDhcpCounters.u4DhcpCountNacks = 0;
        gDhcpCounters.u4DhcpCountDroppedUnKnownClient = 0;
        gDhcpCounters.u4DhcpCountDroppedNotServingSubnet = 0;
    }
    gDhcpCounters.u4DhcpCountResetCounters =
        (UINT4) i4SetValDhcpCountResetCounters;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DhcpCountResetCounters
 Input       :  The Indices

                The Object 
                testValDhcpCountResetCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DhcpCountResetCounters (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValDhcpCountResetCounters)
{

    if ((i4TestValDhcpCountResetCounters == DHCP_RESET) ||
        (i4TestValDhcpCountResetCounters == DHCP_NO_RESET))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhDepv2DhcpCountResetCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DhcpCountResetCounters (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DhcpValidateConfOptions                           */
/*  Description     : This function validate the options depending      */
/*                  : upon the length and type as specified in RFC 2132.*/
/*  Input(s)        : u1Type - type of the option.                      */
/*                    u1Len - Length of the option.                     */
/*                    pValue- option value                              */
/*  Returns         : result of comparison                              */
/************************************************************************/

INT4
DhcpValidateConfOptions (UINT1 u1Type, UINT4 u4Len, UINT1 *pValue)
{

    if (u4Len > DHCP_MAX_OPT_LEN)
    {
        return DHCP_FAILURE;
    }

    /* *INDENT-OFF* */
    
    switch (u1Type)
    {
            /* Options With Specific Values */
        case 19: case 20: case 27: case 29: case 30:
        case 31: case 34: case 36: case 39:

            if (( pValue[0] == 0) || ( pValue[0] == 1)) 
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* Options With Minimum Length 1 */
        case 12: case 14: case 15: case 17: case 18:
        case 40: case 43: case 47: case 64: case 66:
        case 67:
            if (u4Len < 1)
                return DHCP_FAILURE;
            else
                return DHCP_SUCCESS;

            /* Options With Minimum Length 4 */
        case 16:

            if (u4Len < 4)
                return DHCP_FAILURE;
            else
                return DHCP_SUCCESS;

            /* Options With Length 2 and (2*n) */

        case 25:

            if ((u4Len != 0) && (u4Len % 2 == 0))
            {
                return DHCP_SUCCESS;
            }

            return DHCP_FAILURE;

            /* Options With Minimum Length 0 and (4*n) */

        case 68:

            if (u4Len % 4 == 0)
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* With Minimum Length 4 and (4*n) */

        case 1: case 3: case 4: case 5: case 6: case 7: case 8:
        case 9: case 10: case 11: case 41: case 42: case 44:
        case 45: case 48: case 49: case 65: case 69: case 70:
        case 71: case 72: case 73: case 74: case 75: case 76:

            if ((u4Len != 0) && (u4Len % 4 == 0))
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* With Minimum Length 8 and (8*n) */

        case 21: case 33:

            if ((u4Len != 0) && (u4Len % 8 == 0))
            {
                return DHCP_SUCCESS;
            }
            return DHCP_FAILURE;

            /* Should Not Allow Configuration */
        case 0: case 255: case 50: case 51: case 52:
        case 53: case 54: case 55: case 56: case 57:
        case 58: case 59: case 60:

            return DHCP_FAILURE;

        default:
            return DHCP_SUCCESS;

    }
    /* *INDENT-ON* */
}

/************************************************************************/
/*  Function Name   : DhcpHostCompare                                   */
/*  Description     : This function returns an integer less  than,      */
/*                  : equal to, or greater than zero,if pHost1 is       */
/*                  : Lexicographically less than ,eqaul to or greater  */
/*                  : than pHost2                                       */
/*  Input(s)        : pHost1 and pHost2 - Host entries to compare       */
/*  Returns         : result of comparison                              */
/************************************************************************/
INT4
DhcpHostCompare (tDhcpHostEntry * pHost1, tDhcpHostEntry * pHost2)
{
    INT4                Temp;

    /* compare hardware type */
    if (pHost1->u1HwType != pHost2->u1HwType)
        return (pHost1->u1HwType - pHost2->u1HwType);

    /* compare host id */
    Temp = DHCP_MIN (pHost1->u1HostIdLen, pHost2->u1HostIdLen);
    Temp =
        MEMCMP (pHost1->aClientMac, pHost2->aClientMac,
                MEM_MAX_BYTES (Temp, DHCP_MAX_CID_LEN));
    if (Temp != 0)
        return Temp;

    if (pHost1->u1HostIdLen != pHost2->u1HostIdLen)
        return (pHost1->u1HostIdLen - pHost2->u1HostIdLen);

    /* compare pool id */
    if (pHost1->u4PoolId != pHost2->u4PoolId)
        return (pHost1->u4PoolId - pHost2->u4PoolId);

    /* lexicographically equal */
    return 0;
}

/************************************************************************/
/*  Function Name   : DhcpHostOptCompare                                */
/*  Description     : This function returns an integer less  than,      */
/*                  : equal to, or greater than zero,if pHost1 is       */
/*                  : Lexicographically less than ,eqaul to or greater  */
/*                  : than pHost2                                       */
/*  Input(s)        : pHost1 and pHost2 - Host entries to compare       */
/*  Returns         : result of comparison                              */
/************************************************************************/
INT4
DhcpHostOptCompare (tDhcpHostEntry * pHost1, UINT4 Opt1,
                    tDhcpHostEntry * pHost2, UINT4 Opt2)
{
    INT4                Temp;

    /* compare hosts */
    Temp = DhcpHostCompare (pHost1, pHost2);
    if (Temp != 0)
        return Temp;

    /* compare option value */
    if (Opt1 != Opt2)
        return (Opt1 - Opt2);

    /* lexicographically equal */
    return 0;
}

/************************************************************************/
/*  Function Name   : DhcpSetDefNetmask                                 */
/*  Description     : This function set the default netmask and subnet  */
/*                    value for the pool using the ip address of the    */
/*                    pool interface.                                   */
/*  Input(s)        : pPoolEntry - Subnet pool entry.                   */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
DhcpSetDefNetmask (tDhcpPool * pPoolEntry)
{
    UINT4               u4IpAddr, u4NetMask, u4Subnet;

    /* select the network of the start ip-address in the pool as
     * the network for this subnet pool.
     * */

    u4IpAddr = pPoolEntry->u4StartIpAddr;

    if (u4IpAddr == 0)
        return;

    u4NetMask = DhcpGetDefaultNetMask (u4IpAddr);

    pPoolEntry->u4NetMask = u4NetMask;

    if (pPoolEntry->u4Subnet == 0)
    {
        u4Subnet = u4IpAddr & u4NetMask;
        pPoolEntry->u4Subnet = u4Subnet;
    }
    return;

}

/************************************************************************/
/*  Function Name   : DhcpGetDefaultNetMask                             */
/*  Description     : This function get the default netmask for the     */
/*                    given ip address.                                 */
/*  Input(s)        : u4IpAddress - ip address .                        */
/*  Output(s)       : None.                                             */
/*  Returns         : default netmask for the given ip address.         */
/************************************************************************/
UINT4
DhcpGetDefaultNetMask (UINT4 u4IpAddress)
{
    UINT4               u4NetMask;

    if (IP_IS_ADDR_CLASS_A (u4IpAddress))
    {
        u4NetMask = DHCP_NETMASK_CLASS_A;
    }
    else if (IP_IS_ADDR_CLASS_B (u4IpAddress))
    {
        u4NetMask = DHCP_NETMASK_CLASS_B;
    }
    else if (IP_IS_ADDR_CLASS_C (u4IpAddress))
    {
        u4NetMask = DHCP_NETMASK_CLASS_C;
    }
    else
    {
        u4NetMask = DHCP_NETMASK_DEFAULT;
    }

    return u4NetMask;

}
