/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: dhcpsredstb.c,v 1.2 2011/12/06 10:37:50 siva Exp $
 *
 * Description: This file contains DHCP Server Redundancy related
 *              stub routines which will get invoked if L2RED_WANTED
 *              is undefined
 *
 *******************************************************************/
#ifndef __DHCPSREDSTB_C
#define __DHCPSREDSTB_C

#include "dhcpscom.h"

/************************************************************************/
/*  Function Name   : DhcpSRedInitGlobalInfo                            */
/*                                                                      */
/*  Description     : This function is invoked by the DHCP module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

INT4
DhcpSRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedDeInitGlobalInfo                        */
/*                                                                      */
/* Description        : This function is invoked by the DHCP module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register DHCP server with RM.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
DhcpsRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : DhcpSRedHandleRmEvents                          */
/*                                                                      */
/* Description        : This function is invoked by the DHCP module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the DHCP Server Q Msg        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
DhcpSRedHandleRmEvents (tDhcpSQMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : DhcpSRedSendDynamicBindingInfo                  */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the DHCP server offers an IP address */
/*                      to the DHCP client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pDhcpBindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
DhcpSRedSendDynamicBindingInfo (tDhcpBinding * pDhcpBindingInfo)
{
    UNUSED_PARAM (pDhcpBindingInfo);
    return;
}
#endif
