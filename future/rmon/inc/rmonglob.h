/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmonglob.h,v 1.32 2017/01/17 14:10:29 siva Exp $            *
 *                                                                  *
 * Description: Contains global definitions.                        *
 *                                                                  *
 *******************************************************************/
#ifndef RMONGLOB_H
#define RMONGLOB_H

#ifdef RMONMAIN_C
/* Definition of RMON Supported Groups */
tRmonSupportGroups gRmonSupportedGroups;

/* Definition of RMON group tables. */
   tHostTopnCtrlTable      gaHostTopNControlTable[RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT];
   tMatrixControlTable     gaMatrixControlTable[RMON_MAX_INTERFACE_LIMIT];
   tHostControlTable       gaHostControlTable[RMON_MAX_INTERFACE_LIMIT]; 

/*
 * TRUE_MAX_INTERFACE is actually RMON_MAX_INTERFACE_LIMIT-1 
 * since we are not using the 0'th interface even though we
 * have allocated memory for that 
 * so we can use values 1 to RMON_MAX_INTERFACE_LIMIT-1 only
 */

BOOLEAN  grmprB1AnyValidAlarm;
BOOLEAN  grmprB1AnyValidHistory;
BOOLEAN  grmprB1AnyValidTopN;


/* New globals for proprietary MIB */
UINT4   gRmonEnableStatusFlag;
UINT4   gRmonSystemControlFlag;

UINT4   gu4RmonEtherStatsBatchId;

tSNMP_OID_TYPE          grlInterfaceOid;
UINT4                   gau4rlInterfaceOidList [IFINDEX_OID_SIZE];
tSNMP_OID_TYPE          grlVlanOid;
UINT4                   gau4rlVlanOidList[VLAN_INDEX_OID_SIZE];
tTimerListId            gTimerListId1;
tTimerListId            gTimerListId2;

UINT4                   gu4RmonTaskId;
tMemPoolId      gRmonAlarmEntryId;
tMemPoolId      gRmonEventPoolId;
tMemPoolId      gRmonAlarmPoolId;
tMemPoolId      gRmonHistoryCtrlPoolId;
tMemPoolId      gRmonEtherStatsPoolId;
tMemPoolId      gRmonHostTopNCtrlPoolId;
tMemPoolId      gRmonMatrixCtrlPoolId;
tMemPoolId      gRmonHostCtrlPoolId;
tMemPoolId      gRmonLogEventPoolId;
tMemPoolId      gRmonEventCommPoolId;
tMemPoolId      gRmonLogBucketPoolId;
tMemPoolId      gRmonAlarmVarPoolId;
tMemPoolId      gRmonEtherHisPoolId;
tMemPoolId      gRmonHisBucketsPoolId;
tMemPoolId      gRmonHostTablePoolId;
tMemPoolId      gRmonMatrixBucketsPoolId;
tMemPoolId      gRmonTopNReportPoolId;
tMemPoolId      gRmonVlanPoolId;

tRmonHashTable *gpRmonEtherStatsHashTable;
tRmonSLL       gEtherStatsSLL;
tRmonHashTable *gpRmonHistoryControlHashTable;
tRmonHashTable *gpRmonAlarmHashTable;
tRmonHashTable *gpRmonEventHashTable;
#ifdef SNMP_2_WANTED
tSnmpIndex RmonIndexPool1[RMON_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE RmonMultiPool1[RMON_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE RmonOctetPool1[RMON_MAX_INDICES];
tSNMP_OID_TYPE RmonOIDPool1[RMON_MAX_INDICES];
UINT1 au1RmonData1[RMON_MAX_INDICES][1024];
#endif /* SNMP_2_WANTED */
tOsixSemId    gRmonSemId = 0;
tOsixQId      gRmonQId;

#else /*RMONMAIN_C*/

extern tRmonSupportGroups      gRmonSupportedGroups;
extern tHostTopnCtrlTable      gaHostTopNControlTable[RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT];
extern tMatrixControlTable     gaMatrixControlTable[RMON_MAX_INTERFACE_LIMIT];
extern tHostControlTable       gaHostControlTable[RMON_MAX_INTERFACE_LIMIT]; 

extern BOOLEAN                 grmprB1AnyValidAlarm;
extern BOOLEAN                 grmprB1AnyValidHistory;
extern BOOLEAN                 grmprB1AnyValidTopN;

extern tTimerListId            gTimerListId1;
extern tTimerListId            gTimerListId2;

extern tSNMP_OID_TYPE          grlInterfaceOid;
extern tSNMP_OID_TYPE          grlVlanOid;
extern UINT4                   gu4RmonTaskId;
extern tOsixSemId              gRmonSemId;
extern tOsixQId                gRmonQId;
extern tMemPoolId              gRmonAlarmEntryId;
extern tMemPoolId              gRmonEventPoolId;
extern tMemPoolId              gRmonAlarmPoolId;
extern tMemPoolId              gRmonHistoryCtrlPoolId;
extern tMemPoolId              gRmonEtherStatsPoolId;
extern tMemPoolId              gRmonHostTopNCtrlPoolId;
extern tMemPoolId              gRmonMatrixCtrlPoolId;
extern tMemPoolId              gRmonHostCtrlPoolId;
extern tMemPoolId              gRmonLogEventPoolId;
extern tMemPoolId              gRmonEventCommPoolId;
extern tMemPoolId              gRmonLogBucketPoolId;
extern tMemPoolId              gRmonAlarmVarPoolId;
extern tMemPoolId              gRmonEtherHisPoolId;
extern tMemPoolId              gRmonHisBucketsPoolId;
extern tMemPoolId              gRmonHostTablePoolId;
extern tMemPoolId              gRmonMatrixBucketsPoolId;
extern tMemPoolId              gRmonTopNReportPoolId;
extern tMemPoolId              gRmonVlanPoolId;

/* New globals for proprietary MIB */
extern UINT4 gRmonEnableStatusFlag;
extern UINT4 gRmonSystemControlFlag;

extern UINT4 gu4RmonEtherStatsBatchId;

extern tRmonHashTable *gpRmonEtherStatsHashTable;
extern tRmonSLL gEtherStatsSLL;
extern tRmonHashTable *gpRmonHistoryControlHashTable;
extern tRmonHashTable *gpRmonAlarmHashTable;
extern tRmonHashTable *gpRmonEventHashTable;
extern VOID WebnmConvertOidToString (tSNMP_OID_TYPE * pOid,
                                     UINT1 *pu1String);

#ifdef SNMP_2_WANTED
extern tSnmpIndex RmonIndexPool1[RMON_MAX_INDICES];
extern tSNMP_MULTI_DATA_TYPE RmonMultiPool1[RMON_MAX_INDICES];
extern tSNMP_OCTET_STRING_TYPE RmonOctetPool1[RMON_MAX_INDICES];
extern tSNMP_OID_TYPE RmonOIDPool1[RMON_MAX_INDICES];
extern UINT1 au1RmonData1[RMON_MAX_INDICES][1024];
#endif /*SNMP_2_WANTED*/
#endif /*RMONMAIN_C*/

#define   MATRIX_CONTROL_TBL(Index)  \
                         gaMatrixControlTable[Index].pMatrixControlEntry

#define TOPN_TABLE(index)   gaHostTopNControlTable[index].pTopNControlEntry
#define HOST_CONTROL(index) gaHostControlTable[index].pHostControlEntry
#define HOST_CONTROL_STATUS(index)      (gaHostControlTable[index].\
                                        pHostControlEntry->hostControlStatus)

#define VALID_HOST_CONTROL(index)       (HOST_CONTROL_STATUS(index)\
                                        == RMON_VALID)
#define MATRIX_CTRL_TABLE(Index)  (gaMatrixControlTable[Index].\
                    pMatrixControlEntry)
#define NOT_INVALID_MATRIX(Index)  \
             ((gaMatrixControlTable[Index].pMatrixControlEntry != NULL) && \
             (gaMatrixControlTable[Index].pMatrixControlEntry->matrixControlStatus != RMON_INVALID))
#endif   /*  !RMONGLOB_H  */

