
# ifndef stdrmOCON_H
# define stdrmOCON_H
/*
 *  The Constant Declarations for
 *  etherStatsTable
 */

# define ETHERSTATSINDEX                                   (1)
# define ETHERSTATSDATASOURCE                              (2)
# define ETHERSTATSDROPEVENTS                              (3)
# define ETHERSTATSOCTETS                                  (4)
# define ETHERSTATSPKTS                                    (5)
# define ETHERSTATSBROADCASTPKTS                           (6)
# define ETHERSTATSMULTICASTPKTS                           (7)
# define ETHERSTATSCRCALIGNERRORS                          (8)
# define ETHERSTATSUNDERSIZEPKTS                           (9)
# define ETHERSTATSOVERSIZEPKTS                            (10)
# define ETHERSTATSFRAGMENTS                               (11)
# define ETHERSTATSJABBERS                                 (12)
# define ETHERSTATSCOLLISIONS                              (13)
# define ETHERSTATSPKTS64OCTETS                            (14)
# define ETHERSTATSPKTS65TO127OCTETS                       (15)
# define ETHERSTATSPKTS128TO255OCTETS                      (16)
# define ETHERSTATSPKTS256TO511OCTETS                      (17)
# define ETHERSTATSPKTS512TO1023OCTETS                     (18)
# define ETHERSTATSPKTS1024TO1518OCTETS                    (19)
# define ETHERSTATSOWNER                                   (20)
# define ETHERSTATSSTATUS                                  (21)

/*
 *  The Constant Declarations for
 *  historyControlTable
 */

# define HISTORYCONTROLINDEX                               (1)
# define HISTORYCONTROLDATASOURCE                          (2)
# define HISTORYCONTROLBUCKETSREQUESTED                    (3)
# define HISTORYCONTROLBUCKETSGRANTED                      (4)
# define HISTORYCONTROLINTERVAL                            (5)
# define HISTORYCONTROLOWNER                               (6)
# define HISTORYCONTROLSTATUS                              (7)

/*
 *  The Constant Declarations for
 *  etherHistoryTable
 */

# define ETHERHISTORYINDEX                                 (1)
# define ETHERHISTORYSAMPLEINDEX                           (2)
# define ETHERHISTORYINTERVALSTART                         (3)
# define ETHERHISTORYDROPEVENTS                            (4)
# define ETHERHISTORYOCTETS                                (5)
# define ETHERHISTORYPKTS                                  (6)
# define ETHERHISTORYBROADCASTPKTS                         (7)
# define ETHERHISTORYMULTICASTPKTS                         (8)
# define ETHERHISTORYCRCALIGNERRORS                        (9)
# define ETHERHISTORYUNDERSIZEPKTS                         (10)
# define ETHERHISTORYOVERSIZEPKTS                          (11)
# define ETHERHISTORYFRAGMENTS                             (12)
# define ETHERHISTORYJABBERS                               (13)
# define ETHERHISTORYCOLLISIONS                            (14)
# define ETHERHISTORYUTILIZATION                           (15)

/*
 *  The Constant Declarations for
 *  alarmTable
 */

# define ALARMINDEX                                        (1)
# define ALARMINTERVAL                                     (2)
# define ALARMVARIABLE                                     (3)
# define ALARMSAMPLETYPE                                   (4)
# define ALARMVALUE                                        (5)
# define ALARMSTARTUPALARM                                 (6)
# define ALARMRISINGTHRESHOLD                              (7)
# define ALARMFALLINGTHRESHOLD                             (8)
# define ALARMRISINGEVENTINDEX                             (9)
# define ALARMFALLINGEVENTINDEX                            (10)
# define ALARMOWNER                                        (11)
# define ALARMSTATUS                                       (12)

/*
 *  The Constant Declarations for
 *  hostControlTable
 */

# define HOSTCONTROLINDEX                                  (1)
# define HOSTCONTROLDATASOURCE                             (2)
# define HOSTCONTROLTABLESIZE                              (3)
# define HOSTCONTROLLASTDELETETIME                         (4)
# define HOSTCONTROLOWNER                                  (5)
# define HOSTCONTROLSTATUS                                 (6)

/*
 *  The Constant Declarations for
 *  hostTable
 */

# define HOSTADDRESS                                       (1)
# define HOSTCREATIONORDER                                 (2)
# define HOSTINDEX                                         (3)
# define HOSTINPKTS                                        (4)
# define HOSTOUTPKTS                                       (5)
# define HOSTINOCTETS                                      (6)
# define HOSTOUTOCTETS                                     (7)
# define HOSTOUTERRORS                                     (8)
# define HOSTOUTBROADCASTPKTS                              (9)
# define HOSTOUTMULTICASTPKTS                              (10)

/*
 *  The Constant Declarations for
 *  hostTimeTable
 */

# define HOSTTIMEADDRESS                                   (1)
# define HOSTTIMECREATIONORDER                             (2)
# define HOSTTIMEINDEX                                     (3)
# define HOSTTIMEINPKTS                                    (4)
# define HOSTTIMEOUTPKTS                                   (5)
# define HOSTTIMEINOCTETS                                  (6)
# define HOSTTIMEOUTOCTETS                                 (7)
# define HOSTTIMEOUTERRORS                                 (8)
# define HOSTTIMEOUTBROADCASTPKTS                          (9)
# define HOSTTIMEOUTMULTICASTPKTS                          (10)

/*
 *  The Constant Declarations for
 *  hostTopNControlTable
 */

# define HOSTTOPNCONTROLINDEX                              (1)
# define HOSTTOPNHOSTINDEX                                 (2)
# define HOSTTOPNRATEBASE                                  (3)
# define HOSTTOPNTIMEREMAINING                             (4)
# define HOSTTOPNDURATION                                  (5)
# define HOSTTOPNREQUESTEDSIZE                             (6)
# define HOSTTOPNGRANTEDSIZE                               (7)
# define HOSTTOPNSTARTTIME                                 (8)
# define HOSTTOPNOWNER                                     (9)
# define HOSTTOPNSTATUS                                    (10)

/*
 *  The Constant Declarations for
 *  hostTopNTable
 */

# define HOSTTOPNREPORT                                    (1)
# define HOSTTOPNINDEX                                     (2)
# define HOSTTOPNADDRESS                                   (3)
# define HOSTTOPNRATE                                      (4)

/*
 *  The Constant Declarations for
 *  matrixControlTable
 */

# define MATRIXCONTROLINDEX                                (1)
# define MATRIXCONTROLDATASOURCE                           (2)
# define MATRIXCONTROLTABLESIZE                            (3)
# define MATRIXCONTROLLASTDELETETIME                       (4)
# define MATRIXCONTROLOWNER                                (5)
# define MATRIXCONTROLSTATUS                               (6)

/*
 *  The Constant Declarations for
 *  matrixSDTable
 */

# define MATRIXSDSOURCEADDRESS                             (1)
# define MATRIXSDDESTADDRESS                               (2)
# define MATRIXSDINDEX                                     (3)
# define MATRIXSDPKTS                                      (4)
# define MATRIXSDOCTETS                                    (5)
# define MATRIXSDERRORS                                    (6)

/*
 *  The Constant Declarations for
 *  matrixDSTable
 */

# define MATRIXDSSOURCEADDRESS                             (1)
# define MATRIXDSDESTADDRESS                               (2)
# define MATRIXDSINDEX                                     (3)
# define MATRIXDSPKTS                                      (4)
# define MATRIXDSOCTETS                                    (5)
# define MATRIXDSERRORS                                    (6)

/*
 *  The Constant Declarations for
 *  filterTable
 */

# define FILTERINDEX                                       (1)
# define FILTERCHANNELINDEX                                (2)
# define FILTERPKTDATAOFFSET                               (3)
# define FILTERPKTDATA                                     (4)
# define FILTERPKTDATAMASK                                 (5)
# define FILTERPKTDATANOTMASK                              (6)
# define FILTERPKTSTATUS                                   (7)
# define FILTERPKTSTATUSMASK                               (8)
# define FILTERPKTSTATUSNOTMASK                            (9)
# define FILTEROWNER                                       (10)
# define FILTERSTATUS                                      (11)

/*
 *  The Constant Declarations for
 *  channelTable
 */

# define CHANNELINDEX                                      (1)
# define CHANNELIFINDEX                                    (2)
# define CHANNELACCEPTTYPE                                 (3)
# define CHANNELDATACONTROL                                (4)
# define CHANNELTURNONEVENTINDEX                           (5)
# define CHANNELTURNOFFEVENTINDEX                          (6)
# define CHANNELEVENTINDEX                                 (7)
# define CHANNELEVENTSTATUS                                (8)
# define CHANNELMATCHES                                    (9)
# define CHANNELDESCRIPTION                                (10)
# define CHANNELOWNER                                      (11)
# define CHANNELSTATUS                                     (12)

/*
 *  The Constant Declarations for
 *  bufferControlTable
 */

# define BUFFERCONTROLINDEX                                (1)
# define BUFFERCONTROLCHANNELINDEX                         (2)
# define BUFFERCONTROLFULLSTATUS                           (3)
# define BUFFERCONTROLFULLACTION                           (4)
# define BUFFERCONTROLCAPTURESLICESIZE                     (5)
# define BUFFERCONTROLDOWNLOADSLICESIZE                    (6)
# define BUFFERCONTROLDOWNLOADOFFSET                       (7)
# define BUFFERCONTROLMAXOCTETSREQUESTED                   (8)
# define BUFFERCONTROLMAXOCTETSGRANTED                     (9)
# define BUFFERCONTROLCAPTUREDPACKETS                      (10)
# define BUFFERCONTROLTURNONTIME                           (11)
# define BUFFERCONTROLOWNER                                (12)
# define BUFFERCONTROLSTATUS                               (13)

/*
 *  The Constant Declarations for
 *  captureBufferTable
 */

# define CAPTUREBUFFERCONTROLINDEX                         (1)
# define CAPTUREBUFFERINDEX                                (2)
# define CAPTUREBUFFERPACKETID                             (3)
# define CAPTUREBUFFERPACKETDATA                           (4)
# define CAPTUREBUFFERPACKETLENGTH                         (5)
# define CAPTUREBUFFERPACKETTIME                           (6)
# define CAPTUREBUFFERPACKETSTATUS                         (7)

/*
 *  The Constant Declarations for
 *  eventTable
 */

# define EVENTINDEX                                        (1)
# define EVENTDESCRIPTION                                  (2)
# define EVENTTYPE                                         (3)
# define EVENTCOMMUNITY                                    (4)
# define EVENTLASTTIMESENT                                 (5)
# define EVENTOWNER                                        (6)
# define EVENTSTATUS                                       (7)

/*
 *  The Constant Declarations for
 *  logTable
 */

# define LOGEVENTINDEX                                     (1)
# define LOGINDEX                                          (2)
# define LOGTIME                                           (3)
# define LOGDESCRIPTION                                    (4)

#endif /*  stdrmonOCON_H  */
