/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdrmlow.h,v 1.6 2014/12/31 10:58:46 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for EtherStatsTable. */
INT1
nmhValidateIndexInstanceEtherStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for EtherStatsTable  */

INT1
nmhGetFirstIndexEtherStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEtherStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEtherStatsDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetEtherStatsDropEvents ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsBroadcastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsMulticastPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsCRCAlignErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsUndersizePkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsOversizePkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsFragments ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsJabbers ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsCollisions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts64Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts65to127Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts128to255Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts256to511Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts512to1023Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsPkts1024to1518Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEtherStatsStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetEtherStatsDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetEtherStatsOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEtherStatsStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2EtherStatsDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2EtherStatsOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EtherStatsStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2EtherStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for HistoryControlTable. */
INT1
nmhValidateIndexInstanceHistoryControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HistoryControlTable  */

INT1
nmhGetFirstIndexHistoryControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHistoryControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHistoryControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetHistoryControlBucketsRequested ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHistoryControlBucketsGranted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHistoryControlInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHistoryControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHistoryControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetHistoryControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetHistoryControlBucketsRequested ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHistoryControlInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHistoryControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetHistoryControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2HistoryControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2HistoryControlBucketsRequested ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HistoryControlInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HistoryControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2HistoryControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2HistoryControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for EtherHistoryTable. */
INT1
nmhValidateIndexInstanceEtherHistoryTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for EtherHistoryTable  */

INT1
nmhGetFirstIndexEtherHistoryTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEtherHistoryTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEtherHistoryIntervalStart ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryDropEvents ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryBroadcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryMulticastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryCRCAlignErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryUndersizePkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryOversizePkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryFragments ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryJabbers ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryCollisions ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryUtilization ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for AlarmTable. */
INT1
nmhValidateIndexInstanceAlarmTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for AlarmTable  */

INT1
nmhGetFirstIndexAlarmTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexAlarmTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetAlarmInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmVariable ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetAlarmSampleType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmStartupAlarm ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmRisingThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmFallingThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmRisingEventIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmFallingEventIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetAlarmOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetAlarmStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetAlarmInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmVariable ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetAlarmSampleType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmStartupAlarm ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmRisingThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmFallingThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmRisingEventIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmFallingEventIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetAlarmOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetAlarmStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2AlarmInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmVariable ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2AlarmSampleType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmStartupAlarm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmRisingThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmFallingThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmRisingEventIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmFallingEventIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2AlarmOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2AlarmStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2AlarmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for HostControlTable. */
INT1
nmhValidateIndexInstanceHostControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HostControlTable  */

INT1
nmhGetFirstIndexHostControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHostControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetHostControlTableSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostControlLastDeleteTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHostControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHostControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetHostControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetHostControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetHostControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2HostControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2HostControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2HostControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2HostControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for HostTable. */
INT1
nmhValidateIndexInstanceHostTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for HostTable  */

INT1
nmhGetFirstIndexHostTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHostCreationOrder ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetHostInPkts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetHostOutPkts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetHostInOctets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetHostOutOctets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetHostOutErrors ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetHostOutBroadcastPkts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetHostOutMulticastPkts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for HostTimeTable. */
INT1
nmhValidateIndexInstanceHostTimeTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for HostTimeTable  */

INT1
nmhGetFirstIndexHostTimeTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostTimeTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHostTimeAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHostTimeInPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetHostTimeOutPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetHostTimeInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetHostTimeOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetHostTimeOutErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetHostTimeOutBroadcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetHostTimeOutMulticastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for HostTopNControlTable. */
INT1
nmhValidateIndexInstanceHostTopNControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for HostTopNControlTable  */

INT1
nmhGetFirstIndexHostTopNControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostTopNControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHostTopNHostIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostTopNRateBase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostTopNTimeRemaining ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostTopNDuration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostTopNRequestedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostTopNGrantedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetHostTopNStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetHostTopNOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHostTopNStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetHostTopNHostIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHostTopNRateBase ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHostTopNTimeRemaining ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHostTopNRequestedSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetHostTopNOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetHostTopNStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2HostTopNHostIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HostTopNRateBase ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HostTopNTimeRemaining ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HostTopNRequestedSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2HostTopNOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2HostTopNStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2HostTopNControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for HostTopNTable. */
INT1
nmhValidateIndexInstanceHostTopNTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for HostTopNTable  */

INT1
nmhGetFirstIndexHostTopNTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostTopNTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetHostTopNAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetHostTopNRate ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for MatrixControlTable. */
INT1
nmhValidateIndexInstanceMatrixControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MatrixControlTable  */

INT1
nmhGetFirstIndexMatrixControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMatrixControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMatrixControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetMatrixControlTableSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMatrixControlLastDeleteTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMatrixControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMatrixControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMatrixControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetMatrixControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMatrixControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MatrixControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MatrixControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MatrixControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MatrixControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MatrixSDTable. */
INT1
nmhValidateIndexInstanceMatrixSDTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MatrixSDTable  */

INT1
nmhGetFirstIndexMatrixSDTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMatrixSDTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMatrixSDPkts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMatrixSDOctets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMatrixSDErrors ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for MatrixDSTable. */
INT1
nmhValidateIndexInstanceMatrixDSTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MatrixDSTable  */

INT1
nmhGetFirstIndexMatrixDSTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMatrixDSTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMatrixDSPkts ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMatrixDSOctets ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMatrixDSErrors ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for FilterTable. */
INT1
nmhValidateIndexInstanceFilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FilterTable  */

INT1
nmhGetFirstIndexFilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFilterChannelIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFilterPktDataOffset ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFilterPktData ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFilterPktDataMask ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFilterPktDataNotMask ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFilterPktStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFilterPktStatusMask ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFilterPktStatusNotMask ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFilterOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFilterChannelIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFilterPktDataOffset ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFilterPktData ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFilterPktDataMask ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFilterPktDataNotMask ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFilterPktStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFilterPktStatusMask ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFilterPktStatusNotMask ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFilterOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FilterChannelIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FilterPktDataOffset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FilterPktData ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FilterPktDataMask ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FilterPktDataNotMask ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FilterPktStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FilterPktStatusMask ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FilterPktStatusNotMask ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FilterOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for ChannelTable. */
INT1
nmhValidateIndexInstanceChannelTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for ChannelTable  */

INT1
nmhGetFirstIndexChannelTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexChannelTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetChannelIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelAcceptType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelDataControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelTurnOnEventIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelTurnOffEventIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelEventIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelEventStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetChannelMatches ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetChannelDescription ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetChannelOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetChannelStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetChannelIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelAcceptType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelDataControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelTurnOnEventIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelTurnOffEventIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelEventIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelEventStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetChannelDescription ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetChannelOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetChannelStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2ChannelIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelAcceptType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelDataControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelTurnOnEventIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelTurnOffEventIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelEventIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelEventStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2ChannelDescription ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ChannelOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2ChannelStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2ChannelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for BufferControlTable. */
INT1
nmhValidateIndexInstanceBufferControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for BufferControlTable  */

INT1
nmhGetFirstIndexBufferControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBufferControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBufferControlChannelIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlFullStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlFullAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlCaptureSliceSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlDownloadSliceSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlDownloadOffset ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlMaxOctetsRequested ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlMaxOctetsGranted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlCapturedPackets ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetBufferControlTurnOnTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetBufferControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBufferControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetBufferControlChannelIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetBufferControlFullAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetBufferControlCaptureSliceSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetBufferControlDownloadSliceSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetBufferControlDownloadOffset ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetBufferControlMaxOctetsRequested ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetBufferControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetBufferControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2BufferControlChannelIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2BufferControlFullAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2BufferControlCaptureSliceSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2BufferControlDownloadSliceSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2BufferControlDownloadOffset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2BufferControlMaxOctetsRequested ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2BufferControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2BufferControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2BufferControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for CaptureBufferTable. */
INT1
nmhValidateIndexInstanceCaptureBufferTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for CaptureBufferTable  */

INT1
nmhGetFirstIndexCaptureBufferTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCaptureBufferTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCaptureBufferPacketID ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetCaptureBufferPacketData ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetCaptureBufferPacketLength ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetCaptureBufferPacketTime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetCaptureBufferPacketStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for EventTable. */
INT1
nmhValidateIndexInstanceEventTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for EventTable  */

INT1
nmhGetFirstIndexEventTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEventTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEventDescription ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEventType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetEventCommunity ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEventLastTimeSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEventOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEventStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetEventDescription ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEventType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetEventCommunity ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEventOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEventStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2EventDescription ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EventType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2EventCommunity ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EventOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EventStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2EventTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LogTable. */
INT1
nmhValidateIndexInstanceLogTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LogTable  */

INT1
nmhGetFirstIndexLogTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLogTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLogTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetLogDescription ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for EtherStatsHighCapacityTable. */
INT1
nmhValidateIndexInstanceEtherStatsHighCapacityTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for EtherStatsHighCapacityTable  */

INT1
nmhGetFirstIndexEtherStatsHighCapacityTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEtherStatsHighCapacityTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEtherStatsHighCapacityOverflowPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowPkts64Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts64Octets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts65to127Octets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts128to255Octets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts256to511Octets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts512to1023Octets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetEtherStatsHighCapacityPkts1024to1518Octets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for EtherHistoryHighCapacityTable. */
INT1
nmhValidateIndexInstanceEtherHistoryHighCapacityTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for EtherHistoryHighCapacityTable  */

INT1
nmhGetFirstIndexEtherHistoryHighCapacityTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEtherHistoryHighCapacityTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEtherHistoryHighCapacityOverflowPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryHighCapacityPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetEtherHistoryHighCapacityOverflowOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetEtherHistoryHighCapacityOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
