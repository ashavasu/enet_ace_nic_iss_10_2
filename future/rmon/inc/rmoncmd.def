/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: rmoncmd.def,v 1.8 2014/11/05 10:59:22 siva Exp $                                                         
*                                                                    
*********************************************************************/


/*****************************************************************************
 *                   RMON  commands                                          *
 *****************************************************************************/

DEFINE GROUP: RMON_PEXCFG_GRP

  COMMAND  : show rmon [statistics [<short (1-65535)>]] [alarms] [events] [history [<short (1-65535)>] [overview]]
   ACTION  : {
                 UINT1 *pu1Statistics = NULL;
                 UINT1 *pu1History = NULL;
                 UINT4 u4CmdOptions = 0;
                 if($2 != NULL)
                 {
                     u4CmdOptions |= RMON_SHOW_STATS;
                     pu1Statistics = $3;
                 }
                 if($4 != NULL)
                 { 
                     u4CmdOptions |= RMON_SHOW_ALARMS;
                 }
                 if($5 != NULL)
                 { 
                     u4CmdOptions |= RMON_SHOW_EVENTS;
                 }
                 if(($6 != NULL) && ($8 == NULL))
                 { 
                     u4CmdOptions |= RMON_SHOW_HISTORY;
                     pu1History = $7;
                 }
                 if($8 != NULL)
                 {
                     u4CmdOptions |= RMON_SHOW_HISTORY_OVERVIEW;
                     pu1History = $7;
                 }
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_SHOW_RMON, NULL, 
                                     u4CmdOptions, pu1Statistics, pu1History);  
             } 
   SYNTAX  : show rmon [statistics [<stats-index (1-65535)>]] [alarms] [events] [history [history-index(1-65535)] [overview]] 
   HELP    : Display RMON  
  CXT_HELP  : show Displays the configuration/statistics/general information|
              rmon RMON related information|
              statistics Statistics related information|
              (1-65535) Statistics index value|
              alarms Configured alarm details|
              events Configured event details|
              history Configured history details|
              (1-65535) History index|
              overview Overview of RMON history entries|
              <CR> Display RMON
END GROUP


DEFINE GROUP: RMON_GLBCFG_GRP

   COMMAND : set rmon { enable | disable } 
   ACTION  : {
                 if($2 != NULL)
                 { 
                     cli_process_rmon_cmd(CliHandle, CLI_RMON_STATUS, NULL,
                                                                   CLI_ENABLE);
                 }
                 else if($3 != NULL)
                 { 
                     cli_process_rmon_cmd(CliHandle, CLI_RMON_STATUS, NULL,
                                                                   CLI_DISABLE);
                 }
             }
   SYNTAX  : set rmon { enable | disable } 
   PRVID   : 15 
   HELP    : Enables or disables RMON 
   CXT_HELP : set Configures the parameters|
              rmon RMON related configuration|
              enable Enables RMON| 
              disable Disables RMON|
              <CR> Enables or disables RMON

        /* Command Modified for standard CLI Compliance */  
  
   COMMAND :  rmon alarm <short (1-65535)> <string (255)> <short (1-65535)> { absolute | delta } rising-threshold <integer (0-2147483647)> [<integer (1-65535)>] falling-threshold <integer (0-2147483647)> [<integer (1-65535)>] [owner <string (127)>]
   ACTION  : {
                 UINT4 u4Value=0;
                 
                 if($5 != NULL)
                 {
                     u4Value = RMON_ALARM_ABSOLUTE;
                 }
                 else if($6 != NULL)
                 {
                     u4Value = RMON_ALARM_DELTA;
                 }

                 cli_process_rmon_cmd(CliHandle, CLI_RMON_ALARM, NULL, $2,
                                      $3, $4, u4Value, $8, $9, $11, $12, $14);
    
             }

   SYNTAX : rmon alarm <alarm-number> <mib-object-id (255)> \r\n <sample-interval-time (1-65535)> {absolute | delta } \r\n rising-threshold <value (0-2147483647)> [rising-event-number (1-65535)] \r\n falling-threshold <value (0-2147483647)> [falling-event-number (1-65535)] \r\n [owner <ownername (127)>]
   PRVID   : 15 
   HELP    : Set an alarm on a MIB object
   CXT_HELP:  rmon Configures RMON related parameters|
              alarm Alarm configuration on a MIB object|
              (1-65535) Alarm number|
              <string(255)> MIB object ID|
              (1-65535) Sample interval time|
              absolute Each MIB variable is directly tested| 
              delta The change between samples of a variable is tested|
              rising-threshold Number at which alarm is triggered|
              (0-2147483647) Rising threshold value|
              (1-65535) Rising event number|
              falling-threshold Number at which the alarm is reset|
              (0-2147483647) Falling-threshold value|
              (1-65535) Falling event number|
              owner Alarm owner configuration|
              <string(127)> Owner name|
              <CR> Set an alarm on a MIB object

   COMMAND : no rmon alarm <short (1-65535)> 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_NO_ALARM, NULL, $3); 
             }
   SYNTAX  : no rmon alarm <number (1-65535)>  
   PRVID   : 15 
   HELP    : Delete an alarm on the MIB object
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              rmon RMON related configuration|
              alarm Alarm configuration on a MIB object|
              (1-65535) Alarm number|
              <CR> Delete an alarm on the MIB object
 

   COMMAND : rmon event <short (1-65535)> [description <string (127)>] [log] [owner <string (127)>] [trap <string (127)>] 
   ACTION  : {
                 UINT4 u4EventType = RMON_EVENT_NONE;
                 UINT1 *pu1Community = NULL;
                 if($5 != NULL)
                 {
                    if($8 != NULL)
                    {
                        u4EventType = RMON_EVENT_LOG_AND_TRAP;
                        pu1Community = $9;
                    }
                    else
                    {
                        u4EventType = RMON_EVENT_LOG;
                    }
                 } else if($8 != NULL)
                 { 
                     u4EventType = RMON_EVENT_TRAP;
                     pu1Community = $9;
                 } 
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_EVENT, NULL, $2, 
                                      $4, u4EventType, pu1Community, $7);                                       
             }
   SYNTAX  : rmon event <number (1-65535)> [description <event-description (127)>] [log] [owner <ownername (127)>] [trap <community (127)>] 	
   PRVID   : 15 
   HELP    : Add an event in the RMON event table that is associated with an RMON event number
   CXT_HELP : rmon Configures RMON related parameters|
              event Event related configuration|
              (1-65535) Event number|
              description Event description configuration|
              <string(127)> Event description|
              log Log entry is generated|
              owner Event owner configuration|
              <string(127)> Event owner name|
              trap Trap is generated|
              <string(127)> SNMP community string to be passed for the trap|
              <CR> Add an event in the RMON event table that is associated with an RMON event number

   COMMAND : no rmon event <short (1-65535)> 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_NO_EVENT, NULL, $3);
             }
   SYNTAX  : no rmon event <number (1-65535)>
   PRVID   : 15 
   HELP    : Delete an event in the RMON event table
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              rmon RMON related configuration|
              event Event related configuration|
              (1-65535) Event number|
              <CR> Delete an event in the RMON event table
END GROUP


DEFINE GROUP: RMON_INTCFG_GRP

   COMMAND : rmon collection stats <short (1-65535)> [owner <string (127)>] 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_COLLECTION_STATS,
                                      NULL, $3, $5);
             }
   SYNTAX  : rmon collection stats <index (1-65535)> [owner <ownername (127)>]
   PRVID   : 15
   HELP    : Enable RMON statistic collection on the interface 
   CXT_HELP : rmon Configures RMON related parameters|
              collection Collection related configuration|
              stats Statistic collection configuration on the interface|
              (1-65535) Statistics table index|
              owner RMON group owner configuration|
              <string(127)> RMON group owner name|
              <CR> Enable RMON statistic collection on the interface

   COMMAND : no rmon collection stats <short (1-65535)> 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_NO_COLLECTION_STATS,
                                      NULL, $4);
             }
   SYNTAX  : no rmon collection stats <index (1-65535)> 
   PRVID   : 15
   HELP    : Disable RMON statistic collection on the interface 
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              rmon RMON related configuration|
              collection Collection related configuration|
              stats Statistic collection configuration on the interface|
              (1-65535) Statistics table index|
              <CR> Disable RMON statistic collection on the interface


   COMMAND : rmon collection history <short (1-65535)> [buckets <short (1-65535)>] [interval <short (1-3600)>] [owner <string (127)>]
   ACTION  : {
                 UINT4 u4Buckets;
                 UINT4 u4Interval;
                 if($4 != NULL)
                 {
                     u4Buckets = *(INT4 *)$5; 
                 }
                 else
                 {
                     u4Buckets = RMON_DEF_BUCKETS;
                 }
                 if($6 != NULL)
                 {
                     u4Interval = *(INT4 *)$7; 
                 }
                 else
                 {
                     u4Interval = RMON_DEF_INTERVAL;
                 }
 
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_COLLECTION_HISTORY,
                                      NULL, $3, u4Buckets, u4Interval, $9);
             }
   SYNTAX  : rmon collection history <index (1-65535)> [buckets <bucket-number (1-65535)>] [interval <seconds (1-3600)>] [owner <ownername (127)>]
   PRVID   : 15
   HELP    : Enable history collection for the specified number of buckets and time period 
   CXT_HELP : rmon Configures RMON related parameters|
              collection Collection related configuration|
              history Interface statistics history collection configuration|
              (1-65535) History table index|
              buckets Maximum number of buckets desired for the RMON collection history group of statistics|
              (1-65535) Total number of buckets|
              interval Number of seconds in each polling cycle|
              (1-3600) Intreval in seconds|
              owner RMON group owner configuration|
              <string(127)> RMON group owner name|
              <CR> Enable history collection for the specified number of buckets and time period

   COMMAND : no rmon collection history <short (1-65535)> 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_NO_COLLECTION_HISTORY,                                                                     NULL, $4);
             }
   SYNTAX  : no rmon collection history <index (1-65535)> 
   PRVID   : 15
   HELP    : Disable history collection on the interface 
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              rmon RMON related configuration|
              collection Collection related configuration|
              history Interface statistics history collection configuration|
              (1-65535) History table index|
              <CR> Disable history collection on the interface
END GROUP

DEFINE GROUP: RMON_VLAN_GRP

   COMMAND : rmon collection stats <short (1-65535)> [owner <string (127)>] 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_COLLECTION_STATS_FOR_VLAN,
                                      NULL, $3, $5);
             }
   SYNTAX  : rmon collection stats <index (1-65535)> [owner <ownername (127)>]
   PRVID   : 15
   HELP    : Enable RMON statistic collection on the VLAN 
   CXT_HELP : rmon Configures RMON related parameters|
              collection Collection related configuration|
              stats Statistic collection configuration on the VLAN|
              (1-65535) Statistics table index|
              owner RMON group owner configuration|
              <string(127)> RMON group owner name|
              <CR> Enable RMON statistic collection on the VLAN

   COMMAND : no rmon collection stats <short (1-65535)> 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_NO_COLLECTION_STATS_FOR_VLAN,
                                      NULL, $4);
             }
   SYNTAX  : no rmon collection stats <index (1-65535)> 
   PRVID   : 15
   HELP    : Disable RMON statistic collection on the VLAN
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              rmon RMON related configuration|
              collection Collection related configuration|
              stats Statistic collection configuration on the VLAN|
              (1-65535) Statistics table index|
              <CR> Disable RMON statistic collection on the VLAN


   COMMAND : rmon collection history <short (1-65535)> [buckets <short (1-65535)>] [interval <short (1-3600)>] [owner <string (127)>]
   ACTION  : {
                 UINT4 u4Buckets;
                 UINT4 u4Interval;
                 if($4 != NULL)
                 {
                     u4Buckets = *(INT4 *)$5; 
                 }
                 else
                 {
                     u4Buckets = RMON_DEF_BUCKETS;
                 }
                 if($6 != NULL)
                 {
                     u4Interval = *(INT4 *)$7; 
                 }
                 else
                 {
                     u4Interval = RMON_DEF_INTERVAL;
                 }
 
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_COLLECTION_HISTORY_FOR_VLAN,
                                      NULL, $3, u4Buckets, u4Interval, $9);
             }
   SYNTAX  : rmon collection history <index (1-65535)> [buckets <bucket-number (1-65535)>] [interval <seconds (1-3600)>] [owner <ownername (127)>]
   PRVID   : 15
   HELP    : Enable history collection for the specified number of buckets and time period 
   CXT_HELP : rmon Configures RMON related parameters|
              collection Collection related configuration|
              history Statistics history collection configuration based on VLAN|
              (1-65535) History table index|
              buckets Maximum number of buckets desired for the RMON collection history group of statistics|
              (1-65535) Total number of buckets|
              interval Number of seconds in each polling cycle|
              (1-3600) Interval in seconds|
              owner RMON group owner configuration|
              <string(127)> RMON group owner name|
              <CR> Enable history collection for the specified number of buckets and time period


   COMMAND : no rmon collection history <short (1-65535)> 
   ACTION  : {
                 cli_process_rmon_cmd(CliHandle, CLI_RMON_NO_COLLECTION_HISTORY_FOR_VLAN,                                                                     NULL, $4);
             }
   SYNTAX  : no rmon collection history <index (1-65535)> 
   PRVID   : 15
   HELP    : Disable history collection on the VLAN 
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              rmon RMON related configuration|
              collection Collection related configuration|
              history Statistics history collection configuration based on VLAN|
              (1-65535) History table index|
              <CR> Disable history collection on the VLAN
END GROUP


