
# ifndef stdrmOGP_H
# define stdrmOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_ETHERSTATSTABLE                              (0)
# define SNMP_OGP_INDEX_HISTORYCONTROLTABLE                          (1)
# define SNMP_OGP_INDEX_ETHERHISTORYTABLE                            (2)
# define SNMP_OGP_INDEX_ALARMTABLE                                   (3)
# define SNMP_OGP_INDEX_HOSTCONTROLTABLE                             (4)
# define SNMP_OGP_INDEX_HOSTTABLE                                    (5)
# define SNMP_OGP_INDEX_HOSTTIMETABLE                                (6)
# define SNMP_OGP_INDEX_HOSTTOPNCONTROLTABLE                         (7)
# define SNMP_OGP_INDEX_HOSTTOPNTABLE                                (8)
# define SNMP_OGP_INDEX_MATRIXCONTROLTABLE                           (9)
# define SNMP_OGP_INDEX_MATRIXSDTABLE                                (10)
# define SNMP_OGP_INDEX_MATRIXDSTABLE                                (11)
# define SNMP_OGP_INDEX_FILTERTABLE                                  (12)
# define SNMP_OGP_INDEX_CHANNELTABLE                                 (13)
# define SNMP_OGP_INDEX_BUFFERCONTROLTABLE                           (14)
# define SNMP_OGP_INDEX_CAPTUREBUFFERTABLE                           (15)
# define SNMP_OGP_INDEX_EVENTTABLE                                   (16)
# define SNMP_OGP_INDEX_LOGTABLE                                     (17)

#endif /*  stdrmonOGP_H  */
