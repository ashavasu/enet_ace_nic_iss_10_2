#ifndef   RMONDEBG_H
#define   RMONDEBG_H
/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmondebg.h,v 1.15 2007/03/09 13:09:31 iss Exp $            *
 *                                                                  *
 * Description: Contains debug trace definitions.                   *
 *                                                                  *
 ********************************************************************/

#ifdef RMONMAIN_C
UINT4      gu4RmondebgFlag ;
#else
extern UINT4      gu4RmondebgFlag ;
#endif /* RMONMAIN_C */

#define   RMON_DBG_FLAG   gu4RmondebgFlag 

#define   CRITICAL_TRC      0x00000001
#define   MAJOR_TRC         0x00000002
#define   MINOR_TRC         0x00000004

#define   FUNC_ENTRY        0x00000008
#define   FUNC_EXIT         0x00000010
#define   RMON_MGMT_TRC     0x00000040 
#define   PKT_PROC_TRC      0x00000080
#define   MEM_TRC           0x00000100 
#define   CTRL_TMR_TRC      0x00000200
#define   RMON_EVENT_TRC    0x00000400
#define   EVENT_TMR         0x00000050

#if  DEBUG_RMON == ON

#define RMON_DBG(Value ,Fmt) \
        UtlTrcLog(RMON_DBG_FLAG ,Value ,"RMON",Fmt)

#define RMON_DBG1(Value ,Fmt ,Arg1) \
	     UtlTrcLog(RMON_DBG_FLAG ,Value ,"RMON",Fmt ,Arg1)

#define RMON_DBG2(Value ,Fmt ,Arg1 ,Arg2) \
	     UtlTrcLog(RMON_DBG_FLAG ,Value ,"RMON",Fmt ,Arg1 ,Arg2)

#define RMON_DBG3(Value ,Fmt ,Arg1 ,Arg2 ,Arg3) \
        UtlTrcLog(RMON_DBG_FLAG ,Value ,"RMON",Fmt ,Arg1 ,Arg2 ,Arg3)

#define RMON_DBG4(Value ,Fmt ,Arg1 ,Arg2 ,Arg3 ,Arg4) \
        UtlTrcLog(RMON_DBG_FLAG ,Value ,"RMON",Fmt ,Arg1 ,Arg2 ,Arg3 ,Arg4)

#define RMON_PRINTF(Fmt)    printf(Fmt)

#else
#define RMON_DBG(Value ,Fmt)

#define RMON_DBG1(Value ,Fmt ,Arg1) 

#define RMON_DBG2(Value ,Fmt ,Arg1 ,Arg2) 

#define RMON_DBG3(Value ,Fmt ,Arg1 ,Arg2 ,Arg3)

#define RMON_DBG4(Value ,Fmt ,Arg1 ,Arg2 ,Arg3 ,Arg4)

#define RMON_PRINTF(Fmt)

#endif /* DEBUG_RMON == ON */

#endif /*RMONDEBG_H */

