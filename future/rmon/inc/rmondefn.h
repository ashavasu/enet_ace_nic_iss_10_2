#ifndef RMONDEFN_H
#define RMONDEFN_H
/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmondefn.h,v 1.27 2017/01/17 14:10:29 siva Exp $            *
 *                                                                  *
 * Description: Contains definitions.                               *
 *                                                                  *
 *******************************************************************/

/* Maximum OID Length */
#define RMON_MAX_OID_LENGTH  256
#define RMON_MAX_OCTET_STRING_LEN 256

/* Ethernet packet type */
#define     NORMAL           1 
#define     BROADCAST_TYPE   2
#define     BROADCASTERR     3
#define     MULTICAST_TYPE   4
#define     MULTICASTERR     5
#define     FCSERR           6
#define     ALIGNMENTERR     7
#define     UNDERSIZE_TYPE   8
#define     OVERSIZE_TYPE    9
#define     UNDERSIZE_FCSERR       10
#define     UNDERSIZE_ALIGNMENTERR 11
#define     OVERSIZE_FCSERR        12
#define     OVERSIZE_ALIGNMENTERR  13
#define     EQUAL64          14
#define     P65To127         15
#define     P128To255        16
#define     P512To1023       17
#define     P1024To1518      18

#define RMON_SUCCESS         0
#define RMON_FAILURE       (-1)
#define RMON_INVALID_OID     (-2)
#define RMON_OID             "1.3.6.1.2.1.16" 
#define OBJECT_NAME_LEN     256 
#define RMON_OCTET_LEN            127 
#define IFINDEX_OID_SIZE    10
#define VLAN_INDEX_OID_SIZE        13
#define RMON_ONE          1
#define RMON_ZERO          0
#define RMON_DEFAULT_CONTEXT       L2IWF_DEFAULT_CONTEXT

#define MONITOR                      "Monitor\0"
#define MONITOR_LEN                  8

#define RMON_MODULE                  1
#define RMON_HW_MODULE              3

#define MEM_ERR                      stdout

#define ETHER_STATS_TABLE            1
#define HISTORY_CONTROL_TABLE        2
#define HISTORY_DATA_TABLE           3
#define ALARM_TABLE                  4
#define HOST_CONTROL_TABLE           5
#define HOST_DATA_ADDRESS_TABLE      6
#define HOST_DATA_TIME_TABLE         7
#define HOST_TOPN_CONTROL_TABLE      8
#define HOST_TOPN_DATA_TABLE         9
#define MATRIX_CONTROL_TABLE         10
#define MATRIX_DATA_SD_TABLE         11
#define MATRIX_DATA_DS_TABLE         12
#define EVENT_TABLE                  13
#define LOG_TABLE                    14

#define RMON_OBJECT_SET_TIME    600

#define STATS_TABLE_DURATION    (RMON_OBJECT_SET_TIME)
#define HIST_CTRL_DURATION      (RMON_OBJECT_SET_TIME)
#define ALARM_DURATION          (RMON_OBJECT_SET_TIME)
#define HOST_TABLE_DURATION     (RMON_OBJECT_SET_TIME)
#define TOPN_TABLE_DURATION     (RMON_OBJECT_SET_TIME)
#define MTRX_CTRL_DURATION      (RMON_OBJECT_SET_TIME)
#define EVENT_DURATION          (RMON_OBJECT_SET_TIME)

#define GET_DATA_SOURCE_OID      RmonFormDataSourceOid
#define SET_DATA_SOURCE_OID      RmonSetDataSourceOid
#define TEST_DATA_SOURCE_OID     RmonTestDataSourceOid

#define IF_RISING_ALARM_TRAP     1
#define IF_FALLING_ALARM_TRAP    2

#define RMON_RISING_ALARM               1
#define RMON_FALLING_ALARM              2
#define RMON_RISING_OR_FALLING_ALARM    3
#define RMON_ALARM_LIMIT  4
#define RMON_MIN_RISING_THRESHOLD       0
#define RMON_MAX_RISING_THRESHOLD      2147483647
#define RMON_MIN_FALLING_THRESHOLD       0
#define RMON_MAX_FALLING_THRESHOLD      2147483647
#define RMON_MAX_SAMPLE_INDEX      2147483647

#define RMON_ONE_MINUTE   60

#define RMON_HISTORY_CTRL_INTERVAL 1800

#define RMON_ETHERSTATS_HASH_SIZE 20
#define RMON_HISTORY_CONTROL_HASH_SIZE 20
#define RMON_ALARM_HASH_SIZE 20
#define RMON_EVENT_HASH_SIZE 20

#define RMON_MAX_ETHERSTATS_INDEX 65535
#define RMON_MAX_HISTORY_CONTROL_INDEX 65535
#define RMON_MAX_ALARM_INDEX 65535
#define RMON_MAX_EVENT_INDEX 65535
#define RMON_FRAME_DATA_SIZE (1800)

#define RMON_SEM_NAME         ((const UINT1 *)"RMNS")
#define RMON_SEM_COUNT        1
#define RMON_SEM_FLAGS        OSIX_DEFAULT_SEM_MODE
#define RMON_SEM_NODE_ID      SELF

/* Osix, Task, Queue and Event related definitions */
#define RMON_Q_NAME           (UINT1 *) "RMNQ"
#define RMON_Q_DEPTH          500
#define RMON_SEND_EVENT       OsixEvtSend 
#define RMON_RECEIVE_EVENT    OsixEvtRecv 
#define RMON_DELETE_TASK      OsixTskDel
#define RMON_CREATE_SEM       OsixCreateSem 
#define RMON_TAKE_SEM         OsixSemTake 
#define RMON_DELETE_SEM       OsixSemDel 
#define RMON_GIVE_SEM         OsixSemGive 
#define RMON_GET_TASK_ID      OsixGetTaskId
#define RMON_CREATE_QUEUE     OsixQueCrt
#define RMON_DELETE_QUEUE     OsixQueDel
#define RMON_SEND_TO_QUEUE    OsixQueSend
#define RMON_RECV_FROM_QUEUE  OsixQueRecv


#define RMON_TASK_ID          gu4RmonTaskId
#define RMON_SEM_ID           gRmonSemId
#define RMON_QUEUE_ID         gRmonQId

#define RMON_DELETE_VLAN_MSG     1
#define RMON_DELETE_PORT_MSG     2
 
#define RMON_DUMMY(X)  (X = (X))

#define RMON_MIN(X,Y)      (((X)<(Y))?(X):(Y))

#define RMON_INCL_IN_BETWEEN(variable,firstval,secondval) \
          ((variable >= firstval) && (variable <= secondval))

#define RMON_GET_TIMER_REFERENCE_TABLE_TYPE(timer_reference) \
        ((tTimerReference *)&timer_reference)->u1TableType

#define RMON_GET_TIMER_REFERENCE_INDEX(timer_reference) \
        ((tTimerReference *)&timer_reference)->u2Index

#define RMON_SET_TIMER_REFERENCE_TABLE_TYPE(timer_reference,X) \
        ((tTimerReference *)&timer_reference)->u1TableType=X

#define RMON_SET_TIMER_REFERENCE_INDEX(timer_reference,X) \
        ((tTimerReference *)&timer_reference)->u2Index=X

#define RMON_GET_TIMER_REFERENCE_MODULE_TYPE(timer_reference) \
        ((tTimerReference *)&timer_reference)->u1ModuleId

#define RMON_SET_TIMER_REFERENCE_MODULE_TYPE(timer_reference,X) \
        ((tTimerReference *)&timer_reference)->u1ModuleId=X

/* Hash Table related macros */
#define   RMON_HASH_CREATE_TABLE              TMO_HASH_Create_Table

#define   RMON_HASH_GET_FIRST_BUCKET_NODE     TMO_HASH_Get_First_Bucket_Node

#define   RMON_HASH_GET_NEXT_BUCKET_NODE      TMO_HASH_Get_Next_Bucket_Node

#define   RMON_HASH_DELETE_TABLE              TMO_HASH_Delete_Table

#define   RMON_HASH_FREE(ptr)                 TMO_FREE((VOID *)ptr)

#define   RMON_HASH_INIT_NODE(pNode)\
             TMO_HASH_INIT_NODE((tRmonHashNode *)pNode)

#define   RMON_HASH_ADD_NODE(pHashTab, pNode, u4Index,FuncParam)\
             TMO_HASH_Add_Node((tRmonHashTable *)pHashTab,\
             (tRmonHashNode *)pNode,(UINT4)u4Index, (UINT1 *)FuncParam)

#define   RMON_HASH_DEL_NODE(pHashTab, pNode, u4Index)\
             TMO_HASH_Delete_Node((tRmonHashTable *)pHashTab, \
             (tRmonHashNode *)pNode, u4Index)

#define   RMON_HASH_SCAN_BKT(pHashTab, u4Index, pNode, Typecast)\
             TMO_HASH_Scan_Bucket((tRmonHashTable *)pHashTab, u4Index,\
             (tRmonHashNode *)pNode, Typecast)

#define   RMON_HASH_SCAN_TBL(pHashTab, u4Index)\
             TMO_HASH_Scan_Table((tRmonHashTable *)pHashTab, u4Index)


/* SLL related macros */

#define tRmonSLL     tTMO_SLL 

#define tRmonSLLNode tTMO_SLL_NODE

#define RMON_SLL_INIT(pList) \
                      TMO_SLL_Init(pList)

#define RMON_SLL_INIT_NODE(pNode) \
                      TMO_SLL_Init_Node(pNode)

#define RMON_SLL_ADD(pList, pNode) \
                      TMO_SLL_Add(pList, pNode)

#define RMON_SLL_DELETE(pList, pNode) \
                      TMO_SLL_Delete (pList, pNode)

#define RMON_SLL_COUNT(pList) \
                      TMO_SLL_Count(pList)

#define RMON_SLL_NEXT(pList, pNode) \
                      TMO_SLL_Next(pList,pNode)

#define RMON_SLL_FIRST(pList) \
                      TMO_SLL_First(pList)

#define RMON_SLL_GET(pList) \
                      TMO_SLL_Get(pList)

#define RMON_SLL_LAST(pList) \
                      TMO_SLL_Last(pList)

#define RMON_SLL_IS_NODE_IN_LIST(pNode) \
                      TMO_SLL_Is_Node_In_List(pNode)

#define RMON_SLL_SCAN(pList,pNode,TypeCast) \
                      TMO_SLL_Scan(pList,pNode,TypeCast)


#define RMON_IS_STARTED() (gRmonSystemControlFlag == RMON_SYS_START)


#define RMON_IS_ENABLED() (gRmonEnableStatusFlag == RMON_ENABLE)
    
#define   RMON_CHK_NULL_PTR(ptr)                              \
          if(ptr == NULL)                                     \
          {                                                   \
              return;                                         \
          }

#endif /*RMONDEFN_H */ 
 
