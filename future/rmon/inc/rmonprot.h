/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmonprot.h,v 1.28 2017/01/17 14:10:29 siva Exp $            *
 *                                                                  *
 * Description: Contains prototypes.                                *
 *                                                                  *
 *******************************************************************/

#ifndef RMONPROT_H
#define RMONPROT_H
UINT1 RmonisErrorInFCS ARG_LIST((UINT1 *, UINT2));
UINT1 RmonisBroadCast ARG_LIST((UINT1 *));
UINT1 RmonisMultiCast ARG_LIST((UINT1 *));

INT4  RmonStartTimer ARG_LIST((UINT1, INT4, INT4 ));
INT4  RmonStopTimer ARG_LIST((UINT1,INT4));
      
void RmonPeriodicUpdates(void);
void RmonFetchHWStats (void);
void RmonSetHWConfigurations (UINT1);
void RmonClearEtherStatsTable (void);

void  RmonUpdateHistoryTable ARG_LIST((void));
void  RmonUpdateHostTopNTable ARG_LIST((void));
      
void  RmonCheckAndGenerateAlarm ARG_LIST((void));

void  RmonGenerateEvent ARG_LIST((INT4 ,UINT1 ,INT4));
UINT1 * RmonParseSubIdNew ARG_LIST((UINT1 *, UINT4 *));
tSNMP_OID_TYPE * RmonMakeObjIdFromDotNew ARG_LIST((UINT1 *));

void  RmonLogEvent ARG_LIST((INT4));

INT4  RmonGetMibVariableValue ARG_LIST((tSNMP_OID_TYPE *, UINT4 *));

void  RmonFormDataSourceOid ARG_LIST((tSNMP_OID_TYPE *, UINT4, UINT4));
void  RmonSetDataSourceOid  ARG_LIST((tSNMP_OID_TYPE *, UINT4 *, UINT4 *));
INT1  RmonTestDataSourceOid  ARG_LIST((tSNMP_OID_TYPE *));
INT4  RmonCheckDataSourceOIDIsVlanOrInterface ARG_LIST((tSNMP_OID_TYPE *, UINT4 *));
void  RmonTmmSendEvent ARG_LIST((UINT1,UINT4));
void  RmonProcessTimeOut ARG_LIST((UINT4));
      
void  RmonEtherStatTimerExpiry ARG_LIST((UINT4));
void  RmonHistCtrlTimerExpiry ARG_LIST((UINT4));
void  RmonAlrmTimerExpiry ARG_LIST((UINT4));
void  RmonHostCtrlTimerExpiry ARG_LIST((UINT4));
void  RmonTopNCtrlTimerExpiry ARG_LIST((UINT4));
void  RmonMtrxCtrlTimerExpiry ARG_LIST((UINT4));
void  RmonEvntTimerExpiry     ARG_LIST((UINT4));
      

void  RmonInvalidateCtrlTableEntries ARG_LIST((void));
INT4 RmonCreateTask ARG_LIST((void));

INT4 RmonCompareMacAddr ARG_LIST((tMacAddr addr1, tMacAddr addr2));
tHostTableEntry * SearchHostTable ARG_LIST((INT4 i4Index, tMacAddr * pFrameAddress));
tHostTableEntry * CreateNewHostEntry ARG_LIST((INT4 i4Index));

INT4 SetInterfaceOid ARG_LIST((void));
INT4 SetVlanIndexOid ARG_LIST((void));
tTmrAppTimer *RmonGetTableTimer ARG_LIST((UINT1 , INT4));

INT2  RmonMacDiff ARG_LIST((tMacAddr , tMacAddr ));
INT1  RmonIsMacZero ARG_LIST((UINT1 *));

INT4  RmonShutdown(void);
void  RmonHandleQMsg(void);

/* Prototypes for SNMP type objects alloc & free */

tSNMP_OID_TYPE   * rmon_alloc_oid(INT4 i4_size);
void rmon_free_oid(tSNMP_OID_TYPE * temp);
void rmon_free_octetstring(tSNMP_OCTET_STRING_TYPE * temp);
tSNMP_OCTET_STRING_TYPE * rmon_allocmem_octetstring(INT4 i4_size);

void RMON_FreeVarbind(tSNMP_VAR_BIND * vb_ptr);
INT4 RMON_AGT_ConvSubOIDToLong(UINT1 **ppu1_temp_ptr);
tSNMP_OID_TYPE * RMON_AGT_GetOidFromString(INT1 *pi1_str);

tSNMP_OID_TYPE * RMON_AGT_FormOid(UINT4 u4_s_oid[], INT2 i2_length);
void RMON_FreeVarbindList(tSNMP_VAR_BIND * vb_ptr);

INT4 RmonHashFn (UINT4);
tRmonEtherStatsTimerNode* HashSearchEtherStatsNode (tRmonHashTable* , INT4);
tRmonEtherStatsTimerNode* HashSearchEtherStatsNode_DSource (tRmonHashTable* , UINT4, UINT4);
INT4 HashSearchEtherStatsNode_NextValidTindex (tRmonHashTable*, INT4);
tRmonHistoryControlNode* HashSearchHistoryControlNode (tRmonHashTable*, INT4);
INT4 HashSearchHistoryControlNode_NextValidTindex (tRmonHashTable*, INT4);
tRmonAlarmNode* HashSearchAlarmNode (tRmonHashTable*, INT4);
INT4 HashSearchAlarmNode_NextValidTindex (tRmonHashTable*, INT4);
tRmonEventNode* HashSearchEventNode (tRmonHashTable*, INT4);
INT4 HashSearchEventNode_NextValidTindex (tRmonHashTable*, INT4);
UINT1 HashSearchHistoryControlNode_DSource (tRmonHashTable *, UINT4, UINT4);
UINT1 HashSearchAlarmNode_ForStatsIndex (tRmonHashTable *, INT4);
UINT1 rmon_n_compare_oids (tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT2);
UINT1 HashSearchAlarmNode_ForEvent (tRmonHashTable*, INT4);

#endif

