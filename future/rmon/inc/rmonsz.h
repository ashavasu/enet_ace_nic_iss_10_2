/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: rmonsz.h,v 1.4 2017/01/17 14:10:29 siva Exp $               *
 *                                                                  *
 * Description: Contains all Header files included.                 *
 *                                                                  *
 *******************************************************************/
enum {
    MAX_RMON_ALARM_ENTRY_NODES_SIZING_ID,
    MAX_RMON_ALARM_NODES_SIZING_ID,
    MAX_RMON_ALARM_VAR_BLOCKS_SIZING_ID,
    MAX_RMON_ETHER_HIS_BLOCKS_SIZING_ID,
    MAX_RMON_ETHER_STATS_NODES_SIZING_ID,
    MAX_RMON_EVENT_COMM_BLOCKS_SIZING_ID,
    MAX_RMON_EVENT_NODES_SIZING_ID,
    MAX_RMON_HIS_BUCKETS_NODES_SIZING_ID,
    MAX_RMON_HISTORY_NODES_SIZING_ID,
    MAX_RMON_HOST_CTRL_BLOCKS_SIZING_ID,
    MAX_RMON_HOST_TABLE_BLOCKS_SIZING_ID,
    MAX_RMON_LOG_BUCKET_BLOCKS_SIZING_ID,
    MAX_RMON_LOG_EVENT_BLOCKS_SIZING_ID,
    MAX_RMON_MATRIX_BUCKETS_BLOCKS_SIZING_ID,
    MAX_RMON_MATRIX_CTRL_BLOCKS_SIZING_ID,
    MAX_RMON_TOPN_CONTROL_ENTRY_SIZING_ID,
    MAX_RMON_TOPN_REPORT_BLOCKS_SIZING_ID,
    MAX_RMON_Q_MESG_SIZING_ID,
    RMON_MAX_SIZING_ID
};


#ifdef  _RMONSZ_C
tMemPoolId RMONMemPoolIds[ RMON_MAX_SIZING_ID];
INT4  RmonSizingMemCreateMemPools(VOID);
VOID  RmonSizingMemDeleteMemPools(VOID);
INT4  RmonSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RMONSZ_C  */
extern tMemPoolId RMONMemPoolIds[ ];
extern INT4  RmonSizingMemCreateMemPools(VOID);
extern VOID  RmonSizingMemDeleteMemPools(VOID);
extern INT4  RmonSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _RMONSZ_C  */


#ifdef  _RMONSZ_C
tFsModSizingParams FsRMONSizingParams [] = {
{ "tRmonAlarmEntry", "MAX_RMON_ALARM_ENTRY_NODES", sizeof(tRmonAlarmEntry),MAX_RMON_ALARM_ENTRY_NODES, MAX_RMON_ALARM_ENTRY_NODES,0 },
{ "tRmonAlarmNode", "MAX_RMON_ALARM_NODES", sizeof(tRmonAlarmNode),MAX_RMON_ALARM_NODES, MAX_RMON_ALARM_NODES,0 },
{ "UINT1[1024]", "MAX_RMON_ALARM_VAR_BLOCKS", sizeof(UINT1[1024]),MAX_RMON_ALARM_VAR_BLOCKS, MAX_RMON_ALARM_VAR_BLOCKS,0 },
{ "tEtherHistoryEntry", "MAX_RMON_ETHER_HIS_BLOCKS", sizeof(tEtherHistoryEntry),MAX_RMON_ETHER_HIS_BLOCKS, MAX_RMON_ETHER_HIS_BLOCKS,0 },
{ "tRmonEtherStatsTimerNode", "MAX_RMON_ETHER_STATS_NODES", sizeof(tRmonEtherStatsTimerNode),MAX_RMON_ETHER_STATS_NODES, MAX_RMON_ETHER_STATS_NODES,0 },
{ "tRmonEventCommBlock", "MAX_RMON_EVENT_COMM_BLOCKS", sizeof(tRmonEventCommBlock),MAX_RMON_EVENT_COMM_BLOCKS, MAX_RMON_EVENT_COMM_BLOCKS,0 },
{ "tRmonEventNode", "MAX_RMON_EVENT_NODES", sizeof(tRmonEventNode),MAX_RMON_EVENT_NODES, MAX_RMON_EVENT_NODES,0 },
{ "tRmonHisBuckets", "MAX_RMON_HIS_BUCKETS_NODES", sizeof(tRmonHisBuckets),MAX_RMON_HIS_BUCKETS_NODES, MAX_RMON_HIS_BUCKETS_NODES,0 },
{ "tRmonHistoryControlNode", "MAX_RMON_HISTORY_NODES", sizeof(tRmonHistoryControlNode),MAX_RMON_HISTORY_NODES, MAX_RMON_HISTORY_NODES,0 },
{ "tHostControl", "MAX_RMON_HOST_CTRL_BLOCKS", sizeof(tHostControl),MAX_RMON_HOST_CTRL_BLOCKS, MAX_RMON_HOST_CTRL_BLOCKS,0 },
{ "tRmonHostTableBlock", "MAX_RMON_HOST_TABLE_BLOCKS", sizeof(tRmonHostTableBlock),MAX_RMON_HOST_TABLE_BLOCKS, MAX_RMON_HOST_TABLE_BLOCKS,0 },
{ "tRmonLogBucketBlock", "MAX_RMON_LOG_BUCKET_BLOCKS", sizeof(tRmonLogBucketBlock),MAX_RMON_LOG_BUCKET_BLOCKS, MAX_RMON_LOG_BUCKET_BLOCKS,0 },
{ "tLogEvent", "MAX_RMON_LOG_EVENT_BLOCKS", sizeof(tLogEvent),MAX_RMON_LOG_EVENT_BLOCKS, MAX_RMON_LOG_EVENT_BLOCKS,0 },
{ "tMatrixBucketsBlock", "MAX_RMON_MATRIX_BUCKETS_BLOCKS", sizeof(tMatrixBucketsBlock),MAX_RMON_MATRIX_BUCKETS_BLOCKS, MAX_RMON_MATRIX_BUCKETS_BLOCKS,0 },
{ "tMatrixControlEntry", "MAX_RMON_MATRIX_CTRL_BLOCKS", sizeof(tMatrixControlEntry),MAX_RMON_MATRIX_CTRL_BLOCKS, MAX_RMON_MATRIX_CTRL_BLOCKS,0 },
{ "tHostTopnControl", "MAX_RMON_TOPN_CONTROL_ENTRY", sizeof(tHostTopnControl),MAX_RMON_TOPN_CONTROL_ENTRY, MAX_RMON_TOPN_CONTROL_ENTRY,0 },
{ "tTopNReportBlock", "MAX_RMON_TOPN_REPORT_BLOCKS", sizeof(tTopNReportBlock),MAX_RMON_TOPN_REPORT_BLOCKS, MAX_RMON_TOPN_REPORT_BLOCKS,0 },
{ "tRmonQMsg", "MAX_RMON_VLAN_BLOCKS", sizeof(tRmonQMsg),MAX_RMON_VLAN_BLOCKS, MAX_RMON_VLAN_BLOCKS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RMONSZ_C  */
extern tFsModSizingParams FsRMONSizingParams [];
#endif /*  _RMONSZ_C  */


