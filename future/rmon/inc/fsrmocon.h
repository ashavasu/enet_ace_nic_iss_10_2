
# ifndef fsrmoOCON_H
# define fsrmoOCON_H
/*
 *  The Constant Declarations for
 *  futrmon
 */

# define RMONDEBUGTYPE                                     (1)
# define RMONENABLESTATUS                                  (2)
# define RMONHWSTATSSUPP                                   (3)
# define RMONHWHISTORYSUPP                                 (4)
# define RMONHWALARMSUPP                                   (5)
# define RMONHWHOSTSUPP                                    (6)
# define RMONHWHOSTTOPNSUPP                                (7)
# define RMONHWMATRIXSUPP                                  (8)
# define RMONHWEVENTSUPP                                   (9)
# define RMONSYSTEMCONTROL                                 (10)

#endif /*  fsrmonOCON_H  */
