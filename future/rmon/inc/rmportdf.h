/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmportdf.h,v 1.24 2017/01/17 14:10:29 siva Exp $            *
 *                                                                  *
 * Description: Contains definitions that need to be ported.        *
 *                                                                  *
 *******************************************************************/

typedef UINT4 tTimeTicks;

#define RMON_MEM_CMP(a,b,cnt)     MEMCMP(a,b,cnt)

#define COMPARE_MAC_ADDRESS(x, y) RmonCompareMacAddr(x,y) 

#define RMON_MEM_COPY(a,b,count) MEMCPY(a,b,count)
#define COPY_MAC_ADDRESS(x,y)    RMON_MEM_COPY(x,y,ADDRESS_LENGTH)

#define RMON_MEM_SET(a,data,count) MEMSET(a,data,count)
#define ZERO_MAC_ADDRESS(x)      RMON_MEM_SET(x,0,ADDRESS_LENGTH)

#define MAC_DIFF(x, y)          RmonMacDiff(x,y)
#define IS_MAC_ZERO(x)          RmonIsMacZero(x)

#define CMP_HOST(I,A,J,B)   ((I==J)?MAC_DIFF(A,B):I-J)

/*
 * Maximum values
 */

#define EVENT_DESCR_LEN          128
#define LOG_DESCR_LEN            256


/* All Control Table entries are increased by 1 to meet the 
 * Maximum supported values. This is due to not using 0th entry 
 * of control tables. */


/*
 * TRUE_MAX_INTERFACE is actually RMON_MAX_INTERFACE_LIMIT
 * since we are not using the 0'th interface even though we
 * have allocated memory for that
 */
#define MAX_HOST_PER_INTERFACE   8 /* Optimum 100*/
#define MAX_MATRIX_ENTRY         3 /* Optimum 100*/

#define MAX_HISTORY_BUCKETS      50 
#define MAX_LOG_BUCKETS          50 

#define MAX_TOPN_HOSTS           MAX_HOST_PER_INTERFACE

#define STD_LOG_DESCR            "Logging Event With Description : "
#define STD_LOG_DESCR_LEN        33
#define BUCKETS_ADDED            1

#define RMON_NOT_FOUND               (-1)

#define MEM_EQ_KER(X,Y)               X=(Y)
#define KER_EQ_MEM(X,Y)               X=(Y)

#define  MMM_MALLOC       MEM_MALLOC
#define  MMM_CALLOC(x,y)  MEM_CALLOC(x,1,y)

#define MMM_REALLOC       MEM_REALLOC

#ifdef BZERO
#undef BZERO
#endif
#define  RMON_BZERO(s, n)      memset ((s), 0, (n))

#define  debug_print      fprintf

/*
 *defintions which are  not connected to core protocol start here
 */

#ifndef ARG_LIST
#define ARG_LIST(protos) protos
#endif /*!ARG_LIST*/

#define RMON_TASK_NAME              ((const UINT1 *) "RMON")
#define RMON_TASK_PRIORITY          170


#define ONE_SECOND                     1
#define RMON_CONTROL_TIMER_EVENT      0x01
#define RMON_ONE_SECOND_TIMER_EVENT   0x02
#define RMON_QMSG_EVENT               0x04

#define RMON_MAX_INDICES   SNMP_MAX_INDICES 

#define tRMON_TIMER_LIST            tTimerListId

#define RMON_T_GET_TICK             OsixGetSysTime

#define RMON_ISALPHA(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z'))
#define RMON_ISALNUM(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z') || ((c) >= '0' && (c) <= '9'))

#define RMON_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
