#ifndef RMALLINC_H
#define RMALLINC_H

/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmallinc.h,v 1.27 2015/03/27 05:41:27 siva Exp $            *
 *                                                                  *
 * Description: Contains all Header files included.                 *
 *                                                                  *
 *******************************************************************/

#include <stdio.h>
#include <string.h>

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "snmputil.h"
#include "l2iwf.h"
#include "iss.h"
#ifdef SNMP_2_WANTED
#include "fssnmp.h"
#endif

#include "rmportdf.h" 
#include "rmon.h"
#include "rmondebg.h"
#include "rmondefn.h"
#include "rmontdfs.h"
#include "rmonglob.h"
#include "rmonprot.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "rmonnp.h"
#include "nputil.h"
#endif /* NPAPI_WANTED */

#include "fsrmonwr.h"
#include "fsrmolow.h"
#include "stdrmowr.h"
#include "stdrmlow.h"

#include "rmoncli.h"

#include "rmonsz.h"

#ifdef WEBNM_WANTED
#include "fswebnm.h"
#endif

#ifndef _RMON_WEB_C_
#define _RMON_WEB_C_
#ifdef WEBNM_WANTED
VOID RmonUtlGetWebStatsPage (tHttp *);
#endif
#endif
#endif /*RMALLINC_H */
 
