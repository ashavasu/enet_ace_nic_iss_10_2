/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmondb.h,v 1.7 2013/10/28 09:53:34 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef __FUTRMONMBDB_H__
#define __FUTRMONMBDB_H__

UINT1 RmonStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

/*  MIB information  */
UINT4 fsrmon [] ={1,3,6,1,4,1,2076,44};
tSNMP_OID_TYPE fsrmonOID = {8, fsrmon};

/* OID Description */
UINT4 RmonDebugType [ ] ={1,3,6,1,4,1,2076,44,1};
UINT4 RmonEnableStatus [ ] ={1,3,6,1,4,1,2076,44,2};
UINT4 RmonHwStatsSupp [ ] ={1,3,6,1,4,1,2076,44,3};
UINT4 RmonHwHistorySupp [ ] ={1,3,6,1,4,1,2076,44,4};
UINT4 RmonHwAlarmSupp [ ] ={1,3,6,1,4,1,2076,44,5};
UINT4 RmonHwHostSupp [ ] ={1,3,6,1,4,1,2076,44,6};
UINT4 RmonHwHostTopNSupp [ ] ={1,3,6,1,4,1,2076,44,7};
UINT4 RmonHwMatrixSupp [ ] ={1,3,6,1,4,1,2076,44,8};
UINT4 RmonHwEventSupp [ ] ={1,3,6,1,4,1,2076,44,9};
UINT4 RmonStatsOutFCSErrors [ ] ={1,3,6,1,4,1,2076,44,10,1,1};
UINT4 RmonStatsPkts1519to1522Octets [ ] ={1,3,6,1,4,1,2076,44,10,1,2};



/* MBDB entry list */

tMbDbEntry fsrmonMibEntry[]= {

{{9,RmonDebugType}, NULL, RmonDebugTypeGet, RmonDebugTypeSet, RmonDebugTypeTest, RmonDebugTypeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonEnableStatus}, NULL, RmonEnableStatusGet, RmonEnableStatusSet, RmonEnableStatusTest, RmonEnableStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwStatsSupp}, NULL, RmonHwStatsSuppGet, RmonHwStatsSuppSet, RmonHwStatsSuppTest, RmonHwStatsSuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwHistorySupp}, NULL, RmonHwHistorySuppGet, RmonHwHistorySuppSet, RmonHwHistorySuppTest, RmonHwHistorySuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwAlarmSupp}, NULL, RmonHwAlarmSuppGet, RmonHwAlarmSuppSet, RmonHwAlarmSuppTest, RmonHwAlarmSuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwHostSupp}, NULL, RmonHwHostSuppGet, RmonHwHostSuppSet, RmonHwHostSuppTest, RmonHwHostSuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwHostTopNSupp}, NULL, RmonHwHostTopNSuppGet, RmonHwHostTopNSuppSet, RmonHwHostTopNSuppTest, RmonHwHostTopNSuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwMatrixSupp}, NULL, RmonHwMatrixSuppGet, RmonHwMatrixSuppSet, RmonHwMatrixSuppTest, RmonHwMatrixSuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,RmonHwEventSupp}, NULL, RmonHwEventSuppGet, RmonHwEventSuppSet, RmonHwEventSuppTest, RmonHwEventSuppDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,RmonStatsOutFCSErrors}, GetNextIndexRmonStatsTable, RmonStatsOutFCSErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RmonStatsTableINDEX, 1, 0, 0, NULL},

{{11,RmonStatsPkts1519to1522Octets}, GetNextIndexRmonStatsTable, RmonStatsPkts1519to1522OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, RmonStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData fsrmonEntry = { 11, fsrmonMibEntry };

#endif /* __MBDB_H__ */
