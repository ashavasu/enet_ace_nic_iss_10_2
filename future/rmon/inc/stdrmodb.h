/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdrmodb.h,v 1.9 2011/03/25 12:24:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDRMODB_H
#define _STDRMODB_H

UINT1 EtherStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 HistoryControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 EtherHistoryTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlarmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 HostControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 HostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 HostTimeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 HostTopNControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 HostTopNTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 MatrixControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 MatrixSDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MatrixDSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 ChannelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 BufferControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 CaptureBufferTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 EventTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LogTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdrmo [] ={1,3,6,1,2,1,16};
tSNMP_OID_TYPE stdrmoOID = {7, stdrmo};


/* Generated OID's for tables */
UINT4 EtherStatsTable [] ={1,3,6,1,2,1,16,1,1};
tSNMP_OID_TYPE EtherStatsTableOID = {9, EtherStatsTable};


UINT4 HistoryControlTable [] ={1,3,6,1,2,1,16,2,1};
tSNMP_OID_TYPE HistoryControlTableOID = {9, HistoryControlTable};


UINT4 EtherHistoryTable [] ={1,3,6,1,2,1,16,2,2};
tSNMP_OID_TYPE EtherHistoryTableOID = {9, EtherHistoryTable};


UINT4 AlarmTable [] ={1,3,6,1,2,1,16,3,1};
tSNMP_OID_TYPE AlarmTableOID = {9, AlarmTable};


UINT4 HostControlTable [] ={1,3,6,1,2,1,16,4,1};
tSNMP_OID_TYPE HostControlTableOID = {9, HostControlTable};


UINT4 HostTable [] ={1,3,6,1,2,1,16,4,2};
tSNMP_OID_TYPE HostTableOID = {9, HostTable};


UINT4 HostTimeTable [] ={1,3,6,1,2,1,16,4,3};
tSNMP_OID_TYPE HostTimeTableOID = {9, HostTimeTable};


UINT4 HostTopNControlTable [] ={1,3,6,1,2,1,16,5,1};
tSNMP_OID_TYPE HostTopNControlTableOID = {9, HostTopNControlTable};


UINT4 HostTopNTable [] ={1,3,6,1,2,1,16,5,2};
tSNMP_OID_TYPE HostTopNTableOID = {9, HostTopNTable};


UINT4 MatrixControlTable [] ={1,3,6,1,2,1,16,6,1};
tSNMP_OID_TYPE MatrixControlTableOID = {9, MatrixControlTable};


UINT4 MatrixSDTable [] ={1,3,6,1,2,1,16,6,2};
tSNMP_OID_TYPE MatrixSDTableOID = {9, MatrixSDTable};


UINT4 MatrixDSTable [] ={1,3,6,1,2,1,16,6,3};
tSNMP_OID_TYPE MatrixDSTableOID = {9, MatrixDSTable};


UINT4 FilterTable [] ={1,3,6,1,2,1,16,7,1};
tSNMP_OID_TYPE FilterTableOID = {9, FilterTable};


UINT4 ChannelTable [] ={1,3,6,1,2,1,16,7,2};
tSNMP_OID_TYPE ChannelTableOID = {9, ChannelTable};


UINT4 BufferControlTable [] ={1,3,6,1,2,1,16,8,1};
tSNMP_OID_TYPE BufferControlTableOID = {9, BufferControlTable};


UINT4 CaptureBufferTable [] ={1,3,6,1,2,1,16,8,2};
tSNMP_OID_TYPE CaptureBufferTableOID = {9, CaptureBufferTable};


UINT4 EventTable [] ={1,3,6,1,2,1,16,9,1};
tSNMP_OID_TYPE EventTableOID = {9, EventTable};


UINT4 LogTable [] ={1,3,6,1,2,1,16,9,2};
tSNMP_OID_TYPE LogTableOID = {9, LogTable};




UINT4 EtherStatsIndex [ ] ={1,3,6,1,2,1,16,1,1,1,1};
UINT4 EtherStatsDataSource [ ] ={1,3,6,1,2,1,16,1,1,1,2};
UINT4 EtherStatsDropEvents [ ] ={1,3,6,1,2,1,16,1,1,1,3};
UINT4 EtherStatsOctets [ ] ={1,3,6,1,2,1,16,1,1,1,4};
UINT4 EtherStatsPkts [ ] ={1,3,6,1,2,1,16,1,1,1,5};
UINT4 EtherStatsBroadcastPkts [ ] ={1,3,6,1,2,1,16,1,1,1,6};
UINT4 EtherStatsMulticastPkts [ ] ={1,3,6,1,2,1,16,1,1,1,7};
UINT4 EtherStatsCRCAlignErrors [ ] ={1,3,6,1,2,1,16,1,1,1,8};
UINT4 EtherStatsUndersizePkts [ ] ={1,3,6,1,2,1,16,1,1,1,9};
UINT4 EtherStatsOversizePkts [ ] ={1,3,6,1,2,1,16,1,1,1,10};
UINT4 EtherStatsFragments [ ] ={1,3,6,1,2,1,16,1,1,1,11};
UINT4 EtherStatsJabbers [ ] ={1,3,6,1,2,1,16,1,1,1,12};
UINT4 EtherStatsCollisions [ ] ={1,3,6,1,2,1,16,1,1,1,13};
UINT4 EtherStatsPkts64Octets [ ] ={1,3,6,1,2,1,16,1,1,1,14};
UINT4 EtherStatsPkts65to127Octets [ ] ={1,3,6,1,2,1,16,1,1,1,15};
UINT4 EtherStatsPkts128to255Octets [ ] ={1,3,6,1,2,1,16,1,1,1,16};
UINT4 EtherStatsPkts256to511Octets [ ] ={1,3,6,1,2,1,16,1,1,1,17};
UINT4 EtherStatsPkts512to1023Octets [ ] ={1,3,6,1,2,1,16,1,1,1,18};
UINT4 EtherStatsPkts1024to1518Octets [ ] ={1,3,6,1,2,1,16,1,1,1,19};
UINT4 EtherStatsOwner [ ] ={1,3,6,1,2,1,16,1,1,1,20};
UINT4 EtherStatsStatus [ ] ={1,3,6,1,2,1,16,1,1,1,21};
UINT4 HistoryControlIndex [ ] ={1,3,6,1,2,1,16,2,1,1,1};
UINT4 HistoryControlDataSource [ ] ={1,3,6,1,2,1,16,2,1,1,2};
UINT4 HistoryControlBucketsRequested [ ] ={1,3,6,1,2,1,16,2,1,1,3};
UINT4 HistoryControlBucketsGranted [ ] ={1,3,6,1,2,1,16,2,1,1,4};
UINT4 HistoryControlInterval [ ] ={1,3,6,1,2,1,16,2,1,1,5};
UINT4 HistoryControlOwner [ ] ={1,3,6,1,2,1,16,2,1,1,6};
UINT4 HistoryControlStatus [ ] ={1,3,6,1,2,1,16,2,1,1,7};
UINT4 EtherHistoryIndex [ ] ={1,3,6,1,2,1,16,2,2,1,1};
UINT4 EtherHistorySampleIndex [ ] ={1,3,6,1,2,1,16,2,2,1,2};
UINT4 EtherHistoryIntervalStart [ ] ={1,3,6,1,2,1,16,2,2,1,3};
UINT4 EtherHistoryDropEvents [ ] ={1,3,6,1,2,1,16,2,2,1,4};
UINT4 EtherHistoryOctets [ ] ={1,3,6,1,2,1,16,2,2,1,5};
UINT4 EtherHistoryPkts [ ] ={1,3,6,1,2,1,16,2,2,1,6};
UINT4 EtherHistoryBroadcastPkts [ ] ={1,3,6,1,2,1,16,2,2,1,7};
UINT4 EtherHistoryMulticastPkts [ ] ={1,3,6,1,2,1,16,2,2,1,8};
UINT4 EtherHistoryCRCAlignErrors [ ] ={1,3,6,1,2,1,16,2,2,1,9};
UINT4 EtherHistoryUndersizePkts [ ] ={1,3,6,1,2,1,16,2,2,1,10};
UINT4 EtherHistoryOversizePkts [ ] ={1,3,6,1,2,1,16,2,2,1,11};
UINT4 EtherHistoryFragments [ ] ={1,3,6,1,2,1,16,2,2,1,12};
UINT4 EtherHistoryJabbers [ ] ={1,3,6,1,2,1,16,2,2,1,13};
UINT4 EtherHistoryCollisions [ ] ={1,3,6,1,2,1,16,2,2,1,14};
UINT4 EtherHistoryUtilization [ ] ={1,3,6,1,2,1,16,2,2,1,15};
UINT4 AlarmIndex [ ] ={1,3,6,1,2,1,16,3,1,1,1};
UINT4 AlarmInterval [ ] ={1,3,6,1,2,1,16,3,1,1,2};
UINT4 AlarmVariable [ ] ={1,3,6,1,2,1,16,3,1,1,3};
UINT4 AlarmSampleType [ ] ={1,3,6,1,2,1,16,3,1,1,4};
UINT4 AlarmValue [ ] ={1,3,6,1,2,1,16,3,1,1,5};
UINT4 AlarmStartupAlarm [ ] ={1,3,6,1,2,1,16,3,1,1,6};
UINT4 AlarmRisingThreshold [ ] ={1,3,6,1,2,1,16,3,1,1,7};
UINT4 AlarmFallingThreshold [ ] ={1,3,6,1,2,1,16,3,1,1,8};
UINT4 AlarmRisingEventIndex [ ] ={1,3,6,1,2,1,16,3,1,1,9};
UINT4 AlarmFallingEventIndex [ ] ={1,3,6,1,2,1,16,3,1,1,10};
UINT4 AlarmOwner [ ] ={1,3,6,1,2,1,16,3,1,1,11};
UINT4 AlarmStatus [ ] ={1,3,6,1,2,1,16,3,1,1,12};
UINT4 HostControlIndex [ ] ={1,3,6,1,2,1,16,4,1,1,1};
UINT4 HostControlDataSource [ ] ={1,3,6,1,2,1,16,4,1,1,2};
UINT4 HostControlTableSize [ ] ={1,3,6,1,2,1,16,4,1,1,3};
UINT4 HostControlLastDeleteTime [ ] ={1,3,6,1,2,1,16,4,1,1,4};
UINT4 HostControlOwner [ ] ={1,3,6,1,2,1,16,4,1,1,5};
UINT4 HostControlStatus [ ] ={1,3,6,1,2,1,16,4,1,1,6};
UINT4 HostAddress [ ] ={1,3,6,1,2,1,16,4,2,1,1};
UINT4 HostCreationOrder [ ] ={1,3,6,1,2,1,16,4,2,1,2};
UINT4 HostIndex [ ] ={1,3,6,1,2,1,16,4,2,1,3};
UINT4 HostInPkts [ ] ={1,3,6,1,2,1,16,4,2,1,4};
UINT4 HostOutPkts [ ] ={1,3,6,1,2,1,16,4,2,1,5};
UINT4 HostInOctets [ ] ={1,3,6,1,2,1,16,4,2,1,6};
UINT4 HostOutOctets [ ] ={1,3,6,1,2,1,16,4,2,1,7};
UINT4 HostOutErrors [ ] ={1,3,6,1,2,1,16,4,2,1,8};
UINT4 HostOutBroadcastPkts [ ] ={1,3,6,1,2,1,16,4,2,1,9};
UINT4 HostOutMulticastPkts [ ] ={1,3,6,1,2,1,16,4,2,1,10};
UINT4 HostTimeAddress [ ] ={1,3,6,1,2,1,16,4,3,1,1};
UINT4 HostTimeCreationOrder [ ] ={1,3,6,1,2,1,16,4,3,1,2};
UINT4 HostTimeIndex [ ] ={1,3,6,1,2,1,16,4,3,1,3};
UINT4 HostTimeInPkts [ ] ={1,3,6,1,2,1,16,4,3,1,4};
UINT4 HostTimeOutPkts [ ] ={1,3,6,1,2,1,16,4,3,1,5};
UINT4 HostTimeInOctets [ ] ={1,3,6,1,2,1,16,4,3,1,6};
UINT4 HostTimeOutOctets [ ] ={1,3,6,1,2,1,16,4,3,1,7};
UINT4 HostTimeOutErrors [ ] ={1,3,6,1,2,1,16,4,3,1,8};
UINT4 HostTimeOutBroadcastPkts [ ] ={1,3,6,1,2,1,16,4,3,1,9};
UINT4 HostTimeOutMulticastPkts [ ] ={1,3,6,1,2,1,16,4,3,1,10};
UINT4 HostTopNControlIndex [ ] ={1,3,6,1,2,1,16,5,1,1,1};
UINT4 HostTopNHostIndex [ ] ={1,3,6,1,2,1,16,5,1,1,2};
UINT4 HostTopNRateBase [ ] ={1,3,6,1,2,1,16,5,1,1,3};
UINT4 HostTopNTimeRemaining [ ] ={1,3,6,1,2,1,16,5,1,1,4};
UINT4 HostTopNDuration [ ] ={1,3,6,1,2,1,16,5,1,1,5};
UINT4 HostTopNRequestedSize [ ] ={1,3,6,1,2,1,16,5,1,1,6};
UINT4 HostTopNGrantedSize [ ] ={1,3,6,1,2,1,16,5,1,1,7};
UINT4 HostTopNStartTime [ ] ={1,3,6,1,2,1,16,5,1,1,8};
UINT4 HostTopNOwner [ ] ={1,3,6,1,2,1,16,5,1,1,9};
UINT4 HostTopNStatus [ ] ={1,3,6,1,2,1,16,5,1,1,10};
UINT4 HostTopNReport [ ] ={1,3,6,1,2,1,16,5,2,1,1};
UINT4 HostTopNIndex [ ] ={1,3,6,1,2,1,16,5,2,1,2};
UINT4 HostTopNAddress [ ] ={1,3,6,1,2,1,16,5,2,1,3};
UINT4 HostTopNRate [ ] ={1,3,6,1,2,1,16,5,2,1,4};
UINT4 MatrixControlIndex [ ] ={1,3,6,1,2,1,16,6,1,1,1};
UINT4 MatrixControlDataSource [ ] ={1,3,6,1,2,1,16,6,1,1,2};
UINT4 MatrixControlTableSize [ ] ={1,3,6,1,2,1,16,6,1,1,3};
UINT4 MatrixControlLastDeleteTime [ ] ={1,3,6,1,2,1,16,6,1,1,4};
UINT4 MatrixControlOwner [ ] ={1,3,6,1,2,1,16,6,1,1,5};
UINT4 MatrixControlStatus [ ] ={1,3,6,1,2,1,16,6,1,1,6};
UINT4 MatrixSDSourceAddress [ ] ={1,3,6,1,2,1,16,6,2,1,1};
UINT4 MatrixSDDestAddress [ ] ={1,3,6,1,2,1,16,6,2,1,2};
UINT4 MatrixSDIndex [ ] ={1,3,6,1,2,1,16,6,2,1,3};
UINT4 MatrixSDPkts [ ] ={1,3,6,1,2,1,16,6,2,1,4};
UINT4 MatrixSDOctets [ ] ={1,3,6,1,2,1,16,6,2,1,5};
UINT4 MatrixSDErrors [ ] ={1,3,6,1,2,1,16,6,2,1,6};
UINT4 MatrixDSSourceAddress [ ] ={1,3,6,1,2,1,16,6,3,1,1};
UINT4 MatrixDSDestAddress [ ] ={1,3,6,1,2,1,16,6,3,1,2};
UINT4 MatrixDSIndex [ ] ={1,3,6,1,2,1,16,6,3,1,3};
UINT4 MatrixDSPkts [ ] ={1,3,6,1,2,1,16,6,3,1,4};
UINT4 MatrixDSOctets [ ] ={1,3,6,1,2,1,16,6,3,1,5};
UINT4 MatrixDSErrors [ ] ={1,3,6,1,2,1,16,6,3,1,6};
UINT4 FilterIndex [ ] ={1,3,6,1,2,1,16,7,1,1,1};
UINT4 FilterChannelIndex [ ] ={1,3,6,1,2,1,16,7,1,1,2};
UINT4 FilterPktDataOffset [ ] ={1,3,6,1,2,1,16,7,1,1,3};
UINT4 FilterPktData [ ] ={1,3,6,1,2,1,16,7,1,1,4};
UINT4 FilterPktDataMask [ ] ={1,3,6,1,2,1,16,7,1,1,5};
UINT4 FilterPktDataNotMask [ ] ={1,3,6,1,2,1,16,7,1,1,6};
UINT4 FilterPktStatus [ ] ={1,3,6,1,2,1,16,7,1,1,7};
UINT4 FilterPktStatusMask [ ] ={1,3,6,1,2,1,16,7,1,1,8};
UINT4 FilterPktStatusNotMask [ ] ={1,3,6,1,2,1,16,7,1,1,9};
UINT4 FilterOwner [ ] ={1,3,6,1,2,1,16,7,1,1,10};
UINT4 FilterStatus [ ] ={1,3,6,1,2,1,16,7,1,1,11};
UINT4 ChannelIndex [ ] ={1,3,6,1,2,1,16,7,2,1,1};
UINT4 ChannelIfIndex [ ] ={1,3,6,1,2,1,16,7,2,1,2};
UINT4 ChannelAcceptType [ ] ={1,3,6,1,2,1,16,7,2,1,3};
UINT4 ChannelDataControl [ ] ={1,3,6,1,2,1,16,7,2,1,4};
UINT4 ChannelTurnOnEventIndex [ ] ={1,3,6,1,2,1,16,7,2,1,5};
UINT4 ChannelTurnOffEventIndex [ ] ={1,3,6,1,2,1,16,7,2,1,6};
UINT4 ChannelEventIndex [ ] ={1,3,6,1,2,1,16,7,2,1,7};
UINT4 ChannelEventStatus [ ] ={1,3,6,1,2,1,16,7,2,1,8};
UINT4 ChannelMatches [ ] ={1,3,6,1,2,1,16,7,2,1,9};
UINT4 ChannelDescription [ ] ={1,3,6,1,2,1,16,7,2,1,10};
UINT4 ChannelOwner [ ] ={1,3,6,1,2,1,16,7,2,1,11};
UINT4 ChannelStatus [ ] ={1,3,6,1,2,1,16,7,2,1,12};
UINT4 BufferControlIndex [ ] ={1,3,6,1,2,1,16,8,1,1,1};
UINT4 BufferControlChannelIndex [ ] ={1,3,6,1,2,1,16,8,1,1,2};
UINT4 BufferControlFullStatus [ ] ={1,3,6,1,2,1,16,8,1,1,3};
UINT4 BufferControlFullAction [ ] ={1,3,6,1,2,1,16,8,1,1,4};
UINT4 BufferControlCaptureSliceSize [ ] ={1,3,6,1,2,1,16,8,1,1,5};
UINT4 BufferControlDownloadSliceSize [ ] ={1,3,6,1,2,1,16,8,1,1,6};
UINT4 BufferControlDownloadOffset [ ] ={1,3,6,1,2,1,16,8,1,1,7};
UINT4 BufferControlMaxOctetsRequested [ ] ={1,3,6,1,2,1,16,8,1,1,8};
UINT4 BufferControlMaxOctetsGranted [ ] ={1,3,6,1,2,1,16,8,1,1,9};
UINT4 BufferControlCapturedPackets [ ] ={1,3,6,1,2,1,16,8,1,1,10};
UINT4 BufferControlTurnOnTime [ ] ={1,3,6,1,2,1,16,8,1,1,11};
UINT4 BufferControlOwner [ ] ={1,3,6,1,2,1,16,8,1,1,12};
UINT4 BufferControlStatus [ ] ={1,3,6,1,2,1,16,8,1,1,13};
UINT4 CaptureBufferControlIndex [ ] ={1,3,6,1,2,1,16,8,2,1,1};
UINT4 CaptureBufferIndex [ ] ={1,3,6,1,2,1,16,8,2,1,2};
UINT4 CaptureBufferPacketID [ ] ={1,3,6,1,2,1,16,8,2,1,3};
UINT4 CaptureBufferPacketData [ ] ={1,3,6,1,2,1,16,8,2,1,4};
UINT4 CaptureBufferPacketLength [ ] ={1,3,6,1,2,1,16,8,2,1,5};
UINT4 CaptureBufferPacketTime [ ] ={1,3,6,1,2,1,16,8,2,1,6};
UINT4 CaptureBufferPacketStatus [ ] ={1,3,6,1,2,1,16,8,2,1,7};
UINT4 EventIndex [ ] ={1,3,6,1,2,1,16,9,1,1,1};
UINT4 EventDescription [ ] ={1,3,6,1,2,1,16,9,1,1,2};
UINT4 EventType [ ] ={1,3,6,1,2,1,16,9,1,1,3};
UINT4 EventCommunity [ ] ={1,3,6,1,2,1,16,9,1,1,4};
UINT4 EventLastTimeSent [ ] ={1,3,6,1,2,1,16,9,1,1,5};
UINT4 EventOwner [ ] ={1,3,6,1,2,1,16,9,1,1,6};
UINT4 EventStatus [ ] ={1,3,6,1,2,1,16,9,1,1,7};
UINT4 LogEventIndex [ ] ={1,3,6,1,2,1,16,9,2,1,1};
UINT4 LogIndex [ ] ={1,3,6,1,2,1,16,9,2,1,2};
UINT4 LogTime [ ] ={1,3,6,1,2,1,16,9,2,1,3};
UINT4 LogDescription [ ] ={1,3,6,1,2,1,16,9,2,1,4};




tMbDbEntry EtherStatsTableMibEntry[]= {

{{11,EtherStatsIndex}, GetNextIndexEtherStatsTable, EtherStatsIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsDataSource}, GetNextIndexEtherStatsTable, EtherStatsDataSourceGet, EtherStatsDataSourceSet, EtherStatsDataSourceTest, EtherStatsTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsDropEvents}, GetNextIndexEtherStatsTable, EtherStatsDropEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsOctets}, GetNextIndexEtherStatsTable, EtherStatsOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts}, GetNextIndexEtherStatsTable, EtherStatsPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsBroadcastPkts}, GetNextIndexEtherStatsTable, EtherStatsBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsMulticastPkts}, GetNextIndexEtherStatsTable, EtherStatsMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsCRCAlignErrors}, GetNextIndexEtherStatsTable, EtherStatsCRCAlignErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsUndersizePkts}, GetNextIndexEtherStatsTable, EtherStatsUndersizePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsOversizePkts}, GetNextIndexEtherStatsTable, EtherStatsOversizePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsFragments}, GetNextIndexEtherStatsTable, EtherStatsFragmentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsJabbers}, GetNextIndexEtherStatsTable, EtherStatsJabbersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsCollisions}, GetNextIndexEtherStatsTable, EtherStatsCollisionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts64Octets}, GetNextIndexEtherStatsTable, EtherStatsPkts64OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts65to127Octets}, GetNextIndexEtherStatsTable, EtherStatsPkts65to127OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts128to255Octets}, GetNextIndexEtherStatsTable, EtherStatsPkts128to255OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts256to511Octets}, GetNextIndexEtherStatsTable, EtherStatsPkts256to511OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts512to1023Octets}, GetNextIndexEtherStatsTable, EtherStatsPkts512to1023OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsPkts1024to1518Octets}, GetNextIndexEtherStatsTable, EtherStatsPkts1024to1518OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsOwner}, GetNextIndexEtherStatsTable, EtherStatsOwnerGet, EtherStatsOwnerSet, EtherStatsOwnerTest, EtherStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EtherStatsTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsStatus}, GetNextIndexEtherStatsTable, EtherStatsStatusGet, EtherStatsStatusSet, EtherStatsStatusTest, EtherStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, EtherStatsTableINDEX, 1, 0, 1, NULL},
};
tMibData EtherStatsTableEntry = { 21, EtherStatsTableMibEntry };

tMbDbEntry HistoryControlTableMibEntry[]= {

{{11,HistoryControlIndex}, GetNextIndexHistoryControlTable, HistoryControlIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,HistoryControlDataSource}, GetNextIndexHistoryControlTable, HistoryControlDataSourceGet, HistoryControlDataSourceSet, HistoryControlDataSourceTest, HistoryControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, HistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,HistoryControlBucketsRequested}, GetNextIndexHistoryControlTable, HistoryControlBucketsRequestedGet, HistoryControlBucketsRequestedSet, HistoryControlBucketsRequestedTest, HistoryControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HistoryControlTableINDEX, 1, 0, 0, "50"},

{{11,HistoryControlBucketsGranted}, GetNextIndexHistoryControlTable, HistoryControlBucketsGrantedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,HistoryControlInterval}, GetNextIndexHistoryControlTable, HistoryControlIntervalGet, HistoryControlIntervalSet, HistoryControlIntervalTest, HistoryControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HistoryControlTableINDEX, 1, 0, 0, "1800"},

{{11,HistoryControlOwner}, GetNextIndexHistoryControlTable, HistoryControlOwnerGet, HistoryControlOwnerSet, HistoryControlOwnerTest, HistoryControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, HistoryControlTableINDEX, 1, 0, 0, NULL},

{{11,HistoryControlStatus}, GetNextIndexHistoryControlTable, HistoryControlStatusGet, HistoryControlStatusSet, HistoryControlStatusTest, HistoryControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, HistoryControlTableINDEX, 1, 0, 1, NULL},
};
tMibData HistoryControlTableEntry = { 7, HistoryControlTableMibEntry };

tMbDbEntry EtherHistoryTableMibEntry[]= {

{{11,EtherHistoryIndex}, GetNextIndexEtherHistoryTable, EtherHistoryIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistorySampleIndex}, GetNextIndexEtherHistoryTable, EtherHistorySampleIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryIntervalStart}, GetNextIndexEtherHistoryTable, EtherHistoryIntervalStartGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryDropEvents}, GetNextIndexEtherHistoryTable, EtherHistoryDropEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryOctets}, GetNextIndexEtherHistoryTable, EtherHistoryOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryPkts}, GetNextIndexEtherHistoryTable, EtherHistoryPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryBroadcastPkts}, GetNextIndexEtherHistoryTable, EtherHistoryBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryMulticastPkts}, GetNextIndexEtherHistoryTable, EtherHistoryMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryCRCAlignErrors}, GetNextIndexEtherHistoryTable, EtherHistoryCRCAlignErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryUndersizePkts}, GetNextIndexEtherHistoryTable, EtherHistoryUndersizePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryOversizePkts}, GetNextIndexEtherHistoryTable, EtherHistoryOversizePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryFragments}, GetNextIndexEtherHistoryTable, EtherHistoryFragmentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryJabbers}, GetNextIndexEtherHistoryTable, EtherHistoryJabbersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryCollisions}, GetNextIndexEtherHistoryTable, EtherHistoryCollisionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryUtilization}, GetNextIndexEtherHistoryTable, EtherHistoryUtilizationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EtherHistoryTableINDEX, 2, 0, 0, NULL},
};
tMibData EtherHistoryTableEntry = { 15, EtherHistoryTableMibEntry };

tMbDbEntry AlarmTableMibEntry[]= {

{{11,AlarmIndex}, GetNextIndexAlarmTable, AlarmIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmInterval}, GetNextIndexAlarmTable, AlarmIntervalGet, AlarmIntervalSet, AlarmIntervalTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmVariable}, GetNextIndexAlarmTable, AlarmVariableGet, AlarmVariableSet, AlarmVariableTest, AlarmTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmSampleType}, GetNextIndexAlarmTable, AlarmSampleTypeGet, AlarmSampleTypeSet, AlarmSampleTypeTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmValue}, GetNextIndexAlarmTable, AlarmValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmStartupAlarm}, GetNextIndexAlarmTable, AlarmStartupAlarmGet, AlarmStartupAlarmSet, AlarmStartupAlarmTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmRisingThreshold}, GetNextIndexAlarmTable, AlarmRisingThresholdGet, AlarmRisingThresholdSet, AlarmRisingThresholdTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmFallingThreshold}, GetNextIndexAlarmTable, AlarmFallingThresholdGet, AlarmFallingThresholdSet, AlarmFallingThresholdTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmRisingEventIndex}, GetNextIndexAlarmTable, AlarmRisingEventIndexGet, AlarmRisingEventIndexSet, AlarmRisingEventIndexTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmFallingEventIndex}, GetNextIndexAlarmTable, AlarmFallingEventIndexGet, AlarmFallingEventIndexSet, AlarmFallingEventIndexTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmOwner}, GetNextIndexAlarmTable, AlarmOwnerGet, AlarmOwnerSet, AlarmOwnerTest, AlarmTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 0, NULL},

{{11,AlarmStatus}, GetNextIndexAlarmTable, AlarmStatusGet, AlarmStatusSet, AlarmStatusTest, AlarmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, AlarmTableINDEX, 1, 0, 1, NULL},
};
tMibData AlarmTableEntry = { 12, AlarmTableMibEntry };

tMbDbEntry HostControlTableMibEntry[]= {

{{11,HostControlIndex}, GetNextIndexHostControlTable, HostControlIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostControlTableINDEX, 1, 0, 0, NULL},

{{11,HostControlDataSource}, GetNextIndexHostControlTable, HostControlDataSourceGet, HostControlDataSourceSet, HostControlDataSourceTest, HostControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, HostControlTableINDEX, 1, 0, 0, NULL},

{{11,HostControlTableSize}, GetNextIndexHostControlTable, HostControlTableSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostControlTableINDEX, 1, 0, 0, NULL},

{{11,HostControlLastDeleteTime}, GetNextIndexHostControlTable, HostControlLastDeleteTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, HostControlTableINDEX, 1, 0, 0, NULL},

{{11,HostControlOwner}, GetNextIndexHostControlTable, HostControlOwnerGet, HostControlOwnerSet, HostControlOwnerTest, HostControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, HostControlTableINDEX, 1, 0, 0, NULL},

{{11,HostControlStatus}, GetNextIndexHostControlTable, HostControlStatusGet, HostControlStatusSet, HostControlStatusTest, HostControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, HostControlTableINDEX, 1, 0, 1, NULL},
};
tMibData HostControlTableEntry = { 6, HostControlTableMibEntry };

tMbDbEntry HostTableMibEntry[]= {

{{11,HostAddress}, GetNextIndexHostTable, HostAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostCreationOrder}, GetNextIndexHostTable, HostCreationOrderGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostIndex}, GetNextIndexHostTable, HostIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostInPkts}, GetNextIndexHostTable, HostInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostOutPkts}, GetNextIndexHostTable, HostOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostInOctets}, GetNextIndexHostTable, HostInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostOutOctets}, GetNextIndexHostTable, HostOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostOutErrors}, GetNextIndexHostTable, HostOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostOutBroadcastPkts}, GetNextIndexHostTable, HostOutBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},

{{11,HostOutMulticastPkts}, GetNextIndexHostTable, HostOutMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTableINDEX, 2, 0, 0, NULL},
};
tMibData HostTableEntry = { 10, HostTableMibEntry };

tMbDbEntry HostTimeTableMibEntry[]= {

{{11,HostTimeAddress}, GetNextIndexHostTimeTable, HostTimeAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeCreationOrder}, GetNextIndexHostTimeTable, HostTimeCreationOrderGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeIndex}, GetNextIndexHostTimeTable, HostTimeIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeInPkts}, GetNextIndexHostTimeTable, HostTimeInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeOutPkts}, GetNextIndexHostTimeTable, HostTimeOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeInOctets}, GetNextIndexHostTimeTable, HostTimeInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeOutOctets}, GetNextIndexHostTimeTable, HostTimeOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeOutErrors}, GetNextIndexHostTimeTable, HostTimeOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeOutBroadcastPkts}, GetNextIndexHostTimeTable, HostTimeOutBroadcastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},

{{11,HostTimeOutMulticastPkts}, GetNextIndexHostTimeTable, HostTimeOutMulticastPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, HostTimeTableINDEX, 2, 0, 0, NULL},
};
tMibData HostTimeTableEntry = { 10, HostTimeTableMibEntry };

tMbDbEntry HostTopNControlTableMibEntry[]= {

{{11,HostTopNControlIndex}, GetNextIndexHostTopNControlTable, HostTopNControlIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,HostTopNHostIndex}, GetNextIndexHostTopNControlTable, HostTopNHostIndexGet, HostTopNHostIndexSet, HostTopNHostIndexTest, HostTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HostTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,HostTopNRateBase}, GetNextIndexHostTopNControlTable, HostTopNRateBaseGet, HostTopNRateBaseSet, HostTopNRateBaseTest, HostTopNControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, HostTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,HostTopNTimeRemaining}, GetNextIndexHostTopNControlTable, HostTopNTimeRemainingGet, HostTopNTimeRemainingSet, HostTopNTimeRemainingTest, HostTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HostTopNControlTableINDEX, 1, 0, 0, "0"},

{{11,HostTopNDuration}, GetNextIndexHostTopNControlTable, HostTopNDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTopNControlTableINDEX, 1, 0, 0, "0"},

{{11,HostTopNRequestedSize}, GetNextIndexHostTopNControlTable, HostTopNRequestedSizeGet, HostTopNRequestedSizeSet, HostTopNRequestedSizeTest, HostTopNControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, HostTopNControlTableINDEX, 1, 0, 0, "10"},

{{11,HostTopNGrantedSize}, GetNextIndexHostTopNControlTable, HostTopNGrantedSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,HostTopNStartTime}, GetNextIndexHostTopNControlTable, HostTopNStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, HostTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,HostTopNOwner}, GetNextIndexHostTopNControlTable, HostTopNOwnerGet, HostTopNOwnerSet, HostTopNOwnerTest, HostTopNControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, HostTopNControlTableINDEX, 1, 0, 0, NULL},

{{11,HostTopNStatus}, GetNextIndexHostTopNControlTable, HostTopNStatusGet, HostTopNStatusSet, HostTopNStatusTest, HostTopNControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, HostTopNControlTableINDEX, 1, 0, 1, NULL},
};
tMibData HostTopNControlTableEntry = { 10, HostTopNControlTableMibEntry };

tMbDbEntry HostTopNTableMibEntry[]= {

{{11,HostTopNReport}, GetNextIndexHostTopNTable, HostTopNReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTopNTableINDEX, 2, 0, 0, NULL},

{{11,HostTopNIndex}, GetNextIndexHostTopNTable, HostTopNIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTopNTableINDEX, 2, 0, 0, NULL},

{{11,HostTopNAddress}, GetNextIndexHostTopNTable, HostTopNAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, HostTopNTableINDEX, 2, 0, 0, NULL},

{{11,HostTopNRate}, GetNextIndexHostTopNTable, HostTopNRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, HostTopNTableINDEX, 2, 0, 0, NULL},
};
tMibData HostTopNTableEntry = { 4, HostTopNTableMibEntry };

tMbDbEntry MatrixControlTableMibEntry[]= {

{{11,MatrixControlIndex}, GetNextIndexMatrixControlTable, MatrixControlIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,MatrixControlDataSource}, GetNextIndexMatrixControlTable, MatrixControlDataSourceGet, MatrixControlDataSourceSet, MatrixControlDataSourceTest, MatrixControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,MatrixControlTableSize}, GetNextIndexMatrixControlTable, MatrixControlTableSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,MatrixControlLastDeleteTime}, GetNextIndexMatrixControlTable, MatrixControlLastDeleteTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,MatrixControlOwner}, GetNextIndexMatrixControlTable, MatrixControlOwnerGet, MatrixControlOwnerSet, MatrixControlOwnerTest, MatrixControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MatrixControlTableINDEX, 1, 0, 0, NULL},

{{11,MatrixControlStatus}, GetNextIndexMatrixControlTable, MatrixControlStatusGet, MatrixControlStatusSet, MatrixControlStatusTest, MatrixControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MatrixControlTableINDEX, 1, 0, 1, NULL},
};
tMibData MatrixControlTableEntry = { 6, MatrixControlTableMibEntry };

tMbDbEntry MatrixSDTableMibEntry[]= {

{{11,MatrixSDSourceAddress}, GetNextIndexMatrixSDTable, MatrixSDSourceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MatrixSDTableINDEX, 3, 0, 0, NULL},

{{11,MatrixSDDestAddress}, GetNextIndexMatrixSDTable, MatrixSDDestAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MatrixSDTableINDEX, 3, 0, 0, NULL},

{{11,MatrixSDIndex}, GetNextIndexMatrixSDTable, MatrixSDIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MatrixSDTableINDEX, 3, 0, 0, NULL},

{{11,MatrixSDPkts}, GetNextIndexMatrixSDTable, MatrixSDPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixSDTableINDEX, 3, 0, 0, NULL},

{{11,MatrixSDOctets}, GetNextIndexMatrixSDTable, MatrixSDOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixSDTableINDEX, 3, 0, 0, NULL},

{{11,MatrixSDErrors}, GetNextIndexMatrixSDTable, MatrixSDErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixSDTableINDEX, 3, 0, 0, NULL},
};
tMibData MatrixSDTableEntry = { 6, MatrixSDTableMibEntry };

tMbDbEntry MatrixDSTableMibEntry[]= {

{{11,MatrixDSSourceAddress}, GetNextIndexMatrixDSTable, MatrixDSSourceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MatrixDSTableINDEX, 3, 0, 0, NULL},

{{11,MatrixDSDestAddress}, GetNextIndexMatrixDSTable, MatrixDSDestAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MatrixDSTableINDEX, 3, 0, 0, NULL},

{{11,MatrixDSIndex}, GetNextIndexMatrixDSTable, MatrixDSIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MatrixDSTableINDEX, 3, 0, 0, NULL},

{{11,MatrixDSPkts}, GetNextIndexMatrixDSTable, MatrixDSPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixDSTableINDEX, 3, 0, 0, NULL},

{{11,MatrixDSOctets}, GetNextIndexMatrixDSTable, MatrixDSOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixDSTableINDEX, 3, 0, 0, NULL},

{{11,MatrixDSErrors}, GetNextIndexMatrixDSTable, MatrixDSErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MatrixDSTableINDEX, 3, 0, 0, NULL},
};
tMibData MatrixDSTableEntry = { 6, MatrixDSTableMibEntry };

tMbDbEntry FilterTableMibEntry[]= {

{{11,FilterIndex}, GetNextIndexFilterTable, FilterIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterChannelIndex}, GetNextIndexFilterTable, FilterChannelIndexGet, FilterChannelIndexSet, FilterChannelIndexTest, FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterPktDataOffset}, GetNextIndexFilterTable, FilterPktDataOffsetGet, FilterPktDataOffsetSet, FilterPktDataOffsetTest, FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, "0"},

{{11,FilterPktData}, GetNextIndexFilterTable, FilterPktDataGet, FilterPktDataSet, FilterPktDataTest, FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterPktDataMask}, GetNextIndexFilterTable, FilterPktDataMaskGet, FilterPktDataMaskSet, FilterPktDataMaskTest, FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterPktDataNotMask}, GetNextIndexFilterTable, FilterPktDataNotMaskGet, FilterPktDataNotMaskSet, FilterPktDataNotMaskTest, FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterPktStatus}, GetNextIndexFilterTable, FilterPktStatusGet, FilterPktStatusSet, FilterPktStatusTest, FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterPktStatusMask}, GetNextIndexFilterTable, FilterPktStatusMaskGet, FilterPktStatusMaskSet, FilterPktStatusMaskTest, FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterPktStatusNotMask}, GetNextIndexFilterTable, FilterPktStatusNotMaskGet, FilterPktStatusNotMaskSet, FilterPktStatusNotMaskTest, FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterOwner}, GetNextIndexFilterTable, FilterOwnerGet, FilterOwnerSet, FilterOwnerTest, FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FilterTableINDEX, 1, 0, 0, NULL},

{{11,FilterStatus}, GetNextIndexFilterTable, FilterStatusGet, FilterStatusSet, FilterStatusTest, FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FilterTableINDEX, 1, 0, 1, NULL},
};
tMibData FilterTableEntry = { 11, FilterTableMibEntry };

tMbDbEntry ChannelTableMibEntry[]= {

{{11,ChannelIndex}, GetNextIndexChannelTable, ChannelIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelIfIndex}, GetNextIndexChannelTable, ChannelIfIndexGet, ChannelIfIndexSet, ChannelIfIndexTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelAcceptType}, GetNextIndexChannelTable, ChannelAcceptTypeGet, ChannelAcceptTypeSet, ChannelAcceptTypeTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelDataControl}, GetNextIndexChannelTable, ChannelDataControlGet, ChannelDataControlSet, ChannelDataControlTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, "2"},

{{11,ChannelTurnOnEventIndex}, GetNextIndexChannelTable, ChannelTurnOnEventIndexGet, ChannelTurnOnEventIndexSet, ChannelTurnOnEventIndexTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelTurnOffEventIndex}, GetNextIndexChannelTable, ChannelTurnOffEventIndexGet, ChannelTurnOffEventIndexSet, ChannelTurnOffEventIndexTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelEventIndex}, GetNextIndexChannelTable, ChannelEventIndexGet, ChannelEventIndexSet, ChannelEventIndexTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelEventStatus}, GetNextIndexChannelTable, ChannelEventStatusGet, ChannelEventStatusSet, ChannelEventStatusTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, "1"},

{{11,ChannelMatches}, GetNextIndexChannelTable, ChannelMatchesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelDescription}, GetNextIndexChannelTable, ChannelDescriptionGet, ChannelDescriptionSet, ChannelDescriptionTest, ChannelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelOwner}, GetNextIndexChannelTable, ChannelOwnerGet, ChannelOwnerSet, ChannelOwnerTest, ChannelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 0, NULL},

{{11,ChannelStatus}, GetNextIndexChannelTable, ChannelStatusGet, ChannelStatusSet, ChannelStatusTest, ChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, ChannelTableINDEX, 1, 0, 1, NULL},
};
tMibData ChannelTableEntry = { 12, ChannelTableMibEntry };

tMbDbEntry BufferControlTableMibEntry[]= {

{{11,BufferControlIndex}, GetNextIndexBufferControlTable, BufferControlIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlChannelIndex}, GetNextIndexBufferControlTable, BufferControlChannelIndexGet, BufferControlChannelIndexSet, BufferControlChannelIndexTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlFullStatus}, GetNextIndexBufferControlTable, BufferControlFullStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlFullAction}, GetNextIndexBufferControlTable, BufferControlFullActionGet, BufferControlFullActionSet, BufferControlFullActionTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlCaptureSliceSize}, GetNextIndexBufferControlTable, BufferControlCaptureSliceSizeGet, BufferControlCaptureSliceSizeSet, BufferControlCaptureSliceSizeTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, "100"},

{{11,BufferControlDownloadSliceSize}, GetNextIndexBufferControlTable, BufferControlDownloadSliceSizeGet, BufferControlDownloadSliceSizeSet, BufferControlDownloadSliceSizeTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, "100"},

{{11,BufferControlDownloadOffset}, GetNextIndexBufferControlTable, BufferControlDownloadOffsetGet, BufferControlDownloadOffsetSet, BufferControlDownloadOffsetTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, "0"},

{{11,BufferControlMaxOctetsRequested}, GetNextIndexBufferControlTable, BufferControlMaxOctetsRequestedGet, BufferControlMaxOctetsRequestedSet, BufferControlMaxOctetsRequestedTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, "-1"},

{{11,BufferControlMaxOctetsGranted}, GetNextIndexBufferControlTable, BufferControlMaxOctetsGrantedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlCapturedPackets}, GetNextIndexBufferControlTable, BufferControlCapturedPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlTurnOnTime}, GetNextIndexBufferControlTable, BufferControlTurnOnTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlOwner}, GetNextIndexBufferControlTable, BufferControlOwnerGet, BufferControlOwnerSet, BufferControlOwnerTest, BufferControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 0, NULL},

{{11,BufferControlStatus}, GetNextIndexBufferControlTable, BufferControlStatusGet, BufferControlStatusSet, BufferControlStatusTest, BufferControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BufferControlTableINDEX, 1, 0, 1, NULL},
};
tMibData BufferControlTableEntry = { 13, BufferControlTableMibEntry };

tMbDbEntry CaptureBufferTableMibEntry[]= {

{{11,CaptureBufferControlIndex}, GetNextIndexCaptureBufferTable, CaptureBufferControlIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},

{{11,CaptureBufferIndex}, GetNextIndexCaptureBufferTable, CaptureBufferIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},

{{11,CaptureBufferPacketID}, GetNextIndexCaptureBufferTable, CaptureBufferPacketIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},

{{11,CaptureBufferPacketData}, GetNextIndexCaptureBufferTable, CaptureBufferPacketDataGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},

{{11,CaptureBufferPacketLength}, GetNextIndexCaptureBufferTable, CaptureBufferPacketLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},

{{11,CaptureBufferPacketTime}, GetNextIndexCaptureBufferTable, CaptureBufferPacketTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},

{{11,CaptureBufferPacketStatus}, GetNextIndexCaptureBufferTable, CaptureBufferPacketStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, CaptureBufferTableINDEX, 2, 0, 0, NULL},
};
tMibData CaptureBufferTableEntry = { 7, CaptureBufferTableMibEntry };

tMbDbEntry EventTableMibEntry[]= {

{{11,EventIndex}, GetNextIndexEventTable, EventIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EventTableINDEX, 1, 0, 0, NULL},

{{11,EventDescription}, GetNextIndexEventTable, EventDescriptionGet, EventDescriptionSet, EventDescriptionTest, EventTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EventTableINDEX, 1, 0, 0, NULL},

{{11,EventType}, GetNextIndexEventTable, EventTypeGet, EventTypeSet, EventTypeTest, EventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, EventTableINDEX, 1, 0, 0, NULL},

{{11,EventCommunity}, GetNextIndexEventTable, EventCommunityGet, EventCommunitySet, EventCommunityTest, EventTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EventTableINDEX, 1, 0, 0, NULL},

{{11,EventLastTimeSent}, GetNextIndexEventTable, EventLastTimeSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, EventTableINDEX, 1, 0, 0, NULL},

{{11,EventOwner}, GetNextIndexEventTable, EventOwnerGet, EventOwnerSet, EventOwnerTest, EventTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EventTableINDEX, 1, 0, 0, NULL},

{{11,EventStatus}, GetNextIndexEventTable, EventStatusGet, EventStatusSet, EventStatusTest, EventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, EventTableINDEX, 1, 0, 1, NULL},
};
tMibData EventTableEntry = { 7, EventTableMibEntry };

tMbDbEntry LogTableMibEntry[]= {

{{11,LogEventIndex}, GetNextIndexLogTable, LogEventIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LogTableINDEX, 2, 0, 0, NULL},

{{11,LogIndex}, GetNextIndexLogTable, LogIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, LogTableINDEX, 2, 0, 0, NULL},

{{11,LogTime}, GetNextIndexLogTable, LogTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, LogTableINDEX, 2, 0, 0, NULL},

{{11,LogDescription}, GetNextIndexLogTable, LogDescriptionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LogTableINDEX, 2, 0, 0, NULL},
};
tMibData LogTableEntry = { 4, LogTableMibEntry };

#endif /* _STDRMODB_H */

