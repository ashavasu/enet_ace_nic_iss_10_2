#ifndef RMONTDFS_H
#define RMONTDFS_H
/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmontdfs.h,v 1.32 2017/01/17 14:10:29 siva Exp $            *
 *                                                                  *
 * Description: Contains type definitions                           *
 *                                                                  *
 *******************************************************************/

/* Definition for RMON Enable Status */
#define RMON_ENABLE 1
#define RMON_DISABLE 2
#define RMON_SYS_START 1
#define RMON_SYS_SHUTDOWN 2


#define   RMON_EQUAL             0
#define   RMON_LESSER           (-1)
#define   RMON_GREATER           1

#define RMON_FALSE 0
#define RMON_TRUE  1

#define RMON_FREE_OID(oid) rmon_free_oid(oid)

#define RMON_FREE_OCTET_STRING(octet_str) free_octetstring(octet_str) 

typedef enum {
    RMON_NONE = 1,
    LOG = 2,
    SNMP_TRAP = 3,
    LOG_AND_TRAP = 4
} tEventType;

typedef enum {
    ABSOLUTE_VALUE = 1,
    DELTA_VALUE = 2
} tAlarmSample;

typedef enum  {
    hostTopNInPkts=1,
    hostTopNOutPkts = 2,
    hostTopNInOctets = 3,
    hostTopNOutOctets = 4,
    hostTopNOutErrors = 5,
    hostTopNOutBroadcastPkts = 6,
    hostTopNOutMulticastPkts = 7
}tHostTopnRateBase;


typedef struct EthFrameType {
   unsigned char destAddr[6];
   unsigned char srcAddr[6];
   unsigned short protType;
   unsigned char data[1800];
   UINT2  u2PadBits; 
}tEthFrame;


typedef struct LogEntry {
    tTimeTicks               logTime;
    UINT1                    u1LogDescription [LOG_DESCR_LEN];
} tLogEntry;

typedef struct RmonEtherStatsTimerNode {
  
    tRmonHashNode   nextEtherStatsNode; 
    tTmrAppTimer    etherTableTimer;
    tRmonEtherStatsNode  RmonEtherStatsPkts;
    UINT1           u1EtherStatsOwner[OWN_STR_LENGTH];
    UINT4           u4EtherStatsDataSource;
    UINT4           u4EtherStatsDroppedFrames;
    UINT4           u4EtherStatsCreateTime;
    INT4            i4EtherStatsIndex;
    UINT4           u4EtherStatsOIDType; /* Type of EthStats OID can be port */
                                         /* or VLAN, PORT_INDEX_TYPE(1)  */
                                         /* VLAN_INDEX_TYPE(2) */      
    tRmonStatus     etherStatsStatus;

}tRmonEtherStatsTimerNode;

typedef struct EtherHistorySample {
    UINT4                    u4EtherHistoryDropEvents;
    UINT4                    u4EtherHistoryOctets;
    UINT4                    u4EtherHistoryPkts;
    UINT4                    u4EtherHistoryBroadcastPkts;
    UINT4                    u4EtherHistoryMulticastPkts;
    UINT4                    u4EtherHistoryCRCAlignErrors;
    UINT4                    u4EtherHistoryUndersizePkts;
    UINT4                    u4EtherHistoryOversizePkts;
    UINT4                    u4EtherHistoryFragments;
    UINT4                    u4EtherHistoryJabbers;
    UINT4                    u4EtherHistoryCollisions;
    INT4                     i4EtherHistoryUtilization;
    INT4                     i4SampleIndex;
    tTimeTicks               etherHistoryIntervalStart;
    UINT4                    u4EtherHistoryHighCapacityOverflowPkts;
    tSNMP_COUNTER64_TYPE     u8EtherHistoryHighCapacityPkts;
    UINT4                    u4EtherHistoryHighCapacityOverflowOctets;
    tSNMP_COUNTER64_TYPE     u8EtherHistoryHighCapacityOctets;
} tEtherHistorySample;

typedef struct EtherHistoryEntry {
    INT4                    i4BucketsAllocated;
    INT4                    i4BucketToBeFilled;
    tEtherHistorySample     *pHistoryBucket; /* This pointer is to be allocated
                                               * memory in the init routine */
} tEtherHistoryEntry;

typedef struct LogEvent {
    INT4                    i4BucketsAllocated;   /* The Maximum number of 
                                                   * buckets that can be 
                                                   * allocated is fixed to 
                                                   * MAX_LOG_BUCKETS  */
    INT4                    i4BucketToBeFilled;
    INT4                    i4BucketsFilled;      /* No: of buckets filled*/
    tLogEntry               *pLogBucket;          /* This pointer is to be 
                                                   * allocated memory in
                                                   * some init routine  */
} tLogEvent;


typedef struct RmonHistoryControlNode {
    tRmonHashNode           nextHistoryControlEntryNode;
    tTmrAppTimer            EtherHistoryTimer;
    tEtherHistoryEntry      *pHistoryEntry;
    INT4                    i4HistoryControlBucketsRequested;
    INT4                    i4HistoryControlBucketsGranted;
    INT4                    i4HistoryControlInterval;
    INT4                    i4HistoryControlIndex;
    UINT4                   u4HistoryControlDataSource; 
    UINT4                   u4HistoryControlDroppedFrames;
    UINT4                   u4HistoryMinSampleIndex; 
    UINT1                   u1HistoryControlOwner[OWN_STR_LENGTH];
    tRmonStatus             historyControlStatus;
    UINT4                   u4HisCntrlOIDType; /* Type of EthStats OID can be port */
                                               /* or VLAN, PORT_INDEX_TYPE(1)  */
                                               /* VLAN_INDEX_TYPE(2) */      
    UINT2                   u2TimeToMonitor;/* Mirror variable of 
                                           * u2HistoryControlInterval */
    UINT2                   u2HistoryPad; 
}tRmonHistoryControlNode;

typedef struct RmonAlarmNode {
    tRmonHashNode            nextAlarmEntryNode;
    tTmrAppTimer             AlarmTableTimer;
    UINT4                    u4AlarmValue;
    UINT4                    u4PreviousValue; 
                             /* Variable used to find the delta value */
    UINT4                    u4PrevIntervalValue;
    UINT4                    u4AlarmRisingThreshold; 
    UINT4                    u4AlarmFallingThreshold;
    tSNMP_OID_TYPE           alarmVariable;
    INT4                     i4AlarmInterval;
    INT4                     i4TimeToMonitor;   
                             /* Mirror variable of i4AlarmInterval */
    INT4                     i4AlarmRisingEventIndex;
    INT4                     i4AlarmFallingEventIndex;
    UINT1                    u1AlarmOwner[OWN_STR_LENGTH];
    INT4                     i4AlarmStartupAlarm;
    tAlarmSample             alarmSampleType;
    tRmonStatus              alarmStatus;
    INT4                     i4AlarmIndex;
    UINT4                    u4IsAlarmOnce; /* This flag indicates alarm is
                                             * generated atleast once or not, this
                                             * is used in case of absolute alarm
                                             */
    UINT1                    u1FirstAlarm;  /* This flag indicates whether this 
                                             * is the first alarm or not. 
                                             * By default the value will be 
                                             * TRUE and once the first alarm is 
                                             * raised it will change to FALSE.*/
    UINT1                    au1Pad[3];

} tRmonAlarmNode;

typedef struct RmonEventEntry {
    tRmonHashNode            nextEventNode;
    tTmrAppTimer             EventTableTimer;
    tTimeTicks               eventLastTimeSent;
    UINT1                    u1EventDescription[EVENT_DESCR_LEN];
    tEventType               eventType;               
    tSNMP_OCTET_STRING_TYPE  EventCommunity;
    UINT1                    u1EventOwner[OWN_STR_LENGTH];
    tRmonStatus              eventStatus;
    INT4                     i4EventIndex;
    tLogEvent                *pLogEvent;
} tRmonEventNode;

typedef struct  HostTableEntry {
    UINT4                    u4HostInPkts;
    UINT4                    u4HostOutPkts;
    UINT4                    u4HostInOctets;
    UINT4                    u4HostOutOctets;
    UINT4                    u4HostOutErrors;
    UINT4                    u4HostOutBroadcastPkts;
    UINT4                    u4HostOutMulticastPkts;
    UINT4                    u4LastAccess;
    UINT4                    u4CreationTime;
    INT4                     i4HostCreationOrder;
    tMacAddr                 u1HostAddress;
    UINT2                    u2HostPad; 
}tHostTableEntry;

/* 
 * u2HostControlTableSize is declared as UINT2 so restricting 
 * no of hosts to 65535. 
 */
typedef struct  HostControlEntry {
    tTmrAppTimer             hostControlTimer; 
    UINT1                    u1HostControlOwner[OWN_STR_LENGTH];
    UINT4                    u4HostControlDataSource;
    UINT4                    u4HostControlDroppedFrames;
    UINT4                    u4HostControlCreateTime;
    tTimeTicks               hostControlLastDeleteTime;
    tHostTableEntry          *pHostTable;
    tRmonStatus              hostControlStatus;
    UINT2                    u2HostControlTableSize;
    UINT1                    au1Reserved[2];
}tHostControl;

typedef struct SDTableEntry {
    UINT4                    u4MatrixSDPkts;
    UINT4                    u4MatrixSDOctets;
    UINT4                    u4MatrixSDErrors;
    tTimeTicks               lastAccessTime;
    tMacAddr                 matrixSDSourceAddress;
    tMacAddr                 matrixSDDestAddress;
}tSdTableEntry;

typedef struct MatrixControlEntry {
    tTmrAppTimer             MatrixControlTimer;
    UINT4                    u4MatrixControlTableSize;
    tTimeTicks               matrixControlLastDeleteTime;
    tSdTableEntry            *pMatrixSDTable;
    UINT1                    u1MatrixControlOwner[OWN_STR_LENGTH];
    UINT4                    u4MatrixControlDataSource;
    UINT4                    u4MatrixControlDroppedFrames;
    UINT4                    u4MatrixControlCreateTime;
    tRmonStatus              matrixControlStatus;
}tMatrixControlEntry;

typedef struct  HostTopNEntry {
    UINT4                    u4HostTopNRate;
    tMacAddr                 u1HostTopNAddress;
    UINT2                    u2TopNPad; 
}tHostTopn;


typedef struct  TopNSampleEntry {
    UINT4                    u4HostTopNRate;
    UINT4                    u4CreationTime;
    tMacAddr                 u1HostTopNAddress;
    UINT2                    u2TopNEntryPad; 
}tTopnSampleEntry;

typedef struct  TopNSample{
    tTopnSampleEntry         Sample[MAX_TOPN_HOSTS];
    UINT2                    u2NoOfHostInThisSample;
    UINT2                    u2TopNSamplePad;
}tTopnSample;

typedef struct  HostTopNControlEntry {
    tTmrAppTimer             HostTopNControlTimer;
    UINT4                    u4HostTopNTimeRemaining;
    UINT4                    u4HostTopNDuration;
    INT4                     i4HostTopNRequestedSize;
    INT4                     i4HostTopNGrantedSize;
    tTopnSample              FirstSample;
    tHostTopn                *pTopNTable;
    tTimeTicks               u4hostTopNStartTime;
    INT4                     i4HostTopNHostIndex;
    tHostTopnRateBase        hostTopNRateBase;
    UINT1                    u1HostTopNOwner[OWN_STR_LENGTH];
    tRmonStatus              hostTopNStatus;
}tHostTopnControl;

typedef struct  TimerReference {
    UINT1                    u1TableType;
    UINT1                    u1ModuleId;
    UINT2                    u2Index;
}tTimerReference;

/* For converting Static Tables to Dynamic allocations. */

typedef struct HostTopNControlTable {
              tHostTopnControl *pTopNControlEntry;
} tHostTopnCtrlTable;

typedef struct MatrixControlTable {
              tMatrixControlEntry *pMatrixControlEntry;
} tMatrixControlTable;

typedef struct HostControlTable {
         tHostControl *pHostControlEntry;
} tHostControlTable;

/* Data structure to define the RMON Supported Groups */
typedef struct {
    UINT1 u1RmonHwStatsSuppFlag;
    UINT1 u1RmonHwHistorySuppFlag;
    UINT1 u1RmonHwAlarmSuppFlag;
    UINT1 u1RmonHwEventSuppFlag;
    UINT1 u1RmonHwHostSuppFlag;
    UINT1 u1RmonHwHostTopNSuppFlag;
    UINT1 u1RmonHwMatrixSuppFlag;
    UINT1 u1Reserved;
} tRmonSupportGroups;


/* Event Community Mem-Block */
typedef struct _tRmonEventCommBlock
{
    UINT1 au1EventCommBlock[COMM_LENGTH];
}tRmonEventCommBlock;


/* Log Bucket Mem-Block */
typedef struct _tRmonLogBucketBlock
{
    tLogEntry  LogEntryBlock[MAX_LOG_BUCKETS + 1];
}tRmonLogBucketBlock;


/* History Buckets Mem-Block */
typedef struct _tRmonHisBuckets
{
    tEtherHistorySample EtherHistorySample[MAX_HISTORY_BUCKETS + 1];
}tRmonHisBuckets;

/* Host Table Mem-Block */
typedef struct _tRmonHostTableBlock
{
    tHostTableEntry HostTableEntry[MAX_HOST_PER_INTERFACE];
}tRmonHostTableBlock;


/* Matrix Buckets Mem-Block */
typedef struct _tMatrixBucketsBlock
{
    tSdTableEntry MatrixBucketsBlock[MAX_MATRIX_ENTRY];
}tMatrixBucketsBlock;

/* TopNReport Mem-Block */
typedef struct _tTopNReportBlock 
{
    tHostTopn TopNReportBlock[MAX_HOST_PER_INTERFACE];
}tTopNReportBlock;

typedef struct _tRmonQMsg
{
   UINT4       u4IfIndex;  /* Interface Index */
   tVlanId     VlanId;     /* VlanId */
   UINT2       u2L2CxtId;  /* Context Identifier*/
   UINT2       u2Command;
   UINT1       au1pad[2];
}tRmonQMsg;

#endif
