/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsrmonwr.h,v 1.7 2013/10/28 09:53:34 siva Exp $
 *
 * Description:  Protocol Mib Data base
 *********************************************************************/

#ifndef _FSRMONWRAP_H 
#define _FSRMONWRAP_H 
VOID RegisterFSRMON(VOID);
INT4 RmonDebugTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonDebugTypeSet (tSnmpIndex *, tRetVal *);
INT4 RmonDebugTypeGet (tSnmpIndex *, tRetVal *);
INT4 RmonDebugTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonEnableStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonEnableStatusSet (tSnmpIndex *, tRetVal *);
INT4 RmonEnableStatusGet (tSnmpIndex *, tRetVal *);
INT4 RmonEnableStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwStatsSuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwStatsSuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwStatsSuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwStatsSuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwHistorySuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwHistorySuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwHistorySuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwHistorySuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwAlarmSuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwAlarmSuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwAlarmSuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwAlarmSuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwHostSuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwHostSuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwHostSuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwHostSuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwHostTopNSuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwHostTopNSuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwHostTopNSuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwHostTopNSuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwMatrixSuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwMatrixSuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwMatrixSuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwMatrixSuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 RmonHwEventSuppTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 RmonHwEventSuppSet (tSnmpIndex *, tRetVal *);
INT4 RmonHwEventSuppGet (tSnmpIndex *, tRetVal *);
INT4 RmonHwEventSuppDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
/*  GetNext Function Prototypes */
INT4 GetNextIndexRmonStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 RmonStatsOutFCSErrorsGet(tSnmpIndex *, tRetVal *);
INT4 RmonStatsPkts1519to1522OctetsGet(tSnmpIndex *, tRetVal *);

#endif
