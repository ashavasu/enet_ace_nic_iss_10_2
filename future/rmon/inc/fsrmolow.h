/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmolow.h,v 1.8 2013/10/28 09:53:34 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRmonDebugType ARG_LIST((UINT4 *));

INT1
nmhGetRmonEnableStatus ARG_LIST((INT4 *));

INT1
nmhGetRmonHwStatsSupp ARG_LIST((INT4 *));

INT1
nmhGetRmonHwHistorySupp ARG_LIST((INT4 *));

INT1
nmhGetRmonHwAlarmSupp ARG_LIST((INT4 *));

INT1
nmhGetRmonHwHostSupp ARG_LIST((INT4 *));

INT1
nmhGetRmonHwHostTopNSupp ARG_LIST((INT4 *));

INT1
nmhGetRmonHwMatrixSupp ARG_LIST((INT4 *));

INT1
nmhGetRmonHwEventSupp ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetRmonDebugType ARG_LIST((UINT4 ));

INT1
nmhSetRmonEnableStatus ARG_LIST((INT4 ));

INT1
nmhSetRmonHwStatsSupp ARG_LIST((INT4 ));

INT1
nmhSetRmonHwHistorySupp ARG_LIST((INT4 ));

INT1
nmhSetRmonHwAlarmSupp ARG_LIST((INT4 ));

INT1
nmhSetRmonHwHostSupp ARG_LIST((INT4 ));

INT1
nmhSetRmonHwHostTopNSupp ARG_LIST((INT4 ));

INT1
nmhSetRmonHwMatrixSupp ARG_LIST((INT4 ));

INT1
nmhSetRmonHwEventSupp ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2RmonDebugType ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2RmonEnableStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwStatsSupp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwHistorySupp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwAlarmSupp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwHostSupp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwHostTopNSupp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwMatrixSupp ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2RmonHwEventSupp ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2RmonDebugType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonEnableStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwStatsSupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwHistorySupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwAlarmSupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwHostSupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwHostTopNSupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwMatrixSupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2RmonHwEventSupp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for RmonStatsTable. */
INT1
nmhValidateIndexInstanceRmonStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for RmonStatsTable  */

INT1
nmhGetFirstIndexRmonStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRmonStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRmonStatsOutFCSErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetRmonStatsPkts1519to1522Octets ARG_LIST((INT4 ,UINT4 *));
