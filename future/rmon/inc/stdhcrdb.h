/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdhcrdb.h,v 1.1 2014/12/31 12:18:14 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDHCRDB_H
#define _STDHCRDB_H

UINT1 MediaIndependentTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 EtherStatsHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 EtherHistoryHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 HostHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 HostTimeHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 HostTopNHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 MatrixSDHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MatrixDSHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 CaptureBufferHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 ProtocolDistStatsHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 NlHostHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NlMatrixSDHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NlMatrixDSHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 NlMatrixTopNHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlHostHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixSDHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixDSHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 AlMatrixTopNHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 UsrHistoryHighCapacityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdhcr [] ={1,3,6,1,2,1,16,20,5};
tSNMP_OID_TYPE stdhcrOID = {9, stdhcr};

UINT4 EtherStatsHighCapacityTable [] ={1,3,6,1,2,1,16,1,7};
tSNMP_OID_TYPE EtherStatsHighCapacityTableOID = {9, EtherStatsHighCapacityTable};

UINT4 EtherHistoryHighCapacityTable [] ={1,3,6,1,2,1,16,2,6};
tSNMP_OID_TYPE EtherHistoryHighCapacityTableOID = {9, EtherHistoryHighCapacityTable};


UINT4 EtherStatsHighCapacityOverflowPkts [ ] ={1,3,6,1,2,1,16,1,7,1,1};
UINT4 EtherStatsHighCapacityPkts [ ] ={1,3,6,1,2,1,16,1,7,1,2};
UINT4 EtherStatsHighCapacityOverflowOctets [ ] ={1,3,6,1,2,1,16,1,7,1,3};
UINT4 EtherStatsHighCapacityOctets [ ] ={1,3,6,1,2,1,16,1,7,1,4};
UINT4 EtherStatsHighCapacityOverflowPkts64Octets [ ] ={1,3,6,1,2,1,16,1,7,1,5};
UINT4 EtherStatsHighCapacityPkts64Octets [ ] ={1,3,6,1,2,1,16,1,7,1,6};
UINT4 EtherStatsHighCapacityOverflowPkts65to127Octets [ ] ={1,3,6,1,2,1,16,1,7,1,7};
UINT4 EtherStatsHighCapacityPkts65to127Octets [ ] ={1,3,6,1,2,1,16,1,7,1,8};
UINT4 EtherStatsHighCapacityOverflowPkts128to255Octets [ ] ={1,3,6,1,2,1,16,1,7,1,9};
UINT4 EtherStatsHighCapacityPkts128to255Octets [ ] ={1,3,6,1,2,1,16,1,7,1,10};
UINT4 EtherStatsHighCapacityOverflowPkts256to511Octets [ ] ={1,3,6,1,2,1,16,1,7,1,11};
UINT4 EtherStatsHighCapacityPkts256to511Octets [ ] ={1,3,6,1,2,1,16,1,7,1,12};
UINT4 EtherStatsHighCapacityOverflowPkts512to1023Octets [ ] ={1,3,6,1,2,1,16,1,7,1,13};
UINT4 EtherStatsHighCapacityPkts512to1023Octets [ ] ={1,3,6,1,2,1,16,1,7,1,14};
UINT4 EtherStatsHighCapacityOverflowPkts1024to1518Octets [ ] ={1,3,6,1,2,1,16,1,7,1,15};
UINT4 EtherStatsHighCapacityPkts1024to1518Octets [ ] ={1,3,6,1,2,1,16,1,7,1,16};
UINT4 EtherHistoryHighCapacityOverflowPkts [ ] ={1,3,6,1,2,1,16,2,6,1,1};
UINT4 EtherHistoryHighCapacityPkts [ ] ={1,3,6,1,2,1,16,2,6,1,2};
UINT4 EtherHistoryHighCapacityOverflowOctets [ ] ={1,3,6,1,2,1,16,2,6,1,3};
UINT4 EtherHistoryHighCapacityOctets [ ] ={1,3,6,1,2,1,16,2,6,1,4};




tMbDbEntry EtherStatsHighCapacityTableMibEntry[]= {

{{11,EtherStatsHighCapacityOverflowPkts}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowOctets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOctets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowPkts64Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPkts64OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts64Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPkts64OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowPkts65to127Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPkts65to127OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts65to127Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPkts65to127OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowPkts128to255Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPkts128to255OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts128to255Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPkts128to255OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowPkts256to511Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPkts256to511OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts256to511Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPkts256to511OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowPkts512to1023Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPkts512to1023OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts512to1023Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPkts512to1023OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityOverflowPkts1024to1518Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityOverflowPkts1024to1518OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},

{{11,EtherStatsHighCapacityPkts1024to1518Octets}, GetNextIndexEtherStatsHighCapacityTable, EtherStatsHighCapacityPkts1024to1518OctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherStatsHighCapacityTableINDEX, 1, 0, 0, NULL},
};
tMibData EtherStatsHighCapacityTableEntry = { 16, EtherStatsHighCapacityTableMibEntry };

tMbDbEntry EtherHistoryHighCapacityTableMibEntry[]= {

{{11,EtherHistoryHighCapacityOverflowPkts}, GetNextIndexEtherHistoryHighCapacityTable, EtherHistoryHighCapacityOverflowPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, EtherHistoryHighCapacityTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryHighCapacityPkts}, GetNextIndexEtherHistoryHighCapacityTable, EtherHistoryHighCapacityPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherHistoryHighCapacityTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryHighCapacityOverflowOctets}, GetNextIndexEtherHistoryHighCapacityTable, EtherHistoryHighCapacityOverflowOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, EtherHistoryHighCapacityTableINDEX, 2, 0, 0, NULL},

{{11,EtherHistoryHighCapacityOctets}, GetNextIndexEtherHistoryHighCapacityTable, EtherHistoryHighCapacityOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, EtherHistoryHighCapacityTableINDEX, 2, 0, 0, NULL},

};
tMibData EtherHistoryHighCapacityTableEntry = { 4, EtherHistoryHighCapacityTableMibEntry };

#endif /* _STDHCRDB_H */

