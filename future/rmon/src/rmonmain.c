/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmonmain.c,v 1.40 2017/01/17 14:10:30 siva Exp $        *
 *                                                                  *
 * Description:                                                     *    
 *        1) Creates the timer lists and RMON Task.                 *
 *           2) On failure to create the timer lists, deletes the   *
 *           already created resources.                             *     
 *        3) If the H/W compilation switch is enabled, probes       *
 *           and fetches counters periodically.                     *
 *        4) At the configured intervals, snapshot of               *
 *           aperiodic tables will be taken for the periodic        *
 *           groups.                                                *
 *                                                                  *
 *******************************************************************/

#define RMONMAIN_C
#include "rmallinc.h"
#include "fsrmoncli.h"
#include "stdrmocli.h"

#ifdef SNMP_2_WANTED
#include "stdrmowr.h"
#include "fsrmonwr.h"
#endif

tTmrAppTimer        gOneSecTimer;

/************************************************************************
 * Function            : SetInterfaceOid                                *  
 * Input (s)           : None                                           *    
 * Output (s)          : None                                           *
 * Returns             : None                                           *
 * Global Variables                                                     *
 *              Used   : grlInterfaceOid                                *
 * Side effects        : None                                           *
 * Action              : Sets the Interface Oid                         * 
 ************************************************************************
*/

INT4
SetInterfaceOid (void)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function  SetInterfaceOid \n");

    grlInterfaceOid.u4_Length = IFINDEX_OID_SIZE;
    grlInterfaceOid.pu4_OidList = &gau4rlInterfaceOidList[0];

    grlInterfaceOid.pu4_OidList[0] = 1;
    grlInterfaceOid.pu4_OidList[1] = 3;
    grlInterfaceOid.pu4_OidList[2] = 6;
    grlInterfaceOid.pu4_OidList[3] = 1;
    grlInterfaceOid.pu4_OidList[4] = 2;
    grlInterfaceOid.pu4_OidList[5] = 1;
    grlInterfaceOid.pu4_OidList[6] = 2;
    grlInterfaceOid.pu4_OidList[7] = 2;
    grlInterfaceOid.pu4_OidList[8] = 1;
    grlInterfaceOid.pu4_OidList[9] = 1;

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving function : SetInterfaceOid \n");

    return RMON_SUCCESS;
}

/************************************************************************
 * Function            : SetVlanIndexOid                                *  
 * Input (s)           : None                                           *    
 * Output (s)          : None                                           *
 * Returns             : None                                           *
 * Global Variables                                                     *
 *              Used   : grlVlanOid                                     *
 * Side effects        : None                                           *
 * Action              : Sets the Vlan Oid                              * 
 ************************************************************************
*/

INT4
SetVlanIndexOid (void)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function SetVlanIndexOid\n");

    grlVlanOid.u4_Length = VLAN_INDEX_OID_SIZE;
    grlVlanOid.pu4_OidList = &gau4rlVlanOidList[0];

    grlVlanOid.pu4_OidList[0] = 1;
    grlVlanOid.pu4_OidList[1] = 3;
    grlVlanOid.pu4_OidList[2] = 6;
    grlVlanOid.pu4_OidList[3] = 1;
    grlVlanOid.pu4_OidList[4] = 2;
    grlVlanOid.pu4_OidList[5] = 1;
    grlVlanOid.pu4_OidList[6] = 17;
    grlVlanOid.pu4_OidList[7] = 7;
    grlVlanOid.pu4_OidList[8] = 1;
    grlVlanOid.pu4_OidList[9] = 4;
    grlVlanOid.pu4_OidList[10] = 2;
    grlVlanOid.pu4_OidList[11] = 1;
    grlVlanOid.pu4_OidList[12] = 2;

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving function : SetVlanIndexOid\n");

    return RMON_SUCCESS;
}

/************************************************************************
 * Function            : RmonInvalidateCtrlTableEntries                 *  
 * Input (s)           : None                                           *    
 * Output (s)          : None                                           *
 * Returns             : None                                           *
 * Global Variables                                                     *
 *              Used   : None                                           *
 * Side effects        : None                                           *
 * Action              : This function frees the memory for both        *
 *              Control and Data tables.                                *
 ************************************************************************
 */

void                RmonInvalidateCtrlTableEntries
ARG_LIST ((void))
{
    INT4                i4Count;
    INT4                i4Status = RMON_INVALID;
    UINT4               u4HashIndex;
    tTMO_HASH_NODE     *pTempNodePtr2 = NULL, *pTmpNodePtr = NULL;
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonInvalidateCtrlTableEntries\n");

    /* Free the memory allocated for entries. This takes 
     * care of automatically deleting the associated Data Tables. */
    /* The Table index will start from 1 since these are the allocated ones */

    /* Ether History Control Table */
    TMO_HASH_Scan_Table (gpRmonHistoryControlHashTable, u4HashIndex)
    {
        for (pTmpNodePtr =
             TMO_HASH_FIRST_NODE (&gpRmonHistoryControlHashTable->
                                  HashList[u4HashIndex]); pTmpNodePtr;
             pTmpNodePtr = pTempNodePtr2)
        {
            pTempNodePtr2 =
                TMO_HASH_NEXT_NODE (&gpRmonHistoryControlHashTable->
                                    HashList[u4HashIndex], pTmpNodePtr);
            if (pTmpNodePtr != NULL)
            {
                if (nmhSetHistoryControlStatus (((tRmonHistoryControlNode *)
                                                 pTmpNodePtr)->
                                                i4HistoryControlIndex,
                                                i4Status) == SNMP_SUCCESS)
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              "History Control Entry Status set to INVALID\n");
                }
                else
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              "History Control Entry is NOT made INVALID \n");
                }
            }
        }
    }

    /* Alarm Table */
    TMO_HASH_Scan_Table (gpRmonAlarmHashTable, u4HashIndex)
    {
        for (pTmpNodePtr =
             TMO_HASH_FIRST_NODE (&gpRmonAlarmHashTable->HashList[u4HashIndex]);
             pTmpNodePtr; pTmpNodePtr = pTempNodePtr2)
        {
            pTempNodePtr2 =
                TMO_HASH_NEXT_NODE (&gpRmonAlarmHashTable->
                                    HashList[u4HashIndex], pTmpNodePtr);
            if (pTmpNodePtr != NULL)
            {
                if (nmhSetAlarmStatus (((tRmonAlarmNode *) pTmpNodePtr)->
                                       i4AlarmIndex, i4Status) == SNMP_SUCCESS)
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              "Alarm Entry Status set to INVALID\n");
                }
                else
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              " Alarm Entry is NOT made INVALID \n");
                }
            }
        }
    }

    /* HostTopN Control Table */
    for (i4Count = 1; i4Count < RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT; i4Count++)
    {
        if (gaHostTopNControlTable[i4Count].pTopNControlEntry != NULL)
        {
            if (nmhSetHostTopNStatus (i4Count, i4Status) == SNMP_SUCCESS)
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "HostTopN Control Entry Status set to INVALID\n");
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          " HostTopN Control Entry is NOT made INVALID \n");
            }
        }
    }

    /* Hosttopn is dependant on Host. Hence delete this after HostTopN table */
    /* Host Control Table */
    for (i4Count = 1; i4Count < RMON_MAX_INTERFACE_LIMIT; i4Count++)
    {
        if (gaHostControlTable[i4Count].pHostControlEntry != NULL)
        {
            if (nmhSetHostControlStatus (i4Count, i4Status) == SNMP_SUCCESS)
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Host Control Entry Status set to INVALID\n");
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Host Control Entry is NOT made INVALID \n");
            }
        }

    }
    /* Matrix Control Table */
    for (i4Count = 1; i4Count < RMON_MAX_INTERFACE_LIMIT; i4Count++)
    {
        if (gaMatrixControlTable[i4Count].pMatrixControlEntry != NULL)
        {
            if (nmhSetMatrixControlStatus (i4Count, i4Status) == SNMP_SUCCESS)
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Matrix Control Entry Status set to INVALID\n");
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Matrix Control Entry is NOT made INVALID \n");
            }
        }

    }

    /* Event Table */
    TMO_HASH_Scan_Table (gpRmonEventHashTable, u4HashIndex)
    {
        for (pTmpNodePtr =
             TMO_HASH_FIRST_NODE (&gpRmonEventHashTable->HashList[u4HashIndex]);
             pTmpNodePtr; pTmpNodePtr = pTempNodePtr2)
        {
            pTempNodePtr2 =
                TMO_HASH_NEXT_NODE (&gpRmonEventHashTable->
                                    HashList[u4HashIndex], pTmpNodePtr);
            if (pTmpNodePtr != NULL)
            {
                if (nmhSetEventStatus (((tRmonEventNode *) pTmpNodePtr)->
                                       i4EventIndex, i4Status) == SNMP_SUCCESS)
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              "Event Entry Status set to INVALID\n");
                }
                else
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              "Event Entry is NOT made INVALID \n");
                }
            }
        }
    }
    /* Etherstats deletion at the end since Alarm,
       History groups have dependancy. */
    /* Ether Statistics table */
    TMO_HASH_Scan_Table (gpRmonEtherStatsHashTable, u4HashIndex)
    {
        for (pTmpNodePtr =
             TMO_HASH_FIRST_NODE (&gpRmonEtherStatsHashTable->
                                  HashList[u4HashIndex]); pTmpNodePtr;
             pTmpNodePtr = pTempNodePtr2)
        {
            pTempNodePtr2 =
                TMO_HASH_NEXT_NODE (&gpRmonEtherStatsHashTable->
                                    HashList[u4HashIndex], pTmpNodePtr);
            if (pTmpNodePtr != NULL)
            {
                if (nmhSetEtherStatsStatus (((tRmonEtherStatsTimerNode *)
                                             pTmpNodePtr)->i4EtherStatsIndex,
                                            i4Status) == SNMP_SUCCESS)
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              "EtherStats Entry Status set to INVALID\n");
                }
                else
                {
                    RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                              " EtherStats Entry is NOT made INVALID \n");
                }
            }
        }
    }

    /* Delete the Ether Stats Entries that are under creation state */
    TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode, tRmonEtherStatsTimerNode *)
    {
        if (pEtherStatsNode != NULL)
        {
            if (nmhSetEtherStatsStatus (((tRmonEtherStatsTimerNode *)
                                         pEtherStatsNode)->i4EtherStatsIndex,
                                        i4Status) == SNMP_SUCCESS)
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "EtherStats Entry Status set to INVALID\n");
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          " EtherStats Entry is NOT made INVALID \n");
            }
        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : RmonInvalidateCtrlTableEntries\n");
}

/************************************************************************
 * Function            : RmonInitialize                                 *  
 * Input (s)           : None.                                          *    
 * Output (s)          : None.                                          *
 * Returns             : RMON_SUCCESS/RMON_FAILURE                      *
 * Global Variables                                                     *
 *              Used   : None                                           *
 * Side effects        : None                                           *
 * Action              : This function initialises all Satic Tables     *
 *              and Registers the MIBs with FutureSNMP.                 *
 ************************************************************************
 */

INT4
RmonInitialize (void)
{
    UINT4               u4Count = 0;
    /* Dummy pointers for system sizing during run time */
    tRmonEventCommBlock *pRmonEventCommBlock = NULL;
    tRmonLogBucketBlock *pRmonLogBucketBlock = NULL;
    tRmonHisBuckets    *pRmonHisBuckets = NULL;
    tRmonHostTableBlock *pRmonHostTableBlock = NULL;
    tMatrixBucketsBlock *pMatrixBucketsBlock = NULL;
    tTopNReportBlock   *pTopNReportBlock = NULL;

    UNUSED_PARAM (pRmonEventCommBlock);
    UNUSED_PARAM (pRmonLogBucketBlock);
    UNUSED_PARAM (pRmonHisBuckets);
    UNUSED_PARAM (pRmonHostTableBlock);
    UNUSED_PARAM (pMatrixBucketsBlock);
    UNUSED_PARAM (pTopNReportBlock);

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonInitialize\n");

    /* Initialize RmonEnableStatus to RMON_DISABLE */
    gRmonEnableStatusFlag = RMON_DISABLE;

    if (RMON_CREATE_SEM (RMON_SEM_NAME, RMON_SEM_COUNT,
                         RMON_SEM_FLAGS, &(RMON_SEM_ID)) != OSIX_SUCCESS)
    {
        return RMON_FAILURE;
    }

    /* Create Queue */
    if (RMON_CREATE_QUEUE (RMON_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                           RMON_Q_DEPTH, &(RMON_QUEUE_ID)) != OSIX_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | OS_RESOURCE_TRC,
                  "RMON: creation of RMON Queue failed. \n");
        return RMON_FAILURE;
    }

    /* Initialize the InterfaceOid */
    if (SetInterfaceOid () != RMON_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | OS_RESOURCE_TRC,
                  "SetInterfaceOid() failed. \n");
        return RMON_FAILURE;
    }

    /* Initialize the VlanOid */
    if (SetVlanIndexOid () != RMON_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | OS_RESOURCE_TRC,
                  "SetVlanIndexOid() failed. \n");
        return RMON_FAILURE;
    }

    if (RmonSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | OS_RESOURCE_TRC, "MemPool Creation Failed \n");
        return RMON_FAILURE;
    }
    gRmonAlarmEntryId = RMONMemPoolIds[MAX_RMON_ALARM_ENTRY_NODES_SIZING_ID];
    gRmonEventPoolId = RMONMemPoolIds[MAX_RMON_EVENT_NODES_SIZING_ID];
    gRmonAlarmPoolId = RMONMemPoolIds[MAX_RMON_ALARM_NODES_SIZING_ID];
    gRmonHistoryCtrlPoolId = RMONMemPoolIds[MAX_RMON_HISTORY_NODES_SIZING_ID];
    gRmonEtherStatsPoolId =
        RMONMemPoolIds[MAX_RMON_ETHER_STATS_NODES_SIZING_ID];
    gRmonHostTopNCtrlPoolId =
        RMONMemPoolIds[MAX_RMON_TOPN_CONTROL_ENTRY_SIZING_ID];
    gRmonMatrixCtrlPoolId =
        RMONMemPoolIds[MAX_RMON_MATRIX_CTRL_BLOCKS_SIZING_ID];
    gRmonHostCtrlPoolId = RMONMemPoolIds[MAX_RMON_HOST_CTRL_BLOCKS_SIZING_ID];
    gRmonLogEventPoolId = RMONMemPoolIds[MAX_RMON_LOG_EVENT_BLOCKS_SIZING_ID];
    gRmonEventCommPoolId = RMONMemPoolIds[MAX_RMON_EVENT_COMM_BLOCKS_SIZING_ID];
    gRmonLogBucketPoolId = RMONMemPoolIds[MAX_RMON_LOG_BUCKET_BLOCKS_SIZING_ID];
    gRmonAlarmVarPoolId = RMONMemPoolIds[MAX_RMON_ALARM_VAR_BLOCKS_SIZING_ID];
    gRmonEtherHisPoolId = RMONMemPoolIds[MAX_RMON_ETHER_HIS_BLOCKS_SIZING_ID];
    gRmonHisBucketsPoolId =
        RMONMemPoolIds[MAX_RMON_HIS_BUCKETS_NODES_SIZING_ID];
    gRmonHostTablePoolId = RMONMemPoolIds[MAX_RMON_HOST_TABLE_BLOCKS_SIZING_ID];
    gRmonMatrixBucketsPoolId =
        RMONMemPoolIds[MAX_RMON_MATRIX_BUCKETS_BLOCKS_SIZING_ID];
    gRmonTopNReportPoolId =
        RMONMemPoolIds[MAX_RMON_TOPN_REPORT_BLOCKS_SIZING_ID];
    gRmonVlanPoolId =
        RMONMemPoolIds[MAX_RMON_Q_MESG_SIZING_ID];

    if ((gpRmonEtherStatsHashTable =
         RMON_HASH_CREATE_TABLE (RMON_ETHERSTATS_HASH_SIZE,
                                 NULL, RMON_FALSE)) == NULL)
    {
        RMON_DBG (CRITICAL_TRC | RMON_EVENT_TRC,
                  "Ether Stats Entry Table Creation failed. \n");

        return RMON_FAILURE;
    }

    TMO_SLL_Init (&gEtherStatsSLL);

    if ((gpRmonHistoryControlHashTable =
         RMON_HASH_CREATE_TABLE (RMON_HISTORY_CONTROL_HASH_SIZE,
                                 NULL, RMON_FALSE)) == NULL)
    {
        RMON_DBG (CRITICAL_TRC | RMON_EVENT_TRC,
                  "History Control Entry Table Creation failed. \n");

        return RMON_FAILURE;
    }

    if ((gpRmonAlarmHashTable =
         RMON_HASH_CREATE_TABLE (RMON_ALARM_HASH_SIZE,
                                 NULL, RMON_FALSE)) == NULL)
    {
        RMON_DBG (CRITICAL_TRC | RMON_EVENT_TRC,
                  "History Control Entry Table Creation failed. \n");

        return RMON_FAILURE;
    }

    if ((gpRmonEventHashTable =
         RMON_HASH_CREATE_TABLE (RMON_EVENT_HASH_SIZE,
                                 NULL, RMON_FALSE)) == NULL)
    {
        RMON_DBG (CRITICAL_TRC | RMON_EVENT_TRC,
                  "History Control Entry Table Creation failed. \n");

        return RMON_FAILURE;
    }

#if SNMP_2_WANTED
    for (u4Count = 0; u4Count < RMON_MAX_INDICES; u4Count++)
    {
        RmonIndexPool1[u4Count].pIndex = &(RmonMultiPool1[u4Count]);
        RmonMultiPool1[u4Count].pOctetStrValue = &(RmonOctetPool1[u4Count]);
        RmonMultiPool1[u4Count].pOidValue = &(RmonOIDPool1[u4Count]);
        RmonMultiPool1[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1RmonData1[u4Count];
        RmonMultiPool1[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1RmonData1[u4Count];
    }
#else
    UNUSED_PARAM (u4Count);
#endif /*SNMP_2_WANTED */

    /* Call RmonCreateTask function will create the RMON Tasks */
    RmonCreateTask ();

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving the function : RmonInitialize\n");
    return RMON_SUCCESS;

}

/************************************************************************
 * Function            : RmonCreateTask                                 *  
 * Input (s)           : None.                                          *    
 * Output (s)          : None.                                          *
 * Returns             : RMON_SUCCESS/RMON_FAILURE                      *
 * Global Variables                                                     *
 *              Used   : None                                           *
 * Side effects        : None                                           *
 * Action              : This function creates the RmonMain using FSAP2 *
 *                       API "OsixCreateTask".                          *
 ************************************************************************
 */

INT4
RmonCreateTask (void)
{

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonCreateTask\n");

    gRmonSupportedGroups.u1RmonHwStatsSuppFlag = TRUE;
    gRmonSupportedGroups.u1RmonHwHistorySuppFlag = TRUE;
    gRmonSupportedGroups.u1RmonHwAlarmSuppFlag = TRUE;
    gRmonSupportedGroups.u1RmonHwHostSuppFlag = TRUE;
    gRmonSupportedGroups.u1RmonHwHostTopNSuppFlag = TRUE;
    gRmonSupportedGroups.u1RmonHwMatrixSuppFlag = TRUE;
    gRmonSupportedGroups.u1RmonHwEventSuppFlag = TRUE;

#ifdef NPAPI_WANTED
    gRmonSupportedGroups.u1RmonHwHostSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwHostTopNSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwMatrixSuppFlag = FALSE;
#endif

    RMON_DBG (MINOR_TRC | FUNC_EXIT, " Leaving function :RmonCreateTask\n");

    return RMON_SUCCESS;
}


/**************************************************************************/
/*   Function Name   : RmonHandleQMsg                                     */
/*   Description     : This function is used to handle configuration      */
/*                     queue messages.                                    */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RmonHandleQMsg (VOID)
{
    tRmonQMsg      *pRmonQMsg = NULL;
    tOsixMsg       *pOsixQMsg = NULL;

    while (RMON_RECV_FROM_QUEUE
           (RMON_QUEUE_ID, (UINT1 *) &pOsixQMsg, OSIX_DEF_MSG_LEN,
            RMON_ZERO) == OSIX_SUCCESS)
    {
        pRmonQMsg = (tRmonQMsg *) pOsixQMsg;
        if (pRmonQMsg->u2Command == RMON_DELETE_VLAN_MSG)
        {
            RmonDeleteVlan ((UINT4) pRmonQMsg->u2L2CxtId, pRmonQMsg->VlanId);
        }
        if (pRmonQMsg->u2Command == RMON_DELETE_PORT_MSG)
        {
            RmonProcessDeletePort (pRmonQMsg->u4IfIndex);
        }
        MemReleaseMemBlock (gRmonVlanPoolId, (UINT1 *) pRmonQMsg);
    }
    return;
}
/************************************************************************
 * Function            : RmonMain                                       *  
 * Input (s)           : None.                                          *    
 * Output (s)          : None.                                          *
 * Returns             : None.                                          *
 * Global Variables                                                     *
 *              Used   : u4_TableTimerQid                               *
 * Side effects        : None                                           *
 * Action              :                                                *
 *            1) Create the timer lists. Start the RMON task            *
 *             2) Start the one second and control timer;               *
 *               handle the corresponding expiry events.                *
 *            3) Call the functions to perform periodic                 *
 *               fetching and snapshots.                                *
 ************************************************************************
 */

void                RmonMain
ARG_LIST ((INT1 *pDummy))
{
    UINT1               u1ModuleId;
    UINT4               u4Event;
    UINT4               u4TimerRef;
    UINT4               u4TimerValue;
    tTmrAppTimer       *pTimerBlock;

    UNUSED_PARAM (pDummy);

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonMain\n");

    if (RmonInitialize () == RMON_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        RMON_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create Timer lists for one second and Control Timers */

    if (TmrCreateTimerList (RMON_TASK_NAME, RMON_ONE_SECOND_TIMER_EVENT,
                            NULL, &gTimerListId1) != TMR_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | EVENT_TMR,
                  "Failed in creating Rmon Periodic Timer..\n");
        /* Delete the task */
        if (RMON_TASK_ID != 0)
        {
            RMON_DELETE_TASK (RMON_TASK_ID);
            RMON_TASK_ID = 0;
        }
        /* Indicate the status of initialization to the main routine */
        RMON_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (TmrCreateTimerList (RMON_TASK_NAME, RMON_CONTROL_TIMER_EVENT,
                            NULL, &gTimerListId2) != TMR_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | EVENT_TMR,
                  "Failed in creating rmon Control Table Timer..\n");

        /*Delete already created TimerList */
        TmrDeleteTimerList (gTimerListId1);

        /* Delete the task */
        if (RMON_TASK_ID != 0)
        {
            RMON_DELETE_TASK (RMON_TASK_ID);
            RMON_TASK_ID = 0;
        }
        /* Indicate the status of initialization to the main routine */
        RMON_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    RMON_DBG2 (MAJOR_TRC | CTRL_TMR_TRC,
               "Created two timer list with Id :%d & %d \n", gTimerListId1,
               gTimerListId2);

    gOneSecTimer.u4Data = RMON_MODULE;

    u4TimerValue = (UINT4) (ONE_SECOND * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    /* Set the System Control Flag to Start */
    gRmonSystemControlFlag = RMON_SYS_START;

#ifdef NPAPI_WANTED
    /* Init the batch id for collecting stats by batch by batch */
    gu4RmonEtherStatsBatchId = RMON_HW_ETHER_STATS_BATCH_ONE;
#endif

    if (RMON_GET_TASK_ID (SELF, RMON_TASK_NAME, &(RMON_TASK_ID)) !=
        OSIX_SUCCESS)
    {
        RMON_DBG (CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Getting Task Id for rmon task failed \n");

        /* Delete the Timer lists and task. */
        if ((INT4) RmonShutdown () == RMON_FAILURE)
        {
            RMON_DBG (MAJOR_TRC | EVENT_TMR, "Failed to shutdown... \n");
        }
        /* Indicate the status of initialization to the main routine */
        RMON_INIT_COMPLETE (OSIX_FAILURE);
        return;

    }

    /* Indicate the status of initialization to the main routine */
    RMON_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterSTDRMON ();
    RegisterFSRMON ();
    RegisterSTDHCRMON ();
#endif

    while (1)
    {
        RMON_RECEIVE_EVENT (RMON_TASK_ID, RMON_ONE_SECOND_TIMER_EVENT |
                            RMON_CONTROL_TIMER_EVENT | RMON_QMSG_EVENT,
                            OSIX_WAIT, &u4Event);

        RMON_LOCK ();
        /* If RMON is enabled, process periodic timer events. */
        if (gRmonEnableStatusFlag == RMON_ENABLE)
        {
            if (u4Event & RMON_ONE_SECOND_TIMER_EVENT)
            {
                RMON_DBG (MINOR_TRC | RMON_EVENT_TRC,
                          " One second Timer Expired \n");
#ifdef RM_WANTED
                if (RmGetNodeState () == RM_ACTIVE)
#endif
                {
#ifdef NPAPI_WANTED
                    while (gu4RmonEtherStatsBatchId <= RMON_HW_ETHER_STATS_BATCH_MAX)
                    {
                RmonFetchHWStats ();
                        gu4RmonEtherStatsBatchId += 1;
                    }
                    gu4RmonEtherStatsBatchId = RMON_HW_ETHER_STATS_BATCH_ONE;
#endif
                RmonPeriodicUpdates ();
                }

                /* Restart the timer */
                u4TimerValue =
                    (UINT4) (ONE_SECOND * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                if (TmrStartTimer (gTimerListId1, &gOneSecTimer, u4TimerValue)
                    != TMR_SUCCESS)
                {
                    RMON_DBG (CRITICAL_TRC | EVENT_TMR,
                              " Timer Restart failed. \n");

                    /* Delete the Timer lists and task. */
                    if ((INT4) RmonShutdown () == RMON_FAILURE)
                    {
                        RMON_DBG (MAJOR_TRC | EVENT_TMR,
                                  "Failed to shutdown... \n");
                    }
                    RMON_UNLOCK ();
                    return;
                }

            }

            if (u4Event & RMON_QMSG_EVENT)
            {
                RmonHandleQMsg ();
            }
        }                        /* gRmonEnableStatusFlag == RMON_ENABLE */

        /* Process Control table timer expiry events for Module 
         * Enable/Disable state */

        /* For Control Timer expiry Event */
        if (u4Event & RMON_CONTROL_TIMER_EVENT)
        {
            while ((pTimerBlock = TmrGetNextExpiredTimer (gTimerListId2))
                   != NULL)
            {
                u4TimerRef = pTimerBlock->u4Data;
                pTimerBlock->u4Data = 0;
                u1ModuleId = RMON_GET_TIMER_REFERENCE_MODULE_TYPE (u4TimerRef);
                RmonTmmSendEvent (u1ModuleId, u4TimerRef);
            }
        }
        RMON_UNLOCK ();
    }                            /* end of while loop */
}

/****************************************************************************/
/* Function            : RmonPeriodicUpdates                                */
/* Input (s)           : None.                                              */
/* Output (s)          : None.                                              */
/* Returns             : None.                                              */
/* Global Variables                                                         */
/*             Used    : rmprB1AnyValidHistory, rmprB1AnyValidAlarm         */
/*                       rmprB1AnyValidTopN.                                */
/* Side effects        : None                                               */
/* Action              : This function will call the Periodic (History,     */
/*                       Alarm and TopN) functions for taking the snap      */
/*                       shots of Etherstats, Alarm and host tables.        */
/****************************************************************************/

void
RmonPeriodicUpdates (void)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering the function :RmonPeriodicUpdates\n");

    if (grmprB1AnyValidHistory == TRUE)
    {
        RmonUpdateHistoryTable ();
    }

    if (grmprB1AnyValidAlarm == TRUE)
    {
        RmonCheckAndGenerateAlarm ();
    }

    if (grmprB1AnyValidTopN == TRUE)
    {
        RmonUpdateHostTopNTable ();
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function :RmonPeriodicUpdates\n");
}

/******************************************************************************/
/* Function            : RmonShutdown                                         */
/* Input (s)           : None.                                                */
/* Output (s)          : None.                                                */
/* Returns             : RMON_SUCCESS/RMON_FAILURE                            */
/* Global Variables                                                           */
/*             Used    : gTimerListId1, gTimerListId2                         */
/* Action              : This functions frees the memory of control entries   */
/*                       and delete all the timers and the task.              */
/******************************************************************************/

INT4
RmonShutdown (void)
{

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entered in Rmon shut down... \n");

    /* Free the Memory allocated for all Control Table entries. */
    RmonInvalidateCtrlTableEntries ();

    /* To Delete the Tasks and Timers. */
    if (TmrDeleteTimerList (gTimerListId1) != TMR_SUCCESS)
    {
        RMON_DBG (MAJOR_TRC | EVENT_TMR,
                  "Failed in deleting the Timer list1... \n");
        return RMON_FAILURE;
    }

    /* Deleting the Timer list of 1 sec Control Timers. */
    if (TmrDeleteTimerList (gTimerListId2) != TMR_SUCCESS)
    {
        RMON_DBG (MAJOR_TRC | EVENT_TMR,
                  "Failed in deleting the Timer list2... \n");
        return RMON_FAILURE;
    }

    /* Deletion of RMON Queue */
    if (RMON_QUEUE_ID != RMON_ZERO)
    {
        RMON_DELETE_QUEUE (RMON_QUEUE_ID);
    }

    /* Deletion of RMON Task */
    if (RMON_TASK_ID != 0)
    {
        RMON_DELETE_TASK (RMON_TASK_ID);
        RMON_TASK_ID = 0;
    }

    /* Reset the Support Group Variables to FALSE */
    gRmonSupportedGroups.u1RmonHwStatsSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwHistorySuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwAlarmSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwHostSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwHostTopNSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwMatrixSuppFlag = FALSE;
    gRmonSupportedGroups.u1RmonHwEventSuppFlag = FALSE;

    gRmonEnableStatusFlag = RMON_DISABLE;
    gRmonSystemControlFlag = RMON_SYS_SHUTDOWN;

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving Rmon shut down....\n");
    return RMON_SUCCESS;
}
