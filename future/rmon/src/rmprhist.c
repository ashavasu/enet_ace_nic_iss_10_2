/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmprhist.c,v 1.28 2017/08/25 13:44:19 siva Exp $            *
 *                                                                  *
 * Description: Contains Functions to take the SNAP shot of EtherStats*
 *              Table and update the same in History Table.         *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmprhist.c
 *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.
 *
 *    SUBSYSTEM NAME         :  HISTORY MODULE
 *
 *    MODULE NAME            :  RMON PERIODIC
 *
 *    LANGUAGE               :  C
 *
 *    TARGET ENVIRONMENT     :  ANY
 *
 *    DATE OF FIRST RELEASE  :
 *
 *    DESCRIPTION            : This file contain the routines to update
 *                             History Control Table and EtherHistory Samples
 *
 *  --------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |            DESCRIPTION OF
 *                  |        DATE          |                CHANGE
 * -----------------|----------------------|---------------------------------
 *     1.0          |  Vijay G. Krishnan   |           Create Original
 *                  |     18-MAR-1997      |
 * -----------------|----------------------|---------------------------------
 *     2.0          |  Bhupendra           |function StoreSnapShotInHistoryTable
 *                  |  14-AUG-2001         | modified.
 * --------------------------------------------------------------------------
 *     3.0          |  LEELA L             | Changes for Dynamic Tables         
 *                  |  31-MAR-2002         |          
 * --------------------------------------------------------------------------
 *     4.0          |  LEELA L             | Changes for new MIDGEN Tool        
 *                  |  07-JUL-2002         |          
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"

/******************************************************************************
*      Function             : StoreSnapshotInHistoryTable                     *
*                                                                             *
*      Role of the function : This function stores snapshots in the Ether     *
*                             History Table.                                  *
*      Formal Parameters    : i4Index : Index to History Control Table        *
*                             u4Ifindex : Interface number used to index      *
*                                          Ether Stats Table                  *
*                             u4Type : Type of index being sent               *
*      Global Variables     : gaEtherStatsTable, gaEtherHistoryTable          *
*      Side Effects         : Updates gaEtherHistoryTable                     *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : RMON_SUCCESS/RMON_FAILURE                       *
******************************************************************************/

static INT4
StoreSnapshotInHistoryTable (INT4 i4Index, UINT4 u4IfIndex, UINT4 u4Type)
{
    tEtherHistorySample *pEthHistSamp = NULL;
    tEtherHistorySample *pEthHistPrevSamp = NULL;
    INT4               *pEthBktToBeFilled = NULL;

    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    AR_UINT8            u8HCPkts;
    AR_UINT8            u8HCOctets;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : StoreSnapshotInHistoryTable\n");

    pEtherStatsNode =
        HashSearchEtherStatsNode_DSource (gpRmonEtherStatsHashTable, u4IfIndex,
                                          u4Type);

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable, i4Index);

    if ((pEtherStatsNode == NULL) || (pHistoryControlNode == NULL))
    {
        RMON_DBG (MINOR_TRC | MEM_TRC,
                  "EtherStats entry or History Control Entry for this data source is empty. \n");
        return RMON_FAILURE;
    }
    if (pEtherStatsNode->etherStatsStatus == RMON_VALID)
    {
        MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
        if (u4Type == PORT_INDEX_TYPE)
        {

            if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Unable to get the interface speed. \n");
                return RMON_FAILURE;
            }
        }
        pEthBktToBeFilled =
            &(pHistoryControlNode->pHistoryEntry->i4BucketToBeFilled);

        pEthHistSamp =
            pHistoryControlNode->pHistoryEntry->pHistoryBucket +
            pHistoryControlNode->pHistoryEntry->i4BucketToBeFilled;

        pEthHistPrevSamp = pHistoryControlNode->pHistoryEntry->pHistoryBucket;

        if (pHistoryControlNode->pHistoryEntry->i4BucketToBeFilled == 1)
        {

            pEthHistSamp->i4SampleIndex = ((pEthHistPrevSamp +
                                            (pHistoryControlNode->
                                             pHistoryEntry->i4BucketsAllocated -
                                             1))->i4SampleIndex) + 1;
            if (pHistoryControlNode->u4HistoryMinSampleIndex == 0)
            {
                pHistoryControlNode->u4HistoryMinSampleIndex++;
            }
        }
        else
        {
            if (pHistoryControlNode->pHistoryEntry->i4BucketToBeFilled == 0)
            {
                pEthHistSamp->i4SampleIndex = 1;
            }
            else
            {
                pEthHistSamp->i4SampleIndex =
                    (pEthHistSamp - 1)->i4SampleIndex + 1;
            }
        }

        if (pEthHistSamp->i4SampleIndex >
            (pHistoryControlNode->pHistoryEntry->i4BucketsAllocated - 1))
        {
            pHistoryControlNode->u4HistoryMinSampleIndex++;
        }
        /* if previous sample status is less than latest status., then the difference 
         * is taken - else latest status is taken.[counter value would have resetted]*/
        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsDropEvents >=
            pEthHistPrevSamp->u4EtherHistoryDropEvents)
        {
            pEthHistSamp->u4EtherHistoryDropEvents =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsDropEvents -
                 pEthHistPrevSamp->u4EtherHistoryDropEvents);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryDropEvents =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsDropEvents;
        }

        pEthHistPrevSamp->u4EtherHistoryDropEvents =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsDropEvents;

        if (CfaIfInfo.u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            FSAP_Counter64_SUB (&
                                (pEthHistSamp->
                                 u8EtherHistoryHighCapacityOctets),
                                &(pEtherStatsNode->RmonEtherStatsPkts.
                                  u8EtherStatsHighCapacityOctets),
                                &(pEthHistPrevSamp->
                                  u8EtherHistoryHighCapacityOctets));

            FSAP_Counter64_ASSIGN (&(pEthHistPrevSamp->
                                     u8EtherHistoryHighCapacityOctets),
                                   &(pEtherStatsNode->RmonEtherStatsPkts.
                                     u8EtherStatsHighCapacityOctets));

            pEthHistSamp->u4EtherHistoryHighCapacityOverflowOctets =
                pEtherStatsNode->RmonEtherStatsPkts.
                u8EtherStatsHighCapacityOctets.msn;
        }
        else
        {
            if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOctets >=
                pEthHistPrevSamp->u4EtherHistoryOctets)
            {
                pEthHistSamp->u4EtherHistoryOctets =
                    (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOctets -
                     pEthHistPrevSamp->u4EtherHistoryOctets);
            }
            else
            {
                pEthHistSamp->u4EtherHistoryOctets =
                    pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOctets;
            }

            pEthHistPrevSamp->u4EtherHistoryOctets =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOctets;
        }
        if (CfaIfInfo.u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            FSAP_Counter64_SUB (&(pEthHistSamp->u8EtherHistoryHighCapacityPkts),
                                &(pEtherStatsNode->RmonEtherStatsPkts.
                                  u8EtherStatsHighCapacityPkts),
                                &(pEthHistPrevSamp->
                                  u8EtherHistoryHighCapacityPkts));

            FSAP_Counter64_ASSIGN (&(pEthHistPrevSamp->
                                     u8EtherHistoryHighCapacityPkts),
                                   &(pEtherStatsNode->RmonEtherStatsPkts.
                                     u8EtherStatsHighCapacityPkts));

            pEthHistSamp->u4EtherHistoryHighCapacityOverflowPkts =
                pEtherStatsNode->RmonEtherStatsPkts.
                u8EtherStatsHighCapacityPkts.msn;
        }
        else
        {
            if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts >=
                pEthHistPrevSamp->u4EtherHistoryPkts)
            {
                pEthHistSamp->u4EtherHistoryPkts =
                    (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts
                     - pEthHistPrevSamp->u4EtherHistoryPkts);
            }
            else
            {
                pEthHistSamp->u4EtherHistoryPkts =
                    pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts;
            }

        }

        pEthHistPrevSamp->u4EtherHistoryPkts =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsBroadcastPkts >=
            pEthHistPrevSamp->u4EtherHistoryBroadcastPkts)
        {
            pEthHistSamp->u4EtherHistoryBroadcastPkts =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsBroadcastPkts -
                 pEthHistPrevSamp->u4EtherHistoryBroadcastPkts);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryBroadcastPkts =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsBroadcastPkts;
        }

        pEthHistPrevSamp->u4EtherHistoryBroadcastPkts =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsBroadcastPkts;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsMulticastPkts >=
            pEthHistPrevSamp->u4EtherHistoryMulticastPkts)
        {
            pEthHistSamp->u4EtherHistoryMulticastPkts =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsMulticastPkts -
                 pEthHistPrevSamp->u4EtherHistoryMulticastPkts);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryMulticastPkts =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsMulticastPkts;
        }

        pEthHistPrevSamp->u4EtherHistoryMulticastPkts =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsMulticastPkts;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCRCAlignErrors >=
            pEthHistPrevSamp->u4EtherHistoryCRCAlignErrors)
        {
            pEthHistSamp->u4EtherHistoryCRCAlignErrors =
                (pEtherStatsNode->RmonEtherStatsPkts.
                 u4EtherStatsCRCAlignErrors -
                 pEthHistPrevSamp->u4EtherHistoryCRCAlignErrors);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryCRCAlignErrors =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCRCAlignErrors;
        }

        pEthHistPrevSamp->u4EtherHistoryCRCAlignErrors =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCRCAlignErrors;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsUndersizePkts >=
            pEthHistPrevSamp->u4EtherHistoryUndersizePkts)
        {
            pEthHistSamp->u4EtherHistoryUndersizePkts =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsUndersizePkts -
                 pEthHistPrevSamp->u4EtherHistoryUndersizePkts);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryUndersizePkts =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsUndersizePkts;

        }

        pEthHistPrevSamp->u4EtherHistoryUndersizePkts =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsUndersizePkts;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOversizePkts >=
            pEthHistPrevSamp->u4EtherHistoryOversizePkts)
        {
            pEthHistSamp->u4EtherHistoryOversizePkts =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOversizePkts -
                 pEthHistPrevSamp->u4EtherHistoryOversizePkts);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryOversizePkts =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOversizePkts;
        }

        pEthHistPrevSamp->u4EtherHistoryOversizePkts =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOversizePkts;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsFragments >=
            pEthHistPrevSamp->u4EtherHistoryFragments)
        {
            pEthHistSamp->u4EtherHistoryFragments =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsFragments -
                 pEthHistPrevSamp->u4EtherHistoryFragments);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryFragments =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsFragments;
        }

        pEthHistPrevSamp->u4EtherHistoryFragments =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsFragments;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsJabbers >=
            pEthHistPrevSamp->u4EtherHistoryJabbers)
        {
            pEthHistSamp->u4EtherHistoryJabbers =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsJabbers -
                 pEthHistPrevSamp->u4EtherHistoryJabbers);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryJabbers =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsJabbers;
        }

        pEthHistPrevSamp->u4EtherHistoryJabbers =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsJabbers;

        if (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCollisions >=
            pEthHistPrevSamp->u4EtherHistoryCollisions)
        {
            pEthHistSamp->u4EtherHistoryCollisions =
                (pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCollisions -
                 pEthHistPrevSamp->u4EtherHistoryCollisions);
        }
        else
        {
            pEthHistSamp->u4EtherHistoryCollisions =
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCollisions;
        }

        pEthHistPrevSamp->u4EtherHistoryCollisions =
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCollisions;

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "EtherHistory table objects are copied. \n");

        if (*pEthBktToBeFilled == 0)
        {
            RMON_T_GET_TICK ((tOsixSysTime *)
                             & ((pEthHistPrevSamp)->etherHistoryIntervalStart));
        }

        *pEthBktToBeFilled = ((*pEthBktToBeFilled + 1) %
                              (pHistoryControlNode->pHistoryEntry->
                               i4BucketsAllocated));

        if (*pEthBktToBeFilled == 0)
        {
            *pEthBktToBeFilled += 1;

            /*Using the zeroth bucket's time as the storage of the sample 
             * Collection start time*/
            (pEthHistSamp)->etherHistoryIntervalStart =
                (pEthHistPrevSamp)->etherHistoryIntervalStart;

            RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                      "EtherHistory table Timer is reset. \n");

            RMON_T_GET_TICK ((tOsixSysTime *)
                             & ((pEthHistPrevSamp)->etherHistoryIntervalStart));

        }
        else
        {

            (pEthHistSamp)->etherHistoryIntervalStart =
                (pEthHistPrevSamp)->etherHistoryIntervalStart;

            RMON_T_GET_TICK ((tOsixSysTime *)
                             & ((pEthHistPrevSamp)->etherHistoryIntervalStart));
        }

        /* For calculating the EtherHistoryUtilization RFC 2819 gives the
         * formula for 10Mbs 
         *                          Pkts * (9.6 + 6.4) + (Octets * .8)
         *           Utilization = ------------------------------------
         *                                 Interval * 10,000
         * which means interval needs to multiplied with the 
         * interface speed in Kbps 
         *
         * For calculating the EtherHistoryUtilization RFC 3273 gives the
         * formula for half-duplex Fast Ethernet
         *                          Pkts * (.96 + .64) + (Octets * .08)
         *           Utilization = ------------------------------------
         *                                 Interval * 10,000
         * which means interval needs to multiplied with the
         * interface speed in Kbps
         */

        if (CfaIfInfo.u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (CfaIfInfo.u4IfHighSpeed != 0)
            {
                u8HCPkts = (AR_UINT8) (pEthHistSamp->
                                       u8EtherHistoryHighCapacityPkts.msn *
                                       (CFA_ENET_MAX_SPEED_VALUE + 1));
                u8HCPkts = (((u8HCPkts + (AR_UINT8) pEthHistSamp->
                              u8EtherHistoryHighCapacityPkts.lsn) * 16) / 10);
                u8HCOctets = (AR_UINT8) (pEthHistSamp->
                                         u8EtherHistoryHighCapacityOctets.msn *
                                         (CFA_ENET_MAX_SPEED_VALUE + 1));
                u8HCOctets = ((u8HCOctets + (AR_UINT8) (pEthHistSamp->
                                                        u8EtherHistoryHighCapacityOctets.
                                                        lsn)) * 8) / 100;
                pEthHistSamp->i4EtherHistoryUtilization = (INT4) ((u8HCPkts +
                                                                   u8HCOctets) /
                                                                  ((AR_UINT8)
                                                                   pHistoryControlNode->
                                                                   i4HistoryControlInterval
                                                                   *
                                                                   (CfaIfInfo.
                                                                    u4IfHighSpeed
                                                                    * 1000)));
            }
            else if ((CfaIfInfo.u4IfSpeed > 0) &&
                     (CfaIfInfo.u4IfSpeed < CFA_ENET_MAX_SPEED_VALUE))
            {
                u8HCPkts = (AR_UINT8) (pEthHistSamp->
                                       u8EtherHistoryHighCapacityPkts.msn *
                                       (CFA_ENET_MAX_SPEED_VALUE + 1));
                u8HCPkts = (((u8HCPkts + (AR_UINT8) pEthHistSamp->
                              u8EtherHistoryHighCapacityPkts.lsn) * 16) / 10);
                u8HCOctets = (AR_UINT8) (pEthHistSamp->
                                         u8EtherHistoryHighCapacityOctets.msn *
                                         (CFA_ENET_MAX_SPEED_VALUE + 1));
                u8HCOctets = (((u8HCOctets + (AR_UINT8) (pEthHistSamp->
                                                         u8EtherHistoryHighCapacityOctets.
                                                         lsn)) * 8) / 100);
                pEthHistSamp->i4EtherHistoryUtilization = (INT4) ((u8HCPkts +
                                                                   u8HCOctets) /
                                                                  ((AR_UINT8)
                                                                   pHistoryControlNode->
                                                                   i4HistoryControlInterval
                                                                   *
                                                                   (CfaIfInfo.
                                                                    u4IfSpeed /
                                                                    1000)));
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Interface Speed is not set. \n");
            }
        }
        else
        {
            if (CfaIfInfo.u4IfHighSpeed != 0)
            {
                pEthHistSamp->i4EtherHistoryUtilization = (INT4)
                    (((pEthHistSamp->u4EtherHistoryPkts * 16) +
                      ((pEthHistSamp->u4EtherHistoryOctets * 8) / 10)) /
                     ((UINT4) pHistoryControlNode->i4HistoryControlInterval
                      * (CfaIfInfo.u4IfHighSpeed * 1000)));
            }
            else if (CfaIfInfo.u4IfSpeed != 0)
            {
                pEthHistSamp->i4EtherHistoryUtilization = (INT4)
                    (((pEthHistSamp->u4EtherHistoryPkts * 16) +
                      ((pEthHistSamp->u4EtherHistoryOctets * 8) / 10)) /
                     ((UINT4) pHistoryControlNode->i4HistoryControlInterval
                      * CfaIfInfo.u4IfSpeed));
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                          "Interface Speed is not set. \n");
            }
        }

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Utilization calculation is complete. \n");

    }                            /*end if */

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving  function :  StoreSnapshotInHistoryTable \n");

    return RMON_SUCCESS;
}

/******************************************************************************
*      Function             : RmonUpdateHistoryTable                          *
*                                                                             *
*      Role of the function : This function calls StoreSnapshotInHistoryTable *
*                             for every valid entry in History Control Table  *
*      Formal Parameters    : None                                            *
*      Global Variables     : grmprB1AnyValidHistory                          *
*                             gaHistoryControlTable                           *
*      Side Effects         : grmprB1AnyValidHistory becomes true if there    *
*                             is any valid entry                              *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonUpdateHistoryTable (void)
{
    UINT4               u4HashIndex;
    tRmonHistoryControlNode *ptr = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function :RmonUpdateHistoryTable \n");

    /* Global variable indicating whether there is any valid entry in the
     * History Control Table.  */
    grmprB1AnyValidHistory = FALSE;

    TMO_HASH_Scan_Table (gpRmonHistoryControlHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gpRmonHistoryControlHashTable, u4HashIndex, ptr,
                              tRmonHistoryControlNode *)
        {
            if (ptr != NULL)
            {
                if (ptr->historyControlStatus == RMON_VALID)
                {
                    grmprB1AnyValidHistory = TRUE;
                    if (ptr->u2TimeToMonitor == 0)
                    {

                        if (StoreSnapshotInHistoryTable
                            (ptr->i4HistoryControlIndex,
                             ptr->u4HistoryControlDataSource,
                             ptr->u4HisCntrlOIDType) == RMON_SUCCESS)
                        {

                            /* The mirror variable is updated with the original 
                             * value */
                            ptr->u2TimeToMonitor =
                                (UINT2) (ptr->i4HistoryControlInterval - 1);

                            RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                                      "Store EtherStats SnapShot Success \n");
                        }
                    }
                    else
                    {
                        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                                  "Store EtherStats snapshot not done \n");
                        ptr->u2TimeToMonitor--;
                    }
                }                /* Entry Status = RMON_VALID */
            }
        }
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving  function : RmonUpdateHistoryTable \n");
}
