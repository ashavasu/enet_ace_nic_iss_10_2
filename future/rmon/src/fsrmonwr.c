/*************************************************************************
*    Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*    $Id: fsrmonwr.c,v 1.9 2013/10/28 09:53:41 siva Exp $
*    
*    Description: Protocol Wraper Routines
*
* ***********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "fsrmonwr.h"
#include "fsrmondb.h"
#include "rmallinc.h"

VOID
RegisterFSRMON ()
{
    SNMPRegisterMibWithLock (&fsrmonOID, &fsrmonEntry, RmonLock, RmonUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsrmonOID, (const UINT1 *) "fsrmon");
}

INT4
RmonDebugTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonDebugType (pu4Error, pMultiData->u4_ULongValue));
}

INT4
RmonDebugTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonDebugType (pMultiData->u4_ULongValue));
}

INT4
RmonDebugTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonDebugType (&pMultiData->u4_ULongValue));
}

INT4
RmonEnableStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonEnableStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonEnableStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonEnableStatus (pMultiData->i4_SLongValue));
}

INT4
RmonEnableStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonEnableStatus (&pMultiData->i4_SLongValue));
}

INT4
RmonHwStatsSuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwStatsSupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwStatsSuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwStatsSupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwStatsSuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwStatsSupp (&pMultiData->i4_SLongValue));
}

INT4
RmonHwHistorySuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwHistorySupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwHistorySuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwHistorySupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwHistorySuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwHistorySupp (&pMultiData->i4_SLongValue));
}

INT4
RmonHwAlarmSuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwAlarmSupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwAlarmSuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwAlarmSupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwAlarmSuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwAlarmSupp (&pMultiData->i4_SLongValue));
}

INT4
RmonHwHostSuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwHostSupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwHostSuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwHostSupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwHostSuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwHostSupp (&pMultiData->i4_SLongValue));
}

INT4
RmonHwHostTopNSuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwHostTopNSupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwHostTopNSuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwHostTopNSupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwHostTopNSuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwHostTopNSupp (&pMultiData->i4_SLongValue));
}

INT4
RmonHwMatrixSuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwMatrixSupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwMatrixSuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwMatrixSupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwMatrixSuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwMatrixSupp (&pMultiData->i4_SLongValue));
}

INT4
RmonHwEventSuppTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2RmonHwEventSupp (pu4Error, pMultiData->i4_SLongValue));
}

INT4
RmonHwEventSuppSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetRmonHwEventSupp (pMultiData->i4_SLongValue));
}

INT4
RmonHwEventSuppGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRmonHwEventSupp (&pMultiData->i4_SLongValue));
}

INT4
RmonDebugTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonDebugType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonEnableStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonEnableStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwStatsSuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwStatsSupp (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwHistorySuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwHistorySupp
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwAlarmSuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwAlarmSupp (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwHostSuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwHostSupp (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwHostTopNSuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwHostTopNSupp
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwMatrixSuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwMatrixSupp (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
RmonHwEventSuppDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2RmonHwEventSupp (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexRmonStatsTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRmonStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRmonStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
RmonStatsOutFCSErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceRmonStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRmonStatsOutFCSErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
RmonStatsPkts1519to1522OctetsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceRmonStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRmonStatsPkts1519to1522Octets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}
