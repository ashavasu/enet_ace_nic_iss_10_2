/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmoncli.c,v 1.85 2018/02/12 11:10:33 siva Exp $
 *
 * *******************************************************************/

/* SOURCE FILE HEADER :
 *  
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : rmoncli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                              |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : RMON  configuration                              |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    : Linux                                            |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in rmon.mib  |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */
/*     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
 * |         | DATE       |                                                   |
 * +---------|------------|---------------------------------------------------+
 * |   1     | 28/06/2002 | Creation of clirmon.c for RMON CLI commands       |
 * +--------------------------------------------------------------------------+
 * |   2     | 27/08/2002 | Modification for Event Community Input.           |
 * |--------------------------------------------------------------------------|
 * |   3     | 5/11/2004  | Modified for Cisco like CLI  
 * +--------------------------------------------------------------------------+
 */

#ifndef __RMONCLI_C__
#define __RMONCLI_C__

#include "rmallinc.h"
#include "rmoncli.h"
#include "fsrmoncli.h"
#include "stdrmocli.h"
#include "vcm.h"

#define RMON_CLI_MAX_ARGS 9

#ifdef RMON_WANTED

/*************************************************************************/
/*  Function Name :  RmonFormatStatus                                    */
/*  Description   :  This is to format the status returned from Tables to*/
/*                   STRING. 1 - RMON_VALID, 2 - RMON_CREATE_REQUEST,    */
/*                   3 - RMON_UNDER_CREATION, 4 - RMON_INVALID           */
/*                   has to be printed. This function prints the Status  */
/*                                                                       */
/*  Input(s)      :  i4Status - Status value got through LOW Level fun.  */
/*                   pu1String - String indicating the "STATUS field"    */
/*  Output(s)     :  pu1Temp - filled up with the String output          */
/*************************************************************************/
VOID
RmonFormatStatus (tCliHandle CliHandle, INT4 i4Status)
{

    switch (i4Status)
    {
        case RMON_VALID:
            CliPrintf (CliHandle, "active, ");
            break;

        case RMON_CREATE_REQUEST:
            CliPrintf (CliHandle, "RMON_CREATE_REQUEST, ");
            break;

        case RMON_UNDER_CREATION:
            CliPrintf (CliHandle, "RMON_UNDER_CREATION, ");
            break;

        case RMON_INVALID:
            CliPrintf (CliHandle, "INVALID, ");
            break;

        default:
            CliPrintf (CliHandle, "Unknown Status\r\n");
            break;
    }
}

/*********************************************************************/
/*  Function Name : cli_process_rmon_cmd                             */
/*  Description   : This function is called by commands in Command   */
/*                  definition files.This function retrieves all     */
/*                  Input parameters from the command and passes them*/
/*                  to the Protocol Action Routines.                 */
/*                                                                   */
/*  Input(s)      : va_alist - Variable no. of arguments             */
/*  Output(s)     : NONE                                             */
/*                                                                   */
/*  Return Values : None.                                            */
/*********************************************************************/

INT4
cli_process_rmon_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RMON_CLI_MAX_ARGS];
    UINT1               u1OverView = RMON_FALSE;
    INT1                argno = 0;
    INT4                i4IfaceIndex = 0;
    UINT4               u4VlanId = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4RmonStatus = 0;
    INT4                i4Val = 0;
    UINT4               u4ErrCode;
    tRmonAlarmEntry    *pRmonAlarmEntry = NULL;
    /*Start */
    INT4                i4StatsIndex = 0;
    INT4                i4nextEtherStatsIndex = 0;
    tSNMP_OID_TYPE     *pOidValue = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    INT4                i4NextHistoryControlIndex = 0;
    INT4                i4NextetherHistorySampleIndex = 0;
    INT4                i4HistoryControlIndex = 0;
    INT4                i4etherHistorySampleIndex = 0;
    /*End */

    tRmonEventEntry     RmonEventEntry;
    tRmonEthHistCtrlEntry RmonEthHistCtrlEntry;
    tRmonEthStatsEntry  RmonEthStatsEntry;
    UINT2               u2Len = 0;

    /* Variable Argument extraction */
    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array.
     * Store IP_CLI_MAX_ARGS arguements at the max.
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == RMON_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    pOidValue = alloc_oid (RMON_MAX_OID_LEN);
    if (pOidValue == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " Memory allocation Failure - cli_process_rmon_cmd. \n");
        return i4RetStatus;
    }

    MEMSET (pOidValue->pu4_OidList, 0, RMON_MAX_OID_LEN);

    CliRegisterLock (CliHandle, RmonLock, RmonUnLock);
    RMON_LOCK ();

    switch (u4Command)
    {
        case CLI_RMON_SHOW_RMON:
            /* args[0] - command identifier
             * args[1] - statistics index
             * args[2] - history index
             */
            u4Command = CLI_PTR_TO_U4 (args[0]);

            if (gRmonEnableStatusFlag == (UINT4) RMON_ENABLE)
            {
                CliPrintf (CliHandle, "\r\nRMON is enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nRMON is disabled \r\n");
            }

            if (u4Command & RMON_SHOW_STATS)
            {
                /* Call show function to display statistics */
                if (args[1] == NULL)
                {
                    i4RetStatus = RmonShowStatisticsTable (CliHandle);
                }
                else
                {
                    i4RetStatus =
                        RmonShowStatisticsIndex (CliHandle, *(INT4 *) args[1]);
                }
            }
            if (u4Command & RMON_SHOW_ALARMS)
            {
                /* Call show function to display alarms */
                i4RetStatus = RmonShowAlarms (CliHandle);
            }
            if (u4Command & RMON_SHOW_EVENTS)
            {
                /* Call show function to display events */
                i4RetStatus = RmonShowEvents (CliHandle);
            }
            if (u4Command & RMON_SHOW_HISTORY)
            {
                /* Call show function to display history */

                if (args[2] == NULL)
                {
                    i4RetStatus = RmonShowHistoryTable (CliHandle, u1OverView);
                }
                else
                {
                    i4RetStatus =
                        RmonShowHistoryIndex (CliHandle, *(INT4 *) args[2],
                                              u1OverView);
                }
            }
            if (u4Command & RMON_SHOW_HISTORY_OVERVIEW)
            {
                u1OverView = RMON_TRUE;

                if (args[2] == NULL)
                {
                    i4RetStatus = RmonShowHistoryTable (CliHandle, u1OverView);
                }
                else
                {
                    i4RetStatus =
                        RmonShowHistoryIndex (CliHandle, *(INT4 *) args[2],
                                              u1OverView);
                }
            }
            CliPrintf (CliHandle, "\r\n");
            break;

        case CLI_RMON_STATUS:
            /* args[0] - CLI_ENABLE / CLI_DISABLE 
             */
            if (CLI_PTR_TO_I4 (args[0]) == (INT4) CLI_ENABLE)
            {
                i4RmonStatus = (INT4) RMON_ENABLE;
            }
            else if (CLI_PTR_TO_I4 (args[0]) == (INT4) CLI_DISABLE)
            {
                i4RmonStatus = (INT4) RMON_DISABLE;
                if (nmhGetFirstIndexEtherHistoryTable
                    (&i4NextHistoryControlIndex,
                     &i4NextetherHistorySampleIndex) == SNMP_SUCCESS)
                {

                    do
                    {

                        i4HistoryControlIndex = i4NextHistoryControlIndex;
                        i4etherHistorySampleIndex =
                            i4NextetherHistorySampleIndex;

                        CLI_MEMSET (ai1InterfaceName, 0,
                                    CFA_MAX_PORT_NAME_LENGTH);
                        nmhGetHistoryControlDataSource (i4HistoryControlIndex,
                                                        pOidValue);
                        u4IfIndex =
                            pOidValue->pu4_OidList[grlInterfaceOid.u4_Length];
                        RmonEthHistCtrlEntry.i4RmonEthHisIndexType =
                            PORT_INDEX_TYPE;
                        RmonEthHistCtrlEntry.i4HistoryControlIndex =
                            i4HistoryControlIndex;
                        RmonEthHistCtrlEntry.i4RmonEtherPortOrVlanIndex =
                            (INT4) u4IfIndex;
                        i4RetStatus =
                            RmonSetNoCollectionHistory (CliHandle,
                                                        &RmonEthHistCtrlEntry);

                    }
                    while (nmhGetNextIndexEtherHistoryTable
                           (i4HistoryControlIndex, &i4NextHistoryControlIndex,
                            i4etherHistorySampleIndex,
                            &i4NextetherHistorySampleIndex) != SNMP_FAILURE);

                }
                UNUSED_PARAM (i4RetStatus);
                if ((nmhGetFirstIndexEtherStatsTable (&i4StatsIndex)) ==
                    SNMP_SUCCESS)
                {
                    i4nextEtherStatsIndex = i4StatsIndex;

                    do
                    {
                        CLI_MEMSET (ai1InterfaceName, 0,
                                    CFA_MAX_PORT_NAME_LENGTH);
                        nmhGetEtherStatsDataSource (i4nextEtherStatsIndex,
                                                    pOidValue);
                        u4IfIndex =
                            pOidValue->pu4_OidList[grlInterfaceOid.u4_Length];
                        i4StatsIndex = i4nextEtherStatsIndex;
                        RmonEthStatsEntry.i4RmonEthIndexType = PORT_INDEX_TYPE;
                        RmonEthStatsEntry.i4EtherStatsIndex =
                            i4nextEtherStatsIndex;
                        RmonEthStatsEntry.i4RmonEtherPortOrVlanIndex =
                            (INT4) u4IfIndex;

                        i4RetStatus =
                            RmonSetNoCollectionStats (CliHandle,
                                                      &RmonEthStatsEntry);

                    }
                    while (nmhGetNextIndexEtherStatsTable (i4StatsIndex,
                                                           &i4nextEtherStatsIndex)
                           == SNMP_SUCCESS);

                }

                UNUSED_PARAM (i4RetStatus);

            }

            i4RetStatus = RmonSetStatus (CliHandle, i4RmonStatus);
            break;

        case CLI_RMON_ALARM:
            /* To configure the AlarmControlTable.
             * args[0] contains Table Index 
             * args[1] contains Alarmvariable (mib object id) 
             * args[2] contains the alarmInterval
             * args[3] contains sample type 
             * Sample types are "DELTA/ABSOLUTE". 
             * Alarm can be started on rising or falling or rising&falling. 
             * args[4] contains the alarm rising threshold. 
             * args[5] contains the rising event number
             * args[6] contains the alarm falling threshold
             * args[7] contains the falling event number  
             * args[8] contains the Alarm Owner. 

             * Default, CLI_RM_CREATE_REQUEST Status is assigned to the initial Status 
             * Default, CLI_RM_VALID Status is assigned to the Final Status */

            pRmonAlarmEntry =
                (tRmonAlarmEntry *) MemAllocMemBlk (gRmonAlarmEntryId);

            if (pRmonAlarmEntry == NULL)
            {

                CliPrintf (CliHandle,
                           "\r%% RMON : Memory Allocation failure\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pRmonAlarmEntry, 0, sizeof (tRmonAlarmEntry));

            pRmonAlarmEntry->i4AlarmIndex = *(INT4 *) args[0];
            pRmonAlarmEntry->i4RmonAlmStatus1 = (INT4) CLI_RM_CREATE_REQUEST;

            u2Len =
                (UINT2) (STRLEN (args[1]) <
                         (sizeof (pRmonAlarmEntry->au1RmonAlmVariable) -
                          1) ? STRLEN (args[1]) : (sizeof (pRmonAlarmEntry->
                                                           au1RmonAlmVariable)
                                                   - 1));

            CLI_STRNCPY (pRmonAlarmEntry->au1RmonAlmVariable,
                         (UINT1 *) args[1], u2Len);
            pRmonAlarmEntry->au1RmonAlmVariable[u2Len] = '\0';
            pRmonAlarmEntry->i4RmonAlmInterval = *(INT4 *) args[2];
            pRmonAlarmEntry->i4RmonAlmSampleType = CLI_PTR_TO_I4 (args[3]);

            /* Always the default value of AlarmStartupALarm is set to rising &
             * falling alarm */
            pRmonAlarmEntry->i4RmonAlmStartupAlm = 3;

            pRmonAlarmEntry->i4RmonAlmRiseThrshd = *(INT4 *) args[4];

            /* Get the first event number and configure the alarm ,if no event is given in command
               if no event is present in the table , exit without configuring the alarm */

            /* Retrieve the first index of the Event Table 
             * this will be passed when event-number is not specified */

            if (args[5] == NULL)
            {
                if (nmhGetFirstIndexEventTable
                    (&pRmonAlarmEntry->i4RmonAlmRiseEvtIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% RMON : Event table is empty\r\n");
                    i4RetStatus = CLI_FAILURE;
                    if (MemReleaseMemBlock
                        (gRmonAlarmEntryId,
                         (UINT1 *) pRmonAlarmEntry) == MEM_FAILURE)
                    {
                        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                                  " Memory Release Failure - cli_process_rmon_cmd. \n");
                    }

                    break;
                }
            }
            else
            {
                pRmonAlarmEntry->i4RmonAlmRiseEvtIndex = *(INT4 *) args[5];
            }

            pRmonAlarmEntry->i4RmonAlmFallThrshd = *(INT4 *) args[6];

            /* Get the first event number and configure the alarm ,if no event is given in command
               if no event is present in the table , exit without configuring the alarm */

            if (args[7] == NULL)
            {
                if (nmhGetFirstIndexEventTable
                    (&(pRmonAlarmEntry->i4RmonAlmFallEvtIndex)) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% RMON : Event table is empty\r\n");
                    i4RetStatus = CLI_FAILURE;
                    if (MemReleaseMemBlock
                        (gRmonAlarmEntryId,
                         (UINT1 *) pRmonAlarmEntry) == MEM_FAILURE)
                    {
                        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                                  " Memory Release Failure - cli_process_rmon_cmd. \n");
                    }

                    break;
                }
            }
            else
            {
                pRmonAlarmEntry->i4RmonAlmFallEvtIndex = *(INT4 *) args[7];
            }

            /* Allow configuration only if Raising threshold is > Falling threshold */

            if (pRmonAlarmEntry->i4RmonAlmRiseThrshd >
                pRmonAlarmEntry->i4RmonAlmFallThrshd)
            {
                CLI_MEMSET (pRmonAlarmEntry->au1RmonAlmOwner, 0,
                            CLI_OWN_STR_LEN);
                /* Since Owner string is optional, it has to be copied only if not
                 * NULL */
                if (args[8] != NULL)
                {
                    u2Len =
                        (UINT2) (STRLEN (args[8]) <
                                 (sizeof (pRmonAlarmEntry->au1RmonAlmOwner)
                                  -
                                  1) ? STRLEN (args[8])
                                 : (sizeof
                                    (pRmonAlarmEntry->au1RmonAlmOwner) - 1));

                    CLI_STRNCPY (pRmonAlarmEntry->au1RmonAlmOwner, args[8],
                                 u2Len);
                    pRmonAlarmEntry->au1RmonAlmOwner[u2Len] = '\0';
                }
                pRmonAlarmEntry->i4RmonAlmStatus2 = (INT4) CLI_RM_VALID;

                i4RetStatus = RmonSetAlarm (CliHandle, pRmonAlarmEntry);
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r%% Falling-threshold should be less than Rising-threshold\r\n");
                i4RetStatus = CLI_FAILURE;
            }

            if (MemReleaseMemBlock
                (gRmonAlarmEntryId, (UINT1 *) pRmonAlarmEntry) == MEM_FAILURE)
            {
                RMON_DBG (CRITICAL_TRC | MEM_TRC,
                          " Memory Release Failure - cli_process_rmon_cmd. \n");
                i4RetStatus = CLI_FAILURE;
            }

            break;

        case CLI_RMON_NO_ALARM:
            /* args[0] - contains alarm index */
            i4RetStatus = RmonSetNoAlarm (CliHandle, *(INT4 *) args[0]);
            break;

        case CLI_RMON_EVENT:
            /* args[0] - contains event number
             * args[1] - contains event description
             * args[2] - contains event type [log]
             * args[3] - contains event community
             * args[4] - contains event owner
             * Default, CLI_RM_CREATE_REQUEST Status is assigned to the initial 
             * Status
             * Default, CLI_RM_VALID Status is assigned to the Final Status
             */

            RmonEventEntry.i4EventIndex = *(INT4 *) args[0];
            RmonEventEntry.i4RmonEventStatus1 = (INT4) CLI_RM_CREATE_REQUEST;

            /*Event description */
            MEMSET (RmonEventEntry.au1RmonEventDescr, 0, RMON_DESC_LEN);
            if (args[1] != NULL)
            {
                u2Len =
                    (UINT2) (STRLEN (args[1]) <
                             (sizeof (RmonEventEntry.au1RmonEventDescr) -
                              1) ? STRLEN (args[1])
                             : (sizeof (RmonEventEntry.au1RmonEventDescr) - 1));
                CLI_STRNCPY (RmonEventEntry.au1RmonEventDescr, args[1], u2Len);
                RmonEventEntry.au1RmonEventDescr[STRLEN (args[1])] = '\0';
            }

            RmonEventEntry.i4RmonEventType = CLI_PTR_TO_I4 (args[2]);

            /* Owner string */
            CLI_MEMSET (RmonEventEntry.au1RmonEventOwner, 0, CLI_OWN_STR_LEN);
            if (args[4] != NULL)
            {

                u2Len =
                    (UINT2) (STRLEN (args[4]) <
                             (sizeof (RmonEventEntry.au1RmonEventOwner) -
                              1) ? STRLEN (args[4])
                             : (sizeof (RmonEventEntry.au1RmonEventOwner) - 1));

                CLI_STRNCPY (RmonEventEntry.au1RmonEventOwner, args[4], u2Len);
                RmonEventEntry.au1RmonEventOwner[u2Len] = '\0';
            }

            CLI_MEMSET (RmonEventEntry.au1RmonEventCommunity, 0,
                        COMMUNITY_STR_LEN);
            if (args[3] != NULL)
            {
                u2Len =
                    (UINT2) (STRLEN (args[3]) <
                             (sizeof (RmonEventEntry.au1RmonEventCommunity)
                              -
                              1) ? STRLEN (args[3])
                             : (sizeof
                                (RmonEventEntry.au1RmonEventCommunity) - 1));

                CLI_STRNCPY (RmonEventEntry.au1RmonEventCommunity, args[3],
                             u2Len);
                RmonEventEntry.au1RmonEventCommunity[u2Len] = '\0';

            }

            RmonEventEntry.i4RmonEventStatus2 = (INT4) CLI_RM_VALID;

            i4RetStatus = RmonSetEvent (CliHandle, &RmonEventEntry);

            break;

        case CLI_RMON_NO_EVENT:
            /* args[0] - contains event index */
            i4RetStatus = RmonSetNoEvent (CliHandle, *(INT4 *) args[0]);
            break;

        case CLI_RMON_COLLECTION_HISTORY:
            /* args[0] - history index
             * args[1] - number of history buckets requested
             * args[2] - history sample interval
             * args[3] - owner string
             */

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /*Index Type is set as Port */
            RmonEthHistCtrlEntry.i4RmonEthHisIndexType = PORT_INDEX_TYPE;

            RmonEthHistCtrlEntry.i4HistoryControlIndex = *(INT4 *) args[0];
            RmonEthHistCtrlEntry.i4RmonEthHistCtrlStatus1 =
                (INT4) CLI_RM_CREATE_REQUEST;

            /* Interface index is assigned as the Datasource. */
            RmonEthHistCtrlEntry.i4RmonEtherPortOrVlanIndex = i4IfaceIndex;

            RmonEthHistCtrlEntry.i4RmonEthHistBucketsReqstd =
                CLI_PTR_TO_I4 (args[1]);

            RmonEthHistCtrlEntry.i4RmonEthHistCtrlInterval =
                CLI_PTR_TO_I4 (args[2]);

            RmonEthHistCtrlEntry.i4RmonEthHistCtrlStatus2 = (INT4) CLI_RM_VALID;

            /* Since Owner string is optional, it has to be copied only if not
             * NULL */
            CLI_MEMSET (RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner, 0,
                        CLI_OWN_STR_LEN);
            if (args[3] != NULL)
            {
                CLI_STRNCPY (RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner,
                             args[3], STRLEN (args[3]));
                RmonEthHistCtrlEntry.
                    au1RmonEthHistCtrlOwner[STRLEN (args[3])] = '\0';

            }
            /* else copy monitor */
            else
            {
                CLI_STRCPY (RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner,
                            "monitor");
            }

            /* Interface Index is assigned as the datasource for EtherStats */
            i4RetStatus =
                RmonSetCollectionHistory (CliHandle, &RmonEthHistCtrlEntry);

            break;

        case CLI_RMON_NO_COLLECTION_HISTORY:
            /* args[0] - history index */

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /*Index Type is set as Port */
            RmonEthHistCtrlEntry.i4RmonEthHisIndexType = PORT_INDEX_TYPE;

            RmonEthHistCtrlEntry.i4HistoryControlIndex = *(INT4 *) args[0];

            /* Interface index is assigned as the Datasource. */
            RmonEthHistCtrlEntry.i4RmonEtherPortOrVlanIndex = i4IfaceIndex;

            i4RetStatus =
                RmonSetNoCollectionHistory (CliHandle, &RmonEthHistCtrlEntry);
            break;

        case CLI_RMON_COLLECTION_STATS:
            /* args[0] - index
             * args[1] - owner string
             */

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /*Index Type is set as Port */
            RmonEthStatsEntry.i4RmonEthIndexType = PORT_INDEX_TYPE;

            RmonEthStatsEntry.i4EtherStatsIndex = *(INT4 *) args[0];

            RmonEthStatsEntry.i4RmonEtherStatus1 = (INT4) CLI_RM_CREATE_REQUEST;

            /* Since Owner string is optional, it has to be copied only if not
             * NULL */

            CLI_MEMSET (RmonEthStatsEntry.au1RmonEtherOwner, 0,
                        CLI_OWN_STR_LEN);
            if (args[1] != NULL)
            {
                CLI_STRNCPY (RmonEthStatsEntry.au1RmonEtherOwner, args[1],
                             STRLEN (args[1]));
                RmonEthStatsEntry.au1RmonEtherOwner[STRLEN (args[1])] = '\0';
            }
            /* else copy monitor */
            else
            {
                CLI_STRCPY (RmonEthStatsEntry.au1RmonEtherOwner, "monitor");
            }

            /* Interface Index is assigned as the datasource for EtherStats */
            RmonEthStatsEntry.i4RmonEtherPortOrVlanIndex = i4IfaceIndex;

            RmonEthStatsEntry.i4RmonEtherStatus2 = (INT4) CLI_RM_VALID;

            i4RetStatus =
                RmonSetCollectionStats (CliHandle, &RmonEthStatsEntry);

            break;

        case CLI_RMON_NO_COLLECTION_STATS:
            /* args[0] - contains statistics index */

            i4IfaceIndex = CLI_GET_IFINDEX ();

            /*Index Type is set as Vlan */
            RmonEthStatsEntry.i4RmonEthIndexType = PORT_INDEX_TYPE;

            RmonEthStatsEntry.i4EtherStatsIndex = *(INT4 *) args[0];

            /* Interface Index is assigned as the datasource for EtherStats */
            RmonEthStatsEntry.i4RmonEtherPortOrVlanIndex = i4IfaceIndex;

            i4RetStatus =
                RmonSetNoCollectionStats (CliHandle, &RmonEthStatsEntry);
            break;

        case CLI_RMON_COLLECTION_HISTORY_FOR_VLAN:
            /* args[0] - history index
             * args[1] - number of history buckets requested
             * args[2] - history sample interval
             * args[3] - owner string
             */

            if (CLI_GET_CXT_ID () != RMON_DEFAULT_CONTEXT)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4VlanId = (UINT4) i4Val;

            /*Index Type is set as Vlan */
            RmonEthHistCtrlEntry.i4RmonEthHisIndexType = VLAN_INDEX_TYPE;

            RmonEthHistCtrlEntry.i4HistoryControlIndex = *(INT4 *) args[0];
            RmonEthHistCtrlEntry.i4RmonEthHistCtrlStatus1 =
                (INT4) CLI_RM_CREATE_REQUEST;

            /* VlanId is assigned as the Datasource. */
            RmonEthHistCtrlEntry.i4RmonEtherPortOrVlanIndex = (INT4) u4VlanId;

            RmonEthHistCtrlEntry.i4RmonEthHistBucketsReqstd =
                CLI_PTR_TO_I4 (args[1]);

            RmonEthHistCtrlEntry.i4RmonEthHistCtrlInterval =
                CLI_PTR_TO_I4 (args[2]);

            RmonEthHistCtrlEntry.i4RmonEthHistCtrlStatus2 = (INT4) CLI_RM_VALID;

            /* Since Owner string is optional, it has to be copied only if not
             * NULL */
            CLI_MEMSET (RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner, 0,
                        CLI_OWN_STR_LEN);
            if (args[3] != NULL)
            {
                CLI_STRNCPY (RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner,
                             args[3], STRLEN (args[3]));
                RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner[STRLEN (args[3])] =
                    '\0';
            }
            /* else copy monitor */
            else
            {
                CLI_STRCPY (RmonEthHistCtrlEntry.au1RmonEthHistCtrlOwner,
                            "monitor");
            }

            i4RetStatus = RmonSetCollectionHistory (CliHandle,
                                                    &RmonEthHistCtrlEntry);
            break;

        case CLI_RMON_NO_COLLECTION_HISTORY_FOR_VLAN:
            /* args[0] - history index */

            if (CLI_GET_CXT_ID () != RMON_DEFAULT_CONTEXT)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4VlanId = (UINT4) i4Val;

            /*Index Type is set as Vlan */
            RmonEthHistCtrlEntry.i4RmonEthHisIndexType = VLAN_INDEX_TYPE;

            RmonEthHistCtrlEntry.i4HistoryControlIndex = *(INT4 *) args[0];

            /* VlanId is assigned as the Datasource. */
            RmonEthHistCtrlEntry.i4RmonEtherPortOrVlanIndex = (INT4) u4VlanId;

            i4RetStatus =
                RmonSetNoCollectionHistory (CliHandle, &RmonEthHistCtrlEntry);
            break;

        case CLI_RMON_COLLECTION_STATS_FOR_VLAN:
            /* args[0] - statistics index
             * args[1] - owner string
             */

            if (CLI_GET_CXT_ID () != RMON_DEFAULT_CONTEXT)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4VlanId = (UINT4) i4Val;

            /*Index Type is set as Vlan */
            RmonEthStatsEntry.i4RmonEthIndexType = VLAN_INDEX_TYPE;

            RmonEthStatsEntry.i4EtherStatsIndex = *(INT4 *) args[0];

            RmonEthStatsEntry.i4RmonEtherStatus1 = (INT4) CLI_RM_CREATE_REQUEST;

            /* Since Owner string is optional, it has to be copied only if not
             * NULL */

            CLI_MEMSET (RmonEthStatsEntry.au1RmonEtherOwner, 0,
                        CLI_OWN_STR_LEN);
            if (args[1] != NULL)
            {
                CLI_STRNCPY (RmonEthStatsEntry.au1RmonEtherOwner, args[1],
                             STRLEN (args[1]));
                RmonEthStatsEntry.au1RmonEtherOwner[STRLEN (args[1])] = '\0';
            }
            /* else copy monitor */
            else
            {
                CLI_STRCPY (RmonEthStatsEntry.au1RmonEtherOwner, "monitor");
            }

            /* Vlan Id is assigned as the datasource for EtherStats */
            RmonEthStatsEntry.i4RmonEtherPortOrVlanIndex = (INT4) u4VlanId;

            RmonEthStatsEntry.i4RmonEtherStatus2 = (INT4) CLI_RM_VALID;

            i4RetStatus =
                RmonSetCollectionStats (CliHandle, &RmonEthStatsEntry);

            break;

        case CLI_RMON_NO_COLLECTION_STATS_FOR_VLAN:
            /* args[0] - statistics index */

            if (CLI_GET_CXT_ID () != RMON_DEFAULT_CONTEXT)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4Val = CLI_GET_VLANID ();
            if (i4Val == CLI_ERROR)
            {
                CLI_SET_ERR (CLI_RMON_INVALID_CONTEXT);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4VlanId = (UINT4) i4Val;

            /*Index Type is set as Vlan */
            RmonEthStatsEntry.i4RmonEthIndexType = VLAN_INDEX_TYPE;

            RmonEthStatsEntry.i4EtherStatsIndex = *(INT4 *) args[0];

            /* Vlan Id is assigned as the datasource for EtherStats */
            RmonEthStatsEntry.i4RmonEtherPortOrVlanIndex = (INT4) u4VlanId;

            i4RetStatus =
                RmonSetNoCollectionStats (CliHandle, &RmonEthStatsEntry);
            break;

        default:
            CliPrintf (CliHandle, "\rUnknown Command\r\n");
            break;
    }

    free_oid (pOidValue);
    if (((UINT4) i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RMON_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", RmonCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);
    RMON_UNLOCK ();
    return (i4RetStatus);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonShowAlarms                                         */
/*                                                                           */
/* Description      : This function is invoked to display RMON alarms        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonShowAlarms (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OctetRetvalOwner;
    INT4                i4ReturnVal;
    INT4                i4AlarmIndex = 0;
    INT4                i4NextAlarmIndex = 0;
    INT1                i1RetVal = 0;
    INT4                i4Counter = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSNMP_OID_TYPE      AlrmVariable;
    UINT1               au1Temp[RMON_DESC_LEN + 1];
    UINT4               au4Temp[RMON_DESC_LEN];
    /* Making the Alarmvariable string to ZERO */

    OctetRetvalOwner.pu1_OctetList = &au1Temp[0];
    AlrmVariable.pu4_OidList = &au4Temp[0];

    if ((i1RetVal = nmhGetFirstIndexAlarmTable (&i4AlarmIndex)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rAlarm table is empty\r\n");
        return (CLI_SUCCESS);
    }
    /* Outside the DO loop, assign the first index to next alarm index.
     * Inside the DO loop, assign the next index to the first index. */

    i4NextAlarmIndex = i4AlarmIndex;

    do
    {
        CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN + 1);

        i4AlarmIndex = i4NextAlarmIndex;

        CliPrintf (CliHandle, "\rAlarm %d is ", i4AlarmIndex);

        nmhGetAlarmStatus (i4AlarmIndex, &i4ReturnVal);
        /* Call RmonFormatStatus to convert the Status to a String */
        RmonFormatStatus (CliHandle, i4ReturnVal);

        nmhGetAlarmOwner (i4AlarmIndex, &OctetRetvalOwner);
        CliPrintf (CliHandle, " owned by %s\r\n",
                   OctetRetvalOwner.pu1_OctetList);

        /*  OidValue to String Conversion here */
        nmhGetAlarmVariable (i4AlarmIndex, &AlrmVariable);

        CliPrintf (CliHandle, " Monitors ");
        if (AlrmVariable.u4_Length != 0)
        {
            for (i4Counter = 0; i4Counter < (INT4) (AlrmVariable.u4_Length);
                 i4Counter++)
            {
                CliPrintf (CliHandle, "%d",
                           AlrmVariable.pu4_OidList[i4Counter]);

                if (i4Counter < ((INT4) (AlrmVariable.u4_Length - 1)))
                {
                    CliPrintf (CliHandle, ".");
                }
            }
        }

        nmhGetAlarmInterval (i4AlarmIndex, &i4ReturnVal);
        CliPrintf (CliHandle, " every %d second(s)\r\n", i4ReturnVal);

        nmhGetAlarmSampleType (i4AlarmIndex, &i4ReturnVal);

        switch (i4ReturnVal)
        {

            case RMON_ALARM_ABSOLUTE:
                CliPrintf (CliHandle, " Taking absolute samples, ");
                break;

            case RMON_ALARM_DELTA:
                CliPrintf (CliHandle, " Taking delta samples, ");
                break;
            default:
                break;
        }

        nmhGetAlarmValue (i4AlarmIndex, &i4ReturnVal);
        CliPrintf (CliHandle, "last value was %u\r\n", (UINT4) i4ReturnVal);

        nmhGetAlarmRisingThreshold (i4AlarmIndex, &i4ReturnVal);
        CliPrintf (CliHandle, " Rising threshold is %d,", i4ReturnVal);

        nmhGetAlarmRisingEventIndex (i4AlarmIndex, &i4ReturnVal);
        CliPrintf (CliHandle, " assigned to event %d\r\n", i4ReturnVal);

        nmhGetAlarmFallingThreshold (i4AlarmIndex, &i4ReturnVal);
        CliPrintf (CliHandle, " Falling threshold is %d,", i4ReturnVal);

        nmhGetAlarmFallingEventIndex (i4AlarmIndex, &i4ReturnVal);
        CliPrintf (CliHandle, " assigned to event %d\r\n", i4ReturnVal);

        nmhGetAlarmStartupAlarm (i4AlarmIndex, &i4ReturnVal);

        switch (i4ReturnVal)
        {
            case RMON_RISING_ALARM:
                u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                    " On startup enable rising alarm\r\n");
                break;

            case RMON_FALLING_ALARM:
                u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                    " On startup enable falling alarm\r\n");
                break;

            case RMON_RISING_OR_FALLING_ALARM:
                u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                    " On startup enable rising or falling alarm\r\n");
                break;

            default:
                break;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /*User pressed 'q' at more prompt when CLI is waiting for
             *input keys to continue with display, so break processing
             */
            break;
        }
    }
    while (nmhGetNextIndexAlarmTable
           (i4AlarmIndex, &i4NextAlarmIndex) == SNMP_SUCCESS);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonShowEvents                                         */
/*                                                                           */
/* Description      : This function is invoked to display RMON events        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonShowEvents (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OctetRetvalOwner;
    tSNMP_OCTET_STRING_TYPE OctetRetvalEventDescription;
    tSNMP_OCTET_STRING_TYPE OctetRetvalLogDescription;
    tSNMP_OCTET_STRING_TYPE OctetRetvalEventCommunity;

    INT4                i4EventIndex = 0;
    INT4                i4LogIndex = 0;
    INT4                i4NextEventIndex = 0;
    INT4                i4ReturnVal;
    UINT4               u4CounterVal;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                i1RetVal = 0;
    UINT1               au1Temp[RMON_DESC_LEN];
    UINT1               au1Temp1[RMON_DESC_LEN + 1];
    UINT1               au1Temp2[RMON_DESC_LEN + 1];
    UINT1               au1Temp3[RMON_DESC_LEN + 1];
    UINT1               au1Temp4[LOG_DESCR_LEN];
    tRmonEventNode     *pEventNode = NULL;
    CHR1                au1TimeStr[CLI_TIME_STR_BUF_SIZE];

    /* Since Event Table has more parameters of type tSNMP_OCTET_STRING_TYPE
     * All need to be converted so that they can be printed on the console.
     * Allocating Memory for Owner which is of type tSNMP_OCTET_STRING_TYPE */

    OctetRetvalOwner.pu1_OctetList = &au1Temp1[0];
    OctetRetvalEventDescription.pu1_OctetList = &au1Temp2[0];
    OctetRetvalEventCommunity.pu1_OctetList = &au1Temp3[0];
    OctetRetvalLogDescription.pu1_OctetList = &au1Temp4[0];

    /* Retrieve the first index of the Event Table */
    if ((i1RetVal = nmhGetFirstIndexEventTable (&i4EventIndex)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rEvent table is empty\r\n");
        return (CLI_SUCCESS);
    }

    /* Outside the DO loop, assign the First Index to Next Event Index.
     * Inside the DO loop, assign the next Event Index to the First Index */
    i4NextEventIndex = i4EventIndex;

    do
    {
        CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN);
        CLI_MEMSET (au1Temp1, 0, RMON_DESC_LEN + 1);
        CLI_MEMSET (au1Temp2, 0, RMON_DESC_LEN + 1);
        CLI_MEMSET (au1Temp3, 0, RMON_DESC_LEN + 1);

        i4EventIndex = i4NextEventIndex;

        nmhGetEventStatus (i4EventIndex, &i4ReturnVal);
        /* We have VALID event state for which CISCO displays active
         * For other states I am displaying our state values
         */
        CliPrintf (CliHandle, "\r\nEvent %d is ", i4EventIndex);

        RmonFormatStatus (CliHandle, i4ReturnVal);

        nmhGetEventOwner (i4EventIndex, &OctetRetvalOwner);
        CliPrintf (CliHandle, "owned by %s\r\n",
                   OctetRetvalOwner.pu1_OctetList);

        nmhGetEventDescription (i4EventIndex, &OctetRetvalEventDescription);

        CliPrintf (CliHandle, " Description is %s\r\n",
                   OctetRetvalEventDescription.pu1_OctetList);

        nmhGetEventType (i4EventIndex, &i4ReturnVal);
        CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN);
        CliPrintf (CliHandle, " Event firing causes ");
        switch (i4ReturnVal)
        {
            case RMON_EVENT_NONE:
                CliPrintf (CliHandle, "nothing,\r\n");
                break;

            case RMON_EVENT_LOG:
                CliPrintf (CliHandle, "log,\r\n");
                break;

            case RMON_EVENT_TRAP:
                /* Display the community if TRAP */
                nmhGetEventCommunity (i4EventIndex, &OctetRetvalEventCommunity);
                CliPrintf (CliHandle, "trap to community %s,\r\n",
                           OctetRetvalEventCommunity.pu1_OctetList);
                break;

            case RMON_EVENT_LOG_AND_TRAP:
                /* Display the community if TRAP */
                nmhGetEventCommunity (i4EventIndex, &OctetRetvalEventCommunity);
                CliPrintf (CliHandle, "log and trap to community %s,\r\n",
                           OctetRetvalEventCommunity.pu1_OctetList);
                break;
            default:
                break;
        }

        /* Display when the last time was sent */
        nmhGetEventLastTimeSent (i4EventIndex, &u4CounterVal);

        MEMSET (&au1TimeStr, 0, CLI_TIME_STR_BUF_SIZE);

        UtlGetTimeStrForTicks (u4CounterVal, au1TimeStr);

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            " Time last sent is %s \r\n",
                                            au1TimeStr);

        if ((i4ReturnVal != RMON_EVENT_NONE)
            && (i4ReturnVal != RMON_EVENT_TRAP))
        {
            if (u4CounterVal != 0)
            {

                /* For the no. of Log Buckets allocated for the event, get log entries */

                pEventNode =
                    HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

                if (pEventNode == NULL)
                {
                    CliPrintf (CliHandle, "\r\nInvalid Event Index\r\n");
                    return (CLI_FAILURE);

                }
                for (i4LogIndex = 1;
                     i4LogIndex < pEventNode->pLogEvent->i4BucketsFilled + 1;
                     i4LogIndex++)
                {

                    /* Converted Log Description */
                    CLI_MEMSET (au1Temp4, 0, LOG_DESCR_LEN);
                    nmhGetLogDescription (i4EventIndex, i4LogIndex,
                                          &OctetRetvalLogDescription);
                    CliPrintf (CliHandle, " %s\r\n",
                               OctetRetvalLogDescription.pu1_OctetList);

                }                /* End for */

            }
        }
        u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            /*User pressed 'q' at more prompt when CLI is waiting for
             *input keys to continue with display, so break processing
             */
            break;
        }
    }
    while (nmhGetNextIndexEventTable
           (i4EventIndex, &i4NextEventIndex) == SNMP_SUCCESS);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonShowHistoryIndex                                   */
/*                                                                           */
/* Description      : This function is invoked to display RMON history       */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4HistoryIndex - history index                      */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonShowHistoryIndex (tCliHandle CliHandle, INT4 i4HistoryIndex,
                      UINT1 u1OverView)
{

    UINT1               au1Temp[RMON_DESC_LEN];
    CHR1                au1TimeStr[CLI_TIME_STR_BUF_SIZE];
    INT4                i4ReturnVal;
    INT4                i4bucketsGrantedVal = 0;
    INT4                i4HistoryControlInterval = 0;
    INT4                i4etherHistorySampleIndex = 0;
    INT4                i4NextHistoryIndex = 0;
    INT4                i4NextSampleIndex = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    UINT4               u4CounterVal;
    UINT4               u4IfIndex = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4OIDType = 0;
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    tSNMP_OCTET_STRING_TYPE OctetRetvalOwner;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tSNMP_COUNTER64_TYPE u8Value;
    CHR1                ac1Val[RMON_U8_STR_LENGTH];
    FS_UINT8            u8Val;
    UINT4               u4OverFlowValue = 0;
    UINT4               u4IfSpeed = 0;

    OctetRetvalOwner.pu1_OctetList = &au1Temp[0];
    OidValue.pu4_OidList = &au4OidValue[0];

    CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN);
    CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);

    /* To Print the Interface Index or Vlan Id value */
    if (nmhGetHistoryControlDataSource (i4HistoryIndex, &OidValue)
        != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_HISTORY_ENTRY_NOTEXISTS_ERR);
        return (CLI_FAILURE);
    }
    MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
    MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8Val, 0, sizeof (FS_UINT8));

    CliPrintf (CliHandle, "\rEntry %d is ", i4HistoryIndex);

    nmhGetHistoryControlStatus (i4HistoryIndex, &i4ReturnVal);
    RmonFormatStatus (CliHandle, i4ReturnVal);

    nmhGetHistoryControlOwner (i4HistoryIndex, &OctetRetvalOwner);
    CliPrintf (CliHandle, " and owned by %s\r\n",
               OctetRetvalOwner.pu1_OctetList);

    nmhGetHistoryControlInterval (i4HistoryIndex, &i4HistoryControlInterval);

    /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
     * grlInterfaceOid and grlVlanOid are the Global Oid */
    RmonCheckDataSourceOIDIsVlanOrInterface (&OidValue, &u4OIDType);

    if (u4OIDType == PORT_INDEX_TYPE)
    {
        u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];

        CliPrintf (CliHandle, " Monitors ifEntry.1.%u every %d second(s)\r\n",
                   u4IfIndex, i4HistoryControlInterval);

        if (CfaGetIfSpeed (u4IfIndex, &u4IfSpeed) == CFA_FAILURE)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " RmonShowHistoryIndex - "
                      " Unable to get the interface speed. \n");
            return (CLI_FAILURE);
        }
    }
    else if (u4OIDType == VLAN_INDEX_TYPE)
    {
        u4VlanId = OidValue.pu4_OidList[grlVlanOid.u4_Length];

        CliPrintf (CliHandle, " Monitors Vlan %u every %d second(s)\r\n",
                   u4VlanId, i4HistoryControlInterval);
    }

    /* Get the buckets granted value from Control table for the index  */
    nmhGetHistoryControlStatus (i4HistoryIndex, &i4ReturnVal);

    nmhGetHistoryControlBucketsRequested (i4HistoryIndex, &i4bucketsGrantedVal);
    CliPrintf (CliHandle,
               " Requested # of time intervals, ie buckets, is %d, \r\n",
               i4bucketsGrantedVal);
    nmhGetHistoryControlBucketsGranted (i4HistoryIndex, &i4bucketsGrantedVal);
    CliPrintf (CliHandle,
               " Granted # of time intervals, ie buckets, is %d, \r\n",
               i4bucketsGrantedVal);
    i4NextHistoryIndex = i4HistoryIndex;

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryIndex);

    if ((pHistoryControlNode != NULL)
        && (pHistoryControlNode->historyControlStatus == RMON_VALID)
        && (pHistoryControlNode->u4HistoryMinSampleIndex != 0))
    {
        i4NextSampleIndex = (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;
        i4NextHistoryIndex = i4HistoryIndex;
        if (u1OverView == RMON_FALSE)
        {
            do
            {
                if (i4HistoryIndex != i4NextHistoryIndex)
                {
                    break;
                }

                i4etherHistorySampleIndex = i4NextSampleIndex;

                nmhGetEtherHistoryIntervalStart (i4HistoryIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                MEMSET (&au1TimeStr, 0, CLI_TIME_STR_BUF_SIZE);

                UtlGetTimeStrForTicks (u4CounterVal, au1TimeStr);

                CliPrintf (CliHandle, "  Sample %d began measuring at %s\r\n",
                           i4etherHistorySampleIndex, au1TimeStr);

                if (u4IfSpeed >= CFA_ENET_SPEED_1G)
                {
                    if (ISS_HW_SUPPORTED !=
                        IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
                    {
                        nmhGetEtherHistoryOctets (i4HistoryIndex,
                                                  i4etherHistorySampleIndex,
                                                  &u4CounterVal);
                        CliPrintf (CliHandle, "   Received %u octets, ",
                                   u4CounterVal);
                    }
                    else
                    {
                        MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                        MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                        MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                        nmhGetEtherHistoryHighCapacityOctets (i4HistoryIndex,
                                                              i4etherHistorySampleIndex,
                                                              &u8Value);
                        u8Val.u4Hi = u8Value.msn;
                        u8Val.u4Lo = u8Value.lsn;
                        FSAP_U8_2STR (&u8Val, ac1Val);
                        CliPrintf (CliHandle, "   Received %s octets, ",
                                   ac1Val);

                        MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                        MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                        MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                        nmhGetEtherHistoryHighCapacityPkts (i4HistoryIndex,
                                                            i4etherHistorySampleIndex,
                                                            &u8Value);
                        u8Val.u4Hi = u8Value.msn;
                        u8Val.u4Lo = u8Value.lsn;
                        FSAP_U8_2STR (&u8Val, ac1Val);
                        CliPrintf (CliHandle, "%s packets,\r\n", ac1Val);
                    }
                }
                else
                {

                    nmhGetEtherHistoryOctets (i4HistoryIndex,
                                              i4etherHistorySampleIndex,
                                              &u4CounterVal);
                    CliPrintf (CliHandle, "   Received %u octets, ",
                               u4CounterVal);
                }

                if (u4IfSpeed >= CFA_ENET_SPEED_1G)
                {
                    if (ISS_HW_SUPPORTED !=
                        IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
                    {
                        nmhGetEtherHistoryPkts (i4HistoryIndex,
                                                i4etherHistorySampleIndex,
                                                &u4CounterVal);
                        CliPrintf (CliHandle, "%u packets,\r\n", u4CounterVal);
                    }
                    else
                    {
                        u4OverFlowValue = 0;
                        nmhGetEtherHistoryHighCapacityOverflowOctets
                            (i4HistoryIndex, i4etherHistorySampleIndex,
                             &u4OverFlowValue);
                        CliPrintf (CliHandle, "   %u overflow octets, ",
                                   u4OverFlowValue);

                        u4OverFlowValue = 0;
                        nmhGetEtherHistoryHighCapacityOverflowPkts
                            (i4HistoryIndex, i4etherHistorySampleIndex,
                             &u4OverFlowValue);
                        CliPrintf (CliHandle, "%u overflow packets\r\n",
                                   u4OverFlowValue);
                    }
                }
                else
                {

                    nmhGetEtherHistoryPkts (i4HistoryIndex,
                                            i4etherHistorySampleIndex,
                                            &u4CounterVal);
                    CliPrintf (CliHandle, "%u packets,\r\n", u4CounterVal);
                }

                nmhGetEtherHistoryBroadcastPkts (i4HistoryIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                CliPrintf (CliHandle, "   %u broadcast and ", u4CounterVal);

                nmhGetEtherHistoryMulticastPkts (i4HistoryIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                CliPrintf (CliHandle, "%u multicast packets,\r\n",
                           u4CounterVal);

                nmhGetEtherHistoryUndersizePkts (i4HistoryIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                CliPrintf (CliHandle, "   %u undersized and ", u4CounterVal);

                nmhGetEtherHistoryOversizePkts (i4HistoryIndex,
                                                i4etherHistorySampleIndex,
                                                &u4CounterVal);
                CliPrintf (CliHandle, "%u oversized packets,\r\n",
                           u4CounterVal);

                nmhGetEtherHistoryFragments (i4HistoryIndex,
                                             i4etherHistorySampleIndex,
                                             &u4CounterVal);
                CliPrintf (CliHandle, "   %u fragments and ", u4CounterVal);

                nmhGetEtherHistoryJabbers (i4HistoryIndex,
                                           i4etherHistorySampleIndex,
                                           &u4CounterVal);
                CliPrintf (CliHandle, "%u jabbers,\r\n", u4CounterVal);

                nmhGetEtherHistoryCRCAlignErrors (i4HistoryIndex,
                                                  i4etherHistorySampleIndex,
                                                  &u4CounterVal);
                CliPrintf (CliHandle, "   %u CRC alignment errors and ",
                           u4CounterVal);

                nmhGetEtherHistoryCollisions (i4HistoryIndex,
                                              i4etherHistorySampleIndex,
                                              &u4CounterVal);
                CliPrintf (CliHandle, "%u collisions,\r\n", u4CounterVal);

                nmhGetEtherHistoryDropEvents (i4HistoryIndex,
                                              i4etherHistorySampleIndex,
                                              &u4CounterVal);
                CliPrintf (CliHandle,
                           "   # of dropped packet events is %u\r\n",
                           u4CounterVal);

                nmhGetEtherHistoryUtilization (i4HistoryIndex,
                                               i4etherHistorySampleIndex,
                                               &i4ReturnVal);
                i4PageStatus =
                    CliPrintf (CliHandle,
                               "   Network utilization is estimated at %d\r\n",
                               i4ReturnVal);
                if ((UINT4) i4PageStatus == CLI_FAILURE)
                {
                    /* Page quit 'q' pressed */
                    break;
                }
            }
            while (nmhGetNextIndexEtherHistoryTable (i4HistoryIndex,
                                                     &i4NextHistoryIndex,
                                                     i4etherHistorySampleIndex,
                                                     &i4NextSampleIndex) ==
                   SNMP_SUCCESS);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonShowHistoryTable                                   */
/*                                                                           */
/* Description      : This function is invoked to display RMON history table */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonShowHistoryTable (tCliHandle CliHandle, UINT1 u1OverView)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4OIDType = 0;
    INT4                i4ReturnVal;
    INT4                i4NextHistoryControlIndex = 0;
    INT4                i4bucketsGrantedVal = 0;
    INT4                i4HistoryControlIndex = 0;
    INT4                i4HistoryControlInterval;
    INT4                i4etherHistorySampleIndex = 0;
    INT4                i4NextetherHistorySampleIndex = 0;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4CounterVal;
    UINT4               u4InterfaceCount = 0;
    UINT4               u4VlanCount = 0;
    UINT4               u4SampleDisplayCount = 0;
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    tSNMP_OCTET_STRING_TYPE OctetRetvalOwner;
    UINT1               au1Temp[RMON_DESC_LEN + 1];
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    CHR1                au1TimeStr[CLI_TIME_STR_BUF_SIZE];
    tSNMP_COUNTER64_TYPE u8Value;
    CHR1                ac1Val[RMON_U8_STR_LENGTH];
    FS_UINT8            u8Val;
    UINT4               u4OverFlowValue = 0;
    UINT4               u4IfSpeed = 0;

    OctetRetvalOwner.pu1_OctetList = &au1Temp[0];
    OidValue.pu4_OidList = &au4OidValue[0];

    /* Assign the Current first index value to next index outside DO loop
     * Inside DO Loop, reverse assignment. */

    if (nmhGetFirstIndexEtherHistoryTable (&i4NextHistoryControlIndex,
                                           &i4NextetherHistorySampleIndex)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rHistory Ether table is empty\r\n");
        return (CLI_SUCCESS);
    }
    do
    {
        if (i4NextHistoryControlIndex != i4HistoryControlIndex)
        {
            CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN + 1);
            CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
            i4HistoryControlIndex = i4NextHistoryControlIndex;
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4IfSpeed = 0;
            /* To Print the Interface Index or Vlan Id value */

            if (nmhGetHistoryControlDataSource
                (i4HistoryControlIndex, &OidValue) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RMON_HISTORY_ENTRY_NOTEXISTS_ERR);
                return (CLI_FAILURE);
            }

            nmhGetHistoryControlStatus (i4HistoryControlIndex, &i4ReturnVal);

            CliPrintf (CliHandle, "\rEntry %d is ", i4HistoryControlIndex);

            RmonFormatStatus (CliHandle, i4ReturnVal);

            nmhGetHistoryControlOwner (i4HistoryControlIndex,
                                       &OctetRetvalOwner);
            CliPrintf (CliHandle, " and owned by %s\r\n",
                       OctetRetvalOwner.pu1_OctetList);

            nmhGetHistoryControlInterval (i4HistoryControlIndex,
                                          &i4HistoryControlInterval);

            /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
             * grlInterfaceOid and grlVlanOid are the Global Oid */

            RmonCheckDataSourceOIDIsVlanOrInterface (&OidValue, &u4OIDType);

            if (u4OIDType == PORT_INDEX_TYPE)
            {
                u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];

                CliPrintf (CliHandle,
                           " Monitors ifEntry.1.%u every %d second(s)\r\n",
                           u4IfIndex, i4HistoryControlInterval);
                u4InterfaceCount++;
                u4IfSpeed = 0;
                if (CfaGetIfSpeed (u4IfIndex, &u4IfSpeed) == CFA_FAILURE)
                {
                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              " RmonShowHistoryTable - "
                              " Unable to get the interface speed. \n");
                    return (CLI_FAILURE);
                }

            }
            else if (u4OIDType == VLAN_INDEX_TYPE)
            {
                u4VlanId = OidValue.pu4_OidList[grlVlanOid.u4_Length];

                CliPrintf (CliHandle,
                           " Monitors Vlan %u every %d second(s)\r\n",
                           u4VlanId, i4HistoryControlInterval);
                u4VlanCount++;
            }
            /*reseting the Sample display counts for next control index */
            u4SampleDisplayCount = 0;

            /* Get the buckets granted value from Control table for the index  */
            nmhGetHistoryControlBucketsRequested (i4HistoryControlIndex,
                                                  &i4bucketsGrantedVal);
            CliPrintf (CliHandle,
                       " Requested # of time intervals, ie buckets, is %d, \r\n",
                       i4bucketsGrantedVal);
            nmhGetHistoryControlBucketsGranted (i4HistoryControlIndex,
                                                &i4bucketsGrantedVal);
            CliPrintf (CliHandle,
                       " Granted # of time intervals, ie buckets, is %d, \r\n",
                       i4bucketsGrantedVal);

        }

        i4etherHistorySampleIndex = i4NextetherHistorySampleIndex;

        /*Statistical informations will be shown only when any samples are 
         * taken*/
        if ((u1OverView == RMON_FALSE) && (i4etherHistorySampleIndex != 0))
        {
            if (u4SampleDisplayCount < (UINT4) (i4bucketsGrantedVal))
            {
                nmhGetEtherHistoryIntervalStart (i4HistoryControlIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                MEMSET (&au1TimeStr, 0, CLI_TIME_STR_BUF_SIZE);

                UtlGetTimeStrForTicks (u4CounterVal, au1TimeStr);

                CliPrintf (CliHandle, "  Sample %d began measuring at %s\r\n",
                           i4etherHistorySampleIndex, au1TimeStr);

                if (u4IfSpeed >= CFA_ENET_SPEED_1G)
                {
                    if (ISS_HW_SUPPORTED !=
                        IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
                    {
                        nmhGetEtherHistoryOctets (i4HistoryControlIndex,
                                                  i4etherHistorySampleIndex,
                                                  &u4CounterVal);
                        CliPrintf (CliHandle, "   Received %u octets, ",
                                   u4CounterVal);
                    }
                    else
                    {
                        MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                        MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                        MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                        nmhGetEtherHistoryHighCapacityOctets
                            (i4HistoryControlIndex, i4etherHistorySampleIndex,
                             &u8Value);
                        u8Val.u4Hi = u8Value.msn;
                        u8Val.u4Lo = u8Value.lsn;
                        FSAP_U8_2STR (&u8Val, ac1Val);
                        CliPrintf (CliHandle, "   Received %s octets, ",
                                   ac1Val);

                        MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                        MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                        MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                        nmhGetEtherHistoryHighCapacityPkts
                            (i4HistoryControlIndex, i4etherHistorySampleIndex,
                             &u8Value);
                        u8Val.u4Hi = u8Value.msn;
                        u8Val.u4Lo = u8Value.lsn;
                        FSAP_U8_2STR (&u8Val, ac1Val);
                        CliPrintf (CliHandle, "%s packets,\r\n", ac1Val);
                    }
                }
                else
                {
                    nmhGetEtherHistoryOctets (i4HistoryControlIndex,
                                              i4etherHistorySampleIndex,
                                              &u4CounterVal);
                    CliPrintf (CliHandle, "   Received %u octets, ",
                               u4CounterVal);
                }

                if (u4IfSpeed >= CFA_ENET_SPEED_1G)
                {
                    if (ISS_HW_SUPPORTED !=
                        IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
                    {
                        nmhGetEtherHistoryPkts (i4HistoryControlIndex,
                                                i4etherHistorySampleIndex,
                                                &u4CounterVal);
                        CliPrintf (CliHandle, "%u packets,\r\n", u4CounterVal);
                    }
                    else
                    {
                        u4OverFlowValue = 0;
                        nmhGetEtherHistoryHighCapacityOverflowOctets
                            (i4HistoryControlIndex, i4etherHistorySampleIndex,
                             &u4OverFlowValue);
                        CliPrintf (CliHandle, "   %u overflow octets, ",
                                   u4OverFlowValue);

                        u4OverFlowValue = 0;
                        nmhGetEtherHistoryHighCapacityOverflowPkts
                            (i4HistoryControlIndex, i4etherHistorySampleIndex,
                             &u4OverFlowValue);
                        CliPrintf (CliHandle, "%u overflow packets\r\n",
                                   u4OverFlowValue);

                    }
                }
                else
                {
                    nmhGetEtherHistoryPkts (i4HistoryControlIndex,
                                            i4etherHistorySampleIndex,
                                            &u4CounterVal);
                    CliPrintf (CliHandle, "%u packets,\r\n", u4CounterVal);
                }

                nmhGetEtherHistoryBroadcastPkts (i4HistoryControlIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                CliPrintf (CliHandle, "   %u broadcast and ", u4CounterVal);

                nmhGetEtherHistoryMulticastPkts (i4HistoryControlIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                CliPrintf (CliHandle, "%u multicast packets,\r\n",
                           u4CounterVal);

                nmhGetEtherHistoryUndersizePkts (i4HistoryControlIndex,
                                                 i4etherHistorySampleIndex,
                                                 &u4CounterVal);
                CliPrintf (CliHandle, "   %u undersized and ", u4CounterVal);

                nmhGetEtherHistoryOversizePkts (i4HistoryControlIndex,
                                                i4etherHistorySampleIndex,
                                                &u4CounterVal);
                CliPrintf (CliHandle, "%u oversized packets,\r\n",
                           u4CounterVal);

                nmhGetEtherHistoryFragments (i4HistoryControlIndex,
                                             i4etherHistorySampleIndex,
                                             &u4CounterVal);
                CliPrintf (CliHandle, "   %u fragments and ", u4CounterVal);

                nmhGetEtherHistoryJabbers (i4HistoryControlIndex,
                                           i4etherHistorySampleIndex,
                                           &u4CounterVal);
                CliPrintf (CliHandle, "%u jabbers,\r\n", u4CounterVal);

                nmhGetEtherHistoryCRCAlignErrors (i4HistoryControlIndex,
                                                  i4etherHistorySampleIndex,
                                                  &u4CounterVal);
                CliPrintf (CliHandle, "   %u CRC alignment errors and ",
                           u4CounterVal);

                nmhGetEtherHistoryCollisions (i4HistoryControlIndex,
                                              i4etherHistorySampleIndex,
                                              &u4CounterVal);
                CliPrintf (CliHandle, "%u collisions,\r\n", u4CounterVal);

                nmhGetEtherHistoryDropEvents (i4HistoryControlIndex,
                                              i4etherHistorySampleIndex,
                                              &u4CounterVal);
                CliPrintf (CliHandle, "   # of dropped packet events is %u\r\n",
                           u4CounterVal);

                nmhGetEtherHistoryUtilization (i4HistoryControlIndex,
                                               i4etherHistorySampleIndex,
                                               &i4ReturnVal);
                i4PageStatus = CliPrintf (CliHandle,
                                          "   Network utilization is estimated at %d\r\n",
                                          i4ReturnVal);
            }
            u4SampleDisplayCount++;
            if ((UINT4) i4PageStatus == CLI_FAILURE)
            {
                /* Page quit 'q' pressed */
                break;
            }
        }
    }
    while ((nmhGetNextIndexEtherHistoryTable (i4HistoryControlIndex,
                                              &i4NextHistoryControlIndex,
                                              i4etherHistorySampleIndex,
                                              &i4NextetherHistorySampleIndex)
            != SNMP_FAILURE) &&
           (u4SampleDisplayCount <= (UINT4) (i4bucketsGrantedVal)));

    if (u4InterfaceCount > 0)
    {
        CliPrintf (CliHandle,
                   "Number of history collection on interface: %u\r\n",
                   u4InterfaceCount);
    }
    if (u4VlanCount > 0)
    {
        CliPrintf (CliHandle,
                   "Number of history collection on Vlan     : %u\r\n",
                   u4VlanCount);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonShowStatisticsIndex                                */
/*                                                                           */
/* Description      : This function is invoked to display RMON statistics for*/
/*                      a specific index                                     */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4StatsIndex - statistics index                     */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonShowStatisticsIndex (tCliHandle CliHandle, INT4 i4StatsIndex)
{
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */

    tSNMP_OCTET_STRING_TYPE OctetRetvalOwner;
    UINT4               u4IfIndex = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4OIDType = 0;
    INT1                i1RetVal = 0;
    UINT4               u4CounterVal = 0;
    INT4                i4ReturnVal = 0;
    UINT1               au1Temp[RMON_DESC_LEN + 1];
    UINT1               ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    tSNMP_COUNTER64_TYPE u8Value;
    CHR1                ac1Val[RMON_U8_STR_LENGTH];
    FS_UINT8            u8Val;
    UINT4               u4OverFlowValue = 0;
    UINT4               u4IfSpeed = 0;

    /* Validate the index passed. */
    i1RetVal = nmhValidateIndexInstanceEtherStatsTable (i4StatsIndex);

    if (i1RetVal == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RMON_INVALID_STATS_INDEX_ERR);
        return (CLI_FAILURE);
    }

    CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN + 1);
    CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
    CLI_MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    OctetRetvalOwner.pu1_OctetList = &au1Temp[0];
    OidValue.pu4_OidList = &au4OidValue[0];

    MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
    MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8Val, 0, sizeof (FS_UINT8));

    CliPrintf (CliHandle, "\rCollection %d ", i4StatsIndex);

    /* To Print the Interface Index or Vlan Id value */
    if (nmhGetEtherStatsDataSource (i4StatsIndex, &OidValue) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
     * grlInterfaceOid and grlVlanOid are the Global Oid */
    RmonCheckDataSourceOIDIsVlanOrInterface (&OidValue, &u4OIDType);

    if (u4OIDType == PORT_INDEX_TYPE)
    {
        u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];

        CfaCliGetIfName (u4IfIndex, (INT1 *) ai1InterfaceName);

        CliPrintf (CliHandle, "on %s ", ai1InterfaceName);

        if (CfaGetIfSpeed (u4IfIndex, &u4IfSpeed) == CFA_FAILURE)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " RmonShowStatisticsIndex - "
                      " Unable to get the interface speed. \n");
            return (CLI_FAILURE);
        }
    }
    else if (u4OIDType == VLAN_INDEX_TYPE)
    {
        u4VlanId = OidValue.pu4_OidList[grlVlanOid.u4_Length];

        CliPrintf (CliHandle, "on Vlan %u ", u4VlanId);
    }

    CliPrintf (CliHandle, "is ");

    nmhGetEtherStatsStatus (i4StatsIndex, &i4ReturnVal);
    RmonFormatStatus (CliHandle, i4ReturnVal);

    nmhGetEtherStatsOwner (i4StatsIndex, &OctetRetvalOwner);
    CliPrintf (CliHandle, "and owned by %s,\r\n",
               OctetRetvalOwner.pu1_OctetList);

    if (u4OIDType == PORT_INDEX_TYPE)
    {
        CfaCliGetIfName (u4IfIndex, (INT1 *) ai1InterfaceName);
        CliPrintf (CliHandle, "Monitors by %s interface", ai1InterfaceName);
    }
    else if (u4OIDType == VLAN_INDEX_TYPE)
    {
        CliPrintf (CliHandle, " Monitors Vlan %u", u4VlanId);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsOctets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " which has\r\n Received %u octets, ",
                       u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityOctets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle, " which has\r\n Received %s octets, ",
                       ac1Val);

            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle, "%s packets,\r\n", ac1Val);
        }
    }
    else
    {
        nmhGetEtherStatsOctets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " which has\r\n Received %u octets, ",
                   u4CounterVal);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, "%u packets, \r\n", u4CounterVal);
        }
        else
        {
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityOverflowOctets (i4StatsIndex,
                                                        &u4OverFlowValue);
            CliPrintf (CliHandle, " %u overflow octets, ", u4OverFlowValue);

            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityOverflowPkts (i4StatsIndex,
                                                      &u4OverFlowValue);
            CliPrintf (CliHandle, "%u overflow packets\r\n", u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, "%u packets, \r\n", u4CounterVal);
    }

    nmhGetEtherStatsBroadcastPkts (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u broadcast and", u4CounterVal);

    nmhGetEtherStatsMulticastPkts (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u multicast packets,\r\n", u4CounterVal);

    nmhGetEtherStatsUndersizePkts (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u undersized and", u4CounterVal);

    nmhGetEtherStatsOversizePkts (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u oversized packets,\r\n", u4CounterVal);

    nmhGetEtherStatsFragments (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u fragments and", u4CounterVal);

    nmhGetEtherStatsJabbers (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u jabbers,\r\n", u4CounterVal);

    nmhGetEtherStatsCRCAlignErrors (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u CRC alignment errors and", u4CounterVal);

    nmhGetEtherStatsCollisions (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u collisions.\r\n", u4CounterVal);

    nmhGetRmonStatsOutFCSErrors (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u out FCS errors and", u4CounterVal);

    nmhGetEtherStatsDropEvents (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " %u Drop events,\r\n", u4CounterVal);

    CliPrintf (CliHandle, " # of packets received of length (in octets):\r\n");

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts64Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 64: %u,", u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityPkts64Octets (i4StatsIndex, &u8Value);
            nmhGetEtherStatsHighCapacityOverflowPkts64Octets (i4StatsIndex,
                                                              &u4OverFlowValue);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle, " 64: %s and overflow 64 : %u \r\n",
                       ac1Val, u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts64Octets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " 64: %u,", u4CounterVal);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts65to127Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 65-127: %u,", u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityPkts65to127Octets (i4StatsIndex,
                                                           &u8Value);
            nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets (i4StatsIndex,
                                                                   &u4OverFlowValue);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle, " 65-127: %s and overflow 65-127 : %u \r\n",
                       ac1Val, u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts65to127Octets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " 65-127: %u,", u4CounterVal);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts128to255Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 128-255: %u,\r\n", u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityPkts128to255Octets (i4StatsIndex,
                                                            &u8Value);
            nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets
                (i4StatsIndex, &u4OverFlowValue);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle, " 128-255: %s and overflow 128-255 : %u \r\n",
                       ac1Val, u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts128to255Octets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " 128-255: %u,\r\n", u4CounterVal);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts256to511Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 256-511: %u,", u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityPkts256to511Octets (i4StatsIndex,
                                                            &u8Value);
            nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets
                (i4StatsIndex, &u4OverFlowValue);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle, " 256-511: %s and overflow 256-511 : %u \r\n",
                       ac1Val, u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts256to511Octets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " 256-511: %u,", u4CounterVal);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts512to1023Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 512-1023: %u,", u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityPkts512to1023Octets (i4StatsIndex,
                                                             &u8Value);
            nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets
                (i4StatsIndex, &u4OverFlowValue);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle,
                       " 512-1023: %s and overflow 512-1023 : %u \r\n", ac1Val,
                       u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts512to1023Octets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " 512-1023: %u,", u4CounterVal);
    }

    if (u4IfSpeed >= CFA_ENET_SPEED_1G)
    {
        if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
        {
            nmhGetEtherStatsPkts1024to1518Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 1024-1518: %u,\r\n", u4CounterVal);
        }
        else
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            u4OverFlowValue = 0;
            nmhGetEtherStatsHighCapacityPkts1024to1518Octets (i4StatsIndex,
                                                              &u8Value);
            nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets
                (i4StatsIndex, &u4OverFlowValue);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            CliPrintf (CliHandle,
                       " 1024-1518: %s and overflow 1024-1518 : %u \r\n",
                       ac1Val, u4OverFlowValue);
        }
    }
    else
    {
        nmhGetEtherStatsPkts1024to1518Octets (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " 1024-1518: %u,\r\n", u4CounterVal);
    }

    nmhGetRmonStatsPkts1519to1522Octets (i4StatsIndex, &u4CounterVal);
    CliPrintf (CliHandle, " 1519-1522: %u\r\n", u4CounterVal);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonShowStatisticsTable                                */
/*                                                                           */
/* Description      : This function is invoked to display RMON statistics    */
/*                    table                                                  */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonShowStatisticsTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OctetRetvalOwner;
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    INT1                i1RetVal = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4OIDType = 0;
    INT4                i4StatsIndex = 0;
    INT4                i4nextEtherStatsIndex = 0;
    INT4                i4ReturnVal = 0;
    UINT4               u4CounterVal = 0;
    UINT4               u4PagingStatus;
    UINT4               u4InterfaceCount = 0;
    UINT4               u4VlanCount = 0;
    UINT1               au1Temp[RMON_DESC_LEN + 1];
    UINT1               ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    tSNMP_COUNTER64_TYPE u8Value;
    CHR1                ac1Val[RMON_U8_STR_LENGTH];
    FS_UINT8            u8Val;
    UINT4               u4OverFlowValue = 0;
    UINT4               u4IfSpeed = 0;

    if ((i1RetVal = nmhGetFirstIndexEtherStatsTable (&i4StatsIndex))
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rEthernet Statistics table is empty\r\n");
        return (CLI_SUCCESS);
    }
    OctetRetvalOwner.pu1_OctetList = &au1Temp[0];
    OidValue.pu4_OidList = &au4OidValue[0];
    /* Outside the DO loop, assign First Index to Next index
     * Inside the DO loop, reverse assignment. */

    i4nextEtherStatsIndex = i4StatsIndex;

    do
    {
        CLI_MEMSET (au1Temp, 0, RMON_DESC_LEN + 1);
        CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
        CLI_MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
        i4StatsIndex = i4nextEtherStatsIndex;
        u4OIDType = 0;
        MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
        MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
        MEMSET (&u8Val, 0, sizeof (FS_UINT8));

        /* To Print the Interface Index or Vlan Id value */
        nmhGetEtherStatsDataSource (i4StatsIndex, &OidValue);
        /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
         * grlInterfaceOid and grlVlanOid are the Global Oid */

        CliPrintf (CliHandle, "\rCollection %d ", i4StatsIndex);

        RmonCheckDataSourceOIDIsVlanOrInterface (&OidValue, &u4OIDType);

        if (u4OIDType == PORT_INDEX_TYPE)
        {
            u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];

            CfaCliGetIfName (u4IfIndex, (INT1 *) ai1InterfaceName);

            CliPrintf (CliHandle, "on %s ", ai1InterfaceName);

            u4InterfaceCount++;

            if (CfaGetIfSpeed (u4IfIndex, &u4IfSpeed) == CFA_FAILURE)
            {
                RMON_DBG (CRITICAL_TRC | MEM_TRC,
                          " RmonShowStatisticsTable - "
                          " Unable to get the interface speed. \n");
                return (CLI_FAILURE);
            }
        }
        else if (u4OIDType == VLAN_INDEX_TYPE)
        {
            u4VlanId = OidValue.pu4_OidList[grlVlanOid.u4_Length];

            CliPrintf (CliHandle, "on Vlan %u ", u4VlanId);

            u4VlanCount++;
        }

        CliPrintf (CliHandle, "is ");

        nmhGetEtherStatsStatus (i4StatsIndex, &i4ReturnVal);
        RmonFormatStatus (CliHandle, i4ReturnVal);

        nmhGetEtherStatsOwner (i4StatsIndex, &OctetRetvalOwner);
        CliPrintf (CliHandle, "and owned by %s,\r\n",
                   OctetRetvalOwner.pu1_OctetList);

        if (u4OIDType == PORT_INDEX_TYPE)
        {
            CfaCliGetIfName (u4IfIndex, (INT1 *) ai1InterfaceName);
            CliPrintf (CliHandle, "Monitors by %s interface", ai1InterfaceName);
        }
        else if (u4OIDType == VLAN_INDEX_TYPE)
        {
            CliPrintf (CliHandle, " Monitors Vlan %u", u4VlanId);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsOctets (i4StatsIndex, &u4CounterVal);
                CliPrintf (CliHandle, " which has\r\n Received %u octets, ",
                           u4CounterVal);
            }
            else
            {
                nmhGetEtherStatsHighCapacityOctets (i4StatsIndex, &u8Value);
                nmhGetEtherStatsHighCapacityOverflowOctets (i4StatsIndex,
                                                            &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle, " which has\r\n Received %s octets, "
                           " overflow octets : %u\r\n", ac1Val,
                           u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsOctets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " which has\r\n Received %u octets, ",
                       u4CounterVal);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts (i4StatsIndex, &u4CounterVal);
                CliPrintf (CliHandle, "%u packets, \r\n", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts (i4StatsIndex, &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts (i4StatsIndex,
                                                          &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle,
                           " %s packets and overflow packets : %u \r\n", ac1Val,
                           u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, "%u packets, \r\n", u4CounterVal);
        }

        nmhGetEtherStatsBroadcastPkts (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u broadcast and", u4CounterVal);

        nmhGetEtherStatsMulticastPkts (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u multicast packets,\r\n", u4CounterVal);

        nmhGetEtherStatsUndersizePkts (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u undersized and", u4CounterVal);

        nmhGetEtherStatsOversizePkts (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u oversized packets,\r\n", u4CounterVal);

        nmhGetEtherStatsFragments (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u fragments and", u4CounterVal);

        nmhGetEtherStatsJabbers (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u jabbers,\r\n", u4CounterVal);

        nmhGetEtherStatsCRCAlignErrors (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u CRC alignment errors and", u4CounterVal);

        nmhGetEtherStatsCollisions (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u collisions.\r\n", u4CounterVal);

        nmhGetRmonStatsOutFCSErrors (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u out FCS errors and", u4CounterVal);

        nmhGetEtherStatsDropEvents (i4StatsIndex, &u4CounterVal);
        CliPrintf (CliHandle, " %u Drop events,\r\n", u4CounterVal);

        CliPrintf (CliHandle,
                   " # of packets received of length (in octets):\r\n");

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts64Octets (i4StatsIndex, &u4CounterVal);
                CliPrintf (CliHandle, " 64: %u,", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts64Octets (i4StatsIndex,
                                                          &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts64Octets (i4StatsIndex,
                                                                  &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle, " 64: %s and overflow 64 : %u \r\n",
                           ac1Val, u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts64Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 64: %u,", u4CounterVal);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts65to127Octets (i4StatsIndex, &u4CounterVal);
                CliPrintf (CliHandle, " 65-127: %u,", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts65to127Octets (i4StatsIndex,
                                                               &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets
                    (i4StatsIndex, &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle,
                           " 65-127: %s and overflow 65-127 : %u \r\n", ac1Val,
                           u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts65to127Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 65-127: %u,", u4CounterVal);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts128to255Octets (i4StatsIndex,
                                                    &u4CounterVal);
                CliPrintf (CliHandle, " 128-255: %u,\r\n", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts128to255Octets (i4StatsIndex,
                                                                &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets
                    (i4StatsIndex, &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle,
                           " 128-255: %s and overflow 128-255 : %u \r\n",
                           ac1Val, u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts128to255Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 128-255: %u,\r\n", u4CounterVal);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts256to511Octets (i4StatsIndex,
                                                    &u4CounterVal);
                CliPrintf (CliHandle, " 256-511: %u,", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts256to511Octets (i4StatsIndex,
                                                                &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets
                    (i4StatsIndex, &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle,
                           " 256-511: %s and overflow 256-511 : %u \r\n",
                           ac1Val, u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts256to511Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 256-511: %u,", u4CounterVal);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts512to1023Octets (i4StatsIndex,
                                                     &u4CounterVal);
                CliPrintf (CliHandle, " 512-1023: %u,", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts512to1023Octets (i4StatsIndex,
                                                                 &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets
                    (i4StatsIndex, &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle,
                           " 512-1023: %s and overflow 512-1023 : %u \r\n",
                           ac1Val, u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts512to1023Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 512-1023: %u,", u4CounterVal);
        }

        if (u4IfSpeed >= CFA_ENET_SPEED_1G)
        {
            if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_HIGH_CAP_CNTR))
            {
                nmhGetEtherStatsPkts1024to1518Octets (i4StatsIndex,
                                                      &u4CounterVal);
                CliPrintf (CliHandle, " 1024-1518: %u,\r\n", u4CounterVal);
            }
            else
            {
                MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
                MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
                MEMSET (&u8Val, 0, sizeof (FS_UINT8));
                u4OverFlowValue = 0;
                nmhGetEtherStatsHighCapacityPkts1024to1518Octets (i4StatsIndex,
                                                                  &u8Value);
                nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets
                    (i4StatsIndex, &u4OverFlowValue);
                u8Val.u4Hi = u8Value.msn;
                u8Val.u4Lo = u8Value.lsn;
                FSAP_U8_2STR (&u8Val, ac1Val);
                CliPrintf (CliHandle,
                           " 1024-1518: %s and overflow 1024-1518 : %u \r\n",
                           ac1Val, u4OverFlowValue);
            }
        }
        else
        {
            nmhGetEtherStatsPkts1024to1518Octets (i4StatsIndex, &u4CounterVal);
            CliPrintf (CliHandle, " 1024-1518: %u,\r\n", u4CounterVal);
        }

        nmhGetRmonStatsPkts1519to1522Octets (i4StatsIndex, &u4CounterVal);
        u4PagingStatus = (UINT4) CliPrintf
            (CliHandle, " 1519-1522: %u\r\n", u4CounterVal);

        if (u4PagingStatus == CLI_FAILURE)
        {
            /*User pressed 'q' at more prompt when CLI is waiting for
             *input keys to continue with display, so break processing
             */
            break;
        }
        u4IfSpeed = 0;
    }
    while (nmhGetNextIndexEtherStatsTable
           (i4StatsIndex, &i4nextEtherStatsIndex) == SNMP_SUCCESS);
    if (u4InterfaceCount > 0)
    {
        CliPrintf (CliHandle,
                   "Number of statistics collection on interface: %u\r\n",
                   u4InterfaceCount);
    }
    if (u4VlanCount > 0)
    {
        CliPrintf (CliHandle,
                   "Number of statistics collection on Vlan     : %u\r\n",
                   u4VlanCount);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetStatus                                          */
/*                                                                           */
/* Description      : This function is invoked to enable or disable RMON     */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4RmonStatus - RMON_ENABLE or RMON_DISABLE          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetStatus (tCliHandle CliHandle, INT4 i4RmonStatus)
{
    UINT4               u4ErrorCode = 0;
    /* To enable/disable RMON */

    if (gRmonEnableStatusFlag == (UINT4) i4RmonStatus)
    {
        return (CLI_SUCCESS);
    }

    if (nmhTestv2RmonEnableStatus (&u4ErrorCode, i4RmonStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetRmonEnableStatus (i4RmonStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetAlarm                                           */
/*                                                                           */
/* Description      : This function is invoked to set an alarm on MIB object */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pRmonAlarmentry -                                   */
/*                       pointer to tRmonAlarmEntry                          */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetAlarm (tCliHandle CliHandle, tRmonAlarmEntry * pRmonAlarmEntry)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE ownerData;
    tSNMP_OID_TYPE     *pAlrmVariable;
    tRmonAlarmNode     *pAlarmNode = NULL;

    /* Verify the existance of this entry */
    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable,
                                      pRmonAlarmEntry->i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        CLI_SET_ERR (CLI_RMON_ALARM_EXISTS_ERR);
        return (CLI_FAILURE);
    }

    /* To test & set the initial Status of the entry to "CREATE_REQUEST" */
    if ((nmhTestv2AlarmStatus (&u4ErrorCode, pRmonAlarmEntry->i4AlarmIndex,
                               pRmonAlarmEntry->i4RmonAlmStatus1))
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex,
                           pRmonAlarmEntry->i4RmonAlmStatus1) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* To convert Owner field from String to SNMP_OCTET_STRING */
    ownerData.i4_Length = (INT4) STRLEN (pRmonAlarmEntry->au1RmonAlmOwner);
    if (ownerData.i4_Length != 0)
    {
        ownerData.pu1_OctetList = pRmonAlarmEntry->au1RmonAlmOwner;
        /* To test & set the Owner field in Alarm table */
        i1RetVal = nmhTestv2AlarmOwner (&u4ErrorCode,
                                        pRmonAlarmEntry->i4AlarmIndex,
                                        &ownerData);

        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetAlarmOwner (pRmonAlarmEntry->i4AlarmIndex, &ownerData)
            != SNMP_SUCCESS)
        {
            nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }

    }

    /* To convert AlarmVariable from String to tSNMP_OID_TYPE */

    /* Call the RMON function to convert string to tSNMP_OID_TYPE */
    /* The Memory required for pAlrmVariable is allocated in the */
    /* below function. Hence ensure you free the pAlrmvariable Memory */
    /* after usage and fialure conditions. */
    pAlrmVariable =
        RmonMakeObjIdFromDotNew (pRmonAlarmEntry->au1RmonAlmVariable);

    if (pAlrmVariable == NULL)
    {
        /* delete the entry and come out */
        CliPrintf (CliHandle, "\r%% Unknown MIB object: %s\r\n",
                   pRmonAlarmEntry->au1RmonAlmVariable);
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);

    }

    /* To test & set the Alarm Variable in Alarm Table */
    i1RetVal = nmhTestv2AlarmVariable (&u4ErrorCode,
                                       pRmonAlarmEntry->i4AlarmIndex,
                                       pAlrmVariable);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* Free the memory used by AlarmVariable */
        rmon_free_oid (pAlrmVariable);
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetAlarmVariable (pRmonAlarmEntry->i4AlarmIndex, pAlrmVariable)
        != SNMP_SUCCESS)
    {
        /* Free the memory used by AlarmVariable */
        rmon_free_oid (pAlrmVariable);
        /* delete the entry and come out */
        CLI_SET_ERR (CLI_RMON_CONFIG_ERR);
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* Free the memory used by AlarmVariable */
    rmon_free_oid (pAlrmVariable);
    /* To test & set the Alarm Interval in Alarm Table */
    i1RetVal = nmhTestv2AlarmInterval (&u4ErrorCode,
                                       pRmonAlarmEntry->i4AlarmIndex,
                                       pRmonAlarmEntry->i4RmonAlmInterval);

    if (i1RetVal == SNMP_FAILURE)
    {
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetAlarmInterval (pRmonAlarmEntry->i4AlarmIndex,
                             pRmonAlarmEntry->i4RmonAlmInterval) !=
        SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Alarm Sample Type in Alarm Table */
    i1RetVal = nmhTestv2AlarmSampleType (&u4ErrorCode,
                                         pRmonAlarmEntry->i4AlarmIndex,
                                         pRmonAlarmEntry->i4RmonAlmSampleType);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetAlarmSampleType (pRmonAlarmEntry->i4AlarmIndex,
                               pRmonAlarmEntry->i4RmonAlmSampleType)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Alarm Startup Alarm in Alarm Table */
    i1RetVal = nmhTestv2AlarmStartupAlarm (&u4ErrorCode,
                                           pRmonAlarmEntry->i4AlarmIndex,
                                           pRmonAlarmEntry->
                                           i4RmonAlmStartupAlm);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetAlarmStartupAlarm (pRmonAlarmEntry->i4AlarmIndex,
                                 pRmonAlarmEntry->i4RmonAlmStartupAlm)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Alarm Rising Threshold in Alarm Table */
    i1RetVal = nmhTestv2AlarmRisingThreshold (&u4ErrorCode,
                                              pRmonAlarmEntry->i4AlarmIndex,
                                              pRmonAlarmEntry->
                                              i4RmonAlmRiseThrshd);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetAlarmRisingThreshold (pRmonAlarmEntry->i4AlarmIndex,
                                    pRmonAlarmEntry->i4RmonAlmRiseThrshd)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Alarm Rising Event Index in Alarm Table */
    i1RetVal = nmhTestv2AlarmRisingEventIndex (&u4ErrorCode,
                                               pRmonAlarmEntry->i4AlarmIndex,
                                               pRmonAlarmEntry->
                                               i4RmonAlmRiseEvtIndex);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetAlarmRisingEventIndex (pRmonAlarmEntry->i4AlarmIndex,
                                     pRmonAlarmEntry->i4RmonAlmRiseEvtIndex)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Alarm Falling threshold in Alarm Table */
    i1RetVal = nmhTestv2AlarmFallingThreshold (&u4ErrorCode,
                                               pRmonAlarmEntry->i4AlarmIndex,
                                               pRmonAlarmEntry->
                                               i4RmonAlmFallThrshd);
    if (i1RetVal == SNMP_FAILURE)
    {
        /*  delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetAlarmFallingThreshold (pRmonAlarmEntry->i4AlarmIndex,
                                     pRmonAlarmEntry->i4RmonAlmFallThrshd)
        != SNMP_SUCCESS)
    {
        /*  delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Alarm Falling Event Index in Alarm Table */
    i1RetVal = nmhTestv2AlarmFallingEventIndex (&u4ErrorCode,
                                                pRmonAlarmEntry->i4AlarmIndex,
                                                pRmonAlarmEntry->
                                                i4RmonAlmFallEvtIndex);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetAlarmFallingEventIndex (pRmonAlarmEntry->i4AlarmIndex,
                                      pRmonAlarmEntry->i4RmonAlmFallEvtIndex)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the final status of the entry to "VALID" */
    i1RetVal =
        nmhTestv2AlarmStatus (&u4ErrorCode, pRmonAlarmEntry->i4AlarmIndex,
                              pRmonAlarmEntry->i4RmonAlmStatus2);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex,
                           pRmonAlarmEntry->i4RmonAlmStatus2) != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetAlarmStatus (pRmonAlarmEntry->i4AlarmIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     Function Name    : RmonShowRunningConfig                              */
/*                                                                           */
/*     Description      : This function displays the current configuration   */
/*                        information of RMON                                */
/*                                                                           */
/*     Input Parameters : CliHandle - CliContext ID                          */
/*                                                                           */
/*     Output Parameters: None                                               */
/*                                                                           */
/*     Return Value     : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RmonShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UINT1               u1Flag = 1;
    INT4                i4Val;
    CliRegisterLock (CliHandle, RmonLock, RmonUnLock);
    RMON_LOCK ();

    /* If module is not started then return without porocess */
    if (gRmonSystemControlFlag == RMON_SYS_SHUTDOWN)
    {
        RMON_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    if (u4Module == ISS_RMON_SHOW_RUNNING_CONFIG)
    {
        RMON_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        for (i4Val = 1; i4Val <= BRG_MAX_PHY_PLUS_LOG_PORTS; i4Val++)
        {
            RmonShowRunningConfigInterface (CliHandle, u1Flag, i4Val);
        }
        CliRegisterLock (CliHandle, RmonLock, RmonUnLock);
        RMON_LOCK ();
    }
    for (i4Val = 1; i4Val <= VLAN_DEV_MAX_NUM_VLAN; i4Val++)
    {
        RmonShowRunningConfigVlan (CliHandle, u1Flag, (UINT4) i4Val);
    }
    RmonShowRunningConfigGlobal (CliHandle);
    RMON_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetNoAlarm                                         */
/*                                                                           */
/* Description      : This function is invoked to delete an alarm on MIB     */
/*                    object                                                 */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4AlarmIndex - Alarm Index                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetNoAlarm (tCliHandle CliHandle, INT4 i4AlarmIndex)
{
    INT4                i4Status = 4;    /* Invalid Status */
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;

    /* Verify the existance of this entry */

    i1RetVal = nmhValidateIndexInstanceAlarmTable (i4AlarmIndex);

    if (i1RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_INVALID_ALARM_INDEX_ERR);
        return (CLI_FAILURE);
    }

    /* For the given entry in the Table, Set the STATUS to INVALID. */
    /* Test & set the Status Field */
    i1RetVal = nmhTestv2AlarmStatus (&u4ErrorCode, i4AlarmIndex, i4Status);

    if (i1RetVal != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetAlarmStatus (i4AlarmIndex, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetEvent                                           */
/*                                                                           */
/* Description      : This function is invoked to set an event               */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pRmonEventEntry - pointer to event entry            */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetEvent (tCliHandle CliHandle, tRmonEventEntry * pRmonEventEntry)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE eventDescr;    /* Event Description */
    tSNMP_OCTET_STRING_TYPE eventComType;    /* Event Community */
    tSNMP_OCTET_STRING_TYPE ownerData;    /* Owner Data */
    tRmonEventNode     *pEventNode = NULL;
    UNUSED_PARAM (CliHandle);
    /* Verify the existance of this entry */
    pEventNode = HashSearchEventNode (gpRmonEventHashTable,
                                      pRmonEventEntry->i4EventIndex);

    if (pEventNode != NULL)
    {
        CLI_SET_ERR (CLI_RMON_EVENT_ENTRY_EXISTS_ERR);
        return (CLI_FAILURE);
    }
    /* To test & set the initial Status of the entry to "CREATE_REQUEST" */
    i1RetVal = nmhTestv2EventStatus (&u4ErrorCode,
                                     pRmonEventEntry->i4EventIndex,
                                     pRmonEventEntry->i4RmonEventStatus1);

    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetEventStatus (pRmonEventEntry->i4EventIndex,
                           pRmonEventEntry->i4RmonEventStatus1) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_SET_EVENT);
        return (CLI_FAILURE);
    }
    /* To convert Owner field from String to SNMP_OCTET_STRING */

    ownerData.i4_Length = (INT4) STRLEN (pRmonEventEntry->au1RmonEventOwner);
    if (ownerData.i4_Length != 0)
    {
        ownerData.pu1_OctetList = pRmonEventEntry->au1RmonEventOwner;

        /* To test & set the owner of the entry in Event Table */
        i1RetVal = nmhTestv2EventOwner (&u4ErrorCode,
                                        pRmonEventEntry->i4EventIndex,
                                        &ownerData);

        if (i1RetVal == SNMP_FAILURE)
        {
            /* delete the entry and come out */
            nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }
        if (nmhSetEventOwner (pRmonEventEntry->i4EventIndex, &ownerData)
            != SNMP_SUCCESS)
        {
            /* delete the entry and come out */
            CLI_SET_ERR (CLI_RMON_SET_EVENT);
            nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }
    }

    /* To convert Event Description from String to SNMP_OCTET_STRING */
    eventDescr.i4_Length = (INT4) STRLEN (pRmonEventEntry->au1RmonEventDescr);
    if (eventDescr.i4_Length != 0)
    {
        eventDescr.pu1_OctetList = pRmonEventEntry->au1RmonEventDescr;

        /* To test & set the Event Description for the event */
        i1RetVal = nmhTestv2EventDescription (&u4ErrorCode,
                                              pRmonEventEntry->i4EventIndex,
                                              &eventDescr);

        if (i1RetVal == SNMP_FAILURE)
        {
            /* delete the entry and come out */
            nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }
        if (nmhSetEventDescription (pRmonEventEntry->i4EventIndex, &eventDescr)
            != SNMP_SUCCESS)
        {
            /* delete the entry and come out */
            CLI_SET_ERR (CLI_RMON_SET_EVENT);
            nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }

    }

    /* To test & set the Event Type of the entry */
    i1RetVal = nmhTestv2EventType (&u4ErrorCode, pRmonEventEntry->i4EventIndex,
                                   pRmonEventEntry->i4RmonEventType);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetEventType (pRmonEventEntry->i4EventIndex,
                         pRmonEventEntry->i4RmonEventType) != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        CLI_SET_ERR (CLI_RMON_SET_EVENT);
        nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To convert Event Community from String to SNMP_OCTET_STRING */
    eventComType.i4_Length =
        (INT4) STRLEN (pRmonEventEntry->au1RmonEventCommunity);

    if (eventComType.i4_Length != 0)
    {
        eventComType.pu1_OctetList = pRmonEventEntry->au1RmonEventCommunity;

        /* To test & set the Event Community in Event table */
        i1RetVal = nmhTestv2EventCommunity (&u4ErrorCode,
                                            pRmonEventEntry->i4EventIndex,
                                            &eventComType);

        if (i1RetVal == SNMP_FAILURE)
        {
            /* delete the entry and come out */
            nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }

        if (nmhSetEventCommunity (pRmonEventEntry->i4EventIndex, &eventComType)
            != SNMP_SUCCESS)
        {
            /* delete the entry and come out */
            nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
            CLI_SET_ERR (CLI_RMON_SET_EVENT);
            return (CLI_FAILURE);
        }

    }
    /* To test & set the final status of the entry to "VALID" */
    i1RetVal =
        nmhTestv2EventStatus (&u4ErrorCode, pRmonEventEntry->i4EventIndex,
                              pRmonEventEntry->i4RmonEventStatus2);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetEventStatus (pRmonEventEntry->i4EventIndex,
                           pRmonEventEntry->i4RmonEventStatus2) != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetEventStatus (pRmonEventEntry->i4EventIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetNoEvent                                         */
/*                                                                           */
/* Description      : This function is invoked to delete an event            */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4EventIndex - Event Index                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetNoEvent (tCliHandle CliHandle, INT4 i4EventIndex)
{
    /* Invalid Status */

    INT1                i1RetVal = 0;
    INT4                i4Status = 4;
    UINT4               u4ErrorCode = 0;

    /* Verify the existance of this entry */

    i1RetVal = nmhValidateIndexInstanceEventTable (i4EventIndex);

    if (i1RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_EVENT_NOTEXISTS_ERR);
        return (CLI_FAILURE);
    }

    /* For the given entry in the Table, Set the STATUS to INVALID. */
    /* Test & set the Status Field */
    i1RetVal = nmhTestv2EventStatus (&u4ErrorCode, i4EventIndex, i4Status);

    if (i1RetVal != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    i1RetVal = nmhSetEventStatus (i4EventIndex, i4Status);
    if (i1RetVal != SNMP_SUCCESS)
    {
        if (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS)
        {
            if (u4ErrorCode != CLI_RMON_DEPENDENT_ALARM)
            {

                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            else if (u4ErrorCode == CLI_RMON_DEPENDENT_ALARM)
            {
                return CLI_FAILURE;
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetCollectionHistory                               */
/*                                                                           */
/* Description      : This function is invoked to enable history collection  */
/*                    for specified number of buckets and time period        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pRmonEthHistCtrlEntry - pointer to collection       */
/*                                               history entry               */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetCollectionHistory (tCliHandle CliHandle,
                          tRmonEthHistCtrlEntry * pRmonEthHistCtrlEntry)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    tSNMP_OCTET_STRING_TYPE ownerData;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    UNUSED_PARAM (CliHandle);

    /* Verify the existance of this entry */
    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      pRmonEthHistCtrlEntry->
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        CLI_SET_ERR (CLI_RMON_HISTORY_INDEX_EXISTS_ERR);
        return (CLI_FAILURE);
    }
    /* To test & set the initial Status of the entry to "CREATE_REQUEST" */
    i1RetVal = nmhTestv2HistoryControlStatus (&u4ErrorCode,
                                              pRmonEthHistCtrlEntry->
                                              i4HistoryControlIndex,
                                              pRmonEthHistCtrlEntry->
                                              i4RmonEthHistCtrlStatus1);

    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetHistoryControlStatus
        (pRmonEthHistCtrlEntry->i4HistoryControlIndex,
         pRmonEthHistCtrlEntry->i4RmonEthHistCtrlStatus1) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_HIST);
        return (CLI_FAILURE);
    }

    ownerData.i4_Length =
        (INT4) STRLEN (pRmonEthHistCtrlEntry->au1RmonEthHistCtrlOwner);
    if (ownerData.i4_Length != 0)
    {
        ownerData.pu1_OctetList =
            pRmonEthHistCtrlEntry->au1RmonEthHistCtrlOwner;

        /* To test & set the Owner field in History Control table */
        i1RetVal = nmhTestv2HistoryControlOwner (&u4ErrorCode,
                                                 pRmonEthHistCtrlEntry->
                                                 i4HistoryControlIndex,
                                                 &ownerData);

        if (i1RetVal == SNMP_FAILURE)
        {
            /* delete the entry and come out */
            nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                        i4HistoryControlIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }

        if (nmhSetHistoryControlOwner
            (pRmonEthHistCtrlEntry->i4HistoryControlIndex,
             &ownerData) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_RMON_SET_COLLECTION_HIST);

            /* delete the entry and come out */
            nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                        i4HistoryControlIndex, RMON_INVALID);
            return (CLI_FAILURE);
        }

    }

    /* To convert Interface Index and Vlan Id to tSNMP_OID_TYPE */
    /* Calling the RMON Util function to convert Interface Index or Vlan Id to
     * tSNMP_OID_TYPE
     * Need to convert the i4RmonEtherIfIndex to UINT1 */

    MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
    OidValue.pu4_OidList = &au4OidValue[0];

    RmonFormDataSourceOid (&OidValue,
                           ((UINT4) pRmonEthHistCtrlEntry->
                            i4RmonEtherPortOrVlanIndex),
                           ((UINT4) pRmonEthHistCtrlEntry->
                            i4RmonEthHisIndexType));

    /* To test & set the Data Source field in History Control Table */
    i1RetVal = nmhTestv2HistoryControlDataSource (&u4ErrorCode,
                                                  pRmonEthHistCtrlEntry->
                                                  i4HistoryControlIndex,
                                                  &OidValue);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetHistoryControlDataSource
        (pRmonEthHistCtrlEntry->i4HistoryControlIndex,
         &OidValue) != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_HIST);
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the Bucketsrequested variable in History Control Table */
    i1RetVal = nmhTestv2HistoryControlBucketsRequested (&u4ErrorCode,
                                                        pRmonEthHistCtrlEntry->
                                                        i4HistoryControlIndex,
                                                        pRmonEthHistCtrlEntry->
                                                        i4RmonEthHistBucketsReqstd);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetHistoryControlBucketsRequested (pRmonEthHistCtrlEntry->
                                              i4HistoryControlIndex,
                                              pRmonEthHistCtrlEntry->
                                              i4RmonEthHistBucketsReqstd)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_HIST);
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the History Control Interval  in history Control Table */
    i1RetVal = nmhTestv2HistoryControlInterval (&u4ErrorCode,
                                                pRmonEthHistCtrlEntry->
                                                i4HistoryControlIndex,
                                                pRmonEthHistCtrlEntry->
                                                i4RmonEthHistCtrlInterval);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetHistoryControlInterval
        (pRmonEthHistCtrlEntry->i4HistoryControlIndex,
         pRmonEthHistCtrlEntry->i4RmonEthHistCtrlInterval) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_HIST);
        /* delete the entry and come out */
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the final status of the entry to "VALID" */
    i1RetVal = nmhTestv2HistoryControlStatus (&u4ErrorCode,
                                              pRmonEthHistCtrlEntry->
                                              i4HistoryControlIndex,
                                              pRmonEthHistCtrlEntry->
                                              i4RmonEthHistCtrlStatus2);

    if (i1RetVal == SNMP_FAILURE)
    {
        /* delete the entry and come out */
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    if (nmhSetHistoryControlStatus
        (pRmonEthHistCtrlEntry->i4HistoryControlIndex,
         pRmonEthHistCtrlEntry->i4RmonEthHistCtrlStatus2) != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        nmhSetHistoryControlStatus (pRmonEthHistCtrlEntry->
                                    i4HistoryControlIndex, RMON_INVALID);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetNoCollectionHistory                             */
/*                                                                           */
/* Description      : This function is invoked to enable history collection  */
/*                    for specified number of buckets and time period        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pRmonEthHistCtrlEntry - pointer to collection       */
/*                                               history entry               */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetNoCollectionHistory (tCliHandle CliHandle,
                            tRmonEthHistCtrlEntry * pRmonEthHistCtrlEntry)
{

    INT4                i4Status = RMON_INVALID;    /* Invalid Status */
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;
    tSNMP_OID_TYPE      CurrentOidValue;    /* For Data Source */
    tSNMP_OID_TYPE      PreviousOidValue;    /* For Data Source */
    UINT4               au4CurrentOidValue[RMON_MAX_OID_LEN];
    UINT4               au4PreviousOidValue[RMON_MAX_OID_LEN];
    INT4                i4HistoryControlIndex;

    /* Validate the given index */

    i4HistoryControlIndex = pRmonEthHistCtrlEntry->i4HistoryControlIndex;

    i1RetVal =
        nmhValidateIndexInstanceHistoryControlTable (i4HistoryControlIndex);

    if (i1RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_INVALID_HISTORY_INDEX_ERR);
        return (CLI_FAILURE);
    }

    /* Check if the History Index to be deleted is created only for that 
     * particular interface or Vlan, and if so, then delete the stats index*/

    MEMSET (au4CurrentOidValue, 0, (RMON_MAX_OID_LEN * sizeof (UINT4)));
    MEMSET (au4PreviousOidValue, 0, (RMON_MAX_OID_LEN * sizeof (UINT4)));

    CurrentOidValue.pu4_OidList = au4CurrentOidValue;
    PreviousOidValue.pu4_OidList = au4PreviousOidValue;

    RmonFormDataSourceOid (&CurrentOidValue,
                           (UINT4) pRmonEthHistCtrlEntry->
                           i4RmonEtherPortOrVlanIndex,
                           (UINT4) pRmonEthHistCtrlEntry->
                           i4RmonEthHisIndexType);

    /*  Get the DataSource for the passed statistics index */
    i1RetVal =
        nmhGetHistoryControlDataSource (i4HistoryControlIndex,
                                        &PreviousOidValue);

    if (i1RetVal == SNMP_SUCCESS)
    {

        if (MEMCMP (PreviousOidValue.pu4_OidList,
                    CurrentOidValue.pu4_OidList, RMON_MAX_OID_LEN) != 0)
        {

            CLI_SET_ERR (CLI_RMON_HIST_UNMATCHING_INTERFACE_OR_VLAN_ERR);
            return (CLI_FAILURE);

        }

        /* For the given entry in the Table, Set the STATUS to INVALID. */
        /* Test & set the Status Field */
        i1RetVal = nmhTestv2HistoryControlStatus (&u4ErrorCode,
                                                  i4HistoryControlIndex,
                                                  i4Status);

        if (i1RetVal != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetHistoryControlStatus (i4HistoryControlIndex,
                                        i4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetCollectionStats                                 */
/*                                                                           */
/* Description      : This function is invoked to enable statistics          */
/*                    collection on an interface                             */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pRmonEthStatsEntry - pointer to collection          */
/*                                               statistics entry            */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetCollectionStats (tCliHandle CliHandle,
                        tRmonEthStatsEntry * pRmonEthStatsEntry)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE ownerData;    /* This is of type "SNMP_OCTET_STRING */
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    UNUSED_PARAM (CliHandle);
    /* Verify this entry already exists */
    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                pRmonEthStatsEntry->
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        CLI_SET_ERR (CLI_RMON_STATS_ENTRY_EXISTS_ERR);
        return (CLI_FAILURE);
    }

    /* To test & set the initial Status of the entry to "CREATE_REQUEST" */
    i1RetVal = nmhTestv2EtherStatsStatus (&u4ErrorCode,
                                          pRmonEthStatsEntry->i4EtherStatsIndex,
                                          pRmonEthStatsEntry->
                                          i4RmonEtherStatus1);

    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                pRmonEthStatsEntry->i4RmonEtherStatus1)
        != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_STAT);
        return (CLI_FAILURE);
    }

    /* To convert Owner field from String to SNMP_OCTET_STRING */

    ownerData.i4_Length = (INT4) STRLEN (pRmonEthStatsEntry->au1RmonEtherOwner);
    if (ownerData.i4_Length != 0)
    {
        ownerData.pu1_OctetList = pRmonEthStatsEntry->au1RmonEtherOwner;

        /* To test & set the Owner field in Etherstats table */
        i1RetVal = nmhTestv2EtherStatsOwner (&u4ErrorCode,
                                             pRmonEthStatsEntry->
                                             i4EtherStatsIndex, &ownerData);

        if (i1RetVal == SNMP_FAILURE)
        {
            nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                    RMON_INVALID);
            return (CLI_FAILURE);
        }
        if (nmhSetEtherStatsOwner (pRmonEthStatsEntry->i4EtherStatsIndex,
                                   &ownerData) != SNMP_SUCCESS)
        {

            nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                    RMON_INVALID);
            return (CLI_FAILURE);
        }

    }

    /* To convert the Data source Index to tSNMP_OID_TYPE */

    /* Calling the RMON Util function to convert Interface Index to
     * tSNMP_OID_TYPE
     * Need to convert the i4RmonEtherPortOrVlanIndex to UINT1 */

    MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
    OidValue.pu4_OidList = &au4OidValue[0];

    RmonFormDataSourceOid (&OidValue,
                           (UINT4) pRmonEthStatsEntry->
                           i4RmonEtherPortOrVlanIndex,
                           (UINT4) pRmonEthStatsEntry->i4RmonEthIndexType);

    /* To test & set the Data Source field in EtherStats Table */
    i1RetVal = nmhTestv2EtherStatsDataSource (&u4ErrorCode,
                                              pRmonEthStatsEntry->
                                              i4EtherStatsIndex, &OidValue);

    if (i1RetVal == SNMP_FAILURE)
    {
        nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetEtherStatsDataSource (pRmonEthStatsEntry->i4EtherStatsIndex,
                                    &OidValue) != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_STAT);
        nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                RMON_INVALID);
        return (CLI_FAILURE);
    }

    /* To test & set the final status of the entry to "VALID" */
    i1RetVal = nmhTestv2EtherStatsStatus (&u4ErrorCode,
                                          pRmonEthStatsEntry->i4EtherStatsIndex,
                                          pRmonEthStatsEntry->
                                          i4RmonEtherStatus2);

    if (i1RetVal == SNMP_FAILURE)
    {
        /*  delete the entry and come out */
        nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                RMON_INVALID);
        return (CLI_FAILURE);
    }

    if (nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                pRmonEthStatsEntry->i4RmonEtherStatus2)
        != SNMP_SUCCESS)
    {
        /* delete the entry and come out */
        CLI_SET_ERR (CLI_RMON_SET_COLLECTION_STAT);
        nmhSetEtherStatsStatus (pRmonEthStatsEntry->i4EtherStatsIndex,
                                RMON_INVALID);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RmonSetNoCollectionStats                               */
/*                                                                           */
/* Description      : This function is invoked to disable RMON statistics on */
/*                    a specified interface                                  */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pRmonEthStatsEntry - pointer to collection          */
/*                                               statistics entry            */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
RmonSetNoCollectionStats (tCliHandle CliHandle,
                          tRmonEthStatsEntry * pRmonEthStatsEntry)
{
    INT4                i4Status = RMON_INVALID;    /* Invalid Status */
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;
    tSNMP_OID_TYPE      CurrentOidValue;    /* For Data Source */
    tSNMP_OID_TYPE      PreviousOidValue;    /* For Data Source */
    UINT4               au4CurrentOidValue[RMON_MAX_OID_LEN];
    UINT4               au4PreviousOidValue[RMON_MAX_OID_LEN];
    INT4                i4StatsIndex;

    /* Validate the given index */

    i4StatsIndex = pRmonEthStatsEntry->i4EtherStatsIndex;

    i1RetVal = nmhValidateIndexInstanceEtherStatsTable (i4StatsIndex);

    if (i1RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RMON_INVALID_STATS_INDEX_ERR);
        return (CLI_FAILURE);
    }

    /* Check if the Stats Index to be deleted is created only for that 
     * particular interface and if so, then delete the stats index*/

    MEMSET (au4CurrentOidValue, 0, (RMON_MAX_OID_LEN * sizeof (UINT4)));
    MEMSET (au4PreviousOidValue, 0, (RMON_MAX_OID_LEN * sizeof (UINT4)));

    CurrentOidValue.pu4_OidList = au4CurrentOidValue;
    PreviousOidValue.pu4_OidList = au4PreviousOidValue;

    RmonFormDataSourceOid (&CurrentOidValue,
                           (UINT4) pRmonEthStatsEntry->
                           i4RmonEtherPortOrVlanIndex,
                           (UINT4) pRmonEthStatsEntry->i4RmonEthIndexType);

    /*  Get the DataSource for the passed statistics index */
    i1RetVal = nmhGetEtherStatsDataSource (i4StatsIndex, &PreviousOidValue);

    if (i1RetVal == SNMP_SUCCESS)
    {

        if (MEMCMP (PreviousOidValue.pu4_OidList,
                    CurrentOidValue.pu4_OidList, RMON_MAX_OID_LEN) != 0)
        {

            CLI_SET_ERR (CLI_RMON_STATS_UNMATCHING_INTERFACE_OR_VLAN_ERR);
            return (CLI_FAILURE);

        }

        /* For the given entry in the Table, Set the STATUS to INVALID. */
        /* Test & set the Status Field */
        i1RetVal =
            nmhTestv2EtherStatsStatus (&u4ErrorCode, i4StatsIndex, i4Status);

        if (i1RetVal != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        i1RetVal = nmhSetEtherStatsStatus (i4StatsIndex, i4Status);

        if (i1RetVal != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Failed - Check for dependancy in History Control"
                       "and Alarm Tables.\r\n");
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     Function Name    : RmonShowRunningConfigGlobal                        */
/*                                                                           */
/*     Description      : This function displays the current global          */
/*                        configuration information of RMON                  */
/*                                                                           */
/*     Input Parameters : CliHandle - CliContext ID                          */
/*                                                                           */
/*     Output Parameters: None                                               */
/*                                                                           */
/*     Return Value     : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RmonShowRunningConfigGlobal (tCliHandle CliHandle)
{
    UINT1               au1EventTemp1[RMON_DESC_LEN + 1];
    UINT1               au1EventTemp2[RMON_DESC_LEN];
    UINT1               au1EventTemp3[RMON_DESC_LEN + 1];
    UINT1               au1AlrmTemp[RMON_DESC_LEN + 1];
    INT4                i4EnableStatus;
    INT4                i4AlrmReturnVal;
    INT4                i4AlarmIndex = 0;
    INT4                i4NextAlarmIndex = 0;
    INT4                i4AlrmCounter = 0;
    INT4                i4EventIndex = 0;
    INT4                i4NextEventIndex = 0;
    INT4                i4EventType;
    INT4                i4ReturnStatus;

    tSNMP_OID_TYPE     *pAlrmVariable = NULL;
    tSNMP_OCTET_STRING_TYPE OctetRetvalAlrmOwner;

    tSNMP_OCTET_STRING_TYPE OctetRetvalEventOwner;
    tSNMP_OCTET_STRING_TYPE OctetRetvalEventCommunity;
    tSNMP_OCTET_STRING_TYPE OctetRetvalEventDescription;

    /* Making the Alarmvariable string to ZERO */
    pAlrmVariable = alloc_oid (RMON_DESC_LEN);
    if (pAlrmVariable == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " Memory Allocation Failure - RmonShowRunningConfigGlobal. \n");
        return CLI_FAILURE;
    }
    MEMSET (pAlrmVariable->pu4_OidList, 0, RMON_DESC_LEN * sizeof (UINT4));
    OctetRetvalAlrmOwner.pu1_OctetList = &au1AlrmTemp[0];

    /* Since Event Table has more parameters of type tSNMP_OCTET_STRING_TYPE
     * All need to be converted so that they can be printed on the console.
     * Allocating Memory for Owner which is of type tSNMP_OCTET_STRING_TYPE */

    OctetRetvalEventOwner.pu1_OctetList = &au1EventTemp1[0];
    OctetRetvalEventCommunity.pu1_OctetList = &au1EventTemp2[0];
    OctetRetvalEventDescription.pu1_OctetList = &au1EventTemp3[0];

    nmhGetRmonEnableStatus (&i4EnableStatus);
    if (i4EnableStatus != RMON_DISABLE)
    {
        CliPrintf (CliHandle, "set rmon enable\r\n");
    }

    /* Retrieve the first index of the Event Table */
    if (nmhGetFirstIndexEventTable (&i4EventIndex) != SNMP_FAILURE)
    {
        /* Outside the DO loop, assign the First Index to Next Event Index.
         * Inside the DO loop, assign the next Event Index to the First Index */
        i4NextEventIndex = i4EventIndex;

        do
        {
            CLI_MEMSET (au1EventTemp1, 0, RMON_DESC_LEN + 1);
            CLI_MEMSET (au1EventTemp2, 0, RMON_DESC_LEN);
            CLI_MEMSET (au1EventTemp3, 0, RMON_DESC_LEN + 1);

            i4EventIndex = i4NextEventIndex;
            nmhGetEventStatus (i4EventIndex, &i4ReturnStatus);

            /* We have VALID event state for which CISCO displays active
             * For other states I am displaying our state values
             */
            if (i4ReturnStatus == RMON_VALID)
            {
                CliPrintf (CliHandle, "rmon event %d", i4EventIndex);

                nmhGetEventDescription (i4EventIndex,
                                        &OctetRetvalEventDescription);
                if ((OctetRetvalEventDescription.i4_Length) != 0)
                {
                    CliPrintf (CliHandle, " description \"%s\"",
                               OctetRetvalEventDescription.pu1_OctetList);
                }
                nmhGetEventType (i4EventIndex, &i4EventType);
                switch (i4EventType)
                {
                    case RMON_EVENT_LOG:
                        CliPrintf (CliHandle, " log");
                        nmhGetEventOwner (i4EventIndex, &OctetRetvalEventOwner);
                        if ((OctetRetvalEventOwner.i4_Length) != 0)
                        {
                            CliPrintf (CliHandle, " owner \"%s\"",
                                       OctetRetvalEventOwner.pu1_OctetList);
                        }
                        break;

                    case RMON_EVENT_LOG_AND_TRAP:
                        CliPrintf (CliHandle, " log");
                        nmhGetEventOwner (i4EventIndex, &OctetRetvalEventOwner);
                        if ((OctetRetvalEventOwner.i4_Length) != 0)
                        {
                            CliPrintf (CliHandle, " owner \"%s\"",
                                       OctetRetvalEventOwner.pu1_OctetList);
                        }
                        /* Fall through */
                    case RMON_EVENT_TRAP:
                        /* Display the community if TRAP */
                        nmhGetEventCommunity (i4EventIndex,
                                              &OctetRetvalEventCommunity);
                        if ((OctetRetvalEventCommunity.i4_Length) != 0)
                        {
                            CliPrintf (CliHandle, " trap \"%s\"",
                                       OctetRetvalEventCommunity.pu1_OctetList);
                        }
                        break;
                    default:
                        nmhGetEventOwner (i4EventIndex, &OctetRetvalEventOwner);
                        if ((OctetRetvalEventOwner.i4_Length) != 0)
                        {
                            CliPrintf (CliHandle, " owner \"%s\"",
                                       OctetRetvalEventOwner.pu1_OctetList);
                        }
                        break;
                }

                CliPrintf (CliHandle, "\r\n");
            }
        }

        while (nmhGetNextIndexEventTable
               (i4EventIndex, &i4NextEventIndex) == SNMP_SUCCESS);
    }

    if (nmhGetFirstIndexAlarmTable (&i4AlarmIndex) != SNMP_FAILURE)
    {
        /* Outside the DO loop, assign the first index to next alarm index.
         * Inside the DO loop, assign the next index to the first index. */
        i4NextAlarmIndex = i4AlarmIndex;

        do
        {
            CLI_MEMSET (au1AlrmTemp, 0, RMON_DESC_LEN + 1);
            i4AlarmIndex = i4NextAlarmIndex;
            nmhGetAlarmStatus (i4AlarmIndex, &i4ReturnStatus);
            if (i4ReturnStatus == RMON_VALID)
            {
                CliPrintf (CliHandle, "rmon alarm %d ", i4AlarmIndex);

                /*  OidValue to String Conversion here */
                nmhGetAlarmVariable (i4AlarmIndex, pAlrmVariable);
                if (pAlrmVariable->u4_Length != 0)
                {
                    for (i4AlrmCounter = 0;
                         i4AlrmCounter < (INT4) (pAlrmVariable->u4_Length);
                         i4AlrmCounter++)
                    {
                        CliPrintf (CliHandle, "%d",
                                   pAlrmVariable->pu4_OidList[i4AlrmCounter]);

                        if (i4AlrmCounter
                            < ((INT4) (pAlrmVariable->u4_Length - 1)))
                        {
                            CliPrintf (CliHandle, ".");
                        }
                    }
                }

                nmhGetAlarmInterval (i4AlarmIndex, &i4AlrmReturnVal);
                CliPrintf (CliHandle, " %d", i4AlrmReturnVal);

                nmhGetAlarmSampleType (i4AlarmIndex, &i4AlrmReturnVal);
                switch (i4AlrmReturnVal)
                {

                    case RMON_ALARM_ABSOLUTE:
                        CliPrintf (CliHandle, " absolute");
                        break;

                    case RMON_ALARM_DELTA:
                        CliPrintf (CliHandle, " delta");
                        break;
                    default:
                        break;
                }

                nmhGetAlarmRisingThreshold (i4AlarmIndex, &i4AlrmReturnVal);
                CliPrintf (CliHandle, " rising-threshold %d", i4AlrmReturnVal);

                nmhGetAlarmRisingEventIndex (i4AlarmIndex, &i4AlrmReturnVal);
                CliPrintf (CliHandle, " %d", i4AlrmReturnVal);

                nmhGetAlarmFallingThreshold (i4AlarmIndex, &i4AlrmReturnVal);
                CliPrintf (CliHandle, " falling-threshold %d", i4AlrmReturnVal);

                nmhGetAlarmFallingEventIndex (i4AlarmIndex, &i4AlrmReturnVal);
                CliPrintf (CliHandle, " %d", i4AlrmReturnVal);

                nmhGetAlarmOwner (i4AlarmIndex, &OctetRetvalAlrmOwner);
                if ((OctetRetvalAlrmOwner.i4_Length) != 0)
                {
                    CliPrintf (CliHandle, " owner \"%s\"",
                               OctetRetvalAlrmOwner.pu1_OctetList);
                }
                CliPrintf (CliHandle, "\r\n");
            }
        }
        while (nmhGetNextIndexAlarmTable
               (i4AlarmIndex, &i4NextAlarmIndex) == SNMP_SUCCESS);
    }
    free_oid (pAlrmVariable);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     Function Name    : RmonShowRunningConfigInterface                     */
/*                                                                           */
/*     Description      : This function displays the current interface       */
/*                        configuration information of RMON                  */
/*                                                                           */
/*     Input Parameters : CliHandle - CliContext ID                          */
/*                                                                           */
/*     Output Parameters: None                                               */
/*                                                                           */
/*     Return Value     : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RmonShowRunningConfigInterface (tCliHandle CliHandle, UINT1 u1Flag,
                                INT4 i4IfIndex)
{
    UINT1               au1StatsTemp[RMON_DESC_LEN + 1];
    UINT1               au1HistTemp[RMON_DESC_LEN + 1];
    CHR1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                ai2InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4StatsIndex = 0;
    INT4                i4nextEtherStatsIndex = 0;
    INT4                i4nextHistoryControlIndex;
    INT4                i4bucketsRequestedVal = 0;
    INT4                i4HistoryControlIndex;
    INT4                i4HistoryControlInterval;
    INT4                i4ReturnStatus;
    UINT4               u4IfIndex = 0;
    UINT4               u4Count = 0;
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    UINT4               u4Flag = 0;
    UINT4               u4OIDType = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1               *piIfName;
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    tSNMP_OCTET_STRING_TYPE OctetRetvalHistOwner;
    tSNMP_OCTET_STRING_TYPE OctetRetvalStatsOwner;

    OctetRetvalStatsOwner.pu1_OctetList = &au1StatsTemp[0];
    OctetRetvalHistOwner.pu1_OctetList = &au1HistTemp[0];
    OidValue.pu4_OidList = &au4OidValue[0];

    CliRegisterLock (CliHandle, RmonLock, RmonUnLock);
    RMON_LOCK ();

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    if (nmhGetFirstIndexEtherStatsTable (&i4StatsIndex) != SNMP_FAILURE)
    {
        /* Outside the DO loop, assign First Index to Next index
         * Inside the DO loop, reverse assignment. */

        i4nextEtherStatsIndex = i4StatsIndex;

        do
        {
            CLI_MEMSET (au1StatsTemp, 0, (RMON_DESC_LEN + 1));
            CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
            CLI_MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CLI_MEMSET (ai2InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

            i4StatsIndex = i4nextEtherStatsIndex;
            /* To Print the Interface Index value */
            nmhGetEtherStatsStatus (i4StatsIndex, &i4ReturnStatus);
            if (i4ReturnStatus == RMON_VALID)
            {
                nmhGetEtherStatsDataSource (i4StatsIndex, &OidValue);
                if (RmonCheckDataSourceOIDIsVlanOrInterface
                    (&OidValue, &u4OIDType) == RMON_FAILURE)
                {
                    continue;
                }
                if (u4OIDType != PORT_INDEX_TYPE)
                {
                    continue;
                }

                /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
                 * grlInterfaceOid is the Global Oid */
                u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];

                CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) ai2InterfaceName);
                CfaCliGetIfName (u4IfIndex, (INT1 *) ai1InterfaceName);

                if (strcmp (ai1InterfaceName, ai2InterfaceName) == 0)
                {
                    if ((u4Count == 0) && (u1Flag == 1))
                    {
                        u4Count++;
                        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                    }
                    CliPrintf (CliHandle, " rmon collection stats %d",
                               i4StatsIndex);
                    nmhGetEtherStatsOwner (i4StatsIndex,
                                           &OctetRetvalStatsOwner);
                    if ((OctetRetvalStatsOwner.i4_Length) != 0)
                    {
                        if (strcmp
                            ((const char *) OctetRetvalStatsOwner.pu1_OctetList,
                             "monitor") != 0)
                        {

                            CliPrintf (CliHandle, " owner \"%s\"",
                                       OctetRetvalStatsOwner.pu1_OctetList);
                        }
                    }
                    CliPrintf (CliHandle, "\r\n");
                    u4Flag = 1;
                }
            }
        }

        while (nmhGetNextIndexEtherStatsTable
               (i4StatsIndex, &i4nextEtherStatsIndex) == SNMP_SUCCESS);
    }
    /* Get the first Index of the History Control Table */
    if (nmhGetFirstIndexHistoryControlTable
        (&i4HistoryControlIndex) != SNMP_FAILURE)
    {

        /* Assign the Current first index value to next index outside DO loop
         * Inside DO Loop, reverse assignment. */

        i4nextHistoryControlIndex = i4HistoryControlIndex;

        do
        {
            CLI_MEMSET (au1HistTemp, 0, (RMON_DESC_LEN + 1));
            CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
            CLI_MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CLI_MEMSET (ai2InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

            i4HistoryControlIndex = i4nextHistoryControlIndex;
            /* To Print the Interface Index value */
            nmhGetHistoryControlStatus (i4HistoryControlIndex, &i4ReturnStatus);
            if (i4ReturnStatus == RMON_VALID)
            {
                nmhGetHistoryControlDataSource (i4HistoryControlIndex,
                                                &OidValue);
                if (RmonCheckDataSourceOIDIsVlanOrInterface
                    (&OidValue, &u4OIDType) == RMON_FAILURE)
                {
                    continue;
                }
                if (u4OIDType != PORT_INDEX_TYPE)
                {
                    continue;
                }

                /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
                 * grlInterfaceOid is the Global Oid */
                u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];
                CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) ai2InterfaceName);
                CfaCliGetIfName (u4IfIndex, (INT1 *) ai1InterfaceName);

                if (strcmp (ai1InterfaceName, ai2InterfaceName) == 0)
                {
                    if ((u4Count == 0) && (u1Flag == 1))
                    {
                        u4Count++;
                        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
                        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                    }
                    CliPrintf (CliHandle, " rmon collection history %d",
                               i4HistoryControlIndex);

                    nmhGetHistoryControlBucketsRequested (i4HistoryControlIndex,
                                                          &i4bucketsRequestedVal);
                    if (i4bucketsRequestedVal != MAX_HISTORY_BUCKETS)
                    {
                        CliPrintf (CliHandle, " buckets %d",
                                   i4bucketsRequestedVal);
                    }
                    nmhGetHistoryControlInterval (i4HistoryControlIndex,
                                                  &i4HistoryControlInterval);
                    if (i4HistoryControlInterval != RMON_HISTORY_CTRL_INTERVAL)
                    {
                        CliPrintf (CliHandle, " interval %d",
                                   i4HistoryControlInterval);
                    }
                    nmhGetHistoryControlOwner (i4HistoryControlIndex,
                                               &OctetRetvalHistOwner);
                    if ((OctetRetvalHistOwner.i4_Length) != 0)
                    {
                        CliPrintf (CliHandle, " owner \"%s\"",
                                   OctetRetvalHistOwner.pu1_OctetList);
                    }
                    CliPrintf (CliHandle, "\r\n");
                    u4Flag = 1;

                }
            }
        }

        while ((nmhGetNextIndexHistoryControlTable
                (i4HistoryControlIndex,
                 &i4nextHistoryControlIndex)) == SNMP_SUCCESS);
    }
    if ((u1Flag == 1) && (u4Flag == 1))
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    RMON_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     Function Name    : RmonShowRunningConfigVlan                          */
/*                                                                           */
/*     Description      : This function displays the current Vlan            */
/*                        configuration information of RMON                  */
/*                                                                           */
/*     Input Parameters : CliHandle - CliContext ID                          */
/*                                                                           */
/*     Output Parameters: None                                               */
/*                                                                           */
/*     Return Value     : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RmonShowRunningConfigVlan (tCliHandle CliHandle, UINT1 u1Flag, UINT4 u4VlanId)
{
    UINT1               au1StatsTemp[RMON_DESC_LEN + 1];
    UINT1               au1HistTemp[RMON_DESC_LEN + 1];
    INT4                i4StatsIndex = 0;
    INT4                i4nextEtherStatsIndex = 0;
    INT4                i4nextHistoryControlIndex;
    INT4                i4bucketsRequestedVal = 0;
    INT4                i4HistoryControlIndex;
    INT4                i4HistoryControlInterval;
    INT4                i4ReturnStatus;
    UINT4               u4VlanIdInTable = 0;
    UINT4               u4Count = 0;
    UINT4               au4OidValue[RMON_MAX_OID_LEN];
    UINT4               u4Flag = 0;
    UINT4               u4OIDType = 0;
    tSNMP_OID_TYPE      OidValue;    /* For Data Source */
    tSNMP_OCTET_STRING_TYPE OctetRetvalHistOwner;
    tSNMP_OCTET_STRING_TYPE OctetRetvalStatsOwner;

    OctetRetvalStatsOwner.pu1_OctetList = &au1StatsTemp[0];
    OctetRetvalHistOwner.pu1_OctetList = &au1HistTemp[0];
    OidValue.pu4_OidList = &au4OidValue[0];

    if (nmhGetFirstIndexEtherStatsTable (&i4StatsIndex) != SNMP_FAILURE)
    {
        /* Outside the DO loop, assign First Index to Next index
         * Inside the DO loop, reverse assignment. */

        i4nextEtherStatsIndex = i4StatsIndex;

        do
        {
            CLI_MEMSET (au1StatsTemp, 0, (RMON_DESC_LEN + 1));
            CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);

            i4StatsIndex = i4nextEtherStatsIndex;
            /* To Print the Vlan Id */
            nmhGetEtherStatsStatus (i4StatsIndex, &i4ReturnStatus);
            if (i4ReturnStatus == RMON_VALID)
            {
                nmhGetEtherStatsDataSource (i4StatsIndex, &OidValue);
                if (RmonCheckDataSourceOIDIsVlanOrInterface
                    (&OidValue, &u4OIDType) == RMON_FAILURE)
                {
                    continue;
                }
                if (u4OIDType != VLAN_INDEX_TYPE)
                {
                    continue;
                }

                /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
                 * grlVlanOid is the Global Oid */
                u4VlanIdInTable = OidValue.pu4_OidList[grlVlanOid.u4_Length];

                if (u4VlanId == u4VlanIdInTable)
                {
                    if ((u4Count == 0) && (u1Flag == 1))
                    {
                        u4Count++;
                        CliPrintf (CliHandle, "vlan %u\r\n", u4VlanId);
                    }
                    CliPrintf (CliHandle, " rmon collection stats %d",
                               i4StatsIndex);
                    nmhGetEtherStatsOwner (i4StatsIndex,
                                           &OctetRetvalStatsOwner);
                    if ((OctetRetvalStatsOwner.i4_Length) != 0)
                    {
                        if (strcmp
                            ((const char *) OctetRetvalStatsOwner.pu1_OctetList,
                             "monitor") != 0)
                        {

                            CliPrintf (CliHandle, " owner \"%s\"",
                                       OctetRetvalStatsOwner.pu1_OctetList);
                        }
                    }
                    CliPrintf (CliHandle, "\r\n");
                    u4Flag = 1;
                }
            }
        }

        while (nmhGetNextIndexEtherStatsTable
               (i4StatsIndex, &i4nextEtherStatsIndex) == SNMP_SUCCESS);
    }
    /* Get the first Index of the History Control Table */
    if (nmhGetFirstIndexHistoryControlTable
        (&i4HistoryControlIndex) != SNMP_FAILURE)
    {

        /* Assign the Current first index value to next index outside DO loop
         * Inside DO Loop, reverse assignment. */

        i4nextHistoryControlIndex = i4HistoryControlIndex;

        do
        {
            CLI_MEMSET (au1HistTemp, 0, (RMON_DESC_LEN + 1));
            CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);

            i4HistoryControlIndex = i4nextHistoryControlIndex;
            /* To Print the Interface Index value */
            nmhGetHistoryControlStatus (i4HistoryControlIndex, &i4ReturnStatus);
            if (i4ReturnStatus == RMON_VALID)
            {
                nmhGetHistoryControlDataSource (i4HistoryControlIndex,
                                                &OidValue);
                if (RmonCheckDataSourceOIDIsVlanOrInterface
                    (&OidValue, &u4OIDType) == RMON_FAILURE)
                {
                    continue;
                }
                if (u4OIDType != VLAN_INDEX_TYPE)
                {
                    continue;
                }

                /* This is the way the tSNMP_OID_TYPE to UINT1 conversion
                 * grlVlanOid is the Global Oid */
                u4VlanIdInTable = OidValue.pu4_OidList[grlVlanOid.u4_Length];

                if (u4VlanId == u4VlanIdInTable)
                {
                    if ((u4Count == 0) && (u1Flag == 1))
                    {
                        u4Count++;
                        CliPrintf (CliHandle, "vlan %u\r\n", u4VlanId);
                    }
                    CliPrintf (CliHandle, " rmon collection history %d",
                               i4HistoryControlIndex);

                    nmhGetHistoryControlBucketsRequested (i4HistoryControlIndex,
                                                          &i4bucketsRequestedVal);
                    if (i4bucketsRequestedVal != MAX_HISTORY_BUCKETS)
                    {
                        CliPrintf (CliHandle, " buckets %d",
                                   i4bucketsRequestedVal);
                    }
                    nmhGetHistoryControlInterval (i4HistoryControlIndex,
                                                  &i4HistoryControlInterval);
                    if (i4HistoryControlInterval != RMON_HISTORY_CTRL_INTERVAL)
                    {
                        CliPrintf (CliHandle, " interval %d",
                                   i4HistoryControlInterval);
                    }
                    nmhGetHistoryControlOwner (i4HistoryControlIndex,
                                               &OctetRetvalHistOwner);
                    if ((OctetRetvalHistOwner.i4_Length) != 0)
                    {
                        CliPrintf (CliHandle, " owner \"%s\"",
                                   OctetRetvalHistOwner.pu1_OctetList);
                    }
                    CliPrintf (CliHandle, "\r\n");
                    u4Flag = 1;

                }
            }
        }

        while ((nmhGetNextIndexHistoryControlTable
                (i4HistoryControlIndex,
                 &i4nextHistoryControlIndex)) == SNMP_SUCCESS);
    }
    if ((u1Flag == 1) && (u4Flag == 1))
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}
#endif

#endif
