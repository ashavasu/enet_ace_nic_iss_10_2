/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmportst.c,v 1.17 2011/11/24 09:13:59 siva Exp $            *
 *                                                                  *
 * Description: Contains implementation of Functions to check the   *
 *              Type of Packet received.                            *
 *                                                                  *
 *******************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  rmportst.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  RMON PORTING                                 |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides  routines for            |
 * |                             Checking type of packet                     |
 * |                                                                         |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |  Bhupendra          |  Initial Creation                |
 * |                |                     |                                  |
 * |----------------|---------------------|----------------------------------|
 * |  2.0           |  Bhupendra          |  1. Function RmonGenerateErrorPkt|   * |                |                     |     is added for simulating error|
 * |                |                     |     packets                      |
 * |                |                     |  2. functions RmonisErrorInFCS,  |
 * |                |                     |     RmonisBroadCast,             |
 * |                |                     |     RmonisMultiCast are modified.|
 *  -------------------------------------------------------------------------
 * |  3.0           |  Leela L            |   Modified for Dynamic table.    |
 * |                |  28-Jun-2002        |                                  |
 * |----------------|---------------------|----------------------------------|
 *
 */

#include "rmallinc.h"

#ifdef RMON_TEST
/****************************************************************************
*      Role of the function : This function generates error packets         *
*                                                                           *
*      Formal Parameters    : pFrame: frame received from CFA               *
*                            u4PktSize: Packet size                         *
*                                                                           *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : None                                          *
*****************************************************************************/

INT2
RmonGenerateErrorPkt (UINT1 *pFrame, UINT4 *u4PktSize)
{
    tEthFrame          *p_PktRecvdFromCfa = NULL;

    p_PktRecvdFromCfa = (tEthFrame *) (VOID *) pFrame;

    if (p_PktRecvdFromCfa->protType == OSIX_HTONS (0x0806))
    {
        RMON_DBG2 (MAJOR_TRC | PKT_PROC_TRC,
                   "ARP PACKET IS RECEIVED = %d,u4PktSize = %d \n\n",
                   p_PktRecvdFromCfa->protType, *u4PktSize);

        return RMON_FAILURE;
    }

    switch (p_PktRecvdFromCfa->data[0])
    {

        case BROADCASTERR:
            /*
             * Assume we are fixing Last four byte of data for indicating
             * FCS Error, if second Last byte contains anything else than zero
             * it means ,its a broadcast packet with FCS error.
             */
            p_PktRecvdFromCfa->data[984] = 0x0F;
            break;

        case MULTICASTERR:
            /*
             * If second last byte contains anything other than Zero
             * Indicates Multicast packet with FCS error .
             */
            p_PktRecvdFromCfa->data[984] = 0x0F;
            break;

        case FCSERR:
            /*
             * If second last byte contains anything other than Zero
             * Indicates packet with FCS error .
             */
            p_PktRecvdFromCfa->data[984] = 0x0F;
            break;

        case UNDERSIZE_TYPE:
            /* It was transfered with 64 size ,to make it under  size
             * 10 bytes  are deducted
             */
            *u4PktSize -= 10;
            break;

        case UNDERSIZE_FCSERR:
            /* After deduction of 10-bytes second last byte is filled
             * with 0xFF to make it with FCS Error.
             */
            *u4PktSize -= 10;
            p_PktRecvdFromCfa->data[38] = 0xFF;
            break;

        case OVERSIZE_TYPE:
            /* Transmited with 1500 byte size , 100 bytes are added to make
             * it over size.
             */
            *u4PktSize += 100;

            break;

        case OVERSIZE_FCSERR:
            /* It was transfered with 1500 size ,to make it over size
             *  100 bytes are added and second Last byte is filled with
             *  0xFF to Indicate FCS error .
             */
            *u4PktSize += 100;
            p_PktRecvdFromCfa->data[1584] = 0xFF;
            break;

            /* To indicate  Alignment Error a four bit mask is set in 
             *  between of the FCS octets.Assume that this extra nibble is
             *  Inserted by noise  or by collision during transmission and
             * due to the Insertion of it FCS becomes wrong.
             */

        case ALIGNMENTERR:

            p_PktRecvdFromCfa->data[984] = 0x0F;
            break;

        case OVERSIZE_ALIGNMENTERR:

            *u4PktSize += 100;
            p_PktRecvdFromCfa->data[1584] = 0x0F;
            break;

        case UNDERSIZE_ALIGNMENTERR:

            *u4PktSize -= 10;
            p_PktRecvdFromCfa->data[38] = 0x0F;
            break;

        default:
            RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                      " Invalid Packet Type Received\n");
            return RMON_SUCCESS;
    }                            /* End of Switch */
    return RMON_SUCCESS;
}
#endif /* RMON_TEST */

/****************************************************************************
*      Role of the function : This function checks whether packets received *
*                             contains error                                *
*      Formal Parameters    : pEthframe: frame received from CFA            *
*                            u2Length  : Packet size                        *
*                                                                           *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : TRUE or FALSE                                 *
*****************************************************************************/

UINT1
RmonisErrorInFCS (UINT1 *pEthframe, UINT2 u2Length)
{
    UINT2               ethHeader = 14;
    tEthFrame          *pFrame = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: RmonisErrorInFCS \n");
    pFrame = (tEthFrame *) (VOID *) pEthframe;

    /* If the packet length is less than 22, Array
     * index will go in negative. To avoid negative array
     * access, the following if check is added
     * */

    if ((u2Length < (4 + 4 + ethHeader)) || (u2Length > RMON_FRAME_DATA_SIZE))
    {
        return (FALSE);
    }

    if ((pFrame->data[u2Length - (4 + 4 + ethHeader)] == 0) &&
        (pFrame->data[u2Length - (3 + 4 + ethHeader)] == 0) &&
        (pFrame->data[u2Length - (2 + 4 + ethHeader)] == 0) &&
        (pFrame->data[u2Length - (1 + 4 + ethHeader)] == 0))
    {
        return (FALSE);
    }
    else
    {
        return (TRUE);
    }
}

/****************************************************************************
*      Role of the function : This function checks whether packet received  *
*                             is BroadCast                                  *
*      Formal Parameters    : pEthFrame: frame received from CFA            *
*                                                                           *
*                                                                           *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : TRUE or FALSE                                 *
*****************************************************************************/

UINT1
RmonisBroadCast (UINT1 *pEthFrame)
{
    UINT1               u1Count;
    tEthFrame          *pFrame = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: RmonisBroadCast \n");
    pFrame = (tEthFrame *) (VOID *) pEthFrame;

    RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
              "Entering into the function: RmonisBroadCast \n");

    for (u1Count = 0; u1Count < ADDRESS_LENGTH; u1Count++)
    {
        if (pFrame->destAddr[u1Count] != 0xFF)
        {
            return FALSE;
        }
    }
    return TRUE;
}

/****************************************************************************
*      Role of the function : This function checks whether packet received  *
*                             is MultiCast                                  *
*      Formal Parameters    : pEthFrame: frame received from CFA            *
*                                                                           *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : TRUE or FALSE                                 *
*****************************************************************************/

UINT1
RmonisMultiCast (UINT1 *pEthFrame)
{
    tEthFrame          *pFrame = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: RmonisMultiCast \n");
    pFrame = (tEthFrame *) (VOID *) pEthFrame;

    if ((pFrame->destAddr[0] & 01) != 1)
    {
        return FALSE;
    }
    return TRUE;
}

/* RmonMacDiff function starts here */

INT2
RmonMacDiff (tMacAddr x, tMacAddr y)
{

    INT1                i1Count;
    INT2                i2Ret = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: RmonMacDiff \n");

    for (i1Count = 0; i1Count < ADDRESS_LENGTH; i1Count++)
    {
        i2Ret = (INT2) (x[i1Count] - y[i1Count]);
        if (i2Ret != 0)
        {
            break;
        }
    }
    return i2Ret;
}

/* RmonIsMacZero function starts here */

INT1
RmonIsMacZero (UINT1 *x)
{

    INT1                i1Count;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function:  RmonIsMacZero\n");

    for (i1Count = 0; i1Count < ADDRESS_LENGTH; i1Count++)
    {
        if (x[i1Count])
        {
            return FALSE;
        }
    }
    return TRUE;
}

INT4
RmonCompareMacAddr (tMacAddr addr1, tMacAddr addr2)
{
    INT4                i4Val = RMON_EQUAL;

    if (RMON_MEM_CMP (addr1, addr2, ADDRESS_LENGTH) < 0)
    {
        i4Val = RMON_LESSER;
    }
    else if (RMON_MEM_CMP (addr1, addr2, ADDRESS_LENGTH) > 0)
    {
        i4Val = RMON_GREATER;
    }
    else
    {
        i4Val = RMON_EQUAL;
    }

    return i4Val;
}
