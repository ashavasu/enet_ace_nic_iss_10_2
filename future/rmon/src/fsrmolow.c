/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmolow.c,v 1.22 2013/10/28 09:53:41 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "rmallinc.h"
#include "cli.h"
#include "rmoncli.h"
#include "rmgr.h"
extern tTmrAppTimer gOneSecTimer;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRmonDebugType
 Input       :  The Indices

                The Object 
                retValRmonDebugType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonDebugType (UINT4 *pu4RetValRmonDebugType)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhGetRmonDebugType \n");

    *pu4RetValRmonDebugType = gu4RmondebgFlag;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function: nmhGetRmonDebugType \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonEnableStatus
 Input       :  The Indices

                The Object 
                retValRmonEnableStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonEnableStatus (INT4 *pi4RetValRmonEnableStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonEnableStatus\n");
    *pi4RetValRmonEnableStatus = (INT4) gRmonEnableStatusFlag;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonEnableStatus\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwStatsSupp
 Input       :  The Indices

                The Object 
                retValRmonHwStatsSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwStatsSupp (INT4 *pi4RetValRmonHwStatsSupp)
{

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwStatsSupp\n");
    *pi4RetValRmonHwStatsSupp = gRmonSupportedGroups.u1RmonHwStatsSuppFlag;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwStatsSupp\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwHistorySupp
 Input       :  The Indices

                The Object 
                retValRmonHwHistorySupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwHistorySupp (INT4 *pi4RetValRmonHwHistorySupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwHistorySupp \n");
    *pi4RetValRmonHwHistorySupp = gRmonSupportedGroups.u1RmonHwHistorySuppFlag;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwHistorySupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwAlarmSupp
 Input       :  The Indices

                The Object 
                retValRmonHwAlarmSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwAlarmSupp (INT4 *pi4RetValRmonHwAlarmSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwAlarmSupp \n");
    *pi4RetValRmonHwAlarmSupp = gRmonSupportedGroups.u1RmonHwAlarmSuppFlag;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwAlarmSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwHostSupp
 Input       :  The Indices

                The Object 
                retValRmonHwHostSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwHostSupp (INT4 *pi4RetValRmonHwHostSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwHostSupp \n");
    *pi4RetValRmonHwHostSupp = gRmonSupportedGroups.u1RmonHwHostSuppFlag;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwHostSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwHostTopNSupp
 Input       :  The Indices

                The Object 
                retValRmonHwHostTopNSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwHostTopNSupp (INT4 *pi4RetValRmonHwHostTopNSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwHostTopNSupp \n");
    *pi4RetValRmonHwHostTopNSupp =
        gRmonSupportedGroups.u1RmonHwHostTopNSuppFlag;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwHostTopNSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwMatrixSupp
 Input       :  The Indices

                The Object 
                retValRmonHwMatrixSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwMatrixSupp (INT4 *pi4RetValRmonHwMatrixSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwMatrixSupp\n");
    *pi4RetValRmonHwMatrixSupp = gRmonSupportedGroups.u1RmonHwMatrixSuppFlag;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwMatrixSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetRmonHwEventSupp
 Input       :  The Indices

                The Object 
                retValRmonHwEventSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonHwEventSupp (INT4 *pi4RetValRmonHwEventSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetRmonHwEventSupp\n");
    *pi4RetValRmonHwEventSupp = gRmonSupportedGroups.u1RmonHwEventSuppFlag;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhGetRmonHwEventSupp \n");
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetRmonDebugType
 Input       :  The Indices

                The Object 
                setValRmonDebugType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonDebugType (UINT4 u4SetValRmonDebugType)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonDebugType\n");
    gu4RmondebgFlag = u4SetValRmonDebugType;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetRmonDebugType \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonEnableStatus
 Input       :  The Indices

                The Object 
                setValRmonEnableStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
                Also, It invokes RmonCreateTask, if STATUS is RMON_ENABLE.
                This function invokes RmonShutdown if STATUS is RMON_DISABLE
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonEnableStatus (INT4 i4SetValRmonEnableStatus)
{
    UINT4               u4TimerValue;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonEnableStatus\n");
    u4TimerValue = (UINT4) (ONE_SECOND * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    if (gRmonEnableStatusFlag == (UINT4) i4SetValRmonEnableStatus)
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC,
                  "The Previous RMON Enable status is same as the input.\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValRmonEnableStatus == RMON_ENABLE)
    {
#ifdef NPAPI_WANTED
        /* Make the required configurations in Target using NPAPI */
        RmonSetHWConfigurations (FNP_TRUE);
#endif
        /* Check for the VALID control table entries.
         * Start the stopped timers */
        /* Restart the timer */

        if (TmrStartTimer (gTimerListId1, &gOneSecTimer, u4TimerValue)
            != TMR_SUCCESS)
        {
            RMON_DBG (CRITICAL_TRC | EVENT_TMR, " Timer Restart failed. \n");

            return SNMP_FAILURE;
        }

        /* Set the RMON Module status as ENABLE */
        gRmonEnableStatusFlag = (UINT4) i4SetValRmonEnableStatus;
    }
    else if (i4SetValRmonEnableStatus == RMON_DISABLE)
    {
#ifdef NPAPI_WANTED
        /* Delete the configurations in target using NPAPI. */
        /* Make the required configurations in Target using NPAPI */
        RmonSetHWConfigurations (FNP_FALSE);
#else
        RmonClearEtherStatsTable ();
#endif
        /* Stop the Running Timers */
        TmrStopTimer (gTimerListId1, &gOneSecTimer);

        /* Set the RMON Module Status as Disable */
        gRmonEnableStatusFlag = (UINT4) i4SetValRmonEnableStatus;

    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonEnableStatus\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwStatsSupp
 Input       :  The Indices

                The Object 
                setValRmonHwStatsSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwStatsSupp (INT4 i4SetValRmonHwStatsSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonHwStatsSupp\n");

    if ((i4SetValRmonHwStatsSupp < 0) || (i4SetValRmonHwStatsSupp > 1))
    {

        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwStatsSuppFlag =
        (UINT1) i4SetValRmonHwStatsSupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwStatsSupp\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwHistorySupp
 Input       :  The Indices

                The Object 
                setValRmonHwHistorySupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwHistorySupp (INT4 i4SetValRmonHwHistorySupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonHwHistorySupp\n");

    if ((i4SetValRmonHwHistorySupp < 0) || (i4SetValRmonHwHistorySupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwHistorySuppFlag =
        (UINT1) i4SetValRmonHwHistorySupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwHistorySupp\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwAlarmSupp
 Input       :  The Indices

                The Object 
                setValRmonHwAlarmSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwAlarmSupp (INT4 i4SetValRmonHwAlarmSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonHwAlarmSupp\n");

    if ((i4SetValRmonHwAlarmSupp < 0) || (i4SetValRmonHwAlarmSupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwAlarmSuppFlag =
        (UINT1) i4SetValRmonHwAlarmSupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwAlarmSupp\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwHostSupp
 Input       :  The Indices

                The Object 
                setValRmonHwHostSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwHostSupp (INT4 i4SetValRmonHwHostSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonHwHostSupp\n");

    if ((i4SetValRmonHwHostSupp < 0) || (i4SetValRmonHwHostSupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwHostSuppFlag = (UINT1) i4SetValRmonHwHostSupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwHostSupp\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwHostTopNSupp
 Input       :  The Indices

                The Object 
                setValRmonHwHostTopNSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwHostTopNSupp (INT4 i4SetValRmonHwHostTopNSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonHwHostTopNSupp\n");

    if ((i4SetValRmonHwHostTopNSupp < 0) || (i4SetValRmonHwHostTopNSupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwHostTopNSuppFlag =
        (UINT1) i4SetValRmonHwHostTopNSupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwHostTopNSupp\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwMatrixSupp
 Input       :  The Indices

                The Object 
                setValRmonHwMatrixSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwMatrixSupp (INT4 i4SetValRmonHwMatrixSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonHwMatrixSupp\n");

    if ((i4SetValRmonHwMatrixSupp < 0) || (i4SetValRmonHwMatrixSupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwMatrixSuppFlag =
        (UINT1) i4SetValRmonHwMatrixSupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwMatrixSupp\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetRmonHwEventSupp
 Input       :  The Indices

                The Object 
                setValRmonHwEventSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetRmonHwEventSupp (INT4 i4SetValRmonHwEventSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : nmhSetRmonEventSupp\n");

    if ((i4SetValRmonHwEventSupp < 0) || (i4SetValRmonHwEventSupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a BOOLEAN. \n");
        return SNMP_FAILURE;
    }

    gRmonSupportedGroups.u1RmonHwEventSuppFlag =
        (UINT1) i4SetValRmonHwEventSupp;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhSetRmonHwEventSupp\n");
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2RmonDebugType
 Input       :  The Indices

                The Object 
                testValRmonDebugType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonDebugType (UINT4 *pu4ErrorCode, UINT4 u4TestValRmonDebugType)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonDebugType\n");
    RMON_DUMMY (u4TestValRmonDebugType);
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhTestv2RmonDebugType \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonEnableStatus
 Input       :  The Indices

                The Object 
                testValRmonEnableStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonEnableStatus (UINT4 *pu4ErrorCode, INT4 i4TestValRmonEnableStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonEnableStatus\n");
    RMON_DUMMY (i4TestValRmonEnableStatus);
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValRmonEnableStatus < 1) || (i4TestValRmonEnableStatus > 2))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  TruthValue. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhTestv2RmonEnableStatus \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwStatsSupp
 Input       :  The Indices

                The Object 
                testValRmonHwStatsSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwStatsSupp (UINT4 *pu4ErrorCode, INT4 i4TestValRmonHwStatsSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHwStatsSupp\n");

    if ((i4TestValRmonHwStatsSupp < 0) || (i4TestValRmonHwStatsSupp > 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving the function : nmhTestv2RmonHwStatsSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwHistorySupp
 Input       :  The Indices

                The Object 
                testValRmonHwHistorySupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwHistorySupp (UINT4 *pu4ErrorCode,
                            INT4 i4TestValRmonHwHistorySupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHwHistorySupp\n");

    if (!RMON_INCL_IN_BETWEEN (i4TestValRmonHwHistorySupp, 0, 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhTestv2RmonHwHistorySupp \n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwAlarmSupp
 Input       :  The Indices

                The Object 
                testValRmonHwAlarmSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwAlarmSupp (UINT4 *pu4ErrorCode, INT4 i4TestValRmonHwAlarmSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHwAlarmSupp\n");

    if (!RMON_INCL_IN_BETWEEN (i4TestValRmonHwAlarmSupp, 0, 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhTestv2RmonHwAlarmSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwHostSupp
 Input       :  The Indices

                The Object 
                testValRmonHwHostSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwHostSupp (UINT4 *pu4ErrorCode, INT4 i4TestValRmonHwHostSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHostSupp\n");

    if (!RMON_INCL_IN_BETWEEN (i4TestValRmonHwHostSupp, 0, 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhTestv2RmonHostSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwHostTopNSupp
 Input       :  The Indices

                The Object 
                testValRmonHwHostTopNSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwHostTopNSupp (UINT4 *pu4ErrorCode,
                             INT4 i4TestValRmonHwHostTopNSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHwHostTopNSupp\n");

    if (!RMON_INCL_IN_BETWEEN (i4TestValRmonHwHostTopNSupp, 0, 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhTestv2RmonHwHostTopNSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwMatrixSupp
 Input       :  The Indices

                The Object 
                testValRmonHwMatrixSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwMatrixSupp (UINT4 *pu4ErrorCode, INT4 i4TestValRmonHwMatrixSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHwMatrixSupp\n");

    if (!RMON_INCL_IN_BETWEEN (i4TestValRmonHwMatrixSupp, 0, 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhTestv2RmonHwMatrixSupp \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2RmonHwEventSupp
 Input       :  The Indices

                The Object 
                testValRmonHwEventSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2RmonHwEventSupp (UINT4 *pu4ErrorCode, INT4 i4TestValRmonHwEventSupp)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2RmonHwEventSupp\n");

    if (!RMON_INCL_IN_BETWEEN (i4TestValRmonHwEventSupp, 0, 1))
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC, "Value is not a  BOOLEAN. \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhTestv2RmonHwEventSupp \n");
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2RmonDebugType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonDebugType (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonEnableStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonEnableStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwStatsSupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwStatsSupp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwHistorySupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwHistorySupp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwAlarmSupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwAlarmSupp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwHostSupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwHostSupp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwHostTopNSupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwHostTopNSupp (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwMatrixSupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwMatrixSupp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2RmonHwEventSupp
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2RmonHwEventSupp (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : RmonStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRmonStatsTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceRmonStatsTable (INT4 i4EtherStatsIndex)
{
    return nmhValidateIndexInstanceEtherStatsTable (i4EtherStatsIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexRmonStatsTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexRmonStatsTable (INT4 *pi4EtherStatsIndex)
{
    return nmhGetFirstIndexEtherStatsTable (pi4EtherStatsIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexRmonStatsTable
 Input       :  The Indices
                EtherStatsIndex
                nextEtherStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexRmonStatsTable (INT4 i4EtherStatsIndex,
                               INT4 *pi4NextEtherStatsIndex)
{
    return nmhGetNextIndexEtherStatsTable (i4EtherStatsIndex,
                                           pi4NextEtherStatsIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetRmonStatsOutFCSErrors
 Input       :  The Indices
                EtherStatsIndex

                The Object 
                retValRmonStatsOutFCSErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonStatsOutFCSErrors (INT4 i4EtherStatsIndex,
                             UINT4 *pu4RetValRmonStatsOutFCSErrors)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetRmonStatsOutFCSErrors \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValRmonStatsOutFCSErrors = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValRmonStatsOutFCSErrors,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsOutFCSErrors);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValRmonStatsOutFCSErrors,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsOutFCSErrors);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetRmonStatsPkts1519to1522Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object 
                retValRmonStatsPkts1519to1522Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetRmonStatsPkts1519to1522Octets (INT4 i4EtherStatsIndex,
                                     UINT4
                                     *pu4RetValRmonStatsPkts1519to1522Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsPkts1519to1522Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValRmonStatsPkts1519to1522Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValRmonStatsPkts1519to1522Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts1519to1522Octets);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValRmonStatsPkts1519to1522Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts1519to1522Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;

}
