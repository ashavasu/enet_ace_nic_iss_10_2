/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmoncfif.c,v 1.15 2010/08/06 11:24:49 prabuc Exp $            *
 *                                                                  *
 * Description: Contains Function to notify the RMON Task abt the   *   
 *              change in Interface Status                          *
 *                                                                  *
 *******************************************************************/

#include "rmallinc.h"
#include "cfa.h"

/*****************************************************************************/
/* Function        : RmonUpdateInterfaceStatus                               */
/* Description     : This function is called from CFA whenever the Interface */
/*                   Status is changed.                                      */
/* Input(s)        : u4Port, u1OperStatus                                    */
/*                                                                           */
/* Output(s)       : None                                                    */
/*                                                                           */
/* Returns         : RMON_SUCCESS, RMON_FAILURE                              */
/*                                                                           */
/* Action          : Invoked by CFA to post an Event to the RMON Task.       */
/*                                                                           */
/*****************************************************************************/

INT4
RmonUpdateInterfaceStatus (UINT4 u4Port, UINT1 u1OperStatus)
{

    UNUSED_PARAM (u4Port);
    switch (u1OperStatus)
    {
        case CFA_IF_UP:
            break;
            /* When Interface is UP, nothing to be done. */
        case CFA_IF_DORM:
            break;

            /* When Interface is down, all VALID control entries need to be 
             * made INVALID. */
        case CFA_IF_DOWN:
            break;
            /* When Interface is UP, nothing to be done. */
        default:
            break;
    }                            /* End of Switch */

    return RMON_SUCCESS;

}
