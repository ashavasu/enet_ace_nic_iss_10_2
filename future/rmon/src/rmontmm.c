/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmontmm.c,v 1.18 2013/07/12 12:35:05 siva Exp $             *
 *                                                                  *
 * Description: Contains implementation of Time Expiry Functions for*
 *              Control Tables                                      *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmontmm.c                                      
 *                                                                           
 *    PRINCIPAL AUTHOR       :  Aricent Inc.                             
 *                                                                          
 *    SUBSYSTEM NAME         :  EVENT MODULE                                
 *                                                                          
 *    MODULE NAME            :  RMON PERIODIC                                   
 *                                                                           
 *    LANGUAGE               :  C                                      
 *                                                                           
 *    TARGET ENVIRONMENT     :  ANY                                          
 *                                                                           
 *    DATE OF FIRST RELEASE  :                                               
 *                                                                           
 *    DESCRIPTION            :   Contains Timer & Event related routines
 *                                                                           
 *  --------------------------------------------------------------------------  
 *
 *     CHANGE RECORD : 
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |         DESCRIPTION OF    
 *                  |        DATE          |            CHANGE         
 * -----------------|----------------------|---------------------------------
 *      1.0         |       Vignesh D      |        Create Original         
 *                  |     20-MAR-1997      |                                 
 * -------------------------------------------------------------------------
 *      2.0         |    Bhupendra          | RmonGetTableTimer              
 *                  |                       | function is added            
 *                  |   10-AUG-2001         |                               
 * -------------------------------------------------------------------------
 *      3.0         |       LEELA L        |    Changes for Dynamic Tables
 *                  |     31-Mar-2002      |                                 
 * -------------------------------------------------------------------------
 *      4.0         |    Leela L            | RmonGetTableTimer              
 *                  |                       | function is modified         
 *                  |   07-JUL-2002         |                               
****************************************************************************/

#include "rmallinc.h"

#define MATRIX_TABLE(index)     gaMatrixControlTable[index].pMatrixControlEntry

/****************************************************************************
 *    Function Name        : RmonGetTableTimer                              *
 *    Role of the function : This function Returns the Address of the       *
 *                           timer, called by RmonStartTimer and            *
 *                           RmonStopTimer  function.                       *
 *                                                                          *
 *    Formal Parameters    : u1Type : Table Type                            *
 *                           i4Index : Index within Table                  *
 *                                                                          *
 *    Global Variables     : None                                           *
 *    Side Effects         : None                                           *
 *    Exception Handling   : None                                           *   
 *    Use of Recursion     : None                                           * 
 *    Return Value         : Pointer to the Application Timer               *
****************************************************************************/

tTmrAppTimer       *
RmonGetTableTimer (UINT1 u1Type, INT4 i4Index)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tRmonAlarmNode     *pAlarmNode = NULL;
    tRmonEventNode     *pEventNode = NULL;
    tTmrAppTimer       *pTmpTimer = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonGetTableTimer  \n");
    switch (u1Type)
    {
        case ETHER_STATS_TABLE:
        {
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      " EtherStats Table timer Expired  \n");

            pEtherStatsNode =
                HashSearchEtherStatsNode (gpRmonEtherStatsHashTable, i4Index);

            /* Check whether the node is present in Hash Table otherwise in
             * SLL */
            if (pEtherStatsNode != NULL)
            {
                RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                          "Node found in Hash Scan \n");
                pTmpTimer = &(pEtherStatsNode->etherTableTimer);
            }
            else
            {
                TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                              tRmonEtherStatsTimerNode *)
                {
                    if (pEtherStatsNode != NULL)
                    {
                        if (pEtherStatsNode->i4EtherStatsIndex == i4Index)
                        {
                            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                                      "Node found in SLL Scan \n");
                            pTmpTimer = &(pEtherStatsNode->etherTableTimer);
                        }
                    }
                }
            }
            return (pTmpTimer);
        }

        case HISTORY_CONTROL_TABLE:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      " HistoryControl Table timer Expired  \n");

            pHistoryControlNode =
                HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                              i4Index);

            if (pHistoryControlNode != NULL)
            {
                return &(pHistoryControlNode->EtherHistoryTimer);
            }
            else
            {
                return NULL;
            }

        case ALARM_TABLE:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      "Alarm Table timer Expired  \n");

            pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4Index);

            if (pAlarmNode != NULL)
            {
                return &(pAlarmNode->AlarmTableTimer);
            }
            else
            {
                return NULL;
            }

        case HOST_CONTROL_TABLE:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      "HostControl Table timer Expired  \n");

            return &(gaHostControlTable[i4Index].pHostControlEntry->
                     hostControlTimer);

        case HOST_TOPN_CONTROL_TABLE:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      " HostTopNControl Table timer Expired  \n");

            return &(TOPN_TABLE (i4Index)->HostTopNControlTimer);

        case MATRIX_CONTROL_TABLE:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      " MatrixControl Table timer Expired  \n");

            return &(MATRIX_TABLE (i4Index)->MatrixControlTimer);

        case EVENT_TABLE:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC,
                      " Event Table timer Expired  \n");

            pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4Index);

            if (pEventNode != NULL)
            {
                return &(pEventNode->EventTableTimer);
            }
            else
            {
                return NULL;
            }

        default:
            RMON_DBG (MINOR_TRC | CTRL_TMR_TRC, "Timer does't exist \n");
            RMON_DBG (MINOR_TRC | FUNC_EXIT,
                      "Leaving  function : RmonGetTableTimer \n");
            return NULL;
    }
}

/****************************************************************************
*      Function Name        : RmonProcessTimeOut                            *
*      Role of the function : This function calls  Timer Expiry Handler     *
*                             corresponding to the Table                    *
*      Formal Parameters    : u4TimerRef : Timer Reference                  *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : None                                          *
****************************************************************************/

void
RmonProcessTimeOut (u4TmrRef)
     UINT4               u4TmrRef;
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonProcessTimeOut  \n");

    switch (RMON_GET_TIMER_REFERENCE_TABLE_TYPE (u4TmrRef))
    {
        case ETHER_STATS_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC,
                      "Ether Stats Table Timer expired \n");

            RmonEtherStatTimerExpiry (u4TmrRef);
            break;

        case HISTORY_CONTROL_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC,
                      "History Control Table Timer expired \n");

            RmonHistCtrlTimerExpiry (u4TmrRef);
            break;

        case ALARM_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC, "Alarm Table Timer expired \n");
            RmonAlrmTimerExpiry (u4TmrRef);
            break;

        case HOST_CONTROL_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC,
                      " Host Control Table Timer Expired \n");

            RmonHostCtrlTimerExpiry (u4TmrRef);
            break;

        case HOST_TOPN_CONTROL_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC,
                      " Host Topn Control Table Timer Expired \n");
            RmonTopNCtrlTimerExpiry (u4TmrRef);
            break;

        case MATRIX_CONTROL_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC,
                      "Matrix Control Table Timer Expired \n");

            RmonMtrxCtrlTimerExpiry (u4TmrRef);
            break;

        case EVENT_TABLE:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC, "Event Table Timer Expired \n");

            RmonEvntTimerExpiry (u4TmrRef);
            break;
        default:
            RMON_DBG (MAJOR_TRC | CTRL_TMR_TRC, "Request for Unknown Table \n");
            break;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving function :RmonProcessTimeOut \n");
}

/****************************************************************************
*      Function Name        : RmonTmmSendEvent                              *
*      Role of the function : This function calls  Timer Expiry Handler     *
*                             corresponding to the Module                   *
*      Formal Parameters    : u1ModuleID : Module ID                        *
*                             u4TimerRef : Timer Reference                  *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : None                                          *
****************************************************************************/

void
RmonTmmSendEvent (UINT1 u1ModuleID, UINT4 u4TimerRef)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonTmmSendEvent  \n");

    RMON_DBG2 (MINOR_TRC | RMON_EVENT_TRC,
               "Module ID = %d,Timer Reference = %x ", u1ModuleID, u4TimerRef);

    if (u1ModuleID == RMON_MODULE)
    {
        RmonProcessTimeOut (u4TimerRef);
    }
    else
    {
        RMON_DBG (MINOR_TRC | RMON_EVENT_TRC,
                  "Unknown Module for Timer expiry \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving function :RmonTmmSendEvent  \n");
}

/****************************************************************************
*      Function Name        : RmonStartTimer                                *  
*      Role of the function : This function starts the control table timer  *
*                             based on the RMON Groups.                     * 
*      Formal Parameters    : u1Type: Table Type                            *
*                             i4Index: Table entry Index                    *
*                             i4Duration: Timer Duration                    *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : TMR_FAILURE/TMR_SUCCESS                       *
****************************************************************************/

INT4
RmonStartTimer (UINT1 u1Type, INT4 i4Index, INT4 i4Duration)
{
    UINT4               u4TimerRef = 0;
    UINT4               u4TimerValue;
    tTmrAppTimer       *pTimerRef = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonStartTimer  \n");

    pTimerRef = RmonGetTableTimer (u1Type, i4Index);

    if (pTimerRef == NULL)
    {
        RMON_DBG2 (MAJOR_TRC | RMON_EVENT_TRC,
                   "TimerReference not allocated for Table type = %d,Table Index = %x ",
                   u1Type, i4Index);
        return ((INT4) TMR_FAILURE);
    }
    else
    {

        RMON_SET_TIMER_REFERENCE_MODULE_TYPE (u4TimerRef, RMON_MODULE);
        RMON_SET_TIMER_REFERENCE_TABLE_TYPE (u4TimerRef, u1Type);
        RMON_SET_TIMER_REFERENCE_INDEX (u4TimerRef, (UINT2) i4Index);

        pTimerRef->u4Data = u4TimerRef;

        u4TimerValue = ((UINT4) i4Duration * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

        RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving  function : RmonSartTimer\n");

        return ((INT4) TmrStartTimer (gTimerListId2, pTimerRef, u4TimerValue));
    }

}

/****************************************************************************
*      Function Name        : RmonStopTimer                                 *
*      Role of the function : This function stops the RMON control table    *
*                             timer if the row sattus has been set to VALID.*
*      Formal Parameters    : u1Type: Table Type                            *
*                             i4Index: Table entry index                    *
*      Global Variables     : None                                          *
*      Side Effects         : None                                          *
*      Exception Handling   : None                                          *
*      Use of Recursion     : None                                          *
*      Return Value         : TMR_FAILURE/TMR_SUCCESS                       *
****************************************************************************/
INT4
RmonStopTimer (UINT1 u1Type, INT4 i4Index)
{
    tTmrAppTimer       *pTimerRef = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonStopTimer \n");

    pTimerRef = RmonGetTableTimer (u1Type, i4Index);
    if (pTimerRef == NULL)
    {
        RMON_DBG2 (MAJOR_TRC | RMON_EVENT_TRC,
                   "TimerReference not available for Table type = %d,Table Index = %x ",
                   u1Type, i4Index);
        return ((INT4) TMR_FAILURE);
    }
    else
    {
        pTimerRef->u4Data = RMON_ZERO;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving  function :RmonStopTimer  \n");

    return ((INT2) TmrStopTimer (gTimerListId2, pTimerRef));
}
