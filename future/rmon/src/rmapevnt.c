/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmapevnt.c,v 1.29 2015/04/23 12:04:49 siva Exp $            * 
 *                                                                  *
 * Description: Contains implementation of Functions to send the SNMP*
 *                                                                  *
 *              TRAP, to make the SNMPOID from string & to generate *
 *              the Event on occurence.                             *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmapevnt.c                                      
 *                                                                           
 *    PRINCIPAL AUTHOR       :  Aricent Inc.                             
 *                                                                          
 *    SUBSYSTEM NAME         :  EVENT MODULE                                
 *                                                                          
 *    MODULE NAME            :  RMON APERIODIC                                  
 *                                                                           
 *    LANGUAGE               :  C                                      
 *                                                                           
 *    TARGET ENVIRONMENT     :  ANY                                          
 *                                                                           
 *    DATE OF FIRST RELEASE  :                                               
 *                                                                           
 *    DESCRIPTION            : This file contains the routines to generate   
 *                             SNMP Trap and to update the log table.          
 *                                                                           
 *                                                                           
 *  --------------------------------------------------------------------------  
 *
 *     CHANGE RECORD : 
 *     -------------
 *
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |         DESCRIPTION OF    
 *                  |        DATE          |            CHANGE         
 * -----------------|----------------------|---------------------------------
 *      1.0         |  Vijay G. Krishnan   |        Create Original         
 *                  |     20-MAR-1997      |                                     *-------------------------------------------------------------------------
 *      2.0          |  Bhupendra           |                      
 *                   |  13-AUG-2001         |                        
 * --------------------------------------------------------------------------
 *      3.0          |  LEELA L             | Changes for Dynamic Tables       
 *                   |  31-MAR-2002         |                        
 * --------------------------------------------------------------------------
 *      4.0          |  LEELA L             | Changes for New MIDGEN Tool      
 *                   |  07-JUL-2002         |                        
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"
#ifdef SNMP_2_WANTED
#include "rmonoidt.h"
#endif

/**********************************************************************
* Function             : SendTrapMessage                              *
*                                                                     *
* Role of the function : This function sends trap to the manager. It  *   
*                        uses the SNMP agent function to send trap.   *
* Formal Parameters    : u1TrapType   : The type of trap to be        *
*                                         generated                   *
*                      : i4AlarmIndex  : Index of the Alarm table     * 
*                      : i4EventIndex : Index of the event table      * 
*                                                                     *    
* Global Variables     : None                                         *        
* Side Effects         : None                                         *        
* Exception Handling   : None                                         *        
* Use of Recursion     : None                                         *        
* Return Value         : None                                         *        
**********************************************************************/

static void
SendTrapMessage (UINT1 u1TrapType, INT4 i4AlarmIndex, INT4 i4EventIndex)
{
#ifdef SNMP_3_WANTED
    INT1                RMONTRAPSOID[] = RMON_OID;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL;
    UINT1               au1Buf[OBJECT_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tRmonEventNode     *pEventNode = NULL;
    tRmonAlarmNode     *pAlarmNode = NULL;

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    RMON_DBG1 (MINOR_TRC | FUNC_ENTRY,
               "Entering into the function: SendTrapMessage,Traptype = %d\n",
               u1TrapType);

    if ((u1TrapType < 1) || (u1TrapType > 2))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, " Unknown Trap Type\n");
        return;
    }

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);
    if (pEventNode == NULL)
    {
        return;
    }

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode == NULL)
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Alarm Entry is not Configured\n");
        return;
    }

    pEnterpriseOid = RMON_AGT_GetOidFromString ((INT1 *) RMONTRAPSOID);
    if (pEnterpriseOid == NULL)
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Enterprise Oid is returned as NULL \n");
        return;
    }

    u4SpecTrapType = u1TrapType;
    u4GenTrapType = ENTERPRISE_SPECIFIC;

    SPRINTF ((char *) au1Buf, "alarmIndex");

    pOid = RmonMakeObjIdFromDotNew ((UINT1 *) au1Buf);

    if (pOid == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "NULL OID Returned for alarmIndex Object \n");
        /* Release the memory Assigned for Entrerprise Oid */
        rmon_free_oid (pEnterpriseOid);
        return;
    }

    pVbList = ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                        SNMP_DATA_TYPE_INTEGER,
                                                        0, i4AlarmIndex, NULL,
                                                        NULL, pSnmpCnt64Type));

    if (pVbList == NULL)
    {
        /* Release memory Associated with pOid */
        rmon_free_oid (pOid);
        rmon_free_oid (pEnterpriseOid);
        pEnterpriseOid = NULL;
        return;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "alarmVariable");

    pOid = RmonMakeObjIdFromDotNew ((UINT1 *) au1Buf);

    if (pOid == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "NULL OID Returned for alarmVariable  Object \n");
        RMON_FreeVarbindList (pStartVb);
        pVbList = NULL;
        rmon_free_oid (pEnterpriseOid);
        return;
    }

    pOidValue =
        RMON_AGT_FormOid (pAlarmNode->alarmVariable.pu4_OidList,
                          (INT2) pAlarmNode->alarmVariable.u4_Length);
    if (pOidValue == NULL)
    {

        RMON_FreeVarbindList (pStartVb);
        pVbList = NULL;
        rmon_free_oid (pEnterpriseOid);
        /* Release memory Associated with pOid */
        rmon_free_oid (pOid);
        return;
    }

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OBJECT_ID,
                                                  0, 0, NULL, pOidValue,
                                                  pSnmpCnt64Type));

    pVbList = pVbList->pNextVarBind;
    if (pVbList == NULL)
    {
        RMON_FreeVarbindList (pStartVb);
        rmon_free_oid (pOid);
        rmon_free_oid (pEnterpriseOid);
        return;
    }

    SPRINTF ((char *) au1Buf, "alarmSampleType");

    pOid = RmonMakeObjIdFromDotNew ((UINT1 *) au1Buf);

    if (pOid == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "NULL OID Returned for alarmSampleType Object \n");
        /* Release the memory Allocated  at the time of forming OID of
           alarmIndex Object .
         */
        RMON_FreeVarbindList (pStartVb);
        pVbList = NULL;
        rmon_free_oid (pEnterpriseOid);
        return;
    }

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER, 0,
                                                  pAlarmNode->
                                                  alarmSampleType, NULL, NULL,
                                                  pSnmpCnt64Type));
    pVbList = pVbList->pNextVarBind;

    if (pVbList == NULL)
    {
        /* Release memory Associated with Variable pStartVb and with pOid */
        RMON_FreeVarbindList (pStartVb);
        rmon_free_oid (pOid);
        rmon_free_oid (pEnterpriseOid);
        return;
    }

    SPRINTF ((char *) au1Buf, "alarmValue");

    pOid = RmonMakeObjIdFromDotNew ((UINT1 *) au1Buf);

    if (pOid == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "NULL OID Returned for alarmValue Object \n");
        /* Release the memory Associated with pStartVb */
        RMON_FreeVarbindList (pStartVb);
        pVbList = NULL;
        rmon_free_oid (pEnterpriseOid);
        return;

    }

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER, 0,
                                                  (INT4) pAlarmNode->
                                                  u4AlarmValue, NULL, NULL,
                                                  pSnmpCnt64Type));
    pVbList = pVbList->pNextVarBind;

    if (pVbList == NULL)
    {
        RMON_FreeVarbindList (pStartVb);
        rmon_free_oid (pOid);
        rmon_free_oid (pEnterpriseOid);
        return;
    }

    if (u1TrapType == IF_RISING_ALARM_TRAP)
    {
        SPRINTF ((char *) au1Buf, "alarmRisingThreshold");
        pOid = RmonMakeObjIdFromDotNew ((UINT1 *) au1Buf);
        if (pOid == NULL)
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "NULL OID Returned for alarmRisingThreshold Object \n");
            RMON_FreeVarbindList (pStartVb);
            pVbList = NULL;
            rmon_free_oid (pEnterpriseOid);
            return;
        }

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
             (pOid, SNMP_DATA_TYPE_INTEGER, 0,
              (INT4) pAlarmNode->u4AlarmRisingThreshold,
              NULL, NULL, pSnmpCnt64Type));

        pVbList = pVbList->pNextVarBind;

        if (pVbList == NULL)
        {
            RMON_FreeVarbindList (pStartVb);
            rmon_free_oid (pOid);
            rmon_free_oid (pEnterpriseOid);
            return;
        }
    }

    else if (u1TrapType == IF_FALLING_ALARM_TRAP)
    {
        SPRINTF ((char *) au1Buf, "alarmFallingThreshold");
        pOid = RmonMakeObjIdFromDotNew ((UINT1 *) au1Buf);

        if (pOid == NULL)
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "NULL OID Returned for alarmFallingThreshold Object \n");
            RMON_FreeVarbindList (pStartVb);
            pVbList = NULL;
            rmon_free_oid (pEnterpriseOid);
            return;
        }

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
             (pOid, SNMP_DATA_TYPE_INTEGER, 0,
              (INT4) pAlarmNode->u4AlarmFallingThreshold,
              NULL, NULL, pSnmpCnt64Type));

        pVbList = pVbList->pNextVarBind;
        if (pVbList == NULL)
        {
            RMON_FreeVarbindList (pStartVb);
            rmon_free_oid (pOid);
            rmon_free_oid (pEnterpriseOid);
            return;
        }
    }
    /* following Api is called to send the trap to SNMP_Agent */

    SNMP_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                      u4SpecTrapType, pStartVb, &(pEventNode->EventCommunity));
#else
    UNUSED_PARAM (u1TrapType);
    UNUSED_PARAM (i4AlarmIndex);
    UNUSED_PARAM (i4EventIndex);
#endif /* SNMP_2_WANTED */

    RMON_DBG (MINOR_TRC | FUNC_EXIT, " Leaving function: SendTrapMessage \n");

}

/**********************************************************************
* Function             : RmonMakeObjIdFromDotNew                      *
*                                                                     *
* Role of the function : This function returns the pointer to the     *
*                         object id of the passed object to it.       * 
* Formal Parameters    : textStr  :  pointer to the object whose      *
                                      object id is to be found        *
* Global Variables     : None                                         *
* Side Effects         : None                                         *
* Exception Handling   : None                                         *
* Use of Recursion     : None                                         *
* Return Value         : pOid : pointer to object id.                 *
**********************************************************************/

tSNMP_OID_TYPE     *
RmonMakeObjIdFromDotNew (UINT1 *textStr)
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *pOid = NULL;
    INT1               *pTemp = NULL, *pDot = NULL;
    UINT2               i;
    UINT2               u2dotCount;
    UINT1               au1tempBuffer[OBJECT_NAME_LEN + 1];
    UINT2               u2BufferLen = 0;
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: RmonMakeObjIdFromDotNew \n");

    /* Is there an alpha descriptor at begining ?? */
    if (RMON_ISALPHA (*textStr))
    {
        pDot = ((INT1 *) STRCHR ((char *) textStr, '.'));

        /* if no dot, point to end of string */
        if (pDot == NULL)
        {
            pDot = ((INT1 *) (textStr + STRLEN ((char *) textStr)));
        }
        pTemp = ((INT1 *) textStr);

        MEMSET (au1tempBuffer, 0, OBJECT_NAME_LEN + 1);
        STRNCPY (au1tempBuffer, pTemp, (pDot - pTemp));
        au1tempBuffer[pDot - pTemp] = '\0';

        for (i = 0; ((i < (sizeof (origMibOidTable) / sizeof (struct MIB_OID)))
                     && (origMibOidTable[i].pName != NULL)); i++)
        {
            if ((STRCMP (origMibOidTable[i].pName, (INT1 *)
                         au1tempBuffer) == 0) &&
                ((UINT4) (pDot - pTemp) == STRLEN (origMibOidTable[i].pName)))
            {

                u2BufferLen =
                    (UINT2) (STRLEN (origMibOidTable[i].pNumber) <
                            (sizeof(au1tempBuffer)-1) ?
                            STRLEN (origMibOidTable[i].pNumber) :sizeof(au1tempBuffer)-1 );

                STRNCPY ((INT1 *) au1tempBuffer, origMibOidTable[i].pNumber,
                         u2BufferLen);
                au1tempBuffer[u2BufferLen] = '\0';
                break;
            }
        }

        if ((i < (sizeof (origMibOidTable) / sizeof (struct MIB_OID))) &&
            origMibOidTable[i].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = (UINT2) (sizeof (au1tempBuffer) - STRLEN (au1tempBuffer));
        STRNCAT ((INT1 *) au1tempBuffer, (INT1 *) pDot,
                 (u2BufferLen < STRLEN (pDot) ? u2BufferLen : STRLEN (pDot)));
    }
    else
    {                            /* is not alpha, so just copy into au1tempBuffer */
        STRCPY ((INT1 *) au1tempBuffer, (INT1 *) textStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2dotCount = 0;
    for (i = 0; ((i < OBJECT_NAME_LEN + 1) && (au1tempBuffer[i] != '\0')); i++)
    {
        if (au1tempBuffer[i] == '.')
        {
            u2dotCount++;
        }
    }

    if ((pOid = alloc_oid (u2dotCount + 1)) == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC, " CALLOC  Failure for:  pOid. \n");
        return (NULL);
    }

    pOid->u4_Length = (UINT4) (u2dotCount + 1);
    /* now we convert number.number.... strings */
    pTemp = (INT1 *) au1tempBuffer;
    for (i = 0; i < (u2dotCount + 1); i++)
    {
        if ((pTemp =
             ((INT1 *) (RmonParseSubIdNew ((UINT1 *) pTemp,
                                           &pOid->pu4_OidList[i])))) == NULL)
        {
            rmon_free_oid (pOid);
            return (NULL);
        }

        if (*pTemp == '.')
        {
            pTemp++;            /* to skip over dot */
        }
        else if (*pTemp != '\0')
        {
            rmon_free_oid (pOid);
            return (NULL);
        }
    }                            /* end of for loop */

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving function:RmonMakeObjIdFromDotNew  \n");
    return (pOid);
#else
    UNUSED_PARAM (textStr);
    return (NULL);
#endif
}

/**********************************************************************
* Function             : RmonParseSubIdNew                            *
*                                                                     *
* Role of the function : Returns the OID of the passed object if found*  
* Formal Parameters    : **pTemp = Object Name whose oid to be found  * 
*                                                                     *
* Global Variables     : None                                         *
* Side Effects         : None                                         *
* Exception Handling   : None                                         *
* Use of Recursion     : None                                         *
* Return Value         : OID                                          *
**********************************************************************/

UINT1              *
RmonParseSubIdNew (UINT1 *pTemp, UINT4 *pu4Value)
{
    INT4                value = 0;
    UINT1              *tmp = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: RmonParseSubIdNew \n");

    for (tmp = pTemp; (((*tmp >= '0') && (*tmp <= '9')) ||
                       ((*tmp >= 'a') && (*tmp <= 'f')) ||
                       ((*tmp >= 'A') && (*tmp <= 'F'))); tmp++)
    {
        value = ((value * 10) + (*tmp & 0xf));
    }

    if (pTemp == tmp)
    {
        tmp = NULL;
    }
    *pu4Value = (UINT4) value;
    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving function: RmonParseSubIdNew \n");
    return (tmp);
}

/******************************************************************************
*      Function             : RmonGenerateEvent                               *
*                                                                             *
*      Role of the function : This function generates event specified in the  *
*                             Event Table.                                    *
*      Formal Parameters    : i4EventIndex : The index of the Event Table     *
*      Global Variables     : gaEventTable                                    *
*      Side Effects         : u4_eventLastTimeSent in gaEventTable is updated *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonGenerateEvent (INT4 i4AlarmIndex, UINT1 u1TrapType, INT4 i4EventIndex)
{

    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: RmonGenerateEvent \n");

    if ((i4EventIndex == 0) || (i4EventIndex > RMON_MAX_EVENT_INDEX))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, " No Event Configured.\n");
        /* If rising/falling eventIndex is zero nothing is to be done */
        return;
    }

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if ((pEventNode != NULL) && (pEventNode->eventStatus == RMON_VALID))
    {
        switch (pEventNode->eventType)
        {
            case LOG:
                RmonLogEvent (i4EventIndex);
                break;

            case SNMP_TRAP:
                SendTrapMessage (u1TrapType, i4AlarmIndex, i4EventIndex);
                break;

            case LOG_AND_TRAP:
                RmonLogEvent (i4EventIndex);
                SendTrapMessage (u1TrapType, i4AlarmIndex, i4EventIndex);
                break;

            case RMON_NONE:
                break;
            default:
                break;
        }

        RMON_T_GET_TICK ((tOsixSysTime *) & (pEventNode->eventLastTimeSent));
    }
    else
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Entry Status is not in VALID State \n");
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT, " Leaving function: RmonGenerateEvent \n");
}
