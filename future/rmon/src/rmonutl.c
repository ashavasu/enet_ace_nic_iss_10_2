/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmonutl.c,v 1.44 2017/01/17 14:10:30 siva Exp $             *
 *                                                                  *
 * Description: Contains implementation of Functions to get SNMP    *
 *              MIB variable values and also to test/set/get the    *
 *              DataSourceOid.                                      *
 *                                                                  *
 *******************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  rmonutl.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  COMMON                                       |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides routines called          |
 * |                             by low level routines to read/write OID     |
 * |                             and to get the value of an object given     |
 * |                             the OID.                                    |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |    Vijay G Krishnan |         Create Original          |
 * |                |    19-MAR-1997      |                                  |
 * |----------------|---------------------|----------------------------------|
 * |  2.0           |    Bhupendra        |  RmonGetMibVariable function     |
 * |                |    13-AUG-2001      |  modified                        |
 *  -------------------------------------------------------------------------
 * |  3.0           |    LEELA L          |  Changes for Dynamic Tables      |
 * |                |    31-MAR-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  4.0           |    LEELA L          |  Fixed warnings                  |
 * |                |    28-JUN-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  5.0           |    LEELA L          |  Modified for new MIDGEN Tool    |
 * |                |    07-JUL-2002      |                                  |
 *  -------------------------------------------------------------------------
 *
 */

#include "rmallinc.h"
#include "fsrmoncli.h"
#include "stdrmocli.h"

PRIVATE INT1        RmonDeleteEtherStatsNode (tRmonEtherStatsTimerNode *);
PRIVATE VOID        RmonDeleteHistoryControlNode (tRmonHistoryControlNode *);
/******************************************************************************
*      Function Name        : RmonFormDataSourceOid                           *
*                                                                             *
*      Role of the function : RmonFormDataSourceOid function  forms the       *
*                             DataSource Oid from the interface number or     *
*                             Vlan Index                                      *
*      Formal Parameters    : pDataSourceOid, u4IfIndex, u4Type               *
*      Global Variables     : None                                            *
*      Side Effects         : None                                            *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonFormDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid,
                       UINT4 u4PortOrVlanIndex, UINT4 u4Type)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : RmonFormDataSourceOid \n");

    /* grlInterfaceOid and grlVlanOid are Global Variables containing */
    /* the OIDs of Interface Table and Vlan Table respectively */
    /* The DataSource (UINT1) is appended to this based on u4Type and sent. */
    if (u4PortOrVlanIndex == 0)
    {
        pDataSourceOid->u4_Length = 0;
    }
    else if (u4Type == PORT_INDEX_TYPE)
    {
        MEMCPY (pDataSourceOid->pu4_OidList, grlInterfaceOid.pu4_OidList,
                (grlInterfaceOid.u4_Length * sizeof (UINT4)));
        pDataSourceOid->pu4_OidList[grlInterfaceOid.u4_Length] =
            u4PortOrVlanIndex;
        pDataSourceOid->u4_Length = (grlInterfaceOid.u4_Length + 1);
    }
    else if (u4Type == VLAN_INDEX_TYPE)
    {
        MEMCPY (pDataSourceOid->pu4_OidList, grlVlanOid.pu4_OidList,
                (grlVlanOid.u4_Length * sizeof (UINT4)));
        pDataSourceOid->pu4_OidList[grlVlanOid.u4_Length] = u4PortOrVlanIndex;
        pDataSourceOid->u4_Length = (grlVlanOid.u4_Length + 1);
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving Function : RmonFormDataSourceOid \n");

}

/******************************************************************************
*      Function Name        : RmonSetDataSourceOid                            *
*                                                                             *
*      Role of the function : RmonSetDataSourceOid function extracts the      *
*                             interface number or Vlan Index and their type   *
*                             from the DataSource Oid                         *
*      Formal Parameters    : pDataSourceOid, pu4IfIndex, pu4OIDType          *
*      Global Variables     : None                                            *
*      Side Effects         : None                                            *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonSetDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid,
                      UINT4 *pu4PortOrVlanIndex, UINT4 *pu4OIDType)
{

    UINT4               u4OIDType;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : RmonSetDataSourceOid \n");

    /* grlInterfaceOid and grlVlanOid are Global Variables containing */
    /* the OIDs of Interface Table and Vlan Table respectively */
    /* Based on the value of u4OIDType set by the function */
    /* RmonCheckDataSourceOIDIsVlanOrInterface, Interface number or */
    /* Vlan Index is extracted from pDataSourceOid */

    RmonCheckDataSourceOIDIsVlanOrInterface (pDataSourceOid, &u4OIDType);

    if (u4OIDType == PORT_INDEX_TYPE)
    {
        *pu4OIDType = PORT_INDEX_TYPE;
        *pu4PortOrVlanIndex =
            *(pDataSourceOid->pu4_OidList + grlInterfaceOid.u4_Length);
    }
    else if (u4OIDType == VLAN_INDEX_TYPE)
    {
        *pu4OIDType = VLAN_INDEX_TYPE;
        *pu4PortOrVlanIndex =
            *(pDataSourceOid->pu4_OidList + grlVlanOid.u4_Length);
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving Function : RmonSetDataSourceOid \n");
}

/******************************************************************************
*      Function Name        : RmonTestDataSourceOid                           *
*                                                                             *
*      Role of the function : RmonTestDataSourceOid function validates the    *
*                             Data Source Oid                                 * 
*      Formal Parameters    : pDataSourceOid                                  *
*      Global Variables     : None                                            *
*      Side Effects         : None                                            *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE or FALSE                                   *
******************************************************************************/

INT1
RmonTestDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid)
{

    UINT4               u4OIDType;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : RmonTestDataSourceOid \n");

    /* grlInterfaceOid and grlVlanOid are Global Variables containing */
    /* the OIDs of Interface Table and Vlan Table respectively */
    /* Based on the value of u4OIDType set by the function */
    /* RmonCheckDataSourceOIDIsVlanOrInterface, Interface number or */
    /* Vlan Index is extracted from pDataSourceOid and then validated */

    if (RmonCheckDataSourceOIDIsVlanOrInterface (pDataSourceOid, &u4OIDType) ==
        RMON_FAILURE)
    {
        RMON_DBG (CRITICAL_TRC | MGMT_TRC, "RMON_INVALID DataSource Oid\n");
        return FALSE;
    }

    if (u4OIDType == PORT_INDEX_TYPE)
    {
        if ((pDataSourceOid->u4_Length == (grlInterfaceOid.u4_Length + 1)) &&
            (MEMCMP (pDataSourceOid->pu4_OidList, grlInterfaceOid.pu4_OidList,
                     (sizeof (UINT4) * grlInterfaceOid.u4_Length)) == 0) &&
            (CfaValidateIfIndex
             ((UINT4) pDataSourceOid->pu4_OidList[grlInterfaceOid.u4_Length]) ==
             CFA_SUCCESS))
        {
            RMON_DBG (CRITICAL_TRC | MGMT_TRC, "VALID Interface Index\n");
            return TRUE;
        }
        else
        {
            RMON_DBG (CRITICAL_TRC | MGMT_TRC, "INVALID Interface Index\n");
            return FALSE;
        }
    }
    else if (u4OIDType == VLAN_INDEX_TYPE)
    {
        if ((pDataSourceOid->u4_Length == (grlVlanOid.u4_Length + 1)) &&
            (MEMCMP (pDataSourceOid->pu4_OidList, grlVlanOid.pu4_OidList,
                     (sizeof (UINT4) * grlVlanOid.u4_Length)) == 0) &&
            (L2IwfIsVlanActive
             ((tVlanId) pDataSourceOid->pu4_OidList[grlVlanOid.u4_Length]) !=
             OSIX_FALSE))
        {
            RMON_DBG (CRITICAL_TRC | MGMT_TRC, "VALID Vlan Index\n");
            return TRUE;
        }
        else
        {
            RMON_DBG (CRITICAL_TRC | MGMT_TRC, "INVALID Vlan Index\n");
            return FALSE;
        }
    }
    return FALSE;
}

/******************************************************************************
*      Function Name        : RmonCheckDataSourceOIDIsVlanOrInterface         *
*                                                                             *
*      Role of the function : This function check whether the given OID is    *
*                             Interface OID or VLAN OID and returns           *
*                             VLAN_INDEX_TYPE or PORT_INDEX_TYPE              *
*      Formal Parameters    : pDataSourceOid, pu4OIDType                      *
*      Global Variables     : None                                            *
*      Side Effects         : None                                            *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : RMON_SUCCESS/RMON_FAILURE                       *
******************************************************************************/

INT4
RmonCheckDataSourceOIDIsVlanOrInterface (tSNMP_OID_TYPE * pDataSourceOid,
                                         UINT4 *pu4OIDType)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : RmonCheckDataSourceOIDIsVlanOrInterface\n");

    *pu4OIDType = 0;

    if (MEMCMP (pDataSourceOid->pu4_OidList, grlVlanOid.pu4_OidList,
                (grlVlanOid.u4_Length * sizeof (UINT4))) == 0)
    {
        *pu4OIDType = VLAN_INDEX_TYPE;
        return RMON_SUCCESS;
    }
    else if (MEMCMP (pDataSourceOid->pu4_OidList, grlInterfaceOid.pu4_OidList,
                     (grlInterfaceOid.u4_Length * sizeof (UINT4))) == 0)
    {
        *pu4OIDType = PORT_INDEX_TYPE;
        return RMON_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving Function : RmonCheckDataSourceOIDIsVlanOrInterface\n");
    return RMON_FAILURE;
}

/****************************************************************************
*    Function Name        : RmonGetMibVariableValue                         *
*                                                                           *
*    Role of the function : RmonGetMibVariableValue function gets           * 
*                           the value of a MIB variable given its OID.      * 
*    Formal Parameters    : Variable                                        *
*    Global Variables     : None                                            *
*    Side Effects         : None                                            *
*    Exception Handling   : None                                            *
*    Use of Recursion     : None                                            *
*    Return Value         : The value of the variable                       *
****************************************************************************/
INT4
RmonGetMibVariableValue (tSNMP_OID_TYPE * pObjName, UINT4 *pu4GetValue)
{
#ifdef SNMP_2_WANTED
    UINT4               u4Error = 0;
    tSnmpIndex         *pCurIndex = RmonIndexPool1;
    tSNMP_MULTI_DATA_TYPE MultiData;

    /* Checks if the incoming object id is valid */
    if ((pObjName == NULL) || (pObjName->pu4_OidList == NULL))
    {
        return RMON_INVALID_OID;
    }

    /*Allocate memory for the MultiData variables */
    MultiData.pOctetStrValue = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (RMON_MAX_OCTET_STRING_LEN);

    if (MultiData.pOctetStrValue == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " RMON Memory Allocation Failure - Multi Data\n");
        return RMON_FAILURE;
    }

    MultiData.pOidValue = (tSNMP_OID_TYPE *)
        rmon_alloc_oid (RMON_MAX_OID_LENGTH);

    if (MultiData.pOidValue == NULL)
    {
        free_octetstring (MultiData.pOctetStrValue);
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " RMON Memory Allocation Failure - Multi Data\n");
        return RMON_FAILURE;
    }
    /* Unlock here so that , this snmputil fn, can access (rmon) mib object
     * with Lock
     */
    RMON_UNLOCK ();
    if (SNMPGet (*pObjName, &MultiData, &u4Error, pCurIndex,
                 SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
    {
        RMON_LOCK ();
        /* Set the output parameter value from the multi-data type */
        switch (MultiData.i2_DataType)
        {
            case SNMP_DATA_TYPE_INTEGER32:
            case SNMP_DATA_TYPE_TIME_TICKS:
                *pu4GetValue = (UINT1) MultiData.i4_SLongValue;
                break;
            case SNMP_DATA_TYPE_UNSIGNED32:
            case SNMP_DATA_TYPE_COUNTER32:
                *pu4GetValue = MultiData.u4_ULongValue;
                break;
            default:
                free_octetstring (MultiData.pOctetStrValue);
                rmon_free_oid (MultiData.pOidValue);
                return RMON_INVALID_OID;
        }
    }
    else
    {
#ifdef SNMP_3_WANTED
        SnmpApiDecrementCounter (u4Error);
#endif
        RMON_LOCK ();
        free_octetstring (MultiData.pOctetStrValue);
        rmon_free_oid (MultiData.pOidValue);
        return RMON_INVALID_OID;
    }
    free_octetstring (MultiData.pOctetStrValue);
    rmon_free_oid (MultiData.pOidValue);
#else
    UNUSED_PARAM (pObjName);
    UNUSED_PARAM (pu4GetValue);
#endif /*SNMP_2_WANTED */
    return RMON_SUCCESS;
}

/******************************************************************************
*      Function Name        : RmonEtherStatTimerExpiry                        *
*                                                                             *
*      Role of the function : RmonEtherStatTimerExpiry will set the entry in  *
*                             gaEtherStatsTable to RMON_INVALID               * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaEtherStatsTable                               *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonEtherStatTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : RmonEtherStatTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;
    if (nmhSetEtherStatsStatus (i4Index, status) == SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry Status set to RMON_INVALID\n");
    }
    else
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry is NOT made RMON_INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving Function : RmonEtherStatTimerExpiry \n");
}

/******************************************************************************
*      Function Name        : RmonHistCtrlTimerExpiry                         *
*                                                                             *
*      Role of the function : RmonHistCtrlTimerExpiry will set the entry in   *
*                             HistoryControlTable to RMON_INVALID             * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaHistoryControlTable                           *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonHistCtrlTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonHistCtrlTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;
    if (nmhSetHistoryControlStatus (i4Index, status) == SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC, "Entry Status set to INVALID\n");
    }
    else
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC, "Entry is NOT made INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving Function : RmonHistCtrlTimerExpiry \n");
}

/******************************************************************************
*      Function Name        : RmonAlrmTimerExpiry                             *
*                                                                             *
*      Role of the function : RmonAlrmTimerExpiry will set the entry in       *
*                             gaAlarmTable to RMON_INVALID                    * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaAlarmTable                                    *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonAlrmTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonAlrmTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;
    if (nmhSetAlarmStatus (i4Index, status) == SNMP_SUCCESS)
    {
        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry Status set to RMON_INVALID\n");
    }
    else
    {
        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry is NOT made RMON_INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving Function : RmonAlrmTimerExpiry \n");
}

/******************************************************************************
*      Function Name        : RmonEvntTimerExpiry                             *
*                                                                             *
*      Role of the function : RmonEvntTimerExpiry will set the entry in       *
*                             gaEventTable to RMON_INVALID                    * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaEventTable                                    *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonEvntTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonEvntTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;
    if (nmhSetEventStatus (i4Index, status) == SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry Status set to RMON_INVALID\n");

    }
    else
    {
        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry is NOT made RMON_INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving Function : RmonEvntTimerExpiry \n");
}

/******************************************************************************
*      Function Name        : RmonMtrxCtrlTimerExpiry                         *
*                                                                             *
*      Role of the function : RmonMtrxCtrlTimerExpiry will set the entry in   *
*                             gaMatrixControlTable to RMON_INVALID            * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaMatrixControlTable                            *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonMtrxCtrlTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonMtrxCtrlTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;
    if (nmhSetMatrixControlStatus (i4Index, status) == SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry Status set to RMON_INVALID \n");
    }
    else
    {
        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry is NOT made RMON_INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving Function : RmonMtrxCtrlTimerExpiry ");
}

/******************************************************************************
*      Function Name        : RmonHostCtrlTimerExpiry                         *
*                                                                             *
*      Role of the function : RmonHostCtrlTimerExpiry will set the entry in   *
*                               HostControlTable to RMON_INVALID              * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaHostControlTable                              *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonHostCtrlTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonHostCtrlTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;

    if (nmhSetHostControlStatus (i4Index, status) == SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry Status set to RMON_INVALID \n");
    }
    else
    {
        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry is NOT made RMON_INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving Function : RmonHostCtrlTimerExpiry \n");

}

/******************************************************************************
*      Function Name        : RmonTopNCtrlTimerExpiry                         *
*                                                                             *
*      Role of the function : RmonTopNCtrlTimerExpiry will set the entry in   *
*                               TopNControlTable to RMON_INVALID              * 
*      Formal Parameters    : u4TimerRef                                      *
*      Global Variables     : gaHostTopNControlTable                          *
*      Side Effects         : The Status field is made RMON_INVALID           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonTopNCtrlTimerExpiry (UINT4 u4TimerRef)
{
    INT4                i4Index;
    tRmonStatus         status;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function :  RmonTopNCtrlTimerExpiry \n");

    i4Index = RMON_GET_TIMER_REFERENCE_INDEX (u4TimerRef);

    status = RMON_INVALID;

    if (nmhSetHostTopNStatus (i4Index, status) == SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry Status set to RMON_INVALID \n");
    }
    else
    {
        RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                  "Entry is NOT made RMON_INVALID \n");
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving Function : RmonTopNCtrlTimerExpiry \n");

}

/****************************************************************************
 * Function Name   :  RmonUpdateFrameParticulars                            *
 * Description     :  Called from Aperiodic main task whenever a frame      *
 *                    arrives. This functions Updates the statistics table  * 
 *                    according to the arrived frames characteristics.      *
 * Input       :  pEthFrame         - Pointer to the Frame                  *
 *             :  pFrameParticulars - Frame Particulars ((Index,Length)     *
 *                                                                          *
 * Output      :  pFrameParticulars - Frame Particulars                     *
 * Returns     :  NONE                                                      *
****************************************************************************/

INT1
RmonUpdateFrameParticulars (UINT1 *pEthFrame,
                            tFrameParticulars * pFrameParticulars)
{
    UINT2               u2Len;
    INT2                i2Count;
    BOOLEAN             btest = TRUE;

    BOOLEAN             b1IsGoodPacket;
    BOOLEAN             b1ErrPacket;
    BOOLEAN             b1IsErrorInFCS;
    BOOLEAN             b1IsBroadCast;
    BOOLEAN             b1IsMultiCast;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function:  RmonUpdateFrameParticulars \n");

    u2Len = pFrameParticulars->u2Length;

    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | FUNC_ENTRY, "RMON is shutdown \n");
        return RMON_FAILURE;
    }
    if (!(RMON_IS_ENABLED ()))
    {
        RMON_DBG (MINOR_TRC | FUNC_ENTRY, "RMON is disabled \n");
        return RMON_FAILURE;
    }

    for (i2Count = RMON_ZERO; i2Count < (ADDRESS_LENGTH * 2); i2Count++)
    {
        if (u2Len <= (UINT2) i2Count)
        {
            break;
        }
        if (pEthFrame[i2Count] != 0)
        {
            btest = FALSE;
        }
    }
    if (btest == TRUE)
    {
        RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                  "Zero Address Frame is received \n");
        return RMON_FAILURE;
    }

    b1IsGoodPacket = TRUE;
    b1ErrPacket = FALSE;
    b1IsErrorInFCS = FALSE;
    b1IsBroadCast = FALSE;
    b1IsMultiCast = FALSE;

    b1IsErrorInFCS = (BOOLEAN) RmonisErrorInFCS (pEthFrame, u2Len);
    if (u2Len < 64)
    {
        b1ErrPacket = TRUE;
    }
    else if (u2Len > 1518)
    {
        b1ErrPacket = TRUE;
    }
    if ((b1ErrPacket || b1IsErrorInFCS))
    {
        b1IsGoodPacket = FALSE;
    }
    else
    {
        b1IsGoodPacket = TRUE;
    }

    if (b1IsGoodPacket == TRUE)
    {
        b1IsBroadCast = (BOOLEAN) RmonisBroadCast (pEthFrame);
        if (b1IsBroadCast == FALSE)
        {
            b1IsMultiCast = (BOOLEAN) RmonisMultiCast (pEthFrame);
        }
    }
    MEMCPY (&pFrameParticulars->DestAddress, pEthFrame, ADDRESS_LENGTH);
    MEMCPY (&pFrameParticulars->SrcAddress, (pEthFrame + ADDRESS_LENGTH),
            ADDRESS_LENGTH);
    if (b1IsGoodPacket == TRUE)
    {
        pFrameParticulars->b1WhetherError = FALSE;
    }
    else
    {
        pFrameParticulars->b1WhetherError = TRUE;
    }
    pFrameParticulars->b1WhetherBroadcast = b1IsBroadCast;
    pFrameParticulars->b1WhetherMulticast = b1IsMultiCast;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving  function : RmonUpdateFrameParticulars \n");
    return RMON_SUCCESS;

}

/****************************************************************************
 * Function Name   :  rmon_alloc_oid                                        *
 * Description :  This Function Allocates the Memory for an Object of OID   *
 *                Type.                                                     *
 * Input       :  i4_size: Size of Oid.                                     *
 *                                                                          *
 * Output      :  temp   : Allocation of Memory for the Oid for Given Size. *
 * Returns     :  Pointer of tSNMP_OID_TYPE.                                *
 ****************************************************************************/
tSNMP_OID_TYPE     *
rmon_alloc_oid (INT4 i4_size)
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *temp = NULL;

    temp = alloc_oid (i4_size);
    return temp;
#else
    UNUSED_PARAM (i4_size);
    return NULL;
#endif

}

/****************************************************************************
 * Function    :  rmon_free_oid                                             *
 * Description :  This Function Frees the Memory for an Object of OID Type. *
 * Input       :  temp : The Ptr which is to be freed.                      *
 * Output      :  Frees the Given Oid Pointer.                              *
 * Returns     :  None.                                                     *
 ***************************************************************************/

void
rmon_free_oid (tSNMP_OID_TYPE * temp)
{
#ifdef SNMP_2_WANTED
    if (temp == NULL)
    {
        return;
    }
    free_oid (temp);
#else
    UNUSED_PARAM (temp);
#endif
    return;
}

/****************************************************************************
 * Function    :  rmon_allocmem_octetstring                                 *
 * Description :  This Function Allocates Memory for Octet String Type.     *
 * Input       :  i4_size : The Size of Octet String.                       *
 * Output      :  Allocated Octet String Pointer.                           *
 * Returns     :  Pointer to an Octet String Variable.                      *
 ***************************************************************************/

tSNMP_OCTET_STRING_TYPE *
rmon_allocmem_octetstring (INT4 i4_size)
{
    tSNMP_OCTET_STRING_TYPE *temp = NULL;
#ifdef SNMP_2_WANTED
    temp = allocmem_octetstring (i4_size);
#else
    UNUSED_PARAM (i4_size);
#endif
    return temp;
}

/****************************************************************************
 * Function    :  rmon_free_octetstring                                     *   
 * Description :  This Function Frees Memory for Octet String Type Var.     *
 * Input       :  temp : The Pointer to the Octet String.                   *
 * Output      :  Frees the Octet String Pointer.                           *
 * Returns     :  None.                                                     *
 ***************************************************************************/
void
rmon_free_octetstring (tSNMP_OCTET_STRING_TYPE * temp)
{
#ifdef SNMP_2_WANTED
    if (temp == NULL)
    {
        return;
    }
    free_octetstring (temp);
#else
    UNUSED_PARAM (temp);
#endif
    return;
}

/************************************************************************
* Function Name : RMON_AGT_ConvSubOIDToLong                             *
* Description   : To convert sub oid into long int                      *
* Input(s)      : Double pointer to the string                          *
* Output(s)     : Long integer value                                    *
* Returns       : u4_value, in case of success                          *
*                 -1 , in  failure cases                                *
************************************************************************/
INT4
RMON_AGT_ConvSubOIDToLong (UINT1 **ppu1_temp_ptr)
{
    INT2                i2_i;
    UINT4               u4_value;
    UINT1               u1_temp_value;

    u4_value = 0;
    for (i2_i = 0; ((i2_i < 11) && (**ppu1_temp_ptr != '.') &&
                    (**ppu1_temp_ptr != '\0')); i2_i++)
    {
        if (!isdigit (**ppu1_temp_ptr))
        {
            return ((INT4) (-1));
        }
        if (STRNCPY (&u1_temp_value, *ppu1_temp_ptr, 1) == NULL)
        {
            return ((INT4) (-1));
        }
        u4_value = ((u4_value * (UINT4) 10) + (0x0f & (UINT4) u1_temp_value));
        (*ppu1_temp_ptr)++;
    }
    return ((INT4) u4_value);
}

/************************************************************************
* Function Name : RMON_AGT_GetOidFromString                             *
* Description   : To get Oid from Octet string form                     *
* Input(s)      : Oid in Octet string form                              *
* Output(s)     : Pointer to Oid                                        *
* Returns       : p_oid, in case of success                             *
*                 NULL, in  failure cases                               *
************************************************************************/
tSNMP_OID_TYPE     *
RMON_AGT_GetOidFromString (INT1 *pi1_str)
{
    tSNMP_OID_TYPE     *p_oid = NULL;
#ifdef SNMP_2_WANTED
    p_oid = SNMP_AGT_GetOidFromString (pi1_str);
#else
    UNUSED_PARAM (pi1_str);
#endif
    return (p_oid);
}

/****************************************************************************
* Function Name : RMON_AGT_FormOid                                          *
* Description   : Creates the internal datastructure of type tSNMP_OID_TYPE *
*                 and puts the values in to it.                             *
* Input(s)      : Oid in UINT4 array form, i2_length                        *
* Output(s)     : Pointer to Oid                                            *
* Returns       : Pointer to tSNMP_OID_TYPE, in case of success             *
*                 NULL, in  failure cases                                   *
****************************************************************************/
tSNMP_OID_TYPE     *
RMON_AGT_FormOid (UINT4 u4_s_oid[], INT2 i2_length)
{
    tSNMP_OID_TYPE     *obj_ptr = NULL;
#ifdef SNMP_2_WANTED
    INT4                i;

    if (i2_length > RMON_MAX_OID_LENGTH)
    {
        return (NULL);
    }
    /* rml : port change FSAP-2 */
    if ((obj_ptr = alloc_oid (i2_length)) == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " RMON Memory Allocation Failure - FormOid\n");
        return (NULL);
    }

    for (i = 0; i < i2_length; i++)
    {
        obj_ptr->pu4_OidList[i] = (UINT4) u4_s_oid[i];
    }
    obj_ptr->u4_Length = (UINT4) i2_length;
#else
    UNUSED_PARAM (u4_s_oid);
    UNUSED_PARAM (i2_length);
#endif
    return (obj_ptr);
}

/****************************************************************************
* Function Name : RMON_FreeVarbindList                                      *
* Description   : Frees the whole tSNMP_VAR_BIND List allocated             *
* Input(s)      : Oid in UINT4 array form, i2_length                        * 
* Output(s)     : NONE                                                      *
* Returns       : NONE                                                      *
*****************************************************************************/
void
RMON_FreeVarbindList (tSNMP_VAR_BIND * vb_ptr)
{
#ifdef SNMP_2_WANTED
    SNMP_AGT_FreeVarBindList (vb_ptr);
#else
    UNUSED_PARAM (vb_ptr);
#endif
}

/****************************************************************************
* Function Name : RmonHashFn                                                *
* Description   : This function calculate the hash value and returns value  *
* Input(s)      : u4DataSource                                              *
* Output(s)     :                                                           *
* Returns       : Hash Value - INT4                                         *
****************************************************************************/
INT4
RmonHashFn (UINT4 u4DataSource)
{
    return ((INT4) (u4DataSource % RMON_ETHERSTATS_HASH_SIZE));
}

/****************************************************************************
* Function Name : HashSearchEtherStatsNode                                  *
* Description   : This function searches the hash table for the node        *
                  with ether stats index value equal to the value passed    *
* Input(s)      : pHashTab                                                  * 
                  i4EtherStatsTindex                                         *
* Output(s)     :                                                           *
* Returns       : tRmonEtherStatsNode*                                      *
****************************************************************************/
tRmonEtherStatsTimerNode *
HashSearchEtherStatsNode (tRmonHashTable * pHashTab, INT4 i4etherStatsTindex)
{
    UINT4               u4HashIndex;
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex,
                              pEtherStatsNode, tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode != NULL)
            {
                if (pEtherStatsNode->i4EtherStatsIndex == i4etherStatsTindex)
                    return (pEtherStatsNode);
            }
        }
    }
    return NULL;
}

/****************************************************************************
* Function Name : HashSearchEtherStatsNode_DSource                          *
* Description   : This function searches the hash table for the node        *
                  with data source value equal to the value passed          *
* Input(s)      : pHashTab                                                  * 
                  u4DataSource                                              *
* Output(s)     :                                                           *
* Returns       : tRmonEtherStatsNode*                                      *
****************************************************************************/
tRmonEtherStatsTimerNode *
HashSearchEtherStatsNode_DSource (tRmonHashTable * pHashTab, UINT4 u4DataSource,
                                  UINT4 u4Type)
{

    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               u4Hashindex;

    u4Hashindex = (UINT4) RmonHashFn (u4DataSource);

    TMO_HASH_Scan_Bucket (pHashTab, u4Hashindex,
                          pEtherStatsNode, tRmonEtherStatsTimerNode *)
    {
        if (pEtherStatsNode != NULL)
        {
            if ((pEtherStatsNode->u4EtherStatsDataSource == u4DataSource) &&
                (pEtherStatsNode->u4EtherStatsOIDType == u4Type))
            {
                return pEtherStatsNode;
            }
        }
    }
    return NULL;
}

/****************************************************************************
* Function Name : HashSearchEtherStatsNode_NextValidTindex                  *
* Description   : This function searches the hash table for the next valid  *
                  index and returns the value                               *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : INT4                                                      *
****************************************************************************/
INT4
HashSearchEtherStatsNode_NextValidTindex (tRmonHashTable * pHashTab,
                                          INT4 i4Tableindex)
{
    UINT4               u4HashIndex;
    INT4                i4NextIndex = RMON_MAX_ETHERSTATS_INDEX + 1;
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pEtherStatsNode,
                              tRmonEtherStatsTimerNode *)
        {
            if ((pEtherStatsNode->i4EtherStatsIndex > i4Tableindex)
                && (pEtherStatsNode->i4EtherStatsIndex < i4NextIndex))
                i4NextIndex = pEtherStatsNode->i4EtherStatsIndex;
        }
    }
    /* Check in the SLL whether any under creation entries exists */
    pEtherStatsNode = NULL;
    TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode, tRmonEtherStatsTimerNode *)
    {
        if ((pEtherStatsNode->i4EtherStatsIndex > i4Tableindex)
            && (pEtherStatsNode->i4EtherStatsIndex < i4NextIndex))
            i4NextIndex = pEtherStatsNode->i4EtherStatsIndex;
    }
    return i4NextIndex;
}

/****************************************************************************
* Function Name : HashSearchHistoryControlNode                              *
* Description   : This function searches the hash table for the node        *
                  with history control index value equal to the value passed*
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : tRmonHistoryControlNode*                                  *
****************************************************************************/
tRmonHistoryControlNode *
HashSearchHistoryControlNode (tRmonHashTable * pHashTab, INT4 i4Tableindex)
{
    tRmonHistoryControlNode *pHistCtrlNode = NULL;
    INT4                i4Hashindex;

    i4Hashindex = RmonHashFn ((UINT4) i4Tableindex);

    TMO_HASH_Scan_Bucket (pHashTab, i4Hashindex, pHistCtrlNode,
                          tRmonHistoryControlNode *)
    {
        if (pHistCtrlNode->i4HistoryControlIndex == i4Tableindex)
        {
            return pHistCtrlNode;
        }
    }
    return NULL;
}

/****************************************************************************
* Function Name : HashSearchHistoryControlNode_NextValidTindex              *
* Description   : This function searches the hash table for the next valid  *
                  index and returns the value                               *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : INT4                                                      *
****************************************************************************/
INT4
HashSearchHistoryControlNode_NextValidTindex (tRmonHashTable * pHashTab,
                                              INT4 i4Tableindex)
{
    UINT4               u4HashIndex;
    INT4                i4NextIndex = RMON_MAX_HISTORY_CONTROL_INDEX + 1;
    tRmonHistoryControlNode *pHistCtrlNode = NULL;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pHistCtrlNode,
                              tRmonHistoryControlNode *)
        {
            if (pHistCtrlNode != NULL)
            {
                if ((pHistCtrlNode->i4HistoryControlIndex > i4Tableindex)
                    && (pHistCtrlNode->i4HistoryControlIndex < i4NextIndex))
                    i4NextIndex = pHistCtrlNode->i4HistoryControlIndex;
            }
        }

    }
    return i4NextIndex;
}

/****************************************************************************
* Function Name : HashSearchAlarmNode                                       *
* Description   : This function searches the hash table for the node        *
                  with alarm index value equal to the value passed          *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : tRmonAlarmNode*                                           *
****************************************************************************/
tRmonAlarmNode     *
HashSearchAlarmNode (tRmonHashTable * pHashTab, INT4 i4Tableindex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;
    INT4                i4Hashindex;

    i4Hashindex = RmonHashFn ((UINT4) i4Tableindex);

    TMO_HASH_Scan_Bucket (pHashTab, i4Hashindex, pAlarmNode, tRmonAlarmNode *)
    {
        if (pAlarmNode != NULL)
        {
            if (pAlarmNode->i4AlarmIndex == i4Tableindex)
            {
                return pAlarmNode;
            }
        }
    }
    return NULL;
}

/****************************************************************************
* Function Name : HashSearchAlarmNode_NextValidTindex                       *
* Description   : This function searches the hash table for the next valid  *
                  index and returns the value                               *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : INT4                                                      *
****************************************************************************/
INT4
HashSearchAlarmNode_NextValidTindex (tRmonHashTable * pHashTab,
                                     INT4 i4Tableindex)
{
    UINT4               u4HashIndex;
    INT4                i4NextIndex = RMON_MAX_ALARM_INDEX + 1;
    tRmonAlarmNode     *pAlarmNode = NULL;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pAlarmNode,
                              tRmonAlarmNode *)
        {
            if (pAlarmNode != NULL)
            {
                if ((pAlarmNode->i4AlarmIndex > i4Tableindex)
                    && (pAlarmNode->i4AlarmIndex < i4NextIndex))
                    i4NextIndex = pAlarmNode->i4AlarmIndex;
            }
        }

    }
    return i4NextIndex;
}

/****************************************************************************
* Function Name : HashSearchEventNode                                       *
* Description   : This function searches the hash table for the node        *
                  with event index value equal to the value passed          *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : tRmonEventNode*                                           *
****************************************************************************/
tRmonEventNode     *
HashSearchEventNode (tRmonHashTable * pHashTab, INT4 i4Tableindex)
{
    tRmonEventNode     *pEventNode = NULL;
    UINT4               i4Hashindex;

    i4Hashindex = (UINT4) RmonHashFn ((UINT4) i4Tableindex);

    TMO_HASH_Scan_Bucket (pHashTab, i4Hashindex, pEventNode, tRmonEventNode *)
    {
        if (pEventNode->i4EventIndex == i4Tableindex)
        {
            return pEventNode;
        }
    }
    return NULL;
}

/****************************************************************************
* Function Name : HashSearchEventNode_NextValidTindex                       *
* Description   : This function searches the hash table for the next valid  *
                  index and returns the value                               *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : INT4                                                      *
****************************************************************************/
INT4
HashSearchEventNode_NextValidTindex (tRmonHashTable * pHashTab,
                                     INT4 i4Tableindex)
{
    UINT4               u4HashIndex;
    INT4                i4NextIndex = RMON_MAX_EVENT_INDEX + 1;
    tRmonEventNode     *pEventNode = NULL;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pEventNode,
                              tRmonEventNode *)
        {
            if (pEventNode != NULL)
            {
                if ((pEventNode->i4EventIndex > i4Tableindex)
                    && (pEventNode->i4EventIndex < i4NextIndex))
                    i4NextIndex = pEventNode->i4EventIndex;
            }
        }

    }
    return i4NextIndex;
}

/****************************************************************************
* Function Name : HashSearchHistoryControlNode_DSource                      *
* Description   : This function searches the hash table for the node        *
                  with data source value equal to the value passed          *
* Input(s)      : pHashTab                                                  * 
                  u4DataSource                                              *
* Output(s)     :                                                           *
* Returns       : TRUE/FALSE                                                *
****************************************************************************/
UINT1
HashSearchHistoryControlNode_DSource (tRmonHashTable * pHashTab,
                                      UINT4 u4DataSource, UINT4 u4Type)
{
    tRmonHistoryControlNode *pHistCtrlNode = NULL;
    UINT4               u4HashIndex;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pHistCtrlNode,
                              tRmonHistoryControlNode *)
        {
            if (pHistCtrlNode != NULL)
            {
                if ((pHistCtrlNode->u4HistoryControlDataSource == u4DataSource)
                    && (pHistCtrlNode->u4HisCntrlOIDType == u4Type))
                {
                    return RMON_TRUE;
                }
            }
        }
    }
    return RMON_FALSE;
}

/****************************************************************************
* Function Name : HashSearchAlarmNode_ForStatsIndex                         *
* Description   : This function searches the hash table for the node        *
                  with alarm index value equal to the value passed          *
* Input(s)      : pHashTab                                                  * 
                  i4Tableindex                                              *
* Output(s)     :                                                           *
* Returns       : TRUE/FALSE                                                *
****************************************************************************/
UINT1
HashSearchAlarmNode_ForStatsIndex (tRmonHashTable * pHashTab, INT4 i4TableIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;
    UINT4               u4HashIndex;
    tSNMP_OID_TYPE      ethStatsOid;
    UINT4               au4ethStatsOid[10];

    ethStatsOid.u4_Length = 10;
    ethStatsOid.pu4_OidList = &au4ethStatsOid[0];

    ethStatsOid.pu4_OidList[0] = 1;
    ethStatsOid.pu4_OidList[1] = 3;
    ethStatsOid.pu4_OidList[2] = 6;
    ethStatsOid.pu4_OidList[3] = 1;
    ethStatsOid.pu4_OidList[4] = 2;
    ethStatsOid.pu4_OidList[5] = 1;
    ethStatsOid.pu4_OidList[6] = 16;
    ethStatsOid.pu4_OidList[7] = 1;
    ethStatsOid.pu4_OidList[8] = 1;
    ethStatsOid.pu4_OidList[9] = 1;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pAlarmNode,
                              tRmonAlarmNode *)
        {
            if (pAlarmNode != NULL)
            {
                if (pAlarmNode->alarmVariable.
                    pu4_OidList[pAlarmNode->alarmVariable.u4_Length - 1] ==
                    (UINT4) i4TableIndex)
                {
                    if (rmon_n_compare_oids (&ethStatsOid,
                                             &(pAlarmNode->alarmVariable),
                                             10) == RMON_TRUE)
                    {
                        return RMON_TRUE;
                    }
                }
            }
        }
    }
    return RMON_FALSE;
}

UINT1
rmon_n_compare_oids (tSNMP_OID_TYPE * oid1, tSNMP_OID_TYPE * oid2, UINT2 u2Len)
{
    UINT4               i;

    if (!oid1 && !oid2)
    {
        return RMON_TRUE;
    }

    if (!oid1 || !oid2)
    {
        return RMON_FALSE;
    }
    if (oid2->u4_Length == 0 || oid1->u4_Length == 0)
    {
        return RMON_FALSE;
    }

    for (i = 0; i < u2Len; i++)
    {
        if (oid1->pu4_OidList[i] != oid2->pu4_OidList[i])
            return (RMON_FALSE);
    }
    return (RMON_TRUE);
}

/****************************************************************************
* Function Name : HashSearchAlarmNode_ForEvent                              *
* Description   : This function searches the hash table for the node        *
                  with alarm index value equal to the value passed          *
* Input(s)      : pHashTab                                                  * 
                  i4EventIndex                                              *
* Output(s)     :                                                           *
* Returns       : TRUE/FALSE                                                *
****************************************************************************/
UINT1
HashSearchAlarmNode_ForEvent (tRmonHashTable * pHashTab, INT4 i4EventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;
    UINT4               u4HashIndex;

    TMO_HASH_Scan_Table (pHashTab, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pHashTab, u4HashIndex, pAlarmNode,
                              tRmonAlarmNode *)
        {
            if (pAlarmNode != NULL)
            {
                if ((pAlarmNode->i4AlarmRisingEventIndex == i4EventIndex) ||
                    (pAlarmNode->i4AlarmFallingEventIndex == i4EventIndex))
                {
                    return RMON_TRUE;
                }
            }
        }
    }
    return RMON_FALSE;
}

/*****************************************************************************/
/* Function Name      : RmonLock                                             */
/*                                                                           */
/* Description        : This function is used to take the PNAC mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*****************************************************************************/
INT4
RmonLock (VOID)
{
    if (RMON_TAKE_SEM (RMON_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmonUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the RMON mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*****************************************************************************/
INT4
RmonUnLock (VOID)
{
    if (RMON_GIVE_SEM (RMON_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmonDeleteHistoryControlNode                         */
/*                                                                           */
/* Description        : This function is used to delete the particular       */
/*                      HistoryControlNode from the HistoryControlTable      */
/* Input(s)           : pHistoryControlNode                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : RmonDeletePort                                       */
/*****************************************************************************/
VOID
RmonDeleteHistoryControlNode (tRmonHistoryControlNode * pHistoryControlNode)
{
    UINT4               u4Hindex;
    if (pHistoryControlNode != NULL)
    {

        /* Need to free the memory for the data table - History Table */

        if (pHistoryControlNode->pHistoryEntry != NULL)
        {
            if (pHistoryControlNode->pHistoryEntry->pHistoryBucket != NULL)

            {
                MemReleaseMemBlock (gRmonHisBucketsPoolId, (UINT1 *)
                                    (pHistoryControlNode->pHistoryEntry->
                                     pHistoryBucket));
                pHistoryControlNode->pHistoryEntry->pHistoryBucket = NULL;
            }
            MemReleaseMemBlock (gRmonEtherHisPoolId, (UINT1 *)
                                (pHistoryControlNode->pHistoryEntry));
            pHistoryControlNode->pHistoryEntry = NULL;
        }

        /* Free the memory for History Control Entry */
        u4Hindex =
            (UINT4) RmonHashFn ((UINT4) pHistoryControlNode->
                                i4HistoryControlIndex);

        /* Stop the Timer First */
        RmonStopTimer (HISTORY_CONTROL_TABLE,
                       pHistoryControlNode->i4HistoryControlIndex);

        RMON_HASH_DEL_NODE (gpRmonHistoryControlHashTable,
                            &pHistoryControlNode->nextHistoryControlEntryNode,
                            u4Hindex);

        MemReleaseMemBlock (gRmonHistoryCtrlPoolId,
                            (UINT1 *) pHistoryControlNode);
        pHistoryControlNode = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : RmonDeleteEtherStatsNode                             */
/*                                                                           */
/* Description        : This function is used to delete the particular       */
/*                      EtherStatsNode from the etherStatsTable              */
/* Input(s)           : pEtherStatsNode                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RMON_SUCCESS or RMON_FAILURE                        */
/*                                                                           */
/* Called By          : RmonDeletePort                                       */
/*****************************************************************************/
INT1
RmonDeleteEtherStatsNode (tRmonEtherStatsTimerNode * pEtherStatsNode)
{
    UINT4               u4Hindex;
#if defined(NPAPI_WANTED)
    UINT4               u4Port = 0;
#endif
    if (pEtherStatsNode != NULL)
    {
#if defined(NPAPI_WANTED)
        /* Make the entry INVALID in HW if NPAPI_WANTED is supported */
        if (pEtherStatsNode->u4EtherStatsOIDType == PORT_INDEX_TYPE)
        {
            u4Port = pEtherStatsNode->u4EtherStatsDataSource;
            if (RmonFsRmonHwSetEtherStatsTable (u4Port, FNP_FALSE) !=
                FNP_SUCCESS)
            {
                RMON_DBG (MAJOR_TRC | MEM_TRC,
                          "Failed in configuring the HW etherStats table \n");

                return RMON_FAILURE;
            }
        }
#endif
        u4Hindex = (UINT4) RmonHashFn (pEtherStatsNode->u4EtherStatsDataSource);
        RmonStopTimer (ETHER_STATS_TABLE, pEtherStatsNode->i4EtherStatsIndex);
        RMON_HASH_DEL_NODE (gpRmonEtherStatsHashTable,
                            &pEtherStatsNode->nextEtherStatsNode, u4Hindex);
        MemReleaseMemBlock (gRmonEtherStatsPoolId, (UINT1 *) pEtherStatsNode);
        pEtherStatsNode = NULL;

    }
    return RMON_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmonProcessDeletePort                                       */
/*                                                                           */
/* Description        : This function is used to delete the particular       */
/*                      EtherStatsNode from the etherStatsTable              */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RMON_SUCCESS or RMON_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
RmonProcessDeletePort (UINT4 u4IfIndex)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tRmonHistoryControlNode *pTmpHistoryControlNode = NULL;
    UINT4               u4HashIndex;

    TMO_HASH_Scan_Table (gpRmonHistoryControlHashTable, u4HashIndex)
    {
        TMO_HASH_DYN_Scan_Bucket (gpRmonHistoryControlHashTable, u4HashIndex,
                pHistoryControlNode, pTmpHistoryControlNode, tRmonHistoryControlNode *)
        {
            if (pHistoryControlNode != NULL)
            {
                if ((pHistoryControlNode->u4HistoryControlDataSource ==
                            u4IfIndex) && (pHistoryControlNode->u4HisCntrlOIDType ==
                                (UINT4) PORT_INDEX_TYPE))
                {
                    RMON_DBG2 (CRITICAL_TRC, "RmonProcessDeletePort :"
                            "Deleting History node %d ifindex: %d \n",
                            pHistoryControlNode->i4HistoryControlIndex, u4IfIndex);
                    RmonDeleteHistoryControlNode (pHistoryControlNode);

                }
            }
        }
    }

    pEtherStatsNode =
        HashSearchEtherStatsNode_DSource (gpRmonEtherStatsHashTable, u4IfIndex,
                                          (UINT4) PORT_INDEX_TYPE);
    if (pEtherStatsNode != NULL)
    {
        if ((RmonDeleteEtherStatsNode (pEtherStatsNode)) != RMON_SUCCESS)
        {
            return RMON_FAILURE;
        }

    }
    return RMON_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : RmonDeletePort                                       */
/*                                                                           */
/* Description        : This function is used to indicate RMON about         */
/*                      Port deletion                                        */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RMON_SUCCESS or RMON_FAILURE                         */
/*                                                                           */
/* Called By          : CfaIfmDeleteEnetInterface                            */
/*****************************************************************************/
INT4
RmonDeletePort (UINT4 u4IfIndex)
{
    tRmonQMsg     *pRmonQMsg = NULL;

    pRmonQMsg = (tRmonQMsg *) MemAllocMemBlk (gRmonVlanPoolId);
    if (pRmonQMsg == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC,"RmonDeletePort:"
                  " RMON Memory Allocation Failure - Q message\n");
        return RMON_FAILURE;
    }
    pRmonQMsg->u2Command = RMON_DELETE_PORT_MSG;
    pRmonQMsg->u4IfIndex = u4IfIndex;

    /* Enqueue the buffer to the RMON Task */
    if (RMON_SEND_TO_QUEUE (RMON_QUEUE_ID, (UINT1 *) &pRmonQMsg,
                OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gRmonVlanPoolId, (UINT1 *) pRmonQMsg);
        RMON_DBG (CRITICAL_TRC | MEM_TRC,"RmonDeletePort: "
                  " RMON Send to Q failed\n");
        return RMON_FAILURE;
    }

    RMON_SEND_EVENT (RMON_TASK_ID, RMON_QMSG_EVENT);

    return RMON_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmonPostDeleteVlan                                   */
/*                                                                           */
/* Description        : This function is used to indicate RMON about         */
/*                      VLAN deletion                                        */
/* Input(s)           : u4ContextId - Context Id, currently only default     */
/*                                    context is supported                   */
/*                      u4VlanId - Vlan index                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RMON_SUCCESS or RMON_FAILURE                         */
/*                                                                           */
/* Called By          : L2IwfDeleteVlan                                      */
/*****************************************************************************/
INT4
RmonPostDeleteVlan (UINT4 u4ContextId, UINT4 u4VlanId)
{
    tRmonQMsg     *pRmonQMsg = NULL;

    pRmonQMsg = (tRmonQMsg *) MemAllocMemBlk (gRmonVlanPoolId);
    if (pRmonQMsg == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " RMON Memory Allocation Failure - Q message\n");
        return RMON_FAILURE;
    }
    pRmonQMsg->u2Command = RMON_DELETE_VLAN_MSG;
    pRmonQMsg->u2L2CxtId = (UINT2) u4ContextId;
    pRmonQMsg->VlanId = (UINT2) u4VlanId;

    /* Enqueue the buffer to the RMON Task */
    if (RMON_SEND_TO_QUEUE (RMON_QUEUE_ID, (UINT1 *) &pRmonQMsg,
                OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gRmonVlanPoolId, (UINT1 *) pRmonQMsg);
        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                  " RMON Send to Q failed\n");
        return RMON_FAILURE;
    }

    RMON_SEND_EVENT (RMON_TASK_ID, RMON_QMSG_EVENT);

    return RMON_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmonDeleteVlan                                      */
/*                                                                           */
/* Description        : This function is used to delete the particular       */
/*                      EtherStatsNode from the etherStatsTable              */
/* Input(s)           : u2ContextId - Context Id, currently only default     */
/*                                    context is supported                   */
/*                      u4VlanId - Vlan index                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RMON_SUCCESS or RMON_FAILURE                         */
/*                                                                           */
/* Called By          : RmonHandleQMsg                                       */
/*****************************************************************************/
INT4
RmonDeleteVlan (UINT4 u4ContextId, UINT4 u4VlanId)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tRmonHistoryControlNode *pTmpHistoryControlNode = NULL;
    UINT4               u4HashIndex;

    /* Only default context is supported in RMON */
    if (u4ContextId != RMON_DEFAULT_CONTEXT)
    {
        return RMON_SUCCESS;
    }

    TMO_HASH_Scan_Table (gpRmonHistoryControlHashTable, u4HashIndex)
    {
        TMO_HASH_DYN_Scan_Bucket (gpRmonHistoryControlHashTable, u4HashIndex,
                              pHistoryControlNode, pTmpHistoryControlNode,
                              tRmonHistoryControlNode *)
        {
            if (pHistoryControlNode != NULL)
            {
                if ((pHistoryControlNode->u4HistoryControlDataSource ==
                     u4VlanId) && (pHistoryControlNode->u4HisCntrlOIDType ==
                                   (UINT4) VLAN_INDEX_TYPE))
                {
                    RMON_DBG2 (CRITICAL_TRC, "RmonDeleteVlan: "
                            "Deleting History node %d VlanId: %d \n",
                            pHistoryControlNode->i4HistoryControlIndex, u4VlanId);
                    RmonDeleteHistoryControlNode (pHistoryControlNode);

                }
            }
        }
    }

    pEtherStatsNode =
        HashSearchEtherStatsNode_DSource (gpRmonEtherStatsHashTable, u4VlanId,
                                          (UINT4) VLAN_INDEX_TYPE);
    if (pEtherStatsNode != NULL)
    {
        if ((RmonDeleteEtherStatsNode (pEtherStatsNode)) != RMON_SUCCESS)
        {
            return RMON_FAILURE;
        }

    }
    return RMON_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RmonUtlGetEtherStatsExtn                             */
/*                                                                           */
/* Description        : This function returns Ether Stats Table Extensions   */
/*                      This API is used by RMONv2.                          */
/*                                                                           */
/* Input(s)           : i4EtherStatsIndex                                    */
/*                                                                           */
/* Output(s)          : tRmonExtnNode *                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RmonUtlGetEtherStatsExtn (INT4 i4EtherStatsIndex,
                          tRmonExtnNode * pEtherStatsExtn)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (FUNC_ENTRY, "FUNC: ENTRY RmonUtlGetEtherStatsNode\n");

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            pEtherStatsExtn->u4CreateTime =
                pEtherStatsNode->u4EtherStatsCreateTime;
            pEtherStatsExtn->u4DroppedFrames =
                pEtherStatsNode->u4EtherStatsDroppedFrames;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    pEtherStatsExtn->u4CreateTime =
                        pEtherStatsNode->u4EtherStatsCreateTime;
                    pEtherStatsExtn->u4DroppedFrames =
                        pEtherStatsNode->u4EtherStatsDroppedFrames;
                    break;
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : RmonUtlGetHistCtlExtn                                */
/*                                                                           */
/* Description        : This function returns History Control Table Extension*/
/*                      This API is used by RMONv2.                          */
/*                                                                           */
/* Input(s)           : i4HistoryControlIndex                                */
/*                                                                           */
/* Output(s)          : tRmonExtnNode *                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RmonUtlGetHistCtlExtn (INT4 i4HistoryControlIndex, tRmonExtnNode * pHistCtlExtn)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (FUNC_ENTRY, "FUNC: ENTRY RmonUtlGetHistCtlNode\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {
            pHistCtlExtn->u4DroppedFrames =
                pHistoryControlNode->u4HistoryControlDroppedFrames;
        }
    }
}

/*****************************************************************************/
/* Function Name      : RmonUtlGetHostCtlExtn                                */
/*                                                                           */
/* Description        : This function returns Host Control Table Extension   */
/*                      This API is used by RMONv2.                          */
/*                                                                           */
/* Input(s)           : i4HostControlIndex                                   */
/*                                                                           */
/* Output(s)          : tRmonExtnNode *                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RmonUtlGetHostCtlExtn (INT4 i4HostControlIndex, tRmonExtnNode * pHostCtlExtn)
{
    tHostControl       *pHostCtl = NULL;

    pHostCtl = HOST_CONTROL (i4HostControlIndex);
    if (pHostCtl != NULL)
    {
        if (VALID_HOST_CONTROL (i4HostControlIndex))
        {
            pHostCtlExtn->u4CreateTime = pHostCtl->u4HostControlCreateTime;
            pHostCtlExtn->u4DroppedFrames =
                pHostCtl->u4HostControlDroppedFrames;
        }
    }
}

/*****************************************************************************/
/* Function Name      : RmonUtlGetMtrxCtlExtn                                */
/*                                                                           */
/* Description        : This function returns Matrix Control Table Extension */
/*                      This API is used by RMONv2.                          */
/*                                                                           */
/* Input(s)           : i4MatrixControlIndex                                 */
/*                                                                           */
/* Output(s)          : tRmonExtnNode *                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RmonUtlGetMtrxCtlExtn (INT4 i4MatrixControlIndex, tRmonExtnNode * pMtrxCtlExtn)
{
    tMatrixControlEntry *pMatrixControlEntry = NULL;

    pMatrixControlEntry = MATRIX_CTRL_TABLE (i4MatrixControlIndex);
    if (pMatrixControlEntry != NULL)
    {
        if (NOT_INVALID_MATRIX (i4MatrixControlIndex))
        {
            pMtrxCtlExtn->u4CreateTime =
                pMatrixControlEntry->u4MatrixControlCreateTime;
            pMtrxCtlExtn->u4DroppedFrames =
                pMatrixControlEntry->u4MatrixControlDroppedFrames;
        }
    }
}

#ifndef _ISS_WEB_C_
#define _ISS_WEB_C_
#ifdef WEBNM_WANTED
/*****************************************************************************/
/* Function Name      : RmonUtlGetWebStatsPage                               */
/*                                                                           */
/* Description        : This function processes the request coming for the   */
/*                      rmon ether stat page                                 */
/*                                                                           */
/* Input(s)           : pHttp - Pointer to the global HTTP data structure.   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
RmonUtlGetWebStatsPage (tHttp * pHttp)
{
    tSNMP_OID_TYPE          OidValue;    /* For Data Source */
    INT1                    i1RetVal = 0;
    UINT4                   u4IfIndex = 0;
    UINT4                   u4OIDType = 0;
    INT4                    i4Temp = 0;
    INT4                    i4StatsIndex = 0;
    INT4                    i4nextEtherStatsIndex = 0;
    UINT4                   u4CounterVal = 0;
    UINT4                   au4OidValue[RMON_MAX_OID_LEN];
    tSNMP_COUNTER64_TYPE    u8Value;
    CHR1                    ac1Val[RMON_U8_STR_LENGTH];
    FS_UINT8                u8Val;
    UINT4                   u4OverFlowValue = 0;
    UINT4                   u4ifSpeed = 0;
    UINT1                   au1OidValue[RMON_MAX_OID_LEN];

    pHttp->i4Write = 0;
    WebnmRegisterLock (pHttp, RmonLock, RmonUnLock);
    RMON_LOCK ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i1RetVal = nmhGetFirstIndexEtherStatsTable (&i4StatsIndex);

    if (i1RetVal == SNMP_FAILURE)
    {
        RMON_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    OidValue.pu4_OidList = &au4OidValue[0];
    /* Outside the DO loop, assign First Index to Next index
 *      * Inside the DO loop, reverse assignment. */

    i4nextEtherStatsIndex = i4StatsIndex;

    i4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = i4Temp;
        CLI_MEMSET (au4OidValue, 0, RMON_MAX_OID_LEN);
        CLI_MEMSET (au1OidValue, 0, RMON_MAX_OID_LEN);
        i4StatsIndex = i4nextEtherStatsIndex;
        u4OIDType = 0;
        MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
        MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
        MEMSET (&u8Val, 0, sizeof (FS_UINT8));
        u4ifSpeed = 0;

        STRCPY (pHttp->au1KeyString, "etherStatsIndex_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (INT4), "%d",
                  i4StatsIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsDataSource_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetEtherStatsDataSource (i4StatsIndex, &OidValue);
        WebnmConvertOidToString (&OidValue, au1OidValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN (au1OidValue) + 1),
                  "%s", au1OidValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        RmonCheckDataSourceOIDIsVlanOrInterface (&OidValue, &u4OIDType);
        if (u4OIDType == PORT_INDEX_TYPE)
        {
            u4IfIndex = OidValue.pu4_OidList[grlInterfaceOid.u4_Length];
            CfaGetIfSpeed (u4IfIndex, &u4ifSpeed);
        }

        STRCPY (pHttp->au1KeyString, "etherStatsDropEvents_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetEtherStatsDropEvents (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            nmhGetEtherStatsHighCapacityPkts (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsOctets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityOctets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsOctets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsBroadcastPkts_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsBroadcastPkts (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsMulticastPkts_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsMulticastPkts (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsCRCAlignErrors_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsCRCAlignErrors (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsUndersizePkts_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsUndersizePkts (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsOversizePkts_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsOversizePkts (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsFragments_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsFragments (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsJabbers_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsJabbers (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsCollisions_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetEtherStatsCollisions (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);


        STRCPY (pHttp->au1KeyString, "rmonStatsOutFCSErrors_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4CounterVal = 0;
        nmhGetRmonStatsOutFCSErrors (i4StatsIndex, &u4CounterVal);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4CounterVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts64Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts64Octets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts64Octets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts65to127Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts65to127Octets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts65to127Octets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts128to255Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts128to255Octets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts128to255Octets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts256to511Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts256to511Octets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts256to511Octets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts512to1023Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts512to1023Octets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts512to1023Octets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsPkts1024to1518Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4ifSpeed >= CFA_ENET_SPEED_1G)
        {
            MEMSET ((UINT1 *) ac1Val, 0, RMON_U8_STR_LENGTH);
            MEMSET (&u8Value, 0, sizeof (tSNMP_COUNTER64_TYPE));
            MEMSET (&u8Val, 0, sizeof (FS_UINT8));
            nmhGetEtherStatsHighCapacityPkts1024to1518Octets (i4StatsIndex, &u8Value);
            u8Val.u4Hi = u8Value.msn;
            u8Val.u4Lo = u8Value.lsn;
            FSAP_U8_2STR (&u8Val, ac1Val);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, ( STRLEN (ac1Val) + 1),
                      "%s", ac1Val);
        }
        else
        {
            u4CounterVal = 0;
            nmhGetEtherStatsPkts1024to1518Octets (i4StatsIndex, &u4CounterVal);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                      u4CounterVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts (i4StatsIndex,
                                                  &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowOctets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowOctets (i4StatsIndex,
                                                    &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts64Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts64Octets (i4StatsIndex,
                                                          &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts65to127Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets (i4StatsIndex,
                                                               &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts128to255Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets (i4StatsIndex,
                                                                &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts256to511Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets (i4StatsIndex,
                                                                &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts512to1023Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets (i4StatsIndex,
                                                                 &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "etherStatsHighCapacityOverflowPkts1024to1518Octets_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        u4OverFlowValue = 0;
        nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets (i4StatsIndex,
                                                                  &u4OverFlowValue);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4OverFlowValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    while (nmhGetNextIndexEtherStatsTable
           (i4StatsIndex, &i4nextEtherStatsIndex) == SNMP_SUCCESS);

    RMON_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}
#endif
#endif
