/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmapmtrx.c,v 1.19 2013/07/12 12:35:05 siva Exp $            *
 *                                                                  *
 * Description: Contains Functions to update the Statistics in      *
 *              Matrix Group tables, to search the Matrix Table &   *
 *              to create new entry in MatrixSD & matrixDS tables   *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmapmtrx.c                                      
 *                                                                           
 *    PRINCIPAL AUTHOR       :  Aricent Inc.                             
 *                                                                          
 *    SUBSYSTEM NAME         :  MATRIX MODULE                                  
 *                                                                          
 *    MODULE NAME            :  RMON APERIODIC                                 
 *                                                                           
 *    LANGUAGE               :  C                                      
 *                                                                           
 *    TARGET ENVIRONMENT     :  ANY                                          
 *                                                                           
 *    DATE OF FIRST RELEASE  :                                               
 *                                                                           
 *    DESCRIPTION            : This file contains the routines to Create new  
 *                             entry in SDMatrix and updates the statistics.
 *                             
 *                                                                           
 *  --------------------------------------------------------------------------  
 *
 *     CHANGE RECORD : 
 *     -------------
 *
 *
 *  -------------------------------------------------------------------------
 *     VERSION      |        AUTHOR/       |          DESCRIPTION OF    
 *                  |        DATE          |             CHANGE         
 * -----------------|----------------------|---------------------------------
 *      1.0         |  Vijay G. Krishnan   |         Create Original         
 *                  |    21-MAR-1997       |                                 
 * -----------------|----------------------|---------------------------------
 *      2.0         |  Leela L             |        This file is modified for  
 *                  |                      | dynamic creation of RMON tables    
 *                  |    31-MAR-2002       | using malloc.                      
 * -------------------------------------------------------------------------- 
 *      3.0         |  Leela L             |        This file is modified for  
 *                  |                      | new MIDGEN Tool Ouput.             
 *                  |    07-JUL-2002       |                                    
 * -------------------------------------------------------------------------- 
*/

#include "rmallinc.h"

/******************************************************************************
*      Function             : CreateNewEntryInTable                           *
*                                                                             *
*      Role of the function : This function creates new entries in Matrix     *
*                             SD Table.                                       *
*      Formal Parameters    : FrameParticulars : This structure contains      *
*                                                information about the ether  *
*                                                frame.                       *
*      Global Variables     : gaMatrixControlTable                            *
*      Side Effects         : Least Recently Used matrixSDTable entry is      *
*                             replaced by the new one                         *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : Index of the newly created entry                *
******************************************************************************/

static INT4
CreateNewEntryInTable (tFrameParticulars * FrameParticulars)
{
    UINT4               u4LeastTime;
    INT4                i4LRU;
    INT4                i4Index;
    INT4                i4IfIndex;
    tSdTableEntry      *pSDTable = NULL;
    tMatrixControlEntry *pCtrlEntry = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: CreateNewEntryInTable \n");

    i4IfIndex = (INT4) FrameParticulars->u1DataSource;

    /* Assign the MatrixControlEntry */
    pCtrlEntry = MATRIX_CONTROL_TBL (i4IfIndex);

    pSDTable = MATRIX_CONTROL_TBL (i4IfIndex)->pMatrixSDTable;

    /*  New Entry In The SD Table  */
    if (pCtrlEntry->u4MatrixControlTableSize < MAX_MATRIX_ENTRY)
    {
        pCtrlEntry->u4MatrixControlTableSize++;

        return ((INT4) pCtrlEntry->u4MatrixControlTableSize - 1);
    }

    u4LeastTime = (pSDTable + 0)->lastAccessTime;
    i4LRU = -1;
    for (i4Index = 0; i4Index < MAX_MATRIX_ENTRY; i4Index++)
    {
        if ((pSDTable + i4Index)->lastAccessTime < u4LeastTime)
        {
            i4LRU = i4Index;
            u4LeastTime = (pSDTable + i4Index)->lastAccessTime;
        }
    }

    /* Bucket associated with u2Index is Iniatialised with zero to remove 
       the previous  data. 
     */

    RMON_BZERO ((pSDTable + i4LRU), sizeof (tSdTableEntry));

    RMON_T_GET_TICK ((tOsixSysTime *)
                     & (pCtrlEntry->matrixControlLastDeleteTime));

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function : CreateNewEntryInTable \n");
    return i4LRU;

}

/******************************************************************************
*      Function             : SearchMatrixTable                               *
*                                                                             *
*      Role of the function : This function searches for an entry in the      *
*                             Matrix SD Table.                                *
*      Formal Parameters    : FrameParticulars : This structure contains      *
*                                                information about the ether  *
*                                                frame.                       *
*      Global Variables     : gaMatrixControlTable                            *
*      Side Effects         : None                                            *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : The index of the entry found or RMON_NOT_FOUND  *
******************************************************************************/

static INT2
SearchMatrixTable (tFrameParticulars * FrameParticulars)
{
    UINT2               u2Index;
    UINT1               u1Index;
    tSdTableEntry      *pSDTable;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: SearchMatrixTable \n");

    u1Index = FrameParticulars->u1DataSource;

    pSDTable = MATRIX_CONTROL_TBL (u1Index)->pMatrixSDTable;

    for (u2Index = 0; u2Index < MAX_MATRIX_ENTRY; u2Index++)
    {
        if ((MEMCMP (&(pSDTable + u2Index)->matrixSDSourceAddress,
                     &FrameParticulars->SrcAddress,
                     ADDRESS_LENGTH) == RMON_ZERO)
            &&
            (MEMCMP
             (&(pSDTable + u2Index)->matrixSDDestAddress,
              &FrameParticulars->DestAddress, ADDRESS_LENGTH) == RMON_ZERO))
        {

            return ((INT2) u2Index);
        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, " Leaving function: SearchMatrixTable \n");
    return RMON_NOT_FOUND;
}

/******************************************************************************
*      Function             : RmonUpdateMatrixTables                          *
*      Role of the function : This function updates the Matrix SD Table.      *
*      Formal Parameters    : FrameParticulars : This structure contains      *
*                              information about the ether                    *
*                                                frame.                       *
*      Global Variables     : gaMatrixControlTable                            *
*      Side Effects         : p_matrixSDTable is updated                      *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE if updation was successful, otherwise FALSE*
******************************************************************************/

UINT1
RmonUpdateMatrixTables (tFrameParticulars * FrameParticulars)
{
    INT4                i4MatrixIndex;
    INT4                i4Index;
    tSdTableEntry      *pSDTable;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: RmonUpdateMatrixTables \n");

    i4Index = (INT4) FrameParticulars->u1DataSource;

    if (i4Index >= RMON_MAX_INTERFACE_LIMIT)
    {
        RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                  "Index is invalid, beyond the Matrix Table array size\n");
        return FALSE;
    }

    if (MATRIX_CONTROL_TBL (i4Index) == NULL)
    {
        RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                  "Frame can not be processed since the Matrix control "
                  "entry configuration is not done yet \n");

        return FALSE;
    }

    /* Check whether the MatrixControl ENtry Status is VALID */
    if (MATRIX_CONTROL_TBL (i4Index)->matrixControlStatus == RMON_VALID)
    {
        /* Assign the MatrixSD table pointer */
        pSDTable = MATRIX_CONTROL_TBL (i4Index)->pMatrixSDTable;

        i4MatrixIndex = SearchMatrixTable (FrameParticulars);
        if ((i4MatrixIndex == RMON_NOT_FOUND) &&
            (FrameParticulars->b1WhetherError == FALSE))
        {
            i4MatrixIndex = CreateNewEntryInTable (FrameParticulars);
#ifdef RMON2_WANTED
            if (i4MatrixIndex == -1)
            {
                MATRIX_CONTROL_TBL (i4Index)->u4MatrixControlDroppedFrames++;
            }
#endif
            COPY_MAC_ADDRESS (&(pSDTable + i4MatrixIndex)->
                              matrixSDSourceAddress,
                              &FrameParticulars->SrcAddress);
            COPY_MAC_ADDRESS (&(pSDTable + i4MatrixIndex)->
                              matrixSDDestAddress,
                              &FrameParticulars->DestAddress);
        }
        else if (i4MatrixIndex == RMON_NOT_FOUND)
        {

            RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                      "Improper packet received : No updation done \n");

            return FALSE;
        }
        /* Updating The Matrix SD Table */
        (pSDTable + i4MatrixIndex)->u4MatrixSDPkts++;
        (pSDTable + i4MatrixIndex)->u4MatrixSDOctets +=
            FrameParticulars->u2Length;
        if (FrameParticulars->b1WhetherError == TRUE)
        {
            (pSDTable + i4MatrixIndex)->u4MatrixSDErrors++;
        }

        RMON_T_GET_TICK ((tOsixSysTime *)
                         & ((pSDTable + i4MatrixIndex)->lastAccessTime));

        return TRUE;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function: RmonUpdateMatrixTables \n");
    return FALSE;
}
