/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmaplog.c,v 1.17 2013/07/12 12:35:05 siva Exp $             *
 *                                                                  *
 * Description: Contains Functions to log the Event in Log Table    *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmaplog.c                                      
 *                                                                           
 *    PRINCIPAL AUTHOR       :  Aricent Inc.                             
 *                                                                          
 *    SUBSYSTEM NAME         :  LOG MODULE                                     
 *                                                                          
 *    MODULE NAME            :  RMON APERIODIC                                  
 *                                                                           
 *    LANGUAGE               :  C                                      
 *                                                                           
 *    TARGET ENVIRONMENT     :  ANY                                          
 *                                                                           
 *    DATE OF FIRST RELEASE  :                                               
 *                                                                           
 *    DESCRIPTION            :  This file allocates memory for the log 
 *                              buckets and updates the entries of the    
 *                              log table.                             
 *                                                                           
 *  --------------------------------------------------------------------------  
 *
 *     CHANGE RECORD : 
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |          DESCRIPTION OF    
 *                  |        DATE          |             CHANGE         
 * -----------------|----------------------|---------------------------------
 *      1.0         |   Vijay G. Krishnan  |         Create Original         
 *                  |     20-MAR-1997      |                                 
 * -----------------|----------------------|---------------------------------
 *      2.0         |   Leela L            |         This file is modified for
 *                  |     31-MAR-2002      | dynamic creation of RMON Tables  
 * --------------------------------------------------------------------------
 *      3.0         |   Leela L            |         This file is modified for
 *                  |     07-JUL-2002      | New MIDGEN Tool output..         
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"

/******************************************************************************
*      Function             : RmonLogEvent                                    *
*                                                                             *
*      Role of the function : This function                                   *
*                                                                             *
*      Formal Parameters    : i4EventIndex : Event entry that generated the   *
*                                             Log entry.                      *
*      Global Variables     : gaLogTable and gaEventTable                     *
*      Side Effects         : gaLogTable is updated                           *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonLogEvent (INT4 i4EventIndex)
{
    INT4               *pi4BktToBeFilled = NULL;
    tLogEntry          *pLogBkt = NULL;
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : RmonLogEvent \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC, "Event Table is empty.\n");
        return;
    }
    /* Check whether the LogEntry is NULL */
    if (pEventNode->pLogEvent == NULL)
    {
        RMON_DBG (CRITICAL_TRC | MEM_TRC, "LogTable for the Event is empty.\n");
        return;
    }

    pi4BktToBeFilled = &(pEventNode->pLogEvent->i4BucketToBeFilled);
    if (pi4BktToBeFilled)
    {
        if (*pi4BktToBeFilled == 0)
        {
            (*pi4BktToBeFilled)++;
        }
    }
    pLogBkt = (pEventNode->pLogEvent->pLogBucket);
    /* Initialize the LogEntry */
    if ((pLogBkt) && (pi4BktToBeFilled))
    {
        RMON_BZERO ((pLogBkt + *pi4BktToBeFilled), sizeof (tLogEntry));

        /* Get the System Time to record the Log */
        RMON_T_GET_TICK ((tOsixSysTime *) &
                         ((pLogBkt + *pi4BktToBeFilled)->logTime));

        /* Copy the logDescription string */
        STRNCPY ((pLogBkt + *pi4BktToBeFilled)->u1LogDescription, STD_LOG_DESCR,
                 STD_LOG_DESCR_LEN);
        (pLogBkt + *pi4BktToBeFilled)->
            u1LogDescription[STD_LOG_DESCR_LEN] = '\0';

        /* Concatenate the actual log Description with the Standard Description */
        STRNCAT ((char *) ((pLogBkt + *pi4BktToBeFilled)->u1LogDescription +
                           STD_LOG_DESCR_LEN),
                 (char *) pEventNode->u1EventDescription, EVENT_DESCR_LEN);
        (pLogBkt + *pi4BktToBeFilled)->
            u1LogDescription[STD_LOG_DESCR_LEN + EVENT_DESCR_LEN] = '\0';
    }
    if (pi4BktToBeFilled)
    {
        *pi4BktToBeFilled = ((*pi4BktToBeFilled + 1) % MAX_LOG_BUCKETS);

        if (*pi4BktToBeFilled == 0)
        {
            *pi4BktToBeFilled = MAX_LOG_BUCKETS;
            pEventNode->pLogEvent->i4BucketsFilled = MAX_LOG_BUCKETS;
        }
    }
    if (pEventNode->pLogEvent->i4BucketsFilled != MAX_LOG_BUCKETS)
    {
        pEventNode->pLogEvent->i4BucketsFilled++;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT, " Leaving function : RmonLogEvent \n");
}
