/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: rmonport.c,v 1.15 2016/06/29 11:10:57 siva Exp $             *
 *                                                                  *
 *                                                                  *
 *******************************************************************/
/****************************************************************************/
/*   FILE  NAME             :  rmonnp.c                                     */
/*                                                                          */
/*   PRINCIPAL AUTHOR       :  Aricent Inc.                              */
/*                                                                          */
/*   SUBSYSTEM NAME         :  RMON                                         */
/*                                                                          */
/*   MODULE NAME            :  HWAPI                                        */
/*                                                                          */
/*   LANGUAGE               :  C                                            */
/*                                                                          */
/*   TARGET ENVIRONMENT     :  ANY                                          */
/*                                                                          */
/*   DATE OF FIRST RELEASE  :                                               */
/*                                                                          */
/*   DESCRIPTION            :  This file provides the collection of ether   */
/*                             Statistics from Hardware through APIS and    */
/*                             updates the ether stats table for the further*/
/*                             SNMP processing.                             */
/*                             This file file also provides the function    */
/*                             to populate the test statistics              */
/*                                                                          */
/* ************************************************************************ */

#include "rmallinc.h"

/* LOCAL Prototypes declaration */
#ifdef NPAPI_WANTED
PRIVATE VOID        RmonHwGetEtherStatsTable (VOID);
PRIVATE VOID        RmonHwSetEtherStatsTable (UINT1);

#ifdef EXTND_SERVICE
PRIVATE INT4        SearchNewMatrixTable (UINT4, tMacAddr *, tMacAddr *);
PRIVATE INT4        NewCreateEntryInMatrixTable (UINT4);
#endif
#endif

/****************************************************************************/
/* Function            : RmonFetchHWStats                                   */
/* Input (s)           : None.                                              */
/* Output (s)          : None.                                              */
/* Returns             : None.                                              */
/* Global Variables                                                         */
/*             Used    : rmapB1AnyValidStats, rmapB1AnyValidHost,           */
/*                       rmapB1AnyValidMatrix                               */
/* Side effects        : None                                               */
/* Action              : This Task collects the statistics from hardware    */
/*                       for every one second if the Control entry is Valid */
/****************************************************************************/
VOID
RmonFetchHWStats (VOID)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering the function :RmonFetchHWStats\n");

#ifdef NPAPI_WANTED
    /* Check whether any VALID Control entries in EtherStats Table */
    if (gRmonSupportedGroups.u1RmonHwStatsSuppFlag == TRUE)
    {
        /* Invoke the Generic Hardware API to retrieve the Data from
         * Hardware and update the RMON Tables. */
        RmonHwGetEtherStatsTable ();
    }

#endif /* NPAPI_WANTED */

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving  function :RmonFetchHWStats\n");
}

#ifdef NPAPI_WANTED
/****************************************************************************/
/*   Function Name           :  RmonHwGetEtherStatsTable                    */
/*                                                                          */
/*   Description             : This API call collects the ether statistics  */
/*                             from Hardware by calling the specific HW API.*/
/*                                                                          */
/*   Input (s)               : None                                         */
/*                                                                          */
/*   Output (s)              : None                                         */
/*                                                                          */
/*   Global Variables Referred  : HWEtherStatsTable,                        */
/*                                gpRmonEtherStatsHashTable                 */
/*                                                                          */
/*   Global Variables Modified  : None                                      */
/*                                                                          */
/*   Returns                 : None                                         */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
RmonHwGetEtherStatsTable (VOID)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    tRmonEtherStatsNode *pEtherStatsPktsNode;
    tVlanEthStats       VlanStatsEntry;    /* Vlan structure for holding vlan statistics */
    UINT4               u4HashIndex;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    MEMSET (&VlanStatsEntry, 0, sizeof (tVlanEthStats));

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entered in RmonHwGetEtherStatsTable \n");

    /* For all the Configured VALID interfaces in the Control Table ,
     * Collect the HW statistics through specific HW APIs and update 
     * the RMON EtherStats Table */

    TMO_HASH_Scan_Table (gpRmonEtherStatsHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gpRmonEtherStatsHashTable, u4HashIndex,
                              pEtherStatsNode, tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode != NULL)
            {
                pEtherStatsPktsNode = &(pEtherStatsNode->RmonEtherStatsPkts);

                /* Oper status other than UP for an interface does not 
                 * required to have stats collection. */
                u1OperStatus = CFA_IF_DOWN;

                CfaGetIfOperStatus (pEtherStatsNode->u4EtherStatsDataSource, 
                                    &u1OperStatus);
                if (u1OperStatus != CFA_IF_UP)
                {
                    continue;
                }

                if (pEtherStatsNode->etherStatsStatus == RMON_VALID)
                {
                    if (pEtherStatsNode->u4EtherStatsOIDType == PORT_INDEX_TYPE)
                    {
                        pEtherStatsPktsNode->u4EtherStatsBatchId = 
                            gu4RmonEtherStatsBatchId;

                        if (RmonFsRmonHwGetEthStatsTable (pEtherStatsNode->
                                                          u4EtherStatsDataSource,
                                                          pEtherStatsPktsNode)
                            != FNP_SUCCESS)
                        {
                            RMON_DBG1 (RMON_MGMT_TRC,
                                       "Failed to Retrieve the Ether Stats for the interface: %d\n",
                                       pEtherStatsNode->u4EtherStatsDataSource);
                        }
                    }
                    else if (pEtherStatsNode->u4EtherStatsOIDType ==
                             VLAN_INDEX_TYPE)
                    {
                        if (VlanGetRmonStatsPerVlan (RMON_DEFAULT_CONTEXT,
                                                     pEtherStatsNode->
                                                     u4EtherStatsDataSource,
                                                     &VlanStatsEntry) !=
                            FNP_SUCCESS)
                        {
                            RMON_DBG1 (RMON_MGMT_TRC,
                                       "Failed to Retrieve the Vlan Stats for the vlan interface: %d\n",
                                       pEtherStatsNode->u4EtherStatsDataSource);
                        }
                        /* Copy the collected statistics from the vlan structure to actual
                         * rmon structure
                         */
                        MEMCPY (pEtherStatsPktsNode, &VlanStatsEntry,
                                sizeof (tVlanEthStats));
                        /* By default high capacity counters will be updated for VLAN based RMON
                        *  collection and History table will consider either the high capacity/normal
                        *  counters based on the port speed for RMON collection per VLAN*/
                        pEtherStatsNode->RmonEtherStatsPkts.u8EtherStatsHighCapacityPkts.lsn  = VlanStatsEntry.u4VlanStatsPkts;
                        pEtherStatsNode->RmonEtherStatsPkts.u8EtherStatsHighCapacityOctets.lsn = VlanStatsEntry.u4VlanStatsOctets;

                    }
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving RmonHwGetEtherStatsTable \n");

}

#ifdef EXTND_SERVICE
/****************************************************************************/
/*      Function Name        : SearchNewMatrixTable                         */
/*      Role of the function : This function searches for an entry in the   */
/*                             Matrix SD Table.                             */
/*      Formal Parameters    : SrcMacAddr - Source MAC Address              */
/*                             DstMacAddr - Destination MAC Address         */
/*                                                                          */
/*      Global Variables     : gaMatrixControlTable                         */
/*                                                                          */
/*      Side Effects         : None                                         */
/*                                                                          */
/*      Exception Handling   : None                                         */
/*                                                                          */
/*      Use of Recursion     : None                                         */
/*                                                                          */
/*      Return Value         : The index of the entry found or              */
/*                             RMON_NOT_FOUND                               */
/*                                                                          */
/****************************************************************************/
PRIVATE INT4
SearchNewMatrixTable (UINT4 u4IfIndex, tMacAddr * SrcMacAddr,
                      tMacAddr * DstMacAddr)
{
    INT4                i4Index;
    tSdTableEntry      *pSDTable = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: SearchNewMatrixTable \n");

    pSDTable = MATRIX_CONTROL_TBL (u4IfIndex)->pMatrixSDTable;

    for (i4Index = 0; i4Index < MAX_MATRIX_ENTRY; i4Index++)
    {
        if ((COMPARE_MAC_ADDRESS
             ((pSDTable + i4Index)->matrixSDSourceAddress,
              *SrcMacAddr) == RMON_EQUAL)
            &&
            (COMPARE_MAC_ADDRESS
             ((pSDTable + i4Index)->matrixSDDestAddress,
              *DstMacAddr) == RMON_EQUAL))
        {
            RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                      " SearchNewMatrixTable Index retrieval success. \n");

            return i4Index;
        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function: SearchNewMatrixTable \n");
    return RMON_NOT_FOUND;
}

/***************************************************************************/
/*      Function             : NewCreateEntryInMatrixTable                 */
/*                                                                         */
/*      Role of the function : This function creates new entries in Matrix */
/*                             SD Table.                                   */
/*                                                                         */
/*      Formal Parameters    : SrcMacAddr - Source MAC Address             */
/*                             DstMacAddr - Destination MAC Address        */
/*                                                                         */
/*      Global Variables     : gaMatrixControlTable                        */
/*                                                                         */
/*      Side Effects         : Least Recently Used matrixSDTable entry is  */
/*                             replaced by the new one.                    */
/*                                                                         */
/*      Exception Handling   : None                                        */
/*                                                                         */
/*      Use of Recursion     : None                                        */
/*                                                                         */
/*      Return Value         : Index of the newly created entry            */
/*                                                                         */
/***************************************************************************/
PRIVATE INT4
NewCreateEntryInMatrixTable (UINT4 u4IfIndex)
{
    UINT4               u4LeastTime;
    INT4                i4LRU;
    INT4                i4Index;
    tSdTableEntry      *pSDTable = NULL;
    tMatrixControlEntry *pCtrlEntry = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: NewCreateEntryInMatrixTable \n");

    /* Assign the MatrixControlEntry */
    pCtrlEntry = MATRIX_CONTROL_TBL (u4IfIndex);

    pSDTable = MATRIX_CONTROL_TBL (u4IfIndex)->pMatrixSDTable;

    /*  New Entry In The SD Table  */
    if (pCtrlEntry->u4MatrixControlTableSize < MAX_MATRIX_ENTRY)
    {
        pCtrlEntry->u4MatrixControlTableSize++;

        return (pCtrlEntry->u4MatrixControlTableSize - 1);
    }

    u4LeastTime = (pSDTable + 0)->lastAccessTime;
    i4LRU = 0;

    for (i4Index = 0; i4Index < MAX_MATRIX_ENTRY; i4Index++)
    {
        if ((pSDTable + i4Index)->lastAccessTime < u4LeastTime)
        {
            i4LRU = i4Index;
            u4LeastTime = (pSDTable + i4Index)->lastAccessTime;
        }
    }

    /* Bucket associated with u2Index is Iniatialised with zero to remove 
     *  the previous  data. */

    RMON_BZERO ((pSDTable + i4LRU), sizeof (tSdTableEntry));

    RMON_T_GET_TICK ((tOsixSysTime *)
                     & (pCtrlEntry->matrixControlLastDeleteTime));

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function : NewCreateEntryInMatrixTable \n");

    return i4LRU;
}
#endif /* EXTND_SERVICE */

/****************************************************************************/
/*   Function Name           :  RmonHwSetEtherStatsTable                    */
/*                                                                          */
/*   Description             : This API configures the etherstats entry     */
/*                             in Hardware by calling the specific HW API.  */
/*                                                                          */
/*   Input (s)               : None                                         */
/*                                                                          */
/*   Output (s)              : None                                         */
/*                                                                          */
/*   Global Variables Referred  : HWEtherStatsTable,                        */
/*                                gpRmonEtherStatsHashTable                 */
/*                                                                          */
/*   Global Variables Modified  : None                                      */
/*                                                                          */
/*   Returns                 : None                                         */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
RmonHwSetEtherStatsTable (UINT1 u1Status)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               u4HashIndex;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entered in RmonHwSetEtherStatsTable \n");

    /* For all the Configured VALID interfaces in the Control Table ,
     * Set the HW configuration through specific HW APIs  */

    TMO_HASH_Scan_Table (gpRmonEtherStatsHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gpRmonEtherStatsHashTable, u4HashIndex,
                              pEtherStatsNode, tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode != NULL)
            {
                if ((pEtherStatsNode->etherStatsStatus == RMON_VALID) &&
                    ((pEtherStatsNode->u4EtherStatsOIDType == PORT_INDEX_TYPE)
                     || (pEtherStatsNode->u4EtherStatsOIDType ==
                         VLAN_INDEX_TYPE)))
                {
                    if (RmonFsRmonHwSetEtherStatsTable
                        (pEtherStatsNode->u4EtherStatsDataSource,
                         u1Status) != FNP_SUCCESS)
                    {
                        RMON_DBG (MAJOR_TRC | MEM_TRC,
                                  "Failed in configuring the HW etherStats table \n");
                    }
                    /*Clearing the local counter values */
                    if (u1Status == FNP_FALSE)
                    {
                        MEMSET (&(pEtherStatsNode->RmonEtherStatsPkts), 0,
                                sizeof (tRmonEtherStatsNode));
                    }
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving RmonHwSetEtherStatsTable \n");

}
#endif /* NPAPI_WANTED */

/****************************************************************************/
/* Function            : RmonSetHWConfiguration                             */
/* Input (s)           : u1Status (VALID/INVALID)                           */
/* Output (s)          : None.                                              */
/* Returns             : None.                                              */
/* Global Variables                                                         */
/*             Used    : gRmonSupportedGroups                               */
/* Side effects        : None                                               */
/* Action              : This function sets the configurations to hardware  */
/****************************************************************************/
VOID
RmonSetHWConfigurations (UINT1 u1Status)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering the function :RmonSetHWConfigurations \n");

#ifdef NPAPI_WANTED
    /* Check whether any VALID Control entries in EtherStats Table */
    if (gRmonSupportedGroups.u1RmonHwStatsSuppFlag == TRUE)
    {
        /* Invoke the Generic Hardware API to set the EtherStats Configuration 
         * in Hardware . */
        RmonHwSetEtherStatsTable (u1Status);
    }

    /* Check whether any VALID entries in HostControl Table */
#else
    UNUSED_PARAM (u1Status);
#endif /* NPAPI_WANTED */

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving function :RmonSetHWConfigurations\n");
}

/****************************************************************************/
/*   Function Name           :  RmonClearEtherStatsTable                    */
/*                                                                          */
/*   Description             : This api clears the statistics in control    */
/*                             plane data structures of EtherStatsTable     */
/*                                                                          */
/*   Input (s)               : None                                         */
/*                                                                          */
/*   Output (s)              : None                                         */
/*                                                                          */
/*   Global Variables Referred  : HWEtherStatsTable,                        */
/*                                gpRmonEtherStatsHashTable                 */
/*                                                                          */
/*   Global Variables Modified  : None                                      */
/*                                                                          */
/*   Returns                 : None                                         */
/*                                                                          */
/****************************************************************************/
VOID
RmonClearEtherStatsTable (VOID)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               u4HashIndex;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entered in RmonClearEtherStatsTable \n");

    /* For all the Configured VALID interfaces in the Control Table ,
     * Set the HW configuration through specific HW APIs  */

    TMO_HASH_Scan_Table (gpRmonEtherStatsHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gpRmonEtherStatsHashTable, u4HashIndex,
                              pEtherStatsNode, tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode != NULL)
            {
                if ((pEtherStatsNode->etherStatsStatus == RMON_VALID) &&
                    ((pEtherStatsNode->u4EtherStatsOIDType == PORT_INDEX_TYPE)
                     || (pEtherStatsNode->u4EtherStatsOIDType ==
                         VLAN_INDEX_TYPE)))
                {
                    MEMSET (&(pEtherStatsNode->RmonEtherStatsPkts), 0,
                            sizeof (tRmonEtherStatsNode));
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving RmonClearEtherStatsTable \n");

}
