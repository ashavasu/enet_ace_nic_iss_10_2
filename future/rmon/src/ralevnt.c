/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: ralevnt.c,v 1.48 2016/06/18 11:23:16 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for Event Group    *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  ralevnt.c
 *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.
 *
 *    SUBSYSTEM NAME         :  LOW LEVEL CODE
 *
 *    MODULE NAME            :  RMON PERIODIC
 *
 *    LANGUAGE               :  C
 *
 *    TARGET ENVIRONMENT     :  ANY
 *
 *    DATE OF FIRST RELEASE  :
 *
 *    DESCRIPTION            : This file provides the low level routines 
 *                             for the Event group.    
 *
 *
 *  --------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     -------------
 *
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |         DESCRIPTION OF
 *                  |        DATE          |            CHANGE
 * -----------------|----------------------|---------------------------------
 *      1.0         |  Vijay G. Krishnan   |        Create Original
 *                  |     23-APR-1997      |
 * --------------------------------------------------------------------------
 *      2.0         |  LEELA L             |        Modified for dynamic tables
 *                  |     31-MAR-2002      |
 * --------------------------------------------------------------------------
 *      3.0         |  LEELA L             |        Modified for new MIDGEN    
 *                  |     07-JUL-2002      |
 * --------------------------------------------------------------------------
 *      4.0         |  LEELA L             |        Modified for EventCommunity 
 *                  |     27-AUG-2002      |        OctetString.
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"
#include "cli.h"
#include "rmoncli.h"
#include "snmcport.h"
#include "rmgr.h"

 /*
  * LOW LEVEL Routines for Table : eventTable.
  */

 /*
  * GET_EXACT Validate Index Instance Routine.
  */
/***********************************************************************
 Function    :  nmhValidateIndexInstanceEventTable
 Input       :  The Indices
                i4EventIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***********************************************************************/

INT1
nmhValidateIndexInstanceEventTable (INT4 i4EventIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into function: nmhValidateIndexInstanceEventTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        CLI_SET_ERR (CLI_RMON_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4EventIndex >= 1) && (i4EventIndex <= RMON_MAX_EVENT_INDEX)
        && (HashSearchEventNode (gpRmonEventHashTable, i4EventIndex) != NULL))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: Valid Event Index.\n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: EventIndex does't exist\n");
        return SNMP_FAILURE;
    }
}

 /*
  * GET_FIRST Routine.
  */

/*********************************************************************
 Function    :  nmhGetFirstIndexEventTable
 Input       :  The Indices
                pi4EventIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***********************************************************************/

INT1
nmhGetFirstIndexEventTable (INT4 *pi4EventIndex)
{
    INT4                i4Zero = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetFirstIndexEventTable \n");

    return nmhGetNextIndexEventTable (i4Zero, pi4EventIndex);
}

 /*
  * GET_NEXT Routine.
  */
/****************************************************************************
 Function    :  nmhGetNextIndexEventTable
 Input       :  The Indices
                i4EventIndex
                pi4NextEventIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexEventTable (INT4 i4EventIndex, INT4 *pi4NextEventIndex)
{
    INT4                i4Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetNextIndexEventTable \n");

    if ((i4EventIndex < RMON_ZERO) || (i4EventIndex > RMON_MAX_EVENT_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid Index - out of range\n");
        return SNMP_FAILURE;
    }

    i4Index = HashSearchEventNode_NextValidTindex (gpRmonEventHashTable,
                                                   i4EventIndex);

    if ((i4Index != RMON_ZERO) && (i4Index > i4EventIndex) &&
        (i4Index <= RMON_MAX_EVENT_INDEX))
    {
        *pi4NextEventIndex = i4Index;
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure :Passed Event Index is out of range \n");

    return SNMP_FAILURE;
}

 /*
  * Low Level GET Routine for All Objects
  */
/****************************************************************************
 Function    :  nmhGetEventDescription
 Input       :  The Indices
                i4EventIndex

                The Object
                pRetValEventDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEventDescription (INT4 i4EventIndex, tSNMP_OCTET_STRING_TYPE
                        * pRetValEventDescription)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetEventDescription \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus != RMON_INVALID)
        {
            STRNCPY (pRetValEventDescription->pu1_OctetList,
                     pEventNode->u1EventDescription, EVENT_DESCR_LEN);

            pRetValEventDescription->i4_Length =
                (INT4) STRLEN (pEventNode->u1EventDescription);

            RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                      " Event Description retrieved success. \n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure : INVALID Event Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEventType
 Input       :  The Indices
                i4EventIndex

                The Object
                pi4RetValEventType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEventType (INT4 i4EventIndex, INT4 *pi4RetValEventType)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetEventType \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus != RMON_INVALID)
        {
            *pi4RetValEventType = pEventNode->eventType;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure : INVALID Event Index.\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEventCommunity
 Input       :  The Indices
                i4EventIndex

                The Object
                pRetValEventCommunity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEventCommunity (INT4 i4EventIndex, tSNMP_OCTET_STRING_TYPE
                      * pRetValEventCommunity)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetEventCommunity \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if ((pEventNode->eventStatus != RMON_INVALID) &&
            (pEventNode->EventCommunity.pu1_OctetList != NULL))
        {
            if (pEventNode->EventCommunity.i4_Length > 0)
            {
                MEMCPY (pRetValEventCommunity->pu1_OctetList,
                        pEventNode->EventCommunity.pu1_OctetList,
                        pEventNode->EventCommunity.i4_Length);
            }

            pRetValEventCommunity->i4_Length =
                pEventNode->EventCommunity.i4_Length;

            RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                      " Retrieval of Event Community is success. \n");

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure : INVALID Event Index.\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEventLastTimeSent
 Input       :  The Indices
                i4EventIndex

                The Object
                pu4RetValEventLastTimeSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEventLastTimeSent (INT4 i4EventIndex, UINT4 *pu4RetValEventLastTimeSent)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetEventLastTimeSent \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus != RMON_INVALID)
        {
            *pu4RetValEventLastTimeSent = pEventNode->eventLastTimeSent;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              " SNMP Failure : INVALID Event Index.\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEventOwner
 Input       :  The Indices
                i4EventIndex

                The Object
                pRetValEventOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEventOwner (INT4 i4EventIndex, tSNMP_OCTET_STRING_TYPE *
                  pRetValEventOwner)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetEventOwner \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus != RMON_INVALID)
        {
            STRNCPY (pRetValEventOwner->pu1_OctetList,
                     pEventNode->u1EventOwner, OWN_STR_LENGTH);

            pRetValEventOwner->i4_Length =
                (INT4) STRLEN (pEventNode->u1EventOwner);

            RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                      " Retrieval of Event Owner is success. \n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              " SNMP Failure : INVALID Event Index \n ");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEventStatus
 Input       :  The Indices
                i4EventIndex

                The Object
                pi4RetValEventStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEventStatus (INT4 i4EventIndex, INT4 *pi4RetValEventStatus)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetEventStatus \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus != RMON_INVALID)
        {
            *pi4RetValEventStatus = pEventNode->eventStatus;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              " SNMP Failure : INVALID Event Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhSetEventDescription
 Input       :  The Indices
                i4EventIndex

                The Object
                pSetValEventDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEventDescription (INT4 i4EventIndex, tSNMP_OCTET_STRING_TYPE
                        * pSetValEventDescription)
{

    tRmonEventNode     *pEventNode = NULL;
    UINT4               u4Minimum;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetEventDescription \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    u4Minimum =
        (UINT4) ((pSetValEventDescription->i4_Length <
                  EVENT_DESCR_LEN) ? pSetValEventDescription->
                 i4_Length : EVENT_DESCR_LEN);

    if (pEventNode != NULL)
    {
        MEMSET (pEventNode->u1EventDescription, 0, EVENT_DESCR_LEN);

        STRNCPY (pEventNode->u1EventDescription,
                 pSetValEventDescription->pu1_OctetList, u4Minimum);
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetEventDescription \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetEventType                          
 Input       :  The Indices                                                   
                i4EventIndex

                The Object
                i4SetValEventType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEventType (INT4 i4EventIndex, INT4 i4SetValEventType)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetEventType \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        pEventNode->eventType = i4SetValEventType;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetEventType \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetEventCommunity
 Input       :  The Indices
                i4EventIndex

                The Object
                pSetValEventCommunity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEventCommunity (INT4 i4EventIndex, tSNMP_OCTET_STRING_TYPE
                      * pSetValEventCommunity)
{
    UINT4               u4Minimum;
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetEventCommunity \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    u4Minimum = (UINT4) ((pSetValEventCommunity->i4_Length < COMM_LENGTH) ?
                         pSetValEventCommunity->i4_Length : COMM_LENGTH);

    if (pEventNode != NULL)
    {
        MEMCPY (pEventNode->EventCommunity.pu1_OctetList,
                pSetValEventCommunity->pu1_OctetList, u4Minimum);

        pEventNode->EventCommunity.i4_Length = pSetValEventCommunity->i4_Length;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetEventCommunity\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetEventOwner
 Input       :  The Indices
                i4EventIndex

                The Object
                pSetValEventOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEventOwner (INT4 i4EventIndex, tSNMP_OCTET_STRING_TYPE
                  * pSetValEventOwner)
{
    UINT4               u4Minimum;
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetEventOwner \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    u4Minimum = (UINT4) ((pSetValEventOwner->i4_Length < OWN_STR_LENGTH) ?
                         pSetValEventOwner->i4_Length : OWN_STR_LENGTH);

    if (pEventNode != NULL)
    {
        MEMSET (pEventNode->u1EventOwner, 0, OWN_STR_LENGTH);

        STRNCPY (pEventNode->u1EventOwner,
                 pSetValEventOwner->pu1_OctetList, u4Minimum);
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetEventOwner \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetEventStatus
 Input       :  The Indices
                i4EventIndex

                The Object
                i4SetValEventStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEventStatus (INT4 i4EventIndex, INT4 i4SetValEventStatus)
{
    tRmonStatus        *pStatus = NULL;
    tRmonEventNode     *pEventNode = NULL;
    UINT4               u4Hindex = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetEventStatus \n");

    if ((i4EventIndex <= 0) || (i4EventIndex > RMON_MAX_EVENT_INDEX))
    {
        CLI_SET_ERR (CLI_RMON_SET_EVENT);
        return SNMP_FAILURE;
    }

    /*Check and allocate the memory for the *
     * control entry of the Event Control table     */

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if ((pEventNode == NULL) && (i4SetValEventStatus == RMON_CREATE_REQUEST))
    {
        pEventNode = (tRmonEventNode *) MemAllocMemBlk (gRmonEventPoolId);

        if (pEventNode == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      "Memory Allocation Failure - Event Table. \n");
            CLI_SET_ERR (CLI_RMON_MAX_EVENT);
            return SNMP_FAILURE;
        }
        pEventNode->eventStatus = RMON_INVALID;

        pEventNode->pLogEvent = NULL;

        MEMSET (pEventNode->u1EventDescription, 0, EVENT_DESCR_LEN);

        MEMSET (pEventNode->u1EventOwner, 0, OWN_STR_LENGTH);

        pEventNode->i4EventIndex = i4EventIndex;

        u4Hindex = (UINT4) RmonHashFn ((UINT4) pEventNode->i4EventIndex);

        RMON_HASH_ADD_NODE (gpRmonEventHashTable, &pEventNode->nextEventNode,
                            u4Hindex, NULL);
    }
    else if (pEventNode == NULL)
    {
        CLI_SET_ERR (CLI_RMON_SET_EVENT);
        return SNMP_FAILURE;
    }

    if (pEventNode != NULL)
    {
        pStatus = &(pEventNode->eventStatus);

        switch (*pStatus)
        {
            case RMON_VALID:
                if (i4SetValEventStatus == RMON_INVALID)
                {
                    if (HashSearchAlarmNode_ForEvent (gpRmonAlarmHashTable,
                                                      i4EventIndex) ==
                        RMON_TRUE)
                    {
                        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                  "Failed : Dependent Alarm Entry exists\n");
                        CLI_SET_ERR (CLI_RMON_DEPENDENT_ALARM);
                        return SNMP_FAILURE;
                    }
                    /* Memory pointers allocated in log module is freed here */
                    if ((pEventNode->eventType == LOG) ||
                        (pEventNode->eventType == LOG_AND_TRAP))
                    {
                        if (pEventNode->pLogEvent != NULL)
                        {
                            if (pEventNode->pLogEvent->pLogBucket != NULL)
                            {
                                MemReleaseMemBlock (gRmonLogBucketPoolId,
                                                    (UINT1 *) pEventNode->
                                                    pLogEvent->pLogBucket);

                                pEventNode->pLogEvent->pLogBucket = NULL;
                            }

                        }
                    }

                    if (pEventNode->pLogEvent != NULL)
                    {
                        MemReleaseMemBlock (gRmonLogEventPoolId,
                                            (UINT1 *) pEventNode->pLogEvent);
                        pEventNode->pLogEvent = NULL;
                    }

                    /* Memory Pointers allocated for Event Community is freed */

                    if (pEventNode->EventCommunity.pu1_OctetList != NULL)
                    {
                        MemReleaseMemBlock (gRmonEventCommPoolId,
                                            (UINT1 *) pEventNode->
                                            EventCommunity.pu1_OctetList);
                        pEventNode->EventCommunity.pu1_OctetList = NULL;
                    }

                    /* Delete the memory for Event control table */

                    u4Hindex =
                        (UINT4) RmonHashFn ((UINT4) pEventNode->i4EventIndex);

                    RmonStopTimer (EVENT_TABLE, i4EventIndex);

                    RMON_HASH_DEL_NODE (gpRmonEventHashTable,
                                        &pEventNode->nextEventNode, u4Hindex);

                    MemReleaseMemBlock (gRmonEventPoolId, (UINT1 *) pEventNode);
                    pEventNode = NULL;

                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                              " Event entry VALID to INVALID \n\n");
                }
                else if (i4SetValEventStatus == RMON_UNDER_CREATION)
                {

                    if ((pEventNode->eventType == LOG) ||
                        (pEventNode->eventType == LOG_AND_TRAP))
                    {
                        /* Memory pointers allocated in log module is freed here */
                        if (pEventNode->pLogEvent->pLogBucket != NULL)
                        {
                            MemReleaseMemBlock (gRmonLogBucketPoolId,
                                                (UINT1 *) pEventNode->
                                                pLogEvent->pLogBucket);
                            pEventNode->pLogEvent->pLogBucket = NULL;

                        }

                        /* Assigning the no. of buckets granted.. */
                        pEventNode->pLogEvent->i4BucketsAllocated = 0;
                        pEventNode->pLogEvent->i4BucketToBeFilled = 0;
                        pEventNode->pLogEvent->i4BucketsFilled = 0;
                    }

                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                              "Event Entry VALID to UNDER_CREATION - timer started \n");

                    /* Starting timer to delete entries not made valid in 
                     * given time */
                    /* Assign the Status  */
                    *pStatus = i4SetValEventStatus;
                    RmonStartTimer (EVENT_TABLE, i4EventIndex, EVENT_DURATION);
                }
                break;

            case RMON_INVALID:
                *pStatus = i4SetValEventStatus;
                if (i4SetValEventStatus == RMON_CREATE_REQUEST)
                {
                    /* Allocate Memory for Log Table */
                    pEventNode->pLogEvent =
                        MemAllocMemBlk (gRmonLogEventPoolId);

                    if (pEventNode->pLogEvent == NULL)
                    {
                        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                  " Memory allocation for Log Entry failed. \n");
                        CLI_SET_ERR (CLI_RMON_EVENT_ENTRY_MEM_ALLOC_FAIL);
                        return SNMP_FAILURE;
                    }

                    pEventNode->EventCommunity.pu1_OctetList =
                        MemAllocMemBlk (gRmonEventCommPoolId);
                    if (pEventNode->EventCommunity.pu1_OctetList == NULL)
                    {
                        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                  " Memory allocation for"
                                  " Community string failed. \n");
                        MemReleaseMemBlock (gRmonLogEventPoolId,
                                            (UINT1 *) pEventNode->pLogEvent);
                        pEventNode->pLogEvent = NULL;
                        CLI_SET_ERR (CLI_RMON_EVENT_ENTRY_MEM_ALLOC_FAIL);
                        return SNMP_FAILURE;
                    }

                    pEventNode->EventCommunity.i4_Length = 0;

                    /* Assigning the no. of buckets granted.. */
                    pEventNode->pLogEvent->i4BucketsAllocated = 0;
                    pEventNode->pLogEvent->i4BucketToBeFilled = 0;
                    pEventNode->pLogEvent->i4BucketsFilled = 0;

                    /* Assign the Status UNDER_CREATION */
                    *pStatus = RMON_UNDER_CREATION;
                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                              "Event Entry INVALID to UNDER_CREATION - timer started\n");

                    /*Start timer to delete entries not made valid in given time */
                    RmonStartTimer (EVENT_TABLE, i4EventIndex, EVENT_DURATION);
                }
                break;

            case RMON_UNDER_CREATION:
                /* Stop the Timer. */
                if ((i4SetValEventStatus == RMON_VALID) ||
                    (i4SetValEventStatus == RMON_INVALID))
                {

                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                              " UNDER_CREATION to VALID or INVALID - Timer Stopped \n");
                    RmonStopTimer (EVENT_TABLE, i4EventIndex);
                }

                /* For VALID status, */
                if (i4SetValEventStatus == RMON_VALID)
                {
                    if ((pEventNode->eventType == LOG) ||
                        (pEventNode->eventType == LOG_AND_TRAP))
                    {
                        /* Allocate Memory for LOGs Buckets of size MAX_LOG_BUCKETS */
                        pEventNode->pLogEvent->pLogBucket =
                            MemAllocMemBlk (gRmonLogBucketPoolId);

                        pEventNode->pLogEvent->i4BucketsAllocated =
                            MAX_LOG_BUCKETS;
                        pEventNode->pLogEvent->i4BucketToBeFilled = 0;
                        pEventNode->pLogEvent->i4BucketsFilled = 0;

                        if (pEventNode->pLogEvent->pLogBucket == NULL)
                        {

                            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                                      "Memory allocation failed for: Log Buckets. \n");

                            /*  Free the Log Table allocation also */
                            MemReleaseMemBlock (gRmonLogEventPoolId,
                                                (UINT1 *) pEventNode->
                                                pLogEvent);
                            pEventNode->pLogEvent = NULL;

                            /* Free the Memory allocated for Event Entry since associated
                             * Data table is not available */
                            /* Free the Event Community Store */
                            if (pEventNode->EventCommunity.pu1_OctetList !=
                                NULL)
                            {
                                MemReleaseMemBlock (gRmonEventCommPoolId,
                                                    (UINT1 *) pEventNode->
                                                    EventCommunity.
                                                    pu1_OctetList);
                                pEventNode->EventCommunity.pu1_OctetList = NULL;
                            }

                            MemReleaseMemBlock (gRmonEventPoolId,
                                                (UINT1 *) pEventNode);
                            pEventNode = NULL;
                            CLI_SET_ERR (CLI_RMON_EVENT_ENTRY_MEM_ALLOC_FAIL);
                            return SNMP_FAILURE;
                        }        /* Since Log Entry data store allocated separately */
                    }

                    /* Assign the Status */
                    *pStatus = i4SetValEventStatus;
                }                /* End of VALID */

                /* For Status == INVALID */
                if (i4SetValEventStatus == RMON_INVALID)
                {
                    if (pEventNode->EventCommunity.pu1_OctetList != NULL)
                    {
                        /* Free the Memory used for Event Table */
                        MemReleaseMemBlock (gRmonEventCommPoolId,
                                            (UINT1 *) pEventNode->
                                            EventCommunity.pu1_OctetList);
                        pEventNode->EventCommunity.pu1_OctetList = NULL;

                    }
                    /* Free the memory allocated for Event Table */

                    u4Hindex =
                        (UINT4) RmonHashFn ((UINT4) pEventNode->i4EventIndex);

                    RMON_HASH_DEL_NODE (gpRmonEventHashTable,
                                        &pEventNode->nextEventNode, u4Hindex);

                    MemReleaseMemBlock (gRmonEventPoolId, (UINT1 *) pEventNode);
                    pEventNode = NULL;
                }
                break;

            case RMON_CREATE_REQUEST:
            case RMON_MAX_STATE:
            default:
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "SNMP Failure: Entry is in Unknown State \n");
                CLI_SET_ERR (CLI_RMON_SET_EVENT);
                return SNMP_FAILURE;

        }                        /*  end of switch  */

    }                            /* End of if  */

    return SNMP_SUCCESS;
}

 /*
  * Low Level TEST Routines for Event Table
  */

/****************************************************************************
 Function    :  nmhTestv2EventDescription
 Input       :  The Indices
                i4EventIndex

                The Object
                i4TestValEventDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of r
fc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EventDescription (UINT4 *pu4ErrorCode, INT4 i4EventIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValEventDescription)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2EventDescription \n");

    if ((pTestValEventDescription->i4_Length < 0) ||
        (pTestValEventDescription->i4_Length > RMON_OCTET_LEN))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "\tERROR - Wrong length for EventDescription object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValEventDescription->pu1_OctetList,
                              pTestValEventDescription->i4_Length)
        == OSIX_FAILURE)
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "\tERROR - Invalid Characters for Display String object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RMON_NVT_CHAR_ERR);
        return SNMP_FAILURE;
    }
#endif

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry not in UNDER_CREATION State \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2EventType
 Input       :  The Indices
                i4EventIndex

                The Object
                i4TestValEventType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of r
fc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EventType (UINT4 *pu4ErrorCode, INT4 i4EventIndex, INT4
                    i4TestValEventType)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2EventType \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValEventType > 0) && (i4TestValEventType < 5))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        else
        {

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either Entry not in UNDER_CREATION State "
              "or Wrong EventType \n ");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2EventCommunity
 Input       :  The Indices
                i4EventIndex

                The Object
                pTestValEventCommunity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of r
fc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EventCommunity (UINT4 *pu4ErrorCode, INT4 i4EventIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValEventCommunity)
{
    tRmonEventNode     *pEventNode = NULL;
    tCommunityMappingEntry *pCommunity = NULL;

    UINT1               u1Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2EventCommunity \n");

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus == RMON_UNDER_CREATION)
        {

            if ((pTestValEventCommunity->i4_Length < 0) ||
                (pTestValEventCommunity->i4_Length > RMON_OCTET_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          "SNMP Failure: Invalid Community Name Length \n");
                return SNMP_FAILURE;
            }
            for (u1Index = 0; u1Index < pTestValEventCommunity->i4_Length;
                 u1Index++)
            {
                if (RMON_ISALNUM
                    (pTestValEventCommunity->pu1_OctetList[u1Index]) == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		    CLI_SET_ERR (CLI_RMON_INVALID_COMMUNITY);

                    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                              "SNMP Failure: Invalid Community Name\n");
                    return SNMP_FAILURE;
                }
            }
#ifdef SNMP_3_WANTED
            pCommunity = SNMPGetCommunityEntryFromName (pTestValEventCommunity);
#endif
            if (pCommunity == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_RMON_SNMP_COMMUNITY_ABSENT);
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          "SNMP Failure: Specified SNMP Community is not active\n");
                return SNMP_FAILURE;
            }
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Entry not in UNDER_CREATION State \n");
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry not available \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2EventOwner
 Input       :  The Indices
                i4EventIndex

                The Object
                pTestValEventOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of r
fc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EventOwner (UINT4 *pu4ErrorCode, INT4 i4EventIndex,
                     tSNMP_OCTET_STRING_TYPE * pTestValEventOwner)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2EventOwner \n");

    if ((pTestValEventOwner->i4_Length < 0) ||
        (pTestValEventOwner->i4_Length > RMON_OCTET_LEN))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "\tERROR - Wrong length for EventDescription object \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);

    if (pEventNode != NULL)
    {
        if (pEventNode->eventStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry not in UNDER CREATION State \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2EventStatus
 Input       :  The Indices
                i4EventIndex

                The Object
                i4TestValEventStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EventStatus (UINT4 *pu4ErrorCode, INT4 i4EventIndex,
                      INT4 i4TestValEventStatus)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2EventStatus \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RMON_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValEventStatus < RMON_VALID) ||
        (i4TestValEventStatus > RMON_INVALID))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid Event Status Value \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Check and allocate the memory for the     *
     * control entry of the Event Control table  */

    if ((i4EventIndex > 0) && (i4EventIndex <= RMON_MAX_EVENT_INDEX))
    {
        pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4EventIndex);
        if ((pEventNode == NULL)
            && (i4TestValEventStatus == RMON_CREATE_REQUEST))
        {
            /* this scenario will be taken care in Set routine */
            return SNMP_SUCCESS;
        }
        else if (pEventNode == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

        if (i4TestValEventStatus == RMON_VALID)
        {

            /* Check whether row filled or not */
            if (((pEventNode->eventType == SNMP_TRAP) ||
                 (pEventNode->eventType == LOG_AND_TRAP)) &&
                (pEventNode->EventCommunity.i4_Length == 0))
            {
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          "SNMP Failure: Event Community is null\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

        }

        switch (pEventNode->eventStatus)
        {
            case RMON_VALID:
                if (i4TestValEventStatus != RMON_CREATE_REQUEST)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
                break;

            case RMON_INVALID:
                if ((i4TestValEventStatus == RMON_CREATE_REQUEST) ||
                    (i4TestValEventStatus == RMON_INVALID))
                {

                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
                break;

            case RMON_UNDER_CREATION:
                if (i4TestValEventStatus != RMON_CREATE_REQUEST)
                {

                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
                break;

            case RMON_CREATE_REQUEST:    /* The status is automatically changed */
                break;            /* to UNDER_CREATION after setting the */
                /* default values */
            case RMON_MAX_STATE:
            default:
                break;

        }/*****  end of switch  *****/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Event Index out of range ");
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2EventTable
 Input       :  The Indices
                EventIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2EventTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
