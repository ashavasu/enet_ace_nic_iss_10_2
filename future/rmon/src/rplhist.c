/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rplhist.c,v 1.53 2017/01/17 14:10:30 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for History Group  *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rplhist.c
 *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.
 *
 *    SUBSYSTEM NAME         :  LOW LEVEL CODE
 *
 *    MODULE NAME            :  RMON PERIODIC
 *
 *    LANGUAGE               :  C
 *
 *    TARGET ENVIRONMENT     :  ANY
 *
 *    DATE OF FIRST RELEASE  :
 *
 *    DESCRIPTION            :  This provides Lowlevel routines to access 
 *                              variables of of History Table 
 *                              & EtherHistory Table
 *
 *  --------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |         DESCRIPTION OF
 *                  |        DATE          |             CHANGE
 * -----------------|----------------------|---------------------------------
 *      1.0         |  Vijay G. Krishnan   |         Create Original
 *                  |    15-APR-1997       |
-----------------------------------------------------------------------* 
 *     2.0          | Bhupendra            |Get ,Set and Test Routines
 *                  |                      | are modified Acoording to
 *                  |    16-AUG-2001       |        snmpv2        
---------------------------------------------------------------------
 *     3.0          | LEELA L              | Changes for DYNAMIC Tables
 *                  |                      | 
----------------------------------------------------------------------
 *     4.0          | LEELA L              | Changes for New Midgen Tool
 *                  | 07-JUL-2002          | 
---------------------------------------------------------------------*/

#include "rmallinc.h"

#include "cli.h"
#include "rmoncli.h"
#include "rmgr.h"

#define COMP_IDX(a, b, x, y)         (((a) == (x)) ? ((b) - (y)) : ((a) - (x)))

#define IN_BETWEEN(a, b, x, y, p, q) ((COMP_IDX(a, b, p, q) < 0) && \
                                      (COMP_IDX(x, y, p, q) > 0))
extern UINT4        HistoryControlStatus[11];
extern UINT4        HistoryControlOwner[11];
extern UINT4        HistoryControlDataSource[11];
/*
 * LOW LEVEL Routines for Table : historyControlTable.
 */

/*
 * GET_EXACT Validate Index Instance Routine.
 */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceHistoryControlTable
 Input       :  The Indices
                HistoryControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceHistoryControlTable (INT4 i4HistoryControlIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhValidateIndexInstanceHistoryControlTable\n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HistoryControlIndex >= 1) &&
        (i4HistoryControlIndex <= RMON_MAX_HISTORY_CONTROL_INDEX) &&
        (HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                       i4HistoryControlIndex) != NULL))
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "HistoryControlIndex is valid and entry exists. \n");
        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HistoryControlIndex \n");
        return SNMP_FAILURE;
    }
}

/*
 * GET_FIRST Routine.
 */

/****************************************************************************
 Function    :  nmhGetFirstIndexHistoryControlTable
 Input       :  The Indices
                pi4HistoryControlIndex 
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexHistoryControlTable (INT4 *pi4HistoryControlIndex)
{
    INT4                i4Zero = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetFirstIndexHistoryControlTable\n");

    return nmhGetNextIndexHistoryControlTable (i4Zero, pi4HistoryControlIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexHistoryControlTable
 Input       :  The Indices
                HistoryControlIndex
                nextHistoryControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexHistoryControlTable (INT4 i4HistoryControlIndex,
                                    INT4 *pi4NextHistoryControlIndex)
{
    INT4                i4Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetNextIndexHistoryControlTable \n");

    if ((i4HistoryControlIndex < RMON_ZERO) ||
        (i4HistoryControlIndex > RMON_MAX_HISTORY_CONTROL_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid Index - out of range\n");
        return SNMP_FAILURE;
    }

    i4Index =
        HashSearchHistoryControlNode_NextValidTindex
        (gpRmonHistoryControlHashTable, i4HistoryControlIndex);

    if ((i4Index != RMON_ZERO)
        && (i4Index > i4HistoryControlIndex)
        && (i4Index <= RMON_MAX_HISTORY_CONTROL_INDEX))
    {
        *pi4NextHistoryControlIndex = i4Index;
        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: NO Next VALID Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level GET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhGetHistoryControlDataSource
 Input       :  The Indices
                HistoryControlIndex

                The Object
                retValHistoryControlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHistoryControlDataSource (INT4 i4HistoryControlIndex,
                                tSNMP_OID_TYPE *
                                pRetValHistoryControlDataSource)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHistoryControlDataSource\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {
            /* The RmonFormDataSourceOid function forms the OID and
             * returns the pointer to it 
             */
            RmonFormDataSourceOid (pRetValHistoryControlDataSource,
                                   pHistoryControlNode->
                                   u4HistoryControlDataSource,
                                   pHistoryControlNode->u4HisCntrlOIDType);

            RMON_DBG (MINOR_TRC | FUNC_EXIT,
                      " Leaving function nmhGetHistoryControlDataSource\n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " INVALID - HistoryControl Status \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHistoryControlBucketsRequested
 Input       :  The Indices
                HistoryControlIndex

                The Object
                retValHistoryControlBucketsRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHistoryControlBucketsRequested (INT4 i4HistoryControlIndex,
                                      INT4
                                      *pi4RetValHistoryControlBucketsRequested)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHistoryControlBucketsRequested\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {

            *pi4RetValHistoryControlBucketsRequested =
                pHistoryControlNode->i4HistoryControlBucketsRequested;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " INVALID - HistoryControl Status \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHistoryControlBucketsGranted
 Input       :  The Indices
                HistoryControlIndex

                The Object
                retValHistoryControlBucketsGranted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHistoryControlBucketsGranted (INT4 i4HistoryControlIndex,
                                    INT4 *pi4RetValHistoryControlBucketsGranted)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHistoryControlBucketsGranted\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {
            *pi4RetValHistoryControlBucketsGranted =
                pHistoryControlNode->i4HistoryControlBucketsGranted;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " INVALID - HistoryControl Status \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHistoryControlInterval
 Input       :  The Indices
                HistoryControlIndex

                The Object
                retValHistoryControlInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHistoryControlInterval (INT4 i4HistoryControlIndex,
                              INT4 *pi4RetValHistoryControlInterval)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHistoryControlInterval\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {
            *pi4RetValHistoryControlInterval =
                pHistoryControlNode->i4HistoryControlInterval;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " INVALID - HistoryControl Status \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHistoryControlOwner
 Input       :  The Indices
                HistoryControlIndex

                The Object
                retValHistoryControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHistoryControlOwner (INT4 i4HistoryControlIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValHistoryControlOwner)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHistoryControlOwner\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {
            STRNCPY (pRetValHistoryControlOwner->pu1_OctetList,
                     pHistoryControlNode->u1HistoryControlOwner,
                     STRLEN (pHistoryControlNode->u1HistoryControlOwner));

            pRetValHistoryControlOwner->i4_Length =
                (INT4) STRLEN (pHistoryControlNode->u1HistoryControlOwner);

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " INVALID - HistoryControl Status \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHistoryControlStatus
 Input       :  The Indices
                HistoryControlIndex

                The Object
                retValHistoryControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHistoryControlStatus (INT4 i4HistoryControlIndex,
                            INT4 *pi4RetValHistoryControlStatus)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHistoryControlStatus\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus != RMON_INVALID)
        {
            *pi4RetValHistoryControlStatus =
                pHistoryControlNode->historyControlStatus;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " INVALID - HistoryControl Status \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhSetHistoryControlDataSource
 Input       :  The Indices
                HistoryControlIndex

                The Object
                setValHistoryControlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHistoryControlDataSource (INT4 i4HistoryControlIndex,
                                tSNMP_OID_TYPE *
                                pSetValHistoryControlDataSource)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    UINT4               u4SeqNum = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhSetHistoryControlDataSource\n");
    
    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    /* The RmonSetDataSourceOid function/macro will convert OID to UINT4
     * and set the value 
     */

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        RmonSetDataSourceOid (pSetValHistoryControlDataSource,
                              &(pHistoryControlNode->
                                u4HistoryControlDataSource),
                              &(pHistoryControlNode->u4HisCntrlOIDType));

        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.pu4ObjectId = HistoryControlDataSource;
        SnmpNotifyInfo.u4OidLen = sizeof (HistoryControlDataSource) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = RmonLock;
        SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %o", i4HistoryControlIndex,
                          pSetValHistoryControlDataSource));

        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  " Leaving the function  nmhSetHistoryControlDataSource\n");

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " nmhSetHistoryControlDataSource Failed \n");

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function  nmhSetHistoryControlDataSource\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetHistoryControlBucketsRequested
 Input       :  The Indices
                HistoryControlIndex

                The Object
                setValHistoryControlBucketsRequested
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHistoryControlBucketsRequested (INT4 i4HistoryControlIndex,
                                      INT4
                                      i4SetValHistoryControlBucketsRequested)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHistoryControlBucketsRequested \n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode == NULL)
    {
        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  " nmhSetHistoryControlBucketsRequested Failed \n");

        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  " Leaving the function nmhSetHistoryControlBucketsRequested \n");

        return SNMP_FAILURE;
    }

    pHistoryControlNode->i4HistoryControlBucketsRequested =
        i4SetValHistoryControlBucketsRequested;

    if (pHistoryControlNode->i4HistoryControlBucketsRequested
        < MAX_HISTORY_BUCKETS)
    {
        pHistoryControlNode->i4HistoryControlBucketsGranted =
            pHistoryControlNode->i4HistoryControlBucketsRequested;
    }
    else
    {
        pHistoryControlNode->i4HistoryControlBucketsGranted =
            MAX_HISTORY_BUCKETS;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHistoryControlBucketsRequested \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHistoryControlInterval
 Input       :  The Indices
                HistoryControlIndex

                The Object
                setValHistoryControlInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHistoryControlInterval (INT4 i4HistoryControlIndex,
                              INT4 i4SetValHistoryControlInterval)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHistoryControlInterval\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        pHistoryControlNode->i4HistoryControlInterval
            = i4SetValHistoryControlInterval;

        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  " Leaving the function nmhSetHistoryControlInterval\n");

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, " nmhSetHistoryControlInterval Failed \n");

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function  nmhSetHistoryControlInterval\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetHistoryControlOwner
 Input       :  The Indices
                HistoryControlIndex

                The Object
                setValHistoryControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHistoryControlOwner (INT4 i4HistoryControlIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValHistoryControlOwner)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4Minimum;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHistoryControlOwner\n");

    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        u4Minimum =
            (UINT4) ((pSetValHistoryControlOwner->i4_Length <
                      OWN_STR_LENGTH) ? pSetValHistoryControlOwner->
                     i4_Length : OWN_STR_LENGTH);

        MEMSET (pHistoryControlNode->u1HistoryControlOwner, 0, OWN_STR_LENGTH);

        STRNCPY (pHistoryControlNode->u1HistoryControlOwner,
                 pSetValHistoryControlOwner->pu1_OctetList, u4Minimum);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = HistoryControlOwner;
    SnmpNotifyInfo.u4OidLen = sizeof (HistoryControlOwner) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4HistoryControlIndex,
                      pSetValHistoryControlOwner));
 
        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  " Leaving the function nmhSetHistoryControlOwner\n");

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, " nmhSetHistoryControlOwner Failed \n");

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function  nmhSetHistoryControlOwner\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetHistoryControlStatus
 Input       :  The Indices
                HistoryControlIndex

                The Object
                setValHistoryControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHistoryControlStatus (INT4 i4HistoryControlIndex,
                            INT4 i4SetValHistoryControlStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT4               *pBktGranted = NULL;
    tRmonStatus        *pStatus = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    UINT4               u4Hindex;
    UINT4               u4SeqNum = 0;
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHistoryControlStatus\n");
    
    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    if ((i4HistoryControlIndex <= RMON_ZERO) ||
        (i4HistoryControlIndex > RMON_MAX_HISTORY_CONTROL_INDEX))
    {
        return SNMP_FAILURE;
    }

    /*Check and allocate the memory for the *
     * control entry of the History Control table     */

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if ((pHistoryControlNode == NULL) &&
        (i4SetValHistoryControlStatus == RMON_CREATE_REQUEST))
    {
        pHistoryControlNode =
            (tRmonHistoryControlNode *) MemAllocMemBlk (gRmonHistoryCtrlPoolId);

        if (pHistoryControlNode == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " Allocation  Failure for:  HistoryControlTable \n");
            CLI_SET_ERR (CLI_RMON_MAX_HISTORY);
            return SNMP_FAILURE;
        }

        MEMSET (&pHistoryControlNode->pHistoryEntry, 0,
                sizeof (tEtherHistoryEntry));
        MEMSET (&pHistoryControlNode->u1HistoryControlOwner, 0, OWN_STR_LENGTH);

        pHistoryControlNode->i4HistoryControlIndex = i4HistoryControlIndex;
        pHistoryControlNode->i4HistoryControlBucketsRequested = 0;
        pHistoryControlNode->i4HistoryControlBucketsGranted = 0;
        pHistoryControlNode->i4HistoryControlInterval = 0;
        pHistoryControlNode->u4HistoryControlDataSource = 0;
        pHistoryControlNode->u4HistoryControlDroppedFrames = 0;
        pHistoryControlNode->u4HistoryMinSampleIndex = 0;
        pHistoryControlNode->u4HisCntrlOIDType = 0;
        pHistoryControlNode->u2TimeToMonitor = 0;

        /* Set the default status of the entry to INVALID */
        pHistoryControlNode->historyControlStatus = RMON_INVALID;

        u4Hindex =
            (UINT4) RmonHashFn ((UINT4) pHistoryControlNode->
                                i4HistoryControlIndex);

        RMON_HASH_ADD_NODE (gpRmonHistoryControlHashTable,
                            &pHistoryControlNode->nextHistoryControlEntryNode,
                            u4Hindex, NULL);
    }
    else if (pHistoryControlNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pBktGranted = &(pHistoryControlNode->i4HistoryControlBucketsGranted);
    pStatus = &(pHistoryControlNode->historyControlStatus);

    switch (*pStatus)
    {
        case RMON_VALID:
            if (i4SetValHistoryControlStatus == RMON_INVALID)
            {
                /* Memory is freed here */
                /* Need to free the memory for data table first */
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "VALID to INVALID \n");

		MemReleaseMemBlock (gRmonHisBucketsPoolId, (UINT1 *)
				(pHistoryControlNode->pHistoryEntry->
				 pHistoryBucket));
		pHistoryControlNode->pHistoryEntry->pHistoryBucket = NULL;

                /* Need to free the memory for the data table - History Table */

		    MemReleaseMemBlock (gRmonEtherHisPoolId, (UINT1 *)
					(pHistoryControlNode->pHistoryEntry));
		    pHistoryControlNode->pHistoryEntry = NULL;

                /* Free the memory for History Control Entry */
                u4Hindex =
                    (UINT4) RmonHashFn ((UINT4) pHistoryControlNode->
                                        i4HistoryControlIndex);

                /* Stop the Timer First */
                RmonStopTimer (HISTORY_CONTROL_TABLE, i4HistoryControlIndex);

                RMON_HASH_DEL_NODE (gpRmonHistoryControlHashTable,
                                    &pHistoryControlNode->
                                    nextHistoryControlEntryNode, u4Hindex);

                MemReleaseMemBlock (gRmonHistoryCtrlPoolId,
                                    (UINT1 *) pHistoryControlNode);
                pHistoryControlNode = NULL;
            }
            else if (i4SetValHistoryControlStatus == RMON_UNDER_CREATION)
            {
                /* Memory is freed here */
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "VALID to UNDERCREATION \n");

                if ((pHistoryControlNode->
                     pHistoryEntry != NULL) &&
                    (pHistoryControlNode->
                     pHistoryEntry->pHistoryBucket != NULL))
                {
                    MemReleaseMemBlock (gRmonHisBucketsPoolId, (UINT1 *)
                                        (pHistoryControlNode->pHistoryEntry->
                                         pHistoryBucket));
                    pHistoryControlNode->pHistoryEntry->pHistoryBucket = NULL;

                    /* Free the History Table entry  */
                    MemReleaseMemBlock (gRmonEtherHisPoolId, (UINT1 *)
                                        (pHistoryControlNode->pHistoryEntry));
                    pHistoryControlNode->pHistoryEntry = NULL;

                }

                /* Reset the Minimum Sample Index */
                pHistoryControlNode->u4HistoryMinSampleIndex = 0;

                /*  Assign the Status & start timer to delete entries 
                 *  not made valid in given time */
                RmonStartTimer (HISTORY_CONTROL_TABLE, i4HistoryControlIndex,
                                HIST_CTRL_DURATION);
                *pStatus = i4SetValHistoryControlStatus;
            }
            break;

        case RMON_INVALID:
            *pStatus = i4SetValHistoryControlStatus;
            if (i4SetValHistoryControlStatus == RMON_CREATE_REQUEST)
            {

                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "INVALID to CREATE_REQUEST \n");
                /* Setting all default values here */

                pHistoryControlNode->
                    i4HistoryControlBucketsRequested = MAX_HISTORY_BUCKETS;

                *pBktGranted = MAX_HISTORY_BUCKETS;

                /* 1800 secs is the default history control interval set. */
                pHistoryControlNode->
                    i4HistoryControlInterval = RMON_HISTORY_CTRL_INTERVAL;

                *pStatus = RMON_UNDER_CREATION;

                /* Starting timer to delete entries not made valid in 
                   given time */
                RmonStartTimer (HISTORY_CONTROL_TABLE,
                                i4HistoryControlIndex, HIST_CTRL_DURATION);
            }
            else if (i4SetValHistoryControlStatus == RMON_INVALID)
            {
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "INVALID to INVALID \n");
                return SNMP_SUCCESS;
            }
            break;

        case RMON_UNDER_CREATION:
            if (i4SetValHistoryControlStatus == RMON_VALID)
            {
                if (STRLEN (pHistoryControlNode->u1HistoryControlOwner) == 0)
                {
                    /* Create default owner */
                    STRNCPY (&pHistoryControlNode->u1HistoryControlOwner, MONITOR, STRLEN (MONITOR));
                }

                /* Stop the Timer First */
                RmonStopTimer (HISTORY_CONTROL_TABLE, i4HistoryControlIndex);

                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "UNDER_CREATION to VALID \n");

                /* Check whether a DataSource field is set or NOT. */
                if (pHistoryControlNode->u4HistoryControlDataSource == 0)
                {
                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              " History Control Data Source NOT SET.\n");

                    return SNMP_FAILURE;
                }

                if (pHistoryControlNode->pHistoryEntry == NULL)
                {
                    /* Allocate Memory for History Table Entry 
                     * Initialising gaEtherHistoryTable to NULL */

                    pHistoryControlNode->pHistoryEntry =
                        MemAllocMemBlk (gRmonEtherHisPoolId);

                    if (pHistoryControlNode->pHistoryEntry == NULL)
                    {
                        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                                  " Memory Allocation Failure for:  HistoryTable \n");
                        return SNMP_FAILURE;
                    }

                    MEMSET (pHistoryControlNode->pHistoryEntry, 0,
                            sizeof (tEtherHistoryEntry));

                    /* Initialize the Buckets Allocated field */
                    pHistoryControlNode->pHistoryEntry->
                        i4BucketsAllocated = (*pBktGranted + 1);

                }                /* End of NULL */

                /* Allocating Memory for Buckets */
                /* 
                 * The no: of buckets allocated is one more than no: 
                 *  of buckets  granted, since the zeroth one is not used
                 */

                pHistoryControlNode->pHistoryEntry->
                    pHistoryBucket = MemAllocMemBlk (gRmonHisBucketsPoolId);

                /*
                 * Zeroth bucket is not used. So no: of buckets should be
                 * buckets granted plus one.
                 */
                if (pHistoryControlNode->pHistoryEntry->pHistoryBucket == NULL)
                {

                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              " Memory Allocation  Failure for:  HistoryBuckets \n");
                    MemReleaseMemBlock (gRmonEtherHisPoolId,
                                        (UINT1 *) pHistoryControlNode->
                                        pHistoryEntry);
#ifdef RMON2_WANTED
                    pHistoryControlNode->u4HistoryControlDroppedFrames++;
#endif
                    return SNMP_FAILURE;
                }

                RMON_BZERO ((pHistoryControlNode->
                             pHistoryEntry->pHistoryBucket),
                            sizeof (tEtherHistorySample) *
                            (MAX_HISTORY_BUCKETS + 1));

                /* Assign the Status */
                *pStatus = i4SetValHistoryControlStatus;

                /* Set the BOOL to indicate one single VALID Entry. */
                grmprB1AnyValidHistory = TRUE;
            }
            else if (i4SetValHistoryControlStatus == RMON_INVALID)
            {
                /* If status is INVALID, stop the timer first. */
                RmonStopTimer (HISTORY_CONTROL_TABLE, i4HistoryControlIndex);

                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "UNDER_CREATION to INVALID \n");

                /* Free the Memory for Control Entry */
                u4Hindex =
                    (UINT4) RmonHashFn ((UINT4) pHistoryControlNode->
                                        i4HistoryControlIndex);

                RMON_HASH_DEL_NODE (gpRmonHistoryControlHashTable,
                                    &pHistoryControlNode->
                                    nextHistoryControlEntryNode, u4Hindex);

                MemReleaseMemBlock (gRmonHistoryCtrlPoolId,
                                    (UINT1 *) pHistoryControlNode);
                pHistoryControlNode = NULL;
            }
            break;

        case RMON_CREATE_REQUEST:
        case RMON_MAX_STATE:
        default:
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Unknown Entry State \n");
            return SNMP_FAILURE;
    }                            /*  end of switch  */

    if(i4SetValHistoryControlStatus == RMON_INVALID)
    {
        i4SetValHistoryControlStatus = DESTROY;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = HistoryControlStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (HistoryControlStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4HistoryControlIndex,
                      i4SetValHistoryControlStatus));

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHistoryControlStatus\n");
    return SNMP_SUCCESS;

}

 /*
  * Low Level TEST Routines for HistoryControlTable
  */

/****************************************************************************
 Function    :  nmhTestv2HistoryControlDataSource
 Input       :  The Indices
                HistoryControlIndex

                The Object
                testValHistoryControlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HistoryControlDataSource (UINT4 *pu4ErrorCode,
                                   INT4 i4HistoryControlIndex,
                                   tSNMP_OID_TYPE *
                                   pTestValHistoryControlDataSource)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HistoryControlDataSource\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_UNDER_CREATION)
        {
            if (RmonTestDataSourceOid (pTestValHistoryControlDataSource) ==
                TRUE)
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, " Invalid DataSource \n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RMON_INVALID_OID);
            return SNMP_FAILURE;
        }

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " History control status is not UNDER_CREATION \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              " Invalid History control Index \n");
    return SNMP_FAILURE;
}

/*********************************************************************
*******
 Function    :  nmhTestv2HistoryControlBucketsRequested
 Input       :  The Indices
                i4HistoryControlIndex 

                The Object
                i4TestValHistoryControlBucketsRequested
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc 1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of 
                rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**************************************************************************/

INT1
nmhTestv2HistoryControlBucketsRequested (UINT4 *pu4ErrorCode,
                                         INT4 i4HistoryControlIndex,
                                         INT4
                                         i4TestValHistoryControlBucketsRequested)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HistoryControlBucketsRequested\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValHistoryControlBucketsRequested > 0) &&
                (i4TestValHistoryControlBucketsRequested < 65536))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      "History control Status is not UNDER_CREATION \n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid History control Index\n");
    return SNMP_FAILURE;
}

/*********************************************************************
*******
 Function    :  nmhTestv2HistoryControlInterval
 Input       :  The Indices
                i4HistoryControlIndex

                The Object
                i4TestValHistoryControlInterval 
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of r
fc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**********************************************************************
******/

INT1
nmhTestv2HistoryControlInterval (UINT4 *pu4ErrorCode,
                                 INT4 i4HistoryControlIndex,
                                 INT4 i4TestValHistoryControlInterval)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HistoryControlInterval \n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValHistoryControlInterval > 0) &&
                (i4TestValHistoryControlInterval <= 3600))
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      " History control status is not UNDER_CREATION \n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              " Invalid History control Index \n");
    return SNMP_FAILURE;
}

/*********************************************************************
 Function    :  nmhTestv2HistoryControlOwner
 Input       :  The Indices
                i4HistoryControlIndex

                The Object
                pTestValHistoryControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
                (rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
************************************************************************/

INT1
nmhTestv2HistoryControlOwner (UINT4 *pu4ErrorCode,
                              INT4 i4HistoryControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValHistoryControlOwner)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HistoryControlOwner\n");

    if ((pTestValHistoryControlOwner->i4_Length < 0) ||
        (pTestValHistoryControlOwner->i4_Length > RMON_OCTET_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Status is not UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/*************************************************************************
 Function    :  nmhTestv2HistoryControlStatus
 Input       :  The Indices
                i4HistoryControlIndex

                The Object
                i4TestValHistoryControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
                1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of 
                 rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
************************************************************************/

INT1
nmhTestv2HistoryControlStatus (UINT4 *pu4ErrorCode,
                               INT4 i4HistoryControlIndex,
                               INT4 i4TestValHistoryControlStatus)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tRmonHistoryControlNode *pNextHistoryControlNode = NULL;
    INT4		    i4NextHistoryControlIndex = 0;
    INT4		    i4HistoryCtrlIndex = 0;
    INT4		    i4Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HistoryControlStatus \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RMON_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4HistoryControlIndex <= RMON_ZERO) ||
        (i4HistoryControlIndex > RMON_MAX_HISTORY_CONTROL_INDEX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: HistoryControl Index out of range\n");
        return SNMP_FAILURE;
    }

    /*Check and allocate the memory for the *
     * control entry of the History Control table     */

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4HistoryControlIndex);
	
    i4Index =
        HashSearchHistoryControlNode_NextValidTindex
        (gpRmonHistoryControlHashTable, i4NextHistoryControlIndex);

    if ((pHistoryControlNode != NULL) &&
	(pHistoryControlNode->historyControlStatus == RMON_UNDER_CREATION) && 
	(i4TestValHistoryControlStatus == RMON_VALID))
    {
        do
        {
            pNextHistoryControlNode =
                    HashSearchHistoryControlNode (gpRmonHistoryControlHashTable, i4Index);

            i4HistoryCtrlIndex = i4Index;

            if ((pNextHistoryControlNode != NULL) &&
                            (pNextHistoryControlNode->historyControlStatus == RMON_VALID))
            {
                if ((pNextHistoryControlNode->u4HistoryControlDataSource == pHistoryControlNode->u4HistoryControlDataSource)
		   && (STRLEN (pNextHistoryControlNode->u1HistoryControlOwner) == 
			STRLEN (pHistoryControlNode->u1HistoryControlOwner)))
                {

                    if ((pNextHistoryControlNode->i4HistoryControlInterval == pHistoryControlNode->i4HistoryControlInterval)
                        && (pNextHistoryControlNode->i4HistoryControlBucketsRequested ==
                        pHistoryControlNode->i4HistoryControlBucketsRequested) && (MEMCMP (pNextHistoryControlNode->u1HistoryControlOwner,
                        pHistoryControlNode->u1HistoryControlOwner, STRLEN (pHistoryControlNode->u1HistoryControlOwner)) == 0))
                    {
                        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                        CLI_SET_ERR (CLI_RMON_HISTORY_DUPLICATE_ENTRY_EXISTS_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
        }
        while ((i4Index = HashSearchHistoryControlNode_NextValidTindex (gpRmonHistoryControlHashTable, i4HistoryCtrlIndex))
                && (i4Index > i4HistoryCtrlIndex) && (i4Index <= RMON_MAX_HISTORY_CONTROL_INDEX));
     }
			
    if ((pHistoryControlNode == NULL) &&
        (i4TestValHistoryControlStatus == RMON_CREATE_REQUEST))
    {
        /* this scenario will be taken care in Set routine */
        return SNMP_SUCCESS;
    }
    else if (pHistoryControlNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValHistoryControlStatus == RMON_VALID)
    {

        /* Checking whether row is filled or not */
        if (pHistoryControlNode->u4HistoryControlDataSource == 0)
        {
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      " Invalid History control Index \n");

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValHistoryControlStatus < RMON_VALID) ||
        (i4TestValHistoryControlStatus > RMON_INVALID))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Invalid History control Status \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (pHistoryControlNode->historyControlStatus)
    {
        case RMON_VALID:
            if (i4TestValHistoryControlStatus != RMON_CREATE_REQUEST)
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            break;

        case RMON_INVALID:
            if ((i4TestValHistoryControlStatus == RMON_CREATE_REQUEST) ||
                (i4TestValHistoryControlStatus == RMON_INVALID))
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            break;

        case RMON_UNDER_CREATION:
            if (i4TestValHistoryControlStatus != RMON_CREATE_REQUEST)
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            break;

        case RMON_CREATE_REQUEST:    /* The status is automatically changed */
            break;                /* to UNDER_CREATION after setting the */
            /* default values.                     */
        case RMON_MAX_STATE:
        default:
            break;

    }                            /*  end of switch  */

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              " Invalid History control Status\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/*
 * LOW LEVEL Routines for Table : etherHistoryTable.
 */

/*
 * GET_EXACT Validate Index Instance Routine.
 */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEtherHistoryTable
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceEtherHistoryTable (INT4 i4EtherHistoryIndex,
                                           INT4 i4EtherHistorySampleIndex)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhValidateIndexInstanceEtherHistoryTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if ((pHistoryControlNode != NULL)
        && ((UINT4) i4EtherHistorySampleIndex >=
            pHistoryControlNode->u4HistoryMinSampleIndex)
        && ((UINT4) i4EtherHistorySampleIndex <
            (pHistoryControlNode->u4HistoryMinSampleIndex +
             (UINT4) pHistoryControlNode->i4HistoryControlBucketsGranted)))

    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: Valid History Indices.\n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Invalid Ether History Sample Index\n");
        return SNMP_FAILURE;
    }
}

/*
 * GET_FIRST Routine.
 */

/****************************************************************************
 Function    :  nmhGetFirstIndexEtherHistoryTable
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexEtherHistoryTable (INT4 *pi4EtherHistoryIndex,
                                   INT4 *pi4EtherHistorySampleIndex)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetFirstIndexEtherHistoryTable\n");

    *pi4EtherHistoryIndex = 0;

    if (nmhGetFirstIndexHistoryControlTable (pi4EtherHistoryIndex)
        != SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
        return SNMP_FAILURE;
    }

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      *pi4EtherHistoryIndex);

    if (pHistoryControlNode == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  " Invalid History control Index \n");
        return SNMP_FAILURE;
    }

    if (pHistoryControlNode->u4HistoryMinSampleIndex == 0)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Al the buckets Corresponding"
                  " to the History index is empty \n");
    }
    *pi4EtherHistorySampleIndex =
        (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhGetFirstIndexEtherHistoryTable\n");

    return SNMP_SUCCESS;
}

 /*
  * GET_NEXT Routine.
  */

/****************************************************************************
 Function    :  nmhGetNextIndexEtherHistoryTable
 Input       :  The Indices
                EtherHistoryIndex
                nextEtherHistoryIndex
                EtherHistorySampleIndex
                nextEtherHistorySampleIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexEtherHistoryTable (INT4 i4EtherHistoryIndex,
                                  INT4 *pi4NextEtherHistoryIndex,
                                  INT4 i4EtherHistorySampleIndex,
                                  INT4 *pi4NextEtherHistorySampleIndex)
{
    INT4                i4NextHistoryIndex = 1;
    INT4                i4BucketIndex = 1;
    INT1                bNewHistoryIndex = FALSE;

    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tEtherHistorySample *pNewBucket = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetNextIndexEtherHistoryTable\n");

    if (i4EtherHistoryIndex < 0)
    {
        return SNMP_FAILURE;
    }

    i4NextHistoryIndex = i4EtherHistoryIndex;

    do
    {
        pHistoryControlNode =
            HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                          i4NextHistoryIndex);

        if ((pHistoryControlNode != NULL) &&
            (pHistoryControlNode->historyControlStatus == RMON_VALID)) 
        {
            /* we check if the next sample is present in the
             * corresponding bucket of the history index and if it is
             * not present we take the next history index and the minimum
             * sample index for that history index
             * */

            /*For Every new history index, check buckets are filled with samples */
            if (bNewHistoryIndex == TRUE)
            {
                /*If there is no sample, show only History table information
                 * and do not show any statistical information*/
                if (pHistoryControlNode->u4HistoryMinSampleIndex == 0)
                {
                    *pi4NextEtherHistoryIndex = i4NextHistoryIndex;
                    *pi4NextEtherHistorySampleIndex = 0;
                    return SNMP_SUCCESS;
                }
            }

            if (i4NextHistoryIndex == i4EtherHistoryIndex)
            {
                /* When input sample index is zero and current control node min sample
                   index non-zero, return with current start index for the table */
                if ((i4EtherHistorySampleIndex == 0)
                    && (pHistoryControlNode->u4HistoryMinSampleIndex != 0))
                {
                    *pi4NextEtherHistoryIndex = i4NextHistoryIndex;
                    *pi4NextEtherHistorySampleIndex =
                        (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;
                    return SNMP_SUCCESS;
                }

                /* For the same history index we get the index of the
                 * bucket that should contain the next sample index*/
                i4BucketIndex =
                    ((i4EtherHistorySampleIndex + 1) %
                     pHistoryControlNode->i4HistoryControlBucketsGranted);

            }
            else
            {
                /* If next sample doesnt exist in the history bucket,we obtain
                 * the bucket index for the minimum sample index of next history
                 * index
                 * */

                i4BucketIndex = ((INT4) (pHistoryControlNode->
                                         u4HistoryMinSampleIndex) %
                                 pHistoryControlNode->
                                 i4HistoryControlBucketsGranted);

            }
            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pNewBucket = (pHistoryControlNode->pHistoryEntry->
                          pHistoryBucket + i4BucketIndex);

            if (pNewBucket != NULL)
            {
                if (pNewBucket->etherHistoryIntervalStart != 0)
                {
                    *pi4NextEtherHistoryIndex = i4NextHistoryIndex;

                    if (i4NextHistoryIndex == i4EtherHistoryIndex)
                    {
                        if ((i4EtherHistorySampleIndex + 1) <
                            (INT4) (pHistoryControlNode->u4HistoryMinSampleIndex
                                    + (UINT4) (pHistoryControlNode->
                                               i4HistoryControlBucketsGranted)))
                        {
                            *pi4NextEtherHistorySampleIndex =
                                pNewBucket->i4SampleIndex;
                            return SNMP_SUCCESS;
                        }
                    }
                    else
                    {

                        *pi4NextEtherHistorySampleIndex =
                            (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
        else if ((bNewHistoryIndex == TRUE) &&
                  (pHistoryControlNode != NULL) &&
                  (pHistoryControlNode->historyControlStatus == RMON_UNDER_CREATION))
        {
                 *pi4NextEtherHistoryIndex = i4NextHistoryIndex;
                  *pi4NextEtherHistorySampleIndex =
                                (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;
                  return SNMP_SUCCESS;
        } 
        bNewHistoryIndex = TRUE;

    }
    while (nmhGetNextIndexHistoryControlTable (i4NextHistoryIndex,
                                               &i4NextHistoryIndex) ==
           SNMP_SUCCESS);

    /**** $$TRACE_LOG (EXIT,"SNMP Failure\n"); ****/
    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              " Invalid Ether History Index fn: nmhGetNextIndexHistoryControlTable \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level GET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhGetEtherHistoryIntervalStart
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryIntervalStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryIntervalStart (INT4 i4EtherHistoryIndex,
                                 INT4 i4EtherHistorySampleIndex,
                                 UINT4 *pu4RetValEtherHistoryIntervalStart)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhGetEtherHistoryIntervalStart\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);
            if (i4BucketIndex == 0)
            {
                /* The bucket indices range from 1 to i4HistoryControlBucketsGranted
                 * So i4BucketIndex = 0 indicates the bucket with index as 
                 * i4HistoryControlBucketsGranted
                 */
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryIntervalStart =
                    pSampleBucket->etherHistoryIntervalStart;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryDropEvents
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryDropEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryDropEvents (INT4 i4EtherHistoryIndex,
                              INT4 i4EtherHistorySampleIndex,
                              UINT4 *pu4RetValEtherHistoryDropEvents)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryDropEvents\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);
            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryDropEvents =
                    pSampleBucket->u4EtherHistoryDropEvents;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryOctets
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/

INT1
nmhGetEtherHistoryOctets (INT4 i4EtherHistoryIndex,
                          INT4 i4EtherHistorySampleIndex,
                          UINT4 *pu4RetValEtherHistoryOctets)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryOctets\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);
            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryOctets =
                    pSampleBucket->u4EtherHistoryOctets;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetEtherHistoryPkts
Input       :  The Indices
EtherHistoryIndex
EtherHistorySampleIndex

                The Object
                retValEtherHistoryPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryPkts (INT4 i4EtherHistoryIndex,
                        INT4 i4EtherHistorySampleIndex,
                        UINT4 *pu4RetValEtherHistoryPkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhGetEtherHistoryPkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryPkts = pSampleBucket->u4EtherHistoryPkts;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryBroadcastPkts
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryBroadcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryBroadcastPkts (INT4 i4EtherHistoryIndex,
                                 INT4 i4EtherHistorySampleIndex,
                                 UINT4 *pu4RetValEtherHistoryBroadcastPkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryBroadcastPkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryBroadcastPkts =
                    pSampleBucket->u4EtherHistoryBroadcastPkts;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryMulticastPkts
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryMulticastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryMulticastPkts (INT4 i4EtherHistoryIndex,
                                 INT4 i4EtherHistorySampleIndex,
                                 UINT4 *pu4RetValEtherHistoryMulticastPkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhGetEtherHistoryMulticastPkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryMulticastPkts =
                    pSampleBucket->u4EtherHistoryMulticastPkts;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryCRCAlignErrors
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryCRCAlignErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryCRCAlignErrors (INT4 i4EtherHistoryIndex,
                                  INT4 i4EtherHistorySampleIndex,
                                  UINT4 *pu4RetValEtherHistoryCRCAlignErrors)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryCRCAlignErrors\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryCRCAlignErrors =
                    pSampleBucket->u4EtherHistoryCRCAlignErrors;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryUndersizePkts
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryUndersizePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryUndersizePkts (INT4 i4EtherHistoryIndex,
                                 INT4 i4EtherHistorySampleIndex,
                                 UINT4 *pu4RetValEtherHistoryUndersizePkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryUndersizePkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryUndersizePkts =
                    pSampleBucket->u4EtherHistoryUndersizePkts;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryOversizePkts
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryOversizePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryOversizePkts (INT4 i4EtherHistoryIndex,
                                INT4 i4EtherHistorySampleIndex,
                                UINT4 *pu4RetValEtherHistoryOversizePkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryOversizePkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryOversizePkts =
                    pSampleBucket->u4EtherHistoryOversizePkts;

                return SNMP_SUCCESS;
            }
        }

    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryFragments
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryFragments
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryFragments (INT4 i4EtherHistoryIndex,
                             INT4 i4EtherHistorySampleIndex,
                             UINT4 *pu4RetValEtherHistoryFragments)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryFragments\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryFragments =
                    pSampleBucket->u4EtherHistoryFragments;

                return SNMP_SUCCESS;
            }

        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryJabbers
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryJabbers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryJabbers (INT4 i4EtherHistoryIndex,
                           INT4 i4EtherHistorySampleIndex,
                           UINT4 *pu4RetValEtherHistoryJabbers)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryJabbers\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryJabbers =
                    pSampleBucket->u4EtherHistoryJabbers;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryCollisions
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryCollisions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryCollisions (INT4 i4EtherHistoryIndex,
                              INT4 i4EtherHistorySampleIndex,
                              UINT4 *pu4RetValEtherHistoryCollisions)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryCollisions \n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryCollisions =
                    pSampleBucket->u4EtherHistoryCollisions;

                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryUtilization
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryUtilization
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherHistoryUtilization (INT4 i4EtherHistoryIndex,
                               INT4 i4EtherHistorySampleIndex,
                               INT4 *pi4RetValEtherHistoryUtilization)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherHistoryUtilization \n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pi4RetValEtherHistoryUtilization =
                    pSampleBucket->i4EtherHistoryUtilization;

                return SNMP_SUCCESS;
            }

        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;

}

 /* Low Level SET Routine for All Objects */

 /* Low Level TEST Routines for historyControstoryInterval */

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2HistoryControlTable
 Input       :  The Indices
                HistoryControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2HistoryControlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : EtherHistoryHighCapacityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEtherHistoryHighCapacityTable
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
nmhValidateIndexInstanceEtherHistoryHighCapacityTable(INT4 i4EtherHistoryIndex,
                                                      INT4
                                                     i4EtherHistorySampleIndex)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              "nmhValidateIndexInstanceEtherHistoryHighCapacityTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if ((pHistoryControlNode != NULL)
        && ((UINT4) i4EtherHistorySampleIndex >=
            pHistoryControlNode->u4HistoryMinSampleIndex)
        && ((UINT4) i4EtherHistorySampleIndex <
            (pHistoryControlNode->u4HistoryMinSampleIndex +
             (UINT4) pHistoryControlNode->i4HistoryControlBucketsGranted)))

    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: Valid History Indices.\n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Invalid Ether History Sample Index\n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEtherHistoryHighCapacityTable
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
nmhGetFirstIndexEtherHistoryHighCapacityTable(INT4 *pi4EtherHistoryIndex,
                                              INT4 *pi4EtherHistorySampleIndex)
{
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              " nmhGetFirstIndexEtherHistoryHighCapacityTable\n");

    *pi4EtherHistoryIndex = 0;

    if (nmhGetFirstIndexHistoryControlTable (pi4EtherHistoryIndex)
        != SNMP_SUCCESS)
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
        return SNMP_FAILURE;
    }

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      *pi4EtherHistoryIndex);

    if (pHistoryControlNode == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  " Invalid History control Index \n");
        return SNMP_FAILURE;
    }

    if (pHistoryControlNode->u4HistoryMinSampleIndex == 0)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Al the buckets Corresponding"
                  " to the History index is empty \n");
    }
    *pi4EtherHistorySampleIndex =
        (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function "
              " nmhGetFirstIndexEtherHistoryHighCapacityTable\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexEtherHistoryHighCapacityTable
 Input       :  The Indices
                EtherHistoryIndex
                nextEtherHistoryIndex
                EtherHistorySampleIndex
                nextEtherHistorySampleIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
nmhGetNextIndexEtherHistoryHighCapacityTable(INT4 i4EtherHistoryIndex,
                                                  INT4 *pi4NextEtherHistoryIndex,
                                                  INT4 i4EtherHistorySampleIndex,
                                                  INT4
                                                  *pi4NextEtherHistorySampleIndex)
{
    INT4                i4NextHistoryIndex = 0;
    INT4                i4BucketIndex = 0;
    INT1                bNewHistoryIndex = FALSE;

    tRmonHistoryControlNode *pHistoryControlNode = NULL;
    tEtherHistorySample *pNewBucket = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              "nmhGetNextIndexEtherHistoryHighCapacityTable\n");

    if (i4EtherHistoryIndex < 0)
    {
        return SNMP_FAILURE;
    }

    i4NextHistoryIndex = i4EtherHistoryIndex;

    do
    {
        pHistoryControlNode =
            HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                          i4NextHistoryIndex);

        if ((pHistoryControlNode != NULL)
            && (pHistoryControlNode->historyControlStatus == RMON_VALID))
        {
            /* we check if the next sample is present in the
             * corresponding bucket of the history index and if it is
             * not present we take the next history index and the minimum
             * sample index for that history index
             * */

            /*For Every new history index, check buckets are filled with samples */
            if (bNewHistoryIndex == TRUE)
            {
                /*If there is no sample, show only History table information
                 * and do not show any statistical information*/
                if (pHistoryControlNode->u4HistoryMinSampleIndex == 0)
                {
                    *pi4NextEtherHistoryIndex = i4NextHistoryIndex;
                    *pi4NextEtherHistorySampleIndex = 0;
                    return SNMP_SUCCESS;
                }
            }

            if (i4NextHistoryIndex == i4EtherHistoryIndex)
            {
                /* When input sample index is zero and current control node min sample
                   index non-zero, return with current start index for the table */
                if ((i4EtherHistorySampleIndex == 0)
                    && (pHistoryControlNode->u4HistoryMinSampleIndex != 0))
                {
                    *pi4NextEtherHistoryIndex = i4NextHistoryIndex;
                    *pi4NextEtherHistorySampleIndex =
                        (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;
                    return SNMP_SUCCESS;
                }

                /* For the same history index we get the index of the
                 * bucket that should contain the next sample index*/
                i4BucketIndex =
                    ((i4EtherHistorySampleIndex + 1) %
                     pHistoryControlNode->i4HistoryControlBucketsGranted);

            }
            else
            {
                /* If next sample doesnt exist in the history bucket,we obtain
                 * the bucket index for the minimum sample index of next history
                 * index
                 * */

                i4BucketIndex = ((INT4) (pHistoryControlNode->
                                         u4HistoryMinSampleIndex) %
                                 pHistoryControlNode->
                                 i4HistoryControlBucketsGranted);

            }
            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }
            pNewBucket = (pHistoryControlNode->pHistoryEntry->
                          pHistoryBucket + i4BucketIndex);

            if (pNewBucket != NULL)
            {
                if (pNewBucket->etherHistoryIntervalStart != 0)
                {
                    *pi4NextEtherHistoryIndex = i4NextHistoryIndex;

                    if (i4NextHistoryIndex == i4EtherHistoryIndex)
                    {
                        if ((i4EtherHistorySampleIndex + 1) <
                            (INT4) (pHistoryControlNode->u4HistoryMinSampleIndex
                                    + (UINT4) (pHistoryControlNode->
                                               i4HistoryControlBucketsGranted)))
                        {
                            *pi4NextEtherHistorySampleIndex =
                                pNewBucket->i4SampleIndex;
                            return SNMP_SUCCESS;
                        }
                    }
                    else
                    {

                        *pi4NextEtherHistorySampleIndex =
                            (INT4) pHistoryControlNode->u4HistoryMinSampleIndex;
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
        bNewHistoryIndex = TRUE;

    }
    while (nmhGetNextIndexHistoryControlTable (i4NextHistoryIndex,
                                               &i4NextHistoryIndex) ==
           SNMP_SUCCESS);

    /**** $$TRACE_LOG (EXIT,"SNMP Failure\n"); ****/
    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              " Invalid Ether History Index fn: nmhGetNextIndexHistoryControlTable \n");
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEtherHistoryHighCapacityOverflowPkts
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryHighCapacityOverflowPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetEtherHistoryHighCapacityOverflowPkts(INT4 i4EtherHistoryIndex,
                                                INT4 i4EtherHistorySampleIndex,
                                                UINT4
                                *pu4RetValEtherHistoryHighCapacityOverflowPkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              " nmhGetEtherHistoryHighCapacityOverflowPkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryHighCapacityOverflowPkts = 
                pSampleBucket->u8EtherHistoryHighCapacityPkts.msn;
                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryHighCapacityPkts
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryHighCapacityPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetEtherHistoryHighCapacityPkts(INT4 i4EtherHistoryIndex,
                                        INT4 i4EtherHistorySampleIndex,
                                        tSNMP_COUNTER64_TYPE
                                        *pu8RetValEtherHistoryHighCapacityPkts)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              " nmhGetEtherHistoryHighCapacityPkts\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);

            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                FSAP_Counter64_ASSIGN (pu8RetValEtherHistoryHighCapacityPkts,
                                       &(pSampleBucket->
                                        u8EtherHistoryHighCapacityPkts));
                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryHighCapacityOverflowOctets
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryHighCapacityOverflowOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetEtherHistoryHighCapacityOverflowOctets(INT4 i4EtherHistoryIndex,
                                                  INT4 i4EtherHistorySampleIndex,
                                                  UINT4
                                *pu4RetValEtherHistoryHighCapacityOverflowOctets)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              " nmhGetEtherHistoryHighCapacityOverflowOctets\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);
            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                *pu4RetValEtherHistoryHighCapacityOverflowOctets =
                pSampleBucket->u8EtherHistoryHighCapacityOctets.msn;
                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherHistoryHighCapacityOctets
 Input       :  The Indices
                EtherHistoryIndex
                EtherHistorySampleIndex

                The Object
                retValEtherHistoryHighCapacityOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetEtherHistoryHighCapacityOctets(INT4 i4EtherHistoryIndex,
                                          INT4 i4EtherHistorySampleIndex,
                                          tSNMP_COUNTER64_TYPE
                                         *pu8RetValEtherHistoryHighCapacityOctets)
{
    INT4                i4BucketIndex = 0;
    tEtherHistorySample *pSampleBucket = NULL;
    tRmonHistoryControlNode *pHistoryControlNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function "
              " nmhGetEtherHistoryHighCapacityOctets\n");

    pHistoryControlNode =
        HashSearchHistoryControlNode (gpRmonHistoryControlHashTable,
                                      i4EtherHistoryIndex);

    if (pHistoryControlNode != NULL)
    {
        if (pHistoryControlNode->historyControlStatus == RMON_VALID)
        {
            i4BucketIndex = (i4EtherHistorySampleIndex) %
                (pHistoryControlNode->i4HistoryControlBucketsGranted);
            if (i4BucketIndex == 0)
            {
                i4BucketIndex =
                    pHistoryControlNode->i4HistoryControlBucketsGranted;
            }

            pSampleBucket = (pHistoryControlNode->pHistoryEntry->
                             pHistoryBucket + i4BucketIndex);

            if (pSampleBucket != NULL)
            {
                FSAP_Counter64_ASSIGN (pu8RetValEtherHistoryHighCapacityOctets,
                                       &(pSampleBucket->
                                        u8EtherHistoryHighCapacityOctets));
                return SNMP_SUCCESS;
            }
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Ether History Index \n");
    return SNMP_FAILURE;
}

