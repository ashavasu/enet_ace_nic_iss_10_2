/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rpltopn.c,v 1.33 2015/04/23 12:04:49 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for TopN Group     *
 *                                                                  *
 *******************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  rpltopn.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  PERIODIC                                     |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides low level routines for   |
 * |                             accessing TopN Table variables              |
 * |                                                                         |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |     D. Vignesh      |         Create Original          |
 * |                |    19-MAR-1997      |                                  |
 *  -------------------------------------------------------------------------
 * |  2.0           |     LEELA L         |      Changes for Dynamic Tables  |
 * |                |    31-MAR-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  3.0           |     LEELA L         |      Changes for new MIDGEN Tool |
 * |                |    07-JUL-2002      |                                  |
 *  -------------------------------------------------------------------------
 */

#include "rmallinc.h"
#include "rmgr.h"
extern UINT4        HostTopNStatus[11];

#define TOPN_HOST_CONTROL_STATUS(index)      (gaHostTopNControlTable[index].\
                                             pTopNControlEntry->hostTopNStatus)
#define VALID_TOPN_HOST_CONTROL(index)       (TOPN_HOST_CONTROL_STATUS(index)\
                                              == RMON_VALID)

/*
* NOT_INVALID_TOPN_HOST_CONTROL(i4HostTopNControlIndex)H is not the same as VALID_TOPN_HOST_CONTROL(i4HostTopNControlIndex)
* it is has the value TRUE when the status is in
* VALID or UNDER_CREATION or CREATE_REQUEST State
*/
#define TOPN_HOST_CONTROL(i4HostTopNControlIndex)  gaHostTopNControlTable[i4HostTopNControlIndex].pTopNControlEntry
#define NOT_INVALID_TOPN_HOST_CONTROL(index) ((TOPN_HOST_CONTROL(index) != NULL) && (TOPN_HOST_CONTROL_STATUS\
                                             (index) != RMON_INVALID))

#define VALID_GET_TOPN_DATA(i4_report,i4_TopNindex) \
            ((gaHostTopNControlTable[i4_report].pTopNControlEntry->hostTopNStatus == RMON_VALID) &&\
            (gaHostTopNControlTable[i4_report].pTopNControlEntry->u4HostTopNTimeRemaining == 0)\
       && (!IS_MAC_ZERO((UINT1 *)(VOID *)&(gaHostTopNControlTable[i4_report].pTopNControlEntry->pTopNTable \
                           + i4_TopNindex)->u1HostTopNAddress)))

/*
 * LOW LEVEL Routines for Table : hostTopNControlTable.
 */

/*
 * GET_EXACT Validate Index Instance Routine.
 */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceHostTopNControlTable
 Input       :  The Indices
                 i4HostTopNControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceHostTopNControlTable (INT4 i4HostTopNControlIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhValidateIndexInstanceHostTopNControlTable\n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HostTopNControlIndex > 0) &&
        (i4HostTopNControlIndex < RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT) &&
        (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: Hosttopn Control Index is valid.\n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid HostTopNControl Entry\n");

        return SNMP_FAILURE;
    }
}

/*
 * GET_FIRST Routine.
 */
/****************************************************************************
 Function    :  nmhGetFirstIndexHostTopNControlTable
 Input       :  The Indices
                pi4HostTopNControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexHostTopNControlTable (INT4 *pi4HostTopNControlIndex)
{
    INT4                i4TempIndex = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetFirstIndexHostTopNControlTable\n");

    return nmhGetNextIndexHostTopNControlTable (i4TempIndex,
                                                pi4HostTopNControlIndex);
}

/*
 * GET_NEXT Routine.
 */
/****************************************************************************
 Function    :  nmhGetNextIndexHostTopNControlTable
 Input       :  The Indices
                i4HostTopNControlIndex
                pi4NextHostTopNControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexHostTopNControlTable (INT4 i4HostTopNControlIndex,
                                     INT4 *pi4NextHostTopNControlIndex)
{
    INT4                i4Index;

    i4Index = (i4HostTopNControlIndex + 1);

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetNextIndexHostTopNControlTable  \n");

    if (i4Index < RMON_ZERO)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "Invalid HostTopn Control index\n");
        return SNMP_FAILURE;
    }
    for (; i4Index < RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT; i4Index++)
    {
        if (TOPN_HOST_CONTROL (i4Index) != NULL)
        {
            *pi4NextHostTopNControlIndex = i4Index;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index  \n");
    return SNMP_FAILURE;
}

/*
 * Low Level GET Routine for All Objects
 */

/****************************************************************************
 Function    :  nmhGetHostTopNHostIndex
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pi4retValHostTopNHostIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetHostTopNHostIndex (INT4 i4HostTopNControlIndex,
                         INT4 *pi4RetValHostTopNHostIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhGetHostTopNHostIndex \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (NOT_INVALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pi4RetValHostTopNHostIndex,
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    i4HostTopNHostIndex);

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNRateBase
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pi4RetValHostTopNRateBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNRateBase (INT4 i4HostTopNControlIndex,
                        INT4 *pi4RetValHostTopNRateBase)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNRateBase \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (NOT_INVALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pi4RetValHostTopNRateBase,
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    hostTopNRateBase);
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNTimeRemaining
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
               pi4RetValHostTopNTimeRemaining
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNTimeRemaining (INT4 i4HostTopNControlIndex,
                             INT4 *pi4RetValHostTopNTimeRemaining)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNTimeRemaining \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (NOT_INVALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pi4RetValHostTopNTimeRemaining,
                    (INT4) TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    u4HostTopNTimeRemaining);
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNDuration
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pi4RetValHostTopNDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNDuration (INT4 i4HostTopNControlIndex,
                        INT4 *pi4RetValHostTopNDuration)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNDuration  \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (VALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pi4RetValHostTopNDuration,
                    (INT4) TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    u4HostTopNDuration);

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNRequestedSize
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pi4RetValHostTopNRequestedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNRequestedSize (INT4 i4HostTopNControlIndex,
                             INT4 *pi4RetValHostTopNRequestedSize)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNRequestedSize \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (NOT_INVALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pi4RetValHostTopNRequestedSize,
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    i4HostTopNRequestedSize);
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNGrantedSize
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pi4RetValHostTopNGrantedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNGrantedSize (INT4 i4HostTopNControlIndex,
                           INT4 *pi4RetValHostTopNGrantedSize)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNGrantedSize \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (VALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pi4RetValHostTopNGrantedSize,
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    i4HostTopNGrantedSize);

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNStartTime
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pu4RetValHostTopNStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNStartTime (INT4 i4HostTopNControlIndex,
                         UINT4 *pu4RetValHostTopNStartTime)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNStartTime \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (VALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        MEM_EQ_KER (*pu4RetValHostTopNStartTime,
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    u4hostTopNStartTime);

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNOwner
 Input       :  The Indices
                HostTopNControlIndex

                The Object
                retValHostTopNOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNOwner (INT4 i4HostTopNControlIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValHostTopNOwner)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNOwner \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (NOT_INVALID_TOPN_HOST_CONTROL (i4HostTopNControlIndex)))
    {
        STRNCPY (pRetValHostTopNOwner->pu1_OctetList,
                 TOPN_HOST_CONTROL (i4HostTopNControlIndex)->u1HostTopNOwner,
                 OWN_STR_LENGTH);

        pRetValHostTopNOwner->i4_Length =
            (INT4) STRLEN (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                           u1HostTopNOwner);

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid HostTopn Control index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNStatus
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pi4RetValHostTopNStatus 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNStatus (INT4 i4HostTopNControlIndex,
                      INT4 *pi4RetValHostTopNStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNStatus \n");

    if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL) &&
        (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus !=
         RMON_INVALID))
    {
        MEM_EQ_KER (*pi4RetValHostTopNStatus,
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus);

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "SNMP Failure: Status Invalid \n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetHostTopNHostIndex
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4SetValHostTopNHostIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostTopNHostIndex (INT4 i4HostTopNControlIndex,
                         INT4 i4SetValHostTopNHostIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHostTopNHostIndex \n");

    KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->i4HostTopNHostIndex,
                i4SetValHostTopNHostIndex);

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHostTopNHostIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostTopNRateBase
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4SetValHostTopNRateBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostTopNRateBase (INT4 i4HostTopNControlIndex,
                        INT4 i4SetValHostTopNRateBase)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHostTopNRateBase \n");
    KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNRateBase,
                i4SetValHostTopNRateBase);

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHostTopNRateBase \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostTopNTimeRemaining
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4SetValHostTopNTimeRemaining
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostTopNTimeRemaining (INT4 i4HostTopNControlIndex,
                             INT4 i4SetValHostTopNTimeRemaining)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHostTopNTimeRemaining \n");

    RMON_T_GET_TICK ((tOsixSysTime *) &
                     (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                      u4hostTopNStartTime));
    KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->u4HostTopNDuration,
                (UINT4) i4SetValHostTopNTimeRemaining);
    KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                u4HostTopNTimeRemaining, (UINT4) i4SetValHostTopNTimeRemaining);

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHostTopNTimeRemaining \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostTopNRequestedSize
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4SetValHostTopNRequestedSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostTopNRequestedSize (INT4 i4HostTopNControlIndex,
                             INT4 i4SetValHostTopNRequestedSize)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHostTopNRequestedSize  \n");

    KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                i4HostTopNRequestedSize, i4SetValHostTopNRequestedSize);

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHostTopNRequestedSize \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostTopNOwner
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4SetValHostTopNOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostTopNOwner (INT4 i4HostTopNControlIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValHostTopNOwner)
{
    UINT4               u4Minimum;
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHostTopNOwner  \n");

    u4Minimum = (UINT4) (pSetValHostTopNOwner->i4_Length < 
                        (OWN_STR_LENGTH-1) ?
                         pSetValHostTopNOwner->i4_Length : (OWN_STR_LENGTH-1));

    MEMSET (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->u1HostTopNOwner, 0,
            OWN_STR_LENGTH);

    STRNCPY (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->u1HostTopNOwner,
             pSetValHostTopNOwner->pu1_OctetList, u4Minimum);

    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->u1HostTopNOwner[u4Minimum] ='\0';

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function nmhSetHostTopNOwner \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostTopNStatus
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4SetValHostTopNStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostTopNStatus (INT4 i4HostTopNControlIndex, INT4 i4SetValHostTopNStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetHostTopNStatus \n");

    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    /*Check and allocate the memory for the *
     * control entry of the HostTopN Control table     */

    if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) == NULL)
    {
        TOPN_HOST_CONTROL (i4HostTopNControlIndex) =
            (tHostTopnControl *) MemAllocMemBlk (gRmonHostTopNCtrlPoolId);

        if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " Allocation  Failure for:  gaHostTopNControlTable \n");
            return SNMP_FAILURE;
        }

        TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus =
            RMON_INVALID;
    }

    switch (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus)
    {
        case RMON_VALID:
            if (i4SetValHostTopNStatus == RMON_INVALID)
            {

                /* Stop the Control table Timer */
                RmonStopTimer (HOST_TOPN_CONTROL_TABLE, i4HostTopNControlIndex);

                /* First free the memory used by the TopN Report */
                MemReleaseMemBlock (gRmonTopNReportPoolId, (UINT1 *)
                                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                                    pTopNTable);
                /*  Assign NULL to it */
                TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable = NULL;

                /* Delete the Memory allocated for TOPN Control entry */
                MemReleaseMemBlock
                    (gRmonHostTopNCtrlPoolId,
                     (UINT1 *) TOPN_HOST_CONTROL (i4HostTopNControlIndex));
                TOPN_HOST_CONTROL (i4HostTopNControlIndex) = NULL;

            }
            if (i4SetValHostTopNStatus == RMON_UNDER_CREATION)
            {

                /* Free the Memory used by the TopN Report. */
                if ((TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable) !=
                    NULL)
                {
                    MemReleaseMemBlock (gRmonTopNReportPoolId, (UINT1 *)
                                        TOPN_HOST_CONTROL
                                        (i4HostTopNControlIndex)->pTopNTable);
                    /* Assign NULL */
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable =
                        NULL;
                }
                /* Assign the Status & start the timer */
                RmonStartTimer (HOST_TOPN_CONTROL_TABLE, i4HostTopNControlIndex,
                                TOPN_TABLE_DURATION);

                KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                            hostTopNStatus, i4SetValHostTopNStatus);
            }
            break;

        case RMON_UNDER_CREATION:
            if (i4SetValHostTopNStatus == RMON_UNDER_CREATION)
            {
                return SNMP_SUCCESS;
            }
            else if (i4SetValHostTopNStatus == RMON_VALID)
            {

                /* Stop the Control table Timer */
                RmonStopTimer (HOST_TOPN_CONTROL_TABLE, i4HostTopNControlIndex);

                /* Assign the HostTopNGrantedSize after comparing. */

                if (MAX_HOST_PER_INTERFACE <
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                    i4HostTopNRequestedSize)
                {
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                        i4HostTopNGrantedSize = MAX_HOST_PER_INTERFACE;
                }
                else
                {
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                        i4HostTopNGrantedSize =
                        TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                        i4HostTopNRequestedSize;
                }

                /* Allocate memory for the TopN Report */
                TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable =
                    MemAllocMemBlk (gRmonTopNReportPoolId);

                if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable ==
                    NULL)
                {
                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              " Memory Allocation Failed for : TOPN_HOST_CONTROL(i4HostTopNControlIndex).pTopNTable \n");
                    return SNMP_FAILURE;
                }

                /* Assign the Status */
                KER_EQ_MEM (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->
                            hostTopNStatus, i4SetValHostTopNStatus);

                /* Set the BOOLEAN to TRUE to indicate a VALID entry */
                grmprB1AnyValidTopN = TRUE;
            }

            /* If Status is INVALID, Stop the Timer, free the control entry. */
            if (i4SetValHostTopNStatus == RMON_INVALID)
            {
                RmonStopTimer (HOST_TOPN_CONTROL_TABLE, i4HostTopNControlIndex);

                /* Check for  Entry NOT NULL */
                if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable !=
                    NULL)
                {
                    MemReleaseMemBlock (gRmonTopNReportPoolId, (UINT1 *)
                                        TOPN_HOST_CONTROL
                                        (i4HostTopNControlIndex)->pTopNTable);
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex)->pTopNTable =
                        NULL;
                }
                /* Check for  Entry NOT NULL */
                if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL)
                {
                    MemReleaseMemBlock
                        (gRmonHostTopNCtrlPoolId,
                         (UINT1 *) TOPN_HOST_CONTROL (i4HostTopNControlIndex));
                    TOPN_HOST_CONTROL (i4HostTopNControlIndex) = NULL;
                }                /* End of if */
            }
            break;

        case RMON_INVALID:
            if (i4SetValHostTopNStatus == RMON_INVALID)
            {
                return SNMP_SUCCESS;
            }
            else if (i4SetValHostTopNStatus == RMON_CREATE_REQUEST)
            {
                TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus =
                    RMON_UNDER_CREATION;

                RmonStartTimer (HOST_TOPN_CONTROL_TABLE, i4HostTopNControlIndex,
                                TOPN_TABLE_DURATION);
            }
            break;

        case RMON_CREATE_REQUEST:
        case RMON_MAX_STATE:
        default:
            return SNMP_FAILURE;
    } /*****  end of switch  *****/
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = HostTopNStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (HostTopNStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4HostTopNControlIndex,
                      i4SetValHostTopNStatus));

    return SNMP_SUCCESS;
}

/*
 * Low Level TEST Routines for hostTopNControlTable
 */
/****************************************************************************
 Function    :  nmhTestv2HostTopNHostIndex
 Input       :  The Indices
                HostTopNControlIndex

                The Object
                testValHostTopNHostIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1 905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rf c1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostTopNHostIndex (UINT4 *pu4ErrorCode,
                            INT4 i4HostTopNControlIndex,
                            INT4 i4TestValHostTopNHostIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HostTopNHostIndex \n");

    if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL)
    {
        if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus ==
            RMON_UNDER_CREATION)
        {
            if ((i4TestValHostTopNHostIndex > 0) &&
                (i4TestValHostTopNHostIndex < RMON_MAX_INTERFACE_LIMIT) &&
                (HOST_CONTROL (i4TestValHostTopNHostIndex) != NULL))
            {
                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC, " HostIndex is valid \n");
                return SNMP_SUCCESS;
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          " HostIndex is not valid \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }
        }
        else
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      " Status is not UNDER_CREATION. \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid HostTopn Control Index. \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostTopNRateBase
 Input       :  The Indices
                HostTopNControlIndex

                The Object
                testValHostTopNRateBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostTopNRateBase (UINT4 *pu4ErrorCode,
                           INT4 i4HostTopNControlIndex,
                           INT4 i4TestValHostTopNRateBase)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HostTopNRateBase \n");

    if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL)
    {
        if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus ==
            RMON_UNDER_CREATION)
        {
            if (RMON_INCL_IN_BETWEEN (i4TestValHostTopNRateBase,
                                      hostTopNInPkts, hostTopNOutMulticastPkts))
            {
                return SNMP_SUCCESS;
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          " HostTopNRateBase is not a Valid Value. \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }
        }
        else
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Status is not UNDER_CREATION \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid HosttopN ControlIndex \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostTopNTimeRemaining
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4testValHostTopNTimeRemaining
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostTopNTimeRemaining (UINT4 *pu4ErrorCode,
                                INT4 i4HostTopNControlIndex,
                                INT4 i4TestValHostTopNTimeRemaining)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhTestv2HostTopNTimeRemaining \n");

    if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL)
    {
        if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus ==
            RMON_UNDER_CREATION)
        {
            if (i4TestValHostTopNTimeRemaining > 0)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "Time Remaining value is less than 0. \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }
        }
        else
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Status is not UNDER_CREATION. \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "HostTopN Control Index is not valid\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostTopNRequestedSize
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4TestValHostTopNRequestedSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostTopNRequestedSize (UINT4 *pu4ErrorCode,
                                INT4 i4HostTopNControlIndex,
                                INT4 i4TestValHostTopNRequestedSize)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HostTopNRequestedSize  \n");

    if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL)
    {
        if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus ==
            RMON_UNDER_CREATION)
        {
            if ((i4TestValHostTopNRequestedSize > 0))
            {
                return SNMP_SUCCESS;
            }
            else
            {
                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "TopNRequestedSize value is less than 0. \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }
        }
        else
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Status is not UNDER_CREATION. \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "HostTopNControlIndex is invalid \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostTopNOwner
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                pTestValHostTopNOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostTopNOwner (UINT4 *pu4ErrorCode,
                        INT4 i4HostTopNControlIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValHostTopNOwner)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2HostTopNOwner \n");
    RMON_DUMMY (pTestValHostTopNOwner);

    if (TOPN_HOST_CONTROL (i4HostTopNControlIndex) != NULL)
    {
        if (TOPN_HOST_CONTROL (i4HostTopNControlIndex)->hostTopNStatus ==
            RMON_UNDER_CREATION)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Status is not UNDER_CREATION. \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "HostTopNControlIndex is invalid. \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostTopNStatus
 Input       :  The Indices
                i4HostTopNControlIndex

                The Object
                i4TestValHostTopNStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostTopNStatus (UINT4 *pu4ErrorCode,
                         INT4 i4HostTopNControlIndex,
                         INT4 i4TestValHostTopNStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function  nmhTestv2HostTopNStatus \n");
    /* Configuration of this table is not supported for now.
     * Remove this error return, once support is added
     */
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4HostTopNControlIndex);
    UNUSED_PARAM (i4TestValHostTopNStatus);
    return SNMP_FAILURE;
}

/*
 * LOW LEVEL Routines for Table : hostTopNTable.
 */

/*
 * GET_EXACT Validate Index Instance Routine.
 */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceHostTopNTable
 Input       :  The Indices
                i4HostTopNReport
                i4HostTopNIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceHostTopNTable (INT4 i4HostTopNReport,
                                       INT4 i4HostTopNIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhValidateIndexInstanceHostTopNTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HostTopNReport > 0) &&
        (i4HostTopNReport < RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT)
        && (i4HostTopNIndex > 0)
        && (i4HostTopNIndex <
            TOPN_HOST_CONTROL (i4HostTopNReport)->i4HostTopNGrantedSize))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "Valid HostTopN Report Indices.\n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Not a valid HostTopNIndex \n");
        return SNMP_FAILURE;
    }
}

/*
 * GET_FIRST Routine.
 */
/****************************************************************************
 Function    :  nmhGetFirstIndexHostTopNTable
 Input       :  The Indices
                pi4HostTopNReport
                pi4HostTopNIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexHostTopNTable (INT4 *pi4HostTopNReport, INT4 *pi4HostTopNIndex)
{
    INT4                i4TempReport = 0;
    INT4                i4TempIndex = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetFirstIndexHostTopNTable \n");

    return nmhGetNextIndexHostTopNTable (i4TempReport, pi4HostTopNReport,
                                         i4TempIndex, pi4HostTopNIndex);
}

/*
 * GET_NEXT Routine.
 */

/****************************************************************************
 Function    :  nmhGetNextIndexHostTopNTable
 Input       :  The Indices
                i4HostTopNReport
                pi4nextHostTopNReport
                i4HostTopNIndex
                pi4nextHostTopNIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexHostTopNTable (INT4 i4HostTopNReport,
                              INT4 *pi4NextHostTopNReport,
                              INT4 i4HostTopNIndex, INT4 *pi4NextHostTopNIndex)
{

    INT4                i4Report = i4HostTopNReport;
    INT4                i4TopNIndex = (i4HostTopNIndex + 1);

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetNextIndexHostTopNTable \n");

    if (i4Report < RMON_ZERO)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "Not a valid HostTopNreport Value\n");
        return SNMP_FAILURE;
    }
    if (i4Report == RMON_ZERO)
        i4Report = 1;
    while (i4Report < RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT)
    {
        if ((TOPN_HOST_CONTROL (i4Report) != NULL)
            && (TOPN_HOST_CONTROL (i4Report)->hostTopNStatus != RMON_INVALID)
            && (TOPN_HOST_CONTROL (i4Report)->u4HostTopNTimeRemaining == 0))
        {

            if (i4TopNIndex >
                TOPN_HOST_CONTROL (i4Report)->i4HostTopNGrantedSize)
            {
                i4TopNIndex = 1;
                i4Report++;
                continue;
            }
            *pi4NextHostTopNReport = i4Report;
            *pi4NextHostTopNIndex = i4TopNIndex;
            return SNMP_SUCCESS;
        }
        else
        {
            i4Report++;
        }
    }
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Not a valid HostTopNreport \n");
    return SNMP_FAILURE;
}

/*
 * Low Level GET Routine for All Objects
 */

/****************************************************************************
 Function    :  nmhGetHostTopNAddress
 Input       :  The Indices
                i4HostTopNReport
                i4HostTopNIndex

                The Object
                pRetValHostTopNAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNAddress (INT4 i4HostTopNReport,
                       INT4 i4HostTopNIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValHostTopNAddress)
{
    tHostTopn          *pHostTopn = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNAddress  \n");

    /* We are using 0 .. N-1  instead of 1 .. N */
    i4HostTopNIndex--;

    pHostTopn = (TOPN_HOST_CONTROL (i4HostTopNReport)->pTopNTable
                 + i4HostTopNIndex);
    if (VALID_GET_TOPN_DATA (i4HostTopNReport, i4HostTopNIndex))
    {
        /* Copy the Host Address giving. */

        COPY_MAC_ADDRESS (pRetValHostTopNAddress->pu1_OctetList,
                          pHostTopn->u1HostTopNAddress);
        pRetValHostTopNAddress->i4_Length = ADDRESS_LENGTH;
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Not a valid HostTopNreport Value \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTopNRate
 Input       :  The Indices
                i4HostTopNReport
                i4HostTopNIndex

                The Object
                pRetValHostTopNRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTopNRate (INT4 i4HostTopNReport, INT4 i4HostTopNIndex,
                    INT4 *pi4RetValHostTopNRate)
{
    tHostTopn          *pHostTopn = NULL;
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetHostTopNRate \n");

    /* We are using 0 .. N-1  instead of 1 .. N */
    i4HostTopNIndex--;

    pHostTopn = (TOPN_HOST_CONTROL (i4HostTopNReport)->pTopNTable
                 + i4HostTopNIndex);

    if (VALID_GET_TOPN_DATA (i4HostTopNReport, i4HostTopNIndex))
    {
        *pi4RetValHostTopNRate = (INT4) pHostTopn->u4HostTopNRate;
        return SNMP_SUCCESS;
    }
    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              "Not a valid HostTopNreport Value  \n");
    return SNMP_FAILURE;
}

/*
 * Low Level SET Routine for All Objects
 */
/*
 * Low Level TEST Routines for All Objects
 */

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2HostTopNControlTable
 Input       :  The Indices
                HostTopNControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2HostTopNControlTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
