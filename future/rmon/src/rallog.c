/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rallog.c,v 1.18 2013/07/12 12:35:05 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for LOG Group      *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rallog.c
 *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.
 *
 *    SUBSYSTEM NAME         :  LOW LEVEL CODE
 *
 *    MODULE NAME            :  RMON APERIODIC
 *
 *    LANGUAGE               :  C
 *
 *    TARGET ENVIRONMENT     :  ANY
 *
 *    DATE OF FIRST RELEASE  :
 *
 *    DESCRIPTION            : This file provides the low level routines for
 *                             the log table.  
 *
 *
 *  --------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     -------------
 *
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |          DESCRIPTION OF
 *                  |        DATE          |             CHANGE
 * -----------------|----------------------|---------------------------------
 *      1.0         |  Vijay G. Krishnan   |         Create Original
 *                  |      24-APR-1997     |
 * --------------------------------------------------------------------------
 *      2.0         |  Bhupendra           |  changing function signatures
 *                  |  17-AUG-2001         |
 * --------------------------------------------------------------------------
 *      3.0         |  LEELA L             |  changing for Dynamic Tables.
 *                  |  31-MAR-2002         |
 * --------------------------------------------------------------------------
 *      4.0         |  LEELA L             |  changing for New MIDGEN Tool.
 *                  |  07-JUL-2002         |
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"

#define COMP_LOG_IDX(a, b, x, y)      (((a) == (x)) ? ((b) - (y)) : ((a) - (x)))

#define LOG_IN_BETWEEN(a, b, x, y, p, q) ((COMP_LOG_IDX(a, b, p, q) < 0) && \
                                          (COMP_LOG_IDX(x, y, p, q) > 0))

 /*
  * LOW LEVEL Routines for Table : logTable.
  */

 /*
  * GET_EXACT Validate Index Instance Routine.
  */

/*********************************************************************

 Function    :  nmhValidateIndexInstanceLogTable
 Input       :  The Indices
                i4LogEventIndex
                i4LogIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**********************************************************************/

INT1
nmhValidateIndexInstanceLogTable (INT4 i4LogEventIndex, INT4 i4LogIndex)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entering into the function: \
                 nmhValidateIndexInstanceLogTable \n");
    RMON_DUMMY (i4LogIndex);

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4LogEventIndex);

    if ((i4LogIndex >= 1) && (pEventNode != NULL) &&
        (i4LogIndex < pEventNode->pLogEvent->i4BucketsFilled + 1))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Valid LogEvent Indices. \n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid LogEvent Indices. \n");
        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  " Leaving function : nmhValidateIndexInstanceLogTable \n");
        return SNMP_FAILURE;
    }
}

 /*
  * GET_FIRST Routine.
  */

/****************************************************************************
 Function    :  nmhGetFirstIndexLogTable
 Input       :  The Indices
                pi4LogEventIndex
                pi4LogIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexLogTable (INT4 *pi4LogEventIndex, INT4 *pi4LogIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetFirstIndexLogTable \n");

    *pi4LogEventIndex = 0;
    *pi4LogIndex = 1;
    if (nmhGetNextIndexLogTable (0, pi4LogEventIndex, 1, pi4LogIndex) !=
        SNMP_SUCCESS)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, " Invalid Event Index \n");
        return SNMP_FAILURE;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function : nmhGetFirstIndexLogTable \n");
    return SNMP_SUCCESS;
}

 /*
  * GET_NEXT Routine.
  */
/****************************************************************************
 Function    :  nmhGetNextIndexLogTable
 Input       :  The Indices
                i4LogEventIndex
                pi4nextLogEventIndex
                i4LogIndex
                pi4nextLogIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexLogTable (INT4 i4LogEventIndex,
                         INT4 *pi4NextLogEventIndex,
                         INT4 i4LogIndex, INT4 *pi4NextLogIndex)
{
    INT4                i4Index1;
    INT4                i4Index2;
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetNextIndexLogTable \n");

    if (i4LogEventIndex < 0)
    {
        return SNMP_FAILURE;
    }

    if (i4LogIndex < 0)
    {
        i4LogEventIndex++;
        i4LogIndex = 0;
    }

    *pi4NextLogEventIndex = RMON_MAX_EVENT_INDEX;
    *pi4NextLogIndex = (MAX_LOG_BUCKETS + 1);
    i4Index2 = i4LogIndex;
    i4Index1 = i4LogEventIndex;

    do
    {
        pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4Index1);

        if ((pEventNode != NULL) &&
            ((pEventNode->eventType == LOG) ||
             (pEventNode->eventType == LOG_AND_TRAP)))
        {
            if ((pEventNode->pLogEvent != NULL) &&
                (pEventNode->eventStatus == RMON_VALID) &&
                (pEventNode->pLogEvent->i4BucketsAllocated != 0) &&
                (i4Index2 < pEventNode->pLogEvent->i4BucketsFilled + 1))
            {
                for (; i4Index2 < pEventNode->pLogEvent->i4BucketsFilled + 1;
                     i4Index2++)
                {
                    if ((pEventNode->pLogEvent->pLogBucket +
                         i4Index2)->logTime != 0)
                    {
                        if (LOG_IN_BETWEEN (i4LogEventIndex, i4LogIndex,
                                            *pi4NextLogEventIndex,
                                            *pi4NextLogIndex, i4Index1,
                                            i4Index2))
                        {
                            *pi4NextLogEventIndex = i4Index1;
                            *pi4NextLogIndex = i4Index2;

                            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                                      "Next Log event Indices retrieved. \n");

                            return SNMP_SUCCESS;
                        }
                    }
                }                /* for i4Index2 */
            }
        }
        i4Index2 = 1;
    }
    while (nmhGetNextIndexEventTable (i4Index1, &i4Index1) == SNMP_SUCCESS);

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Log event Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level GET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhGetLogTime
 Input       :  The Indices
                i4LogEventIndex
                i4LogIndex

                The Object
                pu4RetValLogTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetLogTime (INT4 i4LogEventIndex, INT4 i4LogIndex, UINT4 *pu4RetValLogTime)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetLogTime\n");

    RMON_DBG2 (MINOR_TRC | RMON_MGMT_TRC,
               "In fn nmhGetLogTime logEventIndex = %d && i4LogIndex = %d\n\n",
               i4LogEventIndex, i4LogIndex);

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4LogEventIndex);

    if (pEventNode == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "Event Entry is not configured. \n");
        return SNMP_FAILURE;
    }

    if ((pEventNode->pLogEvent != NULL) &&
        (pEventNode->eventStatus == RMON_VALID) &&
        (pEventNode->pLogEvent->i4BucketsAllocated != 0) &&
        (i4LogIndex < pEventNode->pLogEvent->i4BucketsFilled + 1))
    {

        *pu4RetValLogTime = (pEventNode->pLogEvent->pLogBucket +
                             i4LogIndex)->logTime;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              "Invalid Log Event Index:fn nmhGetLogTime \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetLogDescription
 Input       :  The Indices
                i4LogEventIndex
                i4LogIndex

                The Object
                pRetValLogDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetLogDescription (INT4 i4LogEventIndex, INT4 i4LogIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValLogDescription)
{
    tRmonEventNode     *pEventNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetLogDescription\n");

    RMON_DBG1 (MINOR_TRC | RMON_MGMT_TRC,
               "In fn nmhGetLogDescription logEventIndex = %d\n\n",
               i4LogEventIndex);

    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4LogEventIndex);

    if (pEventNode == NULL)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "Event Entry is not configured. \n");
        return SNMP_FAILURE;
    }

    if ((pEventNode->pLogEvent != NULL) &&
        (pEventNode->eventStatus == RMON_VALID) &&
        (pEventNode->pLogEvent->i4BucketsAllocated != 0) &&
        (i4LogIndex < pEventNode->pLogEvent->i4BucketsFilled + 1))
    {
        /* Check whether the pLogBucket for the Log Entry is NOT NULL */
        if (pEventNode->pLogEvent->pLogBucket != NULL)
        {
            STRNCPY (pRetValLogDescription->pu1_OctetList,
                     (&(pEventNode->pLogEvent->pLogBucket +
                        i4LogIndex)->u1LogDescription), LOG_DESCR_LEN);

            pRetValLogDescription->i4_Length =
                (INT4) STRLEN (pRetValLogDescription->pu1_OctetList);

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "LogDescription retrieval success. \n");

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              "Invalid Log Event Index :fn-> nmhGetLogDescription\n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

 /*
  * Low Level TEST Routines
  */
