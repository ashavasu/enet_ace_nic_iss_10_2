/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: ralstat.c,v 1.41 2014/12/31 10:58:47 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP lowlevel routines for Statistics Group*
 *                                                                  *
 *******************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  ralstat.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  APERIODIC                                    |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides low level routines for   |
 * |                             accessing Statistics Table variables        |
 * |                                                                         |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |     D. Vignesh      |         Create Original          |
 * |                |    19-MAR-1997      |                                  |
 *  -------------------------------------------------------------------------
 * |  2.0           |   Yogindhar P       |   changing function signatures   |
 * |                |    16-AUG-2001      |   and variables compatible to    |
 * |                |                     |    Snmpv2                        |
 *  -------------------------------------------------------------------------
 * |  3.0           |   LEELA L           |   Changes for Dynamic Tables     |
 * |                |    31-MAR-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  4.0           |   LEELA L           |   Changes for New MIDGEN Tool    |
 * |                |    07-JUL-2002      |                                  |
 *  -------------------------------------------------------------------------
 *
 */

#include "rmallinc.h"

#include "cli.h"
#include "rmoncli.h"
#include "rmgr.h"
extern UINT4        EtherStatsStatus[11];
extern UINT4        EtherStatsDataSource[11];
extern UINT4        EtherStatsOwner[11];
/* LOW LEVEL Routines for Table : gaEtherStatsTablS. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEtherStatsTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceEtherStatsTable (INT4 i4EtherStatsIndex)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entering into the function, \
              nmhValidateIndexInstanceEtherStatsTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4EtherStatsIndex < 1) ||
        (i4EtherStatsIndex > RMON_MAX_ETHERSTATS_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Unknown Interface \n");
        return SNMP_FAILURE;
    }

    if (HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                  i4EtherStatsIndex) != NULL)
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC, "SNMP Success: Valid Index \n");
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "SNMP Success: Valid Index \n");
                return SNMP_SUCCESS;
            }
        }
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC, "SNMP Failure: Unknown Interface \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEtherStatsTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexEtherStatsTable (INT4 *pi4EtherStatsIndex)
{
    INT4                i4Zero = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetFirstIndexEtherStatsTable \n");

    return nmhGetNextIndexEtherStatsTable (i4Zero, pi4EtherStatsIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexEtherStatsTable
 Input       :  The Indices
                EtherStatsIndex
                nextEtherStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexEtherStatsTable (INT4 i4EtherStatsIndex,
                                INT4 *pi4NextEtherStatsIndex)
{
    INT4                i4Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetNextIndexEtherStatsTable \n");

    if ((i4EtherStatsIndex < RMON_ZERO) ||
        (i4EtherStatsIndex > RMON_MAX_ETHERSTATS_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid Index - out of range\n");
        return SNMP_FAILURE;
    }

    i4Index =
        HashSearchEtherStatsNode_NextValidTindex (gpRmonEtherStatsHashTable,
                                                  i4EtherStatsIndex);

    if ((i4Index != RMON_ZERO) &&
        (i4Index > i4EtherStatsIndex) && (i4Index <= RMON_MAX_ETHERSTATS_INDEX))
    {
        *pi4NextEtherStatsIndex = i4Index;
        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: NO Next VALID Index \n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEtherStatsDataSource
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsDataSource (INT4 i4EtherStatsIndex, tSNMP_OID_TYPE
                            * pRetValEtherStatsDataSource)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsDataSource \n");

    TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode, tRmonEtherStatsTimerNode *)
    {
        if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
        {
            if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
            {
                GET_DATA_SOURCE_OID (pRetValEtherStatsDataSource,
                                     pEtherStatsNode->u4EtherStatsDataSource,
                                     pEtherStatsNode->u4EtherStatsOIDType);

                return SNMP_SUCCESS;
            }
        }
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            GET_DATA_SOURCE_OID (pRetValEtherStatsDataSource,
                                 pEtherStatsNode->u4EtherStatsDataSource,
                                 pEtherStatsNode->u4EtherStatsOIDType);

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC, "SNMP Failure: INVALID Entry \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsDropEvents
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsDropEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsDropEvents (INT4 i4EtherStatsIndex, UINT4
                            *pu4RetValEtherStatsDropEvents)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsDropEvents \n");

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsDropEvents,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsDropEvents);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsDropEvents,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsDropEvents);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsOctets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsOctets (INT4 i4EtherStatsIndex, UINT4
                        *pu4RetValEtherStatsOctets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsOctets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsOctets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsOctets,
                        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOctets);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsOctets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsOctets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts (INT4 i4EtherStatsIndex, UINT4 *pu4RetValEtherStatsPkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsPkts \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts,
                        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsBroadcastPkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsBroadcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsBroadcastPkts (INT4 i4EtherStatsIndex, UINT4
                               *pu4RetValEtherStatsBroadcastPkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsBroadcastPkts \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsBroadcastPkts = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsBroadcastPkts,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsBroadcastPkts);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsBroadcastPkts,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsBroadcastPkts);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsMulticastPkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsMulticastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsMulticastPkts (INT4 i4EtherStatsIndex, UINT4
                               *pu4RetValEtherStatsMulticastPkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsMulticastPkts \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsMulticastPkts = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsMulticastPkts,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsMulticastPkts);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsMulticastPkts,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsMulticastPkts);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsCRCAlignErrors
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsCRCAlignErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsCRCAlignErrors (INT4 i4EtherStatsIndex, UINT4
                                *pu4RetValEtherStatsCRCAlignErrors)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsCRCAlignErrors \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsCRCAlignErrors = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsCRCAlignErrors,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsCRCAlignErrors);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsCRCAlignErrors,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsCRCAlignErrors);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsUndersizePkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsUndersizePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsUndersizePkts (INT4 i4EtherStatsIndex, UINT4
                               *pu4RetValEtherStatsUndersizePkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsUndersizePkts \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsUndersizePkts = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsUndersizePkts,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsUndersizePkts);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsUndersizePkts,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsUndersizePkts);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsOversizePkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsOversizePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsOversizePkts (INT4 i4EtherStatsIndex, UINT4
                              *pu4RetValEtherStatsOversizePkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsOversizePkts \n");
    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsOversizePkts = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsOversizePkts,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsOversizePkts);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsOversizePkts,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsOversizePkts);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsFragments
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsFragments
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsFragments (INT4 i4EtherStatsIndex, UINT4
                           *pu4RetValEtherStatsFragments)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsFragments \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsFragments = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsFragments,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsFragments);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsFragments,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsFragments);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsJabbers
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsJabbers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsJabbers (INT4 i4EtherStatsIndex, UINT4
                         *pu4RetValEtherStatsJabbers)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsJabbers \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsJabbers = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsJabbers,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsJabbers);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsJabbers,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsJabbers);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsCollisions
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsCollisions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsCollisions (INT4 i4EtherStatsIndex, UINT4
                            *pu4RetValEtherStatsCollisions)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsCollisions \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsCollisions = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsCollisions,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsCollisions);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsCollisions,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsCollisions);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts64Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts64Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts64Octets (INT4 i4EtherStatsIndex, UINT4
                              *pu4RetValEtherStatsPkts64Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsPkts64Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts64Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts64Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts64Octets);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts64Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts64Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts65to127Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts65to127Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts65to127Octets (INT4 i4EtherStatsIndex, UINT4
                                   *pu4RetValEtherStatsPkts65to127Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsPkts65to127Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts65to127Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts65to127Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts65to127Octets);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts65to127Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts65to127Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts128to255Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts128to255Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts128to255Octets (INT4 i4EtherStatsIndex, UINT4
                                    *pu4RetValEtherStatsPkts128to255Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function \
                      nmhGetEtherStatsPkts128to255Octets \n");
    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts128to255Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts128to255Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts128to255Octets);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts128to255Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts128to255Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts256to511Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts256to511Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts256to511Octets (INT4 i4EtherStatsIndex, UINT4
                                    *pu4RetValEtherStatsPkts256to511Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function \
                      nmhGetEtherStatsPkts256to511Octets \n");
    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts256to511Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts256to511Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts256to511Octets);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts256to511Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts256to511Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts512to1023Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts512to1023Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts512to1023Octets (INT4 i4EtherStatsIndex, UINT4
                                     *pu4RetValEtherStatsPkts512to1023Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function \
                      nmhGetEtherStatsPkts512to1023Octets \n");
    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts512to1023Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts512to1023Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts512to1023Octets);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts512to1023Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts512to1023Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsPkts1024to1518Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsPkts1024to1518Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsPkts1024to1518Octets (INT4 i4EtherStatsIndex, UINT4
                                      *pu4RetValEtherStatsPkts1024to1518Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function \
                       nmhGetEtherStatsPkts1024to1518Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsPkts1024to1518Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsPkts1024to1518Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u4EtherStatsPkts1024to1518Octets);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsPkts1024to1518Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u4EtherStatsPkts1024to1518Octets);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsOwner
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsOwner (INT4 i4EtherStatsIndex, tSNMP_OCTET_STRING_TYPE
                       * pRetValEtherStatsOwner)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsOwner \n");

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            STRNCPY (pRetValEtherStatsOwner->pu1_OctetList,
                     pEtherStatsNode->u1EtherStatsOwner, OWN_STR_LENGTH);

            pRetValEtherStatsOwner->i4_Length =
                (INT4) STRLEN (pEtherStatsNode->u1EtherStatsOwner);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    STRNCPY (pRetValEtherStatsOwner->pu1_OctetList,
                             pEtherStatsNode->u1EtherStatsOwner,
                             OWN_STR_LENGTH);

                    pRetValEtherStatsOwner->i4_Length =
                        (INT4) STRLEN (pEtherStatsNode->u1EtherStatsOwner);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetEtherStatsStatus
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetEtherStatsStatus (INT4 i4EtherStatsIndex, INT4 *pi4RetValEtherStatsStatus)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhGetEtherStatsStatus \n");

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pi4RetValEtherStatsStatus,
                        pEtherStatsNode->etherStatsStatus);

            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pi4RetValEtherStatsStatus,
                                pEtherStatsNode->etherStatsStatus);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetEtherStatsDataSource
 Input       :  The Indices
                EtherStatsIndex

                The Object
                setValEtherStatsDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEtherStatsDataSource (INT4 i4EtherStatsIndex, tSNMP_OID_TYPE
                            * pSetValEtherStatsDataSource)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               u4Hashindex = 0;
    UINT4               u4SeqNum = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetEtherStatsDataSource \n");

    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode == NULL)
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus == RMON_UNDER_CREATION)
                {
                    SET_DATA_SOURCE_OID (pSetValEtherStatsDataSource,
                                         &(pEtherStatsNode->
                                           u4EtherStatsDataSource),
                                         &(pEtherStatsNode->
                                           u4EtherStatsOIDType));

                    u4Hashindex = (UINT4) RmonHashFn (pEtherStatsNode->
                                                      u4EtherStatsDataSource);

                    TMO_SLL_Delete (&gEtherStatsSLL,
                                    &pEtherStatsNode->nextEtherStatsNode);

                    pEtherStatsNode->nextEtherStatsNode.pNext = NULL;

                    RMON_HASH_ADD_NODE (gpRmonEtherStatsHashTable,
                                        &pEtherStatsNode->nextEtherStatsNode,
                                        u4Hashindex, NULL);
                    break;

                }
            }
        }
    }
    else
        SET_DATA_SOURCE_OID (pSetValEtherStatsDataSource,
                             &(pEtherStatsNode->u4EtherStatsDataSource),
                             &(pEtherStatsNode->u4EtherStatsOIDType));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = EtherStatsDataSource;
    SnmpNotifyInfo.u4OidLen = sizeof (EtherStatsDataSource) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %o", i4EtherStatsIndex,
                     pSetValEtherStatsDataSource));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetEtherStatsOwner
 Input       :  The Indices
                EtherStatsIndex

                The Object
                setValEtherStatsOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEtherStatsOwner (INT4 i4EtherStatsIndex, tSNMP_OCTET_STRING_TYPE
                       * pSetValEtherStatsOwner)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4Minimum;
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetEtherStatsOwner \n");
 
     MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    u4Minimum = (UINT4) ((pSetValEtherStatsOwner->i4_Length < OWN_STR_LENGTH) ?
                         pSetValEtherStatsOwner->i4_Length : OWN_STR_LENGTH);

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        MEMSET (pEtherStatsNode->u1EtherStatsOwner, 0, OWN_STR_LENGTH);

        STRNCPY (pEtherStatsNode->u1EtherStatsOwner,
                 pSetValEtherStatsOwner->pu1_OctetList, u4Minimum);
    }
    else
    {
        /* Check whether the node is present in the Linked List */
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                MEMSET (pEtherStatsNode->u1EtherStatsOwner, 0, OWN_STR_LENGTH);

                STRNCPY (pEtherStatsNode->u1EtherStatsOwner,
                         pSetValEtherStatsOwner->pu1_OctetList, u4Minimum);
            }
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = EtherStatsOwner;
    SnmpNotifyInfo.u4OidLen = sizeof (EtherStatsOwner) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4EtherStatsIndex,
                      pSetValEtherStatsOwner));


    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetEtherStatsStatus
 Input       :  The Indices
                EtherStatsIndex

                The Object
                setValEtherStatsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetEtherStatsStatus (INT4 i4EtherStatsIndex, INT4 i4SetValEtherStatsStatus)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               u4Hindex;
#ifdef NPAPI_WANTED
    UINT4               u4Port;
    UINT1               u1StatsCollectionEnable = FNP_FALSE;
#endif
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhSetEtherStatsStatus \n");

    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    if ((i4EtherStatsIndex < 1)
        || (i4EtherStatsIndex > RMON_MAX_ETHERSTATS_INDEX))
    {
        return SNMP_FAILURE;
    }

    /*Check and allocate the memory for the *
     * control entry of the stats table     */

    TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode, tRmonEtherStatsTimerNode *)
    {
        if (pEtherStatsNode != NULL)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                break;
            }
        }
    }

    if (pEtherStatsNode == NULL)
    {
        pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                    i4EtherStatsIndex);
        if ((pEtherStatsNode == NULL) &&
            (i4SetValEtherStatsStatus == RMON_CREATE_REQUEST))
        {
            pEtherStatsNode =
                (tRmonEtherStatsTimerNode *)
                MemAllocMemBlk (gRmonEtherStatsPoolId);

            if (pEtherStatsNode == NULL)
            {
                RMON_DBG (CRITICAL_TRC | MEM_TRC,
                          " Memory Allocation Failure - EtherStats Table. \n");
                return SNMP_FAILURE;
            }
            MEMSET (&pEtherStatsNode->RmonEtherStatsPkts, 0,
                    sizeof (tRmonEtherStatsNode));

            pEtherStatsNode->i4EtherStatsIndex = i4EtherStatsIndex;
            pEtherStatsNode->u4EtherStatsDataSource = 0;
            pEtherStatsNode->u4EtherStatsCreateTime = 0;
            pEtherStatsNode->u4EtherStatsOIDType = 0;
            pEtherStatsNode->etherStatsStatus = RMON_INVALID;
            MEMSET (&pEtherStatsNode->u1EtherStatsOwner, 0, OWN_STR_LENGTH);

            TMO_SLL_Add (&gEtherStatsSLL, &pEtherStatsNode->nextEtherStatsNode);
        }
        else if (pEtherStatsNode == NULL)
        {
            return SNMP_FAILURE;
        }
    }

    switch (pEtherStatsNode->etherStatsStatus)
    {
        case RMON_VALID:

            /* If STATUS is INVALID, free the Control Entry memory */
            if (i4SetValEtherStatsStatus == RMON_INVALID)
            {
                if (HashSearchHistoryControlNode_DSource
                    (gpRmonHistoryControlHashTable,
                     pEtherStatsNode->u4EtherStatsDataSource,
                     pEtherStatsNode->u4EtherStatsOIDType) == RMON_TRUE)
                {
                    RMON_DBG (MAJOR_TRC | MEM_TRC,
                              "History Control Entry exists for this Ether stats\n");
                    return SNMP_FAILURE;
                }
                if (HashSearchAlarmNode_ForStatsIndex (gpRmonAlarmHashTable,
                                                       pEtherStatsNode->
                                                       i4EtherStatsIndex) ==
                    RMON_TRUE)
                {
                    RMON_DBG (MAJOR_TRC | MEM_TRC,
                              "Alarm Entry exists for this Ether stats\n");
                    return SNMP_FAILURE;
                }

                RMON_DBG (MAJOR_TRC | MEM_TRC,
                          "etherStatsStatus  VALID  to INVALID \n");
#if defined(NPAPI_WANTED)
                /* Make the entry INVALID in HW if NPAPI_WANTED is supported */
                if (pEtherStatsNode->u4EtherStatsOIDType == PORT_INDEX_TYPE)
                {
                    u4Port = pEtherStatsNode->u4EtherStatsDataSource;
                    if (RmonFsRmonHwSetEtherStatsTable (u4Port, FNP_FALSE) !=
                        FNP_SUCCESS)
                    {
                        RMON_DBG (MAJOR_TRC | MEM_TRC,
                                  "Failed in configuring the HW etherStats table \n");

                        return SNMP_FAILURE;
                    }
                }
#endif
                u4Hindex =
                    (UINT4) RmonHashFn (pEtherStatsNode->
                                        u4EtherStatsDataSource);
                RmonStopTimer (ETHER_STATS_TABLE, i4EtherStatsIndex);
                RMON_HASH_DEL_NODE (gpRmonEtherStatsHashTable,
                                    &pEtherStatsNode->nextEtherStatsNode,
                                    u4Hindex);

                MemReleaseMemBlock (gRmonEtherStatsPoolId,
                                    (UINT1 *) pEtherStatsNode);
                pEtherStatsNode = NULL;
            }

            if (i4SetValEtherStatsStatus == RMON_UNDER_CREATION)
            {
                RMON_DBG (MAJOR_TRC | MEM_TRC, "etherStatsStatus  VALID \
                     to RMON_UNDER_CREATION - starting the timer \n");

                KER_EQ_MEM (pEtherStatsNode->etherStatsStatus,
                            i4SetValEtherStatsStatus);

                RmonStartTimer (ETHER_STATS_TABLE, i4EtherStatsIndex,
                                STATS_TABLE_DURATION);
            }
            break;

            /* case RMON_CREATE_REQUEST : The status is automatically changed */
            /* to RMON_UNDER_CREATION after setting the */
            /* default values.                          */
        case RMON_CREATE_REQUEST:
            break;

        case RMON_UNDER_CREATION:

            if ((i4SetValEtherStatsStatus != RMON_CREATE_REQUEST) &&
                (i4SetValEtherStatsStatus != RMON_UNDER_CREATION))
            {
                RMON_DBG (MAJOR_TRC | MEM_TRC,
                          " etherStatsStatus  RMON_UNDER_CREATION to VALID or IN..Stopping the timer \n");

                RmonStopTimer (ETHER_STATS_TABLE, i4EtherStatsIndex);
            }

            /* Check for STATUS == RMON_VALID */
            if (i4SetValEtherStatsStatus == RMON_VALID)
            {
                if (STRLEN (pEtherStatsNode->u1EtherStatsOwner) == 0)
                {
                    /* Create default owner */
                    STRNCPY (&pEtherStatsNode->u1EtherStatsOwner, MONITOR, STRLEN (MONITOR));
                }

                KER_EQ_MEM (pEtherStatsNode->etherStatsStatus,
                            i4SetValEtherStatsStatus);
#if defined(NPAPI_WANTED)
                u1StatsCollectionEnable =
                    (i4SetValEtherStatsStatus ==
                     RMON_VALID) ? FNP_TRUE : FNP_FALSE;
                if (pEtherStatsNode->u4EtherStatsOIDType == PORT_INDEX_TYPE)
                {
                    u4Port = pEtherStatsNode->u4EtherStatsDataSource;
                    if (RmonFsRmonHwSetEtherStatsTable
                        (u4Port, u1StatsCollectionEnable) != FNP_SUCCESS)
                    {
                        RMON_DBG (MAJOR_TRC | MEM_TRC,
                                  "Failed in configuring the HW etherStats table \n");
#ifdef RMON2_WANTED
                        pEtherStatsNode->u4EtherStatsDroppedFrames++;
#endif
                        return SNMP_FAILURE;
                    }
                }
#endif
#ifdef RMON2_WANTED
                pEtherStatsNode->u4EtherStatsCreateTime = OsixGetSysUpTime ();
#endif
            }
            if (i4SetValEtherStatsStatus == RMON_INVALID)
            {
                if (pEtherStatsNode != NULL)
                {
                    if (pEtherStatsNode->u4EtherStatsDataSource > 0)
                    {
                        u4Hindex = (UINT4) RmonHashFn (pEtherStatsNode->
                                                       u4EtherStatsDataSource);

                        RMON_HASH_DEL_NODE (gpRmonEtherStatsHashTable,
                                            &pEtherStatsNode->
                                            nextEtherStatsNode, u4Hindex);
                    }
                    else
                    {
                        TMO_SLL_Delete (&gEtherStatsSLL,
                                        &pEtherStatsNode->nextEtherStatsNode);
                    }
                    MemReleaseMemBlock (gRmonEtherStatsPoolId,
                                        (UINT1 *) pEtherStatsNode);
                    pEtherStatsNode = NULL;
                }
            }
            break;

        case RMON_INVALID:
            if (i4SetValEtherStatsStatus == RMON_INVALID)
            {
                return SNMP_SUCCESS;
            }
            else if (i4SetValEtherStatsStatus == RMON_CREATE_REQUEST)
            {
                pEtherStatsNode->etherStatsStatus = RMON_UNDER_CREATION;

                RMON_DBG (MAJOR_TRC | MEM_TRC,
                          " EtherStatsStatus  INVALID  to RMON_UNDER_CREATION - Starting the Timer \n");

                RmonStartTimer (ETHER_STATS_TABLE, i4EtherStatsIndex,
                                STATS_TABLE_DURATION);
            }
            break;
        case RMON_MAX_STATE:
        default:
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      "SNMP Failure: Entry already exists  \n");
            return SNMP_FAILURE;

    } /***** end of switch ******/
    if(i4SetValEtherStatsStatus == RMON_INVALID)
    {
        i4SetValEtherStatsStatus = DESTROY; 
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = EtherStatsStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (EtherStatsStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4EtherStatsIndex,
                      i4SetValEtherStatsStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2EtherStatsDataSource
 Input       :  The Indices
                EtherStatsIndex

                The Object
                testValEtherStatsDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EtherStatsDataSource (UINT4 *pu4ErrorCode, INT4 i4EtherStatsIndex,
                               tSNMP_OID_TYPE * pTestValEtherStatsDataSource)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    UINT4               u4DataSource = 0;
    UINT4               u4OIDType = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2EtherStatsDataSource \n");

    if (TEST_DATA_SOURCE_OID (pTestValEtherStatsDataSource))
    {
        SET_DATA_SOURCE_OID (pTestValEtherStatsDataSource, &u4DataSource,
                             &u4OIDType);

        pEtherStatsNode =
            HashSearchEtherStatsNode_DSource (gpRmonEtherStatsHashTable,
                                              u4DataSource, u4OIDType);

        if (pEtherStatsNode != NULL)
        {
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      "INVALID DataSource Oid - Entry already exists \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RMON_STATS_CONFIGURED_ERR);
            return SNMP_FAILURE;
        }
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus == RMON_UNDER_CREATION)
        {
            if (TEST_DATA_SOURCE_OID (pTestValEtherStatsDataSource))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;

                return SNMP_SUCCESS;
            }

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RMON_INVALID_OID);
            return SNMP_FAILURE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Check whether the node is present in the Linked List */
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus == RMON_UNDER_CREATION)
                {
                    if (TEST_DATA_SOURCE_OID (pTestValEtherStatsDataSource))
                    {
                        *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                        return SNMP_SUCCESS;
                    }

                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (CLI_RMON_INVALID_OID);
                    return SNMP_FAILURE;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry Status not in RMON_UNDER_CREATION State or INVALID DataSource Oid \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2EtherStatsOwner
 Input       :  The Indices
                EtherStatsIndex

                The Object
                testValEtherStatsOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EtherStatsOwner (UINT4 *pu4ErrorCode, INT4 i4EtherStatsIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValEtherStatsOwner)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2EtherStatsOwner \n");

    if ((pTestValEtherStatsOwner->i4_Length < 0) ||
        (pTestValEtherStatsOwner->i4_Length > RMON_OCTET_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Check whether the node is present in the hash table */
    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        /* Check whether the node is present in the Linked List */
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus == RMON_UNDER_CREATION)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry Status not in RMON_UNDER_CREATION  State \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2EtherStatsStatus
 Input       :  The Indices
                EtherStatsIndex

                The Object
                testValEtherStatsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2EtherStatsStatus (UINT4 *pu4ErrorCode, INT4 i4EtherStatsIndex,
                           INT4 i4TestValEtherStatsStatus)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;
    tRmonStatus         etherStatus = RMON_INVALID;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2EtherStatsStatus \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RMON_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4EtherStatsIndex < 1)
        || (i4EtherStatsIndex > RMON_MAX_ETHERSTATS_INDEX))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: EtherStats Index out of range ");
        return SNMP_FAILURE;
    }

    if ((i4TestValEtherStatsStatus < RMON_VALID) ||
        (i4TestValEtherStatsStatus > RMON_INVALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    /* Check whether the node is present in the Linked List */
    if (pEtherStatsNode == NULL)
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                etherStatus = pEtherStatsNode->etherStatsStatus;
                break;
            }
        }
    }
    else
    {
        etherStatus = pEtherStatsNode->etherStatsStatus;
    }

    /*Check and allocate the memory for the *
     * control entry of the stats table     */

    switch (etherStatus)
    {
        case RMON_VALID:
            if (i4TestValEtherStatsStatus != RMON_CREATE_REQUEST)
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;

        case RMON_UNDER_CREATION:
            if (i4TestValEtherStatsStatus == RMON_VALID)
            {
                if (pEtherStatsNode->u4EtherStatsDataSource != 0)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                if (i4TestValEtherStatsStatus != RMON_CREATE_REQUEST)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }

                RMON_DBG (MINOR_TRC | FUNC_ENTRY,
                          "Entry Already  in RMON_UNDER_CREATION State \n");

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            break;
        case RMON_INVALID:
            if (i4TestValEtherStatsStatus == RMON_CREATE_REQUEST)
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            break;

            /* case RMON_CREATE_REQUEST : The status is automatically changed */
            /* to RMON_UNDER_CREATION after setting the */
            /* default values.                     */
        case RMON_CREATE_REQUEST:
            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Error RMON_CREATE_REQUEST ? \n");
            break;
        case RMON_MAX_STATE:
        default:
            break;
    } /*****  end of switch  *****/

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Unknown Entry Status \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2EtherStatsTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2EtherStatsTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : EtherStatsHighCapacityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEtherStatsHighCapacityTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceEtherStatsHighCapacityTable(INT4 i4EtherStatsIndex)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entering into the function, \
              nmhValidateIndexInstanceEtherStatsHighCapacityTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4EtherStatsIndex < RMON_ONE) ||
        (i4EtherStatsIndex > RMON_MAX_ETHERSTATS_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Unknown Interface \n");
        return SNMP_FAILURE;
    }

    if (HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                  i4EtherStatsIndex) != NULL)
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC, "SNMP Success: Valid Index \n");
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "SNMP Success: Valid Index \n");
                return SNMP_SUCCESS;
            }
        }
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC, "SNMP Failure: Unknown Interface \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexEtherStatsHighCapacityTable
 Input       :  The Indices
                EtherStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexEtherStatsHighCapacityTable(INT4 *pi4EtherStatsIndex)
{
    INT4                i4Zero = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetFirstIndexEtherStatsHighCapacityTable \n");

    return nmhGetNextIndexEtherStatsHighCapacityTable (i4Zero, pi4EtherStatsIndex);
}
/****************************************************************************
 Function    :  nmhGetNextIndexEtherStatsHighCapacityTable
 Input       :  The Indices
                EtherStatsIndex
                nextEtherStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexEtherStatsHighCapacityTable(INT4 i4EtherStatsIndex ,
                                                INT4 *pi4NextEtherStatsIndex)
{
    INT4                i4Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetNextIndexEtherStatsHighCapacityTable \n");

    if ((i4EtherStatsIndex < RMON_ZERO) ||
        (i4EtherStatsIndex > RMON_MAX_ETHERSTATS_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid Index - out of range\n");
        return SNMP_FAILURE;
    }

    i4Index =
        HashSearchEtherStatsNode_NextValidTindex (gpRmonEtherStatsHashTable,
                                                  i4EtherStatsIndex);

    if ((i4Index != RMON_ZERO) &&
        (i4Index > i4EtherStatsIndex) && (i4Index <= RMON_MAX_ETHERSTATS_INDEX))
    {
        *pi4NextEtherStatsIndex = i4Index;
        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: NO Next VALID Index \n");
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts(INT4 i4EtherStatsIndex, 
                                              UINT4 
                                  *pu4RetValEtherStatsHighCapacityOverflowPkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts.msn);

                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts(INT4 i4EtherStatsIndex, 
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValEtherStatsHighCapacityPkts)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowOctets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowOctets(INT4 i4EtherStatsIndex, 
                                                UINT4 
                                *pu4RetValEtherStatsHighCapacityOverflowOctets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowOctets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowOctets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
             MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowOctets,
                         pEtherStatsNode->RmonEtherStatsPkts.
                         u8EtherStatsHighCapacityOctets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowOctets,
                                 pEtherStatsNode->RmonEtherStatsPkts.
                                 u8EtherStatsHighCapacityOctets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOctets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOctets(INT4 i4EtherStatsIndex, 
                                        tSNMP_COUNTER64_TYPE 
                                        *pu8RetValEtherStatsHighCapacityOctets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOctets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityOctets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));        
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityOctets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityOctets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityOctets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityOctets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityOctets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityOctets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityOctets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityOctets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts64Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts64Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts64Octets(INT4 i4EtherStatsIndex,
                                                      UINT4 
                          *pu4RetValEtherStatsHighCapacityOverflowPkts64Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts64Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts64Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts64Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts64Octets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts64Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts64Octets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts64Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts64Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts64Octets(INT4 i4EtherStatsIndex, 
                                              tSNMP_COUNTER64_TYPE 
                                  *pu8RetValEtherStatsHighCapacityPkts64Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts64Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts64Octets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts64Octets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts64Octets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts64Octets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts64Octets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts64Octets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts64Octets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts64Octets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts64Octets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID\n");

    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts65to127Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets(INT4 i4EtherStatsIndex, 
                                                           UINT4 
                        *pu4RetValEtherStatsHighCapacityOverflowPkts65to127Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts65to127Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts65to127Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts65to127Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts65to127Octets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts65to127Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts65to127Octets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts65to127Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts65to127Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts65to127Octets(INT4 i4EtherStatsIndex, 
                                                   tSNMP_COUNTER64_TYPE 
                             *pu8RetValEtherStatsHighCapacityPkts65to127Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts65to127Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts65to127Octets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts65to127Octets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts65to127Octets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts65to127Octets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts65to127Octets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts65to127Octets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts65to127Octets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts65to127Octets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts65to127Octets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts128to255Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets(INT4 i4EtherStatsIndex,
                                                            UINT4 
                        *pu4RetValEtherStatsHighCapacityOverflowPkts128to255Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts128to255Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts128to255Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts128to255Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts128to255Octets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts128to255Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts128to255Octets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts128to255Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts128to255Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts128to255Octets(INT4 i4EtherStatsIndex, 
                                                    tSNMP_COUNTER64_TYPE 
                            *pu8RetValEtherStatsHighCapacityPkts128to255Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts128to255Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts128to255Octets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts128to255Octets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts128to255Octets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts128to255Octets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts128to255Octets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts128to255Octets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts128to255Octets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts128to255Octets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts128to255Octets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts256to511Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets(INT4 
                                                            i4EtherStatsIndex, 
                                                            UINT4 
                    *pu4RetValEtherStatsHighCapacityOverflowPkts256to511Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts256to511Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts256to511Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts256to511Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts256to511Octets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts256to511Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts256to511Octets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts256to511Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts256to511Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts256to511Octets(INT4 i4EtherStatsIndex, 
                                                    tSNMP_COUNTER64_TYPE 
                            *pu8RetValEtherStatsHighCapacityPkts256to511Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts256to511Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts256to511Octets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts256to511Octets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts256to511Octets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts256to511Octets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts256to511Octets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts256to511Octets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts256to511Octets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts256to511Octets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts256to511Octets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts512to1023Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets(INT4 
                                                             i4EtherStatsIndex, 
                                                             UINT4 
                   *pu4RetValEtherStatsHighCapacityOverflowPkts512to1023Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts512to1023Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts512to1023Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts512to1023Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts512to1023Octets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts512to1023Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts512to1023Octets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts512to1023Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts512to1023Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts512to1023Octets(INT4 i4EtherStatsIndex, 
                                                     tSNMP_COUNTER64_TYPE 
                           *pu8RetValEtherStatsHighCapacityPkts512to1023Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts512to1023Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts512to1023Octets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts512to1023Octets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts512to1023Octets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts512to1023Octets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts512to1023Octets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts512to1023Octets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts512to1023Octets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts512to1023Octets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts512to1023Octets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityOverflowPkts1024to1518Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets(INT4 
                                                              i4EtherStatsIndex,
                                                              UINT4 
                   *pu4RetValEtherStatsHighCapacityOverflowPkts1024to1518Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityOverflowPkts1024to1518Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        *pu4RetValEtherStatsHighCapacityOverflowPkts1024to1518Octets = 0;
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts1024to1518Octets,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts1024to1518Octets.msn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (*pu4RetValEtherStatsHighCapacityOverflowPkts1024to1518Octets,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts1024to1518Octets.msn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetEtherStatsHighCapacityPkts1024to1518Octets
 Input       :  The Indices
                EtherStatsIndex

                The Object
                retValEtherStatsHighCapacityPkts1024to1518Octets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetEtherStatsHighCapacityPkts1024to1518Octets(INT4 i4EtherStatsIndex, 
                                                      tSNMP_COUNTER64_TYPE 
                          *pu8RetValEtherStatsHighCapacityPkts1024to1518Octets)
{
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function"
              " nmhGetEtherStatsHighCapacityPkts1024to1518Octets \n");

    if (!(RMON_IS_ENABLED ()))
    {
        MEMSET (pu8RetValEtherStatsHighCapacityPkts1024to1518Octets, 0,
                sizeof (tSNMP_COUNTER64_TYPE));
        return SNMP_SUCCESS;
    }

    pEtherStatsNode = HashSearchEtherStatsNode (gpRmonEtherStatsHashTable,
                                                i4EtherStatsIndex);

    if (pEtherStatsNode != NULL)
    {
        if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
        {
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts1024to1518Octets->msn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts1024to1518Octets.msn);
            MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts1024to1518Octets->lsn,
                        pEtherStatsNode->RmonEtherStatsPkts.
                        u8EtherStatsHighCapacityPkts1024to1518Octets.lsn);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gEtherStatsSLL, pEtherStatsNode,
                      tRmonEtherStatsTimerNode *)
        {
            if (pEtherStatsNode->i4EtherStatsIndex == i4EtherStatsIndex)
            {
                if (pEtherStatsNode->etherStatsStatus != RMON_INVALID)
                {
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts1024to1518Octets->msn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts1024to1518Octets.msn);
                    MEM_EQ_KER (pu8RetValEtherStatsHighCapacityPkts1024to1518Octets->lsn,
                                pEtherStatsNode->RmonEtherStatsPkts.
                                u8EtherStatsHighCapacityPkts1024to1518Octets.lsn);
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Index not available OR Entry State is not VALID \n");
    return SNMP_FAILURE;
}

