/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmonsz.c,v 1.3 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RMONSZ_C
#include "rmallinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RmonSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RMON_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsRMONSizingParams[i4SizingId].u4StructSize,
                              FsRMONSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(RMONMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RmonSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RmonSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRMONSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RMONMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RmonSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RMON_MAX_SIZING_ID; i4SizingId++)
    {
        if (RMONMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RMONMemPoolIds[i4SizingId]);
            RMONMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
