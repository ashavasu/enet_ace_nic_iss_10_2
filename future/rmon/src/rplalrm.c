/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rplalrm.c,v 1.50 2015/11/30 10:26:27 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP Low level routine implementation for  *
 *              Alarm Group.                                        *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rplalrm.c
 *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.
 *
 *    SUBSYSTEM NAME         :  LOW LEVEL CODE
 *
 *    MODULE NAME            :  RMON PERIODIC
 *
 *    LANGUAGE               :  C
 *
 *    TARGET ENVIRONMENT     :  ANY
 *
 *    DATE OF FIRST RELEASE  :
 *
 *    DESCRIPTION            : This file provides the low level routines
 *                             for alarm group.   
 *
 *
 *  --------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |         DESCRIPTION OF
 *                  |        DATE          |            CHANGE
 * -----------------|----------------------|---------------------------------
 *     1.0          |  Vijay G. Krishnan   |        Create Original
 *                  |    22-APR-1997       |
*----------------------------------------------------------------------
*     2.0           | Bhupendra Sharma     |                       
*                   |  13-AUG-2001         |                       
*
* --------------------------------------------------------------------------
*     3.0           | LEELA L              |  Changes for Dynamic Tables       
*                   |  31-MAR-2002         |                       
* --------------------------------------------------------------------------
 */

#include "rmallinc.h"

#include "cli.h"
#include "rmoncli.h"
#include "rmgr.h"
extern UINT4        AlarmStatus[11];

 /*
  * LOW LEVEL Routines for Table : alarmTable.
  */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceAlarmTable
 Input       :  The Indices
                i4AlarmIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceAlarmTable (INT4 i4AlarmIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function : \
                 nmhValidateIndexInstanceAlarmTable  \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4AlarmIndex >= 1) && (i4AlarmIndex <= RMON_MAX_ALARM_INDEX) &&
        (HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex) != NULL))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: Alarm entry validation is success.\n");
        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Not a valid Alarm Index \n");
        return SNMP_FAILURE;
    }
}

 /*
  * GET_FIRST Routine.
  */
/****************************************************************************
 Function    :  nmhGetFirstIndexAlarmTable
 Input       :  The Indices
                pi4AlarmIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexAlarmTable (INT4 *pi4AlarmIndex)
{
    INT4                i4Zero = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetFirstIndexAlarmTable  \n");

    return nmhGetNextIndexAlarmTable (i4Zero, pi4AlarmIndex);
}

 /*
  * GET_NEXT Routine.
  */

/****************************************************************************
 Function    :  nmhGetNextIndexAlarmTable
 Input       :  The Indices
                i4AlarmIndex
                pi4NextAlarmIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexAlarmTable (INT4 i4AlarmIndex, INT4 *pi4NextAlarmIndex)
{
    INT4                i4Index = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetNextIndexAlarmTable  \n");

    if ((i4AlarmIndex < RMON_ZERO) || (i4AlarmIndex > RMON_MAX_ALARM_INDEX))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Invalid Index - out of range\n");
        return SNMP_FAILURE;
    }

    i4Index = HashSearchAlarmNode_NextValidTindex (gpRmonAlarmHashTable,
                                                   i4AlarmIndex);

    if ((i4Index != RMON_ZERO) && (i4Index > i4AlarmIndex) &&
        (i4Index <= RMON_MAX_ALARM_INDEX))
    {
        *pi4NextAlarmIndex = i4Index;
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Alarm index is outof range \n");

    return SNMP_FAILURE;
}

 /*
  * Low Level GET Routine for All Objects
  */
/****************************************************************************
 Function    :  nmhGetAlarmInterval
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmInterval (INT4 i4AlarmIndex, INT4 *pi4RetValAlarmInterval)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetAlarmInterval \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmInterval = pAlarmNode->i4AlarmInterval;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmVariable
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pRetValAlarmVariable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmVariable (INT4 i4AlarmIndex, tSNMP_OID_TYPE * pRetValAlarmVariable)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG1 (MINOR_TRC | FUNC_ENTRY,
               " Entering into the function : nmhGetAlarmVariable, \
                i4AlarmIndex = %d \n", i4AlarmIndex);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            if ((pAlarmNode->alarmVariable.pu4_OidList != NULL)
                && (pAlarmNode->alarmVariable.u4_Length != 0))
            {
                pRetValAlarmVariable->u4_Length =
                    pAlarmNode->alarmVariable.u4_Length;

                MEMCPY (pRetValAlarmVariable->pu4_OidList,
                        pAlarmNode->alarmVariable.pu4_OidList,
                        (pAlarmNode->alarmVariable.u4_Length * sizeof (UINT4)));

                pRetValAlarmVariable->u4_Length =
                    pAlarmNode->alarmVariable.u4_Length;

                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "Get Alarm Variable Success.\n");
            }
            else
            {
                /* returns the AlarmVariable length as zero.
                 * This step is taken because a proper row is present,
                 * but proper Alarm variable is not present. 
                 */
                pRetValAlarmVariable->u4_Length = 0;

            }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetAlarmSampleType
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmSampleType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmSampleType (INT4 i4AlarmIndex, INT4 *pi4RetValAlarmSampleType)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG1 (MINOR_TRC | FUNC_ENTRY,
               " Entering into the function : nmhGetAlarmSampleType, \
                   i4AlarmIndex =%d \n", i4AlarmIndex);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmSampleType = pAlarmNode->alarmSampleType;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmValue
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmValue (INT4 i4AlarmIndex, INT4 *pi4RetValAlarmValue)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG1 (MINOR_TRC | FUNC_ENTRY,
               " Entering into the function : nmhGetAlarmValue, \
                   i4AlarmIndex = %d \n", i4AlarmIndex);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmValue = (INT4) pAlarmNode->u4PrevIntervalValue;

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Get Alarm Value - Success\n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmStartupAlarm
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmStartupAlarm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmStartupAlarm (INT4 i4AlarmIndex, INT4 *pi4RetValAlarmStartupAlarm)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG1 (MINOR_TRC | FUNC_ENTRY,
               " Entering into the function :  nmhGetAlarmStartupAlarm, \
                      i4AlarmIndex = %d \n", i4AlarmIndex);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmStartupAlarm = pAlarmNode->i4AlarmStartupAlarm;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmRisingThreshold
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4RetValAlarmRisingThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetAlarmRisingThreshold (INT4 i4AlarmIndex,
                            INT4 *pi4RetValAlarmRisingThreshold)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG1 (MINOR_TRC | FUNC_ENTRY,
               " Entering into the function :  nmhGetAlarmRisingThreshold, \
                  i4AlarmIndex = %d \n", i4AlarmIndex);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmRisingThreshold =
                (INT4) pAlarmNode->u4AlarmRisingThreshold;

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Get Alarm Rising Threshold - Success\n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmFallingThreshold
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmFallingThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmFallingThreshold (INT4 i4AlarmIndex,
                             INT4 *pi4RetValAlarmFallingThreshold)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetAlarmFallingThreshold  \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmFallingThreshold =
                (INT4) pAlarmNode->u4AlarmFallingThreshold;

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Get Alarm Falling Threshold - Success \n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmRisingEventIndex
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmRisingEventIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmRisingEventIndex (INT4 i4AlarmIndex,
                             INT4 *pi4RetValAlarmRisingEventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetAlarmRisingEventIndex \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmRisingEventIndex =
                pAlarmNode->i4AlarmRisingEventIndex;

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Get Alarm Rising Event Index - Success \n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmFallingEventIndex
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmFallingEventIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmFallingEventIndex (INT4 i4AlarmIndex,
                              INT4 *pi4RetValAlarmFallingEventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetAlarmFallingEventIndex \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmFallingEventIndex =
                pAlarmNode->i4AlarmFallingEventIndex;

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Get Alarm Falling Event Index - Success \n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmOwner
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pRetValAlarmOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmOwner (INT4 i4AlarmIndex,
                  tSNMP_OCTET_STRING_TYPE * pRetValAlarmOwner)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function :  pRetValAlarmOwner \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            STRNCPY (pRetValAlarmOwner->pu1_OctetList,
                     pAlarmNode->u1AlarmOwner, OWN_STR_LENGTH);

            pRetValAlarmOwner->i4_Length =
                (INT4) STRLEN (pAlarmNode->u1AlarmOwner);

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Get Alarm Owner - Success\n");
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetAlarmStatus
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pi4RetValAlarmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetAlarmStatus (INT4 i4AlarmIndex, INT4 *pi4RetValAlarmStatus)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhGetAlarmStatus \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus != RMON_INVALID)
        {
            *pi4RetValAlarmStatus = pAlarmNode->alarmStatus;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhSetAlarmInterval
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmInterval (INT4 i4AlarmIndex, INT4 i4SetValAlarmInterval)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmInterval \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        pAlarmNode->i4AlarmInterval = i4SetValAlarmInterval;
        pAlarmNode->i4TimeToMonitor = i4SetValAlarmInterval;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmInterval \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmVariable
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pSetValAlarmVariable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmVariable (INT4 i4AlarmIndex, tSNMP_OID_TYPE * pSetValAlarmVariable)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmVariable  \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmVariable.pu4_OidList == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      "Malloc Failure in alarmVariable  \n");
            return SNMP_FAILURE;
        }

        pAlarmNode->alarmVariable.u4_Length = pSetValAlarmVariable->u4_Length;

        MEMCPY (pAlarmNode->alarmVariable.pu4_OidList,
                pSetValAlarmVariable->pu4_OidList,
                (pAlarmNode->alarmVariable.u4_Length * sizeof (UINT4)));
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmVariable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmSampleType
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmSampleType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmSampleType (INT4 i4AlarmIndex, INT4 i4SetValAlarmSampleType)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmSampleType  \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        pAlarmNode->alarmSampleType = i4SetValAlarmSampleType;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmSampleType \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmStartupAlarm
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmStartupAlarm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmStartupAlarm (INT4 i4AlarmIndex, INT4 i4SetValAlarmStartupAlarm)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmStartupAlarm \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);
    if (pAlarmNode != NULL)
    {
        pAlarmNode->i4AlarmStartupAlarm = i4SetValAlarmStartupAlarm;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmStartupAlarm \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmRisingThreshold
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmRisingThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmRisingThreshold (INT4 i4AlarmIndex,
                            INT4 i4SetValAlarmRisingThreshold)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmRisingThreshold \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);
    if (pAlarmNode != NULL)
    {
        pAlarmNode->u4AlarmRisingThreshold =
            (UINT4) i4SetValAlarmRisingThreshold;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmRisingThreshold \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmFallingThreshold
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmFallingThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmFallingThreshold (INT4 i4AlarmIndex,
                             INT4 i4SetValAlarmFallingThreshold)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmFallingThreshold \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);
    if (pAlarmNode != NULL)
    {
        pAlarmNode->u4AlarmFallingThreshold =
            (UINT4) i4SetValAlarmFallingThreshold;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmFallingThreshold \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmRisingEventIndex
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmRisingEventIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmRisingEventIndex (INT4 i4AlarmIndex,
                             INT4 i4SetValAlarmRisingEventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmRisingEventIndex  \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);
    if (pAlarmNode != NULL)
    {
        pAlarmNode->i4AlarmRisingEventIndex = i4SetValAlarmRisingEventIndex;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmRisingEventIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmFallingEventIndex
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmFallingEventIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmFallingEventIndex (INT4 i4AlarmIndex,
                              INT4 i4SetValAlarmFallingEventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmFallingEventIndex  \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        pAlarmNode->i4AlarmFallingEventIndex = i4SetValAlarmFallingEventIndex;
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmFallingEventIndex \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmOwner
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pSetValAlarmOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmOwner (INT4 i4AlarmIndex, tSNMP_OCTET_STRING_TYPE
                  * pSetValAlarmOwner)
{
    tRmonAlarmNode     *pAlarmNode = NULL;
    UINT4               u4Minimum;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmOwner \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    u4Minimum = (UINT4) ((pSetValAlarmOwner->i4_Length < OWN_STR_LENGTH) ?
                         pSetValAlarmOwner->i4_Length : OWN_STR_LENGTH);
    if (pAlarmNode != NULL)
    {
        MEMSET (pAlarmNode->u1AlarmOwner, RMON_ZERO, OWN_STR_LENGTH);
        STRNCPY (pAlarmNode->u1AlarmOwner,
                 pSetValAlarmOwner->pu1_OctetList, u4Minimum);
    }
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function : nmhSetAlarmOwner \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetAlarmStatus
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4SetValAlarmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetAlarmStatus (INT4 i4AlarmIndex, INT4 i4SetValAlarmStatus)
{
    tRmonAlarmNode     *pAlarmNode = NULL;
    tRmonStatus        *pStatus = NULL;
    UINT4               u4Value = 0;
    UINT4               u4Hindex;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhSetAlarmStatus  \n");

    if ((i4AlarmIndex <= 0) || (i4AlarmIndex > RMON_MAX_ALARM_INDEX))
    {
        CLI_SET_ERR (CLI_RMON_INVALID_ALARM_INDEX_ERR);
	return SNMP_FAILURE;
    }
    /*Check and allocate the memory for the *
     * control entry of the Alarm table     */

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if ((pAlarmNode == NULL) && (i4SetValAlarmStatus == RMON_CREATE_REQUEST))
    {
        pAlarmNode = (tRmonAlarmNode *) MemAllocMemBlk (gRmonAlarmPoolId);
        if (pAlarmNode == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " Memory Allocation Failure - Alarm Table. \n");
            CLI_SET_ERR (CLI_RMON_MAX_ALARM);
            return SNMP_FAILURE;
        }

        pAlarmNode->alarmStatus = RMON_INVALID;

        pAlarmNode->i4AlarmIndex = i4AlarmIndex;
        pAlarmNode->u4IsAlarmOnce = RMON_FALSE;
        pAlarmNode->u1FirstAlarm = RMON_TRUE;
        MEMSET (pAlarmNode->u1AlarmOwner, 0, OWN_STR_LENGTH);

        u4Hindex = (UINT4) RmonHashFn ((UINT4) pAlarmNode->i4AlarmIndex);

        pAlarmNode->nextAlarmEntryNode.pNext = NULL;

        RMON_HASH_ADD_NODE (gpRmonAlarmHashTable,
                            &pAlarmNode->nextAlarmEntryNode, u4Hindex, NULL);
    }
    else if (pAlarmNode == NULL)
    {
         CLI_SET_ERR (CLI_RMON_ALARM_ENTRY_NOTEXISTS_ERR);
	 return SNMP_FAILURE;
    }

    pStatus = &(pAlarmNode->alarmStatus);

    switch (*pStatus)
    {
        case RMON_VALID:
            if (i4SetValAlarmStatus == RMON_INVALID)
            {
                /* Memory pointers used for Alarm variable are freed here */
                if (pAlarmNode->alarmVariable.pu4_OidList != NULL)
                {
                    MemReleaseMemBlock (gRmonAlarmVarPoolId, (UINT1 *)
                                        pAlarmNode->alarmVariable.pu4_OidList);
                    pAlarmNode->alarmVariable.pu4_OidList = NULL;
                }

                u4Hindex =
                    (UINT4) RmonHashFn ((UINT4) pAlarmNode->i4AlarmIndex);

                RmonStopTimer (ALARM_TABLE, i4AlarmIndex);

                RMON_HASH_DEL_NODE (gpRmonAlarmHashTable,
                                    &pAlarmNode->nextAlarmEntryNode, u4Hindex);

                /* Free the Alarm entry */
                MemReleaseMemBlock (gRmonAlarmPoolId, (UINT1 *) pAlarmNode);
                pAlarmNode = NULL;

                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "Alarm Table Entry Switches from VALID to INVALID \n");
            }
            else if (i4SetValAlarmStatus == RMON_UNDER_CREATION)
            {

                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "Alarm Table Entry Switches from VALID to \
                           UNDER_CREATION and Starting the Timer \n\n");

                /* Assign the Status & Starting timer to delete entries 
                 * not made valid in given time */
                *pStatus = i4SetValAlarmStatus;
                RmonStartTimer (ALARM_TABLE, i4AlarmIndex, ALARM_DURATION);
            }

            break;

        case RMON_INVALID:
            *pStatus = i4SetValAlarmStatus;
            if (i4SetValAlarmStatus == RMON_CREATE_REQUEST)
            {
                /* Setting all default values here */
                pAlarmNode->alarmSampleType = ABSOLUTE_VALUE;

                pAlarmNode->i4AlarmStartupAlarm = RMON_RISING_OR_FALLING_ALARM;
                pAlarmNode->i4AlarmInterval = RMON_ONE_MINUTE;

                pAlarmNode->alarmVariable.pu4_OidList =
                    (UINT4 *) MemAllocMemBlk (gRmonAlarmVarPoolId);

                if (pAlarmNode->alarmVariable.pu4_OidList == NULL)
                {
                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              "Malloc Failure : for alarmVariable \n");
                    CLI_SET_ERR (CLI_RMON_MEMORY_ALLOCATION_FAILURE);
		    return SNMP_FAILURE;
                }

                *pStatus = RMON_UNDER_CREATION;

                /* Starting timer to delete entries not made valid in given 
                   time */
                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "Alarm Table Entry Switches from INVALID to \
                        RMON_UNDER_CREATION and Starting the Timer \n\n");

                RmonStartTimer (ALARM_TABLE, i4AlarmIndex, ALARM_DURATION);
            }
            break;

        case RMON_UNDER_CREATION:

            if (i4SetValAlarmStatus == RMON_VALID)
            {
                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "Alarm Table Entry Switches from  UNDER_CREATION to \
                      VALID and Stopping the Timer \n\n");

                RmonStopTimer (ALARM_TABLE, i4AlarmIndex);

                pAlarmNode->u4IsAlarmOnce = RMON_FALSE;

                if (pAlarmNode->alarmVariable.pu4_OidList != NULL)
                {
                    RmonGetMibVariableValue (&(pAlarmNode->
                                               alarmVariable), &u4Value);

                    RMON_DBG1 (MINOR_TRC | RMON_MGMT_TRC,
                               "DURING  UNDER_CREATION Alarm variable Value = %d \n\n",
                               u4Value);

                    if (pAlarmNode->alarmSampleType == ABSOLUTE_VALUE)
                    {
                        pAlarmNode->u4AlarmValue = u4Value;
                        pAlarmNode->u4PrevIntervalValue = u4Value;
                    }

                    /* if AlarmSampleType is Delta type then */
                    else
                    {
                        pAlarmNode->u4PreviousValue = u4Value;
                        pAlarmNode->u4PrevIntervalValue = u4Value;
                    }
                    /* write the Status */
                    *pStatus = i4SetValAlarmStatus;

                    /* Enable the Global BOOLEAN to represent a VALID Alarm */
                    grmprB1AnyValidAlarm = TRUE;
                }

                else
                {
                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                              "Alarm Variable is not configured.\n\n");
                    CLI_SET_ERR (CLI_RMON_ALARM_NOT_CONFIGURED);
		    return SNMP_FAILURE;

                }
            }
            else if (i4SetValAlarmStatus == RMON_INVALID)
            {
                /* If STATUS is INVALID, stop the timer & free the memory */
                RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                          "Alarm Table Entry Switches from UNDER_CREATION to \
                        INVALID and Stopping the Timer \n\n");
                RmonStopTimer (ALARM_TABLE, i4AlarmIndex);

                /* Memory pointers used for Alarm variable are freed here */
                if (pAlarmNode->alarmVariable.pu4_OidList != NULL)
                {
                    MemReleaseMemBlock (gRmonAlarmVarPoolId, (UINT1 *)
                                        pAlarmNode->alarmVariable.pu4_OidList);
                    pAlarmNode->alarmVariable.pu4_OidList = NULL;
                }

                u4Hindex =
                    (UINT4) RmonHashFn ((UINT4) pAlarmNode->i4AlarmIndex);

                RMON_HASH_DEL_NODE (gpRmonAlarmHashTable,
                                    &pAlarmNode->nextAlarmEntryNode, u4Hindex);

                /* Free the Alarm entry */
                MemReleaseMemBlock (gRmonAlarmPoolId, (UINT1 *) pAlarmNode);
                pAlarmNode = NULL;
            }
            break;

        case RMON_CREATE_REQUEST:
        case RMON_MAX_STATE:
        default:

            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Status \n");
            CLI_SET_ERR (CLI_RMON_INVALID_STATUS_ERR);
	    return SNMP_FAILURE;
    }                            /*  end of switch  */

    return SNMP_SUCCESS;
}

 /*
  * Low Level TEST Routines for All Objects
  */

/****************************************************************************
 Function    :  nmhTestv2AlarmInterval
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmInterval (UINT4 *pu4ErrorCode, INT4 i4AlarmIndex,
                        INT4 i4TestValAlarmInterval)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmInterval  \n");

    RMON_DUMMY (i4TestValAlarmInterval);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Alarm Index  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmVariable
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pTestValAlarmVariable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc 1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmVariable (UINT4 *pu4ErrorCode, INT4 i4AlarmIndex,
                        tSNMP_OID_TYPE * pTestValAlarmVariable)
{
    UINT4               u4Value;
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmVariable \n");
    RMON_DUMMY (pTestValAlarmVariable);

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if ((pAlarmNode->alarmStatus == RMON_UNDER_CREATION) &&
            (RmonGetMibVariableValue (pTestValAlarmVariable, &u4Value)
             != RMON_INVALID_OID))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        else
        {
            RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                      "Request is made with Invalid Alarm Variable \n");
            CLI_SET_ERR (CLI_RMON_ALARM_INVALID_MIBID_ERR);
        }
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Alarm Index  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmSampleType
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmSampleType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc 1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmSampleType (UINT4 *pu4ErrorCode, INT4 i4AlarmIndex,
                          INT4 i4TestValAlarmSampleType)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmSampleType \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValAlarmSampleType > 0) &&
                (i4TestValAlarmSampleType < RMON_RISING_OR_FALLING_ALARM))
            {
                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else
            {
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          "Invalid Alarm Sample Type \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }                        /* end outer if */
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid Alarm Index:  Status not in UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmStartupAlarm
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmStartupAlarm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc 1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmStartupAlarm (UINT4 *pu4ErrorCode,
                            INT4 i4AlarmIndex, INT4 i4TestValAlarmStartupAlarm)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmStartupAlarm \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValAlarmStartupAlarm > 0) &&
                (i4TestValAlarmStartupAlarm < RMON_ALARM_LIMIT))
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else
            {
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          "Invalid AlarmStartupAlarm value \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid AlarmIndex: Status not UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmRisingThreshold
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmRisingThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmRisingThreshold (UINT4 *pu4ErrorCode,
                               INT4 i4AlarmIndex,
                               INT4 i4TestValAlarmRisingThreshold)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmRisingThreshold  \n");

    /* Testing the input for negative value alone is sufficient, because
     * RMON allows rising theshold configuration till the maximum value
     * of interger 32 */
    if (i4TestValAlarmRisingThreshold < RMON_MIN_RISING_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "Status is not in UNDER_CREATION state\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmFallingThreshold
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmFallingThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmFallingThreshold (UINT4 *pu4ErrorCode,
                                INT4 i4AlarmIndex,
                                INT4 i4TestValAlarmFallingThreshold)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmFallingThreshold  \n");

    /* Testing the input for negative value alone is sufficient, because
     * RMON allows falling theshold configuration till the maximum value
     * of interger 32 */
    if (i4TestValAlarmFallingThreshold < RMON_MIN_FALLING_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid Alarm Index: Status not in UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmRisingEventIndex
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmRisingEventIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmRisingEventIndex (UINT4 *pu4ErrorCode,
                                INT4 i4AlarmIndex,
                                INT4 i4TestValAlarmRisingEventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmRisingEventIndex \n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValAlarmRisingEventIndex == 0) ||
                ((i4TestValAlarmRisingEventIndex > 0) &&
                 (i4TestValAlarmRisingEventIndex <= 65535) &&
                 (HashSearchEventNode (gpRmonEventHashTable,
                                       i4TestValAlarmRisingEventIndex) !=
                  NULL)))
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_RMON_EVENT_NOT_CONFIGURED_ERR);
                return SNMP_FAILURE;
            }
        }                        /* end outer if */
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid Alarm Index: Status not in UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmFallingEventIndex
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmFallingEventIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmFallingEventIndex (UINT4 *pu4ErrorCode,
                                 INT4 i4AlarmIndex,
                                 INT4 i4TestValAlarmFallingEventIndex)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function nmhTestv2AlarmFallingEventIndex\n");

    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            if ((i4TestValAlarmFallingEventIndex == 0) ||
                ((i4TestValAlarmFallingEventIndex > 0) &&
                 (i4TestValAlarmFallingEventIndex <= 65535) &&
                 (HashSearchEventNode (gpRmonEventHashTable,
                                       i4TestValAlarmFallingEventIndex) !=
                  NULL)))
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_RMON_EVENT_NOT_CONFIGURED_ERR);
                return SNMP_FAILURE;
            }
        }                        /* end outer if */
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid Alarm Index: Status not in UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmOwner
 Input       :  The Indices
                i4AlarmIndex

                The Object
                pTestValAlarmOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmOwner (UINT4 *pu4ErrorCode, INT4 i4AlarmIndex,
                     tSNMP_OCTET_STRING_TYPE * pTestValAlarmOwner)
{
    tRmonAlarmNode     *pAlarmNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmOwner \n");

    /* ST_SNMP_ADD */
    if ((pTestValAlarmOwner->i4_Length < 0) ||
        (pTestValAlarmOwner->i4_Length > RMON_OCTET_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

    if (pAlarmNode != NULL)
    {
        if (pAlarmNode->alarmStatus == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Invalid Alarm Index: Status not in UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2AlarmStatus
 Input       :  The Indices
                i4AlarmIndex

                The Object
                i4TestValAlarmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2AlarmStatus (UINT4 *pu4ErrorCode, INT4 i4AlarmIndex,
                      INT4 i4TestValAlarmStatus)
{
    tRmonAlarmNode     *pAlarmNode = NULL;
    tRmonEventNode     *pEventNode = NULL;
    INT4                i4Index = 0;
    INT4                i4EventIndex = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function : nmhTestv2AlarmStatus  \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RMON_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValAlarmStatus < RMON_VALID) ||
        (i4TestValAlarmStatus > RMON_INVALID))
    {
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  " Invalid Alarm Status value \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

	/* Check to avoid mapping alaram rise/fall index to non-active
	event index */
    i4Index = HashSearchEventNode_NextValidTindex (gpRmonEventHashTable,
                                                   i4EventIndex);
    pEventNode = HashSearchEventNode (gpRmonEventHashTable, i4Index);

    if (i4TestValAlarmStatus != RMON_INVALID)
    {
           if ((pEventNode == NULL) ||
               (pEventNode->eventStatus == RMON_UNDER_CREATION))
           {
                   *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                   return SNMP_FAILURE;
           }
    }


    /*Check and allocate the memory for the *
     * control entry of the Alarm table     */

    if ((i4AlarmIndex > 0) && (i4AlarmIndex <= RMON_MAX_ALARM_INDEX))
    {
        pAlarmNode = HashSearchAlarmNode (gpRmonAlarmHashTable, i4AlarmIndex);

        if ((pAlarmNode == NULL) &&
            (i4TestValAlarmStatus == RMON_CREATE_REQUEST))
        {
            /* this scenario will be taken care in Set routine */
            return SNMP_SUCCESS;

        }
        else if (pAlarmNode == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RMON_INVALID_ALARM_INDEX_ERR);
            return SNMP_FAILURE;
        }

       if (i4TestValAlarmStatus != RMON_INVALID)
       {
               if ((pAlarmNode->i4AlarmRisingEventIndex == 0)
                   || (pAlarmNode->i4AlarmFallingEventIndex == 0))
               {
                       *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                       return SNMP_FAILURE;
               }
       }

        if (i4TestValAlarmStatus == RMON_VALID)
        {
            if ((pAlarmNode->i4AlarmInterval == 0) ||
                (pAlarmNode->alarmVariable.pu4_OidList == NULL) ||
                (pAlarmNode->alarmVariable.u4_Length == 0) ||
                (pAlarmNode->alarmSampleType == 0) ||
                (pAlarmNode->i4AlarmStartupAlarm == 0) ||
                (pAlarmNode->u4AlarmRisingThreshold <=
                 pAlarmNode->u4AlarmFallingThreshold))
            {

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          " Invalid Alarm Configuration information \n");
                return SNMP_FAILURE;
            }
        }

        switch (pAlarmNode->alarmStatus)
        {
            case RMON_VALID:
                if (i4TestValAlarmStatus != RMON_CREATE_REQUEST)
                {

                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
                break;

            case RMON_INVALID:
                if ((i4TestValAlarmStatus == RMON_INVALID) ||
                    (i4TestValAlarmStatus == RMON_CREATE_REQUEST))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
                break;

            case RMON_UNDER_CREATION:
                if (i4TestValAlarmStatus != RMON_CREATE_REQUEST)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                    return SNMP_SUCCESS;
                }
                break;

            case RMON_CREATE_REQUEST:    /* The status is automatically changed */
                break;            /* to UNDER_CREATION after setting the */
                /* default values.                   */
            case RMON_MAX_STATE:
            default:
                break;

        }                        /*  end of switch  */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Alarm Index \n");
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2AlarmTable
 Input       :  The Indices
                AlarmIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2AlarmTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
