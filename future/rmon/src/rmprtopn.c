/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmprtopn.c,v 1.20 2014/01/07 10:33:55 siva Exp $            *
 *                                                                  *
 * Description: Contains Functions to read samples from Host Table &*
 *              and to sort the result and update it in TopN Table  *
 *                                                                  *
 *******************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  rmprtopn.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  APERIODIC                                    |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides updation of Host TopN    |
 * |                             Table                                       |
 * |                                                                         |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |     D. Vignesh      |         Create Original          |
 * |                |    19-MAR-1997      |                                  |
 *  -------------------------------------------------------------------------
 * |  2.0           |     LEELA L         |     Changes for Dynamic Tables   |
 * |                |    31-MAR-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  3.0           |     LEELA L         |     Changes for new MIDGEN Tool  |
 * |                |    07-JUL-2002      |                                  |
 *  -------------------------------------------------------------------------
 *
 */

#include "rmallinc.h"

#define FILL_SAMP_TABLE(X) \
 {\
  for (u4Count = 0; u4Count < pSample->u2NoOfHostInThisSample; u4Count++){\
       COPY_MAC_ADDRESS (&pSample->Sample[u4Count].u1HostTopNAddress,\
          &pHost[u4Count].u1HostAddress);\
       pSample->Sample[u4Count].u4HostTopNRate = pHost[u4Count].X;\
       pSample->Sample[u4Count].u4CreationTime = \
          pHost[u4Count].u4CreationTime;\
  }\
 }

#define HOST_CTRL_TBL(index)   gaHostControlTable[index].pHostControlEntry
#define TOPN_CTRL_TBL(index)   gaHostTopNControlTable[index].pTopNControlEntry

/************************************************************************
* Function            : SetSortedHostTopNTable                          *
*                                                                       *
* Input (s)           : i4Index - The TopN index on which the TopN table*
*                       will be updated.                                *
*                      SampleEntry1 - The initial samples of Hosts.     *
*                      SampleEntry2 - The final samples of Hosts.       *
*                      u4Size    - TopN Table Size.                     *
*                      u4HostIndex - The Host Table index whose         *
*                                     hosts are considered.             *
*                                                                       *
* Output (s)          : None.                                           *
*                                                                       *
* Returns             : None.                                           *
*                                                                       *
* Global Variables                                                      *
*             Used    : gaHostControlTable, gaHostTopNControlTable      *
*                                                                       *
* Side effects        : None                                            *
*                                                                       *
* Action :                                                              *
*                                                                       *
* Calculates the difference in sample and sort them in descending       *
* order in the TopN Table.                                              *
************************************************************************/

static void
SetSortedHostTopNTable (INT4 i4Index,
                        tTopnSample * pFstSample, tTopnSample * pSecSample,
                        UINT4 u4Size, INT4 i4HostIndex)
{
    UINT4               u4Pass, u4Count;

    UINT4               u4NoOfHost = 0;

    void               *pTmpRec = NULL;

    /* This value is initialized tothe maximum of TOPN_HOSTS.
     * This will save the compiler warning. */

    void               *pSortRec[MAX_TOPN_HOSTS];
    UINT4               u4Min;

    UNUSED_PARAM (i4HostIndex);

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : SetSortedHostTopNTable \n");

    u4NoOfHost = (UINT4) RMON_MIN (pSecSample->u2NoOfHostInThisSample,
                                   pFstSample->u2NoOfHostInThisSample);
    if (u4NoOfHost >= MAX_TOPN_HOSTS)
    {
        return;
    }

    for (u4Count = 0; u4Count < u4NoOfHost; u4Count++)
    {
        /*
         * pSortRec is used because We need not swap the records to
         * sort it but just change this pointer values to point the
         * correct record in a sorted order after sorting
         */
        pSortRec[u4Count] = (pSecSample->Sample + u4Count);

        if (pFstSample->Sample[u4Count].u4CreationTime ==
            pSecSample->Sample[u4Count].u4CreationTime)
        {
            pSecSample->Sample[u4Count].u4HostTopNRate -=
                pFstSample->Sample[u4Count].u4HostTopNRate;
        }
    }

    u4NoOfHost = pSecSample->u2NoOfHostInThisSample;
    if (u4NoOfHost >= MAX_TOPN_HOSTS)
    {
        return;
    }

    for ( /*u4Count has correct value */ ; u4Count < u4NoOfHost;
         u4Count++)
    {
        pSortRec[u4Count] = (pSecSample->Sample + u4Count);
    }

    /*
     *    Using Bubble sort to sort this
     *    Its unnessasary to sort the whole table becuause we just need the 
     *    topn So we run through u4Size passes only
     */

    u4Min = RMON_MIN (u4Size, u4NoOfHost);

    for (u4Pass = 0; u4Pass < u4Min; u4Pass++)
    {

        for (u4Count = (u4NoOfHost - 1); u4Count > u4Pass; u4Count--)
        {
            if (((tTopnSampleEntry *) (pSortRec[u4Count]))->
                u4HostTopNRate >
                ((tTopnSampleEntry *) (pSortRec[u4Count - 1]))->u4HostTopNRate)
            {
                /*
                 *          Now swaping the contents of pSortRec
                 */
                pTmpRec = pSortRec[u4Count];
                pSortRec[u4Count] = pSortRec[u4Count - 1];
                pSortRec[u4Count - 1] = pTmpRec;
            }
        }
    }

    for (u4Pass = 0; u4Pass < u4Min; u4Pass++)
    {
        (TOPN_CTRL_TBL (i4Index)->pTopNTable + u4Pass)->u4HostTopNRate
            = ((tTopnSampleEntry *) (pSortRec[u4Pass]))->u4HostTopNRate;
        COPY_MAC_ADDRESS (&
                          (TOPN_CTRL_TBL (i4Index)->pTopNTable +
                           u4Pass)->u1HostTopNAddress,
                          &((tTopnSampleEntry *) (pSortRec[u4Pass]))->
                          u1HostTopNAddress);
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function : SetSortedHostTopNTable \n");

}

/****************************************************************************
* Function           : GetSamplesFromHostTable                              *
*                                                                           *
* Input (s)          : i4HostTopNHostIndex - The index identifing what hosts*
*                                              we should take samples from  *
*                      hostTopNRateBase     - Identifies What variable to   *
*                                             Monitor                       *
*                      pSample - Samples are collected into this address.   *
*                                                                           *
* Output (s)         : None.                                                *
*                                                                           *
* Returns            : None.                                                *
*                                                                           *
* Global Variables                                                          *
*             Used   : gaHostControlTable ,gaHostTopNControlTable           *
*                                                                           *
* Side effects       : None                                                 *
*                                                                           *
* Action :                                                                  *
*                                                                           *
* Gets Sample from a list of host attached to a index and places            *
* the in the specified address.                                             *
****************************************************************************/

static void
GetSamplesFromHostTable (INT4 i4HostTopNHostIndex,
                         tHostTopnRateBase hostTopNRateBase,
                         tTopnSample * pSample)
{
    UINT4               u4Count;
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : GetSamplesFromHostTable \n");

    pHost = HOST_CTRL_TBL (i4HostTopNHostIndex)->pHostTable;

    if (pHost == NULL)
    {
        RMON_DBG (MINOR_TRC | FUNC_ENTRY,
                  "GetSamplesFromHostTable : Fails Since \
                 Host Table is not available \n");
        return;
    }

    if (pSample->u2NoOfHostInThisSample == 0)
    {
        RMON_DBG (MINOR_TRC | FUNC_ENTRY,
                  "GetSamplesFromHostTable : Sample not Available \n");
        return;
    }

    switch (hostTopNRateBase)
    {
        case hostTopNInPkts:
            FILL_SAMP_TABLE (u4HostInPkts);
            break;
        case hostTopNOutPkts:
            FILL_SAMP_TABLE (u4HostOutPkts);
            break;
        case hostTopNInOctets:
            FILL_SAMP_TABLE (u4HostInOctets);
            break;
        case hostTopNOutOctets:
            FILL_SAMP_TABLE (u4HostOutOctets);
            break;
        case hostTopNOutErrors:
            FILL_SAMP_TABLE (u4HostOutErrors);
            break;
        case hostTopNOutBroadcastPkts:
            FILL_SAMP_TABLE (u4HostOutBroadcastPkts);
            break;
        case hostTopNOutMulticastPkts:
            FILL_SAMP_TABLE (u4HostOutMulticastPkts);
            break;
        default:
            break;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function :  GetSamplesFromHostTable \n");

}

/**********************************************************************
* Function           : RmonUpdateHostTopNTable                        *
*                                                                     *
* Input (s)          : None.                                          *
*                                                                     *
* Output (s)         : None.                                          *
*                                                                     *
* Returns            : None.                                          *
*                                                                     *
* Global Variables                                                    *
*             Used   : gaHostTopNControlTable                         *
*                                                                     *
* Side effects       : None                                           *
*                                                                     *
* Action :                                                            *
* Called on each second To update Valid Host TopN Table entries....   *
**********************************************************************/

void                RmonUpdateHostTopNTable
ARG_LIST ((void))
{
    INT4                i4Index;
    tHostTopnControl   *pHostTopNCtrl = NULL;
    tTopnSample         SecndSample;

    MEMSET (&SecndSample, 0, sizeof (tTopnSample));

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function : RmonUpdateHostTopNTable \n");

    /*  Global variable indicating whether there is any valid entry in the
     *  TopN Control Table.  */
    grmprB1AnyValidTopN = FALSE;

    for (i4Index = RMON_ONE; i4Index < RMON_MAX_TOPN_CONTROL_ENTRY_LIMIT;
         i4Index++)
    {
        if (TOPN_CTRL_TBL (i4Index) == NULL)
        {
            continue;
        }
        /* Check for the Control Entry status is VALID */
        if (TOPN_CTRL_TBL (i4Index)->hostTopNStatus == RMON_VALID)
        {
            grmprB1AnyValidTopN = TRUE;

            pHostTopNCtrl = TOPN_CTRL_TBL (i4Index);
            if (pHostTopNCtrl->u4HostTopNTimeRemaining > 0)
            {
                if (pHostTopNCtrl->u4HostTopNTimeRemaining ==
                    pHostTopNCtrl->u4HostTopNDuration)
                {
                    /* Check for Host Control Entry status */
                    if ((HOST_CTRL_TBL (pHostTopNCtrl->i4HostTopNHostIndex) !=
                         NULL)
                        &&
                        ((HOST_CTRL_TBL (pHostTopNCtrl->i4HostTopNHostIndex)->
                          hostControlStatus) == RMON_VALID))
                    {
                        (pHostTopNCtrl->FirstSample).u2NoOfHostInThisSample =
                            HOST_CTRL_TBL (pHostTopNCtrl->i4HostTopNHostIndex)->
                            u2HostControlTableSize;
                    }
                    else
                    {
                        RMON_DBG (CRITICAL_TRC | MEM_TRC,
                                  " Host Control Entry status is not Valid.\n");
                        return;
                    }

                    /* Check no of hosts . If this is GT 0, then do the required
                     * allocations .. */

                    if ((pHostTopNCtrl->FirstSample).u2NoOfHostInThisSample > 0)
                    {

                        GetSamplesFromHostTable (pHostTopNCtrl->
                                                 i4HostTopNHostIndex,
                                                 pHostTopNCtrl->
                                                 hostTopNRateBase,
                                                 &pHostTopNCtrl->FirstSample);
                        pHostTopNCtrl->u4HostTopNTimeRemaining--;
                    }
                }
                else
                {
                    if (pHostTopNCtrl->u4HostTopNTimeRemaining == 1)
                    {
                        SecndSample.u2NoOfHostInThisSample =
                            HOST_CTRL_TBL (pHostTopNCtrl->i4HostTopNHostIndex)->
                            u2HostControlTableSize;

                        GetSamplesFromHostTable (pHostTopNCtrl->
                                                 i4HostTopNHostIndex,
                                                 pHostTopNCtrl->
                                                 hostTopNRateBase,
                                                 &SecndSample);

                        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                                  " Samples from Host table are taken \n");

                        SetSortedHostTopNTable (i4Index,
                                                &pHostTopNCtrl->FirstSample,
                                                &SecndSample,
                                                (UINT4) pHostTopNCtrl->
                                                i4HostTopNGrantedSize,
                                                pHostTopNCtrl->
                                                i4HostTopNHostIndex);

                        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                                  " Samples in HostTopN table are sorted.\n");

                    }
                    pHostTopNCtrl->u4HostTopNTimeRemaining--;    /*For SNMP Acess */
                }
            }
        }                        /*  else condition for if (TOPN _STATUS NOT VALID) */
        else
        {
            RMON_DBG (MINOR_TRC | FUNC_EXIT,
                      " RmonUpdateHostTopNTable: TopNControl Status is not VALID. \n");
            continue;
        }                        /* End of if */
    }                            /* End of for */
    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving function : RmonUpdateHostTopNTable \n");
}
