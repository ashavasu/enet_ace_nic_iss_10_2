/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmpralrm.c,v 1.20 2010/06/16 11:34:03 prabuc Exp $            *
 *                                                                  *
 * Description: Contains Function to check & generate Alarms.       *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmpralrm.c                                      
 *                                                                           
 *    PRINCIPAL AUTHOR       :  Aricent Inc.                             
 *                                                                          
 *    SUBSYSTEM NAME         :  ALARM MODULE                                
 *                                                                          
 *    MODULE NAME            :  RMON PERIODIC                                   
 *                                                                           
 *    LANGUAGE               :  C                                      
 *                                                                           
 *    TARGET ENVIRONMENT     :  ANY                                          
 *                                                                           
 *    DATE OF FIRST RELEASE  :                                               
 *                                                                           
 *    DESCRIPTION            :  This file contain the routine to generates 
 *                              the alarm based on Rising or Falling
 *                              Threshold and previous alarm interval value. 
 *                                                                           
 *  --------------------------------------------------------------------------  
 *
 *     CHANGE RECORD : 
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |          DESCRIPTION OF    
 *                  |        DATE          |             CHANGE         
 * -----------------|----------------------|---------------------------------
 *     1.0          |  Vijay G. Krishnan   |         Create Original      
 *                  |    19-MAR-1997       |                                 
 * -----------------|----------------------|---------------------------------
 *     2.0          |    Yogindhar P       |    modification in the         *   
 *                  |    08-AUG-2001       |    algorithm for checking      *
 *                  |                      |    thresholds                  *
 *---------------------------------------------------------------------------
 *     3.0          |   Bhupendra Sharma    | Fuction call for RmonGenerate *
 *                  |                       | Event  is modified to pass    *
 *                  |   13_AUG-2001         |  three parameter.             *
 * -------------------------------------------------------------------------*
 *     4.0          |   LEELA L             | Changes for Dynamic Tables    *
 *                  |   31-MAR-2002         |                               *
 * -------------------------------------------------------------------------*
 *     5.0          |   LEELA L             | Changes for MIDEGN Tool       *
 *                  |   07-JUL-2002         |                               *
 * -------------------------------------------------------------------------*/

#include "rmallinc.h"
#include "fsrmoncli.h"
#include "stdrmocli.h"

/******************************************************************************
*      Function             : RmonCheckAndGenerateAlarm                       *
*                                                                             *
*      Role of the function : This function checks the Alarm Table and        *
*                             generates alarms                                *
*      Formal Parameters    : None                                            *
*      Global Variables     : Alarm Table                                     *
*      Side Effects         : i4TimeToMonitor & u4AlarmValue in the           *
*                             gaAlarmTable are updated                        *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
RmonCheckAndGenerateAlarm ()
{
    INT4                i4Index = 0;
    INT4               *pi4Time2mon = NULL;
    UINT4               u4Value = 0;
    UINT4               u4PresValue;
    UINT4              *pu4Alrmval = NULL;
    tRmonAlarmNode     *pAlarmNode = NULL;
    UINT4               u4HashIndex;

    grmprB1AnyValidAlarm = FALSE;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering the Function : RmonCheckAndGenerateAlarm \n");

    TMO_HASH_Scan_Table (gpRmonAlarmHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gpRmonAlarmHashTable, u4HashIndex, pAlarmNode,
                              tRmonAlarmNode *)
        {
            if (pAlarmNode != NULL)
            {
                i4Index = pAlarmNode->i4AlarmIndex;
                pi4Time2mon = &(pAlarmNode->i4TimeToMonitor);
                pu4Alrmval = &(pAlarmNode->u4AlarmValue);

                if ((pAlarmNode->alarmStatus == RMON_VALID)
                    && (pAlarmNode->alarmVariable.pu4_OidList != NULL)
                    && (pAlarmNode->alarmVariable.u4_Length != 0))
                {
                    grmprB1AnyValidAlarm = TRUE;

                    (*pi4Time2mon)--;

                    RMON_DBG2 (MINOR_TRC | FUNC_ENTRY,
                               "Entering into the function: RmonCheckAndGenerateAlarm timeToMonitor = %d  u2Index = %d\n\n",
                               *pi4Time2mon, i4Index);

                    if (*pi4Time2mon == 0)
                    {
                        if (RmonGetMibVariableValue (&pAlarmNode->
                                                     alarmVariable,
                                                     &u4Value) != RMON_SUCCESS)
                        {
                            nmhSetAlarmStatus (i4Index, RMON_INVALID);
                            RMON_DBG (MAJOR_TRC | RMON_EVENT_TRC,
                                      " Alarm Variable Configured Out of Scope - Entry is made INVALID \n");
                            return;
                        }

                        if (pAlarmNode->alarmSampleType == DELTA_VALUE)
                        {
                            u4PresValue = u4Value;
                            /* if the counter value is smaller than previous
                             * counter value then it means counters had been
                             * cleared newly and so need to clear the previous
                             * counter value which is stored earlier 
                             */
                            if (u4PresValue < pAlarmNode->u4PreviousValue)
                            {
                                pAlarmNode->u4PreviousValue = 0;
                            }

                            u4Value -= pAlarmNode->u4PreviousValue;

                            RMON_DBG4 (MINOR_TRC | RMON_MGMT_TRC,
                                       "In fn RmonGetMibVariableValue u4PresValue = %d deltaval = %d, prevDelta = %d, u4presval = %d \n\n\n",
                                       u4PresValue, u4Value, *pu4Alrmval,
                                       pAlarmNode->u4PreviousValue);

                            pAlarmNode->u4PreviousValue = u4PresValue;
                        }
                        if (pAlarmNode->alarmSampleType == ABSOLUTE_VALUE)
                        {
                            if (u4Value >= pAlarmNode->u4AlarmRisingThreshold)
                            {
                                if (((pAlarmNode->u4IsAlarmOnce == RMON_FALSE)
                                     && (pAlarmNode->i4AlarmStartupAlarm !=
                                         RMON_FALLING_ALARM))
                                    || ((pAlarmNode->u4IsAlarmOnce == RMON_TRUE)
                                        && (*pu4Alrmval <=
                                            pAlarmNode->
                                            u4AlarmFallingThreshold)
                                        && (pAlarmNode->u4PrevIntervalValue <
                                            pAlarmNode->
                                            u4AlarmRisingThreshold)))
                                {
                                    /* The alarmValue is updated */
                                    *pu4Alrmval = u4Value;

                                    RmonGenerateEvent (i4Index,
                                                       IF_RISING_ALARM_TRAP,
                                                       pAlarmNode->
                                                       i4AlarmRisingEventIndex);

                                    pAlarmNode->u4IsAlarmOnce = RMON_TRUE;

                                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                              "Rising event is generated \n\n");
                                }
                            }
                            else if (u4Value <=
                                     pAlarmNode->u4AlarmFallingThreshold)
                            {
                                if (((pAlarmNode->u4IsAlarmOnce == RMON_FALSE)
                                     && (pAlarmNode->i4AlarmStartupAlarm !=
                                         RMON_RISING_ALARM))
                                    || ((pAlarmNode->u4IsAlarmOnce == RMON_TRUE)
                                        && (*pu4Alrmval >=
                                            pAlarmNode->
                                            u4AlarmRisingThreshold)
                                        && (pAlarmNode->u4PrevIntervalValue >
                                            pAlarmNode->
                                            u4AlarmFallingThreshold)))
                                {
                                    /* The alarmValue is updated */
                                    *pu4Alrmval = u4Value;

                                    RmonGenerateEvent (i4Index,
                                                       IF_FALLING_ALARM_TRAP,
                                                       pAlarmNode->
                                                       i4AlarmFallingEventIndex);

                                    pAlarmNode->u4IsAlarmOnce = RMON_TRUE;

                                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                              "Falling event is generated\n\n");
                                }
                            }
                        }
                        else if (pAlarmNode->alarmSampleType == DELTA_VALUE)
                        {
                            if (u4Value >= pAlarmNode->u4AlarmRisingThreshold)
                            {
                                if ((pAlarmNode->u1FirstAlarm == RMON_TRUE) ||
                                    ((pAlarmNode->u4PrevIntervalValue <
                                      pAlarmNode->u4AlarmRisingThreshold) &&
                                     (*pu4Alrmval <=
                                      pAlarmNode->u4AlarmFallingThreshold)))
                                {
                                    /* The alarmValue is updated */
                                    *pu4Alrmval = u4Value;

                                    RmonGenerateEvent (i4Index,
                                                       IF_RISING_ALARM_TRAP,
                                                       pAlarmNode->
                                                       i4AlarmRisingEventIndex);

                                    /* Reset the firstAlarm flag */
                                    pAlarmNode->u1FirstAlarm = RMON_FALSE;

                                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                              "Rising event is generated\n\n");
                                }
                            }
                            /* Alarm Logic changed.. */
                            else if (u4Value <=
                                     pAlarmNode->u4AlarmFallingThreshold)
                            {
                                if ((pAlarmNode->u1FirstAlarm == RMON_TRUE) ||
                                    ((pAlarmNode->u4PrevIntervalValue >
                                      pAlarmNode->u4AlarmFallingThreshold) &&
                                     (*pu4Alrmval >=
                                      pAlarmNode->u4AlarmRisingThreshold)))
                                {
                                    /* The alarmValue is updated */
                                    *pu4Alrmval = u4Value;

                                    RmonGenerateEvent (i4Index,
                                                       IF_FALLING_ALARM_TRAP,
                                                       pAlarmNode->
                                                       i4AlarmFallingEventIndex);

                                    /* Reset the firstAlarm flag */
                                    pAlarmNode->u1FirstAlarm = RMON_FALSE;

                                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                                              "Falling event is generated\n\n");
                                }
                            }
                        }

                        /* The prev sampling interval value is updated */
                        pAlarmNode->u4PrevIntervalValue = u4Value;

                        /* The mirror variable is updated to the original value */
                        *pi4Time2mon = pAlarmNode->i4AlarmInterval;
                    }
                }
                else
                {
                    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                              "Alarm Variable is not configured or Entry status not VALID \n");
                }
            }                    /* End of if */
        }
    }
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Leaving  function: RmonCheckAndGenerateAlarm \n");
}
