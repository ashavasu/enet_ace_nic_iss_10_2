/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: ralmtrx.c,v 1.35 2015/04/23 12:04:49 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for Matrix Group   *
 *                                                                  *
 *******************************************************************/

/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  ralmtrx.c
 *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.
 *
 *    SUBSYSTEM NAME         :  LOW LEVEL CODE
 *
 *    MODULE NAME            :  RMON PERIODIC
 *
 *    LANGUAGE               :  C
 *
 *    TARGET ENVIRONMENT     :  ANY
 *
 *    DATE OF FIRST RELEASE  :
 *
 *    DESCRIPTION            : This files contains the Low level routines 
 *                                of the Matrix Group.
 *
 *  --------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     -------------
 *
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |         DESCRIPTION OF
 *                  |        DATE          |            CHANGE
 * -----------------|----------------------|---------------------------------
 *      1.0         |  Vijay G. Krishnan   |        Create Original
 *                  |     24-APR-1997      |
 * --------------------------------------------------------------------------
 *      2.0         |  Yogindhar P         | Changing Funtion signatures &
 *                  |  17-AUG-2001         | variables compatible to SNMPv2
 * --------------------------------------------------------------------------
 *      3.0         |  Leela  L            | Changing from Static tables to
 *                  |  31-Mar-2002         | dynamic tables                
 * --------------------------------------------------------------------------
 *      4.0         |  Leela  L            | Compilation warnings fixed.    
 *                  |  28-Jun-2002         |                               
 * --------------------------------------------------------------------------
 *      5.0         |  Leela  L            | Chnages for New MIDGEN Tool    
 *                  |  07-JUL-2002         |                               
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"
#include "rmgr.h"
extern UINT4        MatrixControlStatus[11];
#define INTERFACE_OID_LEN          grlInterfaceOid.u4_Length

#define UNDER_CREATION_MTRX(Index) ((gaMatrixControlTable[Index].pMatrixControlEntry != NULL ) && (gaMatrixControlTable[Index].\
                    pMatrixControlEntry->matrixControlStatus == RMON_UNDER_CREATION))

#define VALID_MATRIX(Index)        ((gaMatrixControlTable[Index].pMatrixControlEntry != NULL) && (gaMatrixControlTable[Index].\
                    pMatrixControlEntry->matrixControlStatus == RMON_VALID))

#define CMP_MTRX(a, b, c, x, y, z) ( (a == x)? \
                                          ( (MAC_DIFF(b, y) == 0)?\
                                             MAC_DIFF(c, z): \
                                             MAC_DIFF(b, y) ): (a - x) )

 /*
  * LOW LEVEL Routines for Table : matrixControlTable.
  */

/******************************************************************************
*      Role of the function : This function gets the position of the entry    *
*                             in the matrixSDTable indexed by MAC addresses   *
*      Formal Parameters    : u2MatrixIndex, matrixSourceAddress              *
*                             matrixDestAddress                               *
*      Global Variables     : None                                            *
*      Side Effects         : None                                            *
*      Exception Handling   : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : Position of the entry in the table              *
******************************************************************************/
static UINT2
GetCurrentIndex (INT4 i4MatrixIndex, tMacAddr matrixSourceAddress,
                 tMacAddr matrixDestAddress)
{
    UINT2               u2Index;
    tSdTableEntry      *pSDTable = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: GetCurrentIndex  \n");

    pSDTable = MATRIX_CTRL_TABLE (i4MatrixIndex)->pMatrixSDTable;

    for (u2Index = 0; u2Index < MAX_MATRIX_ENTRY; u2Index++)
    {
        if ((COMPARE_MAC_ADDRESS ((pSDTable + u2Index)->
                                  matrixSDSourceAddress,
                                  matrixSourceAddress) == RMON_EQUAL)
            &&
            (COMPARE_MAC_ADDRESS
             ((pSDTable + u2Index)->matrixSDDestAddress,
              matrixDestAddress) == RMON_EQUAL))
        {
            break;
        }
    }
    RMON_DBG1 (MAJOR_TRC | PKT_PROC_TRC,
               "LEAVING FUNC GETcurrentIndex Matrix table U2_INDEX IS = %d \n\n",
               u2Index);
    return u2Index;
}

/* LOW LEVEL Routines for Table : MatrixControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMatrixControlTable
 Input       :  The Indices
                MatrixControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMatrixControlTable (INT4 i4MatrixControlIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhValidateIndexInstanceMatrixControlTable  \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4MatrixControlIndex > 0) &&
        (i4MatrixControlIndex < RMON_MAX_INTERFACE_LIMIT)
        && (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL))
    {

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "Invalid Matrix control Index \n");
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexMatrixControlTable
 Input       :  The Indices
                MatrixControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMatrixControlTable (INT4 *pi4MatrixControlIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into function: nmhGetFirstIndexMatrixControlTable \n");

    *pi4MatrixControlIndex = 1;

    while (MATRIX_CTRL_TABLE (*pi4MatrixControlIndex) == NULL)
    {
        (*pi4MatrixControlIndex)++;
        if (*pi4MatrixControlIndex == RMON_MAX_INTERFACE_LIMIT)
        {

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Matrix Control Index is equal to RMON_MAX_INTERFACE_LIMIT \n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMatrixControlTable
 Input       :  The Indices
                MatrixControlIndex
                nextMatrixControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexMatrixControlTable (INT4 i4MatrixControlIndex,
                                   INT4 *pi4NextMatrixControlIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into function: nmhGetNextIndexMatrixControlTable  \n");

    if (i4MatrixControlIndex < 0)
    {
        i4MatrixControlIndex = 0;
    }

    if (i4MatrixControlIndex < (RMON_MAX_INTERFACE_LIMIT - 1))
    {
        *pi4NextMatrixControlIndex = i4MatrixControlIndex + 1;

        while (MATRIX_CTRL_TABLE (*pi4NextMatrixControlIndex) == NULL)
        {
            (*pi4NextMatrixControlIndex)++;
            if (*pi4NextMatrixControlIndex == RMON_MAX_INTERFACE_LIMIT)
            {

                RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                          "Matrix Control index is equal to RMON_MAX_INTERFACE_LIMIT \n");
                return SNMP_FAILURE;
            }
        }

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix Control Index \n");
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMatrixControlDataSource
 Input       :  The Indices
                MatrixControlIndex

                The Object
                retValMatrixControlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixControlDataSource (INT4 i4MatrixControlIndex,
                               tSNMP_OID_TYPE * pRetValMatrixControlDataSource)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixControlDataSource \n");

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
    {
        if (NOT_INVALID_MATRIX (i4MatrixControlIndex))
        {
            RmonFormDataSourceOid (pRetValMatrixControlDataSource,
                                   MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                                   u4MatrixControlDataSource,
                                   (UINT4) PORT_INDEX_TYPE);

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix control Index \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetMatrixControlTableSize
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pi4RetValMatrixControlTableSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixControlTableSize (INT4 i4MatrixControlIndex,
                              INT4 *pi4RetValMatrixControlTableSize)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixControlTableSize  \n");

    if ((MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL) &&
        (NOT_INVALID_MATRIX (i4MatrixControlIndex)))
    {
        *pi4RetValMatrixControlTableSize =
            (INT4) MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
            u4MatrixControlTableSize;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix Control Index : \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixControlLastDeleteTime
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pu4RetValMatrixControlLastDeleteTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixControlLastDeleteTime (INT4 i4MatrixControlIndex,
                                   UINT4 *pu4RetValMatrixControlLastDeleteTime)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhGetMatrixControlLastDeleteTime\n");

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
    {
        if (NOT_INVALID_MATRIX (i4MatrixControlIndex))
        {
            *pu4RetValMatrixControlLastDeleteTime =
                MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                matrixControlLastDeleteTime;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix Control Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixControlOwner
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pRetValMatrixControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixControlOwner (INT4 i4MatrixControlIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValMatrixControlOwner)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhGetMatrixControlOwner  \n");

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
    {
        if (NOT_INVALID_MATRIX (i4MatrixControlIndex))
        {
            STRNCPY (pRetValMatrixControlOwner->pu1_OctetList,
                     MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                     u1MatrixControlOwner, OWN_STR_LENGTH);

            pRetValMatrixControlOwner->i4_Length =
                (INT4) STRLEN (MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                               u1MatrixControlOwner);

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix control Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixControlStatus
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pi4RetValMatrixControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixControlStatus (INT4 i4MatrixControlIndex,
                           INT4 *pi4RetValMatrixControlStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixControlStatus \n");

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
    {
        if (NOT_INVALID_MATRIX (i4MatrixControlIndex))
        {
            *pi4RetValMatrixControlStatus =
                MATRIX_CTRL_TABLE (i4MatrixControlIndex)->matrixControlStatus;

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix Control Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhSetMatrixControlDataSource
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pSetValMatrixControlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetMatrixControlDataSource (INT4 i4MatrixControlIndex,
                               tSNMP_OID_TYPE * pSetValMatrixControlDataSource)
{
    UINT4               u4OidType;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetMatrixControlDataSource  \n");

    RmonSetDataSourceOid (pSetValMatrixControlDataSource,
                          &(MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                            u4MatrixControlDataSource), &u4OidType);

    /* Type of OID (VLAN or PORT) is set in u4OidType */
    /* Currently only PORT will be returned */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMatrixControlOwner
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pSetValMatrixControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetMatrixControlOwner (INT4 i4MatrixControlIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValMatrixControlOwner)
{
    UINT4               u4Minimum;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetMatrixControlOwner \n");

    u4Minimum =
        (UINT4) (pSetValMatrixControlOwner->i4_Length <
                  (OWN_STR_LENGTH-1) ? pSetValMatrixControlOwner->
                 i4_Length : (OWN_STR_LENGTH-1));

    MEMSET (MATRIX_CTRL_TABLE (i4MatrixControlIndex)->u1MatrixControlOwner, 0,
            OWN_STR_LENGTH);

    STRNCPY (MATRIX_CTRL_TABLE (i4MatrixControlIndex)->u1MatrixControlOwner,
             pSetValMatrixControlOwner->pu1_OctetList, u4Minimum);

    MATRIX_CTRL_TABLE (i4MatrixControlIndex)->u1MatrixControlOwner[u4Minimum] = '\0';

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMatrixControlStatus
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pi4SetValMatrixControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetMatrixControlStatus (INT4 i4MatrixControlIndex,
                           INT4 i4SetValMatrixControlStatus)
{
    tRmonStatus        *pStatus = NULL;
#if defined(NPAPI_WANTED) && defined(EXTND_SERVICE)
    UINT1               u1MatrixCtrlEnable;
#endif
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetMatrixControlStatus  \n");

    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    /*Check and allocate the memory for the *
     * control entry of the Matrix Control table     */

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) == NULL)
    {
        MATRIX_CTRL_TABLE (i4MatrixControlIndex) =
            (tMatrixControlEntry *) MemAllocMemBlk (gRmonMatrixCtrlPoolId);

        if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " Memory Allocation Failure - gaMatrixControlTable. \n");

            return SNMP_FAILURE;
        }

        MATRIX_CTRL_TABLE (i4MatrixControlIndex)->matrixControlStatus =
            RMON_INVALID;
    }

    pStatus = &(MATRIX_CTRL_TABLE (i4MatrixControlIndex)->matrixControlStatus);

    switch (*pStatus)
    {
        case RMON_VALID:
            if (i4SetValMatrixControlStatus == RMON_INVALID)
            {
                /* Memory is freed here */
                RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                          "Matrix Entry Status switching from Valid to Invalid\n");

                MemReleaseMemBlock (gRmonMatrixBucketsPoolId, (UINT1 *)
                                    MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                                    pMatrixSDTable);
                MATRIX_CTRL_TABLE (i4MatrixControlIndex)->pMatrixSDTable = NULL;
                /* Free the Matrix Control Entry */
                MemReleaseMemBlock
                    (gRmonMatrixCtrlPoolId,
                     (UINT1 *) MATRIX_CTRL_TABLE (i4MatrixControlIndex));
                MATRIX_CTRL_TABLE (i4MatrixControlIndex) = NULL;

            }
            else if (i4SetValMatrixControlStatus == RMON_UNDER_CREATION)
            {

                /* Memory is freed here */
                MemReleaseMemBlock (gRmonMatrixBucketsPoolId, (UINT1 *)
                                    MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                                    pMatrixSDTable);
                /* Start timer to delete entries not made valid in given time */
                RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                          "Matrix Entry switching from Valid to \
                           Under_Creation and Starting the timer\n");
                RmonStartTimer (MATRIX_CONTROL_TABLE, i4MatrixControlIndex,
                                MTRX_CTRL_DURATION);
                /* Assign the STATUS */
                *pStatus = i4SetValMatrixControlStatus;
            }
            break;

        case RMON_INVALID:
            *pStatus = i4SetValMatrixControlStatus;
            if (i4SetValMatrixControlStatus == RMON_CREATE_REQUEST)
            {
                *pStatus = RMON_UNDER_CREATION;
                /* Start timer to delete entries not made valid in given time */
                RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                          " Entry Status from Invalid to Under_Creation & \
                             Starting the tomer \n");
                RmonStartTimer (MATRIX_CONTROL_TABLE, i4MatrixControlIndex,
                                MTRX_CTRL_DURATION);
            }
            break;

        case RMON_UNDER_CREATION:
            if (i4SetValMatrixControlStatus == RMON_VALID)
            {
                /* First Stop the Timer  */
                RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                          "Entry Status Under_Creation to Valid & \
                              Stopping the timer\n");
                RmonStopTimer (MATRIX_CONTROL_TABLE, i4MatrixControlIndex);
                /* Assign the Status */
                *pStatus = i4SetValMatrixControlStatus;

                /*  Allocating  memory  for  buckets */
                MATRIX_CTRL_TABLE (i4MatrixControlIndex)->pMatrixSDTable =
                    MemAllocMemBlk (gRmonMatrixBucketsPoolId);

                if (MATRIX_CTRL_TABLE (i4MatrixControlIndex)->pMatrixSDTable
                    == NULL)
                {
                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              "Memory Allocation Failure  \n");
                    return SNMP_FAILURE;
                }
#ifdef RMON2_WANTED
                MATRIX_CTRL_TABLE (i4MatrixControlIndex)->
                    u4MatrixControlCreateTime = OsixGetSysUpTime ();
#endif
            }
            else if (i4SetValMatrixControlStatus == RMON_INVALID)
            {
                RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                          "Entry Status Under_Creation to Invalid & \
                              Stopping the timer \n");
                RmonStopTimer (MATRIX_CONTROL_TABLE, i4MatrixControlIndex);

                /* Free  the Control Entry Memory */
                if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
                {
                    MemReleaseMemBlock
                        (gRmonMatrixCtrlPoolId,
                         (UINT1 *) MATRIX_CTRL_TABLE (i4MatrixControlIndex));
                    MATRIX_CTRL_TABLE (i4MatrixControlIndex) = NULL;
                }

            }
            break;

        case RMON_CREATE_REQUEST:
        case RMON_MAX_STATE:
        default:
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      "Invalid MatrixControlStatus \n");
            return SNMP_FAILURE;

    }                            /*  end of switch  */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = MatrixControlStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (MatrixControlStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4MatrixControlIndex,
                      i4SetValMatrixControlStatus));

    return SNMP_SUCCESS;
}

 /*
  * Low Level TEST Routines for All Objects
  */

/****************************************************************************
 Function    :  nmhTestv2MatrixControlDataSource
 Input       :  The Indices
                MatrixControlIndex

                The Object
                testValMatrixControlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc
1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of r
fc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2MatrixControlDataSource (UINT4 *pu4ErrorCode,
                                  INT4 i4MatrixControlIndex,
                                  tSNMP_OID_TYPE *
                                  pTestValMatrixControlDataSource)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhTestv2MatrixControlDataSource\n");

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
    {
        if (UNDER_CREATION_MTRX (i4MatrixControlIndex))
        {
            if (RmonTestDataSourceOid (pTestValMatrixControlDataSource) == TRUE)
            {

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Matrix Status is not Under Creation state \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MatrixControlOwner
 Input       :  The Indices
                i4MatrixControlIndex

                The Object
                pTestValMatrixControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2MatrixControlOwner (UINT4 *pu4ErrorCode,
                             INT4 i4MatrixControlIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValMatrixControlOwner)
{

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2MatrixControlOwner \n");

    RMON_DUMMY (pTestValMatrixControlOwner);

    if (MATRIX_CTRL_TABLE (i4MatrixControlIndex) != NULL)
    {
        if (UNDER_CREATION_MTRX (i4MatrixControlIndex))
        {

            *pu4ErrorCode = SNMP_ERR_NO_ERROR;
            return SNMP_SUCCESS;
        }
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "Entry  Status in not Under creation state  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MatrixControlStatus
 Input       :  The Indices
                MatrixControlIndex

                The Object
                testValMatrixControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc 1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2MatrixControlStatus (UINT4 *pu4ErrorCode,
                              INT4 i4MatrixControlIndex,
                              INT4 i4TestValMatrixControlStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2MatrixControlStatus \n");

    /* Configuration of this table is not supported for now.
     * Remove this error return, once support is added
     */
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4MatrixControlIndex);
    UNUSED_PARAM (i4TestValMatrixControlStatus);
    return SNMP_FAILURE;
}

 /*
  * LOW LEVEL Routines for Table : matrixSDTable.
  */

 /*
  * GET_EXACT Validate Index Instance Routine.
  */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMatrixSDTable
 Input       :  The Indices
                i4MatrixSDIndex
                MatrixSDSourceAddress
                MatrixSDDestAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceMatrixSDTable (INT4 i4MatrixSDIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMatrixSDSourceAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMatrixSDDestAddress)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entering into the function: \
                nmhValidateIndexInstanceMatrixSDTable  \n");
    RMON_DUMMY (*pMatrixSDDestAddress);
    RMON_DUMMY (*pMatrixSDSourceAddress);

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4MatrixSDIndex > 0) &&
        (i4MatrixSDIndex < RMON_MAX_INTERFACE_LIMIT) &&
        (MATRIX_CTRL_TABLE (i4MatrixSDIndex) != NULL))
    {

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Matrix SD Index \n");
        return SNMP_FAILURE;
    }
}

 /*
  * GET_FIRST Routine.
  */

/****************************************************************************
 Function    :  nmhGetFirstIndexMatrixSDTable
 Input       :  The Indices
                pi4MatrixSDIndex
                pMatrixSDSourceAddress
                pMatrixSDDestAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexMatrixSDTable (INT4 *pi4MatrixSDIndex,
                               tSNMP_OCTET_STRING_TYPE * pMatrixSDSourceAddress,
                               tSNMP_OCTET_STRING_TYPE * pMatrixSDDestAddress)
{
    tSNMP_OCTET_STRING_TYPE *pmacZeroAddrs = NULL;

    pmacZeroAddrs =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (ADDRESS_LENGTH);
    if (pmacZeroAddrs == NULL)
    {
        return SNMP_FAILURE;
    }

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetFirstIndexMatrixSDTable  \n");
    ZERO_MAC_ADDRESS (pmacZeroAddrs->pu1_OctetList);

    if (nmhGetNextIndexMatrixSDTable (RMON_ZERO, pi4MatrixSDIndex,
                                      pmacZeroAddrs, pMatrixSDSourceAddress,
                                      pmacZeroAddrs, pMatrixSDDestAddress)
        != SNMP_FAILURE)
    {
        free_octetstring (pmacZeroAddrs);
        return SNMP_SUCCESS;
    }
    free_octetstring (pmacZeroAddrs);
    return SNMP_FAILURE;
}

 /*
  * GET_NEXT Routine.
  */
/****************************************************************************
 Function    :  nmhGetNextIndexMatrixSDTable
 Input       :  The Indices
                i4MatrixSDIndex
                pi4NextMatrixSDIndex
                MatrixSDSourceAddress
                pNextMatrixSDSourceAddress
                MatrixSDDestAddress
                pNextMatrixSDDestAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexMatrixSDTable (INT4 i4MatrixSDIndex,
                              INT4 *pi4NextMatrixSDIndex,
                              tSNMP_OCTET_STRING_TYPE * pMatrixSDSourceAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextMatrixSDSourceAddress,
                              tSNMP_OCTET_STRING_TYPE * pMatrixSDDestAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextMatrixSDDestAddress)
{

    INT4                i4AmatrixIndex = i4MatrixSDIndex;
    tMacAddr            bsrcMacAdd;
    tMacAddr            bdstMacAdd;
    INT4                i4XmatrixIndex;
    tMacAddr            ysrcMacAdd;
    tMacAddr            ydstMacAdd;
    tSdTableEntry      *pMatrix = NULL, *ptempMatrix = NULL;
    INT4                i4Index, i4Matrix;
    INT4                i4TableSize;
    UINT1               u1BreakFlag = FALSE;
    UINT1               u1NextFlag = FALSE;
    tMatrixControlEntry *pMatrixControl = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetNextIndexMatrixSDTable  \n");

    COPY_MAC_ADDRESS (bsrcMacAdd, pMatrixSDSourceAddress->pu1_OctetList);
    COPY_MAC_ADDRESS (bdstMacAdd, pMatrixSDDestAddress->pu1_OctetList);
    *pi4NextMatrixSDIndex = RMON_MAX_INTERFACE_LIMIT;

    if (i4AmatrixIndex <= 0)
    {
        u1NextFlag = TRUE;
        i4AmatrixIndex = 1;
    }

    for (i4Index = i4AmatrixIndex; i4Index < RMON_MAX_INTERFACE_LIMIT;
         i4Index++)
    {
        if (u1BreakFlag == TRUE)
        {
            break;
        }

        if ((MATRIX_CTRL_TABLE (i4Index) != NULL)
            && (MATRIX_CTRL_TABLE (i4Index)->matrixControlStatus == RMON_VALID))
        {
            /* Assign the MatrixControlEntry pointer */
            pMatrixControl = MATRIX_CTRL_TABLE (i4Index);
            pMatrix = pMatrixControl->pMatrixSDTable;
            if (pMatrix == NULL)
            {
                continue;
            }
            i4TableSize = (INT4) pMatrixControl->u4MatrixControlTableSize;

            for (i4Matrix = 0; i4Matrix < i4TableSize; i4Matrix++)
            {
                ptempMatrix = pMatrix + i4Matrix;
                i4XmatrixIndex = i4Index;
                if (u1NextFlag == TRUE)
                {
                    *pi4NextMatrixSDIndex = i4XmatrixIndex;
                    COPY_MAC_ADDRESS (pNextMatrixSDSourceAddress->pu1_OctetList,
                                      ptempMatrix->matrixSDSourceAddress);
                    pNextMatrixSDSourceAddress->i4_Length = ADDRESS_LENGTH;
                    COPY_MAC_ADDRESS (pNextMatrixSDDestAddress->pu1_OctetList,
                                      ptempMatrix->matrixSDDestAddress);
                    pNextMatrixSDDestAddress->i4_Length = ADDRESS_LENGTH;
                    u1BreakFlag = TRUE;
                    break;
                }

                COPY_MAC_ADDRESS (ysrcMacAdd,
                                  ptempMatrix->matrixSDSourceAddress);
                COPY_MAC_ADDRESS (ydstMacAdd, ptempMatrix->matrixSDDestAddress);

                if (CMP_MTRX (i4AmatrixIndex, bsrcMacAdd, bdstMacAdd,
                              i4XmatrixIndex, ysrcMacAdd, ydstMacAdd) == 0)
                {

                    if (i4Matrix == (i4TableSize - 1))
                    {
                        u1NextFlag = TRUE;
                    }
                    else
                    {
                        ptempMatrix = ptempMatrix + 1;
                        *pi4NextMatrixSDIndex = i4XmatrixIndex;
                        COPY_MAC_ADDRESS (pNextMatrixSDSourceAddress->
                                          pu1_OctetList,
                                          &ptempMatrix->matrixSDSourceAddress);
                        pNextMatrixSDSourceAddress->i4_Length = ADDRESS_LENGTH;
                        COPY_MAC_ADDRESS (pNextMatrixSDDestAddress->
                                          pu1_OctetList,
                                          &ptempMatrix->matrixSDDestAddress);
                        pNextMatrixSDDestAddress->i4_Length = ADDRESS_LENGTH;
                        u1BreakFlag = TRUE;
                        break;
                    }

                }
            }
        }
    }
    if (u1BreakFlag == TRUE)
    {
        return SNMP_SUCCESS;
    }

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Matrix SD Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level GET Routine for All Objects
  */

/****************************************************************************
 Function    :  nmhGetMatrixSDPkts
 Input       :  The Indices
                i4MatrixSDIndex
                MatrixSDSourceAddress
                MatrixSDDestAddress

                The Object
                pu4RetValMatrixSDPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixSDPkts (INT4 i4MatrixSDIndex,
                    tSNMP_OCTET_STRING_TYPE * pMatrixSDSourceAddress,
                    tSNMP_OCTET_STRING_TYPE * pMatrixSDDestAddress,
                    UINT4 *pu4RetValMatrixSDPkts)
{
    UINT2               u2Index;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixSDPkts \n");

    if (VALID_MATRIX (i4MatrixSDIndex))
    {
        u2Index = GetCurrentIndex (i4MatrixSDIndex,
                                   pMatrixSDSourceAddress->pu1_OctetList,
                                   pMatrixSDDestAddress->pu1_OctetList);

        RMON_DBG1 (MAJOR_TRC | PKT_PROC_TRC,
                   " Index: nmhGetMatrixSDPkts = %d \n", u2Index);

        if (u2Index == MAX_MATRIX_ENTRY)
        {

            return SNMP_FAILURE;
        }

        *pu4RetValMatrixSDPkts =
            (MATRIX_CTRL_TABLE (i4MatrixSDIndex)->pMatrixSDTable +
             u2Index)->u4MatrixSDPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix SD index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixSDOctets
 Input       :  The Indices
                i4MatrixSDIndex
                MatrixSDSourceAddress
                MatrixSDDestAddress

                The Object
                pu4RetValMatrixSDOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixSDOctets (INT4 i4MatrixSDIndex,
                      tSNMP_OCTET_STRING_TYPE * pMatrixSDSourceAddress,
                      tSNMP_OCTET_STRING_TYPE * pMatrixSDDestAddress,
                      UINT4 *pu4RetValMatrixSDOctets)
{
    UINT2               u2Index;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixSDOctets  \n");

    if (VALID_MATRIX (i4MatrixSDIndex))
    {
        u2Index = GetCurrentIndex (i4MatrixSDIndex,
                                   pMatrixSDSourceAddress->pu1_OctetList,
                                   pMatrixSDDestAddress->pu1_OctetList);
        if (u2Index == MAX_MATRIX_ENTRY)
        {

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Received Index is out of range (equal to MAX_MATRIX_ENTRY)\n");
            return SNMP_FAILURE;
        }

        *pu4RetValMatrixSDOctets =
            (MATRIX_CTRL_TABLE (i4MatrixSDIndex)->pMatrixSDTable +
             u2Index)->u4MatrixSDOctets;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry is not VALID  \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixSDErrors
 Input       :  The Indices
                i4MatrixSDIndex
                MatrixSDSourceAddress
                MatrixSDDestAddress

                The Object
                pu4RetValMatrixSDErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixSDErrors (INT4 i4MatrixSDIndex,
                      tSNMP_OCTET_STRING_TYPE * pMatrixSDSourceAddress,
                      tSNMP_OCTET_STRING_TYPE * pMatrixSDDestAddress,
                      UINT4 *pu4RetValMatrixSDErrors)
{
    UINT2               u2Index;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixSDErrors \n");

    RMON_DBG1 (MAJOR_TRC | RMON_MGMT_TRC,
               "i4MatrixSDIndex????? = %d\n\n", i4MatrixSDIndex);

    if (VALID_MATRIX (i4MatrixSDIndex))
    {
        u2Index = GetCurrentIndex (i4MatrixSDIndex,
                                   pMatrixSDSourceAddress->pu1_OctetList,
                                   pMatrixSDDestAddress->pu1_OctetList);
        if (u2Index == MAX_MATRIX_ENTRY)
        {

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Received Index is out of range (equal to MAX_MATRIX_ENTRY)\n");
            return SNMP_FAILURE;
        }

        *pu4RetValMatrixSDErrors =
            (MATRIX_CTRL_TABLE (i4MatrixSDIndex)->pMatrixSDTable +
             u2Index)->u4MatrixSDErrors;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix SD index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

 /*
  * Low Level TEST Routines for All Objects
  */

 /*
  * LOW LEVEL Routines for Table : matrixDSTable.
  */

 /*
  * GET_EXACT Validate Index Instance Routine.
  */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMatrixDSTable
 Input       :  The Indices
                i4MatrixDSIndex
                MatrixDSDestAddress
                MatrixDSSourceAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceMatrixDSTable (INT4 i4MatrixDSIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMatrixDSDestAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMatrixDSSourceAddress)
{

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhValidateIndexInstanceMatrixDSTable \n");
    RMON_DUMMY (*pMatrixDSSourceAddress);
    RMON_DUMMY (*pMatrixDSDestAddress);

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4MatrixDSIndex > 0) &&
        (i4MatrixDSIndex < RMON_MAX_INTERFACE_LIMIT) &&
        (MATRIX_CTRL_TABLE (i4MatrixDSIndex)->matrixControlStatus ==
         RMON_VALID))
    {

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC, "Invalid Matrix DS Index \n");
        return SNMP_FAILURE;
    }
}

 /*
  * GET_FIRST Routine.
  */

/****************************************************************************
 Function    :  nmhGetFirstIndexMatrixDSTable
 Input       :  The Indices
                pi4MatrixDSIndex
                pMatrixDSDestAddress
                pMatrixDSSourceAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexMatrixDSTable (INT4 *pi4MatrixDSIndex,
                               tSNMP_OCTET_STRING_TYPE * pMatrixDSDestAddress,
                               tSNMP_OCTET_STRING_TYPE * pMatrixDSSourceAddress)
{
    tSNMP_OCTET_STRING_TYPE *pmacZeroAddrs = NULL;

    pmacZeroAddrs =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (ADDRESS_LENGTH);
    if (pmacZeroAddrs == NULL)
    {
        return SNMP_FAILURE;
    }

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetFirstIndexMatrixDSTable \n");

    ZERO_MAC_ADDRESS (pmacZeroAddrs->pu1_OctetList);

    if (nmhGetNextIndexMatrixDSTable (RMON_ZERO, pi4MatrixDSIndex,
                                      pmacZeroAddrs, pMatrixDSDestAddress,
                                      pmacZeroAddrs, pMatrixDSSourceAddress)
        != SNMP_FAILURE)
    {
        free_octetstring (pmacZeroAddrs);
        return SNMP_SUCCESS;
    }

    free_octetstring (pmacZeroAddrs);
    return SNMP_FAILURE;
}

 /*
  * GET_NEXT Routine.
  */

/****************************************************************************
 Function    :  nmhGetNextIndexMatrixDSTable
 Input       :  The Indices
                i4MatrixDSIndex
                pi4NextMatrixDSIndex
                MatrixDSDestAddress
                pNextMatrixDSDestAddress
                MatrixDSSourceAddress
                pNextMatrixDSSourceAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetNextIndexMatrixDSTable (INT4 i4MatrixDSIndex,
                              INT4 *pi4NextMatrixDSIndex,
                              tSNMP_OCTET_STRING_TYPE * pMatrixDSDestAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextMatrixDSDestAddress,
                              tSNMP_OCTET_STRING_TYPE * pMatrixDSSourceAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextMatrixDSSourceAddress)
{
    INT4                i4AmatrixIndex = i4MatrixDSIndex;
    tMacAddr            bsrcMacAdd;
    tMacAddr            bdstMacAdd;
    INT4                i4XmatrixIndex;
    tMacAddr            ysrcMacAdd;
    tMacAddr            ydstMacAdd;
    tSdTableEntry      *pMatrix = NULL, *ptempMatrix = NULL;
    INT4                i4Index, i4Matrix;
    INT4                i4TableSize;
    UINT1               u1BreakFlag = FALSE;
    UINT1               u1NextFlag = FALSE;
    tMatrixControlEntry *pMatrixControl = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetNextIndexMatrixDSTable  \n");

    COPY_MAC_ADDRESS (bsrcMacAdd, pMatrixDSSourceAddress->pu1_OctetList);
    COPY_MAC_ADDRESS (bdstMacAdd, pMatrixDSDestAddress->pu1_OctetList);
    *pi4NextMatrixDSIndex = RMON_MAX_INTERFACE_LIMIT;
    if (i4AmatrixIndex <= 0)
    {
        u1NextFlag = TRUE;
        i4AmatrixIndex = 1;
    }
    for (i4Index = i4AmatrixIndex; i4Index < RMON_MAX_INTERFACE_LIMIT;
         i4Index++)
    {
        if (u1BreakFlag)
        {
            break;
        }

        if ((MATRIX_CTRL_TABLE (i4Index) != NULL)
            && (MATRIX_CTRL_TABLE (i4Index)->matrixControlStatus == RMON_VALID))
        {
            /* Assign the MatrixControlEntry pointer */
            pMatrixControl = MATRIX_CTRL_TABLE (i4Index);
            pMatrix = pMatrixControl->pMatrixSDTable;

            if (pMatrix == NULL)
            {
                continue;
            }
            i4TableSize = (INT4) pMatrixControl->u4MatrixControlTableSize;

            for (i4Matrix = 0; i4Matrix < i4TableSize; i4Matrix++)
            {
                ptempMatrix = pMatrix + i4Matrix;
                i4XmatrixIndex = i4Index;

                if (u1NextFlag)
                {
                    *pi4NextMatrixDSIndex = i4XmatrixIndex;
                    COPY_MAC_ADDRESS (pNextMatrixDSSourceAddress->pu1_OctetList,
                                      &ptempMatrix->matrixSDSourceAddress);
                    pNextMatrixDSSourceAddress->i4_Length = ADDRESS_LENGTH;
                    COPY_MAC_ADDRESS (pNextMatrixDSDestAddress->pu1_OctetList,
                                      &ptempMatrix->matrixSDDestAddress);
                    pNextMatrixDSDestAddress->i4_Length = ADDRESS_LENGTH;
                    u1BreakFlag = TRUE;
                    break;
                }

                COPY_MAC_ADDRESS (ysrcMacAdd,
                                  ptempMatrix->matrixSDSourceAddress);
                COPY_MAC_ADDRESS (ydstMacAdd, ptempMatrix->matrixSDDestAddress);

                if (CMP_MTRX (i4AmatrixIndex, bdstMacAdd, bsrcMacAdd,
                              i4XmatrixIndex, ydstMacAdd, ysrcMacAdd) == 0)
                {

                    if (i4Matrix == (i4TableSize - 1))
                    {
                        u1NextFlag = TRUE;
                    }
                    else
                    {
                        ptempMatrix = ptempMatrix + 1;
                        *pi4NextMatrixDSIndex = i4XmatrixIndex;
                        COPY_MAC_ADDRESS (pNextMatrixDSSourceAddress->
                                          pu1_OctetList,
                                          &ptempMatrix->matrixSDSourceAddress);
                        pNextMatrixDSSourceAddress->i4_Length = ADDRESS_LENGTH;
                        COPY_MAC_ADDRESS (pNextMatrixDSDestAddress->
                                          pu1_OctetList,
                                          &ptempMatrix->matrixSDDestAddress);
                        pNextMatrixDSDestAddress->i4_Length = ADDRESS_LENGTH;
                        u1BreakFlag = TRUE;
                        break;
                    }

                }
            }
        }
    }
    if (u1BreakFlag)
    {
        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix DS index \n");
    return SNMP_FAILURE;
}

  /*
   * Low Level GET Routine for All Objects
   */

/****************************************************************************
 Function    :  nmhGetMatrixDSPkts
 Input       :  The Indices
                i4MatrixDSIndex
                MatrixDSDestAddress
                MatrixDSSourceAddress

                The Object
                pu4RetValMatrixDSPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixDSPkts (INT4 i4MatrixDSIndex,
                    tSNMP_OCTET_STRING_TYPE * pMatrixDSDestAddress,
                    tSNMP_OCTET_STRING_TYPE * pMatrixDSSourceAddress,
                    UINT4 *pu4RetValMatrixDSPkts)
{
    UINT2               u2Index;
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixDSPkts \n");

    if (VALID_MATRIX (i4MatrixDSIndex))
    {
        u2Index = GetCurrentIndex (i4MatrixDSIndex,
                                   pMatrixDSSourceAddress->pu1_OctetList,
                                   pMatrixDSDestAddress->pu1_OctetList);
        if (u2Index == MAX_MATRIX_ENTRY)
        {

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Valid Matrix DS Index but is equal to MAX_MATRIX_ENTRY \n");
            return SNMP_FAILURE;
        }

        *pu4RetValMatrixDSPkts =
            (MATRIX_CTRL_TABLE (i4MatrixDSIndex)->pMatrixSDTable +
             u2Index)->u4MatrixSDPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix DS index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixDSOctets
 Input       :  The Indices
                i4MatrixDSIndex
                MatrixDSDestAddress
                MatrixDSSourceAddress

                The Object
                pu4RetValMatrixDSOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixDSOctets (INT4 i4MatrixDSIndex,
                      tSNMP_OCTET_STRING_TYPE * pMatrixDSDestAddress,
                      tSNMP_OCTET_STRING_TYPE * pMatrixDSSourceAddress,
                      UINT4 *pu4RetValMatrixDSOctets)
{
    UINT2               u2Index;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixDSOctets \n");

    if (VALID_MATRIX (i4MatrixDSIndex))
    {
        u2Index = GetCurrentIndex (i4MatrixDSIndex,
                                   pMatrixDSSourceAddress->pu1_OctetList,
                                   pMatrixDSDestAddress->pu1_OctetList);
        if (u2Index == MAX_MATRIX_ENTRY)
        {

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Received index is out of range (equal to MAX_MATRIX_ENTRY)\n");
            return SNMP_FAILURE;
        }

        *pu4RetValMatrixDSOctets =
            (MATRIX_CTRL_TABLE (i4MatrixDSIndex)->pMatrixSDTable +
             u2Index)->u4MatrixSDOctets;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix DS index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMatrixDSErrors
 Input       :  The Indices
                i4MatrixDSIndex
                MatrixDSDestAddress
                MatrixDSSourceAddress

                The Object
                pu4RetValMatrixDSErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetMatrixDSErrors (INT4 i4MatrixDSIndex,
                      tSNMP_OCTET_STRING_TYPE * pMatrixDSDestAddress,
                      tSNMP_OCTET_STRING_TYPE * pMatrixDSSourceAddress,
                      UINT4 *pu4RetValMatrixDSErrors)
{
    UINT2               u2Index;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetMatrixDSErrors  \n");

    if (VALID_MATRIX (i4MatrixDSIndex))
    {
        u2Index = GetCurrentIndex (i4MatrixDSIndex,
                                   pMatrixDSSourceAddress->pu1_OctetList,
                                   pMatrixDSDestAddress->pu1_OctetList);
        if (u2Index == MAX_MATRIX_ENTRY)
        {

            RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                      "Valid Matrix DS Index but is equal to MAX_MATRIX_ENTRY \n");
            return SNMP_FAILURE;
        }

        *pu4RetValMatrixDSErrors =
            (MATRIX_CTRL_TABLE (i4MatrixDSIndex)->pMatrixSDTable +
             u2Index)->u4MatrixSDErrors;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC, "Invalid Matrix DS Index \n");
    return SNMP_FAILURE;
}

 /*
  * Low Level SET Routine for All Objects
  */

 /*
  * Low Level TEST Routines
  */

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MatrixControlTable
 Input       :  The Indices
                MatrixControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MatrixControlTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
