/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: rmonnpapi.c,v 1.2 2012/12/13 14:38:18 siva Exp $             *
 *                                                                  *
 * Description: It contains definitions for NP wrapper calls         *
 *                                                                  *
 *******************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : RmonFsRmonHwGetEthStatsTable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRmonHwGetEthStatsTable
 *                                                                          
 *    Input(s)            : Arguments of FsRmonHwGetEthStatsTable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __RMON_NP_API_C
#define __RMON_NP_API_C

#include "rmallinc.h"

UINT1
RmonFsRmonHwGetEthStatsTable (UINT4 u4IfIndex,
                              tRmonEtherStatsNode * pEthStatsEntry)
{
    tFsHwNp             FsHwNp;
    tRmonNpModInfo     *pRmonNpModInfo = NULL;
    tRmonNpWrFsRmonHwGetEthStatsTable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMON_MOD,    /* Module ID */
                         FS_RMON_HW_GET_ETH_STATS_TABLE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonNpModInfo = &(FsHwNp.RmonNpModInfo);
    pEntry = &pRmonNpModInfo->RmonNpFsRmonHwGetEthStatsTable;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pEthStatsEntry = pEthStatsEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : RmonFsRmonHwSetEtherStatsTable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsRmonHwSetEtherStatsTable
 *                                                                          
 *    Input(s)            : Arguments of FsRmonHwSetEtherStatsTable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
RmonFsRmonHwSetEtherStatsTable (UINT4 u4IfIndex, UINT1 u1EtherStatsEnable)
{
    tFsHwNp             FsHwNp;
    tRmonNpModInfo     *pRmonNpModInfo = NULL;
    tRmonNpWrFsRmonHwSetEtherStatsTable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_RMON_MOD,    /* Module ID */
                         FS_RMON_HW_SET_ETHER_STATS_TABLE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pRmonNpModInfo = &(FsHwNp.RmonNpModInfo);
    pEntry = &pRmonNpModInfo->RmonNpFsRmonHwSetEtherStatsTable;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1EtherStatsEnable = u1EtherStatsEnable;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#endif /* _RMON_NP_API_C */
