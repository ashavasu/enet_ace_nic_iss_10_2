
/* SOURCE FILE HEADER :
 * ------------------
 *
 *  --------------------------------------------------------------------------
 *    FILE  NAME             :  rmstubs.c                                    *
 *                                                                           *
 *    PRINCIPAL AUTHOR       :  Aricent Inc.                              *
 *                                                                           *
 *    SUBSYSTEM NAME         :  PORTING                                      *
 *                                                                           *
 *    MODULE NAME            :  RMON PORTING                                 *  
 *                                                                           *
 *    LANGUAGE               :  C                                            *
 *                                                                           *
 *    TARGET ENVIRONMENT     :  ANY                                          *
 *                                                                           *
 *    DATE OF FIRST RELEASE  :                                               *
 *                                                                           *
 *    DESCRIPTION            :   contains low level routines of Filter and   *  
 *                               Buffer capture groups                       *   *                                         *
 *  --------------------------------------------------------------------------  
 *
 *     CHANGE RECORD : 
 *     -------------
 *
 *  -------------------------------------------------------------------------
 *    VERSION       |        AUTHOR/       |            DESCRIPTION OF    
 *                  |        DATE          |               CHANGE         
 * -----------------|----------------------|---------------------------------
 *      1.0         |     Vignesh  D       |           Create Original       
 *                  |     19-MAR-1997      |                                 
 * --------------------------------------------------------------------------
 *      2.0         |     Yogindhar P      |   Convertion of Function headers   
 *                  |     20-AUG-2001      |   compatible to snmpv2.  
 *                  |                      |                    
 * --------------------------------------------------------------------------
 */

#include "rmallinc.h"
/*************************************************/
/**** $$TRACE_MODULE_NAME     = RMON_COMMON   ****/
/**** $$TRACE_SUB_MODULE_NAME = LOW           ****/
/*************************************************/

/* LOW LEVEL Routines for Table : FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFilterTable
 Input       :  The Indices
                FilterIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFilterTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFilterTable (INT4 i4FilterIndex)
{
    RMON_DUMMY (i4FilterIndex);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFilterTable
 Input       :  The Indices
                FilterIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFilterTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFilterTable (INT4 *pi4FilterIndex)
{
    RMON_DUMMY (pi4FilterIndex);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", *pi4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFilterTable
 Input       :  The Indices
                FilterIndex
                nextFilterIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFilterTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFilterTable (INT4 i4FilterIndex, INT4 *pi4NextFilterIndex)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4NextFilterIndex);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", *pi4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFilterChannelIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterChannelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterChannelIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterChannelIndex (INT4 i4FilterIndex, INT4 *pi4RetValFilterChannelIndex)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4RetValFilterChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktDataOffset
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktDataOffset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktDataOffset ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktDataOffset (INT4 i4FilterIndex,
                           INT4 *pi4RetValFilterPktDataOffset)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4RetValFilterPktDataOffset);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktData
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktData ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktData (INT4 i4FilterIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValFilterPktData)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pRetValFilterPktData);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktDataMask
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktDataMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktDataMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktDataMask (INT4 i4FilterIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFilterPktDataMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pRetValFilterPktDataMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktDataNotMask
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktDataNotMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktDataNotMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktDataNotMask (INT4 i4FilterIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFilterPktDataNotMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pRetValFilterPktDataNotMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktStatus
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktStatus (INT4 i4FilterIndex, INT4 *pi4RetValFilterPktStatus)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4RetValFilterPktStatus);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktStatusMask
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktStatusMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktStatusMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktStatusMask (INT4 i4FilterIndex,
                           INT4 *pi4RetValFilterPktStatusMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4RetValFilterPktStatusMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterPktStatusNotMask
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterPktStatusNotMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterPktStatusNotMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterPktStatusNotMask (INT4 i4FilterIndex,
                              INT4 *pi4RetValFilterPktStatusNotMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4RetValFilterPktStatusNotMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterOwner
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterOwner (INT4 i4FilterIndex,
                   tSNMP_OCTET_STRING_TYPE * pRetValFilterOwner)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pRetValFilterOwner);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFilterStatus
 Input       :  The Indices
                FilterIndex

                The Object 
                retValFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFilterStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFilterStatus (INT4 i4FilterIndex, INT4 *pi4RetValFilterStatus)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pi4RetValFilterStatus);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFilterChannelIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterChannelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterChannelIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterChannelIndex (INT4 i4FilterIndex, INT4 i4SetValFilterChannelIndex)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4SetValFilterChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterChannelIndex = %d\n", i4SetValFilterChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktDataOffset
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktDataOffset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktDataOffset ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktDataOffset (INT4 i4FilterIndex, INT4 i4SetValFilterPktDataOffset)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4SetValFilterPktDataOffset);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktDataOffset = %d\n", i4SetValFilterPktDataOffset); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktData
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktData
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktData ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktData (INT4 i4FilterIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValFilterPktData)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pSetValFilterPktData);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktDataMask
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktDataMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktDataMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktDataMask (INT4 i4FilterIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValFilterPktDataMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pSetValFilterPktDataMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktDataNotMask
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktDataNotMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktDataNotMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktDataNotMask (INT4 i4FilterIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFilterPktDataNotMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pSetValFilterPktDataNotMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktStatus
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktStatus (INT4 i4FilterIndex, INT4 i4SetValFilterPktStatus)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4SetValFilterPktStatus);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktStatus = %d\n", i4SetValFilterPktStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktStatusMask
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktStatusMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktStatusMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktStatusMask (INT4 i4FilterIndex, INT4 i4SetValFilterPktStatusMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4SetValFilterPktStatusMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktStatusMask = %d\n", i4SetValFilterPktStatusMask); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterPktStatusNotMask
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterPktStatusNotMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterPktStatusNotMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterPktStatusNotMask (INT4 i4FilterIndex,
                              INT4 i4SetValFilterPktStatusNotMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4SetValFilterPktStatusNotMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktStatusNotMask = %d\n", i4SetValFilterPktStatusNotMask); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterOwner
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterOwner (INT4 i4FilterIndex,
                   tSNMP_OCTET_STRING_TYPE * pSetValFilterOwner)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pSetValFilterOwner);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFilterStatus
 Input       :  The Indices
                FilterIndex

                The Object 
                setValFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFilterStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFilterStatus (INT4 i4FilterIndex, INT4 i4SetValFilterStatus)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4SetValFilterStatus);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterStatus = %d\n", i4SetValFilterStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FilterTable
 Input       :  The Indices
                FilterIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FilterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FilterChannelIndex
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterChannelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterChannelIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterChannelIndex (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                             INT4 i4TestValFilterChannelIndex)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4TestValFilterChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterChannelIndex = %d\n", i4TestValFilterChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktDataOffset
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktDataOffset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktDataOffset ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktDataOffset (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                              INT4 i4TestValFilterPktDataOffset)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4TestValFilterPktDataOffset);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktDataOffset = %d\n", i4TestValFilterPktDataOffset); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktData
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktData
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktData ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktData (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValFilterPktData)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pTestValFilterPktData);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktDataMask
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktDataMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktDataMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktDataMask (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValFilterPktDataMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pTestValFilterPktDataMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktDataNotMask
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktDataNotMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktDataNotMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktDataNotMask (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFilterPktDataNotMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pTestValFilterPktDataNotMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktStatus
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktStatus (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                          INT4 i4TestValFilterPktStatus)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4TestValFilterPktStatus);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktStatus = %d\n", i4TestValFilterPktStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktStatusMask
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktStatusMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktStatusMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktStatusMask (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                              INT4 i4TestValFilterPktStatusMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4TestValFilterPktStatusMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktStatusMask = %d\n", i4TestValFilterPktStatusMask); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterPktStatusNotMask
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterPktStatusNotMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterPktStatusNotMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterPktStatusNotMask (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                                 INT4 i4TestValFilterPktStatusNotMask)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4TestValFilterPktStatusNotMask);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterPktStatusNotMask = %d\n", i4TestValFilterPktStatusNotMask); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterOwner
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterOwner (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                      tSNMP_OCTET_STRING_TYPE * pTestValFilterOwner)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (pTestValFilterOwner);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FilterStatus
 Input       :  The Indices
                FilterIndex

                The Object 
                testValFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FilterStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FilterStatus (UINT4 *pu4ErrorCode, INT4 i4FilterIndex,
                       INT4 i4TestValFilterStatus)
{
    RMON_DUMMY (i4FilterIndex);
    RMON_DUMMY (i4TestValFilterStatus);
   /*** $$TRACE_LOG (ENTRY, "FilterIndex = %d\n", i4FilterIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FilterStatus = %d\n", i4TestValFilterStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : ChannelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceChannelTable
 Input       :  The Indices
                ChannelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceChannelTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceChannelTable (INT4 i4ChannelIndex)
{
    RMON_DUMMY (i4ChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexChannelTable
 Input       :  The Indices
                ChannelIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstChannelTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexChannelTable (INT4 *pi4ChannelIndex)
{
    RMON_DUMMY (pi4ChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", *pi4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexChannelTable
 Input       :  The Indices
                ChannelIndex
                nextChannelIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextChannelTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexChannelTable (INT4 i4ChannelIndex, INT4 *pi4NextChannelIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4NextChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", *pi4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetChannelIfIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelIfIndex (INT4 i4ChannelIndex, INT4 *pi4RetValChannelIfIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelIfIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelAcceptType
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelAcceptType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelAcceptType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelAcceptType (INT4 i4ChannelIndex, INT4 *pi4RetValChannelAcceptType)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelAcceptType);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelDataControl
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelDataControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelDataControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelDataControl (INT4 i4ChannelIndex,
                          INT4 *pi4RetValChannelDataControl)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelDataControl);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelTurnOnEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelTurnOnEventIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelTurnOnEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelTurnOnEventIndex (INT4 i4ChannelIndex,
                               INT4 *pi4RetValChannelTurnOnEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelTurnOnEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelTurnOffEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelTurnOffEventIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelTurnOffEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelTurnOffEventIndex (INT4 i4ChannelIndex,
                                INT4 *pi4RetValChannelTurnOffEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelTurnOffEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelEventIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelEventIndex (INT4 i4ChannelIndex, INT4 *pi4RetValChannelEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelEventStatus
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelEventStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelEventStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelEventStatus (INT4 i4ChannelIndex,
                          INT4 *pi4RetValChannelEventStatus)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelEventStatus);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelMatches
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelMatches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelMatches ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelMatches (INT4 i4ChannelIndex, UINT4 *pu4RetValChannelMatches)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pu4RetValChannelMatches);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelDescription
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelDescription ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelDescription (INT4 i4ChannelIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValChannelDescription)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pRetValChannelDescription);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelOwner
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelOwner (INT4 i4ChannelIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValChannelOwner)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pRetValChannelOwner);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetChannelStatus
 Input       :  The Indices
                ChannelIndex

                The Object 
                retValChannelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetChannelStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetChannelStatus (INT4 i4ChannelIndex, INT4 *pi4RetValChannelStatus)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pi4RetValChannelStatus);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetChannelIfIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelIfIndex (INT4 i4ChannelIndex, INT4 i4SetValChannelIfIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelIfIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelIfIndex = %d\n", i4SetValChannelIfIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelAcceptType
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelAcceptType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelAcceptType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelAcceptType (INT4 i4ChannelIndex, INT4 i4SetValChannelAcceptType)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelAcceptType);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelAcceptType = %d\n", i4SetValChannelAcceptType); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelDataControl
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelDataControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelDataControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelDataControl (INT4 i4ChannelIndex, INT4 i4SetValChannelDataControl)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelDataControl);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelDataControl = %d\n", i4SetValChannelDataControl); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelTurnOnEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelTurnOnEventIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelTurnOnEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelTurnOnEventIndex (INT4 i4ChannelIndex,
                               INT4 i4SetValChannelTurnOnEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelTurnOnEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelTurnOnEventIndex = %d\n", i4SetValChannelTurnOnEventIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelTurnOffEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelTurnOffEventIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelTurnOffEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelTurnOffEventIndex (INT4 i4ChannelIndex,
                                INT4 i4SetValChannelTurnOffEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelTurnOffEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelTurnOffEventIndex = %d\n", i4SetValChannelTurnOffEventIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelEventIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelEventIndex (INT4 i4ChannelIndex, INT4 i4SetValChannelEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelEventIndex = %d\n", i4SetValChannelEventIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelEventStatus
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelEventStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelEventStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelEventStatus (INT4 i4ChannelIndex, INT4 i4SetValChannelEventStatus)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelEventStatus);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelEventStatus = %d\n", i4SetValChannelEventStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelDescription
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelDescription ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelDescription (INT4 i4ChannelIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValChannelDescription)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pSetValChannelDescription);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelOwner
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelOwner (INT4 i4ChannelIndex,
                    tSNMP_OCTET_STRING_TYPE * pSetValChannelOwner)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pSetValChannelOwner);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetChannelStatus
 Input       :  The Indices
                ChannelIndex

                The Object 
                setValChannelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetChannelStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetChannelStatus (INT4 i4ChannelIndex, INT4 i4SetValChannelStatus)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4SetValChannelStatus);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelStatus = %d\n", i4SetValChannelStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2ChannelTable
 Input       :  The Indices
                ChannelIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2ChannelTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2ChannelIfIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelIfIndex (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                         INT4 i4TestValChannelIfIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelIfIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelIfIndex = %d\n", i4TestValChannelIfIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelAcceptType
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelAcceptType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelAcceptType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelAcceptType (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                            INT4 i4TestValChannelAcceptType)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelAcceptType);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelAcceptType = %d\n", i4TestValChannelAcceptType); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelDataControl
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelDataControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelDataControl ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelDataControl (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                             INT4 i4TestValChannelDataControl)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelDataControl);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelDataControl = %d\n", i4TestValChannelDataControl); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelTurnOnEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelTurnOnEventIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelTurnOnEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelTurnOnEventIndex (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                                  INT4 i4TestValChannelTurnOnEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelTurnOnEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelTurnOnEventIndex = %d\n", i4TestValChannelTurnOnEventIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelTurnOffEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelTurnOffEventIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelTurnOffEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelTurnOffEventIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4ChannelIndex,
                                   INT4 i4TestValChannelTurnOffEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelTurnOffEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelTurnOffEventIndex = %d\n", i4TestValChannelTurnOffEventIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelEventIndex
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelEventIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelEventIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelEventIndex (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                            INT4 i4TestValChannelEventIndex)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelEventIndex);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelEventIndex = %d\n", i4TestValChannelEventIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelEventStatus
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelEventStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelEventStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelEventStatus (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                             INT4 i4TestValChannelEventStatus)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelEventStatus);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelEventStatus = %d\n", i4TestValChannelEventStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelDescription
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelDescription ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelDescription (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValChannelDescription)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pTestValChannelDescription);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelOwner
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelOwner (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                       tSNMP_OCTET_STRING_TYPE * pTestValChannelOwner)
{
    RMON_DUMMY (*pu4ErrorCode);
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (pTestValChannelOwner);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2ChannelStatus
 Input       :  The Indices
                ChannelIndex

                The Object 
                testValChannelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2ChannelStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2ChannelStatus (UINT4 *pu4ErrorCode, INT4 i4ChannelIndex,
                        INT4 i4TestValChannelStatus)
{
    RMON_DUMMY (i4ChannelIndex);
    RMON_DUMMY (i4TestValChannelStatus);
   /*** $$TRACE_LOG (ENTRY, "ChannelIndex = %d\n", i4ChannelIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "ChannelStatus = %d\n", i4TestValChannelStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : BufferControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceBufferControlTable
 Input       :  The Indices
                BufferControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceBufferControlTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceBufferControlTable (INT4 i4BufferControlIndex)
{
    RMON_DUMMY (i4BufferControlIndex);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBufferControlTable
 Input       :  The Indices
                BufferControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstBufferControlTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexBufferControlTable (INT4 *pi4BufferControlIndex)
{
    RMON_DUMMY (pi4BufferControlIndex);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", *pi4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBufferControlTable
 Input       :  The Indices
                BufferControlIndex
                nextBufferControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextBufferControlTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexBufferControlTable (INT4 i4BufferControlIndex,
                                   INT4 *pi4NextBufferControlIndex)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4NextBufferControlIndex);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", *pi4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBufferControlChannelIndex
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlChannelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlChannelIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlChannelIndex (INT4 i4BufferControlIndex,
                                 INT4 *pi4RetValBufferControlChannelIndex)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlFullStatus
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlFullStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlFullStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlFullStatus (INT4 i4BufferControlIndex,
                               INT4 *pi4RetValBufferControlFullStatus)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlFullStatus);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlFullAction
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlFullAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlFullAction ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlFullAction (INT4 i4BufferControlIndex,
                               INT4 *pi4RetValBufferControlFullAction)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlFullAction);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlCaptureSliceSize
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlCaptureSliceSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlCaptureSliceSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlCaptureSliceSize (INT4 i4BufferControlIndex,
                                     INT4
                                     *pi4RetValBufferControlCaptureSliceSize)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlCaptureSliceSize);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlDownloadSliceSize
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlDownloadSliceSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlDownloadSliceSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlDownloadSliceSize (INT4 i4BufferControlIndex,
                                      INT4
                                      *pi4RetValBufferControlDownloadSliceSize)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlDownloadSliceSize);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlDownloadOffset
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlDownloadOffset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlDownloadOffset ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlDownloadOffset (INT4 i4BufferControlIndex,
                                   INT4 *pi4RetValBufferControlDownloadOffset)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlDownloadOffset);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlMaxOctetsRequested
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlMaxOctetsRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlMaxOctetsRequested ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlMaxOctetsRequested (INT4 i4BufferControlIndex,
                                       INT4
                                       *pi4RetValBufferControlMaxOctetsRequested)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlMaxOctetsRequested);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlMaxOctetsGranted
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlMaxOctetsGranted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlMaxOctetsGranted ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlMaxOctetsGranted (INT4 i4BufferControlIndex,
                                     INT4
                                     *pi4RetValBufferControlMaxOctetsGranted)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlMaxOctetsGranted);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlCapturedPackets
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlCapturedPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlCapturedPackets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlCapturedPackets (INT4 i4BufferControlIndex,
                                    INT4 *pi4RetValBufferControlCapturedPackets)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlCapturedPackets);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlTurnOnTime
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlTurnOnTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlTurnOnTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlTurnOnTime (INT4 i4BufferControlIndex,
                               UINT4 *pu4RetValBufferControlTurnOnTime)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pu4RetValBufferControlTurnOnTime);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlOwner
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlOwner (INT4 i4BufferControlIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValBufferControlOwner)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pRetValBufferControlOwner);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetBufferControlStatus
 Input       :  The Indices
                BufferControlIndex

                The Object 
                retValBufferControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetBufferControlStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetBufferControlStatus (INT4 i4BufferControlIndex,
                           INT4 *pi4RetValBufferControlStatus)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pi4RetValBufferControlStatus);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetBufferControlChannelIndex
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlChannelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlChannelIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlChannelIndex (INT4 i4BufferControlIndex,
                                 INT4 i4SetValBufferControlChannelIndex)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlChannelIndex = %d\n", i4SetValBufferControlChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlFullAction
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlFullAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlFullAction ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlFullAction (INT4 i4BufferControlIndex,
                               INT4 i4SetValBufferControlFullAction)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlFullAction);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlFullAction = %d\n", i4SetValBufferControlFullAction); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlCaptureSliceSize
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlCaptureSliceSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlCaptureSliceSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlCaptureSliceSize (INT4 i4BufferControlIndex,
                                     INT4 i4SetValBufferControlCaptureSliceSize)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlCaptureSliceSize);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlCaptureSliceSize = %d\n", i4SetValBufferControlCaptureSliceSize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlDownloadSliceSize
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlDownloadSliceSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlDownloadSliceSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlDownloadSliceSize (INT4 i4BufferControlIndex,
                                      INT4
                                      i4SetValBufferControlDownloadSliceSize)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlDownloadSliceSize);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlDownloadSliceSize = %d\n", i4SetValBufferControlDownloadSliceSize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlDownloadOffset
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlDownloadOffset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlDownloadOffset ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlDownloadOffset (INT4 i4BufferControlIndex,
                                   INT4 i4SetValBufferControlDownloadOffset)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlDownloadOffset);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlDownloadOffset = %d\n", i4SetValBufferControlDownloadOffset); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlMaxOctetsRequested
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlMaxOctetsRequested
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlMaxOctetsRequested ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlMaxOctetsRequested (INT4 i4BufferControlIndex,
                                       INT4
                                       i4SetValBufferControlMaxOctetsRequested)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlMaxOctetsRequested);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlMaxOctetsRequested = %d\n", i4SetValBufferControlMaxOctetsRequested); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlOwner
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlOwner (INT4 i4BufferControlIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValBufferControlOwner)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pSetValBufferControlOwner);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetBufferControlStatus
 Input       :  The Indices
                BufferControlIndex

                The Object 
                setValBufferControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetBufferControlStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetBufferControlStatus (INT4 i4BufferControlIndex,
                           INT4 i4SetValBufferControlStatus)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4SetValBufferControlStatus);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlStatus = %d\n", i4SetValBufferControlStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2BufferControlTable
 Input       :  The Indices
                BufferControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2BufferControlTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2BufferControlChannelIndex
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlChannelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlChannelIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlChannelIndex (UINT4 *pu4ErrorCode,
                                    INT4 i4BufferControlIndex,
                                    INT4 i4TestValBufferControlChannelIndex)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlChannelIndex);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlChannelIndex = %d\n", i4TestValBufferControlChannelIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlFullAction
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlFullAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlFullAction ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlFullAction (UINT4 *pu4ErrorCode,
                                  INT4 i4BufferControlIndex,
                                  INT4 i4TestValBufferControlFullAction)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlFullAction);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlFullAction = %d\n", i4TestValBufferControlFullAction); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlCaptureSliceSize
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlCaptureSliceSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlCaptureSliceSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlCaptureSliceSize (UINT4 *pu4ErrorCode,
                                        INT4 i4BufferControlIndex,
                                        INT4
                                        i4TestValBufferControlCaptureSliceSize)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlCaptureSliceSize);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlCaptureSliceSize = %d\n", i4TestValBufferControlCaptureSliceSize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlDownloadSliceSize
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlDownloadSliceSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val

 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlDownloadSliceSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlDownloadSliceSize (UINT4 *pu4ErrorCode,
                                         INT4 i4BufferControlIndex,
                                         INT4
                                         i4TestValBufferControlDownloadSliceSize)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlDownloadSliceSize);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlDownloadSliceSize = %d\n", i4TestValBufferControlDownloadSliceSize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlDownloadOffset
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlDownloadOffset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlDownloadOffset ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlDownloadOffset (UINT4 *pu4ErrorCode,
                                      INT4 i4BufferControlIndex,
                                      INT4 i4TestValBufferControlDownloadOffset)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlDownloadOffset);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlDownloadOffset = %d\n", i4TestValBufferControlDownloadOffset); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlMaxOctetsRequested
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlMaxOctetsRequested
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlMaxOctetsRequested ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlMaxOctetsRequested (UINT4 *pu4ErrorCode,
                                          INT4 i4BufferControlIndex,
                                          INT4
                                          i4TestValBufferControlMaxOctetsRequested)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlMaxOctetsRequested);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlMaxOctetsRequested = %d\n", i4TestValBufferControlMaxOctetsRequested); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlOwner
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlOwner ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlOwner (UINT4 *pu4ErrorCode, INT4 i4BufferControlIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValBufferControlOwner)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (pTestValBufferControlOwner);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BufferControlStatus
 Input       :  The Indices
                BufferControlIndex

                The Object 
                testValBufferControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2BufferControlStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2BufferControlStatus (UINT4 *pu4ErrorCode,
                              INT4 i4BufferControlIndex,
                              INT4 i4TestValBufferControlStatus)
{
    RMON_DUMMY (i4BufferControlIndex);
    RMON_DUMMY (i4TestValBufferControlStatus);
   /*** $$TRACE_LOG (ENTRY, "BufferControlIndex = %d\n", i4BufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "BufferControlStatus = %d\n", i4TestValBufferControlStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : CaptureBufferTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCaptureBufferTable
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceCaptureBufferTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceCaptureBufferTable (INT4 i4CaptureBufferControlIndex,
                                            INT4 i4CaptureBufferIndex)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", i4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", i4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexCaptureBufferTable
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstCaptureBufferTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexCaptureBufferTable (INT4 *pi4CaptureBufferControlIndex,
                                    INT4 *pi4CaptureBufferIndex)
{
    RMON_DUMMY (pi4CaptureBufferControlIndex);
    RMON_DUMMY (pi4CaptureBufferIndex);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", *pi4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", *pi4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexCaptureBufferTable
 Input       :  The Indices
                CaptureBufferControlIndex
                nextCaptureBufferControlIndex
                CaptureBufferIndex
                nextCaptureBufferIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextCaptureBufferTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexCaptureBufferTable (INT4 i4CaptureBufferControlIndex,
                                   INT4 *pi4NextCaptureBufferControlIndex,
                                   INT4 i4CaptureBufferIndex,
                                   INT4 *pi4NextCaptureBufferIndex)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (pi4NextCaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
    RMON_DUMMY (pi4NextCaptureBufferIndex);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", *pi4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", *pi4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetCaptureBufferPacketID
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex

                The Object 
                retValCaptureBufferPacketID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetCaptureBufferPacketID ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetCaptureBufferPacketID (INT4 i4CaptureBufferControlIndex,
                             INT4 i4CaptureBufferIndex,
                             INT4 *pi4RetValCaptureBufferPacketID)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
    RMON_DUMMY (pi4RetValCaptureBufferPacketID);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", i4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", i4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetCaptureBufferPacketData
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex

                The Object 
                retValCaptureBufferPacketData
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetCaptureBufferPacketData ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetCaptureBufferPacketData (INT4 i4CaptureBufferControlIndex,
                               INT4 i4CaptureBufferIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValCaptureBufferPacketData)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
    RMON_DUMMY (pRetValCaptureBufferPacketData);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", i4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", i4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetCaptureBufferPacketLength
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex

                The Object 
                retValCaptureBufferPacketLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetCaptureBufferPacketLength ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetCaptureBufferPacketLength (INT4 i4CaptureBufferControlIndex,
                                 INT4 i4CaptureBufferIndex,
                                 INT4 *pi4RetValCaptureBufferPacketLength)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
    RMON_DUMMY (pi4RetValCaptureBufferPacketLength);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", i4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", i4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetCaptureBufferPacketTime
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex

                The Object 
                retValCaptureBufferPacketTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetCaptureBufferPacketTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetCaptureBufferPacketTime (INT4 i4CaptureBufferControlIndex,
                               INT4 i4CaptureBufferIndex,
                               INT4 *pi4RetValCaptureBufferPacketTime)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
    RMON_DUMMY (pi4RetValCaptureBufferPacketTime);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", i4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", i4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetCaptureBufferPacketStatus
 Input       :  The Indices
                CaptureBufferControlIndex
                CaptureBufferIndex

                The Object 
                retValCaptureBufferPacketStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetCaptureBufferPacketStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetCaptureBufferPacketStatus (INT4 i4CaptureBufferControlIndex,
                                 INT4 i4CaptureBufferIndex,
                                 INT4 *pi4RetValCaptureBufferPacketStatus)
{
    RMON_DUMMY (i4CaptureBufferControlIndex);
    RMON_DUMMY (i4CaptureBufferIndex);
    RMON_DUMMY (pi4RetValCaptureBufferPacketStatus);
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferControlIndex = %d\n", i4CaptureBufferControlIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "CaptureBufferIndex = %d\n", i4CaptureBufferIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_FAILURE;
}
