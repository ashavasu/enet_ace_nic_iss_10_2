/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: ralhost.c,v 1.35 2015/04/23 12:04:49 siva Exp $             *
 *                                                                  *
 * Description: Contains SNMP low level routines for Host Group     *
 *                                                                  *
 *******************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  ralhost.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  APERIODIC                                    |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides low level routines for   |
 * |                             accessing Host /Host Time Table variables   |
 * |                                                                         |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |     D. Vignesh      |         Create Original          |
 * |                |    19-MAR-1997      |                                  |
 *  -------------------------------------------------------------------------
 * |  2.0           |    Yogindhar P      |  changing function signatures    |
 * |                |    16-AUG-2001      |  compatible to SNMPv2            |
 *  -------------------------------------------------------------------------
 * |  3.0           |    LEELA L          |  changes for Dynamic Tables      |
 * |                |    31-MAR-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  4.0           |    LEELA L          |  changes for New MIDGEN Tool     |
 * |                |    07-JUL-2002      |                                  |
 *  -------------------------------------------------------------------------
 *
 */

#include "rmallinc.h"
#include "rmgr.h"

extern UINT4        HostControlStatus[11];
/*
* NOT_INVALID_HOST_CONTROL is not the same as VALID_HOST_CONTROL
* it is has the value TRUE when the status is in
* VALID or RMON_UNDER_CREATION or RMON_CREATE_REQUEST State
*/
#define NOT_INVALID_HOST_CONTROL(index) (HOST_CONTROL_STATUS(index) !=\
                                        RMON_INVALID)

/* LOW LEVEL Routines for Table : HostControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceHostControlTable
 Input       :  The Indices
                HostControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceHostControlTable (INT4 i4HostControlIndex)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function: \
                    nmhValidateIndexInstanceHostControlTable \n");

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HostControlIndex > 0)
        && (i4HostControlIndex < RMON_MAX_INTERFACE_LIMIT)
        && (HOST_CONTROL (i4HostControlIndex) != NULL))
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: HostControlIndex validation success \n ");
        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: HostControlIndex is out of range \n ");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexHostControlTable
 Input       :  The Indices
                HostControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexHostControlTable (INT4 *pi4HostControlIndex)
{
    INT4                i4Zero = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entering into the function: \
                    nmhGetFirstIndexHostControlTable \n");

    return nmhGetNextIndexHostControlTable (i4Zero, pi4HostControlIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexHostControlTable
 Input       :  The Indices
                HostControlIndex
                nextHostControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostControlTable (INT4 i4HostControlIndex, INT4
                                 *pi4NextHostControlIndex)
{
    INT4                i4Index = (i4HostControlIndex + 1);

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhGetNextIndexHostControlTable \n");

    if (i4Index < RMON_ZERO)
    {
        RMON_DBG (MAJOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Index is less than zero.\n");
        return SNMP_FAILURE;
    }

    for (; i4Index < RMON_MAX_INTERFACE_LIMIT; i4Index++)
    {
        if (HOST_CONTROL (i4Index) != NULL)
        {
            *pi4NextHostControlIndex = i4Index;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure : All HostControl entries are in INVALID  State \n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetHostControlDataSource
 Input       :  The Indices
                HostControlIndex

                The Object
                retValHostControlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostControlDataSource (INT4 i4HostControlIndex, tSNMP_OID_TYPE
                             * pRetValHostControlDataSource)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostControlDataSource \n");

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (NOT_INVALID_HOST_CONTROL (i4HostControlIndex))
        {
            GET_DATA_SOURCE_OID (pRetValHostControlDataSource,
                                 HOST_CONTROL (i4HostControlIndex)->
                                 u4HostControlDataSource,
                                 (UINT4) PORT_INDEX_TYPE);
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: INVALID HostControl Index \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostControlTableSize
 Input       :  The Indices
                HostControlIndex

                The Object
                retValHostControlTableSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostControlTableSize (INT4 i4HostControlIndex, INT4
                            *pi4RetValHostControlTableSize)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostControlTableSize \n");

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (VALID_HOST_CONTROL (i4HostControlIndex))
        {
            *pi4RetValHostControlTableSize =
                HOST_CONTROL (i4HostControlIndex)->u2HostControlTableSize;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry is not in VALID State. \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostControlLastDeleteTime
 Input       :  The Indices
                HostControlIndex

                The Object
                retValHostControlLastDeleteTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostControlLastDeleteTime (INT4 i4HostControlIndex, UINT4
                                 *pu4RetValHostControlLastDeleteTime)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhGetHostControlLastDeleteTime \n");

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (VALID_HOST_CONTROL (i4HostControlIndex))
        {
            *pu4RetValHostControlLastDeleteTime =
                HOST_CONTROL (i4HostControlIndex)->hostControlLastDeleteTime;
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry is not in VALID State\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostControlOwner
 Input       :  The Indices
                HostControlIndex

                The Object
                retValHostControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostControlOwner (INT4 i4HostControlIndex, tSNMP_OCTET_STRING_TYPE
                        * pRetValHostControlOwner)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostControlOwner \n");

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (NOT_INVALID_HOST_CONTROL (i4HostControlIndex))
        {
            STRNCPY (pRetValHostControlOwner->pu1_OctetList,
                     HOST_CONTROL (i4HostControlIndex)->u1HostControlOwner,
                     OWN_STR_LENGTH);

            pRetValHostControlOwner->i4_Length =
                (INT4) STRLEN (HOST_CONTROL (i4HostControlIndex)->
                               u1HostControlOwner);

            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry is in INVALID State\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostControlStatus
 Input       :  The Indices
                HostControlIndex

                The Object
                retValHostControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostControlStatus (INT4 i4HostControlIndex, INT4
                         *pi4RetValHostControlStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostControlStatus \n");

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (HOST_CONTROL_STATUS (i4HostControlIndex) != RMON_INVALID)
        {
            *pi4RetValHostControlStatus =
                HOST_CONTROL_STATUS (i4HostControlIndex);
            return SNMP_SUCCESS;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry is in INVALID State\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetHostControlDataSource
 Input       :  The Indices
                HostControlIndex

                The Object
                setValHostControlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostControlDataSource (INT4 i4HostControlIndex, tSNMP_OID_TYPE
                             * pSetValHostControlDataSource)
{
    UINT4               u4OidType;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetHostControlDataSource \n");

    SET_DATA_SOURCE_OID (pSetValHostControlDataSource,
                         &(HOST_CONTROL (i4HostControlIndex)->
                           u4HostControlDataSource), &u4OidType);
    /* Type of OID (VLAN or PORT) is set in u4OidType */
    /* Currently only PORT will be returned */

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetHostControlDataSource \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostControlOwner
 Input       :  The Indices
                HostControlIndex

                The Object
                setValHostControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostControlOwner (INT4 i4HostControlIndex, tSNMP_OCTET_STRING_TYPE
                        * pSetValHostControlOwner)
{
    UINT4               u4Minimum;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: nmhSetHostControlOwner \n");

    u4Minimum = (UINT4) (pSetValHostControlOwner->i4_Length < (OWN_STR_LENGTH-1) ?
                         pSetValHostControlOwner->i4_Length : (OWN_STR_LENGTH-1));

    MEMSET (HOST_CONTROL (i4HostControlIndex)->u1HostControlOwner, 0,
            OWN_STR_LENGTH);

    STRNCPY (HOST_CONTROL (i4HostControlIndex)->u1HostControlOwner,
             pSetValHostControlOwner->pu1_OctetList, u4Minimum);

    HOST_CONTROL (i4HostControlIndex)->u1HostControlOwner[u4Minimum] = '\0';

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              " Leaving the function: nmhSetHostControlOwner \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetHostControlStatus
 Input       :  The Indices
                HostControlIndex

                The Object
                setValHostControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetHostControlStatus (INT4 i4HostControlIndex, INT4
                         i4SetValHostControlStatus)
{
#if defined(NPAPI_WANTED) && defined(EXTND_SERVICE)
    UINT1               u1HostControlEnable;
#endif
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhSetHostControlStatus \n");

    MEMSET (&SnmpNotifyInfo, RMON_ZERO, sizeof (tSnmpNotifyInfo));

    /*Check and allocate the memory for the *
     * control entry of the host Control table     */

    if ((HOST_CONTROL (i4HostControlIndex) == NULL) &&
        (i4SetValHostControlStatus == RMON_CREATE_REQUEST))
    {
        HOST_CONTROL (i4HostControlIndex) =
            (tHostControl *) MemAllocMemBlk (gRmonHostCtrlPoolId);

        if (HOST_CONTROL (i4HostControlIndex) == NULL)
        {
            RMON_DBG (CRITICAL_TRC | MEM_TRC,
                      " Memory Allocation Failure - HostControlTable. \n");
            return SNMP_FAILURE;
        }

        HOST_CONTROL (i4HostControlIndex)->u2HostControlTableSize = 0;
        HOST_CONTROL (i4HostControlIndex)->hostControlStatus = RMON_INVALID;
    }
    else if (HOST_CONTROL (i4HostControlIndex) == NULL)
    {
        if (i4SetValHostControlStatus == RMON_INVALID)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            /* The request may valid or undercreation, this is not a valid 
             * operation.*/
            return SNMP_FAILURE;
        }
    }

    switch (HOST_CONTROL (i4HostControlIndex)->hostControlStatus)
    {

        case RMON_VALID:
            if (i4SetValHostControlStatus == RMON_INVALID)
            {
                /* Free the Memory for the Host Data Table */
                if (HOST_CONTROL (i4HostControlIndex)->pHostTable != NULL)
                {
                    MemReleaseMemBlock (gRmonHostTablePoolId, (UINT1 *)
                                        (HOST_CONTROL (i4HostControlIndex)->
                                         pHostTable));
                    KER_EQ_MEM (HOST_CONTROL (i4HostControlIndex)->
                                hostControlStatus, i4SetValHostControlStatus);
                }

                /* Free the Memory for the Host Control entry  */
                if (HOST_CONTROL (i4HostControlIndex) != NULL)
                {
                    MemReleaseMemBlock
                        (gRmonHostCtrlPoolId,
                         (UINT1 *) HOST_CONTROL (i4HostControlIndex));
                    HOST_CONTROL (i4HostControlIndex) = NULL;
                }

            }
            else if (i4SetValHostControlStatus == RMON_UNDER_CREATION)
            {
                if (HOST_CONTROL (i4HostControlIndex)->pHostTable != NULL)
                {
                    /* Assign NULL to the Control Entry */
                    MemReleaseMemBlock (gRmonHostTablePoolId, (UINT1 *)
                                        (HOST_CONTROL (i4HostControlIndex)->
                                         pHostTable));
                    HOST_CONTROL (i4HostControlIndex)->pHostTable = NULL;
                }

                HOST_CONTROL (i4HostControlIndex)->u2HostControlTableSize = 0;
                KER_EQ_MEM (HOST_CONTROL (i4HostControlIndex)->
                            hostControlStatus, i4SetValHostControlStatus);

                RmonStartTimer (HOST_CONTROL_TABLE, i4HostControlIndex,
                                HOST_TABLE_DURATION);
            }
            break;

            /*  case RMON_CREATE_REQUEST :    The status is automatically changed */
            /* to UNDER_CREATION after setting the  default values. */
        case RMON_CREATE_REQUEST:
            return SNMP_FAILURE;

        case RMON_UNDER_CREATION:
            if (i4SetValHostControlStatus == RMON_UNDER_CREATION)
            {
                return SNMP_SUCCESS;
            }

            if (i4SetValHostControlStatus == RMON_VALID)
            {
                /* First Stop the timer */
                RmonStopTimer (HOST_CONTROL_TABLE, i4HostControlIndex);

                /* Allocate Memory for the Hostdata Table */
                HOST_CONTROL (i4HostControlIndex)->pHostTable =
                    MemAllocMemBlk (gRmonHostTablePoolId);

                if (HOST_CONTROL (i4HostControlIndex)->pHostTable == NULL)
                {
                    RMON_DBG (CRITICAL_TRC | MEM_TRC,
                              " Memory Allocation Failed for : \
                                HOST_CONTROL->pHostTable \n");
                    return SNMP_FAILURE;
                }

                /* Assign the Status  */
                KER_EQ_MEM (HOST_CONTROL (i4HostControlIndex)->
                            hostControlStatus, i4SetValHostControlStatus);
#ifdef RMON2_WANTED
                HOST_CONTROL (i4HostControlIndex)->u4HostControlCreateTime =
                    OsixGetSysUpTime ();
#endif
            }

            /* Case for RMON_INVALID */
            if (i4SetValHostControlStatus == RMON_INVALID)
            {
                /* Stop the Timer first. */
                RmonStopTimer (HOST_CONTROL_TABLE, i4HostControlIndex);

                /* Free the Memory for the control entry */
                if (HOST_CONTROL (i4HostControlIndex) != NULL)
                {
                    MemReleaseMemBlock
                        (gRmonHostCtrlPoolId,
                         (UINT1 *) HOST_CONTROL (i4HostControlIndex));
                    HOST_CONTROL (i4HostControlIndex) = NULL;
                }

            }
            break;

        case RMON_INVALID:
            if (i4SetValHostControlStatus == RMON_INVALID)
            {

                return SNMP_SUCCESS;
            }
            else if (i4SetValHostControlStatus == RMON_CREATE_REQUEST)
            {
                /* Assign RMON_UNDER_CREATION status - Start the Timer */
                HOST_CONTROL (i4HostControlIndex)->hostControlStatus =
                    RMON_UNDER_CREATION;

                RmonStartTimer (HOST_CONTROL_TABLE, i4HostControlIndex,
                                HOST_TABLE_DURATION);

            }

            break;
        case RMON_MAX_STATE:
        default:
            RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                      "SNMP Failure: Unknown State \n");
            return SNMP_FAILURE;

    }                            /* end of switch */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = HostControlStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (HostControlStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RmonLock;
    SnmpNotifyInfo.pUnLockPointer = RmonUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4HostControlIndex,
                      i4SetValHostControlStatus));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2HostControlDataSource
 Input       :  The Indices
                HostControlIndex

                The Object
                testValHostControlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostControlDataSource (UINT4 *pu4ErrorCode, INT4
                                i4HostControlIndex,
                                tSNMP_OID_TYPE * pTestValHostControlDataSource)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2HostControlDataSource \n");

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (HOST_CONTROL_STATUS (i4HostControlIndex) == RMON_UNDER_CREATION)
        {
            if (TEST_DATA_SOURCE_OID (pTestValHostControlDataSource))
            {
                RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                          "SNMP Success: Data Source id is valid. \n");

                *pu4ErrorCode = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }

    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either Entry Status is not \
                      UNDER_CREATION or INVALID DataSource Oid \n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostControlOwner
 Input       :  The Indices
                HostControlIndex

                The Object
                testValHostControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostControlOwner (UINT4 *pu4ErrorCode, INT4 i4HostControlIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValHostControlOwner)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2HostControlOwner \n");

    RMON_DUMMY (pTestValHostControlOwner);

    if (HOST_CONTROL (i4HostControlIndex) != NULL)
    {
        if (HOST_CONTROL_STATUS (i4HostControlIndex) == RMON_UNDER_CREATION)
        {
            *pu4ErrorCode = SNMP_ERR_NO_ERROR;

            return SNMP_SUCCESS;
        }

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    }

    *pu4ErrorCode = SNMP_ERR_NO_CREATION;

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Entry Status is not UNDER_CREATION \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2HostControlStatus
 Input       :  The Indices
                HostControlIndex

                The Object
                testValHostControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2HostControlStatus (UINT4 *pu4ErrorCode, INT4 i4HostControlIndex,
                            INT4 i4TestValHostControlStatus)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhTestv2HostControlStatus \n");

    /* Configuration of this table is not supported for now.
     * Remove this error return, once support is added
     */
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4HostControlIndex);
    UNUSED_PARAM (i4TestValHostControlStatus);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : HostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceHostTable
 Input       :  The Indices
                HostIndex
                HostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceHostTable (INT4 i4HostIndex,
                                   tSNMP_OCTET_STRING_TYPE * pHostAddress)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY, " Entering into the function: \
                      nmhValidateIndexInstanceHostTable \n");

    RMON_DUMMY (*pHostAddress);

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HostIndex > 0) && (i4HostIndex < RMON_MAX_INTERFACE_LIMIT) &&
        (HOST_CONTROL (i4HostIndex) != NULL)
        && (HOST_CONTROL_STATUS (i4HostIndex) == RMON_VALID))
    {

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
                  "SNMP  Failure: Index is out of  Range  \n");
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexHostTable
 Input       :  The Indices
                HostIndex
                HostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexHostTable (INT4 *pi4HostIndex,
                           tSNMP_OCTET_STRING_TYPE * pHostAddress)
{
    tSNMP_OCTET_STRING_TYPE *pmacZeroAdd = NULL;

    pmacZeroAdd =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (ADDRESS_LENGTH);
    if (pmacZeroAdd == NULL)
    {
        return SNMP_FAILURE;
    }
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetFirstIndexHostTable \n");

    ZERO_MAC_ADDRESS (pmacZeroAdd->pu1_OctetList);

    if (nmhGetNextIndexHostTable (RMON_ZERO, pi4HostIndex,
                                  pmacZeroAdd, pHostAddress) == SNMP_SUCCESS)
    {
        free_octetstring (pmacZeroAdd);
        return SNMP_SUCCESS;
    }

    free_octetstring (pmacZeroAdd);
    return SNMP_FAILURE;
}

/* SearchLHostTable function starts here */
/****************************************************************************
 Function    :  SearchLHostTable         
 Input       :  
                i4Index
                FrameAddress
 Output      :  Least recently used HostAddress 
 Returns     :  Pointer to tHostTableEntry 
****************************************************************************/
static tHostTableEntry *
SearchLHostTable (INT4 i4Index, tMacAddr FrameAddress)
{
    tHostTableEntry    *pHost = NULL;
    UINT2               u2HostNo;
    UINT2               u2HostTableSize;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: SearchLHostTable \n");

    pHost = HOST_CONTROL (i4Index)->pHostTable;
    u2HostTableSize = HOST_CONTROL (i4Index)->u2HostControlTableSize;

    for (u2HostNo = 0; u2HostNo < u2HostTableSize; u2HostNo++)
    {
        if (COMPARE_MAC_ADDRESS (pHost->u1HostAddress, FrameAddress) ==
            RMON_EQUAL)
        {
            return pHost;
        }
        pHost++;
    }

    return NULL;
}

/* SearchHostTimeTable Funtion Starts here */
/****************************************************************************
 Function    :  SearchHostTimeTable      
 Input       :  
                i4Index
                i4TmrOrder 
 Output      :  Least recently used HostEntry   
 Returns     :  Pointer to tHostTableEntry 
****************************************************************************/
static tHostTableEntry *
SearchHostTimeTable (INT4 i4Index, INT4 i4TmOrder)
{
    tHostTableEntry    *pHost = NULL;
    UINT2               u2HostNo;
    UINT2               u2HostTableSize;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: SearchHostTimeTable \n");

    pHost = HOST_CONTROL (i4Index)->pHostTable;
    u2HostTableSize = HOST_CONTROL (i4Index)->u2HostControlTableSize;

    for (u2HostNo = 0; u2HostNo < u2HostTableSize; u2HostNo++)
    {
        if (pHost->i4HostCreationOrder == i4TmOrder)
        {

            return pHost;
        }
        pHost++;
    }
    return NULL;
}

/****************************************************************************
 Function    :  nmhGetNextIndexHostTable
 Input       :  The Indices
                HostIndex
                nextHostIndex
                HostAddress
                nextHostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostTable (INT4 i4HostIndex, INT4 *pi4NextHostIndex,
                          tSNMP_OCTET_STRING_TYPE * pHostAddress,
                          tSNMP_OCTET_STRING_TYPE * pNextHostAddress)
{
    INT4                i4aHostIndex = i4HostIndex;
    tMacAddr            bmacAdd;
    INT4                i4XhostIndex;
    tMacAddr            ymacAdd;
    tHostTableEntry    *pHost = NULL;
    tHostTableEntry    *ptempHost = NULL;
    INT4                i4Index, i4Host;
    INT2                i2TableSize;
    UINT1               u1BreakFlag = FALSE;
    UINT1               u1NextFlag = FALSE;
    tHostControl       *pHostControl = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetNextIndexHostTable \n");

    COPY_MAC_ADDRESS (bmacAdd, pHostAddress->pu1_OctetList);
    *pi4NextHostIndex = RMON_MAX_INTERFACE_LIMIT;

    if (i4aHostIndex <= 0)
    {
        u1NextFlag = TRUE;
        i4aHostIndex = 1;
    }

    for (i4Index = i4aHostIndex; i4Index < RMON_MAX_INTERFACE_LIMIT; i4Index++)
    {
        if (u1BreakFlag)
        {
            break;
        }

        /* check whether the Host control entry status is VALID */
        if ((HOST_CONTROL (i4Index) != NULL)
            && (HOST_CONTROL_STATUS (i4Index) == RMON_VALID))
        {
            /* Assign the HostControlEntry to pHostControl */
            pHostControl = HOST_CONTROL (i4Index);
            pHost = pHostControl->pHostTable;
            if (pHost == NULL)
            {
                continue;
            }

            /* Get the size of the Host table to retrieve so many entries from
             * Host table */
            i2TableSize = (INT2) pHostControl->u2HostControlTableSize;

            for (i4Host = 0; i4Host < i2TableSize; i4Host++)
            {
                ptempHost = (pHost + i4Host);
                i4XhostIndex = i4Index;
                if (u1NextFlag)
                {
                    *pi4NextHostIndex = i4XhostIndex;
                    COPY_MAC_ADDRESS (pNextHostAddress->pu1_OctetList,
                                      &(ptempHost)->u1HostAddress);
                    pNextHostAddress->i4_Length = ADDRESS_LENGTH;
                    u1BreakFlag = TRUE;
                    break;
                }

                COPY_MAC_ADDRESS (ymacAdd, &ptempHost->u1HostAddress);

                if (CMP_HOST (i4aHostIndex, bmacAdd, i4XhostIndex, ymacAdd) ==
                    0)
                {
                    if (i4Host == (i2TableSize - 1))
                    {
                        u1NextFlag = TRUE;
                    }
                    else
                    {
                        ptempHost = (ptempHost + 1);
                        *pi4NextHostIndex = i4XhostIndex;
                        COPY_MAC_ADDRESS (pNextHostAddress->pu1_OctetList,
                                          &ptempHost->u1HostAddress);
                        pNextHostAddress->i4_Length = ADDRESS_LENGTH;
                        u1BreakFlag = TRUE;
                        break;
                    }
                }

            }                    /* End of for */
        }
    }                            /* End of for */

    if (u1BreakFlag)
    {

        return SNMP_SUCCESS;
    }

    RMON_DBG (CRITICAL_TRC | RMON_MGMT_TRC,
              "SNMP Failure: INVALID Interface \n");
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetHostCreationOrder
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostCreationOrder
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostCreationOrder (INT4 i4HostIndex,
                         tSNMP_OCTET_STRING_TYPE * pHostAddress,
                         INT4 *pi4RetValHostCreationOrder)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostCreationOrder \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);

    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pi4RetValHostCreationOrder = pHost->i4HostCreationOrder;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry \
                 status is not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetHostInPkts
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostInPkts (INT4 i4HostIndex, tSNMP_OCTET_STRING_TYPE * pHostAddress,
                  UINT4 *pu4RetValHostInPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostInPkts \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);
    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostInPkts = pHost->u4HostInPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status \
               is not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetHostOutPkts
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostOutPkts (INT4 i4HostIndex, tSNMP_OCTET_STRING_TYPE * pHostAddress,
                   UINT4 *pu4RetValHostOutPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostOutPkts \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);
    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostOutPkts = pHost->u4HostOutPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status \
                  is not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetHostInOctets
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostInOctets (INT4 i4HostIndex, tSNMP_OCTET_STRING_TYPE * pHostAddress,
                    UINT4 *pu4RetValHostInOctets)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostInOctets \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);
    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostInOctets = pHost->u4HostInOctets;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status \
                   is not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostOutOctets
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostOutOctets (INT4 i4HostIndex, tSNMP_OCTET_STRING_TYPE * pHostAddress,
                     UINT4 *pu4RetValHostOutOctets)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostOutOctets \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);
    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostOutOctets = pHost->u4HostOutOctets;

        return SNMP_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status is \
                    not VALID or Host does't Exist in HostTable\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostOutErrors
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostOutErrors (INT4 i4HostIndex, tSNMP_OCTET_STRING_TYPE * pHostAddress,
                     UINT4 *pu4RetValHostOutErrors)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostOutErrors \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);
    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostOutErrors = pHost->u4HostOutErrors;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status is \
                   not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetHostOutBroadcastPkts
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostOutBroadcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostOutBroadcastPkts (INT4 i4HostIndex,
                            tSNMP_OCTET_STRING_TYPE * pHostAddress,
                            UINT4 *pu4RetValHostOutBroadcastPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostOutBroadcastPkts \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);

    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostOutBroadcastPkts = pHost->u4HostOutBroadcastPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status is \
                    not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostOutMulticastPkts
 Input       :  The Indices
                HostIndex
                HostAddress

                The Object
                retValHostOutMulticastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostOutMulticastPkts (INT4 i4HostIndex,
                            tSNMP_OCTET_STRING_TYPE * pHostAddress,
                            UINT4 *pu4RetValHostOutMulticastPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostOutMulticastPkts \n");

    pHost = SearchLHostTable (i4HostIndex, pHostAddress->pu1_OctetList);

    if (pHost && VALID_HOST_CONTROL (i4HostIndex))
    {
        *pu4RetValHostOutMulticastPkts = pHost->u4HostOutMulticastPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry status is \
                    not VALID or Host does't Exist in HostTable \n");
    return SNMP_FAILURE;

}

/*
 * Low Level SET Routine for All Objects
 */

/*
 * Low Level TEST Routines for
 */

/* LOW LEVEL Routines for Table : HostTimeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceHostTimeTable
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceHostTimeTable (INT4 i4HostTimeIndex,
                                       INT4 i4HostTimeCreationOrder)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY, "Entering into the function: \
                   nmhValidateIndexInstanceHostTimeTable \n");

    RMON_DUMMY (i4HostTimeCreationOrder);

    /* Check whether RMON is started. */
    if (!(RMON_IS_STARTED ()))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: RMON Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4HostTimeIndex > 0) && (i4HostTimeIndex < RMON_MAX_INTERFACE_LIMIT) &&
        (HOST_CONTROL (i4HostTimeIndex) != NULL)
        && (HOST_CONTROL_STATUS (i4HostTimeIndex) == RMON_VALID))
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Success: HostTime Indices are valid.\n");

        return SNMP_SUCCESS;
    }
    else
    {

        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP Failure: Unknown Interface \n");
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexHostTimeTable
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexHostTimeTable (INT4 *pi4HostTimeIndex,
                               INT4 *pi4HostTimeCreationOrder)
{
    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetFirstIndexHostTimeTable \n");

    return nmhGetNextIndexHostTimeTable (RMON_ZERO, pi4HostTimeIndex, RMON_ZERO,
                                         pi4HostTimeCreationOrder);

}

/****************************************************************************
 Function    :  nmhGetNextIndexHostTimeTable
 Input       :  The Indices
                HostTimeIndex
                nextHostTimeIndex
                HostTimeCreationOrder
                nextHostTimeCreationOrder
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexHostTimeTable (INT4 i4HostTimeIndex, INT4
                              *pi4NextHostTimeIndex,
                              INT4 i4HostTimeCreationOrder,
                              INT4 *pi4NextHostTimeCreationOrder)
{
    INT4                i4Index = i4HostTimeIndex;
    INT4                i4TmOrder = (i4HostTimeCreationOrder + 1);
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetNextIndexHostTimeTable \n");

    if (i4TmOrder == MAX_HOST_PER_INTERFACE)
    {
        i4TmOrder = 1;
        i4Index++;
    }

    if (i4Index < RMON_ZERO)
    {
        RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
                  "SNMP_Failure: Index is less than Zero\n");
        return SNMP_FAILURE;
    }

    while (i4Index < RMON_MAX_INTERFACE_LIMIT)
    {
        if ((HOST_CONTROL (i4Index) != NULL)
            && (HOST_CONTROL_STATUS (i4Index) == RMON_VALID))
        {
            pHost = SearchHostTimeTable (i4Index, i4TmOrder);
            if ((pHost != NULL)
                && (HOST_CONTROL_STATUS (i4Index) != RMON_INVALID))
            {
                *pi4NextHostTimeIndex = i4Index;
                *pi4NextHostTimeCreationOrder = i4TmOrder;

                return SNMP_SUCCESS;
            }

            if (i4TmOrder > HOST_CONTROL (i4Index)->u2HostControlTableSize)
            {
                i4TmOrder = 1;
                i4Index++;
            }
            else
            {
                i4TmOrder++;
            }
        }
        else
        {
            i4TmOrder = 1;
            i4Index++;
        }
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP_ Failure: Either Index is greater than \
               RMON_MAX_INTERFACE_LIMIT or ControlTable Entry is not in VALID State \n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetHostTimeAddress
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeAddress (INT4 i4HostTimeIndex, INT4 i4HostTimeCreationOrder,
                       tSNMP_OCTET_STRING_TYPE * pRetValHostTimeAddress)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeAddress \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);
    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        COPY_MAC_ADDRESS (pRetValHostTimeAddress->pu1_OctetList,
                          &pHost->u1HostAddress);
        pRetValHostTimeAddress->i4_Length = ADDRESS_LENGTH;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP_ Failure: Either ControlTable Entry not in \
                VALID State or Host does't exist in TimeTable \n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetHostTimeInPkts
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeInPkts (INT4 i4HostTimeIndex, INT4 i4HostTimeCreationOrder,
                      UINT4 *pu4RetValHostTimeInPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeInPkts \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);
    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeInPkts = pHost->u4HostInPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP_Failure: Either ControlTable Entry not in \
                VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTimeOutPkts
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeOutPkts (INT4 i4HostTimeIndex, INT4 i4HostTimeCreationOrder,
                       UINT4 *pu4RetValHostTimeOutPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeOutPkts \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);

    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeOutPkts = pHost->u4HostOutPkts;

        return SNMP_SUCCESS;
    }

    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP_Failure: Either ControlTable Entry not in \
                VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTimeInOctets
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeInOctets (INT4 i4HostTimeIndex, INT4 i4HostTimeCreationOrder,
                        UINT4 *pu4RetValHostTimeInOctets)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeInOctets \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);

    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeInOctets = pHost->u4HostInOctets;

        return SNMP_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry not in \
               VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTimeOutOctets
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeOutOctets (INT4 i4HostTimeIndex, INT4 i4HostTimeCreationOrder,
                         UINT4 *pu4RetValHostTimeOutOctets)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeOutOctets \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);
    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeOutOctets = pHost->u4HostOutOctets;

        return SNMP_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry not in \
               VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTimeOutErrors
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeOutErrors (INT4 i4HostTimeIndex, INT4 i4HostTimeCreationOrder,
                         UINT4 *pu4RetValHostTimeOutErrors)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeOutErrors \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);

    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeOutErrors = pHost->u4HostOutErrors;

        return SNMP_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP_Failure: Either ControlTable Entry not in \
               VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTimeOutBroadcastPkts
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeOutBroadcastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeOutBroadcastPkts (INT4 i4HostTimeIndex, INT4
                                i4HostTimeCreationOrder,
                                UINT4 *pu4RetValHostTimeOutBroadcastPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeOutBroadcastPkts \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);

    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeOutBroadcastPkts = pHost->u4HostOutBroadcastPkts;

        return SNMP_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry not in \
               VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetHostTimeOutMulticastPkts
 Input       :  The Indices
                HostTimeIndex
                HostTimeCreationOrder

                The Object
                retValHostTimeOutMulticastPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetHostTimeOutMulticastPkts (INT4 i4HostTimeIndex, INT4
                                i4HostTimeCreationOrder,
                                UINT4 *pu4RetValHostTimeOutMulticastPkts)
{
    tHostTableEntry    *pHost = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: nmhGetHostTimeOutMulticastPkts \n");

    pHost = SearchHostTimeTable (i4HostTimeIndex, i4HostTimeCreationOrder);

    if ((pHost) && (VALID_HOST_CONTROL (i4HostTimeIndex)))
    {
        *pu4RetValHostTimeOutMulticastPkts = pHost->u4HostOutMulticastPkts;

        return SNMP_SUCCESS;
    }
    RMON_DBG (MINOR_TRC | RMON_MGMT_TRC,
              "SNMP Failure: Either ControlTable Entry not in \
               VALID State or Host does't exist in HostTimeTable \n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2HostControlTable
 Input       :  The Indices
                HostControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2HostControlTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
