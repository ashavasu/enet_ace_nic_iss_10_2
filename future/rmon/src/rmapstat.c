/********************************************************************
*                                                                  *
* Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
*                                                                  *
* $Id: rmapstat.c,v 1.19 2013/10/28 09:53:41 siva Exp $            *
*                                                                  *
* Description: Contains Functions to update the Statistics in Ether*
*              Stat Group                                          *
*                                                                  *
*******************************************************************/
/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  rmapstat.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  RMON                                         |
 * |                                                                         |
 * |  MODULE NAME            :  APERIODIC                                    |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides updation of Ether Stats  |
 * |                             Table                                       |
 * |                                                                         |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |     D. Vignesh      |         Create Original          |
 * |                |    19-MAR-1997      |                                  |
 * |----------------|---------------------|----------------------------------|
 * |  2.0           |    Bhupendra        | File is modified not to call     |
 * |                |                     | RmonisBroadCast and              |
 * |                |    14-SEP-2001      | RmonisMultiCast  funtions        |
 *  -------------------------------------------------------------------------
 * |  3.0           |    Leela L          |File is modified for dynamic      |
 * |                |                     |rmon table creations using malloc.|
 * |                |    31-Mar-2002      |                                  |
 *  -------------------------------------------------------------------------
 * |  4.0           |    Leela L          |File is modified for new          |
 * |                |                     |MIDGEN Tool                       |
 * |                |    07-JUL-2002      |                                  |
 *  -------------------------------------------------------------------------
 */

#include "rmallinc.h"

/**************************************************************************
 * Function Name      : RmonUpdateStatisticsTable                         *
 *                                                                        *
 * Description        : Function updates the Statistics based on incoming *
 *                      packets. Called from CFA and Vlan routine.        *
 *                                                                        *
 * Input (s)          : pu1EthFrame       - Pointer to the Frame          *
 *                      pFrameParticulars - Frame Particulars             *
 *                                           (Index,Length)               *
 * Output (s)         : pFrameParticulars - Frame Particulars             *
 *                                                                        *
 * Returns            : RMON_SUCCESS/RMON_FAILURE                         *
 *                                                                        *
 * Global Variable    : gaEtherStatsTable                                 *
 *                                                                        *
 * Side effects       : Updates gaEtherStatsTable                         *
 *                                                                        *
 * Action :                                                               *
 *                                                                        *
 * Called from Aperiodic main task whenever a frame arrives               *
 * This function updates the statistics table according to the arrived    *
 * frames characteristics.                                                *
 *************************************************************************/

INT1
RmonUpdateStatisticsTable (UINT1 *pu1EthFrame,
                           tFrameParticulars * pFrameParticulars)
{
    UINT4               u4DataSource;
    UINT4               u4VlanId;
    UINT2               u2Len;

    BOOLEAN             b1IsErrorInFCS;
    tRmonEtherStatsTimerNode *pEtherStatsNode = NULL;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function:  RmonUpdateStatisticsTable \n");

    if (pFrameParticulars->u4Type == PORT_INDEX_TYPE)
    {
        u4DataSource = (UINT4) pFrameParticulars->u1DataSource;

        RMON_DBG1 (MAJOR_TRC | PKT_PROC_TRC,
                   "Frame is received on Interface : %d \n", u4DataSource);

        pEtherStatsNode =
            HashSearchEtherStatsNode_DSource (gpRmonEtherStatsHashTable,
                                              u4DataSource,
                                              pFrameParticulars->u4Type);
    }
    else if (pFrameParticulars->u4Type == VLAN_INDEX_TYPE)
    {
        u4VlanId = (UINT4) pFrameParticulars->u4VlanId;

        RMON_DBG1 (MAJOR_TRC | PKT_PROC_TRC,
                   "Frame is received on Vlan : %d \n", u4VlanId);

        pEtherStatsNode =
            HashSearchEtherStatsNode_DSource (gpRmonEtherStatsHashTable,
                                              u4VlanId,
                                              pFrameParticulars->u4Type);
    }
    if (pEtherStatsNode == NULL)
    {
        RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                  "Frame can not be processed since the control entry "
                  "configuration is not done \n");
        return RMON_FAILURE;
    }

    if (pEtherStatsNode->etherStatsStatus != RMON_VALID)
    {
        RMON_DBG (MINOR_TRC | PKT_PROC_TRC,
                  "EtherStats Status is not VALID - Hence dropping frame \n");
        return RMON_FAILURE;
    }

    u2Len = pFrameParticulars->u2Length;

    RMON_DBG1 (MAJOR_TRC | PKT_PROC_TRC,
               "Received Frame Length is : %d \n", u2Len);

    b1IsErrorInFCS = FALSE;

    pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOctets += u2Len;
    ++pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts;

    b1IsErrorInFCS = (BOOLEAN) RmonisErrorInFCS (pu1EthFrame, u2Len);

    if (u2Len == 64)
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts64Octets++;

        RMON_DBG1 (MINOR_TRC | PKT_PROC_TRC, "64Octets = %d \n",
                   pEtherStatsNode->RmonEtherStatsPkts.
                   u4EtherStatsPkts64Octets);

    }
    else if ((u2Len > 64) && (u2Len < 128))
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts65to127Octets++;

        RMON_DBG1 (MINOR_TRC | PKT_PROC_TRC, "65to127Octets = %d \n",
                   pEtherStatsNode->RmonEtherStatsPkts.
                   u4EtherStatsPkts65to127Octets);

    }
    else if ((u2Len > 127) && (u2Len < 256))
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts128to255Octets++;
    }
    else if ((u2Len > 255) && (u2Len < 512))
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts256to511Octets++;
    }
    else if ((u2Len > 511) && (u2Len < 1024))
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts512to1023Octets++;
    }
    else if ((u2Len > 1023) && (u2Len < 1519))
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts1024to1518Octets++;
    }
    else if ((u2Len > 1518) && (u2Len < 1523))
    {
        pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsPkts1519to1522Octets++;
    }
    else if (u2Len < 64)
    {

        if (b1IsErrorInFCS == TRUE)
        {
            RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                      "UNDER_SIZE Packet with FCS_ERROR received !!!\n");
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsFragments++;
        }
        else
        {
            RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                      "UNDER_SIZE Packet received !!\n");
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsUndersizePkts++;
        }
    }
    else if (u2Len > 1518)
    {

        if (b1IsErrorInFCS == TRUE)
        {
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsJabbers++;
        }
        else
        {
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsOversizePkts++;
        }
    }

    if (pFrameParticulars->b1WhetherError == FALSE)
    {
        if (pFrameParticulars->b1WhetherBroadcast == TRUE)
        {

            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsBroadcastPkts++;
            RMON_DBG1 (MAJOR_TRC | PKT_PROC_TRC, "BroadcastPkts = %d \n",
                       pEtherStatsNode->RmonEtherStatsPkts.
                       u4EtherStatsBroadcastPkts);

        }
        else
        {
            if (pFrameParticulars->b1WhetherMulticast == TRUE)
            {
                RMON_DBG (MAJOR_TRC | PKT_PROC_TRC, " MULTICAST PACKET \n\n");
                pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsMulticastPkts++;
            }
        }
    }

    if ((u2Len >= 64) && (u2Len <= 1518))
    {
        if (b1IsErrorInFCS == TRUE)
        {
            pEtherStatsNode->RmonEtherStatsPkts.u4EtherStatsCRCAlignErrors++;
            RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                      " CRCALIGNMENT Error Packet received \n\n");

        }
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving  function : RmonUpdateStatisticsTable \n");
    return RMON_SUCCESS;

}
