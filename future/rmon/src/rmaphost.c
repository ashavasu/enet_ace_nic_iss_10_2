/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        *
 *                                                                  *
 * $Id: rmaphost.c,v 1.18 2010/09/21 08:25:26 prabuc Exp $            *
 *                                                                  *
 * Description: Contains Functions to Update the Host Group Tables  *
 *              , to search the Host Table on HostMAC address, to   *
 *              create new entry in the HostTable and also to find  *
 *              out the Least recently used Host entry              *
 *                                                                  *
 *******************************************************************/

/*
*
*  -------------------------------------------------------------------------
* |  FILE  NAME             :  rmaphost.c                                   |
* |                                                                         |
* |  PRINCIPAL AUTHOR       :  Aricent Inc.                              |
* |                                                                         |
* |  SUBSYSTEM NAME         :  RMON                                         |
* |                                                                         |
* |  MODULE NAME            :  APERIODIC                                    |
* |                                                                         |
* |  LANGUAGE               :  C                                            |
* |                                                                         |
* |  TARGET ENVIRONMENT     :  ANY                                          |
* |                                                                         |
* |  DATE OF FIRST RELEASE  :                                               |
* |                                                                         |
* |  DESCRIPTION            :   This file provides updation of Ether Host   |
* |                             Tables                                      |
* |                                                                         |
*  -------------------------------------------------------------------------
*
*     CHANGE RECORD :
*     ---------------
*
*
*  -------------------------------------------------------------------------
* |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
* |                |        DATE         |             CHANGE               |
* |----------------|---------------------|----------------------------------|
* |  1.0           |     D. Vignesh      |         Create Original          |
* |                |    19-MAR-1997      |                                  |
* | -------------------------------------------------------------------------
* |  2.0           |     Leela.L         |   This file is modified for      |
* |                |    31-MAR-2002      |   dynamic creation of RMON Tables|
* |                |                     |                                  |
* --------------------------------------------------------------------------|
* |  3.0           |     Leela.L         |   Modified for New MIDGEN Tool   |
* |                |    07-JUL-2002      |                                  |
* |                |                     |                                  |
* --------------------------------------------------------------------------|
*/

#include "rmallinc.h"

#define HOST_CONTROL_TBL(index) gaHostControlTable[index].pHostControlEntry

/************************************************************************
 * Function           : GiveLRUHost                                     *
 *                                                                      *
 * Input (s)          : i4Index - The index on which                    *
 *                      the Least Receently used Host is to be found.   *
 *                                                                      *
 * Output (s)         : None.                                           *
 *                                                                      *
 * Returns            : LRU Host                                        *
 *                                                                      *
 * Global Variables                                                     *
 *             Used   : gaHostControlTable                              *
 *                                                                      *
 * Side effects       : None                                            *
 *                                                                      *
 * Action :                                                             *
 *                                                                      *
 * Called when the frame is from a new host and all host entries        *
 * are already used. The procedure used th time stamp in the            *
 * record to find out the LRU host and returns it address               *
 ************************************************************************/

static tHostTableEntry *
GiveLRUHost (INT4 i4Index)
{
    tHostTableEntry    *pHost = NULL, *pLRUHost = NULL;
    UINT2               u2HostNo;
    UINT4               u4Timestamp;
    INT4                i4OldCreationOrder;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: GiveLRUHost \n");

    pLRUHost = pHost = HOST_CONTROL_TBL (i4Index)->pHostTable;

    u4Timestamp = pHost->u4LastAccess;
    for (u2HostNo = 1; u2HostNo < MAX_HOST_PER_INTERFACE; u2HostNo++)
    {
        pHost++;
        if (u4Timestamp > pHost->u4LastAccess)
        {
            pLRUHost = pHost;
            u4Timestamp = pHost->u4LastAccess;
        }
    }
    i4OldCreationOrder = pLRUHost->i4HostCreationOrder;

    pHost = HOST_CONTROL_TBL (i4Index)->pHostTable;

    /* 
     * Now Decrementing the Creation order of all hosts which are created
     * after the LRU Host
     */

    for (u2HostNo = 0; u2HostNo < MAX_HOST_PER_INTERFACE; u2HostNo++)
    {
        if (pHost->i4HostCreationOrder > i4OldCreationOrder)
        {
            pHost->i4HostCreationOrder--;
        }
        pHost++;
    }

    pLRUHost->i4HostCreationOrder = MAX_HOST_PER_INTERFACE;

    RMON_T_GET_TICK ((tOsixSysTime *) & (HOST_CONTROL_TBL (i4Index)->
                                         hostControlLastDeleteTime));

    RMON_T_GET_TICK ((tOsixSysTime *) & (pLRUHost->u4CreationTime));

    RMON_DBG (MINOR_TRC | FUNC_EXIT, " Leaving function : GiveLRUHost \n");
    return pLRUHost;
}

/*********************************************************************
 * Function           : CreateNewHostEntry                           *
 *                                                                   *
 * Input (s)          : u1Index - The index on which                 *
 *                      the New Host is to be found                  *
 *                                                                   *
 * Output (s)         : None.                                        *
 *                                                                   *
 * Returns            : The New entries address                      *
 *                                                                   *
 * Global Variables                                                  *
 *             Used   : gaHostControlTable                           *
 *                                                                   *
 * Side effects       : None                                         *
 *                                                                   *
 * Action :    Called when the frame is from a new host              *
 ********************************************************************/

tHostTableEntry    *
CreateNewHostEntry (INT4 i4Index)
{
    UINT2               u2hostno =
        HOST_CONTROL_TBL (i4Index)->u2HostControlTableSize;

    tHostTableEntry    *pHost = NULL;

    pHost = HOST_CONTROL_TBL (i4Index)->pHostTable;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: CreateNewHostEntry \n");

    if (u2hostno < MAX_HOST_PER_INTERFACE)
    {
        pHost += u2hostno;

        RMON_T_GET_TICK ((tOsixSysTime *) & (pHost->u4CreationTime));
        pHost->i4HostCreationOrder = (u2hostno + 1);

        HOST_CONTROL_TBL (i4Index)->u2HostControlTableSize++;

        return pHost;
    }
    else
    {

        RMON_DBG (MINOR_TRC | FUNC_EXIT,
                  "Leaving function: CreateNewHostEntry \n");
        return GiveLRUHost (i4Index);
    }
}

/*********************************************************************
 * Function           : SearchHostTable
 *
 * Input (s)           : i4Index - The index on which 
 *                      the Host is to be found
 *                      pFrameAddress- Mac Address of
 *                      the Host which is to be found       
 *
 * Output (s)          : None.    
 *
 * Returns            : The host records address
 *
 * Global Variables
 *             Used   : gaHostControlTable      
 *
 * Side effects       : None
 * 
 * Action :
 *
 * Called when the frame is from a new host to find 
 * Host Corresponding to a MAC Addresss
*********************************************************************/

tHostTableEntry    *
SearchHostTable (INT4 i4Index, tMacAddr * pFrameAddress)
{
    tHostTableEntry    *pHost = NULL;
    UINT2               u2HostNo;
    UINT2               u2HostTableSize;

    pHost = HOST_CONTROL_TBL (i4Index)->pHostTable;
    u2HostTableSize = HOST_CONTROL_TBL (i4Index)->u2HostControlTableSize;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              "Entering into the function: SearchHostTable \n");

    for (u2HostNo = 0; u2HostNo < u2HostTableSize; u2HostNo++)
    {
        if (MEMCMP (&pHost->u1HostAddress, pFrameAddress, ADDRESS_LENGTH)
            == RMON_ZERO)
        {

            return pHost;
        }
        pHost++;
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT, "Leaving function: SearchHostTable \n");
    return NULL;
}

/*************************************************************************
 * Function           : UpdateHostModuleTables
 *
 * Input (s)           : pFrameParticulars Contains info about frame
 *
 * Output (s)          : None.    
 *
 * Returns            : The host records address
 *
 * Global Variables
 *             Used   : gaHostControlTable      
 *
 * Side effects       : None
 * 
 * Action :
 *
 * Called when the frame is got and to update  relevent
 * Host Statistics                          
+-------------------------------------------------------------------*/

void
RmonUpdateHostModuleTables (tFrameParticulars * pFrameParticulars)
{
    tHostTableEntry    *pHostOfSrc = NULL, *pHostOfDest = NULL;
    INT4                i4Index;

    RMON_DBG (MINOR_TRC | FUNC_ENTRY,
              " Entering into the function: RmonUpdateHostModuleTables \n");

    i4Index = pFrameParticulars->u1DataSource;

    if (i4Index >= RMON_MAX_INTERFACE_LIMIT)
    {
        RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                  "Index is invalid, beyond the Host Table array size\n");
        return;
    }

    if (HOST_CONTROL_TBL (i4Index) == NULL)
    {
        RMON_DBG (MAJOR_TRC | PKT_PROC_TRC,
                  "Frame can not be processed since the host control "
                  "entry configuration is not done yet \n");

        return;
    }

    if (HOST_CONTROL_TBL (i4Index)->hostControlStatus == RMON_VALID)
    {
        pHostOfSrc = SearchHostTable (i4Index, &pFrameParticulars->SrcAddress);

        if (pHostOfSrc == NULL)
        {
            pHostOfSrc = CreateNewHostEntry (i4Index);
#ifdef RMON2_WANTED
            if (pHostOfSrc == NULL)
            {
                HOST_CONTROL_TBL (i4Index)->u4HostControlDroppedFrames++;
            }
#endif
            COPY_MAC_ADDRESS (&pHostOfSrc->u1HostAddress,
                              &pFrameParticulars->SrcAddress);
        }
        RMON_CHK_NULL_PTR (pHostOfSrc);
        if (pFrameParticulars->b1WhetherError == TRUE)
        {
            pHostOfSrc->u4HostOutErrors++;
        }
        else
        {
            if ((pFrameParticulars->b1WhetherBroadcast != TRUE) &&
                (pFrameParticulars->b1WhetherMulticast != TRUE))
            {
                pHostOfDest = SearchHostTable (i4Index, &pFrameParticulars->
                                               DestAddress);
                if (pHostOfDest == NULL)
                {
                    pHostOfDest = CreateNewHostEntry (i4Index);
#ifdef RMON2_WANTED
                    if (pHostOfDest == NULL)
                    {
                        HOST_CONTROL_TBL (i4Index)->
                            u4HostControlDroppedFrames++;
                    }
#endif
                    COPY_MAC_ADDRESS (&pHostOfDest->u1HostAddress,
                                      &pFrameParticulars->DestAddress);
                }
                RMON_CHK_NULL_PTR (pHostOfDest);
                pHostOfDest->u4HostInOctets += pFrameParticulars->u2Length;
                pHostOfDest->u4HostInPkts++;

                RMON_T_GET_TICK ((tOsixSysTime *) &
                                 (pHostOfDest->u4LastAccess));
            }
            if (pFrameParticulars->b1WhetherBroadcast == TRUE)
            {
                pHostOfSrc->u4HostOutBroadcastPkts++;
            }

            if (pFrameParticulars->b1WhetherMulticast == TRUE)
            {
                pHostOfSrc->u4HostOutMulticastPkts++;
            }
        }
        pHostOfSrc->u4HostOutOctets += pFrameParticulars->u2Length;
        pHostOfSrc->u4HostOutPkts++;
        RMON_T_GET_TICK ((tOsixSysTime *) & (pHostOfSrc->u4LastAccess));
    }

    RMON_DBG (MINOR_TRC | FUNC_EXIT,
              "Leaving function: RmonUpdateHostModuleTables \n");
}
