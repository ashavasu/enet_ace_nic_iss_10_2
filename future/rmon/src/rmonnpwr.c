/********************************************************************
 *                                                                  *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: rmonnpwr.c,v 1.1 2012/07/10 06:03:03 siva Exp $             *
 *                                                                  *
 * Description: It contains NP wrapper calls for RMON module        *
 *                                                                  *
 *******************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : RmonNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tRmonNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __RMON_NP_WR_C
#define __RMON_NP_WR_C

#include "rmallinc.h"

PUBLIC UINT1
RmonNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tRmonNpModInfo     *pRmonNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRmonNpModInfo = &(pFsHwNp->RmonNpModInfo);

    if (NULL == pRmonNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_RMON_HW_GET_ETH_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwGetEthStatsTable *pEntry = NULL;
            pEntry = &pRmonNpModInfo->RmonNpFsRmonHwGetEthStatsTable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRmonHwGetEthStatsTable (pEntry->u4IfIndex,
                                          pEntry->pEthStatsEntry);
            break;
        }
        case FS_RMON_HW_SET_ETHER_STATS_TABLE:
        {
            tRmonNpWrFsRmonHwSetEtherStatsTable *pEntry = NULL;
            pEntry = &pRmonNpModInfo->RmonNpFsRmonHwSetEtherStatsTable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsRmonHwSetEtherStatsTable (pEntry->u4IfIndex,
                                            pEntry->u1EtherStatsEnable);
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _RMON_NP_WR_C */

