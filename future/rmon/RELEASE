Copyright (C) 2006 Aricent Inc . All Rights Reserved
------------------------------------------------------
PRODUCT NAME            : Aricent RMON
--------------------------------------
     __________________________________________________________________
    |    RELEASE NUMBER  : RMON_3-1-0-2                                |
    |    RELEASE DATE    : Febuary 08, 2008                            |
    '__________________________________________________________________'

     Known problems in this release:
     -------------------------------
     o  In RMON, the history and ether statistics table should be
        independent as described in the rmon mib
        
     ______ CHANGELOG: _________________________________________________
     **  Fix for crash in MSR during red.conf file creation.
     
      + inc/rmportdf.h
                                                             [RFC: 23682]
     ____________________________________________________________________
     **  Modified printf and scanf format specifier for 64 bit migration.

      + src/rmoncli.c
                                                             [RFC: 23837]
     ____________________________________________________________________
     **  Removed warnings for work group package modules in 64 Bit
     **  processor.

      + src/rmoncli.c
      + src/rmapevnt.c
      + inc/rmonprot.h
                                                             [RFC: 24086]
     ____________________________________________________________________
     **  Klocwork fixes for RMON and LA.

     + src/rmonmain.c[c1]                                                                                                                                                                                                 [RFC: 24571]
     __________________________________________________________________
    (______________________________END_________________________________)

                                                                                                                                                                                  
     __________________________________________________________________
    |    RELEASE NUMBER  : RMON_3-0-6                                  |
    |    RELEASE DATE    : November 30, 2007                           |
    |__________________________________________________________________|

    Known problems in this release:
    -------------------------------
    o  In RMON, the history and ether statistics table should be
       independent as described in the rmon mib

    ______ CHANGELOG: ________________________________________________
     **  Changes made in Infrastructure modules.
                                                            [RFC: 24078]
                                                            [RFC: 24169]
                                                            [RFC: 24084]
                                                            [RFC: 24182]
                                                            [RFC: 24341]
                                                            [RFC: 24468]
                                                            [RFC: 24116]
                                                            [RFC: 24255]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : RMON_3-0-5                                  |
    |    RELEASE DATE    : October 31, 2007                            |
    '__________________________________________________________________'

    Known problems in this release:
    -------------------------------
    o  In RMON, the history and ether statistics table should be
       independent as described in the rmon mib

    ______ CHANGELOG: ________________________________________________
     **  Corrected the logic used to calculate rmon
     **  EtherHistoryUtilisation.

      + src/rmprhist.c
                                                            [RFC: 23845]
     __________________________________________________________________
    (______________________________END_________________________________)


     __________________________________________________________________
    |    RELEASE NUMBER  : RMON_3-0-4                                  |
    |    RELEASE DATE    : July 31, 2007                               |
    '__________________________________________________________________'

    Known problems in this release:
    -------------------------------
    o  In RMON, the history and ether statistics table should be
       independent as described in the rmon mib

    ______ CHANGELOG: _________________________________________________
     **  Fix for crash, when we create and delete a history with index 
     **  value is greater than 10.

      + inc/rmonglob.h
      + inc/rmontdfs.h
      + src/rmoncli.c
      + src/rmonmain.c
      + src/rmprhist.c
      + src/rplhist.c
                                                            [RFC: 22528]
     __________________________________________________________________
    (______________________________END_________________________________)
    
     __________________________________________________________________
    |    RELEASE NUMBER  : RMON_3-0-3                                  |
    |    RELEASE DATE    : July 20, 2007                               |
    '__________________________________________________________________'

    Known problems in this release:
    -------------------------------
    o  In RMON, the history and ether statistics table should be
       independent as described in the rmon mib

    ______ CHANGELOG: _________________________________________________
     **  Fix for errors and warnings in rmon module reported by klocwork
     **  tool. 

      + src/rmoncli.c
      + inc/rmportdf.h
      + src/rmontmm.c
      + src/rmapevnt.c
      + src/rplhist.c
      + src/rmonport.c
      + src/rmonmain.c
      + src/ralmtrx.c
      + src/rmprtopn.c
      + inc/rmondebg.h
      + src/rpltopn.c
      + src/rmonutl.c
      + src/rmaplog.c
      + src/rplalrm.c
      + src/rmapmtrx.c
      + inc/rmonglob.h
      + src/ralhost.c
      + src/ralevnt.c
                                                            [RFC: 19500]
     __________________________________________________________________
     **  Modified to prevent the loop in etherhistory table and also to
     **  return the correct snmp error codes. 

      + src/rplhist.c
      + src/rmapstat.c
      + src/rplalrm.c
      + src/ralstat.c
      + src/ralevnt.c
                                                            [RFC: 19761]
     __________________________________________________________________
     **  Corrected the usage of u4Flags of OsixReceiveEvent in protocol
     **  modules 

      + src/rmonmain.c
                                                            [RFC: 19844]
     __________________________________________________________________
     **  Added multiple context access support in snmpv3. 

      + src/rmonutl.c
                                                            [RFC: 19972]
     __________________________________________________________________
     **  FSAP Name to Id changes done for ISS, LR, CFA, CLI, npapi, RMON,
     **  RM, SSL, SSH, UTIL and L2 modules. 

      + src/rmonmain.c
      + inc/rmondefn.h
      + src/rmonutl.c
      + inc/rmonglob.h
                                                            [RFC: 20031]
     __________________________________________________________________
     **  Modified for the proper deletion of log entries that are old. 

      + src/rmoncli.c
      + src/rmaplog.c
      + inc/rmontdfs.h
      + src/rallog.c
      + src/ralevnt.c
                                                            [RFC: 20056]
     __________________________________________________________________
     **  Change structure field for alignment and padding issues in pnac,
     **  stp, la, rmon, snooping, l2iwf. 

      + inc/rmontdfs.h
                                                            [RFC: 20494]
     __________________________________________________________________
     **  Stack compilation warnings removed 

      + src/rmapevnt.c
      + src/rmonutl.c
                                                            [RFC: 22007]
     __________________________________________________________________
    (______________________________END_________________________________)
    
    __________________________________________________________________
    |    RELEASE NUMBER  : RMON_3-0-2-0                                |
    |    RELEASE DATE    : February 23 2007                            |
    '__________________________________________________________________'

     ______ CHANGELOG: _________________________________________________
     
     __________________________________________________________________
     
     **  Modified rmon configurations and display. 

                                                            [RFC: 16867]
     __________________________________________________________________
     
     **  Modified to make Sample Index and Time stamp of the buckets in
         sync.

     **  Modified the logic to store and retrieve samples from buckets
     
     **  Used fsap utilty to display system ticks in human readable form
     
                                                            [RFC: 16920]
     __________________________________________________________________

     **  Modified inconsistent usage of \r\n in CliPrintf function across
     **  modules. 
      
      + rmon/src/rmoncli.c
                                                            [RFC: 16369]
     __________________________________________________________________
     **  Fix for the diab compilation errors in VxWorks 
      
      + rmon/src/rmoncli.c
                                                            [RFC: 17636]
     __________________________________________________________________
     ** Modified to remove compilation errors found in diab and autobuild 
     ** are removed                                                     
      + rmon/inc/rmportdf.h
                                                            [RFC: 19182]
     __________________________________________________________________
     ** Modified to fix the issue -RMON counters are not implemented 
     ** properly for switchcore target

      + rmon/inc/rmportdf.h
                                                            [RFC: 15008]
     __________________________________________________________________

     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL3-0-1-5                                  |
    |    RELEASE DATE    : July 15 2003                                |
    '__________________________________________________________________'

    ...... OBSOLETED: .................................................
      + src/rmhwport.c

    ______ CHANGELOG: _________________________________________________
     **  Moved npapi related files to npapi folder and removed unwanted
     **  functions. 

      + inc/rmonprot.h          inc/rmonglob.h          src/ralevnt.c
      + make.h                  src/rplalrm.c           src/ralhost.c
      + src/ralmtrx.c           Makefile                src/rmonmain.c
      + src/fsrmolow.c          src/rplhist.c           inc/rmallinc.h
      + src/ralstat.c           inc/rmontdfs.h
                                                            [RFC: 5331]
     __________________________________________________________________
     **  Modified for Rmon support global variables reset when module
     **  disabled. 

      + src/rmonmain.c
                                                            [RFC: 5366]
     __________________________________________________________________
     **  Fixed - Owner string and Display string objects get request length
     **  not properly filled. 

      + src/rpltopn.c           src/ralevnt.c           src/rplalrm.c
      + src/ralhost.c           src/ralmtrx.c           src/rplhist.c
      + src/ralstat.c           src/rallog.c
                                                            [RFC: 5376]
     __________________________________________________________________
     **  Modified Function NextValidInex ,
     **  nmhValidateIndexInstanceEtherStatsTable and the get
     **  routines of all the objects in the table to search for
     **  under creation entries. 

      + src/rmonutl.c           src/rmonmain.c          src/ralstat.c
                                                            [RFC: 5384]
     __________________________________________________________________
     **  Added EXTND_SERVICE flag. 

      + make.h
                                                            [RFC: 5498]
    ______ CHANGELOG: _________________________________________________
     **  Updated for fix in Delta type alarm generation. 

      + src/rplalrm.c           src/rmpralrm.c          inc/rmontdfs.h
                                                            [RFC: 5562]
     __________________________________________________________________
    (______________________________END_________________________________)
     __________________________________________________________________
    |    RELEASE NUMBER  : REL3-0-1-4                                  |
    |    RELEASE DATE    : May 15 2003                                 |
    '__________________________________________________________________'

    ...... ADDED: .................................................
     * Added for cli commands seperation
      + inc/rmncds.def        +inc/rmnincds.def         +inc/rmnrtcds.def

     *  Moved cli/src/clirmon.c to rmon/src/rmonfscli.c 
      + src/rmonfscli.c

     *  Added for SNMP_2 Migration 
      + src/stdrmowr.c     +src/fsrmonwr.c
      + inc/stdrmowr.h     +inc/stdrmodb.h
      + src/fsrmonwr.h     +inc/fsrmondb.h

    ...... OBSOLETED: .................................................
      + src/rmoncmds.def

    ______ CHANGELOG: _________________________________________________
     **  Modified to map RMON_MALLOC to MEM_MALLOC 

      + src/rmoncli.c
                                                            [RFC: 4787]
     __________________________________________________________________
     **  Modified to accept the valid range of History Control Interval
     **  value. 

      + src/rplhist.c
                                                            [RFC: 4788]
     __________________________________________________________________
     **  Updated for SNMP_2 Migration 

      + inc/rmonglob.h          make.h                  Makefile
      + src/rmpralrm.c          src/rmonutl.c           inc/rmondefn.h
      + src/rmonmain.c          src/fsrmolow.c          inc/rmallinc.h
      + src/rmapevnt.c          inc/rmportdf.h
                                                            [RFC: 4801]
     __________________________________________________________________
     **  Moved cli/src/clirmon.c to rmon/src/rmonfscli.c 

      + Makefile
                                                            [RFC: 4848]
     __________________________________________________________________
     **  Port down should not delete the entries configured by the manager 

      + src/rmoncfif.c
                                                            [RFC: 4858]
     __________________________________________________________________
     **  Removed warnings arising in DIAB compiler. 

      + inc/rmonprot.h          src/rmontmm.c           src/rmonutl.c
      + src/rmportst.c          src/rmonmain.c          src/fsrmolow.c
      + src/rallog.c            src/rmapevnt.c          src/rmapmtrx.c
      + src/rmhwport.c          src/rmprhist.c
                                                            [RFC: 4875]
     __________________________________________________________________
     **  Updated to include future/inc/cli path. 

      + rmon.dsp
                                                            [RFC: 4915]
     __________________________________________________________________
                    [END OF 3-0-1-4 CHANGES]
     __________________________________________________________________
    |    RELEASE NUMBER  : REL3-0-1-3                                  |
    |    RELEASE DATE    : March 31 2003                               |
    '__________________________________________________________________'

    Bugs Fixed, Enhancements made in this patch release:

    o. File rmoncliprot.h has changed to rmonclipt.h to make 
       uniform for all l2 protocols.
    o. Command action routine for no trace all, function RmonNoTrace is added.
    o. Replaced \n with \r\n in rmoncli.c file

    ______ CHANGELOG: _________________________________________________
     **  Provided support for no trace command and rmoncliprot.h renamed as
     **  rmonclipt.h 

      + make.h                  Makefile                src/rmoncli.c
      + rmoncliprot.h is renamed as rmonclipt.h
                                                            [RFC: 4320]
     __________________________________________________________________
                    [END OF 3-0-1-3 CHANGES]

PATCH RELEASE NUMBER    : 3-0-1-2

RELEASE DATE            : February 19 2003

Bugs Fixed, Enhancements made in this patch release:

o Introduced FUTURE_SNMP_WANTED switch
o Modified for rmon hdc fixes.
o Hardware API changes to support the new data structures
---------------------------

The following files have been modified for this patch release:
-------------------------------------------------------------
FILE : inc/rmonprot.h
FILE : src/rmpralrm.c
FILE : src/rmonutl.c
FILE : src/rmonmain.c
FILE : src/rmapevnt.c

1) Introduced FUTURE_SNMP_WANTED switch
[RFCId: 3983]
-------------------------------------------------------------
FILE : src/ralevnt.c
FILE : src/rmaplog.c
FILE : src/rmoncli.c
FILE : src/rallog.c
FILE : inc/rmportdf.h

1) Fixed trap problem in case of delta type,Removed realloc memory call
and fixed log table problem.
[RFCId: 3935]
-------------------------------------------------------------
FILE : src/ralstat.c
FILE : src/rmhwport.c

1) Hardware API changes to support the new data structures
[RFCId: 3782]
-------------------------------------------------------------
                    [END OF 3-0-1-2 CHANGES]

PATCH RELEASE NUMBER    : 3-0-1-1

RELEASE DATE            : January 21 2003

Bugs Fixed, Enhancements made in this patch release:

1. Provided cli support for stats deletion and event group deletion.
2. Alarm configuration problems fixed.
3.Removed warnings
---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : inc/rmportdf.h
FILE : src/rmapevnt.c
FILE : src/rmoncmds.def
FILE : src/ralevnt.c
FILE : src/rplalrm.c
FILE : src/ralstat.c
FILE : inc/rmonprot.h
FILE : inc/rmoncliprot.h
FILE : src/rmonmain.c
FILE : src/rmoncli.c
FILE : inc/rmondefn.h
FILE : src/rmonutl.c

1) Fixed bugs for NP based switches.
[RFCId: 3712]

-------------------------------------------------------------
FILE: inc/rmoncliprot.h

1) Removed warnings
[RFCId: 3782]
-------------------------------------------------------------
                    [END OF 3-0-1-1 CHANGES]

PRODUCT NAME            : FutureRMON

PATCH RELEASE NUMBER    : 3-0-1-0

RELEASE DATE            : January 10 2003

Bugs Fixed, Enhancements made in this maintenance release:

1. In function StoreSnapshotInHistoryTable(),the floating point 
operation is used to calculate the utility of the bandwidth. 
Which has been modified. 

2.In function RmonGetMibVariableValue() the memory allocted for baseSubOid 
is not freed.  This has been taken care.
---------------------------

The following files have been modified for this maintenance release:

-------------------------------------------------------------
FILE : src/rmprhist.c
FILE : src/rmonutl.c

1) Fix for floating point exception and memory leak in alarm entry
config
[RFCId:3637]

-------------------------------------------------------------
                    [END OF 3-0-1-0 CHANGES]

PATCH RELEASE NUMBER    : REL3-0-0-5

RELEASE DATE            : December 27 2002

Bugs Fixed, Enhancements made in this patch release:

1. Modified the data structures for the tables for the BASIC groups. 
Converted these data structures to hash based implementation to 
support indexing range. Modified all the low level routines the mib 
objects for these groups.

2. rmoncmds.def file has been changed to support event description - can be specified in multiple words.

3. Modified the rmoncli.c file for alarm configuration in case of invalid alarm.

---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : inc/rmportdf.h
FILE : src/rallog.c
FILE : src/rmapevnt.c
FILE : src/rmoncmds.def
FILE : src/rmapstat.c
FILE : inc/rmallinc.h
FILE : src/rplhist.c
FILE : src/ralevnt.c
FILE : src/rplalrm.c
FILE : src/ralstat.c
FILE : inc/rmonprot.h
FILE : src/rmaplog.c
FILE : src/rmprhist.c
FILE : src/rmpralrm.c
FILE : src/rmhwport.c
FILE : src/ralmtrx.c
FILE : src/rmonmain.c
FILE : src/fsrmolow.c
FILE : src/rmontmm.c
FILE : inc/rmonglob.h
FILE : src/rmoncli.c
FILE : inc/rmondefn.h
FILE : inc/rmontdfs.h
FILE : src/rmonutl.c

1) Indexing support for RMON Basic Groups
[RFCId: 3543]

-------------------------------------------------------------
                    [END OF 3-0-0-5 CHANGES]

PATCH RELEASE NUMBER    : 3-0-0-4

RELEASE DATE            : November 29 2002

Bugs Fixed, Enhancements made in this patch release:

o Fixed code wizard warnings and fix rmon HDCs
---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : src/rpltopn.c
FILE : inc/rmportdf.h
FILE : inc/rmondebg.h
FILE : src/rallog.c
FILE : inc/stdrmmdb.h
FILE : src/rmapevnt.c
FILE : inc/fsrmolow.h
FILE : src/rmoncmds.def
FILE : src/stdrmmid.c
FILE : src/rmprtopn.c
FILE : src/rmapstat.c
FILE : src/rplhist.c
FILE : src/ralevnt.c
FILE : src/rplalrm.c
FILE : src/ralstat.c
FILE : inc/rmonprot.h
FILE : src/rmaplog.c
FILE : src/rmapmtrx.c
FILE : src/rmportst.c
FILE : src/rmprhist.c
FILE : inc/stdrmcon.h
FILE : src/rmpralrm.c
FILE : src/rmhwport.c
FILE : src/ralmtrx.c
FILE : inc/rmonoidt.h
FILE : make.h
FILE : src/rmonmain.c
FILE : src/fsrmolow.c
FILE : src/rmontmm.c
FILE : inc/rmonglob.h
FILE : src/rmaphost.c
FILE : src/rmoncli.c
FILE : README
FILE : inc/rmondefn.h
FILE : inc/rmontdfs.h
FILE : src/ralhost.c
FILE : src/rmonutl.c

1) Fixed code wizard warnings and fix rmon HDCs
[RFCId: 3364]

-------------------------------------------------------------
                    [END OF 3-0-0-4 CHANGES]

-------------------------------------------------------------
PATCH RELEASE NUMBER    : 3-0-0-3

RELEASE DATE            : October 28 2002



Bugs Fixed, Enhancements made in this patch release:

o For HW fixes, the resetting of debug flag and also related to
enabling status.
o Modified for EtherStats, Host & Matrix Index validation while
deleting an entry
o Changed the function name from RmonInitialize1() to RmonCreateTask().
---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : src/rmoncmds.def

1) Modified for Index Access problems in EtherStats, Host & Matrix while deleting
an entry
[RFCId: 2592]

-------------------------------------------------------------
FILE : src/ralevnt.c

1) Modified for Index Access problems in EtherStats, Host & Matrix while
deleting an entry
[RFCId: 2735]

-------------------------------------------------------------
FILE : inc/rmonprot.h

1) For HW fixes, the resetting of debug flag and also related to enabling
status.
[RFCId: 3126]

2) Changed the function name from RmonInitialize1() to RmonCreateTask().
[RFCId: 2473]

-------------------------------------------------------------
FILE : src/rmonmain.c

1) For HW fixes, the resetting of debug flag and also related to enabling
status.
[RFCId: 3126]

2) Changed the function name from RmonInitialize1() to RmonCreateTask().
[RFCId: 2473]

-------------------------------------------------------------
FILE : src/fsrmolow.c

1) For HW fixes, the resetting of debug flag and also related to enabling
status.
[RFCId: 3126]

2) Changed the function name from RmonInitialize1() to RmonCreateTask().
[RFCId: 2473]

-------------------------------------------------------------
FILE : src/rmoncli.c

1) Modified for Index Access problems in EtherStats, Host & Matrix while deleting
an entry
[RFCId: 2592]

-------------------------------------------------------------
FILE : src/ralmtrx.c
FILE : make.h
FILE : src/rmhwport.c
FILE : src/rmontmm.c
FILE : src/ralstat.c
FILE : inc/rmondefn.h
FILE : inc/rmportdf.h
FILE : src/ralhost.c
FILE : src/rmonutl.c

1) For HW fixes, the resetting of debug flag and also related to enabling
status.
[RFCId: 3126]

-------------------------------------------------------------
                    [END OF 3-0-0-3 CHANGES]

PATCH RELEASE NUMBER    : 3-0-0-2

RELEASE DATE            : July 23 2002


Known problems in this release:

None.


Bugs Fixed, Enhancements made in this patch release:

o Modified for code cleanup and compiled using latest MIDGEN tool to
  solve the tMacAddr structure usage.
o Removed the files generated by the old MidGen tool. 
o Removed compiler warnings for VC++.
---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : src/fsrmomid.c

-------------------------------------------------------------
FILE : src/rpltopn.c

-------------------------------------------------------------
FILE : inc/rmportdf.h

-------------------------------------------------------------
FILE : src/rallog.c

-------------------------------------------------------------
FILE : src/rmapevnt.c

-------------------------------------------------------------
FILE : src/rmoncmds.def

-------------------------------------------------------------
FILE : src/stdrmmid.c

-------------------------------------------------------------
FILE : src/rmprtopn.c

-------------------------------------------------------------
FILE : src/rmapstat.c

-------------------------------------------------------------
FILE : inc/rmallinc.h

-------------------------------------------------------------
FILE : src/rplhist.c

-------------------------------------------------------------
FILE : src/ralevnt.c

-------------------------------------------------------------
FILE : src/rplalrm.c

-------------------------------------------------------------
FILE : src/ralstat.c

-------------------------------------------------------------
FILE : inc/rmonprot.h

-------------------------------------------------------------
FILE : inc/stdrmlow.h

-------------------------------------------------------------
FILE : src/rmaplog.c

-------------------------------------------------------------
FILE : src/rmapmtrx.c

-------------------------------------------------------------
FILE : src/rmportst.c

-------------------------------------------------------------
FILE : src/rmprhist.c

-------------------------------------------------------------
FILE : src/rmpralrm.c

-------------------------------------------------------------
FILE : src/rmhwport.c

-------------------------------------------------------------
FILE : inc/fsrmomid.h

-------------------------------------------------------------
FILE : src/ralmtrx.c

-------------------------------------------------------------
FILE : inc/rmoncliprot.h

-------------------------------------------------------------
FILE : src/rmonmain.c

-------------------------------------------------------------
FILE : src/fsrmolow.c

-------------------------------------------------------------
FILE : src/rmontmm.c

-------------------------------------------------------------
FILE : inc/stdrmmid.h

-------------------------------------------------------------
FILE : src/rmaphost.c

-------------------------------------------------------------
FILE : src/rmoncli.c

-------------------------------------------------------------
FILE : src/rmstubs.c

-------------------------------------------------------------
FILE : inc/rmontdfs.h

-------------------------------------------------------------
FILE : src/ralhost.c

-------------------------------------------------------------
FILE : src/rmoncfif.c

-------------------------------------------------------------
FILE : src/rmonutl.c

1) Removed the files for new MIDGEN tool 

[RFCId: 2434]

-------------------------------------------------------------
FILE : inc/fsrmon_snmpmbdb.h
-------------------------------------------------------------
FILE : inc/stdrmon_snmpmbdb.h
-------------------------------------------------------------
                    [END OF 3-0-0-2 CHANGES]
-------------------------------------------------------------

PATCH RELEASE NUMBER    : 3-0-0-1

RELEASE DATE            : June 28, 2002


Known problems in this release:

o None

Bugs Fixed, Enhancements made in this patch release:

1. Compilation warnings fixed
2. CLI commands support for RMON 

o Removed compilation warnings
---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : inc/rmonprot.h
FILE : src/rmonmain.c
FILE : src/rmonutl.c
FILE : src/ralmtrx.c

o Added CLI Files for RMON 
---------------------------

The following files have been added for this patch release:

-------------------------------------------------------------
FILE : inc/rmoncliprot.h
FILE : src/rmoncli.c
FILE : src/rmoncmds.def

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : Makefile
FILE : make.h
FILE : README
--------------------------------------------------------------
                    [END OF 3-0-0-1 CHANGES]
--------------------------------------------------------------

PRODUCT NAME            : FutureRMON
-------------------------------------------------------------
RELEASE NUMBER          : 3-0-0-0

RELEASE DATE            : June 6, 2002


Known problems in this release:

o None

Bugs Fixed, Enhancements made in this release:

o Removed compilation warnings 
o Made the code compliant to FS coding guidelines.
o Renamed the Filenames
o Modified the Module organization similar to FS products.
o Supports Dynamic creation of entries in all RMON control Tables.
o Corrected Alarm Hysteresis algorithm.
o Modified the maximum size of allocations for control table entries.
o Added Definitions for new globals and for Dynamic Table entries.
o Added a Function to notify RMON about the Interface Status.
o RmonDeleteTask function is implemented to delete all resources when RMON is
disabled.
o Supports HW API interface for integration with Hardware.
o Supports Dynamic enabling of RMON feature.

------------------------------------------------------------

The following files have been modified for this release:
FILE : rmon/src/ralstat.c
       rmon/src/ralmtrx.c
       rmon/src/ralhost.c
       rmon/src/ralevnt.c
       rmon/src/rallog.c
       rmon/src/rplhist.c
       rmon/src/rpltopn.c
       rmon/src/rplalrm.c
       rmon/src/rmapstat.c
       rmon/src/rmaphost.c
       rmon/src/rmapmtrx.c
       rmon/src/rmapevnt.c
       rmon/src/rmaplog.c
       rmon/src/rmprhist.c
       rmon/src/rmprtopn.c
       rmon/src/rmpralrm.c
       rmon/src/rmontmm.c
       rmon/src/rmonutl.c
       rmon/src/rmonmain.c
       rmon/src/stdrmmid.c
       rmon/inc/rmonglob.h
       rmon/inc/rmontdfs.h
       rmon/inc/rmondefn.h
       rmon/inc/rmonprot.h
       
1)  Conversion of Static Table entry creation to Dynamic Table entries.
[RFCId : 1554]
-------------------------------------------------------------
FILE: rmon/src/rmoncfif.c

1) This file has been created to provide the Interface Status notification
function from CFA.
{RMON_E project]

-------------------------------------------------------------
FILE: rmon/src/fsrmolow.c

1) This file has been updated with additional proprietary MIBs for RMON Enable,
Disable, RMON groups support etc.
[RMON_E Project]
------------------------------------------------------------
FILE: rmon/Makefile
      rmon/make.h

1) These files have been modified for the new directory organization and 
   new file names.
------------------------------------------------------------
FILE: rmon/src/rmonmain.c
      rmon/src/fsrmolow.c

1) This file has been modified to enable/Disable RMON feature dynamically
[RMON_E Project]
------------------------------------------------------------
FILE: rmon/src/rmhwport.c

1) This file supports the HWAPI routines when RMON is integrated with Hardware.
   The function implemented in this file have to be ported for various HW
   targets.
[RMON_E Project]
------------------------------------------------------------
                    [END OF 3-0-0-0 CHANGES]

-------------------------------------------------------------
PATCH RELEASE NUMBER    : 2-0-0-1

RELEASE DATE            : January 10, 2002


Known problems in this release:

o None

Bugs Fixed, Enhancements made in this patch release:

o Removed compilation warnings
---------------------------

The following files have been modified for this patch release:

-------------------------------------------------------------
FILE : common/inc/rmontdfs.h
FILE : porting/inc/rmportdf.h
FILE : common/inc/rmonprot.h
FILE : common/src/rmonmain.c

1) Removed compilation warnings and #if 0 present in the code
[RFCId:1180]

-------------------------------------------------------------

                    [END OF 2-0-0-1 CHANGES]

RELEASE NUMBER    : 2-0-0-0

RELEASE DATE      : October 04 2001

-------------------------------------------------------------

Known problems in this release:

None.

-------------------------------------------------------------

Bugs Fixed, Enhancements made in this patch release:

o Changes for Integeration with LR, FSAP2, New SNMP Architecture.
o Creation of README for REL 2-0-0-0.

-------------------------------------------------------------

The following files have been modified for this patch release:

FILES :  aperiod/src/ral_evnt.c
         common/inc/rmonglob.h
         period/src/rmprtopn.c
         aperiod/src/rmapstat.c
         period/src/rmprhist.c
         README
         common/inc/rmondefn.h
         period/src/rpl_alrm.c
         aperiod/src/ral_host.c
         aperiod/src/ralo_log.c
         aperiod/src/ral_mtrx.c
         porting/src/rmportst.c
         aperiod/src/ral_stat.c
         common/inc/rmontdfs.h
         period/src/rmpralrm.c
         porting/inc/rmportdf.h
         period/src/rpl_topn.c
         aperiod/src/rmapevnt.c
         common/inc/rmonprot.h
         period/src/rpl_hist.c
         common/src/rmonmain.c
         common/src/rmon_tmm.c
         porting/src/rmstubs.c
         aperiod/src/rmaphost.c
         aperiod/src/rmap_log.c
         common/inc/rmallinc.h
         aperiod/src/rmapmtrx.c

1) Changes for integeration with LR, FSAP2 and NEW SNMP Architecture.
2) Changes for Trap support.

[RFCId: 647]

-------------------------------------------------------------
                    [END OF 2-0-0-0 CHANGES]

