####################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.11 2012/12/11 15:05:45 siva Exp $
#
####################################################

#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10/08/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureRMON         |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | Yogindhar P| Creation of makefile                              |
# |         | 10/08/2001 |                                                   |
# +--------------------------------------------------------------------------+
# |   2     | Leela L    | Included new directory folders                    |
# |         | 10/04/2002 |                                                   |
# +--------------------------------------------------------------------------+
# |   3     | Leela L    | Included cli files                                |
# |         | 28/06/2002 |                                                   |
# +--------------------------------------------------------------------------+



# Set the RMON_BASE_DIR as the directory where you untar the project files

RMON_NAME  = FutureRMON

RMON_BASE_DIR    = ${BASE_DIR}/rmon

RMON_SRC_DIR  = ${RMON_BASE_DIR}/src
RMON_CO_INC_DIR   = ${RMON_BASE_DIR}/inc

RMON_OBJ_DIR  = ${RMON_BASE_DIR}/obj

SNMP_AGT_INC_DIR = ${SNMP_BASE_DIR}/agent/inc
SNMP_COM_INC_DIR = ${SNMP_BASE_DIR}/common/inc

# Specify the project level compilation switches here
RMON_COMPILATION_SWITCHES = -DDEBUG_RMON=OFF

ifeq (${NPAPI}, NO)
RMON_COMPILATION_SWITCHES += -DEXTND_SERVICE
endif

ifeq (${NPAPI}, YES)
RMON_COMPILATION_SWITCHES += -DNPAPI_WANTED
endif

# Specify the project include directories and dependencies
RMON_CO_INC_FILES = $(RMON_CO_INC_DIR)/rmallinc.h \
    $(RMON_CO_INC_DIR)/rmportdf.h 

SNMP_RMON_ALL_INC_FILES = $(SNMP_INCL_DIR)/snmccons.h \
    $(SNMP_INCL_DIR)/snmcdefn.h \
    $(SNMP_INCL_DIR)/snmctdfs.h \
    $(COMN_INCL_DIR)/cfa.h \
    $(COMN_INCL_DIR)/rmon.h

RMON_SNMP_INC_FILES    = $(RMON_CO_INC_DIR)/rmonoidt.h \
     $(SNMP_INCL_DIR)/snmctdfs.h \
    $(SNMP_INCL_DIR)/snmcport.h \
    $(SNMP_COM_INC_DIR)/snmcprot.h \
    $(SNMP_AGT_INC_DIR)/snmatdfs.h \
    $(SNMP_AGT_INC_DIR)/snmaprot.h    

RMON_ALL_INC_FILES = $(RMON_CO_INC_FILES)

RMON_INC_DIR  = -I$(RMON_CO_INC_DIR)
#     -I$(SNMP_INC_DIR)

RMON_SNMP_INC_DIR = -I$(RMON_CO_INC_DIR) \
     -I$(SNMP_INCL_DIR) \
     -I$(SNMP_COM_INC_DIR) \
     -I$(SNMP_AGT_INC_DIR)

RMON_SNMP_DEPENDENCIES = $(RMON_SNMP_INC_FILES)

RMON_FINAL_INCLUDES_DIRS = $(RMON_INC_DIR) \
      $(RMON_SNMP_INC_DIR) \
      $(COMMON_INCLUDE_DIRS)

RMON_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
    $(RMON_ALL_INC_FILES) \
                $(COMN_INCL_DIR)/rmon.h \
    $(RMON_BASE_DIR)/Makefile \
    $(RMON_BASE_DIR)/make.h

