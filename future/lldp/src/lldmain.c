/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldmain.c,v 1.33 2017/12/14 10:30:26 siva Exp $
 *
 * Description: This file contains LLDP task main loop and 
 *              initialization routines.
 *********************************************************************/
#include "lldinc.h"
#include "lldglob.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 LldpMainTaskInit PROTO ((VOID));
PRIVATE VOID LldpMainHandleInitFailure PROTO ((VOID));
PRIVATE INT4        LldpMainInitLocSysInfo (VOID);
PRIVATE VOID        LldpMainInitLocSysConfigInfo (VOID);

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainTask
 *                                                                          
 *    DESCRIPTION      : Entry point function of LLDP task. 
 *
 *    INPUT            : pArg - Pointer to the arguments 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pArg);

    if (LldpMainTaskInit () == OSIX_FAILURE)
    {
        LldpMainHandleInitFailure ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Register the protocol MIBS with SNMP */
#ifdef SNMP_2_WANTED
    RegisterFSLLDP ();
    RegisterSTDLLDP ();
    RegisterSTDOT1 ();
    RegisterSTDOT3 ();
    RegisterSLLDV2 ();
    RegisterSD1LV2 ();
    RegisterSD3LV2 ();
    /*LLDP-MED MIB registration with SNMP */
    RegisterFSLLDM ();
    RegisterSTDLLDPM ();
#endif /*SNMP_2_WANTED */

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gLldpGlobalInfo.TaskId, LLDP_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* Mutual exclusion flag ON */
            LldpLock ();

            /* order of proceesing the event:
             * 1. Timer Event
             * 2. Information Change notification event
             * 3. Received PDU event 
             * 
             * Timer event is FSAP event it should be precessed first, 
             * delaying processing of timer event is incorrect.
             * 
             * Local information change should be handled before processing 
             * Received PDU event, otherwise stale information will be 
             * advertised to the peer nodes.
             *
             * Received PDU event is processed at the end, if  send to 
             * queue(RxPduQ) fails it is not handled, ie missing pakcet will
             * not give any problem, because the packet will be transmitted
             * again */
            if (u4Events & LLDP_TMR_EXPIRY_EVENT)
            {
                LLDP_TRC (CONTROL_PLANE_TRC, "MAIN: Recvd Tmr Exp Event\r\n");
                LldpTmrExpHandler ();
            }

            if (u4Events & LLDP_QMSG_EVENT)
            {
                LLDP_TRC (CONTROL_PLANE_TRC, "MAIN: Recvd MsgQ If Event\r\n");
                LldpQueAppMsgHandler ();
            }

            if (u4Events & LLDP_RXPDU_EVENT)
            {
                LLDP_TRC (CONTROL_PLANE_TRC, "MAIN: Recvd PduQ Event\r\n");
                LldpQueRxPduMsgHandler ();
            }

#ifdef L2RED_WANTED
            /* For processing subbulk updates */
            if (u4Events & LLDP_RED_SUB_BULK_UPD_EVENT)
            {
                LldpRedProcBulkUpdReq ();
            }
#endif
            /* Mutual exclusion flag OFF */
            LldpUnLock ();
        }
    }                            /* end of while */
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainTaskInit 
 *                                                                          
 *    DESCRIPTION      : This function creates the task queue, allocates 
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
LldpMainTaskInit (VOID)
{
    MEMSET (&gLldpGlobalInfo, 0, sizeof (tLldpGlobalInfo));

    OsixTskIdSelf (&(gLldpGlobalInfo.TaskId));

    /* LLDP Protocol semaphore - created with initial value 0. */
    if (OsixSemCrt ((UINT1 *) LLDP_PROTO_SEM,
                    &(gLldpGlobalInfo.SemId)) == OSIX_FAILURE)
    {
        LLDP_TRC (OS_RESOURCE_TRC, "LldpMainTaskInit: LLDP protocol semaphore"
                  "creation FAILED!!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gLldpGlobalInfo.SemId);

    /* Lldp MsgQ */
    if (OsixQueCrt ((UINT1 *) LLDP_MSG_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    LLDP_MAX_MSGQ_DEPTH, &(gLldpGlobalInfo.MsgQId))
        == OSIX_FAILURE)
    {
        LLDP_TRC (OS_RESOURCE_TRC, "LldpMainTaskInit: LLDP MsgQ creation"
                  "FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Lldp Received PDU Q */
    if (OsixQueCrt ((UINT1 *) LLDP_RXPDU_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    LLDP_MAX_RXPDUQ_DEPTH, &(gLldpGlobalInfo.RxPduQId))
        == OSIX_FAILURE)
    {
        LLDP_TRC (OS_RESOURCE_TRC, "LldpMainTaskInit: LLDP RxPduQ creation"
                  "FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* start LLDP module */
    if (LldpModuleStart () == OSIX_FAILURE)
    {
        LLDP_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "LldpMainTaskInit: LLDP Module Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainMemClear 
 *                                                                          
 *    DESCRIPTION      : Function that deletes all memepools
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
LldpMainMemClear (VOID)
{
    UINT1               u1GlobShutWhileTmrStatus =
        LLDP_GLOB_SHUT_WHILE_TMR_STATUS ();

#ifdef L2RED_WANTED
    if (LLDP_RED_NODE_STATUS () == RM_STANDBY)
    {
        if (gLldpRedGlobalInfo.bGlobShutWhileTmrSyncUpRvcd == OSIX_TRUE)
        {
            u1GlobShutWhileTmrStatus = LLDP_TMR_RUNNING;
        }
        else
        {
            u1GlobShutWhileTmrStatus = LLDP_TMR_NOT_RUNNING;
        }
    }
#endif

    LldpSizingMemDeleteMemPools ();
    /* Local Port Info memory pool should be deleted after Global ShutWhile
     * timer expires */

    /* LldpMainMemClear - This function is called in many scenarios, one of
     * them is when LLDP module is shutdown. So check the global shutdown while 
     * timer status before deleting port info memory pool. If the global timer
     * is started, then releasing of port info mem blocks
     * and pool id will be taken care during timer expiry. If the timer is not
     * started, it has to be taken care here */
    if (u1GlobShutWhileTmrStatus == LLDP_TMR_NOT_RUNNING)
    {
        if (gLldpGlobalInfo.LocPortInfoPoolId != 0)
        {
            if (MemDeleteMemPool (gLldpGlobalInfo.LocPortInfoPoolId) ==
                MEM_FAILURE)
            {
                LLDP_TRC (OS_RESOURCE_TRC, "LldpMainMemClear: "
                          "Local Port Info MemPool Delete FAILED\r\n");
            }
            gLldpGlobalInfo.LocPortInfoPoolId = 0;
        }
    }

    /* dont delete mempools for QMsgPoolId, RxPduQPoolId here, it will be 
     * deleted while deleting the queues */
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainInitGlobalInfo 
 *                                                                          
 *    DESCRIPTION      : This function initalizes global structure with 
 *                       default values.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
LldpMainInitGlobalInfo (VOID)
{
    UINT1               u1Flag = OSIX_FALSE;

    /* 1. Populate local system information */
    LldpMainInitLocSysInfo ();

    /* 2. Initialize Local default system configuration information */
    LldpMainInitLocSysConfigInfo ();
    /* MEMSET (gLldpGlobalInfo) is done in main, so no need to 
     * initialize variables to 0 wherever required */
    /* 4. Assign initial values for global objects */
    /* Here OR operation is performed so that the trace messages set
     * before shutdown is not over written */
    gLldpGlobalInfo.u4TraceOption |= LLDP_CRITICAL_TRC;

    LldpUtilGetTraceInputValue (gLldpGlobalInfo.au1TraceInput,
                                gLldpGlobalInfo.u4TraceOption);

    /* bProtoVlanSupported */
    LldpPortGetProtoVlanCapab (&u1Flag);
    if (u1Flag == VLAN_TRUE)
    {
        gLldpGlobalInfo.u1ProtoVlanSupported = LLDP_TRUE;
    }
    else
    {
        gLldpGlobalInfo.u1ProtoVlanSupported = LLDP_FALSE;
    }
    gLldpGlobalInfo.bNotificationEnable = OSIX_FALSE;
    gLldpGlobalInfo.bSendRemTblNotif = OSIX_FALSE;
    LLDP_SYSTEM_CONTROL () = LLDP_SHUTDOWN;
    gLldpGlobalInfo.u1ModuleStatus = LLDP_DISABLED;
    gLldpGlobalInfo.i4NextFreeRemIndex = 1;    /* Next Free Remote Index */
    gLldpGlobalInfo.i4NextFreeRemOrgDeInfoIndex = 1;
    gLldpGlobalInfo.i4Version = LLDP_VERSION_05;
    LLDP_NOTIF_INTVAL_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
    LLDP_GLOB_SHUT_WHILE_TMR_STATUS () = LLDP_TMR_NOT_RUNNING;
    /* Initialize the Global Statistics Counters */
    MEMSET (&gLldpGlobalInfo.StatsRemTabInfo, 0, sizeof (tLldpStatsRemTabInfo));
    /* Initialize the error counters */
    gLldpGlobalInfo.i4MemAllocFailure = 0;
    gLldpGlobalInfo.i4InputQOverFlows = 0;
    gLldpGlobalInfo.i4Version = LLDP_VERSION_05;
    /* Tagged Status */
    gLldpGlobalInfo.i4TagStatus = LLDP_TAG_DISABLE;
    MEMSET (gLldpGlobalInfo.MgmtAddr.au1Addr, 0,
            sizeof (gLldpGlobalInfo.MgmtAddr.au1Addr));
    MEMSET (gLldpGlobalInfo.MgmtAddr6.au1Addr, 0,
            sizeof (gLldpGlobalInfo.MgmtAddr6.au1Addr));
#ifndef FM_WANTED
    gLldpGlobalInfo.u4SysLogId = 0;
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainInitLocSysConfigInfo 
 *                                                                          
 *    DESCRIPTION      : This function initializes Local system information 
 *                       in the global structure.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpMainInitLocSysConfigInfo (VOID)
{
    /* Message transmit interval */
    gLldpGlobalInfo.SysConfigInfo.i4MsgTxInterval = LLDP_MSG_TX_INTERVAL;
    /* Hold time multiplier */
    gLldpGlobalInfo.SysConfigInfo.i4MsgTxHoldMultiplier =
        LLDP_MSG_TX_HOLD_MULTIPLIER;
    /* Re-Initialization delay */
    gLldpGlobalInfo.SysConfigInfo.i4ReInitDelay = LLDP_REINIT_DELAY;
    /* Transmit delay */
    gLldpGlobalInfo.SysConfigInfo.i4TxDelay = LLDP_TX_DELAY;
    /* Notification Interval */
    gLldpGlobalInfo.SysConfigInfo.i4NotificationInterval =
        LLDP_NOTIFICATION_INTERVAL;
    gLldpGlobalInfo.SysConfigInfo.u4TxCreditMax = 1;
    gLldpGlobalInfo.SysConfigInfo.u4MessageFastTx = 30;
    gLldpGlobalInfo.SysConfigInfo.u4TxFastInit = 1;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainInitLocSysInfo 
 *                                                                          
 *    DESCRIPTION      : This function initializes Local system information 
 *                       in the global structure.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
LldpMainInitLocSysInfo (VOID)
{
    tMacAddr            SwitchMacAddr;
    /*UINT4               u4PortIndex = 0; */
    UINT2               u2ChassIdLen = 0;

    MEMSET (SwitchMacAddr, 0, sizeof (tMacAddr));
    /* chassis id subtype - default switch mac address */
    LldpPortGetSysMacAddr (SwitchMacAddr);
    /* get chassis id length */
    LldpUtilGetChassisIdLen (LLDP_CHASS_ID_SUB_MAC_ADDR, SwitchMacAddr,
                             &u2ChassIdLen);

    MEMCPY (gLldpGlobalInfo.LocSysInfo.au1LocChassisId, SwitchMacAddr,
            MEM_MAX_BYTES (u2ChassIdLen, sizeof (tMacAddr)));
    /* chassis id - switch mac address */
    gLldpGlobalInfo.LocSysInfo.i4LocChassisIdSubtype =
        LLDP_CHASS_ID_SUB_MAC_ADDR;
    /* system capabilites supported */
    MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocSysCapSupported, 0,
            ISS_SYS_CAPABILITIES_LEN);
    LldpPortGetSysCapaSupported
        (gLldpGlobalInfo.LocSysInfo.au1LocSysCapSupported);
    /* system capabilites enabled */
    MEMSET (gLldpGlobalInfo.LocSysInfo.au1LocSysCapEnabled, 0,
            ISS_SYS_CAPABILITIES_LEN);
    LldpPortGetSysCapaEnabled (gLldpGlobalInfo.LocSysInfo.au1LocSysCapEnabled);
    /* system name */
    LldpPortGetSysName (&(gLldpGlobalInfo.LocSysInfo.au1LocSysName[0]));

    /* system description */
    LldpPortGetSysDescr (&(gLldpGlobalInfo.LocSysInfo.au1LocSysDesc[0]));

    /* LLDP-MED Device class */
    gLldpGlobalInfo.LocSysInfo.u1MedLocDeviceClass =
        LLDP_MED_NW_CONNECTIVITY_DEVICE;

    /*LLDP-MED Inventory Information */
    MEMSET (&gLldpGlobalInfo.LocSysInfo.LocInventoryInfo, 0,
            sizeof (tLldpMedLocInventoryInfo));
    LldpMedPortGetInventoryInfo (&gLldpGlobalInfo.LocSysInfo.LocInventoryInfo);

    /* LLDP-MED PoE Power Information */
    LldpMedPortGetPoEPowerInfo (&gLldpGlobalInfo.LocSysInfo.
                                u1MedLocPSEPowerSource,
                                &gLldpGlobalInfo.LocSysInfo.
                                u1MedLocPoEDeviceType);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : LldpMainHandleInitFailure 
 *                                                                          
 *    DESCRIPTION      : Function called when the initialization of LLDP
 *                       fails at any point during initialization.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
LldpMainHandleInitFailure (VOID)
{
    /* No timer are started in MainTaskInit, Hence no need stop the timers */
    /* Delete RBTree */
    LldpUtilDeleteRBTree ();
    /* delete all mempools except mempools allocated for queues */
    LldpMainMemClear ();
    /* delete appication msg queue and mempool associated with this queue */
    LldpTxUtlDeleteQMsg (gLldpGlobalInfo.MsgQId, gLldpGlobalInfo.QMsgPoolId);

    if (MemDeleteMemPool (gLldpGlobalInfo.QMsgPoolId) == MEM_FAILURE)
    {
        LLDP_TRC (OS_RESOURCE_TRC, "LldpMainHandleInitFailure: Q message"
                  " MemPool Delete FAILED\r\n");
    }

    gLldpGlobalInfo.QMsgPoolId = 0;

    if (gLldpGlobalInfo.MsgQId != 0)
    {
        /* Delete Q */
        OsixQueDel (gLldpGlobalInfo.MsgQId);
    }

    /* delete received pdu queue and mempool associated with this queue */
    LldpTxUtlDeleteQMsg (gLldpGlobalInfo.RxPduQId,
                         gLldpGlobalInfo.RxPduQPoolId);

    if (MemDeleteMemPool (gLldpGlobalInfo.RxPduQPoolId) == MEM_FAILURE)
    {
        LLDP_TRC (OS_RESOURCE_TRC, "LldpMainHandleInitFailure: Q message"
                  " MemPool Delete FAILED\r\n");
    }

    gLldpGlobalInfo.RxPduQPoolId = 0;

    if (gLldpGlobalInfo.RxPduQId != 0)
    {
        /* Delete Q */
        OsixQueDel (gLldpGlobalInfo.RxPduQId);
    }

    /* Delete Semaphore */
    if (gLldpGlobalInfo.SemId != 0)
    {
        OsixSemDel (gLldpGlobalInfo.SemId);
    }

    /* DeInit Timer */
    if (gLldpGlobalInfo.TmrListId != 0)
    {
        if (LldpTmrDeInit () == OSIX_FAILURE)
        {
            LLDP_TRC (OS_RESOURCE_TRC, "LldpMainHandleInitFailure: "
                      "Timer Deinit FAILED\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LldpMainAssignMempoolIds                             */
/*                                                                           */
/* Description        : This function is to assign respective mempool        */
/*                      Id's                                                 */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
LldpMainAssignMempoolIds (VOID)
{
    /*tLldpQMsg */
    gLldpGlobalInfo.QMsgPoolId = LLDPMemPoolIds[MAX_LLDP_Q_MESG_SIZING_ID];
    /*tLldpRxPduQMsg */
    gLldpGlobalInfo.RxPduQPoolId =
        LLDPMemPoolIds[MAX_LLDP_RX_PDU_Q_MESG_SIZING_ID];
    /*tLldpLocManAddrTable */
    gLldpGlobalInfo.LocManAddrPoolId =
        LLDPMemPoolIds[MAX_LLDP_LOCAL_MGMT_ADDR_TABLE_SIZING_ID];
    /*tLldpRemoteNode */
    gLldpGlobalInfo.RemNodePoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_NODES_SIZING_ID];
    /*tLldpRemManAddressTable */
    gLldpGlobalInfo.RemManAddrPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_MGMT_ADDR_TABLE_SIZING_ID];
    /*tLldpRemUnknownTLVTable */
    gLldpGlobalInfo.RemUnknownTLVPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_UNKNOWN_TLV_TABLE_SIZING_ID];
    /*tLldpRemOrgDefInfoTable */
    gLldpGlobalInfo.RemOrgDefInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_ORG_DEF_INFO_TABLE_SIZING_ID];
    /*tLldpxdot1LocProtoIdInfo */
    gLldpGlobalInfo.LocProtoIdInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_LOCAL_PROTO_IDENTIFY_INFO_SIZING_ID];
    /*tLldpxdot1LocProtoVlanInfo */
    gLldpGlobalInfo.LocProtoVlanInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_LOCAL_PROTO_VLAN_INFO_SIZING_ID];
    /*tLldpxdot1RemVlanNameInfo */
    gLldpGlobalInfo.RemVlanNameInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_VLAN_NAME_INFO_SIZING_ID];
    /*tLldpxdot1RemProtoVlanInfo */
    gLldpGlobalInfo.RemProtoVlanPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_PROTO_VLAN_INFO_SIZING_ID];
    /*tLldpxdot1RemProtoIdInfo */
    gLldpGlobalInfo.RemProtocolPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_PROTO_IDENTIFY_INFO];
    /*tLldpxdot3RemPortInfo */
    gLldpGlobalInfo.RemDot3PortInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_REMOTE_PORT_INFO_SIZING_ID];
    /*tLldpAppInfo */
    gLldpGlobalInfo.AppTblPoolId = LLDPMemPoolIds[MAX_LLDP_APP_INFO_SIZING_ID];
    gLldpGlobalInfo.AppTlvPoolId =
        LLDPMemPoolIds[MAX_LLDP_APPL_TLV_COUNT_SIZING_ID];
    /*tLldCliSizingNeighInfo */
    gLldpGlobalInfo.LldpCliSizingNeighPoolId =
        LLDPMemPoolIds[MAX_LLDP_NEIGH_INFO_SIZING_ID];
    /*ChasisIdInfo */
    gLldpGlobalInfo.LldpArrayInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_ARRAY_SIZE_INFO_SIZING_ID];
    /*ManAddrOID */
    gLldpGlobalInfo.LldpManAddrOidPoolId =
        LLDPMemPoolIds[MAX_LLDP_LEN_MAN_OID_SIZING_ID];

    gLldpGlobalInfo.LocPortTablePoolId =
        LLDPMemPoolIds[MAX_LLDP_PORT_TABLE_SIZING_ID];

    gLldpGlobalInfo.LldpDestMacAddrPoolId =
        LLDPMemPoolIds[MAX_LLDP_DEST_MAC_ADDR_TABLE_SIZING_ID];
    gLldpGlobalInfo.LldpAgentToLocPortMappingPoolId =
        LLDPMemPoolIds[MAX_LLDP_AGENT_TO_LOC_PORT_TABLE_SIZING_ID];
    gLldpGlobalInfo.LocPortInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_LOC_PORT_TABLE_SIZING_ID];
    /* tLldpVlanNameTlvCounter */
    gLldpGlobalInfo.LldpVlanNameTlvCounter =
        LLDPMemPoolIds[MAX_LLDP_VLAN_NAME_TLV_COUNTER_SIZING_ID];
    /* tLldpMedLocNwPolicyInfo */
    gLldpGlobalInfo.LldpMedLocNwPolicyPoolId =
        LLDPMemPoolIds[MAX_LLDP_MED_LOC_NW_POLICY_TABLE_SIZING_ID];
    /* tLldpMedRemNwPolicyInfo */
    gLldpGlobalInfo.LldpMedRemNwPolicyPoolId =
        LLDPMemPoolIds[MAX_LLDP_MED_REM_NW_POLICY_TABLE_SIZING_ID];
    /* tLldpMedLocLocationInfo */
    gLldpGlobalInfo.LldpMedLocLocationPoolId =
        LLDPMemPoolIds[MAX_LLDP_MED_LOC_LOCATION_TABLE_SIZING_ID];
    /* tLldpMedRemLocationInfo */
    gLldpGlobalInfo.LldpMedRemLocationPoolId =
        LLDPMemPoolIds[MAX_LLDP_MED_REM_LOCATION_TABLE_SIZING_ID];
    gLldpGlobalInfo.LldpPduInfoPoolId =
        LLDPMemPoolIds[MAX_LLDP_PDU_COUNT_SIZING_ID];
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lldmain.c                      */
/*-----------------------------------------------------------------------*/
