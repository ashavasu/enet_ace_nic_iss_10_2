/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: lldport.c,v 1.33 2016/01/28 11:09:44 siva Exp $
 *
 * Description: This file contains the portable routines which calls APIs 
 *              given by other modules.
 ******************************************************************************/
#ifndef _LLDPORT_C_
#define _LLDPORT_C_

#include "lldinc.h"
#include "arMD5_api.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortCalculatePduMsgDigest
 *
 *    DESCRIPTION      : This function is used to calculate the Message
 *                       Digest of the Received LLDPDU.
 *
 *    INPUT            : pLldpdu - Pointer to the LLDPDU. this is a liner buf
 *                       u2MsgLen - Length of the received PDU
 *                       
 *    OUTPUT           : pu1RetValMsgDigest - Returns the calculated Message
 *                                            Digest value.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortCalculatePduMsgDigest (UINT1 *pu1Lldpdu, UINT2 u2MsgLen,
                               UINT1 *pu1RetValMsgDigest)
{
    Md5GetKeyDigest (pu1Lldpdu, u2MsgLen, pu1RetValMsgDigest);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetSysName
 *
 *    DESCRIPTION      : This is the portable routine to get the System Name.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : pu1RetValSysName - Return the System Name  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetSysName (UINT1 *pu1RetValSysName)
{
    UINT1               au1SysName[SNMP_MAX_OCTETSTRING_SIZE];

    MEMSET (&au1SysName, 0, SNMP_MAX_OCTETSTRING_SIZE);

#ifdef SNMP_3_WANTED
    SnmpGetSysName (&au1SysName[0]);
#endif

    MEMCPY (pu1RetValSysName, &au1SysName, LLDP_MAX_LEN_SYSNAME);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetSysDescr
 *
 *    DESCRIPTION      : This is the portable routine to get the System 
 *                       Description.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : pu1RetValSysDescr - Return the System Description  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetSysDescr (UINT1 *pu1RetValSysDescr)
{
    UINT1               au1SysDescr[SNMP_MAX_OCTETSTRING_SIZE];

    MEMSET (&au1SysDescr, 0, SNMP_MAX_OCTETSTRING_SIZE);
#ifdef SNMP_3_WANTED
    SnmpGetSysDescr (&au1SysDescr[0]);
#endif
    MEMCPY (pu1RetValSysDescr, &au1SysDescr, LLDP_MAX_LEN_SYSDESC);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetAgentCktId
 *
 *    DESCRIPTION      : This is the portable routine to get the Agent Circuit
 *                       ID.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : pu1RetValSysName - Return the System Name  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetAgentCktId (UINT4 u4PortNum, UINT4 *pu4RetValAgentCktId)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef DHCP_RLY_WANTED
    if (DhrlApiGetIfCiruitId ((INT4) u4PortNum, pu4RetValAgentCktId)
        != DHCP_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4PortNum);
    UNUSED_PARAM (pu4RetValAgentCktId);
#endif
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : LldpPortGetSysMacAddr
 *
 * Description        : This function is used by LLDP to get the 
 *                      Switch/System MAC address
 *
 * Input(s)           : BaseMacAddr - pointer to System MAC Address 
 *
 * Output(s)          : BaseMacAddr - System MAC Address
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
LldpPortGetSysMacAddr (tMacAddr BaseMacAddr)
{
    CfaGetSysMacAddress (BaseMacAddr);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetSysCapaSupported
 *
 *    DESCRIPTION      : This is the portable routine to get the System 
 *                       capability supported bitmap
 *
 *    INPUT            : None
 *
 *    OUTPUT           : pu2SysCapaSupported - Returns the Bitmap that
 *                       describe the system capabilities supported.
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
PUBLIC VOID
LldpPortGetSysCapaSupported (UINT1 *pu1SysCapaSupported)
{
#ifdef ISS_WANTED
    IssGetSysCapabilitiesSupported (pu1SysCapaSupported);
#else
    /* Set Capability ISS_SYS_CAPAB_OTHER */
    OSIX_BITLIST_SET_BIT (pu1SysCapaSupported, ISS_SYS_CAPAB_OTHER,
                          ISS_SYS_CAPABILITIES_LEN);
#endif
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetSysCapaEnabled
 *
 *    DESCRIPTION      : This is the portable routine to get the system
 *                       capabilities enabled bitmap.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : pu2SysCapaEnabled - Returns the bitmap that 
 *                        describe the system capabilities enabled
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
PUBLIC VOID
LldpPortGetSysCapaEnabled (UINT1 *pu1SysCapaEnabled)
{
#ifdef ISS_WANTED
    IssGetSysCapabilitiesEnabled (pu1SysCapaEnabled);
#else
    /* Set Capability ISS_SYS_CAPAB_OTHER */
    OSIX_BITLIST_SET_BIT (pu1SysCapaEnabled, ISS_SYS_CAPAB_OTHER,
                          ISS_SYS_CAPABILITIES_LEN);
#endif
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetIfOperStatus 
 *
 *    DESCRIPTION      : This is the portable routine to get the port
 *                       oper status.
 *
 *    INPUT            : u4LocPortNum - port number for which the oper status
 *                                      has to be obtained
 *                       pu1OperStatus- pointer to oper status              
 *
 *    OUTPUT           : pu1OperStatus- pointer to oper status 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetIfOperStatus (UINT4 u4LocPortNum, UINT1 *pu1OperStatus)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (L2IwfGetPortOperStatus (LLDP_MODULE, u4LocPortNum,
                                pu1OperStatus) != L2IWF_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : LldpPortGetIfAlias
 *
 *  Description       : This function is used by LLDP to update  the
 *                      Interface Alias(ifalias and ifname are same) of a
 *                      particular physical interface. This object(ifname) is
 *                      present in tCfaIfInfo itself, so by calling
 *                      LldpPortGetIfInfo we can update this object, but we
 *                      cant assume others also use ifname as ifalias, hence
 *                      this function(portable API) can be ported as required
 *                      by changing the CfaGetInterfaceNameFromIndex to new API.
 *
 * Input(s)           : u4IfIndex - Interface index
 *                      pu1IfAlias - pointer to Port interface alias
 *
 * Output(s)          : pu1IfAlias - pointer to Port interface alias 
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
LldpPortGetIfAlias (UINT4 u4IfIndex, UINT1 *pu1IfAlias)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    /* ifAlias and ifName are same, so get ifName as ifAlias */
    if (CfaCliGetIfName (u4IfIndex, (INT1 *) pu1IfAlias) != OSIX_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/******************************************************************************
 * Function Name      : LldpPortGetIfInfo
 *
 * Description        : This function is used by LLDP to get the following
 *                      Interface parameters of a particular physical interface.
 *                      1. MTU 2. Operational status 3. Hardware address.
 *                      4. Duplexity 5. Auto-Neg status
 *
 * Input(s)           : u4IfIndex - interface index for which information needs
 *                                  to be obtained
 *                      
 * Output(s)          : pCfaIfInfo - pointer to Port information structure
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
LldpPortGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pCfaIfInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    /* Get Interface related information */
    if (CfaGetIfInfo (u4IfIndex, pCfaIfInfo) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetAggStatus 
 *
 *    DESCRIPTION      : This function gets the aggregation status of the port
 *
 *    INPUT            : u4LocPortNum- port number for which agg status has
 *                                     to be obtained
 *                       pu1AggStatus - pointer to port agg status              
 *
 *    OUTPUT           : pu1AggStatus - pointer to port agg status
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetAggStatus (UINT4 u4LocPortNum, UINT1 *pu1AggStatus)
{
    UINT2               u2AggId = 0;

    if (L2IwfIsPortInPortChannel (u4LocPortNum) == L2IWF_SUCCESS)
    {
        *pu1AggStatus |= LLDP_AGG_CAP_BMAP;
        if (L2IwfGetPortChannelForPort ((UINT2) u4LocPortNum, &u2AggId)
            != L2IWF_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (L2IwfIsPortActiveInPortChannel (u4LocPortNum, u2AggId)
            == L2IWF_TRUE)
        {
            *pu1AggStatus |= LLDP_AGG_STATUS_BMAP;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetProtoVlanStatus
 *
 *    DESCRIPTION      : This function initializes the proto vlan status
 *
 *    INPUT            : u4LocPortNum- port number for which vlan status has
 *                                     to be obtained
 *                       pu1VlanStatus - pointer to port vlan status
 *
 *    OUTPUT           : pu1VlanStatus - pointer to port vlan status
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetProtoVlanStatus (UINT4 u4LocPortNum, UINT1 *pu1ProtoVlanStatus)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (VlanGetProtoVlanStatusOnPort (u4LocPortNum, pu1ProtoVlanStatus)
        != VLAN_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetPortVlanId
 *
 *    DESCRIPTION      : This function initializes the port vlan id
 *
 *    INPUT            : u4LocPortNum- port number for which vlan status has
 *                                     to be obtained
 *                       pu2PvId - pointer to port vlan id
 *
 *    OUTPUT           : pu2PvId - pointer to port vlan id
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetPortVlanId (UINT4 u4LocPortNum, UINT2 *pu2PvId)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (L2IwfGetVlanPortPvid ((UINT2) u4LocPortNum, pu2PvId) != L2IWF_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetProtoVlan
 *
 *    DESCRIPTION      : This function gets the Protocol VLANs associated 
 *                       with the port.
 *
 *    INPUT            : u4LocPortNum - Port number for which Protocol VLANs
 *                                      to be obtained.
 *
 *    OUTPUT           : pu2ConfProtoVlans - pointer to Protocol VLANs
 *                       pu1NumVlan - pointer to number of Protocol VLANs 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetProtoVlan (UINT4 u4LocPortNum, UINT2 *pu2ConfProtoVlans,
                      UINT1 *pu1NumVlan)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (VlanGetProtoVlanOnPort (u4LocPortNum, pu2ConfProtoVlans,
                                pu1NumVlan) != VLAN_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetProtoVlanCapab
 *
 *    DESCRIPTION      : This function gets port and protocol vlan support 
 *                       capability on the system
 *
 *    INPUT            : None
 *
 *    OUTPUT           : pu1ProtoVlanCapab - pointer to ProtoVlanCapabilities 
 *                                           flag
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
LldpPortGetProtoVlanCapab (UINT1 *pu1ProtoVlanCapab)
{
    *pu1ProtoVlanCapab = OSIX_FALSE;
    if (VlanIsProtocolVlanSupported () == VLAN_TRUE)
    {
        *pu1ProtoVlanCapab = OSIX_TRUE;
        return;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetIpv4Addr 
 *
 *    DESCRIPTION      : This function gets the Ipv4Addr based on the 
 *                       interface index
 *
 *    INPUT            : u4IfIndex - interface index
 *
 *    OUTPUT           : pu4Ipv4Addr - pointer to NetIpv4 address
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetIpv4Addr (UINT4 u4IfIndex, UINT4 *pu4Ipv4Addr)
{
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4               u4Port = 0;

    MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));

    /* get the corresponding layer3 port number for the given layer2 ifindex */
    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) != NETIPV4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) != NETIPV4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    *pu4Ipv4Addr = NetIpv4IfInfo.u4Addr;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetIfIndexFromPort
 *
 *    DESCRIPTION      : This function gets the layer2 interface index
 *                       based on the given layer3 interface port number.
 *
 *    INPUT            : u4Port - layer3 port number
 *
 *    OUTPUT           : pu4IfIndex - pointer to layer2 interface index
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    /* get the corresponding layer2 ifindex for the given layer3 port number */
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, pu4IfIndex) != NETIPV4_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetOidforIfIndex 
 *
 *    DESCRIPTION      : This function returns the OID for the ifmib object
 *                       "ifIndex". 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : pRetValOidStr - OID value of obj ifIndex in 
 *                                       tSNMP_OID_TYPE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetOidforIfIndex (tLldpManAddrOid * pRetValOidStr)
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *pOid = NULL;

    if ((pOid = alloc_oid (SNMP_TRAP_OID_LEN)) == NULL)
    {
        return OSIX_FAILURE;
    }

    if (CfaApiGetOIDFromObjName ((UINT1 *) "ifIndex", pOid) != CFA_SUCCESS)
    {
        SNMP_FreeOid (pOid);
        return OSIX_FAILURE;
    }

    if (pOid->u4_Length > LLDP_MAX_LEN_MAN_OID)
    {
        pOid->u4_Length = LLDP_MAX_LEN_MAN_OID;
    }

    pRetValOidStr->u4_Length = pOid->u4_Length;
    MEMCPY (pRetValOidStr->au4_OidList, pOid->pu4_OidList,
            pRetValOidStr->u4_Length * sizeof (UINT4));

    SNMP_FreeOid (pOid);

#else
    UNUSED_PARAM (pRetValOidStr);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  LldpPortGetDefaultRouterIfIndex
 *
 *    DESCRIPTION      : This function returns the OID for the ifmib object
 *                       "ifIndex". 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None 
 *
 *    RETURNS          : NULL / OID value of obj ifIndex in tSNMP_OID_TYPE 
 *                      format
 *
 ****************************************************************************/
PUBLIC VOID
LldpPortGetDefaultRouterIfIndex (UINT4 *pu4DefIfIndex)
{
    *pu4DefIfIndex = CfaGetDefaultRouterIfIndex ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortNotifyFaults 
 *
 *    DESCRIPTION      : This function Sends the trap message to the Fault 
 *                       Manager.
 *
 *    INPUT            : tSNMP_VAR_BIND * - pointer the trapmessage.
 *                       pu1Msg  *        - Pointer to the log message.
 *                       u4ModuleId       - ModuleId
 *                       
 *    OUTPUT           : Calls the FmApiNotifyFaults to send the
 *                       Trap message to FaultManager.
 *
 *    RETURNS          : None
 *
 ****************************************************************************/

PUBLIC VOID
LldpPortNotifyFaults (tSNMP_VAR_BIND * pTrapMsg, UINT1 *pu1Msg,
                      UINT4 u4ModuleId)
{
    tFmFaultMsg         FmFaultMsg;
    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = u4ModuleId;
#ifdef FM_WANTED
    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        SNMP_free_snmp_vb_list (pTrapMsg);
        LLDP_TRC (ALL_FAILURE_TRC, "LldpPortNotifyFaults: Sending "
                  "Notfication To FM is Failed");
    }
#endif
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortGetNextValidPort
 *
 *    DESCRIPTION      : This function gets the next valid interface index 
 *                       from L2IWF
 *
 *    INPUT            : u2PrevPort - previous port number 
 *
 *    OUTPUT           : pu2PortIndex - pointer to next inerface index
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortGetNextValidPort (UINT2 u2PrevPort, UINT2 *pu2PortIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (L2IwfGetNextValidPort (u2PrevPort, pu2PortIndex) != L2IWF_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortValidateIfIndex
 *
 *    DESCRIPTION      : This function validates the given interface index.
 *
 *    INPUT            : u4PortIndex - Interface Index to be validated
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortValidateIfIndex (UINT4 u4PortIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (CfaValidateIfIndex (u4PortIndex) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return (i4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortRegisterWithIp
 *
 *    DESCRIPTION      : This function register with Ip module (Ipv4 and Ipv6)
 *                       the given interface index.
 *                       LldpApiNotifyIpv4IfStatusChange and
 *                       LldpApiNotifyIpv6IfStatusChange APIs are used for this
 *                       registration.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpPortRegisterWithIp (VOID)
{
    tNetIpRegInfo       NetRegInfo;
    UINT4               u4Mask = 0;

    /* Register with IPv4 */
    MEMSET ((UINT1 *) &NetRegInfo, 0, sizeof (tNetIpRegInfo));
    NetRegInfo.pIfStChng = (VOID *) LldpApiNotifyIpv4IfStatusChange;
    NetRegInfo.u2InfoMask = NETIPV4_IFCHG_REQ;
    NetRegInfo.u1ProtoId = LLDP_ID;    /* As LLDP module does not run
                                       above IP module. So there is no
                                       valid protocol Id present to 
                                       register with IP. here one
                                       unused protocol id is used */
    NetRegInfo.pRtChng = NULL;
    NetRegInfo.pProtoPktRecv = NULL;

    if (NetIpv4RegisterHigherLayerProtocol (&NetRegInfo) == NETIPV4_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Failed to register with IPv4 module.\r\n");
        return OSIX_FAILURE;
    }

#ifdef IP6_WANTED
    u4Mask = NETIPV6_ADDRESS_CHANGE;
    if (NetIpv6RegisterHigherLayerProtocol (LLDP_ID, u4Mask,
                                            LldpApiNotifyIpv6IfStatusChange)
        == NETIPV6_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Failed to register with IPv6 module.\r\n");
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4Mask);
#endif /* for IP6_WANTED */

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpPortDeRegisterWithIp
 *
 *    DESCRIPTION      : This function de-register with Ip module (Ipv4 and Ipv6)
 *                       the given interface index.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortDeRegisterWithIp (VOID)
{
    /* De-register with IPv4 */
    NetIpv4DeRegisterHigherLayerProtocolInCxt (LLDP_ZERO, LLDP_ID);

#ifdef IP6_WANTED
    /* De-register with IPv6 */
    if (NetIpv6DeRegisterHigherLayerProtocol (LLDP_ID) == NETIPV6_FAILURE)
    {
        LLDP_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Failed to de-register with IPv6 module.\r\n");
        return OSIX_FAILURE;
    }
#endif /* for IP6_WANTED */

    LLDP_TRC (CONTROL_PLANE_TRC, "Successful de-registered with "
              "IP module\r\n");
    return OSIX_SUCCESS;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : LldpPortHandleOutgoingPkt
 *
 *     DESCRIPTION      : This function handles the outgoing paket in LLDP.
 *
 *     INPUT            : pPreFormedLldpdu  - pointer to prefromed buffer
 *                        u4PortNum         - Port Number
 *                        u4FrameSize       - maximum frame size
 *
 *     OUTPUT           :  None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpPortHandleOutgoingPkt (tCRU_BUF_CHAIN_HEADER * pPktToSend,
                           UINT4 u4LocPortNum, UINT4 u4FrameSize)
{

    if (L2IwfHandleOutgoingPktOnPort (pPktToSend, (UINT2) u4LocPortNum,
                                      u4FrameSize, LLDP_ENET_TYPE,
                                      CFA_ENCAP_NONE) != L2IWF_SUCCESS)
    {
        LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "LldpPortHandleOutgoingPkt:  L2IwfHandleOutgoingPktOnPort"
                  "returned Failure!!!\r\n");
        return OSIX_FAILURE;
    }
    LLDP_TRC_ARG1 (CONTROL_PLANE_TRC, "LldpPortHandleOutgoingPkt: LLDP Frame "
                   "is successfully transmitted on Port: %u.\r\n",
                   u4LocPortNum);
    LLDP_PKT_DUMP (DUMP_TRC, pPktToSend, u4FrameSize,
                   " Dumping Transmitted LLDP FRAME :\n");

    return OSIX_SUCCESS;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : LldpPortDownIndication
 *
 *     DESCRIPTION      : In ISS, CFA processes the oper down indication 
 *                        and then notifies the  oper state change to all
 *                        higher layers. As per LLDP standard, when the oper
 *                        down indication is received from CFA, LLDP sends a
 *                        shutdown frame on that port to make the remote
 *                        system ageout the learnt informations and then send
 *                        the notification to CFA about the completion of
 *                        processing the oper down notification from CFA
 *
 *     INPUT            : u4IfIndex         - Port Number
 *
 *     OUTPUT           :  None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpPortDownProcessedIndication (UINT4 u4IfIndex)
{
    /* Indication to lower layers should be coded here */
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/**************************************************************************
 *
 *     FUNCTION NAME    : LldpPortValidatePortIdSubType
 *
 *     DESCRIPTION      : This function validates the given port id subtype.
 *                        In our implementation, configuing the PortIdSubType 
 *                        as NW_ADDR & SUB_AGENT_CKT_ID is not allowed. 
 *
 *                        Reason: All physical interfaces (L2 interfaces) are 
 *                        visible to LLDP. To configure the PortIdSubType as 
 *                        NW_ADDR and SUB_AGENT_CKT_ID, the interface should be
 *                        a L3 interface. When the interface is configured as 
 *                        L3 interface, it is removed form L2 modules including
 *                        LLDP. So it cannot be tested in our implementation 
 *                        and hence given a seperate portable API which can be 
 *                        ported to return SUCCESS if 
 *                        NW_ADDR & SUB_AGENT_CKT_ID are configured.
 *
 *     INPUT            : i4PortIdSubType - port id subtype
 *
 *     OUTPUT           :  None
 *
 *     RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
LldpPortValidatePortIdSubType (INT4 i4PortIdSubType)
{
    if ((i4PortIdSubType < LLDP_PORT_ID_SUB_IF_ALIAS) ||
        (i4PortIdSubType > LLDP_PORT_ID_SUB_LOCAL))
    {
        LLDP_TRC (ALL_FAILURE_TRC, "LldpPortValidatePortIdSubType: Invalid "
                  "port id subtype\r\n");
        return OSIX_FAILURE;
    }

    /* LLDP advertises only Layer2 information and it doesnt advertise
     * Layer3 information. Network address and Agent Circuit ID are Layer3
     * information, so these information are not advertised by LLDP.*/
    if ((i4PortIdSubType == LLDP_PORT_ID_SUB_NW_ADDR) ||
        (i4PortIdSubType == LLDP_PORT_ID_SUB_AGENT_CKT_ID))
    {
        LLDP_TRC_ARG1 (ALL_FAILURE_TRC, "LldpPortValidatePortIdSubType:"
                       "Port id subtype %d is not supported\r\n",
                       i4PortIdSubType);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : LldpPortRegisterWithRM                               
 *                                                                           
 * DESCRIPTION        : This function registers LLDP with RM                 
 *                                                                           
 * INPUT              : NONE 
 *                                                                           
 * OUTPUT             : NONE                                                 
 *                                                                           
 * RETURNS            : NONE                            
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
LldpPortRegisterWithRM (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_LLDP_APP_ID;
    RmRegParams.pFnRcvPkt = LldpRedRcvPktFromRm;

    if (RmRegisterProtocols (&RmRegParams) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
#endif
    return i4RetVal;
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : LldpPortDeRegisterWithRM 
 *                                                                           
 * DESCRIPTION        : This function DeRegisters/UnRegisters LLDP with RM                 
 *                                                                           
 * INPUT              : RM_LLDP_APP_ID - LLDP application ID   
 *                                                                           
 * OUTPUT             : NONE                                                 
 *                                                                           
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
LldpPortDeRegisterWithRM (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmDeRegisterProtocols (RM_LLDP_APP_ID) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
#endif
    return i4RetVal;
}

#ifdef L2RED_WANTED
/*****************************************************************************
 *
 * FUNCTION NAME      : LldpPortRelRmMsgMem 
 * 
 * DESCRIPTION        : This function calls the RM API to release the memory
 *                      allocated for RM message.
 *
 * INPUT              : pu1Block - Memory block 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
LldpPortRelRmMsgMem (UINT1 *pu1Block)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmReleaseMemoryForMsg (pu1Block) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : LldpPortEnqMsgToRm 
 * 
 * DESCRIPTION        : This function calls the RM Module to enqueue the
 *                      message from applications to RM task.
 *
 * INPUT              : pRmMsg - msg from application(LLDP)
 *                      u2DataLen - Len of msg
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpPortEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    UINT4               u4SrcEntId = (UINT4) RM_LLDP_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_LLDP_APP_ID;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId)
        != RM_SUCCESS)
    {
        LLDP_TRC (LLDP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "LldpPortEnqMsgToRm:Enqueuing sync up message to "
                  "RM failed\r\n");
        /* memory allocated for pRmMsg is freed here, only in failure case. 
         * In success case RM will free the memory */
        RM_FREE (pRmMsg);
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : LldpPortGetRmNodeState
 * 
 * DESCRIPTION        : This function gets the node state from RM
 *
 * INPUT              : NONE 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : UINT4 - Node status 
 *                                                                           
 ****************************************************************************/
PUBLIC UINT4
LldpPortGetRmNodeState (VOID)
{
    return RmGetNodeState ();
}

/*****************************************************************************
 *
 * FUNCTION NAME      : LldpPortGetStandbyNodeCount 
 * 
 * DESCRIPTION        : This function gets the standby node count from RM
 *
 * INPUT              : NONE 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : UINT1 - Standby node count 
 *                                                                           
 ****************************************************************************/
PUBLIC UINT1
LldpPortGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************
 *
 * FUNCTION NAME      : LldpPortSetBulkUpdateStatus 
 * 
 * DESCRIPTION        : This function sets LLDP bulk update status in RM
 *                      
 * INPUT              : NONE
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
LldpPortSetBulkUpdateStatus (VOID)
{
    RmSetBulkUpdatesStatus (RM_LLDP_APP_ID);

    return;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : LldpPortSendEventToRm 
 * 
 * DESCRIPTION        : This function posts an event to RM
 *                      
 * INPUT              : u1Event - Event to be sent to RM. And the event can be
 *                      any of the following,
 *                      RM_PROTOCOL_BULK_UPDT_COMPLETION
 *                      RM_BULK_UPDT_ABORT
 *                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
 *                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED
 *                      RM_STANDBY_EVT_PROCESSED
 *
 *                      u1Error - In case the event is RM_BULK_UPDATE_ABORT, 
 *                      the reason for the failure is send in the error. And 
 *                      the error can be any of the following,
 *                      RM_MEMALLOC_FAIL
 *                      RM_SENTO_FAIL
 *                      RM_PROCESS_FAIL
 *                      
 * OUTPUT             : pEvt - pointer to protocol event structure 
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                           
 ****************************************************************************/
PUBLIC INT4
LldpPortSendEventToRm (tRmProtoEvt * pEvt)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmApiHandleProtocolEvent (pEvt) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

#endif /* L2RED_WANTED */
/******************************************************************************
 *    Function Name       : LldpNotifyLocSysInfoChg                           *
 *                                                                            *
 *    Description         : This routine is used to send notification to      *
 *                          other modules like ECFM about any change          *
 *                          in port-id or chassis-id.                         *
 *                                                                            *
 *    Input(s)            : u1InfoType - Type of information to be updated    *
 *                                       port-id or chassis-id                *
 *                          u4IfIndex - Interface index of the port.          *
 *                          i4IdSubType - Port-id/Chassis-id subtype.         *
 *                          pu1Id - Pointer to port-id/chassis-id             *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns             : None                                              * 
 *****************************************************************************/
PUBLIC VOID
LldpNotifyLocSysInfoChg (UINT1 u1InfoType, UINT4 u4IfIndex, INT4 i4IdSubType,
                         UINT1 *pu1Id)
{
#ifdef ECFM_WANTED
    UINT2               u2IdLen = 0;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IdSubType);
    UNUSED_PARAM (pu1Id);
#endif
    switch (u1InfoType)
    {
        case LLDP_UPDATE_PORTID:
#ifdef ECFM_WANTED
            LldpUtilGetPortIdLen (i4IdSubType, pu1Id, &u2IdLen);
            if (LldpUtilGetPortIdLen (i4IdSubType, pu1Id, &u2IdLen) !=
                OSIX_SUCCESS)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "LldpNotifyLocSysInfoChg:  LldpUtilGetPortIdLen"
                          " returned Failure!!!\r\n");
                return;
            }
            if (EcfmUpdateLocalSysInfo (ECFM_UPDATE_PORTID,
                                        u4IfIndex,
                                        (UINT1) i4IdSubType,
                                        pu1Id, u2IdLen) != ECFM_SUCCESS)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "LldpNotifyLocSysInfoChg:  EcfmUpdateLocalSysInfo"
                          " returned Failure for port-id!!!\r\n");
            }
#endif
            break;
        case LLDP_UPDATE_CHASSISID:
#ifdef ECFM_WANTED
            LldpUtilGetPortIdLen (i4IdSubType, pu1Id, &u2IdLen);
            if (LldpUtilGetChassisIdLen (i4IdSubType, pu1Id, &u2IdLen) !=
                OSIX_SUCCESS)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "LldpNotifyLocSysInfoChg:  LldpUtilGetChassisIdLen"
                          " returned Failure!!!\r\n");
                return;
            }
            if (EcfmUpdateLocalSysInfo (ECFM_UPDATE_CHASSISID,
                                        u4IfIndex,
                                        (UINT1) i4IdSubType,
                                        pu1Id, u2IdLen) != ECFM_SUCCESS)
            {
                LLDP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "LldpNotifyLocSysInfoChg:  EcfmUpdateLocalSysInfo"
                          " returned Failure for cahssis-id!!!\r\n");
            }
#endif
            break;
        default:
            return;
    }
    return;
}

/******************************************************************************
 *    Function Name       : LldpPortGetPortsForPortChannel                    *
 *                                                                            *
 *    Description         : This routines returns the port channel member     *
 *                          ports.
 *                                                                            *
 *    Input(s)            : u2AggId - Port channel ID                         *
 *                          
 *    Output(s)           : pu2MemberPorts - Pointer to array of LAG member   *
 *                                           ports                            *
 *                          pu2NumPorts - Number of ports in LAG
 *                                                                            *
 *    Returns             : None                                              * 
 *****************************************************************************/
PUBLIC VOID
LldpPortGetPortsForPortChannel (UINT2 u2AggId, UINT2 *pu2MemberPorts,
                                UINT2 *pu2NumPorts)
{
    L2IwfGetConfiguredPortsForPortChannel (u2AggId, pu2MemberPorts,
                                           pu2NumPorts);
}

/******************************************************************************
 *    Function Name       : LldpPortSendReRegToApp                            *
 *                                                                            *
 *    Description         : This routines calls L2IWF function to post        *
 *                          re-registration event to all the applications     *
 *                          registered before LLDP has booted up              *    
 *                                                                            *
 *    Input(s)            : None                                              *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns             : None                                              * 
 *****************************************************************************/
PUBLIC VOID
LldpPortSendReRegToApp (VOID)
{
    L2IwfLldpPostApplReReg ();
}

/*****************************************************************************/
/* Function Name      : LldpPortIsPortInPortChannel                          */
/*                                                                           */
/* Description        : This routine checks whether the port is a member of  */
/*                      any Port Channel and if so returns SUCCESS.          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
LldpPortIsPortInPortChannel (UINT4 u4IfIndex)
{
    if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : LldpPortGetPortChannelForPort                        */
/*                                                                           */
/* Description        : This routine returns the Port Channel of which the   */
/*                      given port is a member. It accesses the L2Iwf common */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : pPortInfo - Pointer to port table                    */
/*                                                                           */
/* Output(s)          : u2AggId     - Port Channel Index                     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

PUBLIC VOID
LldpPortGetPortChannelForPort (tLldpLocPortInfo * pPortInfo, UINT4 *pu4AggId)
{
    UINT2               u2AggId = 0;

    L2IwfGetPortChannelForPort (pPortInfo->u4LocPortNum, &u2AggId);

    if (L2IWF_TRUE ==
        L2IwfIsPortActiveInPortChannel (pPortInfo->u4LocPortNum, u2AggId))
    {
        *pu4AggId = (UINT2) u2AggId;
    }
    else
    {
        /* If the recevied port is not active in port channel then
         * reset the pending app flag so that once the port becomes
         * active, TLV will be sent to the application */
        pPortInfo->bIsAnyPendingApp = OSIX_TRUE;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : LLdpLaIsPortInIcclIf                                 */
/*                                                                           */
/* Description        : This routine checks whether the port is a member     */
/*                      of ICCL interface.                                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index of port                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
LLdpLaIsPortInIcclIf (UINT4 u4IfIndex)
{

#ifdef LA_WANTED
    return (LaApiIsMemberofICCL (u4IfIndex));
#else
    UNUSED_PARAM (u4IfIndex);
    return OSIX_FAILURE;
#endif
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedPortGetInventoryInfo
 *
 *    DESCRIPTION      : This is the portable routine to get the physical 
 *                       entity device information from the entity module that
 *                       has to be send in LLDP-MED Inventory TLV
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : pInventoryInfo - Returns the local device Inventory
 *                       Info  
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedPortGetInventoryInfo (tLldpMedLocInventoryInfo *pInventoryInfo)
{

    EntApiGetInventoryInfo(pInventoryInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedPortGetPSEPowerInfo
 *
 *    DESCRIPTION      : This is the portable routine to get the power 
 *                       information of the particular port from the entity 
 *                       module that has to be send in LLDP-MED Power-via-MDI TLV
 *
 *    INPUT            : None
 *
 *    OUTPUT           : u2PsePowerAv - Power value
 *                       u1PsePowerPriority - Power Priority
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedPortGetPSEPowerInfo (tLldpLocPortTable *pPortEntry)
{
    MEMSET(&pPortEntry->MedPSEPowerInfo, 0, sizeof(tLldpMedLocPSEPowerInfo));

    pPortEntry->MedPSEPowerInfo.u2PsePowerAv = (UINT2) LLDP_MED_POWER_VALUE;
    pPortEntry->MedPSEPowerInfo.u1PsePowerPriority = LLDP_MED_PRI_CRITICAL;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpMedPortGetPoEPowerInfo
 *
 *    DESCRIPTION      : Function that returns the PoE Power Information
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : u1MedLocPSEPowerSource - Power Source
 *                       u1MedLocPoEDeviceType - Power Device Type
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpMedPortGetPoEPowerInfo (UINT1 *pu1PSEPowerSource, UINT1 *pu1PoEDeviceType)
{
    /* LLDP-MED Power source Information */
    *pu1PSEPowerSource = LLDP_MED_DEFAULT_PSE_SRC;
    /* LLDP-MED power Device Type */
    *pu1PoEDeviceType =  LLDP_MED_DEFAULT_DEVICE;
    return OSIX_SUCCESS;
}

#endif /* _LLDPORT_C_ */
/*---------------------------------------------------------------------------*/
/*                        End of the file  <lldport.c>                       */
/*---------------------------------------------------------------------------*/
