/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslldmlw.c,v 1.6 2016/02/23 10:05:11 siva Exp $
*
* Description: LLDP-MED Low Level Routines
*********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "lldinc.h"

/* LOW LEVEL Routines for Table : FsLldpMedPortConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpMedPortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpMedPortConfigTable(INT4
        i4LldpV2PortConfigIfIndex , UINT4 u4LldpV2PortConfigDestAddressIndex) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    /*Get Destination MacAddress from Destination MacAddressIndex */
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                Lldpv2DestMacAddrTblRBTree,
                (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Call Validate function in Util as it is commaon for both standard and
     * proprietary */
    if (LldpMedUtlValidateIndexPortConfTbl
            (i4LldpV2PortConfigIfIndex, pDestTbl->Lldpv2DestMacAddress) !=
            OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpMedPortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpMedPortConfigTable(INT4 *pi4LldpV2PortConfigIfIndex
        , UINT4 *pu4LldpV2PortConfigDestAddressIndex) 
{
    if ((LldpMedUtlGetFirstIndexPortConfTbl
                (pi4LldpV2PortConfigIfIndex,
                 pu4LldpV2PortConfigDestAddressIndex) != OSIX_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpMedPortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpMedPortConfigTable(INT4 i4LldpV2PortConfigIfIndex
        ,INT4 *pi4NextLldpV2PortConfigIfIndex  , UINT4
        u4LldpV2PortConfigDestAddressIndex ,UINT4
        *pu4NextLldpV2PortConfigDestAddressIndex ) 
{
    if (LldpMedUtlGetNextIndexPortConfTbl (i4LldpV2PortConfigIfIndex, 
                pi4NextLldpV2PortConfigIfIndex, 
                u4LldpV2PortConfigDestAddressIndex, 
                pu4NextLldpV2PortConfigDestAddressIndex) !=
            OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpMedPortCapSupported
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValFsLldpMedPortCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedPortCapSupported(INT4 i4LldpV2PortConfigIfIndex , UINT4
        u4LldpV2PortConfigDestAddressIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpMedPortCapSupported) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    /* Get Destination MacAddress using Destination MacAddressIndex */
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                Lldpv2DestMacAddrTblRBTree,
                (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get Capability Supported information in util as it is common for
     * both standard and proprietary */
    if (LldpMedUtlGetLocPortCapSupported(i4LldpV2PortConfigIfIndex,
                pDestTbl->Lldpv2DestMacAddress,
                pRetValFsLldpMedPortCapSupported) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValFsLldpMedPortConfigTLVsTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedPortConfigTLVsTxEnable(INT4 i4LldpV2PortConfigIfIndex ,
        UINT4 u4LldpV2PortConfigDestAddressIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpMedPortConfigTLVsTxEnable) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    /* Get Destination MacAddress using Destination MacAddressIndex */
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                Lldpv2DestMacAddrTblRBTree,
                (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpMedUtlGetLldpPortConfigTLVsTxEnable
            (i4LldpV2PortConfigIfIndex, pDestTbl->Lldpv2DestMacAddress,
             pRetValFsLldpMedPortConfigTLVsTxEnable) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpMedPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValFsLldpMedPortConfigTLVsTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedPortConfigTLVsTxEnable(INT4 i4LldpV2PortConfigIfIndex ,
        UINT4 u4LldpV2PortConfigDestAddressIndex , tSNMP_OCTET_STRING_TYPE
        *pSetValFsLldpMedPortConfigTLVsTxEnable) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                Lldpv2DestMacAddrTblRBTree,
                (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                "nmhSetFsLldpMedPortConfigTLVsTxEnable: "
                "Destination MacAddr Node is not present\r\n");
        return SNMP_FAILURE;
    }

    /* Set the value for Capabilities Enabled Tlv */
    if (LldpMedUtlSetPortConfigTLVsTxEnable
            (i4LldpV2PortConfigIfIndex, pDestTbl->Lldpv2DestMacAddress,
             pSetValFsLldpMedPortConfigTLVsTxEnable) != OSIX_SUCCESS)
    {
        LLDP_TRC (ALL_FAILURE_TRC | MGMT_TRC,
                "nmhSetFsLldpMedPortConfigTLVsTxEnable: "
                "Unable to set Enabled Tlvs \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpMedPortConfigTLVsTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValFsLldpMedPortConfigTLVsTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedPortConfigTLVsTxEnable(UINT4 *pu4ErrorCode , INT4
        i4LldpV2PortConfigIfIndex , UINT4 u4LldpV2PortConfigDestAddressIndex ,
        tSNMP_OCTET_STRING_TYPE *pTestValFsLldpMedPortConfigTLVsTxEnable) 
{

    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhTestv2FsLldpMedPortConfigTLVsTxEnable: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    DestTbl.u4LlldpV2DestAddrTblIndex = u4LldpV2PortConfigDestAddressIndex;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                Lldpv2DestMacAddrTblRBTree,
                (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        LLDP_TRC (ALL_FAILURE_TRC, " nmhTestv2FsLldpMedPortConfigTLVsTxEnable:"
                  "Destination MacAddr Node is not present\r\n");
        return SNMP_FAILURE;
    }

    /* Call LldpMedUtlTestPortConfigTLVsTxEnable for testing the
     * Ifindex, DestinationAddressIndex 
       and other conditions */
    if (LldpMedUtlTestPortConfigTLVsTxEnable
            (pu4ErrorCode, i4LldpV2PortConfigIfIndex,
             pDestTbl->Lldpv2DestMacAddress,
             pTestValFsLldpMedPortConfigTLVsTxEnable ) != OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpMedPortConfigTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsLldpMedPortConfigTable(UINT4 *pu4ErrorCode, tSnmpIndexList
        *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind) 
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpMedLocMediaPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpMedLocMediaPolicyTable(INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType) 
{
    if (LldpMedUtlValIndexLocNwPolTbl (i4LldpV2LocPortIfIndex, 
                pFsLldpMedLocMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpMedLocMediaPolicyTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpMedLocMediaPolicyTable(INT4 *pi4LldpV2LocPortIfIndex
        , tSNMP_OCTET_STRING_TYPE * pFsLldpMedLocMediaPolicyAppType) 
{
    if (LldpMedUtlGetFirstIndexLocNwPolTbl (pi4LldpV2LocPortIfIndex,
                pFsLldpMedLocMediaPolicyAppType) != OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpMedLocMediaPolicyTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType
                nextFsLldpMedLocMediaPolicyAppType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpMedLocMediaPolicyTable(INT4 i4LldpV2LocPortIfIndex
        ,INT4 *pi4NextLldpV2LocPortIfIndex  , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType ,tSNMP_OCTET_STRING_TYPE *
        pNextFsLldpMedLocMediaPolicyAppType ) 
{
    if (LldpMedUtlGetNextIndexLocNwPolTbl (i4LldpV2LocPortIfIndex,
                pi4NextLldpV2LocPortIfIndex,
                pFsLldpMedLocMediaPolicyAppType,
                pNextFsLldpMedLocMediaPolicyAppType) !=
            OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpMedLocMediaPolicyVlanID
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                retValFsLldpMedLocMediaPolicyVlanID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocMediaPolicyVlanID(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        *pi4RetValFsLldpMedLocMediaPolicyVlanID) 
{
    if (LldpMedUtlGetLocNwPolVlanID(i4LldpV2LocPortIfIndex,
                            pFsLldpMedLocMediaPolicyAppType,
                   pi4RetValFsLldpMedLocMediaPolicyVlanID) !=
        OSIX_SUCCESS)
    {
         return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedLocMediaPolicyPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                retValFsLldpMedLocMediaPolicyPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocMediaPolicyPriority(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        *pi4RetValFsLldpMedLocMediaPolicyPriority) 
{
    if (LldpMedUtlGetLocNwPolPriority(i4LldpV2LocPortIfIndex,
                             pFsLldpMedLocMediaPolicyAppType,
                  pi4RetValFsLldpMedLocMediaPolicyPriority) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedLocMediaPolicyDscp
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                retValFsLldpMedLocMediaPolicyDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocMediaPolicyDscp(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        *pi4RetValFsLldpMedLocMediaPolicyDscp) 
{
    if (LldpMedUtlGetLocNwPolDscp(i4LldpV2LocPortIfIndex,
                          pFsLldpMedLocMediaPolicyAppType,
                     pi4RetValFsLldpMedLocMediaPolicyDscp)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedLocMediaPolicyUnknown
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                retValFsLldpMedLocMediaPolicyUnknown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocMediaPolicyUnknown(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        *pi4RetValFsLldpMedLocMediaPolicyUnknown) 
{
    if (LldpMedUtlGetLocNwPolUnknown(i4LldpV2LocPortIfIndex,
                            pFsLldpMedLocMediaPolicyAppType,
                     pi4RetValFsLldpMedLocMediaPolicyUnknown)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsLldpMedLocMediaPolicyTagged
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                retValFsLldpMedLocMediaPolicyTagged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocMediaPolicyTagged(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        *pi4RetValFsLldpMedLocMediaPolicyTagged) 
{
    if (LldpMedUtlGetLocNwPolTagged(i4LldpV2LocPortIfIndex,
                            pFsLldpMedLocMediaPolicyAppType,
                     pi4RetValFsLldpMedLocMediaPolicyTagged)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedLocMediaPolicyRowStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                retValFsLldpMedLocMediaPolicyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocMediaPolicyRowStatus(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        *pi4RetValFsLldpMedLocMediaPolicyRowStatus) 
{
    if (LldpMedUtlGetLocNwPolRowStatus(i4LldpV2LocPortIfIndex,
                              pFsLldpMedLocMediaPolicyAppType,
                     pi4RetValFsLldpMedLocMediaPolicyRowStatus)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpMedLocMediaPolicyVlanID
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                setValFsLldpMedLocMediaPolicyVlanID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocMediaPolicyVlanID(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        i4SetValFsLldpMedLocMediaPolicyVlanID) 
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    INT4 i4VlanId = 0;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType = *(pFsLldpMedLocMediaPolicyAppType
            ->pu1_OctetList);

    pLldpMedLocPolicy = 
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) & LldpMedLocPolicy);
    if (pLldpMedLocPolicy == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhSetFsLldpMedLocMediaPolicyVlanID: "
                "Entry is not present  \r\n");
        return SNMP_FAILURE;
    }

    i4VlanId = (INT4)pLldpMedLocPolicy->u2LocPolicyVlanId;
    if (i4SetValFsLldpMedLocMediaPolicyVlanID == i4VlanId)
    {
        return SNMP_SUCCESS;
    }
    pLldpMedLocPolicy->u2LocPolicyVlanId = 
        (UINT2)i4SetValFsLldpMedLocMediaPolicyVlanID;

    /* Reset Policy Unknown flag and Tagged flag if Vlan ID is configured */
    pLldpMedLocPolicy->bLocPolicyUnKnown = 0;
    pLldpMedLocPolicy->bLocPolicyTagged = LLDP_MED_VLAN_TAGGED;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsLldpMedLocMediaPolicyPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                setValFsLldpMedLocMediaPolicyPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocMediaPolicyPriority(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        i4SetValFsLldpMedLocMediaPolicyPriority) 
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    INT4 i4Priority = 0;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType = *(pFsLldpMedLocMediaPolicyAppType
            ->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) & LldpMedLocPolicy);
    if (pLldpMedLocPolicy == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhSetFsLldpMedLocMediaPolicyPriority: "
                "Entry is not present  \r\n");
        return SNMP_FAILURE;
    }

    i4Priority = (INT4)pLldpMedLocPolicy->u1LocPolicyPriority;
    if (i4SetValFsLldpMedLocMediaPolicyPriority == i4Priority)
    {
        return SNMP_SUCCESS;
    }

    pLldpMedLocPolicy->u1LocPolicyPriority =
        (UINT1)i4SetValFsLldpMedLocMediaPolicyPriority;
    /* Reset Policy Unknown flag and Tagged flag if Priority is configured */
    pLldpMedLocPolicy->bLocPolicyUnKnown = 0;
    pLldpMedLocPolicy->bLocPolicyTagged = LLDP_MED_VLAN_TAGGED;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsLldpMedLocMediaPolicyDscp
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                setValFsLldpMedLocMediaPolicyDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocMediaPolicyDscp(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        i4SetValFsLldpMedLocMediaPolicyDscp) 
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    INT4 i4Dscp = 0;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType = *(pFsLldpMedLocMediaPolicyAppType
            ->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) & LldpMedLocPolicy);
    if (pLldpMedLocPolicy == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhSetFsLldpMedLocMediaPolicyDscp: "
                "Entry is not present  \r\n");
        return SNMP_FAILURE;
    }

    i4Dscp = (INT4)pLldpMedLocPolicy->u1LocPolicyDscp;
    if (i4SetValFsLldpMedLocMediaPolicyDscp == i4Dscp)
    {
        return SNMP_SUCCESS;
    }

    pLldpMedLocPolicy->u1LocPolicyDscp = 
        (UINT1)i4SetValFsLldpMedLocMediaPolicyDscp;
    /* Reset Policy Unknown flag if DSCP is configured */
    pLldpMedLocPolicy->bLocPolicyUnKnown = 0;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsLldpMedLocMediaPolicyUnknown
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                setValFsLldpMedLocMediaPolicyUnknown
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocMediaPolicyUnknown(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        i4SetValFsLldpMedLocMediaPolicyUnknown) 
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    INT4 i4UnknownPolicy = 0;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType = *(pFsLldpMedLocMediaPolicyAppType
            ->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) & LldpMedLocPolicy);
    if (pLldpMedLocPolicy == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhSetFsLldpMedLocMediaPolicyUnknown: "
                "Entry is not present  \r\n");
        return SNMP_FAILURE;
    }

    i4UnknownPolicy = (INT4)pLldpMedLocPolicy->bLocPolicyUnKnown;
    if (i4SetValFsLldpMedLocMediaPolicyUnknown == i4UnknownPolicy)
    {
        return SNMP_SUCCESS;
    }

    pLldpMedLocPolicy->bLocPolicyUnKnown =
        (BOOL1)i4SetValFsLldpMedLocMediaPolicyUnknown;
    /* Unknown flag is set for this application type, so
     * reset all objects of Local Network policy table with zero */
     pLldpMedLocPolicy->u2LocPolicyVlanId = 0;
     pLldpMedLocPolicy->u1LocPolicyPriority = 0;
     pLldpMedLocPolicy->u1LocPolicyDscp = 0;
     pLldpMedLocPolicy->bLocPolicyTagged = 0;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsLldpMedLocMediaPolicyTagged
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                setValFsLldpMedLocMediaPolicyTagged
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocMediaPolicyTagged(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        i4SetValFsLldpMedLocMediaPolicyTagged) 
{
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    INT4 i4VlanType = 0;

    MEMSET (&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));
    LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LldpMedLocPolicy.u1LocPolicyAppType = *(pFsLldpMedLocMediaPolicyAppType
            ->pu1_OctetList);

    pLldpMedLocPolicy =
        (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) & LldpMedLocPolicy);
    if (pLldpMedLocPolicy == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhSetFsLldpMedLocMediaPolicyTagged: "
                "Entry is not present  \r\n");
        return SNMP_FAILURE;
    }

    i4VlanType = (INT4)pLldpMedLocPolicy->bLocPolicyTagged;
    if (i4SetValFsLldpMedLocMediaPolicyTagged == i4VlanType)
    {
        return SNMP_SUCCESS;
    }

    pLldpMedLocPolicy->bLocPolicyTagged =
        (BOOL1)i4SetValFsLldpMedLocMediaPolicyTagged;

    if (pLldpMedLocPolicy->bLocPolicyTagged == LLDP_MED_VLAN_UNTAGGED)
    {
        pLldpMedLocPolicy->u2LocPolicyVlanId = 0;
        pLldpMedLocPolicy->u1LocPolicyPriority = 0;
        pLldpMedLocPolicy->bLocPolicyUnKnown = 0;

    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsLldpMedLocMediaPolicyRowStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                setValFsLldpMedLocMediaPolicyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocMediaPolicyRowStatus(INT4 i4LldpV2LocPortIfIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpMedLocMediaPolicyAppType , INT4
        i4SetValFsLldpMedLocMediaPolicyRowStatus) 
{
    tLldpMedLocNwPolicyInfo *pLldpMedLocPolicy = NULL;
    tLldpMedLocNwPolicyInfo LldpMedLocPolicy;
    INT4                    i4AdminStatus = 0;

    MEMSET(&LldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));

    switch (i4SetValFsLldpMedLocMediaPolicyRowStatus)
    {
        case CREATE_AND_WAIT:

            if ((pLldpMedLocPolicy =
                        (tLldpMedLocNwPolicyInfo *)
                        (MemAllocMemBlk
                         (gLldpGlobalInfo.LldpMedLocNwPolicyPoolId))) == NULL) 
            {

                LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                        "nmhSetFsLldpMedLocMediaPolicyRowStatus: "
                        "Mempool Allocation Failure for New Network Policy"
                        "Node\r\n");
                LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                return SNMP_FAILURE;
            }

            MEMSET(pLldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));

            pLldpMedLocPolicy->u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
            pLldpMedLocPolicy->u1LocPolicyAppType = 
                *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

            if (RBTreeAdd(gLldpGlobalInfo.
                        LldpMedLocNwPolicyInfoRBTree,
                        (tRBElem *)pLldpMedLocPolicy) == RB_FAILURE)
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocNwPolicyPoolId,
                        (UINT1 *) pLldpMedLocPolicy);
                return SNMP_FAILURE;
            }
            pLldpMedLocPolicy->i4RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:

            LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
            LldpMedLocPolicy.u1LocPolicyAppType = 
                *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

            pLldpMedLocPolicy = (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                    LldpMedLocNwPolicyInfoRBTree,
                    (tRBElem *) &LldpMedLocPolicy);
            if (pLldpMedLocPolicy == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                        "nmhSetFsLldpMedLocMediaPolicyRowStatus: "
                        "Entry is not present  \r\n");
                return SNMP_FAILURE;
            }
            if (pLldpMedLocPolicy->i4RowStatus != NOT_IN_SERVICE)
            {
                pLldpMedLocPolicy->i4RowStatus = NOT_IN_SERVICE;
                /*Since there is a change in local database, construct the
                 * preformed buffer for that port */
                LldpMedGetAdminStatus(i4LldpV2LocPortIfIndex, &i4AdminStatus);
                if (i4AdminStatus == LLDP_ENABLED)
                {
                    LldpMedUtlHandlePolicyChg (i4LldpV2LocPortIfIndex);
                }
            }

            break;

        case CREATE_AND_GO:

            if ((pLldpMedLocPolicy =
                        (tLldpMedLocNwPolicyInfo *)
                        (MemAllocMemBlk
                         (gLldpGlobalInfo.LldpMedLocNwPolicyPoolId))) == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                        "nmhSetFsLldpMedLocMediaPolicyRowStatus: "
                        "Mempool Allocation Failure for New Network Policy Node\r\n");
                LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                return SNMP_FAILURE;
            }

            MEMSET(pLldpMedLocPolicy, 0, sizeof (tLldpMedLocNwPolicyInfo));

            pLldpMedLocPolicy->u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
            pLldpMedLocPolicy->u1LocPolicyAppType =
                *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

            if (RBTreeAdd(gLldpGlobalInfo.
                        LldpMedLocNwPolicyInfoRBTree,
                        (tRBElem *)pLldpMedLocPolicy) == RB_FAILURE)
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocNwPolicyPoolId,
                        (UINT1 *) pLldpMedLocPolicy);
                return SNMP_FAILURE;
            }
            pLldpMedLocPolicy->i4RowStatus = ACTIVE;
            /*Since there is a change in local database, construct the
             * preformed buffer for that port */
            LldpMedGetAdminStatus(i4LldpV2LocPortIfIndex, &i4AdminStatus);
            if (i4AdminStatus == LLDP_ENABLED)
            {
                LldpMedUtlHandlePolicyChg (i4LldpV2LocPortIfIndex);
            }
            break;

        case ACTIVE:

            LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
            LldpMedLocPolicy.u1LocPolicyAppType =
                *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

            pLldpMedLocPolicy = (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                    LldpMedLocNwPolicyInfoRBTree,
                    (tRBElem *) &LldpMedLocPolicy);
            if (pLldpMedLocPolicy == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                        "nmhSetFsLldpMedLocMediaPolicyRowStatus: "
                        "Entry is not present  \r\n");
                return SNMP_FAILURE;
            }
            if (pLldpMedLocPolicy->i4RowStatus != ACTIVE)
            {
                pLldpMedLocPolicy->i4RowStatus = ACTIVE;                   

                /*Since there is a change in local database, construct the
                 * preformed buffer for that port */
                LldpMedGetAdminStatus(i4LldpV2LocPortIfIndex, &i4AdminStatus);
                if (i4AdminStatus == LLDP_ENABLED)
                {
                    LldpMedUtlHandlePolicyChg (i4LldpV2LocPortIfIndex);
                }
            }

            break;

        case DESTROY:

            LldpMedLocPolicy.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
            LldpMedLocPolicy.u1LocPolicyAppType =
                *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

            pLldpMedLocPolicy = (tLldpMedLocNwPolicyInfo *) RBTreeGet (gLldpGlobalInfo.
                    LldpMedLocNwPolicyInfoRBTree,
                    (tRBElem *) &LldpMedLocPolicy);
            if (pLldpMedLocPolicy == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                        "nmhSetFsLldpMedLocMediaPolicyRowStatus: "
                        "Entry is not present  \r\n");
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (gLldpGlobalInfo.
                        LldpMedLocNwPolicyInfoRBTree,
                        (tRBElem *) pLldpMedLocPolicy) == RB_FAILURE)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                        " Cannot Delete Local Policy \r\n");
                return SNMP_FAILURE;
            }
            MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocNwPolicyPoolId,
                    (UINT1 *) pLldpMedLocPolicy);

            /*Since there is a change in local database, construct the
             * preformed buffer for that port */
            LldpMedGetAdminStatus(i4LldpV2LocPortIfIndex, &i4AdminStatus);
            if (i4AdminStatus == LLDP_ENABLED)
            {
                LldpMedUtlHandlePolicyChg (i4LldpV2LocPortIfIndex);
            }

            break;

        default:

            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                    " Invalid Row Status \r\n");
            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocMediaPolicyVlanID
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                testValFsLldpMedLocMediaPolicyVlanID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocMediaPolicyVlanID(UINT4 *pu4ErrorCode , INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType , INT4
        i4TestValFsLldpMedLocMediaPolicyVlanID) 
{
    tLldpMedLocNwPolicyInfo *pLocPolicyInfo = NULL;
    tLldpMedLocNwPolicyInfo LocPolicyInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhTestv2FsLldpMedLocMediaPolicyVlanID: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLldpMedLocMediaPolicyVlanID < LLDP_MIN_VLAN_ID) ||
            (i4TestValFsLldpMedLocMediaPolicyVlanID > LLDP_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&LocPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    LocPolicyInfo.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LocPolicyInfo.u1LocPolicyAppType =
        *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;
    pLocPolicyInfo = (tLldpMedLocNwPolicyInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) &LocPolicyInfo);

    if (pLocPolicyInfo == NULL)
     {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Allow to edit the VlanId if and only if
     * Row Status is NOT_IN_SERVICE*/
    if (pLocPolicyInfo->i4RowStatus != NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocMediaPolicyPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                testValFsLldpMedLocMediaPolicyPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocMediaPolicyPriority(UINT4 *pu4ErrorCode , INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType , INT4
        i4TestValFsLldpMedLocMediaPolicyPriority) 
{
    tLldpMedLocNwPolicyInfo *pLocPolicyInfo = NULL;
    tLldpMedLocNwPolicyInfo LocPolicyInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhTestv2FsLldpMedLocMediaPolicyPriority: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLldpMedLocMediaPolicyPriority < LLDP_MED_MIN_PRIORITY) ||
            (i4TestValFsLldpMedLocMediaPolicyPriority > LLDP_MED_MAX_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&LocPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    LocPolicyInfo.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LocPolicyInfo.u1LocPolicyAppType =
        *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;
    pLocPolicyInfo = (tLldpMedLocNwPolicyInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) &LocPolicyInfo);

    if (pLocPolicyInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Allow to edit the Priority if and only if
     * Row Status is NOT_IN_SERVICE*/
    if (pLocPolicyInfo->i4RowStatus != NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocMediaPolicyDscp
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                testValFsLldpMedLocMediaPolicyDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocMediaPolicyDscp(UINT4 *pu4ErrorCode , INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType , INT4
        i4TestValFsLldpMedLocMediaPolicyDscp) 
{
    tLldpMedLocNwPolicyInfo *pLocPolicyInfo = NULL;
    tLldpMedLocNwPolicyInfo LocPolicyInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhTestv2FsLldpMedLocMediaPolicyDscp: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLldpMedLocMediaPolicyDscp < LLDP_MED_MIN_DSCP) ||
            (i4TestValFsLldpMedLocMediaPolicyDscp > LLDP_MED_MAX_DSCP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&LocPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    LocPolicyInfo.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LocPolicyInfo.u1LocPolicyAppType =
        *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

    pLocPolicyInfo = (tLldpMedLocNwPolicyInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) &LocPolicyInfo);

    if (pLocPolicyInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Allow to edit the Dscp if and only if
     * Row Status is NOT_IN_SERVICE*/
    if (pLocPolicyInfo->i4RowStatus != NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocMediaPolicyUnknown
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                testValFsLldpMedLocMediaPolicyUnknown
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocMediaPolicyUnknown(UINT4 *pu4ErrorCode , INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType , INT4
        i4TestValFsLldpMedLocMediaPolicyUnknown) 
{
    tLldpMedLocNwPolicyInfo *pLocPolicyInfo = NULL;
    tLldpMedLocNwPolicyInfo LocPolicyInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                "nmhTestv2FsLldpMedLocMediaPolicyUnknown: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsLldpMedLocMediaPolicyUnknown >
            LLDP_MED_UNKNOWN_FLAG)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;        
        return SNMP_FAILURE;
    }

    MEMSET (&LocPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    LocPolicyInfo.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LocPolicyInfo.u1LocPolicyAppType =
        *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

    pLocPolicyInfo = (tLldpMedLocNwPolicyInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) &LocPolicyInfo);

    if (pLocPolicyInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Allow to edit the Unknown Policy Flag if and only if
     * Row Status is NOT_IN_SERVICE*/
    if (pLocPolicyInfo->i4RowStatus != NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocMediaPolicyTagged
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                testValFsLldpMedLocMediaPolicyTagged
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocMediaPolicyTagged(UINT4 *pu4ErrorCode , INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType , INT4
        i4TestValFsLldpMedLocMediaPolicyTagged) 
{
    tLldpMedLocNwPolicyInfo *pLocPolicyInfo = NULL;
    tLldpMedLocNwPolicyInfo LocPolicyInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhTestv2FsLldpMedLocMediaPolicyTagged: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsLldpMedLocMediaPolicyTagged != LLDP_MED_VLAN_TAGGED)
            && (i4TestValFsLldpMedLocMediaPolicyTagged !=
                LLDP_MED_VLAN_UNTAGGED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;   
        return SNMP_FAILURE;
    }

    MEMSET (&LocPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    LocPolicyInfo.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LocPolicyInfo.u1LocPolicyAppType =
        *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;

    pLocPolicyInfo = (tLldpMedLocNwPolicyInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) &LocPolicyInfo);

    if (pLocPolicyInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Allow to edit the VlanType if and only if
     * Row Status is NOT_IN_SERVICE*/
    if (pLocPolicyInfo->i4RowStatus != NOT_IN_SERVICE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocMediaPolicyRowStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType

                The Object 
                testValFsLldpMedLocMediaPolicyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocMediaPolicyRowStatus(UINT4 *pu4ErrorCode , INT4
        i4LldpV2LocPortIfIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpMedLocMediaPolicyAppType , INT4
        i4TestValFsLldpMedLocMediaPolicyRowStatus) 
{
    tLldpMedLocNwPolicyInfo *pLocPolicyInfo = NULL;
    tLldpMedLocNwPolicyInfo LocPolicyInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhTestv2FsLldpMedLocMediaPolicyRowStatus: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&LocPolicyInfo, 0, sizeof (tLldpMedLocNwPolicyInfo));

    LocPolicyInfo.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    LocPolicyInfo.u1LocPolicyAppType = 
        *pFsLldpMedLocMediaPolicyAppType->pu1_OctetList;
    LocPolicyInfo.i4RowStatus = i4TestValFsLldpMedLocMediaPolicyRowStatus;

    pLocPolicyInfo = (tLldpMedLocNwPolicyInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocNwPolicyInfoRBTree,
                (tRBElem *) &LocPolicyInfo);

    if ((pLocPolicyInfo == NULL) &&
            ((i4TestValFsLldpMedLocMediaPolicyRowStatus != CREATE_AND_WAIT) &&
             (i4TestValFsLldpMedLocMediaPolicyRowStatus != CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsLldpMedLocMediaPolicyRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            if (pLocPolicyInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:

            if (pLocPolicyInfo->bLocPolicyTagged == LLDP_MED_VLAN_TAGGED)
            {
                if (pLocPolicyInfo->u2LocPolicyVlanId == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
        case NOT_READY:
        case NOT_IN_SERVICE:
        case DESTROY:
            break;
       
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpMedLocMediaPolicyTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                FsLldpMedLocMediaPolicyAppType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsLldpMedLocMediaPolicyTable(UINT4 *pu4ErrorCode, tSnmpIndexList
        *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpMedLocLocationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpMedLocLocationTable(INT4 i4LldpLocPortNum , INT4 
        i4LldpXMedLocLocationSubtype)
{
    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Validate Interface Index */
    if (LldpTxUtlValidateInterfaceIndex(i4LldpLocPortNum) !=
            OSIX_SUCCESS)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhValidateIndexInstanceFsLldpMedLocLocationTable: "
                " Interface Index is invalid \r\n");
        return SNMP_FAILURE;
    }
    /* Validate Location subtype */
    if ((i4LldpXMedLocLocationSubtype < LLDP_MED_COORDINATE_LOC) ||
        (i4LldpXMedLocLocationSubtype > LLDP_MED_ELIN_LOC))
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 " nmhValidateIndexInstanceFsLldpMedLocLocationTable: "
                 " Location Sub Type is not within supported range \r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpMedLocLocationTable(INT4 *pi4LldpLocPortNum , INT4
        *pi4LldpXMedLocLocationSubtype)
{
    tLldpMedLocLocationInfo *pLldpMedLocLocation = NULL;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Get First Index of PolicyTable */
    pLldpMedLocLocation =
        (tLldpMedLocLocationInfo *) RBTreeGetFirst(gLldpGlobalInfo.
                                                 LldpMedLocLocationInfoRBTree);
    if (pLldpMedLocLocation == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 " nmhGetFirstIndexFsLldpMedLocLocationTable: "
                 " Failed to get Location Table Index \r\n");

        return SNMP_FAILURE;
    }

    /* Assigning IfIndex and Policy App type */
    *pi4LldpLocPortNum = (INT4) pLldpMedLocLocation->u4IfIndex;
    *pi4LldpXMedLocLocationSubtype = (INT4) pLldpMedLocLocation->u1LocationDataFormat;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                nextLldpLocPortNum
                LldpXMedLocLocationSubtype
                nextLldpXMedLocLocationSubtype
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpMedLocLocationTable(INT4 i4LldpLocPortNum ,INT4 
        *pi4NextLldpLocPortNum  , INT4 i4LldpXMedLocLocationSubtype ,INT4 
        *pi4NextLldpXMedLocLocationSubtype )
{
    tLldpMedLocLocationInfo CurrLocLocation;
    tLldpMedLocLocationInfo *pNextLocLocation = NULL;

    MEMSET(&CurrLocLocation, 0, sizeof (tLldpMedLocLocationInfo));
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    /* Assigning the Current IfIndex */
    CurrLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
    CurrLocLocation.u1LocationDataFormat = (UINT1) i4LldpXMedLocLocationSubtype;

    /* Get Next Index of Local Location Table */
    pNextLocLocation =
       (tLldpMedLocLocationInfo *) RBTreeGetNext (gLldpGlobalInfo.
                                               LldpMedLocLocationInfoRBTree,
                                               &CurrLocLocation,
                                               LldpMedLocLocationInfoRBCmpInfo);

    if (pNextLocLocation == NULL)
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 " nmhGetNextIndexFsLldpMedLocLocationTable: "
                 " Failed to get Location Table Index \r\n");

        return SNMP_FAILURE;
    }

    *pi4NextLldpLocPortNum = (INT4) pNextLocLocation->u4IfIndex;
    *pi4NextLldpXMedLocLocationSubtype = (INT4) pNextLocLocation->u1LocationDataFormat;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedLocLocationRowStatus
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype

                The Object 
                retValFsLldpMedLocLocationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedLocLocationRowStatus(INT4 i4LldpLocPortNum , INT4 
        i4LldpXMedLocLocationSubtype , INT4 
        *pi4RetValFsLldpMedLocLocationRowStatus)
{
    tLldpMedLocLocationInfo LldpMedLocLocation;
    tLldpMedLocLocationInfo *pLldpMedLocLocation = NULL;

    MEMSET(&LldpMedLocLocation, 0, sizeof (tLldpMedLocLocationInfo));
    LldpMedLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
    LldpMedLocLocation.u1LocationDataFormat = 
           (UINT1) i4LldpXMedLocLocationSubtype;

    pLldpMedLocLocation =
        (tLldpMedLocLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                                             LldpMedLocLocationInfoRBTree,
                                            (tRBElem *) & LldpMedLocLocation);

    if (pLldpMedLocLocation == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsLldpMedLocLocationRowStatus =
        pLldpMedLocLocation->i4RowStatus;
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpMedLocLocationRowStatus
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype

                The Object 
                setValFsLldpMedLocLocationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedLocLocationRowStatus(INT4 i4LldpLocPortNum , INT4 
        i4LldpXMedLocLocationSubtype , INT4 i4SetValFsLldpMedLocLocationRowStatus)
{
    tLldpMedLocLocationInfo *pLldpMedLocLocation = NULL;
    tLldpMedLocLocationInfo LldpMedLocLocation;
    INT4                    i4AdminStatus = 0;

    MEMSET(&LldpMedLocLocation, 0, sizeof (tLldpMedLocLocationInfo));

    switch (i4SetValFsLldpMedLocLocationRowStatus)
    {
        case CREATE_AND_WAIT:

            if ((pLldpMedLocLocation =
                        (tLldpMedLocLocationInfo *)
                        (MemAllocMemBlk
                         (gLldpGlobalInfo.LldpMedLocLocationPoolId))) == NULL)
            {

                LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                        "nmhSetFsLldpMedLocLocationRowStatus: "
                        "Mempool Allocation Failure for New Location Information"
                        "Node\r\n");
                LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                return SNMP_FAILURE;
            }

            MEMSET(pLldpMedLocLocation, 0, sizeof (tLldpMedLocLocationInfo));

            pLldpMedLocLocation->u4IfIndex = (UINT4) i4LldpLocPortNum;
            pLldpMedLocLocation->u1LocationDataFormat =
                (UINT1) i4LldpXMedLocLocationSubtype;

            if (RBTreeAdd(gLldpGlobalInfo.
                        LldpMedLocLocationInfoRBTree,
                        (tRBElem *)pLldpMedLocLocation) == RB_FAILURE)
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocLocationPoolId,
                        (UINT1 *) pLldpMedLocLocation);
                return SNMP_FAILURE;
            }
            pLldpMedLocLocation->i4RowStatus = NOT_READY;
            break;

        case NOT_IN_SERVICE:

            LldpMedLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
            LldpMedLocLocation.u1LocationDataFormat =
                (UINT1) i4LldpXMedLocLocationSubtype;

            pLldpMedLocLocation = (tLldpMedLocLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                    LldpMedLocLocationInfoRBTree,
                    (tRBElem *) &LldpMedLocLocation);
            if (pLldpMedLocLocation == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                        "nmhSetFsLldpMedLocLocationRowStatus: "
                        "Entry is not present  \r\n");
                return SNMP_FAILURE;
            }
            if (pLldpMedLocLocation->i4RowStatus != NOT_IN_SERVICE)
            {
                pLldpMedLocLocation->i4RowStatus = NOT_IN_SERVICE;
                /*Since there is a change in local database, construct the
                 * preformed buffer for that port */
                LldpMedGetAdminStatus(i4LldpLocPortNum, &i4AdminStatus);
                if (i4AdminStatus == LLDP_ENABLED)
                {
                    LldpMedUtlHandleLocationChg (i4LldpLocPortNum);
                }
            }

            break;

        case CREATE_AND_GO:

            if ((pLldpMedLocLocation =
                        (tLldpMedLocLocationInfo *)
                        (MemAllocMemBlk
                         (gLldpGlobalInfo.LldpMedLocLocationPoolId))) == NULL)
            {
                LLDP_TRC (ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                        "nmhSetFsLldpMedLocLocationRowStatus: "
                        "Mempool Allocation Failure for New Location Node\r\n");
                LLDP_ERR_CNTR_MEM_ALLOC_FAIL++;
                return SNMP_FAILURE;
            }

            MEMSET(pLldpMedLocLocation, 0, sizeof (tLldpMedLocLocationInfo));

            pLldpMedLocLocation->u4IfIndex = (UINT4) i4LldpLocPortNum;
            pLldpMedLocLocation->u1LocationDataFormat =
                (UINT1) i4LldpXMedLocLocationSubtype;

            if (RBTreeAdd(gLldpGlobalInfo.
                        LldpMedLocLocationInfoRBTree,
                        (tRBElem *)pLldpMedLocLocation) == RB_FAILURE)
            {
                MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocLocationPoolId,
                        (UINT1 *) pLldpMedLocLocation);
                return SNMP_FAILURE;
            }
            pLldpMedLocLocation->i4RowStatus = ACTIVE;
            /*Since there is a change in local database, construct the
             * preformed buffer for that port */
            LldpMedGetAdminStatus(i4LldpLocPortNum, &i4AdminStatus);
            if (i4AdminStatus == LLDP_ENABLED)
            {
                LldpMedUtlHandleLocationChg (i4LldpLocPortNum);
            }
            break;

        case ACTIVE:

            LldpMedLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
            LldpMedLocLocation.u1LocationDataFormat =
                (UINT1) i4LldpXMedLocLocationSubtype;

            pLldpMedLocLocation = (tLldpMedLocLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                    LldpMedLocLocationInfoRBTree,
                    (tRBElem *) &LldpMedLocLocation);
            if (pLldpMedLocLocation == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                        "nmhSetFsLldpMedLocLocationRowStatus: "
                        "Entry is not present  \r\n");
                return SNMP_FAILURE;
            }
            if (pLldpMedLocLocation->i4RowStatus != ACTIVE)
            {
                pLldpMedLocLocation->i4RowStatus = ACTIVE;

                /*Since there is a change in local database, construct the
                 * preformed buffer for that port */
                LldpMedGetAdminStatus(i4LldpLocPortNum, &i4AdminStatus);
                if (i4AdminStatus == LLDP_ENABLED)
                {
                    LldpMedUtlHandleLocationChg (i4LldpLocPortNum);
                }
            }

            break;

        case DESTROY:

            LldpMedLocLocation.u4IfIndex = (UINT4) i4LldpLocPortNum;
            LldpMedLocLocation.u1LocationDataFormat =
                (UINT1) i4LldpXMedLocLocationSubtype;

            pLldpMedLocLocation = (tLldpMedLocLocationInfo *) RBTreeGet (gLldpGlobalInfo.
                    LldpMedLocLocationInfoRBTree,
                    (tRBElem *) &LldpMedLocLocation);
            if (pLldpMedLocLocation == NULL)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                        "nmhSetFsLldpMedLocLocationRowStatus: "
                        "Entry is not present  \r\n");
                return SNMP_FAILURE;
            }

            if (RBTreeRemove (gLldpGlobalInfo.
                        LldpMedLocLocationInfoRBTree,
                        (tRBElem *) pLldpMedLocLocation) == RB_FAILURE)
            {
                LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC | LLDP_CRITICAL_TRC,
                        " Cannot Delete Local Location Information \r\n");
                return SNMP_FAILURE;
            }
            MemReleaseMemBlock (gLldpGlobalInfo.LldpMedLocLocationPoolId,
                    (UINT1 *) pLldpMedLocLocation);

            /*Since there is a change in local database, construct the
             * preformed buffer for that port */
                LldpMedGetAdminStatus(i4LldpLocPortNum, &i4AdminStatus);
                if (i4AdminStatus == LLDP_ENABLED)
                {
                    LldpMedUtlHandleLocationChg (i4LldpLocPortNum);
                }

            break;

        default:

            LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                    " Invalid Row Status \r\n");
            return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpMedLocLocationRowStatus
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype

                The Object 
                testValFsLldpMedLocLocationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedLocLocationRowStatus(UINT4 *pu4ErrorCode , INT4 
        i4LldpLocPortNum , INT4 i4LldpXMedLocLocationSubtype , 
        INT4 i4TestValFsLldpMedLocLocationRowStatus)
{
    tLldpMedLocLocationInfo *pLocLocationInfo = NULL;
    tLldpMedLocLocationInfo LocLocationInfo;

    if (LLDP_IS_SHUTDOWN ())
    {
        LLDP_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                " nmhTestv2FsLldpMedLocLocationRowStatus: "
                "LLDP should be started before accessing its "
                "MIB objects !!\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&LocLocationInfo, 0, sizeof (tLldpMedLocLocationInfo));

    LocLocationInfo.u4IfIndex = (UINT4) i4LldpLocPortNum;
    LocLocationInfo.u1LocationDataFormat =
        (UINT1) i4LldpXMedLocLocationSubtype;
    LocLocationInfo.i4RowStatus = i4TestValFsLldpMedLocLocationRowStatus;

    pLocLocationInfo = (tLldpMedLocLocationInfo *)
        RBTreeGet (gLldpGlobalInfo.
                LldpMedLocLocationInfoRBTree,
                (tRBElem *) &LocLocationInfo);

    if ((pLocLocationInfo == NULL) &&
            ((i4TestValFsLldpMedLocLocationRowStatus != CREATE_AND_WAIT) &&
             (i4TestValFsLldpMedLocLocationRowStatus != CREATE_AND_GO)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsLldpMedLocLocationRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            if (pLocLocationInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_READY:
        case NOT_IN_SERVICE:
        case DESTROY:
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpMedLocLocationTable
 Input       :  The Indices
                LldpLocPortNum
                LldpXMedLocLocationSubtype
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsLldpMedLocLocationTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpXMedRemCapabilitiesTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpXMedRemCapabilitiesTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpXMedRemCapabilitiesTable(UINT4
        u4LldpV2RemTimeMark , INT4 i4LldpV2RemLocalIfIndex , UINT4
        u4LldpV2RemLocalDestMACAddress , INT4 i4LldpV2RemIndex) 
{
    if (LldpMedUtlValIndexRemCapInvTbl(u4LldpV2RemTimeMark,
                                       i4LldpV2RemLocalIfIndex,
                                       u4LldpV2RemLocalDestMACAddress,
                                       i4LldpV2RemIndex) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpXMedRemCapabilitiesTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpXMedRemCapabilitiesTable(UINT4 *pu4LldpV2RemTimeMark
        , INT4 *pi4LldpV2RemLocalIfIndex , UINT4
        *pu4LldpV2RemLocalDestMACAddress , INT4 *pi4LldpV2RemIndex) 
{
    if (LldpMedUtlGetFirstIndexRemCapTbl(pu4LldpV2RemTimeMark,
                                         pi4LldpV2RemLocalIfIndex,
                                         pu4LldpV2RemLocalDestMACAddress,
                                         pi4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpXMedRemCapabilitiesTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpXMedRemCapabilitiesTable(UINT4 u4LldpV2RemTimeMark
        ,UINT4 *pu4NextLldpV2RemTimeMark  , INT4 i4LldpV2RemLocalIfIndex ,INT4
        *pi4NextLldpV2RemLocalIfIndex  , UINT4 u4LldpV2RemLocalDestMACAddress
        ,UINT4 *pu4NextLldpV2RemLocalDestMACAddress  , INT4 i4LldpV2RemIndex
        ,INT4 *pi4NextLldpV2RemIndex ) 
{
    if (LldpMedUtlGetNextIndexRemCapTbl(u4LldpV2RemTimeMark,
                pu4NextLldpV2RemTimeMark,
                i4LldpV2RemLocalIfIndex,
                pi4NextLldpV2RemLocalIfIndex,
                u4LldpV2RemLocalDestMACAddress,
                pu4NextLldpV2RemLocalDestMACAddress,
                i4LldpV2RemIndex,
                pi4NextLldpV2RemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemCapSupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemCapSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemCapSupported(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemCapSupported) 
{
    if (LldpMedUtlGetRemCapSupported(u4LldpV2RemTimeMark,
                                     i4LldpV2RemLocalIfIndex,
                                     u4LldpV2RemLocalDestMACAddress,
                                     i4LldpV2RemIndex,
                                     pRetValFsLldpXMedRemCapSupported) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemCapCurrent
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemCapCurrent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemCapCurrent(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemCapCurrent) 
{
    if (LldpMedUtlGetRemCapCurrent(u4LldpV2RemTimeMark,
                                   i4LldpV2RemLocalIfIndex,
                                   u4LldpV2RemLocalDestMACAddress,
                                   i4LldpV2RemIndex,
                                   pRetValFsLldpXMedRemCapCurrent) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemDeviceClass
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemDeviceClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemDeviceClass(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , INT4 *pi4RetValFsLldpXMedRemDeviceClass) 
{
    if (LldpMedUtlGetRemDeviceClass(u4LldpV2RemTimeMark,
                                    i4LldpV2RemLocalIfIndex,
                                    u4LldpV2RemLocalDestMACAddress,
                                    i4LldpV2RemIndex,
                                    pi4RetValFsLldpXMedRemDeviceClass) != 
            OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpXMedRemMediaPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpXMedRemMediaPolicyTable(UINT4
        u4LldpV2RemTimeMark , INT4 i4LldpV2RemLocalIfIndex , UINT4
        u4LldpV2RemLocalDestMACAddress , INT4 i4LldpV2RemIndex ,
        tSNMP_OCTET_STRING_TYPE *pFsLldpXMedRemMediaPolicyAppType) 
{
    if (LldpMedUtlValIndexRemNwPolTbl
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpXMedRemMediaPolicyTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpXMedRemMediaPolicyTable(UINT4 *pu4LldpV2RemTimeMark
        , INT4 *pi4LldpV2RemLocalIfIndex , UINT4
        *pu4LldpV2RemLocalDestMACAddress , INT4 *pi4LldpV2RemIndex ,
        tSNMP_OCTET_STRING_TYPE * pFsLldpXMedRemMediaPolicyAppType) 
{
    if (LldpMedUtlGetFirstIndexRemNwPolTbl
            (pu4LldpV2RemTimeMark,
             pi4LldpV2RemLocalIfIndex,
             pu4LldpV2RemLocalDestMACAddress,
             pi4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpXMedRemMediaPolicyTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType
                nextFsLldpXMedRemMediaPolicyAppType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpXMedRemMediaPolicyTable(UINT4 u4LldpV2RemTimeMark
        ,UINT4 *pu4NextLldpV2RemTimeMark  , INT4 i4LldpV2RemLocalIfIndex ,INT4
        *pi4NextLldpV2RemLocalIfIndex  , UINT4 u4LldpV2RemLocalDestMACAddress
        ,UINT4 *pu4NextLldpV2RemLocalDestMACAddress  , INT4 i4LldpV2RemIndex
        ,INT4 *pi4NextLldpV2RemIndex  , tSNMP_OCTET_STRING_TYPE
        *pFsLldpXMedRemMediaPolicyAppType ,tSNMP_OCTET_STRING_TYPE *
        pNextFsLldpXMedRemMediaPolicyAppType ) 
{
    if (LldpMedUtlGetNextIndexRemNwPolTbl
            (u4LldpV2RemTimeMark,
             pu4NextLldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             pi4NextLldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             pu4NextLldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pi4NextLldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType,
             pNextFsLldpXMedRemMediaPolicyAppType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemMediaPolicyVlanID
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType

                The Object 
                retValFsLldpXMedRemMediaPolicyVlanID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemMediaPolicyVlanID(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpXMedRemMediaPolicyAppType , INT4
        *pi4RetValFsLldpXMedRemMediaPolicyVlanID) 
{
    if (LldpMedUtlGetRemNwPolVlanID
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType,
             pi4RetValFsLldpXMedRemMediaPolicyVlanID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemMediaPolicyPriority
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType

                The Object 
                retValFsLldpXMedRemMediaPolicyPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemMediaPolicyPriority(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpXMedRemMediaPolicyAppType , INT4
        *pi4RetValFsLldpXMedRemMediaPolicyPriority) 
{
    if (LldpMedUtlGetRemNwPolPriority
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType,
             pi4RetValFsLldpXMedRemMediaPolicyPriority) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemMediaPolicyDscp
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType

                The Object 
                retValFsLldpXMedRemMediaPolicyDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemMediaPolicyDscp(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpXMedRemMediaPolicyAppType , INT4
        *pi4RetValFsLldpXMedRemMediaPolicyDscp) 
{
    if (LldpMedUtlGetRemNwPolDscp
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType,
             pi4RetValFsLldpXMedRemMediaPolicyDscp) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemMediaPolicyUnknown
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType

                The Object 
                retValFsLldpXMedRemMediaPolicyUnknown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemMediaPolicyUnknown(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpXMedRemMediaPolicyAppType , INT4
        *pi4RetValFsLldpXMedRemMediaPolicyUnknown) 
{
    if (LldpMedUtlGetRemNwPolUnknown
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType,
             pi4RetValFsLldpXMedRemMediaPolicyUnknown) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemMediaPolicyTagged
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                FsLldpXMedRemMediaPolicyAppType

                The Object 
                retValFsLldpXMedRemMediaPolicyTagged
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemMediaPolicyTagged(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE
        *pFsLldpXMedRemMediaPolicyAppType , INT4
        *pi4RetValFsLldpXMedRemMediaPolicyTagged) 
{
    if (LldpMedUtlGetRemNwPolTagged
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pFsLldpXMedRemMediaPolicyAppType,
             pi4RetValFsLldpXMedRemMediaPolicyTagged) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpXMedRemInventoryTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpXMedRemInventoryTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpXMedRemInventoryTable(UINT4
        u4LldpV2RemTimeMark , INT4 i4LldpV2RemLocalIfIndex , UINT4
        u4LldpV2RemLocalDestMACAddress , INT4 i4LldpV2RemIndex)
{
    if (LldpMedUtlValIndexRemCapInvTbl(u4LldpV2RemTimeMark,
                i4LldpV2RemLocalIfIndex, u4LldpV2RemLocalDestMACAddress,
                i4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpXMedRemInventoryTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpXMedRemInventoryTable(UINT4 *pu4LldpV2RemTimeMark ,
        INT4 *pi4LldpV2RemLocalIfIndex , UINT4 *pu4LldpV2RemLocalDestMACAddress
        , INT4 *pi4LldpV2RemIndex)
{
    if (LldpMedUtlGetFirstIndexRemInvTbl(pu4LldpV2RemTimeMark,
                pi4LldpV2RemLocalIfIndex, pu4LldpV2RemLocalDestMACAddress,
                pi4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpXMedRemInventoryTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpXMedRemInventoryTable(UINT4 u4LldpV2RemTimeMark
        ,UINT4 *pu4NextLldpV2RemTimeMark  , INT4 i4LldpV2RemLocalIfIndex ,INT4
        *pi4NextLldpV2RemLocalIfIndex  , UINT4 u4LldpV2RemLocalDestMACAddress
        ,UINT4 *pu4NextLldpV2RemLocalDestMACAddress  , INT4 i4LldpV2RemIndex
        ,INT4 *pi4NextLldpV2RemIndex ) 
{
    if (LldpMedUtlGetNextIndexRemInvTbl(u4LldpV2RemTimeMark,
                pu4NextLldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                pi4NextLldpV2RemLocalIfIndex, u4LldpV2RemLocalDestMACAddress,
                pu4NextLldpV2RemLocalDestMACAddress, i4LldpV2RemIndex,
                pi4NextLldpV2RemIndex) != OSIX_SUCCESS) 
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemHardwareRev
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemHardwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemHardwareRev(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemHardwareRev) 
{
    if (LldpMedUtlGetRemHardwareRev(u4LldpV2RemTimeMark,
                                    i4LldpV2RemLocalIfIndex,
                                    u4LldpV2RemLocalDestMACAddress,
                                    i4LldpV2RemIndex,
                                    pRetValFsLldpXMedRemHardwareRev) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemFirmwareRev
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemFirmwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemFirmwareRev(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemFirmwareRev) 
{
    if (LldpMedUtlGetRemFirmwareRev(u4LldpV2RemTimeMark,
                                    i4LldpV2RemLocalIfIndex,
                                    u4LldpV2RemLocalDestMACAddress,
                                    i4LldpV2RemIndex,
                                    pRetValFsLldpXMedRemFirmwareRev) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemSoftwareRev
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemSoftwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemSoftwareRev(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemSoftwareRev) 
{
    if (LldpMedUtlGetRemSoftwareRev(u4LldpV2RemTimeMark,
                                     i4LldpV2RemLocalIfIndex,
                                     u4LldpV2RemLocalDestMACAddress,
                                     i4LldpV2RemIndex,
                                     pRetValFsLldpXMedRemSoftwareRev) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemSerialNum
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemSerialNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemSerialNum(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemSerialNum) 
{
    if (LldpMedUtlGetRemSerialNum(u4LldpV2RemTimeMark,
                                  i4LldpV2RemLocalIfIndex,
                                  u4LldpV2RemLocalDestMACAddress,
                                  i4LldpV2RemIndex,
                                  pRetValFsLldpXMedRemSerialNum) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemMfgName
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemMfgName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemMfgName(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemMfgName) 
{
    if (LldpMedUtlGetRemMfgName(u4LldpV2RemTimeMark,
                                i4LldpV2RemLocalIfIndex,
                                u4LldpV2RemLocalDestMACAddress,
                                i4LldpV2RemIndex,
                                pRetValFsLldpXMedRemMfgName) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemModelName
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemModelName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemModelName(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemModelName) 
{
    if (LldpMedUtlGetRemModelName(u4LldpV2RemTimeMark,
                                  i4LldpV2RemLocalIfIndex,
                                  u4LldpV2RemLocalDestMACAddress,
                                  i4LldpV2RemIndex,
                                  pRetValFsLldpXMedRemModelName) 
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemAssetID
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemAssetID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemAssetID(UINT4 u4LldpV2RemTimeMark , INT4
        i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , INT4
        i4LldpV2RemIndex , tSNMP_OCTET_STRING_TYPE *
        pRetValFsLldpXMedRemAssetID) 
{
    if (LldpMedUtlGetRemAssetID(u4LldpV2RemTimeMark,
                                i4LldpV2RemLocalIfIndex,
                                u4LldpV2RemLocalDestMACAddress,
                                i4LldpV2RemIndex,
                                pRetValFsLldpXMedRemAssetID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpMedStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpMedStatsTable
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpMedStatsTable(INT4 i4FsLldpMedStatsIfIndex ,
        UINT4 u4FsLldpMedStatsDestMACAddress) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpv2DestAddrTbl  DestTbl;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    /*Get Destination MacAddress from Destination MacAddressIndex */
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                Lldpv2DestMacAddrTblRBTree,
                (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Call Validate function in Util as it is commaon for both standard and
     * proprietary */
    if (LldpMedUtlValidateIndexPortConfTbl
            (i4FsLldpMedStatsIfIndex, pDestTbl->Lldpv2DestMacAddress) !=
            OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpMedStatsTable
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpMedStatsTable(INT4 *pi4FsLldpMedStatsIfIndex , UINT4
        *pu4FsLldpMedStatsDestMACAddress) 
{
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;

    /* Checking Whether LLDP is Shutdown. If it is true, then we should not
     * configure */
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }
    pLldpv2AgentToLocPort = (tLldpv2AgentToLocPort *)
        RBTreeGetFirst(gLldpGlobalInfo.Lldpv2AgentMapTblRBTree);
    if (pLldpv2AgentToLocPort == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsLldpMedStatsIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4FsLldpMedStatsDestMACAddress =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpMedStatsTable
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                nextFsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress
                nextFsLldpMedStatsDestMACAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpMedStatsTable(INT4 i4FsLldpMedStatsIfIndex ,INT4
        *pi4NextFsLldpMedStatsIfIndex  , UINT4 u4FsLldpMedStatsDestMACAddress,
        UINT4 *pu4NextFsLldpMedStatsDestMACAddress ) 
{

    tLldpv2AgentToLocPort Lldpv2AgentToLocPort;
    tLldpv2AgentToLocPort *pLldpv2AgentToLocPort = NULL;
    MEMSET (&Lldpv2AgentToLocPort, 0, sizeof (tLldpv2AgentToLocPort));
    if (LLDP_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

   Lldpv2AgentToLocPort.i4IfIndex = i4FsLldpMedStatsIfIndex;
   Lldpv2AgentToLocPort.u4DstMacAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
   pLldpv2AgentToLocPort =
        (tLldpv2AgentToLocPort *) RBTreeGetNext (gLldpGlobalInfo.
                                             Lldpv2AgentMapTblRBTree,
                                             &Lldpv2AgentToLocPort,
                                             LldpAgentToLocPortIndexUtlRBCmpInfo);

    if (pLldpv2AgentToLocPort == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsLldpMedStatsIfIndex = pLldpv2AgentToLocPort->i4IfIndex;
    *pu4NextFsLldpMedStatsDestMACAddress =
        pLldpv2AgentToLocPort->u4DstMacAddrTblIndex;

    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsTxFramesTotal
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsTxFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsTxFramesTotal(INT4 i4FsLldpMedStatsIfIndex , UINT4
        u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsTxFramesTotal) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex, 
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsTxFramesTotal = pLldpLocalPortInfo->MedStatsInfo.
                                            u4TxMedFramesTotal;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxFramesTotal
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxFramesTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxFramesTotal(INT4 i4FsLldpMedStatsIfIndex , UINT4
        u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsRxFramesTotal) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex, 
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxFramesTotal = pLldpLocalPortInfo->MedStatsInfo.
                                            u4RxMedFramesTotal;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxFramesDiscardedTotal
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxFramesDiscardedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxFramesDiscardedTotal(INT4 i4FsLldpMedStatsIfIndex ,
        UINT4 u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsRxFramesDiscardedTotal) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex, 
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxFramesDiscardedTotal = 
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedFramesDiscardedTotal;

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxTLVsDiscardedTotal
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxTLVsDiscardedTotal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxTLVsDiscardedTotal(INT4 i4FsLldpMedStatsIfIndex ,
        UINT4 u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsRxTLVsDiscardedTotal) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex, 
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxTLVsDiscardedTotal = 
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedTLVsDiscardedTotal;

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxCapTLVsDiscarded
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxCapTLVsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxCapTLVsDiscarded(INT4 i4FsLldpMedStatsIfIndex ,
        UINT4 u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsRxCapTLVsDiscarded) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex, 
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxCapTLVsDiscarded = 
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedCapTLVsDiscarded;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxPolicyTLVsDiscarded
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxPolicyTLVsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxPolicyTLVsDiscarded(INT4 i4FsLldpMedStatsIfIndex ,
        UINT4 u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsRxPolicyTLVsDiscarded) 
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex, 
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxPolicyTLVsDiscarded = 
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedPolicyTLVsDiscarded;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxInventoryTLVsDiscarded
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxInventoryTLVsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxInventoryTLVsDiscarded(INT4 i4FsLldpMedStatsIfIndex,
        UINT4 u4FsLldpMedStatsDestMACAddress , UINT4
        *pu4RetValFsLldpMedStatsRxInventoryTLVsDiscarded)
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxInventoryTLVsDiscarded = 
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedInventoryTLVsDiscarded;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxLocationTLVsDiscarded
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object
                retValFsLldpMedStatsRxLocationTLVsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxLocationTLVsDiscarded(INT4 i4FsLldpMedStatsIfIndex , 
        UINT4 u4FsLldpMedStatsDestMACAddress , UINT4 
        *pu4RetValFsLldpMedStatsRxLocationTLVsDiscarded)
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxLocationTLVsDiscarded =
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedLocationTLVsDiscarded;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpMedStatsRxExPowerMDITLVsDiscarded
 Input       :  The Indices
                FsLldpMedStatsIfIndex
                FsLldpMedStatsDestMACAddress

                The Object 
                retValFsLldpMedStatsRxExPowerMDITLVsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedStatsRxExPowerMDITLVsDiscarded(INT4 i4FsLldpMedStatsIfIndex , UINT4 
        u4FsLldpMedStatsDestMACAddress , UINT4 
        *pu4RetValFsLldpMedStatsRxExPowerMDITLVsDiscarded)
{
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsLldpMedStatsRxExPowerMDITLVsDiscarded =
                  pLldpLocalPortInfo->MedStatsInfo.u4RxMedExPowerMDITLVsDiscarded;
    return SNMP_SUCCESS;
}
  /****************************************************************************
   Function    :  nmhGetFsLldpMedStatsRxCapTLVsDiscardedReason
   Input       :  The Indices
                  FsLldpMedStatsIfIndex
                  FsLldpMedStatsDestMACAddress

                  The Object
                  retValFsLldpMedStatsRxCapTLVsDiscardedReason
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  ****************************************************************************/
  INT1 nmhGetFsLldpMedStatsRxCapTLVsDiscardedReason(INT4 i4FsLldpMedStatsIfIndex ,
          UINT4 u4FsLldpMedStatsDestMACAddress ,
          tSNMP_OCTET_STRING_TYPE * pRetValFsLldpMedStatsRxCapTLVsDiscardedReason)
  {
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;
    UINT1               au1ReasonCode[LLDP_MED_MAX_DISPLAY_STRING];

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&au1ReasonCode, 0, LLDP_MED_MAX_DISPLAY_STRING);
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The below line have to be removed once implementation is completed for Reason code */
      STRNCPY (au1ReasonCode,
              LLDP_MED_DEFAULT_STRING, STRLEN(LLDP_MED_DEFAULT_STRING));
      au1ReasonCode[STRLEN(LLDP_MED_DEFAULT_STRING)] = '\0';
      MEMCPY(pRetValFsLldpMedStatsRxCapTLVsDiscardedReason->pu1_OctetList,
              au1ReasonCode, STRLEN(au1ReasonCode));
      pRetValFsLldpMedStatsRxCapTLVsDiscardedReason->i4_Length =
          (INT4) STRLEN(au1ReasonCode);
      return SNMP_SUCCESS;
  }
  /****************************************************************************
   Function    :  nmhGetFsLldpMedStatsRxPolicyDiscardedReason
   Input       :  The Indices
                  FsLldpMedStatsIfIndex
                  FsLldpMedStatsDestMACAddress

                  The Object
                  retValFsLldpMedStatsRxPolicyDiscardedReason
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  ****************************************************************************/
  INT1 nmhGetFsLldpMedStatsRxPolicyDiscardedReason(INT4 i4FsLldpMedStatsIfIndex ,
          UINT4 u4FsLldpMedStatsDestMACAddress ,
          tSNMP_OCTET_STRING_TYPE * pRetValFsLldpMedStatsRxPolicyDiscardedReason)
  {
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;
    UINT1               au1ReasonCode[LLDP_MED_MAX_DISPLAY_STRING];

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&au1ReasonCode, 0, LLDP_MED_MAX_DISPLAY_STRING);
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The below line have to be removed once implementation is completed for Reason code */
      STRNCPY (au1ReasonCode,
              LLDP_MED_DEFAULT_STRING, STRLEN(LLDP_MED_DEFAULT_STRING));
      au1ReasonCode[STRLEN(LLDP_MED_DEFAULT_STRING)] = '\0';
      MEMCPY(pRetValFsLldpMedStatsRxPolicyDiscardedReason->pu1_OctetList,
              au1ReasonCode, STRLEN(au1ReasonCode));
      pRetValFsLldpMedStatsRxPolicyDiscardedReason->i4_Length =
          (INT4) STRLEN(au1ReasonCode);
      return SNMP_SUCCESS;
  }
  /****************************************************************************
   Function    :  nmhGetFsLldpMedStatsRxInventoryDiscardedReason
   Input       :  The Indices
                  FsLldpMedStatsIfIndex
                  FsLldpMedStatsDestMACAddress

                  The Object
                  retValFsLldpMedStatsRxInventoryDiscardedReason
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  ****************************************************************************/
  INT1 nmhGetFsLldpMedStatsRxInventoryDiscardedReason(INT4 i4FsLldpMedStatsIfIndex ,
          UINT4 u4FsLldpMedStatsDestMACAddress ,
          tSNMP_OCTET_STRING_TYPE * pRetValFsLldpMedStatsRxInventoryDiscardedReason)
  {
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;
    UINT1               au1ReasonCode[LLDP_MED_MAX_DISPLAY_STRING];

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&au1ReasonCode, 0, LLDP_MED_MAX_DISPLAY_STRING);
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The below line have to be removed once implementation is completed for Reason code */
      STRNCPY (au1ReasonCode,
              LLDP_MED_DEFAULT_STRING, STRLEN(LLDP_MED_DEFAULT_STRING));
      au1ReasonCode[STRLEN(LLDP_MED_DEFAULT_STRING)] = '\0';
      MEMCPY(pRetValFsLldpMedStatsRxInventoryDiscardedReason->pu1_OctetList,
              au1ReasonCode, STRLEN(au1ReasonCode));
      pRetValFsLldpMedStatsRxInventoryDiscardedReason->i4_Length =
          (INT4) STRLEN(au1ReasonCode);
      return SNMP_SUCCESS;
  }
  /****************************************************************************
   Function    :  nmhGetFsLldpMedStatsRxLocationDiscardedReason
   Input       :  The Indices
                  FsLldpMedStatsIfIndex
                  FsLldpMedStatsDestMACAddress

                  The Object
                  retValFsLldpMedStatsRxLocationDiscardedReason
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  ****************************************************************************/
  INT1 nmhGetFsLldpMedStatsRxLocationDiscardedReason(INT4 i4FsLldpMedStatsIfIndex ,
          UINT4 u4FsLldpMedStatsDestMACAddress ,
          tSNMP_OCTET_STRING_TYPE * pRetValFsLldpMedStatsRxLocationDiscardedReason)
  {
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;
    UINT1               au1ReasonCode[LLDP_MED_MAX_DISPLAY_STRING];

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&au1ReasonCode, 0, LLDP_MED_MAX_DISPLAY_STRING);
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The below line have to be removed once implementation is completed for Reason code */
      STRNCPY (au1ReasonCode,
              LLDP_MED_DEFAULT_STRING, STRLEN(LLDP_MED_DEFAULT_STRING));
      au1ReasonCode[STRLEN(LLDP_MED_DEFAULT_STRING)] = '\0';
      MEMCPY(pRetValFsLldpMedStatsRxLocationDiscardedReason->pu1_OctetList,
              au1ReasonCode, STRLEN(au1ReasonCode));
      pRetValFsLldpMedStatsRxLocationDiscardedReason->i4_Length =
          (INT4) STRLEN(au1ReasonCode);
      return SNMP_SUCCESS;
  }
  /****************************************************************************
   Function    :  nmhGetFsLldpMedStatsRxExPowerDiscardedReason
   Input       :  The Indices
                  FsLldpMedStatsIfIndex
                  FsLldpMedStatsDestMACAddress

                  The Object
                  retValFsLldpMedStatsRxExPowerDiscardedReason
   Output      :  The Get Low Lev Routine Take the Indices &
                  store the Value requested in the Return val.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  ****************************************************************************/
  INT1 nmhGetFsLldpMedStatsRxExPowerDiscardedReason(INT4 i4FsLldpMedStatsIfIndex ,
          UINT4 u4FsLldpMedStatsDestMACAddress ,
          tSNMP_OCTET_STRING_TYPE * pRetValFsLldpMedStatsRxExPowerDiscardedReason)
  {
    tLldpv2DestAddrTbl *pDestTbl = NULL;
    tLldpLocPortInfo   *pLldpLocalPortInfo = NULL;
    tLldpv2DestAddrTbl  DestTbl;
    tLldpLocPortInfo    LldpLocPortInfo;
    UINT4               u4LocPort = 0;
    UINT1               au1ReasonCode[LLDP_MED_MAX_DISPLAY_STRING];

    MEMSET (&DestTbl, 0, sizeof (tLldpv2DestAddrTbl));
    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));
    MEMSET (&au1ReasonCode, 0, LLDP_MED_MAX_DISPLAY_STRING);
    DestTbl.u4LlldpV2DestAddrTblIndex = u4FsLldpMedStatsDestMACAddress;
    pDestTbl =
        (tLldpv2DestAddrTbl *) RBTreeGet (gLldpGlobalInfo.
                                          Lldpv2DestMacAddrTblRBTree,
                                          (tRBElem *) & DestTbl);

    if (pDestTbl == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LldpUtlGetLocalPort (i4FsLldpMedStatsIfIndex,
                pDestTbl->Lldpv2DestMacAddress, &u4LocPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    LldpLocPortInfo.u4LocPortNum = u4LocPort;

    pLldpLocalPortInfo =
        (tLldpLocPortInfo *) RBTreeGet (gLldpGlobalInfo.
                                        LldpLocPortAgentInfoRBTree,
                                        (tRBElem *) & LldpLocPortInfo);
    if (pLldpLocalPortInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The below line have to be removed once implementation is completed for Reason code */
      STRNCPY (au1ReasonCode,
              LLDP_MED_DEFAULT_STRING, STRLEN(LLDP_MED_DEFAULT_STRING));
      au1ReasonCode[STRLEN(LLDP_MED_DEFAULT_STRING)] = '\0';
      MEMCPY(pRetValFsLldpMedStatsRxExPowerDiscardedReason->pu1_OctetList,
              au1ReasonCode, STRLEN(au1ReasonCode));
      pRetValFsLldpMedStatsRxExPowerDiscardedReason->i4_Length =
          (INT4) STRLEN(au1ReasonCode);
      return SNMP_SUCCESS;
  }

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpMedClearStats
 Input       :  The Indices

                The Object 
                retValFsLldpMedClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpMedClearStats(INT4 *pi4RetValFsLldpMedClearStats)
{
    *pi4RetValFsLldpMedClearStats = gLldpGlobalInfo.bMedClearStats;
	return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLldpMedClearStats
 Input       :  The Indices

                The Object 
                setValFsLldpMedClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsLldpMedClearStats(INT4 i4SetValFsLldpMedClearStats)
{
    tLldpLocPortInfo   *pLldpLocPortInfo = NULL;
    tLldpLocPortInfo    LldpLocPortInfo;


    MEMSET (&LldpLocPortInfo, 0, sizeof (tLldpLocPortInfo));

    if (i4SetValFsLldpMedClearStats == LLDP_MED_SET_CLEAR_COUNTER)
    {

        gLldpGlobalInfo.bMedClearStats = LLDP_MED_SET_CLEAR_COUNTER;
        pLldpLocPortInfo = (tLldpLocPortInfo *)
            RBTreeGetFirst (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree);

        /* Clear LLDP-MED statistics info for every ports */
        while (pLldpLocPortInfo != NULL)
        {
            /* Clear LLDP-MED statistics info */
            pLldpLocPortInfo->MedStatsInfo.u4TxMedFramesTotal = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedFramesTotal = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedFramesDiscardedTotal = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedTLVsDiscardedTotal = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedCapTLVsDiscarded = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedPolicyTLVsDiscarded = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedInventoryTLVsDiscarded = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedLocationTLVsDiscarded = 0;
            pLldpLocPortInfo->MedStatsInfo.u4RxMedExPowerMDITLVsDiscarded = 0;

            LldpLocPortInfo.u4LocPortNum = pLldpLocPortInfo->u4LocPortNum;
            pLldpLocPortInfo = (tLldpLocPortInfo *)
                RBTreeGetNext (gLldpGlobalInfo.LldpLocPortAgentInfoRBTree,
                        &LldpLocPortInfo, LldpAgentInfoUtlRBCmpInfo);
        }
        gLldpGlobalInfo.bMedClearStats = LLDP_MED_NO_SET_CLEAR_COUNTER;
    }
	return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLldpMedClearStats
 Input       :  The Indices

                The Object 
                testValFsLldpMedClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsLldpMedClearStats(UINT4 *pu4ErrorCode , INT4
        i4TestValFsLldpMedClearStats)
{
    if ((i4TestValFsLldpMedClearStats != LLDP_MED_SET_CLEAR_COUNTER) &&
        (i4TestValFsLldpMedClearStats != LLDP_MED_NO_SET_CLEAR_COUNTER ))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
	return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsLldpMedClearStats
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsLldpMedClearStats(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpXMedRemLocationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpXMedRemLocationTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXMedRemLocationSubtype
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpXMedRemLocationTable(UINT4 u4LldpV2RemTimeMark , 
        INT4 i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , 
        INT4 i4LldpV2RemIndex , INT4 i4LldpXMedRemLocationSubtype)
{
    if (LldpMedUtlValIndexRemLocationTbl
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             i4LldpXMedRemLocationSubtype) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpXMedRemLocationTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXMedRemLocationSubtype
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpXMedRemLocationTable(UINT4 *pu4LldpV2RemTimeMark , 
        INT4 *pi4LldpV2RemLocalIfIndex , UINT4 *pu4LldpV2RemLocalDestMACAddress , 
        INT4 *pi4LldpV2RemIndex , INT4 *pi4LldpXMedRemLocationSubtype)
{
    if (LldpMedUtlGetFirstIndexRemLocationTbl
            (pu4LldpV2RemTimeMark,
             pi4LldpV2RemLocalIfIndex,
             pu4LldpV2RemLocalDestMACAddress,
             pi4LldpV2RemIndex,
             pi4LldpXMedRemLocationSubtype) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpXMedRemLocationTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXMedRemLocationSubtype
                nextLldpXMedRemLocationSubtype
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpXMedRemLocationTable(UINT4 u4LldpV2RemTimeMark ,
        UINT4 *pu4NextLldpV2RemTimeMark  , INT4 i4LldpV2RemLocalIfIndex ,
        INT4 *pi4NextLldpV2RemLocalIfIndex  , UINT4 u4LldpV2RemLocalDestMACAddress ,
        UINT4 *pu4NextLldpV2RemLocalDestMACAddress  , INT4 i4LldpV2RemIndex ,
        INT4 *pi4NextLldpV2RemIndex  , INT4 i4LldpXMedRemLocationSubtype ,
        INT4 *pi4NextLldpXMedRemLocationSubtype )
{
    if (LldpMedUtlGetNextIndexRemLocationTbl
            (u4LldpV2RemTimeMark,
             pu4NextLldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             pi4NextLldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             pu4NextLldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             pi4NextLldpV2RemIndex,
             i4LldpXMedRemLocationSubtype,
             pi4NextLldpXMedRemLocationSubtype) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemLocationInfo
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXMedRemLocationSubtype

                The Object 
                retValFsLldpXMedRemLocationInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemLocationInfo(UINT4 u4LldpV2RemTimeMark , 
        INT4 i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , 
        INT4 i4LldpV2RemIndex , INT4 i4LldpXMedRemLocationSubtype , 
        tSNMP_OCTET_STRING_TYPE * pRetValFsLldpXMedRemLocationInfo)
{
    if (LldpMedUtlGetRemLocationInfo
            (u4LldpV2RemTimeMark,
             i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex,
             i4LldpXMedRemLocationSubtype,
             pRetValFsLldpXMedRemLocationInfo) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsLldpXMedRemXPoEPDTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsLldpXMedRemXPoEPDTable(UINT4 u4LldpV2RemTimeMark , 
        INT4 i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , 
        INT4 i4LldpV2RemIndex)
{
    if (LldpMedUtlValIndexRemCapInvTbl(u4LldpV2RemTimeMark,
                                       i4LldpV2RemLocalIfIndex,
                                       u4LldpV2RemLocalDestMACAddress,
                                       i4LldpV2RemIndex) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsLldpXMedRemXPoEPDTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsLldpXMedRemXPoEPDTable(UINT4 *pu4LldpV2RemTimeMark , 
        INT4 *pi4LldpV2RemLocalIfIndex , UINT4 *pu4LldpV2RemLocalDestMACAddress , 
        INT4 *pi4LldpV2RemIndex)
{
    if (LldpMedUtlGetFirstIndexRemCapTbl(pu4LldpV2RemTimeMark,
                                         pi4LldpV2RemLocalIfIndex,
                                         pu4LldpV2RemLocalDestMACAddress,
                                         pi4LldpV2RemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsLldpXMedRemXPoEPDTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsLldpXMedRemXPoEPDTable(UINT4 u4LldpV2RemTimeMark ,
        UINT4 *pu4NextLldpV2RemTimeMark  , INT4 i4LldpV2RemLocalIfIndex ,
        INT4 *pi4NextLldpV2RemLocalIfIndex  , UINT4 u4LldpV2RemLocalDestMACAddress ,
        UINT4 *pu4NextLldpV2RemLocalDestMACAddress  , INT4 i4LldpV2RemIndex ,INT4 *pi4NextLldpV2RemIndex )
{
    if (LldpMedUtlGetNextIndexRemCapTbl(u4LldpV2RemTimeMark,
                pu4NextLldpV2RemTimeMark,
                i4LldpV2RemLocalIfIndex,
                pi4NextLldpV2RemLocalIfIndex,
                u4LldpV2RemLocalDestMACAddress,
                pu4NextLldpV2RemLocalDestMACAddress,
                i4LldpV2RemIndex,
                pi4NextLldpV2RemIndex) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemXPoEDeviceType
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemXPoEDeviceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemXPoEDeviceType(UINT4 u4LldpV2RemTimeMark , INT4 i4LldpV2RemLocalIfIndex ,
        UINT4 u4LldpV2RemLocalDestMACAddress , INT4 i4LldpV2RemIndex , 
        INT4 *pi4RetValFsLldpXMedRemXPoEDeviceType)
{
    if (LldpMedUtlGetRemPoEDeviceType(u4LldpV2RemTimeMark,
                i4LldpV2RemLocalIfIndex,
                u4LldpV2RemLocalDestMACAddress,
                i4LldpV2RemIndex,
                pi4RetValFsLldpXMedRemXPoEDeviceType) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemXPoEPDPowerReq
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemXPoEPDPowerReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemXPoEPDPowerReq(UINT4 u4LldpV2RemTimeMark , 
        INT4 i4LldpV2RemLocalIfIndex , UINT4 u4LldpV2RemLocalDestMACAddress , 
        INT4 i4LldpV2RemIndex , UINT4 *pu4RetValFsLldpXMedRemXPoEPDPowerReq)
{
    if (LldpMedUtlGetRemPoEPDPowerReq(u4LldpV2RemTimeMark,
                                      i4LldpV2RemLocalIfIndex,
                                      u4LldpV2RemLocalDestMACAddress,
                                      i4LldpV2RemIndex,
                                      pu4RetValFsLldpXMedRemXPoEPDPowerReq)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemXPoEPDPowerSource
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemXPoEPDPowerSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemXPoEPDPowerSource(UINT4 u4LldpV2RemTimeMark , INT4 i4LldpV2RemLocalIfIndex , 
        UINT4 u4LldpV2RemLocalDestMACAddress , INT4 i4LldpV2RemIndex , INT4 *pi4RetValFsLldpXMedRemXPoEPDPowerSource)
{
    if (LldpMedUtlGetRemPoEPDPowerSource(u4LldpV2RemTimeMark,
                                         i4LldpV2RemLocalIfIndex,
                                         u4LldpV2RemLocalDestMACAddress,
                                         i4LldpV2RemIndex,
                                         pi4RetValFsLldpXMedRemXPoEPDPowerSource)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsLldpXMedRemXPoEPDPowerPriority
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFsLldpXMedRemXPoEPDPowerPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsLldpXMedRemXPoEPDPowerPriority(UINT4 u4LldpV2RemTimeMark , INT4 i4LldpV2RemLocalIfIndex , 
        UINT4 u4LldpV2RemLocalDestMACAddress , INT4 i4LldpV2RemIndex , INT4 *pi4RetValFsLldpXMedRemXPoEPDPowerPriority)
{
    if (LldpMedUtlGetRemPoEPDPowerPriority(u4LldpV2RemTimeMark,
                                           i4LldpV2RemLocalIfIndex,
                                           u4LldpV2RemLocalDestMACAddress,
                                           i4LldpV2RemIndex,
                                           pi4RetValFsLldpXMedRemXPoEPDPowerPriority)
            != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
